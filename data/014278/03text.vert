<s>
Zoopark	zoopark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
</s>
<s>
ZOO	zoo	k1gFnSc1
PARK	park	k1gInSc1
Vyškov	Vyškov	k1gInSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Vyškov	Vyškov	k1gInSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Sokolíček	sokolíček	k1gMnSc1
Datum	datum	k1gInSc4
založení	založení	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1965	#num#	k4
Ředitel	ředitel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Dagmar	Dagmar	k1gFnSc1
Nepeřená	Nepeřený	k2eAgFnSc1d1
Zaměření	zaměření	k1gNnSc4
chovu	chov	k1gInSc3
</s>
<s>
domácí	domácí	k2eAgNnPc1d1
a	a	k8xC
hospodářská	hospodářský	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
Počet	počet	k1gInSc1
druhů	druh	k1gInPc2
</s>
<s>
142	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
zvířat	zvíře	k1gNnPc2
</s>
<s>
520	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
7	#num#	k4
ha	ha	kA
Členství	členství	k1gNnSc1
</s>
<s>
EARAZA	EARAZA	kA
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Zoopark	zoopark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Zoo	zoo	k1gFnSc1
park	park	k1gInSc1
Vyškov	Vyškov	k1gInSc1
je	být	k5eAaImIp3nS
zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
okresním	okresní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Vyškově	Vyškov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
chová	chovat	k5eAaImIp3nS
480	#num#	k4
zvířat	zvíře	k1gNnPc2
v	v	k7c6
82	#num#	k4
druzích	druh	k1gInPc6
na	na	k7c6
ploše	plocha	k1gFnSc6
4	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozována	provozován	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
jako	jako	k9
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
ZOO	zoo	k1gFnSc2
PARK	park	k1gInSc1
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
především	především	k9
na	na	k7c4
chov	chov	k1gInSc4
domácích	domácí	k1gMnPc2
i	i	k8xC
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chová	chovat	k5eAaImIp3nS
1197	#num#	k4
zvířat	zvíře	k1gNnPc2
ve	v	k7c6
123	#num#	k4
druzích	druh	k1gInPc6
nebo	nebo	k8xC
plemenech	plemeno	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Původní	původní	k2eAgInSc1d1
zookoutek	zookoutek	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
dubnu	duben	k1gInSc6
1965	#num#	k4
Zdeňkem	Zdeněk	k1gMnSc7
Sokolíčkem	sokolíček	k1gMnSc7
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1965	#num#	k4
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Koutku	koutek	k1gInSc2
živé	živý	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
na	na	k7c6
ploše	plocha	k1gFnSc6
30	#num#	k4
×	×	k?
15	#num#	k4
metrů	metr	k1gInPc2
za	za	k7c2
pomoci	pomoc	k1gFnSc2
brigádníků	brigádník	k1gMnPc2
a	a	k8xC
zápalu	zápal	k1gInSc2
pro	pro	k7c4
věc	věc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
ploše	plocha	k1gFnSc6
bylo	být	k5eAaImAgNnS
umístěno	umístit	k5eAaPmNgNnS
přibližně	přibližně	k6eAd1
20	#num#	k4
druhů	druh	k1gInPc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
se	se	k3xPyFc4
zaměřovala	zaměřovat	k5eAaImAgFnS
převážně	převážně	k6eAd1
na	na	k7c4
primitivní	primitivní	k2eAgNnPc4d1
domácí	domácí	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrada	zahrada	k1gFnSc1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
procházela	procházet	k5eAaImAgFnS
obdobím	období	k1gNnSc7
rozkvětu	rozkvět	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
doslova	doslova	k6eAd1
živořením	živoření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
výstavba	výstavba	k1gFnSc1
nové	nový	k2eAgFnSc2d1
části	část	k1gFnSc2
zookoutku	zookoutek	k1gInSc2
na	na	k7c6
pozemku	pozemek	k1gInSc6
školního	školní	k2eAgInSc2d1
statku	statek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zookoutek	Zookoutek	k1gInSc1
tak	tak	k6eAd1
zvětšil	zvětšit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
plochu	plocha	k1gFnSc4
na	na	k7c4
1,6	1,6	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zookoutek	zookoutek	k1gInSc1
stal	stát	k5eAaPmAgInS
na	na	k7c4
čas	čas	k1gInSc4
zimovištěm	zimoviště	k1gNnSc7
cirkusových	cirkusový	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejkritičtějším	kritický	k2eAgNnSc7d3
byl	být	k5eAaImAgInS
rok	rok	k1gInSc4
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rozhodovalo	rozhodovat	k5eAaImAgNnS
o	o	k7c6
celém	celý	k2eAgNnSc6d1
uzavření	uzavření	k1gNnSc6
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Příchodem	příchod	k1gInSc7
nového	nový	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
Josefa	Josef	k1gMnSc2
Kachlíka	Kachlík	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
se	se	k3xPyFc4
realizoval	realizovat	k5eAaBmAgInS
záměr	záměr	k1gInSc1
vybudovat	vybudovat	k5eAaPmF
z	z	k7c2
této	tento	k3xDgFnSc2
zahrady	zahrada	k1gFnSc2
moderní	moderní	k2eAgFnSc2d1
zoo	zoo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
jeho	jeho	k3xOp3gInSc7
počinem	počin	k1gInSc7
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
Babiččina	babiččin	k2eAgInSc2d1
dvorečku	dvoreček	k1gInSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
vzorem	vzor	k1gInSc7
k	k	k7c3
této	tento	k3xDgFnSc3
stavbě	stavba	k1gFnSc3
bylo	být	k5eAaImAgNnS
berlínské	berlínský	k2eAgNnSc1d1
zoo	zoo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
něm	on	k3xPp3gInSc6
přichází	přicházet	k5eAaImIp3nS
návštěvníci	návštěvník	k1gMnPc1
do	do	k7c2
přímého	přímý	k2eAgInSc2d1
kontaktu	kontakt	k1gInSc2
s	s	k7c7
domácími	domácí	k2eAgNnPc7d1
zvířaty	zvíře	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
</s>
<s>
Úpravy	úprava	k1gFnPc1
příjezdové	příjezdový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
k	k	k7c3
zooparku	zoopark	k1gInSc3
2007	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
celého	celý	k2eAgInSc2d1
areálu	areál	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnula	zahrnout	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
vstupní	vstupní	k2eAgInSc4d1
areál	areál	k1gInSc4
s	s	k7c7
prodejnou	prodejna	k1gFnSc7
suvenýrů	suvenýr	k1gInPc2
a	a	k8xC
sociálním	sociální	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
vystavěny	vystavěn	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
výběhy	výběh	k1gInPc1
pro	pro	k7c4
domácí	domácí	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálu	areál	k1gInSc6
bylo	být	k5eAaImAgNnS
vybudovano	vybudovana	k1gFnSc5
dětské	dětský	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
<g/>
,	,	kIx,
venkovní	venkovní	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
s	s	k7c7
občerstvením	občerstvení	k1gNnSc7
a	a	k8xC
restaurace	restaurace	k1gFnSc1
s	s	k7c7
ubytovacími	ubytovací	k2eAgInPc7d1
prostory	prostor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vstupem	vstup	k1gInSc7
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
rozlehlé	rozlehlý	k2eAgNnSc1d1
parkoviště	parkoviště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
celé	celý	k2eAgFnSc2d1
zoo	zoo	k1gFnSc2
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS
z	z	k7c2
1,6	1,6	k4
ha	ha	kA
na	na	k7c4
4	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
proběhly	proběhnout	k5eAaPmAgFnP
úpravy	úprava	k1gFnPc1
celého	celý	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
zooparku	zoopark	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
vystavena	vystavit	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
přímo	přímo	k6eAd1
k	k	k7c3
zooparku	zoopark	k1gInSc3
<g/>
,	,	kIx,
od	od	k7c2
křižovatky	křižovatka	k1gFnSc2
od	od	k7c2
obchodního	obchodní	k2eAgInSc2d1
domu	dům	k1gInSc2
Lidl	Lidla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmizet	zmizet	k5eAaPmF
mělo	mít	k5eAaImAgNnS
zbořeniště	zbořeniště	k1gNnSc1
mezi	mezi	k7c7
hřbitovem	hřbitov	k1gInSc7
a	a	k8xC
zámeckou	zámecký	k2eAgFnSc7d1
zahradou	zahrada	k1gFnSc7
u	u	k7c2
příjezdové	příjezdový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
k	k	k7c3
zoo	zoo	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vysazena	vysadit	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
zeleň	zeleň	k1gFnSc1
a	a	k8xC
celé	celý	k2eAgNnSc1d1
parkoviště	parkoviště	k1gNnSc1
před	před	k7c7
zooparkem	zoopark	k1gInSc7
se	se	k3xPyFc4
dočkalo	dočkat	k5eAaPmAgNnS
atraktivnějšího	atraktivní	k2eAgInSc2d2
vzhledu	vzhled	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
návštěvníků	návštěvník	k1gMnPc2
</s>
<s>
33	#num#	k4
508	#num#	k4
</s>
<s>
36	#num#	k4
476	#num#	k4
</s>
<s>
180	#num#	k4
000	#num#	k4
</s>
<s>
241	#num#	k4
376	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
navštívilo	navštívit	k5eAaPmAgNnS
Zoopark	zoopark	k1gInSc4
Vyškov	Vyškov	k1gInSc1
33	#num#	k4
508	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
oproti	oproti	k7c3
roku	rok	k1gInSc3
2005	#num#	k4
návštěvnost	návštěvnost	k1gFnSc1
zvětšila	zvětšit	k5eAaPmAgFnS
pětkrát	pětkrát	k6eAd1
<g/>
:	:	kIx,
z	z	k7c2
36	#num#	k4
476	#num#	k4
na	na	k7c4
180	#num#	k4
000	#num#	k4
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
unikátním	unikátní	k2eAgMnSc7d1
DinoParkem	DinoParek	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
tak	tak	k9
Zoopark	zoopark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
stal	stát	k5eAaPmAgInS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejnavštěvovanější	navštěvovaný	k2eAgInSc1d3
atrakcí	atrakce	k1gFnSc7
na	na	k7c6
Jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
Zoopark	zoopark	k1gInSc4
a	a	k8xC
DinoPark	DinoPark	k1gInSc4
navštívilo	navštívit	k5eAaPmAgNnS
historicky	historicky	k6eAd1
rekordních	rekordní	k2eAgInPc2d1
241	#num#	k4
376	#num#	k4
platících	platící	k2eAgMnPc2d1
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výčet	výčet	k1gInSc1
chovných	chovný	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
</s>
<s>
Skot	Skot	k1gMnSc1
stepní	stepní	k2eAgFnSc2d1
uherský	uherský	k2eAgInSc4d1
(	(	kIx(
<g/>
Bos	bos	k2eAgInSc4d1
primigenius	primigenius	k1gInSc4
f.	f.	k?
taurus	taurus	k1gInSc1
)	)	kIx)
</s>
<s>
Hlodavci	hlodavec	k1gMnPc1
–	–	k?
morče	morče	k1gNnSc1
domácí	domácí	k1gMnSc1
(	(	kIx(
<g/>
Cavia	Cavia	k1gFnSc1
aperea	aperea	k1gMnSc1
f.	f.	k?
porcellus	porcellus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
18	#num#	k4
plemen	plemeno	k1gNnPc2
králíka	králík	k1gMnSc2
domácího	domácí	k1gMnSc2
(	(	kIx(
<g/>
Oryctolagus	Oryctolagus	k1gMnSc1
cuniculus	cuniculus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Lichokopytníci	lichokopytník	k1gMnPc1
–	–	k?
kůň	kůň	k1gMnSc1
shirský	shirský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Equus	Equus	k1gMnSc1
caballus	caballus	k1gMnSc1
f.	f.	k?
caballus	caballus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osel	osel	k1gMnSc1
domácí	domácí	k1gMnSc1
(	(	kIx(
<g/>
Equus	Equus	k1gMnSc1
africanus	africanus	k1gMnSc1
f.	f.	k?
asinus	asinus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pony	pony	k1gMnSc1
shetlandský	shetlandský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Equus	Equus	k1gMnSc1
caballus	caballus	k1gMnSc1
f.	f.	k?
caballus	caballus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kůň	kůň	k1gMnSc1
fjordský	fjordský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Equus	Equus	k1gMnSc1
caballus	caballus	k1gMnSc1
f.	f.	k?
caballus	caballus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Primáti	primát	k1gMnPc1
–	–	k?
lemur	lemur	k1gMnSc1
kata	kat	k1gMnSc2
(	(	kIx(
<g/>
Lemur	lemur	k1gMnSc1
catta	catta	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Sudokopytníci	sudokopytník	k1gMnPc1
–	–	k?
buvol	buvol	k1gMnSc1
domácí	domácí	k1gMnSc1
(	(	kIx(
<g/>
Bubalus	Bubalus	k1gMnSc1
arnee	arne	k1gFnSc2
f.	f.	k?
bubalis	bubalis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koza	koza	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
bílá	bílý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Capra	Capra	k1gFnSc1
aegagrus	aegagrus	k1gMnSc1
f.	f.	k?
hircus	hircus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koza	koza	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
kamerunská	kamerunský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Capra	Capra	k1gFnSc1
aegagrus	aegagrus	k1gMnSc1
f.	f.	k?
hircus	hircus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koza	koza	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
karpatská	karpatský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Capra	Capra	k1gFnSc1
aegagrus	aegagrus	k1gMnSc1
f.	f.	k?
hircus	hircus	k1gMnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
koza	koza	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
kašmírská	kašmírský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Capra	Capra	k1gFnSc1
aegagrus	aegagrus	k1gMnSc1
f.	f.	k?
hircus	hircus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koza	koza	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
walliská	walliská	k1gFnSc1
(	(	kIx(
<g/>
Capra	Capra	k1gMnSc1
aegagrus	aegagrus	k1gMnSc1
f.	f.	k?
hircus	hircus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovce	ovce	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
romanovská	romanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
ammon	ammon	k1gInSc1
f.	f.	k?
aries	aries	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovce	ovce	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
cápová	cápová	k1gFnSc1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
ammon	ammon	k1gMnSc1
f.	f.	k?
aries	aries	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovce	ovce	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
kamerunská	kamerunský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
ammon	ammon	k1gInSc1
f.	f.	k?
aries	aries	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovce	ovce	k1gFnSc2
domácí	domácí	k2eAgFnSc2d1
vřesovištní	vřesovištní	k2eAgInSc4d1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc4
ammon	ammona	k1gFnPc2
f.	f.	k?
aries	aries	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovce	ovce	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
quessantská	quessantský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
ammon	ammon	k1gInSc1
f.	f.	k?
aries	aries	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lama	lama	k1gFnSc1
krotká	krotký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Lama	lama	k1gFnSc1
guanicoe	guanico	k1gFnSc2
f.	f.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
glama	glama	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prase	prase	k1gNnSc1
domácí	domácí	k1gMnSc1
mangalica	mangalica	k1gMnSc1
(	(	kIx(
<g/>
Sus	Sus	k1gMnSc1
scrofa	scrof	k1gMnSc2
f.	f.	k?
domestica	domesticus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skot	skot	k1gInSc1
skotský	skotský	k1gInSc1
náhorní	náhorní	k2eAgInSc1d1
(	(	kIx(
<g/>
Bos	bos	k2eAgInSc1d1
primigenius	primigenius	k1gInSc1
f.	f.	k?
taurus	taurus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skot	skot	k1gInSc1
stepní	stepní	k2eAgInSc1d1
uherský	uherský	k2eAgInSc1d1
(	(	kIx(
<g/>
Bos	bos	k2eAgInSc1d1
primigenius	primigenius	k1gInSc1
f.	f.	k?
taurus	taurus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velbloud	velbloud	k1gMnSc1
dvouhrbý	dvouhrbý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Camelus	Camelus	k1gMnSc1
bactrianus	bactrianus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
watussi	watusse	k1gFnSc4
(	(	kIx(
<g/>
Bos	bos	k2eAgInSc1d1
primigenius	primigenius	k1gInSc1
f.	f.	k?
taurus	taurus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zebu	zábst	k5eAaImIp1nS
zakrslý	zakrslý	k2eAgInSc1d1
(	(	kIx(
<g/>
Bos	bos	k2eAgInSc1d1
primigenius	primigenius	k1gInSc1
f.	f.	k?
taurus	taurus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velbloud	velbloud	k1gMnSc1
jednohrbý	jednohrbý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Camelus	Camelus	k1gMnSc1
dromedarius	dromedarius	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ryby	Ryby	k1gFnPc1
–	–	k?
karas	karas	k1gMnSc1
stříbřitý	stříbřitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Carassius	Carassius	k1gMnSc1
auratus	auratus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapr	kapr	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
nishikigoi	nishikigo	k1gFnPc4
(	(	kIx(
<g/>
Cyprinus	Cyprinus	k1gMnSc1
carpio	carpio	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Želvy	želva	k1gFnPc1
–	–	k?
želva	želva	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Trachemys	Trachemys	k1gInSc1
scripta	script	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Ptáci	pták	k1gMnPc1
</s>
<s>
Holubník	holubník	k1gInSc1
na	na	k7c6
Babiččině	babiččin	k2eAgInSc6d1
dvorečku	dvoreček	k1gInSc6
</s>
<s>
brodiví	brodivý	k2eAgMnPc1d1
–	–	k?
čáp	čáp	k1gMnSc1
bílý	bílý	k1gMnSc1
(	(	kIx(
<g/>
ciconia	ciconium	k1gNnSc2
ciconia	ciconium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
dravci	dravec	k1gMnPc1
–	–	k?
káně	káně	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Buteo	Buteo	k1gMnSc1
buteo	buteo	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
poštolka	poštolka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Falco	Falco	k1gMnSc1
tinnunculus	tinnunculus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
měkkozobí	měkkozobit	k5eAaPmIp3nS
–	–	k?
holub	holub	k1gMnSc1
domácí	domácí	k1gMnSc1
(	(	kIx(
<g/>
Columba	Columba	k1gMnSc1
livia	livius	k1gMnSc2
f.	f.	k?
domestica	domesticus	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
nanduové	nandu	k1gMnPc1
–	–	k?
nandu	nandu	k1gMnSc1
pampový	pampový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Rhea	Rhe	k2eAgFnSc1d1
americana	americana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
papoušci	papoušek	k1gMnPc1
–	–	k?
papoušek	papoušek	k1gMnSc1
vlnkovaný	vlnkovaný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Melopsittacus	Melopsittacus	k1gMnSc1
undulatus	undulatus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
pěvci	pěvec	k1gMnPc1
–	–	k?
krkavec	krkavec	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Corvus	Corvus	k1gInSc1
corax	corax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
sovy	sova	k1gFnPc1
–	–	k?
puštík	puštík	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
výr	výr	k1gMnSc1
velký	velký	k2eAgInSc4d1
</s>
<s>
vrubozobí	vrubozobí	k1gMnPc1
–	–	k?
husa	husa	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
landéská	landéská	k1gFnSc1
(	(	kIx(
<g/>
Anser	Anser	k1gMnSc1
anser	anser	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
husa	husa	k1gFnSc1
velká	velká	k1gFnSc1
(	(	kIx(
<g/>
Anser	Anser	k1gMnSc1
anser	anser	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kachna	kachna	k1gFnSc1
domácí	domácí	k1gMnSc1
indický	indický	k2eAgMnSc1d1
běžec	běžec	k1gMnSc1
(	(	kIx(
<g/>
Anas	Anas	k1gInSc1
platyrhynchos	platyrhynchos	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
labuť	labuť	k1gFnSc1
velká	velká	k1gFnSc1
(	(	kIx(
<g/>
Cygnus	Cygnus	k1gMnSc1
olor	olor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pižmovka	pižmovka	k1gFnSc1
velká	velká	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cairina	Cairin	k2eAgFnSc1d1
moschata	moschata	k1gFnSc1
f.	f.	k?
domestica	domestica	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
husa	husa	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
labutí	labuť	k1gFnSc7
(	(	kIx(
<g/>
Anser	Anser	k1gMnSc1
anser	anser	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kachnička	kachnička	k1gFnSc1
karolínská	karolínský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Aix	Aix	k1gFnSc1
sponsa	sponsa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kachnička	kachnička	k1gFnSc1
mandarinská	mandarinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Aix	Aix	k1gFnSc1
galericulata	galericule	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
hrabaví	hrabavý	k2eAgMnPc1d1
–	–	k?
bažant	bažant	k1gMnSc1
zlatý	zlatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Chrysolophus	Chrysolophus	k1gMnSc1
pictus	pictus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kur	kur	k1gMnSc1
domácí	domácí	k1gFnSc2
brahmánka	brahmánka	k1gFnSc1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
zlatá	zlatý	k2eAgFnSc1d1
kropenka	kropenka	k1gFnSc1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kur	kur	k1gMnSc1
domácí	domácí	k1gFnSc2
japonka	japonka	k1gFnSc1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
kočinka	kočinka	k1gFnSc1
zakrslá	zakrslý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kur	kur	k1gMnSc1
zakrslý	zakrslý	k2eAgMnSc1d1
rousný	rousný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
páv	páv	k1gMnSc1
korunkatý	korunkatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pavo	Pavo	k1gMnSc1
cristatus	cristatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
perlička	perlička	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Numida	Numida	k1gFnSc1
meleagris	meleagris	k1gFnSc2
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malajská	malajský	k2eAgFnSc1d1
bojovnice	bojovnice	k1gFnSc1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
kur	kur	k1gMnSc1
bankivský	bankivský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Gallus	Gallus	k1gMnSc1
gallus	gallus	k1gMnSc1
f.	f.	k?
domestica	domestica	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
</s>
<s>
Součástí	součást	k1gFnSc7
areálu	areál	k1gInSc2
zooparku	zoopark	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
i	i	k8xC
replika	replika	k1gFnSc1
hanáckého	hanácký	k2eAgInSc2d1
statku	statek	k1gInSc2
z	z	k7c2
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
expozicí	expozice	k1gFnSc7
tradičního	tradiční	k2eAgInSc2d1
venkovského	venkovský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
centrem	centrum	k1gNnSc7
environmentální	environmentální	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
jsou	být	k5eAaImIp3nP
interaktivní	interaktivní	k2eAgFnPc1d1
expozice	expozice	k1gFnPc1
s	s	k7c7
ekologickými	ekologický	k2eAgNnPc7d1
tématy	téma	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
zooparku	zoopark	k1gInSc2
je	být	k5eAaImIp3nS
i	i	k9
DinoPark	DinoPark	k1gInSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vyškovská	vyškovský	k2eAgFnSc1d1
hvězdárna	hvězdárna	k1gFnSc1
v	v	k7c6
přírodním	přírodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
Marchanického	Marchanický	k2eAgInSc2d1
hájku	hájek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Model	model	k1gInSc1
ještěra	ještěr	k1gMnSc2
v	v	k7c6
Dinoparku	Dinopark	k1gInSc6
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2007	#num#	k4
byl	být	k5eAaImAgInS
otevřen	otevřen	k2eAgInSc1d1
skanzen	skanzen	k1gInSc1
starých	starý	k2eAgInPc2d1
zemědělských	zemědělský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgInS
zoopark	zoopark	k1gInSc1
přijat	přijmout	k5eAaPmNgInS
do	do	k7c2
Euroasijské	euroasijský	k2eAgFnSc2d1
regionální	regionální	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
zoologických	zoologický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
byla	být	k5eAaImAgFnS
zoo	zoo	k1gFnSc1
udělena	udělit	k5eAaPmNgFnS
cena	cena	k1gFnSc1
Spokojený	spokojený	k2eAgMnSc1d1
zákazník	zákazník	k1gMnSc1
jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
dětské	dětský	k2eAgFnPc4d1
atrakce	atrakce	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Babiččin	babiččin	k2eAgInSc1d1
dvoreček	dvoreček	k1gInSc1
s	s	k7c7
volným	volný	k2eAgInSc7d1
vstupem	vstup	k1gInSc7
mezi	mezi	k7c4
domácí	domácí	k2eAgFnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7
chovného	chovný	k2eAgNnSc2d1
stáda	stádo	k1gNnSc2
koní	kůň	k1gMnPc2
fjordských	fjordský	k2eAgMnPc2d1
byl	být	k5eAaImAgInS
hřebec	hřebec	k1gMnSc1
Čaromír	Čaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
Tři	tři	k4xCgInPc4
oříšky	oříšek	k1gInPc4
pro	pro	k7c4
popelku	popelka	k1gFnSc4
na	na	k7c6
něm	on	k3xPp3gNnSc6
jezdil	jezdit	k5eAaImAgMnS
herec	herec	k1gMnSc1
Jan	Jan	k1gMnSc1
Libíček	Libíček	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
obrázek	obrázek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zoopark	zoopark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
má	mít	k5eAaImIp3nS
společný	společný	k2eAgInSc1d1
vchod	vchod	k1gInSc1
a	a	k8xC
vstupné	vstupné	k1gNnSc1
s	s	k7c7
DinoParkem	DinoParko	k1gNnSc7
Vyškov	Vyškov	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
návštěvníci	návštěvník	k1gMnPc1
procházet	procházet	k5eAaImF
areálem	areál	k1gInSc7
s	s	k7c7
modely	model	k1gInPc7
pravěkých	pravěký	k2eAgMnPc2d1
ještěrů	ještěr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Unie	unie	k1gFnSc2
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
zoologických	zoologický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
2007	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.zoo-vyskov.cz	www.zoo-vyskov.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zoopark	zoopark	k1gInSc1
a	a	k8xC
Dinopark	Dinopark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
–	–	k?
informační	informační	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
sezóna	sezóna	k1gFnSc1
2007	#num#	k4
<g/>
↑	↑	k?
článek	článek	k1gInSc1
v	v	k7c6
Rovnosti	rovnost	k1gFnSc6
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.zoo-vyskov.cz	www.zoo-vyskov.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zpravodaj	zpravodaj	k1gInSc1
Zoopark	zoopark	k1gInSc1
a	a	k8xC
DinoPark	DinoPark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
Hanácký	hanácký	k2eAgInSc1d1
statek	statek	k1gInSc1
-	-	kIx~
Centrum	centrum	k1gNnSc1
environmentální	environmentální	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZOO	zoo	k1gNnSc7
Park	park	k1gInSc1
Vyškov	Vyškov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
www.dinopark.cz	www.dinopark.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Navštivte	navštívit	k5eAaPmRp2nP
Hvězdárnu	hvězdárna	k1gFnSc4
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdárna	hvězdárna	k1gFnSc1
Vyškov	Vyškov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zoopark	zoopark	k1gInSc1
Vyškov	Vyškov	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Zooparku	zoopark	k1gInSc2
Vyškov	Vyškov	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
DinoParku	DinoPark	k1gInSc2
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Zoo	zoo	k1gFnSc1
park	park	k1gInSc1
Vyškov	Vyškov	k1gInSc1
–	–	k?
Toulavá	toulavý	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2017-12-03	2017-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Lemuři	lemur	k1gMnPc1
kata	kata	k9
</s>
<s>
Krocan	krocan	k1gMnSc1
domácí	domácí	k2eAgFnSc2d1
bronzový	bronzový	k2eAgInSc4d1
(	(	kIx(
<g/>
Meleagris	Meleagris	k1gInSc4
gallopavo	gallopava	k1gFnSc5
f.	f.	k?
domestica	domesticum	k1gNnPc4
<g/>
)	)	kIx)
</s>
<s>
Lama	lama	k1gFnSc1
krotká	krotký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Lama	lama	k1gFnSc1
guanicoe	guanico	k1gMnSc2
f.	f.	k?
glama	glam	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Velbloud	velbloud	k1gMnSc1
dvouhrbý	dvouhrbý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Camelus	Camelus	k1gMnSc1
bactrianus	bactrianus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kůň	kůň	k1gMnSc1
shirský	shirský	k2eAgMnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2016900882	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
231145541830496600346	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Vyškov	Vyškov	k1gInSc1
|	|	kIx~
Živočichové	živočich	k1gMnPc1
</s>
