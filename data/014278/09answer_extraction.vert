replika	replika	k1gFnSc1
hanáckého	hanácký	k2eAgInSc2d1
statku	statek	k1gInSc2
z	z	k7c2
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
expozicí	expozice	k1gFnSc7
tradičního	tradiční	k2eAgInSc2d1
venkovského	venkovský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
centrem	centrum	k1gNnSc7
environmentální	environmentální	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
jsou	být	k5eAaImIp3nP
interaktivní	interaktivní	k2eAgFnPc1d1
expozice	expozice	k1gFnPc1
s	s	k7c7
ekologickými	ekologický	k2eAgNnPc7d1
tématy	téma	k1gNnPc7
