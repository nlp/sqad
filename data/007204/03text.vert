<s>
Výrazem	výraz	k1gInSc7	výraz
němý	němý	k2eAgInSc4d1	němý
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neměly	mít	k5eNaImAgFnP	mít
žádnou	žádný	k3yNgFnSc4	žádný
synchronizovanou	synchronizovaný	k2eAgFnSc4d1	synchronizovaná
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
stopu	stopa	k1gFnSc4	stopa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
němé	němý	k2eAgFnPc1d1	němá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zejména	zejména	k9	zejména
bez	bez	k7c2	bez
mluvených	mluvený	k2eAgInPc2d1	mluvený
dialogů	dialog	k1gInPc2	dialog
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
i	i	k9	i
bez	bez	k7c2	bez
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
ruchů	ruch	k1gInPc2	ruch
<g/>
,	,	kIx,	,
postrádaly	postrádat	k5eAaImAgFnP	postrádat
filmovou	filmový	k2eAgFnSc4d1	filmová
hudbu	hudba	k1gFnSc4	hudba
či	či	k8xC	či
filmový	filmový	k2eAgInSc4d1	filmový
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
natáčely	natáčet	k5eAaImAgInP	natáčet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
filmu	film	k1gInSc2	film
se	s	k7c7	s
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
zvukem	zvuk	k1gInSc7	zvuk
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
jako	jako	k8xC	jako
samotná	samotný	k2eAgFnSc1d1	samotná
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgFnPc1d1	technická
podmínky	podmínka	k1gFnPc1	podmínka
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc1	tento
umožnily	umožnit	k5eAaPmAgFnP	umožnit
až	až	k9	až
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
předělový	předělový	k2eAgInSc1d1	předělový
snímek	snímek	k1gInSc1	snímek
mezi	mezi	k7c7	mezi
němou	němý	k2eAgFnSc7d1	němá
a	a	k8xC	a
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
érou	éra	k1gFnSc7	éra
kinematografie	kinematografie	k1gFnSc2	kinematografie
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
označuje	označovat	k5eAaImIp3nS	označovat
film	film	k1gInSc4	film
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
značný	značný	k2eAgInSc4d1	značný
finanční	finanční	k2eAgInSc4d1	finanční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
jako	jako	k8xC	jako
němá	němý	k2eAgFnSc1d1	němá
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
málo	málo	k6eAd1	málo
mluvených	mluvený	k2eAgFnPc2d1	mluvená
a	a	k8xC	a
zpívaných	zpívaný	k2eAgFnPc2d1	zpívaná
pasáží	pasáž	k1gFnPc2	pasáž
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
i	i	k9	i
následující	následující	k2eAgInPc4d1	následující
nejranější	raný	k2eAgInPc4d3	nejranější
zvukové	zvukový	k2eAgInPc4d1	zvukový
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dekády	dekáda	k1gFnSc2	dekáda
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
celosvětově	celosvětově	k6eAd1	celosvětově
přestalo	přestat	k5eAaPmAgNnS	přestat
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
jako	jako	k8xS	jako
hlavního	hlavní	k2eAgInSc2d1	hlavní
kinematografického	kinematografický	k2eAgInSc2d1	kinematografický
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
neměly	mít	k5eNaImAgInP	mít
synchronní	synchronní	k2eAgInSc4d1	synchronní
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
slovního	slovní	k2eAgInSc2d1	slovní
doprovodu	doprovod	k1gInSc2	doprovod
filmu	film	k1gInSc2	film
plnily	plnit	k5eAaImAgInP	plnit
mezititulky	mezititulek	k1gInPc1	mezititulek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
různého	různý	k2eAgInSc2d1	různý
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Klasickými	klasický	k2eAgInPc7d1	klasický
mezititulky	mezititulek	k1gInPc7	mezititulek
byly	být	k5eAaImAgFnP	být
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obstarávaly	obstarávat	k5eAaImAgFnP	obstarávat
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
dialogy	dialog	k1gInPc1	dialog
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
či	či	k8xC	či
vyprávění	vyprávění	k1gNnSc1	vyprávění
klíčových	klíčový	k2eAgFnPc2d1	klíčová
částí	část	k1gFnPc2	část
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
používalo	používat	k5eAaImAgNnS	používat
diegetických	diegetický	k2eAgInPc2d1	diegetický
nápisů	nápis	k1gInPc2	nápis
a	a	k8xC	a
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Mezititulky	mezititulek	k1gInPc1	mezititulek
měly	mít	k5eAaImAgInP	mít
často	často	k6eAd1	často
grafickou	grafický	k2eAgFnSc4d1	grafická
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vizuálně	vizuálně	k6eAd1	vizuálně
doplňovala	doplňovat	k5eAaImAgFnS	doplňovat
či	či	k8xC	či
komentovala	komentovat	k5eAaBmAgFnS	komentovat
děj	děj	k1gInSc4	děj
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
"	"	kIx"	"
<g/>
němý	němý	k2eAgInSc1d1	němý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
promítání	promítání	k1gNnSc4	promítání
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
rozpoznána	rozpoznán	k2eAgFnSc1d1	rozpoznána
jako	jako	k8xC	jako
nepostradatelná	postradatelný	k2eNgFnSc1d1	nepostradatelná
<g/>
,	,	kIx,	,
spoluvytvářející	spoluvytvářející	k2eAgFnSc4d1	spoluvytvářející
atmosféru	atmosféra	k1gFnSc4	atmosféra
v	v	k7c6	v
sále	sál	k1gInSc6	sál
a	a	k8xC	a
poskytující	poskytující	k2eAgFnSc3d1	poskytující
divákům	divák	k1gMnPc3	divák
emocionální	emocionální	k2eAgInPc4d1	emocionální
podněty	podnět	k1gInPc7	podnět
při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Kina	kino	k1gNnPc1	kino
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
městech	město	k1gNnPc6	město
a	a	k8xC	a
předměstská	předměstský	k2eAgNnPc1d1	předměstské
kina	kino	k1gNnPc1	kino
měla	mít	k5eAaImAgNnP	mít
tradičně	tradičně	k6eAd1	tradičně
pianistu	pianista	k1gMnSc4	pianista
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
ozvučení	ozvučení	k1gNnSc3	ozvučení
gramofon	gramofon	k1gInSc1	gramofon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgNnP	začít
velká	velký	k2eAgNnPc1d1	velké
městská	městský	k2eAgNnPc1d1	Městské
kina	kino	k1gNnPc1	kino
využívat	využívat	k5eAaImF	využívat
dokonce	dokonce	k9	dokonce
celé	celý	k2eAgInPc1d1	celý
orchestry	orchestr	k1gInPc1	orchestr
s	s	k7c7	s
živými	živý	k2eAgMnPc7d1	živý
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Repertoár	repertoár	k1gInSc1	repertoár
pro	pro	k7c4	pro
rané	raný	k2eAgInPc4d1	raný
němé	němý	k2eAgInPc4d1	němý
filmy	film	k1gInPc4	film
byl	být	k5eAaImAgInS	být
jednak	jednak	k8xC	jednak
improvizovaný	improvizovaný	k2eAgInSc1d1	improvizovaný
a	a	k8xC	a
jednak	jednak	k8xC	jednak
sestávající	sestávající	k2eAgMnSc1d1	sestávající
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
běžné	běžný	k2eAgInPc1d1	běžný
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
býval	bývat	k5eAaImAgInS	bývat
hudební	hudební	k2eAgInSc1d1	hudební
doprovod	doprovod	k1gInSc1	doprovod
určený	určený	k2eAgInSc1d1	určený
a	a	k8xC	a
vybraný	vybraný	k2eAgInSc1d1	vybraný
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
napsal	napsat	k5eAaPmAgMnS	napsat
skladatel	skladatel	k1gMnSc1	skladatel
Joseph	Joseph	k1gMnSc1	Joseph
Carl	Carl	k1gMnSc1	Carl
Breil	Breil	k1gMnSc1	Breil
původní	původní	k2eAgFnSc4d1	původní
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
D.	D.	kA	D.
W.	W.	kA	W.
Griffitha	Griffith	k1gMnSc2	Griffith
Zrození	zrození	k1gNnSc4	zrození
národa	národ	k1gInSc2	národ
a	a	k8xC	a
následně	následně	k6eAd1	následně
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
relativně	relativně	k6eAd1	relativně
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
ty	ten	k3xDgInPc4	ten
největší	veliký	k2eAgInPc4d3	veliký
filmové	filmový	k2eAgInPc4d1	filmový
trháky	trhák	k1gInPc4	trhák
byla	být	k5eAaImAgFnS	být
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
hudba	hudba	k1gFnSc1	hudba
originální	originální	k2eAgFnSc1d1	originální
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
originální	originální	k2eAgFnSc1d1	originální
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
film	film	k1gInSc4	film
psána	psán	k2eAgFnSc1d1	psána
již	již	k9	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
od	od	k7c2	od
Camille	Camille	k1gFnSc2	Camille
Saint-Saënsa	Saint-Saënsa	k1gFnSc1	Saint-Saënsa
ke	k	k7c3	k
klasickému	klasický	k2eAgInSc3d1	klasický
filmu	film	k1gInSc3	film
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Art	Art	k1gFnSc2	Art
Zavraždění	zavraždění	k1gNnSc2	zavraždění
vévody	vévoda	k1gMnSc2	vévoda
Guiese	Guiese	k1gFnSc2	Guiese
<g/>
,	,	kIx,	,
či	či	k8xC	či
skladatele	skladatel	k1gMnSc2	skladatel
Michaila	Michail	k1gMnSc2	Michail
Ippolitov-Ivanova	Ippolitov-Ivanův	k2eAgMnSc2d1	Ippolitov-Ivanův
k	k	k7c3	k
prvnímu	první	k4xOgMnSc3	první
ruskému	ruský	k2eAgMnSc3d1	ruský
filmu	film	k1gInSc6	film
Stěnka	stěnka	k1gFnSc1	stěnka
Razin	Razin	k1gInSc1	Razin
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgInS	být
filmový	filmový	k2eAgInSc4d1	filmový
průmysl	průmysl	k1gInSc4	průmysl
největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
pro	pro	k7c4	pro
instrumentální	instrumentální	k2eAgMnPc4d1	instrumentální
muzikanty	muzikant	k1gMnPc4	muzikant
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
pianisty	pianista	k1gMnPc4	pianista
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
potom	potom	k6eAd1	potom
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
těchto	tento	k3xDgMnPc2	tento
muzikantů	muzikant	k1gMnPc2	muzikant
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
zavádění	zavádění	k1gNnSc2	zavádění
zvuku	zvuk	k1gInSc2	zvuk
do	do	k7c2	do
kinematografie	kinematografie	k1gFnSc2	kinematografie
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
časově	časově	k6eAd1	časově
krylo	krýt	k5eAaImAgNnS	krýt
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
krizí	krize	k1gFnSc7	krize
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
přinášel	přinášet	k5eAaImAgInS	přinášet
zvuk	zvuk	k1gInSc1	zvuk
do	do	k7c2	do
filmu	film	k1gInSc2	film
i	i	k8xC	i
jinými	jiný	k2eAgInPc7d1	jiný
způsoby	způsob	k1gInPc7	způsob
než	než	k8xS	než
instrumentální	instrumentální	k2eAgFnSc7d1	instrumentální
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Rané	raný	k2eAgInPc1d1	raný
zpívající	zpívající	k2eAgInPc1d1	zpívající
filmy	film	k1gInPc1	film
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
například	například	k6eAd1	například
využívaly	využívat	k5eAaPmAgFnP	využívat
zpěváky	zpěvák	k1gMnPc4	zpěvák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zpívali	zpívat	k5eAaImAgMnP	zpívat
za	za	k7c7	za
plátnem	plátno	k1gNnSc7	plátno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
měly	mít	k5eAaImAgInP	mít
filmy	film	k1gInPc1	film
nejen	nejen	k6eAd1	nejen
živou	živý	k2eAgFnSc4d1	živá
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
benši	bensat	k5eAaPmIp1nSwK	bensat
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgMnPc4d1	umělecký
vypravěče	vypravěč	k1gMnPc4	vypravěč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
komentáře	komentář	k1gInPc4	komentář
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obstarávaly	obstarávat	k5eAaImAgInP	obstarávat
dialogy	dialog	k1gInPc1	dialog
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Benši	Benš	k1gMnPc1	Benš
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
ústředním	ústřední	k2eAgInSc7d1	ústřední
elementem	element	k1gInSc7	element
japonského	japonský	k2eAgInSc2d1	japonský
filmu	film	k1gInSc2	film
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
popularita	popularita	k1gFnSc1	popularita
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
nově	nově	k6eAd1	nově
restaurují	restaurovat	k5eAaBmIp3nP	restaurovat
a	a	k8xC	a
dostávají	dostávat	k5eAaImIp3nP	dostávat
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
do	do	k7c2	do
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
i	i	k9	i
hudební	hudební	k2eAgInSc1d1	hudební
doprovod	doprovod	k1gInSc1	doprovod
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
nový	nový	k2eAgInSc1d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
existují	existovat	k5eAaImIp3nP	existovat
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
němé	němý	k2eAgInPc4d1	němý
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Carl	Carl	k1gInSc4	Carl
Davis	Davis	k1gFnSc2	Davis
nebo	nebo	k8xC	nebo
Robert	Robert	k1gMnSc1	Robert
Israel	Israel	k1gMnSc1	Israel
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
existují	existovat	k5eAaImIp3nP	existovat
hudební	hudební	k2eAgFnPc1d1	hudební
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgFnPc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
ozvučování	ozvučování	k1gNnSc4	ozvučování
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
Alloy	Alloa	k1gFnPc1	Alloa
Orchestra	orchestra	k1gFnSc1	orchestra
nebo	nebo	k8xC	nebo
Silent	Silent	k1gMnSc1	Silent
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgInSc1d1	specifický
typ	typ	k1gInSc1	typ
herectví	herectví	k1gNnSc2	herectví
v	v	k7c6	v
němém	němý	k2eAgInSc6d1	němý
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
toho	ten	k3xDgInSc2	ten
raného	raný	k2eAgInSc2d1	raný
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
herci	herec	k1gMnPc1	herec
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
řeč	řeč	k1gFnSc4	řeč
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
výrazy	výraz	k1gInPc1	výraz
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
diváci	divák	k1gMnPc1	divák
lépe	dobře	k6eAd2	dobře
chápali	chápat	k5eAaImAgMnP	chápat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
postava	postava	k1gFnSc1	postava
pociťovala	pociťovat	k5eAaImAgFnS	pociťovat
či	či	k8xC	či
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
herectví	herectví	k1gNnSc2	herectví
zvykem	zvyk	k1gInSc7	zvyk
herců	herc	k1gInPc2	herc
přešlých	přešlý	k2eAgInPc2d1	přešlý
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgFnSc1d1	převládající
přítomnost	přítomnost	k1gFnSc1	přítomnost
divadelních	divadelní	k2eAgMnPc2d1	divadelní
herců	herec	k1gMnPc2	herec
ve	v	k7c6	v
filmu	film	k1gInSc6	film
byla	být	k5eAaImAgFnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
příčinou	příčina	k1gFnSc7	příčina
k	k	k7c3	k
výroku	výrok	k1gInSc3	výrok
amerického	americký	k2eAgMnSc2d1	americký
režiséra	režisér	k1gMnSc2	režisér
Marshalla	Marshall	k1gMnSc2	Marshall
Neilana	Neilan	k1gMnSc2	Neilan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čím	co	k3yInSc7	co
dříve	dříve	k6eAd2	dříve
herci	herec	k1gMnPc1	herec
přešlí	přešlý	k2eAgMnPc1d1	přešlý
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
odejdou	odejít	k5eAaPmIp3nP	odejít
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
pro	pro	k7c4	pro
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Specifické	specifický	k2eAgNnSc1d1	specifické
médium	médium	k1gNnSc1	médium
filmu	film	k1gInSc2	film
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
postupně	postupně	k6eAd1	postupně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
herecký	herecký	k2eAgInSc4d1	herecký
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
střídmějším	střídmý	k2eAgMnSc7d2	střídmější
a	a	k8xC	a
zaměřeným	zaměřený	k2eAgInSc7d1	zaměřený
spíše	spíše	k9	spíše
na	na	k7c4	na
jemné	jemný	k2eAgFnPc4d1	jemná
nuance	nuance	k1gFnPc4	nuance
ve	v	k7c6	v
výrazu	výraz	k1gInSc6	výraz
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Herečky	herečka	k1gFnPc1	herečka
jako	jako	k8xS	jako
Mary	Mary	k1gFnSc1	Mary
Pickford	Pickforda	k1gFnPc2	Pickforda
či	či	k8xC	či
Greta	Greto	k1gNnSc2	Greto
Garbo	Garba	k1gFnSc5	Garba
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jejich	jejich	k3xOp3gFnPc2	jejich
hereckých	herecký	k2eAgFnPc2d1	herecká
kreací	kreace	k1gFnPc2	kreace
měli	mít	k5eAaImAgMnP	mít
zdrženlivý	zdrženlivý	k2eAgInSc4d1	zdrženlivý
herecký	herecký	k2eAgInSc4d1	herecký
styl	styl	k1gInSc4	styl
a	a	k8xC	a
více	hodně	k6eAd2	hodně
přirozené	přirozený	k2eAgNnSc1d1	přirozené
herectví	herectví	k1gNnSc1	herectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
mnoho	mnoho	k4c1	mnoho
herců	herec	k1gMnPc2	herec
adaptováno	adaptovat	k5eAaBmNgNnS	adaptovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
umírněnější	umírněný	k2eAgInSc4d2	umírněnější
herecký	herecký	k2eAgInSc4d1	herecký
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
diváci	divák	k1gMnPc1	divák
mohli	moct	k5eAaImAgMnP	moct
s	s	k7c7	s
přehnaně	přehnaně	k6eAd1	přehnaně
afektovaným	afektovaný	k2eAgNnSc7d1	afektované
a	a	k8xC	a
nepřirozeným	přirozený	k2eNgNnSc7d1	nepřirozené
herectvím	herectví	k1gNnSc7	herectví
setkávat	setkávat	k5eAaImF	setkávat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Avantgardní	avantgardní	k2eAgNnSc1d1	avantgardní
hnutí	hnutí	k1gNnSc1	hnutí
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
často	často	k6eAd1	často
počítaly	počítat	k5eAaImAgFnP	počítat
i	i	k9	i
se	s	k7c7	s
specifickým	specifický	k2eAgInSc7d1	specifický
typem	typ	k1gInSc7	typ
herectví	herectví	k1gNnSc1	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
německý	německý	k2eAgInSc1d1	německý
Expresionismus	expresionismus	k1gInSc1	expresionismus
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
přehnaný	přehnaný	k2eAgInSc1d1	přehnaný
<g/>
,	,	kIx,	,
expresionistický	expresionistický	k2eAgInSc1d1	expresionistický
herecký	herecký	k2eAgInSc4d1	herecký
styl	styl	k1gInSc4	styl
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Metropolis	Metropolis	k1gFnSc2	Metropolis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
éra	éra	k1gFnSc1	éra
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
potýkala	potýkat	k5eAaImAgFnS	potýkat
z	z	k7c2	z
prudce	prudko	k6eAd1	prudko
hořlavou	hořlavý	k2eAgFnSc7d1	hořlavá
nitrocelulózovou	nitrocelulózový	k2eAgFnSc7d1	nitrocelulózová
podložkou	podložka	k1gFnSc7	podložka
filmového	filmový	k2eAgInSc2d1	filmový
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Promítací	promítací	k2eAgInPc1d1	promítací
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
uzavřenými	uzavřený	k2eAgInPc7d1	uzavřený
bubny	buben	k1gInPc7	buben
pro	pro	k7c4	pro
cívky	cívka	k1gFnPc4	cívka
filmových	filmový	k2eAgFnPc2d1	filmová
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc2	rozšíření
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Vzplanutí	vzplanutí	k1gNnSc1	vzplanutí
filmu	film	k1gInSc2	film
v	v	k7c6	v
promítací	promítací	k2eAgFnSc6d1	promítací
kabině	kabina	k1gFnSc6	kabina
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebylo	být	k5eNaImAgNnS	být
žádnou	žádný	k3yNgFnSc7	žádný
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
požárům	požár	k1gInPc3	požár
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
nitrocelulózu	nitrocelulóza	k1gFnSc4	nitrocelulóza
neuhasila	uhasit	k5eNaPmAgFnS	uhasit
<g/>
,	,	kIx,	,
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
promítacího	promítací	k2eAgInSc2d1	promítací
stroje	stroj	k1gInSc2	stroj
byly	být	k5eAaImAgFnP	být
umístěny	umístěn	k2eAgFnPc1d1	umístěna
nádoby	nádoba	k1gFnPc1	nádoba
s	s	k7c7	s
pískem	písek	k1gInSc7	písek
<g/>
,	,	kIx,	,
na	na	k7c4	na
udušení	udušení	k1gNnSc4	udušení
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
formátem	formát	k1gInSc7	formát
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgInS	být
klasický	klasický	k2eAgInSc4d1	klasický
35	[number]	k4	35
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
standardizaci	standardizace	k1gFnSc3	standardizace
projekční	projekční	k2eAgFnSc2d1	projekční
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c4	na
24	[number]	k4	24
okének	okénko	k1gNnPc2	okénko
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
frames	frames	k1gInSc1	frames
per	pero	k1gNnPc2	pero
second	second	k1gInSc1	second
-	-	kIx~	-
fps	fps	k?	fps
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zvukový	zvukový	k2eAgInSc4d1	zvukový
film	film	k1gInSc4	film
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1926	[number]	k4	1926
a	a	k8xC	a
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
natáčeny	natáčen	k2eAgInPc1d1	natáčen
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
mezi	mezi	k7c7	mezi
12	[number]	k4	12
a	a	k8xC	a
26	[number]	k4	26
fps	fps	k?	fps
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
projekční	projekční	k2eAgFnSc1d1	projekční
rychlost	rychlost	k1gFnSc1	rychlost
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
činila	činit	k5eAaImAgFnS	činit
potom	potom	k6eAd1	potom
16	[number]	k4	16
fps	fps	k?	fps
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postačovala	postačovat	k5eAaImAgFnS	postačovat
pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
iluze	iluze	k1gFnSc2	iluze
pohybu	pohyb	k1gInSc2	pohyb
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
projekční	projekční	k2eAgFnSc2d1	projekční
rychlosti	rychlost	k1gFnSc2	rychlost
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
nejenom	nejenom	k6eAd1	nejenom
zvýšení	zvýšení	k1gNnSc1	zvýšení
kvality	kvalita	k1gFnSc2	kvalita
iluze	iluze	k1gFnSc2	iluze
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
nutnost	nutnost	k1gFnSc4	nutnost
zvýšit	zvýšit	k5eAaPmF	zvýšit
rychlost	rychlost	k1gFnSc4	rychlost
posunu	posun	k1gInSc2	posun
filmu	film	k1gInSc2	film
na	na	k7c6	na
zvukovém	zvukový	k2eAgInSc6d1	zvukový
snímači	snímač	k1gInSc6	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
16	[number]	k4	16
fps	fps	k?	fps
nebyl	být	k5eNaImAgInS	být
dosažen	dosažen	k2eAgInSc4d1	dosažen
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
akustický	akustický	k2eAgInSc4d1	akustický
kmitočet	kmitočet	k1gInSc4	kmitočet
a	a	k8xC	a
kvalita	kvalita	k1gFnSc1	kvalita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
pečlivého	pečlivý	k2eAgNnSc2d1	pečlivé
promítání	promítání	k1gNnSc2	promítání
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
zamýšlené	zamýšlený	k2eAgFnSc6d1	zamýšlená
rychlosti	rychlost	k1gFnSc6	rychlost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
nepřirozeně	přirozeně	k6eNd1	přirozeně
rychlé	rychlý	k2eAgInPc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
filmy	film	k1gInPc4	film
či	či	k8xC	či
pouze	pouze	k6eAd1	pouze
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
scény	scéna	k1gFnPc1	scéna
byly	být	k5eAaImAgFnP	být
záměrně	záměrně	k6eAd1	záměrně
natáčeny	natáčet	k5eAaImNgFnP	natáčet
nižší	nízký	k2eAgFnSc7d2	nižší
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
promítání	promítání	k1gNnSc6	promítání
byla	být	k5eAaImAgFnS	být
akce	akce	k1gFnSc1	akce
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
-	-	kIx~	-
obzvláště	obzvláště	k6eAd1	obzvláště
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dělo	dělo	k1gNnSc1	dělo
u	u	k7c2	u
komedií	komedie	k1gFnPc2	komedie
a	a	k8xC	a
akčních	akční	k2eAgInPc2d1	akční
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
"	"	kIx"	"
<g/>
zrychlovacího	zrychlovací	k2eAgInSc2d1	zrychlovací
<g/>
"	"	kIx"	"
jevu	jev	k1gInSc2	jev
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Chaplinovy	Chaplinův	k2eAgFnPc1d1	Chaplinova
grotesky	groteska	k1gFnPc1	groteska
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
natáčení	natáčení	k1gNnSc1	natáčení
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
barevný	barevný	k2eAgInSc4d1	barevný
materiál	materiál	k1gInSc4	materiál
ještě	ještě	k6eAd1	ještě
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
v	v	k7c6	v
němé	němý	k2eAgFnSc6d1	němá
éře	éra	k1gFnSc6	éra
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
postprodukčně	postprodukčně	k6eAd1	postprodukčně
obarvovány	obarvován	k2eAgFnPc1d1	obarvován
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
éře	éra	k1gFnSc6	éra
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
kinematografie	kinematografie	k1gFnSc2	kinematografie
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
filmy	film	k1gInPc1	film
často	často	k6eAd1	často
ručně	ručně	k6eAd1	ručně
kolorovány	kolorován	k2eAgFnPc1d1	kolorována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
atraktivnější	atraktivní	k2eAgFnSc1d2	atraktivnější
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
dlouhometrážní	dlouhometrážní	k2eAgInPc1d1	dlouhometrážní
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
zase	zase	k9	zase
virážovány	virážovat	k5eAaPmNgInP	virážovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zvýrazňovala	zvýrazňovat	k5eAaImAgFnS	zvýrazňovat
určitá	určitý	k2eAgFnSc1d1	určitá
nálada	nálada	k1gFnSc1	nálada
či	či	k8xC	či
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
sépiově	sépiově	k6eAd1	sépiově
zbarvenými	zbarvený	k2eAgInPc7d1	zbarvený
němými	němý	k2eAgInPc7d1	němý
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňování	uplatňování	k1gNnSc1	uplatňování
barvy	barva	k1gFnSc2	barva
v	v	k7c6	v
němém	němý	k2eAgInSc6d1	němý
filmu	film	k1gInSc6	film
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
D.	D.	kA	D.
W.	W.	kA	W.
Griffitha	Griffitha	k1gMnSc1	Griffitha
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
projevoval	projevovat	k5eAaImAgMnS	projevovat
stálý	stálý	k2eAgInSc4d1	stálý
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
systematické	systematický	k2eAgNnSc4d1	systematické
využití	využití	k1gNnSc4	využití
barev	barva	k1gFnPc2	barva
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
epickém	epický	k2eAgInSc6d1	epický
snímku	snímek	k1gInSc6	snímek
Zrození	zrození	k1gNnSc4	zrození
národa	národ	k1gInSc2	národ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
použil	použít	k5eAaPmAgInS	použít
často	často	k6eAd1	často
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
modrou	modrý	k2eAgFnSc4d1	modrá
nebo	nebo	k8xC	nebo
výrazně	výrazně	k6eAd1	výrazně
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
klíčových	klíčový	k2eAgFnPc2d1	klíčová
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
hořící	hořící	k2eAgFnSc1d1	hořící
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
klimaxová	klimaxový	k2eAgFnSc1d1	klimaxová
jízda	jízda	k1gFnSc1	jízda
Ku-klux-klanu	Kuluxlan	k1gInSc2	Ku-klux-klan
na	na	k7c6	na
konci	konec	k1gInSc6	konec
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Griffith	Griffith	k1gMnSc1	Griffith
později	pozdě	k6eAd2	pozdě
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
systém	systém	k1gInSc4	systém
obarvování	obarvování	k1gNnSc2	obarvování
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
barevná	barevný	k2eAgNnPc1d1	barevné
světla	světlo	k1gNnPc1	světlo
blýskala	blýskat	k5eAaImAgNnP	blýskat
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
části	část	k1gFnSc6	část
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
posiloval	posilovat	k5eAaImAgMnS	posilovat
barevný	barevný	k2eAgInSc4d1	barevný
efekt	efekt	k1gInSc4	efekt
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
nenávratně	návratně	k6eNd1	návratně
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nitrátový	nitrátový	k2eAgInSc1d1	nitrátový
filmový	filmový	k2eAgInSc1d1	filmový
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
němé	němý	k2eAgFnSc6d1	němá
éře	éra	k1gFnSc6	éra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
a	a	k8xC	a
náchylný	náchylný	k2eAgInSc1d1	náchylný
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
filmů	film	k1gInPc2	film
bylo	být	k5eAaImAgNnS	být
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
prošlo	projít	k5eAaPmAgNnS	projít
filmovou	filmový	k2eAgFnSc7d1	filmová
distribucí	distribuce	k1gFnSc7	distribuce
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
záměrně	záměrně	k6eAd1	záměrně
ničeno	ničen	k2eAgNnSc1d1	ničeno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mělo	mít	k5eAaImAgNnS	mít
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
až	až	k9	až
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
domácího	domácí	k2eAgNnSc2d1	domácí
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolo	okolo	k7c2	okolo
75	[number]	k4	75
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
většinu	většina	k1gFnSc4	většina
ztracených	ztracený	k2eAgInPc2d1	ztracený
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
již	již	k6eAd1	již
nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
neuvidí	uvidět	k5eNaPmIp3nS	uvidět
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
nacházeny	nacházen	k2eAgFnPc1d1	nacházena
ve	v	k7c6	v
filmových	filmový	k2eAgInPc6d1	filmový
archivech	archiv	k1gInPc6	archiv
nebo	nebo	k8xC	nebo
soukromých	soukromý	k2eAgFnPc6d1	soukromá
sbírkách	sbírka	k1gFnPc6	sbírka
a	a	k8xC	a
následně	následně	k6eAd1	následně
restaurovány	restaurován	k2eAgFnPc1d1	restaurována
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
uváděny	uvádět	k5eAaImNgFnP	uvádět
do	do	k7c2	do
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
současná	současný	k2eAgFnSc1d1	současná
generace	generace	k1gFnSc1	generace
filmových	filmový	k2eAgMnPc2d1	filmový
diváků	divák	k1gMnPc2	divák
může	moct	k5eAaImIp3nS	moct
nově	nově	k6eAd1	nově
objevovat	objevovat	k5eAaImF	objevovat
staré	starý	k2eAgInPc4d1	starý
němé	němý	k2eAgInPc4d1	němý
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc2	Le
Voyage	Voyag	k1gMnSc2	Voyag
dans	dans	k6eAd1	dans
la	la	k1gNnSc4	la
Lune	Lun	k1gFnSc2	Lun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
–	–	k?	–
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědeckofantastický	vědeckofantastický	k2eAgMnSc1d1	vědeckofantastický
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Georges	Georges	k1gInSc1	Georges
Méliè	Méliè	k1gFnSc4	Méliè
Velká	velký	k2eAgFnSc1d1	velká
železniční	železniční	k2eAgFnSc1d1	železniční
loupež	loupež	k1gFnSc1	loupež
(	(	kIx(	(
<g/>
The	The	k1gFnSc7	The
Great	Great	k2eAgInSc4d1	Great
Train	Train	k1gInSc4	Train
Robbery	Robbera	k1gFnSc2	Robbera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
western	western	k1gInSc4	western
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Edwin	Edwin	k1gInSc1	Edwin
S.	S.	kA	S.
Porter	porter	k1gInSc4	porter
Upíři	upír	k1gMnPc1	upír
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
<g />
.	.	kIx.	.
</s>
<s>
Vampires	Vampires	k1gInSc1	Vampires
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
–	–	k?	–
francouzský	francouzský	k2eAgInSc1d1	francouzský
thriller	thriller	k1gInSc1	thriller
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Louis	Louis	k1gMnSc1	Louis
Feuillade	Feuillad	k1gInSc5	Feuillad
Zrození	zrození	k1gNnSc1	zrození
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Birth	Birtha	k1gFnPc2	Birtha
of	of	k?	of
a	a	k8xC	a
Nation	Nation	k1gInSc1	Nation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
–	–	k?	–
americké	americký	k2eAgNnSc1d1	americké
historické	historický	k2eAgNnSc1d1	historické
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
režie	režie	k1gFnPc1	režie
D.	D.	kA	D.
W.	W.	kA	W.
Griffith	Griffitha	k1gFnPc2	Griffitha
Intolerance	intolerance	k1gFnSc1	intolerance
(	(	kIx(	(
<g/>
Intolerance	intolerance	k1gFnSc1	intolerance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
–	–	k?	–
americké	americký	k2eAgFnPc4d1	americká
historické	historický	k2eAgFnPc4d1	historická
<g />
.	.	kIx.	.
</s>
<s>
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
režie	režie	k1gFnPc1	režie
D.	D.	kA	D.
W.	W.	kA	W.
Griffith	Griffith	k1gMnSc1	Griffith
Vozka	vozka	k1gMnSc1	vozka
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
Körkarlen	Körkarlen	k2eAgMnSc1d1	Körkarlen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
–	–	k?	–
švédský	švédský	k2eAgInSc1d1	švédský
horor	horor	k1gInSc1	horor
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Victor	Victor	k1gMnSc1	Victor
Sjöström	Sjöström	k1gMnSc1	Sjöström
Tu	tu	k6eAd1	tu
ten	ten	k3xDgInSc1	ten
kámen	kámen	k1gInSc1	kámen
aneb	aneb	k?	aneb
Kterak	kterak	k8xS	kterak
láskou	láska	k1gFnSc7	láska
možno	možno	k6eAd1	možno
v	v	k7c6	v
mžiku	mžik	k1gInSc6	mžik
vzplanout	vzplanout	k5eAaPmF	vzplanout
třeba	třeba	k6eAd1	třeba
k	k	k7c3	k
nebožtíku	nebožtík	k1gMnSc3	nebožtík
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
komedie	komedie	k1gFnSc1	komedie
s	s	k7c7	s
Vlastou	Vlasta	k1gMnSc7	Vlasta
Burianem	Burian	k1gMnSc7	Burian
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g />
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Anton	Anton	k1gMnSc1	Anton
Michael	Michael	k1gMnSc1	Michael
(	(	kIx(	(
<g/>
Mikaël	Mikaël	k1gMnSc1	Mikaël
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
–	–	k?	–
německé	německý	k2eAgNnSc1d1	německé
filmové	filmový	k2eAgNnSc1d1	filmové
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodor	k1gMnSc1	Theodor
Dreyer	Dreyer	k1gMnSc1	Dreyer
Metropolis	Metropolis	k1gFnSc1	Metropolis
(	(	kIx(	(
<g/>
Metropolis	Metropolis	k1gInSc1	Metropolis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
–	–	k?	–
německý	německý	k2eAgMnSc1d1	německý
vědeckofantastický	vědeckofantastický	k2eAgMnSc1d1	vědeckofantastický
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Fritz	Fritz	k1gMnSc1	Fritz
Lang	Lang	k1gMnSc1	Lang
Disneyho	Disney	k1gMnSc4	Disney
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
Fantazie	fantazie	k1gFnSc2	fantazie
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
osmi	osm	k4xCc2	osm
animovaných	animovaný	k2eAgFnPc2d1	animovaná
hudebních	hudební	k2eAgFnPc2d1	hudební
sekvencí	sekvence	k1gFnPc2	sekvence
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
na	na	k7c4	na
němý	němý	k2eAgInSc4d1	němý
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Sunset	Sunset	k1gMnSc1	Sunset
Blvd	Blvd	k1gMnSc1	Blvd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
kontrast	kontrast	k1gInSc1	kontrast
mezi	mezi	k7c7	mezi
němou	němý	k2eAgFnSc7d1	němá
a	a	k8xC	a
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
érou	éra	k1gFnSc7	éra
kinematografie	kinematografie	k1gFnSc2	kinematografie
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
hvězdy	hvězda	k1gFnSc2	hvězda
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
Normy	Norma	k1gFnSc2	Norma
Desmond	Desmonda	k1gFnPc2	Desmonda
<g/>
.	.	kIx.	.
</s>
<s>
Zpívání	zpívání	k1gNnSc1	zpívání
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
tematizuje	tematizovat	k5eAaImIp3nS	tematizovat
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
periodu	perioda	k1gFnSc4	perioda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Hollywood	Hollywood	k1gInSc1	Hollywood
musel	muset	k5eAaImAgInS	muset
vypořádávat	vypořádávat	k5eAaImF	vypořádávat
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
od	od	k7c2	od
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
ke	k	k7c3	k
zvukovému	zvukový	k2eAgNnSc3d1	zvukové
<g/>
.	.	kIx.	.
</s>
<s>
Mel	mlít	k5eAaImRp2nS	mlít
Brooks	Brooks	k1gInSc4	Brooks
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
poctu	pocta	k1gFnSc4	pocta
němé	němý	k2eAgFnSc3d1	němá
filmové	filmový	k2eAgFnSc3d1	filmová
grotesce	groteska	k1gFnSc3	groteska
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Bogdanovich	Bogdanovich	k1gMnSc1	Bogdanovich
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bijásek	bijásek	k1gInSc1	bijásek
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
zmatek	zmatek	k1gInSc4	zmatek
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
během	během	k7c2	během
raných	raný	k2eAgNnPc2d1	rané
10	[number]	k4	10
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
finského	finský	k2eAgMnSc2d1	finský
filmaře	filmař	k1gMnSc2	filmař
Aki	Aki	k1gMnSc2	Aki
Kaurismäkiho	Kaurismäki	k1gMnSc2	Kaurismäki
Juha	Juhus	k1gMnSc2	Juhus
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
styl	styl	k1gInSc4	styl
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
francouzského	francouzský	k2eAgMnSc2d1	francouzský
režiséra	režisér	k1gMnSc2	režisér
Michela	Michel	k1gMnSc2	Michel
Hazanaviciuse	Hazanaviciuse	k1gFnSc2	Hazanaviciuse
The	The	k1gMnSc1	The
Artist	Artist	k1gMnSc1	Artist
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
stylisticky	stylisticky	k6eAd1	stylisticky
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
němému	němý	k2eAgInSc3d1	němý
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Scorsese	Scorsese	k1gFnSc2	Scorsese
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hugo	Hugo	k1gMnSc1	Hugo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
velký	velký	k2eAgInSc1d1	velký
objev	objev	k1gInSc1	objev
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
poctu	pocta	k1gFnSc4	pocta
průkopníkovi	průkopník	k1gMnSc3	průkopník
raného	raný	k2eAgInSc2d1	raný
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
Georgesi	Georgese	k1gFnSc4	Georgese
Méliè	Méliè	k1gFnSc4	Méliè
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Silent	Silent	k1gInSc1	Silent
Life	Lif	k1gFnSc2	Lif
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
životě	život	k1gInSc6	život
ikony	ikona	k1gFnSc2	ikona
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
Rudolpha	Rudolph	k1gMnSc4	Rudolph
Valentina	Valentin	k1gMnSc4	Valentin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Silent	Silent	k1gInSc1	Silent
film	film	k1gInSc1	film
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
