<s>
Jako	jako	k8xC	jako
předělový	předělový	k2eAgInSc1d1	předělový
snímek	snímek	k1gInSc1	snímek
mezi	mezi	k7c7	mezi
němou	němý	k2eAgFnSc7d1	němá
a	a	k8xC	a
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
érou	éra	k1gFnSc7	éra
kinematografie	kinematografie	k1gFnSc2	kinematografie
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
označuje	označovat	k5eAaImIp3nS	označovat
film	film	k1gInSc4	film
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
značný	značný	k2eAgInSc4d1	značný
finanční	finanční	k2eAgInSc4d1	finanční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
