<s>
Výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gMnSc1	Bubo
bubo	bubo	k1gMnSc1	bubo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
sovy	sova	k1gFnSc2	sova
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
puštíkovitých	puštíkovitý	k2eAgFnPc2d1	puštíkovitý
(	(	kIx(	(
<g/>
Strigidae	Strigidae	k1gFnPc2	Strigidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
přezdíváno	přezdíván	k2eAgNnSc1d1	přezdíváno
král	král	k1gMnSc1	král
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
3	[number]	k4	3
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
monotypické	monotypický	k2eAgFnSc2d1	monotypický
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
výr	výr	k1gMnSc1	výr
bledý	bledý	k2eAgMnSc1d1	bledý
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gMnSc1	Bubo
ascalaphus	ascalaphus	k1gMnSc1	ascalaphus
<g/>
)	)	kIx)	)
a	a	k8xC	a
výr	výr	k1gMnSc1	výr
bengálský	bengálský	k2eAgMnSc1d1	bengálský
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gMnSc1	Bubo
bengalensis	bengalensis	k1gFnSc2	bengalensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
Evropy	Evropa	k1gFnSc2	Evropa
obývá	obývat	k5eAaImIp3nS	obývat
výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gMnSc1	Bubo
bubo	bubo	k1gMnSc1	bubo
bubo	bubo	k1gMnSc1	bubo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
poloostrov	poloostrov	k1gInSc1	poloostrov
v.	v.	k?	v.
v.	v.	k?	v.
španělský	španělský	k2eAgMnSc1d1	španělský
(	(	kIx(	(
<g/>
B.	B.	kA	B.
b.	b.	k?	b.
hispanus	hispanus	k1gInSc1	hispanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
v.	v.	k?	v.
v.	v.	k?	v.
východoruský	východoruský	k2eAgMnSc1d1	východoruský
(	(	kIx(	(
<g/>
B.	B.	kA	B.
b.	b.	k?	b.
ruthenus	ruthenus	k1gInSc1	ruthenus
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
a	a	k8xC	a
Charkova	Charkov	k1gInSc2	Charkov
<g/>
,	,	kIx,	,
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
,	,	kIx,	,
Krym	Krym	k1gInSc4	Krym
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
Malou	malý	k2eAgFnSc4d1	malá
Asii	Asie	k1gFnSc4	Asie
v.	v.	k?	v.
v.	v.	k?	v.
černomořský	černomořský	k2eAgMnSc1d1	černomořský
(	(	kIx(	(
<g/>
B.	B.	kA	B.
b.	b.	k?	b.
interpositus	interpositus	k1gInSc1	interpositus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
mohutný	mohutný	k2eAgMnSc1d1	mohutný
pták	pták	k1gMnSc1	pták
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
sov	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
káně	káně	k1gFnSc1	káně
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
zřetelně	zřetelně	k6eAd1	zřetelně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
např.	např.	kA	např.
orel	orel	k1gMnSc1	orel
skalní	skalní	k2eAgMnSc1d1	skalní
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgNnSc4d2	veliký
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
160	[number]	k4	160
<g/>
–	–	k?	–
<g/>
188	[number]	k4	188
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
samce	samec	k1gInSc2	samec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
2800	[number]	k4	2800
g	g	kA	g
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
mezi	mezi	k7c7	mezi
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
4200	[number]	k4	4200
g.	g.	k?	g.
Silueta	silueta	k1gFnSc1	silueta
sedícího	sedící	k2eAgMnSc2d1	sedící
výra	výr	k1gMnSc2	výr
velkého	velký	k2eAgInSc2d1	velký
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
zaměnitelná	zaměnitelný	k2eAgFnSc1d1	zaměnitelná
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
z	z	k7c2	z
peříček	peříčko	k1gNnPc2	peříčko
výrazné	výrazný	k2eAgInPc4d1	výrazný
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
chvostky	chvostek	k1gInPc4	chvostek
připomínající	připomínající	k2eAgFnSc2d1	připomínající
ouška	ouško	k1gNnSc2	ouško
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
ušatá	ušatý	k2eAgFnSc1d1	ušatá
sova	sova	k1gFnSc1	sova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
velikostí	velikost	k1gFnSc7	velikost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
určovací	určovací	k2eAgInSc4d1	určovací
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
svrchu	svrchu	k6eAd1	svrchu
žluto	žluto	k6eAd1	žluto
<g/>
(	(	kIx(	(
<g/>
oranžovo-rezavo	oranžovoezava	k1gFnSc5	oranžovo-rezava
<g/>
)	)	kIx)	)
<g/>
hnědé	hnědý	k2eAgFnPc1d1	hnědá
s	s	k7c7	s
tmavohnědými	tmavohnědý	k2eAgFnPc7d1	tmavohnědá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
světlejší	světlý	k2eAgInSc1d2	světlejší
odstín	odstín	k1gInSc1	odstín
téhož	týž	k3xTgInSc2	týž
s	s	k7c7	s
výraznými	výrazný	k2eAgMnPc7d1	výrazný
podélným	podélný	k2eAgNnSc7d1	podélné
tmavým	tmavý	k2eAgNnSc7d1	tmavé
skvrněním	skvrnění	k1gNnSc7	skvrnění
a	a	k8xC	a
jemným	jemný	k2eAgNnSc7d1	jemné
příčným	příčný	k2eAgNnSc7d1	příčné
mramorováním	mramorování	k1gNnSc7	mramorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
krku	krk	k1gInSc2	krk
a	a	k8xC	a
okolo	okolo	k7c2	okolo
zobáku	zobák	k1gInSc2	zobák
je	být	k5eAaImIp3nS	být
peří	peří	k1gNnSc1	peří
světlejší	světlý	k2eAgNnSc1d2	světlejší
až	až	k6eAd1	až
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Duhovka	duhovka	k1gFnSc1	duhovka
je	být	k5eAaImIp3nS	být
ohnivě	ohnivě	k6eAd1	ohnivě
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
a	a	k8xC	a
prsty	prst	k1gInPc1	prst
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgInPc1d1	porostlý
jemnými	jemný	k2eAgInPc7d1	jemný
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
žlutohnědými	žlutohnědý	k2eAgInPc7d1	žlutohnědý
až	až	k8xS	až
rezavými	rezavý	k2eAgNnPc7d1	rezavé
pírky	pírko	k1gNnPc7	pírko
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
porostlá	porostlý	k2eAgNnPc1d1	porostlé
zprvu	zprvu	k6eAd1	zprvu
jemným	jemný	k2eAgNnSc7d1	jemné
žlutobílým	žlutobílý	k2eAgNnSc7d1	žlutobílé
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
po	po	k7c6	po
opeření	opeření	k1gNnSc6	opeření
mají	mít	k5eAaImIp3nP	mít
matnější	matný	k2eAgNnPc1d2	matnější
zbarvení	zbarvení	k1gNnPc1	zbarvení
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
sovy	sova	k1gFnPc4	sova
létá	létat	k5eAaImIp3nS	létat
velmi	velmi	k6eAd1	velmi
tiše	tiš	k1gFnSc2	tiš
a	a	k8xC	a
střídá	střídat	k5eAaImIp3nS	střídat
mávavý	mávavý	k2eAgInSc4d1	mávavý
let	let	k1gInSc4	let
s	s	k7c7	s
plachtěním	plachtění	k1gNnSc7	plachtění
<g/>
.	.	kIx.	.
</s>
<s>
Výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
středomořské	středomořský	k2eAgFnSc2d1	středomořská
oblasti	oblast	k1gFnSc2	oblast
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
a	a	k8xC	a
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
části	část	k1gFnSc6	část
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
severních	severní	k2eAgFnPc2d1	severní
a	a	k8xC	a
jižních	jižní	k2eAgFnPc2d1	jižní
oblastí	oblast	k1gFnPc2	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
a	a	k8xC	a
potulný	potulný	k2eAgInSc1d1	potulný
druh	druh	k1gInSc1	druh
zimující	zimující	k2eAgInSc1d1	zimující
v	v	k7c6	v
hnízdním	hnízdní	k2eAgInSc6d1	hnízdní
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vhodném	vhodný	k2eAgNnSc6d1	vhodné
prostředí	prostředí	k1gNnSc6	prostředí
i	i	k8xC	i
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
trvalého	trvalý	k2eAgNnSc2d1	trvalé
pronásledování	pronásledování	k1gNnSc2	pronásledování
téměř	téměř	k6eAd1	téměř
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
,	,	kIx,	,
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
navyšování	navyšování	k1gNnSc3	navyšování
stavů	stav	k1gInPc2	stav
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
až	až	k9	až
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
zákonné	zákonný	k2eAgFnSc2d1	zákonná
ochrany	ochrana	k1gFnSc2	ochrana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
77	[number]	k4	77
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
na	na	k7c4	na
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
950	[number]	k4	950
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
sledovanými	sledovaný	k2eAgNnPc7d1	sledované
obdobími	období	k1gNnPc7	období
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
také	také	k9	také
počet	počet	k1gInSc1	počet
obsazených	obsazený	k2eAgInPc2d1	obsazený
kvadrátů	kvadrát	k1gInPc2	kvadrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
téměř	téměř	k6eAd1	téměř
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
z	z	k7c2	z
35	[number]	k4	35
%	%	kIx~	%
na	na	k7c4	na
63	[number]	k4	63
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
byly	být	k5eAaImAgInP	být
početní	početní	k2eAgInPc1d1	početní
stavy	stav	k1gInPc1	stav
odhadnuty	odhadnut	k2eAgInPc1d1	odhadnut
na	na	k7c4	na
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
900	[number]	k4	900
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
hnízdění	hnízdění	k1gNnSc4	hnízdění
a	a	k8xC	a
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
osidluje	osidlovat	k5eAaImIp3nS	osidlovat
kamenité	kamenitý	k2eAgFnPc4d1	kamenitá
stráně	stráň	k1gFnPc4	stráň
<g/>
,	,	kIx,	,
zříceniny	zřícenina	k1gFnPc4	zřícenina
s	s	k7c7	s
lesy	les	k1gInPc7	les
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
obývá	obývat	k5eAaImIp3nS	obývat
i	i	k9	i
oblasti	oblast	k1gFnPc4	oblast
beze	beze	k7c2	beze
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dutiny	dutina	k1gFnPc4	dutina
stromů	strom	k1gInPc2	strom
či	či	k8xC	či
opuštěná	opuštěný	k2eAgNnPc1d1	opuštěné
dravčí	dravčí	k2eAgNnPc1d1	dravčí
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
malými	malý	k2eAgFnPc7d1	malá
a	a	k8xC	a
středně	středně	k6eAd1	středně
velkými	velký	k2eAgMnPc7d1	velký
savci	savec	k1gMnPc7	savec
a	a	k8xC	a
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
obojživelníky	obojživelník	k1gMnPc4	obojživelník
<g/>
,	,	kIx,	,
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
větší	veliký	k2eAgInSc4d2	veliký
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převažují	převažovat	k5eAaImIp3nP	převažovat
druhy	druh	k1gInPc1	druh
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
do	do	k7c2	do
2	[number]	k4	2
kg	kg	kA	kg
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
a	a	k8xC	a
zajíci	zajíc	k1gMnPc1	zajíc
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
však	však	k9	však
udolat	udolat	k5eAaPmF	udolat
i	i	k9	i
lišku	liška	k1gFnSc4	liška
nebo	nebo	k8xC	nebo
srnče	srnče	k1gNnSc4	srnče
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
vedle	vedle	k7c2	vedle
holubovitých	holubovitý	k2eAgFnPc2d1	holubovitý
<g/>
,	,	kIx,	,
krkavcovitých	krkavcovitý	k2eAgFnPc2d1	krkavcovitý
nebo	nebo	k8xC	nebo
kachnovitých	kachnovitý	k2eAgFnPc2d1	kachnovitý
běžně	běžně	k6eAd1	běžně
loví	lovit	k5eAaImIp3nP	lovit
také	také	k9	také
dravce	dravec	k1gMnPc4	dravec
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
druhy	druh	k1gMnPc4	druh
sov	sova	k1gFnPc2	sova
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
potravní	potravní	k2eAgFnSc6d1	potravní
nabídce	nabídka	k1gFnSc6	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
revíru	revír	k1gInSc6	revír
totiž	totiž	k9	totiž
přednostně	přednostně	k6eAd1	přednostně
loví	lovit	k5eAaImIp3nP	lovit
kořist	kořist	k1gFnSc4	kořist
nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
87	[number]	k4	87
ve	v	k7c6	v
vývržcích	vývržek	k1gInPc6	vývržek
a	a	k8xC	a
jako	jako	k9	jako
kořist	kořist	k1gFnSc4	kořist
na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
celkem	celek	k1gInSc7	celek
7144	[number]	k4	7144
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
objevoval	objevovat	k5eAaImAgMnS	objevovat
hraboš	hraboš	k1gMnSc1	hraboš
polní	polní	k2eAgMnSc1d1	polní
<g/>
,	,	kIx,	,
ježek	ježek	k1gMnSc1	ježek
<g/>
,	,	kIx,	,
potkan	potkan	k1gMnSc1	potkan
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
<g/>
,	,	kIx,	,
myšice	myšice	k1gFnSc1	myšice
a	a	k8xC	a
hryzec	hryzec	k1gMnSc1	hryzec
vodní	vodní	k2eAgMnPc1d1	vodní
<g/>
,	,	kIx,	,
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
koroptev	koroptev	k1gFnSc4	koroptev
<g/>
,	,	kIx,	,
káně	káně	k1gNnSc4	káně
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
kalous	kalous	k1gMnSc1	kalous
ušatý	ušatý	k2eAgMnSc1d1	ušatý
<g/>
,	,	kIx,	,
puštík	puštík	k1gMnSc1	puštík
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
holub	holub	k1gMnSc1	holub
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
bažant	bažant	k1gMnSc1	bažant
a	a	k8xC	a
vrána	vrána	k1gFnSc1	vrána
šedá	šedá	k1gFnSc1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
lokalitách	lokalita	k1gFnPc6	lokalita
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
rozborem	rozbor	k1gInSc7	rozbor
vývržků	vývržek	k1gInPc2	vývržek
a	a	k8xC	a
zbytků	zbytek	k1gInPc2	zbytek
potravy	potrava	k1gFnSc2	potrava
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
celkem	celkem	k6eAd1	celkem
3549	[number]	k4	3549
obratlovců	obratlovec	k1gMnPc2	obratlovec
v	v	k7c6	v
80	[number]	k4	80
druzích	druh	k1gInPc6	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
59	[number]	k4	59
druhů	druh	k1gInPc2	druh
představovali	představovat	k5eAaImAgMnP	představovat
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
19	[number]	k4	19
savci	savec	k1gMnPc7	savec
a	a	k8xC	a
2	[number]	k4	2
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
57,3	[number]	k4	57,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
42,3	[number]	k4	42,3
%	%	kIx~	%
a	a	k8xC	a
obojživelníků	obojživelník	k1gMnPc2	obojživelník
0,42	[number]	k4	0,42
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgMnS	objevovat
hraboš	hraboš	k1gMnSc1	hraboš
polní	polní	k2eAgMnSc1d1	polní
(	(	kIx(	(
<g/>
31,2	[number]	k4	31,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
zajícem	zajíc	k1gMnSc7	zajíc
a	a	k8xC	a
koroptví	koroptev	k1gFnSc7	koroptev
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
po	po	k7c6	po
8,7	[number]	k4	8,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
sledovaných	sledovaný	k2eAgFnPc2d1	sledovaná
lokalit	lokalita	k1gFnPc2	lokalita
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
lovenými	lovený	k2eAgMnPc7d1	lovený
druhy	druh	k1gMnPc7	druh
významně	významně	k6eAd1	významně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
také	také	k9	také
racek	racek	k1gMnSc1	racek
chechtavý	chechtavý	k2eAgMnSc1d1	chechtavý
(	(	kIx(	(
<g/>
14,1	[number]	k4	14,1
%	%	kIx~	%
<g/>
;	;	kIx,	;
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
souhrnu	souhrn	k1gInSc6	souhrn
3,7	[number]	k4	3,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
loveckého	lovecký	k2eAgInSc2d1	lovecký
revíru	revír	k1gInSc2	revír
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
úživnosti	úživnost	k1gFnSc6	úživnost
lokality	lokalita	k1gFnSc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
přitom	přitom	k6eAd1	přitom
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
revíry	revír	k1gInPc1	revír
s	s	k7c7	s
bohatší	bohatý	k2eAgFnSc7d2	bohatší
potravní	potravní	k2eAgFnSc7d1	potravní
nabídkou	nabídka	k1gFnSc7	nabídka
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
bráněny	bráněn	k2eAgFnPc1d1	bráněna
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
příslušníkům	příslušník	k1gMnPc3	příslušník
vlastního	vlastní	k2eAgInSc2d1	vlastní
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
překrývání	překrývání	k1gNnSc3	překrývání
<g/>
.	.	kIx.	.
</s>
<s>
Lovecká	lovecký	k2eAgFnSc1d1	lovecká
aktivita	aktivita	k1gFnSc1	aktivita
výra	výr	k1gMnSc2	výr
začíná	začínat	k5eAaImIp3nS	začínat
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
a	a	k8xC	a
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
až	až	k9	až
v	v	k7c6	v
časně	časně	k6eAd1	časně
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
do	do	k7c2	do
časně	časně	k6eAd1	časně
ranních	ranní	k2eAgFnPc2d1	ranní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
při	při	k7c6	při
pomalém	pomalý	k2eAgInSc6d1	pomalý
letu	let	k1gInSc6	let
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
terénem	terén	k1gInSc7	terén
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vsedě	vsedě	k6eAd1	vsedě
z	z	k7c2	z
vyvýšeného	vyvýšený	k2eAgNnSc2d1	vyvýšené
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc2	lov
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
špatných	špatný	k2eAgFnPc2d1	špatná
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
hlavně	hlavně	k9	hlavně
sluchem	sluch	k1gInSc7	sluch
<g/>
,	,	kIx,	,
kořistí	kořistit	k5eAaImIp3nS	kořistit
se	se	k3xPyFc4	se
tak	tak	k9	tak
většinou	většinou	k6eAd1	většinou
stávají	stávat	k5eAaImIp3nP	stávat
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
hlučněji	hlučně	k6eAd2	hlučně
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
kořist	kořist	k1gFnSc1	kořist
polyká	polykat	k5eAaImIp3nS	polykat
celou	celá	k1gFnSc4	celá
<g/>
,	,	kIx,	,
u	u	k7c2	u
větší	veliký	k2eAgFnSc1d2	veliký
často	často	k6eAd1	často
nejprve	nejprve	k6eAd1	nejprve
oddělí	oddělit	k5eAaPmIp3nP	oddělit
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
ježků	ježek	k1gMnPc2	ježek
začíná	začínat	k5eAaImIp3nS	začínat
obvykle	obvykle	k6eAd1	obvykle
břichem	břich	k1gInSc7	břich
a	a	k8xC	a
ostny	osten	k1gInPc1	osten
porostlou	porostlý	k2eAgFnSc4d1	porostlá
kůži	kůže	k1gFnSc4	kůže
nechává	nechávat	k5eAaImIp3nS	nechávat
nedotčenou	dotčený	k2eNgFnSc4d1	nedotčená
<g/>
.	.	kIx.	.
</s>
<s>
Nestravitelných	stravitelný	k2eNgFnPc2d1	nestravitelná
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
srst	srst	k1gFnSc4	srst
nebo	nebo	k8xC	nebo
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vývržků	vývržek	k1gInPc2	vývržek
<g/>
.	.	kIx.	.
</s>
<s>
Výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
stabilní	stabilní	k2eAgInPc4d1	stabilní
páry	pár	k1gInPc4	pár
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
věrností	věrnost	k1gFnSc7	věrnost
k	k	k7c3	k
hnízdišti	hnízdiště	k1gNnSc3	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oblasti	oblast	k1gFnSc6	oblast
několik	několik	k4yIc4	několik
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
postupně	postupně	k6eAd1	postupně
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
věrní	věrný	k2eAgMnPc1d1	věrný
i	i	k8xC	i
hnízdům	hnízdo	k1gNnPc3	hnízdo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
jim	on	k3xPp3gMnPc3	on
byla	být	k5eAaImAgNnP	být
vybrána	vybrán	k2eAgNnPc1d1	vybráno
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
až	až	k8xS	až
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
si	se	k3xPyFc3	se
během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
teritorium	teritorium	k1gNnSc1	teritorium
výrazným	výrazný	k2eAgNnSc7d1	výrazné
"	"	kIx"	"
<g/>
hú	hú	k0	hú
<g/>
–	–	k?	–
<g/>
hú	hú	k0	hú
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
občas	občas	k6eAd1	občas
prokládá	prokládat	k5eAaImIp3nS	prokládat
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
zvuky	zvuk	k1gInPc7	zvuk
<g/>
,	,	kIx,	,
připomínajícími	připomínající	k2eAgFnPc7d1	připomínající
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
řehot	řehot	k1gInSc4	řehot
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
toku	tok	k1gInSc2	tok
ozývá	ozývat	k5eAaImIp3nS	ozývat
hlouběji	hluboko	k6eAd2	hluboko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tišeji	ticho	k6eAd2	ticho
než	než	k8xS	než
samec	samec	k1gMnSc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
stromu	strom	k1gInSc2	strom
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
hnízdě	hnízdo	k1gNnSc6	hnízdo
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
volavek	volavka	k1gFnPc2	volavka
či	či	k8xC	či
čápů	čáp	k1gMnPc2	čáp
černých	černý	k2eAgMnPc2d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výstelku	výstelka	k1gFnSc4	výstelka
mu	on	k3xPp3gInSc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
byliny	bylina	k1gFnSc2	bylina
<g/>
,	,	kIx,	,
spadané	spadaný	k2eAgInPc4d1	spadaný
listy	list	k1gInPc4	list
nebo	nebo	k8xC	nebo
rozdrobené	rozdrobený	k2eAgInPc4d1	rozdrobený
vývržky	vývržek	k1gInPc4	vývržek
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
–	–	k?	–
<g/>
li	li	k8xS	li
o	o	k7c4	o
vejce	vejce	k1gNnSc4	vejce
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
inkubace	inkubace	k1gFnSc2	inkubace
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
snést	snést	k5eAaPmF	snést
náhradní	náhradní	k2eAgFnSc4d1	náhradní
snůšku	snůška	k1gFnSc4	snůška
<g/>
,	,	kIx,	,
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
-li	i	k?	-li
však	však	k9	však
mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
se	se	k3xPyFc4	se
tak	tak	k9	tak
zpravidla	zpravidla	k6eAd1	zpravidla
nestává	stávat	k5eNaImIp3nS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
nebo	nebo	k8xC	nebo
počátkem	počátkem	k7c2	počátkem
dubna	duben	k1gInSc2	duben
snáší	snášet	k5eAaImIp3nS	snášet
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
téměř	téměř	k6eAd1	téměř
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
,	,	kIx,	,
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgNnPc1d1	bílé
vejce	vejce	k1gNnPc1	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
60,20	[number]	k4	60,20
x	x	k?	x
49,37	[number]	k4	49,37
mm	mm	kA	mm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
přibližně	přibližně	k6eAd1	přibližně
73,3	[number]	k4	73,3
g.	g.	k?	g.
Snášena	snášen	k2eAgFnSc1d1	snášena
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
intervalech	interval	k1gInPc6	interval
2-3	[number]	k4	2-3
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
začíná	začínat	k5eAaImIp3nS	začínat
sedět	sedět	k5eAaImF	sedět
od	od	k7c2	od
snesení	snesení	k1gNnPc2	snesení
1	[number]	k4	1
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
.	.	kIx.	.
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
ji	on	k3xPp3gFnSc4	on
během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
období	období	k1gNnSc2	období
inkubace	inkubace	k1gFnSc2	inkubace
krmí	krmě	k1gFnPc2	krmě
<g/>
,	,	kIx,	,
kořist	kořist	k1gFnSc1	kořist
jí	on	k3xPp3gFnSc3	on
ale	ale	k9	ale
většinou	většinou	k6eAd1	většinou
nepředává	předávat	k5eNaImIp3nS	předávat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
sezení	sezení	k1gNnSc2	sezení
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
prvních	první	k4xOgInPc2	první
dní	den	k1gInPc2	den
zahřívána	zahříván	k2eAgFnSc1d1	zahřívána
samicí	samice	k1gFnSc7	samice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
krmena	krmen	k2eAgFnSc1d1	krmena
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
týdnech	týden	k1gInPc6	týden
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
a	a	k8xC	a
rozlézají	rozlézat	k5eAaImIp3nP	rozlézat
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
rodiči	rodič	k1gMnPc7	rodič
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
krmena	krmen	k2eAgFnSc1d1	krmena
až	až	k9	až
do	do	k7c2	do
dosažení	dosažení	k1gNnSc2	dosažení
vzletnosti	vzletnost	k1gFnSc2	vzletnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
9	[number]	k4	9
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
paradoxní	paradoxní	k2eAgNnSc1d1	paradoxní
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
největší	veliký	k2eAgFnSc1d3	veliký
sova	sova	k1gFnSc1	sova
<g/>
,	,	kIx,	,
vybrat	vybrat	k5eAaPmF	vybrat
jeho	jeho	k3xOp3gNnSc4	jeho
hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
staví	stavit	k5eAaImIp3nS	stavit
výhrůžně	výhrůžně	k6eAd1	výhrůžně
<g/>
,	,	kIx,	,
syčí	syčet	k5eAaImIp3nS	syčet
a	a	k8xC	a
nafukuje	nafukovat	k5eAaImIp3nS	nafukovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
člověka	člověk	k1gMnSc4	člověk
neodradí	odradit	k5eNaPmIp3nS	odradit
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
rychlý	rychlý	k2eAgInSc4d1	rychlý
ústup	ústup	k1gInSc4	ústup
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
povzdálí	povzdálí	k6eAd1	povzdálí
smutně	smutně	k6eAd1	smutně
houká	houkat	k5eAaImIp3nS	houkat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
podstatně	podstatně	k6eAd1	podstatně
menších	malý	k2eAgMnPc2d2	menší
puštíků	puštík	k1gMnPc2	puštík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
svou	svůj	k3xOyFgFnSc4	svůj
snůšku	snůška	k1gFnSc4	snůška
fanaticky	fanaticky	k6eAd1	fanaticky
brání	bránit	k5eAaImIp3nP	bránit
proti	proti	k7c3	proti
jakémukoliv	jakýkoliv	k3yIgMnSc3	jakýkoliv
útočníkovi	útočník	k1gMnSc3	útočník
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
28,9	[number]	k4	28,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
se	s	k7c7	s
ztrátami	ztráta	k1gFnPc7	ztráta
daleko	daleko	k6eAd1	daleko
vyššími	vysoký	k2eAgFnPc7d2	vyšší
<g/>
;	;	kIx,	;
např.	např.	kA	např.
v	v	k7c6	v
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
nebylo	být	k5eNaImAgNnS	být
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
58,4	[number]	k4	58,4
%	%	kIx~	%
započatých	započatý	k2eAgNnPc2d1	započaté
hnízdění	hnízdění	k1gNnPc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
ztrát	ztráta	k1gFnPc2	ztráta
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
predace	predace	k1gFnSc1	predace
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
výrem	výr	k1gMnSc7	výr
velkým	velký	k2eAgMnPc3d1	velký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
počasí	počasí	k1gNnSc1	počasí
a	a	k8xC	a
neoplozená	oplozený	k2eNgNnPc1d1	neoplozené
vejce	vejce	k1gNnPc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
však	však	k9	však
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
až	až	k9	až
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
známý	známý	k2eAgInSc1d1	známý
věk	věk	k1gInSc1	věk
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
je	být	k5eAaImIp3nS	být
člověkem	člověk	k1gMnSc7	člověk
odpradávna	odpradávna	k6eAd1	odpradávna
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
jako	jako	k8xS	jako
škodná	škodný	k2eAgFnSc1d1	škodná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
Evropy	Evropa	k1gFnSc2	Evropa
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vyhubení	vyhubení	k1gNnSc3	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnSc1d2	podrobnější
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
racionálně	racionálně	k6eAd1	racionálně
hodnotila	hodnotit	k5eAaImAgFnS	hodnotit
význam	význam	k1gInSc4	význam
přítomnosti	přítomnost	k1gFnSc2	přítomnost
výra	výr	k1gMnSc2	výr
v	v	k7c6	v
honitbě	honitba	k1gFnSc6	honitba
na	na	k7c4	na
stav	stav	k1gInSc4	stav
lovné	lovný	k2eAgFnSc2d1	lovná
zvěře	zvěř	k1gFnSc2	zvěř
přitom	přitom	k6eAd1	přitom
dodnes	dodnes	k6eAd1	dodnes
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
"	"	kIx"	"
<g/>
na	na	k7c4	na
výrovku	výrovka	k1gFnSc4	výrovka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
přivázal	přivázat	k5eAaPmAgMnS	přivázat
na	na	k7c4	na
relativně	relativně	k6eAd1	relativně
viditelné	viditelný	k2eAgNnSc4d1	viditelné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
lovec	lovec	k1gMnSc1	lovec
s	s	k7c7	s
puškou	puška	k1gFnSc7	puška
a	a	k8xC	a
vyčkávalo	vyčkávat	k5eAaImAgNnS	vyčkávat
se	se	k3xPyFc4	se
na	na	k7c4	na
agresivní	agresivní	k2eAgFnPc4d1	agresivní
reakce	reakce	k1gFnPc4	reakce
dravců	dravec	k1gMnPc2	dravec
a	a	k8xC	a
krkavcovitých	krkavcovitý	k2eAgMnPc2d1	krkavcovitý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
výra	výr	k1gMnSc4	výr
útočili	útočit	k5eAaImAgMnP	útočit
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc4	všechen
ostatní	ostatní	k1gNnSc4	ostatní
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
a	a	k8xC	a
dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
před	před	k7c4	před
pušku	puška	k1gFnSc4	puška
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
denních	denní	k2eAgMnPc2d1	denní
ptáků	pták	k1gMnPc2	pták
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
při	při	k7c6	při
odchytu	odchyt	k1gInSc6	odchyt
ptactva	ptactvo	k1gNnSc2	ptactvo
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
kroužkování	kroužkování	k1gNnSc2	kroužkování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
maketa	maketa	k1gFnSc1	maketa
výra	výr	k1gMnSc2	výr
umístí	umístit	k5eAaPmIp3nS	umístit
před	před	k7c4	před
ornitologickou	ornitologický	k2eAgFnSc4d1	ornitologická
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zvláště	zvláště	k6eAd1	zvláště
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dat	datum	k1gNnPc2	datum
získaných	získaný	k2eAgNnPc2d1	získané
od	od	k7c2	od
našich	náš	k3xOp1gMnPc2	náš
okroužkovaných	okroužkovaný	k2eAgMnPc2d1	okroužkovaný
ptáků	pták	k1gMnPc2	pták
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
úhynu	úhyn	k1gInSc2	úhyn
je	být	k5eAaImIp3nS	být
zabití	zabití	k1gNnSc1	zabití
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
srážka	srážka	k1gFnSc1	srážka
s	s	k7c7	s
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnSc1	silnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
nelegální	legální	k2eNgInSc1d1	nelegální
odstřel	odstřel	k1gInSc1	odstřel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
střelení	střelený	k2eAgMnPc1d1	střelený
ptáci	pták	k1gMnPc1	pták
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
mortalitě	mortalita	k1gFnSc6	mortalita
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gInPc1	jejich
nálezy	nález	k1gInPc1	nález
nikdo	nikdo	k3yNnSc1	nikdo
nehlásí	hlásit	k5eNaImIp3nS	hlásit
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
mrtev	mrtev	k2eAgMnSc1d1	mrtev
bez	bez	k7c2	bez
bližších	blízký	k2eAgInPc2d2	bližší
údajů	údaj	k1gInPc2	údaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
