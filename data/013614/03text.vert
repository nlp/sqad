<s>
Renata	Renata	k1gFnSc1
Knapiková-Miazgová	Knapiková-Miazgový	k2eAgFnSc1d1
</s>
<s>
Renata	Renata	k1gFnSc1
Knapiková-Miazgová	Knapiková-Miazgový	k2eAgFnSc1d1
Narození	narození	k1gNnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1988	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Tarnów	Tarnów	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
šermířka	šermířka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
šermu	šerm	k1gInSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
2013	#num#	k4
</s>
<s>
šerm	šerm	k1gInSc1
kordem	kord	k1gInSc7
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
2016	#num#	k4
</s>
<s>
šerm	šerm	k1gInSc1
kordem	kord	k1gInSc7
</s>
<s>
Renata	Renata	k1gFnSc1
Knapiková-Miazgová	 Knapiková-Miazgová		k1gFnSc1
rozená	rozený	k2eAgFnSc1d1
Renata	Renata	k1gFnSc1
Knapiková	Knapiková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1988	#num#	k4
Tarnów	Tarnów	k1gFnPc2
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
polská	polský	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
šermířka	šermířka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
šerm	šerm	k1gInSc4
kordem	kord	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polsko	Polsko	k1gNnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
od	od	k7c2
druhého	druhý	k4xOgNnSc2
desetiletí	desetiletí	k1gNnSc2
jednadvacátého	jednadvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
a	a	k8xC
2016	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
místo	místo	k7c2
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
jednotlivkyň	jednotlivkyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Renata	Renata	k1gFnSc1
Knapiková-Miazgová	Knapiková-Miazgový	k2eAgFnSc1d1
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
