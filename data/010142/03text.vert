<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Catcher	Catchra	k1gFnPc2	Catchra
in	in	k?	in
the	the	k?	the
Rye	Rye	k1gFnSc1	Rye
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
Jerome	Jerom	k1gInSc5	Jerom
David	David	k1gMnSc1	David
Salinger	Salingero	k1gNnPc2	Salingero
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1951	[number]	k4	1951
jej	on	k3xPp3gMnSc4	on
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Little	Little	k1gFnSc2	Little
<g/>
,	,	kIx,	,
Brown	Brown	k1gMnSc1	Brown
and	and	k?	and
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
hlavně	hlavně	k6eAd1	hlavně
kvůli	kvůli	k7c3	kvůli
vulgarismům	vulgarismus	k1gInPc3	vulgarismus
<g/>
,	,	kIx,	,
vykreslené	vykreslený	k2eAgFnSc3d1	vykreslená
sexualitě	sexualita	k1gFnSc3	sexualita
a	a	k8xC	a
hněvu	hněv	k1gInSc3	hněv
dospívající	dospívající	k2eAgFnSc2d1	dospívající
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
a	a	k8xC	a
časopis	časopis	k1gInSc1	časopis
Time	Tim	k1gInSc2	Tim
ji	on	k3xPp3gFnSc4	on
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
stovku	stovka	k1gFnSc4	stovka
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
anglicky	anglicky	k6eAd1	anglicky
napsaných	napsaný	k2eAgFnPc2d1	napsaná
knih	kniha	k1gFnPc2	kniha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
<g/>
Kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Rudolfa	Rudolf	k1gMnSc2	Rudolf
a	a	k8xC	a
Luby	lub	k1gInPc7	lub
Pellarových	Pellarová	k1gFnPc2	Pellarová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1949	[number]	k4	1949
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgNnSc6d1	fiktivní
pennsylvánském	pennsylvánský	k2eAgNnSc6d1	pennsylvánský
městečku	městečko	k1gNnSc6	městečko
Agerstown	Agerstowna	k1gFnPc2	Agerstowna
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěčem	vypravěč	k1gMnSc7	vypravěč
je	být	k5eAaImIp3nS	být
Holden	Holdna	k1gFnPc2	Holdna
Caulfield	Caulfield	k1gMnSc1	Caulfield
<g/>
,	,	kIx,	,
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
syn	syn	k1gMnSc1	syn
zámožného	zámožný	k2eAgMnSc2d1	zámožný
newyorského	newyorský	k2eAgMnSc2d1	newyorský
advokáta	advokát	k1gMnSc2	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
svérázný	svérázný	k2eAgMnSc1d1	svérázný
mudrlant	mudrlant	k1gMnSc1	mudrlant
<g/>
,	,	kIx,	,
sarkasticky	sarkasticky	k6eAd1	sarkasticky
glosující	glosující	k2eAgNnSc4d1	glosující
dění	dění	k1gNnSc4	dění
okolo	okolo	k7c2	okolo
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
pohrdající	pohrdající	k2eAgNnSc1d1	pohrdající
měšťáckým	měšťácký	k2eAgNnSc7d1	měšťácké
snobstvím	snobství	k1gNnSc7	snobství
a	a	k8xC	a
stádností	stádnost	k1gFnSc7	stádnost
<g/>
,	,	kIx,	,
za	za	k7c7	za
cynickou	cynický	k2eAgFnSc7d1	cynická
slupkou	slupka	k1gFnSc7	slupka
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
hluboká	hluboký	k2eAgFnSc1d1	hluboká
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
nejistota	nejistota	k1gFnSc1	nejistota
a	a	k8xC	a
zoufalé	zoufalý	k2eAgNnSc1d1	zoufalé
hledání	hledání	k1gNnSc1	hledání
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
má	mít	k5eAaImIp3nS	mít
opravdu	opravdu	k6eAd1	opravdu
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
je	být	k5eAaImIp3nS	být
Holden	Holdno	k1gNnPc2	Holdno
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
prestižní	prestižní	k2eAgFnSc2d1	prestižní
soukromé	soukromý	k2eAgFnSc2d1	soukromá
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Agerstownu	Agerstown	k1gInSc6	Agerstown
pro	pro	k7c4	pro
nezájem	nezájem	k1gInSc4	nezájem
o	o	k7c6	o
učení	učení	k1gNnSc6	učení
a	a	k8xC	a
neustálé	neustálý	k2eAgInPc4d1	neustálý
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
pedagogy	pedagog	k1gMnPc7	pedagog
a	a	k8xC	a
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepůjde	jít	k5eNaImIp3nS	jít
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
přímo	přímo	k6eAd1	přímo
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ubytuje	ubytovat	k5eAaPmIp3nS	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
a	a	k8xC	a
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
se	se	k3xPyFc4	se
do	do	k7c2	do
newyorského	newyorský	k2eAgInSc2d1	newyorský
nočního	noční	k2eAgInSc2d1	noční
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
barmany	barman	k1gMnPc7	barman
<g/>
,	,	kIx,	,
taxikáři	taxikář	k1gMnPc7	taxikář
a	a	k8xC	a
prostitutkami	prostitutka	k1gFnPc7	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Citlivý	citlivý	k2eAgMnSc1d1	citlivý
mladík	mladík	k1gMnSc1	mladík
postupně	postupně	k6eAd1	postupně
poznává	poznávat	k5eAaImIp3nS	poznávat
pokrytectví	pokrytectví	k1gNnSc1	pokrytectví
<g/>
,	,	kIx,	,
bezohlednost	bezohlednost	k1gFnSc1	bezohlednost
a	a	k8xC	a
chamtivost	chamtivost	k1gFnSc1	chamtivost
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
během	běh	k1gInSc7	běh
bloudění	bloudění	k1gNnSc2	bloudění
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
duševního	duševní	k2eAgNnSc2d1	duševní
a	a	k8xC	a
fyzického	fyzický	k2eAgNnSc2d1	fyzické
zhroucení	zhroucení	k1gNnSc2	zhroucení
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjede	odjet	k5eAaPmIp3nS	odjet
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
a	a	k8xC	a
najde	najít	k5eAaPmIp3nS	najít
si	se	k3xPyFc3	se
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
ranči	ranč	k1gInSc6	ranč
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Phoebe	Phoeb	k1gInSc5	Phoeb
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
šel	jít	k5eAaImAgMnS	jít
domů	dům	k1gInPc2	dům
a	a	k8xC	a
přiznal	přiznat	k5eAaPmAgMnS	přiznat
se	se	k3xPyFc4	se
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Holden	Holdna	k1gFnPc2	Holdna
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
další	další	k2eAgFnPc4d1	další
postavy	postava	k1gFnPc4	postava
<g/>
:	:	kIx,	:
Holdenovi	Holdenův	k2eAgMnPc1d1	Holdenův
spolubydlící	spolubydlící	k1gMnPc1	spolubydlící
na	na	k7c6	na
internátu	internát	k1gInSc6	internát
<g/>
,	,	kIx,	,
natvrdlý	natvrdlý	k2eAgInSc1d1	natvrdlý
Ackley	Ackle	k1gMnPc7	Ackle
a	a	k8xC	a
sebestředný	sebestředný	k2eAgInSc4d1	sebestředný
seladon	seladon	k1gInSc4	seladon
Stradlater	Stradlatra	k1gFnPc2	Stradlatra
<g/>
,	,	kIx,	,
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Sally	Salla	k1gFnSc2	Salla
Hayesová	Hayesový	k2eAgFnSc1d1	Hayesová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
pohádá	pohádat	k5eAaPmIp3nS	pohádat
kvůli	kvůli	k7c3	kvůli
jejím	její	k3xOp3gInPc3	její
konformním	konformní	k2eAgInPc3d1	konformní
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
nabubřelý	nabubřelý	k2eAgMnSc1d1	nabubřelý
intelektuál	intelektuál	k1gMnSc1	intelektuál
Carl	Carl	k1gMnSc1	Carl
Luce	Luce	k1gNnSc4	Luce
nebo	nebo	k8xC	nebo
Holdenův	Holdenův	k2eAgMnSc1d1	Holdenův
bývalý	bývalý	k2eAgMnSc1d1	bývalý
učitel	učitel	k1gMnSc1	učitel
pan	pan	k1gMnSc1	pan
Antolini	Antolin	k2eAgMnPc1d1	Antolin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gNnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Známkou	známka	k1gFnSc7	známka
nezralého	zralý	k2eNgMnSc4d1	nezralý
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
kvůli	kvůli	k7c3	kvůli
něčemu	něco	k3yInSc3	něco
podstoupit	podstoupit	k5eAaPmF	podstoupit
vznešenou	vznešený	k2eAgFnSc4d1	vznešená
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
známkou	známka	k1gFnSc7	známka
zralého	zralý	k2eAgMnSc2d1	zralý
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
kvůli	kvůli	k7c3	kvůli
něčemu	něco	k3yInSc3	něco
v	v	k7c6	v
příkoří	příkoří	k1gNnSc6	příkoří
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Holden	Holdna	k1gFnPc2	Holdna
také	také	k9	také
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
přímo	přímo	k6eAd1	přímo
nevystupují	vystupovat	k5eNaImIp3nP	vystupovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
:	:	kIx,	:
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Allieho	Allie	k1gMnSc2	Allie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
leukémii	leukémie	k1gFnSc4	leukémie
<g/>
,	,	kIx,	,
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
D.B.	D.B.	k1gMnSc2	D.B.
<g/>
,	,	kIx,	,
nadaného	nadaný	k2eAgMnSc2d1	nadaný
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hollywoodským	hollywoodský	k2eAgMnSc7d1	hollywoodský
scenáristou	scenárista	k1gMnSc7	scenárista
nebo	nebo	k8xC	nebo
kamarádku	kamarádka	k1gFnSc4	kamarádka
Hanku	Hanka	k1gFnSc4	Hanka
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgMnS	poznat
na	na	k7c6	na
prázdninách	prázdniny	k1gFnPc6	prázdniny
v	v	k7c6	v
Maine	Main	k1gMnSc5	Main
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
parafrází	parafráze	k1gFnSc7	parafráze
na	na	k7c4	na
báseň	báseň	k1gFnSc4	báseň
Roberta	Robert	k1gMnSc2	Robert
Burnse	Burns	k1gMnSc2	Burns
Comin	Comin	k1gMnSc1	Comin
<g/>
'	'	kIx"	'
Thro	Thro	k1gMnSc1	Thro
The	The	k1gMnSc1	The
Rye	Rye	k1gMnSc1	Rye
<g/>
.	.	kIx.	.
</s>
<s>
Holden	Holdna	k1gFnPc2	Holdna
potká	potkat	k5eAaPmIp3nS	potkat
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
chlapečka	chlapeček	k1gMnSc2	chlapeček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
zpívá	zpívat	k5eAaImIp3nS	zpívat
"	"	kIx"	"
<g/>
Když	když	k8xS	když
někoho	někdo	k3yInSc4	někdo
potká	potkat	k5eAaPmIp3nS	potkat
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jde	jít	k5eAaImIp3nS	jít
žitem	žito	k1gNnSc7	žito
kdes	kdes	k6eAd1	kdes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Holden	Holdna	k1gFnPc2	Holdna
dává	dávat	k5eAaImIp3nS	dávat
své	svůj	k3xOyFgFnSc3	svůj
intuici	intuice	k1gFnSc3	intuice
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
formálním	formální	k2eAgNnSc7d1	formální
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
,	,	kIx,	,
vsugeruje	vsugerovat	k5eAaPmIp3nS	vsugerovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
verš	verš	k1gInSc1	verš
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
Když	když	k8xS	když
někoho	někdo	k3yInSc4	někdo
chytí	chytit	k5eAaPmIp3nS	chytit
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jde	jít	k5eAaImIp3nS	jít
žitem	žito	k1gNnSc7	žito
kdes	kdes	k6eAd1	kdes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
pak	pak	k6eAd1	pak
Phoebe	Phoeb	k1gInSc5	Phoeb
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
má	mít	k5eAaImIp3nS	mít
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
budoucím	budoucí	k2eAgNnSc6d1	budoucí
povolání	povolání	k1gNnSc6	povolání
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
jí	on	k3xPp3gFnSc3	on
o	o	k7c6	o
žitném	žitný	k2eAgNnSc6d1	žitné
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hrají	hrát	k5eAaImIp3nP	hrát
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
strmý	strmý	k2eAgInSc4d1	strmý
sráz	sráz	k1gInSc4	sráz
a	a	k8xC	a
u	u	k7c2	u
něho	on	k3xPp3gInSc2	on
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k1gInSc4	stát
Holden	Holdna	k1gFnPc2	Holdna
a	a	k8xC	a
chytat	chytat	k5eAaImF	chytat
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nespadly	spadnout	k5eNaPmAgFnP	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc4d1	jediné
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
https://web.archive.org/web/20140907073840/http://salinger.org/index.php?title=The_Catcher_in_the_Rye_(book	[url]	k1gInSc1	https://web.archive.org/web/20140907073840/http://salinger.org/index.php?title=The_Catcher_in_the_Rye_(book
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.cliffsnotes.com/literature/c/the-catcher-in-the-rye/the-catcher-in-the-rye-at-a-glance	[url]	k1gFnSc1	http://www.cliffsnotes.com/literature/c/the-catcher-in-the-rye/the-catcher-in-the-rye-at-a-glance
</s>
</p>
<p>
<s>
http://www.rozbor-dila.cz/kdo-chyta-v-zite-rozbor-dila/	[url]	k?	http://www.rozbor-dila.cz/kdo-chyta-v-zite-rozbor-dila/
</s>
</p>
<p>
<s>
http://www.cesky-jazyk.cz/ctenarsky-denik/jerome-david-salinger/kdo-chyta-v-zite-2.html	[url]	k1gMnSc1	http://www.cesky-jazyk.cz/ctenarsky-denik/jerome-david-salinger/kdo-chyta-v-zite-2.html
</s>
</p>
