<s>
Neverbální	verbální	k2eNgFnSc1d1	neverbální
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
nonverbální	nonverbální	k2eAgFnSc1d1	nonverbální
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc4	souhrn
mimoslovních	mimoslovní	k2eAgNnPc2d1	mimoslovní
sdělení	sdělení	k1gNnPc2	sdělení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vědomě	vědomě	k6eAd1	vědomě
nebo	nebo	k8xC	nebo
nevědomě	vědomě	k6eNd1	vědomě
předávána	předávat	k5eAaImNgFnS	předávat
člověkem	člověk	k1gMnSc7	člověk
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
osobě	osoba	k1gFnSc3	osoba
nebo	nebo	k8xC	nebo
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
komunikační	komunikační	k2eAgFnSc6d1	komunikační
výměně	výměna	k1gFnSc6	výměna
<g/>
,	,	kIx,	,
též	též	k9	též
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
i	i	k9	i
při	při	k7c6	při
sociální	sociální	k2eAgFnSc6d1	sociální
interakci	interakce	k1gFnSc6	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
opsáno	opsán	k2eAgNnSc1d1	opsáno
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
komunikaci	komunikace	k1gFnSc4	komunikace
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Produkované	produkovaný	k2eAgInPc1d1	produkovaný
neverbální	verbální	k2eNgInPc1d1	neverbální
signály	signál	k1gInPc1	signál
jsou	být	k5eAaImIp3nP	být
mnohovýznamnové	mnohovýznamnový	k2eAgInPc1d1	mnohovýznamnový
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
jak	jak	k8xC	jak
na	na	k7c6	na
úrovní	úroveň	k1gFnPc2	úroveň
kulturní	kulturní	k2eAgFnPc4d1	kulturní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
individuální	individuální	k2eAgFnSc6d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Neverbální	verbální	k2eNgInPc1d1	neverbální
projevy	projev	k1gInPc1	projev
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
haptika	haptika	k1gFnSc1	haptika
-	-	kIx~	-
dotyk	dotyk	k1gInSc1	dotyk
kinezika	kinezik	k1gMnSc2	kinezik
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
součást	součást	k1gFnSc1	součást
gestika	gestika	k1gFnSc1	gestika
-	-	kIx~	-
pohyby	pohyb	k1gInPc1	pohyb
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
rukou	ruka	k1gFnPc2	ruka
mimika	mimika	k1gFnSc1	mimika
-	-	kIx~	-
pohyby	pohyb	k1gInPc1	pohyb
obličeje	obličej	k1gInSc2	obličej
oční	oční	k2eAgInSc4d1	oční
kontakt	kontakt	k1gInSc4	kontakt
(	(	kIx(	(
<g/>
občas	občas	k6eAd1	občas
též	též	k9	též
vizika	vizika	k1gFnSc1	vizika
<g/>
)	)	kIx)	)
posturika	posturika	k1gFnSc1	posturika
-	-	kIx~	-
postoj	postoj	k1gInSc1	postoj
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
proxemika	proxemika	k1gFnSc1	proxemika
-	-	kIx~	-
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
komunikujících	komunikující	k2eAgFnPc2d1	komunikující
chronemika	chronemika	k1gFnSc1	chronemika
-	-	kIx~	-
nakládání	nakládání	k1gNnSc1	nakládání
s	s	k7c7	s
časem	čas	k1gInSc7	čas
při	při	k7c6	při
neverbální	verbální	k2eNgFnSc6d1	neverbální
komunikaci	komunikace	k1gFnSc6	komunikace
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
neverbální	verbální	k2eNgFnSc2d1	neverbální
komunikace	komunikace	k1gFnSc2	komunikace
si	se	k3xPyFc3	se
s	s	k7c7	s
druhými	druhý	k4xOgInPc7	druhý
lidmi	člověk	k1gMnPc7	člověk
předáváme	předávat	k5eAaImIp1nP	předávat
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vnímáme	vnímat	k5eAaImIp1nP	vnímat
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
a	a	k8xC	a
o	o	k7c6	o
vlastních	vlastní	k2eAgFnPc6d1	vlastní
emocích	emoce	k1gFnPc6	emoce
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc6	napětí
a	a	k8xC	a
rozpoložení	rozpoložení	k1gNnSc6	rozpoložení
<g/>
.	.	kIx.	.
</s>
<s>
Neverbální	verbální	k2eNgInPc1d1	neverbální
signály	signál	k1gInPc1	signál
vysíláme	vysílat	k5eAaImIp1nP	vysílat
většinou	většinou	k6eAd1	většinou
podvědomě	podvědomě	k6eAd1	podvědomě
a	a	k8xC	a
přijímáme	přijímat	k5eAaImIp1nP	přijímat
je	on	k3xPp3gFnPc4	on
také	také	k9	také
podvědomě	podvědomě	k6eAd1	podvědomě
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
však	však	k9	však
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
konvenční	konvenční	k2eAgFnSc2d1	konvenční
morálky	morálka	k1gFnSc2	morálka
předstírají	předstírat	k5eAaImIp3nP	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
jim	on	k3xPp3gMnPc3	on
sympatičtí	sympatický	k2eAgMnPc1d1	sympatický
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
něco	něco	k3yInSc1	něco
zajímá	zajímat	k5eAaImIp3nS	zajímat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všemu	všecek	k3xTgNnSc3	všecek
rozumí	rozumět	k5eAaImIp3nS	rozumět
apod.	apod.	kA	apod.
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
i	i	k9	i
mimoslovní	mimoslovní	k2eAgFnSc4d1	mimoslovní
komunikaci	komunikace	k1gFnSc4	komunikace
předstíranému	předstíraný	k2eAgInSc3d1	předstíraný
dojmu	dojem	k1gInSc3	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Podívejme	podívat	k5eAaPmRp1nP	podívat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
na	na	k7c4	na
fotografie	fotografia	k1gFnPc4	fotografia
politiků	politik	k1gMnPc2	politik
<g/>
:	:	kIx,	:
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
používá	používat	k5eAaImIp3nS	používat
naučeného	naučený	k2eAgNnSc2d1	naučené
gesta	gesto	k1gNnSc2	gesto
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dlaní	dlaň	k1gFnPc2	dlaň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mimoslovně	mimoslovně	k6eAd1	mimoslovně
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
"	"	kIx"	"
<g/>
nemám	mít	k5eNaImIp1nS	mít
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
jdu	jít	k5eAaImIp1nS	jít
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
s	s	k7c7	s
čistými	čistý	k2eAgInPc7d1	čistý
<g/>
,	,	kIx,	,
přátelskými	přátelský	k2eAgInPc7d1	přátelský
úmysly	úmysl	k1gInPc7	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
naučené	naučený	k2eAgNnSc1d1	naučené
a	a	k8xC	a
záměrně	záměrně	k6eAd1	záměrně
použité	použitý	k2eAgNnSc4d1	Použité
gesto	gesto	k1gNnSc4	gesto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vyslat	vyslat	k5eAaPmF	vyslat
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
signály	signál	k1gInPc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
"	"	kIx"	"
<g/>
čtení	čtení	k1gNnSc6	čtení
<g/>
"	"	kIx"	"
mimoslovní	mimoslovní	k2eAgFnSc1d1	mimoslovní
komunikace	komunikace	k1gFnSc1	komunikace
se	se	k3xPyFc4	se
nelze	lze	k6eNd1	lze
spoléhat	spoléhat	k5eAaImF	spoléhat
jen	jen	k9	jen
na	na	k7c4	na
výraz	výraz	k1gInSc4	výraz
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
cenné	cenný	k2eAgFnPc4d1	cenná
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohé	mnohé	k1gNnSc1	mnohé
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
účelově	účelově	k6eAd1	účelově
předstírané	předstíraný	k2eAgNnSc1d1	předstírané
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
chceme	chtít	k5eAaImIp1nP	chtít
orientovat	orientovat	k5eAaBmF	orientovat
podle	podle	k7c2	podle
řeči	řeč	k1gFnSc2	řeč
těla	tělo	k1gNnSc2	tělo
druhých	druhý	k4xOgMnPc2	druhý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
být	být	k5eAaImF	být
jisti	jist	k2eAgMnPc1d1	jist
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
alespoň	alespoň	k9	alespoň
tři	tři	k4xCgInPc1	tři
projevy	projev	k1gInPc1	projev
mimoslovní	mimoslovní	k2eAgFnSc2d1	mimoslovní
komunikace	komunikace	k1gFnSc2	komunikace
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
<g/>
,	,	kIx,	,
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
když	když	k8xS	když
tři	tři	k4xCgInPc1	tři
signály	signál	k1gInPc1	signál
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
například	například	k6eAd1	například
rozpaky	rozpak	k1gInPc4	rozpak
<g/>
,	,	kIx,	,
pohodu	pohoda	k1gFnSc4	pohoda
<g/>
,	,	kIx,	,
nervozity	nervozita	k1gFnPc4	nervozita
<g/>
,	,	kIx,	,
agresivitu	agresivita	k1gFnSc4	agresivita
nebo	nebo	k8xC	nebo
oddanost	oddanost	k1gFnSc4	oddanost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
signály	signál	k1gInPc4	signál
řeči	řeč	k1gFnSc2	řeč
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
klid	klid	k1gInSc4	klid
a	a	k8xC	a
vyrovnanost	vyrovnanost	k1gFnSc4	vyrovnanost
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
vnímat	vnímat	k5eAaImF	vnímat
<g/>
,	,	kIx,	,
když	když	k8xS	když
druhý	druhý	k4xOgMnSc1	druhý
člověk	člověk	k1gMnSc1	člověk
klidně	klidně	k6eAd1	klidně
a	a	k8xC	a
volně	volně	k6eAd1	volně
využívá	využívat	k5eAaPmIp3nS	využívat
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
nesouměrné	souměrný	k2eNgNnSc4d1	nesouměrné
posazení	posazení	k1gNnSc4	posazení
<g/>
,	,	kIx,	,
vizuální	vizuální	k2eAgInSc4d1	vizuální
kontakt	kontakt	k1gInSc4	kontakt
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
negativní	negativní	k2eAgInPc1d1	negativní
signály	signál	k1gInPc1	signál
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
něco	něco	k3yInSc1	něco
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
hodnotit	hodnotit	k5eAaImF	hodnotit
nevyužívání	nevyužívání	k1gNnPc4	nevyužívání
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
nervózní	nervózní	k2eAgInPc4d1	nervózní
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
roztěkanost	roztěkanost	k1gFnSc1	roztěkanost
<g/>
,	,	kIx,	,
neklid	neklid	k1gInSc1	neklid
<g/>
,	,	kIx,	,
dotýkání	dotýkání	k1gNnSc1	dotýkání
se	se	k3xPyFc4	se
obličeje	obličej	k1gInPc1	obličej
<g/>
,	,	kIx,	,
zaťaté	zaťatý	k2eAgFnPc1d1	zaťatá
pěsti	pěst	k1gFnPc1	pěst
<g/>
,	,	kIx,	,
upjatost	upjatost	k1gFnSc1	upjatost
<g/>
,	,	kIx,	,
afektovaný	afektovaný	k2eAgInSc1d1	afektovaný
projev	projev	k1gInSc1	projev
<g/>
.	.	kIx.	.
</s>
<s>
Nervozitu	nervozita	k1gFnSc4	nervozita
poznáme	poznat	k5eAaPmIp1nP	poznat
podle	podle	k7c2	podle
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
podle	podle	k7c2	podle
nohou	noha	k1gFnPc2	noha
-	-	kIx~	-
podle	podle	k7c2	podle
podvědomých	podvědomý	k2eAgInPc2d1	podvědomý
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
druhá	druhý	k4xOgFnSc1	druhý
osoba	osoba	k1gFnSc1	osoba
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
svoje	svůj	k3xOyFgNnSc4	svůj
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nemluví	mluvit	k5eNaImIp3nS	mluvit
pravdu	pravda	k1gFnSc4	pravda
či	či	k8xC	či
předstírají	předstírat	k5eAaImIp3nP	předstírat
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
méně	málo	k6eAd2	málo
gestikulace	gestikulace	k1gFnSc2	gestikulace
a	a	k8xC	a
více	hodně	k6eAd2	hodně
tzv.	tzv.	kA	tzv.
autokontaktů	autokontakt	k1gInPc2	autokontakt
(	(	kIx(	(
<g/>
mnou	já	k3xPp1nSc7	já
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
tahají	tahat	k5eAaImIp3nP	tahat
se	se	k3xPyFc4	se
za	za	k7c4	za
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
se	s	k7c7	s
tváří	tvář	k1gFnSc7	tvář
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nepřátelství	nepřátelství	k1gNnSc6	nepřátelství
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
podle	podle	k7c2	podle
agresivních	agresivní	k2eAgInPc2d1	agresivní
pohybů	pohyb	k1gInPc2	pohyb
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
sevřených	sevřený	k2eAgFnPc2d1	sevřená
pěstí	pěst	k1gFnPc2	pěst
<g/>
,	,	kIx,	,
chladného	chladný	k2eAgInSc2d1	chladný
pohledu	pohled	k1gInSc2	pohled
apod.	apod.	kA	apod.
Signály	signál	k1gInPc4	signál
sdělující	sdělující	k2eAgInPc4d1	sdělující
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
partner	partner	k1gMnSc1	partner
uzavřít	uzavřít	k5eAaPmF	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
jsou	být	k5eAaImIp3nP	být
náhlé	náhlý	k2eAgNnSc4d1	náhlé
uvolnění	uvolnění	k1gNnSc4	uvolnění
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
zrakový	zrakový	k2eAgInSc1d1	zrakový
kontakt	kontakt	k1gInSc1	kontakt
<g/>
,	,	kIx,	,
přiblížení	přiblížení	k1gNnSc1	přiblížení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
někdo	někdo	k3yInSc1	někdo
psychicky	psychicky	k6eAd1	psychicky
vyloženě	vyloženě	k6eAd1	vyloženě
"	"	kIx"	"
<g/>
ladí	ladit	k5eAaImIp3nP	ladit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projeví	projevit	k5eAaPmIp3nS	projevit
i	i	k9	i
souladem	soulad	k1gInSc7	soulad
<g/>
,	,	kIx,	,
zrcadlením	zrcadlení	k1gNnSc7	zrcadlení
<g/>
,	,	kIx,	,
mimoslovní	mimoslovní	k2eAgFnSc1d1	mimoslovní
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
stejnou	stejný	k2eAgFnSc4d1	stejná
polohu	poloha	k1gFnSc4	poloha
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgNnSc1d1	podobné
držení	držení	k1gNnSc1	držení
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgNnSc4d1	podobné
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
hlasitost	hlasitost	k1gFnSc4	hlasitost
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
podobnou	podobný	k2eAgFnSc4d1	podobná
dynamiku	dynamika	k1gFnSc4	dynamika
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
takovým	takový	k3xDgMnSc7	takový
člověkem	člověk	k1gMnSc7	člověk
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
sympatičtí	sympatický	k2eAgMnPc1d1	sympatický
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
máme	mít	k5eAaImIp1nP	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vůli	vůle	k1gFnSc4	vůle
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
druhým	druhý	k4xOgNnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
sebedůvěře	sebedůvěra	k1gFnSc6	sebedůvěra
a	a	k8xC	a
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Mimika	mimika	k1gFnSc1	mimika
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
emocích	emoce	k1gFnPc6	emoce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nejsnadněji	snadno	k6eAd3	snadno
čitelná	čitelný	k2eAgFnSc1d1	čitelná
a	a	k8xC	a
napodobitelná	napodobitelný	k2eAgFnSc1d1	napodobitelná
(	(	kIx(	(
<g/>
úsměv	úsměv	k1gInSc1	úsměv
<g/>
,	,	kIx,	,
zachmuření	zachmuření	k1gNnSc1	zachmuření
se	se	k3xPyFc4	se
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Výraz	výraz	k1gInSc1	výraz
hlasu	hlas	k1gInSc2	hlas
-	-	kIx~	-
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
melodie	melodie	k1gFnSc1	melodie
<g/>
,	,	kIx,	,
intenzita	intenzita	k1gFnSc1	intenzita
-	-	kIx~	-
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
o	o	k7c6	o
psychickém	psychický	k2eAgNnSc6d1	psychické
napětí	napětí	k1gNnSc6	napětí
či	či	k8xC	či
uvolnění	uvolnění	k1gNnSc6	uvolnění
(	(	kIx(	(
<g/>
monotónní	monotónní	k2eAgInSc1d1	monotónní
hlas	hlas	k1gInSc1	hlas
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
melodický	melodický	k2eAgMnSc1d1	melodický
uvolnění	uvolnění	k1gNnSc1	uvolnění
<g/>
;	;	kIx,	;
vysoký	vysoký	k2eAgInSc1d1	vysoký
hlas	hlas	k1gInSc1	hlas
napětí	napětí	k1gNnSc2	napětí
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
velmi	velmi	k6eAd1	velmi
hlasitý	hlasitý	k2eAgInSc1d1	hlasitý
slovní	slovní	k2eAgInSc1d1	slovní
projev	projev	k1gInSc1	projev
je	být	k5eAaImIp3nS	být
signálem	signál	k1gInSc7	signál
touhy	touha	k1gFnSc2	touha
ovládnout	ovládnout	k5eAaPmF	ovládnout
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Gesta	gesto	k1gNnPc1	gesto
jsou	být	k5eAaImIp3nP	být
průvodcem	průvodce	k1gMnSc7	průvodce
verbální	verbální	k2eAgFnSc2d1	verbální
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
gesta	gesto	k1gNnPc4	gesto
tzv.	tzv.	kA	tzv.
symbolická	symbolický	k2eAgFnSc5d1	symbolická
<g/>
,	,	kIx,	,
ilustrační	ilustrační	k2eAgFnSc6d1	ilustrační
<g/>
,	,	kIx,	,
regulační	regulační	k2eAgFnSc6d1	regulační
a	a	k8xC	a
adaptační	adaptační	k2eAgFnSc6d1	adaptační
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
používání	používání	k1gNnSc1	používání
gest	gesto	k1gNnPc2	gesto
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
zvýšené	zvýšený	k2eAgFnPc4d1	zvýšená
emoce	emoce	k1gFnPc4	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Meziosobní	Meziosobní	k2eAgFnSc7d1	Meziosobní
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
komunikujeme	komunikovat	k5eAaImIp1nP	komunikovat
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
podřízenosti	podřízenost	k1gFnSc2	podřízenost
a	a	k8xC	a
nadřízenosti	nadřízenost	k1gFnSc2	nadřízenost
a	a	k8xC	a
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
sympatií	sympatie	k1gFnPc2	sympatie
a	a	k8xC	a
antipatií	antipatie	k1gFnPc2	antipatie
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
typy	typ	k1gInPc1	typ
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
:	:	kIx,	:
intimní	intimní	k2eAgFnSc1d1	intimní
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
a	a	k8xC	a
veřejná	veřejný	k2eAgFnSc1d1	veřejná
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
pracovníkem	pracovník	k1gMnSc7	pracovník
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
můžeme	moct	k5eAaImIp1nP	moct
při	při	k7c6	při
rozhovoru	rozhovor	k1gInSc6	rozhovor
manipulovat	manipulovat	k5eAaImF	manipulovat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
když	když	k8xS	když
sedíme	sedit	k5eAaImIp1nP	sedit
-	-	kIx~	-
můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
naklonit	naklonit	k5eAaPmF	naklonit
nebo	nebo	k8xC	nebo
zaklonit	zaklonit	k5eAaPmF	zaklonit
<g/>
.	.	kIx.	.
</s>
<s>
Dotyky	dotyk	k1gInPc4	dotyk
(	(	kIx(	(
<g/>
podání	podání	k1gNnSc3	podání
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
poplácání	poplácání	k1gNnSc2	poplácání
<g/>
,	,	kIx,	,
pohlazení	pohlazení	k1gNnSc2	pohlazení
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
komunikujeme	komunikovat	k5eAaImIp1nP	komunikovat
emocionální	emocionální	k2eAgInSc4d1	emocionální
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
společenské	společenský	k2eAgNnSc4d1	společenské
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
ruku	ruka	k1gFnSc4	ruka
podáme	podat	k5eAaPmIp1nP	podat
nebo	nebo	k8xC	nebo
přijmeme	přijmout	k5eAaPmIp1nP	přijmout
<g/>
,	,	kIx,	,
dáváme	dávat	k5eAaImIp1nP	dávat
první	první	k4xOgInSc4	první
signál	signál	k1gInSc4	signál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblečením	oblečení	k1gNnSc7	oblečení
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
signalizujeme	signalizovat	k5eAaImIp1nP	signalizovat
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
společenské	společenský	k2eAgFnSc3d1	společenská
vrstvě	vrstva	k1gFnSc3	vrstva
<g/>
,	,	kIx,	,
názorovému	názorový	k2eAgInSc3d1	názorový
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
sebepojetí	sebepojetí	k1gNnSc3	sebepojetí
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
důležitým	důležitý	k2eAgInSc7d1	důležitý
komunikačním	komunikační	k2eAgInSc7d1	komunikační
kanálem	kanál	k1gInSc7	kanál
je	být	k5eAaImIp3nS	být
komunikace	komunikace	k1gFnSc1	komunikace
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
vědomě	vědomě	k6eAd1	vědomě
dešifrujeme	dešifrovat	k5eAaBmIp1nP	dešifrovat
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
si	se	k3xPyFc3	se
uvědomujeme	uvědomovat	k5eAaImIp1nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jasnou	jasný	k2eAgFnSc4d1	jasná
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Činy	čina	k1gFnPc4	čina
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
signalizujeme	signalizovat	k5eAaImIp1nP	signalizovat
náš	náš	k3xOp1gInSc4	náš
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
například	například	k6eAd1	například
pozdními	pozdní	k2eAgInPc7d1	pozdní
příchody	příchod	k1gInPc7	příchod
<g/>
,	,	kIx,	,
opomenutím	opomenutí	k1gNnSc7	opomenutí
pozdravu	pozdrav	k1gInSc2	pozdrav
<g/>
,	,	kIx,	,
nesplněním	nesplnění	k1gNnSc7	nesplnění
slibu	slib	k1gInSc2	slib
<g/>
,	,	kIx,	,
porušením	porušení	k1gNnSc7	porušení
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
přinesením	přinesení	k1gNnSc7	přinesení
květin	květina	k1gFnPc2	květina
apod.	apod.	kA	apod.
Křivohlavý	Křivohlavý	k2eAgInSc4d1	Křivohlavý
<g/>
,	,	kIx,	,
Jaro	jaro	k6eAd1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
lépe	dobře	k6eAd2	dobře
porozumíme	porozumět	k5eAaPmIp1nP	porozumět
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
240	[number]	k4	240
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
25-095-88	[number]	k4	25-095-88
Peasovi	Peasův	k2eAgMnPc1d1	Peasův
<g/>
,	,	kIx,	,
Allan	Allan	k1gMnSc1	Allan
a	a	k8xC	a
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
360	[number]	k4	360
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7367-921-7	[number]	k4	978-80-7367-921-7
Hartley	Hartlea	k1gFnSc2	Hartlea
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
104	[number]	k4	104
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-262-0033-8	[number]	k4	978-80-262-0033-8
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
neverbální	verbální	k2eNgFnSc2d1	neverbální
komunikace	komunikace	k1gFnSc2	komunikace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Příklady	příklad	k1gInPc1	příklad
neverbální	verbální	k2eNgFnSc2d1	neverbální
komunikace	komunikace	k1gFnSc2	komunikace
Příklady	příklad	k1gInPc1	příklad
neverbální	verbální	k2eNgFnSc2d1	neverbální
komunikace	komunikace	k1gFnSc2	komunikace
2	[number]	k4	2
</s>
