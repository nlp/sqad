<s>
Neverbální	verbální	k2eNgFnSc1d1
komunikace	komunikace	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC
nonverbální	nonverbální	k2eAgFnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
souhrn	souhrn	k1gInSc4
mimoslovních	mimoslovní	k2eAgNnPc2d1
sdělení	sdělení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1
jsou	být	k5eAaImIp3nP
vědomě	vědomě	k6eAd1
nebo	nebo	k8xC
nevědomě	vědomě	k6eNd1
předávána	předávat	k5eAaImNgFnS
člověkem	člověk	k1gMnSc7
k	k	k7c3
jiné	jiný	k2eAgFnSc3d1
osobě	osoba	k1gFnSc3
nebo	nebo	k8xC
lidem	člověk	k1gMnPc3
<g/>
.	.	kIx.
</s>