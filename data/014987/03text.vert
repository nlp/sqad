<s>
Eudimorphodon	Eudimorphodon	k1gMnSc1
</s>
<s>
EudimorphodonStratigrafický	EudimorphodonStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
svrchní	svrchní	k2eAgInSc1d1
trias	trias	k1gInSc1
před	před	k7c4
220	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
<g/>
lety	let	k1gInPc4
Eudimorphodon	Eudimorphodona	k1gFnPc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Nadřád	nadřád	k1gInSc1
</s>
<s>
archosauři	archosauř	k1gFnSc3
(	(	kIx(
<g/>
Archosauria	Archosaurium	k1gNnSc2
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
ptakoještěři	ptakoještěr	k1gMnPc1
(	(	kIx(
<g/>
Pterosauria	Pterosaurium	k1gNnPc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
Rhamphorhynchoidea	Rhamphorhynchoide	k2eAgFnSc1d1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Eudimorphodontidae	Eudimorphodontidae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
Eudimorphodon	Eudimorphodon	k1gInSc1
Druhy	druh	k1gInPc1
</s>
<s>
E.	E.	kA
ranzii	ranzie	k1gFnSc6
-	-	kIx~
Zambelli	Zambelle	k1gFnSc6
<g/>
,	,	kIx,
1973	#num#	k4
</s>
<s>
E.	E.	kA
cromptonellus	cromptonellus	k1gMnSc1
-	-	kIx~
Jenkins	Jenkins	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
E.	E.	kA
rosenfeldi	rosenfeldit	k5eAaPmRp2nS
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eudimorphodon	Eudimorphodon	k1gNnSc1
(	(	kIx(
<g/>
opravdu	opravdu	k6eAd1
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc1
zubů	zub	k1gInPc2
=	=	kIx~
eu	eu	k?
–	–	k?
dobrý	dobrý	k2eAgInSc1d1
<g/>
,	,	kIx,
di	di	k?
–	–	k?
dvojitý	dvojitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
don	don	k1gMnSc1
–	–	k?
zub	zub	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
primitivním	primitivní	k2eAgFnPc3d1
ptakoještěr	ptakoještěr	k1gMnSc1
<g/>
,	,	kIx,
létající	létající	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žil	žíla	k1gFnPc2
před	před	k7c7
220	#num#	k4
miliony	milion	k4xCgInPc7
lety	léto	k1gNnPc7
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
kostra	kostra	k1gFnSc1
byla	být	k5eAaImAgFnS
nalezena	nalézt	k5eAaBmNgFnS,k5eAaPmNgFnS
v	v	k7c6
břidlicích	břidlice	k1gFnPc6
z	z	k7c2
severní	severní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
nález	nález	k1gInSc1
je	být	k5eAaImIp3nS
datován	datovat	k5eAaImNgInS
do	do	k7c2
pozdního	pozdní	k2eAgInSc2d1
triasu	trias	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
tohoto	tento	k3xDgMnSc2
dinosaura	dinosaurus	k1gMnSc2
dělá	dělat	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejstarších	starý	k2eAgMnPc2d3
ptakoještěrů	ptakoještěr	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Eudimorphodon	Eudimorphodon	k1gInSc1
vypadal	vypadat	k5eAaPmAgInS,k5eAaImAgInS
jako	jako	k9
typický	typický	k2eAgMnSc1d1
pterosaurus	pterosaurus	k1gMnSc1
–	–	k?
měl	mít	k5eAaImAgInS
dlouhá	dlouhý	k2eAgNnPc4d1
křídla	křídlo	k1gNnPc4
s	s	k7c7
rozpětím	rozpětí	k1gNnSc7
kolem	kolem	k7c2
1	#num#	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
křídlech	křídlo	k1gNnPc6
měl	mít	k5eAaImAgMnS
4	#num#	k4
prsty	prst	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtý	čtvrtý	k4xOgInSc1
prst	prst	k1gInSc1
byl	být	k5eAaImAgInS
prodloužen	prodloužit	k5eAaPmNgInS
a	a	k8xC
bylo	být	k5eAaImAgNnS
na	na	k7c4
něj	on	k3xPp3gInSc4
navázána	navázán	k2eAgFnSc1d1
membrána	membrána	k1gFnSc1
tvořící	tvořící	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
připojeno	připojit	k5eAaPmNgNnS
také	také	k9
ke	k	k7c3
stehenní	stehenní	k2eAgFnSc3d1
kosti	kost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Též	též	k6eAd1
měl	mít	k5eAaImAgInS
poměrně	poměrně	k6eAd1
slabé	slabý	k2eAgFnPc4d1
zadní	zadní	k2eAgFnPc4d1
končetiny	končetina	k1gFnPc4
a	a	k8xC
dlouhý	dlouhý	k2eAgInSc4d1
ocas	ocas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
zakončen	zakončit	k5eAaPmNgInS
zvláštním	zvláštní	k2eAgInSc7d1
výstupkem	výstupek	k1gInSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
kosočtverce	kosočtverec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozšíření	rozšíření	k1gNnSc1
by	by	kYmCp3nS
posloužilo	posloužit	k5eAaPmAgNnS
jako	jako	k9
kormidlo	kormidlo	k1gNnSc1
při	při	k7c6
letu	let	k1gInSc6
a	a	k8xC
také	také	k9
by	by	kYmCp3nS
vyvážilo	vyvážit	k5eAaPmAgNnS
hmotnost	hmotnost	k1gFnSc4
hlavy	hlava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k8xS,k8xC
název	název	k1gInSc1
napovídá	napovídat	k5eAaBmIp3nS
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
dva	dva	k4xCgMnPc4
druhy	druh	k1gMnPc4
zubů	zub	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
vidět	vidět	k5eAaImF
i	i	k9
po	po	k7c6
zavření	zavření	k1gNnSc6
tlamy	tlama	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vpředu	vpředu	k6eAd1
měl	mít	k5eAaImAgInS
dlouhé	dlouhý	k2eAgFnPc4d1
špičaté	špičatý	k2eAgFnPc4d1
<g/>
,	,	kIx,
vzadu	vzadu	k6eAd1
naopak	naopak	k6eAd1
širší	široký	k2eAgFnSc1d2
a	a	k8xC
kratší	krátký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jiných	jiný	k2eAgMnPc2d1
pterosaurů	pterosaurus	k1gMnPc2
se	se	k3xPyFc4
zuby	zub	k1gInPc7
vyskytovaly	vyskytovat	k5eAaImAgFnP
jen	jen	k9
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
zakrnělé	zakrnělý	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
úplně	úplně	k6eAd1
zmizely	zmizet	k5eAaPmAgFnP
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
malé	malý	k2eAgFnSc6d1
čelisti	čelist	k1gFnSc6
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
celkem	celkem	k6eAd1
asi	asi	k9
110	#num#	k4
zoubků	zoubek	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lebka	lebka	k1gFnSc1
byla	být	k5eAaImAgFnS
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
lov	lov	k1gInSc4
hmyzu	hmyz	k1gInSc2
a	a	k8xC
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
přiblížily	přiblížit	k5eAaPmAgFnP
k	k	k7c3
mořské	mořský	k2eAgFnSc3d1
hladině	hladina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hlavě	hlava	k1gFnSc6
se	se	k3xPyFc4
také	také	k6eAd1
vyskytovaly	vyskytovat	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
spánkové	spánkový	k2eAgFnPc4d1
jamky	jamka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
fyziologie	fyziologie	k1gFnSc1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgMnSc1
tvor	tvor	k1gMnSc1
se	se	k3xPyFc4
živil	živit	k5eAaImAgMnS
převážně	převážně	k6eAd1
rybami	ryba	k1gFnPc7
a	a	k8xC
hmyzem	hmyz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
tvrzení	tvrzení	k1gNnSc1
také	také	k9
dokazuje	dokazovat	k5eAaImIp3nS
zachovalý	zachovalý	k2eAgInSc1d1
nález	nález	k1gInSc1
žaludku	žaludek	k1gInSc2
obsahující	obsahující	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
ryb	ryba	k1gFnPc2
z	z	k7c2
rodu	rod	k1gInSc2
Parapholidophorů	Parapholidophor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
Rod	rod	k1gInSc1
Eudimorphodonů	Eudimorphodon	k1gMnPc2
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
popsán	popsat	k5eAaPmNgInS
Zambellim	Zambellim	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
vědec	vědec	k1gMnSc1
objevil	objevit	k5eAaPmAgMnS
exemplář	exemplář	k1gInSc4
Eudimorphodoa	Eudimorphodous	k1gMnSc2
ranzii	ranzie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc4
později	pozdě	k6eAd2
popsal	popsat	k5eAaPmAgMnS
profesor	profesor	k1gMnSc1
Silvio	Silvio	k1gMnSc1
Ranzi	Ranze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
druh	druh	k1gInSc4
Eudimorphodona	Eudimorphodon	k1gMnSc2
pojmenoval	pojmenovat	k5eAaPmAgMnS
Dalla	Dall	k1gMnSc4
Vecchia	Vecchius	k1gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
nalezl	nalézt	k5eAaBmAgInS,k5eAaPmAgInS
dva	dva	k4xCgInPc1
zástupce	zástupce	k1gMnSc2
Eudimorphodona	Eudimorphodona	k1gFnSc1
cromptonellus	cromptonellus	k1gInSc1
na	na	k7c6
severu	sever	k1gInSc6
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
další	další	k2eAgFnPc4d1
práce	práce	k1gFnPc4
Dalla	Dalla	k1gFnSc1
Vecchia	Vecchia	k1gFnSc1
říkala	říkat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc1
jiného	jiný	k2eAgInSc2d1
rodu	rod	k1gInSc2
pouze	pouze	k6eAd1
s	s	k7c7
jedním	jeden	k4xCgMnSc7
zástupcem	zástupce	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
Vecchia	Vecchius	k1gMnSc4
pojmenoval	pojmenovat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
Carniadastylus	Carniadastylus	k1gInSc4
rosenfeldi	rosenfeld	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgMnSc1d1
druh	druh	k1gMnSc1
Eudimorphodon	Eudimorphodon	k1gMnSc1
rosenfeldi	rosenfeld	k1gMnPc1
byl	být	k5eAaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
v	v	k7c6
Grónsku	Grónsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
jej	on	k3xPp3gMnSc4
nalezl	nalézt	k5eAaBmAgInS,k5eAaPmAgInS
Jeckins	Jeckins	k1gInSc1
s	s	k7c7
týmem	tým	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usoudili	usoudit	k5eAaPmAgMnP
že	že	k8xS
tento	tento	k3xDgMnSc1
dinousaurus	dinousaurus	k1gMnSc1
je	být	k5eAaImIp3nS
exemplářem	exemplář	k1gInSc7
druhu	druh	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
vzorek	vzorek	k1gInSc1
nalezl	naleznout	k5eAaPmAgInS,k5eAaBmAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
profesor	profesor	k1gMnSc1
Alfred	Alfred	k1gMnSc1
Walter	Walter	k1gMnSc1
Crompton	Crompton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
Eudimorphodon	Eudimorphodona	k1gFnPc2
rosenfeldi	rosenfeld	k1gMnPc1
je	být	k5eAaImIp3nS
zdrobnělina	zdrobnělina	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
nálezy	nález	k1gInPc1
měly	mít	k5eAaImAgInP
rozpětí	rozpětí	k1gNnSc4
křídel	křídlo	k1gNnPc2
asi	asi	k9
pouhých	pouhý	k2eAgInPc2d1
25	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
byly	být	k5eAaImAgFnP
nalezeny	naleznout	k5eAaPmNgFnP,k5eAaBmNgFnP
v	v	k7c6
západním	západní	k2eAgInSc6d1
Texasu	Texas	k1gInSc6
vzorky	vzorek	k1gInPc4
zubů	zub	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
obtížné	obtížný	k2eAgNnSc1d1
určit	určit	k5eAaPmF
zda	zda	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
bez	bez	k7c2
většího	veliký	k2eAgInSc2d2
nálezu	nález	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgInPc1d1
vzorky	vzorek	k1gInPc1
zubů	zub	k1gInPc2
jsou	být	k5eAaImIp3nP
Eudimorphodonovi	Eudimorphodon	k1gMnSc3
velmi	velmi	k6eAd1
podobné	podobný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
WELLNHOFER	WELLNHOFER	kA
<g/>
,	,	kIx,
PETER	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
illustrated	illustrated	k1gMnSc1
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
pterosaurs	pterosaursa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Salamander	Salamander	k1gMnSc1
Books	Booksa	k1gFnPc2
Ltd	ltd	kA
<g/>
.	.	kIx.
192	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
861015665	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780861015665	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
25761108	#num#	k4
1	#num#	k4
2	#num#	k4
3	#num#	k4
B.	B.	kA
CEMPÍREK	CEMPÍREK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
-	-	kIx~
objevy	objev	k1gInPc1
<g/>
,	,	kIx,
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
zánik	zánik	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
:	:	kIx,
contmedia	contmedium	k1gNnPc1
GmbH	GmbH	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
95	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
625	#num#	k4
<g/>
-	-	kIx~
<g/>
12054	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
43	#num#	k4
<g/>
,	,	kIx,
44	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Prehistoric	Prehistoric	k1gMnSc1
life	lifat	k5eAaPmIp3nS
:	:	kIx,
[	[	kIx(
<g/>
the	the	k?
definitive	definitiv	k1gInSc5
visual	visuat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
history	histor	k1gMnPc4
of	of	k?
life	lifat	k5eAaPmIp3nS
on	on	k3xPp3gMnSc1
earth	earth	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
st	st	kA
American	Americana	k1gFnPc2
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
<g/>
Y.	Y.	kA
<g/>
:	:	kIx,
DK	DK	kA
Pub	Pub	k1gFnSc1
512	#num#	k4
pages	pages	k1gMnSc1
s.	s.	k?
ISBN	ISBN	kA
9780756655730	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
756655730	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
444710202	#num#	k4
1	#num#	k4
2	#num#	k4
CALUAN	CALUAN	kA
<g/>
,	,	kIx,
Karen	Karen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Splash	Splash	k1gMnSc1
of	of	k?
colour	colour	k1gMnSc1
<g/>
:	:	kIx,
It	It	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Fun	Fun	k1gFnSc7
to	ten	k3xDgNnSc4
Draw	Draw	k1gFnPc1
Dinosaurs	Dinosaursa	k1gFnPc2
and	and	k?
Other	Other	k1gMnSc1
Prehistoric	Prehistoric	k1gMnSc1
Creatures	Creatures	k1gMnSc1
£	£	k?
<g/>
5.99	5.99	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Child	Child	k1gMnSc1
Care	car	k1gMnSc5
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
32	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1753	#num#	k4
<g/>
-	-	kIx~
<g/>
9900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.129	10.129	k4
<g/>
68	#num#	k4
<g/>
/	/	kIx~
<g/>
chca	chca	k6eAd1
<g/>
.2	.2	k4
<g/>
011.8.7.32	011.8.7.32	k4
<g/>
c.	c.	k?
↑	↑	k?
ŐSI	ŐSI	kA
<g/>
,	,	kIx,
Attila	Attila	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feeding-related	Feeding-related	k1gInSc1
characters	characters	k1gInSc4
in	in	k?
basal	basal	k1gInSc1
pterosaurs	pterosaurs	k1gInSc1
<g/>
:	:	kIx,
implications	implications	k1gInSc1
for	forum	k1gNnPc2
jaw	jawa	k1gFnPc2
mechanism	mechanisma	k1gFnPc2
<g/>
,	,	kIx,
dental	dentat	k5eAaPmAgInS,k5eAaImAgInS
function	function	k1gInSc1
and	and	k?
diet	dieta	k1gFnPc2
<g/>
:	:	kIx,
Feeding-related	Feeding-related	k1gInSc1
characters	characters	k1gInSc1
in	in	k?
pterosaurs	pterosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lethaia	Lethaium	k1gNnPc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
44	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
136	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1502	.1502	k4
<g/>
-	-	kIx~
<g/>
3931.2010	3931.2010	k4
<g/>
.00230	.00230	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ADMIN	ADMIN	kA
<g/>
,	,	kIx,
ScienceOpen	ScienceOpen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rivista	Rivist	k1gMnSc2
Italiana	Italian	k1gMnSc2
di	di	k?
Paleontologia	Paleontologius	k1gMnSc2
e	e	k0
Stratigrafia	Stratigrafium	k1gNnPc4
<g/>
.	.	kIx.
dx	dx	k?
<g/>
.	.	kIx.
<g/>
doi	doi	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.142	10.142	k4
<g/>
93	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
2199	#num#	k4
<g/>
-	-	kIx~
<g/>
1006.1	1006.1	k4
<g/>
.	.	kIx.
<g/>
sor-geo	sor-geo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
cley	cley	k1gInPc7
<g/>
0	#num#	k4
<g/>
wb	wb	k?
<g/>
.	.	kIx.
<g/>
v	v	k7c6
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KELLNER	Kellner	k1gMnSc1
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
W.A.	W.A.	k1gFnSc2
Comments	Comments	k1gInSc4
on	on	k3xPp3gMnSc1
Triassic	Triassic	k1gMnSc1
pterosaurs	pterosaursa	k1gFnPc2
with	with	k1gInSc4
discussion	discussion	k1gInSc1
about	about	k1gInSc1
ontogeny	ontogen	k1gInPc1
and	and	k?
description	description	k1gInSc1
of	of	k?
new	new	k?
taxa	taxa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anais	Anais	k1gFnSc1
da	da	k?
Academia	academia	k1gFnSc1
Brasileira	Brasileira	k1gFnSc1
de	de	k?
Ciê	Ciê	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
87	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
669	#num#	k4
<g/>
–	–	k?
<g/>
689	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3765	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.159	10.159	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3765201520150307	#num#	k4
<g/>
.	.	kIx.
</s>
