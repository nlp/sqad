<s>
Jack	Jack	k1gMnSc1	Jack
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Thomas	Thomas	k1gMnSc1	Thomas
Jack	Jack	k1gMnSc1	Jack
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znám	k2eAgMnSc1d1	znám
je	být	k5eAaImIp3nS	být
také	také	k9	také
pod	pod	k7c7	pod
přezdívkami	přezdívka	k1gFnPc7	přezdívka
JB	JB	kA	JB
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Jables	Jables	k1gMnSc1	Jables
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Hermosa	Hermosa	k1gFnSc1	Hermosa
Beach	Beach	k1gInSc1	Beach
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Kyle	Kyle	k1gNnSc7	Kyle
Gassem	Gasso	k1gNnSc7	Gasso
tvoří	tvořit	k5eAaImIp3nS	tvořit
komediální	komediální	k2eAgNnSc1d1	komediální
a	a	k8xC	a
rockové	rockový	k2eAgNnSc1d1	rockové
duo	duo	k1gNnSc1	duo
Tenacious	Tenacious	k1gInSc1	Tenacious
D.	D.	kA	D.
<g/>
Oba	dva	k4xCgMnPc1	dva
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
pořadu	pořad	k1gInSc6	pořad
Tenacious	Tenacious	k1gMnSc1	Tenacious
D	D	kA	D
na	na	k7c6	na
HBO	HBO	kA	HBO
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
zde	zde	k6eAd1	zde
také	také	k9	také
zazněly	zaznět	k5eAaImAgFnP	zaznět
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
prvním	první	k4xOgInSc6	první
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Black	Black	k1gMnSc1	Black
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
např.	např.	kA	např.
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Vodní	vodní	k2eAgInSc1d1	vodní
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
Rok	rok	k1gInSc1	rok
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
Závist	závist	k1gFnSc1	závist
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
Prázdniny	prázdniny	k1gFnPc4	prázdniny
nebo	nebo	k8xC	nebo
Kung	Kung	k1gInSc4	Kung
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
s	s	k7c7	s
Kylem	Kyl	k1gMnSc7	Kyl
Gassem	Gass	k1gMnSc7	Gass
natočili	natočit	k5eAaBmAgMnP	natočit
komediální	komediální	k2eAgInSc4d1	komediální
film	film	k1gInSc4	film
Králové	Králová	k1gFnSc2	Králová
ro	ro	k?	ro
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
ku	k	k7c3	k
<g/>
.	.	kIx.	.
</s>
<s>
Tenacious	Tenacious	k1gMnSc1	Tenacious
D	D	kA	D
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tenacious	Tenacious	k1gInSc1	Tenacious
defense	defense	k1gFnSc2	defense
<g/>
"	"	kIx"	"
=	=	kIx~	=
<g/>
Neústupná	ústupný	k2eNgFnSc1d1	neústupná
obrana	obrana	k1gFnSc1	obrana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
Jack	Jack	k1gMnSc1	Jack
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
Kylem	Kyl	k1gMnSc7	Kyl
Gassem	Gass	k1gMnSc7	Gass
<g/>
.	.	kIx.	.
</s>
<s>
Kyle	Kyle	k1gInSc1	Kyle
začínál	začínál	k1gInSc1	začínál
v	v	k7c6	v
reklamách	reklama	k1gFnPc6	reklama
na	na	k7c4	na
7-UP	[number]	k4	7-UP
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
přijali	přijmout	k5eAaPmAgMnP	přijmout
šestnáctiletého	šestnáctiletý	k2eAgMnSc4d1	šestnáctiletý
Jacka	Jacek	k1gMnSc4	Jacek
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
nesnášeli	snášet	k5eNaImAgMnP	snášet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
srovnali	srovnat	k5eAaPmAgMnP	srovnat
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
Gass	Gass	k1gInSc1	Gass
učil	učit	k5eAaImAgInS	učit
Jacka	Jacek	k1gInSc2	Jacek
kytaře	kytara	k1gFnSc3	kytara
a	a	k8xC	a
tak	tak	k6eAd1	tak
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Tenacious	Tenacious	k1gInSc1	Tenacious
D.	D.	kA	D.
Mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
satirické	satirický	k2eAgInPc1d1	satirický
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
dvěma	dva	k4xCgMnPc3	dva
satelitním	satelitní	k2eAgMnPc3d1	satelitní
inženýrům	inženýr	k1gMnPc3	inženýr
Judith	Judith	k1gInSc4	Judith
Black	Blacka	k1gFnPc2	Blacka
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Cohen	Cohen	k2eAgInSc4d1	Cohen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c4	na
Hubble	Hubble	k1gFnSc4	Hubble
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
(	(	kIx(	(
<g/>
Hubblově	Hubblův	k2eAgInSc6d1	Hubblův
kosmickém	kosmický	k2eAgInSc6d1	kosmický
dalekohledu	dalekohled	k1gInSc6	dalekohled
<g/>
)	)	kIx)	)
a	a	k8xC	a
Thomasu	Thomasu	k?	Thomasu
Williamu	William	k1gInSc2	William
Blackovi	Blacek	k1gMnSc3	Blacek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
anglický	anglický	k2eAgMnSc1d1	anglický
<g/>
,	,	kIx,	,
irský	irský	k2eAgMnSc1d1	irský
<g/>
,	,	kIx,	,
skotsko-irský	skotskorský	k2eAgInSc1d1	skotsko-irský
a	a	k8xC	a
německý	německý	k2eAgInSc1d1	německý
původ	původ	k1gInSc1	původ
<g/>
,	,	kIx,	,
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
k	k	k7c3	k
judaismu	judaismus	k1gInSc3	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
hebrejskou	hebrejský	k2eAgFnSc4d1	hebrejská
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
Bar	bar	k1gInSc1	bar
micva	micvo	k1gNnSc2	micvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Tonight	Tonight	k1gInSc4	Tonight
show	show	k1gFnPc2	show
with	with	k1gMnSc1	with
Jay	Jay	k1gMnSc1	Jay
leno	leno	k1gMnSc1	leno
řekl	říct	k5eAaPmAgMnS	říct
že	že	k9	že
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
pracovního	pracovní	k2eAgNnSc2d1	pracovní
jména	jméno	k1gNnSc2	jméno
kováře	kovář	k1gMnSc2	kovář
<g/>
.	.	kIx.	.
</s>
<s>
Blackovi	Blackův	k2eAgMnPc1d1	Blackův
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
Culver	Culvra	k1gFnPc2	Culvra
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
matčin	matčin	k2eAgInSc4d1	matčin
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
třináct	třináct	k4xCc1	třináct
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
Activision	Activision	k1gInSc4	Activision
game	game	k1gInSc1	game
Pitfall	Pitfall	k1gInSc1	Pitfall
<g/>
,	,	kIx,	,
reklama	reklama	k1gFnSc1	reklama
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázala	ukázat	k5eAaPmAgFnS	ukázat
na	na	k7c4	na
live	live	k1gNnSc4	live
vystoupení	vystoupení	k1gNnSc2	vystoupení
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Kimmela	Kimmel	k1gMnSc2	Kimmel
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
The	The	k1gFnSc6	The
Tonight	Tonighta	k1gFnPc2	Tonighta
Show	show	k1gFnSc2	show
s	s	k7c7	s
Conanem	Conan	k1gMnSc7	Conan
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Brienem	Brien	k1gMnSc7	Brien
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Jack	Jack	k1gMnSc1	Jack
vstupoval	vstupovat	k5eAaImAgMnS	vstupovat
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
rodiče	rodič	k1gMnPc1	rodič
ho	on	k3xPp3gMnSc4	on
zapsali	zapsat	k5eAaPmAgMnP	zapsat
na	na	k7c6	na
Poseidon	Poseidon	k1gMnSc1	Poseidon
school	school	k1gInSc4	school
<g/>
,	,	kIx,	,
soukromou	soukromý	k2eAgFnSc4d1	soukromá
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
navrženou	navržený	k2eAgFnSc4d1	navržená
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
potýkající	potýkající	k2eAgMnPc4d1	potýkající
se	se	k3xPyFc4	se
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
školském	školský	k2eAgInSc6d1	školský
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
střední	střední	k2eAgInSc4d1	střední
Crossroads	Crossroads	k1gInSc4	Crossroads
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vynikal	vynikat	k5eAaImAgMnS	vynikat
v	v	k7c6	v
hraní	hraní	k1gNnSc6	hraní
<g/>
,	,	kIx,	,
v	v	k7c4	v
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
UCLA	UCLA	kA	UCLA
(	(	kIx(	(
<g/>
University	universita	k1gFnSc2	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
[	[	kIx(	[
<g/>
Kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
ale	ale	k9	ale
zanechal	zanechat	k5eAaPmAgMnS	zanechat
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
<g/>
,	,	kIx,	,
<g/>
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
zábavnímu	zábavní	k2eAgInSc3d1	zábavní
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
přestal	přestat	k5eAaPmAgMnS	přestat
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
UCLA	UCLA	kA	UCLA
Tim	Tim	k?	Tim
Robbins	Robbins	k1gInSc1	Robbins
ho	on	k3xPp3gInSc4	on
později	pozdě	k6eAd2	pozdě
obsadil	obsadit	k5eAaPmAgMnS	obsadit
do	do	k7c2	do
Bob	Bob	k1gMnSc1	Bob
Roberts	Roberts	k1gInSc1	Roberts
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
měl	mít	k5eAaImAgMnS	mít
opakované	opakovaný	k2eAgFnPc4d1	opakovaná
role	role	k1gFnPc4	role
v	v	k7c6	v
komediálním	komediální	k2eAgInSc6d1	komediální
seriálu	seriál	k1gInSc6	seriál
na	na	k7c4	na
HBO	HBO	kA	HBO
Mr	Mr	k1gFnPc4	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Tanyu	Tanya	k1gFnSc4	Tanya
Haden	Hadna	k1gFnPc2	Hadna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
trojice	trojice	k1gFnSc2	trojice
dcer	dcera	k1gFnPc2	dcera
jazzu	jazz	k1gInSc2	jazz
The	The	k1gMnSc1	The
Haden	Hadna	k1gFnPc2	Hadna
Triplets	Triplets	k1gInSc4	Triplets
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
sestrám	sestra	k1gFnPc3	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
violoncellistka	violoncellistka	k1gFnSc1	violoncellistka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
společně	společně	k6eAd1	společně
vystudovali	vystudovat	k5eAaPmAgMnP	vystudovat
Crossroads	Crossroads	k1gInSc4	Crossroads
school	schoola	k1gFnPc2	schoola
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
absolvování	absolvování	k1gNnSc2	absolvování
<g/>
,	,	kIx,	,
na	na	k7c6	na
kamarádově	kamarádův	k2eAgFnSc6d1	kamarádova
oslavě	oslava	k1gFnSc6	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
svatbu	svatba	k1gFnSc4	svatba
kolem	kolem	k7c2	kolem
Vánoc	Vánoce	k1gFnPc2	Vánoce
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
v	v	k7c6	v
Big	Big	k1gFnSc6	Big
Sur	Sur	k1gFnSc2	Sur
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Jason	Jason	k1gMnSc1	Jason
"	"	kIx"	"
<g/>
Sammy	Samm	k1gInPc1	Samm
<g/>
"	"	kIx"	"
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
v	v	k7c4	v
Cedars-Sinai	Cedars-Sinai	k1gNnSc4	Cedars-Sinai
Medical	Medical	k1gFnSc2	Medical
Center	centrum	k1gNnPc2	centrum
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Thomas	Thomas	k1gMnSc1	Thomas
David	David	k1gMnSc1	David
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Black	Black	k1gMnSc1	Black
je	být	k5eAaImIp3nS	být
ateista	ateista	k1gMnSc1	ateista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teď	teď	k6eAd1	teď
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Black	Black	k1gMnSc1	Black
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
