<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Curie-Skłodowska	Curie-Skłodowska	k1gFnSc1	Curie-Skłodowska
stala	stát	k5eAaPmAgFnS	stát
šéfem	šéf	k1gMnSc7	šéf
vojenské	vojenský	k2eAgFnSc2d1	vojenská
lékařské	lékařský	k2eAgFnSc2d1	lékařská
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
organizací	organizace	k1gFnSc7	organizace
polních	polní	k2eAgFnPc2d1	polní
rentgenografických	rentgenografický	k2eAgFnPc2d1	rentgenografický
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyšetřily	vyšetřit	k5eAaPmAgFnP	vyšetřit
celkem	celkem	k6eAd1	celkem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
milióny	milión	k4xCgInPc1	milión
případů	případ	k1gInPc2	případ
zranění	zranění	k1gNnPc4	zranění
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
