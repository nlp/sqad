<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Izraele	Izrael	k1gInSc2	Izrael
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgInPc7	dva
modrými	modrý	k2eAgInPc7d1	modrý
pruhy	pruh	k1gInPc7	pruh
podél	podél	k7c2	podél
horního	horní	k2eAgInSc2d1	horní
i	i	k8xC	i
dolního	dolní	k2eAgInSc2d1	dolní
okraje	okraj	k1gInSc2	okraj
listu	list	k1gInSc2	list
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
podkladu	podklad	k1gInSc2	podklad
<g/>
,	,	kIx,	,
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
šesticípou	šesticípý	k2eAgFnSc7d1	šesticípá
Davidovou	Davidův	k2eAgFnSc7d1	Davidova
hvězdou	hvězda	k1gFnSc7	hvězda
(	(	kIx(	(
<g/>
Davidovým	Davidův	k2eAgInSc7d1	Davidův
štítem	štít	k1gInSc7	štít
<g/>
)	)	kIx)	)
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
přetínajícími	přetínající	k2eAgFnPc7d1	přetínající
se	se	k3xPyFc4	se
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
