<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Izraele	Izrael	k1gInSc2	Izrael
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgInPc7	dva
modrými	modrý	k2eAgInPc7d1	modrý
pruhy	pruh	k1gInPc7	pruh
podél	podél	k7c2	podél
horního	horní	k2eAgInSc2d1	horní
i	i	k8xC	i
dolního	dolní	k2eAgInSc2d1	dolní
okraje	okraj	k1gInSc2	okraj
listu	list	k1gInSc2	list
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
podkladu	podklad	k1gInSc2	podklad
<g/>
,	,	kIx,	,
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
šesticípou	šesticípý	k2eAgFnSc7d1	šesticípá
Davidovou	Davidův	k2eAgFnSc7d1	Davidova
hvězdou	hvězda	k1gFnSc7	hvězda
(	(	kIx(	(
<g/>
Davidovým	Davidův	k2eAgInSc7d1	Davidův
štítem	štít	k1gInSc7	štít
<g/>
)	)	kIx)	)
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
přetínajícími	přetínající	k2eAgFnPc7d1	přetínající
se	se	k3xPyFc4	se
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
jsou	být	k5eAaImIp3nP	být
barvy	barva	k1gFnPc1	barva
židovských	židovský	k2eAgFnPc2d1	židovská
bohoslužebných	bohoslužebný	k2eAgFnPc2d1	bohoslužebná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
znamenají	znamenat	k5eAaImIp3nP	znamenat
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
interpretaci	interpretace	k1gFnSc6	interpretace
čistotu	čistota	k1gFnSc4	čistota
sionistických	sionistický	k2eAgInPc2d1	sionistický
ideálů	ideál	k1gInPc2	ideál
a	a	k8xC	a
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
předlohou	předloha	k1gFnSc7	předloha
jí	on	k3xPp3gFnSc7	on
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
sionistického	sionistický	k2eAgNnSc2d1	sionistické
hnutí	hnutí	k1gNnSc2	hnutí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
schválená	schválený	k2eAgFnSc1d1	schválená
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
izraelské	izraelský	k2eAgFnPc4d1	izraelská
vlajky	vlajka	k1gFnPc4	vlajka
s	s	k7c7	s
nápisy	nápis	k1gInPc7	nápis
mají	mít	k5eAaImIp3nP	mít
přední	přední	k2eAgMnPc1d1	přední
<g/>
,	,	kIx,	,
lícové	lícový	k2eAgFnPc1d1	lícová
strany	strana	k1gFnPc1	strana
umístěné	umístěný	k2eAgFnPc1d1	umístěná
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
žerdi	žerď	k1gFnSc2	žerď
–	–	k?	–
vexilologický	vexilologický	k2eAgInSc4d1	vexilologický
symbol	symbol	k1gInSc4	symbol
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
jako	jako	k8xS	jako
izraelských	izraelský	k2eAgFnPc2d1	izraelská
národních	národní	k2eAgFnPc2d1	národní
barev	barva	k1gFnPc2	barva
podle	podle	k7c2	podle
všeho	všecek	k3xTgInSc2	všecek
přišel	přijít	k5eAaPmAgMnS	přijít
rakousko-židovský	rakousko-židovský	k2eAgMnSc1d1	rakousko-židovský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Ludwig	Ludwig	k1gMnSc1	Ludwig
August	August	k1gMnSc1	August
von	von	k1gInSc4	von
Frankl	Frankl	k1gFnSc2	Frankl
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Civ	civět	k5eAaImRp2nS	civět
<g/>
'	'	kIx"	'
<g/>
ej	ej	k0	ej
'	'	kIx"	'
<g/>
Erec	Erec	k1gInSc1	Erec
Jehuda	Jehudo	k1gNnSc2	Jehudo
(	(	kIx(	(
<g/>
Barvy	barva	k1gFnSc2	barva
judské	judský	k2eAgFnSc2d1	Judská
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
národní	národní	k2eAgFnPc1d1	národní
barvy	barva	k1gFnPc1	barva
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
až	až	k9	až
během	během	k7c2	během
první	první	k4xOgFnSc2	první
alije	alij	k1gFnSc2	alij
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Mordechaj	Mordechaj	k1gMnSc1	Mordechaj
ben	ben	k?	ben
Hilel	Hillo	k1gNnPc2	Hillo
ha-Kohen	ha-Kohen	k1gInSc1	ha-Kohen
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
oslavu	oslava	k1gFnSc4	oslava
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
stých	stý	k4xOgFnPc2	stý
narozenin	narozeniny	k1gFnPc2	narozeniny
filantropa	filantrop	k1gMnSc2	filantrop
Mošeho	Moše	k1gMnSc2	Moše
Montefioreho	Montefiore	k1gMnSc2	Montefiore
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
"	"	kIx"	"
<g/>
jsme	být	k5eAaImIp1nP	být
poprvé	poprvé	k6eAd1	poprvé
odhalili	odhalit	k5eAaPmAgMnP	odhalit
před	před	k7c7	před
širokou	široký	k2eAgFnSc7d1	široká
veřejností	veřejnost	k1gFnSc7	veřejnost
naše	náš	k3xOp1gFnPc4	náš
národní	národní	k2eAgFnPc4d1	národní
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnozí	mnohý	k2eAgMnPc1d1	mnohý
ani	ani	k8xC	ani
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
jsou	být	k5eAaImIp3nP	být
barvami	barva	k1gFnPc7	barva
našeho	náš	k3xOp1gInSc2	náš
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
nebyl	být	k5eNaImAgInS	být
náhodný	náhodný	k2eAgInSc1d1	náhodný
a	a	k8xC	a
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
na	na	k7c4	na
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
tradici	tradice	k1gFnSc4	tradice
<g/>
:	:	kIx,	:
modrá	modré	k1gNnPc1	modré
a	a	k8xC	a
bílá	bílé	k1gNnPc1	bílé
jsou	být	k5eAaImIp3nP	být
barvy	barva	k1gFnPc1	barva
židovského	židovský	k2eAgInSc2d1	židovský
modlitebního	modlitební	k2eAgInSc2d1	modlitební
pláště	plášť	k1gInSc2	plášť
talitu	talit	k1gInSc2	talit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Izraele	Izrael	k1gInSc2	Izrael
</s>
</p>
<p>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Izraele	Izrael	k1gInSc2	Izrael
</s>
</p>
<p>
<s>
Jeruzalémská	jeruzalémský	k2eAgFnSc1d1	Jeruzalémská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
izraelská	izraelský	k2eAgFnSc1d1	izraelská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Eretz	Eretz	k1gInSc1	Eretz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
Filipín	Filipíny	k1gFnPc2	Filipíny
zapsána	zapsán	k2eAgFnSc1d1	zapsána
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
jako	jako	k8xC	jako
největší	veliký	k2eAgFnSc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
+	+	kIx~	+
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
