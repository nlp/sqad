<s>
Lepra	lepra	k1gFnSc1	lepra
(	(	kIx(	(
<g/>
též	též	k9	též
malomocenství	malomocenství	k1gNnSc1	malomocenství
či	či	k8xC	či
Hansenova	Hansenův	k2eAgFnSc1d1	Hansenova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
způsobované	způsobovaný	k2eAgNnSc1d1	způsobované
bakterií	bakterie	k1gFnSc7	bakterie
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
leprae	lepraat	k5eAaPmIp3nS	lepraat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
sklon	sklon	k1gInSc4	sklon
napadat	napadat	k5eAaBmF	napadat
periferní	periferní	k2eAgInPc4d1	periferní
nervy	nerv	k1gInPc4	nerv
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
chladnější	chladný	k2eAgFnPc4d2	chladnější
oblasti	oblast	k1gFnPc4	oblast
těla	tělo	k1gNnSc2	tělo
-	-	kIx~	-
kůži	kůže	k1gFnSc3	kůže
a	a	k8xC	a
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
.	.	kIx.	.
</s>
