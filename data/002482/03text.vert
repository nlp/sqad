<s>
Tennessee	Tennessee	k1gInSc1	Tennessee
Williams	Williams	k1gInSc1	Williams
[	[	kIx(	[
<g/>
viljems	viljems	k1gInSc1	viljems
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Thomas	Thomas	k1gMnSc1	Thomas
Lanier	Lanier	k1gMnSc1	Lanier
Williams	Williams	k1gInSc4	Williams
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1911	[number]	k4	1911
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
psychologického	psychologický	k2eAgNnSc2d1	psychologické
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
žurnalistiku	žurnalistika	k1gFnSc4	žurnalistika
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Missouri	Missouri	k1gFnPc4	Missouri
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
školu	škola	k1gFnSc4	škola
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
jméno	jméno	k1gNnSc4	jméno
Tennessee	Tennesse	k1gFnSc2	Tennesse
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
hrách	hra	k1gFnPc6	hra
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
především	především	k9	především
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
platila	platit	k5eAaImAgNnP	platit
velmi	velmi	k6eAd1	velmi
puritánská	puritánský	k2eAgNnPc1d1	puritánské
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinové	Hrdinová	k1gFnPc1	Hrdinová
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
často	často	k6eAd1	často
utíkají	utíkat	k5eAaImIp3nP	utíkat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
iluzí	iluze	k1gFnPc2	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
andělů	anděl	k1gMnPc2	anděl
-	-	kIx~	-
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
verzí	verze	k1gFnSc7	verze
Sestup	sestup	k1gInSc4	sestup
Orfeův	Orfeův	k2eAgInSc4d1	Orfeův
-	-	kIx~	-
1955	[number]	k4	1955
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
inscenováno	inscenovat	k5eAaBmNgNnS	inscenovat
v	v	k7c6	v
r.	r.	kA	r.
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c4	na
kombinaci	kombinace	k1gFnSc4	kombinace
dvou	dva	k4xCgInPc2	dva
mýtů	mýtus	k1gInPc2	mýtus
-	-	kIx~	-
řeckého	řecký	k2eAgNnSc2d1	řecké
o	o	k7c6	o
Orfeovi	Orfeus	k1gMnSc6	Orfeus
a	a	k8xC	a
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
o	o	k7c6	o
Kristovi	Kristus	k1gMnSc6	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Orfea	Orfeus	k1gMnSc4	Orfeus
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
Krista	Kristus	k1gMnSc4	Kristus
představuje	představovat	k5eAaImIp3nS	představovat
tulák	tulák	k1gMnSc1	tulák
s	s	k7c7	s
kytarou	kytara	k1gFnSc7	kytara
Val	val	k1gInSc4	val
Xavier	Xavier	k1gMnSc1	Xavier
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
jižanského	jižanský	k2eAgNnSc2d1	jižanské
pekla	peklo	k1gNnSc2	peklo
pokrytecké	pokrytecký	k2eAgFnSc2d1	pokrytecká
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
rasismu	rasismus	k1gInSc2	rasismus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
Lady	lady	k1gFnSc4	lady
(	(	kIx(	(
<g/>
Eurydiku	Eurydika	k1gFnSc4	Eurydika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zastřelena	zastřelen	k2eAgFnSc1d1	zastřelena
svým	svůj	k1gMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
když	když	k8xS	když
brání	bránit	k5eAaImIp3nS	bránit
Vala	Vala	k1gMnSc1	Vala
před	před	k7c7	před
lynčováním	lynčování	k1gNnSc7	lynčování
<g/>
.	.	kIx.	.
</s>
<s>
Skleněný	skleněný	k2eAgMnSc1d1	skleněný
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
-	-	kIx~	-
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Touha	touha	k1gFnSc1	touha
-	-	kIx~	-
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
.	.	kIx.	.
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
vrchol	vrchol	k1gInSc1	vrchol
americké	americký	k2eAgFnSc2d1	americká
dramatické	dramatický	k2eAgFnSc2d1	dramatická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
;	;	kIx,	;
zevrubná	zevrubný	k2eAgFnSc1d1	zevrubná
analýza	analýza	k1gFnSc1	analýza
krutého	krutý	k2eAgNnSc2d1	kruté
zhroucení	zhroucení	k1gNnSc2	zhroucení
iluzivních	iluzivní	k2eAgFnPc2d1	iluzivní
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
sebeklamu	sebeklam	k1gInSc3	sebeklam
<g/>
;	;	kIx,	;
krutost	krutost	k1gFnSc1	krutost
<g/>
,	,	kIx,	,
hrubost	hrubost	k1gFnSc1	hrubost
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc1	násilí
<g/>
,	,	kIx,	,
brutalita	brutalita	k1gFnSc1	brutalita
-	-	kIx~	-
ničení	ničení	k1gNnSc1	ničení
lidských	lidský	k2eAgFnPc2d1	lidská
tužeb	tužba	k1gFnPc2	tužba
a	a	k8xC	a
nadějí	naděje	k1gFnPc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Hl.	Hl.	k?	Hl.
<g/>
postava	postava	k1gFnSc1	postava
<g/>
:	:	kIx,	:
Blanche	Blanche	k1gFnSc1	Blanche
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Stella	Stella	k1gFnSc1	Stella
(	(	kIx(	(
<g/>
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g/>
)	)	kIx)	)
Léto	léto	k1gNnSc1	léto
a	a	k8xC	a
dým	dým	k1gInSc1	dým
Tetovaná	tetovaný	k2eAgFnSc1d1	tetovaná
růže	růže	k1gFnSc1	růže
-	-	kIx~	-
1951	[number]	k4	1951
Sestup	sestup	k1gInSc1	sestup
Orfeův	Orfeův	k2eAgInSc1d1	Orfeův
-	-	kIx~	-
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
verze	verze	k1gFnSc1	verze
Bitvy	bitva	k1gFnSc2	bitva
Andělů	Anděl	k1gMnPc2	Anděl
Kočka	kočka	k1gFnSc1	kočka
na	na	k7c6	na
rozpálené	rozpálený	k2eAgFnSc6d1	rozpálená
plechové	plechový	k2eAgFnSc6d1	plechová
střeše	střecha	k1gFnSc6	střecha
-	-	kIx~	-
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc4d1	zfilmováno
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
Maggie	Maggie	k1gFnSc1	Maggie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vdá	vdát	k5eAaPmIp3nS	vdát
za	za	k7c4	za
syna	syn	k1gMnSc4	syn
jižanského	jižanský	k2eAgMnSc2d1	jižanský
milionáře	milionář	k1gMnSc2	milionář
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
milionář	milionář	k1gMnSc1	milionář
dostane	dostat	k5eAaPmIp3nS	dostat
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c6	na
snaze	snaha	k1gFnSc6	snaha
mnoha	mnoho	k4c2	mnoho
zúčastněných	zúčastněný	k2eAgInPc2d1	zúčastněný
získat	získat	k5eAaPmF	získat
přízeň	přízeň	k1gFnSc4	přízeň
milionáře	milionář	k1gMnSc2	milionář
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
závěti	závěť	k1gFnSc2	závěť
<g/>
.	.	kIx.	.
</s>
<s>
Maggie	Maggie	k1gFnSc1	Maggie
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
sporům	spor	k1gInPc3	spor
znovu	znovu	k6eAd1	znovu
sblíží	sblížit	k5eAaPmIp3nS	sblížit
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
alkoholismu	alkoholismus	k1gInSc3	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
Sladké	Sladké	k2eAgNnSc1d1	Sladké
ptáče	ptáče	k1gNnSc1	ptáče
mládí	mládí	k1gNnSc2	mládí
-	-	kIx~	-
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
mužského	mužský	k2eAgMnSc4d1	mužský
hrdinu	hrdina	k1gMnSc4	hrdina
(	(	kIx(	(
<g/>
ztroskotanec	ztroskotanec	k1gMnSc1	ztroskotanec
<g/>
,	,	kIx,	,
gigolo	gigolo	k1gMnSc1	gigolo
<g/>
)	)	kIx)	)
vykastrují	vykastrovat	k5eAaPmIp3nP	vykastrovat
najatí	najatý	k2eAgMnPc1d1	najatý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
lety	léto	k1gNnPc7	léto
nakazil	nakazit	k5eAaPmAgMnS	nakazit
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
chorobou	choroba	k1gFnSc7	choroba
dceru	dcera	k1gFnSc4	dcera
jižanského	jižanský	k2eAgMnSc4d1	jižanský
politického	politický	k2eAgMnSc4d1	politický
bosse	boss	k1gMnSc4	boss
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
s	s	k7c7	s
leguánem	leguán	k1gMnSc7	leguán
-	-	kIx~	-
1961	[number]	k4	1961
Dále	daleko	k6eAd2	daleko
napsal	napsat	k5eAaBmAgMnS	napsat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
jednoaktovek	jednoaktovka	k1gFnPc2	jednoaktovka
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
podobným	podobný	k2eAgInSc7d1	podobný
námětem	námět	k1gInSc7	námět
<g/>
.	.	kIx.	.
</s>
<s>
Inventura	inventura	k1gFnSc1	inventura
ve	v	k7c4	v
Fontana	Fontan	k1gMnSc4	Fontan
Bella	Bell	k1gMnSc4	Bell
<g/>
,	,	kIx,	,
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Radoslav	Radoslav	k1gMnSc1	Radoslav
Nenadál	nadát	k5eNaBmAgMnS	nadát
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Mančal	Mančal	k1gMnSc1	Mančal
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
<g/>
:	:	kIx,	:
Libuše	Libuše	k1gFnSc1	Libuše
Švormová	Švormový	k2eAgFnSc1d1	Švormový
Římské	římský	k2eAgNnSc4d1	římské
jaro	jaro	k1gNnSc4	jaro
paní	paní	k1gFnSc2	paní
Stoneové	Stoneová	k1gFnSc2	Stoneová
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Louka	louka	k1gFnSc1	louka
modrých	modrý	k2eAgFnPc2d1	modrá
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Osm	osm	k4xCc4	osm
smrtelnic	smrtelnice	k1gFnPc2	smrtelnice
posedlých	posedlý	k2eAgFnPc2d1	posedlá
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Anděl	Anděla	k1gFnPc2	Anděla
ve	v	k7c6	v
výklenku	výklenek	k1gInSc6	výklenek
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Sladké	Sladké	k2eAgNnSc1d1	Sladké
ptáče	ptáče	k1gNnSc1	ptáče
mládí	mládí	k1gNnSc2	mládí
(	(	kIx(	(
<g/>
románová	románový	k2eAgFnSc1d1	románová
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Skleněný	skleněný	k2eAgMnSc1d1	skleněný
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1944	[number]	k4	1944
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milan	Milan	k1gMnSc1	Milan
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
úprava	úprava	k1gFnSc1	úprava
a	a	k8xC	a
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
:	:	kIx,	:
Klára	Klára	k1gFnSc1	Klára
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Ondřej	Ondřej	k1gMnSc1	Ondřej
Gášek	Gášek	k1gMnSc1	Gášek
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc1	osoba
a	a	k8xC	a
obsazení	obsazení	k1gNnSc1	obsazení
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
(	(	kIx(	(
<g/>
Kajetán	Kajetán	k1gMnSc1	Kajetán
Písařovic	Písařovice	k1gFnPc2	Písařovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amanda	Amanda	k1gFnSc1	Amanda
(	(	kIx(	(
<g/>
Vilma	Vilma	k1gFnSc1	Vilma
Cibulková	Cibulková	k1gFnSc1	Cibulková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
(	(	kIx(	(
<g/>
Ivana	Ivana	k1gFnSc1	Ivana
Uhlířová	Uhlířová	k1gFnSc1	Uhlířová
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Písařík	písařík	k1gMnSc1	písařík
<g/>
)	)	kIx)	)
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Mančal	Mančal	k1gMnSc1	Mančal
<g/>
.	.	kIx.	.
</s>
