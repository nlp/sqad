<s>
Ernest	Ernest	k1gMnSc1	Ernest
Thomas	Thomas	k1gMnSc1	Thomas
Sinton	Sinton	k1gInSc4	Sinton
Walton	Walton	k1gInSc1	Walton
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1903	[number]	k4	1903
Dunngarvan	Dunngarvan	k1gMnSc1	Dunngarvan
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
J.	J.	kA	J.
D.	D.	kA	D.
Cockcroftem	Cockcroft	k1gMnSc7	Cockcroft
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
za	za	k7c2	za
objevné	objevný	k2eAgFnSc2d1	objevná
práce	práce	k1gFnSc2	práce
přeměny	přeměna	k1gFnSc2	přeměna
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
uměle	uměle	k6eAd1	uměle
urychlenými	urychlený	k2eAgFnPc7d1	urychlená
jadernými	jaderný	k2eAgFnPc7d1	jaderná
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
