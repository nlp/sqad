<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Purnululu	Purnululu	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Purnululu	Purnululu	k1gInSc1
National	National	k2eAgInSc1d1
Park	park	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
spolkového	spolkový	k2eAgInSc2d1
státu	stát	k1gInSc2
Západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Kimberley	cKimberley	k1gFnSc1
<g/>
.	.	kIx.
</s>