<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Purnululu	Purnulul	k1gInSc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgFnSc3d1
park	park	k1gInSc1
PurnululuPurnululu	PurnululuPurnululum	k1gNnSc6
National	National	k1gFnSc2
ParkIUCN	ParkIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
II	II	kA
(	(	kIx(
<g/>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
Typické	typický	k2eAgFnPc1d1
zvětralé	zvětralý	k2eAgFnPc1d1
pískovcové	pískovcový	k2eAgFnPc1d1
skályZákladní	skályZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1987	#num#	k4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
287	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
2397	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Spolkový	spolkový	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Západní	západní	k2eAgFnSc1d1
Austrálie	Austrálie	k1gFnSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Kununurra	Kununurra	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
17	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
128	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Purnululu	Purnulul	k1gInSc2
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
parks	parks	k1gInSc1
<g/>
.	.	kIx.
<g/>
dpaw	dpaw	k?
<g/>
.	.	kIx.
<g/>
wa	wa	k?
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
/	/	kIx~
<g/>
park	park	k1gInSc4
<g/>
/	/	kIx~
<g/>
purnululu	purnulula	k1gFnSc4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
PurnululuSvětové	PurnululuSvětové	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCOSmluvní	UNESCOSmluvný	k2eAgMnPc5d1
stát	stát	k1gInSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Typ	typ	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
Kritérium	kritérium	k1gNnSc1
</s>
<s>
vii	vii	k?
<g/>
,	,	kIx,
viii	viii	k6eAd1
Odkaz	odkaz	k1gInSc1
</s>
<s>
1094	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
2003	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Purnululu	Purnululu	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Purnululu	Purnululu	k1gInSc1
National	National	k2eAgInSc1d1
Park	park	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
spolkového	spolkový	k2eAgInSc2d1
státu	stát	k1gInSc2
Západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Kimberley	cKimberley	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1
neobydlené	obydlený	k2eNgNnSc1d1
území	území	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
ekosystémy	ekosystém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středu	střed	k1gInSc6
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
ten	ten	k3xDgMnSc1
nejvzácnější	vzácný	k2eAgMnSc1d3
<g/>
,	,	kIx,
450	#num#	k4
km²	km²	k?
rozlehlá	rozlehlý	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
Bungle	Bungle	k1gFnSc1
Bungle	Bungle	k1gFnSc1
<g/>
,	,	kIx,
vytvořená	vytvořený	k2eAgFnSc1d1
v	v	k7c6
období	období	k1gNnSc6
devonu	devon	k1gInSc2
z	z	k7c2
křemenných	křemenný	k2eAgInPc2d1
pískovců	pískovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgFnSc7d1
a	a	k8xC
větrnou	větrný	k2eAgFnSc7d1
erozí	eroze	k1gFnSc7
byly	být	k5eAaImAgFnP
za	za	k7c4
období	období	k1gNnSc4
cca	cca	kA
20	#num#	k4
miliónů	milión	k4xCgInPc2
let	léto	k1gNnPc2
skály	skála	k1gFnSc2
vymodelovány	vymodelovat	k5eAaPmNgInP
do	do	k7c2
tvarů	tvar	k1gInPc2
konických	konický	k2eAgFnPc2d1
věží	věž	k1gFnPc2
nebo	nebo	k8xC
úlů	úl	k1gInPc2
vysokých	vysoký	k2eAgInPc2d1
až	až	k6eAd1
250	#num#	k4
m.	m.	k?
Jejichž	jejichž	k3xOyRp3gInPc1
strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
<g/>
,	,	kIx,
spadající	spadající	k2eAgInPc1d1
do	do	k7c2
hlubokých	hluboký	k2eAgFnPc2d1
<g/>
,	,	kIx,
úzkých	úzký	k2eAgFnPc2d1
a	a	k8xC
křivolatých	křivolatý	k2eAgFnPc2d1
soutěsek	soutěska	k1gFnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zřetelně	zřetelně	k6eAd1
tmavě	tmavě	k6eAd1
šedě	šedě	k6eAd1
a	a	k8xC
oranžově	oranžově	k6eAd1
pruhované	pruhovaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
<g/>
,	,	kIx,
světově	světově	k6eAd1
proslulými	proslulý	k2eAgInPc7d1
krasovitými	krasovitý	k2eAgInPc7d1
útvary	útvar	k1gInPc7
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgMnPc4
nejvyšší	vysoký	k2eAgMnPc4d3
a	a	k8xC
z	z	k7c2
hlediska	hledisko	k1gNnSc2
geologického	geologický	k2eAgNnSc2d1
složení	složení	k1gNnSc2
ojedinělé	ojedinělý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Součásti	součást	k1gFnSc3
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihu	jih	k1gInSc6
travnaté	travnatý	k2eAgNnSc1d1
a	a	k8xC
hluboko	hluboko	k6eAd1
zařezané	zařezaný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
řeky	řeka	k1gFnSc2
Ord	Ord	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
vápencový	vápencový	k2eAgInSc1d1
horský	horský	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
Osmand	Osmand	k1gInSc1
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
zalesněné	zalesněný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
Osmand	Osmand	k1gInSc1
Range	Range	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poloha	poloha	k1gFnSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
</s>
<s>
Nejbližší	blízký	k2eAgNnSc1d3
významnější	významný	k2eAgNnSc1d2
město	město	k1gNnSc1
<g/>
,	,	kIx,
pětitisícová	pětitisícový	k2eAgFnSc1d1
Kununurra	Kununurra	k1gFnSc1
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
250	#num#	k4
km	km	kA
jižněji	jižně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
parku	park	k1gInSc3
o	o	k7c6
rozloze	rozloha	k1gFnSc6
téměř	téměř	k6eAd1
2400	#num#	k4
km²	km²	k?
přiléhá	přiléhat	k5eAaImIp3nS
ještě	ještě	k9
ochranné	ochranný	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
(	(	kIx(
<g/>
Reserve	Reserev	k1gFnPc1
Ord	orda	k1gFnPc2
River	Rivra	k1gFnPc2
<g/>
)	)	kIx)
velké	velká	k1gFnPc1
asi	asi	k9
800	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
zabránit	zabránit	k5eAaPmF
průniku	průnik	k1gInSc3
nežádoucích	žádoucí	k2eNgInPc2d1
vlivů	vliv	k1gInPc2
ze	z	k7c2
sousedních	sousední	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
z	z	k7c2
případné	případný	k2eAgFnSc2d1
hornické	hornický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Region	region	k1gInSc1
má	mít	k5eAaImIp3nS
suché	suchý	k2eAgNnSc4d1
monzunové	monzunový	k2eAgNnSc4d1
klima	klima	k1gNnSc4
s	s	k7c7
výraznými	výrazný	k2eAgFnPc7d1
sezonami	sezona	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
dešťů	dešť	k1gInPc2
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
až	až	k8xS
březen	březen	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
horké	horký	k2eAgNnSc1d1
<g/>
,	,	kIx,
průměrné	průměrný	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
v	v	k7c6
říjnu	říjen	k1gInSc6
jsou	být	k5eAaImIp3nP
38	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
celkové	celkový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
srážek	srážka	k1gFnPc2
okolo	okolo	k7c2
600	#num#	k4
mm	mm	kA
je	být	k5eAaImIp3nS
doprovázeno	doprovázet	k5eAaImNgNnS
prudkými	prudký	k2eAgFnPc7d1
bouřemi	bouř	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
sucha	sucho	k1gNnSc2
(	(	kIx(
<g/>
duben	duben	k1gInSc1
až	až	k8xS
říjen	říjen	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
mnohem	mnohem	k6eAd1
nižší	nízký	k2eAgFnSc1d2
a	a	k8xC
vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
i	i	k9
noční	noční	k2eAgInPc1d1
mrazíky	mrazík	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Park	park	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
oblastí	oblast	k1gFnPc2
vlhké	vlhký	k2eAgFnSc2d1
tropické	tropický	k2eAgFnSc2d1
a	a	k8xC
vnitrozemské	vnitrozemský	k2eAgFnSc2d1
vyprahlé	vyprahlý	k2eAgFnPc4d1
oblastí	oblast	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
i	i	k9
vegetace	vegetace	k1gFnSc1
v	v	k7c6
parku	park	k1gInSc6
velmi	velmi	k6eAd1
různá	různý	k2eAgFnSc1d1
a	a	k8xC
promíšena	promíšen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
tam	tam	k6eAd1
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
653	#num#	k4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
628	#num#	k4
vyšších	vysoký	k2eAgFnPc2d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fauna	fauna	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
parku	park	k1gInSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
asi	asi	k9
40	#num#	k4
druhů	druh	k1gInPc2
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
150	#num#	k4
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
80	#num#	k4
plazů	plaz	k1gMnPc2
a	a	k8xC
10	#num#	k4
obojživelníků	obojživelník	k1gMnPc2
a	a	k8xC
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Stáří	stáří	k1gNnSc1
hornin	hornina	k1gFnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
500	#num#	k4
miliónů	milión	k4xCgInPc2
let	léto	k1gNnPc2
a	a	k8xC
nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gMnPc6
stromatolity	stromatolit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
procesy	proces	k1gInPc4
sedimentace	sedimentace	k1gFnSc2
<g/>
,	,	kIx,
utužováním	utužování	k1gNnSc7
a	a	k8xC
následným	následný	k2eAgNnSc7d1
vyzvednutím	vyzvednutí	k1gNnSc7
<g/>
,	,	kIx,
způsobené	způsobený	k2eAgNnSc1d1
střetem	střet	k1gInSc7
Gondwany	Gondwana	k1gFnSc2
a	a	k8xC
Laurasie	Laurasie	k1gFnSc2
asi	asi	k9
před	před	k7c7
300	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
a	a	k8xC
sražením	sražení	k1gNnSc7
Indo-australské	Indo-australský	k2eAgFnSc2d1
desky	deska	k1gFnSc2
desky	deska	k1gFnSc2
s	s	k7c7
Pacifickou	pacifický	k2eAgFnSc4d1
asi	asi	k9
před	před	k7c7
20	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
původního	původní	k2eAgNnSc2d1
starobylého	starobylý	k2eAgNnSc2d1
říčního	říční	k2eAgNnSc2d1
koryta	koryto	k1gNnSc2
byly	být	k5eAaImAgFnP
usazené	usazený	k2eAgFnPc1d1
pískovcové	pískovcový	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
geologickou	geologický	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
vyzvednuty	vyzvednut	k2eAgInPc1d1
vysoko	vysoko	k6eAd1
nad	nad	k7c4
okolí	okolí	k1gNnSc4
a	a	k8xC
vodní	vodní	k2eAgNnSc4d1
i	i	k8xC
větrná	větrný	k2eAgFnSc1d1
eroze	eroze	k1gFnSc1
je	být	k5eAaImIp3nS
za	za	k7c4
mnoho	mnoho	k4c4
miliónů	milión	k4xCgInPc2
let	léto	k1gNnPc2
vymodelovala	vymodelovat	k5eAaPmAgFnS
do	do	k7c2
současných	současný	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
Bungle	Bungl	k1gMnSc2
Bungle	Bungl	k1gMnSc2
je	být	k5eAaImIp3nS
578	#num#	k4
m	m	kA
nad	nad	k7c7
hladinou	hladina	k1gFnSc7
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nezvyklé	zvyklý	k2eNgFnPc1d1
oranžové	oranžový	k2eAgFnPc1d1
a	a	k8xC
tmavě	tmavě	k6eAd1
šedé	šedý	k2eAgInPc4d1
vodorovné	vodorovný	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
na	na	k7c6
kónických	kónický	k2eAgInPc6d1
skalních	skalní	k2eAgInPc6d1
útvarech	útvar	k1gInPc6
jsou	být	k5eAaImIp3nP
způsobené	způsobený	k2eAgInPc1d1
rozdílnými	rozdílný	k2eAgInPc7d1
uloženými	uložený	k2eAgInPc7d1
materiály	materiál	k1gInPc7
<g/>
.	.	kIx.
<g/>
Tmavší	tmavý	k2eAgInPc1d2
pásy	pás	k1gInPc1
jsou	být	k5eAaImIp3nP
nasákavější	nasákavý	k2eAgFnPc4d2
horniny	hornina	k1gFnPc4
jímající	jímající	k2eAgFnPc4d1
více	hodně	k6eAd2
vlhkosti	vlhkost	k1gFnPc4
a	a	k8xC
rostou	růst	k5eAaImIp3nP
v	v	k7c6
nich	on	k3xPp3gFnPc6
řasy	řasa	k1gFnPc1
a	a	k8xC
sinice	sinice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oranžově	oranžově	k6eAd1
zbarvené	zbarvený	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
jsou	být	k5eAaImIp3nP
obarveny	obarven	k2eAgInPc1d1
kysličníky	kysličník	k1gInPc1
železa	železo	k1gNnSc2
a	a	k8xC
manganu	mangan	k1gInSc2
z	z	k7c2
blízkých	blízký	k2eAgNnPc2d1
ložisek	ložisko	k1gNnPc2
nerostů	nerost	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
březích	břeh	k1gInPc6
řeky	řeka	k1gFnSc2
Ord	orda	k1gFnPc2
žili	žít	k5eAaImAgMnP
domorodí	domorodý	k2eAgMnPc1d1
Australané	Australan	k1gMnPc1
<g/>
,	,	kIx,
sběrači	sběrač	k1gMnPc1
a	a	k8xC
lovci	lovec	k1gMnPc1
<g/>
,	,	kIx,
po	po	k7c4
dobu	doba	k1gFnSc4
nejméně	málo	k6eAd3
20	#num#	k4
000	#num#	k4
roků	rok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
setkávali	setkávat	k5eAaImAgMnP
a	a	k8xC
mísili	mísit	k5eAaImAgMnP
obyvatelé	obyvatel	k1gMnPc1
pouští	poušť	k1gFnPc2
i	i	k8xC
zelených	zelený	k2eAgFnPc2d1
savan	savana	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
jako	jako	k9
jejich	jejich	k3xOp3gFnPc1
kultury	kultura	k1gFnPc1
a	a	k8xC
jazyky	jazyk	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1885	#num#	k4
počali	počnout	k5eAaPmAgMnP
úrodnější	úrodný	k2eAgFnSc3d2
oblasti	oblast	k1gFnSc3
kolonizovat	kolonizovat	k5eAaBmF
chovatele	chovatel	k1gMnSc4
dobytka	dobytek	k1gInSc2
a	a	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
vzdáleném	vzdálený	k2eAgNnSc6d1
jen	jen	k9
100	#num#	k4
km	km	kA
propukla	propuknout	k5eAaPmAgFnS
zlatá	zlatý	k2eAgFnSc1d1
horečka	horečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
o	o	k7c6
této	tento	k3xDgFnSc6
krajinné	krajinný	k2eAgFnSc6d1
zvláštnosti	zvláštnost	k1gFnSc6
věděli	vědět	k5eAaImAgMnP
jen	jen	k9
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
(	(	kIx(
<g/>
jméno	jméno	k1gNnSc1
Purnululu	Purnulul	k1gInSc2
znamená	znamenat	k5eAaImIp3nS
v	v	k7c6
jazyce	jazyk	k1gInSc6
domorodců	domorodec	k1gMnPc2
„	„	k?
<g/>
pískovec	pískovec	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
odvysílání	odvysílání	k1gNnSc6
filmového	filmový	k2eAgInSc2d1
dokumentu	dokument	k1gInSc2
o	o	k7c6
těchto	tento	k3xDgFnPc6
krásách	krása	k1gFnPc6
vznikl	vzniknout	k5eAaPmAgInS
zájem	zájem	k1gInSc1
a	a	k8xC
území	území	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
vlády	vláda	k1gFnPc4
Západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
za	za	k7c4
národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
jej	on	k3xPp3gNnSc4
UNESCO	Unesco	k1gNnSc1
zařadilo	zařadit	k5eAaPmAgNnS
na	na	k7c4
seznam	seznam	k1gInSc4
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návštěva	návštěva	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Purnululu	Purnulul	k1gInSc2
lze	lze	k6eAd1
navštívit	navštívit	k5eAaPmF
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
návštěvnické	návštěvnický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
je	být	k5eAaImIp3nS
otevřeno	otevřít	k5eAaPmNgNnS
jen	jen	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjezdová	příjezdový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
vede	vést	k5eAaImIp3nS
náročným	náročný	k2eAgInSc7d1
terénem	terén	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
jen	jen	k9
pro	pro	k7c4
vozidla	vozidlo	k1gNnPc4
s	s	k7c7
náhonem	náhon	k1gInSc7
na	na	k7c4
čtyři	čtyři	k4xCgNnPc4
kola	kolo	k1gNnPc4
a	a	k8xC
jednonápravové	jednonápravový	k2eAgInPc4d1
přívěsy	přívěs	k1gInPc4
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
i	i	k9
přes	přes	k7c4
brod	brod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parku	park	k1gInSc6
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
el.	el.	k?
energie	energie	k1gFnSc1
ani	ani	k8xC
pitná	pitný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
obchod	obchod	k1gInSc4
a	a	k8xC
není	být	k5eNaImIp3nS
povoleno	povolen	k2eAgNnSc1d1
vnášet	vnášet	k5eAaImF
domácí	domácí	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
pokrytí	pokrytí	k1gNnSc4
telefonním	telefonní	k2eAgMnSc7d1
ani	ani	k8xC
internetovým	internetový	k2eAgInSc7d1
signálem	signál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tam	tam	k6eAd1
pouze	pouze	k6eAd1
dvě	dva	k4xCgNnPc4
placená	placený	k2eAgNnPc4d1
kempovací	kempovací	k2eAgNnPc4d1
místa	místo	k1gNnPc4
s	s	k7c7
užitkovou	užitkový	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
vyznačené	vyznačený	k2eAgFnSc2d1
vycházkové	vycházkový	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
a	a	k8xC
zbudována	zbudován	k2eAgFnSc1d1
přistávací	přistávací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
pro	pro	k7c4
vrtulník	vrtulník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Park	park	k1gInSc1
nemá	mít	k5eNaImIp3nS
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
strážce	strážce	k1gMnPc4
<g/>
,	,	kIx,
trvalých	trvalý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Purnululu	Purnulul	k1gInSc2
National	National	k1gFnSc2
Park	park	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Heritage	Heritag	k1gFnSc2
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
UNESCO	Unesco	k1gNnSc1
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
FR	fr	k0
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Australian	Australiany	k1gInPc2
Heritage	Heritage	k1gInSc1
Database	Databasa	k1gFnSc3
-	-	kIx~
Purnululu	Purnululum	k1gNnSc3
National	National	k1gFnSc2
Park	park	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Government	Government	k1gMnSc1
<g/>
,	,	kIx,
Department	department	k1gInSc1
of	of	k?
the	the	k?
Environment	Environment	k1gMnSc1
<g/>
,	,	kIx,
Canberra	Canberra	k1gMnSc1
<g/>
,	,	kIx,
AU	au	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
World	Worldo	k1gNnPc2
Heritage	Heritage	k1gFnPc2
Places	Placesa	k1gFnPc2
-	-	kIx~
Purnululu	Purnulula	k1gFnSc4
National	National	k1gFnSc2
Park	park	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Government	Government	k1gMnSc1
<g/>
,	,	kIx,
Department	department	k1gInSc1
of	of	k?
the	the	k?
Environment	Environment	k1gMnSc1
<g/>
,	,	kIx,
Canberra	Canberra	k1gMnSc1
<g/>
,	,	kIx,
AU	au	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kurrajong	Kurrajong	k1gInSc1
(	(	kIx(
<g/>
Purnululu	Purnulul	k1gInSc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
Parks	Parks	k1gInSc1
and	and	k?
Wildlife	Wildlif	k1gMnSc5
<g/>
,	,	kIx,
Goverment	Goverment	k1gInSc1
of	of	k?
Western	western	k1gInSc1
Australia	Australia	k1gFnSc1
<g/>
,	,	kIx,
Perth	Perth	k1gInSc1
<g/>
,	,	kIx,
WA	WA	kA
<g/>
,	,	kIx,
AU	au	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Purnululu	Purnulul	k1gInSc2
National	National	k1gFnSc2
Park	park	k1gInSc1
and	and	k?
the	the	k?
Bungle	Bungle	k1gFnSc1
Bungle	Bungle	k1gFnSc1
Range	Range	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australia	Australia	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
North	North	k1gMnSc1
West	West	k1gMnSc1
<g/>
,	,	kIx,
Perth	Perth	k1gMnSc1
<g/>
,	,	kIx,
AU	au	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
Purnululu	Purnulul	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
PURNULULU	PURNULULU	kA
NATIONAL	NATIONAL	kA
PARK	park	k1gInSc1
<g/>
,	,	kIx,
WESTERN	western	k1gInSc1
AUSTRALIA	AUSTRALIA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
OSN	OSN	kA
pro	pro	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Australské	australský	k2eAgFnSc2d1
památky	památka	k1gFnSc2
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Velký	velký	k2eAgInSc1d1
bariérový	bariérový	k2eAgInSc1d1
útes	útes	k1gInSc1
•	•	k?
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Kakadu	kakadu	k1gMnSc1
•	•	k?
jezera	jezero	k1gNnSc2
Willandra	Willandra	k1gFnSc1
•	•	k?
Tasmánská	tasmánský	k2eAgFnSc1d1
divočina	divočina	k1gFnSc1
•	•	k?
ostrovy	ostrov	k1gInPc1
lorda	lord	k1gMnSc2
Howa	Howus	k1gMnSc2
•	•	k?
Gondwanské	Gondwanský	k2eAgInPc4d1
deštné	deštný	k2eAgInPc4d1
pralesy	prales	k1gInPc4
Austrálie	Austrálie	k1gFnSc2
•	•	k?
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Uluru-Kata	Uluru-Kat	k1gMnSc2
Tjuta	Tjut	k1gMnSc2
•	•	k?
vlhké	vlhký	k2eAgInPc1d1
tropy	trop	k1gInPc1
Queenslandu	Queensland	k1gInSc2
•	•	k?
Žraločí	žraločí	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
•	•	k?
ostrov	ostrov	k1gInSc1
Fraser	Fraser	k1gInSc1
•	•	k?
archeologická	archeologický	k2eAgFnSc1d1
naleziště	naleziště	k1gNnSc4
Riversleigh	Riversleigh	k1gInSc1
a	a	k8xC
Naracoorte	Naracoort	k1gInSc5
•	•	k?
Heardův	Heardův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
a	a	k8xC
McDonaldovy	McDonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
ostrov	ostrov	k1gInSc1
Macquarie	Macquarie	k1gFnSc1
•	•	k?
krajina	krajina	k1gFnSc1
Modrých	modrý	k2eAgFnPc2d1
hor	hora	k1gFnPc2
•	•	k?
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Purnululu	Purnulul	k1gInSc2
•	•	k?
Královská	královský	k2eAgFnSc1d1
výstavní	výstavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
a	a	k8xC
zahrady	zahrada	k1gFnSc2
Carlton	Carlton	k1gInSc1
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
•	•	k?
budova	budova	k1gFnSc1
opery	opera	k1gFnSc2
v	v	k7c6
Sydney	Sydney	k1gNnPc2
•	•	k?
Australské	australský	k2eAgFnSc2d1
trestanecké	trestanecký	k2eAgFnSc2d1
osady	osada	k1gFnSc2
•	•	k?
pobřeží	pobřeží	k1gNnSc1
Ningaloo	Ningaloo	k1gMnSc1
•	•	k?
Budj	Budj	k1gMnSc1
Bim	bim	k0
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
315527592	#num#	k4
</s>
