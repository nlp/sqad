<s>
Součásti	součást	k1gFnSc3
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihu	jih	k1gInSc6
travnaté	travnatý	k2eAgNnSc1d1
a	a	k8xC
hluboko	hluboko	k6eAd1
zařezané	zařezaný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
řeky	řeka	k1gFnSc2
Ord	Ord	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
vápencový	vápencový	k2eAgInSc1d1
horský	horský	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
Osmand	Osmand	k1gInSc1
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
zalesněné	zalesněný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
Osmand	Osmand	k1gInSc1
Range	Range	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>