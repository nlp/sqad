<s>
Mensa	mensa	k1gFnSc1	mensa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
pobočka	pobočka	k1gFnSc1	pobočka
organizace	organizace	k1gFnSc2	organizace
Mensa	mensa	k1gFnSc1	mensa
International	International	k1gFnSc2	International
<g/>
;	;	kIx,	;
dobrovolné	dobrovolný	k2eAgNnSc4d1	dobrovolné
<g/>
,	,	kIx,	,
nepolitické	politický	k2eNgNnSc4d1	nepolitické
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
a	a	k8xC	a
neziskové	ziskový	k2eNgNnSc4d1	neziskové
sdružení	sdružení	k1gNnSc4	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Mensa	mensa	k1gFnSc1	mensa
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hanou	Hana	k1gFnSc7	Hana
Drábkovou	Drábková	k1gFnSc7	Drábková
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
učinila	učinit	k5eAaPmAgFnS	učinit
první	první	k4xOgInPc4	první
pokusy	pokus	k1gInPc4	pokus
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
registrována	registrován	k2eAgFnSc1d1	registrována
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
Mensy	mensa	k1gFnSc2	mensa
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
v	v	k7c6	v
testu	test	k1gInSc6	test
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
schváleném	schválený	k2eAgInSc6d1	schválený
mezinárodním	mezinárodní	k2eAgMnSc7d1	mezinárodní
dozorčím	dozorčí	k2eAgMnSc7d1	dozorčí
psychologem	psycholog	k1gMnSc7	psycholog
Mensy	mensa	k1gFnSc2	mensa
International	International	k1gFnSc2	International
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
MIL	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výsledku	výsledek	k1gInSc6	výsledek
mezi	mezi	k7c7	mezi
horními	horní	k2eAgInPc7d1	horní
dvěma	dva	k4xCgInPc7	dva
procenty	procent	k1gInPc7	procent
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
svých	svůj	k3xOyFgFnPc2	svůj
stanov	stanova	k1gFnPc2	stanova
Mensa	mensa	k1gFnSc1	mensa
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
rozvíjení	rozvíjení	k1gNnSc3	rozvíjení
inteligence	inteligence	k1gFnSc2	inteligence
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
svůj	svůj	k3xOyFgInSc4	svůj
účel	účel	k1gInSc4	účel
Mensa	mensa	k1gFnSc1	mensa
ČR	ČR	kA	ČR
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
mj.	mj.	kA	mj.
na	na	k7c4	na
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
talentovaných	talentovaný	k2eAgFnPc2d1	talentovaná
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
soutěž	soutěž	k1gFnSc1	soutěž
Logická	logický	k2eAgFnSc1d1	logická
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
49.000	[number]	k4	49.000
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
)	)	kIx)	)
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc4	provoz
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
nadané	nadaný	k2eAgFnPc4d1	nadaná
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
Mensa	mensa	k1gFnSc1	mensa
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dříve	dříve	k6eAd2	dříve
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
Osmileté	osmiletý	k2eAgNnSc1d1	osmileté
gymnázium	gymnázium	k1gNnSc1	gymnázium
Buďánka	Buďánka	k1gFnSc1	Buďánka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mensa	mensa	k1gFnSc1	mensa
NTC	NTC	kA	NTC
Learning	Learning	k1gInSc1	Learning
–	–	k?	–
projekt	projekt	k1gInSc4	projekt
rozvoje	rozvoj	k1gInSc2	rozvoj
rozumových	rozumový	k2eAgFnPc2d1	rozumová
schopností	schopnost	k1gFnPc2	schopnost
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
předškolním	předškolní	k2eAgInSc6d1	předškolní
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
zakládání	zakládání	k1gNnSc6	zakládání
herních	herní	k2eAgInPc2d1	herní
klubů	klub	k1gInPc2	klub
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
aktivního	aktivní	k2eAgNnSc2d1	aktivní
trávení	trávení	k1gNnSc2	trávení
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
Kluby	klub	k1gInPc1	klub
nadaných	nadaný	k2eAgFnPc2d1	nadaná
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořádání	pořádání	k1gNnSc1	pořádání
letních	letní	k2eAgInPc2d1	letní
táborů	tábor	k1gInPc2	tábor
pro	pro	k7c4	pro
nadané	nadaný	k2eAgFnPc4d1	nadaná
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
příměstských	příměstský	k2eAgInPc2d1	příměstský
táborů	tábor	k1gInPc2	tábor
podpora	podpora	k1gFnSc1	podpora
a	a	k8xC	a
poradenství	poradenství	k1gNnSc4	poradenství
pro	pro	k7c4	pro
rodiče	rodič	k1gMnPc4	rodič
talentovaných	talentovaný	k2eAgFnPc2d1	talentovaná
dětí	dítě	k1gFnPc2	dítě
testování	testování	k1gNnSc2	testování
IQ	iq	kA	iq
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
i	i	k8xC	i
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
seberealizace	seberealizace	k1gFnSc2	seberealizace
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
fungování	fungování	k1gNnSc1	fungování
Mensy	mensa	k1gFnSc2	mensa
řídí	řídit	k5eAaImIp3nS	řídit
Rada	rada	k1gFnSc1	rada
Mensy	mensa	k1gFnSc2	mensa
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
Mensy	mensa	k1gFnSc2	mensa
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
je	být	k5eAaImIp3nS	být
tajné	tajný	k2eAgNnSc1d1	tajné
<g/>
,	,	kIx,	,
hlasy	hlas	k1gInPc1	hlas
mají	mít	k5eAaImIp3nP	mít
rovnou	rovný	k2eAgFnSc4d1	rovná
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
předsedou	předseda	k1gMnSc7	předseda
Mensy	mensa	k1gFnSc2	mensa
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
.	.	kIx.	.
</s>
<s>
Mensa	mensa	k1gFnSc1	mensa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
organizuje	organizovat	k5eAaBmIp3nS	organizovat
vstupní	vstupní	k2eAgNnSc1d1	vstupní
testování	testování	k1gNnSc1	testování
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
agendu	agenda	k1gFnSc4	agenda
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
časopis	časopis	k1gInSc1	časopis
Mensa	mensa	k1gFnSc1	mensa
<g/>
,	,	kIx,	,
spravuje	spravovat	k5eAaImIp3nS	spravovat
své	svůj	k3xOyFgFnPc4	svůj
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
svolává	svolávat	k5eAaImIp3nS	svolávat
valnou	valný	k2eAgFnSc4d1	valná
hromadu	hromada	k1gFnSc4	hromada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
devítičlenná	devítičlenný	k2eAgFnSc1d1	devítičlenná
rada	rada	k1gFnSc1	rada
Mensy	mensa	k1gFnSc2	mensa
<g/>
,	,	kIx,	,
redakční	redakční	k2eAgFnSc1d1	redakční
a	a	k8xC	a
internetová	internetový	k2eAgFnSc1d1	internetová
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
autorizací	autorizace	k1gFnSc7	autorizace
pro	pro	k7c4	pro
vstupní	vstupní	k2eAgNnSc4d1	vstupní
testování	testování	k1gNnSc4	testování
<g/>
.	.	kIx.	.
</s>
<s>
Mensa	mensa	k1gFnSc1	mensa
je	být	k5eAaImIp3nS	být
dobrovolnickou	dobrovolnický	k2eAgFnSc7d1	dobrovolnická
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
aktivity	aktivita	k1gFnPc1	aktivita
a	a	k8xC	a
projekty	projekt	k1gInPc1	projekt
Mensy	mensa	k1gFnSc2	mensa
jsou	být	k5eAaImIp3nP	být
vykonávány	vykonáván	k2eAgMnPc4d1	vykonáván
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
Mensy	mensa	k1gFnSc2	mensa
<g/>
,	,	kIx,	,
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
provoz	provoz	k1gInSc1	provoz
Mensy	mensa	k1gFnSc2	mensa
je	být	k5eAaImIp3nS	být
financován	financovat	k5eAaBmNgInS	financovat
z	z	k7c2	z
členských	členský	k2eAgInPc2d1	členský
příspěvků	příspěvek	k1gInPc2	příspěvek
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
testovaní	testovaný	k2eAgMnPc1d1	testovaný
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
grantů	grant	k1gInPc2	grant
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlastním	vlastní	k2eAgInSc7d1	vlastní
provozem	provoz	k1gInSc7	provoz
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
nutná	nutný	k2eAgFnSc1d1	nutná
režie	režie	k1gFnSc1	režie
vedení	vedení	k1gNnSc2	vedení
neziskové	ziskový	k2eNgFnSc2d1	nezisková
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
poznávací	poznávací	k2eAgFnPc4d1	poznávací
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
akce	akce	k1gFnPc4	akce
(	(	kIx(	(
<g/>
především	především	k9	především
exkurze	exkurze	k1gFnSc2	exkurze
a	a	k8xC	a
přednášky	přednáška	k1gFnSc2	přednáška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořádání	pořádání	k1gNnSc4	pořádání
setkání	setkání	k1gNnSc2	setkání
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
místních	místní	k2eAgFnPc2d1	místní
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
členy	člen	k1gMnPc7	člen
Mensy	mensa	k1gFnSc2	mensa
geograficky	geograficky	k6eAd1	geograficky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořádání	pořádání	k1gNnSc1	pořádání
akcí	akce	k1gFnPc2	akce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zájmových	zájmový	k2eAgFnPc2d1	zájmová
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
členy	člen	k1gMnPc7	člen
Mensy	mensa	k1gFnSc2	mensa
tematicky	tematicky	k6eAd1	tematicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořádání	pořádání	k1gNnSc1	pořádání
celostátních	celostátní	k2eAgNnPc2d1	celostátní
setkání	setkání	k1gNnPc2	setkání
mensanů	mensan	k1gMnPc2	mensan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
financování	financování	k1gNnSc6	financování
ostatních	ostatní	k2eAgFnPc2d1	ostatní
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nadaných	nadaný	k2eAgFnPc2d1	nadaná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
Mensa	mensa	k1gFnSc1	mensa
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnPc1d1	jiná
neziskové	ziskový	k2eNgFnPc1d1	nezisková
společnosti	společnost	k1gFnPc1	společnost
<g/>
)	)	kIx)	)
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
dotace	dotace	k1gFnPc4	dotace
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgInPc4d1	soukromý
dary	dar	k1gInPc4	dar
(	(	kIx(	(
<g/>
daňově	daňově	k6eAd1	daňově
odpočitatelné	odpočitatelný	k2eAgFnSc2d1	odpočitatelná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
výnosy	výnos	k1gInPc1	výnos
z	z	k7c2	z
reklam	reklama	k1gFnPc2	reklama
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
časopise	časopis	k1gInSc6	časopis
či	či	k8xC	či
z	z	k7c2	z
výnosů	výnos	k1gInPc2	výnos
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
komerčními	komerční	k2eAgFnPc7d1	komerční
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Mensa	mensa	k1gFnSc1	mensa
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
především	především	k9	především
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nadaných	nadaný	k2eAgFnPc2d1	nadaná
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
svých	svůj	k3xOyFgInPc2	svůj
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
mnohé	mnohý	k2eAgFnPc4d1	mnohá
aktivity	aktivita	k1gFnPc4	aktivita
širší	široký	k2eAgFnSc3d2	širší
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
její	její	k3xOp3gFnPc1	její
akce	akce	k1gFnPc1	akce
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgFnPc1d1	přístupná
i	i	k9	i
pro	pro	k7c4	pro
nečleny	nečlen	k1gMnPc4	nečlen
a	a	k8xC	a
valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
,	,	kIx,	,
článků	článek	k1gInPc2	článek
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
webu	web	k1gInSc2	web
a	a	k8xC	a
časopisu	časopis	k1gInSc2	časopis
Mensy	mensa	k1gFnSc2	mensa
přístupná	přístupný	k2eAgFnSc1d1	přístupná
komukoliv	kdokoliv	k3yInSc3	kdokoliv
<g/>
.	.	kIx.	.
</s>
