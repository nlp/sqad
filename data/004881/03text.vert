<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
rinotracheitida	rinotracheitida	k1gFnSc1	rinotracheitida
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
také	také	k9	také
infekční	infekční	k2eAgFnSc1d1	infekční
bovinní	bovinní	k2eAgFnSc1d1	bovinní
rinotracheitida	rinotracheitida	k1gFnSc1	rinotracheitida
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
IBR	IBR	kA	IBR
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
rhinotracheitis	rhinotracheitis	k1gFnSc1	rhinotracheitis
infectiosa	infectiosa	k1gFnSc1	infectiosa
bovum	bovum	k1gInSc1	bovum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
jiných	jiný	k2eAgMnPc2d1	jiný
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
turovitých	turovití	k1gMnPc2	turovití
a	a	k8xC	a
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
bovinní	bovinní	k2eAgInSc1d1	bovinní
herpes	herpes	k1gInSc1	herpes
virus	virus	k1gInSc1	virus
BHV-	BHV-	k1gFnSc1	BHV-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
respirační	respirační	k2eAgFnSc6d1	respirační
(	(	kIx(	(
<g/>
záněty	zánět	k1gInPc1	zánět
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
a	a	k8xC	a
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
formě	forma	k1gFnSc3	forma
genitální	genitální	k2eAgFnSc2d1	genitální
(	(	kIx(	(
<g/>
záněty	zánět	k1gInPc7	zánět
na	na	k7c6	na
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
orgánech	orgán	k1gInPc6	orgán
obou	dva	k4xCgInPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
latentní	latentní	k2eAgNnSc1d1	latentní
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
klinických	klinický	k2eAgInPc2d1	klinický
projevů	projev	k1gInPc2	projev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
enzooticky	enzooticky	k6eAd1	enzooticky
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
významně	významně	k6eAd1	významně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
produktivitu	produktivita	k1gFnSc4	produktivita
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
skotu	skot	k1gInSc2	skot
–	–	k?	–
snižuje	snižovat	k5eAaImIp3nS	snižovat
laktaci	laktace	k1gFnSc4	laktace
u	u	k7c2	u
krav	kráva	k1gFnPc2	kráva
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
hmotnostní	hmotnostní	k2eAgInPc4d1	hmotnostní
přírůstky	přírůstek	k1gInPc4	přírůstek
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
plodnost	plodnost	k1gFnSc1	plodnost
krav	kráva	k1gFnPc2	kráva
(	(	kIx(	(
<g/>
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
zabřezáváním	zabřezávání	k1gNnSc7	zabřezávání
<g/>
,	,	kIx,	,
potraty	potrat	k1gInPc1	potrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
povinný	povinný	k2eAgInSc1d1	povinný
národní	národní	k2eAgInSc1d1	národní
ozdravovací	ozdravovací	k2eAgInSc1d1	ozdravovací
program	program	k1gInSc1	program
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
eradikovat	eradikovat	k5eAaImF	eradikovat
IBR	IBR	kA	IBR
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
chovů	chov	k1gInPc2	chov
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Kovařík	Kovařík	k1gMnSc1	Kovařík
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Bažant	Bažant	k1gMnSc1	Bažant
J.	J.	kA	J.
Národní	národní	k2eAgInSc1d1	národní
ozdravovací	ozdravovací	k2eAgInSc1d1	ozdravovací
program	program	k1gInSc1	program
od	od	k7c2	od
infekční	infekční	k2eAgFnSc2d1	infekční
rinotracheitidy	rinotracheitis	k1gFnSc2	rinotracheitis
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
IBR	IBR	kA	IBR
<g/>
)	)	kIx)	)
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ŠTĚRBA	štěrba	k1gFnSc1	štěrba
O.	O.	kA	O.
Virové	virový	k2eAgFnSc2d1	virová
choroby	choroba	k1gFnSc2	choroba
spárkaté	spárkatý	k2eAgFnSc2d1	spárkatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
epizootologie	epizootologie	k1gFnSc2	epizootologie
<g/>
,	,	kIx,	,
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
</s>
