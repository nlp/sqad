<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
rinotracheitida	rinotracheitida	k1gFnSc1	rinotracheitida
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
také	také	k9	také
infekční	infekční	k2eAgFnSc1d1	infekční
bovinní	bovinní	k2eAgFnSc1d1	bovinní
rinotracheitida	rinotracheitida	k1gFnSc1	rinotracheitida
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
IBR	IBR	kA	IBR
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
rhinotracheitis	rhinotracheitis	k1gFnSc1	rhinotracheitis
infectiosa	infectiosa	k1gFnSc1	infectiosa
bovum	bovum	k1gInSc1	bovum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
jiných	jiný	k2eAgMnPc2d1	jiný
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
turovitých	turovití	k1gMnPc2	turovití
a	a	k8xC	a
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
.	.	kIx.	.
</s>
