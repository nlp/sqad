<s>
Neděle	neděle	k1gFnSc1	neděle
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
za	za	k7c4	za
sedmý	sedmý	k4xOgMnSc1	sedmý
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
)	)	kIx)	)
den	den	k1gInSc1	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
křesťanském	křesťanský	k2eAgMnSc6d1	křesťanský
a	a	k8xC	a
židovském	židovský	k2eAgInSc6d1	židovský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
den	den	k1gInSc4	den
první	první	k4xOgFnSc2	první
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
dne	den	k1gInSc2	den
neděle	neděle	k1gFnSc2	neděle
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
nedělati	dělat	k5eNaImF	dělat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
neděle	neděle	k1gFnSc1	neděle
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nepracuje	pracovat	k5eNaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kulturách	kultura	k1gFnPc6	kultura
vzešlých	vzešlý	k2eAgFnPc2d1	vzešlá
z	z	k7c2	z
křesťanství	křesťanství	k1gNnSc2	křesťanství
je	být	k5eAaImIp3nS	být
neděle	neděle	k1gFnSc2	neděle
svátečním	sváteční	k2eAgInSc7d1	sváteční
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
křesťanů	křesťan	k1gMnPc2	křesťan
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
po	po	k7c6	po
sobotě	sobota	k1gFnSc6	sobota
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připomíná	připomínat	k5eAaImIp3nS	připomínat
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
rovněž	rovněž	k9	rovněž
přikázání	přikázání	k1gNnSc4	přikázání
z	z	k7c2	z
desatera	desatero	k1gNnSc2	desatero
o	o	k7c4	o
svěcení	svěcení	k1gNnSc4	svěcení
dne	den	k1gInSc2	den
odpočinku	odpočinek	k1gInSc2	odpočinek
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
bylo	být	k5eAaImAgNnS	být
aplikováno	aplikovat	k5eAaBmNgNnS	aplikovat
na	na	k7c4	na
sobotu	sobota	k1gFnSc4	sobota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
význam	význam	k1gInSc1	význam
neděle	neděle	k1gFnSc2	neděle
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
např.	např.	kA	např.
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
názvu	název	k1gInSc6	název
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
в	в	k?	в
vaskresenie	vaskresenie	k1gFnSc1	vaskresenie
-	-	kIx~	-
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
přikázání	přikázání	k1gNnPc2	přikázání
o	o	k7c4	o
svěcení	svěcení	k1gNnSc4	svěcení
dne	den	k1gInSc2	den
odpočinku	odpočinek	k1gInSc2	odpočinek
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
neděle	neděle	k1gFnSc2	neděle
stal	stát	k5eAaPmAgInS	stát
den	den	k1gInSc4	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výnos	výnos	k1gInSc1	výnos
o	o	k7c4	o
zachovávání	zachovávání	k1gNnSc4	zachovávání
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Constantina	Constantin	k1gMnSc2	Constantin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
321	[number]	k4	321
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
výnosem	výnos	k1gInSc7	výnos
byla	být	k5eAaImAgFnS	být
neděle	neděle	k1gFnSc2	neděle
určena	určit	k5eAaPmNgFnS	určit
za	za	k7c4	za
den	den	k1gInSc4	den
úředního	úřední	k2eAgInSc2d1	úřední
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Povoleno	povolen	k2eAgNnSc1d1	povoleno
vykonávat	vykonávat	k5eAaImF	vykonávat
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
manumissiones	manumissiones	k1gMnSc1	manumissiones
<g/>
"	"	kIx"	"
–	–	k?	–
propouštění	propouštění	k1gNnSc2	propouštění
otroků	otrok	k1gMnPc2	otrok
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
neděle	neděle	k1gFnSc1	neděle
nijak	nijak	k6eAd1	nijak
neodlišovala	odlišovat	k5eNaImAgFnS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
dnů	den	k1gInPc2	den
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pro	pro	k7c4	pro
neděli	neděle	k1gFnSc4	neděle
odvozený	odvozený	k2eAgMnSc1d1	odvozený
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
(	(	kIx(	(
<g/>
dies	diesit	k5eAaPmRp2nS	diesit
<g/>
)	)	kIx)	)
dominica	dominica	k1gMnSc1	dominica
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
Páně	páně	k2eAgNnSc2d1	páně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
jakožto	jakožto	k8xS	jakožto
den	den	k1gInSc4	den
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
je	být	k5eAaImIp3nS	být
jemu	on	k3xPp3gMnSc3	on
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Pánův	pánův	k2eAgInSc4d1	pánův
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
italština	italština	k1gFnSc1	italština
(	(	kIx(	(
<g/>
Domenica	Domenica	k1gFnSc1	Domenica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Domingo	Domingo	k1gMnSc1	Domingo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
(	(	kIx(	(
<g/>
Dimanche	Dimanche	k1gInSc1	Dimanche
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
názvy	název	k1gInPc1	název
jako	jako	k8xC	jako
anglicky	anglicky	k6eAd1	anglicky
Sunday	Sunda	k2eAgFnPc1d1	Sunda
či	či	k8xC	či
německy	německy	k6eAd1	německy
Sonntag	Sonntaga	k1gFnPc2	Sonntaga
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
předkřesťanské	předkřesťanský	k2eAgFnSc2d1	předkřesťanská
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
(	(	kIx(	(
<g/>
Dies	Dies	k1gInSc1	Dies
solis	solis	k1gFnSc2	solis
<g/>
)	)	kIx)	)
-	-	kIx~	-
označují	označovat	k5eAaImIp3nP	označovat
neděli	neděle	k1gFnSc4	neděle
za	za	k7c4	za
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
Slunce	slunce	k1gNnSc2	slunce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
severštině	severština	k1gFnSc6	severština
je	být	k5eAaImIp3nS	být
neděle	neděle	k1gFnSc1	neděle
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
bohyni	bohyně	k1gFnSc4	bohyně
Sol	sol	k1gNnSc2	sol
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Neděle	neděle	k1gFnSc2	neděle
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
současně	současně	k6eAd1	současně
jméno	jméno	k1gNnSc1	jméno
protagonisty	protagonista	k1gMnSc2	protagonista
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
australský	australský	k2eAgMnSc1d1	australský
spisovatel	spisovatel	k1gMnSc1	spisovatel
žánru	žánr	k1gInSc2	žánr
sci-fi	scii	k1gFnSc2	sci-fi
Garth	Garth	k1gMnSc1	Garth
Nix	Nix	k1gMnSc1	Nix
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
neděle	neděle	k1gFnSc2	neděle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
neděle	neděle	k1gFnSc2	neděle
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
