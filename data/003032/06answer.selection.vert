<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
za	za	k7c4	za
sedmý	sedmý	k4xOgMnSc1	sedmý
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
)	)	kIx)	)
den	den	k1gInSc1	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
křesťanském	křesťanský	k2eAgMnSc6d1	křesťanský
a	a	k8xC	a
židovském	židovský	k2eAgInSc6d1	židovský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
den	den	k1gInSc4	den
první	první	k4xOgFnSc2	první
<g/>
.	.	kIx.	.
</s>
