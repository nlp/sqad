<s>
Hydrolasa	Hydrolasa	k1gFnSc1	Hydrolasa
nebo	nebo	k8xC	nebo
též	též	k9	též
hydroláza	hydroláza	k1gFnSc1	hydroláza
je	být	k5eAaImIp3nS	být
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
katalyzuje	katalyzovat	k5eAaBmIp3nS	katalyzovat
hydrolýzu	hydrolýza	k1gFnSc4	hydrolýza
–	–	k?	–
tedy	tedy	k8xC	tedy
rozkladnou	rozkladný	k2eAgFnSc4d1	rozkladná
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
činitelem	činitel	k1gMnSc7	činitel
rozkladu	rozklad	k1gInSc2	rozklad
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
reakci	reakce	k1gFnSc4	reakce
–	–	k?	–
tzv.	tzv.	kA	tzv.
hydrolytické	hydrolytický	k2eAgNnSc1d1	hydrolytické
štěpení	štěpení	k1gNnSc1	štěpení
vazeb	vazba	k1gFnPc2	vazba
–	–	k?	–
lze	lze	k6eAd1	lze
obecně	obecně	k6eAd1	obecně
popsat	popsat	k5eAaPmF	popsat
chemickou	chemický	k2eAgFnSc7d1	chemická
rovnicí	rovnice	k1gFnSc7	rovnice
A	a	k8xC	a
<g/>
–	–	k?	–
<g/>
B	B	kA	B
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
A	a	k8xC	a
<g/>
–	–	k?	–
<g/>
OH	OH	kA	OH
+	+	kIx~	+
B	B	kA	B
<g/>
–	–	k?	–
<g/>
H	H	kA	H
Původní	původní	k2eAgFnSc1d1	původní
látka	látka	k1gFnSc1	látka
A	a	k8xC	a
<g/>
–	–	k?	–
<g/>
B	B	kA	B
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vodou	voda	k1gFnSc7	voda
rozkládána	rozkládán	k2eAgNnPc4d1	rozkládáno
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
hydroláza	hydroláza	k1gFnSc1	hydroláza
(	(	kIx(	(
<g/>
v	v	k7c6	v
rovnici	rovnice	k1gFnSc6	rovnice
není	být	k5eNaImIp3nS	být
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
reakcí	reakce	k1gFnSc7	reakce
nemění	měnit	k5eNaImIp3nS	měnit
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydrolázy	Hydroláza	k1gFnPc1	Hydroláza
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
třídou	třída	k1gFnSc7	třída
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
amidáza	amidáza	k1gFnSc1	amidáza
amyláza	amyláza	k1gFnSc1	amyláza
karboxypeptidáza	karboxypeptidáza	k1gFnSc1	karboxypeptidáza
chymotrypsin	chymotrypsin	k1gInSc1	chymotrypsin
nukleáza	nukleáza	k1gFnSc1	nukleáza
<g/>
,	,	kIx,	,
např.	např.	kA	např.
deoxyribonukleáza	deoxyribonukleáza	k1gFnSc1	deoxyribonukleáza
(	(	kIx(	(
<g/>
DNáza	DNáza	k1gFnSc1	DNáza
<g/>
)	)	kIx)	)
esteráza	esteráza	k1gFnSc1	esteráza
glykozidáza	glykozidáza	k1gFnSc1	glykozidáza
hemicelluláza	hemicelluláza	k1gFnSc1	hemicelluláza
laktáza	laktáza	k1gFnSc1	laktáza
lipáza	lipáza	k1gFnSc1	lipáza
peptidáza	peptidáza	k1gFnSc1	peptidáza
trypsin	trypsin	k1gInSc4	trypsin
ureáza	ureáza	k1gFnSc1	ureáza
pepsin	pepsin	k1gInSc1	pepsin
GTPáza	GTPáza	k1gFnSc1	GTPáza
ATPáza	ATPáza	k1gFnSc1	ATPáza
Další	další	k2eAgFnSc2d1	další
typy	typa	k1gFnSc2	typa
enzymů	enzym	k1gInPc2	enzym
<g/>
:	:	kIx,	:
Izomeráza	Izomeráza	k1gFnSc1	Izomeráza
Ligáza	Ligáza	k1gFnSc1	Ligáza
Lyáza	Lyáza	k1gFnSc1	Lyáza
Oxidoreduktáza	Oxidoreduktáza	k1gFnSc1	Oxidoreduktáza
Transferáza	Transferáza	k1gFnSc1	Transferáza
</s>
