<s>
Tabríz	tabríz	k1gInSc1
(	(	kIx(
<g/>
koberec	koberec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tabríz	tabríz	k1gInSc1
je	být	k5eAaImIp3nS
ručně	ručně	k6eAd1
vázaný	vázaný	k2eAgInSc1d1
koberec	koberec	k1gInSc1
vyrobený	vyrobený	k2eAgInSc1d1
v	v	k7c6
íránském	íránský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Tabrízu	tabríz	k1gInSc2
a	a	k8xC
blízkém	blízký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Způsob	způsob	k1gInSc1
výroby	výroba	k1gFnSc2
</s>
<s>
Koberce	koberec	k1gInPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
vážou	vázat	k5eAaImIp3nP
z	z	k7c2
vlněné	vlněný	k2eAgFnSc2d1
nebo	nebo	k8xC
hedvábné	hedvábný	k2eAgFnSc2d1
příze	příz	k1gFnSc2
na	na	k7c6
bavlněné	bavlněný	k2eAgFnSc6d1
podkladové	podkladový	k2eAgFnSc6d1
tkanině	tkanina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzorování	vzorování	k1gNnPc4
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
rozmanité	rozmanitý	k2eAgFnSc2d1
–	–	k?
od	od	k7c2
jednoduchých	jednoduchý	k2eAgInPc2d1
figurálních	figurální	k2eAgInPc2d1
motivů	motiv	k1gInPc2
a	a	k8xC
medailonů	medailon	k1gInPc2
až	až	k9
po	po	k7c4
trojrozměrné	trojrozměrný	k2eAgInPc4d1
vzory	vzor	k1gInPc4
s	s	k7c7
velmi	velmi	k6eAd1
širokou	široký	k2eAgFnSc7d1
paletou	paleta	k1gFnSc7
barev	barva	k1gFnPc2
a	a	k8xC
odstínů	odstín	k1gInPc2
mědi	měď	k1gFnSc2
<g/>
,	,	kIx,
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
sloní	sloní	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
<g/>
,	,	kIx,
modré	modrý	k2eAgFnPc1d1
<g/>
,	,	kIx,
zelené	zelená	k1gFnPc1
aj.	aj.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koberce	koberec	k1gInPc1
jsou	být	k5eAaImIp3nP
vázané	vázaný	k2eAgFnPc1d1
tzv.	tzv.	kA
symetrickými	symetrický	k2eAgInPc7d1
uzly	uzel	k1gInPc7
v	v	k7c6
hustotě	hustota	k1gFnSc6
cca	cca	kA
200	#num#	k4
000	#num#	k4
až	až	k9
1	#num#	k4
milion	milion	k4xCgInSc1
na	na	k7c6
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hustota	hustota	k1gFnSc1
uzlů	uzel	k1gInPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
měří	měřit	k5eAaImIp3nS
jednotkou	jednotka	k1gFnSc7
Raj	Raj	k1gFnSc2
(	(	kIx(
<g/>
počet	počet	k1gInSc1
uzlů	uzel	k1gInPc2
na	na	k7c4
cca	cca	kA
7	#num#	k4
cm	cm	kA
šířky	šířka	k1gFnSc2
koberce	koberec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
označena	označit	k5eAaPmNgFnS
na	na	k7c6
rubu	rub	k1gInSc6
koberce	koberec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakost	jakost	k1gFnSc1
koberců	koberec	k1gInPc2
sahá	sahat	k5eAaImIp3nS
od	od	k7c2
podřadných	podřadný	k2eAgInPc2d1
bazarových	bazarový	k2eAgInPc2d1
až	až	k9
po	po	k7c4
výrobky	výrobek	k1gInPc4
vzácného	vzácný	k2eAgNnSc2d1
uměleckého	umělecký	k2eAgNnSc2d1
řemesla	řemeslo	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
koberec	koberec	k1gInSc4
400	#num#	k4
×	×	k?
500	#num#	k4
cm	cm	kA
s	s	k7c7
250	#num#	k4
000	#num#	k4
uzly	uzel	k1gInPc7
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
nabízený	nabízený	k2eAgInSc4d1
za	za	k7c4
42	#num#	k4
500	#num#	k4
€	€	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hodnota	hodnota	k1gFnSc1
tabrízských	tabrízský	k2eAgInPc2d1
koberců	koberec	k1gInPc2
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
vedle	vedle	k7c2
jemnosti	jemnost	k1gFnSc2
především	především	k9
na	na	k7c4
sestavení	sestavení	k1gNnSc4
barev	barva	k1gFnPc2
a	a	k8xC
způsobu	způsob	k1gInSc2
vzorování	vzorování	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
historie	historie	k1gFnSc2
tabrízských	tabrízský	k2eAgInPc2d1
koberců	koberec	k1gInPc2
</s>
<s>
Tabríz	tabríz	k1gInSc4
je	být	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejstarších	starý	k2eAgNnPc2d3
středisek	středisko	k1gNnPc2
výroby	výroba	k1gFnSc2
koberců	koberec	k1gInPc2
v	v	k7c6
Íránu	Írán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
mezi	mezi	k7c7
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
stoletím	století	k1gNnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
nejvýraznější	výrazný	k2eAgFnSc4d3
etapu	etapa	k1gFnSc4
v	v	k7c6
rozvoji	rozvoj	k1gInSc6
tohoto	tento	k3xDgNnSc2
řemesla	řemeslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jistém	jistý	k2eAgInSc6d1
úpadku	úpadek	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
renezanci	renezance	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
Tabrízu	tabríz	k1gInSc6
usadilo	usadit	k5eAaPmAgNnS
několik	několik	k4yIc1
nejznámějších	známý	k2eAgMnPc2d3
výrobců	výrobce	k1gMnPc2
koberců	koberec	k1gInPc2
a	a	k8xC
zvýšil	zvýšit	k5eAaPmAgInS
se	se	k3xPyFc4
prodej	prodej	k1gInSc1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
dekádě	dekáda	k1gFnSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zabývalo	zabývat	k5eAaImAgNnS
výrobou	výroba	k1gFnSc7
tabrízských	tabrízský	k2eAgInPc2d1
koberců	koberec	k1gInPc2
asi	asi	k9
120	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
regionů	region	k1gInPc2
vázali	vázat	k5eAaImAgMnP
zde	zde	k6eAd1
koberce	koberec	k1gInSc2
převážně	převážně	k6eAd1
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gFnPc2
pracovalo	pracovat	k5eAaImAgNnS
asi	asi	k9
30	#num#	k4
%	%	kIx~
v	v	k7c6
manufakturách	manufaktura	k1gFnPc6
<g/>
,	,	kIx,
40	#num#	k4
%	%	kIx~
jako	jako	k8xS,k8xC
domáčtí	domácký	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
ve	v	k7c6
městě	město	k1gNnSc6
a	a	k8xC
30	#num#	k4
%	%	kIx~
v	v	k7c6
okolních	okolní	k2eAgFnPc6d1
vesnicích	vesnice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
výrobků	výrobek	k1gInPc2
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
městské	městský	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
,	,	kIx,
jen	jen	k9
asi	asi	k9
1	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
z	z	k7c2
venkova	venkov	k1gInSc2
<g/>
,	,	kIx,
asi	asi	k9
40	#num#	k4
%	%	kIx~
šlo	jít	k5eAaImAgNnS
na	na	k7c4
export	export	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
za	za	k7c4
180	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
=	=	kIx~
35	#num#	k4
%	%	kIx~
celkového	celkový	k2eAgInSc2d1
vývozu	vývoz	k1gInSc2
íránských	íránský	k2eAgInPc2d1
koberců	koberec	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
tabrízských	tabrízský	k2eAgInPc2d1
koberců	koberec	k1gInPc2
</s>
<s>
Heyvanli	Heyvanl	k1gMnPc1
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
čtvrtiny	čtvrtina	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Tabríz	tabríz	k1gInSc1
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
čtvrtiny	čtvrtina	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Afšán	Afšán	k1gInSc1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Zili	Zili	k6eAd1
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Bachšaiš	Bachšaiš	k5eAaPmIp2nS
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Tabríz	tabríz	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
810	#num#	k4
000	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
vlas	vlas	k1gInSc1
vlna	vlna	k1gFnSc1
<g/>
/	/	kIx~
<g/>
hedvábí	hedvábí	k1gNnSc1
na	na	k7c6
hedvábném	hedvábný	k2eAgInSc6d1
podkladě	podklad	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
What	What	k1gInSc1
are	ar	k1gInSc5
Tabriz	Tabriz	k1gMnSc1
Rugs	Rugs	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fine	Fin	k1gMnSc5
Rug	Rug	k1gFnPc7
Collection	Collection	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tabriz	Tabriz	k1gInSc1
Rugs	Rugs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Catalina	Catalina	k1gFnSc1
Rug	Rug	k1gFnSc1
<g/>
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tabriz	Tabriz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CarpetVista	CarpetVista	k1gMnSc1
<g/>
,	,	kIx,
2005-2017	2005-2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Teppichlexikon	Teppichlexikon	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mesgarzadeh	Mesgarzadeha	k1gFnPc2
<g/>
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A	a	k8xC
Short	Short	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Tabriz	Tabriz	k1gInSc1
Rugs	Rugs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behnam	Behnam	k1gInSc1
Rugs	Rugs	k1gInSc4
<g/>
,	,	kIx,
2016-06-07	2016-06-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Persian	Persian	k1gInSc1
carpet	carpet	k1gMnSc1
manufacturing	manufacturing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Queensland	Queensland	k1gInSc1
University	universita	k1gFnSc2
of	of	k?
technology	technolog	k1gMnPc7
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
