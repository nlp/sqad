<s>
Umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
(	(	kIx(	(
<g/>
UI	UI	kA	UI
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Artificial	Artificial	k1gInSc1	Artificial
intelligence	intelligence	k1gFnSc2	intelligence
<g/>
,	,	kIx,	,
AI	AI	kA	AI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
informatiky	informatika	k1gFnSc2	informatika
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
tvorbou	tvorba	k1gFnSc7	tvorba
strojů	stroj	k1gInPc2	stroj
vykazujících	vykazující	k2eAgInPc2d1	vykazující
známky	známka	k1gFnPc4	známka
inteligentního	inteligentní	k2eAgNnSc2d1	inteligentní
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
inteligentní	inteligentní	k2eAgNnSc1d1	inteligentní
chování	chování	k1gNnSc1	chování
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jako	jako	k9	jako
etalon	etalon	k1gInSc1	etalon
inteligence	inteligence	k1gFnSc2	inteligence
užívá	užívat	k5eAaImIp3nS	užívat
lidský	lidský	k2eAgInSc4d1	lidský
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
poprvé	poprvé	k6eAd1	poprvé
přišel	přijít	k5eAaPmAgMnS	přijít
John	John	k1gMnSc1	John
McCarthy	McCartha	k1gFnSc2	McCartha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
odborný	odborný	k2eAgInSc4d1	odborný
a	a	k8xC	a
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
často	často	k6eAd1	často
nelze	lze	k6eNd1	lze
názorově	názorově	k6eAd1	názorově
spojit	spojit	k5eAaPmF	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
technických	technický	k2eAgInPc2d1	technický
problémů	problém	k1gInPc2	problém
<g/>
;	;	kIx,	;
některá	některý	k3yIgFnSc1	některý
podpole	podpole	k1gFnSc1	podpole
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
řešením	řešení	k1gNnSc7	řešení
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
zase	zase	k9	zase
například	například	k6eAd1	například
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
nástrojů	nástroj	k1gInPc2	nástroj
či	či	k8xC	či
dosažení	dosažení	k1gNnSc4	dosažení
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
možné	možný	k2eAgNnSc1d1	možné
sestrojit	sestrojit	k5eAaPmF	sestrojit
umělou	umělý	k2eAgFnSc4d1	umělá
inteligenci	inteligence	k1gFnSc4	inteligence
je	být	k5eAaImIp3nS	být
také	také	k9	také
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
,	,	kIx,	,
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
provádí	provádět	k5eAaImIp3nP	provádět
lidský	lidský	k2eAgInSc4d1	lidský
mozek	mozek	k1gInSc4	mozek
sám	sám	k3xTgMnSc1	sám
nebo	nebo	k8xC	nebo
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
evoluce	evoluce	k1gFnSc2	evoluce
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobnými	podobný	k2eAgNnPc7d1	podobné
dilematy	dilema	k1gNnPc7	dilema
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
filosofie	filosofie	k1gFnSc1	filosofie
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výzkumu	výzkum	k1gInSc2	výzkum
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
patří	patřit	k5eAaImIp3nS	patřit
uvažování	uvažování	k1gNnSc1	uvažování
<g/>
,	,	kIx,	,
znalosti	znalost	k1gFnPc1	znalost
<g/>
,	,	kIx,	,
plánování	plánování	k1gNnSc1	plánování
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
zpracovávání	zpracovávání	k1gNnSc1	zpracovávání
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vnímání	vnímání	k1gNnSc6	vnímání
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
či	či	k8xC	či
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
obecné	obecný	k2eAgFnSc2d1	obecná
inteligence	inteligence	k1gFnSc2	inteligence
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
psychosociálního	psychosociální	k2eAgNnSc2d1	psychosociální
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
ne-lidské	idský	k2eNgFnSc2d1	-lidský
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Turingův	Turingův	k2eAgInSc4d1	Turingův
test	test	k1gInSc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
srovnání	srovnání	k1gNnSc6	srovnání
spočívá	spočívat	k5eAaImIp3nS	spočívat
také	také	k9	také
myšlenka	myšlenka	k1gFnSc1	myšlenka
Turingova	Turingův	k2eAgInSc2d1	Turingův
testu	test	k1gInSc2	test
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
informatik	informatik	k1gMnSc1	informatik
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc4	Turing
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
"	"	kIx"	"
<g/>
Computing	Computing	k1gInSc1	Computing
machinery	machiner	k1gMnPc7	machiner
and	and	k?	and
intelligence	intelligenec	k1gInSc2	intelligenec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
můžeme	moct	k5eAaImIp1nP	moct
stroj	stroj	k1gInSc1	stroj
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
nerozeznáme	rozeznat	k5eNaPmIp1nP	rozeznat
<g/>
-li	i	k?	-li
jeho	jeho	k3xOp3gInSc4	jeho
lingvistický	lingvistický	k2eAgInSc4d1	lingvistický
výstup	výstup	k1gInSc4	výstup
od	od	k7c2	od
lingvistického	lingvistický	k2eAgInSc2d1	lingvistický
výstupu	výstup	k1gInSc2	výstup
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Argument	argument	k1gInSc1	argument
čínského	čínský	k2eAgInSc2d1	čínský
pokoje	pokoj	k1gInSc2	pokoj
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
protiargument	protiargument	k1gInSc4	protiargument
k	k	k7c3	k
Turingovu	Turingův	k2eAgInSc3d1	Turingův
testu	test	k1gInSc3	test
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
existovat	existovat	k5eAaImF	existovat
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
inteligentní	inteligentní	k2eAgNnSc1d1	inteligentní
chování	chování	k1gNnSc1	chování
simuloval	simulovat	k5eAaImAgInS	simulovat
připravenou	připravený	k2eAgFnSc7d1	připravená
sadou	sada	k1gFnSc7	sada
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
nad	nad	k7c7	nad
čímkoliv	číkoliv	k3xOyIgNnSc7	číkoliv
"	"	kIx"	"
<g/>
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
článku	článek	k1gInSc6	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc4	Turing
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
svůj	svůj	k3xOyFgInSc4	svůj
slavný	slavný	k2eAgInSc4d1	slavný
test	test	k1gInSc4	test
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
ho	on	k3xPp3gMnSc4	on
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
imitační	imitační	k2eAgFnSc1d1	imitační
hra	hra	k1gFnSc1	hra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
na	na	k7c6	na
přelomu	přelom	k1gInSc2	přelom
milénií	milénium	k1gNnPc2	milénium
<g/>
)	)	kIx)	)
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
počítače	počítač	k1gInPc4	počítač
paměťovou	paměťový	k2eAgFnSc4d1	paměťová
kapacitu	kapacita	k1gFnSc4	kapacita
109	[number]	k4	109
bitů	bit	k1gInPc2	bit
a	a	k8xC	a
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
imitační	imitační	k2eAgFnSc2d1	imitační
hry	hra	k1gFnSc2	hra
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
70	[number]	k4	70
<g/>
%	%	kIx~	%
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
správně	správně	k6eAd1	správně
pozná	poznat	k5eAaPmIp3nS	poznat
lidskou	lidský	k2eAgFnSc4d1	lidská
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
odhad	odhad	k1gInSc1	odhad
paměťových	paměťový	k2eAgFnPc2d1	paměťová
schopností	schopnost	k1gFnPc2	schopnost
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
neobyčejně	obyčejně	k6eNd1	obyčejně
přesným	přesný	k2eAgInSc7d1	přesný
<g/>
,	,	kIx,	,
neumí	umět	k5eNaImIp3nS	umět
dnešní	dnešní	k2eAgInPc4d1	dnešní
počítače	počítač	k1gInPc4	počítač
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
jazykem	jazyk	k1gInSc7	jazyk
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
asi	asi	k9	asi
Turing	Turing	k1gInSc4	Turing
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgNnSc1d1	počáteční
nadšení	nadšení	k1gNnSc1	nadšení
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
měnilo	měnit	k5eAaImAgNnS	měnit
v	v	k7c4	v
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
skepsi	skepse	k1gFnSc4	skepse
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
naprogramování	naprogramování	k1gNnSc1	naprogramování
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
lidem	člověk	k1gMnPc3	člověk
připadají	připadat	k5eAaImIp3nP	připadat
triviální	triviální	k2eAgInPc1d1	triviální
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rozpoznávání	rozpoznávání	k1gNnSc1	rozpoznávání
tvarů	tvar	k1gInPc2	tvar
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
než	než	k8xS	než
vytvořit	vytvořit	k5eAaPmF	vytvořit
stroje	stroj	k1gInPc4	stroj
řešící	řešící	k2eAgInPc4d1	řešící
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgInPc4d1	klasický
<g/>
"	"	kIx"	"
problémy	problém	k1gInPc4	problém
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
hra	hra	k1gFnSc1	hra
šachů	šach	k1gMnPc2	šach
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
daří	dařit	k5eAaImIp3nS	dařit
vytvářet	vytvářet	k5eAaImF	vytvářet
řešení	řešení	k1gNnSc4	řešení
v	v	k7c6	v
reálném	reálný	k2eAgNnSc6d1	reálné
či	či	k8xC	či
složitém	složitý	k2eAgNnSc6d1	složité
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
samořídící	samořídící	k2eAgNnSc1d1	samořídící
motorové	motorový	k2eAgNnSc1d1	motorové
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
odezírání	odezírání	k1gNnSc1	odezírání
řeči	řeč	k1gFnSc2	řeč
ze	z	k7c2	z
rtů	ret	k1gInPc2	ret
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
soudní	soudní	k2eAgNnSc4d1	soudní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
signifikantní	signifikantní	k2eAgInSc1d1	signifikantní
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
předmětu	předmět	k1gInSc6	předmět
automatického	automatický	k2eAgInSc2d1	automatický
překladu	překlad	k1gInSc2	překlad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
prošel	projít	k5eAaPmAgInS	projít
obdobím	období	k1gNnSc7	období
nekritického	kritický	k2eNgNnSc2d1	nekritické
nadšení	nadšení	k1gNnSc2	nadšení
i	i	k8xC	i
hluboké	hluboký	k2eAgFnSc2d1	hluboká
deziluze	deziluze	k1gFnSc2	deziluze
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
naděje	nadát	k5eAaBmIp3nS	nadát
vkládané	vkládaný	k2eAgNnSc1d1	vkládané
do	do	k7c2	do
umělých	umělý	k2eAgFnPc2d1	umělá
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
neúspěchů	neúspěch	k1gInPc2	neúspěch
vytvořit	vytvořit	k5eAaPmF	vytvořit
sítě	síť	k1gFnPc1	síť
řešící	řešící	k2eAgInPc4d1	řešící
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
úkoly	úkol	k1gInPc4	úkol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
neschopnost	neschopnost	k1gFnSc1	neschopnost
jednoho	jeden	k4xCgInSc2	jeden
perceptronu	perceptron	k1gInSc2	perceptron
simulovat	simulovat	k5eAaImF	simulovat
funkci	funkce	k1gFnSc4	funkce
XOR	XOR	kA	XOR
byla	být	k5eAaImAgFnS	být
mylně	mylně	k6eAd1	mylně
zobecněna	zobecnit	k5eAaPmNgFnS	zobecnit
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
neuronové	neuronový	k2eAgFnPc4d1	neuronová
sítě	síť	k1gFnPc4	síť
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
algoritmus	algoritmus	k1gInSc1	algoritmus
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
učení	učení	k1gNnSc4	učení
zbrzdil	zbrzdit	k5eAaPmAgInS	zbrzdit
vývoj	vývoj	k1gInSc1	vývoj
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
i	i	k9	i
dvouvrstvá	dvouvrstvý	k2eAgFnSc1d1	dvouvrstvá
síť	síť	k1gFnSc1	síť
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
aproximovat	aproximovat	k5eAaBmF	aproximovat
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UI	UI	kA	UI
můžeme	moct	k5eAaImIp1nP	moct
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
slabé	slabý	k2eAgNnSc4d1	slabé
a	a	k8xC	a
silné	silný	k2eAgNnSc4d1	silné
-	-	kIx~	-
ty	ten	k3xDgFnPc4	ten
slabé	slabý	k2eAgFnPc4d1	slabá
by	by	kYmCp3nP	by
sice	sice	k8xC	sice
měly	mít	k5eAaImAgFnP	mít
projít	projít	k5eAaPmF	projít
Turingovým	Turingový	k2eAgInSc7d1	Turingový
testem	test	k1gInSc7	test
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
"	"	kIx"	"
<g/>
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnPc1	slovo
smyslu	smysl	k1gInSc2	smysl
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
Argument	argument	k1gInSc4	argument
čínského	čínský	k2eAgInSc2d1	čínský
pokoje	pokoj	k1gInSc2	pokoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
silných	silný	k2eAgNnPc2d1	silné
UI	UI	kA	UI
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
toto	tento	k3xDgNnSc4	tento
dělení	dělení	k1gNnSc4	dělení
smysl	smysl	k1gInSc4	smysl
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
filosofických	filosofický	k2eAgFnPc2d1	filosofická
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
obecnou	obecná	k1gFnSc7	obecná
umělou	umělý	k2eAgFnSc7d1	umělá
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
nesmírně	smírně	k6eNd1	smírně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
během	během	k7c2	během
posledních	poslední	k2eAgInPc2d1	poslední
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
sadu	sada	k1gFnSc4	sada
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
dílčích	dílčí	k2eAgInPc2d1	dílčí
úspěchů	úspěch	k1gInPc2	úspěch
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgFnPc1d1	umělá
neuronové	neuronový	k2eAgFnPc1d1	neuronová
sítě	síť	k1gFnPc1	síť
v	v	k7c6	v
umělé	umělý	k2eAgFnSc6d1	umělá
inteligenci	inteligence	k1gFnSc6	inteligence
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
chování	chování	k1gNnSc2	chování
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
biologických	biologický	k2eAgFnPc2d1	biologická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
výpočetních	výpočetní	k2eAgInPc2d1	výpočetní
modelů	model	k1gInPc2	model
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
předávají	předávat	k5eAaImIp3nP	předávat
signály	signál	k1gInPc4	signál
a	a	k8xC	a
transformují	transformovat	k5eAaBmIp3nP	transformovat
je	on	k3xPp3gFnPc4	on
pomocí	pomocí	k7c2	pomocí
funkce	funkce	k1gFnSc2	funkce
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
"	"	kIx"	"
<g/>
neuronům	neuron	k1gInPc3	neuron
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Genetické	genetický	k2eAgNnSc1d1	genetické
programování	programování	k1gNnSc1	programování
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgNnSc1d1	genetické
programování	programování	k1gNnSc1	programování
striktně	striktně	k6eAd1	striktně
vzato	vzat	k2eAgNnSc1d1	vzato
není	být	k5eNaImIp3nS	být
prostředek	prostředek	k1gInSc4	prostředek
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecný	obecný	k2eAgInSc1d1	obecný
programátorský	programátorský	k2eAgInSc1d1	programátorský
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
namísto	namísto	k7c2	namísto
sepsání	sepsání	k1gNnSc2	sepsání
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
algoritmu	algoritmus	k1gInSc2	algoritmus
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
úkolu	úkol	k1gInSc2	úkol
hledá	hledat	k5eAaImIp3nS	hledat
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
evolučními	evoluční	k2eAgFnPc7d1	evoluční
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Expertní	expertní	k2eAgInSc4d1	expertní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Expertní	expertní	k2eAgInSc1d1	expertní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
poskytovat	poskytovat	k5eAaImF	poskytovat
expertní	expertní	k2eAgFnPc4d1	expertní
rady	rada	k1gFnPc4	rada
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nebo	nebo	k8xC	nebo
doporučit	doporučit	k5eAaPmF	doporučit
řešení	řešení	k1gNnSc4	řešení
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Expertní	expertní	k2eAgInPc1d1	expertní
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
nenumerické	numerický	k2eNgFnPc4d1	numerický
a	a	k8xC	a
neurčité	určitý	k2eNgFnPc4d1	neurčitá
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
řešit	řešit	k5eAaImF	řešit
tak	tak	k9	tak
úlohy	úloha	k1gFnPc1	úloha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
řešitelné	řešitelný	k2eAgFnPc4d1	řešitelná
tradičními	tradiční	k2eAgInPc7d1	tradiční
algoritmickými	algoritmický	k2eAgInPc7d1	algoritmický
postupy	postup	k1gInPc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prohledávání	prohledávání	k1gNnSc2	prohledávání
stavového	stavový	k2eAgInSc2d1	stavový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
algoritmů	algoritmus	k1gInPc2	algoritmus
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
klasických	klasický	k2eAgFnPc2d1	klasická
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
šachů	šach	k1gInPc2	šach
<g/>
,	,	kIx,	,
dámy	dáma	k1gFnSc2	dáma
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
účelné	účelný	k2eAgNnSc1d1	účelné
zadefinovat	zadefinovat	k5eAaImF	zadefinovat
si	se	k3xPyFc3	se
množinu	množina	k1gFnSc4	množina
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
dostat	dostat	k5eAaPmF	dostat
<g/>
,	,	kIx,	,
přípustné	přípustný	k2eAgInPc1d1	přípustný
tahy	tah	k1gInPc1	tah
neboli	neboli	k8xC	neboli
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
stavy	stav	k1gInPc7	stav
a	a	k8xC	a
počáteční	počáteční	k2eAgFnPc1d1	počáteční
a	a	k8xC	a
koncové	koncový	k2eAgFnPc1d1	koncová
pozice	pozice	k1gFnPc1	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Hledáme	hledat	k5eAaImIp1nP	hledat
pak	pak	k6eAd1	pak
cestu	cesta	k1gFnSc4	cesta
od	od	k7c2	od
počátečních	počáteční	k2eAgInPc2d1	počáteční
stavů	stav	k1gInPc2	stav
ke	k	k7c3	k
koncovým	koncový	k2eAgInPc3d1	koncový
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
znamenají	znamenat	k5eAaImIp3nP	znamenat
náš	náš	k3xOp1gInSc4	náš
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stavové	stavový	k2eAgFnPc1d1	stavová
prostory	prostora	k1gFnPc1	prostora
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
go	go	k?	go
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
volit	volit	k5eAaImF	volit
chytré	chytrý	k2eAgFnPc1d1	chytrá
metody	metoda	k1gFnPc1	metoda
ořezávání	ořezávání	k1gNnSc2	ořezávání
nevhodných	vhodný	k2eNgFnPc2d1	nevhodná
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
ohodnocování	ohodnocování	k1gNnPc2	ohodnocování
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Data	datum	k1gNnSc2	datum
mining	mining	k1gInSc1	mining
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc4d1	velký
soubory	soubor	k1gInPc4	soubor
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
uložené	uložený	k2eAgInPc4d1	uložený
v	v	k7c6	v
databázích	databáze	k1gFnPc6	databáze
<g/>
)	)	kIx)	)
o	o	k7c6	o
nějakém	nějaký	k3yIgInSc6	nějaký
systému	systém	k1gInSc6	systém
nejsou	být	k5eNaImIp3nP	být
použitelné	použitelný	k2eAgFnPc1d1	použitelná
a	a	k8xC	a
pochopitelné	pochopitelný	k2eAgFnPc1d1	pochopitelná
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
informace	informace	k1gFnPc1	informace
a	a	k8xC	a
vzory	vzor	k1gInPc1	vzor
chování	chování	k1gNnSc1	chování
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
dobývání	dobývání	k1gNnSc2	dobývání
znalostí	znalost	k1gFnPc2	znalost
převádí	převádět	k5eAaImIp3nS	převádět
data	datum	k1gNnPc4	datum
do	do	k7c2	do
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
a	a	k8xC	a
explicitní	explicitní	k2eAgFnSc2d1	explicitní
formy	forma	k1gFnSc2	forma
popisující	popisující	k2eAgInSc1d1	popisující
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
použitelná	použitelný	k2eAgFnSc1d1	použitelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
smyslu	smysl	k1gInSc6	smysl
nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c6	o
zpracování	zpracování	k1gNnSc6	zpracování
elementárních	elementární	k2eAgNnPc2d1	elementární
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
kategoriálních	kategoriální	k2eAgNnPc2d1	kategoriální
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
zpracování	zpracování	k1gNnSc4	zpracování
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
obrázků	obrázek	k1gInPc2	obrázek
(	(	kIx(	(
<g/>
Digitální	digitální	k2eAgNnSc1d1	digitální
zpracování	zpracování	k1gNnSc1	zpracování
obrazu	obraz	k1gInSc2	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
videa	video	k1gNnSc2	video
<g/>
,	,	kIx,	,
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zpracování	zpracování	k1gNnSc4	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
korpus	korpus	k1gInSc1	korpus
<g/>
)	)	kIx)	)
a	a	k8xC	a
bioinformatických	bioinformatický	k2eAgNnPc2d1	bioinformatický
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
bioinformatika	bioinformatika	k1gFnSc1	bioinformatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstupy	výstup	k1gInPc1	výstup
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
úlohy	úloha	k1gFnPc4	úloha
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
taky	taky	k6eAd1	taky
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yRnSc3	co
je	být	k5eAaImIp3nS	být
chceme	chtít	k5eAaImIp1nP	chtít
použít	použít	k5eAaPmF	použít
a	a	k8xC	a
co	co	k3yInSc1	co
(	(	kIx(	(
<g/>
a	a	k8xC	a
jak	jak	k6eAd1	jak
kvalitně	kvalitně	k6eAd1	kvalitně
<g/>
)	)	kIx)	)
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
vydolovat	vydolovat	k5eAaPmF	vydolovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Strojové	strojový	k2eAgNnSc4d1	strojové
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
hra	hra	k1gFnSc1	hra
šachy	šach	k1gInPc4	šach
byla	být	k5eAaImAgFnS	být
už	už	k9	už
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
informatiky	informatika	k1gFnSc2	informatika
předmětem	předmět	k1gInSc7	předmět
analýz	analýza	k1gFnPc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
porazil	porazit	k5eAaPmAgInS	porazit
systém	systém	k1gInSc1	systém
Deep	Deep	k1gMnSc1	Deep
Blue	Blue	k1gFnSc1	Blue
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
Garriho	Garri	k1gMnSc2	Garri
Kasparova	Kasparův	k2eAgMnSc2d1	Kasparův
<g/>
.	.	kIx.	.
</s>
<s>
Chinook	Chinook	k1gInSc1	Chinook
je	být	k5eAaImIp3nS	být
program	program	k1gInSc4	program
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
anglické	anglický	k2eAgFnSc2d1	anglická
dámy	dáma	k1gFnSc2	dáma
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
tvůrci	tvůrce	k1gMnPc7	tvůrce
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
prohrát	prohrát	k5eAaPmF	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
předtím	předtím	k6eAd1	předtím
pravidelně	pravidelně	k6eAd1	pravidelně
porážel	porážet	k5eAaImAgMnS	porážet
lidské	lidský	k2eAgMnPc4d1	lidský
oponenty	oponent	k1gMnPc4	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
výsledku	výsledek	k1gInSc2	výsledek
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
kombinací	kombinace	k1gFnSc7	kombinace
hrubé	hrubá	k1gFnSc2	hrubá
síly	síla	k1gFnSc2	síla
při	při	k7c6	při
prohledávání	prohledávání	k1gNnSc6	prohledávání
pozic	pozice	k1gFnPc2	pozice
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc7d1	dobrá
databází	databáze	k1gFnSc7	databáze
zahájení	zahájení	k1gNnSc2	zahájení
a	a	k8xC	a
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgInPc1d1	počítačový
programy	program	k1gInPc1	program
hrající	hrající	k2eAgInSc1d1	hrající
go	go	k?	go
si	se	k3xPyFc3	se
často	často	k6eAd1	často
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
nevedly	vést	k5eNaImAgInP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
zřejmě	zřejmě	k6eAd1	zřejmě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
goban	goban	k1gInSc1	goban
(	(	kIx(	(
<g/>
deska	deska	k1gFnSc1	deska
na	na	k7c4	na
go	go	k?	go
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
dalším	další	k2eAgInSc7d1	další
položeným	položený	k2eAgInSc7d1	položený
kamenem	kámen	k1gInSc7	kámen
stoupá	stoupat	k5eAaImIp3nS	stoupat
komplexita	komplexita	k1gFnSc1	komplexita
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
lidé	člověk	k1gMnPc1	člověk
šanci	šance	k1gFnSc4	šance
zvládnout	zvládnout	k5eAaPmF	zvládnout
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vrozené	vrozený	k2eAgFnSc3d1	vrozená
schopnosti	schopnost	k1gFnSc3	schopnost
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
programy	program	k1gInPc4	program
používající	používající	k2eAgInPc4d1	používající
jak	jak	k8xC	jak
řešení	řešení	k1gNnSc4	řešení
hrubou	hrubý	k2eAgFnSc7d1	hrubá
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
stromové	stromový	k2eAgNnSc1d1	stromové
prohledávání	prohledávání	k1gNnSc1	prohledávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
intuici	intuice	k1gFnSc4	intuice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
porážet	porážet	k5eAaImF	porážet
i	i	k9	i
mistry	mistr	k1gMnPc7	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
bojová	bojový	k2eAgFnSc1d1	bojová
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
ALPHA	ALPHA	kA	ALPHA
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vést	vést	k5eAaImF	vést
letecké	letecký	k2eAgInPc4d1	letecký
souboje	souboj	k1gInPc4	souboj
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
lidští	lidský	k2eAgMnPc1d1	lidský
piloti	pilot	k1gMnPc1	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Rozumné	rozumný	k2eAgInPc1d1	rozumný
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
vděčné	vděčný	k2eAgNnSc4d1	vděčné
téma	téma	k1gNnSc4	téma
pro	pro	k7c4	pro
spisovatele	spisovatel	k1gMnPc4	spisovatel
vědecké	vědecký	k2eAgFnSc2d1	vědecká
fikce	fikce	k1gFnSc2	fikce
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k6eAd1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
věnoval	věnovat	k5eAaPmAgInS	věnovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
povídkové	povídkový	k2eAgFnSc2d1	povídková
tvorby	tvorba	k1gFnSc2	tvorba
tématům	téma	k1gNnPc3	téma
robotické	robotický	k2eAgFnSc2d1	robotická
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
povídková	povídkový	k2eAgFnSc1d1	povídková
sbírka	sbírka	k1gFnSc1	sbírka
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
povídka	povídka	k1gFnSc1	povídka
Dvěstěletý	dvěstěletý	k2eAgMnSc1d1	dvěstěletý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
autor	autor	k1gMnSc1	autor
Stanisław	Stanisław	k1gMnSc1	Stanisław
Lem	lem	k1gInSc4	lem
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
filosofickými	filosofický	k2eAgMnPc7d1	filosofický
aspekty	aspekt	k1gInPc1	aspekt
inteligence	inteligence	k1gFnSc2	inteligence
u	u	k7c2	u
nelidí	nečlověk	k1gMnPc2	nečlověk
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
Kyberiáda	Kyberiáda	k1gFnSc1	Kyberiáda
a	a	k8xC	a
Solaris	Solaris	k1gFnSc1	Solaris
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
aspekty	aspekt	k1gInPc1	aspekt
strojové	strojový	k2eAgFnSc2d1	strojová
inteligence	inteligence	k1gFnSc2	inteligence
rozebral	rozebrat	k5eAaPmAgInS	rozebrat
i	i	k9	i
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Golem	Golem	k1gMnSc1	Golem
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
publikací	publikace	k1gFnPc2	publikace
současného	současný	k2eAgInSc2d1	současný
stylu	styl	k1gInSc2	styl
scifi	scifi	k1gFnSc2	scifi
kyberpunku	kyberpunk	k1gInSc2	kyberpunk
se	se	k3xPyFc4	se
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
váže	vázat	k5eAaImIp3nS	vázat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
k	k	k7c3	k
pronikání	pronikání	k1gNnSc3	pronikání
vlastností	vlastnost	k1gFnPc2	vlastnost
lidského	lidský	k2eAgNnSc2d1	lidské
a	a	k8xC	a
strojového	strojový	k2eAgNnSc2d1	strojové
<g/>
,	,	kIx,	,
tak	tak	k9	tak
k	k	k7c3	k
vyrovnávání	vyrovnávání	k1gNnSc3	vyrovnávání
se	se	k3xPyFc4	se
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
inteligentního	inteligentní	k2eAgInSc2d1	inteligentní
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc4	příklad
uveďme	uvést	k5eAaPmRp1nP	uvést
knihu	kniha	k1gFnSc4	kniha
Neuromancer	Neuromancra	k1gFnPc2	Neuromancra
Williama	William	k1gMnSc4	William
Gibsona	Gibson	k1gMnSc4	Gibson
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgNnSc1d1	filmové
publikum	publikum	k1gNnSc1	publikum
středního	střední	k2eAgInSc2d1	střední
proudu	proud	k1gInSc2	proud
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
nejvíce	hodně	k6eAd3	hodně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
trilogie	trilogie	k1gFnSc1	trilogie
Matrix	Matrix	k1gInSc1	Matrix
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
světě	svět	k1gInSc6	svět
ovládaném	ovládaný	k2eAgInSc6d1	ovládaný
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
původně	původně	k6eAd1	původně
vytvořenou	vytvořený	k2eAgFnSc7d1	vytvořená
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vlivná	vlivný	k2eAgNnPc4d1	vlivné
starší	starý	k2eAgNnPc4d2	starší
díla	dílo	k1gNnPc4	dílo
řadíme	řadit	k5eAaImIp1nP	řadit
filmy	film	k1gInPc4	film
Terminátor	terminátor	k1gInSc4	terminátor
nebo	nebo	k8xC	nebo
Blade	Blad	k1gInSc5	Blad
Runner	Runnra	k1gFnPc2	Runnra
<g/>
.	.	kIx.	.
</s>
