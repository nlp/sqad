<s>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
letopočtu	letopočet	k1gInSc2	letopočet
chovali	chovat	k5eAaImAgMnP	chovat
divoké	divoký	k2eAgMnPc4d1	divoký
králíky	králík	k1gMnPc4	králík
v	v	k7c6	v
oborách	obora	k1gFnPc6	obora
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
leporáriích	leporárium	k1gNnPc6	leporárium
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
dosud	dosud	k6eAd1	dosud
nenarozených	narozený	k2eNgFnPc2d1	nenarozená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
pochoutku	pochoutka	k1gFnSc4	pochoutka
<g/>
.	.	kIx.	.
</s>
