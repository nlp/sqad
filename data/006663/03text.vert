<s>
Králík	Králík	k1gMnSc1	Králík
domácí	domácí	k1gMnSc1	domácí
(	(	kIx(	(
<g/>
Oryctolagus	Oryctolagus	k1gMnSc1	Oryctolagus
cuniculus	cuniculus	k1gMnSc1	cuniculus
f.	f.	k?	f.
domesticus	domesticus	k1gMnSc1	domesticus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
evropského	evropský	k2eAgMnSc2d1	evropský
králíka	králík	k1gMnSc2	králík
divokého	divoký	k2eAgMnSc2d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Králíci	Králík	k1gMnPc1	Králík
jsou	být	k5eAaImIp3nP	být
domácím	domácí	k2eAgNnSc7d1	domácí
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
chovat	chovat	k5eAaImF	chovat
v	v	k7c6	v
malochovech	malochov	k1gInPc6	malochov
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgNnSc4d1	bílé
králičí	králičí	k2eAgNnSc4d1	králičí
maso	maso	k1gNnSc4	maso
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
domácími	domácí	k1gMnPc7	domácí
zvířaty	zvíře	k1gNnPc7	zvíře
nejméně	málo	k6eAd3	málo
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Zakrslá	zakrslý	k2eAgNnPc1d1	zakrslé
plemena	plemeno	k1gNnPc1	plemeno
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
zvířetem	zvíře	k1gNnSc7	zvíře
chovaným	chovaný	k2eAgNnSc7d1	chované
jako	jako	k8xC	jako
společníci	společník	k1gMnPc1	společník
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Divocí	divoký	k2eAgMnPc1d1	divoký
králíci	králík	k1gMnPc1	králík
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
už	už	k6eAd1	už
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
letopočtu	letopočet	k1gInSc2	letopočet
chovali	chovat	k5eAaImAgMnP	chovat
divoké	divoký	k2eAgMnPc4d1	divoký
králíky	králík	k1gMnPc4	králík
v	v	k7c6	v
oborách	obora	k1gFnPc6	obora
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
leporáriích	leporárium	k1gNnPc6	leporárium
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
dosud	dosud	k6eAd1	dosud
nenarozených	narozený	k2eNgFnPc2d1	nenarozená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
pochoutku	pochoutka	k1gFnSc4	pochoutka
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
také	také	k6eAd1	také
králíky	králík	k1gMnPc7	králík
vysadili	vysadit	k5eAaPmAgMnP	vysadit
na	na	k7c6	na
Baleárských	baleárský	k2eAgInPc6d1	baleárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přemnožili	přemnožit	k5eAaPmAgMnP	přemnožit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
likvidaci	likvidace	k1gFnSc3	likvidace
nasazena	nasazen	k2eAgFnSc1d1	nasazena
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
chovali	chovat	k5eAaImAgMnP	chovat
králíky	králík	k1gMnPc7	králík
především	především	k6eAd1	především
mniši	mnich	k1gMnPc1	mnich
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
neosrstěných	osrstěný	k2eNgNnPc2d1	osrstěný
mláďat	mládě	k1gNnPc2	mládě
bylo	být	k5eAaImAgNnS	být
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
postní	postní	k2eAgInSc4d1	postní
pokrm	pokrm	k1gInSc4	pokrm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
sám	sám	k3xTgMnSc1	sám
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byli	být	k5eAaImAgMnP	být
králíci	králík	k1gMnPc1	králík
chováni	chován	k2eAgMnPc1d1	chován
ve	v	k7c6	v
stájích	stáj	k1gFnPc6	stáj
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uklízeli	uklízet	k5eAaImAgMnP	uklízet
zbytky	zbytek	k1gInPc4	zbytek
potravy	potrava	k1gFnSc2	potrava
po	po	k7c6	po
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
známe	znát	k5eAaImIp1nP	znát
první	první	k4xOgFnPc4	první
bílé	bílý	k2eAgFnPc4d1	bílá
králíky	králík	k1gMnPc7	králík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
barevná	barevný	k2eAgNnPc4d1	barevné
a	a	k8xC	a
masná	masný	k2eAgNnPc4d1	masné
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
ke	k	k7c3	k
skutečnému	skutečný	k2eAgInSc3d1	skutečný
rozvoji	rozvoj	k1gInSc3	rozvoj
chovatelství	chovatelství	k1gNnSc2	chovatelství
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
chov	chov	k1gInSc1	chov
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
králíky	králík	k1gMnPc4	králík
chované	chovaný	k2eAgFnSc2d1	chovaná
pro	pro	k7c4	pro
kožešinu	kožešina	k1gFnSc4	kožešina
<g/>
,	,	kIx,	,
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
středem	střed	k1gInSc7	střed
pozornosti	pozornost	k1gFnSc2	pozornost
stala	stát	k5eAaPmAgFnS	stát
masná	masný	k2eAgFnSc1d1	Masná
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
chovu	chov	k1gInSc2	chov
byla	být	k5eAaImAgFnS	být
tradičně	tradičně	k6eAd1	tradičně
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
dnenší	dnenší	k2eAgFnSc2d1	dnenší
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
či	či	k8xC	či
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
chov	chov	k1gInSc1	chov
králíků	králík	k1gMnPc2	králík
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
až	až	k9	až
po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
chov	chov	k1gInSc1	chov
králíků	králík	k1gMnPc2	králík
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
až	až	k9	až
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
asi	asi	k9	asi
100	[number]	k4	100
plemen	plemeno	k1gNnPc2	plemeno
králíků	králík	k1gMnPc2	králík
(	(	kIx(	(
<g/>
65	[number]	k4	65
základních	základní	k2eAgMnPc2d1	základní
<g/>
,	,	kIx,	,
35	[number]	k4	35
jsou	být	k5eAaImIp3nP	být
rexové	rexová	k1gFnPc1	rexová
a	a	k8xC	a
zakrslá	zakrslý	k2eAgNnPc1d1	zakrslé
plemena	plemeno	k1gNnPc1	plemeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
má	mít	k5eAaImIp3nS	mít
králík	králík	k1gMnSc1	králík
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
?	?	kIx.	?
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
spolu	spolu	k6eAd1	spolu
obě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
souvisejí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
králík	králík	k1gMnSc1	králík
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
původním	původní	k2eAgNnSc7d1	původní
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
naši	náš	k3xOp1gMnPc1	náš
předkové	předek	k1gMnPc1	předek
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
přivezeni	přivézt	k5eAaPmNgMnP	přivézt
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
první	první	k4xOgMnPc1	první
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
i	i	k9	i
jejich	jejich	k3xOp3gNnSc4	jejich
německé	německý	k2eAgNnSc4d1	německé
jméno	jméno	k1gNnSc4	jméno
das	das	k?	das
Kaninchen	Kaninchna	k1gFnPc2	Kaninchna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
králíka	králík	k1gMnSc2	králík
cuniculus	cuniculus	k1gInSc4	cuniculus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
fonetické	fonetický	k2eAgFnSc2d1	fonetická
podobnosti	podobnost	k1gFnSc2	podobnost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
Kaninchen	Kaninchna	k1gFnPc2	Kaninchna
za	za	k7c4	za
Königchen	Königchen	k1gInSc4	Königchen
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
z	z	k7c2	z
der	drát	k5eAaImRp2nS	drát
König	König	k1gMnSc1	König
-	-	kIx~	-
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
a	a	k8xC	a
české	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
králík	králík	k1gMnSc1	králík
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
králíka	králík	k1gMnSc2	králík
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
slovem	slovo	k1gNnSc7	slovo
ramlice	ramlice	k?	ramlice
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slovesa	sloveso	k1gNnSc2	sloveso
rammeln	rammelna	k1gFnPc2	rammelna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
hovorové	hovorový	k2eAgNnSc1d1	hovorové
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
vulgární	vulgární	k2eAgNnSc4d1	vulgární
slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
páření	páření	k1gNnSc4	páření
(	(	kIx(	(
<g/>
něco	něco	k3yInSc1	něco
jako	jako	k8xS	jako
české	český	k2eAgInPc1d1	český
"	"	kIx"	"
<g/>
šoustat	šoustat	k5eAaImF	šoustat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
králík	králík	k1gMnSc1	králík
domácí	domácí	k1gMnSc1	domácí
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
typech	typ	k1gInPc6	typ
chovů	chov	k1gInPc2	chov
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
účelu	účel	k1gInSc6	účel
jejich	jejich	k3xOp3gInSc2	jejich
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
komerční	komerční	k2eAgFnSc4d1	komerční
produkci	produkce	k1gFnSc4	produkce
masa	maso	k1gNnSc2	maso
slouží	sloužit	k5eAaImIp3nP	sloužit
velkochovy	velkochov	k1gInPc1	velkochov
masných	masný	k2eAgMnPc2d1	masný
brojlerů	brojler	k1gMnPc2	brojler
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
chovají	chovat	k5eAaImIp3nP	chovat
králíci	králík	k1gMnPc1	králík
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
produkce	produkce	k1gFnSc2	produkce
kožešiny	kožešina	k1gFnSc2	kožešina
nebo	nebo	k8xC	nebo
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Králík	Králík	k1gMnSc1	Králík
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
zvíře	zvíře	k1gNnSc4	zvíře
a	a	k8xC	a
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
akreditovaných	akreditovaný	k2eAgInPc6d1	akreditovaný
laboratorních	laboratorní	k2eAgInPc6d1	laboratorní
chovech	chov	k1gInPc6	chov
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
typem	typ	k1gInSc7	typ
chovu	chov	k1gInSc2	chov
jsou	být	k5eAaImIp3nP	být
drobnochovy	drobnochův	k2eAgFnPc1d1	drobnochův
králíků	králík	k1gMnPc2	králík
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
hobby	hobby	k1gNnSc4	hobby
<g/>
,	,	kIx,	,
a	a	k8xC	a
individuální	individuální	k2eAgInPc1d1	individuální
chovy	chov	k1gInPc1	chov
zakrslých	zakrslý	k2eAgMnPc2d1	zakrslý
králíků	králík	k1gMnPc2	králík
chovaných	chovaný	k2eAgMnPc2d1	chovaný
ze	z	k7c2	z
záliby	záliba	k1gFnSc2	záliba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
typ	typ	k1gInSc1	typ
chovů	chov	k1gInPc2	chov
má	mít	k5eAaImIp3nS	mít
svá	svůj	k3xOyFgNnPc4	svůj
specifika	specifikon	k1gNnPc4	specifikon
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
jsou	být	k5eAaImIp3nP	být
chovaní	chovaný	k2eAgMnPc1d1	chovaný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
krmeni	krmen	k2eAgMnPc1d1	krmen
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
králíků	králík	k1gMnPc2	králík
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
v	v	k7c4	v
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
kusech	kus	k1gInPc6	kus
<g/>
)	)	kIx)	)
Králík	Králík	k1gMnSc1	Králík
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
divoký	divoký	k2eAgMnSc1d1	divoký
předek	předek	k1gMnSc1	předek
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívá	dospívat	k5eAaImIp3nS	dospívat
už	už	k6eAd1	už
ve	v	k7c6	v
3	[number]	k4	3
nebo	nebo	k8xC	nebo
4	[number]	k4	4
měsících	měsíc	k1gInPc6	měsíc
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
chovatelská	chovatelský	k2eAgFnSc1d1	chovatelská
dospělost	dospělost	k1gFnSc1	dospělost
nastává	nastávat	k5eAaImIp3nS	nastávat
až	až	k9	až
v	v	k7c6	v
osmém	osmý	k4xOgInSc6	osmý
měsíci	měsíc	k1gInSc6	měsíc
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Ramlice	Ramlice	k?	Ramlice
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
králíka	králík	k1gMnSc2	králík
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nepravidelnou	pravidelný	k2eNgFnSc4d1	nepravidelná
říji	říje	k1gFnSc4	říje
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
vždy	vždy	k6eAd1	vždy
asi	asi	k9	asi
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Králík	Králík	k1gMnSc1	Králík
má	mít	k5eAaImIp3nS	mít
tzv.	tzv.	kA	tzv.
provokovanou	provokovaný	k2eAgFnSc4d1	provokovaná
ovulaci	ovulace	k1gFnSc4	ovulace
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
vyplavení	vyplavení	k1gNnSc3	vyplavení
vajíček	vajíčko	k1gNnPc2	vajíčko
dojde	dojít	k5eAaPmIp3nS	dojít
až	až	k9	až
při	při	k7c6	při
dráždění	dráždění	k1gNnSc6	dráždění
pochvy	pochva	k1gFnSc2	pochva
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
šanci	šance	k1gFnSc4	šance
vajíček	vajíčko	k1gNnPc2	vajíčko
na	na	k7c6	na
oplození	oplození	k1gNnSc6	oplození
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
pak	pak	k6eAd1	pak
trvá	trvat	k5eAaImIp3nS	trvat
28	[number]	k4	28
až	až	k9	až
35	[number]	k4	35
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
králíčků	králíček	k1gMnPc2	králíček
tím	ten	k3xDgNnSc7	ten
déle	dlouho	k6eAd2	dlouho
trvá	trvat	k5eAaImIp3nS	trvat
březost	březost	k1gFnSc1	březost
<g/>
.	.	kIx.	.
</s>
<s>
Březí	březí	k1gNnSc1	březí
samice	samice	k1gFnSc1	samice
si	se	k3xPyFc3	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
hnízdo	hnízdo	k1gNnSc4	hnízdo
pro	pro	k7c4	pro
vrh	vrh	k1gInSc4	vrh
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
u	u	k7c2	u
králic	králice	k1gFnPc2	králice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sice	sice	k8xC	sice
byly	být	k5eAaImAgInP	být
připuštěny	připuštěn	k2eAgInPc1d1	připuštěn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezabřezly	zabřeznout	k5eNaPmAgFnP	zabřeznout
–	–	k?	–
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
březost	březost	k1gFnSc4	březost
falešnou	falešný	k2eAgFnSc4d1	falešná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
bývá	bývat	k5eAaImIp3nS	bývat
6	[number]	k4	6
až	až	k9	až
12	[number]	k4	12
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
plemeni	plemeno	k1gNnSc6	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Porodní	porodní	k2eAgFnSc1d1	porodní
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
60	[number]	k4	60
g	g	kA	g
<g/>
,	,	kIx,	,
králíčata	králíče	k1gNnPc1	králíče
jsou	být	k5eAaImIp3nP	být
holá	holý	k2eAgNnPc1d1	holé
<g/>
,	,	kIx,	,
slepá	slepý	k2eAgNnPc1d1	slepé
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
<g/>
.	.	kIx.	.
</s>
<s>
Králice	králice	k1gFnSc1	králice
kojí	kojit	k5eAaImIp3nS	kojit
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
vydatné	vydatný	k2eAgFnPc4d1	vydatná
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
králíci	králík	k1gMnPc1	králík
otvírají	otvírat	k5eAaImIp3nP	otvírat
oči	oko	k1gNnPc4	oko
9	[number]	k4	9
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
a	a	k8xC	a
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Odstavují	odstavovat	k5eAaImIp3nP	odstavovat
se	se	k3xPyFc4	se
do	do	k7c2	do
54	[number]	k4	54
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1	zajímavost
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
samice	samice	k1gFnSc1	samice
příliš	příliš	k6eAd1	příliš
ztloustne	ztloustnout	k5eAaPmIp3nS	ztloustnout
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
přechodně	přechodně	k6eAd1	přechodně
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
mít	mít	k5eAaImF	mít
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Zhubnutí	zhubnutí	k1gNnSc1	zhubnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
možnosti	možnost	k1gFnSc2	možnost
březosti	březost	k1gFnSc2	březost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tlustá	tlustý	k2eAgFnSc1d1	tlustá
samice	samice	k1gFnSc1	samice
přejde	přejít	k5eAaPmIp3nS	přejít
na	na	k7c4	na
dietu	dieta	k1gFnSc4	dieta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
drobnochovech	drobnochov	k1gInPc6	drobnochov
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
samici	samice	k1gFnSc3	samice
dává	dávat	k5eAaImIp3nS	dávat
jen	jen	k9	jen
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
seno	seno	k1gNnSc1	seno
<g/>
.	.	kIx.	.
</s>
<s>
Trávící	trávící	k2eAgFnSc1d1	trávící
soustava	soustava	k1gFnSc1	soustava
králíků	králík	k1gMnPc2	králík
je	být	k5eAaImIp3nS	být
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
potrava	potrava	k1gFnSc1	potrava
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Králík	Králík	k1gMnSc1	Králík
je	být	k5eAaImIp3nS	být
nepřežvýkavý	přežvýkavý	k2eNgMnSc1d1	nepřežvýkavý
býložravec	býložravec	k1gMnSc1	býložravec
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
trávení	trávení	k1gNnSc1	trávení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
trávení	trávení	k1gNnSc1	trávení
koně	kůň	k1gMnSc2	kůň
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
on	on	k3xPp3gMnSc1	on
využívá	využívat	k5eAaPmIp3nS	využívat
velkého	velký	k2eAgNnSc2d1	velké
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
plného	plný	k2eAgNnSc2d1	plné
symbiotických	symbiotický	k2eAgNnPc2d1	symbiotické
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
k	k	k7c3	k
fermentaci	fermentace	k1gFnSc3	fermentace
vlákniny	vláknina	k1gFnSc2	vláknina
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
tak	tak	k6eAd1	tak
živiny	živina	k1gFnPc4	živina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
byly	být	k5eAaImAgFnP	být
nedostupné	dostupný	k2eNgFnPc1d1	nedostupná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
získávání	získávání	k1gNnSc1	získávání
živin	živina	k1gFnPc2	živina
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
a	a	k8xC	a
králík	králík	k1gMnSc1	králík
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
využil	využít	k5eAaPmAgMnS	využít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
požírá	požírat	k5eAaImIp3nS	požírat
měkké	měkký	k2eAgInPc4d1	měkký
bobky	bobek	k1gInPc4	bobek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
i	i	k8xC	i
vitamíny	vitamín	k1gInPc4	vitamín
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
chování	chování	k1gNnSc4	chování
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
normální	normální	k2eAgMnSc1d1	normální
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
krmné	krmný	k2eAgFnSc2d1	krmná
dávky	dávka	k1gFnSc2	dávka
králíka	králík	k1gMnSc2	králík
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgNnP	mít
tvořit	tvořit	k5eAaImF	tvořit
objemná	objemný	k2eAgNnPc1d1	objemné
krmiva	krmivo	k1gNnPc1	krmivo
<g/>
,	,	kIx,	,
v	v	k7c6	v
drobnochovech	drobnochov	k1gInPc6	drobnochov
tedy	tedy	k8xC	tedy
seno	seno	k1gNnSc1	seno
a	a	k8xC	a
sláma	sláma	k1gFnSc1	sláma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
krmit	krmit	k5eAaImF	krmit
také	také	k9	také
zelená	zelená	k1gFnSc1	zelená
krmiva	krmivo	k1gNnSc2	krmivo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bazalka	bazalka	k1gFnSc1	bazalka
<g/>
,	,	kIx,	,
brokolice	brokolice	k1gFnSc1	brokolice
<g/>
,	,	kIx,	,
celer	celer	k1gInSc1	celer
<g/>
,	,	kIx,	,
čekanka	čekanka	k1gFnSc1	čekanka
<g/>
,	,	kIx,	,
jabloňové	jabloňový	k2eAgNnSc1d1	jabloňové
listí	listí	k1gNnSc1	listí
<g/>
,	,	kIx,	,
jitrocel	jitrocel	k1gInSc1	jitrocel
<g/>
,	,	kIx,	,
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
kokoška	kokoška	k1gFnSc1	kokoška
pastuší	pastuší	k2eAgFnSc1d1	pastuší
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
,	,	kIx,	,
kopřiva	kopřiva	k1gFnSc1	kopřiva
(	(	kIx(	(
<g/>
zavadlá	zavadlý	k2eAgFnSc1d1	zavadlá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pampeliška	pampeliška	k1gFnSc1	pampeliška
<g/>
,	,	kIx,	,
petržel	petržel	k1gFnSc1	petržel
<g/>
,	,	kIx,	,
salát	salát	k1gInSc1	salát
<g/>
,	,	kIx,	,
nejedovaté	jedovatý	k2eNgInPc1d1	nejedovatý
plevele	plevel	k1gInPc1	plevel
-	-	kIx~	-
pýr	pýr	k1gInSc1	pýr
<g/>
,	,	kIx,	,
lebeda	lebeda	k1gFnSc1	lebeda
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Vhodné	vhodný	k2eAgInPc1d1	vhodný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
okopaniny	okopanina	k1gFnPc1	okopanina
<g/>
,	,	kIx,	,
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
krmná	krmný	k2eAgFnSc1d1	krmná
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
tuřín	tuřín	k1gInSc1	tuřín
<g/>
,	,	kIx,	,
topinambury	topinambur	k1gInPc1	topinambur
nebo	nebo	k8xC	nebo
vařené	vařený	k2eAgInPc1d1	vařený
brambory	brambor	k1gInPc1	brambor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jadrných	jadrný	k2eAgNnPc2d1	jadrné
krmiv	krmivo	k1gNnPc2	krmivo
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
oves	oves	k1gInSc4	oves
nebo	nebo	k8xC	nebo
ječmen	ječmen	k1gInSc4	ječmen
<g/>
,	,	kIx,	,
možno	možno	k6eAd1	možno
zkrmovat	zkrmovat	k5eAaImF	zkrmovat
také	také	k9	také
hrách	hrách	k1gInSc4	hrách
nebo	nebo	k8xC	nebo
sóju	sója	k1gFnSc4	sója
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lněné	lněný	k2eAgNnSc1d1	lněné
semínko	semínko	k1gNnSc1	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Králík	Králík	k1gMnSc1	Králík
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
vždy	vždy	k6eAd1	vždy
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ČR	ČR	kA	ČR
králíci	králík	k1gMnPc1	králík
chovaní	chovaný	k2eAgMnPc1d1	chovaný
hlavně	hlavně	k6eAd1	hlavně
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
;	;	kIx,	;
spotřeba	spotřeba	k1gFnSc1	spotřeba
králičího	králičí	k2eAgNnSc2d1	králičí
masa	maso	k1gNnSc2	maso
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
rok	rok	k1gInSc4	rok
činí	činit	k5eAaImIp3nS	činit
2,7	[number]	k4	2,7
–	–	k?	–
3,0	[number]	k4	3,0
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Drobnochovatelé	drobnochovatel	k1gMnPc1	drobnochovatel
chovají	chovat	k5eAaImIp3nP	chovat
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
-	-	kIx~	-
nabídka	nabídka	k1gFnSc1	nabídka
králičího	králičí	k2eAgNnSc2d1	králičí
masa	maso	k1gNnSc2	maso
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
velkochovů	velkochov	k1gInPc2	velkochov
bíle	bíle	k6eAd1	bíle
pigmentovaných	pigmentovaný	k2eAgMnPc2d1	pigmentovaný
brojlerových	brojlerový	k2eAgMnPc2d1	brojlerový
králíků	králík	k1gMnPc2	králík
<g/>
.	.	kIx.	.
</s>
<s>
Brojleroví	brojlerový	k2eAgMnPc1d1	brojlerový
králíci	králík	k1gMnPc1	králík
jsou	být	k5eAaImIp3nP	být
hybridi	hybrid	k1gMnPc1	hybrid
šlechtění	šlechtění	k1gNnSc4	šlechtění
speciálně	speciálně	k6eAd1	speciálně
na	na	k7c4	na
ranost	ranost	k1gFnSc4	ranost
<g/>
,	,	kIx,	,
výbornou	výborný	k2eAgFnSc4d1	výborná
zmasilost	zmasilost	k1gFnSc4	zmasilost
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
intenzita	intenzita	k1gFnSc1	intenzita
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
konverze	konverze	k1gFnSc2	konverze
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
jsou	být	k5eAaImIp3nP	být
králíci	králík	k1gMnPc1	králík
chováni	chován	k2eAgMnPc1d1	chován
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
klimatizovaných	klimatizovaný	k2eAgFnPc6d1	klimatizovaná
halách	hala	k1gFnPc6	hala
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
chovu	chov	k1gInSc2	chov
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
klecový	klecový	k2eAgInSc1d1	klecový
<g/>
.	.	kIx.	.
</s>
<s>
Králice	králice	k1gFnPc1	králice
jsou	být	k5eAaImIp3nP	být
intenzivně	intenzivně	k6eAd1	intenzivně
využívané	využívaný	k2eAgFnPc1d1	využívaná
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
6-8	[number]	k4	6-8
vrhů	vrh	k1gInPc2	vrh
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Krmí	krmit	k5eAaImIp3nS	krmit
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
granulovanou	granulovaný	k2eAgFnSc7d1	granulovaná
krmnou	krmný	k2eAgFnSc7d1	krmná
směsí	směs	k1gFnSc7	směs
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
brojlerových	brojlerový	k2eAgMnPc2d1	brojlerový
králíků	králík	k1gMnPc2	králík
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
plsti	plst	k1gFnSc2	plst
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
kožešin	kožešina	k1gFnPc2	kožešina
jsou	být	k5eAaImIp3nP	být
chována	chován	k2eAgNnPc1d1	chováno
rexová	rexový	k2eAgNnPc1d1	rexový
plemena	plemeno	k1gNnPc1	plemeno
králíků	králík	k1gMnPc2	králík
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
vlny	vlna	k1gFnSc2	vlna
pak	pak	k6eAd1	pak
angorská	angorský	k2eAgNnPc4d1	angorské
plemena	plemeno	k1gNnPc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratorních	laboratorní	k2eAgInPc6d1	laboratorní
chovech	chov	k1gInPc6	chov
jsou	být	k5eAaImIp3nP	být
králíci	králík	k1gMnPc1	králík
chováni	chován	k2eAgMnPc1d1	chován
v	v	k7c6	v
klecích	klec	k1gFnPc6	klec
a	a	k8xC	a
krmeni	krmit	k5eAaImNgMnP	krmit
kompletní	kompletní	k2eAgFnSc7d1	kompletní
krmnou	krmný	k2eAgFnSc7d1	krmná
směsí	směs	k1gFnSc7	směs
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
jiného	jiný	k2eAgNnSc2d1	jiné
složení	složení	k1gNnSc2	složení
než	než	k8xS	než
směs	směs	k1gFnSc1	směs
k	k	k7c3	k
výkrmu	výkrm	k1gInSc3	výkrm
brojlerů	brojler	k1gMnPc2	brojler
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
zamezení	zamezení	k1gNnSc4	zamezení
styku	styk	k1gInSc2	styk
s	s	k7c7	s
choroboplodnými	choroboplodný	k2eAgInPc7d1	choroboplodný
zárodky	zárodek	k1gInPc7	zárodek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drobnochovech	drobnochov	k1gInPc6	drobnochov
jsou	být	k5eAaImIp3nP	být
králíci	králík	k1gMnPc1	králík
nejčastěji	často	k6eAd3	často
ustájeni	ustájit	k5eAaPmNgMnP	ustájit
v	v	k7c6	v
králíkárnách	králíkárna	k1gFnPc6	králíkárna
a	a	k8xC	a
krmeni	krmit	k5eAaImNgMnP	krmit
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
zrní	zrní	k1gNnSc2	zrní
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
v	v	k7c6	v
košilkách	košilka	k1gFnPc6	košilka
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
košilek	košilka	k1gFnPc2	košilka
ho	on	k3xPp3gInSc4	on
snědí	sníst	k5eAaPmIp3nP	sníst
více	hodně	k6eAd2	hodně
a	a	k8xC	a
ztloustnou	ztloustnout	k5eAaPmIp3nP	ztloustnout
<g/>
,	,	kIx,	,
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
trávy	tráva	k1gFnSc2	tráva
a	a	k8xC	a
sena	seno	k1gNnSc2	seno
(	(	kIx(	(
<g/>
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
zapařené	zapařený	k2eAgNnSc1d1	zapařené
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
se	se	k3xPyFc4	se
nafouknout	nafouknout	k5eAaPmF	nafouknout
a	a	k8xC	a
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakrslí	zakrslý	k2eAgMnPc1d1	zakrslý
králíci	králík	k1gMnPc1	králík
chovaní	chovaný	k2eAgMnPc1d1	chovaný
ze	z	k7c2	z
záliby	záliba	k1gFnSc2	záliba
jsou	být	k5eAaImIp3nP	být
oblíbení	oblíbený	k2eAgMnPc1d1	oblíbený
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
klecích	klec	k1gFnPc6	klec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
minimální	minimální	k2eAgInSc4d1	minimální
co	co	k8xS	co
největší	veliký	k2eAgInSc4d3	veliký
rozměry	rozměr	k1gInPc4	rozměr
(	(	kIx(	(
<g/>
ideální	ideální	k2eAgInPc1d1	ideální
jsou	být	k5eAaImIp3nP	být
100	[number]	k4	100
<g/>
x	x	k?	x
<g/>
60	[number]	k4	60
a	a	k8xC	a
s	s	k7c7	s
plastovým	plastový	k2eAgInSc7d1	plastový
úkrytem	úkryt	k1gInSc7	úkryt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kleci	klec	k1gFnSc6	klec
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podestýlka	podestýlka	k1gFnSc1	podestýlka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kočkolit	kočkolit	k1gInSc1	kočkolit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
pak	pak	k6eAd1	pak
seno	seno	k1gNnSc1	seno
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
výběh	výběh	k1gInSc1	výběh
mimo	mimo	k7c4	mimo
klec	klec	k1gFnSc4	klec
<g/>
.	.	kIx.	.
</s>
<s>
Králíci	Králík	k1gMnPc1	Králík
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
chováni	chovat	k5eAaImNgMnP	chovat
po	po	k7c6	po
jednotlivcích	jednotlivec	k1gMnPc6	jednotlivec
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
teritorialitě	teritorialita	k1gFnSc3	teritorialita
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
králíci	králík	k1gMnPc1	králík
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
jistě	jistě	k6eAd1	jistě
nesnesou	snést	k5eNaPmIp3nP	snést
<g/>
.	.	kIx.	.
</s>
<s>
Králík	Králík	k1gMnSc1	Králík
je	být	k5eAaImIp3nS	být
citlivý	citlivý	k2eAgMnSc1d1	citlivý
na	na	k7c4	na
hluk	hluk	k1gInSc4	hluk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
klec	klec	k1gFnSc1	klec
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
tiché	tichý	k2eAgFnSc6d1	tichá
části	část	k1gFnSc6	část
bytu	byt	k1gInSc2	byt
(	(	kIx(	(
<g/>
ne	ne	k9	ne
u	u	k7c2	u
televize	televize	k1gFnSc2	televize
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
přicházet	přicházet	k5eAaImF	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
tabákovým	tabákový	k2eAgInSc7d1	tabákový
kouřem	kouř	k1gInSc7	kouř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
králík	králík	k1gMnSc1	králík
příliš	příliš	k6eAd1	příliš
ztuční	ztučnit	k5eAaPmIp3nS	ztučnit
(	(	kIx(	(
<g/>
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
strava	strava	k1gFnSc1	strava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
krmné	krmný	k2eAgFnSc2d1	krmná
dávky	dávka	k1gFnSc2	dávka
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
seno	seno	k1gNnSc4	seno
granule	granule	k1gFnSc2	granule
pro	pro	k7c4	pro
králíky	králík	k1gMnPc4	králík
v	v	k7c6	v
dávce	dávka	k1gFnSc6	dávka
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
dospělého	dospělý	k2eAgNnSc2d1	dospělé
zvířete	zvíře	k1gNnSc2	zvíře
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
%	%	kIx~	%
nebo	nebo	k8xC	nebo
neomezeně	omezeně	k6eNd1	omezeně
u	u	k7c2	u
mláděte	mládě	k1gNnSc2	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
králičí	králičí	k2eAgInSc1d1	králičí
mor	mor	k1gInSc1	mor
myxomatóza	myxomatóza	k1gFnSc1	myxomatóza
syfilis	syfilis	k1gFnSc1	syfilis
králíků	králík	k1gMnPc2	králík
paréza	paréza	k1gFnSc1	paréza
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
kojících	kojící	k2eAgInPc2d1	kojící
ramlic	ramlic	k?	ramlic
<g />
.	.	kIx.	.
</s>
<s>
toxikóza	toxikóza	k1gFnSc1	toxikóza
březích	březí	k2eAgInPc2d1	březí
ramlic	ramlic	k?	ramlic
kokcidióza	kokcidióza	k1gFnSc1	kokcidióza
</s>
<s>
Zajímavostí	zajímavost	k1gFnPc2	zajímavost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
králík	králík	k1gMnSc1	králík
domácí	domácí	k1gMnSc1	domácí
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pohybovat	pohybovat	k5eAaImF	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
56	[number]	k4	56
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	kA	h
<g />
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Konrád	Konrád	k1gMnSc1	Konrád
<g/>
:	:	kIx,	:
Nemoci	nemoc	k1gFnPc1	nemoc
králíků	králík	k1gMnPc2	králík
se	s	k7c7	s
základy	základ	k1gInPc7	základ
hygieny	hygiena	k1gFnSc2	hygiena
chovu	chov	k1gInSc2	chov
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fingerland	Fingerland	k1gInSc1	Fingerland
<g/>
:	:	kIx,	:
Domácí	domácí	k2eAgInSc1d1	domácí
chov	chov	k1gInSc1	chov
králíků	králík	k1gMnPc2	králík
<g/>
,	,	kIx,	,
Brázda	brázda	k1gFnSc1	brázda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-209-0184-1	[number]	k4	80-209-0184-1
Plemena	plemeno	k1gNnSc2	plemeno
králíků	králík	k1gMnPc2	králík
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
králík	králík	k1gMnSc1	králík
domácí	domácí	k2eAgMnSc1d1	domácí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
