<s>
Valčík	valčík	k1gInSc1	valčík
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
podobnými	podobný	k2eAgInPc7d1	podobný
tanci	tanec	k1gInPc7	tanec
z	z	k7c2	z
lidového	lidový	k2eAgInSc2d1	lidový
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
z	z	k7c2	z
rakouského	rakouský	k2eAgInSc2d1	rakouský
Ländleru	ländler	k1gInSc2	ländler
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Deutscheru	Deutscher	k1gInSc2	Deutscher
<g/>
.	.	kIx.	.
</s>
