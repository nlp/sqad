<s>
Valčík	valčík	k1gInSc1	valčík
je	být	k5eAaImIp3nS	být
postupový	postupový	k2eAgInSc1d1	postupový
<g/>
,	,	kIx,	,
kolový	kolový	k2eAgInSc1d1	kolový
tanec	tanec	k1gInSc1	tanec
v	v	k7c6	v
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
taktu	takt	k1gInSc2	takt
nebo	nebo	k8xC	nebo
zřídka	zřídka	k6eAd1	zřídka
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
v	v	k7c6	v
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
taktu	takt	k1gInSc2	takt
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
první	první	k4xOgFnSc4	první
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tanec	tanec	k1gInSc1	tanec
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
v	v	k7c6	v
páru	pár	k1gInSc6	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
páru	pár	k1gInSc2	pár
vždy	vždy	k6eAd1	vždy
uhýbá	uhýbat	k5eAaImIp3nS	uhýbat
druhému	druhý	k4xOgInSc3	druhý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jde	jít	k5eAaImIp3nS	jít
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
během	během	k7c2	během
taktu	takt	k1gInSc2	takt
otočí	otočit	k5eAaPmIp3nS	otočit
celý	celý	k2eAgInSc1d1	celý
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
tancem	tanec	k1gInSc7	tanec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
tančil	tančit	k5eAaImAgInS	tančit
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
těsném	těsný	k2eAgNnSc6d1	těsné
držení	držení	k1gNnSc6	držení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
městské	městský	k2eAgFnSc6d1	městská
společnosti	společnost	k1gFnSc6	společnost
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Valčík	valčík	k1gInSc1	valčík
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
podobnými	podobný	k2eAgInPc7d1	podobný
tanci	tanec	k1gInPc7	tanec
z	z	k7c2	z
lidového	lidový	k2eAgInSc2d1	lidový
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
z	z	k7c2	z
rakouského	rakouský	k2eAgInSc2d1	rakouský
Ländleru	ländler	k1gInSc2	ländler
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Deutscheru	Deutscher	k1gInSc2	Deutscher
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc4d1	divadelní
premiéru	premiéra	k1gFnSc4	premiéra
měl	mít	k5eAaImAgMnS	mít
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Una	Una	k1gMnSc2	Una
cosa	cosus	k1gMnSc2	cosus
rara	rarus	k1gMnSc2	rarus
(	(	kIx(	(
<g/>
Vzácná	vzácný	k2eAgFnSc1d1	vzácná
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Vincent	Vincent	k1gMnSc1	Vincent
Martin	Martin	k1gMnSc1	Martin
<g/>
)	)	kIx)	)
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velice	velice	k6eAd1	velice
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
děl	dělo	k1gNnPc2	dělo
význačných	význačný	k2eAgMnPc2d1	význačný
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
:	:	kIx,	:
např.	např.	kA	např.
Johanna	Johann	k1gMnSc2	Johann
Strausse	Strauss	k1gMnSc2	Strauss
staršího	starší	k1gMnSc2	starší
<g/>
,	,	kIx,	,
Johanna	Johann	k1gMnSc2	Johann
Strausse	Strauss	k1gMnSc2	Strauss
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Lannera	Lanner	k1gMnSc2	Lanner
<g/>
,	,	kIx,	,
atd	atd	kA	atd
<g/>
...	...	k?	...
Z	z	k7c2	z
hudebního	hudební	k2eAgNnSc2d1	hudební
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
valčíků	valčík	k1gInPc2	valčík
<g/>
:	:	kIx,	:
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
valčík	valčík	k1gInSc1	valčík
-	-	kIx~	-
řetěz	řetěz	k1gInSc1	řetěz
několika	několik	k4yIc2	několik
valčíkových	valčíkový	k2eAgFnPc2d1	Valčíková
melodií	melodie	k1gFnPc2	melodie
za	za	k7c7	za
sebou	se	k3xPyFc7	se
pařížský	pařížský	k2eAgInSc4d1	pařížský
valčík	valčík	k1gInSc4	valčík
-	-	kIx~	-
předehra	předehra	k1gFnSc1	předehra
a	a	k8xC	a
trio	trio	k1gNnSc1	trio
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
valčíkové	valčíkový	k2eAgFnSc2d1	Valčíková
melodie	melodie	k1gFnSc2	melodie
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc4	píseň
Pásla	pásnout	k5eAaImAgFnS	pásnout
ovečky	ovečka	k1gFnPc4	ovečka
<g/>
.	.	kIx.	.
</s>
<s>
Otáčka	otáčka	k1gFnSc1	otáčka
vpravo	vpravo	k6eAd1	vpravo
-	-	kIx~	-
Natural	Natural	k?	Natural
Turn	Turna	k1gFnPc2	Turna
Otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Reverse	reverse	k1gFnSc1	reverse
Turn	Turna	k1gFnPc2	Turna
Uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
změny	změna	k1gFnSc2	změna
vpřed	vpřed	k6eAd1	vpřed
-	-	kIx~	-
Closed	Closed	k1gMnSc1	Closed
Forward	Forward	k1gMnSc1	Forward
Changes	Changes	k1gMnSc1	Changes
Uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
změny	změna	k1gFnPc4	změna
vzad	vzad	k6eAd1	vzad
-	-	kIx~	-
Closed	Closed	k1gMnSc1	Closed
Backward	Backward	k1gMnSc1	Backward
Changes	Changes	k1gMnSc1	Changes
Valčík	valčík	k1gInSc4	valčík
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vpravo	vpravo	k6eAd1	vpravo
-	-	kIx~	-
Natural	Natural	k?	Natural
Fleckerl	Fleckerl	k1gInSc1	Fleckerl
Valčík	valčík	k1gInSc1	valčík
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Reverse	reverse	k1gFnSc1	reverse
Fleckerl	Fleckerl	k1gInSc1	Fleckerl
Protizarážka	Protizarážka	k1gFnSc1	Protizarážka
-	-	kIx~	-
Contra	Contra	k1gFnSc1	Contra
Check	Checka	k1gFnPc2	Checka
</s>
