<p>
<s>
Campari	campari	k1gNnSc1	campari
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
italských	italský	k2eAgInPc2d1	italský
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
řazených	řazený	k2eAgInPc2d1	řazený
mezi	mezi	k7c4	mezi
kořeněné	kořeněný	k2eAgInPc4d1	kořeněný
bittery	bitter	k1gInPc4	bitter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tajná	tajný	k2eAgFnSc1d1	tajná
receptura	receptura	k1gFnSc1	receptura
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
86	[number]	k4	86
různých	různý	k2eAgFnPc2d1	různá
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
typická	typický	k2eAgFnSc1d1	typická
rubínová	rubínový	k2eAgFnSc1d1	rubínová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
barvivem	barvivo	k1gNnSc7	barvivo
získaným	získaný	k2eAgNnSc7d1	získané
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
červce	červec	k1gMnSc2	červec
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
120	[number]	k4	120
<g/>
,	,	kIx,	,
karmín	karmín	k1gInSc1	karmín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
25	[number]	k4	25
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
<g/>
Bitter	Bitter	k1gInSc1	Bitter
–	–	k?	–
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
líh	líh	k1gInSc1	líh
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgNnPc1d1	přírodní
aroma	aroma	k1gNnPc1	aroma
<g/>
,	,	kIx,	,
barviva	barvivo	k1gNnPc1	barvivo
<g/>
:	:	kIx,	:
E122	E122	k1gFnSc2	E122
(	(	kIx(	(
<g/>
Azorubin	Azorubina	k1gFnPc2	Azorubina
–	–	k?	–
Skóre	skóre	k1gNnSc4	skóre
škodlivosti	škodlivost	k1gFnSc2	škodlivost
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E102	E102	k1gFnSc1	E102
(	(	kIx(	(
<g/>
Tartrazin	Tartrazin	k1gInSc1	Tartrazin
–	–	k?	–
Skóre	skóre	k1gNnSc7	skóre
škodlivosti	škodlivost	k1gFnSc2	škodlivost
<g/>
:	:	kIx,	:
4	[number]	k4	4
–	–	k?	–
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E133	E133	k1gFnSc1	E133
(	(	kIx(	(
<g/>
Brilantní	brilantní	k2eAgFnSc1d1	brilantní
modř	modř	k1gFnSc1	modř
FCF	FCF	kA	FCF
–	–	k?	–
Skóre	skóre	k1gNnSc1	skóre
škodlivosti	škodlivost	k1gFnSc2	škodlivost
<g/>
:	:	kIx,	:
5	[number]	k4	5
–	–	k?	–
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
je	být	k5eAaImIp3nS	být
hostinský	hostinský	k1gMnSc1	hostinský
a	a	k8xC	a
destilatér	destilatér	k1gMnSc1	destilatér
Caspar	Caspar	k1gMnSc1	Caspar
David	David	k1gMnSc1	David
Campari	campari	k1gNnPc2	campari
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začínal	začínat	k5eAaImAgInS	začínat
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
výrobou	výroba	k1gFnSc7	výroba
lihovin	lihovina	k1gFnPc2	lihovina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
nápoj	nápoj	k1gInSc1	nápoj
prodává	prodávat	k5eAaImIp3nS	prodávat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
190	[number]	k4	190
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
světových	světový	k2eAgFnPc2d1	světová
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skupina	skupina	k1gFnSc1	skupina
Campari	campari	k1gNnSc2	campari
==	==	k?	==
</s>
</p>
<p>
<s>
Campari	campari	k1gNnSc1	campari
Group	Group	k1gMnSc1	Group
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
název	název	k1gInSc4	název
nadnárodní	nadnárodní	k2eAgFnSc2d1	nadnárodní
firmy	firma	k1gFnSc2	firma
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
se	s	k7c7	s
14	[number]	k4	14
výrobními	výrobní	k2eAgInPc7d1	výrobní
závody	závod	k1gInPc7	závod
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
4	[number]	k4	4
000	[number]	k4	000
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
a	a	k8xC	a
obratem	obratem	k6eAd1	obratem
asi	asi	k9	asi
1,8	[number]	k4	1,8
miliardy	miliarda	k4xCgFnPc4	miliarda
EUR	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gNnSc1	lexikon
aperitivů	aperitiv	k1gInPc2	aperitiv
a	a	k8xC	a
digestivů	digestiv	k1gInPc2	digestiv
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7234-570-2	[number]	k4	80-7234-570-2
</s>
</p>
