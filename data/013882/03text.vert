<s>
Radek	Radek	k1gMnSc1
Slončík	Slončík	k1gMnSc1
</s>
<s>
Radek	Radek	k1gMnSc1
SlončíkOsobní	SlončíkOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1973	#num#	k4
(	(	kIx(
<g/>
47	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Šumperk	Šumperk	k1gInSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
174	#num#	k4
cm	cm	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
záložník	záložník	k1gMnSc1
Současná	současný	k2eAgFnSc1d1
funkce	funkce	k1gFnPc4
</s>
<s>
sportovní	sportovní	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Mládežnické	mládežnický	k2eAgNnSc1d1
kluby	klub	k1gInPc1
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
19871987	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
</s>
<s>
Lokomotiva-Pramet	Lokomotiva-Pramet	k1gInSc1
Šumperk	Šumperk	k1gInSc1
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
20002000	#num#	k4
<g/>
–	–	k?
<g/>
20012002	#num#	k4
<g/>
–	–	k?
<g/>
20032003	#num#	k4
<g/>
–	–	k?
<g/>
20062007	#num#	k4
<g/>
–	–	k?
<g/>
20082009	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
Újpest	Újpest	k1gInSc1
FC	FC	kA
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Fotbal	fotbal	k1gInSc1
Fulnek	Fulnek	k1gMnSc1
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
<g/>
0	#num#	k4
<g/>
1300	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
3000	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
6400	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
3700	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
2600	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
19961996	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
Česko	Česko	k1gNnSc1
U21	U21	k1gFnPc2
Česko	Česko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
1300	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
1700	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Pohár	pohár	k1gInSc1
ČMFS	ČMFS	kA
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Petr	Petr	k1gMnSc1
Slončík	Slončík	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
bratranec	bratranec	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Slončík	Slončík	k1gMnSc1
</s>
<s>
Radek	Radek	k1gMnSc1
Slončík	Slončík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1973	#num#	k4
<g/>
,	,	kIx,
Šumperk	Šumperk	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
<g/>
,	,	kIx,
reprezentant	reprezentant	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
kvalitní	kvalitní	k2eAgMnSc1d1
tvůrce	tvůrce	k1gMnSc1
hry	hra	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
špílmachr	špílmachr	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
s	s	k7c7
precizní	precizní	k2eAgFnSc7d1
a	a	k8xC
překvapivou	překvapivý	k2eAgFnSc7d1
rozehrávkou	rozehrávka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
bratrancem	bratranec	k1gMnSc7
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
ligový	ligový	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
Petr	Petr	k1gMnSc1
Slončík	Slončík	k1gMnSc1xF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Klubová	klubový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
S	s	k7c7
fotbalem	fotbal	k1gInSc7
začínal	začínat	k5eAaImAgMnS
v	v	k7c6
rodném	rodný	k2eAgInSc6d1
Šumperku	Šumperk	k1gInSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
v	v	k7c6
dorostu	dorost	k1gInSc6
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Baník	Baník	k1gInSc4
hrál	hrát	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgInSc1d1
angažmá	angažmá	k1gNnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
příliš	příliš	k6eAd1
nevydařilo	vydařit	k5eNaPmAgNnS
a	a	k8xC
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
maďarského	maďarský	k2eAgInSc2d1
týmu	tým	k1gInSc2
Újpest	Újpest	k1gMnSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
Baníku	Baník	k1gInSc2
a	a	k8xC
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
něj	on	k3xPp3gMnSc4
do	do	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
i	i	k9
v	v	k7c6
třetiligovém	třetiligový	k2eAgInSc6d1
Fulneku	Fulnek	k1gInSc6
<g/>
,	,	kIx,
aktivní	aktivní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
zakončil	zakončit	k5eAaPmAgMnS
v	v	k7c6
klubu	klub	k1gInSc6
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Radek	Radek	k1gMnSc1
Slončík	Slončík	k1gMnSc1
zasáhl	zasáhnout	k5eAaPmAgMnS
v	v	k7c6
letech	let	k1gInPc6
1994	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
v	v	k7c6
dresu	dres	k1gInSc6
české	český	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
do	do	k7c2
13	#num#	k4
utkání	utkání	k1gNnPc2
(	(	kIx(
<g/>
6	#num#	k4
výher	výhra	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
remízy	remíza	k1gFnPc1
a	a	k8xC
5	#num#	k4
proher	prohra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
vstřelil	vstřelit	k5eAaPmAgMnS
gól	gól	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českém	český	k2eAgNnSc6d1
reprezentačním	reprezentační	k2eAgNnSc6d1
A-mužstvu	A-mužstvo	k1gNnSc6
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1996	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
17	#num#	k4
zápasů	zápas	k1gInPc2
(	(	kIx(
<g/>
12	#num#	k4
výher	výhra	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
2	#num#	k4
prohry	prohra	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gól	gól	k1gInSc4
nevstřelil	vstřelit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Téměř	téměř	k6eAd1
vždy	vždy	k6eAd1
střídal	střídat	k5eAaImAgMnS
<g/>
,	,	kIx,
odehrál	odehrát	k5eAaPmAgMnS
jen	jen	k9
jedno	jeden	k4xCgNnSc4
kompletní	kompletní	k2eAgNnSc4d1
kvalifikační	kvalifikační	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1997	#num#	k4
na	na	k7c6
Maltě	Malta	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
ČR	ČR	kA
vyhrála	vyhrát	k5eAaPmAgFnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zápasy	zápas	k1gInPc1
Radka	Radek	k1gMnSc2
Slončíka	Slončík	k1gMnSc2
v	v	k7c6
A-mužstvu	A-mužstvo	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RokZápasyGóly	RokZápasyGóla	k1gFnPc1
</s>
<s>
199620	#num#	k4
</s>
<s>
199760	#num#	k4
</s>
<s>
199860	#num#	k4
</s>
<s>
199920	#num#	k4
</s>
<s>
200010	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
170	#num#	k4
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
OTP	OTP	kA
Bank	bank	k1gInSc1
Liga	liga	k1gFnSc1
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Újpest	Újpest	k1gFnSc1
FC	FC	kA
</s>
<s>
OTP	OTP	kA
Bank	bank	k1gInSc1
Liga	liga	k1gFnSc1
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Újpest	Újpest	k1gFnSc1
FC	FC	kA
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
CELKEM	celkem	k6eAd1
</s>
<s>
286	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Statistiky	statistika	k1gFnPc1
Radka	Radek	k1gMnSc2
Slončíka	Slončík	k1gMnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Fotbalové	fotbalový	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
↑	↑	k?
Fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Malta	Malta	k1gFnSc1
-	-	kIx~
ČR	ČR	kA
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c6
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Slončík	Slončík	k1gInSc1
bere	brát	k5eAaImIp3nS
i	i	k9
nevýhodný	výhodný	k2eNgInSc1d1
kontrakt	kontrakt	k1gInSc1
</s>
<s>
Opavský	opavský	k2eAgInSc1d1
deník	deník	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
-	-	kIx~
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Blažek	Blažek	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Poštulka	Poštulka	k1gFnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Patrice	patrice	k1gFnSc1
Etong	Etonga	k1gFnPc2
Abanda	Aband	k1gMnSc2
•	•	k?
René	René	k1gMnSc1
Bolf	Bolf	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Flachbart	Flachbarta	k1gFnPc2
•	•	k?
Milan	Milan	k1gMnSc1
Fukal	Fukal	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Grygera	Grygero	k1gNnSc2
•	•	k?
Martin	Martin	k1gMnSc1
Hašek	Hašek	k1gMnSc1
•	•	k?
Radim	Radim	k1gMnSc1
Holub	Holub	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Horňák	Horňák	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Jarošík	Jarošík	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Jun	jun	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Kincl	Kincl	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Klein	Klein	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Koloušek	koloušek	k1gMnSc1
•	•	k?
Ivica	Ivica	k1gFnSc1
Križanac	Križanac	k1gFnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Labant	Labant	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Lengyel	Lengyel	k1gMnSc1
•	•	k?
Leandro	Leandra	k1gFnSc5
Lazzaro	Lazzara	k1gFnSc5
Liuni	Liuň	k1gMnSc3
•	•	k?
Radek	Radek	k1gMnSc1
Mynář	Mynář	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Novotný	Novotný	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Novotný	Novotný	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Obajdin	Obajdin	k2eAgMnSc1d1
•	•	k?
Petr	Petr	k1gMnSc1
Papoušek	Papoušek	k1gMnSc1
•	•	k?
Martin	Martin	k1gInSc1
Prohászka	Prohászka	k1gFnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
•	•	k?
Horst	Horst	k1gMnSc1
Siegl	Siegl	k1gMnSc1
•	•	k?
Libor	Libor	k1gMnSc1
Sionko	Sionko	k1gNnSc1
•	•	k?
Radek	Radek	k1gMnSc1
Slončík	Slončík	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
-	-	kIx~
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Laštůvka	laštůvka	k1gFnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Besta	Besta	k1gMnSc1
•	•	k?
René	René	k1gMnSc1
Bolf	Bolf	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Bystroň	Bystroň	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Čížek	Čížek	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Drozd	Drozd	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Dvorník	dvorník	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Heinz	Heinz	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Hoffmann	Hoffmann	k1gMnSc1
•	•	k?
Rostislav	Rostislav	k1gMnSc1
Kiša	Kiša	k1gMnSc1
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Krpec	krpec	k1gInSc1
•	•	k?
Radoslav	Radoslav	k1gMnSc1
Látal	Látal	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Lička	lička	k1gFnSc1
•	•	k?
Lukáš	Lukáš	k1gMnSc1
Magera	Magero	k1gNnSc2
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Matušovič	Matušovič	k1gMnSc1
•	•	k?
Aleš	Aleš	k1gMnSc1
Neuwirth	Neuwirth	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Pospěch	pospěch	k1gInSc1
•	•	k?
Martin	Martin	k1gInSc1
Prohászka	Prohászka	k1gFnSc1
•	•	k?
Radek	Radek	k1gMnSc1
Slončík	Slončík	k1gMnSc1
•	•	k?
Adam	Adam	k1gMnSc1
Varadi	Varad	k1gMnPc1
•	•	k?
Libor	Libor	k1gMnSc1
Žůrek	Žůrek	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Komňacký	Komňacký	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
