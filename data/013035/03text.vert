<p>
<s>
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc4	třetí
největší	veliký	k2eAgFnSc4d3	veliký
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
páté	pátý	k4xOgNnSc1	pátý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
(	(	kIx(	(
<g/>
větší	veliký	k2eAgInPc1d2	veliký
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
města	město	k1gNnPc1	město
Sã	Sã	k1gMnPc2	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Rio	Rio	k1gMnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
a	a	k8xC	a
Fortaleza	Fortaleza	k1gFnSc1	Fortaleza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
Minas	Minasa	k1gFnPc2	Minasa
Gerais	Gerais	k1gFnSc2	Gerais
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
architektem	architekt	k1gMnSc7	architekt
Aarã	Aarã	k1gMnSc7	Aarã
Reisem	Reis	k1gMnSc7	Reis
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
z	z	k7c2	z
plánovaně	plánovaně	k6eAd1	plánovaně
založených	založený	k2eAgNnPc2d1	založené
brazilských	brazilský	k2eAgNnPc2d1	brazilské
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
odlehčit	odlehčit	k5eAaPmF	odlehčit
populačnímu	populační	k2eAgInSc3d1	populační
tlaku	tlak	k1gInSc2	tlak
přímořských	přímořský	k2eAgFnPc2d1	přímořská
aglomerací	aglomerace	k1gFnPc2	aglomerace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
takřka	takřka	k6eAd1	takřka
2,4	[number]	k4	2,4
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
5	[number]	k4	5
miliony	milion	k4xCgInPc1	milion
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
ekonomickým	ekonomický	k2eAgInPc3d1	ekonomický
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
portugalštině	portugalština	k1gFnSc6	portugalština
"	"	kIx"	"
<g/>
Pěkná	pěkný	k2eAgFnSc1d1	pěkná
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
starosty	starosta	k1gMnSc2	starosta
Juscelina	Juscelin	k2eAgInSc2d1	Juscelin
Kubitscheka	Kubitscheek	k1gInSc2	Kubitscheek
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Pampulha	Pampulha	k1gMnSc1	Pampulha
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
umělé	umělý	k2eAgNnSc1d1	umělé
jezero	jezero	k1gNnSc1	jezero
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
březích	břeh	k1gInPc6	břeh
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
budov	budova	k1gFnPc2	budova
včetně	včetně	k7c2	včetně
okolních	okolní	k2eAgInPc2d1	okolní
parků	park	k1gInPc2	park
a	a	k8xC	a
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Oscar	Oscar	k1gMnSc1	Oscar
Niemeyer	Niemeyer	k1gMnSc1	Niemeyer
a	a	k8xC	a
Roberto	Roberta	k1gFnSc5	Roberta
Burle	Burle	k1gNnSc7	Burle
Marx	Marx	k1gMnSc1	Marx
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
soubor	soubor	k1gInSc4	soubor
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
parků	park	k1gInPc2	park
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
metro	metro	k1gNnSc1	metro
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
metro	metro	k1gNnSc4	metro
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
však	však	k9	však
jediná	jediný	k2eAgFnSc1d1	jediná
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
19	[number]	k4	19
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
stanic	stanice	k1gFnPc2	stanice
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
dostačující	dostačující	k2eAgNnSc1d1	dostačující
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
rozšíření	rozšíření	k1gNnSc4	rozšíření
již	již	k6eAd1	již
fungující	fungující	k2eAgFnSc2d1	fungující
linky	linka	k1gFnSc2	linka
o	o	k7c4	o
pět	pět	k4xCc4	pět
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
dvou	dva	k4xCgInPc2	dva
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgFnPc2d1	nová
tras	trasa	k1gFnPc2	trasa
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodáci	rodák	k1gMnPc5	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Dilma	Dilma	k1gFnSc1	Dilma
Rousseff	Rousseff	k1gMnSc1	Rousseff
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
prezidentka	prezidentka	k1gFnSc1	prezidentka
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
Tostã	Tostã	k?	Tostã
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Cavalera	Cavaler	k1gMnSc2	Cavaler
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
(	(	kIx(	(
<g/>
Sepultura	Sepultura	k1gFnSc1	Sepultura
<g/>
,	,	kIx,	,
Soulfly	Soulfly	k1gMnSc2	Soulfly
a	a	k8xC	a
Cavalera	Cavaler	k1gMnSc2	Cavaler
Conspiracy	Conspiraca	k1gMnSc2	Conspiraca
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Igor	Igor	k1gMnSc1	Igor
Cavalera	Cavaler	k1gMnSc2	Cavaler
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
(	(	kIx(	(
<g/>
Sepultura	Sepultura	k1gFnSc1	Sepultura
a	a	k8xC	a
Cavalera	Cavaler	k1gMnSc2	Cavaler
Conspiracy	Conspiraca	k1gMnSc2	Conspiraca
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ana	Ana	k?	Ana
Paula	Paula	k1gFnSc1	Paula
Valadã	Valadã	k1gFnSc1	Valadã
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brazilská	brazilský	k2eAgFnSc1d1	brazilská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Cristiano	Cristiana	k1gFnSc5	Cristiana
da	da	k?	da
Matta	Matto	k1gNnPc4	Matto
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
</s>
</p>
<p>
<s>
Evanilson	Evanilson	k1gNnSc1	Evanilson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Junqueira	Junqueira	k1gMnSc1	Junqueira
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
</s>
</p>
<p>
<s>
André	André	k1gMnSc1	André
Sá	Sá	k1gMnSc1	Sá
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Dedê	Dedê	k?	Dedê
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Afonso	Afonsa	k1gFnSc5	Afonsa
Alves	Alves	k1gInSc4	Alves
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Soares	Soares	k1gMnSc1	Soares
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Marcelo	Marcela	k1gFnSc5	Marcela
Melo	Melo	k?	Melo
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Wallace	Wallace	k1gFnSc1	Wallace
Nunes	Nunesa	k1gFnPc2	Nunesa
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slavný	slavný	k2eAgMnSc1d1	slavný
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
neméně	málo	k6eNd2	málo
slavný	slavný	k2eAgMnSc1d1	slavný
dispečer	dispečer	k1gMnSc1	dispečer
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Betlém	Betlém	k1gInSc1	Betlém
<g/>
,	,	kIx,	,
Stát	stát	k1gInSc1	stát
Palestina	Palestina	k1gFnSc1	Palestina
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cuenca	Cuenca	k1gFnSc1	Cuenca
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fort	Fort	k?	Fort
Lauderdale	Lauderdala	k1gFnSc3	Lauderdala
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Granada	Granada	k1gFnSc1	Granada
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Havana	Havana	k1gFnSc1	Havana
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Homs	Homs	k1gInSc1	Homs
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lagos	Lagos	k1gInSc1	Lagos
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Luanda	Luanda	k1gFnSc1	Luanda
<g/>
,	,	kIx,	,
Angola	Angola	k1gFnSc1	Angola
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masaya	Masaya	k1gFnSc1	Masaya
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Minsk	Minsk	k1gInSc1	Minsk
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nanking	Nanking	k1gInSc1	Nanking
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Newark	Newark	k1gInSc1	Newark
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Porto	porto	k1gNnSc1	porto
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tegucigalpa	Tegucigalpa	k1gFnSc1	Tegucigalpa
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zahlé	Zahl	k1gMnPc1	Zahl
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
belo	bela	k1gFnSc5	bela
Horizonte	horizont	k1gInSc5	horizont
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
