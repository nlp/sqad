<s>
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
pilotovaný	pilotovaný	k2eAgInSc1d1	pilotovaný
kosmický	kosmický	k2eAgInSc1d1	kosmický
let	let	k1gInSc1	let
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc4	Apollo
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
lidé	člověk	k1gMnPc1	člověk
poprvé	poprvé	k6eAd1	poprvé
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
