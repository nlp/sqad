<s>
Impersonální	impersonální	k2eAgFnSc1d1	impersonální
láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
,	,	kIx,	,
předmětům	předmět	k1gInPc3	předmět
<g/>
,	,	kIx,	,
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
sexuálního	sexuální	k2eAgInSc2d1	sexuální
faktoru	faktor	k1gInSc2	faktor
<g/>
.	.	kIx.	.
</s>
