<p>
<s>
Kapitalismus	kapitalismus	k1gInSc1	kapitalismus
je	být	k5eAaImIp3nS	být
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
výrobní	výrobní	k2eAgInPc4d1	výrobní
prostředky	prostředek	k1gInPc4	prostředek
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgFnP	provozovat
v	v	k7c4	v
prostředí	prostředí	k1gNnSc4	prostředí
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc1	dosažení
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Akumulací	akumulace	k1gFnPc2	akumulace
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
zisku	zisk	k1gInSc2	zisk
vzniká	vznikat	k5eAaImIp3nS	vznikat
kapitál	kapitál	k1gInSc1	kapitál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
investován	investovat	k5eAaBmNgMnS	investovat
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
navýšení	navýšení	k1gNnSc2	navýšení
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
více	hodně	k6eAd2	hodně
pojetí	pojetí	k1gNnSc1	pojetí
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
závisející	závisející	k2eAgNnSc1d1	závisející
na	na	k7c6	na
historické	historický	k2eAgFnSc6d1	historická
době	doba	k1gFnSc6	doba
a	a	k8xC	a
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
také	také	k9	také
zpravidla	zpravidla	k6eAd1	zpravidla
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
jen	jen	k9	jen
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
společenský	společenský	k2eAgInSc4d1	společenský
a	a	k8xC	a
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
možnost	možnost	k1gFnSc4	možnost
takový	takový	k3xDgInSc4	takový
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
systém	systém	k1gInSc4	systém
regulovat	regulovat	k5eAaImF	regulovat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zákoníkem	zákoník	k1gInSc7	zákoník
práce	práce	k1gFnSc2	práce
či	či	k8xC	či
obchodním	obchodní	k2eAgInSc7d1	obchodní
zákoníkem	zákoník	k1gInSc7	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1	Anarchokapitalismus
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
název	název	k1gInSc4	název
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
státní	státní	k2eAgFnSc2d1	státní
regulace	regulace	k1gFnSc2	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomiky	ekonomika	k1gFnPc1	ekonomika
takových	takový	k3xDgInPc2	takový
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stát	stát	k1gInSc1	stát
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
smíšené	smíšený	k2eAgInPc1d1	smíšený
<g/>
;	;	kIx,	;
tržní	tržní	k2eAgInPc1d1	tržní
principy	princip	k1gInPc1	princip
fungují	fungovat	k5eAaImIp3nP	fungovat
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
však	však	k9	však
vládne	vládnout	k5eAaImIp3nS	vládnout
monopol	monopol	k1gInSc1	monopol
či	či	k8xC	či
monopson	monopson	k1gInSc1	monopson
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
==	==	k?	==
</s>
</p>
<p>
<s>
Kapitalistické	kapitalistický	k2eAgInPc1d1	kapitalistický
vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
pomalu	pomalu	k6eAd1	pomalu
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
vztahy	vztah	k1gInPc4	vztah
feudální	feudální	k2eAgInPc4d1	feudální
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
světa	svět	k1gInSc2	svět
a	a	k8xC	a
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
původních	původní	k2eAgInPc2d1	původní
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
a	a	k8xC	a
společenských	společenský	k2eAgInPc2d1	společenský
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
dochází	docházet	k5eAaImIp3nS	docházet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
gramotností	gramotnost	k1gFnSc7	gramotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Will	Will	k1gInSc1	Will
Hutton	Hutton	k1gInSc1	Hutton
a	a	k8xC	a
Anthony	Anthona	k1gFnPc1	Anthona
Giddens	Giddensa	k1gFnPc2	Giddensa
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
charakteristiky	charakteristika	k1gFnPc4	charakteristika
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
majetku	majetek	k1gInSc2	majetek
</s>
</p>
<p>
<s>
Hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
činnost	činnost	k1gFnSc4	činnost
řídí	řídit	k5eAaImIp3nS	řídit
cenové	cenový	k2eAgInPc4d1	cenový
signály	signál	k1gInPc4	signál
trhu	trh	k1gInSc2	trh
</s>
</p>
<p>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
motivaci	motivace	k1gFnSc4	motivace
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
vycházející	vycházející	k2eAgFnSc4d1	vycházející
z	z	k7c2	z
hledání	hledání	k1gNnSc2	hledání
zisku	zisk	k1gInSc2	zisk
a	a	k8xC	a
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
motivaci	motivace	k1gFnSc6	motivace
je	být	k5eAaImIp3nS	být
závislýKarl	závislýKarl	k1gMnSc1	závislýKarl
Marx	Marx	k1gMnSc1	Marx
definuje	definovat	k5eAaBmIp3nS	definovat
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
jako	jako	k8xC	jako
výrobní	výrobní	k2eAgInSc4d1	výrobní
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c4	na
námezdní	námezdní	k2eAgFnSc4d1	námezdní
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
<g/>
Pojem	pojem	k1gInSc1	pojem
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
marxistickou	marxistický	k2eAgFnSc7d1	marxistická
teorií	teorie	k1gFnSc7	teorie
jako	jako	k8xC	jako
negativní	negativní	k2eAgNnSc1d1	negativní
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
tržní	tržní	k2eAgNnSc4d1	tržní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existovali	existovat	k5eAaImAgMnP	existovat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
teoretici	teoretik	k1gMnPc1	teoretik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
či	či	k8xC	či
oné	onen	k3xDgFnSc3	onen
podobě	podoba	k1gFnSc3	podoba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
dával	dávat	k5eAaImAgMnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
protestantismem	protestantismus	k1gInSc7	protestantismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
definice	definice	k1gFnSc1	definice
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
jako	jako	k8xC	jako
takového	takový	k3xDgNnSc2	takový
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
termín	termín	k1gInSc1	termín
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
pejorativním	pejorativní	k2eAgNnSc7d1	pejorativní
označením	označení	k1gNnSc7	označení
nedostatků	nedostatek	k1gInPc2	nedostatek
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
používán	používat	k5eAaImNgInS	používat
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
především	především	k6eAd1	především
levicově	levicově	k6eAd1	levicově
smýšlejícími	smýšlející	k2eAgFnPc7d1	smýšlející
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
začal	začít	k5eAaPmAgInS	začít
představovat	představovat	k5eAaImF	představovat
ale	ale	k8xC	ale
i	i	k9	i
symbol	symbol	k1gInSc4	symbol
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
protiklad	protiklad	k1gInSc1	protiklad
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
východního	východní	k2eAgInSc2d1	východní
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
alternativy	alternativa	k1gFnSc2	alternativa
==	==	k?	==
</s>
</p>
<p>
<s>
Kapitalismus	kapitalismus	k1gInSc1	kapitalismus
je	být	k5eAaImIp3nS	být
kritizován	kritizovat	k5eAaImNgInS	kritizovat
kvůli	kvůli	k7c3	kvůli
údajným	údajný	k2eAgInPc3d1	údajný
bezcitným	bezcitný	k2eAgInPc3d1	bezcitný
bojům	boj	k1gInPc3	boj
<g/>
,	,	kIx,	,
válkám	válka	k1gFnPc3	válka
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ropu	ropa	k1gFnSc4	ropa
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgFnPc4d1	pracovní
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odpůrci	odpůrce	k1gMnPc1	odpůrce
této	tento	k3xDgFnSc2	tento
politické	politický	k2eAgFnSc2d1	politická
filozofie	filozofie	k1gFnSc2	filozofie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
společně	společně	k6eAd1	společně
něco	něco	k3yInSc4	něco
užitečného	užitečný	k2eAgNnSc2d1	užitečné
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
posílání	posílání	k1gNnSc4	posílání
do	do	k7c2	do
nerovné	rovný	k2eNgFnSc2d1	nerovná
soutěže	soutěž	k1gFnSc2	soutěž
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
získávání	získávání	k1gNnSc6	získávání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vítězí	vítězit	k5eAaImIp3nS	vítězit
jen	jen	k9	jen
hrstka	hrstka	k1gFnSc1	hrstka
jedinců	jedinec	k1gMnPc2	jedinec
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Tihle	tenhle	k3xDgMnPc1	tenhle
jedinci	jedinec	k1gMnPc1	jedinec
postupně	postupně	k6eAd1	postupně
ovládají	ovládat	k5eAaImIp3nP	ovládat
úplně	úplně	k6eAd1	úplně
všechno	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
udržení	udržení	k1gNnSc1	udržení
si	se	k3xPyFc3	se
svých	svůj	k3xOyFgNnPc2	svůj
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
zisků	zisk	k1gInPc2	zisk
<g/>
,	,	kIx,	,
manipulují	manipulovat	k5eAaImIp3nP	manipulovat
veřejným	veřejný	k2eAgNnSc7d1	veřejné
míněním	mínění	k1gNnSc7	mínění
<g/>
,	,	kIx,	,
omezují	omezovat	k5eAaImIp3nP	omezovat
lidské	lidský	k2eAgFnPc4d1	lidská
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
bezcitně	bezcitně	k6eAd1	bezcitně
vykořisťují	vykořisťovat	k5eAaImIp3nP	vykořisťovat
pracující	pracující	k2eAgMnPc1d1	pracující
<g/>
,	,	kIx,	,
rozpoutávají	rozpoutávat	k5eAaImIp3nP	rozpoutávat
války	válka	k1gFnSc2	válka
nebo	nebo	k8xC	nebo
bezohledně	bezohledně	k6eAd1	bezohledně
drancují	drancovat	k5eAaImIp3nP	drancovat
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
alternativám	alternativa	k1gFnPc3	alternativa
systémů	systém	k1gInPc2	systém
společenského	společenský	k2eAgNnSc2d1	společenské
uspořádání	uspořádání	k1gNnSc2	uspořádání
a	a	k8xC	a
reformám	reforma	k1gFnPc3	reforma
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
samospráva	samospráva	k1gFnSc1	samospráva
pracujících	pracující	k1gMnPc2	pracující
participativní	participativní	k2eAgFnSc1d1	participativní
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc1d1	základní
nepodmíněný	podmíněný	k2eNgInSc1d1	nepodmíněný
příjem	příjem	k1gInSc1	příjem
<g/>
,	,	kIx,	,
komunismus	komunismus	k1gInSc1	komunismus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ekonomie	ekonomie	k1gFnSc1	ekonomie
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
úplného	úplný	k2eAgInSc2d1	úplný
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
pravicoví	pravicový	k2eAgMnPc1d1	pravicový
libertariáni	libertarián	k1gMnPc1	libertarián
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
tzv.	tzv.	kA	tzv.
kamarádšoft	kamarádšoft	k?	kamarádšoft
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
(	(	kIx(	(
<g/>
jánabráchismus	jánabráchismus	k1gInSc1	jánabráchismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
nich	on	k3xPp3gFnPc2	on
zvýhodňovány	zvýhodňovat	k5eAaImNgFnP	zvýhodňovat
některé	některý	k3yIgFnPc1	některý
banky	banka	k1gFnPc1	banka
a	a	k8xC	a
korporace	korporace	k1gFnPc1	korporace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podle	podle	k7c2	podle
libertariánů	libertarián	k1gMnPc2	libertarián
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
principy	princip	k1gInPc7	princip
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
formu	forma	k1gFnSc4	forma
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapitál	kapitál	k1gInSc1	kapitál
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
dědičný	dědičný	k2eAgInSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Gatsbyho	Gatsbyze	k6eAd1	Gatsbyze
křivka	křivka	k1gFnSc1	křivka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sociální	sociální	k2eAgFnSc1d1	sociální
nerovnost	nerovnost	k1gFnSc1	nerovnost
(	(	kIx(	(
<g/>
Giniho	Gini	k1gMnSc2	Gini
koeficient	koeficient	k1gInSc1	koeficient
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nadále	nadále	k6eAd1	nadále
děděná	děděný	k2eAgFnSc1d1	děděná
jako	jako	k8xS	jako
za	za	k7c2	za
feudalismu	feudalismus	k1gInSc2	feudalismus
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
nenabízí	nabízet	k5eNaImIp3nS	nabízet
rovné	rovný	k2eAgFnPc4d1	rovná
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marxisté	marxista	k1gMnPc1	marxista
považují	považovat	k5eAaImIp3nP	považovat
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
za	za	k7c4	za
poslední	poslední	k2eAgNnSc4d1	poslední
období	období	k1gNnSc4	období
nerovné	rovný	k2eNgFnSc2d1	nerovná
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
marxismu	marxismus	k1gInSc2	marxismus
vykořisťování	vykořisťování	k1gNnSc2	vykořisťování
a	a	k8xC	a
odcizení	odcizení	k1gNnSc2	odcizení
člověka	člověk	k1gMnSc2	člověk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dělník	dělník	k1gMnSc1	dělník
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tvoří	tvořit	k5eAaImIp3nP	tvořit
nadhodnotu	nadhodnota	k1gFnSc4	nadhodnota
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
výrobek	výrobek	k1gInSc1	výrobek
neslouží	sloužit	k5eNaImIp3nS	sloužit
ani	ani	k8xC	ani
jemu	on	k3xPp3gMnSc3	on
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
však	však	k9	však
se	se	k3xPyFc4	se
nestává	stávat	k5eNaImIp3nS	stávat
jeho	jeho	k3xOp3gInSc7	jeho
vlastním	vlastní	k2eAgInSc7d1	vlastní
předmětem	předmět	k1gInSc7	předmět
směny	směna	k1gFnSc2	směna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
konečného	konečný	k2eAgInSc2d1	konečný
výrobku	výrobek	k1gInSc2	výrobek
odcizen	odcizen	k2eAgMnSc1d1	odcizen
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
nepodílí	podílet	k5eNaImIp3nS	podílet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
směně	směna	k1gFnSc6	směna
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
tedy	tedy	k9	tedy
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
výnos	výnos	k1gInSc1	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Výrobek	výrobek	k1gInSc1	výrobek
patří	patřit	k5eAaImIp3nS	patřit
kapitalistovi	kapitalista	k1gMnSc3	kapitalista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
bohatne	bohatnout	k5eAaImIp3nS	bohatnout
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
odskutečněných	odskutečněný	k2eAgInPc2d1	odskutečněný
<g/>
,	,	kIx,	,
od	od	k7c2	od
výsledků	výsledek	k1gInPc2	výsledek
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
oddělených	oddělený	k2eAgInPc2d1	oddělený
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
samotných	samotný	k2eAgMnPc2d1	samotný
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
zisk	zisk	k1gInSc4	zisk
jeho	jeho	k3xOp3gNnSc2	jeho
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
dělníka	dělník	k1gMnSc4	dělník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
výrobku	výrobek	k1gInSc2	výrobek
účast	účast	k1gFnSc1	účast
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odskutečněn	odskutečnit	k5eAaPmNgMnS	odskutečnit
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
podstaty	podstata	k1gFnSc2	podstata
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
hodnotu	hodnota	k1gFnSc4	hodnota
pro	pro	k7c4	pro
kapitalistu	kapitalista	k1gMnSc4	kapitalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leninismus	leninismus	k1gInSc1	leninismus
přidává	přidávat	k5eAaImIp3nS	přidávat
pojem	pojem	k1gInSc1	pojem
imperialismus	imperialismus	k1gInSc1	imperialismus
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
Lenin	Lenin	k1gMnSc1	Lenin
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
označil	označit	k5eAaPmAgMnS	označit
soudobou	soudobý	k2eAgFnSc4d1	soudobá
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
něj	on	k3xPp3gInSc2	on
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc3	fáze
vývoje	vývoj	k1gInSc2	vývoj
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
charakterizovanou	charakterizovaný	k2eAgFnSc7d1	charakterizovaná
nadvládou	nadvláda	k1gFnSc7	nadvláda
monopolů	monopol	k1gInPc2	monopol
a	a	k8xC	a
dělením	dělení	k1gNnSc7	dělení
světa	svět	k1gInSc2	svět
mezi	mezi	k7c4	mezi
světové	světový	k2eAgFnPc4d1	světová
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přínosy	přínos	k1gInPc1	přínos
a	a	k8xC	a
obhajoba	obhajoba	k1gFnSc1	obhajoba
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
škola	škola	k1gFnSc1	škola
===	===	k?	===
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
ekonomů	ekonom	k1gMnPc2	ekonom
rakouské	rakouský	k2eAgFnSc2d1	rakouská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Mises	Mises	k1gInSc1	Mises
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
volnému	volný	k2eAgInSc3d1	volný
trhu	trh	k1gInSc3	trh
<g/>
,	,	kIx,	,
osobnímu	osobní	k2eAgNnSc3d1	osobní
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
a	a	k8xC	a
dobrovolné	dobrovolný	k2eAgFnSc3d1	dobrovolná
směně	směna	k1gFnSc3	směna
možné	možný	k2eAgFnSc3d1	možná
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
maximálního	maximální	k2eAgNnSc2d1	maximální
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
naplnění	naplnění	k1gNnSc2	naplnění
lidských	lidský	k2eAgFnPc2d1	lidská
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
díky	díky	k7c3	díky
soukromému	soukromý	k2eAgNnSc3d1	soukromé
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
výrobních	výrobní	k2eAgInPc2d1	výrobní
statků	statek	k1gInPc2	statek
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
motivaci	motivace	k1gFnSc3	motivace
spořit	spořit	k5eAaImF	spořit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
akumulovat	akumulovat	k5eAaBmF	akumulovat
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
produktivitu	produktivita	k1gFnSc4	produktivita
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
je	být	k5eAaImIp3nS	být
spjato	spjat	k2eAgNnSc1d1	spjato
i	i	k8xC	i
ohromné	ohromný	k2eAgNnSc1d1	ohromné
zrychlení	zrychlení	k1gNnSc1	zrychlení
bohatnutí	bohatnutí	k1gNnSc2	bohatnutí
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
popsal	popsat	k5eAaPmAgMnS	popsat
již	již	k6eAd1	již
Adam	Adam	k1gMnSc1	Adam
Smith	Smith	k1gMnSc1	Smith
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Bohatství	bohatství	k1gNnSc2	bohatství
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnPc1	společnost
bohatnou	bohatnout	k5eAaImIp3nP	bohatnout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gInPc3	on
umožněna	umožnit	k5eAaPmNgFnS	umožnit
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
směna	směna	k1gFnSc1	směna
a	a	k8xC	a
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
i	i	k9	i
zdánlivě	zdánlivě	k6eAd1	zdánlivě
špatné	špatný	k2eAgFnPc4d1	špatná
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
chamtivost	chamtivost	k1gFnSc4	chamtivost
<g/>
,	,	kIx,	,
projeví	projevit	k5eAaPmIp3nS	projevit
snahou	snaha	k1gFnSc7	snaha
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
uspokojit	uspokojit	k5eAaPmF	uspokojit
zákazníka	zákazník	k1gMnSc4	zákazník
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
obohacení	obohacení	k1gNnSc3	obohacení
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
transakce	transakce	k1gFnSc2	transakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kapitalismus	kapitalismus	k1gInSc4	kapitalismus
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
i	i	k9	i
kupónová	kupónový	k2eAgFnSc1d1	kupónová
privatizace	privatizace	k1gFnSc1	privatizace
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
symbolický	symbolický	k2eAgInSc4d1	symbolický
poplatek	poplatek	k1gInSc4	poplatek
se	se	k3xPyFc4	se
občan	občan	k1gMnSc1	občan
stal	stát	k5eAaPmAgMnS	stát
akcionářem	akcionář	k1gMnSc7	akcionář
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
díky	díky	k7c3	díky
kupónové	kupónový	k2eAgFnSc3d1	kupónová
privatizaci	privatizace	k1gFnSc3	privatizace
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
tunelování	tunelování	k1gNnSc3	tunelování
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgMnPc4d1	hlavní
aktéry	aktér	k1gMnPc4	aktér
této	tento	k3xDgFnSc2	tento
privatizace	privatizace	k1gFnSc2	privatizace
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ježek	Ježek	k1gMnSc1	Ježek
a	a	k8xC	a
Dušan	Dušan	k1gMnSc1	Dušan
Tříska	Tříska	k1gMnSc1	Tříska
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
privatizaci	privatizace	k1gFnSc3	privatizace
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
politických	politický	k2eAgFnPc2d1	politická
kauz	kauza	k1gFnPc2	kauza
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Kauza	kauza	k1gFnSc1	kauza
harvardských	harvardský	k2eAgInPc2d1	harvardský
fondů	fond	k1gInPc2	fond
<g/>
,	,	kIx,	,
Kauza	kauza	k1gFnSc1	kauza
bytů	byt	k1gInPc2	byt
OKD	OKD	kA	OKD
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BRAUDEL	BRAUDEL	kA	BRAUDEL
<g/>
,	,	kIx,	,
Fernand	Fernand	k1gInSc1	Fernand
<g/>
.	.	kIx.	.
</s>
<s>
Dynamika	dynamika	k1gFnSc1	dynamika
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
81	[number]	k4	81
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
193	[number]	k4	193
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRÜN	GRÜN	kA	GRÜN
<g/>
,	,	kIx,	,
Max	max	kA	max
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
dopisy	dopis	k1gInPc1	dopis
Pospischielovi	Pospischiel	k1gMnSc3	Pospischiel
<g/>
.	.	kIx.	.
</s>
<s>
Doslov	doslov	k1gInSc1	doslov
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NZB	NZB	kA	NZB
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
274	[number]	k4	274
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905864	[number]	k4	905864
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Kdo	kdo	k3yQnSc1	kdo
vládne	vládnout	k5eAaImIp3nS	vládnout
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
Eko-konzult	Ekoonzult	k1gMnSc1	Eko-konzult
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-8079-019-1	[number]	k4	80-8079-019-1
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
</s>
</p>
<p>
<s>
URBAN	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
<g/>
.	.	kIx.	.
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1	kapitalismus
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
<g/>
:	:	kIx,	:
k	k	k7c3	k
otázkám	otázka	k1gFnPc3	otázka
formování	formování	k1gNnSc2	formování
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Martin	Martin	k1gMnSc1	Martin
Sekera	Sekera	k1gMnSc1	Sekera
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
323	[number]	k4	323
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Socialismus	socialismus	k1gInSc1	socialismus
</s>
</p>
<p>
<s>
Komunismus	komunismus	k1gInSc1	komunismus
</s>
</p>
<p>
<s>
Antikapitalismus	antikapitalismus	k1gInSc1	antikapitalismus
</s>
</p>
<p>
<s>
Trh	trh	k1gInSc1	trh
(	(	kIx(	(
<g/>
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Globalizace	globalizace	k1gFnSc1	globalizace
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgInSc4d1	sociální
darwinismus	darwinismus	k1gInSc4	darwinismus
</s>
</p>
<p>
<s>
Kumpánský	kumpánský	k2eAgInSc4d1	kumpánský
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
(	(	kIx(	(
<g/>
crony	cron	k1gInPc4	cron
capitalism	capitalism	k1gInSc1	capitalism
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Kapitalismus	kapitalismus	k1gInSc1	kapitalismus
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
