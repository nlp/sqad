<s>
Genocida	genocida	k1gFnSc1	genocida
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
genocidium	genocidium	k1gNnSc1	genocidium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zločin	zločin	k1gInSc1	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
definovaný	definovaný	k2eAgInSc1d1	definovaný
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
trestním	trestní	k2eAgNnSc7d1	trestní
právem	právo	k1gNnSc7	právo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
a	a	k8xC	a
systematické	systematický	k2eAgNnSc1d1	systematické
zničení	zničení	k1gNnSc1	zničení
<g/>
,	,	kIx,	,
celé	celá	k1gFnPc1	celá
nebo	nebo	k8xC	nebo
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
etnické	etnický	k2eAgFnPc1d1	etnická
<g/>
,	,	kIx,	,
rasové	rasový	k2eAgFnPc1d1	rasová
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnPc1d1	náboženská
nebo	nebo	k8xC	nebo
národnostní	národnostní	k2eAgFnPc1d1	národnostní
skupiny	skupina	k1gFnPc1	skupina
<g/>
"	"	kIx"	"
ačkoliv	ačkoliv	k8xS	ačkoliv
co	co	k3yInSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
část	část	k1gFnSc1	část
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
debaty	debata	k1gFnSc2	debata
právníků	právník	k1gMnPc2	právník
<g/>
.	.	kIx.	.
</s>
