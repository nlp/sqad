<s>
Lech	Lech	k1gMnSc1	Lech
Wałęsa	Wałęs	k1gMnSc2	Wałęs
[	[	kIx(	[
<g/>
vauensa	vauens	k1gMnSc2	vauens
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1943	[number]	k4	1943
Popowo	Popowo	k6eAd1	Popowo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgMnSc1d1	polský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
nezávislých	závislý	k2eNgInPc2d1	nezávislý
odborů	odbor	k1gInPc2	odbor
Solidarita	solidarita	k1gFnSc1	solidarita
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
byl	být	k5eAaImAgMnS	být
elektrikář	elektrikář	k1gMnSc1	elektrikář
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
a	a	k8xC	a
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1980	[number]	k4	1980
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stávky	stávka	k1gFnSc2	stávka
a	a	k8xC	a
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
odborová	odborový	k2eAgFnSc1d1	odborová
organizace	organizace	k1gFnSc1	organizace
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
tesaře	tesař	k1gMnSc2	tesař
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostávalo	dostávat	k5eAaImAgNnS	dostávat
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
výchovy	výchova	k1gFnPc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
věřící	věřící	k2eAgMnSc1d1	věřící
katolík	katolík	k1gMnSc1	katolík
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
víra	víra	k1gFnSc1	víra
pomohla	pomoct	k5eAaPmAgFnS	pomoct
překonat	překonat	k5eAaPmF	překonat
těžké	těžký	k2eAgFnPc4d1	těžká
chvíle	chvíle	k1gFnPc4	chvíle
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
obtížné	obtížný	k2eAgNnSc1d1	obtížné
období	období	k1gNnSc1	období
Solidarity	solidarita	k1gFnSc2	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Gdaňských	gdaňský	k2eAgFnPc2d1	Gdaňská
Leninových	Leninových	k2eAgFnPc2d1	Leninových
loděnic	loděnice	k1gFnPc2	loděnice
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
post	post	k1gInSc4	post
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Danutou	Danutý	k2eAgFnSc7d1	Danutý
Gołoś	Gołoś	k1gFnSc7	Gołoś
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc4	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jaroslaw	Jaroslaw	k1gFnSc2	Jaroslaw
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
členem	člen	k1gMnSc7	člen
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
polského	polský	k2eAgInSc2d1	polský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
polských	polský	k2eAgInPc2d1	polský
protestů	protest	k1gInPc2	protest
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
trestu	trest	k1gInSc2	trest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
ilegální	ilegální	k2eAgFnSc2d1	ilegální
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
členy	člen	k1gInPc4	člen
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
Bogdan	Bogdan	k1gMnSc1	Bogdan
Borusewicz	Borusewicz	k1gMnSc1	Borusewicz
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc1	Andrzej
Gwiazda	Gwiazda	k1gMnSc1	Gwiazda
<g/>
,	,	kIx,	,
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Wyszkowski	Wyszkowsk	k1gFnSc2	Wyszkowsk
<g/>
,	,	kIx,	,
Lech	Lech	k1gMnSc1	Lech
Kaczyński	Kaczyńsk	k1gFnSc2	Kaczyńsk
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Walentynowiczová	Walentynowiczová	k1gFnSc1	Walentynowiczová
<g/>
,	,	kIx,	,
Antoni	Anton	k1gMnPc1	Anton
Sokołowski	Sokołowski	k1gNnPc2	Sokołowski
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
budoucí	budoucí	k2eAgFnSc2d1	budoucí
osobnosti	osobnost	k1gFnSc2	osobnost
polské	polský	k2eAgFnSc2d1	polská
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgInP	začít
protesty	protest	k1gInPc1	protest
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Wałęsa	Wałęs	k1gMnSc4	Wałęs
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
těchto	tento	k3xDgInPc2	tento
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
Gdaňsku	Gdaňsk	k1gInSc2	Gdaňsk
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
zahájily	zahájit	k5eAaPmAgInP	zahájit
rozpad	rozpad	k1gInSc4	rozpad
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
částečně	částečně	k6eAd1	částečně
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
požadavkům	požadavek	k1gInPc3	požadavek
loďařů	loďař	k1gMnPc2	loďař
a	a	k8xC	a
dovolila	dovolit	k5eAaPmAgFnS	dovolit
jim	on	k3xPp3gMnPc3	on
zakládat	zakládat	k5eAaImF	zakládat
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Wałęsa	Wałęs	k1gMnSc2	Wałęs
stal	stát	k5eAaPmAgMnS	stát
odborovým	odborový	k2eAgMnSc7d1	odborový
předákem	předák	k1gMnSc7	předák
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
stanného	stanný	k2eAgNnSc2d1	stanné
práva	právo	k1gNnSc2	právo
vyhlášeného	vyhlášený	k2eAgMnSc4d1	vyhlášený
polským	polský	k2eAgMnSc7d1	polský
vůdcem	vůdce	k1gMnSc7	vůdce
Jaruzelskim	Jaruzelskima	k1gFnPc2	Jaruzelskima
<g/>
.	.	kIx.	.
</s>
<s>
Propuštěn	propustit	k5eAaPmNgInS	propustit
byl	být	k5eAaImAgInS	být
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
po	po	k7c6	po
11	[number]	k4	11
měsících	měsíc	k1gInPc6	měsíc
ve	v	k7c4	v
vězení	vězení	k1gNnSc4	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
loděnic	loděnice	k1gFnPc2	loděnice
na	na	k7c4	na
post	post	k1gInSc4	post
řadového	řadový	k2eAgMnSc2d1	řadový
elektrotechnika	elektrotechnik	k1gMnSc2	elektrotechnik
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
skutečně	skutečně	k6eAd1	skutečně
dostal	dostat	k5eAaPmAgInS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Lech	Lech	k1gMnSc1	Lech
Walesa	Walesa	k1gMnSc1	Walesa
si	se	k3xPyFc3	se
cenu	cena	k1gFnSc4	cena
osobně	osobně	k6eAd1	osobně
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
nepřevzal	převzít	k5eNaPmAgMnS	převzít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mu	on	k3xPp3gNnSc3	on
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
neumožnil	umožnit	k5eNaPmAgInS	umožnit
návrat	návrat	k1gInSc1	návrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
proto	proto	k8xC	proto
převzala	převzít	k5eAaPmAgFnS	převzít
v	v	k7c4	v
zastoupení	zastoupení	k1gNnSc4	zastoupení
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Danuta	Danut	k2eAgFnSc1d1	Danuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
vedl	vést	k5eAaImAgInS	vést
pololegální	pololegální	k2eAgInSc1d1	pololegální
zákonný	zákonný	k2eAgInSc1d1	zákonný
výbor	výbor	k1gInSc1	výbor
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
znovu	znovu	k6eAd1	znovu
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
stávku	stávka	k1gFnSc4	stávka
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zlepšit	zlepšit	k5eAaPmF	zlepšit
postavení	postavení	k1gNnSc4	postavení
Solidarity	solidarita	k1gFnSc2	solidarita
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmdesáti	osmdesát	k4xCc2	osmdesát
dnech	den	k1gInPc6	den
vláda	vláda	k1gFnSc1	vláda
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
jednáním	jednání	k1gNnSc7	jednání
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
přibližně	přibližně	k6eAd1	přibližně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
.	.	kIx.	.
</s>
<s>
Solidarita	solidarita	k1gFnSc1	solidarita
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
legitimní	legitimní	k2eAgNnSc1d1	legitimní
nekomunistickou	komunistický	k2eNgFnSc7d1	nekomunistická
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
první	první	k4xOgFnPc4	první
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
nekomunistický	komunistický	k2eNgInSc4d1	nekomunistický
politický	politický	k2eAgInSc4d1	politický
útvar	útvar	k1gInSc4	útvar
v	v	k7c6	v
sovětském	sovětský	k2eAgInSc6d1	sovětský
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
už	už	k9	už
Wałęsovi	Wałęs	k1gMnSc3	Wałęs
nic	nic	k3yNnSc1	nic
nebránilo	bránit	k5eNaImAgNnS	bránit
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
realizací	realizace	k1gFnSc7	realizace
přestavby	přestavba	k1gFnSc2	přestavba
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sice	sice	k8xC	sice
stále	stále	k6eAd1	stále
teoreticky	teoreticky	k6eAd1	teoreticky
bylo	být	k5eAaImAgNnS	být
komunistickým	komunistický	k2eAgInSc7d1	komunistický
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyjednaná	vyjednaný	k2eAgFnSc1d1	vyjednaná
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
demokratickým	demokratický	k2eAgInPc3d1	demokratický
ústupkům	ústupek	k1gInPc3	ústupek
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
kontaktu	kontakt	k1gInSc3	kontakt
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1990	[number]	k4	1990
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
nekomunistického	komunistický	k2eNgNnSc2d1	nekomunistické
Polska	Polsko	k1gNnSc2	Polsko
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
éra	éra	k1gFnSc1	éra
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
výměnou	výměna	k1gFnSc7	výměna
vlád	vláda	k1gFnPc2	vláda
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Sám	sám	k3xTgMnSc1	sám
Wałęsa	Wałęs	k1gMnSc4	Wałęs
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
něco	něco	k3yInSc1	něco
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
transformací	transformace	k1gFnSc7	transformace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
poražen	porazit	k5eAaPmNgMnS	porazit
Aleksandrem	Aleksandr	k1gInSc7	Aleksandr
Kwaśniewským	Kwaśniewský	k2eAgInSc7d1	Kwaśniewský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
znovu	znovu	k6eAd1	znovu
kandidovat	kandidovat	k5eAaImF	kandidovat
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
ovšem	ovšem	k9	ovšem
necelé	celý	k2eNgInPc4d1	necelý
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pro	pro	k7c4	pro
politické	politický	k2eAgFnPc4d1	politická
neshody	neshoda	k1gFnPc4	neshoda
ze	z	k7c2	z
Solidarity	solidarita	k1gFnSc2	solidarita
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Odborům	odbor	k1gInPc3	odbor
vytýkal	vytýkat	k5eAaImAgInS	vytýkat
především	především	k9	především
podporu	podpora	k1gFnSc4	podpora
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
Lecha	Lech	k1gMnSc2	Lech
a	a	k8xC	a
Jarosława	Jarosławus	k1gMnSc2	Jarosławus
Kaczyńských	Kaczyńských	k2eAgMnSc2d1	Kaczyńských
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
prezidentury	prezidentura	k1gFnSc2	prezidentura
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
politicky	politicky	k6eAd1	politicky
zaměřeným	zaměřený	k2eAgFnPc3d1	zaměřená
přednáškám	přednáška	k1gFnPc3	přednáška
na	na	k7c6	na
evropských	evropský	k2eAgFnPc6d1	Evropská
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
odtajněné	odtajněný	k2eAgInPc1d1	odtajněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
podezřívají	podezřívat	k5eAaImIp3nP	podezřívat
Wałęsu	Wałęsa	k1gFnSc4	Wałęsa
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
veden	vést	k5eAaImNgInS	vést
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Bolek	Bolek	k1gMnSc1	Bolek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
podezření	podezření	k1gNnSc1	podezření
se	se	k3xPyFc4	se
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Institut	institut	k1gInSc1	institut
národní	národní	k2eAgFnSc2d1	národní
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
IPN	IPN	kA	IPN
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Czesława	Czesławus	k1gMnSc2	Czesławus
Kiszcaka	Kiszcak	k1gMnSc2	Kiszcak
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
dva	dva	k4xCgInPc4	dva
svazky	svazek	k1gInPc4	svazek
dokládající	dokládající	k2eAgFnSc4d1	dokládající
tuto	tento	k3xDgFnSc4	tento
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
s	s	k7c7	s
Wałęsovým	Wałęsův	k2eAgInSc7d1	Wałęsův
vlastnoručním	vlastnoruční	k2eAgInSc7d1	vlastnoruční
podpisem	podpis	k1gInSc7	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Wałęsu	Wałęsa	k1gFnSc4	Wałęsa
polský	polský	k2eAgInSc1d1	polský
soud	soud	k1gInSc1	soud
očistil	očistit	k5eAaPmAgInS	očistit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
sám	sám	k3xTgMnSc1	sám
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
podpis	podpis	k1gInSc4	podpis
spolupráce	spolupráce	k1gFnSc2	spolupráce
vzápětí	vzápětí	k6eAd1	vzápětí
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c6	o
"	"	kIx"	"
<g/>
fiktivní	fiktivní	k2eAgFnSc6d1	fiktivní
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgMnS	učinit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prosby	prosba	k1gFnSc2	prosba
agenta	agent	k1gMnSc2	agent
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pouze	pouze	k6eAd1	pouze
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
získat	získat	k5eAaPmF	získat
důvod	důvod	k1gInSc4	důvod
pro	pro	k7c4	pro
vyúčtování	vyúčtování	k1gNnSc4	vyúčtování
chybějících	chybějící	k2eAgInPc2d1	chybějící
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
operativní	operativní	k2eAgFnSc4d1	operativní
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Gdaňsku	Gdaňsk	k1gInSc6	Gdaňsk
přejmenováno	přejmenován	k2eAgNnSc1d1	přejmenováno
na	na	k7c4	na
"	"	kIx"	"
<g/>
Letiště	letiště	k1gNnSc4	letiště
Lecha	Lech	k1gMnSc2	Lech
Wałęsy	Wałęsa	k1gFnSc2	Wałęsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
logo	logo	k1gNnSc4	logo
letiště	letiště	k1gNnSc2	letiště
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
i	i	k9	i
z	z	k7c2	z
Wałęsova	Wałęsův	k2eAgInSc2d1	Wałęsův
podpisu	podpis	k1gInSc2	podpis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Lech	lecha	k1gFnPc2	lecha
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lech	lecha	k1gFnPc2	lecha
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Lech	lech	k1gInSc1	lech
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
