<s>
Původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
byl	být	k5eAaImAgMnS	být
elektrikář	elektrikář	k1gMnSc1	elektrikář
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
a	a	k8xC	a
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1980	[number]	k4	1980
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stávky	stávka	k1gFnSc2	stávka
a	a	k8xC	a
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
odborová	odborový	k2eAgFnSc1d1	odborová
organizace	organizace	k1gFnSc1	organizace
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
