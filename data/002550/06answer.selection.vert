<s>
Psychóza	psychóza	k1gFnSc1	psychóza
je	být	k5eAaImIp3nS	být
závažný	závažný	k2eAgInSc4d1	závažný
duševní	duševní	k2eAgInSc4d1	duševní
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
neschopnost	neschopnost	k1gFnSc4	neschopnost
chovat	chovat	k5eAaImF	chovat
se	se	k3xPyFc4	se
a	a	k8xC	a
jednat	jednat	k5eAaImF	jednat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
.	.	kIx.	.
</s>
