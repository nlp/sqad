<s>
Psychóza	psychóza	k1gFnSc1	psychóza
je	být	k5eAaImIp3nS	být
závažný	závažný	k2eAgInSc4d1	závažný
duševní	duševní	k2eAgInSc4d1	duševní
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
neschopnost	neschopnost	k1gFnSc4	neschopnost
chovat	chovat	k5eAaImF	chovat
se	se	k3xPyFc4	se
a	a	k8xC	a
jednat	jednat	k5eAaImF	jednat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
disociace	disociace	k1gFnSc1	disociace
mezi	mezi	k7c7	mezi
vnímáním	vnímání	k1gNnSc7	vnímání
<g/>
,	,	kIx,	,
chováním	chování	k1gNnSc7	chování
a	a	k8xC	a
prožíváním	prožívání	k1gNnSc7	prožívání
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
vztah	vztah	k1gInSc1	vztah
nemocného	nemocný	k1gMnSc2	nemocný
k	k	k7c3	k
realitě	realita	k1gFnSc3	realita
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
psychózou	psychóza	k1gFnSc7	psychóza
a	a	k8xC	a
neurózou	neuróza	k1gFnSc7	neuróza
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
psychóze	psychóza	k1gFnSc6	psychóza
postiženému	postižený	k1gMnSc3	postižený
chybí	chybět	k5eAaImIp3nS	chybět
nadhled	nadhled	k1gInSc1	nadhled
(	(	kIx(	(
<g/>
svým	svůj	k3xOyFgInPc3	svůj
bludům	blud	k1gInPc3	blud
bezmezně	bezmezně	k6eAd1	bezmezně
věří	věřit	k5eAaImIp3nS	věřit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
neurotický	neurotický	k2eAgMnSc1d1	neurotický
pacient	pacient	k1gMnSc1	pacient
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
vědom	vědom	k2eAgMnSc1d1	vědom
(	(	kIx(	(
<g/>
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
vjemy	vjem	k1gInPc4	vjem
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psychózy	psychóza	k1gFnPc1	psychóza
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
provázeny	provázen	k2eAgInPc1d1	provázen
abnormálními	abnormální	k2eAgInPc7d1	abnormální
biochemickými	biochemický	k2eAgInPc7d1	biochemický
pochody	pochod	k1gInPc7	pochod
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
části	část	k1gFnSc2	část
postižených	postižený	k2eAgFnPc2d1	postižená
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
onemocnění	onemocnění	k1gNnPc2	onemocnění
podílejí	podílet	k5eAaImIp3nP	podílet
genetické	genetický	k2eAgInPc1d1	genetický
předpoklady	předpoklad	k1gInPc1	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Psychotické	psychotický	k2eAgInPc1d1	psychotický
projevy	projev	k1gInPc1	projev
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsoben	k2eAgInPc1d1	způsoben
také	také	k9	také
jinými	jiný	k2eAgFnPc7d1	jiná
chorobami	choroba	k1gFnPc7	choroba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
poruchami	porucha	k1gFnPc7	porucha
látkové	látkový	k2eAgFnSc2d1	látková
výměny	výměna	k1gFnSc2	výměna
nebo	nebo	k8xC	nebo
nádorem	nádor	k1gInSc7	nádor
na	na	k7c6	na
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
účinkem	účinek	k1gInSc7	účinek
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
drog	droga	k1gFnPc2	droga
nebo	nebo	k8xC	nebo
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
predisponovaných	predisponovaný	k2eAgFnPc2d1	predisponovaná
osob	osoba	k1gFnPc2	osoba
může	moct	k5eAaImIp3nS	moct
psychózu	psychóza	k1gFnSc4	psychóza
vyvolat	vyvolat	k5eAaPmF	vyvolat
též	též	k9	též
abnormální	abnormální	k2eAgFnSc1d1	abnormální
psychická	psychický	k2eAgFnSc1d1	psychická
zátěž	zátěž	k1gFnSc1	zátěž
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
biochemických	biochemický	k2eAgFnPc2d1	biochemická
teorií	teorie	k1gFnPc2	teorie
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
nejpravděpodobnější	pravděpodobný	k2eAgNnSc1d3	nejpravděpodobnější
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
působení	působení	k1gNnSc4	působení
antipsychotik	antipsychotika	k1gFnPc2	antipsychotika
<g/>
)	)	kIx)	)
dopaminová	dopaminový	k2eAgFnSc1d1	dopaminová
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tonická	tonický	k2eAgFnSc1d1	tonická
komponenta	komponenta	k1gFnSc1	komponenta
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
extrasynaptická	extrasynaptický	k2eAgFnSc1d1	extrasynaptický
<g/>
)	)	kIx)	)
tvorby	tvorba	k1gFnSc2	tvorba
dopaminu	dopamin	k1gInSc2	dopamin
je	být	k5eAaImIp3nS	být
snížená	snížený	k2eAgFnSc1d1	snížená
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
fázický	fázický	k2eAgInSc1d1	fázický
přenos	přenos	k1gInSc1	přenos
(	(	kIx(	(
<g/>
synaptický	synaptický	k2eAgMnSc1d1	synaptický
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
teorií	teorie	k1gFnPc2	teorie
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
serotoninu	serotonin	k1gInSc2	serotonin
(	(	kIx(	(
<g/>
serotoninové	serotoninový	k2eAgFnPc1d1	serotoninová
dráhy	dráha	k1gFnPc1	dráha
působí	působit	k5eAaImIp3nP	působit
inhibičně	inhibičně	k6eAd1	inhibičně
na	na	k7c4	na
dopaminový	dopaminový	k2eAgInSc4d1	dopaminový
systém	systém	k1gInSc4	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vliv	vliv	k1gInSc1	vliv
excitačních	excitační	k2eAgFnPc2d1	excitační
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
glutamát	glutamát	k1gInSc1	glutamát
a	a	k8xC	a
aspartát	aspartát	k1gInSc1	aspartát
<g/>
)	)	kIx)	)
-	-	kIx~	-
kdy	kdy	k6eAd1	kdy
např.	např.	kA	např.
hypofunkce	hypofunkce	k1gFnSc2	hypofunkce
NMDA	NMDA	kA	NMDA
(	(	kIx(	(
<g/>
N-methyl-D-aspartátových	Nethyl-Dspartátův	k2eAgInPc2d1	N-methyl-D-aspartátův
<g/>
)	)	kIx)	)
receptorů	receptor	k1gInPc2	receptor
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
stavy	stav	k1gInPc1	stav
podobné	podobný	k2eAgInPc1d1	podobný
schizofrenii	schizofrenie	k1gFnSc6	schizofrenie
-	-	kIx~	-
toto	tento	k3xDgNnSc4	tento
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
např.	např.	kA	např.
droga	droga	k1gFnSc1	droga
PCP	PCP	kA	PCP
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
:	:	kIx,	:
halucinace	halucinace	k1gFnSc1	halucinace
-	-	kIx~	-
typicky	typicky	k6eAd1	typicky
sluchové	sluchový	k2eAgNnSc1d1	sluchové
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
člověk	člověk	k1gMnSc1	člověk
slyší	slyšet	k5eAaImIp3nS	slyšet
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
přikazují	přikazovat	k5eAaImIp3nP	přikazovat
<g/>
,	,	kIx,	,
pomlouvají	pomlouvat	k5eAaImIp3nP	pomlouvat
či	či	k8xC	či
rozmlouvají	rozmlouvat	k5eAaImIp3nP	rozmlouvat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
zesměšňují	zesměšňovat	k5eAaImIp3nP	zesměšňovat
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
vyhrožují	vyhrožovat	k5eAaImIp3nP	vyhrožovat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bludy	blud	k1gInPc1	blud
-	-	kIx~	-
typicky	typicky	k6eAd1	typicky
paranoidní	paranoidní	k2eAgFnSc1d1	paranoidní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
sledován	sledován	k2eAgMnSc1d1	sledován
<g/>
,	,	kIx,	,
odposloucháván	odposloucháván	k2eAgMnSc1d1	odposloucháván
<g/>
,	,	kIx,	,
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
apod.	apod.	kA	apod.
Mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
poruchy	porucha	k1gFnPc4	porucha
emotivity	emotivita	k1gFnSc2	emotivita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
např.	např.	kA	např.
směje	smát	k5eAaImIp3nS	smát
nepřiměřeně	přiměřeně	k6eNd1	přiměřeně
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
tzv.	tzv.	kA	tzv.
katatonní	katatonní	k2eAgInPc1d1	katatonní
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
nehýbe	hýbat	k5eNaImIp3nS	hýbat
<g/>
,	,	kIx,	,
strnule	strnule	k6eAd1	strnule
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
polohách	poloha	k1gFnPc6	poloha
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
neklidný	klidný	k2eNgMnSc1d1	neklidný
<g/>
,	,	kIx,	,
agresivní	agresivní	k2eAgMnSc1d1	agresivní
a	a	k8xC	a
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
či	či	k8xC	či
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
psychotické	psychotický	k2eAgInPc1d1	psychotický
prožitky	prožitek	k1gInPc1	prožitek
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
schizofrenie	schizofrenie	k1gFnSc1	schizofrenie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
při	při	k7c6	při
užívání	užívání	k1gNnSc6	užívání
alkoholu	alkohol	k1gInSc2	alkohol
či	či	k8xC	či
drog	droga	k1gFnPc2	droga
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
stimulancia	stimulans	k1gNnPc1	stimulans
-	-	kIx~	-
pervitin	pervitin	k1gInSc1	pervitin
či	či	k8xC	či
kokain	kokain	k1gInSc1	kokain
případně	případně	k6eAd1	případně
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
užívání	užívání	k1gNnSc4	užívání
geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
marihuany	marihuana	k1gFnSc2	marihuana
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
u	u	k7c2	u
demencí	demence	k1gFnPc2	demence
či	či	k8xC	či
úrazů	úraz	k1gInPc2	úraz
s	s	k7c7	s
postižením	postižení	k1gNnSc7	postižení
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
přechodně	přechodně	k6eAd1	přechodně
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
po	po	k7c6	po
operacích	operace	k1gFnPc6	operace
<g/>
.	.	kIx.	.
</s>
<s>
Psychotické	psychotický	k2eAgInPc1d1	psychotický
prožitky	prožitek	k1gInPc1	prožitek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
u	u	k7c2	u
těžkých	těžký	k2eAgFnPc2d1	těžká
mánií	mánie	k1gFnPc2	mánie
či	či	k8xC	či
depresivních	depresivní	k2eAgInPc2d1	depresivní
stavů	stav	k1gInPc2	stav
<g/>
;	;	kIx,	;
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
ale	ale	k9	ale
projevy	projev	k1gInPc1	projev
trochu	trochu	k6eAd1	trochu
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
předešlých	předešlý	k2eAgInPc2d1	předešlý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
polygenní	polygenní	k2eAgFnSc1d1	polygenní
dědičnost	dědičnost	k1gFnSc1	dědičnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vnějším	vnější	k2eAgInPc3d1	vnější
vlivům	vliv	k1gInPc3	vliv
patří	patřit	k5eAaImIp3nS	patřit
-	-	kIx~	-
podvýživa	podvýživa	k1gFnSc1	podvýživa
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
trimestru	trimestr	k1gInSc2	trimestr
<g/>
,	,	kIx,	,
chřipkové	chřipkový	k2eAgNnSc1d1	chřipkové
onemocnění	onemocnění	k1gNnSc1	onemocnění
matky	matka	k1gFnSc2	matka
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
trimestru	trimestr	k1gInSc2	trimestr
<g/>
,	,	kIx,	,
hyperprotektivní	hyperprotektivní	k2eAgMnPc1d1	hyperprotektivní
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
vztah	vztah	k1gInSc4	vztah
rodičů	rodič	k1gMnPc2	rodič
k	k	k7c3	k
dítěti	dítě	k1gNnSc3	dítě
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
stresující	stresující	k2eAgFnPc4d1	stresující
události	událost	k1gFnPc4	událost
v	v	k7c6	v
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
ztráta	ztráta	k1gFnSc1	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
porod	porod	k1gInSc1	porod
<g/>
,	,	kIx,	,
rozvod	rozvod	k1gInSc1	rozvod
<g/>
,	,	kIx,	,
svatba	svatba	k1gFnSc1	svatba
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
psychóza	psychóza	k1gFnSc1	psychóza
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
