<p>
<s>
Maja	Maja	k1gFnSc1	Maja
Alm	Alm	k1gFnSc1	Alm
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dánská	dánský	k2eAgFnSc1d1	dánská
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
běhu	běh	k1gInSc6	běh
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
je	být	k5eAaImIp3nS	být
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
medaile	medaile	k1gFnSc1	medaile
ze	z	k7c2	z
sprintu	sprint	k1gInSc2	sprint
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
běhá	běhat	k5eAaImIp3nS	běhat
za	za	k7c4	za
švédský	švédský	k2eAgInSc4d1	švédský
klub	klub	k1gInSc4	klub
Ulricehamns	Ulricehamns	k1gInSc4	Ulricehamns
OK	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maja	Maja	k1gFnSc1	Maja
Almová	Almový	k2eAgFnSc1d1	Almový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Runners	Runners	k1gInSc4	Runners
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
