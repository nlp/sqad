<p>
<s>
Obec	obec	k1gFnSc1	obec
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Unter	Unter	k1gMnSc1	Unter
Wisternitz	Wisternitz	k1gMnSc1	Wisternitz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
severně	severně	k6eAd1	severně
od	od	k7c2	od
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
(	(	kIx(	(
<g/>
Pálavy	Pálavy	k?	Pálavy
<g/>
)	)	kIx)	)
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Mikulova	Mikulov	k1gInSc2	Mikulov
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
Novomlýnských	novomlýnský	k2eAgFnPc2d1	Novomlýnská
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
316	[number]	k4	316
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
obce	obec	k1gFnSc2	obec
tvoří	tvořit	k5eAaImIp3nP	tvořit
Sklepní	sklepní	k2eAgFnPc1d1	sklepní
ulice	ulice	k1gFnPc1	ulice
s	s	k7c7	s
původními	původní	k2eAgInPc7d1	původní
i	i	k8xC	i
nově	nově	k6eAd1	nově
vybudovanými	vybudovaný	k2eAgInPc7d1	vybudovaný
vinnými	vinný	k2eAgInPc7d1	vinný
sklepy	sklep	k1gInPc7	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Vinařství	vinařství	k1gNnSc1	vinařství
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
tisíciletou	tisíciletý	k2eAgFnSc4d1	tisíciletá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
úpatí	úpatí	k1gNnSc6	úpatí
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napuštění	napuštění	k1gNnSc6	napuštění
Novomlýnských	novomlýnský	k2eAgFnPc2d1	Novomlýnská
nádrží	nádrž	k1gFnPc2	nádrž
se	se	k3xPyFc4	se
obec	obec	k1gFnSc1	obec
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
za	za	k7c7	za
zachovaným	zachovaný	k2eAgInSc7d1	zachovaný
úsekem	úsek	k1gInSc7	úsek
původního	původní	k2eAgNnSc2d1	původní
koryta	koryto	k1gNnSc2	koryto
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
staré	starý	k2eAgFnSc6d1	stará
obchodní	obchodní	k2eAgFnSc6d1	obchodní
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
i	i	k9	i
dnes	dnes	k6eAd1	dnes
spojuje	spojovat	k5eAaImIp3nS	spojovat
Mikulov	Mikulov	k1gInSc1	Mikulov
s	s	k7c7	s
Hustopečemi	Hustopeč	k1gFnPc7	Hustopeč
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
po	po	k7c6	po
hrázi	hráz	k1gFnSc6	hráz
mezi	mezi	k7c7	mezi
střední	střední	k2eAgFnSc7d1	střední
a	a	k8xC	a
dolní	dolní	k2eAgFnSc7d1	dolní
nádrží	nádrž	k1gFnSc7	nádrž
Nových	Nových	k2eAgInPc2d1	Nových
Mlýnů	mlýn	k1gInPc2	mlýn
do	do	k7c2	do
Strachotína	Strachotín	k1gInSc2	Strachotín
<g/>
;	;	kIx,	;
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Pavlova	Pavlův	k2eAgNnSc2d1	Pavlovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
podél	podél	k7c2	podél
toku	tok	k1gInSc2	tok
Dyje	Dyje	k1gFnSc2	Dyje
do	do	k7c2	do
Lednice	Lednice	k1gFnSc2	Lednice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sousedními	sousední	k2eAgFnPc7d1	sousední
obcemi	obec	k1gFnPc7	obec
sídla	sídlo	k1gNnSc2	sídlo
jsou	být	k5eAaImIp3nP	být
Horní	horní	k2eAgFnSc4d1	horní
Věstonice	Věstonice	k1gFnPc4	Věstonice
<g/>
,	,	kIx,	,
Pouzdřany	Pouzdřan	k1gMnPc4	Pouzdřan
<g/>
,	,	kIx,	,
Strachotín	Strachotín	k1gInSc4	Strachotín
<g/>
,	,	kIx,	,
Pasohlávky	Pasohlávka	k1gFnPc4	Pasohlávka
a	a	k8xC	a
Pavlov	Pavlov	k1gInSc4	Pavlov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
obce	obec	k1gFnSc2	obec
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
jak	jak	k6eAd1	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
četné	četný	k2eAgInPc4d1	četný
archeologické	archeologický	k2eAgInPc4d1	archeologický
nálezy	nález	k1gInPc4	nález
zejména	zejména	k9	zejména
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
(	(	kIx(	(
<g/>
stáří	stář	k1gFnSc7	stář
asi	asi	k9	asi
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zpřístupněny	zpřístupněn	k2eAgFnPc4d1	zpřístupněna
veřejnosti	veřejnost	k1gFnPc4	veřejnost
v	v	k7c6	v
zdejším	zdejší	k2eAgNnSc6d1	zdejší
archeologickém	archeologický	k2eAgNnSc6d1	Archeologické
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
zdejším	zdejší	k2eAgInSc7d1	zdejší
archeologickým	archeologický	k2eAgInSc7d1	archeologický
nálezem	nález	k1gInSc7	nález
je	být	k5eAaImIp3nS	být
plastika	plastika	k1gFnSc1	plastika
Věstonická	věstonický	k2eAgFnSc1d1	Věstonická
venuše	venuše	k1gFnSc1	venuše
objevená	objevený	k2eAgFnSc1d1	objevená
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
naleziště	naleziště	k1gNnSc4	naleziště
zapsáno	zapsán	k2eAgNnSc4d1	zapsáno
mezi	mezi	k7c4	mezi
Národní	národní	k2eAgFnPc4d1	národní
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1312	[number]	k4	1312
(	(	kIx(	(
<g/>
Wistanicz	Wistanicz	k1gInSc1	Wistanicz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnáctém	šestnáctý	k4xOgNnSc6	šestnáctý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
novokřtěnci	novokřtěnec	k1gMnPc1	novokřtěnec
–	–	k?	–
habáni	habán	k1gMnPc1	habán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1653	[number]	k4	1653
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1665	[number]	k4	1665
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
farářem	farář	k1gMnSc7	farář
italský	italský	k2eAgMnSc1d1	italský
minorita	minorita	k1gMnSc1	minorita
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Giovanni	Giovanň	k1gMnSc3	Giovanň
Battista	Battista	k1gMnSc1	Battista
Alouisi	Alouis	k1gMnSc3	Alouis
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
měla	mít	k5eAaImAgFnS	mít
obec	obec	k1gFnSc1	obec
688	[number]	k4	688
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
93,3	[number]	k4	93,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
–	–	k?	–
tito	tento	k3xDgMnPc1	tento
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vesměs	vesměs	k6eAd1	vesměs
vysídleni	vysídlit	k5eAaPmNgMnP	vysídlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
žilo	žít	k5eAaImAgNnS	žít
celkem	celek	k1gInSc7	celek
313	[number]	k4	313
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
170	[number]	k4	170
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
143	[number]	k4	143
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
45,3	[number]	k4	45,3
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
provedeném	provedený	k2eAgInSc6d1	provedený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
žilo	žít	k5eAaImAgNnS	žít
294	[number]	k4	294
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
bylo	být	k5eAaImAgNnS	být
(	(	kIx(	(
<g/>
16,3	[number]	k4	16,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
50	[number]	k4	50
do	do	k7c2	do
59	[number]	k4	59
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
tvořily	tvořit	k5eAaImAgFnP	tvořit
10,9	[number]	k4	10,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
senioři	senior	k1gMnPc1	senior
nad	nad	k7c4	nad
70	[number]	k4	70
let	léto	k1gNnPc2	léto
úhrnem	úhrnem	k6eAd1	úhrnem
7,1	[number]	k4	7,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
262	[number]	k4	262
občanů	občan	k1gMnPc2	občan
obce	obec	k1gFnSc2	obec
starších	starší	k1gMnPc2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
mělo	mít	k5eAaImAgNnS	mít
vzdělání	vzdělání	k1gNnSc1	vzdělání
42,4	[number]	k4	42,4
%	%	kIx~	%
střední	střední	k2eAgNnSc1d1	střední
vč.	vč.	k?	vč.
vyučení	vyučení	k1gNnSc1	vyučení
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
maturity	maturita	k1gFnSc2	maturita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
6,1	[number]	k4	6,1
%	%	kIx~	%
a	a	k8xC	a
bez	bez	k7c2	bez
vzdělání	vzdělání	k1gNnSc2	vzdělání
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
0	[number]	k4	0
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cenzu	cenzus	k1gInSc2	cenzus
dále	daleko	k6eAd2	daleko
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
141	[number]	k4	141
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
87,9	[number]	k4	87,9
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
řadilo	řadit	k5eAaImAgNnS	řadit
mezi	mezi	k7c4	mezi
zaměstnané	zaměstnaný	k1gMnPc4	zaměstnaný
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
69,5	[number]	k4	69,5
%	%	kIx~	%
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
2,8	[number]	k4	2,8
%	%	kIx~	%
k	k	k7c3	k
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
účet	účet	k1gInSc4	účet
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
celých	celý	k2eAgNnPc2d1	celé
48,3	[number]	k4	48,3
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
nebylo	být	k5eNaImAgNnS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
svou	svůj	k3xOyFgFnSc4	svůj
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
aktivitu	aktivita	k1gFnSc4	aktivita
uvést	uvést	k5eAaPmF	uvést
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
114	[number]	k4	114
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
38,8	[number]	k4	38,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
82	[number]	k4	82
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
Moravanů	Moravan	k1gMnPc2	Moravan
a	a	k8xC	a
7	[number]	k4	7
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
78	[number]	k4	78
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
svou	svůj	k3xOyFgFnSc4	svůj
národnost	národnost	k1gFnSc4	národnost
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
i	i	k9	i
příslušnost	příslušnost	k1gFnSc4	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
či	či	k8xC	či
následné	následný	k2eAgNnSc4d1	následné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženský	náboženský	k2eAgInSc4d1	náboženský
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
farnosti	farnost	k1gFnSc2	farnost
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
děkanátu	děkanát	k1gInSc2	děkanát
Mikulov	Mikulov	k1gInSc1	Mikulov
-	-	kIx~	-
Brněnské	brněnský	k2eAgFnSc3d1	brněnská
dicéze	dicéza	k1gFnSc3	dicéza
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
censu	census	k1gInSc6	census
prováděném	prováděný	k2eAgInSc6d1	prováděný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
67	[number]	k4	67
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
36	[number]	k4	36
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
či	či	k8xC	či
náboženské	náboženský	k2eAgFnSc3d1	náboženská
obci	obec	k1gFnSc3	obec
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
25	[number]	k4	25
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
dále	daleko	k6eAd2	daleko
-	-	kIx~	-
1	[number]	k4	1
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
4	[number]	k4	4
k	k	k7c3	k
českobratrským	českobratrský	k2eAgMnPc3d1	českobratrský
evangelíkům	evangelík	k1gMnPc3	evangelík
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
85	[number]	k4	85
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
142	[number]	k4	142
lidí	člověk	k1gMnPc2	člověk
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
své	svůj	k3xOyFgFnSc2	svůj
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecní	obecní	k2eAgFnSc1d1	obecní
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
starostou	starosta	k1gMnSc7	starosta
Jaromír	Jaromír	k1gMnSc1	Jaromír
Sasínek	Sasínek	k1gMnSc1	Sasínek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ustavujícím	ustavující	k2eAgNnSc6d1	ustavující
zasedání	zasedání	k1gNnSc6	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
zvolena	zvolen	k2eAgFnSc1d1	zvolena
Erika	Erika	k1gFnSc1	Erika
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prosincovém	prosincový	k2eAgNnSc6d1	prosincové
zasedání	zasedání	k1gNnSc6	zasedání
obecního	obecní	k2eAgNnSc2d1	obecní
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
byla	být	k5eAaImAgFnS	být
novou	nový	k2eAgFnSc7d1	nová
starostkou	starostka	k1gFnSc7	starostka
zvolena	zvolit	k5eAaPmNgFnS	zvolit
Ing.	ing.	kA	ing.
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Rajchlová	Rajchlová	k1gFnSc1	Rajchlová
<g/>
,	,	kIx,	,
PhD	PhD	k1gFnSc1	PhD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Děvičky	Děvička	k1gFnSc2	Děvička
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrch	vrch	k1gInSc4	vrch
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
Děvín	Děvín	k1gInSc4	Děvín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
významná	významný	k2eAgFnSc1d1	významná
geologická	geologický	k2eAgFnSc1d1	geologická
lokalita	lokalita	k1gFnSc1	lokalita
Kalendář	kalendář	k1gInSc1	kalendář
věků	věk	k1gInPc2	věk
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
významných	významný	k2eAgInPc2d1	významný
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
archanděla	archanděl	k1gMnSc2	archanděl
připomínán	připomínat	k5eAaImNgInS	připomínat
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1389	[number]	k4	1389
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
radnice	radnice	k1gFnSc2	radnice
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
umístěna	umístěn	k2eAgFnSc1d1	umístěna
archeologická	archeologický	k2eAgFnSc1d1	archeologická
expozice	expozice	k1gFnSc1	expozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
pomocné	pomocný	k2eAgFnSc6d1	pomocná
od	od	k7c2	od
Ignáce	Ignác	k1gMnSc2	Ignác
Lengelachera	Lengelacher	k1gMnSc2	Lengelacher
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1724	[number]	k4	1724
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Habánské	habánský	k2eAgNnSc1d1	habánský
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Husí	husí	k2eAgInSc1d1	husí
plácek	plácek	k1gInSc1	plácek
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
zvláštnostem	zvláštnost	k1gFnPc3	zvláštnost
mikulovské	mikulovský	k2eAgFnSc2d1	Mikulovská
vinařské	vinařský	k2eAgFnSc2d1	vinařská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Habánské	habánský	k2eAgInPc1d1	habánský
vinné	vinný	k2eAgInPc1d1	vinný
sklepy	sklep	k1gInPc1	sklep
zde	zde	k6eAd1	zde
kdysi	kdysi	k6eAd1	kdysi
tvořily	tvořit	k5eAaImAgFnP	tvořit
celé	celý	k2eAgNnSc4d1	celé
náměstíčko	náměstíčko	k1gNnSc4	náměstíčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Dolních	dolní	k2eAgFnPc2d1	dolní
Věstonic	Věstonice	k1gFnPc2	Věstonice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stezka	stezka	k1gFnSc1	stezka
Lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
–	–	k?	–
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
2007-09-23	[number]	k4	2007-09-23
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
