<s>
Prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
Josef	Josef	k1gMnSc1	Josef
Bertl	Bertl	k1gMnSc1	Bertl
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1866	[number]	k4	1866
Soběslav	Soběslava	k1gFnPc2	Soběslava
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1955	[number]	k4	1955
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
a	a	k8xC	a
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
stavebních	stavební	k2eAgFnPc6d1	stavební
hmotách	hmota	k1gFnPc6	hmota
na	na	k7c6	na
Císařské	císařský	k2eAgFnSc6d1	císařská
a	a	k8xC	a
královské	královský	k2eAgFnSc6d1	královská
české	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
