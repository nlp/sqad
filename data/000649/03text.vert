<s>
Prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
Josef	Josef	k1gMnSc1	Josef
Bertl	Bertl	k1gMnSc1	Bertl
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1866	[number]	k4	1866
Soběslav	Soběslava	k1gFnPc2	Soběslava
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1955	[number]	k4	1955
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
a	a	k8xC	a
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
stavebních	stavební	k2eAgFnPc6d1	stavební
hmotách	hmota	k1gFnPc6	hmota
na	na	k7c6	na
Císařské	císařský	k2eAgFnSc6d1	císařská
a	a	k8xC	a
královské	královský	k2eAgFnSc6d1	královská
české	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
učitele	učitel	k1gMnSc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
vyšší	vysoký	k2eAgFnSc2d2	vyšší
reálky	reálka	k1gFnSc2	reálka
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
obdržel	obdržet	k5eAaPmAgMnS	obdržet
stipendium	stipendium	k1gNnSc4	stipendium
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
Umělecko-průmyslové	uměleckorůmyslový	k2eAgFnSc6d1	umělecko-průmyslová
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Císařskou	císařský	k2eAgFnSc4d1	císařská
a	a	k8xC	a
královskou	královský	k2eAgFnSc4d1	královská
českou	český	k2eAgFnSc4d1	Česká
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
technickou	technický	k2eAgFnSc4d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokončil	dokončit	k5eAaPmAgInS	dokončit
studia	studio	k1gNnPc4	studio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Ringhoffer	Ringhoffra	k1gFnPc2	Ringhoffra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
prof.	prof.	kA	prof.
Jiřího	Jiří	k1gMnSc4	Jiří
Pacolda	Pacold	k1gMnSc4	Pacold
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
byl	být	k5eAaImAgInS	být
autorizován	autorizovat	k5eAaBmNgInS	autorizovat
stavitelem	stavitel	k1gMnSc7	stavitel
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Otakara	Otakar	k1gMnSc2	Otakar
Materny	Materna	k1gMnSc2	Materna
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
stavební	stavební	k2eAgFnSc2d1	stavební
firmy	firma	k1gFnSc2	firma
Václav	Václav	k1gMnSc1	Václav
Nekvasil	kvasit	k5eNaImAgMnS	kvasit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
její	její	k3xOp3gFnPc4	její
stavební	stavební	k2eAgFnPc4d1	stavební
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
technickou	technický	k2eAgFnSc4d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
koncepci	koncepce	k1gFnSc4	koncepce
areálu	areál	k1gInSc2	areál
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
byl	být	k5eAaImAgMnS	být
děkanem	děkan	k1gMnSc7	děkan
odboru	odbor	k1gInSc2	odbor
stavebního	stavební	k2eAgNnSc2d1	stavební
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
rektorem	rektor	k1gMnSc7	rektor
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
klubu	klub	k1gInSc2	klub
SK	Sk	kA	Sk
Moravská	moravský	k2eAgFnSc1d1	Moravská
Slavia	Slavia	k1gFnSc1	Slavia
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
a	a	k8xC	a
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
stavebních	stavební	k2eAgFnPc6d1	stavební
hmotách	hmota	k1gFnPc6	hmota
na	na	k7c6	na
Císařské	císařský	k2eAgFnSc6d1	císařská
a	a	k8xC	a
královské	královský	k2eAgFnSc6d1	královská
české	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
akademický	akademický	k2eAgInSc4d1	akademický
rok	rok	k1gInSc4	rok
1908-1909	[number]	k4	1908-1909
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
rektorem	rektor	k1gMnSc7	rektor
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
byl	být	k5eAaImAgMnS	být
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
komory	komora	k1gFnSc2	komora
pro	pro	k7c4	pro
Království	království	k1gNnSc4	království
české	český	k2eAgNnSc4d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
členem	člen	k1gMnSc7	člen
Spolku	spolek	k1gInSc2	spolek
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
mladočeské	mladočeský	k2eAgFnSc6d1	mladočeská
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Československá	československý	k2eAgFnSc1d1	Československá
národní	národní	k2eAgFnSc1d1	národní
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1897	[number]	k4	1897
budova	budova	k1gFnSc1	budova
pivovaru	pivovar	k1gInSc2	pivovar
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
<g/>
:	:	kIx,	:
Otakar	Otakar	k1gMnSc1	Otakar
Bureš	Bureš	k1gMnSc1	Bureš
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
budova	budova	k1gFnSc1	budova
kadetní	kadetní	k2eAgFnSc2d1	kadetní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Eckert	Eckert	k1gMnSc1	Eckert
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
<g />
.	.	kIx.	.
</s>
<s>
221	[number]	k4	221
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnSc2d1	Mariánská
hradby	hradba	k1gFnSc2	hradba
2	[number]	k4	2
<g/>
,	,	kIx,	,
Tychonova	Tychonův	k2eAgFnSc1d1	Tychonova
1	[number]	k4	1
<g/>
,	,	kIx,	,
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
135	[number]	k4	135
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
objekt	objekt	k1gInSc4	objekt
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
objekt	objekt	k1gInSc1	objekt
vojenských	vojenský	k2eAgFnPc2d1	vojenská
zásobáren	zásobárna	k1gFnPc2	zásobárna
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
-	-	kIx~	-
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
229	[number]	k4	229
<g/>
,	,	kIx,	,
Generála	generál	k1gMnSc2	generál
Píky	píka	k1gFnSc2	píka
1	[number]	k4	1
1904	[number]	k4	1904
budova	budova	k1gFnSc1	budova
reálky	reálka	k1gFnSc2	reálka
v	v	k7c6	v
Litovli	Litovel	k1gFnSc6	Litovel
1907	[number]	k4	1907
vila	vila	k1gFnSc1	vila
<g />
.	.	kIx.	.
</s>
<s>
čp.	čp.	k?	čp.
279	[number]	k4	279
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
-	-	kIx~	-
Bubeneč	Bubeneč	k1gFnSc1	Bubeneč
<g/>
,	,	kIx,	,
Pod	pod	k7c4	pod
Kaštany	kaštan	k1gInPc4	kaštan
16	[number]	k4	16
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
budova	budova	k1gFnSc1	budova
VUT	VUT	kA	VUT
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
331	[number]	k4	331
<g/>
,	,	kIx,	,
Veveří	veveří	k2eAgFnSc1d1	veveří
95	[number]	k4	95
Městská	městský	k2eAgFnSc1d1	městská
spořitelna	spořitelna	k1gFnSc1	spořitelna
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
pobočka	pobočka	k1gFnSc1	pobočka
v	v	k7c6	v
Mukačevu	Mukačev	k1gInSc6	Mukačev
1910	[number]	k4	1910
vlastní	vlastní	k2eAgFnSc1d1	vlastní
vila	vila	k1gFnSc1	vila
čp.	čp.	k?	čp.
1098	[number]	k4	1098
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Hradešínská	Hradešínský	k2eAgFnSc1d1	Hradešínská
2	[number]	k4	2
vila	vila	k1gFnSc1	vila
čp.	čp.	k?	čp.
1099	[number]	k4	1099
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
-	-	kIx~	-
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Hradešínská	Hradešínský	k2eAgFnSc1d1	Hradešínská
14	[number]	k4	14
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
spořitelna	spořitelna	k1gFnSc1	spořitelna
ve	v	k7c6	v
Vršovicích	Vršovice	k1gFnPc6	Vršovice
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
<g/>
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Balšánek	balšánek	k1gMnSc1	balšánek
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
67	[number]	k4	67
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
-	-	kIx~	-
Vršovice	Vršovice	k1gFnPc4	Vršovice
<g/>
,	,	kIx,	,
Vršovické	vršovický	k2eAgNnSc4d1	Vršovické
náměstí	náměstí	k1gNnSc4	náměstí
8	[number]	k4	8
stavební	stavební	k2eAgInSc1d1	stavební
program	program	k1gInSc1	program
<g />
.	.	kIx.	.
</s>
<s>
areálu	areál	k1gInSc3	areál
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Dejvicích	Dejvice	k1gFnPc6	Dejvice
tržnice	tržnice	k1gFnSc2	tržnice
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
zastavovací	zastavovací	k2eAgInSc4d1	zastavovací
plán	plán	k1gInSc4	plán
Spořilova	spořilův	k2eAgMnSc2d1	spořilův
Jiří	Jiří	k1gMnSc2	Jiří
Pacold	Pacolda	k1gFnPc2	Pacolda
<g/>
:	:	kIx,	:
Konstrukce	konstrukce	k1gFnSc1	konstrukce
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
-	-	kIx~	-
spoluautor	spoluautor	k1gMnSc1	spoluautor
Novostavby	novostavba	k1gFnSc2	novostavba
c.	c.	k?	c.
k.	k.	k?	k.
české	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
vlastní	vlastní	k2eAgInSc1d1	vlastní
náklad	náklad	k1gInSc1	náklad
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
Konstruktivní	konstruktivní	k2eAgInSc1d1	konstruktivní
stavitelství	stavitelství	k1gNnSc3	stavitelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
posluchačů	posluchač	k1gMnPc2	posluchač
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
Stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
nařízení	nařízení	k1gNnSc1	nařízení
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
předpisy	předpis	k1gInPc1	předpis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
vlastní	vlastní	k2eAgInSc1d1	vlastní
náklad	náklad	k1gInSc1	náklad
(	(	kIx(	(
<g/>
komise	komise	k1gFnSc1	komise
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
F.	F.	kA	F.
Řivnáč	řivnáč	k1gMnSc1	řivnáč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
h.c.	h.c.	k?	h.c.
<g/>
)	)	kIx)	)
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
.	.	kIx.	.
</s>
