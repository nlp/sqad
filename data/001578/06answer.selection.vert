<s>
George	Georg	k1gMnSc2	Georg
Paget	Paget	k1gMnSc1	Paget
Thomson	Thomson	k1gMnSc1	Thomson
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1892	[number]	k4	1892
Cambridge	Cambridge	k1gFnSc1	Cambridge
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1975	[number]	k4	1975
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
)	)	kIx)	)
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
experimentální	experimentální	k2eAgInSc4d1	experimentální
objev	objev	k1gInSc4	objev
rozptylu	rozptyl	k1gInSc2	rozptyl
elektronů	elektron	k1gInPc2	elektron
na	na	k7c6	na
krystalech	krystal	k1gInPc6	krystal
společně	společně	k6eAd1	společně
s	s	k7c7	s
Clintonem	Clinton	k1gMnSc7	Clinton
Josephem	Joseph	k1gInSc7	Joseph
Davissonem	Davisson	k1gInSc7	Davisson
<g/>
.	.	kIx.	.
</s>
