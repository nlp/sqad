<s>
Čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
čínské	čínský	k2eAgInPc1d1	čínský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
汉	汉	k?	汉
/	/	kIx~	/
漢	漢	k?	漢
<g/>
;	;	kIx,	;
Chan-jü	Chanü	k1gMnSc1	Chan-jü
nebo	nebo	k8xC	nebo
中	中	k?	中
<g/>
;	;	kIx,	;
Čung-wen	Čungen	k1gInSc1	Čung-wen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
vzájemně	vzájemně	k6eAd1	vzájemně
nesrozumitelných	srozumitelný	k2eNgInPc2d1	nesrozumitelný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
sinotibetské	sinotibetský	k2eAgFnSc2d1	sinotibetský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
svými	svůj	k3xOyFgMnPc7	svůj
mluvčími	mluvčí	k1gMnPc7	mluvčí
často	často	k6eAd1	často
označovány	označovat	k5eAaImNgFnP	označovat
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dialekty	dialekt	k1gInPc4	dialekt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
jazyky	jazyk	k1gInPc4	jazyk
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
velice	velice	k6eAd1	velice
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
a	a	k8xC	a
čínština	čínština	k1gFnSc1	čínština
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
větví	větev	k1gFnPc2	větev
sinotibetských	sinotibetský	k2eAgInPc2d1	sinotibetský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
tou	ten	k3xDgFnSc7	ten
druhou	druhý	k4xOgFnSc4	druhý
jsou	být	k5eAaImIp3nP	být
tibetobarmské	tibetobarmský	k2eAgInPc1d1	tibetobarmský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
čínské	čínský	k2eAgInPc1d1	čínský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
tónové	tónový	k2eAgInPc1d1	tónový
a	a	k8xC	a
analytické	analytický	k2eAgInPc1d1	analytický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
klasifikačním	klasifikační	k2eAgInSc6d1	klasifikační
systému	systém	k1gInSc6	systém
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čínštiny	čínština	k1gFnSc2	čínština
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
7-13	[number]	k4	7-13
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
severočínské	severočínský	k2eAgInPc1d1	severočínský
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
960	[number]	k4	960
milionů	milion	k4xCgInPc2	milion
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
;	;	kIx,	;
následuje	následovat	k5eAaImIp3nS	následovat
Wu	Wu	k1gFnSc1	Wu
s	s	k7c7	s
80	[number]	k4	80
miliony	milion	k4xCgInPc7	milion
mluvčími	mluvčí	k1gMnPc7	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
kantonština	kantonština	k1gFnSc1	kantonština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
<g/>
.	.	kIx.	.
</s>
<s>
Čínštinu	čínština	k1gFnSc4	čínština
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
kolem	kolem	k7c2	kolem
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Čínsky	čínsky	k6eAd1	čínsky
mluví	mluvit	k5eAaImIp3nS	mluvit
především	především	k9	především
Chanové	Chanový	k2eAgNnSc1d1	Chanový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
čínských	čínský	k2eAgFnPc6d1	čínská
komunitách	komunita	k1gFnPc6	komunita
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Standardizovanou	standardizovaný	k2eAgFnSc7d1	standardizovaná
a	a	k8xC	a
spisovnou	spisovný	k2eAgFnSc7d1	spisovná
formou	forma	k1gFnSc7	forma
čínštiny	čínština	k1gFnSc2	čínština
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
pekingském	pekingský	k2eAgNnSc6d1	pekingské
nářečí	nářečí	k1gNnSc6	nářečí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
širší	široký	k2eAgFnSc2d2	širší
skupiny	skupina	k1gFnSc2	skupina
severočínských	severočínský	k2eAgInPc2d1	severočínský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
logogramy	logogram	k1gInPc1	logogram
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jim	on	k3xPp3gMnPc3	on
nemají	mít	k5eNaImIp3nP	mít
problém	problém	k1gInSc4	problém
porozumět	porozumět	k5eAaPmF	porozumět
ani	ani	k8xC	ani
mluvčí	mluvčí	k1gFnSc1	mluvčí
vzájemně	vzájemně	k6eAd1	vzájemně
nesrozumitelných	srozumitelný	k2eNgInPc2d1	nesrozumitelný
čínských	čínský	k2eAgInPc2d1	čínský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
jednacích	jednací	k2eAgInPc2d1	jednací
jazyků	jazyk	k1gInPc2	jazyk
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
jazykovém	jazykový	k2eAgNnSc6d1	jazykové
prostředí	prostředí	k1gNnSc6	prostředí
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
čínština	čínština	k1gFnSc1	čínština
<g/>
"	"	kIx"	"
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
standardní	standardní	k2eAgFnSc4d1	standardní
čínštinu	čínština	k1gFnSc4	čínština
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
現	現	k?	現
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
现	现	k?	现
pinyin	pinyin	k1gInSc1	pinyin
<g/>
:	:	kIx,	:
Xià	Xià	k1gFnSc1	Xià
biā	biā	k?	biā
Hà	Hà	k1gFnSc1	Hà
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Sien-taj	Sienaj	k1gMnSc1	Sien-taj
piao-čun	piao-čun	k1gMnSc1	piao-čun
Chan-jü	Chanü	k1gMnSc1	Chan-jü
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
lexikálně-gramatického	lexikálněramatický	k2eAgNnSc2d1	lexikálně-gramatický
hlediska	hledisko	k1gNnSc2	hledisko
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
vzorových	vzorový	k2eAgInPc6d1	vzorový
dílech	díl	k1gInPc6	díl
napsaných	napsaný	k2eAgInPc6d1	napsaný
jazykem	jazyk	k1gInSc7	jazyk
paj-chua	pajhuus	k1gMnSc2	paj-chuus
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fonetického	fonetický	k2eAgInSc2d1	fonetický
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
pekingském	pekingský	k2eAgInSc6d1	pekingský
dialektu	dialekt	k1gInSc6	dialekt
a	a	k8xC	a
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgFnSc1d1	severní
dialektická	dialektický	k2eAgFnSc1d1	dialektická
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
jako	jako	k8xC	jako
mandarínština	mandarínština	k1gFnSc1	mandarínština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
názoru	názor	k1gInSc2	názor
některých	některý	k3yIgMnPc2	některý
badatelů	badatel	k1gMnPc2	badatel
není	být	k5eNaImIp3nS	být
čínština	čínština	k1gFnSc1	čínština
jednotným	jednotný	k2eAgMnSc7d1	jednotný
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
několika	několik	k4yIc2	několik
menších	malý	k2eAgInPc2d2	menší
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
počet	počet	k1gInSc1	počet
7	[number]	k4	7
podle	podle	k7c2	podle
sedmi	sedm	k4xCc2	sedm
velkých	velký	k2eAgFnPc2d1	velká
skupin	skupina	k1gFnPc2	skupina
dialektů	dialekt	k1gInPc2	dialekt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
dialekty	dialekt	k1gInPc1	dialekt
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zásadních	zásadní	k2eAgFnPc6d1	zásadní
charakteristikách	charakteristika	k1gFnPc6	charakteristika
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
rozšířenost	rozšířenost	k1gFnSc4	rozšířenost
sporný	sporný	k2eAgInSc4d1	sporný
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
připočteme	připočíst	k5eAaPmIp1nP	připočíst
<g/>
-li	i	k?	-li
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgInPc1d1	definován
i	i	k9	i
mimojazykovými	mimojazykový	k2eAgFnPc7d1	mimojazyková
skutečnostmi	skutečnost	k1gFnPc7	skutečnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kulturními	kulturní	k2eAgFnPc7d1	kulturní
a	a	k8xC	a
politickými	politický	k2eAgFnPc7d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgFnSc1d1	úřední
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
norma	norma	k1gFnSc1	norma
<g/>
)	)	kIx)	)
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
nazývá	nazývat	k5eAaImIp3nS	nazývat
pǔ	pǔ	k?	pǔ
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
普	普	k?	普
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
普	普	k?	普
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
pchu-tchung-chua	pchuchunghua	k1gFnSc1	pchu-tchung-chua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
republice	republika	k1gFnSc6	republika
na	na	k7c6	na
Taiwanu	Taiwan	k1gInSc6	Taiwan
guóyǔ	guóyǔ	k?	guóyǔ
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
národní	národní	k2eAgInSc1d1	národní
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
國	國	k?	國
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
国	国	k?	国
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
kuo-jü	kuoü	k?	kuo-jü
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
huáyǔ	huáyǔ	k?	huáyǔ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
čínský	čínský	k2eAgInSc1d1	čínský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
華	華	k?	華
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
华	华	k?	华
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
chua-jü	chuaü	k?	chua-jü
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
moderní	moderní	k2eAgFnSc2d1	moderní
standardní	standardní	k2eAgFnSc2d1	standardní
čínštiny	čínština	k1gFnSc2	čínština
užívají	užívat	k5eAaImIp3nP	užívat
rodilí	rodilý	k2eAgMnPc1d1	rodilý
mluvčí	mluvčí	k1gMnPc1	mluvčí
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
lze	lze	k6eAd1	lze
dle	dle	k7c2	dle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
dělit	dělit	k5eAaImF	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
moderní	moderní	k2eAgFnSc1d1	moderní
standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
dorozumívací	dorozumívací	k2eAgInSc4d1	dorozumívací
prostředek	prostředek	k1gInSc4	prostředek
mezi	mezi	k7c7	mezi
mluvčími	mluvčí	k1gFnPc7	mluvčí
odlišných	odlišný	k2eAgFnPc2d1	odlišná
nářečních	nářeční	k2eAgFnPc2d1	nářeční
skupin	skupina	k1gFnPc2	skupina
bez	bez	k7c2	bez
<g />
.	.	kIx.	.
</s>
<s>
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výslovnost	výslovnost	k1gFnSc1	výslovnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
slabik	slabika	k1gFnPc2	slabika
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
rodným	rodný	k2eAgNnSc7d1	rodné
nářečím	nářečí	k1gNnSc7	nářečí
mluvčího	mluvčí	k1gMnSc2	mluvčí
a	a	k8xC	a
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
stanovené	stanovený	k2eAgFnSc2d1	stanovená
normy	norma	k1gFnSc2	norma
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
omezené	omezený	k2eAgFnSc3d1	omezená
míře	míra	k1gFnSc3	míra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
u	u	k7c2	u
pedagogů	pedagog	k1gMnPc2	pedagog
speciálně	speciálně	k6eAd1	speciálně
cvičených	cvičený	k2eAgFnPc2d1	cvičená
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
čínského	čínský	k2eAgInSc2d1	čínský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
pro	pro	k7c4	pro
čínštinu	čínština	k1gFnSc4	čínština
Hà	Hà	k1gFnSc2	Hà
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
obecně	obecně	k6eAd1	obecně
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
národa	národ	k1gInSc2	národ
Hanů	Han	k1gMnPc2	Han
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgMnSc1d1	neutrální
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psanosti	psanost	k1gFnSc2	psanost
či	či	k8xC	či
mluvenosti	mluvenost	k1gFnSc2	mluvenost
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
k	k	k7c3	k
psané	psaný	k2eAgFnSc3d1	psaná
čínštině	čínština	k1gFnSc3	čínština
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
pojem	pojem	k1gInSc1	pojem
Zhō	Zhō	k1gFnSc2	Zhō
<g/>
,	,	kIx,	,
k	k	k7c3	k
čínštině	čínština	k1gFnSc3	čínština
mluvené	mluvený	k2eAgFnSc3d1	mluvená
pak	pak	k8xC	pak
Zhō	Zhō	k1gMnSc3	Zhō
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
中	中	k?	中
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
中	中	k?	中
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Čung-kuo-chua	Čunguohua	k1gFnSc1	Čung-kuo-chua
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srovnej	srovnat	k5eAaPmRp2nS	srovnat
významy	význam	k1gInPc4	význam
morfémů	morfém	k1gInPc2	morfém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pojmech	pojem	k1gInPc6	pojem
obsaženy	obsažen	k2eAgFnPc1d1	obsažena
<g/>
:	:	kIx,	:
yǔ	yǔ	k?	yǔ
语	语	k?	语
'	'	kIx"	'
<g/>
jazyk	jazyk	k1gInSc1	jazyk
coby	coby	k?	coby
systém	systém	k1gInSc1	systém
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
wén	wén	k?	wén
文	文	k?	文
'	'	kIx"	'
<g/>
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
psaný	psaný	k2eAgInSc1d1	psaný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
huà	huà	k?	huà
话	话	k?	话
'	'	kIx"	'
<g/>
řeč	řeč	k1gFnSc1	řeč
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínština	čínština	k1gFnSc1	čínština
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
čínskými	čínský	k2eAgInPc7d1	čínský
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přečteny	přečten	k2eAgFnPc1d1	přečtena
v	v	k7c4	v
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
výslovnosti	výslovnost	k1gFnPc4	výslovnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
kterémkoli	kterýkoli	k3yIgInSc6	kterýkoli
dialektu	dialekt	k1gInSc6	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
čínské	čínský	k2eAgInPc4d1	čínský
dialekty	dialekt	k1gInPc4	dialekt
se	se	k3xPyFc4	se
valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
textů	text	k1gInPc2	text
píše	psát	k5eAaImIp3nS	psát
současným	současný	k2eAgInSc7d1	současný
spisovným	spisovný	k2eAgInSc7d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
現	現	k?	現
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
现	现	k?	现
pinyin	pinyin	k1gInSc1	pinyin
<g/>
:	:	kIx,	:
xià	xià	k?	xià
shū	shū	k?	shū
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
sien-taj	sienaj	k1gInSc1	sien-taj
šu-mien-jü	šuienü	k?	šu-mien-jü
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vycházejícím	vycházející	k2eAgFnPc3d1	vycházející
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
paj-chua	pajhu	k1gInSc2	paj-chu
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgFnSc1d1	severní
dialektická	dialektický	k2eAgFnSc1d1	dialektická
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
se	se	k3xPyFc4	se
na	na	k7c6	na
pevninské	pevninský	k2eAgFnSc6d1	pevninská
Číně	Čína	k1gFnSc6	Čína
užívá	užívat	k5eAaImIp3nS	užívat
zjednodušených	zjednodušený	k2eAgInPc2d1	zjednodušený
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
nebo	nebo	k8xC	nebo
čínské	čínský	k2eAgFnSc2d1	čínská
hláskové	hláskový	k2eAgFnSc2d1	hlásková
abecedy	abeceda	k1gFnSc2	abeceda
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
pchin-jin	pchinin	k1gInSc1	pchin-jin
<g/>
,	,	kIx,	,
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
tradičních	tradiční	k2eAgFnPc2d1	tradiční
(	(	kIx(	(
<g/>
nezjednodušených	zjednodušený	k2eNgFnPc2d1	zjednodušený
<g/>
)	)	kIx)	)
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
dialektickou	dialektický	k2eAgFnSc4d1	dialektická
skupinu	skupina	k1gFnSc4	skupina
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
užívá	užívat	k5eAaImIp3nS	užívat
anglismus	anglismus	k1gInSc4	anglismus
mandarínština	mandarínština	k1gFnSc1	mandarínština
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
současnou	současný	k2eAgFnSc4d1	současná
standardní	standardní	k2eAgFnSc4d1	standardní
čínštinu	čínština	k1gFnSc4	čínština
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
upouští	upouštět	k5eAaImIp3nS	upouštět
od	od	k7c2	od
zavádějícího	zavádějící	k2eAgInSc2d1	zavádějící
pojmu	pojem	k1gInSc2	pojem
Mandarin	Mandarin	k1gInSc1	Mandarin
Chinese	Chinese	k1gFnSc2	Chinese
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
akademických	akademický	k2eAgInPc6d1	akademický
kruzích	kruh	k1gInPc6	kruh
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
pojmem	pojem	k1gInSc7	pojem
Modern	Moderno	k1gNnPc2	Moderno
Standard	standard	k1gInSc1	standard
Chinese	Chinese	k1gFnSc1	Chinese
<g/>
.	.	kIx.	.
</s>
<s>
Čínština	čínština	k1gFnSc1	čínština
se	se	k3xPyFc4	se
z	z	k7c2	z
typologického	typologický	k2eAgInSc2d1	typologický
pohledu	pohled	k1gInSc2	pohled
definuje	definovat	k5eAaBmIp3nS	definovat
pomocí	pomocí	k7c2	pomocí
tří	tři	k4xCgFnPc2	tři
charakteristik	charakteristika	k1gFnPc2	charakteristika
<g/>
:	:	kIx,	:
Analytický	analytický	k2eAgInSc1d1	analytický
jazyk	jazyk	k1gInSc1	jazyk
Čínština	čínština	k1gFnSc1	čínština
coby	coby	k?	coby
analytický	analytický	k2eAgInSc1d1	analytický
jazyk	jazyk	k1gInSc1	jazyk
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
především	především	k9	především
dva	dva	k4xCgInPc4	dva
prostředky	prostředek	k1gInPc4	prostředek
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
slovosled	slovosled	k1gInSc4	slovosled
a	a	k8xC	a
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
gramatická	gramatický	k2eAgNnPc4d1	gramatické
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
slova	slovo	k1gNnPc4	slovo
nemění	měnit	k5eNaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
jako	jako	k9	jako
ve	v	k7c6	v
flektivních	flektivní	k2eAgInPc6d1	flektivní
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slabičný	slabičný	k2eAgInSc1d1	slabičný
jazyk	jazyk	k1gInSc1	jazyk
V	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
hranice	hranice	k1gFnSc1	hranice
morfému	morfém	k1gInSc2	morfém
s	s	k7c7	s
hranicí	hranice	k1gFnSc7	hranice
slabiky	slabika	k1gFnSc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
morfém	morfém	k1gInSc1	morfém
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
语	语	k?	语
(	(	kIx(	(
<g/>
yǔ	yǔ	k?	yǔ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
označuje	označovat	k5eAaImIp3nS	označovat
nejmenší	malý	k2eAgFnSc1d3	nejmenší
jazyková	jazykový	k2eAgFnSc1d1	jazyková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
rovinu	rovina	k1gFnSc4	rovina
významovou	významový	k2eAgFnSc4d1	významová
a	a	k8xC	a
rovinu	rovina	k1gFnSc4	rovina
výrazovou	výrazový	k2eAgFnSc4d1	výrazová
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nedělitelná	dělitelný	k2eNgFnSc1d1	nedělitelná
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
význam	význam	k1gInSc4	význam
a	a	k8xC	a
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
pár	pár	k4xCyI	pár
výjimek	výjimka	k1gFnPc2	výjimka
si	se	k3xPyFc3	se
nelze	lze	k6eNd1	lze
představit	představit	k5eAaPmF	představit
hlásku	hláska	k1gFnSc4	hláska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
realizovatelná	realizovatelný	k2eAgFnSc1d1	realizovatelná
mimo	mimo	k7c4	mimo
slabiku	slabika	k1gFnSc4	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Tónový	tónový	k2eAgInSc4d1	tónový
jazyk	jazyk	k1gInSc4	jazyk
Každá	každý	k3xTgFnSc1	každý
slabika	slabika	k1gFnSc1	slabika
je	být	k5eAaImIp3nS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
tónu	tón	k1gInSc6	tón
(	(	kIx(	(
<g/>
=	=	kIx~	=
hlasová	hlasový	k2eAgFnSc1d1	hlasová
melodie	melodie	k1gFnSc1	melodie
<g/>
,	,	kIx,	,
doprovázející	doprovázející	k2eAgFnSc1d1	doprovázející
výslovnost	výslovnost	k1gFnSc1	výslovnost
celé	celý	k2eAgFnSc2d1	celá
slabiky	slabika	k1gFnSc2	slabika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tón	tón	k1gInSc1	tón
nese	nést	k5eAaImIp3nS	nést
význam	význam	k1gInSc4	význam
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
slabiky	slabika	k1gFnSc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
klasifikací	klasifikace	k1gFnSc7	klasifikace
čínštiny	čínština	k1gFnSc2	čínština
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejích	její	k3xOp3gFnPc2	její
genealogických	genealogický	k2eAgFnPc2d1	genealogická
(	(	kIx(	(
<g/>
příbuzenských	příbuzenský	k2eAgFnPc2d1	příbuzenská
<g/>
)	)	kIx)	)
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
i	i	k8xC	i
mezi	mezi	k7c7	mezi
čínskými	čínský	k2eAgMnPc7d1	čínský
badateli	badatel	k1gMnPc7	badatel
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
francouzská	francouzský	k2eAgFnSc1d1	francouzská
klasifikace	klasifikace	k1gFnSc1	klasifikace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Henri	Henri	k1gNnSc1	Henri
Maspero	Maspero	k1gNnSc4	Maspero
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
základ	základ	k1gInSc4	základ
své	svůj	k3xOyFgFnSc2	svůj
klasifikace	klasifikace	k1gFnSc2	klasifikace
si	se	k3xPyFc3	se
Maspero	Maspero	k1gNnSc4	Maspero
vzal	vzít	k5eAaPmAgMnS	vzít
vnější	vnější	k2eAgFnSc4d1	vnější
podobu	podoba	k1gFnSc4	podoba
sinických	sinický	k2eAgInPc2d1	sinický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vnější	vnější	k2eAgFnSc6d1	vnější
stránce	stránka	k1gFnSc6	stránka
podobají	podobat	k5eAaImIp3nP	podobat
čínštině	čínština	k1gFnSc3	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
všechny	všechen	k3xTgInPc4	všechen
sinické	sinický	k2eAgInPc4d1	sinický
jazyky	jazyk	k1gInPc4	jazyk
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c6	na
monotónické	monotónický	k2eAgFnSc6d1	monotónický
(	(	kIx(	(
<g/>
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
jazyky	jazyk	k1gInPc1	jazyk
austronéské	austronéský	k2eAgInPc1d1	austronéský
a	a	k8xC	a
jazyky	jazyk	k1gInPc1	jazyk
mon-khmérské	monhmérský	k2eAgInPc1d1	mon-khmérský
<g/>
)	)	kIx)	)
a	a	k8xC	a
polytónické	polytónický	k2eAgFnPc1d1	polytónický
<g/>
.	.	kIx.	.
</s>
<s>
Polytónické	Polytónický	k2eAgInPc4d1	Polytónický
jazyky	jazyk	k1gInPc4	jazyk
dále	daleko	k6eAd2	daleko
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
podle	podle	k7c2	podle
slovosledu	slovosled	k1gInSc2	slovosled
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
na	na	k7c4	na
SVO	SVO	kA	SVO
(	(	kIx(	(
<g/>
subject-verb-object	subjecterbbject	k1gMnSc1	subject-verb-object
=	=	kIx~	=
podmět-sloveso-předmět	podmětlovesoředmět	k5eAaPmF	podmět-sloveso-předmět
<g/>
,	,	kIx,	,
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nP	patřit
sino-thajské	sinohajský	k2eAgInPc1d1	sino-thajský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
a	a	k8xC	a
SOV	sova	k1gFnPc2	sova
(	(	kIx(	(
<g/>
subject-object-verb	subjectbjecterb	k1gInSc1	subject-object-verb
=	=	kIx~	=
podmět-předmět-sloveso	podmětředmětlovesa	k1gFnSc5	podmět-předmět-slovesa
<g/>
,	,	kIx,	,
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nP	patřit
tibetobarmské	tibetobarmský	k2eAgInPc1d1	tibetobarmský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
typu	typ	k1gInSc2	typ
SVO	SVO	kA	SVO
nakonec	nakonec	k6eAd1	nakonec
ještě	ještě	k6eAd1	ještě
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
přívlastku	přívlastek	k1gInSc2	přívlastek
a	a	k8xC	a
slova	slovo	k1gNnPc4	slovo
určovaného	určovaný	k2eAgNnSc2d1	určované
na	na	k7c6	na
HA	ha	kA	ha
(	(	kIx(	(
<g/>
head-attribute	headttribut	k1gInSc5	head-attribut
=	=	kIx~	=
slovo	slovo	k1gNnSc1	slovo
určované-přívlastek	určovanéřívlastka	k1gFnPc2	určované-přívlastka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
jazyky	jazyk	k1gInPc4	jazyk
thaj-annamské	thajnnamský	k2eAgInPc4d1	thaj-annamský
<g/>
)	)	kIx)	)
a	a	k8xC	a
AH	ah	k0	ah
(	(	kIx(	(
<g/>
attribute-head	attributeead	k1gInSc1	attribute-head
=	=	kIx~	=
přívlastek-slovo	přívlasteklův	k2eAgNnSc1d1	přívlastek-slův
určované	určovaný	k2eAgNnSc1d1	určované
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
čínština	čínština	k1gFnSc1	čínština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
klasifikace	klasifikace	k1gFnSc1	klasifikace
je	být	k5eAaImIp3nS	být
diskutabilní	diskutabilní	k2eAgFnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Typologická	typologický	k2eAgNnPc1d1	typologické
kritéria	kritérion	k1gNnPc1	kritérion
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
monotónických	monotónický	k2eAgInPc2d1	monotónický
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
polytónické	polytónický	k2eAgFnPc1d1	polytónický
<g/>
:	:	kIx,	:
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
se	se	k3xPyFc4	se
tóny	tón	k1gInPc7	tón
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
sufixů	sufix	k1gInPc2	sufix
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
slovosled	slovosled	k1gInSc1	slovosled
není	být	k5eNaImIp3nS	být
neměnný	měnný	k2eNgInSc1d1	neměnný
<g/>
:	:	kIx,	:
Zatímco	zatímco	k8xS	zatímco
klasická	klasický	k2eAgFnSc1d1	klasická
čínština	čínština	k1gFnSc1	čínština
měla	mít	k5eAaImAgFnS	mít
slovosled	slovosled	k1gInSc4	slovosled
SVO	SVO	kA	SVO
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
čínština	čínština	k1gFnSc1	čínština
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
slovosled	slovosled	k1gInSc4	slovosled
SOV	sova	k1gFnPc2	sova
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
klasifikace	klasifikace	k1gFnSc1	klasifikace
čínštiny	čínština	k1gFnSc2	čínština
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Paula	Paul	k1gMnSc2	Paul
Benedikta	Benedikt	k1gMnSc2	Benedikt
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
základního	základní	k2eAgNnSc2d1	základní
dělení	dělení	k1gNnSc2	dělení
sinických	sinický	k2eAgInPc2d1	sinický
jazyků	jazyk	k1gInPc2	jazyk
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
austroasijská	austroasijský	k2eAgFnSc1d1	austroasijský
rodina	rodina	k1gFnSc1	rodina
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jazyky	jazyk	k1gInPc4	jazyk
mon-khmérské	monhmérský	k2eAgInPc4d1	mon-khmérský
<g/>
,	,	kIx,	,
vietnamštinu	vietnamština	k1gFnSc4	vietnamština
<g/>
,	,	kIx,	,
miao	miao	k1gMnSc1	miao
<g/>
,	,	kIx,	,
yao	yao	k?	yao
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
munda	mundo	k1gNnSc2	mundo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
austronéské	austronéský	k2eAgInPc1d1	austronéský
a	a	k8xC	a
thajské	thajský	k2eAgInPc1d1	thajský
jazyky	jazyk	k1gInPc1	jazyk
patří	patřit	k5eAaImIp3nP	patřit
jazyky	jazyk	k1gInPc1	jazyk
Polynésie	Polynésie	k1gFnSc2	Polynésie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
malajština	malajština	k1gFnSc1	malajština
a	a	k8xC	a
thajština	thajština	k1gFnSc1	thajština
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
sinotibetské	sinotibetský	k2eAgInPc1d1	sinotibetský
jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
čínštinu	čínština	k1gFnSc4	čínština
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
tibetobarmské	tibetobarmský	k2eAgInPc4d1	tibetobarmský
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
tibetštinu	tibetština	k1gFnSc4	tibetština
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
lolobarmské	lolobarmský	k2eAgInPc4d1	lolobarmský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
jazyk	jazyk	k1gInSc4	jazyk
lolo	lolo	k6eAd1	lolo
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
yi	yi	k?	yi
<g/>
)	)	kIx)	)
a	a	k8xC	a
barmština	barmština	k1gFnSc1	barmština
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
Benediktově	Benediktův	k2eAgFnSc3d1	Benediktova
klasifikaci	klasifikace	k1gFnSc3	klasifikace
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
glotochronologické	glotochronologický	k2eAgInPc1d1	glotochronologický
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
prováděl	provádět	k5eAaImAgInS	provádět
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
profesor	profesor	k1gMnSc1	profesor
Jachontov	Jachontov	k1gInSc1	Jachontov
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
probíhají	probíhat	k5eAaImIp3nP	probíhat
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
procento	procento	k1gNnSc1	procento
shody	shoda	k1gFnSc2	shoda
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
určitých	určitý	k2eAgInPc2d1	určitý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgInPc1d1	čínský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
dialekty	dialekt	k1gInPc1	dialekt
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
formě	forma	k1gFnSc6	forma
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
preferencí	preference	k1gFnSc7	preference
některých	některý	k3yIgInPc2	některý
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
o	o	k7c4	o
podobnosti	podobnost	k1gFnPc4	podobnost
čínštiny	čínština	k1gFnSc2	čínština
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
odlišným	odlišný	k2eAgInSc7d1	odlišný
jazykem	jazyk	k1gInSc7	jazyk
–	–	k?	–
japonštinou	japonština	k1gFnSc7	japonština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dialektech	dialekt	k1gInPc6	dialekt
užíváno	užívat	k5eAaImNgNnS	užívat
mnoho	mnoho	k4c1	mnoho
znaků	znak	k1gInPc2	znak
unikátních	unikátní	k2eAgInPc2d1	unikátní
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
čínštině	čínština	k1gFnSc6	čínština
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
čínském	čínský	k2eAgInSc6d1	čínský
dialektu	dialekt	k1gInSc6	dialekt
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Mluvené	mluvený	k2eAgFnPc1d1	mluvená
formy	forma	k1gFnPc1	forma
dialektů	dialekt	k1gInPc2	dialekt
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
mnohdy	mnohdy	k6eAd1	mnohdy
nesrozumitelné	srozumitelný	k2eNgNnSc4d1	nesrozumitelné
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
čínština	čínština	k1gFnSc1	čínština
vs	vs	k?	vs
<g/>
.	.	kIx.	.
pekingská	pekingský	k2eAgFnSc1d1	Pekingská
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
šanghajština	šanghajština	k1gFnSc1	šanghajština
a	a	k8xC	a
ningpoština	ningpoština	k1gFnSc1	ningpoština
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
spadají	spadat	k5eAaImIp3nP	spadat
pod	pod	k7c4	pod
dialekty	dialekt	k1gInPc4	dialekt
Wu	Wu	k1gFnSc2	Wu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínština	čínština	k1gFnSc1	čínština
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
7	[number]	k4	7
jazykových	jazykový	k2eAgFnPc2d1	jazyková
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
odlišném	odlišný	k2eAgInSc6d1	odlišný
vývoji	vývoj	k1gInSc6	vývoj
znělých	znělý	k2eAgFnPc2d1	znělá
iniciál	iniciála	k1gFnPc2	iniciála
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
čínštiny	čínština	k1gFnSc2	čínština
<g/>
:	:	kIx,	:
Severočínské	severočínský	k2eAgInPc1d1	severočínský
dialekty	dialekt	k1gInPc1	dialekt
–	–	k?	–
960	[number]	k4	960
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
mluvčích	mluvčí	k1gMnPc2	mluvčí
i	i	k8xC	i
území	území	k1gNnSc2	území
mimo	mimo	k7c4	mimo
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
oblast	oblast	k1gFnSc4	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
mluvčí	mluvčí	k1gMnPc4	mluvčí
jiných	jiný	k2eAgInPc2d1	jiný
dialektů	dialekt	k1gInPc2	dialekt
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
společný	společný	k2eAgInSc1d1	společný
dorozumívací	dorozumívací	k2eAgInSc1d1	dorozumívací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
;	;	kIx,	;
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
nářečích	nářečí	k1gNnPc6	nářečí
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
i	i	k8xC	i
Singapuru	Singapur	k1gInSc6	Singapur
Wu	Wu	k1gFnSc1	Wu
(	(	kIx(	(
<g/>
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dialektem	dialekt	k1gInSc7	dialekt
wu	wu	k?	wu
je	být	k5eAaImIp3nS	být
šanghajština	šanghajština	k1gFnSc1	šanghajština
–	–	k?	–
cca	cca	kA	cca
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
)	)	kIx)	)
–	–	k?	–
80	[number]	k4	80
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
Min	mina	k1gFnPc2	mina
(	(	kIx(	(
<g/>
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
fuťienský	fuťienský	k2eAgInSc1d1	fuťienský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
–	–	k?	–
70	[number]	k4	70
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
zejména	zejména	k9	zejména
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
provincii	provincie	k1gFnSc6	provincie
Fu-ťien	Fu-ťina	k1gFnPc2	Fu-ťina
a	a	k8xC	a
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
Jüe	Jüe	k1gFnSc2	Jüe
–	–	k?	–
60	[number]	k4	60
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
provincii	provincie	k1gFnSc6	provincie
Kuang-tung	Kuangunga	k1gFnPc2	Kuang-tunga
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kantonštinu	kantonština	k1gFnSc4	kantonština
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejobvyklejší	obvyklý	k2eAgInSc1d3	nejobvyklejší
jazyk	jazyk	k1gInSc1	jazyk
zámořských	zámořský	k2eAgFnPc2d1	zámořská
čínských	čínský	k2eAgFnPc2d1	čínská
komunit	komunita	k1gFnPc2	komunita
a	a	k8xC	a
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
Hongkongu	Hongkong	k1gInSc2	Hongkong
Siang	Siang	k1gInSc1	Siang
–	–	k?	–
40	[number]	k4	40
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gFnPc2	mluvčí
Hakka	Hakek	k1gInSc2	Hakek
–	–	k?	–
35	[number]	k4	35
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
roztroušených	roztroušený	k2eAgMnPc2d1	roztroušený
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
i	i	k8xC	i
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
Kan	Kan	k1gFnSc2	Kan
–	–	k?	–
25	[number]	k4	25
miliónů	milión	k4xCgInPc2	milión
mluvčích	mluvčí	k1gMnPc2	mluvčí
Jazykový	jazykový	k2eAgInSc4d1	jazykový
atlas	atlas	k1gInSc4	atlas
Číny	Čína	k1gFnSc2	Čína
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
ještě	ještě	k9	ještě
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Ťin	Ťin	k1gFnPc7	Ťin
–	–	k?	–
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
se	s	k7c7	s
severočínskými	severočínský	k2eAgInPc7d1	severočínský
dialekty	dialekt	k1gInPc7	dialekt
<g/>
)	)	kIx)	)
Chuej	Chuej	k1gInSc1	Chuej
–	–	k?	–
4,6	[number]	k4	4,6
milionu	milion	k4xCgInSc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
wu	wu	k?	wu
<g/>
)	)	kIx)	)
Pching-chua	Pchinghua	k1gFnSc1	Pching-chua
–	–	k?	–
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
jüe	jüe	k?	jüe
<g/>
)	)	kIx)	)
Kromě	kromě	k7c2	kromě
dialektů	dialekt	k1gInPc2	dialekt
čínštiny	čínština	k1gFnSc2	čínština
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Číny	Čína	k1gFnSc2	Čína
používá	používat	k5eAaImIp3nS	používat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
o	o	k7c4	o
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
odlišnosti	odlišnost	k1gFnPc4	odlišnost
od	od	k7c2	od
čínštiny	čínština	k1gFnSc2	čínština
se	se	k3xPyFc4	se
nepochybuje	pochybovat	k5eNaImIp3nS	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
menšinového	menšinový	k2eAgInSc2d1	menšinový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
tibetština	tibetština	k1gFnSc1	tibetština
<g/>
,	,	kIx,	,
mongolština	mongolština	k1gFnSc1	mongolština
<g/>
,	,	kIx,	,
ujgurština	ujgurština	k1gFnSc1	ujgurština
a	a	k8xC	a
čuangština	čuangština	k1gFnSc1	čuangština
<g/>
.	.	kIx.	.
</s>
<s>
Periodizace	periodizace	k1gFnSc1	periodizace
dějin	dějiny	k1gFnPc2	dějiny
čínského	čínský	k2eAgInSc2d1	čínský
jazyka	jazyk	k1gInSc2	jazyk
podle	podle	k7c2	podle
profesora	profesor	k1gMnSc2	profesor
Wang	Wanga	k1gFnPc2	Wanga
Liho	Liho	k1gMnSc1	Liho
<g/>
:	:	kIx,	:
stará	starý	k2eAgFnSc1d1	stará
čínština	čínština	k1gFnSc1	čínština
(	(	kIx(	(
<g/>
polovina	polovina	k1gFnSc1	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starou	starý	k2eAgFnSc4d1	stará
čínštinu	čínština	k1gFnSc4	čínština
je	být	k5eAaImIp3nS	být
záhodno	záhodno	k6eAd1	záhodno
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
čínština	čínština	k1gFnSc1	čínština
nápisů	nápis	k1gInPc2	nápis
na	na	k7c6	na
věštebných	věštebný	k2eAgFnPc6d1	věštebná
kostech	kost	k1gFnPc6	kost
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předklasická	předklasický	k2eAgFnSc1d1	předklasická
čínština	čínština	k1gFnSc1	čínština
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
*	*	kIx~	*
klasickou	klasický	k2eAgFnSc4d1	klasická
čínštinu	čínština	k1gFnSc4	čínština
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
poklasickou	poklasický	k2eAgFnSc4d1	poklasický
čínštinu	čínština	k1gFnSc4	čínština
(	(	kIx(	(
<g/>
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
200	[number]	k4	200
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
čínština	čínština	k1gFnSc1	čínština
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozčlenit	rozčlenit	k5eAaPmF	rozčlenit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
čínština	čínština	k1gFnSc1	čínština
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
současná	současný	k2eAgFnSc1d1	současná
čínština	čínština	k1gFnSc1	čínština
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
však	však	k9	však
jen	jen	k6eAd1	jen
rámcové	rámcový	k2eAgNnSc1d1	rámcové
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgFnPc2d1	jiná
periodizací	periodizace	k1gFnPc2	periodizace
<g/>
,	,	kIx,	,
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
kritériích	kritérion	k1gNnPc6	kritérion
a	a	k8xC	a
vykazujících	vykazující	k2eAgFnPc6d1	vykazující
větší	veliký	k2eAgFnSc4d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc4d2	menší
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
fonologickou	fonologický	k2eAgFnSc7d1	fonologická
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
slabika	slabika	k1gFnSc1	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
čínské	čínský	k2eAgFnSc2d1	čínská
slabiky	slabika	k1gFnSc2	slabika
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
iniciála	iniciála	k1gFnSc1	iniciála
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
finála	finála	k1gFnSc1	finála
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Iniciál	iniciála	k1gFnPc2	iniciála
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
standardní	standardní	k2eAgFnSc6d1	standardní
čínštině	čínština	k1gFnSc6	čínština
21	[number]	k4	21
<g/>
,	,	kIx,	,
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
iniciálou	iniciála	k1gFnSc7	iniciála
22	[number]	k4	22
(	(	kIx(	(
<g/>
v	v	k7c6	v
pinyinu	pinyin	k1gInSc6	pinyin
<g/>
:	:	kIx,	:
b	b	k?	b
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
zh	zh	k?	zh
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
sh	sh	k?	sh
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
,	,	kIx,	,
q	q	k?	q
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finál	finála	k1gFnPc2	finála
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
(	(	kIx(	(
<g/>
v	v	k7c6	v
pinyinu	pinyin	k1gInSc6	pinyin
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
ai	ai	k?	ai
<g/>
,	,	kIx,	,
ei	ei	k?	ei
<g/>
,	,	kIx,	,
ao	ao	k?	ao
<g/>
,	,	kIx,	,
ou	ou	k0	ou
<g/>
,	,	kIx,	,
an	an	k?	an
<g/>
,	,	kIx,	,
en	en	k?	en
<g/>
,	,	kIx,	,
ang	ang	k?	ang
<g/>
,	,	kIx,	,
eng	eng	k?	eng
<g/>
,	,	kIx,	,
i	i	k9	i
,	,	kIx,	,
ia	ia	k0	ia
<g/>
,	,	kIx,	,
ie	ie	k?	ie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
iao	iao	k?	iao
<g/>
,	,	kIx,	,
iu	iu	k?	iu
<g/>
,	,	kIx,	,
ian	ian	k?	ian
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
,	,	kIx,	,
iang	iang	k1gMnSc1	iang
<g/>
,	,	kIx,	,
ing	ing	kA	ing
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
ua	ua	k?	ua
<g/>
,	,	kIx,	,
uo	uo	k?	uo
<g/>
,	,	kIx,	,
uai	uai	k?	uai
<g/>
,	,	kIx,	,
ui	ui	k?	ui
<g/>
,	,	kIx,	,
uan	uan	k?	uan
<g/>
,	,	kIx,	,
un	un	k?	un
<g/>
,	,	kIx,	,
uang	uang	k1gMnSc1	uang
<g/>
,	,	kIx,	,
ueng	ueng	k1gMnSc1	ueng
<g/>
,	,	kIx,	,
ong	ong	k?	ong
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
ue	ue	k?	ue
<g/>
,	,	kIx,	,
uan	uan	k?	uan
<g/>
,	,	kIx,	,
un	un	k?	un
<g/>
,	,	kIx,	,
iong	iong	k1gInSc1	iong
+	+	kIx~	+
i	i	k9	i
<g/>
,	,	kIx,	,
er	er	k?	er
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Finálu	finála	k1gFnSc4	finála
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
na	na	k7c6	na
mediálu	mediál	k1gInSc6	mediál
a	a	k8xC	a
subfinálu	subfinál	k1gInSc6	subfinál
neboli	neboli	k8xC	neboli
rým	rým	k1gInSc1	rým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
centrálu	centrála	k1gFnSc4	centrála
a	a	k8xC	a
terminálu	terminála	k1gFnSc4	terminála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tóny	Tóna	k1gFnPc1	Tóna
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
(	(	kIx(	(
<g/>
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
,	,	kIx,	,
stoupavý	stoupavý	k2eAgInSc1d1	stoupavý
<g/>
,	,	kIx,	,
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
,	,	kIx,	,
klesavý	klesavý	k2eAgInSc1d1	klesavý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nulovým	nulový	k2eAgMnSc7d1	nulový
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
slabik	slabika	k1gFnPc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
tajština	tajština	k1gFnSc1	tajština
<g/>
,	,	kIx,	,
vietnamština	vietnamština	k1gFnSc1	vietnamština
<g/>
,	,	kIx,	,
tibetština	tibetština	k1gFnSc1	tibetština
<g/>
,	,	kIx,	,
barmština	barmština	k1gFnSc1	barmština
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
tónové	tónový	k2eAgInPc4d1	tónový
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
slabika	slabika	k1gFnSc1	slabika
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
intonací	intonace	k1gFnSc7	intonace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tónový	tónový	k2eAgInSc1d1	tónový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odlišný	odlišný	k2eAgMnSc1d1	odlišný
u	u	k7c2	u
severních	severní	k2eAgMnPc2d1	severní
dialektu	dialekt	k1gInSc2	dialekt
včetně	včetně	k7c2	včetně
oficiálního	oficiální	k2eAgMnSc2d1	oficiální
pekingského	pekingský	k2eAgMnSc2d1	pekingský
(	(	kIx(	(
<g/>
pǔ	pǔ	k?	pǔ
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
dialektů	dialekt	k1gInPc2	dialekt
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pekingském	pekingský	k2eAgInSc6d1	pekingský
dialektu	dialekt	k1gInSc6	dialekt
existují	existovat	k5eAaImIp3nP	existovat
4	[number]	k4	4
intonace	intonace	k1gFnPc4	intonace
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgNnSc4d1	označované
číslem	číslo	k1gNnSc7	číslo
nebo	nebo	k8xC	nebo
diakritikou	diakritika	k1gFnSc7	diakritika
nad	nad	k7c7	nad
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
:	:	kIx,	:
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
rovnou	rovnou	k6eAd1	rovnou
čárou	čára	k1gFnSc7	čára
stoupavá	stoupavý	k2eAgFnSc1d1	stoupavá
ze	z	k7c2	z
středních	střední	k2eAgFnPc2d1	střední
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
intonace	intonace	k1gFnSc1	intonace
otázky	otázka	k1gFnSc2	otázka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
čárkou	čárka	k1gFnSc7	čárka
zprava	zprava	k6eAd1	zprava
stoupavá	stoupavý	k2eAgFnSc1d1	stoupavá
z	z	k7c2	z
hlubokých	hluboký	k2eAgInPc2d1	hluboký
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
poloh	poloha	k1gFnPc2	poloha
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
hluboké	hluboký	k2eAgNnSc4d1	hluboké
(	(	kIx(	(
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
přeptávání	přeptávání	k1gNnSc1	přeptávání
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
Cože	cože	k6eAd1	cože
<g/>
?!!!	?!!!	k?	?!!!
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
háčkem	háček	k1gInSc7	háček
klesavý	klesavý	k2eAgInSc1d1	klesavý
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
do	do	k7c2	do
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
ostrý	ostrý	k2eAgInSc1d1	ostrý
rozkaz	rozkaz	k1gInSc1	rozkaz
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
slabika	slabika	k1gFnSc1	slabika
"	"	kIx"	"
<g/>
ma	ma	k?	ma
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
ma	ma	k?	ma
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
妈	妈	k?	妈
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ma	ma	k?	ma
<g/>
2	[number]	k4	2
<g/>
"	"	kIx"	"
konopí	konopí	k1gNnSc1	konopí
<g/>
,	,	kIx,	,
麻	麻	k?	麻
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ma	ma	k?	ma
<g/>
3	[number]	k4	3
<g/>
"	"	kIx"	"
–	–	k?	–
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
马	马	k?	马
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ma	ma	k?	ma
<g/>
4	[number]	k4	4
<g/>
"	"	kIx"	"
–	–	k?	–
nadávat	nadávat	k5eAaImF	nadávat
<g/>
,	,	kIx,	,
spílat	spílat	k5eAaImF	spílat
<g/>
,	,	kIx,	,
hubovat	hubovat	k5eAaImF	hubovat
<g/>
,	,	kIx,	,
骂	骂	k?	骂
Věta	věta	k1gFnSc1	věta
"	"	kIx"	"
<g/>
matka	matka	k1gFnSc1	matka
nadává	nadávat	k5eAaImIp3nS	nadávat
koni	kůň	k1gMnPc1	kůň
<g/>
"	"	kIx"	"
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
řekla	říct	k5eAaPmAgFnS	říct
"	"	kIx"	"
<g/>
ma	ma	k?	ma
<g/>
1	[number]	k4	1
ma	ma	k?	ma
<g/>
4	[number]	k4	4
ma	ma	k?	ma
<g/>
3	[number]	k4	3
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
jedné	jeden	k4xCgFnSc2	jeden
slabiky	slabika	k1gFnSc2	slabika
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
intonacemi	intonace	k1gFnPc7	intonace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
slabika	slabika	k1gFnSc1	slabika
"	"	kIx"	"
<g/>
yi	yi	k?	yi
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
yi	yi	k?	yi
<g/>
1	[number]	k4	1
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
číslovka	číslovka	k1gFnSc1	číslovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
一	一	k?	一
<g/>
,	,	kIx,	,
yi	yi	k?	yi
<g/>
2	[number]	k4	2
–	–	k?	–
obřad	obřad	k1gInSc1	obřad
<g/>
,	,	kIx,	,
仪	仪	k?	仪
<g/>
,	,	kIx,	,
yi	yi	k?	yi
<g/>
3	[number]	k4	3
–	–	k?	–
myslet	myslet	k5eAaImF	myslet
(	(	kIx(	(
<g/>
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
čínštině	čínština	k1gFnSc6	čínština
velmi	velmi	k6eAd1	velmi
používané	používaný	k2eAgNnSc1d1	používané
slovo	slovo	k1gNnSc1	slovo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
mnoha	mnoho	k4c2	mnoho
složených	složený	k2eAgNnPc2d1	složené
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
frekventované	frekventovaný	k2eAgNnSc4d1	frekventované
pomocné	pomocný	k2eAgNnSc4d1	pomocné
sloveso	sloveso	k1gNnSc4	sloveso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
以	以	k?	以
<g/>
,	,	kIx,	,
yi	yi	k?	yi
<g/>
4	[number]	k4	4
–	–	k?	–
sto	sto	k4xCgNnSc4	sto
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
číslovka	číslovka	k1gFnSc1	číslovka
a	a	k8xC	a
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
hodně	hodně	k6eAd1	hodně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
亿	亿	k?	亿
Důležité	důležitý	k2eAgMnPc4d1	důležitý
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pekingské	pekingský	k2eAgFnSc6d1	Pekingská
čínštině	čínština	k1gFnSc6	čínština
není	být	k5eNaImIp3nS	být
tónování	tónování	k1gNnSc4	tónování
založené	založený	k2eAgFnSc2d1	založená
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
výškových	výškový	k2eAgFnPc6d1	výšková
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
absolutní	absolutní	k2eAgInPc4d1	absolutní
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
výška	výška	k1gFnSc1	výška
hlasu	hlas	k1gInSc2	hlas
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
relativní	relativní	k2eAgInSc4d1	relativní
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc4d1	vysoký
tón	tón	k1gInSc4	tón
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
urostlého	urostlý	k2eAgMnSc2d1	urostlý
horníka	horník	k1gMnSc2	horník
se	s	k7c7	s
zakouřenými	zakouřený	k2eAgFnPc7d1	zakouřená
plícemi	plíce	k1gFnPc7	plíce
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
mnohem	mnohem	k6eAd1	mnohem
níž	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
nízký	nízký	k2eAgInSc1d1	nízký
tón	tón	k1gInSc1	tón
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
šestnáctileté	šestnáctiletý	k2eAgFnSc2d1	šestnáctiletá
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
relativní	relativní	k2eAgInPc4d1	relativní
vztahy	vztah	k1gInPc4	vztah
–	–	k?	–
první	první	k4xOgInSc4	první
tón	tón	k1gInSc4	tón
je	být	k5eAaImIp3nS	být
rovný	rovný	k2eAgMnSc1d1	rovný
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
začíná	začínat	k5eAaImIp3nS	začínat
vysoko	vysoko	k6eAd1	vysoko
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
nízko	nízko	k6eAd1	nízko
atd.	atd.	kA	atd.
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
se	se	k3xPyFc4	se
čínštině	čínština	k1gFnSc6	čínština
důležité	důležitý	k2eAgInPc1d1	důležitý
nejen	nejen	k6eAd1	nejen
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
tón	tón	k1gInSc4	tón
v	v	k7c6	v
izolovaném	izolovaný	k2eAgNnSc6d1	izolované
slově	slovo	k1gNnSc6	slovo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
nahrávkách	nahrávka	k1gFnPc6	nahrávka
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
tónech	tón	k1gInPc6	tón
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
rychlém	rychlý	k2eAgInSc6d1	rychlý
hovoru	hovor	k1gInSc6	hovor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
čínských	čínský	k2eAgInPc6d1	čínský
dialektech	dialekt	k1gInPc6	dialekt
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
šanghajském	šanghajský	k2eAgInSc6d1	šanghajský
existuje	existovat	k5eAaImIp3nS	existovat
"	"	kIx"	"
<g/>
absolutně	absolutně	k6eAd1	absolutně
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
"	"	kIx"	"
x	x	k?	x
"	"	kIx"	"
<g/>
absolutně	absolutně	k6eAd1	absolutně
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
"	"	kIx"	"
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Tónování	tónování	k1gNnSc1	tónování
není	být	k5eNaImIp3nS	být
neměnné	neměnný	k2eAgNnSc1d1	neměnné
<g/>
,	,	kIx,	,
frekventovaná	frekventovaný	k2eAgNnPc1d1	frekventované
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xC	jako
například	například	k6eAd1	například
číslovka	číslovka	k1gFnSc1	číslovka
"	"	kIx"	"
<g/>
yi	yi	k?	yi
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
záporka	záporka	k1gFnSc1	záporka
"	"	kIx"	"
<g/>
bu	bu	kA	bu
<g/>
4	[number]	k4	4
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
tónu	tón	k1gInSc6	tón
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
slovo	slovo	k1gNnSc1	slovo
následuje	následovat	k5eAaImIp3nS	následovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
celé	celý	k2eAgNnSc1d1	celé
spojení	spojení	k1gNnSc1	spojení
<g />
.	.	kIx.	.
</s>
<s>
vyslovovalo	vyslovovat	k5eAaImAgNnS	vyslovovat
snáze	snadno	k6eAd2	snadno
–	–	k?	–
např.	např.	kA	např.
před	před	k7c7	před
slovem	slovo	k1gNnSc7	slovo
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
tónu	tón	k1gInSc2	tón
se	se	k3xPyFc4	se
yi	yi	k?	yi
<g/>
1	[number]	k4	1
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
yi	yi	k?	yi
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
zkuste	zkusit	k5eAaPmRp2nP	zkusit
vyslovit	vyslovit	k5eAaPmF	vyslovit
oboje	oboj	k1gFnPc4	oboj
a	a	k8xC	a
ucítíte	ucítit	k5eAaPmIp2nP	ucítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
tón	tón	k1gInSc4	tón
po	po	k7c6	po
sobě	se	k3xPyFc3	se
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
a	a	k8xC	a
snáze	snadno	k6eAd2	snadno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rychlé	rychlý	k2eAgFnSc6d1	rychlá
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
tóny	tón	k1gInPc4	tón
u	u	k7c2	u
nepřízvučných	přízvučný	k2eNgNnPc2d1	nepřízvučné
slov	slovo	k1gNnPc2	slovo
zcela	zcela	k6eAd1	zcela
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
<g/>
,	,	kIx,	,
což	což	k9	což
rodilým	rodilý	k2eAgMnSc7d1	rodilý
mluvčím	mluvčí	k1gMnSc7	mluvčí
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
pochopí	pochopit	k5eAaPmIp3nP	pochopit
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
slovní	slovní	k2eAgFnSc2d1	slovní
intonace	intonace	k1gFnSc2	intonace
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
i	i	k9	i
větná	větný	k2eAgFnSc1d1	větná
intonace	intonace	k1gFnSc1	intonace
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
–	–	k?	–
celková	celkový	k2eAgFnSc1d1	celková
stoupavá	stoupavý	k2eAgFnSc1d1	stoupavá
intonace	intonace	k1gFnSc1	intonace
při	při	k7c6	při
otázce	otázka	k1gFnSc6	otázka
atd.	atd.	kA	atd.
Skloubit	skloubit	k5eAaPmF	skloubit
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
prvky	prvek	k1gInPc1	prvek
činí	činit	k5eAaImIp3nP	činit
cizincům	cizinec	k1gMnPc3	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
čínsky	čínsky	k6eAd1	čínsky
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc1d1	velká
potíže	potíž	k1gFnPc1	potíž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
naučí	naučit	k5eAaPmIp3nS	naučit
velmi	velmi	k6eAd1	velmi
obstojně	obstojně	k6eAd1	obstojně
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
tóny	tón	k1gInPc4	tón
v	v	k7c6	v
hovoru	hovor	k1gInSc6	hovor
a	a	k8xC	a
používat	používat	k5eAaImF	používat
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
intonace	intonace	k1gFnPc4	intonace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
natolik	natolik	k6eAd1	natolik
"	"	kIx"	"
<g/>
zpřirozenit	zpřirozenit	k5eAaPmF	zpřirozenit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvládnou	zvládnout	k5eAaPmIp3nP	zvládnout
i	i	k9	i
větné	větný	k2eAgFnPc1d1	větná
intonace	intonace	k1gFnPc1	intonace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
Číňané	Číňan	k1gMnPc1	Číňan
dokáží	dokázat	k5eAaPmIp3nP	dokázat
podle	podle	k7c2	podle
sluchu	sluch	k1gInSc2	sluch
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
poznat	poznat	k5eAaPmF	poznat
cizince	cizinec	k1gMnPc4	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
systém	systém	k1gInSc4	systém
intonací	intonace	k1gFnPc2	intonace
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
stejně	stejně	k6eAd1	stejně
včetně	včetně	k7c2	včetně
intonace	intonace	k1gFnSc2	intonace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
se	se	k3xPyFc4	se
jiným	jiný	k2eAgInSc7d1	jiný
znakem	znak	k1gInSc7	znak
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
homonym	homonymum	k1gNnPc2	homonymum
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ji	on	k3xPp3gFnSc4	on
<g/>
1	[number]	k4	1
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
slepice	slepice	k1gFnPc1	slepice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
鸡	鸡	k?	鸡
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
základ	základ	k1gInSc1	základ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
基	基	k?	基
V	v	k7c6	v
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
řeší	řešit	k5eAaImIp3nS	řešit
používáním	používání	k1gNnSc7	používání
většinou	většinou	k6eAd1	většinou
jednoznačných	jednoznačný	k2eAgNnPc2d1	jednoznačné
složených	složený	k2eAgNnPc2d1	složené
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
i	i	k9	i
tady	tady	k6eAd1	tady
existují	existovat	k5eAaImIp3nP	existovat
úplná	úplný	k2eAgNnPc1d1	úplné
homonyma	homonymum	k1gNnPc1	homonymum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
studentům	student	k1gMnPc3	student
čínštiny	čínština	k1gFnSc2	čínština
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
činit	činit	k5eAaImF	činit
problémy	problém	k1gInPc4	problém
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
mají	mít	k5eAaImIp3nP	mít
nějaký	nějaký	k3yIgInSc4	nějaký
komunikační	komunikační	k2eAgInSc4d1	komunikační
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chybí	chybět	k5eAaImIp3nS	chybět
jim	on	k3xPp3gFnPc3	on
dostatečně	dostatečně	k6eAd1	dostatečně
velká	velký	k2eAgFnSc1d1	velká
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
mohli	moct	k5eAaImAgMnP	moct
vždy	vždy	k6eAd1	vždy
domyslet	domyslet	k5eAaPmF	domyslet
ten	ten	k3xDgInSc4	ten
správný	správný	k2eAgInSc4d1	správný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
tónům	tón	k1gInPc3	tón
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
otázka	otázka	k1gFnSc1	otázka
sluchu	sluch	k1gInSc2	sluch
nebo	nebo	k8xC	nebo
schopností	schopnost	k1gFnPc2	schopnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
otázka	otázka	k1gFnSc1	otázka
vytrvalosti	vytrvalost	k1gFnSc2	vytrvalost
a	a	k8xC	a
zvyku	zvyk	k1gInSc2	zvyk
<g/>
,	,	kIx,	,
při	při	k7c6	při
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
tréninku	trénink	k1gInSc6	trénink
to	ten	k3xDgNnSc1	ten
zvládá	zvládat	k5eAaImIp3nS	zvládat
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
čínštinu	čínština	k1gFnSc4	čínština
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
používání	používání	k1gNnSc3	používání
významotvorných	významotvorný	k2eAgFnPc2d1	významotvorná
intonací	intonace	k1gFnPc2	intonace
mají	mít	k5eAaImIp3nP	mít
Číňané	Číňan	k1gMnPc1	Číňan
průměrně	průměrně	k6eAd1	průměrně
vytříbenější	vytříbený	k2eAgInSc4d2	vytříbenější
hudební	hudební	k2eAgInSc4d1	hudební
sluch	sluch	k1gInSc4	sluch
než	než	k8xS	než
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
vyšší	vysoký	k2eAgNnSc1d2	vyšší
procento	procento	k1gNnSc1	procento
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
absolutním	absolutní	k2eAgInSc7d1	absolutní
sluchem	sluch	k1gInSc7	sluch
(	(	kIx(	(
<g/>
schopností	schopnost	k1gFnPc2	schopnost
napevno	napevno	k6eAd1	napevno
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
výšku	výška	k1gFnSc4	výška
určitého	určitý	k2eAgInSc2d1	určitý
tónu	tón	k1gInSc2	tón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
doklady	doklad	k1gInPc4	doklad
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
pocházejí	pocházet	k5eAaImIp3nP	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
cca	cca	kA	cca
1200	[number]	k4	1200
př.n.l.	př.n.l.	k?	př.n.l.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
samozřejmě	samozřejmě	k6eAd1	samozřejmě
čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
doznalo	doznat	k5eAaPmAgNnS	doznat
řadu	řada	k1gFnSc4	řada
výrazných	výrazný	k2eAgFnPc2d1	výrazná
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
jazykové	jazykový	k2eAgFnSc3d1	jazyková
jednotce	jednotka	k1gFnSc3	jednotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
sepětím	sepětí	k1gNnSc7	sepětí
zvukové	zvukový	k2eAgFnSc2d1	zvuková
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
jedné	jeden	k4xCgFnSc3	jeden
slabice	slabika	k1gFnSc3	slabika
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
fonetické	fonetický	k2eAgFnSc6d1	fonetická
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
morfému	morfém	k1gInSc2	morfém
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jazykové	jazykový	k2eAgFnSc6d1	jazyková
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stará	starý	k2eAgFnSc1d1	stará
čínština	čínština	k1gFnSc1	čínština
byla	být	k5eAaImAgFnS	být
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
většina	většina	k1gFnSc1	většina
slov	slovo	k1gNnPc2	slovo
čítala	čítat	k5eAaImAgFnS	čítat
jeden	jeden	k4xCgInSc4	jeden
morfém	morfém	k1gInSc4	morfém
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
jednomu	jeden	k4xCgNnSc3	jeden
slovu	slovo	k1gNnSc3	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
však	však	k9	však
diametrálně	diametrálně	k6eAd1	diametrálně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
čtení	čtení	k1gNnSc1	čtení
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
slabiku	slabika	k1gFnSc4	slabika
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
významem	význam	k1gInSc7	význam
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
zápisu	zápis	k1gInSc2	zápis
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
jediného	jediný	k2eAgInSc2d1	jediný
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
dialektech	dialekt	k1gInPc6	dialekt
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
konvenčně	konvenčně	k6eAd1	konvenčně
k	k	k7c3	k
znaku	znak	k1gInSc3	znak
vázán	vázán	k2eAgInSc1d1	vázán
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Číňané	Číňan	k1gMnPc1	Číňan
znalí	znalý	k2eAgMnPc1d1	znalý
písma	písmo	k1gNnSc2	písmo
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
oblastí	oblast	k1gFnPc2	oblast
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
písemnou	písemný	k2eAgFnSc7d1	písemná
formou	forma	k1gFnSc7	forma
dorozumějí	dorozumět	k5eAaPmIp3nP	dorozumět
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
mluveném	mluvený	k2eAgInSc6d1	mluvený
projevu	projev	k1gInSc6	projev
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
součástek	součástka	k1gFnPc2	součástka
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
(	(	kIx(	(
<g/>
v	v	k7c6	v
90	[number]	k4	90
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
součástí	součást	k1gFnPc2	součást
<g/>
:	:	kIx,	:
fonetikum	fonetikum	k?	fonetikum
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
výslovnost	výslovnost	k1gFnSc1	výslovnost
a	a	k8xC	a
radikál	radikál	k1gInSc1	radikál
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
významovou	významový	k2eAgFnSc4d1	významová
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
<g/>
,	,	kIx,	,
piktografický	piktografický	k2eAgInSc1d1	piktografický
typ	typ	k1gInSc1	typ
znaků	znak	k1gInPc2	znak
stylizovaně	stylizovaně	k6eAd1	stylizovaně
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
označuje	označovat	k5eAaImIp3nS	označovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
strom	strom	k1gInSc1	strom
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
však	však	k9	však
od	od	k7c2	od
nejstarší	starý	k2eAgFnSc2d3	nejstarší
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
menšině	menšina	k1gFnSc6	menšina
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
zastoupení	zastoupení	k1gNnSc4	zastoupení
téměř	téměř	k6eAd1	téměř
nulové	nulový	k2eAgFnPc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
nové	nový	k2eAgInPc4d1	nový
(	(	kIx(	(
<g/>
zjednodušené	zjednodušený	k2eAgInPc4d1	zjednodušený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavedené	zavedený	k2eAgFnPc4d1	zavedená
reformou	reforma	k1gFnSc7	reforma
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
pevninské	pevninský	k2eAgFnSc6d1	pevninská
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc6d1	tradiční
(	(	kIx(	(
<g/>
složité	složitý	k2eAgFnSc6d1	složitá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
Tchaiwanu	Tchaiwana	k1gFnSc4	Tchaiwana
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
a	a	k8xC	a
v	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
čínských	čínský	k2eAgFnPc6d1	čínská
komunitách	komunita	k1gFnPc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jedním	jeden	k4xCgNnSc7	jeden
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
abecedou	abeceda	k1gFnSc7	abeceda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
zapsat	zapsat	k5eAaPmF	zapsat
znaky	znak	k1gInPc4	znak
jiného	jiný	k2eAgNnSc2d1	jiné
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Transkripcí	transkripce	k1gFnSc7	transkripce
čínštiny	čínština	k1gFnSc2	čínština
se	se	k3xPyFc4	se
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
začali	začít	k5eAaPmAgMnP	začít
učit	učit	k5eAaImF	učit
čínsky	čínsky	k6eAd1	čínsky
<g/>
,	,	kIx,	,
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
čtenáře	čtenář	k1gMnSc4	čtenář
či	či	k8xC	či
pisatele	pisatel	k1gMnSc4	pisatel
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
aktuální	aktuální	k2eAgInPc1d1	aktuální
zejména	zejména	k9	zejména
dvě	dva	k4xCgNnPc1	dva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
různé	různý	k2eAgInPc4d1	různý
zmatky	zmatek	k1gInPc4	zmatek
a	a	k8xC	a
nepřesnosti	nepřesnost	k1gFnPc4	nepřesnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
uměleckých	umělecký	k2eAgInPc6d1	umělecký
textech	text	k1gInPc6	text
i	i	k8xC	i
části	část	k1gFnPc1	část
médií	médium	k1gNnPc2	médium
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkáme	setkat	k5eAaPmIp1nP	setkat
se	s	k7c7	s
standardním	standardní	k2eAgInSc7d1	standardní
českým	český	k2eAgInSc7d1	český
přepisem	přepis	k1gInSc7	přepis
z	z	k7c2	z
r.	r.	kA	r.
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Oldřich	Oldřich	k1gMnSc1	Oldřich
Švarný	švarný	k2eAgMnSc1d1	švarný
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
výslovnost	výslovnost	k1gFnSc1	výslovnost
čínštiny	čínština	k1gFnSc2	čínština
a	a	k8xC	a
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
výslovnosti	výslovnost	k1gFnSc6	výslovnost
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
ch	ch	k0	ch
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
čte	číst	k5eAaImIp3nS	číst
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ch	ch	k0	ch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
jako	jako	k9	jako
anglické	anglický	k2eAgNnSc1d1	anglické
"	"	kIx"	"
<g/>
č	č	k0	č
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
čtení	čtení	k1gNnSc4	čtení
nedělá	dělat	k5eNaImIp3nS	dělat
českému	český	k2eAgMnSc3d1	český
čtenáři	čtenář	k1gMnSc3	čtenář
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
včetně	včetně	k7c2	včetně
Číňanů	Číňan	k1gMnPc2	Číňan
však	však	k9	však
není	být	k5eNaImIp3nS	být
použitelný	použitelný	k2eAgInSc1d1	použitelný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
při	při	k7c6	při
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
styku	styk	k1gInSc6	styk
je	být	k5eAaImIp3nS	být
praktické	praktický	k2eAgNnSc1d1	praktické
následovat	následovat	k5eAaImF	následovat
příkladu	příklad	k1gInSc3	příklad
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
používá	používat	k5eAaImIp3nS	používat
oficiální	oficiální	k2eAgFnSc1d1	oficiální
čínská	čínský	k2eAgFnSc1d1	čínská
transkripce	transkripce	k1gFnSc1	transkripce
pchin-jin	pchinina	k1gFnPc2	pchin-jina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
čínštiny	čínština	k1gFnSc2	čínština
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
psaná	psaný	k2eAgNnPc1d1	psané
v	v	k7c6	v
pinyinu	pinyin	k1gInSc6	pinyin
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
bychom	by	kYmCp1nP	by
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvyklostí	zvyklost	k1gFnPc2	zvyklost
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
např.	např.	kA	např.
qin	qin	k?	qin
přečteme	přečíst	k5eAaPmIp1nP	přečíst
ne	ne	k9	ne
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kvin	kvin	k1gMnSc1	kvin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
čchin	čchin	k1gInSc1	čchin
<g/>
,	,	kIx,	,
xie	xie	k?	xie
jako	jako	k8xS	jako
sie	sie	k?	sie
<g/>
,	,	kIx,	,
jia	jia	k?	jia
jako	jako	k8xC	jako
ťia	ťia	k?	ťia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglických	anglický	k2eAgInPc6d1	anglický
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
Wade-Gilesovou	Wade-Gilesový	k2eAgFnSc7d1	Wade-Gilesový
transkripcí	transkripce	k1gFnSc7	transkripce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
jako	jako	k9	jako
nejčastější	častý	k2eAgInSc4d3	nejčastější
přepis	přepis	k1gInSc4	přepis
v	v	k7c6	v
anglofonních	anglofonní	k2eAgFnPc6d1	anglofonní
zemích	zem	k1gFnPc6	zem
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jako	jako	k9	jako
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
standard	standard	k1gInSc4	standard
prosadil	prosadit	k5eAaPmAgMnS	prosadit
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
.	.	kIx.	.
</s>
<s>
Uveďme	uvést	k5eAaPmRp1nP	uvést
jeden	jeden	k4xCgInSc4	jeden
příklad	příklad	k1gInSc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
ČLR	ČLR	kA	ČLR
"	"	kIx"	"
<g/>
Peking	Peking	k1gInSc1	Peking
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
znaky	znak	k1gInPc4	znak
北	北	k?	北
a	a	k8xC	a
京	京	k?	京
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
sever	sever	k1gInSc4	sever
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
čínštině	čínština	k1gFnSc6	čínština
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
českou	český	k2eAgFnSc7d1	Česká
transkripcí	transkripce	k1gFnSc7	transkripce
dostaneme	dostat	k5eAaPmIp1nP	dostat
tvar	tvar	k1gInSc4	tvar
"	"	kIx"	"
<g/>
Pej-ťing	Pej-ťing	k1gInSc4	Pej-ťing
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
pchinjinu	pchinjin	k1gInSc6	pchinjin
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc4	samý
slovo	slovo	k1gNnSc4	slovo
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Beijing	Beijing	k1gInSc1	Beijing
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
světovými	světový	k2eAgNnPc7d1	světové
médii	médium	k1gNnPc7	médium
pomalu	pomalu	k6eAd1	pomalu
opouštěný	opouštěný	k2eAgInSc1d1	opouštěný
"	"	kIx"	"
<g/>
Peking	Peking	k1gInSc1	Peking
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
starších	starý	k2eAgFnPc2d2	starší
transkripcí	transkripce	k1gFnPc2	transkripce
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
skutečně	skutečně	k6eAd1	skutečně
vyslovoval	vyslovovat	k5eAaImAgInS	vyslovovat
zhruba	zhruba	k6eAd1	zhruba
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Pak-king	Paking	k1gInSc1	Pak-king
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
severních	severní	k2eAgInPc6d1	severní
dialektech	dialekt	k1gInPc6	dialekt
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tak	tak	k9	tak
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
<g/>
,	,	kIx,	,
v	v	k7c4	v
severních	severní	k2eAgMnPc2d1	severní
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
posunu	posun	k1gInSc2	posun
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ki	ki	k?	ki
→	→	k?	→
ťi	ťi	k?	ťi
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Průšek	Průšek	k1gMnSc1	Průšek
<g/>
:	:	kIx,	:
Učebnice	učebnice	k1gFnSc1	učebnice
mluvené	mluvený	k2eAgFnSc2d1	mluvená
čínštiny	čínština	k1gFnSc2	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
lidová	lidový	k2eAgFnSc1d1	lidová
škola	škola	k1gFnSc1	škola
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
153	[number]	k4	153
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
O.	O.	kA	O.
Švarný	švarný	k2eAgMnSc1d1	švarný
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Bartůšek	Bartůšek	k1gMnSc1	Bartůšek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Kalousková	Kalousková	k1gFnSc1	Kalousková
a	a	k8xC	a
Ťing-jü	Ťingü	k1gFnSc1	Ťing-jü
Rotterová	Rotterová	k1gFnSc1	Rotterová
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
hovorové	hovorový	k2eAgFnSc2d1	hovorová
čínštiny	čínština	k1gFnSc2	čínština
<g/>
.	.	kIx.	.
</s>
<s>
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
I.	I.	kA	I.
<g/>
,	,	kIx,	,
458	[number]	k4	458
pp	pp	k?	pp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
606	[number]	k4	606
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
O.	O.	kA	O.
Švarný	švarný	k2eAgMnSc1d1	švarný
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Lomová	lomový	k2eAgFnSc1d1	Lomová
a	a	k8xC	a
Tchang	Tchang	k1gInSc1	Tchang
Jün-ling	Jüning	k1gInSc1	Jün-ling
Rusková	Rusková	k1gFnSc1	Rusková
<g/>
:	:	kIx,	:
Gramatika	gramatika	k1gFnSc1	gramatika
hovorové	hovorový	k2eAgFnSc2d1	hovorová
čínštiny	čínština	k1gFnSc2	čínština
v	v	k7c6	v
příkladech	příklad	k1gInPc6	příklad
[	[	kIx(	[
<g/>
skriptum	skriptum	k1gNnSc4	skriptum
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Vydavateľstvo	Vydavateľstvo	k1gNnSc1	Vydavateľstvo
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
I.	I.	kA	I.
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
307	[number]	k4	307
pp	pp	k?	pp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
567	[number]	k4	567
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Švarný	švarný	k2eAgMnSc1d1	švarný
<g/>
:	:	kIx,	:
Učební	učební	k2eAgInSc1d1	učební
slovník	slovník	k1gInSc1	slovník
jazyka	jazyk	k1gInSc2	jazyk
čínského	čínský	k2eAgInSc2d1	čínský
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
289	[number]	k4	289
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Liščák	Liščák	k1gMnSc1	Liščák
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Transkripce	transkripce	k1gFnSc1	transkripce
čínštiny	čínština	k1gFnSc2	čínština
(	(	kIx(	(
<g/>
I.	I.	kA	I.
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
;	;	kIx,	;
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tabulky	tabulka	k1gFnPc1	tabulka
a	a	k8xC	a
návody	návod	k1gInPc1	návod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česko-čínská	česko-čínský	k2eAgFnSc1d1	česko-čínská
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
260	[number]	k4	260
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
:	:	kIx,	:
Knížka	knížka	k1gFnSc1	knížka
o	o	k7c6	o
čínštině	čínština	k1gFnSc6	čínština
<g/>
,	,	kIx,	,
DesertRose	DesertRosa	k1gFnSc6	DesertRosa
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
207	[number]	k4	207
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čínština	čínština	k1gFnSc1	čínština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čínština	čínština	k1gFnSc1	čínština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.cinsky.cz	www.cinsky.cz	k1gMnSc1	www.cinsky.cz
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
se	s	k7c7	s
slovníkem	slovník	k1gInSc7	slovník
a	a	k8xC	a
automatickou	automatický	k2eAgFnSc7d1	automatická
tabulkou	tabulka	k1gFnSc7	tabulka
pro	pro	k7c4	pro
převod	převod	k1gInSc4	převod
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
čínskou	čínský	k2eAgFnSc7d1	čínská
transkripcí	transkripce	k1gFnSc7	transkripce
(	(	kIx(	(
<g/>
pchin-jinem	pchinin	k1gInSc7	pchin-jin
<g/>
)	)	kIx)	)
www.cinstina.cz	www.cinstina.cz	k1gInSc1	www.cinstina.cz
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
s	s	k7c7	s
podrobným	podrobný	k2eAgInSc7d1	podrobný
popisem	popis	k1gInSc7	popis
čínštiny	čínština	k1gFnSc2	čínština
a	a	k8xC	a
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
www.hua2.cz	www.hua2.cz	k1gInSc4	www.hua2.cz
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
čínštině	čínština	k1gFnSc6	čínština
a	a	k8xC	a
jazykovými	jazykový	k2eAgInPc7d1	jazykový
nástroji	nástroj	k1gInPc7	nástroj
Čínsko-český	čínsko-český	k2eAgInSc4d1	čínsko-český
slovník	slovník	k1gInSc4	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
Stručná	stručný	k2eAgFnSc1d1	stručná
čínská	čínský	k2eAgFnSc1d1	čínská
konverzace	konverzace	k1gFnSc1	konverzace
volně	volně	k6eAd1	volně
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
–	–	k?	–
znaky	znak	k1gInPc7	znak
i	i	k8xC	i
transkripcí	transkripce	k1gFnSc7	transkripce
pchin-jin	pchinina	k1gFnPc2	pchin-jina
–	–	k?	–
také	také	k9	také
online	onlinout	k5eAaPmIp3nS	onlinout
Videolekce	Videolekce	k1gFnSc1	Videolekce
čínštiny	čínština	k1gFnSc2	čínština
na	na	k7c4	na
rtbot	rtbot	k1gInSc4	rtbot
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
(	(	kIx(	(
<g/>
některé	některý	k3yIgNnSc4	některý
se	s	k7c7	s
známými	známý	k2eAgFnPc7d1	známá
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
)	)	kIx)	)
</s>
