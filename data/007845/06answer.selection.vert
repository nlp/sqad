<s>
Čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
čínské	čínský	k2eAgInPc1d1	čínský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
汉	汉	k?	汉
/	/	kIx~	/
漢	漢	k?	漢
<g/>
;	;	kIx,	;
Chan-jü	Chanü	k1gMnSc1	Chan-jü
nebo	nebo	k8xC	nebo
中	中	k?	中
<g/>
;	;	kIx,	;
Čung-wen	Čungen	k1gInSc1	Čung-wen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
vzájemně	vzájemně	k6eAd1	vzájemně
nesrozumitelných	srozumitelný	k2eNgInPc2d1	nesrozumitelný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
sinotibetské	sinotibetský	k2eAgFnSc2d1	sinotibetský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
