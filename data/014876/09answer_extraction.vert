Roku	rok	k1gInSc2
833	#num#	k4
se	se	k3xPyFc4
knížeti	kníže	k1gMnSc3
Mojmírovi	Mojmír	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
Pribinu	Pribina	k1gFnSc4
z	z	k7c2
Nitry	Nitra	k1gFnSc2
vypudit	vypudit	k5eAaPmF
<g/>
,	,	kIx,
připojil	připojit	k5eAaPmAgInS
jeho	jeho	k3xOp3gNnSc4
knížectví	knížectví	k1gNnSc4
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
a	a	k8xC
tento	tento	k3xDgInSc1
krok	krok	k1gInSc1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
vznik	vznik	k1gInSc4
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
