<s>
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
MoravaRegnum	MoravaRegnum	k1gInSc1
Marauorum	Marauorum	k1gInSc1
/	/	kIx~
Marahensium	Marahensium	k1gNnSc1
(	(	kIx(
<g/>
la	la	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Terra	Terra	k1gFnSc1
Marauorum	Marauorum	k1gInSc1
/	/	kIx~
Marahensium	Marahensium	k1gNnSc1
(	(	kIx(
<g/>
la	la	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
←	←	k?
←	←	k?
←	←	k?
←	←	k?
</s>
<s>
833	#num#	k4
<g/>
–	–	k?
<g/>
907	#num#	k4
</s>
<s>
→	→	k?
→	→	k?
→	→	k?
→	→	k?
→	→	k?
→	→	k?
→	→	k?
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Přibližná	přibližný	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
členění	členění	k1gNnSc1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Veligrad	Veligrad	k1gInSc1
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Moravané	Moravan	k1gMnPc1
<g/>
,	,	kIx,
Slezané	Slezan	k1gMnPc1
<g/>
,	,	kIx,
Lužičtí	lužický	k2eAgMnPc1d1
Srbové	Srb	k1gMnPc1
<g/>
,	,	kIx,
Češi	Čech	k1gMnPc1
<g/>
,	,	kIx,
Uhři	Uhř	k1gMnPc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
staroslověnština	staroslověnština	k1gFnSc1
<g/>
,	,	kIx,
latina	latina	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
slovanské	slovanský	k2eAgNnSc4d1
křesťanství	křesťanství	k1gNnSc4
<g/>
,	,	kIx,
<g/>
latinské	latinský	k2eAgNnSc4d1
křesťanství	křesťanství	k1gNnSc4
<g/>
,	,	kIx,
<g/>
staroslovanské	staroslovanský	k2eAgNnSc4d1
(	(	kIx(
<g/>
„	„	k?
<g/>
pohanské	pohanský	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
monarchie	monarchie	k1gFnSc1
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
833	#num#	k4
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
907	#num#	k4
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Nitranské	nitranský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Vislané	Vislaný	k2eAgNnSc1d1
</s>
<s>
Avarská	avarský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Maďarské	maďarský	k2eAgNnSc1d1
velkoknížectví	velkoknížectví	k1gNnSc1
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Nitranské	nitranský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Polské	polský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Východofranská	východofranský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Lutici	Lutik	k1gMnPc1
</s>
<s>
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Moravia	Moravia	k1gFnSc1
Magna	Magna	k1gFnSc1
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
Μ	Μ	k?
Μ	Μ	k?
–	–	k?
megáli	megále	k1gFnSc3
Moravía	Moravía	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pozdější	pozdní	k2eAgNnSc4d2
historické	historický	k2eAgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
první	první	k4xOgNnSc4
stabilní	stabilní	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
západních	západní	k2eAgMnPc2d1
Slovanů	Slovan	k1gMnPc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
označení	označení	k1gNnSc6
„	„	k?
<g/>
stát	stát	k1gInSc1
Mojmírovců	Mojmírovec	k1gMnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
neexistuje	existovat	k5eNaImIp3nS
nebo	nebo	k8xC
není	být	k5eNaImIp3nS
znám	znám	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládala	rozkládat	k5eAaImAgNnP
se	se	k3xPyFc4
převážně	převážně	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
Slovenska	Slovensko	k1gNnSc2
a	a	k8xC
Maďarska	Maďarsko	k1gNnSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
833	#num#	k4
<g/>
–	–	k?
<g/>
907	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Důvod	důvod	k1gInSc1
zániku	zánik	k1gInSc2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
přímo	přímo	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
invazemi	invaze	k1gFnPc7
Maďarů	Maďar	k1gMnPc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
nebyla	být	k5eNaImAgFnS
to	ten	k3xDgNnSc1
příčina	příčina	k1gFnSc1
jediná	jediný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
mohly	moct	k5eAaImAgInP
sehrát	sehrát	k5eAaPmF
i	i	k9
faktory	faktor	k1gInPc4
ekologické	ekologický	k2eAgInPc4d1
(	(	kIx(
<g/>
přední	přední	k2eAgInPc4d1
moravské	moravský	k2eAgInPc4d1
hrady	hrad	k1gInPc4
ležely	ležet	k5eAaImAgFnP
v	v	k7c6
říčních	říční	k2eAgFnPc6d1
nivách	niva	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hospodářské	hospodářský	k2eAgNnSc4d1
(	(	kIx(
<g/>
údajné	údajný	k2eAgNnSc4d1
oslabení	oslabení	k1gNnSc4
obchodních	obchodní	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
pominout	pominout	k5eAaPmF
nelze	lze	k6eNd1
ani	ani	k8xC
nesvornost	nesvornost	k1gFnSc1
domácích	domácí	k2eAgFnPc2d1
elit	elita	k1gFnPc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
projevem	projev	k1gInSc7
byl	být	k5eAaImAgMnS
střet	střet	k1gInSc4
Mojmíra	Mojmír	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Svatopluka	Svatopluk	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
synů	syn	k1gMnPc2
Svatopluka	Svatopluk	k1gMnSc2
I.	I.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Lubomír	Lubomír	k1gMnSc1
Havlík	Havlík	k1gMnSc1
však	však	k9
má	mít	k5eAaImIp3nS
teorii	teorie	k1gFnSc4
o	o	k7c6
zániku	zánik	k1gInSc6
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1055	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdůvodňuje	zdůvodňovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Moravané	Moravan	k1gMnPc1
se	se	k3xPyFc4
bitvy	bitva	k1gFnSc2
neúčastnili	účastnit	k5eNaImAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
nešlo	jít	k5eNaImAgNnS
o	o	k7c4
jejich	jejich	k3xOp3gInPc4
životní	životní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
a	a	k8xC
moravský	moravský	k2eAgInSc1d1
stát	stát	k1gInSc1
nezanikl	zaniknout	k5eNaPmAgInS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
čemuž	což	k3yQnSc3,k3yRnSc3
nasvědčují	nasvědčovat	k5eAaImIp3nP
i	i	k8xC
prameny	pramen	k1gInPc1
z	z	k7c2
období	období	k1gNnSc2
po	po	k7c6
roce	rok	k1gInSc6
907	#num#	k4
<g/>
,	,	kIx,
například	například	k6eAd1
spis	spis	k1gInSc1
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porfyrogenneta	Porfyrogenneta	k1gFnSc1
"	"	kIx"
<g/>
O	o	k7c6
ceremoniích	ceremonie	k1gFnPc6
byzantského	byzantský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
,	,	kIx,
<g/>
"	"	kIx"
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
zmíněn	zmíněn	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
spisů	spis	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Žádné	žádný	k3yNgInPc1
prameny	pramen	k1gInPc1
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
název	název	k1gInSc1
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
neobsahují	obsahovat	k5eNaImIp3nP
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
používaly	používat	k5eAaImAgInP
jednoduché	jednoduchý	k2eAgInPc1d1
názvy	název	k1gInPc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
M.	M.	kA
<g/>
ráwa	ráwa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
t	t	k?
<g/>
,	,	kIx,
Marava	Marava	k1gFnSc1
<g/>
,	,	kIx,
Murava	Murava	k1gFnSc1
<g/>
,	,	kIx,
Marawa	Marawa	k1gFnSc1
<g/>
,	,	kIx,
Marauia	Marauia	k1gFnSc1
<g/>
,	,	kIx,
Maraha	Maraha	k1gFnSc1
(	(	kIx(
<g/>
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maroara	Maroara	k1gFnSc1
lond	lond	k1gMnSc1
<g/>
,	,	kIx,
terra	terra	k1gMnSc1
Marauorum	Marauorum	k1gInSc1
(	(	kIx(
<g/>
Moravská	moravský	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
terra	terra	k6eAd1
Marahensium	Marahensium	k1gNnSc1
(	(	kIx(
<g/>
země	zem	k1gFnPc1
Moravanů	Moravan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
stát	stát	k1gInSc4
a	a	k8xC
říši	říše	k1gFnSc4
se	se	k3xPyFc4
používaly	používat	k5eAaImAgInP
další	další	k2eAgInPc1d1
tvary	tvar	k1gInPc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
regnum	regnum	k1gInSc1
Marauorum	Marauorum	k1gInSc1
<g/>
,	,	kIx,
regnum	regnum	k1gInSc1
Marahauorum	Marahauorum	k1gInSc1
<g/>
,	,	kIx,
regnum	regnum	k1gInSc1
Margorum	Margorum	k1gInSc1
<g/>
,	,	kIx,
regnum	regnum	k1gNnSc1
Marahensium	Marahensium	k1gNnSc1
(	(	kIx(
<g/>
království	království	k1gNnSc1
Moravanů	Moravan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
regnum	regnum	k1gInSc1
Sclavorum	Sclavorum	k1gNnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
království	království	k1gNnSc1
Slovanů	Slovan	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
popř.	popř.	kA
"	"	kIx"
<g/>
regnum	regnum	k1gInSc1
Rastizi	Rastize	k1gFnSc3
(	(	kIx(
<g/>
království	království	k1gNnSc1
Rostislavovo	Rostislavův	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
regnum	regnum	k1gInSc4
Zuentibaldi	Zuentibald	k1gMnPc1
(	(	kIx(
<g/>
království	království	k1gNnSc1
Svatoplukovo	Svatoplukův	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
je	být	k5eAaImIp3nS
pozdějšího	pozdní	k2eAgNnSc2d2
data	datum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgMnS
zmíněn	zmínit	k5eAaPmNgMnS
byzantským	byzantský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Konstantinem	Konstantin	k1gMnSc7
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porfyrogennetem	Porfyrogennet	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
950	#num#	k4
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
díle	dílo	k1gNnSc6
De	De	k?
administrando	administrando	k6eAd1
imperio	imperio	k6eAd1
(	(	kIx(
<g/>
O	o	k7c6
spravování	spravování	k1gNnSc6
říše	říš	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
"	"	kIx"
<g/>
hé	hé	k0
megalé	megalý	k2eAgFnPc1d1
Moravia	Moravium	k1gNnPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
avšak	avšak	k8xC
její	její	k3xOp3gFnSc4
polohu	poloha	k1gFnSc4
umísťuje	umísťovat	k5eAaImIp3nS
na	na	k7c4
Balkán	Balkán	k1gInSc4
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
od	od	k7c2
sídel	sídlo	k1gNnPc2
Maďarů	Maďar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
argumenty	argument	k1gInPc7
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
některé	některý	k3yIgMnPc4
badatele	badatel	k1gMnPc4
(	(	kIx(
<g/>
Boba	Bob	k1gMnSc4
<g/>
,	,	kIx,
Eggers	Eggers	k1gInSc1
<g/>
,	,	kIx,
Bowlus	Bowlus	k1gInSc1
<g/>
)	)	kIx)
důvodem	důvod	k1gInSc7
k	k	k7c3
úvahám	úvaha	k1gFnPc3
<g/>
,	,	kIx,
zda	zda	k8xS
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
nehledat	hledat	k5eNaImF
spíše	spíše	k9
na	na	k7c6
území	území	k1gNnSc6
bývalé	bývalý	k2eAgFnSc2d1
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiným	jiný	k2eAgNnSc7d1
možným	možný	k2eAgNnSc7d1
vysvětlením	vysvětlení	k1gNnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hunská	hunský	k2eAgFnSc1d1
enkláva	enkláva	k1gFnSc1
v	v	k7c6
srdci	srdce	k1gNnSc6
říše	říš	k1gFnSc2
rozdělila	rozdělit	k5eAaPmAgFnS
původně	původně	k6eAd1
souvislé	souvislý	k2eAgNnSc4d1
území	území	k1gNnSc4
se	s	k7c7
slovanským	slovanský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
a	a	k8xC
Konstantinovi	Konstantinův	k2eAgMnPc1d1
VII	VII	kA
byly	být	k5eAaImAgInP
známy	znám	k2eAgInPc1d1
jen	jen	k8xS
jižní	jižní	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
jako	jako	k8xS,k8xC
následníci	následník	k1gMnPc1
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gMnSc6
použil	použít	k5eAaPmAgMnS
názvu	název	k1gInSc3
"	"	kIx"
<g/>
Velika	Velika	k1gFnSc1
Morava	Morava	k1gFnSc1
<g/>
"	"	kIx"
až	až	k9
autor	autor	k1gMnSc1
legendy	legenda	k1gFnSc2
"	"	kIx"
<g/>
Uspenie	Uspenie	k1gFnSc1
Kirila	Kiril	k1gMnSc2
Filozofa	filozof	k1gMnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
tento	tento	k3xDgInSc1
název	název	k1gInSc1
spojoval	spojovat	k5eAaImAgInS
s	s	k7c7
dobou	doba	k1gFnSc7
vlády	vláda	k1gFnSc2
Rostislava	Rostislava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
během	během	k7c2
středověku	středověk	k1gInSc2
se	se	k3xPyFc4
ovšem	ovšem	k9
tento	tento	k3xDgInSc1
přídavek	přídavek	k1gInSc1
"	"	kIx"
<g/>
Velký	velký	k2eAgInSc1d1
<g/>
"	"	kIx"
stal	stát	k5eAaPmAgInS
nesrozumitelným	srozumitelný	k2eNgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
takový	takový	k3xDgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
součástí	součást	k1gFnPc2
sporů	spor	k1gInPc2
i	i	k8xC
mezi	mezi	k7c7
národy	národ	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
dnes	dnes	k6eAd1
obývají	obývat	k5eAaImIp3nP
předpokládané	předpokládaný	k2eAgNnSc4d1
území	území	k1gNnSc4
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Češi	Čech	k1gMnPc1
a	a	k8xC
Němci	Němec	k1gMnPc1
trvají	trvat	k5eAaImIp3nP
na	na	k7c4
označení	označení	k1gNnSc4
"	"	kIx"
<g/>
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
"	"	kIx"
<g/>
/	/	kIx~
<g/>
"	"	kIx"
<g/>
Großmähren	Großmährno	k1gNnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
např.	např.	kA
Maďaři	Maďar	k1gMnPc1
je	on	k3xPp3gMnPc4
zavrhují	zavrhovat	k5eAaImIp3nP
a	a	k8xC
používají	používat	k5eAaImIp3nP
jiné	jiný	k2eAgInPc1d1
výrazy	výraz	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
"	"	kIx"
<g/>
Reich	Reich	k?
der	drát	k5eAaImRp2nS
Mährer	Mährer	k1gMnSc1
<g/>
"	"	kIx"
<g/>
/	/	kIx~
<g/>
"	"	kIx"
<g/>
Říše	říše	k1gFnSc1
Moravanů	Moravan	k1gMnPc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Označení	označení	k1gNnSc1
"	"	kIx"
<g/>
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
"	"	kIx"
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
opodstatnění	opodstatnění	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
jako	jako	k8xC,k8xS
paralela	paralela	k1gFnSc1
k	k	k7c3
obdobným	obdobný	k2eAgInPc3d1
označením	označení	k1gNnSc7
(	(	kIx(
<g/>
Velkopolsko	Velkopolsko	k1gNnSc1
<g/>
,	,	kIx,
Velkorusko	Velkorusko	k1gNnSc1
<g/>
)	)	kIx)
u	u	k7c2
vědomí	vědomí	k1gNnSc2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
označení	označení	k1gNnSc1
nikterak	nikterak	k6eAd1
nekvalifikuje	kvalifikovat	k5eNaBmIp3nS
tehdejší	tehdejší	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Morava	Morava	k1gFnSc1
a	a	k8xC
Nitransko	Nitransko	k1gNnSc1
do	do	k7c2
roku	rok	k1gInSc2
833	#num#	k4
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Hlaholice	hlaholice	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
existoval	existovat	k5eAaImAgMnS
pravděpodobný	pravděpodobný	k2eAgMnSc1d1
předchůdce	předchůdce	k1gMnSc1
moravského	moravský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Sámova	Sámův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
rozkládal	rozkládat	k5eAaImAgInS
nejspíše	nejspíše	k9
v	v	k7c6
povodí	povodí	k1gNnSc6
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
na	na	k7c6
jihozápadním	jihozápadní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
na	na	k7c6
územích	území	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
Moravské	moravský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
a	a	k8xC
Nitranské	nitranský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
(	(	kIx(
<g/>
oba	dva	k4xCgInPc1
názvy	název	k1gInPc1
jsou	být	k5eAaImIp3nP
výtvorem	výtvor	k1gInSc7
novodobé	novodobý	k2eAgFnSc2d1
historiografie	historiografie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
ale	ale	k9
spíše	spíše	k9
o	o	k7c4
kmenový	kmenový	k2eAgInSc4d1
svaz	svaz	k1gInSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
ještě	ještě	k9
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
hodnocena	hodnotit	k5eAaImNgFnS
jako	jako	k8xS,k8xC
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
době	doba	k1gFnSc6
od	od	k7c2
zániku	zánik	k1gInSc2
Sámovy	Sámův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
659	#num#	k4
<g/>
–	–	k?
<g/>
661	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
zformování	zformování	k1gNnSc2
moravského	moravský	k2eAgInSc2d1
státu	stát	k1gInSc2
veškeré	veškerý	k3xTgInPc4
historické	historický	k2eAgInPc4d1
prameny	pramen	k1gInPc4
mlčí	mlčet	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
nejasné	jasný	k2eNgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
na	na	k7c6
moravském	moravský	k2eAgNnSc6d1
území	území	k1gNnSc6
odehrávalo	odehrávat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
již	již	k6eAd1
pro	pro	k7c4
druhou	druhý	k4xOgFnSc4
polovinu	polovina	k1gFnSc4
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
archeologické	archeologický	k2eAgInPc1d1
prameny	pramen	k1gInPc1
dokládají	dokládat	k5eAaImIp3nP
existenci	existence	k1gFnSc4
místních	místní	k2eAgFnPc2d1
elit	elita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
orientovaly	orientovat	k5eAaBmAgFnP
na	na	k7c4
avarský	avarský	k2eAgInSc4d1
kaganát	kaganát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
ke	k	k7c3
konci	konec	k1gInSc3
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zhroutil	zhroutit	k5eAaPmAgMnS
avarský	avarský	k2eAgInSc4d1
kaganát	kaganát	k1gInSc4
pod	pod	k7c7
útokem	útok	k1gInSc7
vojsk	vojsko	k1gNnPc2
Karla	Karel	k1gMnSc2
Velikého	veliký	k2eAgInSc2d1
<g/>
,	,	kIx,
místní	místní	k2eAgFnSc2d1
elity	elita	k1gFnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
orientovat	orientovat	k5eAaBmF
spíše	spíše	k9
na	na	k7c4
franckou	francký	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
a	a	k8xC
stabilizovaly	stabilizovat	k5eAaBmAgFnP
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
knížectví	knížectví	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
spolu	spolu	k6eAd1
vedou	vést	k5eAaImIp3nP
politický	politický	k2eAgInSc4d1
boj	boj	k1gInSc4
–	–	k?
Moravská	moravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
knížete	kníže	k1gMnSc2
Mojmíra	Mojmír	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
sídlícího	sídlící	k2eAgInSc2d1
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
jihomoravských	jihomoravský	k2eAgFnPc6d1
Mikulčicích	Mikulčice	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
Nitranské	nitranský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
se	s	k7c7
střediskem	středisko	k1gNnSc7
v	v	k7c6
Nitře	Nitra	k1gFnSc6
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
dnešního	dnešní	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlil	sídlit	k5eAaImAgMnS
kníže	kníže	k1gMnSc1
Pribina	Pribina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
roku	rok	k1gInSc2
822	#num#	k4
však	však	k9
Moravané	Moravan	k1gMnPc1
vystupovali	vystupovat	k5eAaImAgMnP
jako	jako	k8xC,k8xS
kmen	kmen	k1gInSc4
(	(	kIx(
<g/>
ne	ne	k9
jako	jako	k9
knížectví	knížectví	k1gNnSc1
řízené	řízený	k2eAgNnSc1d1
jediným	jediný	k2eAgNnSc7d1
knížetem	kníže	k1gNnSc7wR
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gMnPc1
vyslanci	vyslanec	k1gMnPc1
dostavili	dostavit	k5eAaPmAgMnP
spolu	spolu	k6eAd1
s	s	k7c7
vyslanci	vyslanec	k1gMnPc7
dalších	další	k2eAgFnPc2d1
na	na	k7c6
Franské	franský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
závislých	závislý	k2eAgMnPc2d1
Slovanů	Slovan	k1gMnPc2
a	a	k8xC
Avarů	Avar	k1gMnPc2
<g/>
,	,	kIx,
sídlících	sídlící	k2eAgMnPc2d1
při	při	k7c6
hranici	hranice	k1gFnSc6
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
Ludvíku	Ludvík	k1gMnSc3
Pobožnému	pobožný	k2eAgMnSc3d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
svolal	svolat	k5eAaPmAgInS
na	na	k7c4
listopad	listopad	k1gInSc4
roku	rok	k1gInSc2
822	#num#	k4
do	do	k7c2
Frankfurtu	Frankfurt	k1gInSc2
všeobecný	všeobecný	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgNnSc6
se	se	k3xPyFc4
měly	mít	k5eAaImAgInP
probírat	probírat	k5eAaImF
záležitosti	záležitost	k1gFnPc4
východní	východní	k2eAgFnSc2d1
části	část	k1gFnSc2
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
i	i	k8xC
záležitosti	záležitost	k1gFnSc3
říši	říš	k1gFnSc3
poddaných	poddaná	k1gFnPc2
Slovanů	Slovan	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účast	účast	k1gFnSc1
Moravanů	Moravan	k1gMnPc2
na	na	k7c6
tomto	tento	k3xDgInSc6
(	(	kIx(
<g/>
pro	pro	k7c4
kmeny	kmen	k1gInPc4
poddané	poddaný	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
)	)	kIx)
povinném	povinný	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
sem	sem	k6eAd1
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
ostatní	ostatní	k2eAgMnPc1d1
dostavili	dostavit	k5eAaPmAgMnP
„	„	k?
<g/>
s	s	k7c7
dary	dar	k1gInPc7
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
v	v	k7c6
pramenech	pramen	k1gInPc6
dvojznačně	dvojznačně	k6eAd1
splývají	splývat	k5eAaImIp3nP
s	s	k7c7
daněmi	daň	k1gFnPc7
a	a	k8xC
tributy	tribut	k1gInPc7
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byli	být	k5eAaImAgMnP
Moravané	Moravan	k1gMnPc1
poddaní	poddaný	k2eAgMnPc1d1
a	a	k8xC
poplatní	poplatní	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
tehdy	tehdy	k6eAd1
ale	ale	k8xC
uvnitř	uvnitř	k7c2
moravského	moravský	k2eAgInSc2d1
kmene	kmen	k1gInSc2
patrně	patrně	k6eAd1
(	(	kIx(
<g/>
dle	dle	k7c2
názoru	názor	k1gInSc2
historika	historik	k1gMnSc2
Dušana	Dušan	k1gMnSc2
Třeštíka	Třeštík	k1gMnSc2
<g/>
)	)	kIx)
probíhaly	probíhat	k5eAaImAgFnP
výrazné	výrazný	k2eAgInPc4d1
mocenské	mocenský	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
ve	v	k7c4
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
9	#num#	k4
<g/>
.	.	kIx.
stol	stol	k1gInSc1
vyústily	vyústit	k5eAaPmAgFnP
dvěma	dva	k4xCgFnPc7
význačnými	význačný	k2eAgFnPc7d1
událostmi	událost	k1gFnPc7
<g/>
:	:	kIx,
oficiálním	oficiální	k2eAgInSc7d1
„	„	k?
<g/>
křtem	křest	k1gInSc7
Moravy	Morava	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
vyhnáním	vyhnání	k1gNnSc7
Pribiny	Pribina	k1gFnSc2
z	z	k7c2
Nitry	Nitra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Územní	územní	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
a	a	k8xC
členění	členění	k1gNnSc1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
moci	moc	k1gFnSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
833	#num#	k4
<g/>
–	–	k?
<g/>
907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInPc1d1
Franky	Franky	k1gInPc1
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
na	na	k7c4
Moravu	Morava	k1gFnSc4
pronikat	pronikat	k5eAaImF
již	již	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
Franské	franský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
bavorské	bavorský	k2eAgFnSc2d1
mise	mise	k1gFnSc2
z	z	k7c2
Pasova	Pasov	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
také	také	k9
prostřednictvím	prostřednictvím	k7c2
kněží	kněz	k1gMnPc2
z	z	k7c2
Itálie	Itálie	k1gFnSc2
a	a	k8xC
Dalmacie	Dalmacie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Slovanská	slovanský	k2eAgFnSc1d1
aristokracie	aristokracie	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Panonie	Panonie	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
přijímala	přijímat	k5eAaImAgFnS
křesťanství	křesťanství	k1gNnSc4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
po	po	k7c6
vzoru	vzor	k1gInSc6
franckých	francký	k2eAgMnPc2d1
velmožů	velmož	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc1
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
napodovala	napodovat	k5eAaBmAgFnS,k5eAaImAgFnS,k5eAaPmAgFnS
jako	jako	k9
určitý	určitý	k2eAgInSc4d1
"	"	kIx"
<g/>
nobilitující	nobilitující	k2eAgInSc4d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vznešený	vznešený	k2eAgInSc4d1
<g/>
)	)	kIx)
standard	standard	k1gInSc4
a	a	k8xC
především	především	k6eAd1
jako	jako	k9
model	model	k1gInSc1
vykonávání	vykonávání	k1gNnSc1
a	a	k8xC
organizace	organizace	k1gFnSc1
moci	moc	k1gFnSc2
–	–	k?
tyto	tento	k3xDgInPc1
procesy	proces	k1gInPc1
se	se	k3xPyFc4
odehrávaly	odehrávat	k5eAaImAgInP
i	i	k9
na	na	k7c6
dnešní	dnešní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
a	a	k8xC
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
831	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tzv.	tzv.	kA
křtu	křest	k1gInSc2
všech	všecek	k3xTgMnPc2
Moravanů	Moravan	k1gMnPc2
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgMnSc6
přijal	přijmout	k5eAaPmAgMnS
kníže	kníže	k1gMnSc1
Mojmír	Mojmír	k1gMnSc1
a	a	k8xC
jemu	on	k3xPp3gMnSc3
věrní	věrný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
křest	křest	k1gInSc4
z	z	k7c2
rukou	ruka	k1gFnPc2
pasovského	pasovský	k1gMnSc2
biskupa	biskup	k1gMnSc2
Reginhara	Reginhar	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
oficiální	oficiální	k2eAgInSc4d1
křest	křest	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
ho	on	k3xPp3gMnSc4
přijal	přijmout	k5eAaPmAgMnS
pouze	pouze	k6eAd1
kníže	kníže	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
okolí	okolí	k1gNnSc1
<g/>
,	,	kIx,
zavazoval	zavazovat	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
kmen	kmen	k1gInSc1
Moravanů	Moravan	k1gMnPc2
–	–	k?
i	i	k8xC
přes	přes	k7c4
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
moravská	moravský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nadále	nadále	k6eAd1
(	(	kIx(
<g/>
ještě	ještě	k9
v	v	k7c6
r.	r.	kA
869	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Metoděj	Metoděj	k1gMnSc1
ujal	ujmout	k5eAaPmAgMnS
správy	správa	k1gFnPc4
moravské	moravský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
)	)	kIx)
z	z	k7c2
části	část	k1gFnSc2
pohanská	pohanský	k2eAgFnSc1d1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Úvahy	úvaha	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
samotném	samotný	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
na	na	k7c6
předpolí	předpolí	k1gNnSc6
knížecího	knížecí	k2eAgInSc2d1
hradu	hrad	k1gInSc2
v	v	k7c6
Mikulčicích	Mikulčice	k1gFnPc6
<g/>
,	,	kIx,
stála	stát	k5eAaImAgFnS
současně	současně	k6eAd1
s	s	k7c7
křesťanskými	křesťanský	k2eAgInPc7d1
chrámy	chrám	k1gInPc7
v	v	k7c6
polovině	polovina	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
pohanská	pohanský	k2eAgFnSc1d1
svatyně	svatyně	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
neprokázaly	prokázat	k5eNaPmAgInP
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Důsledkem	důsledek	k1gInSc7
přijetí	přijetí	k1gNnSc2
křesťanství	křesťanství	k1gNnSc2
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Mojmíra	Mojmír	k1gMnSc4
i	i	k8xC
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
nástupce	nástupce	k1gMnSc1
posílení	posílení	k1gNnSc2
moci	moc	k1gFnSc2
panovníka	panovník	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
trvání	trvání	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
vystupuje	vystupovat	k5eAaImIp3nS
jako	jako	k9
autokratický	autokratický	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
obklopený	obklopený	k2eAgMnSc1d1
"	"	kIx"
<g/>
knížaty	kníže	k1gMnPc7wR
<g/>
"	"	kIx"
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
činí	činit	k5eAaImIp3nS
důležitá	důležitý	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
si	se	k3xPyFc3
kníže	kníže	k1gMnSc1
jakožto	jakožto	k8xS
křesťanský	křesťanský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
vylepšil	vylepšit	k5eAaPmAgMnS
i	i	k9
své	svůj	k3xOyFgNnSc4
postavení	postavení	k1gNnSc4
při	při	k7c6
jednáních	jednání	k1gNnPc6
s	s	k7c7
křesťanskou	křesťanský	k2eAgFnSc7d1
Franskou	franský	k2eAgFnSc7d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
833	#num#	k4
se	se	k3xPyFc4
knížeti	kníže	k1gMnSc3
Mojmírovi	Mojmír	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
Pribinu	Pribina	k1gFnSc4
z	z	k7c2
Nitry	Nitra	k1gFnSc2
vypudit	vypudit	k5eAaPmF
<g/>
,	,	kIx,
připojil	připojit	k5eAaPmAgInS
jeho	jeho	k3xOp3gNnSc4
knížectví	knížectví	k1gNnSc4
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
a	a	k8xC
tento	tento	k3xDgInSc1
krok	krok	k1gInSc1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
vznik	vznik	k1gInSc4
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mojmír	Mojmír	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
velkomoravským	velkomoravský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1
misie	misie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
860	#num#	k4
vyslal	vyslat	k5eAaPmAgMnS
Rostislav	Rostislav	k1gMnSc1
poselstvo	poselstvo	k1gNnSc4
k	k	k7c3
papeži	papež	k1gMnSc3
Mikuláši	Mikuláš	k1gMnSc3
I.	I.	kA
a	a	k8xC
požádal	požádat	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
o	o	k7c4
„	„	k?
<g/>
učitele	učitel	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
zajistit	zajistit	k5eAaPmF
prohloubení	prohloubení	k1gNnSc4
křesťanské	křesťanský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
zajistit	zajistit	k5eAaPmF
vzdělání	vzdělání	k1gNnSc4
dostatečného	dostatečný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
domácích	domácí	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využil	využít	k5eAaPmAgMnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
po	po	k7c6
záchvatu	záchvat	k1gInSc6
mrtvice	mrtvice	k1gFnSc2
tehdejšího	tehdejší	k2eAgInSc2d1
pasovského	pasovský	k2eAgInSc2d1
biskupa	biskup	k1gInSc2
Hartwiga	Hartwiga	k1gFnSc1
upadla	upadnout	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
pasovská	pasovský	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
včetně	včetně	k7c2
misijních	misijní	k2eAgNnPc2d1
území	území	k1gNnPc2
do	do	k7c2
chaosu	chaos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgFnPc4d1
vyslání	vyslání	k1gNnPc4
učitele	učitel	k1gMnSc2
se	se	k3xPyFc4
však	však	k9
mělo	mít	k5eAaImAgNnS
stát	stát	k5eAaImF,k5eAaPmF
jen	jen	k6eAd1
prvním	první	k4xOgInSc7
krokem	krok	k1gInSc7
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
samostatné	samostatný	k2eAgFnSc2d1
církevní	církevní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
arcibiskupem	arcibiskup	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Papež	Papež	k1gMnSc1
však	však	k9
potřeboval	potřebovat	k5eAaImAgMnS
spojenectví	spojenectví	k1gNnSc4
východofranského	východofranský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
Němce	Němec	k1gMnSc2
a	a	k8xC
nedovolil	dovolit	k5eNaPmAgMnS
si	se	k3xPyFc3
podniknout	podniknout	k5eAaPmF
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
poškodit	poškodit	k5eAaPmF
bavorský	bavorský	k2eAgInSc4d1
episkopát	episkopát	k1gInSc4
a	a	k8xC
popudit	popudit	k5eAaPmF
Ludvíka	Ludvík	k1gMnSc4
Němce	Němec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
odpověď	odpověď	k1gFnSc4
na	na	k7c4
Rostislavovu	Rostislavův	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
negativní	negativní	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
v	v	k7c6
Římě	Řím	k1gInSc6
vyslal	vyslat	k5eAaPmAgMnS
Rostislav	Rostislav	k1gMnSc1
obdobné	obdobný	k2eAgNnSc4d1
poselstvo	poselstvo	k1gNnSc4
k	k	k7c3
byzantskému	byzantský	k2eAgMnSc3d1
císaři	císař	k1gMnSc3
Michalovi	Michal	k1gMnSc3
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
jeho	jeho	k3xOp3gFnSc4
žádosti	žádost	k1gFnSc6
vyhověl	vyhovět	k5eAaPmAgInS
a	a	k8xC
na	na	k7c4
Moravu	Morava	k1gFnSc4
vyslal	vyslat	k5eAaPmAgMnS
misii	misie	k1gFnSc4
vedenou	vedený	k2eAgFnSc4d1
jedním	jeden	k4xCgNnSc7
z	z	k7c2
tehdy	tehdy	k6eAd1
nejvýznamnějších	významný	k2eAgMnPc2d3
byzantských	byzantský	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
Konstantinem	Konstantin	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
Metodějem	Metoděj	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Když	když	k8xS
pak	pak	k6eAd1
dorazil	dorazit	k5eAaPmAgMnS
na	na	k7c4
Moravu	Morava	k1gFnSc4
(	(	kIx(
<g/>
Konstantin	Konstantin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přijal	přijmout	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
Rastislav	Rastislav	k1gMnSc1
s	s	k7c7
velikou	veliký	k2eAgFnSc7d1
poctou	pocta	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
shromáždiv	shromáždit	k5eAaPmDgInS
učedníky	učedník	k1gMnPc7
<g/>
,	,	kIx,
svěřil	svěřit	k5eAaPmAgMnS
(	(	kIx(
<g/>
mu	on	k3xPp3gMnSc3
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
na	na	k7c6
učení	učení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
přeložen	přeložit	k5eAaPmNgInS
veškerý	veškerý	k3xTgInSc4
církevní	církevní	k2eAgInSc4d1
řád	řád	k1gInSc4
a	a	k8xC
naučil	naučit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
jitřním	jitřní	k2eAgNnSc7d1
i	i	k8xC
hodinkám	hodinka	k1gFnPc3
<g/>
,	,	kIx,
(	(	kIx(
<g/>
mši	mše	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nešporám	nešpora	k1gFnPc3
a	a	k8xC
kompletáři	kompletář	k1gMnPc1
i	i	k9
obřadům	obřad	k1gInPc3
mešním	mešní	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
otevřely	otevřít	k5eAaPmAgFnP
se	se	k3xPyFc4
podle	podle	k7c2
slov	slovo	k1gNnPc2
prorokových	prorokův	k2eAgNnPc2d1
uši	ucho	k1gNnPc4
hluchých	hluchý	k2eAgFnPc2d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
uslyšely	uslyšet	k5eAaPmAgFnP
slova	slovo	k1gNnSc2
knih	kniha	k1gFnPc2
a	a	k8xC
jazyk	jazyk	k1gInSc1
koktavých	koktavý	k2eAgInPc2d1
zjasněl	zjasnět	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
pak	pak	k9
se	se	k3xPyFc4
nad	nad	k7c7
tím	ten	k3xDgNnSc7
velice	velice	k6eAd1
zaradoval	zaradovat	k5eAaPmAgMnS
a	a	k8xC
ďábel	ďábel	k1gMnSc1
byl	být	k5eAaImAgMnS
zahanben	zahanbit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Žitije	Žitije	k1gMnSc4
Konstantina	Konstantin	k1gMnSc4
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
s	s	k7c7
Metodějem	Metoděj	k1gMnSc7
sestavili	sestavit	k5eAaPmAgMnP
také	také	k9
takzvaný	takzvaný	k2eAgInSc4d1
Zakon	Zakon	k1gInSc4
sudnyj	sudnyj	k1gFnSc4
ljudem	ljud	k1gInSc7
(	(	kIx(
<g/>
česky	česky	k6eAd1
Soudní	soudní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
pro	pro	k7c4
laiky	laik	k1gMnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
bodě	bod	k1gInSc6
vypořádává	vypořádávat	k5eAaImIp3nS
s	s	k7c7
lidmi	člověk	k1gMnPc7
co	co	k9
nepřijali	přijmout	k5eNaPmAgMnP
křesťanství	křesťanství	k1gNnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
stanovuje	stanovovat	k5eAaImIp3nS
přísné	přísný	k2eAgInPc4d1
tresty	trest	k1gInPc4
pro	pro	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
konají	konat	k5eAaImIp3nP
pohanské	pohanský	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
nebo	nebo	k8xC
přísahy	přísaha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestem	trest	k1gInSc7
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
Každá	každý	k3xTgFnSc1
vesnice	vesnice	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
oběti	oběť	k1gFnPc1
nebo	nebo	k8xC
přísahy	přísaha	k1gFnPc1
pohanské	pohanský	k2eAgFnPc1d1
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
je	být	k5eAaImIp3nS
předána	předat	k5eAaPmNgFnS
Božímu	boží	k2eAgNnSc3d1
chrámu	chrám	k1gInSc2
se	se	k3xPyFc4
vším	všecek	k3xTgInSc7
majetkem	majetek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
patří	patřit	k5eAaImIp3nS
pánům	pan	k1gMnPc3
v	v	k7c6
této	tento	k3xDgFnSc6
vesnici	vesnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
konají	konat	k5eAaImIp3nP
oběti	oběť	k1gFnPc4
a	a	k8xC
přísahy	přísaha	k1gFnPc4
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
jsou	být	k5eAaImIp3nP
prodáni	prodán	k2eAgMnPc1d1
s	s	k7c7
veškerým	veškerý	k3xTgNnSc7
svým	svůj	k3xOyFgInSc7
majetkem	majetek	k1gInSc7
a	a	k8xC
získaný	získaný	k2eAgInSc1d1
výnos	výnos	k1gInSc1
ať	ať	k9
se	se	k3xPyFc4
rozdá	rozdat	k5eAaPmIp3nS
chudým	chudý	k2eAgInSc7d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
z	z	k7c2
Byzance	Byzanc	k1gFnSc2
nešlo	jít	k5eNaImAgNnS
očekávat	očekávat	k5eAaImF
založení	založení	k1gNnSc1
(	(	kIx(
<g/>
arci	arci	k0
<g/>
)	)	kIx)
<g/>
biskupství	biskupství	k1gNnSc1
<g/>
,	,	kIx,
několikaletá	několikaletý	k2eAgFnSc1d1
intenzivní	intenzivní	k2eAgFnSc1d1
kulturně-misijní	kulturně-misijní	k2eAgFnSc1d1
i	i	k8xC
církevně-politická	církevně-politický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
Konstantina	Konstantin	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
položila	položit	k5eAaPmAgFnS
nezbytné	zbytný	k2eNgInPc4d1,k2eAgInPc4d1
základy	základ	k1gInPc4
pro	pro	k7c4
následné	následný	k2eAgNnSc4d1
budování	budování	k1gNnSc4
samostatné	samostatný	k2eAgNnSc4d1
církevní	církevní	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Konstantin	Konstantin	k1gMnSc1
také	také	k9
vytvořil	vytvořit	k5eAaPmAgMnS
nejstarší	starý	k2eAgNnSc4d3
staroslovanské	staroslovanský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
–	–	k?
hlaholici	hlaholice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nicméně	nicméně	k8xC
Slované	Slovan	k1gMnPc1
přešli	přejít	k5eAaPmAgMnP
k	k	k7c3
latince	latinka	k1gFnSc3
a	a	k8xC
cyrilici	cyrilice	k1gFnSc3
a	a	k8xC
hlaholice	hlaholice	k1gFnSc1
se	se	k3xPyFc4
již	již	k6eAd1
nadále	nadále	k6eAd1
neužívala	užívat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Rostislavově	Rostislavův	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
obsadil	obsadit	k5eAaPmAgMnS
Karloman	Karloman	k1gMnSc1
pravděpodobně	pravděpodobně	k6eAd1
bez	bez	k7c2
boje	boj	k1gInSc2
centra	centrum	k1gNnSc2
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správu	správa	k1gFnSc4
obsazeného	obsazený	k2eAgNnSc2d1
území	území	k1gNnSc2
svěřil	svěřit	k5eAaPmAgInS
hrabatům	hrabě	k1gNnPc3
Wilhelmovi	Wilhelm	k1gMnSc3
a	a	k8xC
Engilšalkovi	Engilšalka	k1gMnSc3
<g/>
,	,	kIx,
na	na	k7c4
nejdůležitější	důležitý	k2eAgNnSc4d3
hradiště	hradiště	k1gNnSc4
byly	být	k5eAaImAgFnP
vsazeny	vsazen	k2eAgFnPc1d1
franské	franský	k2eAgFnPc1d1
posádky	posádka	k1gFnPc1
a	a	k8xC
církevní	církevní	k2eAgFnPc1d1
správy	správa	k1gFnSc2
Moravy	Morava	k1gFnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
pasovský	pasovský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Hermanarik	Hermanarik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vláda	vláda	k1gFnSc1
Svatopluka	Svatopluk	k1gMnSc2
I.	I.	kA
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
I.	I.	kA
se	s	k7c7
třemi	tři	k4xCgInPc7
syny	syn	k1gMnPc7
–	–	k?
Mojmírem	Mojmír	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Svatoplukem	Svatopluk	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Predslavem	Predslav	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1
bula	bula	k1gFnSc1
Scire	Scir	k1gInSc5
vos	vosa	k1gFnPc2
volumus	volumus	k1gInSc1
adresovaná	adresovaný	k2eAgFnSc1d1
knížeti	kníže	k1gMnSc3
Svatoplukovi	Svatopluk	k1gMnSc3
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
870	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Svatopluk	Svatopluk	k1gMnSc1
I.	I.	kA
svého	svůj	k1gMnSc4
strýce	strýc	k1gMnSc4
Rostislava	Rostislav	k1gMnSc4
do	do	k7c2
zajetí	zajetí	k1gNnSc2
Frankům	Franky	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
obsadili	obsadit	k5eAaPmAgMnP
moravskou	moravský	k2eAgFnSc4d1
část	část	k1gFnSc4
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
jako	jako	k9
správce	správce	k1gMnSc4
učinili	učinit	k5eAaImAgMnP,k5eAaPmAgMnP
hrabata	hrabě	k1gNnPc4
Wilhelma	Wilhelmum	k1gNnSc2
a	a	k8xC
Engilšalka	Engilšalka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
Frankové	Franková	k1gFnPc4
zajali	zajmout	k5eAaPmAgMnP
i	i	k8xC
Svatopluka	Svatopluk	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
vládl	vládnout	k5eAaImAgInS
ještě	ještě	k6eAd1
v	v	k7c6
nitranském	nitranský	k2eAgInSc6d1
údělu	úděl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
871	#num#	k4
vypukne	vypuknout	k5eAaPmIp3nS
povstání	povstání	k1gNnSc2
a	a	k8xC
Frankové	Franková	k1gFnSc2
zde	zde	k6eAd1
rychle	rychle	k6eAd1
ztrácejí	ztrácet	k5eAaImIp3nP
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francký	francký	k2eAgMnSc1d1
král	král	k1gMnSc1
propustil	propustit	k5eAaPmAgMnS
Svatopluka	Svatopluk	k1gMnSc4
pod	pod	k7c7
podmínkou	podmínka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
pomůže	pomoct	k5eAaPmIp3nS
dobýt	dobýt	k5eAaPmF
říši	říše	k1gFnSc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Svatopluk	Svatopluk	k1gMnSc1
přestoupil	přestoupit	k5eAaPmAgMnS
na	na	k7c4
stranu	strana	k1gFnSc4
povstalců	povstalec	k1gMnPc2
<g/>
,	,	kIx,
převzal	převzít	k5eAaPmAgInS
jejich	jejich	k3xOp3gNnSc4
velení	velení	k1gNnSc4
a	a	k8xC
vyhnal	vyhnat	k5eAaPmAgMnS
Franky	Franky	k1gInPc4
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
úspěšně	úspěšně	k6eAd1
obhájil	obhájit	k5eAaPmAgInS
nezávislost	nezávislost	k1gFnSc4
své	svůj	k3xOyFgFnSc2
říše	říš	k1gFnSc2
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
880	#num#	k4
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
vydal	vydat	k5eAaPmAgInS
bulu	bula	k1gFnSc4
Industriae	Industriae	k1gNnSc2
tuae	tua	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zaručovala	zaručovat	k5eAaImAgFnS
církevní	církevní	k2eAgFnSc4d1
nezávislost	nezávislost	k1gFnSc4
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
arcidiecézí	arcidiecéze	k1gFnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
arcibiskupem	arcibiskup	k1gMnSc7
Metodějem	Metoděj	k1gMnSc7
(	(	kIx(
<g/>
archiepiscopo	archiepiscopa	k1gFnSc5
sanctę	sanctę	k?
ecclesię	ecclesię	k?
Marabensis	Marabensis	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k6eAd1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
německého	německý	k2eAgMnSc4d1
klerika	klerik	k1gMnSc4
Wichinga	Wiching	k1gMnSc4
jako	jako	k8xC,k8xS
biskupa	biskup	k1gMnSc4
Nitry	Nitra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staroslověnština	staroslověnština	k1gFnSc1
byla	být	k5eAaImAgFnS
uznána	uznat	k5eAaPmNgFnS
jako	jako	k9
čtvrtý	čtvrtý	k4xOgInSc1
liturgický	liturgický	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
latinou	latina	k1gFnSc7
<g/>
,	,	kIx,
řečtinou	řečtina	k1gFnSc7
a	a	k8xC
hebrejštinou	hebrejština	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Metodějově	Metodějův	k2eAgFnSc6d1
(	(	kIx(
<g/>
885	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
Svatopluk	Svatopluk	k1gMnSc1
východofranskými	východofranský	k2eAgMnPc7d1
biskupy	biskup	k1gMnPc7
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
žáky	žák	k1gMnPc7
bratrů	bratr	k1gMnPc2
Konstantina	Konstantin	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
ze	z	k7c2
země	zem	k1gFnSc2
vyhnal	vyhnat	k5eAaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
podporovat	podporovat	k5eAaImF
latinskou	latinský	k2eAgFnSc4d1
(	(	kIx(
<g/>
tj.	tj.	kA
západní	západní	k2eAgFnSc1d1
<g/>
)	)	kIx)
liturgii	liturgie	k1gFnSc4
namísto	namísto	k7c2
staroslověnské	staroslověnský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
staroslověnština	staroslověnština	k1gFnSc1
jako	jako	k8xS,k8xC
misijní	misijní	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
byla	být	k5eAaImAgFnS
papežem	papež	k1gMnSc7
nadále	nadále	k6eAd1
ceněna	ceněn	k2eAgFnSc1d1
a	a	k8xC
schvalována	schvalován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Usmíření	usmíření	k1gNnSc1
se	s	k7c7
sousední	sousední	k2eAgFnSc7d1
Franskou	franský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
umožnilo	umožnit	k5eAaPmAgNnS
Svatoplukovi	Svatopluk	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
konsolidoval	konsolidovat	k5eAaBmAgInS
své	svůj	k3xOyFgFnPc4
vojenské	vojenský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
územní	územní	k2eAgFnSc3d1
expanzi	expanze	k1gFnSc3
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgInSc2
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
říše	říš	k1gFnSc2
zaujímala	zaujímat	k5eAaImAgFnS
následující	následující	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
dnešní	dnešní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
a	a	k8xC
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
s	s	k7c7
mocenským	mocenský	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
při	při	k7c6
středním	střední	k2eAgInSc6d1
a	a	k8xC
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
dnešní	dnešní	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
(	(	kIx(
<g/>
se	s	k7c7
závislým	závislý	k2eAgNnSc7d1
knížectvím	knížectví	k1gNnSc7
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
části	část	k1gFnPc1
dnešních	dnešní	k2eAgInPc2d1
Horních	horní	k2eAgInPc2d1
a	a	k8xC
Dolních	dolní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
–	–	k?
severně	severně	k6eAd1
od	od	k7c2
Dunaje	Dunaj	k1gInSc2
</s>
<s>
oblasti	oblast	k1gFnPc1
při	při	k7c6
horní	horní	k2eAgFnSc6d1
Visle	Visla	k1gFnSc6
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgNnSc1d1
Malopolsko	Malopolsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Odře	odřít	k5eAaPmIp3nS
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgNnSc4d1
Horní	horní	k2eAgNnSc4d1
Slezsko	Slezsko	k1gNnSc4
<g/>
)	)	kIx)
–	–	k?
zde	zde	k6eAd1
ovšem	ovšem	k9
víme	vědět	k5eAaImIp1nP
jenom	jenom	k9
o	o	k7c6
poraženém	poražený	k2eAgNnSc6d1
knížeti	kníže	k1gNnSc6wR
na	na	k7c6
Visle	Visla	k1gFnSc6
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
Svatopluk	Svatopluk	k1gMnSc1
zde	zde	k6eAd1
–	–	k?
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
Čechách	Čechy	k1gFnPc6
–	–	k?
nikdy	nikdy	k6eAd1
přímo	přímo	k6eAd1
nevládl	vládnout	k5eNaImAgMnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
zde	zde	k6eAd1
pouze	pouze	k6eAd1
vybíral	vybírat	k5eAaImAgMnS
daň	daň	k1gFnSc4
</s>
<s>
Oblasti	oblast	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
zpochybněny	zpochybnit	k5eAaPmNgFnP
moderními	moderní	k2eAgMnPc7d1
historiky	historik	k1gMnPc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
dunajská	dunajský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgNnSc1d1
jihovýchodní	jihovýchodní	k2eAgNnSc1d1
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
Ukrajina	Ukrajina	k1gFnSc1
a	a	k8xC
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
tehdejší	tehdejší	k2eAgFnSc1d1
Panonie	Panonie	k1gFnSc1
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
soutok	soutok	k1gInSc4
Tisy	Tisa	k1gFnSc2
s	s	k7c7
Dunajem	Dunaj	k1gInSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
s	s	k7c7
Dunajem	Dunaj	k1gInSc7
</s>
<s>
dnešní	dnešní	k2eAgFnSc1d1
Lužice	Lužice	k1gFnSc1
neboli	neboli	k8xC
Lužické	lužický	k2eAgNnSc1d1
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
až	až	k9
po	po	k7c4
Sálu	Sála	k1gFnSc4
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
dnešní	dnešní	k2eAgFnSc2d1
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Rus	Rus	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
součást	součást	k1gFnSc1
Ukrajiny	Ukrajina	k1gFnSc2
</s>
<s>
Podle	podle	k7c2
Fuldských	Fuldský	k2eAgInPc2d1
letopisů	letopis	k1gInPc2
franský	franský	k2eAgMnSc1d1
král	král	k1gMnSc1
Arnulf	Arnulf	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
napadl	napadnout	k5eAaPmAgMnS
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
892	#num#	k4
<g/>
,	,	kIx,
Svatopluk	Svatopluk	k1gMnSc1
ale	ale	k8xC
zůstal	zůstat	k5eAaPmAgMnS
neporažen	porazit	k5eNaPmNgMnS
i	i	k9
přes	přes	k7c4
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Frankům	Frank	k1gMnPc3
poskytli	poskytnout	k5eAaPmAgMnP
Maďaři	Maďar	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
však	však	k9
neostýchali	ostýchat	k5eNaImAgMnP
pomáhat	pomáhat	k5eAaImF
ani	ani	k8xC
Moravanům	Moravan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatopluk	Svatopluk	k1gMnSc1
I.	I.	kA
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
létě	léto	k1gNnSc6
894	#num#	k4
a	a	k8xC
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Mojmír	Mojmír	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Svatopluka	Svatopluk	k1gMnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
syn	syn	k1gMnSc1
Mojmír	Mojmír	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Svatopluk	Svatopluk	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
získal	získat	k5eAaPmAgMnS
pravděpodobně	pravděpodobně	k6eAd1
úděl	úděl	k1gInSc4
v	v	k7c6
Nitře	Nitra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajný	údajný	k2eAgMnSc1d1
třetí	třetí	k4xOgMnSc1
Svatoplukův	Svatoplukův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
(	(	kIx(
<g/>
snad	snad	k9
Predeslav	Predeslav	k1gMnSc1
<g/>
)	)	kIx)
známý	známý	k1gMnSc1
z	z	k7c2
tradice	tradice	k1gFnSc2
je	on	k3xPp3gMnPc4
historiky	historik	k1gMnPc4
zpochybňován	zpochybňován	k2eAgInSc4d1
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
vůbec	vůbec	k9
neexistoval	existovat	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mojmír	Mojmír	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
expanzivní	expanzivní	k2eAgInPc4d1
plány	plán	k1gInPc4
svého	svůj	k1gMnSc2
otce	otec	k1gMnSc2
týkájící	týkájící	k1gFnSc2
se	se	k3xPyFc4
dobytí	dobytí	k1gNnSc1
celého	celý	k2eAgNnSc2d1
Panonského	panonský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgInS
s	s	k7c7
Bavory	Bavor	k1gMnPc7
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
otcových	otcův	k2eAgInPc2d1
územních	územní	k2eAgInPc2d1
zisků	zisk	k1gInPc2
v	v	k7c6
Panonii	Panonie	k1gFnSc6
tak	tak	k9
Mojmírovi	Mojmír	k1gMnSc3
zůstala	zůstat	k5eAaPmAgFnS
jen	jen	k9
Horní	horní	k2eAgFnSc1d1
Panonie	Panonie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
ale	ale	k8xC
nebyla	být	k5eNaImAgFnS
jediná	jediný	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
už	už	k6eAd1
v	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
895	#num#	k4
odpadly	odpadnout	k5eAaPmAgFnP
od	od	k7c2
Velké	velká	k1gFnSc2
Moravy	Morava	k1gFnPc1
Čechy	Čechy	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
měsíce	měsíc	k1gInSc2
července	červenec	k1gInSc2
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
městě	město	k1gNnSc6
Řezně	řezně	k6eAd1
obecný	obecný	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
;	;	kIx,
tam	tam	k6eAd1
přišla	přijít	k5eAaPmAgFnS
ke	k	k7c3
králi	král	k1gMnSc3
z	z	k7c2
území	území	k1gNnSc2
Slovanů	Slovan	k1gInPc2
všechna	všechen	k3xTgNnPc4
knížata	kníže	k1gMnPc4wR
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
vévoda	vévoda	k1gMnSc1
Svatopluk	Svatopluk	k1gMnSc1
předtím	předtím	k6eAd1
násilně	násilně	k6eAd1
odloučil	odloučit	k5eAaPmAgInS
a	a	k8xC
odtrhl	odtrhnout	k5eAaPmAgInS
ze	z	k7c2
společenství	společenství	k1gNnSc2
a	a	k8xC
z	z	k7c2
moci	moc	k1gFnSc2
národa	národ	k1gInSc2
bavorského	bavorský	k2eAgInSc2d1
-	-	kIx~
jejich	jejich	k3xOp3gInPc4
náčelníky	náčelník	k1gInPc4
byli	být	k5eAaImAgMnP
Spytihněv	Spytihněv	k1gMnPc1
a	a	k8xC
Vitizla	Vitizla	k1gMnPc1
–	–	k?
a	a	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
král	král	k1gMnSc1
důstojně	důstojně	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
<g/>
,	,	kIx,
podáním	podání	k1gNnSc7
ruky	ruka	k1gFnSc2
–	–	k?
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
zvykem	zvyk	k1gInSc7
–	–	k?
se	se	k3xPyFc4
smířeni	smířen	k2eAgMnPc1d1
podrobili	podrobit	k5eAaPmAgMnP
královské	královský	k2eAgFnSc3d1
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Fuldské	Fuldský	k2eAgInPc4d1
anály	anály	k1gInPc4
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
odpadnutí	odpadnutí	k1gNnSc6
Čech	Čechy	k1gFnPc2
ztratila	ztratit	k5eAaPmAgFnS
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
přímé	přímý	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
se	s	k7c7
srbskými	srbský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
v	v	k7c6
okolí	okolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Sály	Sála	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
skončila	skončit	k5eAaPmAgFnS
i	i	k9
jejich	jejich	k3xOp3gFnSc1
závislost	závislost	k1gFnSc1
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
898	#num#	k4
Svatopluk	Svatopluk	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
podporovaný	podporovaný	k2eAgInSc1d1
markrabětem	markrabě	k1gMnSc7
Aribem	Arib	k1gMnSc7
povstal	povstat	k5eAaPmAgMnS
proti	proti	k7c3
svému	svůj	k3xOyFgMnSc3
staršímu	starý	k2eAgMnSc3d2
bratru	bratr	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
bavorskou	bavorský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
byl	být	k5eAaImAgMnS
Svatopluk	Svatopluk	k1gMnSc1
poražen	porazit	k5eAaPmNgMnS
a	a	k8xC
sesazen	sesazen	k2eAgMnSc1d1
a	a	k8xC
musel	muset	k5eAaImAgInS
pak	pak	k6eAd1
hledat	hledat	k5eAaImF
útočiště	útočiště	k1gNnSc4
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obnovení	obnovení	k1gNnSc1
moravské	moravský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
Mojmírovi	Mojmír	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
konsolidovat	konsolidovat	k5eAaBmF
poměry	poměr	k1gInPc4
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
vyslal	vyslat	k5eAaPmAgMnS
na	na	k7c6
jaře	jaro	k1gNnSc6
899	#num#	k4
(	(	kIx(
<g/>
nebo	nebo	k8xC
snad	snad	k9
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
898	#num#	k4
<g/>
)	)	kIx)
poselstvo	poselstvo	k1gNnSc4
k	k	k7c3
papeži	papež	k1gMnSc3
do	do	k7c2
Říma	Řím	k1gInSc2
se	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c6
vysvěcení	vysvěcení	k1gNnSc6
biskupů	biskup	k1gMnPc2
a	a	k8xC
obnovu	obnova	k1gFnSc4
moravské	moravský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
Nedávno	nedávno	k6eAd1
zvolený	zvolený	k2eAgMnSc1d1
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
vyslat	vyslat	k5eAaPmF
na	na	k7c4
Moravu	Morava	k1gFnSc4
tři	tři	k4xCgInPc4
své	svůj	k3xOyFgMnPc4
legáty	legát	k1gMnPc4
(	(	kIx(
<g/>
arcibiskupa	arcibiskup	k1gMnSc4
Jana	Jan	k1gMnSc4
a	a	k8xC
biskupy	biskup	k1gMnPc4
Benedikta	Benedikt	k1gMnSc4
a	a	k8xC
Daniela	Daniel	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
měli	mít	k5eAaImAgMnP
znovuvybudovat	znovuvybudovat	k5eAaPmF
moravskou	moravský	k2eAgFnSc4d1
církevní	církevní	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
vysvětili	vysvětit	k5eAaPmAgMnP
arcibiskupa	arcibiskup	k1gMnSc4
a	a	k8xC
tři	tři	k4xCgInPc4
jeho	jeho	k3xOp3gInPc4
biskupy	biskup	k1gInPc1
sufragány	sufragán	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Moravská	moravský	k2eAgFnSc1d1
církevní	církevní	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
plně	plně	k6eAd1
dotvořena	dotvořit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
událostech	událost	k1gFnPc6
informuje	informovat	k5eAaBmIp3nS
pouze	pouze	k6eAd1
stížný	stížný	k2eAgInSc1d1
list	list	k1gInSc1
bavorských	bavorský	k2eAgMnPc2d1
biskupů	biskup	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
něj	on	k3xPp3gNnSc2
se	se	k3xPyFc4
ale	ale	k9
nedovídáme	dovídat	k5eNaImIp1nP
ani	ani	k8xC
jméno	jméno	k1gNnSc4
nového	nový	k2eAgMnSc2d1
moravského	moravský	k2eAgMnSc2d1
arcibiskupa	arcibiskup	k1gMnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
žádné	žádný	k3yNgFnPc4
informace	informace	k1gFnPc4
o	o	k7c6
sídlech	sídlo	k1gNnPc6
třech	tři	k4xCgNnPc6
sufraganních	sufraganní	k2eAgMnPc2d1
biskupů	biskup	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historikové	historik	k1gMnPc1
většinou	většina	k1gFnSc7
předpokládají	předpokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
sídlo	sídlo	k1gNnSc1
arcibiskupa	arcibiskup	k1gMnSc2
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
někde	někde	k6eAd1
v	v	k7c6
centru	centrum	k1gNnSc6
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
ve	v	k7c6
Valech	val	k1gInPc6
u	u	k7c2
Mikulčic	Mikulčice	k1gFnPc2
případně	případně	k6eAd1
v	v	k7c6
aglomeraci	aglomerace	k1gFnSc6
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
–	–	k?
<g/>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
ze	z	k7c2
sídel	sídlo	k1gNnPc2
sufragánních	sufragánní	k2eAgNnPc2d1
biskupství	biskupství	k1gNnSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
kladeno	kladen	k2eAgNnSc1d1
do	do	k7c2
Nitry	Nitra	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
navázalo	navázat	k5eAaPmAgNnS
na	na	k7c4
starší	starý	k2eAgNnSc4d2
Wichingovo	Wichingův	k2eAgNnSc4d1
biskupství	biskupství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgInPc1d1
dvě	dva	k4xCgNnPc1
biskupství	biskupství	k1gNnPc1
bývají	bývat	k5eAaImIp3nP
lokalizována	lokalizovat	k5eAaBmNgNnP
například	například	k6eAd1
do	do	k7c2
Olomouce	Olomouc	k1gFnSc2
a	a	k8xC
Krakova	Krakov	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
horní	horní	k2eAgFnSc2d1
Panonie	Panonie	k1gFnSc2
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Marky	marka	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
případně	případně	k6eAd1
do	do	k7c2
Panonie	Panonie	k1gFnSc2
a	a	k8xC
Zadunajska	Zadunajsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zánik	zánik	k1gInSc1
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1
velkomoravského	velkomoravský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
Kostolec	Kostolec	k1gMnSc1
</s>
<s>
Ještě	ještě	k9
v	v	k7c6
letech	let	k1gInPc6
903	#num#	k4
<g/>
–	–	k?
<g/>
904	#num#	k4
dokládá	dokládat	k5eAaImIp3nS
tzv.	tzv.	kA
Raffelstettenský	Raffelstettenský	k2eAgInSc1d1
celní	celní	k2eAgInSc1d1
tarif	tarif	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
pokládána	pokládán	k2eAgNnPc1d1
za	za	k7c2
spolehlivého	spolehlivý	k2eAgMnSc2d1
obchodního	obchodní	k2eAgMnSc2d1
partnera	partner	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
maďarská	maďarský	k2eAgFnSc1d1
hrozba	hrozba	k1gFnSc1
zjevně	zjevně	k6eAd1
nebyla	být	k5eNaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
něco	něco	k3yInSc4
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
zásadně	zásadně	k6eAd1
změnit	změnit	k5eAaPmF
poměry	poměr	k1gInPc4
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
změnila	změnit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
904	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
zavražděn	zavražděn	k2eAgMnSc1d1
maďarský	maďarský	k2eAgMnSc1d1
velkokníže	velkokníže	k1gMnSc1
Kurszán	Kurszán	k2eAgMnSc1d1
spolu	spolu	k6eAd1
s	s	k7c7
celou	celý	k2eAgFnSc7d1
svou	svůj	k3xOyFgFnSc7
družinou	družina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dušan	Dušan	k1gMnSc1
Třeštík	Třeštík	k1gMnSc1
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgInSc1
odvetný	odvetný	k2eAgInSc1d1
maďarský	maďarský	k2eAgInSc1d1
úder	úder	k1gInSc1
nesměřoval	směřovat	k5eNaImAgInS
proti	proti	k7c3
Bavorsku	Bavorsko	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
proti	proti	k7c3
Velké	velký	k2eAgFnSc3d1
Moravě	Morava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
odehrát	odehrát	k5eAaPmF
někde	někde	k6eAd1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
(	(	kIx(
<g/>
snad	snad	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
Nitry	Nitra	k1gFnSc2
<g/>
)	)	kIx)
někdy	někdy	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
905	#num#	k4
<g/>
–	–	k?
<g/>
906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
vypálena	vypálen	k2eAgNnPc1d1
a	a	k8xC
vyrabována	vyrabován	k2eAgNnPc1d1
hradiště	hradiště	k1gNnPc1
v	v	k7c6
Mikulčicích	Mikulčice	k1gFnPc6
<g/>
,	,	kIx,
Pohansku	Pohansko	k1gNnSc6
<g/>
,	,	kIx,
Strachotíně	Strachotín	k1gInSc6
<g/>
,	,	kIx,
Starých	Starých	k2eAgInPc2d1
zámků	zámek	k1gInPc2
u	u	k7c2
Lišně	lišeň	k1gFnSc2
<g/>
,	,	kIx,
Hradiště	Hradiště	k1gNnSc1
svatého	svatý	k1gMnSc2
Hypolita	Hypolita	k1gFnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k8xC
Sadech	sad	k1gInPc6
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
zahynul	zahynout	k5eAaPmAgMnS
i	i	k9
kníže	kníže	k1gMnSc1
Mojmír	Mojmír	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
totiž	totiž	k9
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
907	#num#	k4
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
u	u	k7c2
Bratislavy	Bratislava	k1gFnSc2
k	k	k7c3
rozhodující	rozhodující	k2eAgFnSc3d1
bitvě	bitva	k1gFnSc3
mezi	mezi	k7c7
Bavory	Bavor	k1gMnPc7
a	a	k8xC
Maďary	Maďar	k1gMnPc7
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
mezi	mezi	k7c7
jejími	její	k3xOp3gMnPc7
účastníky	účastník	k1gMnPc7
Moravané	Moravan	k1gMnPc1
uváděni	uvádět	k5eAaImNgMnP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bojovalo	bojovat	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
(	(	kIx(
<g/>
bývalé	bývalý	k2eAgFnSc2d1
<g/>
)	)	kIx)
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
]	]	kIx)
Samotný	samotný	k2eAgInSc1d1
zánik	zánik	k1gInSc1
velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
totiž	totiž	k9
pravděpodobně	pravděpodobně	k6eAd1
neznamenal	znamenat	k5eNaImAgInS
ani	ani	k8xC
zánik	zánik	k1gInSc1
samotné	samotný	k2eAgFnSc2d1
Mojmírovské	Mojmírovský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
a	a	k8xC
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
ve	v	k7c6
franských	franský	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
totiž	totiž	k9
vyskytují	vyskytovat	k5eAaImIp3nP
hrabě	hrabě	k1gMnSc1
Mojmír	Mojmír	k1gMnSc1
a	a	k8xC
urozený	urozený	k2eAgMnSc1d1
muž	muž	k1gMnSc1
Svatopluk	Svatopluk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
jejich	jejich	k3xOp3gInSc4
význam	význam	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
na	na	k7c4
ojedinělost	ojedinělost	k1gFnSc4
jejich	jejich	k3xOp3gNnPc2
jmen	jméno	k1gNnPc2
ve	v	k7c6
franském	franský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
se	se	k3xPyFc4
tak	tak	k9
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
potomky	potomek	k1gMnPc4
(	(	kIx(
<g/>
příbuzné	příbuzný	k1gMnPc4
<g/>
)	)	kIx)
Mojmírovců	Mojmírovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
na	na	k7c6
rychlém	rychlý	k2eAgInSc6d1
rozkladu	rozklad	k1gInSc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
měly	mít	k5eAaImAgInP
pravděpodobně	pravděpodobně	k6eAd1
i	i	k9
nepříznivé	příznivý	k2eNgInPc4d1
klimatické	klimatický	k2eAgInPc4d1
poměry	poměr	k1gInPc4
na	na	k7c6
počátku	počátek	k1gInSc6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jestliže	jestliže	k8xS
politicky	politicky	k6eAd1
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
k	k	k7c3
roku	rok	k1gInSc3
907	#num#	k4
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
především	především	k9
archeologické	archeologický	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
že	že	k8xS
některé	některý	k3yIgFnPc1
sídelní	sídelní	k2eAgFnPc1d1
<g/>
,	,	kIx,
hospodářské	hospodářský	k2eAgFnPc1d1
a	a	k8xC
společenské	společenský	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
nebyly	být	k5eNaImAgFnP
zcela	zcela	k6eAd1
rozbity	rozbit	k2eAgFnPc1d1
a	a	k8xC
přežívaly	přežívat	k5eAaImAgFnP
dál	daleko	k6eAd2
během	během	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archeologické	archeologický	k2eAgInPc1d1
výzkumy	výzkum	k1gInPc1
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
i	i	k8xC
Slovensku	Slovensko	k1gNnSc6
nenacházejí	nacházet	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
znaky	znak	k1gInPc4
násilného	násilný	k2eAgInSc2d1
konce	konec	k1gInSc2
těchto	tento	k3xDgNnPc2
sídel	sídlo	k1gNnPc2
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
dobytí	dobytí	k1gNnSc4
či	či	k8xC
vyplenění	vyplenění	k1gNnSc4
dokazují	dokazovat	k5eAaImIp3nP
výsledky	výsledek	k1gInPc1
výzkumů	výzkum	k1gInPc2
na	na	k7c6
lokalitách	lokalita	k1gFnPc6
v	v	k7c6
Mikulčicích	Mikulčice	k1gFnPc6
<g/>
,	,	kIx,
Znojmě	Znojmo	k1gNnSc6
nebo	nebo	k8xC
Ducovém	Ducový	k2eAgNnSc6d1
na	na	k7c6
Pováží	Pováží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
Řím	Řím	k1gInSc4
(	(	kIx(
<g/>
papeže	papež	k1gMnSc2
a	a	k8xC
císaře	císař	k1gMnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
oficiálním	oficiální	k2eAgInSc7d1
nástupnickým	nástupnický	k2eAgInSc7d1
státem	stát	k1gInSc7
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
Maďarské	maďarský	k2eAgNnSc1d1
velkoknížectví	velkoknížectví	k1gNnSc1
a	a	k8xC
Uherské	uherský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dle	dle	k7c2
některých	některý	k3yIgInPc2
názorů	názor	k1gInPc2
některých	některý	k3yIgMnPc2
historiků	historik	k1gMnPc2
stále	stále	k6eAd1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
byla	být	k5eAaImAgFnS
centrální	centrální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
říše	říš	k1gFnSc2
se	se	k3xPyFc4
totiž	totiž	k9
patrně	patrně	k6eAd1
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
uchovaly	uchovat	k5eAaPmAgInP
zbytky	zbytek	k1gInPc1
samostatné	samostatný	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
možná	možná	k9
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
závislé	závislý	k2eAgFnSc2d1
na	na	k7c6
Maďarech	Maďar	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naznačuje	naznačovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zpráva	zpráva	k1gFnSc1
z	z	k7c2
doby	doba	k1gFnSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
staří	starý	k2eAgMnPc1d1
Maďaři	Maďar	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
zajati	zajmout	k5eAaPmNgMnP
v	v	k7c6
maurském	maurský	k2eAgNnSc6d1
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
vypovídali	vypovídat	k5eAaImAgMnP,k5eAaPmAgMnP
o	o	k7c6
své	svůj	k3xOyFgFnSc6
zemi	zem	k1gFnSc6
na	na	k7c6
severu	sever	k1gInSc6
sousedící	sousedící	k2eAgFnSc1d1
s	s	k7c7
Moravou	Morava	k1gFnSc7
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
zpráva	zpráva	k1gFnSc1
o	o	k7c6
Moravanech	Moravan	k1gMnPc6
Al-Mas	Al-Masa	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
'	'	kIx"
<g/>
údího	údíze	k6eAd1
Rýžoviště	rýžoviště	k1gNnSc1
zlata	zlato	k1gNnSc2
a	a	k8xC
doly	dol	k1gInPc4
drahokamů	drahokam	k1gInPc2
z	z	k7c2
r.	r.	kA
947	#num#	k4
nebo	nebo	k8xC
Knihy	kniha	k1gFnPc1
ponaučení	ponaučení	k1gNnSc2
a	a	k8xC
přezkoumání	přezkoumání	k1gNnSc2
téhož	týž	k3xTgMnSc2
autora	autor	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Dokonce	dokonce	k9
ve	v	k7c6
spisu	spis	k1gInSc6
O	o	k7c6
ceremoniích	ceremonie	k1gFnPc6
byzantského	byzantský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
je	být	k5eAaImIp3nS
zmíněn	zmíněn	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
Moravy	Morava	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
podřízenosti	podřízenost	k1gFnSc6
Moravy	Morava	k1gFnSc2
Maďarům	Maďar	k1gMnPc3
či	či	k8xC
tributu	tribut	k1gInSc2
však	však	k9
žádné	žádný	k3yNgFnPc4
zprávy	zpráva	k1gFnPc4
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Zda	zda	k8xS
rod	rod	k1gInSc1
Mojmírovců	Mojmírovec	k1gMnPc2
v	v	k7c6
období	období	k1gNnSc6
zániku	zánik	k1gInSc2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
vymřel	vymřít	k5eAaPmAgInS
nebo	nebo	k8xC
pokračoval	pokračovat	k5eAaImAgInS
dál	daleko	k6eAd2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
v	v	k7c6
písemných	písemný	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
zaznamenáno	zaznamenán	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
maďarské	maďarský	k2eAgFnSc2d1
středověké	středověký	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
Maďaři	Maďar	k1gMnPc1
koupili	koupit	k5eAaPmAgMnP
Velkomoravskou	velkomoravský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
od	od	k7c2
Svatopluka	Svatopluk	k1gMnSc2
I.	I.	kA
za	za	k7c4
bílého	bílý	k2eAgMnSc4d1
hřebce	hřebec	k1gMnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
samozřejmě	samozřejmě	k6eAd1
pouhá	pouhý	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
legitimizovat	legitimizovat	k5eAaBmF
„	„	k?
<g/>
zaujetí	zaujetí	k1gNnSc4
země	zem	k1gFnSc2
<g/>
“	“	k?
Maďary	maďar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
moderních	moderní	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
byl	být	k5eAaImAgMnS
první	první	k4xOgMnSc1
historický	historický	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Bořivoj	Bořivoj	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
†	†	k?
889	#num#	k4
<g/>
)	)	kIx)
dosazen	dosadit	k5eAaPmNgInS
na	na	k7c4
stolec	stolec	k1gInSc4
knížetem	kníže	k1gMnSc7
Svatoplukem	Svatopluk	k1gMnSc7
a	a	k8xC
pokřtěn	pokřtít	k5eAaPmNgInS
s	s	k7c7
manželkou	manželka	k1gFnSc7
Ludmilou	Ludmila	k1gFnSc7
moravským	moravský	k2eAgMnSc7d1
a	a	k8xC
panonským	panonský	k2eAgMnSc7d1
arcibiskupem	arcibiskup	k1gMnSc7
Metodějem	Metoděj	k1gMnSc7
(	(	kIx(
<g/>
†	†	k?
885	#num#	k4
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
byla	být	k5eAaImAgFnS
uznána	uznán	k2eAgFnSc1d1
svrchovanost	svrchovanost	k1gFnSc1
Moravy	Morava	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
připomíná	připomínat	k5eAaImIp3nS
pozdější	pozdní	k2eAgNnPc4d2
údělná	údělný	k2eAgNnPc4d1
panství	panství	k1gNnPc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
těch	ten	k3xDgMnPc2
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nedosáhli	dosáhnout	k5eNaPmAgMnP
na	na	k7c4
pražský	pražský	k2eAgInSc4d1
knížecí	knížecí	k2eAgInSc4d1
či	či	k8xC
královský	královský	k2eAgInSc4d1
stolec	stolec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bylo	být	k5eAaImAgNnS
by	by	kYmCp3nS
tedy	tedy	k9
možno	možno	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Mojmírovci	Mojmírovec	k1gMnPc1
(	(	kIx(
<g/>
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
měli	mít	k5eAaImAgMnP
své	svůj	k3xOyFgNnSc4
pokračování	pokračování	k1gNnSc4
v	v	k7c6
Přemyslovcích	Přemyslovec	k1gMnPc6
a	a	k8xC
českém	český	k2eAgInSc6d1
státě	stát	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Přemyslovci	Přemyslovec	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
stát	stát	k1gInSc4
mají	mít	k5eAaImIp3nP
počátky	počátek	k1gInPc1
již	již	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
ovšem	ovšem	k9
pouze	pouze	k6eAd1
domněnka	domněnka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
zániku	zánik	k1gInSc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
je	být	k5eAaImIp3nS
též	též	k9
zmiňován	zmiňován	k2eAgMnSc1d1
moravský	moravský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Vracen	vracen	k2eAgMnSc1d1
na	na	k7c6
sněmu	sněm	k1gInSc6
v	v	k7c6
Mohuči	Mohuč	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
pravděpodobně	pravděpodobně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
církevní	církevní	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
přetrvala	přetrvat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
i	i	k8xC
po	po	k7c6
zániku	zánik	k1gInSc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
přetrvával	přetrvávat	k5eAaImAgInS
význam	význam	k1gInSc1
bývalých	bývalý	k2eAgFnPc2d1
velkomoravských	velkomoravský	k2eAgFnPc2d1
žup	župa	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Nitra	Nitra	k1gFnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
atd.	atd.	kA
Jejich	jejich	k3xOp3gMnPc1
političtí	politický	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
na	na	k7c4
formování	formování	k1gNnSc4
uherského	uherský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
slovenské	slovenský	k2eAgNnSc4d1
území	území	k1gNnSc4
zůstalo	zůstat	k5eAaPmAgNnS
prakticky	prakticky	k6eAd1
autonomní	autonomní	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
pod	pod	k7c7
suverenitou	suverenita	k1gFnSc7
Maďarů	Maďar	k1gMnPc2
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
tehdejší	tehdejší	k2eAgFnSc1d1
Nitranské	nitranský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
zahrnující	zahrnující	k2eAgNnSc1d1
území	území	k1gNnSc1
nynější	nynější	k2eAgFnSc2d1
SR	SR	kA
začlenilo	začlenit	k5eAaPmAgNnS
do	do	k7c2
Uherska	Uhersko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sídla	sídlo	k1gNnPc1
</s>
<s>
Křesťanský	křesťanský	k2eAgInSc1d1
křížek	křížek	k1gInSc1
nalezený	nalezený	k2eAgInSc1d1
v	v	k7c6
Mikulčicích	Mikulčice	k1gFnPc6
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
Velkomoravské	velkomoravský	k2eAgInPc4d1
šperky	šperk	k1gInPc4
</s>
<s>
Sídelní	sídelní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
zahrnovala	zahrnovat	k5eAaImAgFnS
opevněné	opevněný	k2eAgFnPc4d1
slovanské	slovanský	k2eAgFnPc4d1
osady	osada	k1gFnPc4
–	–	k?
hradiště	hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgMnPc4d3
<g/>
,	,	kIx,
doložené	doložený	k2eAgMnPc4d1
přímo	přímo	k6eAd1
v	v	k7c6
pramenech	pramen	k1gInPc6
z	z	k7c2
doby	doba	k1gFnSc2
existence	existence	k1gFnSc2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
na	na	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
:	:	kIx,
Nitra	Nitra	k1gFnSc1
<g/>
,	,	kIx,
Devín	Devín	k1gInSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Maďarsku	Maďarsko	k1gNnSc6
<g/>
:	:	kIx,
Blatnohrad	Blatnohrad	k1gInSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgInSc1d1
Zalavár	Zalavár	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Mezi	mezi	k7c4
později	pozdě	k6eAd2
zmiňované	zmiňovaný	k2eAgFnPc4d1
nebo	nebo	k8xC
archeologicky	archeologicky	k6eAd1
probádané	probádaný	k2eAgFnSc2d1
osady	osada	k1gFnSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
na	na	k7c6
dnešní	dnešní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
:	:	kIx,
Mikulčice	Mikulčice	k1gFnPc1
(	(	kIx(
<g/>
palác	palác	k1gInSc1
velmože	velmož	k1gMnSc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
kostelů	kostel	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aglomerace	aglomerace	k1gFnPc1
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
+	+	kIx~
<g/>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
obchodní	obchodní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
křižovatka	křižovatka	k1gFnSc1
důležitých	důležitý	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
,	,	kIx,
uváděno	uvádět	k5eAaImNgNnS
jako	jako	k8xS,k8xC
Veligrad	Veligrad	k1gInSc1
<g/>
)	)	kIx)
<g/>
+	+	kIx~
<g/>
Sady	sad	k1gInPc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
klášter	klášter	k1gInSc4
či	či	k8xC
snad	snad	k9
„	„	k?
<g/>
arcibiskupská	arcibiskupský	k2eAgFnSc1d1
<g/>
“	“	k?
bazilika	bazilika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pohansko	Pohansko	k1gNnSc4
u	u	k7c2
Břeclavi	Břeclav	k1gFnSc2
<g/>
,	,	kIx,
Staré	Staré	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
u	u	k7c2
Líšně	Líšeň	k1gFnSc2
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Hradiště	Hradiště	k1gNnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
<g/>
,	,	kIx,
Rajhrad	Rajhrad	k1gInSc4
<g/>
,	,	kIx,
Réna	Rén	k1gInSc2
u	u	k7c2
Ivančic	Ivančice	k1gFnPc2
<g/>
,	,	kIx,
Pohansko	Pohansko	k1gNnSc4
u	u	k7c2
Nejdku	Nejdek	k1gInSc2
<g/>
,	,	kIx,
Petrova	Petrův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
u	u	k7c2
Strachotína	Strachotín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
na	na	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
:	:	kIx,
Ducové	Ducové	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bojná	bojný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Pobedim	Pobedim	k1gInSc1
<g/>
,	,	kIx,
Jur	jura	k1gFnPc2
u	u	k7c2
Bratislavy	Bratislava	k1gFnSc2
</s>
<s>
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Maďarsku	Maďarsko	k1gNnSc6
<g/>
:	:	kIx,
Ostřihom	Ostřihom	k1gInSc1
<g/>
,	,	kIx,
Feldebrő	Feldebrő	k1gFnSc1
</s>
<s>
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Rakousku	Rakousko	k1gNnSc6
<g/>
:	:	kIx,
Kirchberg	Kirchberg	k1gMnSc1
nad	nad	k7c7
Stillfriedem	Stillfried	k1gMnSc7
<g/>
,	,	kIx,
Klementberg	Klementberg	k1gMnSc1
u	u	k7c2
Ober-Leis	Ober-Leis	k1gFnSc2
<g/>
,	,	kIx,
Gars-Thunau	Gars-Thunaus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mocenským	mocenský	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
byl	být	k5eAaImAgInS
Veligrad	Veligrad	k1gInSc1
<g/>
,	,	kIx,
o	o	k7c4
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
umístění	umístění	k1gNnSc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
těchto	tento	k3xDgFnPc2
lokalit	lokalita	k1gFnPc2
–	–	k?
nebývale	nebývale	k6eAd1
rozlehlé	rozlehlý	k2eAgFnSc3d1
aglomeraci	aglomerace	k1gFnSc3
Staré	Stará	k1gFnSc2
Město	město	k1gNnSc1
–	–	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
–	–	k?
se	se	k3xPyFc4
již	již	k6eAd1
téměř	téměř	k6eAd1
nepochybuje	pochybovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Slovo	slovo	k1gNnSc1
pohanství	pohanství	k1gNnSc2
je	být	k5eAaImIp3nS
s	s	k7c7
pejorativním	pejorativní	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
užívaný	užívaný	k2eAgInSc1d1
výraz	výraz	k1gInSc1
především	především	k9
v	v	k7c6
křesťanské	křesťanský	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
pro	pro	k7c4
v	v	k7c6
podstatě	podstata	k1gFnSc6
všechna	všechen	k3xTgFnSc1
mimo-křesťanská	mimo-křesťanský	k2eAgFnSc1d1
a	a	k8xC
před-křesťanská	před-křesťanský	k2eAgNnPc1d1
náboženství	náboženství	k1gNnPc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Podrobně	podrobně	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
důvody	důvod	k1gInPc4
neúspěchu	neúspěch	k1gInSc2
poselstva	poselstvo	k1gNnSc2
Vladimír	Vladimír	k1gMnSc1
Vavřínek	Vavřínek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Obdobné	obdobný	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
zaujal	zaujmout	k5eAaPmAgInS
naposledy	naposledy	k6eAd1
i	i	k9
Zdeněk	Zdeněk	k1gMnSc1
Měřínský	Měřínský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
Dušan	Dušan	k1gMnSc1
Třeštík	Třeštík	k1gMnSc1
uvažoval	uvažovat	k5eAaImAgMnS
na	na	k7c6
základě	základ	k1gInSc6
tvrzeni	tvrdit	k5eAaImNgMnP
papežské	papežský	k2eAgNnSc4d1
buly	buly	k1gNnSc7
Gloria	gloria	k1gNnPc2
in	in	k?
Excelsis	Excelsis	k1gFnSc2
Deo	Deo	k1gMnSc2
Hadriána	Hadrián	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
roku	rok	k1gInSc2
869	#num#	k4
i	i	k8xC
o	o	k7c6
variantě	varianta	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
Rostislav	Rostislav	k1gMnSc1
nepočkal	počkat	k5eNaPmAgMnS
dostatečně	dostatečně	k6eAd1
dlouho	dlouho	k6eAd1
na	na	k7c4
odpověď	odpověď	k1gFnSc4
a	a	k8xC
než	než	k8xS
stačil	stačit	k5eAaBmAgMnS
papež	papež	k1gMnSc1
odpovědět	odpovědět	k5eAaPmF
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgMnS
Rostislav	Rostislav	k1gMnSc1
o	o	k7c4
učitele	učitel	k1gMnPc4
v	v	k7c6
Byzantské	byzantský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Moravská	moravský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
byla	být	k5eAaImAgFnS
ochromena	ochromit	k5eAaPmNgFnS
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
893	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
definitivně	definitivně	k6eAd1
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
Moravy	Morava	k1gFnSc2
biskup	biskup	k1gMnSc1
Wiching	Wiching	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
pak	pak	k6eAd1
existovaly	existovat	k5eAaImAgFnP
silné	silný	k2eAgFnPc1d1
snahy	snaha	k1gFnPc1
o	o	k7c6
podřízení	podřízení	k1gNnSc6
moravské	moravský	k2eAgFnPc1d1
církevní	církevní	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
bavorské	bavorský	k2eAgFnPc1d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Ján	Ján	k1gMnSc1
Steinhübel	Steinhübel	k1gMnSc1
je	on	k3xPp3gNnSc4
lokalizuje	lokalizovat	k5eAaBmIp3nS
do	do	k7c2
Šoproně	Šoproň	k1gFnSc2
a	a	k8xC
Mautern	Mauterna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Richard	Richard	k1gMnSc1
Marsina	Marsin	k2eAgInSc2d1
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
přelomu	přelom	k1gInSc6
9	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zde	zde	k6eAd1
Mojmír	Mojmír	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
ovládal	ovládat	k5eAaImAgMnS
ještě	ještě	k6eAd1
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
území	území	k1gNnPc4
a	a	k8xC
uvedená	uvedený	k2eAgNnPc1d1
biskupství	biskupství	k1gNnPc1
mohla	moct	k5eAaImAgNnP
vzniknout	vzniknout	k5eAaPmF
právě	právě	k9
tady	tady	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc1
centry	centr	k1gInPc1
by	by	kYmCp3nP
pak	pak	k6eAd1
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
Veszprém	Veszprý	k2eAgNnSc6d1
<g/>
,	,	kIx,
Feldebrő	Feldebrő	k1gMnPc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
Ráb	Ráb	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Marsinovu	Marsinův	k2eAgFnSc4d1
lokalizaci	lokalizace	k1gFnSc4
označil	označit	k5eAaPmAgMnS
později	pozdě	k6eAd2
za	za	k7c4
pravděpodobnou	pravděpodobný	k2eAgFnSc4d1
i	i	k8xC
Ján	Ján	k1gMnSc1
Steinhübel	Steinhübel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdůraznil	zdůraznit	k5eAaPmAgMnS
ale	ale	k9
<g/>
,	,	kIx,
že	že	k8xS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
biskupství	biskupství	k1gNnPc2
mohlo	moct	k5eAaImAgNnS
vzniknout	vzniknout	k5eAaPmF
i	i	k9
v	v	k7c6
Potisí	Potisí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
Marsinově	Marsinův	k2eAgFnSc3d1
loikalizaci	loikalizace	k1gFnSc3
se	se	k3xPyFc4
přiklání	přiklánět	k5eAaImIp3nS
i	i	k9
Petr	Petr	k1gMnSc1
Elbel	Elbel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Dušan	Dušan	k1gMnSc1
Třeštík	Třeštík	k1gMnSc1
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
i	i	k9
Zdeněk	Zdeněk	k1gMnSc1
Meřínský	Meřínský	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
uvažují	uvažovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
bavorské	bavorský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
pokus	pokus	k1gInSc4
o	o	k7c6
restituci	restituce	k1gFnSc6
vlády	vláda	k1gFnSc2
Mojmírovců	Mojmírovec	k1gMnPc2
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
pak	pak	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
v	v	k7c6
bavorském	bavorský	k2eAgInSc6d1
exilu	exil	k1gInSc6
stále	stále	k6eAd1
ještě	ještě	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
pobýval	pobývat	k5eAaImAgMnS
Svatopluk	Svatopluk	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východofranská	východofranský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
totiž	totiž	k9
neměla	mít	k5eNaImAgFnS
zájem	zájem	k1gInSc4
na	na	k7c4
zničení	zničení	k1gNnSc4
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
usilovala	usilovat	k5eAaImAgFnS
jen	jen	k9
o	o	k7c4
její	její	k3xOp3gFnSc4
podřízenost	podřízenost	k1gFnSc4
a	a	k8xC
závislost	závislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
↑	↑	k?
Kalhous	Kalhous	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
,	,	kIx,
Náčelnictví	náčelnictví	k1gNnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
stát	stát	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
poznámek	poznámka	k1gFnPc2
k	k	k7c3
článku	článek	k1gInSc3
Jiřího	Jiří	k1gMnSc2
Macháčka	Macháček	k1gMnSc2
o	o	k7c6
charakteru	charakter	k1gInSc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archeologické	archeologický	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
<g/>
,2014	,2014	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
66	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
177	#num#	k4
<g/>
-	-	kIx~
<g/>
180	#num#	k4
<g/>
;	;	kIx,
Štefan	Štefan	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
,	,	kIx,
Mocní	mocný	k2eAgMnPc1d1
náčelníci	náčelník	k1gMnPc1
od	od	k7c2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Poznámky	poznámka	k1gFnPc4
ke	k	k7c3
struktuře	struktura	k1gFnSc3
raných	raný	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Archeologické	archeologický	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
66	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
141	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
;	;	kIx,
Macháček	Macháček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Velkomoravský	velkomoravský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
"	"	kIx"
<g/>
:	:	kIx,
kontroverze	kontroverze	k1gFnSc1
středoevropské	středoevropský	k2eAgFnSc2d1
medievistiky	medievistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archeologické	archeologický	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
64	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
775	#num#	k4
<g/>
-	-	kIx~
<g/>
787	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bartl	Bartl	k1gMnSc1
2002	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Róna-Tas	Róna-Tas	k1gInSc1
1999	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
61	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
druhé	druhý	k4xOgNnSc4
vydání	vydání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8561	#num#	k4
<g/>
-	-	kIx~
<g/>
706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
301	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
druhé	druhý	k4xOgNnSc4
vydání	vydání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8561	#num#	k4
<g/>
-	-	kIx~
<g/>
706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
303	#num#	k4
-	-	kIx~
304	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9788085617061	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
353	#num#	k4
<g/>
-	-	kIx~
<g/>
354	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
138	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
270	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Třeštík	Třeštík	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
:	:	kIx,
Moravané	Moravan	k1gMnPc1
<g/>
,	,	kIx,
Čechové	Čech	k1gMnPc1
a	a	k8xC
střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
791	#num#	k4
<g/>
-	-	kIx~
<g/>
871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
101	#num#	k4
<g/>
-	-	kIx~
<g/>
103	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Třeštík	Třeštík	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Centrum	centrum	k1gNnSc4
experimentální	experimentální	k2eAgFnSc2d1
archeologie	archeologie	k1gFnSc2
a	a	k8xC
živé	živý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
:	:	kIx,
Život	život	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
/	/	kIx~
Křesťanství	křesťanství	k1gNnSc1
<g/>
.	.	kIx.
cea	cea	k?
<g/>
.	.	kIx.
<g/>
livinghistory	livinghistor	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Třeštík	Třeštík	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
128	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Třeštík	Třeštík	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
269	#num#	k4
<g/>
-	-	kIx~
<g/>
270	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Třeštík	Třeštík	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
120	#num#	k4
<g/>
-	-	kIx~
<g/>
121	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vznik	vznik	k1gInSc1
Velké	velká	k1gFnSc2
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
vyd	vyd	k?
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S.	S.	kA
128	#num#	k4
<g/>
-	-	kIx~
<g/>
130	#num#	k4
<g/>
↑	↑	k?
Třeštík	Třeštík	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
301	#num#	k4
<g/>
.	.	kIx.
viz	vidět	k5eAaImRp2nS
např.	např.	kA
zákaz	zákaz	k1gInSc1
společného	společný	k2eAgNnSc2d1
stolování	stolování	k1gNnSc2
pohanů	pohan	k1gMnPc2
s	s	k7c7
křesťany	křesťan	k1gMnPc7
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
franská	franský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
používala	používat	k5eAaImAgFnS
při	při	k7c6
misiích	misie	k1gFnPc6
jako	jako	k8xS,k8xC
nátlaku	nátlak	k1gInSc6
na	na	k7c4
pohanské	pohanský	k2eAgMnPc4d1
šlechtice	šlechtic	k1gMnPc4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
ELBEL	ELBEL	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
neúspěchu	neúspěch	k1gInSc2
aneb	aneb	k?
úsilí	úsilí	k1gNnSc2
Přemyslovců	Přemyslovec	k1gMnPc2
o	o	k7c4
zřízení	zřízení	k1gNnSc4
arcibiskupství	arcibiskupství	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
REITINGER	REITINGER	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměna	proměna	k1gFnSc1
středovýchodní	středovýchodní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
raného	raný	k2eAgInSc2d1
a	a	k8xC
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Matice	matice	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Elbel	Elbel	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86488	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
242	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církevní	církevní	k2eAgFnSc1d1
misie	misie	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
.	.	kIx.
202	#num#	k4
s.	s.	k?
S.	S.	kA
63	#num#	k4
<g/>
-	-	kIx~
<g/>
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Církevní	církevní	k2eAgFnPc1d1
misie	misie	k1gFnPc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
od	od	k7c2
příchodu	příchod	k1gInSc2
Slovanů	Slovan	k1gInPc2
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
258	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třeštík	Třeštík	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
180	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DVORNÍK	dvorník	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantské	byzantský	k2eAgFnPc4d1
misie	misie	k1gFnPc4
u	u	k7c2
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
393	#num#	k4
s.	s.	k?
S.	S.	kA
117	#num#	k4
<g/>
-	-	kIx~
<g/>
120	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Byzantské	byzantský	k2eAgFnPc1d1
misie	misie	k1gFnPc1
u	u	k7c2
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
↑	↑	k?
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
od	od	k7c2
příchodu	příchod	k1gInSc2
Slovanů	Slovan	k1gInPc2
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
287	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zakon	Zakon	k1gInSc1
sudnyj	sudnyj	k1gFnSc1
ľjudem	ľjud	k1gInSc7
-	-	kIx~
dostupný	dostupný	k2eAgInSc1d1
v	v	k7c6
online	onlinout	k5eAaPmIp3nS
podobě	podoba	k1gFnSc3
<g/>
↑	↑	k?
Elbel	Elbel	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
244	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
od	od	k7c2
příchodu	příchod	k1gInSc2
Slovanů	Slovan	k1gInPc2
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
350	#num#	k4
<g/>
-	-	kIx~
<g/>
351	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Svatoplukova	Svatoplukův	k2eAgFnSc1d1
dvojnásobná	dvojnásobný	k2eAgFnSc1d1
zrada	zrada	k1gFnSc1
<g/>
,	,	kIx,
E-Středověk	E-Středověk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
od	od	k7c2
příchodu	příchod	k1gInSc2
Slovanů	Slovan	k1gInPc2
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
97	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitrianske	Nitrianske	k1gNnSc1
kniežatstvo	kniežatstvo	k1gNnSc1
:	:	kIx,
počiatky	počiatek	k1gInPc1
stredovekého	stredoveký	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
dopl	dopla	k1gFnPc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budmerice	Budmerika	k1gFnSc6
<g/>
:	:	kIx,
Rak	rak	k1gMnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
594	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8550164	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
226	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Dále	daleko	k6eAd2
jen	jen	k9
Steinhübel	Steinhübel	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Měřínský	Měřínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
910	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Steinhübel	Steinhübel	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
227	#num#	k4
<g/>
–	–	k?
<g/>
228	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BLÁHOVÁ	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
FROLÍK	FROLÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
PROFANTOVÁ	PROFANTOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
I.	I.	kA
Do	do	k7c2
roku	rok	k1gInSc2
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
800	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
253	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
I.	I.	kA
↑	↑	k?
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
:	:	kIx,
mezi	mezi	k7c7
Konstantinopolí	Konstantinopole	k1gFnSc7
a	a	k8xC
Římem	Řím	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
375	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
482	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
318	#num#	k4
<g/>
–	–	k?
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
<g/>
.	.	kIx.
↑	↑	k?
ELBEL	ELBEL	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
neúspěchu	neúspěch	k1gInSc2
aneb	aneb	k?
úsilí	úsilí	k1gNnSc2
Přemyslovců	Přemyslovec	k1gMnPc2
o	o	k7c4
zřízení	zřízení	k1gNnSc4
arcibiskupství	arcibiskupství	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
REITINGER	REITINGER	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměna	proměna	k1gFnSc1
středovýchodní	středovýchodní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
raného	raný	k2eAgInSc2d1
a	a	k8xC
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Matice	matice	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86488	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
255	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JAN	Jan	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
moravského	moravský	k2eAgNnSc2d1
křesťanství	křesťanství	k1gNnSc2
a	a	k8xC
církevní	církevní	k2eAgFnPc1d1
správa	správa	k1gFnSc1
do	do	k7c2
doby	doba	k1gFnSc2
husitské	husitský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
KORDIOVSKÝ	KORDIOVSKÝ	kA
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
církevní	církevní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
:	:	kIx,
XXVII	XXVII	kA
<g/>
.	.	kIx.
mikulovské	mikulovský	k2eAgNnSc1d1
sympozium	sympozium	k1gNnSc1
:	:	kIx,
9	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikulov	Mikulov	k1gInSc1
;	;	kIx,
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgInSc1d1
okresní	okresní	k2eAgInSc1d1
archiv	archiv	k1gInSc1
Břeclav	Břeclav	k1gFnSc1
;	;	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7275	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JAN	Jan	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stará	starý	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
mezi	mezi	k7c7
Východem	východ	k1gInSc7
a	a	k8xC
Západem	západ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
SOMMER	Sommer	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatý	svatý	k1gMnSc1
Prokop	Prokop	k1gMnSc1
<g/>
,	,	kIx,
Čechy	Čechy	k1gFnPc1
a	a	k8xC
střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
790	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
257	#num#	k4
<g/>
–	–	k?
<g/>
258	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štyri	Štyri	k1gNnSc1
veľkomoravské	veľkomoravský	k2eAgNnSc1d1
biskupstvá	biskupstvat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanské	slovanský	k2eAgFnPc4d1
štúdie	štúdie	k1gFnPc4
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
583	#num#	k4
<g/>
-	-	kIx~
<g/>
564	#num#	k4
<g/>
X.	X.	kA
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnPc2
grossmährischen	grossmährischna	k1gFnPc2
Bistümer	Bistümer	k1gInSc1
zur	zur	k?
Zeit	Zeit	k2eAgInSc1d1
Mojmírs	Mojmírs	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeitschrift	Zeitschrift	k1gMnSc1
für	für	k?
Geschichte	Geschicht	k1gInSc5
und	und	k?
Kultur	kultura	k1gFnPc2
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Länder	Länder	k1gInSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
37	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
523	#num#	k4
<g/>
-	-	kIx~
<g/>
8587	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kirchliche	Kirchlich	k1gInSc2
Organisation	Organisation	k1gInSc1
in	in	k?
Grossmähren	Grossmährna	k1gFnPc2
zur	zur	k?
Zeit	Zeit	k1gInSc1
Mojmirs	Mojmirs	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
URBANCZYK	URBANCZYK	kA
<g/>
,	,	kIx,
Przemyslaw	Przemyslaw	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Early	earl	k1gMnPc7
Christianity	Christianita	k1gFnSc2
in	in	k?
Central	Central	k1gMnSc1
and	and	k?
East	East	k1gMnSc1
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warszawa	Warszaw	k1gInSc2
<g/>
:	:	kIx,
Semper	Sempra	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
8585435	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
87	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
MARSINA	MARSINA	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cirkevná	Cirkevný	k2eAgFnSc1d1
organizácia	organizácia	k1gFnSc1
na	na	k7c6
Veľkej	Veľkej	k1gFnSc6
Morave	Morav	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
;	;	kIx,
KOUŘIL	Kouřil	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
mezi	mezi	k7c7
východem	východ	k1gInSc7
a	a	k8xC
západem	západ	k1gInSc7
=	=	kIx~
Großmähren	Großmährna	k1gFnPc2
zwischen	zwischen	k1gInSc1
West	West	k1gMnSc1
und	und	k?
Ost	Ost	k1gMnSc1
:	:	kIx,
sborník	sborník	k1gInSc1
příspěvků	příspěvek	k1gInPc2
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
:	:	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Staré	Stará	k1gFnPc1
Město	město	k1gNnSc1
28	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Archeologický	archeologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86023	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
294	#num#	k4
<g/>
–	–	k?
<g/>
295	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitrianske	Nitrianske	k1gNnSc1
kniežatstvo	kniežatstvo	k1gNnSc1
:	:	kIx,
počiatky	počiatek	k1gInPc1
stredovekého	stredoveký	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
:	:	kIx,
rozprávanie	rozprávanie	k1gFnSc1
o	o	k7c6
dejinách	dejina	k1gFnPc6
nášho	náš	k1gMnSc2
územia	územius	k1gMnSc2
a	a	k8xC
okolitých	okolitý	k2eAgFnPc2d1
krajín	krajína	k1gFnPc2
od	od	k7c2
sťahovania	sťahovanium	k1gNnSc2
národov	národov	k1gInSc1
do	do	k7c2
začiatku	začiatek	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
<g/>
storočia	storočium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Veda	vést	k5eAaImSgInS
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
576	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
224	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
410	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOUŘIL	Kouřil	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staří	starý	k2eAgMnPc1d1
Maďaři	Maďar	k1gMnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
archeologie	archeologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Klápště	Klápšť	k1gFnSc2
<g/>
;	;	kIx,
Eva	Eva	k1gFnSc1
Plešková	Plešková	k1gFnSc1
<g/>
;	;	kIx,
Josef	Josef	k1gMnSc1
Žemlička	Žemlička	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
ve	v	k7c6
věku	věk	k1gInSc6
nejistot	nejistota	k1gFnPc2
:	:	kIx,
sborník	sborník	k1gInSc1
k	k	k7c3
příležitosti	příležitost	k1gFnSc3
70	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
Dušana	Dušan	k1gMnSc2
Třeštíka	Třeštík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Staří	starý	k2eAgMnPc1d1
Maďaři	Maďar	k1gMnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
archeologie	archeologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
647	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
111	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typologie	typologie	k1gFnSc1
raně	raně	k6eAd1
feudálních	feudální	k2eAgInPc2d1
slovanských	slovanský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
československých	československý	k2eAgFnPc2d1
a	a	k8xC
světových	světový	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Třeštík	Třeštík	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
na	na	k7c6
úsvitě	úsvit	k1gInSc6
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
655	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7275	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
88	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
615	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Meřínský	Meřínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
život	život	k1gInSc1
mojmírovských	mojmírovský	k2eAgMnPc2d1
knížat	kníže	k1gMnPc2wR
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
MACHÁČEK	Macháček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
:	:	kIx,
aneb	aneb	k?
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
hrobu	hrob	k1gInSc2
153	#num#	k4
na	na	k7c6
Pohansku	Pohansko	k1gNnSc6
u	u	k7c2
Břeclavi	Břeclav	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Wihoda	Wihoda	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
548	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
154	#num#	k4
<g/>
-	-	kIx~
<g/>
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
JOTA	jota	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8561	#num#	k4
<g/>
-	-	kIx~
<g/>
706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
"	"	kIx"
<g/>
O	o	k7c6
dějinách	dějiny	k1gFnPc6
a	a	k8xC
společnosti	společnost	k1gFnSc2
státu	stát	k1gInSc2
Moravanů	Moravan	k1gMnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8561	#num#	k4
<g/>
-	-	kIx~
<g/>
706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
263	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
východní	východní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
přínos	přínos	k1gInSc1
archeologie	archeologie	k1gFnSc2
k	k	k7c3
poznání	poznání	k1gNnSc3
jejich	jejich	k3xOp3gFnPc2
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
;	;	kIx,
KOUŘIL	Kouřil	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
MITÁČEK	MITÁČEK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
;	;	kIx,
Archeologický	archeologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
Brno	Brno	k1gNnSc4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
242	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
266	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8561	#num#	k4
<g/>
-	-	kIx~
<g/>
706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
304	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kontler	Kontler	k1gInSc1
1999	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
42	#num#	k4
<g/>
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
Svatopluk	Svatopluk	k1gMnSc1
I.	I.	kA
Veliký	veliký	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
Moravanů	Moravan	k1gMnPc2
a	a	k8xC
Slovanů	Slovan	k1gMnPc2
<g/>
.	.	kIx.
první	první	k4xOgFnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
54	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
171	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
-	-	kIx~
Sady	sada	k1gFnPc1
<g/>
,	,	kIx,
křesťanské	křesťanský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
říše	říš	k1gFnSc2
Velkomoravské	velkomoravský	k2eAgNnSc1d1
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
124	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
O	o	k7c6
přenesení	přenesení	k1gNnSc6
království	království	k1gNnSc2
a	a	k8xC
koruny	koruna	k1gFnSc2
z	z	k7c2
Moravy	Morava	k1gFnSc2
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
Moravský	moravský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
sborník	sborník	k1gInSc1
-	-	kIx~
Ročenka	ročenka	k1gFnSc1
Moravského	moravský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravský	moravský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
kongres	kongres	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86488	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
225	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prameny	pramen	k1gInPc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
KLP	KLP	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87773	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.pohanstvi.net/inde.php?menu=slovanimorava	http://www.pohanstvi.net/inde.php?menu=slovanimorava	k1gFnSc1
<g/>
↑	↑	k?
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slované	Slovan	k1gMnPc1
–	–	k?
doteky	dotek	k1gInPc7
předků	předek	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
75	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
59	#num#	k4
<g/>
,	,	kIx,
70	#num#	k4
<g/>
,	,	kIx,
75	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BEZDĚČKA	BEZDĚČKA	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
ČOUPEK	ČOUPEK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
;	;	kIx,
POJSL	POJSL	kA
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
TARCALOVÁ	TARCALOVÁ	kA
<g/>
,	,	kIx,
Ludmila	Ludmila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
87	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíciletá	tisíciletý	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
státu	stát	k1gInSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
.	.	kIx.
116	#num#	k4
s.	s.	k?
</s>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
FROLÍK	FROLÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
PROFANTOVÁ	PROFANTOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
I.	I.	kA
Do	do	k7c2
roku	rok	k1gInSc2
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
800	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DEKAN	dekan	k1gInSc1
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
:	:	kIx,
doba	doba	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
258	#num#	k4
s.	s.	k?
</s>
<s>
DVORNÍK	dvorník	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantské	byzantský	k2eAgFnPc4d1
misie	misie	k1gFnPc4
u	u	k7c2
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
393	#num#	k4
s.	s.	k?
</s>
<s>
ELBEL	ELBEL	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
neúspěchu	neúspěch	k1gInSc2
aneb	aneb	k?
úsilí	úsilí	k1gNnSc2
Přemyslovců	Přemyslovec	k1gMnPc2
o	o	k7c4
zřízení	zřízení	k1gNnSc4
arcibiskupství	arcibiskupství	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
REITINGER	REITINGER	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměna	proměna	k1gFnSc1
středovýchodní	středovýchodní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
raného	raný	k2eAgInSc2d1
a	a	k8xC
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Matice	matice	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86488	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
238	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkomoravští	velkomoravský	k2eAgMnPc1d1
Mojmírovci	Mojmírovec	k1gMnPc1
-	-	kIx~
první	první	k4xOgMnPc1
vládci	vládce	k1gMnPc1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
MITÁČEK	MITÁČEK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
BALCÁREK	Balcárek	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládcové	vládce	k1gMnPc1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
statí	stať	k1gFnPc2
ze	z	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
přednášek	přednáška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
304	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
a	a	k8xC
středoevropští	středoevropský	k2eAgMnPc1d1
Slované	Slovan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
.	.	kIx.
489	#num#	k4
s.	s.	k?
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
:	:	kIx,
k	k	k7c3
problematice	problematika	k1gFnSc3
politického	politický	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc2d1
a	a	k8xC
vládní	vládní	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
a	a	k8xC
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
158	#num#	k4
s.	s.	k?
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
dotisk	dotisk	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
dopl	dopnout	k5eAaPmAgInS
<g/>
.	.	kIx.
a	a	k8xC
upr	upr	k?
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
398	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8561	#num#	k4
<g/>
-	-	kIx~
<g/>
706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgInPc1d1
letopisy	letopis	k1gInPc1
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Moravy	Morava	k1gFnSc2
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
186	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatopluk	Svatopluk	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
Moravanů	Moravan	k1gMnPc2
a	a	k8xC
Slovanů	Slovan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
129	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
utrpení	utrpení	k1gNnSc4
Rostislava	Rostislav	k1gMnSc2
<g/>
,	,	kIx,
krále	král	k1gMnSc2
Moravanů	Moravan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Moravský	moravský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
sborník	sborník	k1gInSc1
:	:	kIx,
ročenka	ročenka	k1gFnSc1
Moravského	moravský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravský	moravský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
kongres	kongres	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
238	#num#	k4
<g/>
-	-	kIx~
<g/>
1183	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
85	#num#	k4
<g/>
-	-	kIx~
<g/>
230	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
E.	E.	kA
<g/>
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Magnae	Magnae	k1gInSc1
Moraviae	Moraviae	k1gNnSc2
Fontes	Fontesa	k1gFnPc2
Historici	historik	k1gMnPc1
I	i	k8xC
<g/>
–	–	k?
<g/>
V	V	kA
<g/>
,	,	kIx,
operi	operi	k6eAd1
edendo	edendo	k6eAd1
praefuit	praefuit	k1gMnSc1
L.	L.	kA
E.	E.	kA
Havlík	Havlík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
1966	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HERWIG	HERWIG	kA
<g/>
,	,	kIx,
Wolfram	wolfram	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salzburg	Salzburg	k1gInSc1
<g/>
,	,	kIx,
Bayern	Bayern	k1gInSc1
<g/>
,	,	kIx,
Österreich	Österreich	k1gInSc1
:	:	kIx,
Die	Die	k1gFnSc1
Conversio	Conversio	k1gMnSc1
Bagoariorum	Bagoariorum	k1gInSc1
et	et	k?
Carantanorum	Carantanorum	k1gInSc1
und	und	k?
die	die	k?
Quellen	Quellen	k2eAgMnSc1d1
ihrer	ihrer	k1gMnSc1
Zeit	Zeit	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gInSc1
;	;	kIx,
München	München	k1gInSc1
<g/>
:	:	kIx,
R.	R.	kA
Oldenbourg	Oldenbourg	k1gMnSc1
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
464	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
48664833	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CHRZANOWSKI	CHRZANOWSKI	kA
<g/>
,	,	kIx,
Witold	Witold	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Świętopełk	Świętopełka	k1gFnPc2
I	i	k8xC
Wielki	Wielki	k1gNnPc2
:	:	kIx,
król	król	k1gInSc1
wielkomorawski	wielkomorawski	k1gNnSc1
(	(	kIx(
<g/>
ok	oka	k1gFnPc2
<g/>
.	.	kIx.
844	#num#	k4
<g/>
-	-	kIx~
<g/>
894	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krakow	Krakow	k1gFnSc1
<g/>
:	:	kIx,
Avalon	Avalon	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
229	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
60448	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LYSÝ	Lysý	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravania	Moravanium	k1gNnSc2
<g/>
,	,	kIx,
Mojmírovci	Mojmírovec	k1gMnPc1
a	a	k8xC
Franská	franský	k2eAgFnSc1d1
ríša	ríša	k1gFnSc1
:	:	kIx,
štúdie	štúdie	k1gFnSc1
k	k	k7c3
etnogenéze	etnogenéza	k1gFnSc3
<g/>
,	,	kIx,
politickým	politický	k2eAgFnPc3d1
inštitúciám	inštitúcia	k1gFnPc3
a	a	k8xC
ústavnému	ústavný	k2eAgNnSc3d1
zriadeniu	zriadenium	k1gNnSc3
na	na	k7c6
území	území	k1gNnSc6
Slovenska	Slovensko	k1gNnSc2
vo	vo	k?
včasnom	včasnom	k1gInSc4
stredoveku	stredovéct	k5eAaPmIp1nSwK
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Atticum	Atticum	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
373	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
971381	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
od	od	k7c2
příchodu	příchod	k1gInSc2
Slovanů	Slovan	k1gInPc2
po	po	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
967	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
105	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
na	na	k7c6
úsvitě	úsvit	k1gInSc6
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
652	#num#	k4
s.	s.	k?
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
I.	I.	kA
<g/>
/	/	kIx~
<g/>
I.	I.	kA
Od	od	k7c2
nejstarších	starý	k2eAgFnPc2d3
dob	doba	k1gFnPc2
do	do	k7c2
smrti	smrt	k1gFnSc2
knížete	kníže	k1gMnSc2
Oldřicha	Oldřich	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Laichter	Laichter	k1gMnSc1
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
782	#num#	k4
s.	s.	k?
</s>
<s>
PANIC	panic	k1gMnSc1
<g/>
,	,	kIx,
Idzi	Idz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatnie	Ostatnie	k1gFnSc1
lata	lata	k1gFnSc1
Wielkich	Wielkich	k1gInSc4
Moraw	Moraw	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katowice	Katowice	k1gFnSc1
<g/>
:	:	kIx,
Wydawnictwo	Wydawnictwo	k1gNnSc1
Uniwersytetu	Uniwersytet	k1gInSc2
Śląskiego	Śląskiego	k6eAd1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
239	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
83	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
960	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnPc2
grossmährischen	grossmährischna	k1gFnPc2
Bistümer	Bistümer	k1gInSc1
zur	zur	k?
Zeit	Zeit	k2eAgInSc1d1
Mojmírs	Mojmírs	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeitschrift	Zeitschrift	k1gMnSc1
für	für	k?
Geschichte	Geschicht	k1gInSc5
und	und	k?
Kultur	kultura	k1gFnPc2
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Länder	Länder	k1gInSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
37	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
523	#num#	k4
<g/>
-	-	kIx~
<g/>
8587	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kirchliche	Kirchlich	k1gInSc2
Organisation	Organisation	k1gInSc1
in	in	k?
Grossmähren	Grossmährna	k1gFnPc2
zur	zur	k?
Zeit	Zeit	k1gInSc1
Mojmirs	Mojmirs	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
URBANCZYK	URBANCZYK	kA
<g/>
,	,	kIx,
Przemyslaw	Przemyslaw	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Early	earl	k1gMnPc7
Christianity	Christianita	k1gFnSc2
in	in	k?
Central	Central	k1gMnSc1
and	and	k?
East	East	k1gMnSc1
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warszawa	Warszaw	k1gInSc2
<g/>
:	:	kIx,
Semper	Sempra	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
8585435	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
87	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
STEINHÜBEL	STEINHÜBEL	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitrianske	Nitrianske	k1gNnSc1
kniežatstvo	kniežatstvo	k1gNnSc1
:	:	kIx,
počiatky	počiatek	k1gInPc1
stredovekého	stredoveký	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
:	:	kIx,
rozprávanie	rozprávanie	k1gFnSc1
o	o	k7c6
dejinách	dejina	k1gFnPc6
nášho	náš	k1gMnSc2
územia	územius	k1gMnSc2
a	a	k8xC
okolitých	okolitý	k2eAgFnPc2d1
krajín	krajína	k1gFnPc2
od	od	k7c2
sťahovania	sťahovanium	k1gNnSc2
národov	národov	k1gInSc1
do	do	k7c2
začiatku	začiatek	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
<g/>
storočia	storočium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Veda	vést	k5eAaImSgInS
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
576	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
224	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
812	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typologie	typologie	k1gFnSc1
raně	raně	k6eAd1
feudálních	feudální	k2eAgInPc2d1
slovanských	slovanský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
československých	československý	k2eAgFnPc2d1
a	a	k8xC
světových	světový	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
27	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdy	kdy	k6eAd1
zanikla	zaniknout	k5eAaPmAgFnS
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
mediavelia	mediavelium	k1gNnSc2
Pragensia	pragensia	k1gNnPc1
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
862	#num#	k4
<g/>
-	-	kIx~
<g/>
8017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
stavu	stav	k1gInSc3
a	a	k8xC
potřebám	potřeba	k1gFnPc3
bádání	bádání	k1gNnSc2
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
časopis	časopis	k1gInSc1
historický	historický	k2eAgInSc1d1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
97	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
689	#num#	k4
<g/>
-	-	kIx~
<g/>
727	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
862	#num#	k4
<g/>
-	-	kIx~
<g/>
6111	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
:	:	kIx,
Moravané	Moravan	k1gMnPc1
<g/>
,	,	kIx,
Čechové	Čech	k1gMnPc1
a	a	k8xC
střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
791	#num#	k4
<g/>
-	-	kIx~
<g/>
871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
384	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
482	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VAŠICA	VAŠICA	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgFnPc4d1
památky	památka	k1gFnPc4
epochy	epocha	k1gFnSc2
velkomoravské	velkomoravský	k2eAgFnSc2d1
863	#num#	k4
<g/>
-	-	kIx~
<g/>
885	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Řím	Řím	k1gInSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
;	;	kIx,
Křesťanská	křesťanský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
340	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
169	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církevní	církevní	k2eAgFnSc1d1
misie	misie	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
.	.	kIx.
202	#num#	k4
s.	s.	k?
</s>
<s>
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
:	:	kIx,
mezi	mezi	k7c7
Konstantinopolí	Konstantinopole	k1gFnSc7
a	a	k8xC
Římem	Řím	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
375	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
482	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mojmírovci	Mojmírovec	k1gMnPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Česka	Česko	k1gNnSc2
</s>
<s>
Archeoskanzen	Archeoskanzen	k2eAgMnSc1d1
Modrá	modrat	k5eAaImIp3nS
</s>
<s>
Památník	památník	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
sv.	sv.	kA
Hypolita	Hypolit	k2eAgMnSc4d1
</s>
<s>
Pohřebiště	pohřebiště	k1gNnSc1
ve	v	k7c6
Znojmě-Hradišti	Znojmě-Hradiště	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Stránky	stránka	k1gFnPc4
věnované	věnovaný	k2eAgFnSc3d1
Velkomoravské	velkomoravský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
a	a	k8xC
životu	život	k1gInSc3
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
Archivováno	archivovat	k5eAaBmNgNnS
6	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Moravia	Moravia	k1gFnSc1
magna	magna	k6eAd1
–	–	k?
stránky	stránka	k1gFnPc4
věnované	věnovaný	k2eAgFnSc3d1
historii	historie	k1gFnSc3
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
sv.	sv.	kA
Hypolita	Hypolita	k1gFnSc1
(	(	kIx(
<g/>
Znojmo	Znojmo	k1gNnSc1
–	–	k?
Hradiště	Hradiště	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
stránky	stránka	k1gFnSc2
o	o	k7c6
významném	významný	k2eAgNnSc6d1
velkomoravském	velkomoravský	k2eAgNnSc6d1
hradišti	hradiště	k1gNnSc6
na	na	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
</s>
<s>
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cs	cs	k?
<g/>
:	:	kIx,
Kolaps	kolaps	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
–	–	k?
záznam	záznam	k1gInSc1
televizního	televizní	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
</s>
<s>
Dejiny	Dejin	k2eAgFnPc1d1
Veľkej	Veľkej	k1gFnPc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
počiatky	počiatka	k1gFnSc2
Uhorska	Uhorsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turč	Turč	k1gInSc1
<g/>
.	.	kIx.
sv.	sv.	kA
Martin	Martin	k1gMnSc1
:	:	kIx,
Matica	Matica	k1gFnSc1
slovenská	slovenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
16	#num#	k4
s.	s.	k?
–	–	k?
dostupné	dostupný	k2eAgFnSc3d1
v	v	k7c6
Digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
UKB	UKB	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131256	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
316639017	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Morava	Morava	k1gFnSc1
</s>
