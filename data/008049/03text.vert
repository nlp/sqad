<s>
Kondom	kondom	k1gInSc1	kondom
nebo	nebo	k8xC	nebo
též	též	k9	též
prezervativ	prezervativ	k1gInSc1	prezervativ
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
šprcka	šprcka	k1gFnSc1	šprcka
i	i	k8xC	i
šprcguma	šprcguma	k1gFnSc1	šprcguma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ochranný	ochranný	k2eAgInSc4d1	ochranný
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
lidský	lidský	k2eAgInSc1d1	lidský
penis	penis	k1gInSc1	penis
během	během	k7c2	během
sexuálního	sexuální	k2eAgInSc2d1	sexuální
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
zabránit	zabránit	k5eAaPmF	zabránit
početí	početí	k1gNnSc2	početí
a	a	k8xC	a
přenosu	přenos	k1gInSc2	přenos
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Kondomy	kondom	k1gInPc1	kondom
se	se	k3xPyFc4	se
též	též	k9	též
nazývají	nazývat	k5eAaImIp3nP	nazývat
profylaktika	profylaktikum	k1gNnPc4	profylaktikum
<g/>
,	,	kIx,	,
ochranné	ochranný	k2eAgInPc4d1	ochranný
prostředky	prostředek	k1gInPc4	prostředek
nebo	nebo	k8xC	nebo
hovorově	hovorově	k6eAd1	hovorově
i	i	k9	i
guma	guma	k1gFnSc1	guma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
latexu	latex	k1gInSc2	latex
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
kondom	kondom	k1gInSc1	kondom
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
condom	condom	k1gInSc1	condom
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
uváděn	uvádět	k5eAaImNgMnS	uvádět
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
jménem	jméno	k1gNnSc7	jméno
Condom	Condom	k1gInSc1	Condom
nebo	nebo	k8xC	nebo
Quondam	Quondam	k1gInSc1	Quondam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1660	[number]	k4	1660
<g/>
–	–	k?	–
<g/>
1685	[number]	k4	1685
osobním	osobnit	k5eAaImIp1nS	osobnit
lékařem	lékař	k1gMnSc7	lékař
anglického	anglický	k2eAgNnSc2d1	anglické
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Condom	Condom	k1gInSc1	Condom
doporučoval	doporučovat	k5eAaImAgInS	doporučovat
používat	používat	k5eAaImF	používat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
střevo	střevo	k1gNnSc4	střevo
skopce	skopec	k1gMnSc2	skopec
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
měl	mít	k5eAaImAgMnS	mít
aspoň	aspoň	k9	aspoň
14	[number]	k4	14
nelegitimních	legitimní	k2eNgFnPc2d1	nelegitimní
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejspíš	nejspíš	k9	nejspíš
tyto	tento	k3xDgInPc4	tento
kondomy	kondom	k1gInPc4	kondom
nebyly	být	k5eNaImAgInP	být
příliš	příliš	k6eAd1	příliš
efektivní	efektivní	k2eAgInSc4d1	efektivní
nebo	nebo	k8xC	nebo
je	on	k3xPp3gInPc4	on
král	král	k1gMnSc1	král
nepoužíval	používat	k5eNaImAgMnS	používat
systematicky	systematicky	k6eAd1	systematicky
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
snahy	snaha	k1gFnPc1	snaha
vyrobit	vyrobit	k5eAaPmF	vyrobit
kondom	kondom	k1gInSc4	kondom
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
použití	použití	k1gNnSc4	použití
tkaných	tkaný	k2eAgFnPc2d1	tkaná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrobky	výrobek	k1gInPc1	výrobek
nebyly	být	k5eNaImAgFnP	být
příliš	příliš	k6eAd1	příliš
efektivní	efektivní	k2eAgFnPc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
efektivní	efektivní	k2eAgInPc1d1	efektivní
kondomy	kondom	k1gInPc1	kondom
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
ovčího	ovčí	k2eAgNnSc2d1	ovčí
střeva	střevo	k1gNnSc2	střevo
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
blány	blána	k1gFnPc1	blána
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
dostupné	dostupný	k2eAgFnPc1d1	dostupná
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
lepší	dobrý	k2eAgFnSc4d2	lepší
schopnost	schopnost	k1gFnSc4	schopnost
přenášet	přenášet	k5eAaImF	přenášet
tělesné	tělesný	k2eAgNnSc4d1	tělesné
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
hmatové	hmatový	k2eAgInPc4d1	hmatový
vjemy	vjem	k1gInPc4	vjem
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
syntetickými	syntetický	k2eAgInPc7d1	syntetický
kondomy	kondom	k1gInPc7	kondom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
natolik	natolik	k6eAd1	natolik
efektivní	efektivní	k2eAgFnPc1d1	efektivní
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
kondomů	kondom	k1gInPc2	kondom
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
doklad	doklad	k1gInSc1	doklad
o	o	k7c6	o
použití	použití	k1gNnSc6	použití
kondomu	kondom	k1gInSc2	kondom
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
rytině	rytina	k1gFnSc6	rytina
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Combarelles	Combarellesa	k1gFnPc2	Combarellesa
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
se	se	k3xPyFc4	se
při	při	k7c6	při
syfilických	syfilický	k2eAgFnPc6d1	syfilický
epidemiích	epidemie	k1gFnPc6	epidemie
používaly	používat	k5eAaImAgFnP	používat
plátěné	plátěný	k2eAgInPc4d1	plátěný
kondomy	kondom	k1gInPc4	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
doby	doba	k1gFnSc2	doba
spadá	spadat	k5eAaImIp3nS	spadat
i	i	k9	i
první	první	k4xOgNnSc1	první
použití	použití	k1gNnSc1	použití
spermicidů	spermicid	k1gInPc2	spermicid
<g/>
.	.	kIx.	.
</s>
<s>
Sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
pryžových	pryžový	k2eAgInPc2d1	pryžový
kondomů	kondom	k1gInPc2	kondom
začala	začít	k5eAaPmAgFnS	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
vulkanizace	vulkanizace	k1gFnSc2	vulkanizace
pryže	pryž	k1gFnSc2	pryž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
objevil	objevit	k5eAaPmAgMnS	objevit
Charles	Charles	k1gMnSc1	Charles
Goodyear	Goodyear	k1gMnSc1	Goodyear
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
kondomy	kondom	k1gInPc1	kondom
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
jako	jako	k8xC	jako
omyvatelné	omyvatelný	k2eAgInPc1d1	omyvatelný
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
použitelné	použitelný	k2eAgNnSc4d1	použitelné
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
latexových	latexový	k2eAgFnPc2d1	latexová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
lubrikované	lubrikovaný	k2eAgInPc1d1	lubrikovaný
kondomy	kondom	k1gInPc1	kondom
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
prodávány	prodávat	k5eAaImNgInP	prodávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kondomy	kondom	k1gInPc1	kondom
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
potisku	potisk	k1gInSc2	potisk
i	i	k8xC	i
příchutí	příchuť	k1gFnPc2	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
polovinou	polovina	k1gFnSc7	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
prodej	prodej	k1gFnSc4	prodej
kondomů	kondom	k1gInPc2	kondom
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
následně	následně	k6eAd1	následně
povolilo	povolit	k5eAaPmAgNnS	povolit
jejich	jejich	k3xOp3gInSc4	jejich
prodej	prodej	k1gInSc4	prodej
"	"	kIx"	"
<g/>
jen	jen	k9	jen
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
infekce	infekce	k1gFnSc2	infekce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
dostupnost	dostupnost	k1gFnSc1	dostupnost
kondomů	kondom	k1gInPc2	kondom
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
potenciálním	potenciální	k2eAgMnPc3d1	potenciální
zákazníkům	zákazník	k1gMnPc3	zákazník
oznámena	oznámit	k5eAaPmNgFnS	oznámit
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
inzercí	inzerce	k1gFnSc7	inzerce
"	"	kIx"	"
<g/>
gumových	gumový	k2eAgInPc2d1	gumový
tamponů	tampon	k1gInPc2	tampon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Latexové	latexový	k2eAgInPc1d1	latexový
kondomy	kondom	k1gInPc1	kondom
jsou	být	k5eAaImIp3nP	být
baleny	balit	k5eAaImNgInP	balit
v	v	k7c6	v
rolovací	rolovací	k2eAgFnSc6d1	rolovací
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
nasunutí	nasunutí	k1gNnSc3	nasunutí
na	na	k7c4	na
konec	konec	k1gInSc4	konec
penisu	penis	k1gInSc2	penis
a	a	k8xC	a
pak	pak	k6eAd1	pak
natažení	natažení	k1gNnSc2	natažení
přes	přes	k7c4	přes
penis	penis	k1gInSc4	penis
v	v	k7c6	v
erekci	erekce	k1gFnSc6	erekce
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
správnou	správný	k2eAgFnSc4d1	správná
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nesprávnou	správný	k2eNgFnSc4d1	nesprávná
<g/>
"	"	kIx"	"
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
první	první	k4xOgFnSc1	první
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
musí	muset	k5eAaImIp3nS	muset
uživatel	uživatel	k1gMnSc1	uživatel
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
která	který	k3yQgFnSc1	který
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
dotyk	dotyk	k1gInSc1	dotyk
penisu	penis	k1gInSc2	penis
na	na	k7c6	na
"	"	kIx"	"
<g/>
nesprávné	správný	k2eNgFnSc6d1	nesprávná
<g/>
"	"	kIx"	"
straně	strana	k1gFnSc6	strana
rolovaného	rolovaný	k2eAgInSc2d1	rolovaný
kondomu	kondom	k1gInSc2	kondom
před	před	k7c7	před
aplikací	aplikace	k1gFnSc7	aplikace
potenciálně	potenciálně	k6eAd1	potenciálně
'	'	kIx"	'
<g/>
kontaminuje	kontaminovat	k5eAaBmIp3nS	kontaminovat
<g/>
'	'	kIx"	'
povrch	povrch	k6eAd1wR	povrch
tekutinou	tekutina	k1gFnSc7	tekutina
obsahující	obsahující	k2eAgNnSc4d1	obsahující
sperma	sperma	k1gNnSc4	sperma
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
kondom	kondom	k1gInSc1	kondom
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
latexové	latexový	k2eAgInPc4d1	latexový
kondomy	kondom	k1gInPc4	kondom
byly	být	k5eAaImAgFnP	být
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
některé	některý	k3yIgInPc1	některý
eventuálně	eventuálně	k6eAd1	eventuálně
začaly	začít	k5eAaPmAgInP	začít
mít	mít	k5eAaImF	mít
konce	konec	k1gInPc4	konec
s	s	k7c7	s
nádržkou	nádržka	k1gFnSc7	nádržka
pro	pro	k7c4	pro
ejakulovanou	ejakulovaný	k2eAgFnSc4d1	ejakulovaný
tekutinu	tekutina	k1gFnSc4	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
relativně	relativně	k6eAd1	relativně
skorá	skorá	k?	skorá
inovace	inovace	k1gFnSc1	inovace
–	–	k?	–
"	"	kIx"	"
<g/>
krátká	krátký	k2eAgFnSc1d1	krátká
čepice	čepice	k1gFnSc1	čepice
<g/>
"	"	kIx"	"
–	–	k?	–
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
jen	jen	k9	jen
hlavu	hlava	k1gFnSc4	hlava
penisu	penis	k1gInSc2	penis
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
krok	krok	k1gInSc4	krok
zpět	zpět	k6eAd1	zpět
při	při	k7c6	při
redukci	redukce	k1gFnSc6	redukce
přenosu	přenos	k1gInSc2	přenos
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
dekádách	dekáda	k1gFnPc6	dekáda
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
výrobci	výrobce	k1gMnPc1	výrobce
produkují	produkovat	k5eAaImIp3nP	produkovat
mnoho	mnoho	k4c4	mnoho
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
kondomů	kondom	k1gInPc2	kondom
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
aromatických	aromatický	k2eAgInPc2d1	aromatický
a	a	k8xC	a
speciálních	speciální	k2eAgInPc2d1	speciální
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
stimulující	stimulující	k2eAgInPc4d1	stimulující
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stimulační	stimulační	k2eAgFnPc1d1	stimulační
vlastnosti	vlastnost	k1gFnPc1	vlastnost
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
zvětšené	zvětšený	k2eAgInPc4d1	zvětšený
konce	konec	k1gInPc4	konec
nebo	nebo	k8xC	nebo
váčky	váček	k1gInPc4	váček
pro	pro	k7c4	pro
snadnější	snadný	k2eAgFnSc4d2	snazší
akomodaci	akomodace	k1gFnSc4	akomodace
žaludu	žalud	k1gInSc2	žalud
a	a	k8xC	a
tkaninové	tkaninový	k2eAgInPc4d1	tkaninový
povrchy	povrch	k1gInPc4	povrch
jako	jako	k8xC	jako
žebrování	žebrování	k1gNnPc4	žebrování
nebo	nebo	k8xC	nebo
výstupky	výstupek	k1gInPc4	výstupek
(	(	kIx(	(
<g/>
malé	malý	k2eAgInPc1d1	malý
hrboly	hrbol	k1gInPc1	hrbol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kondomů	kondom	k1gInPc2	kondom
má	mít	k5eAaImIp3nS	mít
přidaný	přidaný	k2eAgInSc1d1	přidaný
spermicidní	spermicidní	k2eAgInSc1d1	spermicidní
lubrikant	lubrikant	k1gInSc1	lubrikant
(	(	kIx(	(
<g/>
mazadlo	mazadlo	k1gNnSc1	mazadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
efektivní	efektivní	k2eAgFnSc1d1	efektivní
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
separátní	separátní	k2eAgNnSc4d1	separátní
použití	použití	k1gNnSc4	použití
spermicidu	spermicid	k1gInSc2	spermicid
(	(	kIx(	(
<g/>
antikoncepčního	antikoncepční	k2eAgInSc2d1	antikoncepční
prostředku	prostředek	k1gInSc2	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
testu	test	k1gInSc2	test
kondomů	kondom	k1gInPc2	kondom
na	na	k7c4	na
mikroskopické	mikroskopický	k2eAgFnPc4d1	mikroskopická
díry	díra	k1gFnPc4	díra
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
umístění	umístění	k1gNnSc4	umístění
testovaného	testovaný	k2eAgInSc2d1	testovaný
kondomu	kondom	k1gInSc2	kondom
přes	přes	k7c4	přes
vodicí	vodicí	k2eAgFnSc4d1	vodicí
formu	forma	k1gFnSc4	forma
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
vodicí	vodicí	k2eAgFnSc7d1	vodicí
formou	forma	k1gFnSc7	forma
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Nezabrání	zabránit	k5eNaPmIp3nP	zabránit
<g/>
-li	i	k?	-li
kondom	kondom	k1gInSc4	kondom
přechodu	přechod	k1gInSc2	přechod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
vodicími	vodicí	k2eAgFnPc7d1	vodicí
formami	forma	k1gFnPc7	forma
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
to	ten	k3xDgNnSc1	ten
vadu	vada	k1gFnSc4	vada
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
dírách	díra	k1gFnPc6	díra
v	v	k7c6	v
kondomech	kondom	k1gInPc6	kondom
se	se	k3xPyFc4	se
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
být	být	k5eAaImF	být
nepravděpodobné	pravděpodobný	k2eNgNnSc4d1	nepravděpodobné
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dodrženy	dodržen	k2eAgInPc4d1	dodržen
adekvátní	adekvátní	k2eAgInPc4d1	adekvátní
způsoby	způsob	k1gInPc4	způsob
použití	použití	k1gNnSc2	použití
(	(	kIx(	(
<g/>
popis	popis	k1gInSc1	popis
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kondomy	kondom	k1gInPc1	kondom
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
jehněčí	jehněčí	k2eAgFnSc2d1	jehněčí
kůže	kůže	k1gFnSc2	kůže
<g/>
"	"	kIx"	"
–	–	k?	–
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
jehněčích	jehněčí	k2eAgNnPc2d1	jehněčí
střev	střevo	k1gNnPc2	střevo
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
efektivní	efektivní	k2eAgMnPc1d1	efektivní
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Nemnoho	nemnoho	k6eAd1	nemnoho
firem	firma	k1gFnPc2	firma
dnes	dnes	k6eAd1	dnes
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
též	též	k9	též
kondomy	kondom	k1gInPc4	kondom
z	z	k7c2	z
polyetylénu	polyetylén	k1gInSc2	polyetylén
nebo	nebo	k8xC	nebo
polyuretanu	polyuretan	k1gInSc2	polyuretan
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
efektivní	efektivní	k2eAgNnSc4d1	efektivní
jako	jako	k8xS	jako
latex	latex	k1gInSc4	latex
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgInP	být
natolik	natolik	k6eAd1	natolik
testovány	testovat	k5eAaImNgInP	testovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
alternativní	alternativní	k2eAgInPc1d1	alternativní
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
užitečné	užitečný	k2eAgInPc1d1	užitečný
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
alergie	alergie	k1gFnSc2	alergie
na	na	k7c4	na
latex	latex	k1gInSc4	latex
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
metoda	metoda	k1gFnSc1	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
mají	mít	k5eAaImIp3nP	mít
kondomy	kondom	k1gInPc4	kondom
výhodu	výhod	k1gInSc2	výhod
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejích	její	k3xOp3gNnPc2	její
použití	použití	k1gNnPc2	použití
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
efektů	efekt	k1gInPc2	efekt
a	a	k8xC	a
že	že	k8xS	že
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
i	i	k9	i
před	před	k7c7	před
sexuálně	sexuálně	k6eAd1	sexuálně
přenášenými	přenášený	k2eAgFnPc7d1	přenášená
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
paradox	paradox	k1gInSc1	paradox
v	v	k7c6	v
použití	použití	k1gNnSc6	použití
kondomů	kondom	k1gInPc2	kondom
pro	pro	k7c4	pro
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
<g/>
:	:	kIx,	:
jejích	její	k3xOp3gMnPc6	její
teoretická	teoretický	k2eAgFnSc1d1	teoretická
efektivnost	efektivnost	k1gFnSc1	efektivnost
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejích	její	k3xOp3gFnPc2	její
aktuální	aktuální	k2eAgInSc1d1	aktuální
efektivnost	efektivnost	k1gFnSc4	efektivnost
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
nedodrží	dodržet	k5eNaPmIp3nP	dodržet
adekvátní	adekvátní	k2eAgFnPc1d1	adekvátní
procedury	procedura	k1gFnPc1	procedura
použití	použití	k1gNnSc2	použití
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
dotyk	dotyk	k1gInSc4	dotyk
ženských	ženský	k2eAgFnPc2d1	ženská
genitálií	genitálie	k1gFnPc2	genitálie
stejnou	stejný	k2eAgFnSc7d1	stejná
(	(	kIx(	(
<g/>
neumytou	umytý	k2eNgFnSc7d1	neumytá
<g/>
)	)	kIx)	)
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
byl	být	k5eAaImAgMnS	být
stažen	stažen	k2eAgInSc4d1	stažen
kondom	kondom	k1gInSc4	kondom
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
potenciálně	potenciálně	k6eAd1	potenciálně
způsobit	způsobit	k5eAaPmF	způsobit
těhotenství	těhotenství	k1gNnSc4	těhotenství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Navíc	navíc	k6eAd1	navíc
průzkumy	průzkum	k1gInPc1	průzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
uživatelů	uživatel	k1gMnPc2	uživatel
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
správně	správně	k6eAd1	správně
kondom	kondom	k1gInSc4	kondom
nasadit	nasadit	k5eAaPmF	nasadit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
natržení	natržení	k1gNnSc2	natržení
a	a	k8xC	a
sklouznutí	sklouznutí	k1gNnSc2	sklouznutí
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proto	proto	k8xC	proto
kondomy	kondom	k1gInPc4	kondom
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
středně	středně	k6eAd1	středně
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	se	k3xPyFc4	se
spermicidy	spermicid	k1gInPc7	spermicid
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
porovnatelná	porovnatelný	k2eAgFnSc1d1	porovnatelná
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
metodami	metoda	k1gFnPc7	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
kondomu	kondom	k1gInSc2	kondom
kombinováno	kombinován	k2eAgNnSc1d1	kombinováno
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
metodou	metoda	k1gFnSc7	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
se	se	k3xPyFc4	se
spermicidy	spermicid	k1gInPc7	spermicid
nebo	nebo	k8xC	nebo
antikoncepčními	antikoncepční	k2eAgFnPc7d1	antikoncepční
tabletkami	tabletka	k1gFnPc7	tabletka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těhotenství	těhotenství	k1gNnSc1	těhotenství
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
lidé	člověk	k1gMnPc1	člověk
považují	považovat	k5eAaImIp3nP	považovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
natažení	natažení	k1gNnSc1	natažení
může	moct	k5eAaImIp3nS	moct
přerušit	přerušit	k5eAaPmF	přerušit
milostní	milostný	k2eAgMnPc1d1	milostný
předehru	předehra	k1gFnSc4	předehra
a	a	k8xC	a
něhu	něha	k1gFnSc4	něha
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
limitují	limitovat	k5eAaBmIp3nP	limitovat
jejich	jejich	k3xOp3gInSc4	jejich
prožitek	prožitek	k1gInSc4	prožitek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
eliminují	eliminovat	k5eAaBmIp3nP	eliminovat
kožní	kožní	k2eAgInSc4d1	kožní
kontakt	kontakt	k1gInSc4	kontakt
a	a	k8xC	a
redukují	redukovat	k5eAaBmIp3nP	redukovat
senzorickou	senzorický	k2eAgFnSc4d1	senzorická
stimulaci	stimulace	k1gFnSc4	stimulace
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ženy	žena	k1gFnPc1	žena
částečně	částečně	k6eAd1	částečně
mohou	moct	k5eAaImIp3nP	moct
řešit	řešit	k5eAaImF	řešit
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
tréninkem	trénink	k1gInSc7	trénink
vaginálních	vaginální	k2eAgInPc2d1	vaginální
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
specificky	specificky	k6eAd1	specificky
Pubococcygeus	Pubococcygeus	k1gInSc1	Pubococcygeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
i	i	k9	i
tyto	tento	k3xDgFnPc1	tento
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
kondomů	kondom	k1gInPc2	kondom
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
uváděny	uvádět	k5eAaImNgInP	uvádět
jako	jako	k8xC	jako
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	nejvíc	k6eAd1	nejvíc
selhání	selhání	k1gNnSc1	selhání
kondomu	kondom	k1gInSc2	kondom
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
chybné	chybný	k2eAgNnSc4d1	chybné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
některé	některý	k3yIgMnPc4	některý
experty	expert	k1gMnPc4	expert
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
k	k	k7c3	k
požadavku	požadavek	k1gInSc3	požadavek
za	za	k7c4	za
dřívější	dřívější	k2eAgFnSc4d1	dřívější
a	a	k8xC	a
explicitní	explicitní	k2eAgFnSc4d1	explicitní
(	(	kIx(	(
<g/>
jasnou	jasný	k2eAgFnSc4d1	jasná
<g/>
)	)	kIx)	)
sexuální	sexuální	k2eAgFnSc4d1	sexuální
výchovu	výchova	k1gFnSc4	výchova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
limitují	limitovat	k5eAaBmIp3nP	limitovat
religiózní	religiózní	k2eAgFnPc1d1	religiózní
skupiny	skupina	k1gFnPc1	skupina
protestující	protestující	k2eAgFnPc1d1	protestující
proti	proti	k7c3	proti
předmanželskému	předmanželský	k2eAgInSc3d1	předmanželský
sexu	sex	k1gInSc3	sex
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
cítí	cítit	k5eAaImIp3nP	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
použití	použití	k1gNnSc1	použití
kondomů	kondom	k1gInPc2	kondom
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
antikoncepčních	antikoncepční	k2eAgInPc2d1	antikoncepční
prostředků	prostředek	k1gInPc2	prostředek
mládeži	mládež	k1gFnSc3	mládež
podpoří	podpořit	k5eAaPmIp3nS	podpořit
podobné	podobný	k2eAgNnSc4d1	podobné
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
programy	program	k1gInPc4	program
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
jen	jen	k9	jen
na	na	k7c4	na
sexuální	sexuální	k2eAgFnSc4d1	sexuální
abstinenci	abstinence	k1gFnSc4	abstinence
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
výskyt	výskyt	k1gInSc4	výskyt
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
FHI	FHI	kA	FHI
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
uživatelů	uživatel	k1gMnPc2	uživatel
kondomů	kondom	k1gInPc2	kondom
zřídka	zřídka	k6eAd1	zřídka
zažila	zažít	k5eAaPmAgFnS	zažít
natržení	natržení	k1gNnSc3	natržení
nebo	nebo	k8xC	nebo
sklouznutí	sklouznutí	k1gNnSc3	sklouznutí
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
kondomy	kondom	k1gInPc1	kondom
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
%	%	kIx~	%
míru	mír	k1gInSc2	mír
selhání	selhání	k1gNnSc2	selhání
včetně	včetně	k7c2	včetně
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgInSc1d1	další
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zřídkavý	zřídkavý	k2eAgInSc4d1	zřídkavý
důvod	důvod	k1gInSc4	důvod
selhání	selhání	k1gNnSc4	selhání
kondomu	kondom	k1gInSc2	kondom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
sabotáž	sabotáž	k1gFnSc1	sabotáž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
partner	partner	k1gMnSc1	partner
chtěl	chtít	k5eAaImAgMnS	chtít
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
ne	ne	k9	ne
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
provedla	provést	k5eAaPmAgFnS	provést
podobnou	podobný	k2eAgFnSc4d1	podobná
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obvykle	obvykle	k6eAd1	obvykle
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
proděravění	proděravění	k1gNnSc4	proděravění
konce	konec	k1gInSc2	konec
kondomu	kondom	k1gInSc2	kondom
víckrát	víckrát	k6eAd1	víckrát
s	s	k7c7	s
jehlou	jehla	k1gFnSc7	jehla
před	před	k7c7	před
stykem	styk	k1gInSc7	styk
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
těhotenství	těhotenství	k1gNnSc1	těhotenství
nechtěné	chtěný	k2eNgNnSc1d1	nechtěné
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
podvodné	podvodný	k2eAgNnSc4d1	podvodné
a	a	k8xC	a
neetické	etický	k2eNgNnSc4d1	neetické
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
mužských	mužský	k2eAgInPc2d1	mužský
kondomů	kondom	k1gInPc2	kondom
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
:	:	kIx,	:
Nikdy	nikdy	k6eAd1	nikdy
kondom	kondom	k1gInSc1	kondom
nechytat	chytat	k5eNaImF	chytat
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
nehty	nehet	k1gInPc7	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Nasadit	nasadit	k5eAaPmF	nasadit
kondom	kondom	k1gInSc4	kondom
na	na	k7c4	na
penis	penis	k1gInSc4	penis
hned	hned	k6eAd1	hned
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
erekce	erekce	k1gFnSc1	erekce
a	a	k8xC	a
před	před	k7c7	před
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
vagínou	vagína	k1gFnSc7	vagína
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
před	před	k7c7	před
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
neporušenou	porušený	k2eNgFnSc4d1	neporušená
předkožku	předkožka	k1gFnSc4	předkožka
nebo	nebo	k8xC	nebo
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
obnovu	obnova	k1gFnSc4	obnova
předkožky	předkožka	k1gFnSc2	předkožka
<g/>
,	,	kIx,	,
kondom	kondom	k1gInSc1	kondom
se	se	k3xPyFc4	se
nejlépe	dobře	k6eAd3	dobře
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
se	s	k7c7	s
stáhnutou	stáhnutý	k2eAgFnSc7d1	stáhnutá
předkožkou	předkožka	k1gFnSc7	předkožka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
maximalizuje	maximalizovat	k5eAaBmIp3nS	maximalizovat
mobilitu	mobilita	k1gFnSc4	mobilita
a	a	k8xC	a
redukuje	redukovat	k5eAaBmIp3nS	redukovat
možnost	možnost	k1gFnSc4	možnost
natržení	natržení	k1gNnSc2	natržení
během	během	k7c2	během
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zvýšení	zvýšení	k1gNnSc1	zvýšení
citlivosti	citlivost	k1gFnSc2	citlivost
penisu	penis	k1gInSc2	penis
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
přidání	přidání	k1gNnSc1	přidání
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
lubrikantu	lubrikant	k1gInSc2	lubrikant
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
kondomu	kondom	k1gInSc2	kondom
před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
nasazením	nasazení	k1gNnSc7	nasazení
kondomu	kondom	k1gInSc2	kondom
na	na	k7c4	na
ztopořený	ztopořený	k2eAgInSc4d1	ztopořený
penis	penis	k1gInSc4	penis
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
lubrikace	lubrikace	k1gFnSc1	lubrikace
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
protržení	protržení	k1gNnSc2	protržení
latexoveho	latexove	k1gMnSc2	latexove
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Ponechte	Ponecht	k1gMnSc5	Ponecht
trochu	trochu	k6eAd1	trochu
prostoru	prostor	k1gInSc2	prostor
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kondomu	kondom	k1gInSc2	kondom
pro	pro	k7c4	pro
semeno	semeno	k1gNnSc4	semeno
–	–	k?	–
většina	většina	k1gFnSc1	většina
kondomů	kondom	k1gInPc2	kondom
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
váček	váček	k1gInSc1	váček
<g/>
.	.	kIx.	.
</s>
<s>
Stiskněte	stisknout	k5eAaPmRp2nP	stisknout
tento	tento	k3xDgInSc4	tento
váček	váček	k1gInSc4	váček
během	během	k7c2	během
natahování	natahování	k1gNnSc2	natahování
kondomu	kondom	k1gInSc2	kondom
na	na	k7c6	na
prevenci	prevence	k1gFnSc6	prevence
vzduchové	vzduchový	k2eAgFnSc2d1	vzduchová
bubliny	bublina	k1gFnSc2	bublina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
později	pozdě	k6eAd2	pozdě
způsobit	způsobit	k5eAaPmF	způsobit
protržení	protržení	k1gNnSc4	protržení
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
sexuální	sexuální	k2eAgFnPc1d1	sexuální
lubrikanty	lubrikanta	k1gFnPc1	lubrikanta
(	(	kIx(	(
<g/>
mazadla	mazadlo	k1gNnPc1	mazadlo
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
s	s	k7c7	s
kondomy	kondom	k1gInPc7	kondom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
olejové	olejový	k2eAgInPc1d1	olejový
lubrikanty	lubrikant	k1gInPc1	lubrikant
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgInP	mít
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oslabují	oslabovat	k5eAaImIp3nP	oslabovat
latex	latex	k1gInSc4	latex
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vagína	vagína	k1gFnSc1	vagína
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
lubrikant	lubrikant	k1gInSc1	lubrikant
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
redukci	redukce	k1gFnSc6	redukce
odírání	odírání	k1gNnSc2	odírání
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nepoužívejte	používat	k5eNaImRp2nP	používat
kondomy	kondom	k1gInPc4	kondom
s	s	k7c7	s
prošlou	prošlý	k2eAgFnSc7d1	prošlá
lhůtou	lhůta	k1gFnSc7	lhůta
použití	použití	k1gNnSc2	použití
i	i	k8xC	i
když	když	k8xS	když
mohou	moct	k5eAaImIp3nP	moct
vypadat	vypadat	k5eAaImF	vypadat
dobré	dobrý	k2eAgInPc1d1	dobrý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
roztrhnout	roztrhnout	k5eAaPmF	roztrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
delší	dlouhý	k2eAgNnSc4d2	delší
působení	působení	k1gNnSc4	působení
vyšší	vysoký	k2eAgFnSc2d2	vyšší
(	(	kIx(	(
<g/>
i	i	k9	i
tělesné	tělesný	k2eAgFnSc2d1	tělesná
<g/>
)	)	kIx)	)
teploty	teplota	k1gFnSc2	teplota
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
latex	latex	k1gInSc1	latex
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nenoste	nosit	k5eNaImRp2nP	nosit
kondomy	kondom	k1gInPc4	kondom
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
,	,	kIx,	,
v	v	k7c6	v
peněžence	peněženka	k1gFnSc6	peněženka
atd.	atd.	kA	atd.
a	a	k8xC	a
neskladujte	skladovat	k5eNaImRp2nP	skladovat
je	on	k3xPp3gNnSc4	on
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
tepelnými	tepelný	k2eAgInPc7d1	tepelný
výkyvy	výkyv	k1gInPc7	výkyv
(	(	kIx(	(
<g/>
např.	např.	kA	např.
za	za	k7c7	za
okny	okno	k1gNnPc7	okno
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Penis	penis	k1gInSc1	penis
s	s	k7c7	s
kondomem	kondom	k1gInSc7	kondom
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vytažen	vytáhnout	k5eAaPmNgMnS	vytáhnout
hned	hned	k6eAd1	hned
po	po	k7c6	po
ejakulaci	ejakulace	k1gFnSc6	ejakulace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
možno	možno	k6eAd1	možno
udržet	udržet	k5eAaPmF	udržet
erekci	erekce	k1gFnSc4	erekce
dál	daleko	k6eAd2	daleko
<g/>
;	;	kIx,	;
ponechání	ponechání	k1gNnSc1	ponechání
znamená	znamenat	k5eAaImIp3nS	znamenat
zbytečný	zbytečný	k2eAgInSc4d1	zbytečný
risk	risk	k1gInSc4	risk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytažení	vytažení	k1gNnSc6	vytažení
penisu	penis	k1gInSc2	penis
přidržte	přidržet	k5eAaPmRp2nP	přidržet
kondom	kondom	k1gInSc4	kondom
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohl	moct	k5eNaImAgMnS	moct
sklouznout	sklouznout	k5eAaPmF	sklouznout
<g/>
.	.	kIx.	.
</s>
<s>
Ruce	ruka	k1gFnPc1	ruka
a	a	k8xC	a
penis	penis	k1gInSc1	penis
před	před	k7c7	před
dalším	další	k2eAgInSc7d1	další
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
partnerem	partner	k1gMnSc7	partner
nutno	nutno	k6eAd1	nutno
pečlivě	pečlivě	k6eAd1	pečlivě
umýt	umýt	k5eAaPmF	umýt
<g/>
.	.	kIx.	.
</s>
<s>
Kondomy	kondom	k1gInPc1	kondom
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
použití	použití	k1gNnSc6	použití
hned	hned	k6eAd1	hned
zahoďte	zahodit	k5eAaPmRp2nP	zahodit
<g/>
.	.	kIx.	.
</s>
<s>
Cítíte	cítit	k5eAaImIp2nP	cítit
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyčejné	obyčejný	k2eAgInPc4d1	obyčejný
kondomy	kondom	k1gInPc4	kondom
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
nebo	nebo	k8xC	nebo
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc4d1	velké
<g/>
,	,	kIx,	,
uvažujte	uvažovat	k5eAaImRp2nP	uvažovat
nad	nad	k7c7	nad
použitím	použití	k1gNnSc7	použití
speciálních	speciální	k2eAgInPc2d1	speciální
rozměrů	rozměr	k1gInPc2	rozměr
nebo	nebo	k8xC	nebo
ženského	ženský	k2eAgInSc2d1	ženský
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Testujte	testovat	k5eAaImRp2nP	testovat
použití	použití	k1gNnSc4	použití
kondomu	kondom	k1gInSc2	kondom
nejdřív	dříve	k6eAd3	dříve
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
na	na	k7c6	na
dobrém	dobrý	k2eAgNnSc6d1	dobré
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
děláte	dělat	k5eAaImIp2nP	dělat
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
použijete	použít	k5eAaPmIp2nP	použít
kondom	kondom	k1gInSc4	kondom
poprvé	poprvé	k6eAd1	poprvé
před	před	k7c7	před
sexem	sex	k1gInSc7	sex
<g/>
.	.	kIx.	.
</s>
<s>
Nasazení	nasazení	k1gNnSc1	nasazení
mužského	mužský	k2eAgInSc2d1	mužský
kondomu	kondom	k1gInSc2	kondom
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
:	:	kIx,	:
Kontrola	kontrola	k1gFnSc1	kontrola
data	datum	k1gNnSc2	datum
expirace	expirace	k1gFnSc1	expirace
–	–	k?	–
kondomy	kondom	k1gInPc1	kondom
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
vytištěné	vytištěný	k2eAgNnSc1d1	vytištěné
datum	datum	k1gNnSc1	datum
expirace	expirace	k1gFnSc2	expirace
a	a	k8xC	a
číslo	číslo	k1gNnSc4	číslo
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Nepoužívejte	používat	k5eNaImRp2nP	používat
kondomy	kondom	k1gInPc4	kondom
po	po	k7c6	po
expiraci	expirace	k1gFnSc6	expirace
<g/>
.	.	kIx.	.
</s>
<s>
Opatrně	opatrně	k6eAd1	opatrně
otevřete	otevřít	k5eAaPmIp2nP	otevřít
fóliový	fóliový	k2eAgInSc4d1	fóliový
obal	obal	k1gInSc4	obal
podél	podél	k7c2	podél
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dbejte	dbát	k5eAaImRp2nP	dbát
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
kondom	kondom	k1gInSc4	kondom
nepoškodili	poškodit	k5eNaPmAgMnP	poškodit
ostrými	ostrý	k2eAgInPc7d1	ostrý
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Pevně	pevně	k6eAd1	pevně
stlačte	stlačit	k5eAaPmRp2nP	stlačit
spolu	spolu	k6eAd1	spolu
konec	konec	k1gInSc4	konec
kondomu	kondom	k1gInSc2	kondom
na	na	k7c4	na
vyvedení	vyvedení	k1gNnSc4	vyvedení
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mohl	moct	k5eAaImAgInS	moct
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
kondomu	kondom	k1gInSc6	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Vzduchové	vzduchový	k2eAgFnPc1d1	vzduchová
bubliny	bublina	k1gFnPc1	bublina
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
natržení	natržení	k1gNnSc4	natržení
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Dejte	dát	k5eAaPmRp2nP	dát
srolovaný	srolovaný	k2eAgInSc4d1	srolovaný
kondom	kondom	k1gInSc4	kondom
na	na	k7c4	na
konec	konec	k1gInSc4	konec
penisu	penis	k1gInSc2	penis
<g/>
.	.	kIx.	.
</s>
<s>
Ujistěte	ujistit	k5eAaPmRp2nP	ujistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
odroluje	odrolovat	k5eAaPmIp3nS	odrolovat
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
natažením	natažení	k1gNnSc7	natažení
<g/>
.	.	kIx.	.
</s>
<s>
Natáhněte	natáhnout	k5eAaPmRp2nP	natáhnout
kondom	kondom	k1gInSc4	kondom
přes	přes	k7c4	přes
penis	penis	k1gInSc4	penis
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Natahuje	natahovat	k5eAaImIp3nS	natahovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pozorně	pozorně	k6eAd1	pozorně
zkoumejte	zkoumat	k5eAaImRp2nP	zkoumat
kterým	který	k3yIgInSc7	který
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
kondom	kondom	k1gInSc1	kondom
dotkne	dotknout	k5eAaPmIp3nS	dotknout
konce	konec	k1gInPc4	konec
penisu	penis	k1gInSc2	penis
<g/>
.	.	kIx.	.
</s>
<s>
Ujistěte	ujistit	k5eAaPmRp2nP	ujistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kondom	kondom	k1gInSc1	kondom
není	být	k5eNaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
nebo	nebo	k8xC	nebo
že	že	k8xS	že
nespadne	spadnout	k5eNaPmIp3nS	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgInSc1d1	molekulární
kondom	kondom	k1gInSc1	kondom
Bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
Hygiena	hygiena	k1gFnSc1	hygiena
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kondom	kondom	k1gInSc1	kondom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kondom	kondom	k1gInSc1	kondom
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Návod	návod	k1gInSc1	návod
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
kondomu	kondom	k1gInSc2	kondom
</s>
