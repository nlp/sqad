<s>
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
a	a	k8xC	a
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
komunikační	komunikační	k2eAgInSc1d1	komunikační
systém	systém	k1gInSc1	systém
tvořený	tvořený	k2eAgInSc1d1	tvořený
specifickými	specifický	k2eAgInPc7d1	specifický
vizuálně-pohybovými	vizuálněohybův	k2eAgInPc7d1	vizuálně-pohybův
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
tvary	tvar	k1gInPc1	tvar
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
postavením	postavení	k1gNnSc7	postavení
a	a	k8xC	a
pohyby	pohyb	k1gInPc7	pohyb
<g/>
,	,	kIx,	,
mimikou	mimika	k1gFnSc7	mimika
<g/>
,	,	kIx,	,
pozicemi	pozice	k1gFnPc7	pozice
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Znakové	znakový	k2eAgInPc1d1	znakový
jazyky	jazyk	k1gInPc1	jazyk
vznikaly	vznikat	k5eAaImAgInP	vznikat
spontánně	spontánně	k6eAd1	spontánně
v	v	k7c6	v
komunitách	komunita	k1gFnPc6	komunita
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
této	tento	k3xDgFnSc2	tento
komunity	komunita	k1gFnSc2	komunita
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kromě	kromě	k7c2	kromě
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
a	a	k8xC	a
nedoslýchavých	nedoslýchavý	k2eAgMnPc2d1	nedoslýchavý
také	také	k6eAd1	také
tlumočníci	tlumočník	k1gMnPc1	tlumočník
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
<g/>
.	.	kIx.	.
</s>
<s>
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
poruchou	porucha	k1gFnSc7	porucha
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
afázie	afázie	k1gFnSc1	afázie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
schválila	schválit	k5eAaPmAgFnS	schválit
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
oficiální	oficiální	k2eAgInSc4d1	oficiální
státní	státní	k2eAgInSc4d1	státní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
stal	stát	k5eAaPmAgInS	stát
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
podobné	podobný	k2eAgInPc1d1	podobný
až	až	k8xS	až
těžko	těžko	k6eAd1	těžko
rozlišitelné	rozlišitelný	k2eAgInPc4d1	rozlišitelný
pojmy	pojem	k1gInPc4	pojem
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
znakovaný	znakovaný	k2eAgInSc1d1	znakovaný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
znaková	znakový	k2eAgFnSc1d1	znaková
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
posunková	posunkový	k2eAgFnSc1d1	posunková
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
posunčina	posunčina	k1gFnSc1	posunčina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
přesnou	přesný	k2eAgFnSc4d1	přesná
definici	definice	k1gFnSc4	definice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
improvizovanou	improvizovaný	k2eAgFnSc7d1	improvizovaná
náhradou	náhrada	k1gFnSc7	náhrada
slovního	slovní	k2eAgNnSc2d1	slovní
dorozumívání	dorozumívání	k1gNnSc2	dorozumívání
<g/>
.	.	kIx.	.
</s>
<s>
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
český	český	k2eAgInSc1d1	český
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
dorozumívací	dorozumívací	k2eAgInSc1d1	dorozumívací
systém	systém	k1gInSc1	systém
sice	sice	k8xC	sice
národní	národní	k2eAgFnSc1d1	národní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
český	český	k2eAgInSc1d1	český
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
přímé	přímý	k2eAgFnSc2d1	přímá
návaznosti	návaznost	k1gFnSc2	návaznost
na	na	k7c4	na
mluvený	mluvený	k2eAgInSc4d1	mluvený
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
mluvených	mluvený	k2eAgFnPc2d1	mluvená
(	(	kIx(	(
<g/>
a	a	k8xC	a
psaných	psaný	k2eAgInPc2d1	psaný
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
znakové	znakový	k2eAgInPc1d1	znakový
jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
setkají	setkat	k5eAaPmIp3nP	setkat
uživatelé	uživatel	k1gMnPc1	uživatel
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
znakových	znakový	k2eAgInPc2d1	znakový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
komunikace	komunikace	k1gFnSc1	komunikace
značně	značně	k6eAd1	značně
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
se	se	k3xPyFc4	se
setkají	setkat	k5eAaPmIp3nP	setkat
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
hovořící	hovořící	k2eAgInSc1d1	hovořící
různými	různý	k2eAgInPc7d1	různý
mluvenými	mluvený	k2eAgInPc7d1	mluvený
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
otevírá	otevírat	k5eAaImIp3nS	otevírat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
komunitě	komunita	k1gFnSc3	komunita
neslyšících	slyšící	k2eNgFnPc2d1	neslyšící
<g/>
.	.	kIx.	.
</s>
<s>
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
však	však	k9	však
není	být	k5eNaImIp3nS	být
univerzální	univerzální	k2eAgNnSc1d1	univerzální
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
navzájem	navzájem	k6eAd1	navzájem
bližších	blízký	k2eAgInPc2d2	bližší
nebo	nebo	k8xC	nebo
vzdálenějších	vzdálený	k2eAgInPc2d2	vzdálenější
znakových	znakový	k2eAgInPc2d1	znakový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
navzájem	navzájem	k6eAd1	navzájem
nesrozumitelné	srozumitelný	k2eNgNnSc1d1	nesrozumitelné
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
vzato	vzít	k5eAaPmNgNnS	vzít
každý	každý	k3xTgInSc1	každý
mluvený	mluvený	k2eAgMnSc1d1	mluvený
(	(	kIx(	(
<g/>
a	a	k8xC	a
psaný	psaný	k2eAgInSc1d1	psaný
<g/>
)	)	kIx)	)
jazyk	jazyk	k1gInSc1	jazyk
má	mít	k5eAaImIp3nS	mít
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
ještě	ještě	k6eAd1	ještě
jazyk	jazyk	k1gInSc1	jazyk
znakový	znakový	k2eAgInSc1d1	znakový
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nP	hovořit
tímto	tento	k3xDgInSc7	tento
mluveným	mluvený	k2eAgInSc7d1	mluvený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
komunita	komunita	k1gFnSc1	komunita
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
<g/>
.	.	kIx.	.
</s>
<s>
Znakové	znakový	k2eAgInPc1d1	znakový
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
izolovány	izolovat	k5eAaBmNgInP	izolovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mluvené	mluvený	k2eAgInPc1d1	mluvený
jazyky	jazyk	k1gInPc1	jazyk
geografickými	geografický	k2eAgFnPc7d1	geografická
a	a	k8xC	a
kulturními	kulturní	k2eAgFnPc7d1	kulturní
hranicemi	hranice	k1gFnPc7	hranice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nejsou	být	k5eNaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
znakové	znakový	k2eAgInPc1d1	znakový
jazyky	jazyk	k1gInPc1	jazyk
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
jazycích	jazyk	k1gInPc6	jazyk
mluvených	mluvený	k2eAgInPc6d1	mluvený
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
národní	národní	k2eAgFnSc2d1	národní
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spíše	spíše	k9	spíše
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
daný	daný	k2eAgInSc1d1	daný
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Český	český	k2eAgInSc1d1	český
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
Americký	americký	k2eAgInSc1d1	americký
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
USA	USA	kA	USA
také	také	k9	také
třeba	třeba	k6eAd1	třeba
i	i	k9	i
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc6d1	mluvící
části	část	k1gFnSc6	část
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Quebecký	quebecký	k2eAgInSc1d1	quebecký
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
např.	např.	kA	např.
i	i	k8xC	i
Britský	britský	k2eAgInSc1d1	britský
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
nebo	nebo	k8xC	nebo
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
znakový	znakový	k2eAgInSc1d1	znakový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
"	"	kIx"	"
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přirozeným	přirozený	k2eAgInSc7d1	přirozený
vývojem	vývoj	k1gInSc7	vývoj
z	z	k7c2	z
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
souboru	soubor	k1gInSc2	soubor
znaků	znak	k1gInPc2	znak
Gestuno	Gestuna	k1gFnSc5	Gestuna
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
žádná	žádný	k3yNgNnPc4	žádný
gramatická	gramatický	k2eAgNnPc4d1	gramatické
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
z	z	k7c2	z
Gestuna	Gestuna	k1gFnSc1	Gestuna
a	a	k8xC	a
z	z	k7c2	z
gramatik	gramatika	k1gFnPc2	gramatika
znakových	znakový	k2eAgInPc2d1	znakový
jazyků	jazyk	k1gInPc2	jazyk
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pidžin	pidžin	k1gInSc1	pidžin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
při	při	k7c6	při
Deaflympiádě	Deaflympiáda	k1gFnSc6	Deaflympiáda
a	a	k8xC	a
při	při	k7c6	při
setkáních	setkání	k1gNnPc6	setkání
Světové	světový	k2eAgFnSc2d1	světová
federace	federace	k1gFnSc2	federace
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
neslyšících	slyšící	k2eNgFnPc2d1	neslyšící
však	však	k9	však
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
znakový	znakový	k2eAgInSc4d1	znakový
systém	systém	k1gInSc4	systém
neovládá	ovládat	k5eNaImIp3nS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
již	již	k6eAd1	již
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Znakovaný	Znakovaný	k2eAgInSc1d1	Znakovaný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
znakovaná	znakovaný	k2eAgFnSc1d1	znakovaná
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
umělý	umělý	k2eAgInSc1d1	umělý
jazykový	jazykový	k2eAgInSc1d1	jazykový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
skladbu	skladba	k1gFnSc4	skladba
mluvené	mluvený	k2eAgFnSc2d1	mluvená
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
gramatické	gramatický	k2eAgInPc4d1	gramatický
prostředky	prostředek	k1gInPc4	prostředek
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
artikulován	artikulovat	k5eAaImNgInS	artikulovat
současně	současně	k6eAd1	současně
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
českého	český	k2eAgInSc2d1	český
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
umělým	umělý	k2eAgFnPc3d1	umělá
systémům	systém	k1gInPc3	systém
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
národní	národní	k2eAgFnPc4d1	národní
verze	verze	k1gFnPc4	verze
však	však	k9	však
neslyšící	slyšící	k2eNgMnSc1d1	neslyšící
mnohdy	mnohdy	k6eAd1	mnohdy
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zpravidla	zpravidla	k6eAd1	zpravidla
neovládají	ovládat	k5eNaImIp3nP	ovládat
gramatiku	gramatika	k1gFnSc4	gramatika
mluveného	mluvený	k2eAgInSc2d1	mluvený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
generace	generace	k1gFnPc1	generace
jsou	být	k5eAaImIp3nP	být
zvyklé	zvyklý	k2eAgFnPc1d1	zvyklá
spíše	spíše	k9	spíše
na	na	k7c4	na
znakovanou	znakovaný	k2eAgFnSc4d1	znakovaná
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Znaková	znakový	k2eAgFnSc1d1	znaková
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
znakové	znakový	k2eAgFnSc6d1	znaková
řeči	řeč	k1gFnSc6	řeč
definovaný	definovaný	k2eAgInSc4d1	definovaný
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
oba	dva	k4xCgInPc4	dva
uvedené	uvedený	k2eAgInPc4d1	uvedený
systémy	systém	k1gInPc4	systém
dorozumívání	dorozumívání	k1gNnSc2	dorozumívání
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
český	český	k2eAgInSc4d1	český
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
i	i	k8xC	i
znakovanou	znakovaný	k2eAgFnSc4d1	znakovaná
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Gabrielová	Gabrielová	k1gFnSc1	Gabrielová
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Paur	Paur	k1gMnSc1	Paur
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Paur	Paur	k1gMnSc1	Paur
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
znakové	znakový	k2eAgFnSc2d1	znaková
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
Horizont	horizont	k1gInSc1	horizont
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1988	[number]	k4	1988
Miloň	Miloň	k1gMnSc1	Miloň
Potměšil	potměšil	k1gMnSc1	potměšil
<g/>
:	:	kIx,	:
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
Federace	federace	k1gFnSc1	federace
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
sluchově	sluchově	k6eAd1	sluchově
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
Alena	Alena	k1gFnSc1	Alena
Macurová	Macurová	k1gFnSc1	Macurová
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
výzkumu	výzkum	k1gInSc2	výzkum
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
komora	komora	k1gFnSc1	komora
tlumočníků	tlumočník	k1gMnPc2	tlumočník
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87218-00-6	[number]	k4	978-80-87218-00-6
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
Dílo	dílo	k1gNnSc1	dílo
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
znakové	znakový	k2eAgFnSc6d1	znaková
řeči	řeč	k1gFnSc6	řeč
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Výuka	výuka	k1gFnSc1	výuka
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
znakovém	znakový	k2eAgInSc6d1	znakový
jazyce	jazyk	k1gInSc6	jazyk
Historie	historie	k1gFnSc1	historie
Českého	český	k2eAgInSc2d1	český
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
Historie	historie	k1gFnSc2	historie
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
znakovou	znakový	k2eAgFnSc7d1	znaková
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
znakovým	znakový	k2eAgInSc7d1	znakový
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
znakovanou	znakovaný	k2eAgFnSc7d1	znakovaná
češtinou	čeština	k1gFnSc7	čeština
Poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
humor	humor	k1gInSc4	humor
ve	v	k7c6	v
znakovém	znakový	k2eAgInSc6d1	znakový
jazyce	jazyk	k1gInSc6	jazyk
Tlumočení	tlumočení	k1gNnSc2	tlumočení
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
do	do	k7c2	do
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
Video	video	k1gNnSc1	video
slovník	slovník	k1gInSc1	slovník
znakových	znakový	k2eAgInPc2d1	znakový
jazyků	jazyk	k1gInPc2	jazyk
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
komunikačních	komunikační	k2eAgInPc6d1	komunikační
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
znakové	znakový	k2eAgFnSc6d1	znaková
řeči	řeč	k1gFnSc6	řeč
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
