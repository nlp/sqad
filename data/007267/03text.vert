<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
mléko	mléko	k1gNnSc1	mléko
produkované	produkovaný	k2eAgNnSc1d1	produkované
laktací	laktace	k1gFnSc7	laktace
v	v	k7c6	v
mléčných	mléčný	k2eAgFnPc6d1	mléčná
žlázách	žláza	k1gFnPc6	žláza
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
prsou	prsa	k1gNnPc6	prsa
sloužící	sloužící	k2eAgFnSc2d1	sloužící
k	k	k7c3	k
výživě	výživa	k1gFnSc3	výživa
novorozeného	novorozený	k2eAgMnSc2d1	novorozený
potomka	potomek	k1gMnSc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
přirozená	přirozený	k2eAgFnSc1d1	přirozená
strava	strava	k1gFnSc1	strava
pro	pro	k7c4	pro
novorozence	novorozenec	k1gMnSc4	novorozenec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
veškeré	veškerý	k3xTgFnPc4	veškerý
potřebné	potřebný	k2eAgFnPc4d1	potřebná
látky	látka	k1gFnPc4	látka
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
novorozenci	novorozenec	k1gMnPc1	novorozenec
předáváno	předáván	k2eAgNnSc4d1	předáváno
pomocí	pomocí	k7c2	pomocí
kojení	kojení	k1gNnSc2	kojení
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yIgNnSc2	který
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
uvolňováno	uvolňován	k2eAgNnSc1d1	uvolňováno
čiré	čirý	k2eAgNnSc1d1	čiré
vodnější	vodný	k2eAgNnSc1d2	vodný
mléko	mléko	k1gNnSc1	mléko
k	k	k7c3	k
uhašení	uhašení	k1gNnSc3	uhašení
žízně	žízeň	k1gFnSc2	žízeň
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
tučnější	tučný	k2eAgNnSc1d2	tučnější
tzv.	tzv.	kA	tzv.
zadní	zadní	k2eAgNnSc1d1	zadní
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
dítěte	dítě	k1gNnSc2	dítě
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
minimálně	minimálně	k6eAd1	minimálně
půlroční	půlroční	k2eAgNnSc4d1	půlroční
plné	plný	k2eAgNnSc4d1	plné
kojení	kojení	k1gNnSc4	kojení
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
prolaktinem	prolaktin	k1gInSc7	prolaktin
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc3	jeho
uvolňování	uvolňování	k1gNnSc3	uvolňování
z	z	k7c2	z
prsní	prsní	k2eAgFnSc2d1	prsní
žlážy	žláža	k1gFnSc2	žláža
pak	pak	k6eAd1	pak
oxytocinem	oxytocin	k1gInSc7	oxytocin
<g/>
.	.	kIx.	.
</s>
<s>
Hladiny	hladina	k1gFnPc4	hladina
prolaktinu	prolaktin	k1gInSc2	prolaktin
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
léky	lék	k1gInPc4	lék
–	–	k?	–
především	především	k6eAd1	především
agonisty	agonista	k1gMnPc4	agonista
dopaminu	dopamin	k1gInSc2	dopamin
–	–	k?	–
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
zástavě	zástava	k1gFnSc3	zástava
laktace	laktace	k1gFnSc2	laktace
<g/>
.	.	kIx.	.
</s>
<s>
Oxytocin	oxytocin	k1gInSc1	oxytocin
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
citového	citový	k2eAgInSc2d1	citový
vztahu	vztah	k1gInSc2	vztah
matka	matka	k1gFnSc1	matka
<g/>
/	/	kIx~	/
<g/>
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
má	mít	k5eAaImIp3nS	mít
snížený	snížený	k2eAgInSc4d1	snížený
obsah	obsah	k1gInSc4	obsah
bílkovin	bílkovina	k1gFnPc2	bílkovina
než	než	k8xS	než
jiné	jiný	k2eAgInPc4d1	jiný
savčí	savčí	k2eAgInPc4d1	savčí
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
novorozence	novorozenec	k1gMnSc2	novorozenec
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
hlavně	hlavně	k9	hlavně
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bílkovin	bílkovina	k1gFnPc2	bílkovina
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
hlavně	hlavně	k9	hlavně
laktalbumin	laktalbumin	k2eAgMnSc1d1	laktalbumin
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
připadá	připadat	k5eAaImIp3nS	připadat
kasein	kasein	k1gInSc4	kasein
<g/>
.	.	kIx.	.
</s>
<s>
Tuky	tuk	k1gInPc1	tuk
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
mateřském	mateřský	k2eAgNnSc6d1	mateřské
mléce	mléko	k1gNnSc6	mléko
proměnlivou	proměnlivý	k2eAgFnSc4d1	proměnlivá
složku	složka	k1gFnSc4	složka
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
stravovacích	stravovací	k2eAgInPc6d1	stravovací
návycích	návyk	k1gInPc6	návyk
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
tuků	tuk	k1gInPc2	tuk
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
nenasycené	nasycený	k2eNgNnSc4d1	nenasycené
mastné	mastné	k1gNnSc4	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
kravskému	kravský	k2eAgNnSc3d1	kravské
mléku	mléko	k1gNnSc3	mléko
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
4	[number]	k4	4
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
bohaté	bohatý	k2eAgNnSc1d1	bohaté
i	i	k9	i
na	na	k7c4	na
imunologicky	imunologicky	k6eAd1	imunologicky
aktivní	aktivní	k2eAgFnPc4d1	aktivní
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
vývoj	vývoj	k1gInSc4	vývoj
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
kojence	kojenec	k1gMnSc4	kojenec
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
některé	některý	k3yIgFnPc4	některý
adipokiny	adipokina	k1gFnPc4	adipokina
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
AFABP	AFABP	kA	AFABP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
déle	dlouho	k6eAd2	dlouho
kojené	kojený	k2eAgFnPc1d1	kojená
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
méně	málo	k6eAd2	málo
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
obezitou	obezita	k1gFnSc7	obezita
a	a	k8xC	a
metabolickými	metabolický	k2eAgFnPc7d1	metabolická
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trávení	trávení	k1gNnSc6	trávení
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
hlavně	hlavně	k9	hlavně
enzym	enzym	k1gInSc4	enzym
lipáza	lipáza	k1gFnSc1	lipáza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mateřském	mateřský	k2eAgNnSc6d1	mateřské
mléce	mléko	k1gNnSc6	mléko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
tento	tento	k3xDgInSc1	tento
enzym	enzym	k1gInSc4	enzym
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
se	se	k3xPyFc4	se
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
ženy	žena	k1gFnSc2	žena
mohou	moct	k5eAaImIp3nP	moct
dostávat	dostávat	k5eAaImF	dostávat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
syntetické	syntetický	k2eAgFnPc1d1	syntetická
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
některé	některý	k3yIgFnPc4	některý
UV	UV	kA	UV
filtry	filtr	k1gInPc4	filtr
používané	používaný	k2eAgInPc4d1	používaný
v	v	k7c6	v
opalovacích	opalovací	k2eAgInPc6d1	opalovací
krémech	krém	k1gInPc6	krém
nebo	nebo	k8xC	nebo
perzistentní	perzistentní	k2eAgFnPc4d1	perzistentní
znečišťující	znečišťující	k2eAgFnPc4d1	znečišťující
látky	látka	k1gFnPc4	látka
jako	jako	k8xC	jako
polychlorované	polychlorovaný	k2eAgInPc1d1	polychlorovaný
bifenyly	bifenyl	k1gInPc1	bifenyl
(	(	kIx(	(
<g/>
PCB	PCB	kA	PCB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
