<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
přirozená	přirozený	k2eAgFnSc1d1	přirozená
strava	strava	k1gFnSc1	strava
pro	pro	k7c4	pro
novorozence	novorozenec	k1gMnSc4	novorozenec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
veškeré	veškerý	k3xTgFnPc4	veškerý
potřebné	potřebný	k2eAgFnPc4d1	potřebná
látky	látka	k1gFnPc4	látka
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
