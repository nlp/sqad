<s>
Meteoritické	Meteoritický	k2eAgNnSc1d1	Meteoritický
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dopadu	dopad	k1gInSc2	dopad
meteoritu	meteorit	k1gInSc2	meteorit
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
