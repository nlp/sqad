<p>
<s>
Meteoritické	Meteoritický	k2eAgNnSc1d1	Meteoritický
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dopadu	dopad	k1gInSc2	dopad
meteoritu	meteorit	k1gInSc2	meteorit
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
meteoritického	meteoritický	k2eAgInSc2d1	meteoritický
kráteru	kráter	k1gInSc2	kráter
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zaplněna	zaplnit	k5eAaPmNgFnS	zaplnit
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
nebo	nebo	k8xC	nebo
podpovrchovou	podpovrchový	k2eAgFnSc7d1	podpovrchová
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největšim	Největšim	k?	Největšim
meteoritickým	meteoritický	k2eAgInSc7d1	meteoritický
kráterem	kráter	k1gInSc7	kráter
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
Siljan	Siljan	k1gInSc4	Siljan
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Lac	Lac	k?	Lac
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Eau	Eau	k1gMnSc5	Eau
Claire	Clair	k1gMnSc5	Clair
(	(	kIx(	(
<g/>
Clearwater	Clearwater	k1gMnSc1	Clearwater
Lakes	Lakes	k1gMnSc1	Lakes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Manicouagan	Manicouagan	k1gInSc1	Manicouagan
<g/>
,	,	kIx,	,
Pingualuit	Pingualuit	k1gInSc1	Pingualuit
<g/>
,	,	kIx,	,
Wanapitei	Wanapitei	k1gNnSc1	Wanapitei
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elgygytgyn	Elgygytgyn	k1gNnSc1	Elgygytgyn
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lappajärvi	Lappajärev	k1gFnSc3	Lappajärev
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bosumtwi	Bosumtwi	k1gNnSc1	Bosumtwi
(	(	kIx(	(
<g/>
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Morasko	Morasko	k1gNnSc1	Morasko
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karakul	Karakul	k1gInSc1	Karakul
(	(	kIx(	(
<g/>
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jezioro	Jeziora	k1gFnSc5	Jeziora
meteorytowe	meteorytowe	k1gNnPc2	meteorytowe
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kráter	kráter	k1gInSc1	kráter
</s>
</p>
<p>
<s>
Patera	Patera	k1gMnSc1	Patera
</s>
</p>
