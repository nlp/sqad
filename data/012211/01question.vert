<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
druh	druh	k1gInSc1	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dopadu	dopad	k1gInSc2	dopad
meteoritu	meteorit	k1gInSc2	meteorit
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
