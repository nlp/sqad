<p>
<s>
Seriál	seriál	k1gInSc1	seriál
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
opakující	opakující	k2eAgFnSc4d1	opakující
se	se	k3xPyFc4	se
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
děj	děj	k1gInSc4	děj
nebo	nebo	k8xC	nebo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sled	sled	k1gInSc4	sled
novinových	novinový	k2eAgInPc2d1	novinový
či	či	k8xC	či
časopiseckých	časopisecký	k2eAgInPc2d1	časopisecký
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
,	,	kIx,	,
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
fotografií	fotografia	k1gFnPc2	fotografia
nebo	nebo	k8xC	nebo
o	o	k7c4	o
audiovizuální	audiovizuální	k2eAgNnSc4d1	audiovizuální
dílo	dílo	k1gNnSc4	dílo
s	s	k7c7	s
více	hodně	k6eAd2	hodně
díly	dílo	k1gNnPc7	dílo
<g/>
/	/	kIx~	/
<g/>
epizodami	epizoda	k1gFnPc7	epizoda
na	na	k7c6	na
pokračování	pokračování	k1gNnSc6	pokračování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
Časopisecký	časopisecký	k2eAgInSc1d1	časopisecký
seriál	seriál	k1gInSc1	seriál
–	–	k?	–
textové	textový	k2eAgNnSc1d1	textové
dílo	dílo	k1gNnSc1	dílo
vydávané	vydávaný	k2eAgNnSc1d1	vydávané
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc2d1	populární
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
seriál	seriál	k1gInSc1	seriál
–	–	k?	–
film	film	k1gInSc4	film
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
do	do	k7c2	do
několika	několik	k4yIc2	několik
segmentů	segment	k1gInPc2	segment
promítaných	promítaný	k2eAgInPc2d1	promítaný
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
<g/>
,	,	kIx,	,
populární	populární	k2eAgInSc1d1	populární
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
seriál	seriál	k1gInSc1	seriál
–	–	k?	–
zvukové	zvukový	k2eAgNnSc1d1	zvukové
seriálové	seriálový	k2eAgNnSc1d1	seriálové
dílo	dílo	k1gNnSc1	dílo
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
rozhlasem	rozhlas	k1gInSc7	rozhlas
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
–	–	k?	–
audiovizuální	audiovizuální	k2eAgNnSc1d1	audiovizuální
seriálové	seriálový	k2eAgNnSc1d1	seriálové
dílo	dílo	k1gNnSc1	dílo
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
televizí	televize	k1gFnSc7	televize
</s>
</p>
<p>
<s>
Webový	webový	k2eAgInSc1d1	webový
seriál	seriál	k1gInSc1	seriál
–	–	k?	–
audiovizuální	audiovizuální	k2eAgNnSc1d1	audiovizuální
seriálové	seriálový	k2eAgNnSc1d1	seriálové
dílo	dílo	k1gNnSc1	dílo
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
World	Worlda	k1gFnPc2	Worlda
Wide	Wid	k1gMnSc2	Wid
Webu	web	k1gInSc2	web
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
PETRÁČKOVÁ	Petráčková	k1gFnSc1	Petráčková
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
;	;	kIx,	;
KRAUS	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
A-	A-	k1gMnPc2	A-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
834	[number]	k4	834
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
607	[number]	k4	607
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
série	série	k1gFnSc1	série
</s>
</p>
