<p>
<s>
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
je	být	k5eAaImIp3nS	být
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
americkou	americký	k2eAgFnSc7d1	americká
společností	společnost	k1gFnSc7	společnost
Oath	Oatha	k1gFnPc2	Oatha
z	z	k7c2	z
koncernu	koncern	k1gInSc2	koncern
Verizon	Verizona	k1gFnPc2	Verizona
Communications	Communicationsa	k1gFnPc2	Communicationsa
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc4	portál
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1994	[number]	k4	1994
David	David	k1gMnSc1	David
Filo	Fila	k1gMnSc5	Fila
a	a	k8xC	a
Jerry	Jerr	k1gMnPc7	Jerr
Yang	Yanga	k1gFnPc2	Yanga
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
Stanfordovy	Stanfordův	k2eAgFnSc2d1	Stanfordova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Sunnyvale	Sunnyvala	k1gFnSc6	Sunnyvala
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gMnSc2	Vallea
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Yahoo	Yahoo	k6eAd1	Yahoo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
prvními	první	k4xOgMnPc7	první
písmeny	písmeno	k1gNnPc7	písmeno
slov	slovo	k1gNnPc2	slovo
Yet	Yet	k1gMnSc2	Yet
Another	Anothra	k1gFnPc2	Anothra
Hierarchical	Hierarchical	k1gMnSc1	Hierarchical
Officious	Officious	k1gMnSc1	Officious
Oracle	Oracle	k1gFnSc1	Oracle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Yahoo	Yahoo	k6eAd1	Yahoo
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
známé	známá	k1gFnSc2	známá
svým	svůj	k3xOyFgInSc7	svůj
internetovým	internetový	k2eAgInSc7d1	internetový
portálem	portál	k1gInSc7	portál
<g/>
,	,	kIx,	,
vyhledávačem	vyhledávač	k1gMnSc7	vyhledávač
Yahoo	Yahoo	k6eAd1	Yahoo
Search	Searcha	k1gFnPc2	Searcha
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
službami	služba	k1gFnPc7	služba
jako	jako	k8xS	jako
Yahoo	Yahoo	k6eAd1	Yahoo
Mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gNnSc1	Yahoo
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k6eAd1	Yahoo
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gNnSc1	Yahoo
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k6eAd1	Yahoo
odpovědi	odpověď	k1gFnPc1	odpověď
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
služba	služba	k1gFnSc1	služba
Yahoo	Yahoo	k6eAd1	Yahoo
fotky	fotka	k1gFnSc2	fotka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
s	s	k7c7	s
webem	web	k1gInSc7	web
Flickr	Flickr	k1gMnSc1	Flickr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Yahoo	Yahoo	k6eAd1	Yahoo
získala	získat	k5eAaPmAgFnS	získat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
ředitelkou	ředitelka	k1gFnSc7	ředitelka
a	a	k8xC	a
prezidentkou	prezidentka	k1gFnSc7	prezidentka
společnosti	společnost	k1gFnSc2	společnost
jmenována	jmenován	k2eAgFnSc1d1	jmenována
Marissa	Marissa	k1gFnSc1	Marissa
Mayerová	Mayerová	k1gFnSc1	Mayerová
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
pracovnice	pracovnice	k1gFnSc1	pracovnice
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Yahoo	Yahoo	k6eAd1	Yahoo
oficiálně	oficiálně	k6eAd1	oficiálně
převzetí	převzetí	k1gNnSc2	převzetí
blogovacích	blogovací	k2eAgFnPc2d1	blogovací
stránek	stránka	k1gFnPc2	stránka
Tumblr	Tumblr	k1gMnSc1	Tumblr
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnPc1	akcie
firmy	firma	k1gFnSc2	firma
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
Mayerové	Mayerová	k1gFnSc2	Mayerová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přešla	přejít	k5eAaPmAgFnS	přejít
k	k	k7c3	k
Yahoo	Yahoo	k6eAd1	Yahoo
z	z	k7c2	z
Googlu	Googl	k1gInSc2	Googl
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
oživit	oživit	k5eAaPmF	oživit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
internetových	internetový	k2eAgFnPc2d1	internetová
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
stouply	stoupnout	k5eAaPmAgFnP	stoupnout
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
hack	hack	k1gMnSc1	hack
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
všechny	všechen	k3xTgInPc4	všechen
uživatelské	uživatelský	k2eAgInPc4d1	uživatelský
účty	účet	k1gInPc4	účet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
</p>
<p>
<s>
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
</p>
