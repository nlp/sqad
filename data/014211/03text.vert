<s>
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
v	v	k7c6
únoru	únor	k1gInSc6
2009	#num#	k4
v	v	k7c4
OsluRodné	OsluRodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Richard	Richard	k1gMnSc1
Matthew	Matthew	k1gMnSc1
Stallman	Stallman	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc4
1953	#num#	k4
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Bydliště	bydliště	k1gNnSc1
</s>
<s>
Boston	Boston	k1gInSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Židé	Žid	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Harvardova	Harvardův	k2eAgMnSc2d1
univerzitaMassachusettský	univerzitaMassachusettský	k2eAgInSc4d1
technologický	technologický	k2eAgInSc4d1
institutHarvard	institutHarvard	k1gInSc4
School	School	k1gInSc1
of	of	k?
Engineering	Engineering	k1gInSc1
and	and	k?
Applied	Applied	k1gInSc1
Sciences	Sciencesa	k1gFnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
programátor	programátor	k1gMnSc1
<g/>
,	,	kIx,
informatik	informatik	k1gMnSc1
<g/>
,	,	kIx,
bloger	bloger	k1gMnSc1
<g/>
,	,	kIx,
inženýr	inženýr	k1gMnSc1
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc1
a	a	k8xC
free	free	k1gFnSc1
sofware	sofwar	k1gMnSc5
activist	activist	k1gInSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Massachusettský	massachusettský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Grace	Grace	k1gFnSc2
Murray	Murraa	k1gFnSc2
Hopperové	Hopperová	k1gFnSc2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
EFF	EFF	kA
Pioneer	Pioneer	kA
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
Památeční	památeční	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Yuriho	Yuri	k1gMnSc2
Rubinskyho	Rubinsky	k1gMnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
University	universita	k1gFnSc2
v	v	k7c6
Glasgow	Glasgow	k1gNnSc6
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Vrije	Vrij	k1gFnSc2
Universiteit	Universiteit	k1gMnSc1
Brussel	Brussel	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Nábož	Nábož	k1gFnSc4
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc4
</s>
<s>
ateismus	ateismus	k1gInSc1
Funkce	funkce	k1gFnSc2
</s>
<s>
Chief	Chief	k1gInSc1
GNUisance	GNUisanec	k1gInSc2
(	(	kIx(
<g/>
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
<g/>
;	;	kIx,
od	od	k7c2
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
Free	Free	k1gFnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
;	;	kIx,
1985	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
Web	web	k1gInSc1
</s>
<s>
stallman	stallman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Richard	Richard	k1gMnSc1
Matthew	Matthew	k1gMnSc1
Stallman	Stallman	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1953	#num#	k4
<g/>
,	,	kIx,
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
známý	známý	k2eAgMnSc1d1
též	též	k9
pod	pod	k7c7
iniciálami	iniciála	k1gFnPc7
RMS	RMS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zakladatel	zakladatel	k1gMnSc1
hnutí	hnutí	k1gNnSc2
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnPc2
a	a	k8xC
v	v	k7c6
říjnu	říjen	k1gInSc6
1985	#num#	k4
také	také	k9
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
spoluzakladatel	spoluzakladatel	k1gMnSc1
League	Leagu	k1gFnSc2
for	forum	k1gNnPc2
Programming	Programming	k1gInSc1
Freedom	Freedom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	k9
ochránil	ochránit	k5eAaPmAgInS
ideály	ideál	k1gInPc7
tohoto	tento	k3xDgNnSc2
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
Stallman	Stallman	k1gMnSc1
s	s	k7c7
konceptem	koncept	k1gInSc7
tzv.	tzv.	kA
copyleftu	copyleft	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
princip	princip	k1gInSc1
uplatnil	uplatnit	k5eAaPmAgInS
v	v	k7c6
široce	široko	k6eAd1
užívané	užívaný	k2eAgFnSc6d1
softwarové	softwarový	k2eAgFnSc6d1
licenci	licence	k1gFnSc6
GPL	GPL	kA
(	(	kIx(
<g/>
a	a	k8xC
později	pozdě	k6eAd2
též	též	k9
GFDL	GFDL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stallman	Stallman	k1gMnSc1
je	být	k5eAaImIp3nS
také	také	k9
známý	známý	k2eAgMnSc1d1
hacker	hacker	k1gMnSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
jehož	jehož	k3xOyRp3gInPc4
hlavní	hlavní	k2eAgInPc4d1
programátorské	programátorský	k2eAgInPc4d1
počiny	počin	k1gInPc4
patří	patřit	k5eAaImIp3nS
textový	textový	k2eAgInSc1d1
editor	editor	k1gInSc1
GNU	gnu	k1gNnSc1
Emacs	Emacs	k1gInSc1
<g/>
,	,	kIx,
překladač	překladač	k1gInSc1
GCC	GCC	kA
a	a	k8xC
debugger	debugger	k1gMnSc1
GDB	GDB	kA
–	–	k?
vše	všechen	k3xTgNnSc4
součást	součást	k1gFnSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
už	už	k6eAd1
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
převážně	převážně	k6eAd1
jen	jen	k6eAd1
obhajobě	obhajoba	k1gFnSc3
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
a	a	k8xC
zbývající	zbývající	k2eAgInSc4d1
čas	čas	k1gInSc4
programuje	programovat	k5eAaImIp3nS
GNU	gnu	k1gNnSc1
Emacs	Emacsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Rané	raný	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
si	se	k3xPyFc3
dodělal	dodělat	k5eAaPmAgMnS
maturitu	maturita	k1gFnSc4
na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
najmut	najmout	k5eAaPmNgMnS
firmou	firma	k1gFnSc7
IBM	IBM	kA
New	New	k1gFnSc7
York	York	k1gInSc1
Scientific	Scientifice	k1gInPc2
Center	centrum	k1gNnPc2
a	a	k8xC
během	během	k7c2
toho	ten	k3xDgNnSc2
léta	léto	k1gNnSc2
u	u	k7c2
nich	on	k3xPp3gMnPc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
program	program	k1gInSc4
−	−	k?
preprocesor	preprocesor	k1gInSc1
pro	pro	k7c4
programovací	programovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
PL	PL	kA
<g/>
/	/	kIx~
<g/>
I	i	k9
na	na	k7c4
IBM	IBM	kA
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
byl	být	k5eAaImAgMnS
Stallman	Stallman	k1gMnSc1
dobrovolným	dobrovolný	k2eAgMnSc7d1
laboratorním	laboratorní	k2eAgMnSc7d1
asistentem	asistent	k1gMnSc7
v	v	k7c6
oddělení	oddělení	k1gNnSc6
biologie	biologie	k1gFnSc2
na	na	k7c6
Rockefellerově	Rockefellerův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
dále	daleko	k6eAd2
studovat	studovat	k5eAaImF
matematiku	matematika	k1gFnSc4
nebo	nebo	k8xC
fyziku	fyzika	k1gFnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc4
profesor	profesor	k1gMnSc1
biologie	biologie	k1gFnSc2
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
zůstat	zůstat	k5eAaPmF
u	u	k7c2
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1971	#num#	k4
se	se	k3xPyFc4
Stallman	Stallman	k1gMnSc1
jako	jako	k8xC,k8xS
prvák	prvák	k1gMnSc1
na	na	k7c6
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
stal	stát	k5eAaPmAgMnS
programátorem	programátor	k1gInSc7
v	v	k7c6
AI	AI	kA
Laboratory	Laborator	k1gMnPc7
na	na	k7c4
MIT	MIT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
stálým	stálý	k2eAgMnSc7d1
členem	člen	k1gMnSc7
hackerské	hackerský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
znám	znám	k2eAgInSc1d1
pod	pod	k7c7
iniciálami	iniciála	k1gFnPc7
„	„	k?
<g/>
RMS	RMS	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
vydání	vydání	k1gNnSc6
Hackerského	Hackerský	k2eAgInSc2d1
slovníku	slovník	k1gInSc2
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
„	„	k?
<g/>
‚	‚	k?
<g/>
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
<g/>
‘	‘	k?
je	být	k5eAaImIp3nS
mé	můj	k3xOp1gNnSc4
světské	světský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
můžete	moct	k5eAaImIp2nP
mi	já	k3xPp1nSc3
však	však	k9
říkat	říkat	k5eAaImF
‚	‚	k?
<g/>
rms	rms	k?
<g/>
‘	‘	k?
<g/>
.	.	kIx.
<g/>
“	“	k?
Stallman	Stallman	k1gMnSc1
promoval	promovat	k5eAaBmAgMnS
jako	jako	k9
bakalář	bakalář	k1gMnSc1
fyziky	fyzika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Linux	linux	k1gInSc1
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvaldsa	k1gFnPc2
</s>
<s>
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
RMS	RMS	kA
zpívá	zpívat	k5eAaImIp3nS
Guantanamero	Guantanamero	k1gNnSc4
na	na	k7c4
YouTube	YouTub	k1gInSc5
–	–	k?
levicová	levicový	k2eAgFnSc1d1
parodie	parodie	k1gFnSc1
americké	americký	k2eAgFnSc2d1
základny	základna	k1gFnSc2
Guantánamo	Guantánama	k1gFnSc5
na	na	k7c4
melodii	melodie	k1gFnSc4
slavné	slavný	k2eAgFnSc2d1
kubánské	kubánský	k2eAgFnSc2d1
písně	píseň	k1gFnSc2
</s>
<s>
RMS	RMS	kA
zpívá	zpívat	k5eAaImIp3nS
Free	Free	k1gInSc1
Software	software	k1gInSc1
Song	song	k1gInSc4
na	na	k7c4
YouTube	YouTub	k1gInSc5
–	–	k?
„	„	k?
<g/>
hymna	hymna	k1gFnSc1
svobodného	svobodný	k2eAgNnSc2d1
software	software	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
složená	složený	k2eAgFnSc1d1
jím	on	k3xPp3gMnSc7
samým	samý	k3xTgMnSc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
Historie	historie	k1gFnSc1
</s>
<s>
GNU	gnu	k1gNnSc1
Manifest	manifest	k1gInSc1
•	•	k?
Projekt	projekt	k1gInSc1
GNU	gnu	k1gMnSc1
•	•	k?
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
•	•	k?
Historie	historie	k1gFnSc1
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
</s>
<s>
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
Affero	Affero	k1gNnSc1
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Free	Fre	k1gFnSc2
Documentation	Documentation	k1gInSc1
License	License	k1gFnSc2
•	•	k?
GPL	GPL	kA
linking	linking	k1gInSc1
exception	exception	k1gInSc1
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
GNU	gnu	k1gMnSc1
•	•	k?
GNU	gnu	k1gMnSc1
Health	Health	k1gMnSc1
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
NG	NG	kA
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
•	•	k?
GNOME	GNOME	kA
•	•	k?
Gnuzilla	Gnuzilla	k1gMnSc1
•	•	k?
IceCat	IceCat	k1gInSc1
•	•	k?
Gnash	Gnash	k1gInSc1
•	•	k?
Bash	Bash	k1gInSc1
•	•	k?
GCC	GCC	kA
•	•	k?
GNU	gnu	k1gNnSc2
Emacs	Emacsa	k1gFnPc2
•	•	k?
GNU	gnu	k1gMnSc1
C	C	kA
Library	Librara	k1gFnPc1
•	•	k?
Coreutils	Coreutils	k1gInSc1
•	•	k?
GNU	gnu	k1gMnSc1
build	build	k1gMnSc1
system	syst	k1gMnSc7
•	•	k?
gettext	gettext	k1gMnSc1
•	•	k?
Ostatní	ostatní	k2eAgMnSc1d1
GNU	gnu	k1gMnSc1
balíčky	balíček	k1gInPc4
a	a	k8xC
programy	program	k1gInPc4
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
</s>
<s>
Robert	Robert	k1gMnSc1
J.	J.	kA
Chassell	Chassell	k1gMnSc1
•	•	k?
Loï	Loï	k1gFnSc1
Dachary	Dachara	k1gFnSc2
•	•	k?
Ricardo	Ricardo	k1gNnSc4
Galli	Galle	k1gFnSc3
•	•	k?
Georg	Georg	k1gInSc1
C.	C.	kA
F.	F.	kA
Greve	Greev	k1gFnPc1
•	•	k?
Federico	Federico	k1gMnSc1
Heinz	Heinz	k1gMnSc1
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Mako	mako	k1gNnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Bradley	Bradlea	k1gFnSc2
M.	M.	kA
Kuhn	Kuhn	k1gMnSc1
•	•	k?
Eben	eben	k1gInSc1
Moglen	Moglen	k1gInSc1
•	•	k?
Brett	Brett	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Leonard	Leonard	k1gMnSc1
H.	H.	kA
Tower	Tower	k1gMnSc1
ml.	ml.	kA
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
-	-	kIx~
spor	spor	k1gInSc1
o	o	k7c4
jméno	jméno	k1gNnSc4
•	•	k?
Revolution	Revolution	k1gInSc1
OS	OS	kA
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Code	Code	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
124015891	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
4041	#num#	k4
2147	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
86110448	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
172877655	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
86110448	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
|	|	kIx~
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
