<s>
Daniel	Daniel	k1gMnSc1	Daniel
Vávra	Vávra	k1gMnSc1	Vávra
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1975	[number]	k4	1975
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
videoherní	videoherní	k2eAgMnSc1d1	videoherní
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
designér	designér	k1gMnSc1	designér
a	a	k8xC	a
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
firmy	firma	k1gFnSc2	firma
Warhorse	Warhorse	k1gFnSc2	Warhorse
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
videohra	videohra	k1gFnSc1	videohra
Mafia	Mafia	k1gFnSc1	Mafia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	city	k1gNnSc1	city
of	of	k?	of
Lost	Lostum	k1gNnPc2	Lostum
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
.	.	kIx.	.
</s>

