<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
český	český	k2eAgMnSc1d1	český
videoherní	videoherní	k2eAgMnSc1d1	videoherní
scénárista	scénárista	k1gMnSc1	scénárista
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
mezi	mezi	k7c7	mezi
jehož	jehož	k3xOyRp3gNnSc7	jehož
nejznámější	známý	k2eAgNnSc1d3	nejznámější
dílo	dílo	k1gNnSc1	dílo
patří	patřit	k5eAaImIp3nS	patřit
Mafia	Mafia	k1gFnSc1	Mafia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	city	k1gNnSc1	city
of	of	k?	of
Lost	Lostum	k1gNnPc2	Lostum
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
?	?	kIx.	?
</s>
