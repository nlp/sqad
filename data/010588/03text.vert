<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Vávra	Vávra	k1gMnSc1	Vávra
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1975	[number]	k4	1975
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
videoherní	videoherní	k2eAgMnSc1d1	videoherní
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
designér	designér	k1gMnSc1	designér
a	a	k8xC	a
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
firmy	firma	k1gFnSc2	firma
Warhorse	Warhorse	k1gFnSc2	Warhorse
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
videohra	videohra	k1gFnSc1	videohra
Mafia	Mafia	k1gFnSc1	Mafia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	city	k1gNnSc1	city
of	of	k?	of
Lost	Lostum	k1gNnPc2	Lostum
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
RPG	RPG	kA	RPG
hře	hra	k1gFnSc6	hra
Kingdom	Kingdom	k1gInSc4	Kingdom
Come	Com	k1gMnSc2	Com
<g/>
:	:	kIx,	:
Deliverance	Deliveranec	k1gMnSc2	Deliveranec
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2018	[number]	k4	2018
a	a	k8xC	a
hned	hned	k6eAd1	hned
velmi	velmi	k6eAd1	velmi
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
<g/>
.	.	kIx.	.
</s>
<s>
Obsadila	obsadit	k5eAaPmAgFnS	obsadit
přední	přední	k2eAgFnPc4d1	přední
příčky	příčka	k1gFnPc4	příčka
herních	herní	k2eAgFnPc2d1	herní
hitparád	hitparáda	k1gFnPc2	hitparáda
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
informační	informační	k2eAgFnPc4d1	informační
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
kreslí	kreslit	k5eAaImIp3nS	kreslit
komiksy	komiks	k1gInPc4	komiks
a	a	k8xC	a
fotografuje	fotografovat	k5eAaImIp3nS	fotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Střední	střední	k2eAgFnSc4d1	střední
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
<g/>
,	,	kIx,	,
Kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgMnS	začít
jako	jako	k9	jako
grafik	grafik	k1gMnSc1	grafik
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
TIPA	TIPA	kA	TIPA
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
uměleckého	umělecký	k2eAgMnSc2d1	umělecký
kováře	kovář	k1gMnSc2	kovář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejen	nejen	k6eAd1	nejen
hry	hra	k1gFnSc2	hra
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
píše	psát	k5eAaImIp3nS	psát
pro	pro	k7c4	pro
herní	herní	k2eAgInSc4d1	herní
magazín	magazín	k1gInSc4	magazín
Level	level	k1gInSc4	level
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
rubriku	rubrika	k1gFnSc4	rubrika
<g/>
;	;	kIx,	;
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
herní	herní	k2eAgFnPc4d1	herní
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisícíletí	tisícíletí	k1gNnSc1	tisícíletí
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
demoscéně	demoscéna	k1gFnSc6	demoscéna
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Broncs	Broncsa	k1gFnPc2	Broncsa
jako	jako	k8xS	jako
grafik	grafika	k1gFnPc2	grafika
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Hellboy	Hellboa	k1gFnSc2	Hellboa
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
sportovní	sportovní	k2eAgInPc4d1	sportovní
záliby	zálib	k1gInPc4	zálib
patří	patřit	k5eAaImIp3nS	patřit
hraní	hraní	k1gNnSc1	hraní
paintballu	paintball	k1gInSc2	paintball
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
protestech	protest	k1gInPc6	protest
proti	proti	k7c3	proti
zadržení	zadržení	k1gNnSc3	zadržení
Ivana	Ivan	k1gMnSc2	Ivan
Buchty	Buchta	k1gMnSc2	Buchta
a	a	k8xC	a
Martina	Martin	k1gMnSc2	Martin
Pelzara	Pelzar	k1gMnSc2	Pelzar
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Bohemia	bohemia	k1gFnSc1	bohemia
Interactive	Interactiv	k1gInSc5	Interactiv
<g/>
,	,	kIx,	,
vývojáře	vývojář	k1gMnSc4	vývojář
videohry	videohra	k1gFnSc2	videohra
Arma	Arma	k1gFnSc1	Arma
3	[number]	k4	3
během	během	k7c2	během
jejich	jejich	k3xOp3gFnSc2	jejich
dovolené	dovolená	k1gFnSc2	dovolená
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Illusion	Illusion	k1gInSc1	Illusion
Softworks	Softworks	k1gInSc1	Softworks
/	/	kIx~	/
2K	[number]	k4	2K
Czech	Czecha	k1gFnPc2	Czecha
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Illusion	Illusion	k1gInSc1	Illusion
Softworks	Softworks	k1gInSc1	Softworks
jako	jako	k8xS	jako
2D	[number]	k4	2D
grafik	grafika	k1gFnPc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
projekt	projekt	k1gInSc4	projekt
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
Hidden	Hiddna	k1gFnPc2	Hiddna
&	&	k?	&
Dangerous	Dangerous	k1gMnSc1	Dangerous
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
projektem	projekt	k1gInSc7	projekt
byla	být	k5eAaImAgFnS	být
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
hra	hra	k1gFnSc1	hra
Mafia	Mafia	k1gFnSc1	Mafia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	city	k1gNnSc1	city
of	of	k?	of
Lost	Lostum	k1gNnPc2	Lostum
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
designér	designér	k1gMnSc1	designér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
pražských	pražský	k2eAgInPc2d1	pražský
Illusion	Illusion	k1gInSc1	Illusion
Softworks	Softworks	k1gInSc1	Softworks
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
též	též	k9	též
na	na	k7c6	na
hře	hra	k1gFnSc6	hra
Wings	Wingsa	k1gFnPc2	Wingsa
of	of	k?	of
War	War	k1gFnPc2	War
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Hi-Tech	Hi-Tech	k1gInSc1	Hi-Tech
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
Illusion	Illusion	k1gInSc4	Illusion
Softworks	Softworksa	k1gFnPc2	Softworksa
je	být	k5eAaImIp3nS	být
Mafia	Mafia	k1gFnSc1	Mafia
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
napsal	napsat	k5eAaBmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
Mafie	mafie	k1gFnSc2	mafie
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
koupena	koupit	k5eAaPmNgFnS	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
společností	společnost	k1gFnPc2	společnost
2K	[number]	k4	2K
Games	Gamesa	k1gFnPc2	Gamesa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
z	z	k7c2	z
Illusion	Illusion	k1gInSc1	Illusion
Softworks	Softworksa	k1gFnPc2	Softworksa
na	na	k7c4	na
2K	[number]	k4	2K
Czech	Czecha	k1gFnPc2	Czecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
studio	studio	k1gNnSc4	studio
2K	[number]	k4	2K
Czech	Czecha	k1gFnPc2	Czecha
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Warhorse	Warhorse	k1gFnSc1	Warhorse
Studios	Studios	k?	Studios
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
spoluzaložil	spoluzaložit	k5eAaPmAgMnS	spoluzaložit
firmu	firma	k1gFnSc4	firma
Warhorse	Warhorse	k1gFnSc2	Warhorse
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
vydala	vydat	k5eAaPmAgFnS	vydat
RPG	RPG	kA	RPG
hru	hra	k1gFnSc4	hra
Kingdom	Kingdom	k1gInSc1	Kingdom
Come	Come	k1gNnSc2	Come
<g/>
:	:	kIx,	:
Deliverance	Deliverance	k1gFnSc2	Deliverance
<g/>
,	,	kIx,	,
realistické	realistický	k2eAgFnSc2d1	realistická
historické	historický	k2eAgFnSc2d1	historická
RPG	RPG	kA	RPG
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
realistickými	realistický	k2eAgInPc7d1	realistický
souboji	souboj	k1gInPc7	souboj
a	a	k8xC	a
jízdou	jízda	k1gFnSc7	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
<g/>
Přes	přes	k7c4	přes
obchod	obchod	k1gInSc4	obchod
Steam	Steam	k1gInSc1	Steam
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
kopií	kopie	k1gFnPc2	kopie
hry	hra	k1gFnSc2	hra
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
(	(	kIx(	(
<g/>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
KC	KC	kA	KC
<g/>
:	:	kIx,	:
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
společnost	společnost	k1gFnSc1	společnost
Warhorse	Warhorse	k1gFnSc1	Warhorse
oznámila	oznámit	k5eAaPmAgFnS	oznámit
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnSc7	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Videohry	videohra	k1gFnSc2	videohra
===	===	k?	===
</s>
</p>
<p>
<s>
Hidden	Hiddno	k1gNnPc2	Hiddno
&	&	k?	&
Dangerous	Dangerous	k1gMnSc1	Dangerous
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Illusion	Illusion	k1gInSc1	Illusion
Softworks	Softworksa	k1gFnPc2	Softworksa
</s>
</p>
<p>
<s>
Mafia	Mafia	k1gFnSc1	Mafia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Lost	Lost	k1gInSc1	Lost
Heaven	Heavna	k1gFnPc2	Heavna
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Illusion	Illusion	k1gInSc1	Illusion
Softworks	Softworksa	k1gFnPc2	Softworksa
</s>
</p>
<p>
<s>
Mafia	Mafia	k1gFnSc1	Mafia
II	II	kA	II
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2K	[number]	k4	2K
Czech	Czecha	k1gFnPc2	Czecha
</s>
</p>
<p>
<s>
Kingdom	Kingdom	k1gInSc1	Kingdom
Come	Come	k1gInSc1	Come
<g/>
:	:	kIx,	:
Deliverance	Deliverance	k1gFnSc1	Deliverance
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
Warhorse	Warhorse	k1gFnSc1	Warhorse
Studios	Studios	k?	Studios
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránkaDaniel	stránkaDaniel	k1gMnSc1	stránkaDaniel
Vávra	Vávra	k1gMnSc1	Vávra
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Vávra	Vávra	k1gMnSc1	Vávra
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Vávra	Vávra	k1gMnSc1	Vávra
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
