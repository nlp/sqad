<s>
Chlorečnan	chlorečnan	k1gInSc1	chlorečnan
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
synonymum	synonymum	k1gNnSc4	synonymum
Bertholetova	Bertholetův	k2eAgFnSc1d1	Bertholetův
sůl	sůl	k1gFnSc1	sůl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
chemickým	chemický	k2eAgInSc7d1	chemický
vzorcem	vzorec	k1gInSc7	vzorec
KClO	KClO	k1gFnSc2	KClO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
