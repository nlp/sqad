<p>
<s>
Chlorečnan	chlorečnan	k1gInSc1	chlorečnan
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
synonymum	synonymum	k1gNnSc4	synonymum
Bertholetova	Bertholetův	k2eAgFnSc1d1	Bertholetův
sůl	sůl	k1gFnSc1	sůl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
chemickým	chemický	k2eAgInSc7d1	chemický
vzorcem	vzorec	k1gInSc7	vzorec
KClO	KClO	k1gFnSc2	KClO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
explozivní	explozivní	k2eAgFnPc4d1	explozivní
a	a	k8xC	a
silné	silný	k2eAgFnPc4d1	silná
oxidační	oxidační	k2eAgFnPc4d1	oxidační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
a	a	k8xC	a
používal	používat	k5eAaImAgMnS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
plevele	plevel	k1gFnSc2	plevel
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
KCl	KCl	k1gFnPc2	KCl
nebo	nebo	k8xC	nebo
zaváděním	zavádění	k1gNnSc7	zavádění
Cl	Cl	k1gFnSc7	Cl
do	do	k7c2	do
teplého	teplý	k2eAgInSc2d1	teplý
roztoku	roztok	k1gInSc2	roztok
KOH	KOH	kA	KOH
(	(	kIx(	(
<g/>
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
šťavelovou	šťavelový	k2eAgFnSc7d1	šťavelová
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
vysoce	vysoce	k6eAd1	vysoce
reaktivní	reaktivní	k2eAgNnSc1d1	reaktivní
a	a	k8xC	a
nestabilní	stabilní	k2eNgNnSc1d1	nestabilní
ClO	clo	k1gNnSc1	clo
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rizika	riziko	k1gNnSc2	riziko
==	==	k?	==
</s>
</p>
<p>
<s>
Chlorečnan	chlorečnan	k1gInSc1	chlorečnan
draselný	draselný	k2eAgInSc1d1	draselný
je	být	k5eAaImIp3nS	být
toxická	toxický	k2eAgFnSc1d1	toxická
a	a	k8xC	a
zdraví	zdravit	k5eAaImIp3nS	zdravit
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znečišťuje	znečišťovat	k5eAaImIp3nS	znečišťovat
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
vysoce	vysoce	k6eAd1	vysoce
škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
chloru	chlor	k1gInSc2	chlor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
oxidační	oxidační	k2eAgFnPc4d1	oxidační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hoří	hořet	k5eAaImIp3nS	hořet
i	i	k9	i
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorečnan	chlorečnan	k1gInSc1	chlorečnan
draselný	draselný	k2eAgInSc1d1	draselný
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prekurzory	prekurzor	k1gInPc1	prekurzor
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
na	na	k7c4	na
zákaz	zákaz	k1gInSc4	zákaz
prodeje	prodej	k1gInSc2	prodej
obecné	obecný	k2eAgFnSc3d1	obecná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Chlorečnan	chlorečnan	k1gInSc1	chlorečnan
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
Molotovova	Molotovův	k2eAgInSc2d1	Molotovův
koktejlu	koktejl	k1gInSc2	koktejl
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
sirek	sirka	k1gFnPc2	sirka
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívané	používaný	k2eNgFnSc2d1	nepoužívaná
chlorátové	chlorátový	k2eAgFnSc2d1	chlorátový
trhaviny	trhavina	k1gFnSc2	trhavina
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
KClO	KClO	k1gFnSc2	KClO
<g/>
3	[number]	k4	3
a	a	k8xC	a
organického	organický	k2eAgNnSc2d1	organické
paliva	palivo	k1gNnSc2	palivo
–	–	k?	–
například	například	k6eAd1	například
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
moučky	moučka	k1gFnSc2	moučka
<g/>
,	,	kIx,	,
vánočních	vánoční	k2eAgFnPc2d1	vánoční
prskavek	prskavka	k1gFnPc2	prskavka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
využíval	využívat	k5eAaPmAgInS	využívat
pro	pro	k7c4	pro
barvení	barvení	k1gNnSc4	barvení
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
využíval	využívat	k5eAaPmAgMnS	využívat
například	například	k6eAd1	například
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chlorečnan	chlorečnan	k1gInSc1	chlorečnan
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
Chloristan	chloristan	k1gInSc1	chloristan
draselný	draselný	k2eAgInSc1d1	draselný
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
látek	látka	k1gFnPc2	látka
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c4	za
prekurzory	prekurzor	k1gInPc4	prekurzor
výbušnin	výbušnina	k1gFnPc2	výbušnina
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
