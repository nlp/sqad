<s>
Domesday	Domesday	k1gInPc1
Book	Booka	k1gFnPc2
(	(	kIx(
<g/>
také	také	k9
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Domesday	Domesdaa	k1gFnPc1
<g/>
,	,	kIx,
Kniha	kniha	k1gFnSc1
posledního	poslední	k2eAgInSc2d1
soudu	soud	k1gInSc2
nebo	nebo	k8xC
Kniha	kniha	k1gFnSc1
z	z	k7c2
Winchesteru	Winchester	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
záznam	záznam	k1gInSc4
podrobného	podrobný	k2eAgInSc2d1
majetkového	majetkový	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
nařízen	nařídit	k5eAaPmNgInS
Vilémem	Vilém	k1gMnSc7
Dobyvatelem	dobyvatel	k1gMnSc7
a	a	k8xC
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1086	#num#	k4
<g/>
.	.	kIx.
</s>