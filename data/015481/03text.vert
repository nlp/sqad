<s>
Domesday	Domesdaa	k1gFnPc1
Book	Booka	k1gFnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Domesday	Domesdaa	k1gFnPc1
<g/>
,	,	kIx,
stránka	stránka	k1gFnSc1
o	o	k7c4
Warwickshire	Warwickshir	k1gInSc5
</s>
<s>
Perokresba	perokresba	k1gFnSc1
Domesday	Domesdaa	k1gFnSc2
Book	Booka	k1gFnPc2
</s>
<s>
Domesday	Domesday	k1gInPc1
Book	Booka	k1gFnPc2
(	(	kIx(
<g/>
také	také	k9
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Domesday	Domesdaa	k1gFnPc1
<g/>
,	,	kIx,
Kniha	kniha	k1gFnSc1
posledního	poslední	k2eAgInSc2d1
soudu	soud	k1gInSc2
nebo	nebo	k8xC
Kniha	kniha	k1gFnSc1
z	z	k7c2
Winchesteru	Winchester	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
záznam	záznam	k1gInSc4
podrobného	podrobný	k2eAgInSc2d1
majetkového	majetkový	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
nařízen	nařídit	k5eAaPmNgInS
Vilémem	Vilém	k1gMnSc7
Dobyvatelem	dobyvatel	k1gMnSc7
a	a	k8xC
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1086	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průzkum	průzkum	k1gInSc1
byl	být	k5eAaImAgInS
podobný	podobný	k2eAgInSc1d1
dnešnímu	dnešní	k2eAgInSc3d1
sčítání	sčítání	k1gNnSc4
lidu	lid	k1gInSc2
a	a	k8xC
Domesday	Domesdaa	k1gFnSc2
je	být	k5eAaImIp3nS
seznam	seznam	k1gInSc4
panství	panství	k1gNnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
s	s	k7c7
počty	počet	k1gInPc7
domů	dům	k1gInPc2
a	a	k8xC
hospodářství	hospodářství	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
jejich	jejich	k3xOp3gNnSc2
ocenění	ocenění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vilém	Vilém	k1gMnSc1
potřeboval	potřebovat	k5eAaImAgMnS
informace	informace	k1gFnPc4
o	o	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
právě	právě	k6eAd1
dobyl	dobýt	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
mohl	moct	k5eAaImAgMnS
dobře	dobře	k6eAd1
spravovat	spravovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
průzkumu	průzkum	k1gInSc2
bylo	být	k5eAaImAgNnS
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
vlastní	vlastnit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
aby	aby	kYmCp3nS
tak	tak	k6eAd1
majetek	majetek	k1gInSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
zdaněn	zdanit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záznamy	záznam	k1gInPc1
knihy	kniha	k1gFnSc2
byly	být	k5eAaImAgInP
konečné	konečný	k2eAgInPc1d1
–	–	k?
cokoliv	cokoliv	k3yInSc1
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
o	o	k7c6
majiteli	majitel	k1gMnSc6
či	či	k8xC
hodnotě	hodnota	k1gFnSc6
majetku	majetek	k1gInSc2
bylo	být	k5eAaImAgNnS
nezpochybnitelným	zpochybnitelný	k2eNgInSc7d1
zákonem	zákon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
napsána	napsat	k5eAaBmNgFnS,k5eAaPmNgFnS
latinsky	latinsky	k6eAd1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
byly	být	k5eAaImAgFnP
použity	použít	k5eAaPmNgFnP
i	i	k9
některé	některý	k3yIgInPc1
místní	místní	k2eAgInPc1d1
názvy	název	k1gInPc1
bez	bez	k7c2
latinských	latinský	k2eAgInPc2d1
ekvivalentů	ekvivalent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
psána	psát	k5eAaImNgFnS
velmi	velmi	k6eAd1
úhledným	úhledný	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
text	text	k1gInSc1
je	být	k5eAaImIp3nS
plný	plný	k2eAgInSc1d1
zkratek	zkratka	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
obtížně	obtížně	k6eAd1
čte	číst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
„	„	k?
<g/>
Domesday	Domesdaa	k1gMnSc2
<g/>
“	“	k?
znamená	znamenat	k5eAaImIp3nS
Poslední	poslední	k2eAgInSc1d1
soud	soud	k1gInSc1
a	a	k8xC
kronikář	kronikář	k1gMnSc1
R.	R.	kA
FitzNigel	FitzNigel	k1gMnSc1
roku	rok	k1gInSc2
1179	#num#	k4
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tak	tak	k6eAd1
knize	kniha	k1gFnSc3
říkalo	říkat	k5eAaImAgNnS
proto	proto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gNnSc4
ustanovení	ustanovení	k1gNnSc4
byla	být	k5eAaImAgFnS
definitivní	definitivní	k2eAgFnSc1d1
a	a	k8xC
nezměnitelná	změnitelný	k2eNgFnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
u	u	k7c2
Posledního	poslední	k2eAgInSc2d1
soudu	soud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
knihy	kniha	k1gFnSc2
</s>
<s>
Domesday	Domesdaa	k1gFnPc1
Book	Booka	k1gFnPc2
se	se	k3xPyFc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
samostatných	samostatný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Malá	malý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Little	Little	k1gFnSc1
<g/>
)	)	kIx)
Domesday	Domesday	k1gInPc1
<g/>
,	,	kIx,
pokrývá	pokrývat	k5eAaImIp3nS
Norfolk	Norfolk	k1gInSc1
<g/>
,	,	kIx,
Suffolk	Suffolk	k1gInSc1
a	a	k8xC
Essex	Essex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgNnPc1
<g/>
,	,	kIx,
Velká	velká	k1gFnSc1
(	(	kIx(
<g/>
Great	Great	k1gInSc1
<g/>
)	)	kIx)
Domesday	Domesdaa	k1gFnPc1
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
zbytek	zbytek	k1gInSc1
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
území	území	k1gNnSc2
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
známých	známá	k1gFnPc2
jako	jako	k8xC,k8xS
Westmorland	Westmorlanda	k1gFnPc2
<g/>
,	,	kIx,
Cumberland	Cumberlanda	k1gFnPc2
<g/>
,	,	kIx,
Northumberland	Northumberlanda	k1gFnPc2
a	a	k8xC
hrabství	hrabství	k1gNnSc4
Durham	Durham	k1gInSc1
(	(	kIx(
<g/>
protože	protože	k8xS
některé	některý	k3yIgFnPc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
oblastí	oblast	k1gFnPc2
byly	být	k5eAaImAgInP
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Skotska	Skotsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průzkum	průzkum	k1gInSc1
také	také	k6eAd1
nebyl	být	k5eNaImAgInS
proveden	provést	k5eAaPmNgInS
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
Winchesteru	Winchester	k1gInSc6
a	a	k8xC
některých	některý	k3yIgNnPc6
dalších	další	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc4
dvě	dva	k4xCgNnPc4
velká	velký	k2eAgNnPc4d1
města	město	k1gNnPc4
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
vynechána	vynechán	k2eAgFnSc1d1
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
velikosti	velikost	k1gFnSc3
<g/>
,	,	kIx,
Cumberland	Cumberland	k1gInSc1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgInS
dobyt	dobýt	k5eAaPmNgInS
až	až	k9
několik	několik	k4yIc4
let	léto	k1gNnPc2
po	po	k7c6
průzkumu	průzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výlučné	výlučný	k2eAgFnPc4d1
právo	právo	k1gNnSc1
vybírat	vybírat	k5eAaImF
daně	daň	k1gFnPc4
v	v	k7c6
Durhamu	Durham	k1gInSc6
měl	mít	k5eAaImAgMnS
princ	princ	k1gMnSc1
biskup	biskup	k1gMnSc1
William	William	k1gInSc4
ze	z	k7c2
St.	st.	kA
Carilef	Carilef	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynechání	vynechání	k1gNnSc1
dalších	další	k2eAgFnPc2d1
částí	část	k1gFnPc2
dosud	dosud	k6eAd1
nebylo	být	k5eNaImAgNnS
uspokojivě	uspokojivě	k6eAd1
vysvětleno	vysvětlen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
severní	severní	k2eAgFnSc2d1
Anglie	Anglie	k1gFnSc2
byla	být	k5eAaImAgFnS
prozkoumána	prozkoumán	k2eAgFnSc1d1
roku	rok	k1gInSc2
1183	#num#	k4
v	v	k7c6
„	„	k?
<g/>
Boldon	Boldon	k1gMnSc1
Book	Book	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zaznamenává	zaznamenávat	k5eAaImIp3nS
oblast	oblast	k1gFnSc4
zdaňovanou	zdaňovaný	k2eAgFnSc4d1
biskupem	biskup	k1gInSc7
z	z	k7c2
Durhamu	Durham	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
svému	svůj	k3xOyFgNnSc3
jménu	jméno	k1gNnSc3
je	být	k5eAaImIp3nS
Malá	Malá	k1gFnSc1
Domesday	Domesdaa	k1gFnSc2
vlastně	vlastně	k9
větší	veliký	k2eAgInSc1d2
–	–	k?
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
detailnější	detailní	k2eAgNnSc1d2
<g/>
,	,	kIx,
zmiňuje	zmiňovat	k5eAaImIp3nS
až	až	k6eAd1
počty	počet	k1gInPc4
kusů	kus	k1gInPc2
dobytka	dobytek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
badatelů	badatel	k1gMnPc2
vznikla	vzniknout	k5eAaPmAgFnS
Malá	Malá	k1gFnSc1
Domesday	Domesdaa	k1gFnSc2
jako	jako	k9
první	první	k4xOgNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
být	být	k5eAaImF
nemožným	nemožná	k1gFnPc3
dokončit	dokončit	k5eAaPmF
i	i	k9
Velkou	velký	k2eAgFnSc4d1
Domesday	Domesdaa	k1gFnPc4
na	na	k7c4
stejně	stejně	k6eAd1
detailní	detailní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Záznamy	záznam	k1gInPc1
v	v	k7c6
obou	dva	k4xCgInPc6
svazcích	svazek	k1gInPc6
nebyly	být	k5eNaImAgFnP
řazeny	řadit	k5eAaImNgFnP
geograficky	geograficky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
podle	podle	k7c2
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
určitému	určitý	k2eAgNnSc3d1
lénu	léno	k1gNnSc3
či	či	k8xC
panství	panství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemky	pozemek	k1gInPc7
se	se	k3xPyFc4
tak	tak	k6eAd1
řadily	řadit	k5eAaImAgFnP
k	k	k7c3
místnímu	místní	k2eAgMnSc3d1
baronovi	baron	k1gMnSc3
nebo	nebo	k8xC
jiné	jiný	k2eAgFnSc3d1
osobě	osoba	k1gFnSc3
či	či	k8xC
instituci	instituce	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
půdu	půda	k1gFnSc4
udělenou	udělený	k2eAgFnSc4d1
přímo	přímo	k6eAd1
od	od	k7c2
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
pro	pro	k7c4
každé	každý	k3xTgNnSc4
hrabství	hrabství	k1gNnSc4
vždy	vždy	k6eAd1
začíná	začínat	k5eAaImIp3nS
majetky	majetek	k1gInPc4
samého	samý	k3xTgMnSc4
krále	král	k1gMnSc2
<g/>
,	,	kIx,
následují	následovat	k5eAaImIp3nP
majetky	majetek	k1gInPc4
církevní	církevní	k2eAgInPc4d1
a	a	k8xC
klášterní	klášterní	k2eAgInPc4d1
<g/>
,	,	kIx,
po	po	k7c6
nich	on	k3xPp3gFnPc6
hlavní	hlavní	k2eAgMnSc1d1
nájemci	nájemce	k1gMnPc1
(	(	kIx(
<g/>
baroni	baron	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
nakonec	nakonec	k9
několik	několik	k4yIc1
málo	málo	k4c1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
také	také	k9
držely	držet	k5eAaImAgFnP
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
některých	některý	k3yIgNnPc2
hrabství	hrabství	k1gNnPc2
byl	být	k5eAaImAgInS
pro	pro	k7c4
jedno	jeden	k4xCgNnSc4
či	či	k8xC
několik	několik	k4yIc4
důležitých	důležitý	k2eAgNnPc2d1
měst	město	k1gNnPc2
vytvořen	vytvořit	k5eAaPmNgInS
zvláštní	zvláštní	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
pro	pro	k7c4
sporné	sporný	k2eAgInPc4d1
nároky	nárok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
pravidla	pravidlo	k1gNnPc1
byla	být	k5eAaImAgNnP
více	hodně	k6eAd2
uplatněna	uplatnit	k5eAaPmNgNnP
u	u	k7c2
většího	veliký	k2eAgInSc2d2
svazku	svazek	k1gInSc2
<g/>
,	,	kIx,
systém	systém	k1gInSc1
u	u	k7c2
Malé	Malé	k2eAgFnSc2d1
Domesday	Domesdaa	k1gFnSc2
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
precizní	precizní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Domesday	Domesdaa	k1gFnPc4
pokrývá	pokrývat	k5eAaImIp3nS
kromě	kromě	k7c2
celého	celý	k2eAgInSc2d1
venkova	venkov	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
tvoří	tvořit	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
objem	objem	k1gInSc1
záznamů	záznam	k1gInPc2
<g/>
,	,	kIx,
i	i	k8xC
většinu	většina	k1gFnSc4
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
měst	město	k1gNnPc2
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
některá	některý	k3yIgNnPc4
zvyková	zvykový	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
povinnosti	povinnost	k1gFnPc4
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
práv	právo	k1gNnPc2
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
ražení	ražení	k1gNnSc2
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
atd.	atd.	kA
</s>
<s>
Průzkum	průzkum	k1gInSc1
</s>
<s>
Anglosaská	anglosaský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
plánování	plánování	k1gNnSc1
průzkumu	průzkum	k1gInSc2
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1085	#num#	k4
a	a	k8xC
podle	podle	k7c2
údajů	údaj	k1gInPc2
v	v	k7c4
Domesday	Domesday	k1gInPc4
Book	Booka	k1gFnPc2
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přesně	přesně	k6eAd1
byla	být	k5eAaImAgNnP
data	datum	k1gNnPc1
zpracována	zpracován	k2eAgNnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
Velká	velká	k1gFnSc1
Domesday	Domesdaa	k1gFnSc2
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
opsána	opsat	k5eAaPmNgFnS
jedinou	jediný	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Skupina	skupina	k1gFnSc1
královských	královský	k2eAgMnPc2d1
zmocněnců	zmocněnec	k1gMnPc2
(	(	kIx(
<g/>
legátů	legát	k1gMnPc2
<g/>
)	)	kIx)
navštívila	navštívit	k5eAaPmAgFnS
každé	každý	k3xTgNnSc4
hrabství	hrabství	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
vznesly	vznést	k5eAaPmAgFnP
veřejnou	veřejný	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
na	na	k7c6
velkém	velký	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
za	za	k7c2
účasti	účast	k1gFnSc2
představitelů	představitel	k1gMnPc2
všech	všecek	k3xTgNnPc2
měst	město	k1gNnPc2
a	a	k8xC
všech	všecek	k3xTgMnPc2
místních	místní	k2eAgMnPc2d1
šlechticů	šlechtic	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
odevzdání	odevzdání	k1gNnSc4
výsledků	výsledek	k1gInPc2
průzkumu	průzkum	k1gInSc2
odpovídalo	odpovídat	k5eAaImAgNnS
dvanáct	dvanáct	k4xCc1
místních	místní	k2eAgMnPc2d1
soudních	soudní	k2eAgMnPc2d1
přísedících	přísedící	k1gMnPc2
za	za	k7c4
každou	každý	k3xTgFnSc4
administrativní	administrativní	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
„	„	k?
<g/>
Hundred	Hundred	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
část	část	k1gFnSc1
hrabství	hrabství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polovina	polovina	k1gFnSc1
zmocněnců	zmocněnec	k1gMnPc2
byli	být	k5eAaImAgMnP
Angličané	Angličan	k1gMnPc1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
polovina	polovina	k1gFnSc1
Normané	Norman	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2
osudy	osud	k1gInPc1
Domesday	Domesdaa	k1gFnSc2
Book	Book	k1gMnSc1
</s>
<s>
Domesday	Domesdaa	k1gFnSc2
Book	Booka	k1gFnPc2
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
uložena	uložit	k5eAaPmNgFnS
v	v	k7c6
královské	královský	k2eAgFnSc6d1
pokladnici	pokladnice	k1gFnSc6
ve	v	k7c6
Winchesteru	Winchester	k1gInSc6
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
normanské	normanský	k2eAgFnSc2d1
Anglie	Anglie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
také	také	k9
název	název	k1gInSc4
Kniha	kniha	k1gFnSc1
z	z	k7c2
Winchesteru	Winchester	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
pokladnice	pokladnice	k1gFnSc1
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
Westminsteru	Westminster	k1gInSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
za	za	k7c2
vlády	vláda	k1gFnSc2
Jindřicha	Jindřich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
také	také	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
byly	být	k5eAaImAgInP
její	její	k3xOp3gInPc1
záznamy	záznam	k1gInPc1
běžně	běžně	k6eAd1
používány	používat	k5eAaImNgInP
jako	jako	k9
podklady	podklad	k1gInPc4
soudních	soudní	k2eAgFnPc2d1
pří	pře	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
dnes	dnes	k6eAd1
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgInPc6
soudních	soudní	k2eAgInPc6d1
případech	případ	k1gInPc6
používána	používat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faksimile	faksimile	k1gNnSc7
Domesday	Domesdaa	k1gFnSc2
Book	Booka	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1862	#num#	k4
bylo	být	k5eAaImAgNnS
první	první	k4xOgFnSc6
takovou	takový	k3xDgFnSc7
reprodukcí	reprodukce	k1gFnSc7
celé	celý	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Westminsteru	Westminster	k1gInSc6
zůstala	zůstat	k5eAaPmAgFnS
kniha	kniha	k1gFnSc1
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
vlády	vláda	k1gFnSc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
přesunech	přesun	k1gInPc6
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
umístěna	umístit	k5eAaPmNgFnS
ve	v	k7c6
vitríně	vitrína	k1gFnSc6
muzea	muzeum	k1gNnSc2
Národního	národní	k2eAgInSc2d1
archivu	archiv	k1gInSc2
v	v	k7c6
Kew	Kew	k1gFnSc6
<g/>
,	,	kIx,
jihozápadním	jihozápadní	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1986	#num#	k4
byla	být	k5eAaImAgFnS
kniha	kniha	k1gFnSc1
znovu	znovu	k6eAd1
svázána	svázat	k5eAaPmNgFnS
do	do	k7c2
dvou	dva	k4xCgInPc2
a	a	k8xC
tří	tři	k4xCgInPc2
svazků	svazek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Domesday	Domesdaa	k1gFnSc2
Book	Book	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Domesday	Domesdaa	k1gFnSc2
Book	Booka	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Scan	Scan	k1gInSc1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
vyhledávání	vyhledávání	k1gNnSc2
a	a	k8xC
mapky	mapka	k1gFnSc2
</s>
<s>
Domesday	Domesdaa	k1gFnPc1
Book	Booka	k1gFnPc2
<g/>
,	,	kIx,
kompletní	kompletní	k2eAgFnSc1d1
textová	textový	k2eAgFnSc1d1
a	a	k8xC
rastrová	rastrový	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
z	z	k7c2
Národního	národní	k2eAgInSc2d1
archivu	archiv	k1gInSc2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
plnému	plný	k2eAgInSc3d1
textu	text	k1gInSc3
je	být	k5eAaImIp3nS
za	za	k7c4
poplatek	poplatek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4150402-1	4150402-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81063021	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
174988839	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81063021	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
