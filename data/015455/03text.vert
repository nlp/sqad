<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
(	(	kIx(
<g/>
mošav	mošav	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
ב	ב	k?
ג	ג	k?
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Bar	bar	k1gInSc4
GioraPoloha	GioraPoloh	k1gMnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
701	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgMnSc1d1
oblastní	oblastní	k2eAgMnSc1d1
rada	rada	k1gMnSc1
</s>
<s>
Mate	mást	k5eAaImIp3nS
Jehuda	Jehuda	k1gMnSc1
</s>
<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
623	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1950	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Židé	Žid	k1gMnPc1
z	z	k7c2
Jemenu	Jemen	k1gInSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ב	ב	k?
<g/>
ַ	ַ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
ג	ג	k?
<g/>
ִ	ִ	k?
<g/>
ּ	ּ	k?
<g/>
י	י	k?
<g/>
ּ	ּ	k?
<g/>
ו	ו	k?
<g/>
ֹ	ֹ	k?
<g/>
ר	ר	k?
<g/>
ָ	ָ	k?
<g/>
א	א	k?
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Bar	bar	k1gInSc1
Giyyora	Giyyora	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
typu	typ	k1gInSc2
mošav	mošava	k1gFnPc2
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Jeruzalémském	jeruzalémský	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Oblastní	oblastní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
Mate	mást	k5eAaImIp3nS
Jehuda	Jehuda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
701	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
zalesněných	zalesněný	k2eAgInPc6d1
svazích	svah	k1gInPc6
Judských	judský	k2eAgMnPc2d1
hor.	hor.	k?
Jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
lokalitě	lokalita	k1gFnSc6
Ejn	Ejn	k1gMnSc1
Azen	Azen	k1gMnSc1
vádí	vádí	k1gNnSc2
Nachal	Nachal	k1gMnSc1
Azen	Azen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západně	západně	k6eAd1
od	od	k7c2
vesnice	vesnice	k1gFnSc2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
vádí	vádí	k1gNnSc4
Nachal	Nachal	k1gMnSc1
ha-Me	ha-M	k1gInSc2
<g/>
'	'	kIx"
<g/>
ara	ara	k1gMnSc1
a	a	k8xC
Nachal	Nachal	k1gMnSc1
Dolev	Dolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severovýchodně	severovýchodně	k6eAd1
odtud	odtud	k6eAd1
počíná	počínat	k5eAaImIp3nS
vádí	vádí	k1gNnSc4
Nachal	Nachal	k1gMnSc1
Ktalav	Ktalav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
terén	terén	k1gInSc1
prudce	prudko	k6eAd1
klesá	klesat	k5eAaImIp3nS
do	do	k7c2
kaňonu	kaňon	k1gInSc2
vádí	vádí	k1gNnSc2
Nachal	Nachal	k1gMnSc1
Refa	Refa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
im	im	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okraj	okraj	k1gInSc4
kaňonu	kaňon	k1gInSc2
lemují	lemovat	k5eAaImIp3nP
vyrazné	vyrazný	k2eAgInPc1d1
kopce	kopec	k1gInPc1
jako	jako	k8xS,k8xC
Har	Har	k1gFnSc1
Giora	Giora	k1gFnSc1
<g/>
,	,	kIx,
podobné	podobný	k2eAgFnPc1d1
vyvýšeniny	vyvýšenina	k1gFnPc1
se	se	k3xPyFc4
zvedají	zvedat	k5eAaImIp3nP
i	i	k9
na	na	k7c6
protější	protější	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
Har	Har	k1gMnSc1
Pitulim	Pitulim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
41	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
břehu	břeh	k1gInSc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
cca	cca	kA
47	#num#	k4
kilometrů	kilometr	k1gInPc2
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Tel	tel	kA
Avivu	Aviva	k1gFnSc4
<g/>
,	,	kIx,
cca	cca	kA
16	#num#	k4
kilometrů	kilometr	k1gInPc2
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
historického	historický	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
Jeruzalému	Jeruzalém	k1gInSc2
a	a	k8xC
7	#num#	k4
kilometrů	kilometr	k1gInPc2
východně	východně	k6eAd1
od	od	k7c2
Bejt	Bejt	k?
Šemeš	Šemeš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bar	bar	k1gInSc4
Giora	Gior	k1gInSc2
obývají	obývat	k5eAaImIp3nP
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
je	být	k5eAaImIp3nS
etnicky	etnicky	k6eAd1
převážně	převážně	k6eAd1
židovské	židovský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mošav	Mošav	k1gInSc1
je	být	k5eAaImIp3nS
situován	situovat	k5eAaBmNgInS
4	#num#	k4
kilometry	kilometr	k1gInPc7
od	od	k7c2
Zelené	Zelené	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
odděluje	oddělovat	k5eAaImIp3nS
Izrael	Izrael	k1gInSc4
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
mezinárodně	mezinárodně	k6eAd1
uznávaných	uznávaný	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
od	od	k7c2
území	území	k1gNnSc2
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Jordánu	Jordán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgInP
přilehlé	přilehlý	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
(	(	kIx(
<g/>
palestinské	palestinský	k2eAgInPc1d1
<g/>
)	)	kIx)
oblastí	oblast	k1gFnSc7
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
od	od	k7c2
Izraele	Izrael	k1gInSc2
odděleny	oddělit	k5eAaPmNgFnP
pomocí	pomocí	k7c2
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
bariéry	bariéra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
Zelenou	zelený	k2eAgFnSc7d1
linií	linie	k1gFnSc7
se	se	k3xPyFc4
ale	ale	k9
v	v	k7c6
přilehlé	přilehlý	k2eAgFnSc6d1
části	část	k1gFnSc6
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
i	i	k9
kompaktní	kompaktní	k2eAgInSc4d1
blok	blok	k1gInSc4
židovských	židovský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
Guš	Guš	k1gMnSc1
Ecion	Ecion	k1gMnSc1
včetně	včetně	k7c2
velkého	velký	k2eAgNnSc2d1
města	město	k1gNnSc2
Bejtar	Bejtar	k1gMnSc1
Ilit	Ilit	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c4
dopravní	dopravní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
napojen	napojen	k2eAgMnSc1d1
pomocí	pomocí	k7c2
lokální	lokální	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
číslo	číslo	k1gNnSc1
386	#num#	k4
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
tu	tu	k6eAd1
k	k	k7c3
západu	západ	k1gInSc3
odbočuje	odbočovat	k5eAaImIp3nS
lokání	lokání	k1gNnSc1
silnice	silnice	k1gFnSc1
číslo	číslo	k1gNnSc1
3866	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
Nachal	Nachal	k1gMnSc1
Refa	Refus	k1gMnSc2
<g/>
'	'	kIx"
<g/>
im	im	k?
vede	vést	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Tel	tel	kA
Aviv-Jeruzalém	Aviv-Jeruzalý	k2eAgInSc6d1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
zde	zde	k6eAd1
ale	ale	k8xC
nemá	mít	k5eNaImIp3nS
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Novověké	novověký	k2eAgNnSc1d1
židovské	židovský	k2eAgNnSc1d1
osidlování	osidlování	k1gNnSc1
tohoto	tento	k3xDgInSc2
regionu	region	k1gInSc2
začalo	začít	k5eAaPmAgNnS
po	po	k7c6
válce	válka	k1gFnSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
tedy	tedy	k9
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Jeruzalémský	jeruzalémský	k2eAgInSc4d1
koridor	koridor	k1gInSc4
ovládla	ovládnout	k5eAaPmAgFnS
izraelská	izraelský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
a	a	k8xC
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vysídlení	vysídlení	k1gNnSc3
většiny	většina	k1gFnSc2
zdejší	zdejší	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
se	se	k3xPyFc4
cca	cca	kA
jeden	jeden	k4xCgInSc4
kilometr	kilometr	k1gInSc4
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
nynějšího	nynější	k2eAgMnSc2d1
mošavu	mošavat	k5eAaPmIp1nS
rozkládala	rozkládat	k5eAaImAgFnS
arabská	arabský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Allar	Allara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stávala	stávat	k5eAaImAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
patrně	patrně	k6eAd1
mešita	mešita	k1gFnSc1
a	a	k8xC
muslimská	muslimský	k2eAgFnSc1d1
svatyně	svatyně	k1gFnSc1
al-Šajch	al-Šajch	k1gMnSc1
Ibrahim	Ibrahim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pitnou	pitný	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
zajišťoval	zajišťovat	k5eAaImAgMnS
pramen	pramen	k1gInSc4
Ajn	Ajn	k1gMnSc1
al-Tanur	al-Tanur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1945	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c4
Allar	Allar	k1gInSc4
440	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelci	Izraelec	k1gMnSc6
byl	být	k5eAaImAgInS
Allar	Allar	k1gInSc1
dobyt	dobýt	k5eAaPmNgInS
v	v	k7c6
říjnu	říjen	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástavba	zástavba	k1gFnSc1
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
zbořena	zbořen	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
budovy	budova	k1gFnSc2
školy	škola	k1gFnSc2
a	a	k8xC
mešity	mešita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
ještě	ještě	k6eAd1
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
provizorně	provizorně	k6eAd1
přebývali	přebývat	k5eAaImAgMnP
v	v	k7c6
okolní	okolní	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
postupně	postupně	k6eAd1
byli	být	k5eAaImAgMnP
vypuzeni	vypuzen	k2eAgMnPc1d1
při	při	k7c6
finálním	finální	k2eAgNnSc6d1
přebírání	přebírání	k1gNnSc6
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
tímto	tento	k3xDgInSc7
regionem	region	k1gInSc7
izraelskými	izraelský	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mošav	Mošav	k1gInSc1
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc4
zakladateli	zakladatel	k1gMnSc3
byla	být	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
Židů	Žid	k1gMnPc2
z	z	k7c2
Jemenu	Jemen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
provizorní	provizorní	k2eAgInSc4d1
pracovní	pracovní	k2eAgInSc4d1
tábor	tábor	k1gInSc4
zvaný	zvaný	k2eAgInSc1d1
Ejtanim	Ejtanim	k1gInSc1
(	(	kIx(
<g/>
א	א	k?
<g/>
)	)	kIx)
zbudovaný	zbudovaný	k2eAgInSc1d1
v	v	k7c6
prostoru	prostor	k1gInSc6
bývalého	bývalý	k2eAgNnSc2d1
rekreačního	rekreační	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
z	z	k7c2
doby	doba	k1gFnSc2
britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1954	#num#	k4
se	se	k3xPyFc4
proměnil	proměnit	k5eAaPmAgInS
na	na	k7c4
trvalé	trvalý	k2eAgNnSc4d1
civilní	civilní	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
napojené	napojený	k2eAgNnSc4d1
na	na	k7c4
osadnickou	osadnický	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
Miškej	Miškej	k1gMnSc1
Cherut	Cherut	k1gMnSc1
Betar	Betar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesnice	vesnice	k1gFnSc1
v	v	k7c6
současnosti	současnost	k1gFnSc6
prochází	procházet	k5eAaImIp3nS
stavební	stavební	k2eAgFnSc7d1
expanzí	expanze	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Západně	západně	k6eAd1
od	od	k7c2
vesnice	vesnice	k1gFnSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Bejt	Bejt	k?
Ita	Ita	k1gFnSc1
<g/>
'	'	kIx"
<g/>
b.	b.	k?
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
pojmenována	pojmenován	k2eAgFnSc1d1
podle	podle	k7c2
starověkého	starověký	k2eAgMnSc2d1
židovského	židovský	k2eAgMnSc2d1
válečníka	válečník	k1gMnSc2
Šimona	Šimona	k1gFnSc1
bar	bar	k1gInSc1
Giory	Giora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c4
Bar	bar	k1gInSc4
Giora	Gioro	k1gNnSc2
Židé	Žid	k1gMnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
statistické	statistický	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
"	"	kIx"
<g/>
ostatní	ostatní	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
nearabské	arabský	k2eNgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
ale	ale	k8xC
bez	bez	k7c2
formální	formální	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
židovskému	židovský	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
menší	malý	k2eAgFnSc4d2
obec	obec	k1gFnSc4
vesnického	vesnický	k2eAgInSc2d1
typu	typ	k1gInSc2
s	s	k7c7
dlouhodobě	dlouhodobě	k6eAd1
rostoucí	rostoucí	k2eAgFnSc7d1
populací	populace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2014	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
623	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
populace	populace	k1gFnSc1
klesla	klesnout	k5eAaPmAgFnS
o	o	k7c4
0,2	0,2	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
Bar	bar	k1gInSc1
Giora	Giora	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1961	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
179232269299346358378397414419438573590567607624623	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
י	י	k?
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
י	י	k?
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Separation	Separation	k1gInSc1
Barrier	Barrier	k1gMnSc1
Map	mapa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
<g/>
´	´	k?
<g/>
Tselem	Tsel	k1gInSc7
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Welcome	Welcom	k1gInSc5
To	ten	k3xDgNnSc1
'	'	kIx"
<g/>
Allar	Allar	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestine	Palestin	k1gInSc5
Remembered	Remembered	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ב	ב	k?
ג	ג	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
m-yehuda	m-yehuda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
il	il	k?
<g/>
/	/	kIx~
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ר	ר	k?
ה	ה	k?
<g/>
,	,	kIx,
מ	מ	k?
ג	ג	k?
ו	ו	k?
1948,1961	1948,1961	k4
<g/>
,1972	,1972	k4
<g/>
,1983	,1983	k4
<g/>
,	,	kIx,
1995	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ש	ש	k?
י	י	k?
א	א	k?
a	a	k8xC
další	další	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
demografického	demografický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
sídel	sídlo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
mošav	mošav	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bar	bar	k1gInSc1
Giora	Gioro	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Oblastní	oblastní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Mate	mást	k5eAaImIp3nS
Jehuda	Jehuda	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oblastní	oblastní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Mate	mást	k5eAaImIp3nS
Jehuda	Jehuda	k1gFnSc1
Kibucy	kibuc	k1gInPc4
</s>
<s>
Cor	Cor	k?
<g/>
'	'	kIx"
<g/>
a	a	k8xC
</s>
<s>
Cova	Cova	k6eAd1
</s>
<s>
Har	Har	k?
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Kirjat	Kirjat	k2eAgInSc1d1
Anavim	Anavim	k1gInSc1
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
ale	ale	k8xC
ha-Chamiša	ha-Chamiša	k6eAd1
</s>
<s>
Nachšon	Nachšon	k1gMnSc1
</s>
<s>
Netiv	Netiv	k6eAd1
ha-Lamed	ha-Lamed	k1gInSc1
He	he	k0
</s>
<s>
Ramat	Ramat	k2eAgInSc1d1
Rachel	Rachel	k1gInSc1
Mošavy	Mošava	k1gFnSc2
</s>
<s>
Aderet	Aderet	k1gMnSc1
</s>
<s>
Agur	Agur	k1gMnSc1
</s>
<s>
Aminadav	Aminadat	k5eAaPmDgInS
</s>
<s>
Avi	Avi	k?
<g/>
'	'	kIx"
<g/>
ezer	ezer	k1gMnSc1
</s>
<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
</s>
<s>
Bejt	Bejt	k?
Zajit	zajit	k2eAgMnSc1d1
</s>
<s>
Bejt	Bejt	k?
Me	Me	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ir	ir	k?
</s>
<s>
Bejt	Bejt	k?
Nekofa	Nekof	k1gMnSc2
</s>
<s>
Beko	Beko	k1gNnSc1
<g/>
'	'	kIx"
<g/>
a	a	k8xC
</s>
<s>
Cafririm	Cafririm	k6eAd1
</s>
<s>
Celafon	Celafon	k1gInSc1
</s>
<s>
Ešta	Ešta	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ol	ol	k?
</s>
<s>
Even	Even	k1gMnSc1
Sapir	Sapir	k1gMnSc1
</s>
<s>
Gefen	Gefen	k1gInSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Je	být	k5eAaImIp3nS
<g/>
'	'	kIx"
<g/>
arim	arim	k6eAd1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Ješa	Ješa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
jahu	jahu	k5eAaPmIp1nS
</s>
<s>
Jad	Jad	k?
ha-Šmona	ha-Šmona	k1gFnSc1
</s>
<s>
Jiš	Jiš	k?
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Kfar	Kfar	k1gMnSc1
Urija	Urija	k1gMnSc1
</s>
<s>
Ksalon	Ksalon	k1gInSc1
</s>
<s>
Luzit	Luzit	k1gMnSc1
</s>
<s>
Mevo	Mevo	k1gMnSc1
Bejtar	Bejtar	k1gMnSc1
</s>
<s>
Mata	mást	k5eAaImSgMnS
</s>
<s>
Machseja	Machseja	k6eAd1
</s>
<s>
Mesilat	Mesilat	k5eAaPmF,k5eAaImF
Cijon	Cijon	k1gNnSc4
</s>
<s>
Nacham	Nacham	k6eAd1
</s>
<s>
Nechuša	Nechuša	k6eAd1
</s>
<s>
Nes	nést	k5eAaImRp2nS
Harim	Harim	k1gInPc3
</s>
<s>
Neve	Neve	k1gFnSc1
Ilan	Ilana	k1gFnPc2
</s>
<s>
Neve	Neve	k1gFnSc1
Micha	Micha	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Ora	Ora	k?
</s>
<s>
Ramat	Ramat	k1gInSc1
Razi	Razi	k1gNnSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Roglit	Roglit	k1gInSc1
(	(	kIx(
<g/>
sloučen	sloučen	k2eAgInSc1d1
s	s	k7c7
Neve	Neve	k1gFnSc7
Micha	Micha	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Sdot	Sdot	k1gMnSc1
Micha	Micha	k1gMnSc1
</s>
<s>
Šo	Šo	k?
<g/>
'	'	kIx"
<g/>
eva	eva	k?
</s>
<s>
Šoreš	Šorat	k5eAaPmIp2nS,k5eAaBmIp2nS
</s>
<s>
Ta	ten	k3xDgFnSc1
<g/>
'	'	kIx"
<g/>
oz	oz	k?
</s>
<s>
Tal	Tal	k?
Šachar	Šachar	k1gInSc1
</s>
<s>
Tarum	Tarum	k1gInSc1
</s>
<s>
Tiroš	Tiroš	k1gMnSc1
</s>
<s>
Zanoach	Zanoach	k1gMnSc1
</s>
<s>
Zecharja	Zecharja	k6eAd1
Společné	společný	k2eAgFnPc1d1
osady	osada	k1gFnPc1
</s>
<s>
Cur	Cur	k?
Hadasa	Hadasa	k1gFnSc1
</s>
<s>
Gizo	Gizo	k6eAd1
</s>
<s>
Nataf	Nataf	k1gMnSc1
</s>
<s>
Neve	Neve	k6eAd1
Šalom	šalom	k0
</s>
<s>
Moca	Moca	k1gMnSc1
Ilit	Ilit	k1gMnSc1
</s>
<s>
Srigim	Srigim	k1gInSc1
Arabské	arabský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
</s>
<s>
Ajn	Ajn	k?
Nakuba	Nakuba	k1gFnSc1
</s>
<s>
Ajn	Ajn	k?
Rafa	Raf	k1gInSc2
Další	další	k2eAgNnPc1d1
sídla	sídlo	k1gNnPc1
(	(	kIx(
<g/>
školy	škola	k1gFnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
klášter	klášter	k1gInSc1
Dejr	Dejra	k1gFnPc2
Rafat	rafat	k5eAaImF
</s>
<s>
Ejtanim	Ejtanim	k6eAd1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Šemeš	Šemeš	k1gFnSc1
</s>
<s>
Jedida	Jedida	k1gFnSc1
</s>
<s>
Kfar	Kfar	k1gMnSc1
Zoharim	Zoharim	k1gMnSc1
</s>
<s>
mládežnická	mládežnický	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Kirjat	Kirjat	k1gInSc1
Je	být	k5eAaImIp3nS
<g/>
'	'	kIx"
<g/>
arim	arim	k6eAd1
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Ejn	Ejn	k1gFnSc2
Kerem	Kerem	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
