<s>
Bar	bar	k1gInSc1
Giora	Gior	k1gInSc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ב	ב	k?
<g/>
ַ	ַ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
ג	ג	k?
<g/>
ִ	ִ	k?
<g/>
ּ	ּ	k?
<g/>
י	י	k?
<g/>
ּ	ּ	k?
<g/>
ו	ו	k?
<g/>
ֹ	ֹ	k?
<g/>
ר	ר	k?
<g/>
ָ	ָ	k?
<g/>
א	א	k?
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Bar	bar	k1gInSc1
Giyyora	Giyyora	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
typu	typ	k1gInSc2
mošav	mošava	k1gFnPc2
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Jeruzalémském	jeruzalémský	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Oblastní	oblastní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
Mate	mást	k5eAaImIp3nS
Jehuda	Jehuda	k1gMnSc1
<g/>
.	.	kIx.
</s>