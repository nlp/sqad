<s>
Afanasij	Afanasít	k5eAaPmRp2nS	Afanasít
Afanasjevič	Afanasjevič	k1gMnSc1	Afanasjevič
Fet	Fet	k1gMnSc1	Fet
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
А	А	k?	А
А	А	k?	А
Ф	Ф	k?	Ф
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1820	[number]	k4	1820
poblíž	poblíž	k7c2	poblíž
Mcenska	Mcensko	k1gNnSc2	Mcensko
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1892	[number]	k4	1892
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgMnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Šenšin	Šenšin	k2eAgInSc4d1	Šenšin
(	(	kIx(	(
<g/>
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
postav	postava	k1gFnPc2	postava
ruské	ruský	k2eAgFnSc2d1	ruská
poezie	poezie	k1gFnSc2	poezie
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Afanasij	Afanasít	k5eAaPmRp2nS	Afanasít
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Němky	Němka	k1gFnSc2	Němka
jménem	jméno	k1gNnSc7	jméno
Charlotta	Charlotta	k1gFnSc1	Charlotta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c4	za
Johanna	Johann	k1gMnSc4	Johann
Feta	Fetus	k1gMnSc4	Fetus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c4	po
narození	narození	k1gNnSc4	narození
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
znovu	znovu	k6eAd1	znovu
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
bohatého	bohatý	k2eAgMnSc4d1	bohatý
Rusa	Rus	k1gMnSc4	Rus
Šenšina	Šenšin	k2eAgMnSc4d1	Šenšin
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
Afanasij	Afanasij	k1gMnPc2	Afanasij
potomkem	potomek	k1gMnSc7	potomek
Šenšina	Šenšin	k2eAgNnSc2d1	Šenšin
či	či	k8xC	či
Feta	Fetum	k1gNnSc2	Fetum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Svatá	svatý	k2eAgFnSc1d1	svatá
konzistoř	konzistoř	k1gFnSc1	konzistoř
v	v	k7c6	v
Orjolu	Orjol	k1gInSc6	Orjol
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Afanasij	Afanasij	k1gMnSc1	Afanasij
bude	být	k5eAaImBp3nS	být
používat	používat	k5eAaImF	používat
jméno	jméno	k1gNnSc4	jméno
Fet	Fet	k1gFnSc2	Fet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rozsudek	rozsudek	k1gInSc4	rozsudek
Afanasije	Afanasije	k1gMnSc1	Afanasije
velmi	velmi	k6eAd1	velmi
traumatizoval	traumatizovat	k5eAaBmAgMnS	traumatizovat
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
spíš	spíš	k9	spíš
potomkem	potomek	k1gMnSc7	potomek
Šenšina	Šenšin	k2eAgInSc2d1	Šenšin
<g/>
.	.	kIx.	.
</s>
<s>
Stigma	stigma	k1gNnSc1	stigma
nelegitimity	nelegitimita	k1gFnSc2	nelegitimita
ho	on	k3xPp3gNnSc2	on
pronásledovalo	pronásledovat	k5eAaImAgNnS	pronásledovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
mu	on	k3xPp3gInSc3	on
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
změněno	změněn	k2eAgNnSc1d1	změněno
jméno	jméno	k1gNnSc1	jméno
na	na	k7c4	na
Šenšin	Šenšin	k2eAgInSc4d1	Šenšin
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
postup	postup	k1gInSc1	postup
v	v	k7c6	v
armádních	armádní	k2eAgFnPc6d1	armádní
hodnostech	hodnost	k1gFnPc6	hodnost
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
lepší	dobrý	k2eAgNnSc4d2	lepší
přijetí	přijetí	k1gNnSc4	přijetí
ruskou	ruský	k2eAgFnSc7d1	ruská
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Fet	Fet	k?	Fet
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsob	způsob	k1gInSc1	způsob
básníkova	básníkův	k2eAgInSc2d1	básníkův
života	život	k1gInSc2	život
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jeho	jeho	k3xOp3gFnSc4	jeho
poezii	poezie	k1gFnSc4	poezie
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
pohrdání	pohrdání	k1gNnSc6	pohrdání
radikály	radikál	k1gInPc1	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
navázal	navázat	k5eAaPmAgInS	navázat
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Lvem	Lev	k1gMnSc7	Lev
Tolstým	Tolstý	k2eAgMnSc7d1	Tolstý
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
vždy	vždy	k6eAd1	vždy
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
Stěpanovka	Stěpanovka	k1gFnSc1	Stěpanovka
v	v	k7c6	v
Orelské	orelský	k2eAgFnSc6d1	orelská
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
skutečným	skutečný	k2eAgMnSc7d1	skutečný
literátem	literát	k1gMnSc7	literát
mezi	mezi	k7c7	mezi
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
psal	psát	k5eAaImAgInS	psát
své	své	k1gNnSc4	své
literární	literární	k2eAgFnSc2d1	literární
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
překládal	překládat	k5eAaImAgMnS	překládat
Aeneidu	Aeneida	k1gFnSc4	Aeneida
a	a	k8xC	a
Schopenhauerovu	Schopenhauerův	k2eAgFnSc4d1	Schopenhauerova
knihu	kniha	k1gFnSc4	kniha
Svět	svět	k1gInSc1	svět
jako	jako	k8xS	jako
vůle	vůle	k1gFnSc1	vůle
a	a	k8xC	a
představa	představa	k1gFnSc1	představa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
utrpení	utrpení	k1gNnSc1	utrpení
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
neúnosným	únosný	k2eNgMnSc7d1	neúnosný
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
ho	on	k3xPp3gMnSc4	on
zachránila	zachránit	k5eAaPmAgFnS	zachránit
a	a	k8xC	a
během	během	k7c2	během
dalšího	další	k2eAgInSc2d1	další
sebevražedného	sebevražedný	k2eAgInSc2d1	sebevražedný
pokusu	pokus	k1gInSc2	pokus
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
básně	báseň	k1gFnPc4	báseň
Fet	Fet	k1gFnSc3	Fet
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
si	se	k3xPyFc3	se
jist	jist	k2eAgInSc4d1	jist
svými	svůj	k3xOyFgFnPc7	svůj
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
své	svůj	k3xOyFgFnPc4	svůj
básně	báseň	k1gFnPc4	báseň
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
ohodnocení	ohodnocení	k1gNnSc3	ohodnocení
Turgeněvovi	Turgeněva	k1gMnSc3	Turgeněva
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
platil	platit	k5eAaImAgMnS	platit
za	za	k7c4	za
arbitra	arbiter	k1gMnSc4	arbiter
vkusu	vkus	k1gInSc2	vkus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turgeněv	Turgeněv	k1gFnSc1	Turgeněv
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
některé	některý	k3yIgInPc4	některý
osobité	osobitý	k2eAgInPc4d1	osobitý
rysy	rys	k1gInPc4	rys
a	a	k8xC	a
originální	originální	k2eAgInPc4d1	originální
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Fetovy	Fetův	k2eAgFnPc1d1	Fetův
básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
tematicky	tematicky	k6eAd1	tematicky
dost	dost	k6eAd1	dost
konvenční	konvenční	k2eAgFnSc1d1	konvenční
<g/>
:	:	kIx,	:
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
prostá	prostý	k2eAgFnSc1d1	prostá
příroda	příroda	k1gFnSc1	příroda
středního	střední	k2eAgNnSc2d1	střední
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
řecké	řecký	k2eAgFnSc2d1	řecká
sochy	socha	k1gFnSc2	socha
a	a	k8xC	a
vznešenost	vznešenost	k1gFnSc1	vznešenost
Boží	božit	k5eAaImIp3nS	božit
<g/>
.	.	kIx.	.
</s>
<s>
Fet	Fet	k?	Fet
s	s	k7c7	s
těmito	tento	k3xDgNnPc7	tento
tématy	téma	k1gNnPc7	téma
ale	ale	k8xC	ale
pracoval	pracovat	k5eAaImAgInS	pracovat
impresionistickou	impresionistický	k2eAgFnSc7d1	impresionistická
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zachytit	zachytit	k5eAaPmF	zachytit
prchavý	prchavý	k2eAgInSc4d1	prchavý
okamžik	okamžik	k1gInSc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
jménem	jméno	k1gNnSc7	jméno
Šepot	šepot	k1gInSc1	šepot
nepoužil	použít	k5eNaPmAgMnS	použít
ani	ani	k9	ani
jedno	jeden	k4xCgNnSc4	jeden
sloveso	sloveso	k1gNnSc4	sloveso
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
dojmu	dojem	k1gInSc3	dojem
nepokojné	pokojný	k2eNgFnSc2d1	nepokojná
dynamičnosti	dynamičnost	k1gFnSc2	dynamičnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
pozdní	pozdní	k2eAgNnPc4d1	pozdní
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgNnPc4d1	napsané
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Charlese	Charles	k1gMnSc2	Charles
Baudelaira	Baudelair	k1gMnSc2	Baudelair
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
temná	temný	k2eAgNnPc1d1	temné
a	a	k8xC	a
složitá	složitý	k2eAgNnPc1d1	složité
<g/>
,	,	kIx,	,
evokují	evokovat	k5eAaBmIp3nP	evokovat
a	a	k8xC	a
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
jemné	jemný	k2eAgFnPc4d1	jemná
asociace	asociace	k1gFnPc4	asociace
a	a	k8xC	a
polozapomenuté	polozapomenutý	k2eAgFnPc4d1	polozapomenutá
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
věcí	věc	k1gFnSc7	věc
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
je	být	k5eAaImIp3nS	být
nit	nit	k1gFnSc1	nit
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojí	spojit	k5eAaPmIp3nS	spojit
všechny	všechen	k3xTgFnPc4	všechen
neuspořádané	uspořádaný	k2eNgFnPc4d1	neuspořádaná
asociace	asociace	k1gFnPc4	asociace
do	do	k7c2	do
pevně	pevně	k6eAd1	pevně
strukturované	strukturovaný	k2eAgFnSc2d1	strukturovaná
krátké	krátký	k2eAgFnSc2d1	krátká
básně	báseň	k1gFnSc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nebyl	být	k5eNaImAgInS	být
Fet	Fet	k1gFnSc3	Fet
příliš	příliš	k6eAd1	příliš
známý	známý	k2eAgInSc1d1	známý
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ale	ale	k9	ale
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
školu	škola	k1gFnSc4	škola
ruských	ruský	k2eAgMnPc2d1	ruský
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Anněnského	Anněnský	k1gMnSc4	Anněnský
a	a	k8xC	a
Bloka	Bloek	k1gMnSc4	Bloek
<g/>
.	.	kIx.	.
</s>
<s>
Večery	večer	k1gInPc1	večer
a	a	k8xC	a
noci	noc	k1gFnPc1	noc
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Slavík	Slavík	k1gMnSc1	Slavík
Roční	roční	k2eAgFnSc2d1	roční
doby	doba	k1gFnSc2	doba
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kabíček	Kabíček	k1gMnSc1	Kabíček
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Afanasy	Afanasa	k1gFnSc2	Afanasa
Fet	Fet	k1gFnPc2	Fet
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Afanasij	Afanasij	k1gFnSc2	Afanasij
Fet	Fet	k1gFnSc2	Fet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Afanasij	Afanasij	k1gFnSc1	Afanasij
Fet	Fet	k1gFnSc1	Fet
</s>
