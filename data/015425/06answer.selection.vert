<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1823	#num#	k4
Per	Per	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Norstedt	Norstedt	k1gMnSc1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
P.	P.	kA
A.	A.	kA
Norstedt	Norstedt	k1gMnSc1
&	&	k?
Söner	Söner	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
P.	P.	kA
A.	A.	kA
Norstedt	Norstedta	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kořeny	kořen	k1gInPc1
nakladatelství	nakladatelství	k1gNnSc2
jsou	být	k5eAaImIp3nP
však	však	k9
ještě	ještě	k6eAd1
mnohem	mnohem	k6eAd1
starší	starý	k2eAgFnPc1d2
a	a	k8xC
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1526	#num#	k4
<g/>
.	.	kIx.
</s>