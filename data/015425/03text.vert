<s>
Norstedts	Norstedts	k6eAd1
Förlag	Förlag	k1gInSc1
</s>
<s>
Norstedts	Norstedts	k1gInSc1
Förlag	Förlag	k1gInSc1
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1823	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnPc1
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc4
</s>
<s>
www.norstedts.se	www.norstedts.se	k6eAd1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Norstedts	Norstedts	k1gInSc1
Förlag	Förlag	k1gInSc1
je	být	k5eAaImIp3nS
nakladatelství	nakladatelství	k1gNnSc1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
nejstarší	starý	k2eAgNnSc4d3
švédské	švédský	k2eAgNnSc4d1
nakladatelství	nakladatelství	k1gNnSc4
a	a	k8xC
jedno	jeden	k4xCgNnSc1
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1823	#num#	k4
Per	Per	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Norstedt	Norstedt	k1gMnSc1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
P.	P.	kA
A.	A.	kA
Norstedt	Norstedt	k1gMnSc1
&	&	k?
Söner	Söner	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
P.	P.	kA
A.	A.	kA
Norstedt	Norstedta	k1gFnPc2
&	&	k?
synové	syn	k1gMnPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kořeny	kořen	k1gInPc1
nakladatelství	nakladatelství	k1gNnSc2
jsou	být	k5eAaImIp3nP
však	však	k9
ještě	ještě	k6eAd1
mnohem	mnohem	k6eAd1
starší	starý	k2eAgFnPc1d2
a	a	k8xC
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1526	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Nakladatelská	nakladatelský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
Per	pero	k1gNnPc2
Adolfa	Adolf	k1gMnSc2
Norstedta	Norstedt	k1gMnSc2
započala	započnout	k5eAaPmAgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
roku	rok	k1gInSc2
1821	#num#	k4
zakoupil	zakoupit	k5eAaPmAgMnS
tiskárnu	tiskárna	k1gFnSc4
od	od	k7c2
vdovy	vdova	k1gFnSc2
po	po	k7c4
J.	J.	kA
P.	P.	kA
Lindhovi	Lindhův	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
společnosti	společnost	k1gFnPc4
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
až	až	k9
v	v	k7c6
Královské	královský	k2eAgFnSc6d1
tiskařské	tiskařský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1526	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Per	prát	k5eAaImRp2nS
Adolf	Adolf	k1gMnSc1
Norstedt	Norstedt	k1gMnSc1
zapojil	zapojit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1823	#num#	k4
do	do	k7c2
obchodní	obchodní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
také	také	k9
své	svůj	k3xOyFgMnPc4
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
Carla	Carl	k1gMnSc4
a	a	k8xC
Adolfa	Adolf	k1gMnSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
tak	tak	k9
založena	založit	k5eAaPmNgFnS
společnost	společnost	k1gFnSc1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
P.	P.	kA
A.	A.	kA
Norstedt	Norstedt	k1gMnSc1
&	&	k?
Söner	Söner	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
tisk	tisk	k1gInSc4
královských	královský	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
o	o	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protože	protože	k8xS
ani	ani	k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
synů	syn	k1gMnPc2
neměl	mít	k5eNaImAgMnS
dědice	dědic	k1gMnSc4
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
řízena	řídit	k5eAaImNgFnS
Emilií	Emilie	k1gFnSc7
Nostedtovou	Nostedtový	k2eAgFnSc7d1
<g/>
,	,	kIx,
neteří	neteř	k1gFnSc7
Per	pero	k1gNnPc2
Adolfa	Adolf	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
velkoobchodníka	velkoobchodník	k1gMnSc4
Gustafa	Gustaf	k1gMnSc4
Philipa	Philip	k1gMnSc4
Laurina	Laurin	k1gMnSc4
(	(	kIx(
<g/>
1808	#num#	k4
<g/>
–	–	k?
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
řízena	řídit	k5eAaImNgFnS
jejich	jejich	k3xOp3gInSc7
syny	syn	k1gMnPc7
Göstou	Göstá	k1gFnSc4
Laurinem	Laurin	k1gInSc7
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
–	–	k?
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
Laurinem	Laurin	k1gInSc7
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Albert	Albert	k1gMnSc1
Laurinem	Laurin	k1gInSc7
(	(	kIx(
<g/>
1842	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carl	Carl	k1gInSc1
Laurin	Laurin	k1gInSc4
byl	být	k5eAaImAgInS
strojní	strojní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
a	a	k8xC
staral	starat	k5eAaImAgMnS
se	se	k3xPyFc4
tak	tak	k9
o	o	k7c4
technické	technický	k2eAgNnSc4d1
a	a	k8xC
tiskařské	tiskařský	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
současně	současně	k6eAd1
finančním	finanční	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nakladatelská	nakladatelský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Norstedts	Norstedts	k6eAd1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
Norstedts	Norstedtsa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
krásnou	krásný	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
a	a	k8xC
literaturu	literatura	k1gFnSc4
faktu	fakt	k1gInSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
Rabén	Rabén	k1gInSc1
&	&	k?
Sjögren	Sjögrna	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
dětskou	dětský	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rabén	Rabén	k1gInSc1
&	&	k?
Sjögren	Sjögrna	k1gFnPc2
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
samostatné	samostatný	k2eAgNnSc1d1
švédské	švédský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1942	#num#	k4
Hans	hansa	k1gFnPc2
Rabén	Rabén	k1gInSc4
a	a	k8xC
Carl-Olof	Carl-Olof	k1gInSc4
Sjögren	Sjögrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejúspěšnější	úspěšný	k2eAgInPc1d3
tituly	titul	k1gInPc1
Rabén	Rabén	k1gInSc1
&	&	k?
Sjögren	Sjögrna	k1gFnPc2
jsou	být	k5eAaImIp3nP
knihy	kniha	k1gFnSc2
Astrid	Astrida	k1gFnPc2
Lindgrenové	Lindgrenová	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
další	další	k2eAgMnPc4d1
vydávané	vydávaný	k2eAgMnPc4d1
autory	autor	k1gMnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
britská	britský	k2eAgFnSc1d1
autorka	autorka	k1gFnSc1
Enid	Enida	k1gFnPc2
Blytonová	Blytonový	k2eAgFnSc1d1
nebo	nebo	k8xC
norský	norský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Jostein	Jostein	k1gMnSc1
Gaarder	Gaarder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
ještě	ještě	k9
několik	několik	k4yIc1
dalších	další	k2eAgMnPc2d1
původně	původně	k6eAd1
nezávislých	závislý	k2eNgMnPc2d1
nakladatelů	nakladatel	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
skupina	skupina	k1gFnSc1
získala	získat	k5eAaPmAgFnS
různými	různý	k2eAgFnPc7d1
akvizicemi	akvizice	k1gFnPc7
<g/>
:	:	kIx,
Prisma	prisma	k1gFnSc1
<g/>
,	,	kIx,
Nautiska	Nautiska	k1gFnSc1
Förlaget	Förlaget	k1gInSc1
<g/>
,	,	kIx,
Norstedts	Norstedts	k1gInSc1
Akademiska	Akademiska	k1gFnSc1
<g/>
,	,	kIx,
Tivoli	Tivole	k1gFnSc4
<g/>
,	,	kIx,
Eriksson	Eriksson	k1gNnSc4
&	&	k?
Lindgren	Lindgrna	k1gFnPc2
<g/>
,	,	kIx,
Tiden	Tidna	k1gFnPc2
a	a	k8xC
Gammafon	Gammafon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
části	část	k1gFnPc4
fungovali	fungovat	k5eAaImAgMnP
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
pod	pod	k7c7
svými	svůj	k3xOyFgInPc7
původními	původní	k2eAgInPc7d1
názvy	název	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
starší	starý	k2eAgFnSc2d2
obchodní	obchodní	k2eAgFnSc2d1
značky	značka	k1gFnSc2
jako	jako	k8xC,k8xS
AWE	AWE	kA
/	/	kIx~
Gebers	Gebers	k1gInSc1
nebo	nebo	k8xC
PAN	Pan	k1gMnSc1
již	již	k6eAd1
zanikly	zaniknout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Nakladatelská	nakladatelský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
má	mít	k5eAaImIp3nS
přes	přes	k7c4
100	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
vydala	vydat	k5eAaPmAgFnS
přibližně	přibližně	k6eAd1
400	#num#	k4
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
mělo	mít	k5eAaImAgNnS
nakladatelství	nakladatelství	k1gNnSc1
obrat	obrat	k1gInSc1
přibližně	přibližně	k6eAd1
435	#num#	k4
miliónů	milión	k4xCgInPc2
SEK	SEK	kA
<g/>
,	,	kIx,
čistý	čistý	k2eAgInSc4d1
zisk	zisk	k1gInSc4
asi	asi	k9
37	#num#	k4
miliónů	milión	k4xCgInPc2
SEK	SEK	kA
a	a	k8xC
disponibilní	disponibilní	k2eAgFnSc4d1
hotovost	hotovost	k1gFnSc4
cca	cca	kA
71	#num#	k4
miliónů	milión	k4xCgInPc2
SEK	SEK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
také	také	k9
vlastníkem	vlastník	k1gMnSc7
nebo	nebo	k8xC
částečným	částečný	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
několika	několik	k4yIc2
knižních	knižní	k2eAgInPc2d1
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
vlastnila	vlastnit	k5eAaImAgFnS
Kooperativa	kooperativa	k1gFnSc1
Förbundet	Förbundet	k1gMnSc1
(	(	kIx(
<g/>
švédské	švédský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
spotřebních	spotřební	k2eAgNnPc2d1
družstev	družstvo	k1gNnPc2
<g/>
,	,	kIx,
sdružení	sdružení	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
již	již	k9
roku	rok	k1gInSc2
1899	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
</s>
<s>
bylo	být	k5eAaImAgNnS
nakladatelství	nakladatelství	k1gNnSc1
zakoupeno	zakoupen	k2eAgNnSc1d1
společností	společnost	k1gFnSc7
Storytel	Storytela	k1gFnPc2
(	(	kIx(
<g/>
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
vydáváním	vydávání	k1gNnSc7
audioknih	audiokniha	k1gFnPc2
a	a	k8xC
e-knih	e-kniha	k1gFnPc2
<g/>
)	)	kIx)
za	za	k7c4
152	#num#	k4
miliónů	milión	k4xCgInPc2
SEK	SEK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výkonným	výkonný	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
společnosti	společnost	k1gFnSc2
od	od	k7c2
srpna	srpen	k1gInSc2
2014	#num#	k4
je	být	k5eAaImIp3nS
Otto	Otto	k1gMnSc1
Sjöberg	Sjöberg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
společnosti	společnost	k1gFnPc1
je	být	k5eAaImIp3nS
v	v	k7c6
historické	historický	k2eAgFnSc6d1
budově	budova	k1gFnSc6
bývalé	bývalý	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
Norstedtshuset	Norstedtshuseta	k1gFnPc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Riddarholmen	Riddarholmen	k1gInSc1
v	v	k7c6
centru	centrum	k1gNnSc6
Stockholmu	Stockholm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vydávaní	vydávaný	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
</s>
<s>
Mezi	mezi	k7c4
švédské	švédský	k2eAgMnPc4d1
autory	autor	k1gMnPc4
a	a	k8xC
autorky	autorka	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
knihy	kniha	k1gFnPc1
nakladatelství	nakladatelství	k1gNnSc1
vydalo	vydat	k5eAaPmAgNnS
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
Hjalmar	Hjalmar	k1gMnSc1
Gullberg	Gullberg	k1gMnSc1
<g/>
,	,	kIx,
Maria	Maria	k1gFnSc1
Lang	Lang	k1gMnSc1
<g/>
,	,	kIx,
Stig	Stig	k1gMnSc1
Dagerman	Dagerman	k1gMnSc1
<g/>
,	,	kIx,
Birgitta	Birgitta	k1gMnSc1
Stenberg	Stenberg	k1gMnSc1
<g/>
,	,	kIx,
Pär	Pär	k1gMnSc1
Rå	Rå	k1gMnSc1
<g/>
,	,	kIx,
Elsa	Elsa	k1gFnSc1
Grave	grave	k1gNnSc2
<g/>
,	,	kIx,
Ingmar	Ingmar	k1gMnSc1
Bergman	Bergman	k1gMnSc1
<g/>
,	,	kIx,
Per	prát	k5eAaImRp2nS
Olov	olovit	k5eAaImRp2nS
Enquist	Enquist	k1gInSc1
<g/>
,	,	kIx,
Agneta	Agnet	k2eAgFnSc1d1
Pleijel	Pleijel	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Torgny	Torgno	k1gNnPc7
Lindgren	Lindgrna	k1gFnPc2
<g/>
,	,	kIx,
Astrid	Astrid	k1gInSc1
Lindgrenová	Lindgrenová	k1gFnSc1
<g/>
,	,	kIx,
Barbro	Barbro	k1gNnSc1
Lindgrenová	Lindgrenová	k1gFnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
Strindberg	Strindberg	k1gMnSc1
<g/>
,	,	kIx,
Henning	Henning	k1gInSc1
Mankell	Mankell	k1gInSc1
<g/>
,	,	kIx,
Sigrid	Sigrid	k1gInSc1
Combüchen	Combüchen	k1gInSc1
<g/>
,	,	kIx,
Anders	Anders	k1gInSc1
Ehnmark	Ehnmark	k1gInSc1
<g/>
,	,	kIx,
Mikael	Mikael	k1gInSc1
Niemi	Nie	k1gFnPc7
<g/>
,	,	kIx,
Majgull	Majgull	k1gMnSc1
Axelsson	Axelsson	k1gMnSc1
<g/>
,	,	kIx,
Torbjörn	Torbjörn	k1gMnSc1
Flygt	Flygt	k1gMnSc1
<g/>
,	,	kIx,
Carl-Henning	Carl-Henning	k1gInSc1
Wijkmark	Wijkmark	k1gInSc1
<g/>
,	,	kIx,
Jonas	Jonas	k1gInSc1
Hassen	Hassna	k1gFnPc2
Khemiri	Khemir	k1gFnSc2
<g/>
,	,	kIx,
Frans	Frans	k1gMnSc1
G.	G.	kA
Bengtsson	Bengtsson	k1gMnSc1
<g/>
,	,	kIx,
Kjell	Kjell	k1gMnSc1
Espmark	Espmark	k1gInSc1
<g/>
,	,	kIx,
Per	pero	k1gNnPc2
Odensten	Odensten	k2eAgInSc1d1
<g/>
,	,	kIx,
Jonas	Jonas	k1gInSc1
Gardell	Gardella	k1gFnPc2
<g/>
,	,	kIx,
Maj	Maja	k1gFnPc2
Sjöwallová	Sjöwallový	k2eAgFnSc1d1
–	–	k?
Per	pero	k1gNnPc2
Wahlöö	Wahlöö	k1gMnPc2
a	a	k8xC
Stieg	Stiega	k1gFnPc2
Larsson	Larssona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
vydávané	vydávaný	k2eAgMnPc4d1
zahraniční	zahraniční	k2eAgMnPc4d1
autory	autor	k1gMnPc4
patří	patřit	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc1
nositelů	nositel	k1gMnPc2
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
literaturu	literatura	k1gFnSc4
<g/>
:	:	kIx,
peruánský	peruánský	k2eAgMnSc1d1
romanopisec	romanopisec	k1gMnSc1
a	a	k8xC
esejista	esejista	k1gMnSc1
Mario	Mario	k1gMnSc1
Vargas	Vargas	k1gMnSc1
Llosa	Llosa	k1gFnSc1
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Jean-Marie	Jean-Marie	k1gFnSc2
Gustave	Gustav	k1gMnSc5
Le	Le	k1gMnSc5
Clézio	Clézia	k1gMnSc5
<g/>
,	,	kIx,
turecký	turecký	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Orhan	Orhan	k1gMnSc1
Pamuk	Pamuk	k1gMnSc1
<g/>
,	,	kIx,
maďarský	maďarský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Imre	Imr	k1gFnSc2
Kertész	Kertész	k1gMnSc1
a	a	k8xC
americký	americký	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
Bob	Bob	k1gMnSc1
Dylan	Dylan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgMnPc4d1
význačné	význačný	k2eAgMnPc4d1
vydávané	vydávaný	k2eAgMnPc4d1
autory	autor	k1gMnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Graham	graham	k1gInSc1
Greene	Green	k1gInSc5
<g/>
,	,	kIx,
E.	E.	kA
L.	L.	kA
James	James	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Connelly	Connella	k1gFnSc2
<g/>
,	,	kIx,
Suzanne	Suzann	k1gInSc5
Brø	Brø	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
R.	R.	kA
R.	R.	kA
Tolkien	Tolkina	k1gFnPc2
<g/>
,	,	kIx,
Isabel	Isabela	k1gFnPc2
Allende	Allend	k1gMnSc5
nebo	nebo	k8xC
Joanne	Joann	k1gMnSc5
Rowlingová	Rowlingový	k2eAgNnPc5d1
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
staré	starý	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
</s>
<s>
Budova	budova	k1gFnSc1
staré	starý	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Norstedt	Norstedt	k2eAgInSc1d1
Building	Building	k1gInSc1
<g/>
,	,	kIx,
švédsky	švédsky	k6eAd1
Norstedtshuset	Norstedtshuset	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rohu	roh	k1gInSc6
ulic	ulice	k1gFnPc2
Norra	Norr	k1gInSc2
Riddarholms	Riddarholmsa	k1gFnPc2
a	a	k8xC
Arkivgatan	Arkivgatan	k1gInSc1
na	na	k7c6
malém	malý	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
</s>
<s>
Riddarholmen	Riddarholmen	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
starého	starý	k2eAgNnSc2d1
města	město	k1gNnSc2
Gamla	Gamlo	k1gNnSc2
stan	stan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budovu	budova	k1gFnSc4
navrhl	navrhnout	k5eAaPmAgMnS
švédský	švédský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Magnus	Magnus	k1gMnSc1
Isæ	Isæ	k1gMnSc1
a	a	k8xC
postavena	postaven	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1882	#num#	k4
<g/>
–	–	k?
<g/>
1891	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
střecha	střecha	k1gFnSc1
s	s	k7c7
věžemi	věž	k1gFnPc7
tvoří	tvořit	k5eAaImIp3nS
dobře	dobře	k6eAd1
známou	známý	k2eAgFnSc4d1
siluetu	silueta	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
panoramatu	panorama	k1gNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
bývá	bývat	k5eAaImIp3nS
fotografováno	fotografovat	k5eAaImNgNnS
společně	společně	k6eAd1
s	s	k7c7
mostem	most	k1gInSc7
Vasabron	Vasabron	k1gInSc1
v	v	k7c6
popředí	popředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
(	(	kIx(
<g/>
Akrivgatan	Akrivgatan	k1gInSc1
3	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
stylově	stylově	k6eAd1
podobná	podobný	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Gamla	Gamla	k1gMnSc1
Riksarkivet	Riksarkivet	k1gMnSc1
<g/>
:	:	kIx,
stará	starý	k2eAgFnSc1d1
budova	budova	k1gFnSc1
národního	národní	k2eAgInSc2d1
archívu	archív	k1gInSc2
(	(	kIx(
<g/>
Říšského	říšský	k2eAgInSc2d1
archívu	archív	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
Norstedtshuset	Norstedtshuset	k1gInSc4
původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
tiskárna	tiskárna	k1gFnSc1
nakladatelství	nakladatelství	k1gNnSc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
administrativní	administrativní	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Norstedts	Norstedts	k1gInSc1
Förlag	Förlag	k1gInSc1
na	na	k7c6
Frankfurtském	frankfurtský	k2eAgInSc6d1
knižním	knižní	k2eAgInSc6d1
veletrhu	veletrh	k1gInSc6
</s>
<s>
Norstedtshuset	Norstedtshuset	k1gInSc1
<g/>
:	:	kIx,
budova	budova	k1gFnSc1
staré	starý	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
na	na	k7c4
Riddarholmen	Riddarholmen	k1gInSc4
</s>
<s>
Norstedtshuset	Norstedtshuset	k1gInSc1
<g/>
:	:	kIx,
noční	noční	k2eAgInSc4d1
pohled	pohled	k1gInSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Riddarholmen	Riddarholmen	k1gInSc1
</s>
<s>
Gamla	Gamla	k6eAd1
stan	stan	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Norstedts	Norstedts	k1gInSc1
förlag	förlag	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Norstedt	Norstedt	k2eAgInSc1d1
Building	Building	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Rabén	Rabén	k1gInSc4
&	&	k?
Sjögren	Sjögrna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Anders	Anders	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grafisk	Grafisk	k1gInSc1
industri	industr	k1gFnSc2
–	–	k?
Foretagsminnen	Foretagsminnen	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Storytel	Storytela	k1gFnPc2
buys	buys	k6eAd1
Swedish	Swedish	k1gInSc1
publisher	publishra	k1gFnPc2
Norstedts	Norstedtsa	k1gFnPc2
for	forum	k1gNnPc2
£	£	k?
<g/>
12.5	12.5	k4
<g/>
m	m	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Författare	Författar	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Må	Må	k1gMnSc1
<g/>
,	,	kIx,
Johan	Johan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guide	Guid	k1gInSc5
till	till	k1gMnSc1
Stockholms	Stockholmsa	k1gFnPc2
arkitektur	arkitektura	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
nd	nd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stockholm	Stockholm	k1gInSc1
<g/>
:	:	kIx,
Arkitektur	Arkitektura	k1gFnPc2
Förlag	Förlaga	k1gFnPc2
AB	AB	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
91	#num#	k4
<g/>
-	-	kIx~
<g/>
86050	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Norra	Norr	k1gInSc2
innerstaden	innerstaden	k2eAgInSc1d1
<g/>
,	,	kIx,
s.	s.	k?
136	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Norstedts	Norstedtsa	k1gFnPc2
Förlag	Förlaga	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Norstedts	Norstedtsa	k1gFnPc2
Förlag	Förlaga	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
16321109-7	16321109-7	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0627	#num#	k4
116X	116X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
92062860	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
221371901	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
92062860	#num#	k4
</s>
