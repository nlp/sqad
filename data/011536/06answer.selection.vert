<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
vzlety	vzlet	k1gInPc4	vzlet
<g/>
,	,	kIx,	,
přistání	přistání	k1gNnSc4	přistání
(	(	kIx(	(
<g/>
vzletová	vzletový	k2eAgFnSc1d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozemní	pozemní	k2eAgInPc4d1	pozemní
pohyby	pohyb	k1gInPc4	pohyb
letadel	letadlo	k1gNnPc2	letadlo
po	po	k7c6	po
pojezdových	pojezdový	k2eAgFnPc6d1	pojezdová
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
