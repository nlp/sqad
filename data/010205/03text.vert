<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Adamec	Adamec	k1gMnSc1	Adamec
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1909	[number]	k4	1909
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
student	student	k1gMnSc1	student
a	a	k8xC	a
studentský	studentský	k2eAgMnSc1d1	studentský
funkcionář	funkcionář	k1gMnSc1	funkcionář
popravený	popravený	k2eAgMnSc1d1	popravený
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Adamec	Adamec	k1gMnSc1	Adamec
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Libni	Libeň	k1gFnSc6	Libeň
a	a	k8xC	a
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
insigniády	insigniáda	k1gFnSc2	insigniáda
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mluvčích	mluvčí	k1gMnPc2	mluvčí
českých	český	k2eAgMnPc2d1	český
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
až	až	k9	až
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Ústředního	ústřední	k2eAgInSc2d1	ústřední
svazu	svaz	k1gInSc2	svaz
československého	československý	k2eAgNnSc2d1	Československé
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
1937-9	[number]	k4	1937-9
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
dozorčí	dozorčí	k2eAgFnSc6d1	dozorčí
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působil	působit	k5eAaImAgMnS	působit
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
mládeži	mládež	k1gFnSc6	mládež
Národního	národní	k2eAgNnSc2d1	národní
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
Klubu	klub	k1gInSc2	klub
národně	národně	k6eAd1	národně
socialistických	socialistický	k2eAgMnPc2d1	socialistický
akademiků	akademik	k1gMnPc2	akademik
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
Národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
pracující	pracující	k2eAgFnSc2d1	pracující
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
od	od	k7c2	od
září	září	k1gNnSc2	září
1939	[number]	k4	1939
jednatelem	jednatel	k1gMnSc7	jednatel
Národního	národní	k2eAgInSc2d1	národní
svazu	svaz	k1gInSc2	svaz
českého	český	k2eAgNnSc2d1	české
studentstva	studentstvo	k1gNnSc2	studentstvo
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
časných	časný	k2eAgFnPc6d1	časná
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
zatčen	zatknout	k5eAaPmNgMnS	zatknout
gestapem	gestapo	k1gNnSc7	gestapo
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
popraven	popraven	k2eAgInSc4d1	popraven
v	v	k7c6	v
ruzyňských	ruzyňský	k2eAgFnPc6d1	Ruzyňská
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
memoriam	memoriam	k6eAd1	memoriam
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
JUDr.	JUDr.	kA	JUDr.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PASÁK	pasák	k1gMnSc1	pasák
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1939	[number]	k4	1939
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
ISBN	ISBN	kA	ISBN
80-7184-255-9	[number]	k4	80-7184-255-9
</s>
</p>
<p>
<s>
LEIKERT	LEIKERT	kA	LEIKERT
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
pátek	pátek	k1gInSc1	pátek
sedmnáctého	sedmnáctý	k4xOgInSc2	sedmnáctý
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
ISBN	ISBN	kA	ISBN
80-238-7632-5	[number]	k4	80-238-7632-5
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A-	A-	k1gFnSc1	A-
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
A.	A.	kA	A.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
155	[number]	k4	155
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Adamec	Adamec	k1gMnSc1	Adamec
–	–	k?	–
životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
http://valka.cz	[url]	k1gFnPc2	http://valka.cz
</s>
</p>
