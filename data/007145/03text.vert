<s>
Silniční	silniční	k2eAgInSc1d1	silniční
válec	válec	k1gInSc1	válec
je	být	k5eAaImIp3nS	být
stavební	stavební	k2eAgInSc1d1	stavební
stroj	stroj	k1gInSc1	stroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
hutnění	hutnění	k1gNnSc4	hutnění
povrchů	povrch	k1gInPc2	povrch
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
pracovním	pracovní	k2eAgInSc7d1	pracovní
nástrojem	nástroj	k1gInSc7	nástroj
je	být	k5eAaImIp3nS	být
běhoun	běhoun	k1gInSc1	běhoun
-	-	kIx~	-
ocelový	ocelový	k2eAgInSc1d1	ocelový
válec	válec	k1gInSc1	válec
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
kulidlo	kulidlo	k1gNnSc1	kulidlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
statické	statický	k2eAgFnSc3d1	statická
-	-	kIx~	-
působí	působit	k5eAaImIp3nS	působit
pouze	pouze	k6eAd1	pouze
svojí	svůj	k3xOyFgFnSc7	svůj
statickou	statický	k2eAgFnSc7d1	statická
hmotností	hmotnost	k1gFnSc7	hmotnost
vibrační	vibrační	k2eAgFnSc7d1	vibrační
-	-	kIx~	-
mimo	mimo	k7c4	mimo
svoji	svůj	k3xOyFgFnSc4	svůj
statickou	statický	k2eAgFnSc4d1	statická
hmotnost	hmotnost	k1gFnSc4	hmotnost
působí	působit	k5eAaImIp3nS	působit
též	též	k9	též
vibračním	vibrační	k2eAgInSc7d1	vibrační
účinkem	účinek	k1gInSc7	účinek
parní	parní	k2eAgFnSc3d1	parní
válce	válka	k1gFnSc3	válka
válce	válka	k1gFnSc3	válka
poháněné	poháněný	k2eAgMnPc4d1	poháněný
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
válce	válec	k1gInSc2	válec
tandemové	tandemový	k2eAgNnSc1d1	tandemové
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
2	[number]	k4	2
běhouny	běhoun	k1gInPc4	běhoun
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
hutnění	hutnění	k1gNnSc4	hutnění
živic	živice	k1gFnPc2	živice
<g/>
)	)	kIx)	)
válce	válec	k1gInSc2	válec
tahačové	tahačové	k2eAgMnSc1d1	tahačové
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
1	[number]	k4	1
běhoun	běhoun	k1gInSc4	běhoun
a	a	k8xC	a
nápravu	náprava	k1gFnSc4	náprava
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
<g />
.	.	kIx.	.
</s>
<s>
pneumatikami	pneumatika	k1gFnPc7	pneumatika
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
hutnění	hutnění	k1gNnSc4	hutnění
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
válce	válec	k1gInSc2	válec
pneumatikové	pneumatik	k1gMnPc5	pneumatik
-	-	kIx~	-
místo	místo	k1gNnSc4	místo
běhounu	běhoun	k1gInSc2	běhoun
mají	mít	k5eAaImIp3nP	mít
sadu	sada	k1gFnSc4	sada
pneumatik	pneumatika	k1gFnPc2	pneumatika
s	s	k7c7	s
hladkým	hladký	k2eAgInSc7d1	hladký
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
statické	statický	k2eAgInPc4d1	statický
válce	válec	k1gInPc4	válec
válce	válec	k1gInSc2	válec
tříkolové	tříkolový	k2eAgMnPc4d1	tříkolový
-	-	kIx~	-
rovněž	rovněž	k9	rovněž
statické	statický	k2eAgFnSc3d1	statická
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgFnSc6d1	vybavená
jedním	jeden	k4xCgInSc7	jeden
běhounem	běhoun	k1gInSc7	běhoun
vepředu	vepříst	k5eAaPmIp1nS	vepříst
a	a	k8xC	a
dvěma	dva	k4xCgNnPc7	dva
ocelovými	ocelový	k2eAgNnPc7d1	ocelové
koly	kolo	k1gNnPc7	kolo
vzadu	vzadu	k6eAd1	vzadu
válce	válka	k1gFnSc3	válka
tažené	tažený	k2eAgFnSc3d1	tažená
(	(	kIx(	(
<g/>
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
běhounem	běhoun	k1gInSc7	běhoun
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
tažené	tažený	k2eAgMnPc4d1	tažený
jiným	jiný	k2eAgInSc7d1	jiný
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
traktorem	traktor	k1gInSc7	traktor
<g/>
)	)	kIx)	)
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
rámem	rám	k1gInSc7	rám
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc6	řízení
natáčením	natáčení	k1gNnSc7	natáčení
vlastních	vlastní	k2eAgInPc2d1	vlastní
běhounů	běhoun	k1gInPc2	běhoun
<g/>
)	)	kIx)	)
kloubové	kloubový	k2eAgNnSc4d1	kloubové
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc4	řízení
natáčením	natáčení	k1gNnSc7	natáčení
předního	přední	k2eAgMnSc2d1	přední
a	a	k8xC	a
zadního	zadní	k2eAgInSc2d1	zadní
rámu	rám	k1gInSc2	rám
vzájemně	vzájemně	k6eAd1	vzájemně
<g/>
)	)	kIx)	)
Z	z	k7c2	z
uvedeného	uvedený	k2eAgInSc2d1	uvedený
výčtu	výčet	k1gInSc2	výčet
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
pouze	pouze	k6eAd1	pouze
válce	válka	k1gFnSc3	válka
vibrační	vibrační	k2eAgFnSc3d1	vibrační
a	a	k8xC	a
statické	statický	k2eAgFnSc3d1	statická
válce	válka	k1gFnSc3	válka
pneumatikové	pneumatik	k1gMnPc1	pneumatik
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
kategorie	kategorie	k1gFnPc1	kategorie
slouží	sloužit	k5eAaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
historickou	historický	k2eAgFnSc4d1	historická
úplnost	úplnost	k1gFnSc4	úplnost
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
silniční	silniční	k2eAgInSc4d1	silniční
válec	válec	k1gInSc4	válec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
