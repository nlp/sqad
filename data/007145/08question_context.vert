<s>
Silniční	silniční	k2eAgInSc1d1	silniční
válec	válec	k1gInSc1	válec
je	být	k5eAaImIp3nS	být
stavební	stavební	k2eAgInSc1d1	stavební
stroj	stroj	k1gInSc1	stroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
hutnění	hutnění	k1gNnSc4	hutnění
povrchů	povrch	k1gInPc2	povrch
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
pracovním	pracovní	k2eAgInSc7d1	pracovní
nástrojem	nástroj	k1gInSc7	nástroj
je	být	k5eAaImIp3nS	být
běhoun	běhoun	k1gInSc1	běhoun
-	-	kIx~	-
ocelový	ocelový	k2eAgInSc1d1	ocelový
válec	válec	k1gInSc1	válec
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
kulidlo	kulidlo	k1gNnSc1	kulidlo
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
