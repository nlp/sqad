<p>
<s>
Malina	Malina	k1gMnSc1	Malina
je	být	k5eAaImIp3nS	být
plodem	plod	k1gInSc7	plod
(	(	kIx(	(
<g/>
botanicky	botanicky	k6eAd1	botanicky
přesně	přesně	k6eAd1	přesně
plodenstvím	plodenství	k1gNnSc7	plodenství
<g/>
)	)	kIx)	)
rostliny	rostlina	k1gFnSc2	rostlina
ostružiníku	ostružiník	k1gInSc2	ostružiník
maliníku	maliník	k1gInSc2	maliník
<g/>
.	.	kIx.	.
</s>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
drobných	drobný	k2eAgFnPc2d1	drobná
kuliček	kulička	k1gFnPc2	kulička
<g/>
,	,	kIx,	,
peckovic	peckovice	k1gFnPc2	peckovice
sytě	sytě	k6eAd1	sytě
růžové	růžový	k2eAgFnSc2d1	růžová
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgInPc2d1	tvořící
známé	známý	k2eAgInPc4d1	známý
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maliny	Malina	k1gMnPc4	Malina
jsou	být	k5eAaImIp3nP	být
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
lesním	lesní	k2eAgInPc3d1	lesní
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
použití	použití	k1gNnSc2	použití
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc4d1	široké
–	–	k?	–
malinový	malinový	k2eAgInSc4d1	malinový
sirup	sirup	k1gInSc4	sirup
<g/>
,	,	kIx,	,
do	do	k7c2	do
jogurtů	jogurt	k1gInPc2	jogurt
<g/>
,	,	kIx,	,
zmrzliny	zmrzlina	k1gFnSc2	zmrzlina
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
přírodní	přírodní	k2eAgNnSc4d1	přírodní
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
malinovou	malinový	k2eAgFnSc7d1	malinová
příchutí	příchuť	k1gFnSc7	příchuť
například	například	k6eAd1	například
v	v	k7c6	v
pudinku	pudink	k1gInSc6	pudink
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘÍHA	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ovoce	ovoce	k1gNnSc1	ovoce
:	:	kIx,	:
Díl	díl	k1gInSc1	díl
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
srstky	srstka	k1gFnPc1	srstka
<g/>
,	,	kIx,	,
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
maliny	malina	k1gFnPc1	malina
a	a	k8xC	a
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ovocnický	ovocnický	k2eAgInSc1d1	ovocnický
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Maliny	malina	k1gFnSc2	malina
a	a	k8xC	a
ostružiny	ostružina	k1gFnSc2	ostružina
<g/>
,	,	kIx,	,
s.	s.	k?	s.
179	[number]	k4	179
<g/>
-	-	kIx~	-
<g/>
190	[number]	k4	190
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ostružiník	ostružiník	k1gInSc1	ostružiník
maliník	maliník	k1gInSc1	maliník
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
malina	malina	k1gFnSc1	malina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
malina	malina	k1gFnSc1	malina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Malina	malina	k1gFnSc1	malina
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
malina	malina	k1gFnSc1	malina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
