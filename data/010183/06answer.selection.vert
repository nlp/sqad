<s>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schlacht	Schlacht	k2eAgInSc1d1	Schlacht
am	am	k?	am
Weißen	Weißen	k2eAgInSc1d1	Weißen
Berg	Berg	k1gInSc1	Berg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svedená	svedený	k2eAgFnSc1d1	svedená
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
bitva	bitva	k1gFnSc1	bitva
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
