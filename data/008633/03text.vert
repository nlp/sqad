<p>
<s>
Telex	telex	k1gInSc4	telex
byla	být	k5eAaImAgFnS	být
strakonická	strakonický	k2eAgFnSc1d1	Strakonická
punková	punkový	k2eAgFnSc1d1	punková
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
Telex	telex	k1gInSc1	telex
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nazývala	nazývat	k5eAaImAgFnS	nazývat
Novodur	novodur	k1gInSc4	novodur
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
přejaté	přejatý	k2eAgFnPc4d1	přejatá
punkrockové	punkrockový	k2eAgFnPc4d1	punkrocková
písně	píseň	k1gFnPc4	píseň
po	po	k7c6	po
českých	český	k2eAgFnPc6d1	Česká
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
však	však	k9	však
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
koncert	koncert	k1gInSc1	koncert
rozehnán	rozehnán	k2eAgInSc1d1	rozehnán
příslušníky	příslušník	k1gMnPc7	příslušník
SNB	SNB	kA	SNB
a	a	k8xC	a
kapele	kapela	k1gFnSc6	kapela
zakázána	zakázán	k2eAgFnSc1d1	zakázána
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
znovu	znovu	k6eAd1	znovu
formuje	formovat	k5eAaImIp3nS	formovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Armavir	Armavira	k1gFnPc2	Armavira
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc4	činnost
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
však	však	k9	však
brzy	brzy	k6eAd1	brzy
zakázána	zakázat	k5eAaPmNgFnS	zakázat
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
vzniká	vznikat	k5eAaImIp3nS	vznikat
kapela	kapela	k1gFnSc1	kapela
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Telex	telex	k1gInSc1	telex
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
<g/>
,	,	kIx,	,
než	než	k8xS	než
kapel	kapela	k1gFnPc2	kapela
původních	původní	k2eAgFnPc2d1	původní
<g/>
,	,	kIx,	,
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
o	o	k7c4	o
punkrock	punkrock	k1gInSc4	punkrock
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
téměř	téměř	k6eAd1	téměř
o	o	k7c6	o
hardcore	hardcor	k1gInSc5	hardcor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgInSc4	první
MC	MC	kA	MC
–	–	k?	–
demo	demo	k2eAgNnSc1d1	demo
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
"	"	kIx"	"
<g/>
Řeznickej	Řeznickej	k?	Řeznickej
krám	krám	k1gInSc1	krám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
demu	demus	k1gInSc6	demus
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
koncertní	koncertní	k2eAgFnSc3d1	koncertní
aktivitě	aktivita	k1gFnSc3	aktivita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
však	však	k9	však
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
a	a	k8xC	a
zakládají	zakládat	k5eAaImIp3nP	zakládat
ostře	ostro	k6eAd1	ostro
hardcorové	hardcorové	k2eAgMnSc1d1	hardcorové
L.D.	L.D.	k1gMnSc1	L.D.
<g/>
Totenkopf	Totenkopf	k1gMnSc1	Totenkopf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
Telex	telex	k1gInSc1	telex
<g/>
"	"	kIx"	"
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
schází	scházet	k5eAaImIp3nS	scházet
celá	celý	k2eAgFnSc1d1	celá
původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
a	a	k8xC	a
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
CD	CD	kA	CD
"	"	kIx"	"
<g/>
Punk	punk	k1gInSc1	punk
Rádio	rádio	k1gNnSc1	rádio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
většinou	většina	k1gFnSc7	většina
skladby	skladba	k1gFnSc2	skladba
z	z	k7c2	z
MC	MC	kA	MC
"	"	kIx"	"
<g/>
Řeznickej	Řeznickej	k?	Řeznickej
krám	krám	k1gInSc1	krám
<g/>
"	"	kIx"	"
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
po	po	k7c6	po
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
však	však	k9	však
opětovně	opětovně	k6eAd1	opětovně
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
navrací	navracet	k5eAaImIp3nS	navracet
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sporadickém	sporadický	k2eAgNnSc6d1	sporadické
koncertování	koncertování	k1gNnSc6	koncertování
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
končí	končit	k5eAaImIp3nP	končit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
však	však	k9	však
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
občasně	občasně	k6eAd1	občasně
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Bouša	Bouša	k1gMnSc1	Bouša
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
kapele	kapela	k1gFnSc6	kapela
Carlos	Carlos	k1gMnSc1	Carlos
and	and	k?	and
His	his	k1gNnSc2	his
Coyotes	Coyotesa	k1gFnPc2	Coyotesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Bouša	Bouša	k1gMnSc1	Bouša
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Ouředník	Ouředník	k1gMnSc1	Ouředník
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Horský	Horský	k1gMnSc1	Horský
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Vondrášek	Vondrášek	k1gMnSc1	Vondrášek
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
(	(	kIx(	(
<g/>
textař	textař	k1gMnSc1	textař
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Král	Král	k1gMnSc1	Král
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
do	do	k7c2	do
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Milanovský	Milanovský	k2eAgMnSc1d1	Milanovský
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Řeznickej	Řeznickej	k?	Řeznickej
krám	krám	k1gInSc1	krám
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Punk	punk	k1gInSc1	punk
Rádio	rádio	k1gNnSc1	rádio
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
