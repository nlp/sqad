<s>
Tessar	Tessar	k1gMnSc1
</s>
<s>
Tessar	Tessar	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
4	#num#	k4
elementů	element	k1gInPc2
ve	v	k7c6
třech	tři	k4xCgFnPc6
skupinách	skupina	k1gFnPc6
</s>
<s>
35	#num#	k4
<g/>
mm	mm	kA
fotoaparát	fotoaparát	k1gInSc1
Zeiss	Zeiss	k1gInSc1
Ikon	ikon	k1gInSc1
Contessa	Contessa	k1gFnSc1
s	s	k7c7
objektivem	objektiv	k1gInSc7
Carl	Carl	k1gInSc1
Zeiss	Zeissa	k1gFnPc2
Tessar	Tessar	k1gMnSc1
50	#num#	k4
<g/>
mm	mm	kA
f	f	k?
<g/>
/	/	kIx~
<g/>
2.8	2.8	k4
</s>
<s>
Tessar	Tessar	k1gInSc1
je	být	k5eAaImIp3nS
anastigmat	anastigmat	k1gInSc1
složený	složený	k2eAgInSc4d1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
čoček	čočka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
a	a	k8xC
zkonstruoval	zkonstruovat	k5eAaPmAgMnS
Paul	Paul	k1gMnSc1
Rudolph	Rudolph	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tessar	Tessar	k1gInSc1
je	být	k5eAaImIp3nS
variantou	varianta	k1gFnSc7
triplety	triplet	k1gInPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
první	první	k4xOgFnSc1
čočka	čočka	k1gFnSc1
je	být	k5eAaImIp3nS
plankonvexní	plankonvexní	k2eAgFnSc1d1
spojka	spojka	k1gFnSc1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
bikonkávní	bikonkávní	k2eAgFnSc1d1
rozptylka	rozptylka	k1gFnSc1
a	a	k8xC
zadní	zadní	k2eAgFnPc1d1
čočky	čočka	k1gFnPc1
jsou	být	k5eAaImIp3nP
stmelené	stmelený	k2eAgFnPc1d1
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
achromatický	achromatický	k2eAgInSc4d1
kladný	kladný	k2eAgInSc4d1
meniskus	meniskus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tessar	Tessar	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
základem	základ	k1gInSc7
dalších	další	k2eAgInPc2d1
objektivů	objektiv	k1gInPc2
(	(	kIx(
<g/>
Belar	Belar	k1gMnSc1
<g/>
,	,	kIx,
Elmar	Elmar	k1gMnSc1
<g/>
,	,	kIx,
Industar	Industar	k1gMnSc1
<g/>
,	,	kIx,
Xenar	Xenar	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejpoužívanějším	používaný	k2eAgInPc3d3
objektivům	objektiv	k1gInPc3
v	v	k7c6
astronomické	astronomický	k2eAgFnSc6d1
fotografii	fotografia	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tessar	Tessara	k1gFnPc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Normální	normální	k2eAgInSc4d1
objektiv	objektiv	k1gInSc4
</s>
<s>
Širokoúhlý	širokoúhlý	k2eAgInSc4d1
objektiv	objektiv	k1gInSc4
</s>
<s>
Teleobjektiv	teleobjektiv	k1gInSc1
</s>
<s>
Anamorfický	Anamorfický	k2eAgInSc4d1
objektiv	objektiv	k1gInSc4
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tessar	Tessara	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
