<s>
Umění	umění	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
od	od	k7c2
starověku	starověk	k1gInSc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c6
umění	umění	k1gNnSc6
výtvarná	výtvarný	k2eAgNnPc1d1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1
vytvářejí	vytvářet	k5eAaImIp3nP
trvalá	trvalý	k2eAgNnPc1d1
vizuální	vizuální	k2eAgNnPc1d1
umělecká	umělecký	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
(	(	kIx(
<g/>
například	například	k6eAd1
malířství	malířství	k1gNnSc1
<g/>
,	,	kIx,
sochařství	sochařství	k1gNnSc1
<g/>
,	,	kIx,
architektura	architektura	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
umění	umění	k1gNnPc1
múzická	múzický	k2eAgNnPc1d1
či	či	k8xC
performativní	performativní	k2eAgNnPc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
umělec	umělec	k1gMnSc1
sám	sám	k3xTgMnSc1
vystupuje	vystupovat	k5eAaImIp3nS
před	před	k7c7
publikem	publikum	k1gNnSc7
(	(	kIx(
<g/>
například	například	k6eAd1
tanec	tanec	k1gInSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
divadlo	divadlo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>