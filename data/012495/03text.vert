<p>
<s>
Umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
od	od	k7c2	od
"	"	kIx"	"
<g/>
uměti	umět	k5eAaImF	umět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
τ	τ	k?	τ
<g/>
,	,	kIx,	,
techné	techné	k1gFnSc1	techné
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
technika	technika	k1gFnSc1	technika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc1	součást
lidské	lidský	k2eAgFnSc2d1	lidská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
užitečná	užitečný	k2eAgFnSc1d1	užitečná
dovednost	dovednost	k1gFnSc1	dovednost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
neovládá	ovládat	k5eNaImIp3nS	ovládat
každý	každý	k3xTgInSc4	každý
a	a	k8xC	a
které	který	k3yQgNnSc4	který
je	být	k5eAaImIp3nS	být
případně	případně	k6eAd1	případně
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
naučit	naučit	k5eAaPmF	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
se	se	k3xPyFc4	se
však	však	k9	však
nejčastěji	často	k6eAd3	často
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
krásných	krásný	k2eAgNnPc2d1	krásné
umění	umění	k1gNnPc2	umění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
tvořivost	tvořivost	k1gFnSc4	tvořivost
<g/>
,	,	kIx,	,
originalitu	originalita	k1gFnSc4	originalita
a	a	k8xC	a
individualitu	individualita	k1gFnSc4	individualita
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
je	být	k5eAaImIp3nS	být
však	však	k9	však
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
umění	umění	k1gNnSc6	umění
výtvarná	výtvarný	k2eAgNnPc1d1	výtvarné
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
trvalá	trvalý	k2eAgNnPc1d1	trvalé
vizuální	vizuální	k2eAgNnPc1d1	vizuální
umělecká	umělecký	k2eAgNnPc1d1	umělecké
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
umění	umění	k1gNnPc1	umění
múzická	múzický	k2eAgNnPc1d1	múzické
či	či	k8xC	či
performativní	performativní	k2eAgNnPc1d1	performativní
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umělec	umělec	k1gMnSc1	umělec
sám	sám	k3xTgMnSc1	sám
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozlišení	rozlišení	k1gNnSc1	rozlišení
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
díky	díky	k7c3	díky
písmu	písmo	k1gNnSc3	písmo
<g/>
,	,	kIx,	,
notovému	notový	k2eAgInSc3d1	notový
záznamu	záznam	k1gInSc3	záznam
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
záznamovým	záznamový	k2eAgFnPc3d1	záznamová
technikám	technika	k1gFnPc3	technika
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
přežilo	přežít	k5eAaPmAgNnS	přežít
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
užívá	užívat	k5eAaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vědy	věda	k1gFnPc1	věda
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Uměním	umění	k1gNnSc7	umění
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
řada	řada	k1gFnSc1	řada
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
estetika	estetika	k1gFnSc1	estetika
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
specializované	specializovaný	k2eAgInPc1d1	specializovaný
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Estetika	estetika	k1gFnSc1	estetika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
povahou	povaha	k1gFnSc7	povaha
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
sociologie	sociologie	k1gFnSc1	sociologie
jeho	jeho	k3xOp3gFnPc3	jeho
individuálním	individuální	k2eAgFnPc3d1	individuální
a	a	k8xC	a
společenským	společenský	k2eAgNnPc3d1	společenské
působením	působení	k1gNnPc3	působení
či	či	k8xC	či
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
obory	obor	k1gInPc1	obor
(	(	kIx(	(
<g/>
dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
sledují	sledovat	k5eAaImIp3nP	sledovat
vývoj	vývoj	k1gInSc4	vývoj
uměleckých	umělecký	k2eAgInPc2d1	umělecký
projevů	projev	k1gInPc2	projev
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
slohů	sloh	k1gInPc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Sémiotika	sémiotika	k1gFnSc1	sémiotika
chápe	chápat	k5eAaImIp3nS	chápat
umění	umění	k1gNnSc4	umění
jako	jako	k8xS	jako
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
určitých	určitý	k2eAgInPc2d1	určitý
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
<g/>
,	,	kIx,	,
literárních	literární	k2eAgInPc2d1	literární
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
atd.	atd.	kA	atd.
a	a	k8xC	a
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
a	a	k8xC	a
působení	působení	k1gNnSc4	působení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc1	definice
umění	umění	k1gNnSc2	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
mnohotvárný	mnohotvárný	k2eAgInSc1d1	mnohotvárný
a	a	k8xC	a
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
odborníků	odborník	k1gMnPc2	odborník
nelze	lze	k6eNd1	lze
definovat	definovat	k5eAaBmF	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
definici	definice	k1gFnSc4	definice
vycházejí	vycházet	k5eAaImIp3nP	vycházet
buď	buď	k8xC	buď
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
činností	činnost	k1gFnPc2	činnost
samých	samý	k3xTgMnPc6	samý
(	(	kIx(	(
<g/>
umění	umění	k1gNnSc1	umění
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
esteticky	esteticky	k6eAd1	esteticky
působí	působit	k5eAaImIp3nS	působit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
původ	původ	k1gInSc1	původ
z	z	k7c2	z
kultu	kult	k1gInSc2	kult
a	a	k8xC	a
rituálu	rituál	k1gInSc2	rituál
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc1d1	společenský
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgFnPc1d1	umělecká
instituce	instituce	k1gFnPc1	instituce
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
jen	jen	k9	jen
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
určitá	určitý	k2eAgFnSc1d1	určitá
společnost	společnost	k1gFnSc1	společnost
pokládá	pokládat	k5eAaImIp3nS	pokládat
(	(	kIx(	(
<g/>
konstruktivismus	konstruktivismus	k1gInSc4	konstruktivismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
kladl	klást	k5eAaImAgInS	klást
různě	různě	k6eAd1	různě
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
mimetickou	mimetický	k2eAgFnSc4d1	mimetická
či	či	k8xC	či
zobrazovací	zobrazovací	k2eAgFnSc4d1	zobrazovací
stránku	stránka	k1gFnSc4	stránka
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
na	na	k7c4	na
stránku	stránka	k1gFnSc4	stránka
vyjadřovací	vyjadřovací	k2eAgFnSc4d1	vyjadřovací
či	či	k8xC	či
expresivní	expresivní	k2eAgFnSc4d1	expresivní
<g/>
,	,	kIx,	,
sdělovací	sdělovací	k2eAgFnSc4d1	sdělovací
či	či	k8xC	či
komunikativní	komunikativní	k2eAgFnSc4d1	komunikativní
nebo	nebo	k8xC	nebo
tvořivou	tvořivý	k2eAgFnSc4d1	tvořivá
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgNnPc2	tento
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc1	umění
samo	sám	k3xTgNnSc1	sám
sobě	se	k3xPyFc3	se
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
umění	umění	k1gNnSc2	umění
také	také	k9	také
účelově	účelově	k6eAd1	účelově
užívá	užívat	k5eAaImIp3nS	užívat
ke	k	k7c3	k
zdobení	zdobení	k1gNnSc3	zdobení
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyvolávání	vyvolávání	k1gNnSc3	vyvolávání
nálad	nálada	k1gFnPc2	nálada
a	a	k8xC	a
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
určitých	určitý	k2eAgInPc2d1	určitý
postojů	postoj	k1gInPc2	postoj
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
nebo	nebo	k8xC	nebo
v	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Ottova	Ottův	k2eAgInSc2d1	Ottův
slovníku	slovník	k1gInSc2	slovník
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc1	umění
"	"	kIx"	"
<g/>
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
tvoření	tvoření	k1gNnSc1	tvoření
nebo	nebo	k8xC	nebo
konání	konání	k1gNnSc1	konání
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výsledek	výsledek	k1gInSc1	výsledek
nad	nad	k7c4	nad
jiné	jiný	k2eAgInPc4d1	jiný
výtvory	výtvor	k1gInPc4	výtvor
a	a	k8xC	a
výkony	výkon	k1gInPc4	výkon
vyniká	vynikat	k5eAaImIp3nS	vynikat
jistou	jistý	k2eAgFnSc7d1	jistá
hodnotou	hodnota	k1gFnSc7	hodnota
již	již	k6eAd1	již
při	při	k7c6	při
pouhém	pouhý	k2eAgNnSc6d1	pouhé
nazírání	nazírání	k1gNnSc6	nazírání
a	a	k8xC	a
vnímání	vnímání	k1gNnSc6	vnímání
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hodnotou	hodnota	k1gFnSc7	hodnota
estetickou	estetický	k2eAgFnSc7d1	estetická
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopedia	Encyclopedium	k1gNnPc4	Encyclopedium
Britannica	Britannic	k1gInSc2	Britannic
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
umění	umění	k1gNnSc1	umění
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
užívání	užívání	k1gNnSc1	užívání
dovedností	dovednost	k1gFnPc2	dovednost
a	a	k8xC	a
představivosti	představivost	k1gFnSc2	představivost
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
estetických	estetický	k2eAgInPc2d1	estetický
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
prostředí	prostředí	k1gNnSc2	prostředí
nebo	nebo	k8xC	nebo
zážitků	zážitek	k1gInPc2	zážitek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
lze	lze	k6eAd1	lze
sdílet	sdílet	k5eAaImF	sdílet
s	s	k7c7	s
druhými	druhý	k4xOgMnPc7	druhý
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
prof.	prof.	kA	prof.
C.	C.	kA	C.
Tiedemanna	Tiedemanna	k1gFnSc1	Tiedemanna
z	z	k7c2	z
hamburské	hamburský	k2eAgFnSc2d1	hamburská
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc1	umění
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
oblast	oblast	k1gFnSc1	oblast
kulturní	kulturní	k2eAgFnSc2d1	kulturní
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
snaží	snažit	k5eAaImIp3nP	snažit
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
nadání	nadání	k1gNnSc2	nadání
<g/>
,	,	kIx,	,
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
dovedností	dovednost	k1gFnPc2	dovednost
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
city	cit	k1gInPc4	cit
buď	buď	k8xC	buď
vytvářením	vytváření	k1gNnSc7	vytváření
děl	dělo	k1gNnPc2	dělo
nebo	nebo	k8xC	nebo
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Umění	umění	k1gNnSc1	umění
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Umění	umění	k1gNnSc1	umění
provází	provázet	k5eAaImIp3nS	provázet
lidské	lidský	k2eAgFnPc4d1	lidská
společnosti	společnost	k1gFnPc4	společnost
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
například	například	k6eAd1	například
zdobení	zdobení	k1gNnSc1	zdobení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
obydlí	obydlí	k1gNnSc2	obydlí
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeskynní	jeskynní	k2eAgNnSc1d1	jeskynní
malířství	malířství	k1gNnSc1	malířství
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
transu	trans	k1gInSc3	trans
a	a	k8xC	a
užívání	užívání	k1gNnSc3	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
společnostech	společnost	k1gFnPc6	společnost
bylo	být	k5eAaImAgNnS	být
úzce	úzko	k6eAd1	úzko
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
kultem	kult	k1gInSc7	kult
a	a	k8xC	a
rituály	rituál	k1gInPc7	rituál
<g/>
,	,	kIx,	,
s	s	k7c7	s
posvátnými	posvátný	k2eAgNnPc7d1	posvátné
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc7	chrám
a	a	k8xC	a
slavnostmi	slavnost	k1gFnPc7	slavnost
<g/>
,	,	kIx,	,
u	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
a	a	k8xC	a
antických	antický	k2eAgMnPc2d1	antický
básníků	básník	k1gMnPc2	básník
je	být	k5eAaImIp3nS	být
božského	božský	k2eAgInSc2d1	božský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
výraz	výraz	k1gInSc4	výraz
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
prestiže	prestiž	k1gFnSc2	prestiž
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
zejména	zejména	k9	zejména
jako	jako	k9	jako
chrámové	chrámový	k2eAgNnSc4d1	chrámové
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc2d1	hudební
i	i	k8xC	i
divadelní	divadelní	k2eAgFnSc2d1	divadelní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většina	k1gFnSc7	většina
jako	jako	k8xS	jako
anonymní	anonymní	k2eAgFnSc7d1	anonymní
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
obracel	obracet	k5eAaImAgInS	obracet
k	k	k7c3	k
originalitě	originalita	k1gFnSc3	originalita
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
galerie	galerie	k1gFnPc1	galerie
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc1d1	umělecká
akademie	akademie	k1gFnPc1	akademie
a	a	k8xC	a
umělecké	umělecký	k2eAgInPc1d1	umělecký
předměty	předmět	k1gInPc1	předmět
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
předmětem	předmět	k1gInSc7	předmět
sběratelství	sběratelství	k1gNnSc1	sběratelství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
osvícenské	osvícenský	k2eAgNnSc4d1	osvícenské
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
umění	umění	k1gNnSc2	umění
jako	jako	k8xC	jako
výrazu	výraz	k1gInSc2	výraz
harmonie	harmonie	k1gFnSc2	harmonie
(	(	kIx(	(
<g/>
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
romantické	romantický	k2eAgNnSc4d1	romantické
pojetí	pojetí	k1gNnSc4	pojetí
umělce	umělec	k1gMnSc2	umělec
jako	jako	k8xS	jako
génia	génius	k1gMnSc4	génius
reagovali	reagovat	k5eAaBmAgMnP	reagovat
někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
odmítáním	odmítání	k1gNnSc7	odmítání
dobových	dobový	k2eAgFnPc2d1	dobová
konvencí	konvence	k1gFnPc2	konvence
a	a	k8xC	a
akademismu	akademismus	k1gInSc2	akademismus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
protestem	protest	k1gInSc7	protest
a	a	k8xC	a
provokací	provokace	k1gFnSc7	provokace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
umění	umění	k1gNnSc4	umění
nové	nový	k2eAgFnSc2d1	nová
technické	technický	k2eAgFnSc2d1	technická
možnosti	možnost	k1gFnSc2	možnost
jednak	jednak	k8xC	jednak
reprodukce	reprodukce	k1gFnSc2	reprodukce
a	a	k8xC	a
záznamu	záznam	k1gInSc2	záznam
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
nová	nový	k2eAgNnPc1d1	nové
média	médium	k1gNnPc1	médium
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvorby	tvorba	k1gFnSc2	tvorba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
fotografie	fotografie	k1gFnSc1	fotografie
a	a	k8xC	a
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
oceňovat	oceňovat	k5eAaImF	oceňovat
lidové	lidový	k2eAgNnSc4d1	lidové
umění	umění	k1gNnSc4	umění
i	i	k8xC	i
umělecké	umělecký	k2eAgNnSc4d1	umělecké
řemeslo	řemeslo	k1gNnSc4	řemeslo
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
se	se	k3xPyFc4	se
také	také	k9	také
stávalo	stávat	k5eAaImAgNnS	stávat
nástrojem	nástroj	k1gInSc7	nástroj
propagandy	propaganda	k1gFnSc2	propaganda
(	(	kIx(	(
<g/>
plakát	plakát	k1gInSc1	plakát
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
reklama	reklama	k1gFnSc1	reklama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
také	také	k9	také
první	první	k4xOgFnPc1	první
umělecké	umělecký	k2eAgFnPc1d1	umělecká
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
umělecké	umělecký	k2eAgFnPc1d1	umělecká
instituce	instituce	k1gFnPc1	instituce
jako	jako	k9	jako
například	například	k6eAd1	například
výstavy	výstava	k1gFnPc4	výstava
a	a	k8xC	a
festivaly	festival	k1gInPc4	festival
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
umění	umění	k1gNnSc1	umění
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
své	svůj	k3xOyFgFnPc4	svůj
znázorňovací	znázorňovací	k2eAgFnPc4d1	znázorňovací
funkce	funkce	k1gFnPc4	funkce
(	(	kIx(	(
<g/>
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
umění	umění	k1gNnSc4	umění
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
slohu	sloh	k1gInSc2	sloh
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
individualizuje	individualizovat	k5eAaImIp3nS	individualizovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
tlakem	tlak	k1gInSc7	tlak
komercializace	komercializace	k1gFnSc2	komercializace
a	a	k8xC	a
soutěže	soutěž	k1gFnSc2	soutěž
o	o	k7c4	o
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
další	další	k2eAgNnPc1d1	další
nová	nový	k2eAgNnPc1d1	nové
média	médium	k1gNnPc1	médium
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
smazávat	smazávat	k5eAaImF	smazávat
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
uměním	umění	k1gNnSc7	umění
a	a	k8xC	a
běžným	běžný	k2eAgInSc7d1	běžný
životem	život	k1gInSc7	život
(	(	kIx(	(
<g/>
akční	akční	k2eAgNnSc1d1	akční
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
happening	happening	k1gInSc1	happening
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
individuálního	individuální	k2eAgNnSc2d1	individuální
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgNnSc7d1	základní
kritériem	kritérion	k1gNnSc7	kritérion
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
umělecké	umělecký	k2eAgFnSc2d1	umělecká
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gInSc1	jeho
estetický	estetický	k2eAgInSc1d1	estetický
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Estetický	estetický	k2eAgInSc1d1	estetický
účinek	účinek	k1gInSc1	účinek
je	být	k5eAaImIp3nS	být
však	však	k9	však
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
osobní	osobní	k2eAgFnPc4d1	osobní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
požadovat	požadovat	k5eAaImF	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ostatní	ostatní	k2eAgMnPc1d1	ostatní
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
umění	umění	k1gNnSc4	umění
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
co	co	k9	co
my	my	k3xPp1nPc1	my
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sociálního	sociální	k2eAgNnSc2d1	sociální
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
umělecký	umělecký	k2eAgInSc4d1	umělecký
proces	proces	k1gInSc4	proces
<g/>
)	)	kIx)	)
nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
nástrojem	nástroj	k1gInSc7	nástroj
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
nových	nový	k2eAgInPc2d1	nový
či	či	k8xC	či
k	k	k7c3	k
inovaci	inovace	k1gFnSc3	inovace
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
k	k	k7c3	k
ověřování	ověřování	k1gNnSc3	ověřování
jejich	jejich	k3xOp3gFnSc2	jejich
účinnosti	účinnost	k1gFnSc2	účinnost
(	(	kIx(	(
<g/>
umělec	umělec	k1gMnSc1	umělec
ji	on	k3xPp3gFnSc4	on
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
napřed	napřed	k6eAd1	napřed
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
svých	svůj	k3xOyFgMnPc6	svůj
divácích	divák	k1gMnPc6	divák
či	či	k8xC	či
posluchačích	posluchač	k1gMnPc6	posluchač
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ke	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
dohodě	dohoda	k1gFnSc3	dohoda
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
významu	význam	k1gInSc6	význam
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
prosazování	prosazování	k1gNnSc1	prosazování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
osobních	osobní	k2eAgInPc2d1	osobní
účinků	účinek	k1gInPc2	účinek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
umělecký	umělecký	k2eAgInSc4d1	umělecký
znak	znak	k1gInSc4	znak
tak	tak	k6eAd1	tak
po	po	k7c6	po
jisté	jistý	k2eAgFnSc6d1	jistá
době	doba	k1gFnSc6	doba
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
znakového	znakový	k2eAgInSc2d1	znakový
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
obecně	obecně	k6eAd1	obecně
sdíleným	sdílený	k2eAgInSc7d1	sdílený
a	a	k8xC	a
opakovatelně	opakovatelně	k6eAd1	opakovatelně
vyvolavatelným	vyvolavatelný	k2eAgInSc7d1	vyvolavatelný
významem	význam	k1gInSc7	význam
a	a	k8xC	a
posiluje	posilovat	k5eAaImIp3nS	posilovat
tak	tak	k6eAd1	tak
užívaný	užívaný	k2eAgInSc1d1	užívaný
komunikační	komunikační	k2eAgInSc1d1	komunikační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
metaforou	metafora	k1gFnSc7	metafora
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
nový	nový	k2eAgInSc1d1	nový
znak	znak	k1gInSc1	znak
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
inovoval	inovovat	k5eAaBmAgInS	inovovat
vztahy	vztah	k1gInPc1	vztah
ke	k	k7c3	k
znakům	znak	k1gInPc3	znak
na	na	k7c4	na
něj	on	k3xPp3gMnSc2	on
návazným	návazný	k2eAgMnPc3d1	návazný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abychom	aby	kYmCp1nP	aby
umělecký	umělecký	k2eAgInSc4d1	umělecký
(	(	kIx(	(
<g/>
metaforický	metaforický	k2eAgInSc4d1	metaforický
<g/>
)	)	kIx)	)
účinek	účinek	k1gInSc4	účinek
dokázali	dokázat	k5eAaPmAgMnP	dokázat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
se	se	k3xPyFc4	se
vědomě	vědomě	k6eAd1	vědomě
nebo	nebo	k8xC	nebo
nevědomky	nevědomky	k6eAd1	nevědomky
naučit	naučit	k5eAaPmF	naučit
dosavadním	dosavadní	k2eAgInPc3d1	dosavadní
znakovým	znakový	k2eAgInPc3d1	znakový
systémům	systém	k1gInPc3	systém
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
umělecký	umělecký	k2eAgInSc4d1	umělecký
znak	znak	k1gInSc4	znak
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
výchova	výchova	k1gFnSc1	výchova
k	k	k7c3	k
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Cumming	Cumming	k1gInSc1	Cumming
<g/>
,	,	kIx,	,
Umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
26	[number]	k4	26
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
170	[number]	k4	170
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Pijoán	Pijoán	k1gInSc1	Pijoán
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc1	umění
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1977-1982	[number]	k4	1977-1982
(	(	kIx(	(
<g/>
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
1998	[number]	k4	1998
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ritter	Ritter	k1gMnSc1	Ritter
–	–	k?	–
Gründer	gründer	k1gMnSc1	gründer
<g/>
,	,	kIx,	,
Historisches	Historisches	k1gMnSc1	Historisches
Wörterbuch	Wörterbuch	k1gMnSc1	Wörterbuch
der	drát	k5eAaImRp2nS	drát
Philosophie	Philosophie	k1gFnPc1	Philosophie
<g/>
.	.	kIx.	.
</s>
<s>
Basel	Basel	k1gInSc1	Basel
1981	[number]	k4	1981
<g/>
nn	nn	k?	nn
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc1	heslo
Kunst	Kunst	k1gInSc1	Kunst
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
,	,	kIx,	,
sl.	sl.	k?	sl.
1357	[number]	k4	1357
<g/>
nn	nn	k?	nn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umění	umění	k1gNnSc1	umění
<g/>
:	:	kIx,	:
velký	velký	k2eAgMnSc1d1	velký
obrazový	obrazový	k2eAgMnSc1d1	obrazový
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
uměleckých	umělecký	k2eAgInPc2d1	umělecký
směrů	směr	k1gInPc2	směr
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Užité	užitý	k2eAgNnSc1d1	užité
umění	umění	k1gNnSc1	umění
</s>
</p>
<p>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
</s>
</p>
<p>
<s>
Nová	nový	k2eAgNnPc1d1	nové
média	médium	k1gNnPc1	médium
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
umění	umění	k1gNnSc2	umění
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dictionary	Dictionara	k1gFnPc1	Dictionara
of	of	k?	of
the	the	k?	the
History	Histor	k1gInPc1	Histor
of	of	k?	of
ideas	ideas	k1gInSc1	ideas
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Art	Art	k1gFnSc2	Art
and	and	k?	and
Play	play	k0	play
<g/>
,	,	kIx,	,
etext	etext	k1gMnSc1	etext
<g/>
.	.	kIx.	.
<g/>
lib	líbit	k5eAaImRp2nS	líbit
<g/>
.	.	kIx.	.
<g/>
virginia	virginium	k1gNnSc2	virginium
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Portál	portál	k1gInSc1	portál
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
witcombe	witcombat	k5eAaPmIp3nS	witcombat
<g/>
.	.	kIx.	.
<g/>
sbc	sbc	k?	sbc
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stanford	Stanford	k1gInSc1	Stanford
encyclopedia	encyclopedium	k1gNnSc2	encyclopedium
of	of	k?	of
philosophy	philosoph	k1gInPc1	philosoph
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Art	Art	k1gMnPc2	Art
definitions	definitions	k1gInSc4	definitions
<g/>
,	,	kIx,	,
plato	plato	k1gNnSc4	plato
<g/>
.	.	kIx.	.
<g/>
stanford	stanford	k1gInSc1	stanford
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
</s>
</p>
