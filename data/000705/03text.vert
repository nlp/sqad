<s>
Oreb	orba	k1gFnPc2	orba
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
okrese	okres	k1gInSc6	okres
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
260	[number]	k4	260
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
256,3	[number]	k4	256,3
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obtéká	obtékat	k5eAaImIp3nS	obtékat
řeka	řeka	k1gFnSc1	řeka
Dědina	dědina	k1gFnSc1	dědina
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
po	po	k7c6	po
biblické	biblický	k2eAgFnSc6d1	biblická
hoře	hora	k1gFnSc6	hora
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
r.	r.	kA	r.
1419	[number]	k4	1419
od	od	k7c2	od
shromážděných	shromážděný	k2eAgFnPc2d1	shromážděná
Husových	Husová	k1gFnPc2	Husová
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starší	starý	k2eAgFnSc2d2	starší
kaple	kaple	k1gFnSc2	kaple
a	a	k8xC	a
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
r.	r.	kA	r.
1835	[number]	k4	1835
postaven	postavit	k5eAaPmNgInS	postavit
kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
hory	hora	k1gFnSc2	hora
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
připojilo	připojit	k5eAaPmAgNnS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
jménu	jméno	k1gNnSc3	jméno
nedaleké	daleký	k2eNgNnSc4d1	nedaleké
město	město	k1gNnSc4	město
Třebechovice	Třebechovice	k1gFnPc1	Třebechovice
pod	pod	k7c7	pod
Orebem	Oreb	k1gInSc7	Oreb
<g/>
.	.	kIx.	.
</s>
<s>
Oreb	orba	k1gFnPc2	orba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Třebechovice	Třebechovice	k1gFnPc1	Třebechovice
pod	pod	k7c7	pod
Orebem	Oreb	k1gInSc7	Oreb
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nevýrazný	výrazný	k2eNgInSc1d1	nevýrazný
malý	malý	k2eAgInSc1d1	malý
svědecký	svědecký	k2eAgInSc1d1	svědecký
vrch	vrch	k1gInSc1	vrch
je	být	k5eAaImIp3nS	být
krytý	krytý	k2eAgInSc1d1	krytý
říčními	říční	k2eAgInPc7d1	říční
štěrky	štěrk	k1gInPc7	štěrk
a	a	k8xC	a
písky	písek	k1gInPc1	písek
středopleistocenní	středopleistocenní	k2eAgFnSc2d1	středopleistocenní
terasy	terasa	k1gFnSc2	terasa
Dědiny	dědina	k1gFnSc2	dědina
<g/>
,	,	kIx,	,
spočívajícími	spočívající	k2eAgFnPc7d1	spočívající
na	na	k7c6	na
slínovcích	slínovec	k1gInPc6	slínovec
a	a	k8xC	a
jílovcích	jílovec	k1gInPc6	jílovec
svrchního	svrchní	k2eAgInSc2d1	svrchní
turonu	turon	k1gInSc2	turon
až	až	k6eAd1	až
do	do	k7c2	do
koniaku	koniak	k1gInSc2	koniak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nezalesněných	zalesněný	k2eNgInPc6d1	nezalesněný
svazích	svah	k1gInPc6	svah
rostou	růst	k5eAaImIp3nP	růst
pouze	pouze	k6eAd1	pouze
místy	místy	k6eAd1	místy
borové	borový	k2eAgInPc4d1	borový
porosty	porost	k1gInPc4	porost
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
smrku	smrk	k1gInSc2	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Ční	čnět	k5eAaImIp3nS	čnět
necelých	celý	k2eNgInPc2d1	necelý
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
říčky	říčka	k1gFnSc2	říčka
Dědiny	dědina	k1gFnSc2	dědina
nedalekoho	dalekoze	k6eNd1	dalekoze
jejího	její	k3xOp3gNnSc2	její
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Orlice	Orlice	k1gFnSc2	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
asymetrické	asymetrický	k2eAgFnSc6d1	asymetrická
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
Dědina	dědina	k1gFnSc1	dědina
výrazně	výrazně	k6eAd1	výrazně
podepsala	podepsat	k5eAaPmAgFnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Boční	boční	k2eAgFnSc7d1	boční
erozí	eroze	k1gFnSc7	eroze
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jeho	jeho	k3xOp3gInPc4	jeho
východní	východní	k2eAgInPc4d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgInPc4d1	jižní
svahy	svah	k1gInPc4	svah
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
jediné	jediný	k2eAgFnPc1d1	jediná
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
výrazné	výrazný	k2eAgFnPc4d1	výrazná
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
širokém	široký	k2eAgNnSc6d1	široké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInPc1d1	severní
a	a	k8xC	a
západní	západní	k2eAgInPc1d1	západní
svahy	svah	k1gInPc1	svah
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
tabuli	tabule	k1gFnSc4	tabule
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
mírné	mírný	k2eAgFnPc1d1	mírná
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgFnPc1d1	nízká
a	a	k8xC	a
táhlé	táhlý	k2eAgFnPc1d1	táhlá
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nejstrmějším	strmý	k2eAgInSc7d3	nejstrmější
<g/>
,	,	kIx,	,
východním	východní	k2eAgInSc7d1	východní
svahem	svah	k1gInSc7	svah
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
Dědina	dědina	k1gFnSc1	dědina
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
svahem	svah	k1gInSc7	svah
stoupá	stoupat	k5eAaImIp3nS	stoupat
kamenné	kamenný	k2eAgNnSc1d1	kamenné
schodiště	schodiště	k1gNnSc1	schodiště
od	od	k7c2	od
Třebechovic	Třebechovice	k1gFnPc2	Třebechovice
až	až	k9	až
po	po	k7c4	po
temeno	temeno	k1gNnSc4	temeno
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
stojí	stát	k5eAaImIp3nS	stát
kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
vrch	vrch	k1gInSc1	vrch
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
celku	celek	k1gInSc2	celek
Orlická	orlický	k2eAgFnSc1d1	Orlická
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
podcelku	podcelka	k1gFnSc4	podcelka
Třebechovická	třebechovický	k2eAgFnSc1d1	Třebechovická
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc6	okrsek
Choceňská	choceňský	k2eAgFnSc1d1	Choceňská
plošina	plošina	k1gFnSc1	plošina
a	a	k8xC	a
podokrsku	podokrsko	k1gNnSc6	podokrsko
Týnišťská	týnišťský	k2eAgFnSc1d1	Týnišťská
kotlina	kotlina	k1gFnSc1	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
alternativního	alternativní	k2eAgNnSc2d1	alternativní
členění	členění	k1gNnSc2	členění
Demka	Demek	k1gInSc2	Demek
a	a	k8xC	a
Mackovčina	Mackovčin	k2eAgFnSc1d1	Mackovčin
náleží	náležet	k5eAaImIp3nS	náležet
vrch	vrch	k1gInSc4	vrch
do	do	k7c2	do
okrsku	okrsek	k1gInSc2	okrsek
Bědovická	Bědovický	k2eAgFnSc1d1	Bědovický
plošina	plošina	k1gFnSc1	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
hora	hora	k1gFnSc1	hora
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Vinice	vinice	k1gFnPc4	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
slínovcových	slínovcův	k2eAgInPc6d1	slínovcův
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
významu	význam	k1gInSc3	význam
jako	jako	k8xS	jako
poutního	poutní	k2eAgNnSc2d1	poutní
místa	místo	k1gNnSc2	místo
přispěl	přispět	k5eAaPmAgInS	přispět
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
stála	stát	k5eAaImAgFnS	stát
menší	malý	k2eAgFnSc1d2	menší
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1419	[number]	k4	1419
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
shromáždění	shromáždění	k1gNnSc6	shromáždění
příznivců	příznivec	k1gMnPc2	příznivec
husitského	husitský	k2eAgNnSc2d1	husitské
hnutí	hnutí	k1gNnSc2	hnutí
z	z	k7c2	z
východních	východní	k2eAgFnPc2d1	východní
Čech	Čechy	k1gFnPc2	Čechy
kněz	kněz	k1gMnSc1	kněz
Ambrož	Ambrož	k1gMnSc1	Ambrož
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
ke	k	k7c3	k
vzpouře	vzpoura	k1gFnSc3	vzpoura
proti	proti	k7c3	proti
purkrabímu	purkrabí	k1gMnSc3	purkrabí
Jindřichu	Jindřich	k1gMnSc3	Jindřich
z	z	k7c2	z
Vartemberka	Vartemberka	k1gFnSc1	Vartemberka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
hory	hora	k1gFnSc2	hora
na	na	k7c6	na
Oreb	orba	k1gFnPc2	orba
podle	podle	k7c2	podle
biblické	biblický	k2eAgFnSc2d1	biblická
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
zjevil	zjevit	k5eAaPmAgMnS	zjevit
Bůh	bůh	k1gMnSc1	bůh
Mojžíšovi	Mojžíšův	k2eAgMnPc1d1	Mojžíšův
v	v	k7c6	v
hořícím	hořící	k2eAgInSc6d1	hořící
keři	keř	k1gInSc6	keř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hoře	hora	k1gFnSc6	hora
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
pojmenovalo	pojmenovat	k5eAaPmAgNnS	pojmenovat
i	i	k8xC	i
místní	místní	k2eAgNnSc4d1	místní
vojenské	vojenský	k2eAgNnSc4d1	vojenské
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
Orebské	orebský	k2eAgFnSc2d1	Orebská
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Orebité	orebita	k1gMnPc1	orebita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
a	a	k8xC	a
Táborském	táborský	k2eAgInSc6d1	táborský
stalo	stát	k5eAaPmAgNnS	stát
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
Hynek	Hynek	k1gMnSc1	Hynek
Krušina	krušina	k1gFnSc1	krušina
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
konal	konat	k5eAaImAgInS	konat
tábor	tábor	k1gInSc1	tábor
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
severovýchodních	severovýchodní	k2eAgFnPc2d1	severovýchodní
Čech	Čechy	k1gFnPc2	Čechy
přes	přes	k7c4	přes
zákrok	zákrok	k1gInSc4	zákrok
četnictva	četnictvo	k1gNnSc2	četnictvo
a	a	k8xC	a
vojska	vojsko	k1gNnSc2	vojsko
postavili	postavit	k5eAaPmAgMnP	postavit
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Orebu	Oreb	k1gInSc2	Oreb
se	se	k3xPyFc4	se
také	také	k9	také
pojmenovalo	pojmenovat	k5eAaPmAgNnS	pojmenovat
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
Orebem	Oreb	k1gInSc7	Oreb
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
Oreb	orba	k1gFnPc2	orba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
hory	hora	k1gFnSc2	hora
již	již	k6eAd1	již
před	před	k7c7	před
dobou	doba	k1gFnSc7	doba
husitskou	husitský	k2eAgFnSc7d1	husitská
stála	stát	k5eAaImAgFnS	stát
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
z	z	k7c2	z
jinak	jinak	k6eAd1	jinak
nevýrazné	výrazný	k2eNgFnSc2d1	nevýrazná
hory	hora	k1gFnSc2	hora
stalo	stát	k5eAaPmAgNnS	stát
poutní	poutní	k2eAgNnSc1d1	poutní
místo	místo	k1gNnSc1	místo
husitů	husita	k1gMnPc2	husita
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
původní	původní	k2eAgFnSc1d1	původní
kaple	kaple	k1gFnSc1	kaple
na	na	k7c4	na
Orebu	Oreba	k1gFnSc4	Oreba
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Mohla	moct	k5eAaImAgFnS	moct
zaniknout	zaniknout	k5eAaPmF	zaniknout
během	během	k7c2	během
husitství	husitství	k1gNnSc2	husitství
či	či	k8xC	či
kvůli	kvůli	k7c3	kvůli
výstavbě	výstavba	k1gFnSc3	výstavba
nového	nový	k2eAgInSc2d1	nový
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
kostelíka	kostelík	k1gInSc2	kostelík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
roku	rok	k1gInSc2	rok
1528	[number]	k4	1528
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
třebechovické	třebechovický	k2eAgNnSc4d1	Třebechovické
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Kostelík	kostelík	k1gInSc1	kostelík
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgInSc3d1	špatný
technickému	technický	k2eAgInSc3d1	technický
stavu	stav	k1gInSc3	stav
zbořen	zbořit	k5eAaPmNgInS	zbořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
současný	současný	k2eAgInSc1d1	současný
kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
za	za	k7c2	za
velkého	velký	k2eAgNnSc2d1	velké
přispění	přispění	k1gNnSc2	přispění
třebechovického	třebechovický	k2eAgMnSc2d1	třebechovický
rodáka	rodák	k1gMnSc2	rodák
doktora	doktor	k1gMnSc2	doktor
Jana	Jan	k1gMnSc2	Jan
Theobalda	Theobald	k1gMnSc2	Theobald
Helda	Held	k1gMnSc2	Held
<g/>
.	.	kIx.	.
</s>
<s>
Held	Held	k1gMnSc1	Held
původně	původně	k6eAd1	původně
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
podobu	podoba	k1gFnSc4	podoba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
římského	římský	k2eAgInSc2d1	římský
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
však	však	k9	však
Třebechovice	Třebechovice	k1gFnPc1	Třebechovice
zamítly	zamítnout	k5eAaPmAgFnP	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
klasicistního	klasicistní	k2eAgInSc2d1	klasicistní
kostela	kostel	k1gInSc2	kostel
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
kamenném	kamenný	k2eAgInSc6d1	kamenný
podstavci	podstavec	k1gInSc6	podstavec
železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
upravený	upravený	k2eAgInSc1d1	upravený
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pilířích	pilíř	k1gInPc6	pilíř
hřbitovní	hřbitovní	k2eAgFnSc2d1	hřbitovní
brány	brána	k1gFnSc2	brána
najdeme	najít	k5eAaPmIp1nP	najít
zazděné	zazděný	k2eAgInPc1d1	zazděný
dva	dva	k4xCgInPc1	dva
náhrobníky	náhrobník	k1gInPc1	náhrobník
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
během	během	k7c2	během
mobilizace	mobilizace	k1gFnSc2	mobilizace
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
kostela	kostel	k1gInSc2	kostel
hláska	hláska	k1gFnSc1	hláska
protiletecké	protiletecký	k2eAgFnSc2d1	protiletecká
obrany	obrana	k1gFnSc2	obrana
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
