<s>
Oreb	orba	k1gFnPc2	orba
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
okrese	okres	k1gInSc6	okres
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
260	[number]	k4	260
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
256,3	[number]	k4	256,3
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obtéká	obtékat	k5eAaImIp3nS	obtékat
řeka	řeka	k1gFnSc1	řeka
Dědina	dědina	k1gFnSc1	dědina
<g/>
.	.	kIx.	.
</s>
