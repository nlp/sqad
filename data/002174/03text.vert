<s>
Augusta	Augusta	k1gMnSc1	Augusta
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Kennebec	Kennebec	k1gMnSc1	Kennebec
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
Maine	Main	k1gMnSc5	Main
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
18	[number]	k4	18
560	[number]	k4	560
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejseverovýchodnější	severovýchodný	k2eAgNnSc4d3	severovýchodný
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
osídlena	osídlit	k5eAaPmNgFnS	osídlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
angličtí	anglický	k2eAgMnPc1d1	anglický
kolonisté	kolonista	k1gMnPc1	kolonista
z	z	k7c2	z
kolonie	kolonie	k1gFnSc2	kolonie
Plymouth	Plymouth	k1gInSc1	Plymouth
založili	založit	k5eAaPmAgMnP	založit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
osadu	osada	k1gFnSc4	osada
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Kennebec	Kennebec	k1gInSc4	Kennebec
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
indiánský	indiánský	k2eAgInSc1d1	indiánský
název	název	k1gInSc1	název
oblasti	oblast	k1gFnSc2	oblast
zněl	znět	k5eAaImAgInS	znět
Cushnoc	Cushnoc	k1gInSc1	Cushnoc
(	(	kIx(	(
<g/>
také	také	k9	také
Coussinoc	Coussinoc	k1gFnSc4	Coussinoc
nebo	nebo	k8xC	nebo
Koussinoc	Koussinoc	k1gFnSc4	Koussinoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
kožešinou	kožešina	k1gFnSc7	kožešina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
obchod	obchod	k1gInSc4	obchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
vlivem	vlivem	k7c2	vlivem
indiánských	indiánský	k2eAgInPc2d1	indiánský
nájezdů	nájezd	k1gInPc2	nájezd
a	a	k8xC	a
poklesu	pokles	k1gInSc2	pokles
tržby	tržba	k1gFnSc2	tržba
pozastaven	pozastavit	k5eAaPmNgMnS	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgNnPc2d1	následující
75	[number]	k4	75
let	léto	k1gNnPc2	léto
zůstala	zůstat	k5eAaPmAgFnS	zůstat
oblast	oblast	k1gFnSc1	oblast
prázdná	prázdný	k2eAgFnSc1d1	prázdná
a	a	k8xC	a
vysídlená	vysídlený	k2eAgFnSc1d1	vysídlená
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
hrála	hrát	k5eAaImAgFnS	hrát
také	také	k9	také
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
během	během	k7c2	během
indiánských	indiánský	k2eAgNnPc2d1	indiánské
povstání	povstání	k1gNnPc2	povstání
vůči	vůči	k7c3	vůči
osadníkům	osadník	k1gMnPc3	osadník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgFnS	nacházet
pevnost	pevnost	k1gFnSc1	pevnost
britských	britský	k2eAgMnPc2d1	britský
osadníků	osadník	k1gMnPc2	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Cushnoc	Cushnoc	k1gFnSc1	Cushnoc
<g/>
,	,	kIx,	,
administrativně	administrativně	k6eAd1	administrativně
přidružována	přidružován	k2eAgFnSc1d1	přidružován
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
okolním	okolní	k2eAgFnPc3d1	okolní
osadám	osada	k1gFnPc3	osada
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
měnila	měnit	k5eAaImAgFnS	měnit
také	také	k9	také
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
se	se	k3xPyFc4	se
osada	osada	k1gFnSc1	osada
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
dnešní	dnešní	k2eAgInSc4d1	dnešní
název	název	k1gInSc4	název
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Augustě	Augusta	k1gFnSc3	Augusta
Dearbornové	Dearbornový	k2eAgFnSc2d1	Dearbornový
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc3	dcera
místního	místní	k2eAgMnSc2d1	místní
veterána	veterán	k1gMnSc2	veterán
z	z	k7c2	z
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Henryho	Henry	k1gMnSc2	Henry
Dearborna	Dearborno	k1gNnSc2	Dearborno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Augusta	Augusta	k1gMnSc1	Augusta
administrativním	administrativní	k2eAgMnSc7d1	administrativní
centrem	centr	k1gMnSc7	centr
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
okresu	okres	k1gInSc2	okres
Kennebeck	Kennebecka	k1gFnPc2	Kennebecka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
Maine	Main	k1gInSc5	Main
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Statut	statut	k1gInSc1	statut
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
19	[number]	k4	19
136	[number]	k4	136
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
94,1	[number]	k4	94,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
1,1	[number]	k4	1,1
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Augustě	Augusta	k1gFnSc6	Augusta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
University	universita	k1gFnSc2	universita
of	of	k?	of
Maine	Main	k1gInSc5	Main
at	at	k?	at
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
státu	stát	k1gInSc2	stát
Maine	Main	k1gInSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
Pine	pin	k1gInSc5	pin
Tree	Treus	k1gInSc5	Treus
State	status	k1gInSc5	status
Arboretum	arboretum	k1gNnSc1	arboretum
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
přes	přes	k7c4	přes
90,6	[number]	k4	90,6
ha	ha	kA	ha
<g/>
,	,	kIx,	,
a	a	k8xC	a
státní	státní	k2eAgNnSc1d1	státní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
rodným	rodný	k2eAgNnSc7d1	rodné
místem	místo	k1gNnSc7	místo
senátorky	senátorka	k1gFnSc2	senátorka
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Maine	Main	k1gInSc5	Main
Olympie	Olympia	k1gFnPc1	Olympia
Snowe	Snow	k1gMnSc2	Snow
a	a	k8xC	a
guvernéra	guvernér	k1gMnSc2	guvernér
Josepha	Joseph	k1gMnSc2	Joseph
Williamse	Williams	k1gMnSc2	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
papírnický	papírnický	k2eAgInSc1d1	papírnický
<g/>
,	,	kIx,	,
ocelářský	ocelářský	k2eAgInSc1d1	ocelářský
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
počítačový	počítačový	k2eAgInSc1d1	počítačový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
