<s>
Annie	Annie	k1gFnSc1
Award	Awarda	k1gFnPc2
</s>
<s>
Annie	Annie	k1gFnSc1
Award	Awarda	k1gFnPc2
je	být	k5eAaImIp3nS
americké	americký	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
za	za	k7c4
úspěchy	úspěch	k1gInPc4
v	v	k7c6
animaci	animace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
udělované	udělovaný	k2eAgNnSc1d1
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
pobočkou	pobočka	k1gFnSc7
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
animovaného	animovaný	k2eAgInSc2d1
filmu	film	k1gInSc2
od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
bylo	být	k5eAaImAgNnS
navrženo	navrhnout	k5eAaPmNgNnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
hodnotilo	hodnotit	k5eAaImAgNnS
celoživotní	celoživotní	k2eAgNnSc1d1
příspěvky	příspěvek	k1gInPc7
animaci	animace	k1gFnSc4
v	v	k7c6
oblastech	oblast	k1gFnPc6
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc2
<g/>
,	,	kIx,
animace	animace	k1gFnSc2
<g/>
,	,	kIx,
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
scénáře	scénář	k1gInSc2
<g/>
,	,	kIx,
dabingu	dabing	k1gInSc2
<g/>
,	,	kIx,
zvuku	zvuk	k1gInSc2
a	a	k8xC
zvukových	zvukový	k2eAgInPc2d1
efektů	efekt	k1gInPc2
atd.	atd.	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c4
počest	počest	k1gFnSc4
animace	animace	k1gFnSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
vytvořena	vytvořen	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgInSc4d3
animovaný	animovaný	k2eAgInSc4d1
celovečerní	celovečerní	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k1gMnSc1
Animated	Animated	k1gMnSc1
Feature	Featur	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c4
ASIFA-Hollywood	ASIFA-Hollywood	k1gInSc4
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
všeobecný	všeobecný	k2eAgInSc1d1
člen	člen	k1gInSc1
<g/>
,	,	kIx,
patron	patron	k1gMnSc1
a	a	k8xC
student	student	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstup	vstup	k1gInSc1
do	do	k7c2
ASIFA-Hollywood	ASIFA-Hollywooda	k1gFnPc2
je	být	k5eAaImIp3nS
po	po	k7c6
zaplacení	zaplacení	k1gNnSc6
členského	členský	k2eAgInSc2d1
poplatku	poplatek	k1gInSc2
umožněn	umožnit	k5eAaPmNgInS
profesionálům	profesionál	k1gMnPc3
<g/>
,	,	kIx,
studentům	student	k1gMnPc3
i	i	k8xC
fanouškům	fanoušek	k1gMnPc3
animace	animace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
ASIFA-Hollywood	ASIFA-Hollywooda	k1gFnPc2
mohou	moct	k5eAaImIp3nP
hlasovat	hlasovat	k5eAaImF
o	o	k7c6
vítězi	vítěz	k1gMnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třicátý	třicátý	k4xOgInSc4
devátý	devátý	k4xOgInSc4
ročník	ročník	k1gInSc4
ocenění	ocenění	k1gNnSc4
Annie	Annie	k1gFnSc2
Award	Awarda	k1gFnPc2
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
v	v	k7c6
kampusu	kampus	k1gInSc6
Univerzity	univerzita	k1gFnSc2
Kalifornie	Kalifornie	k1gFnSc2
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Produkce	produkce	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgInSc4d3
animovaný	animovaný	k2eAgInSc4d1
celovečerní	celovečerní	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k1gMnSc1
Animated	Animated	k1gMnSc1
Feature	Featur	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgFnPc4d3
animace	animace	k1gFnPc4
pro	pro	k7c4
domácí	domácí	k2eAgFnSc4d1
zábavu	zábava	k1gFnSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k1gMnSc1
Animated	Animated	k1gMnSc1
Home	Hom	k1gFnSc2
Entertainment	Entertainment	k1gMnSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgInSc4d3
animovaný	animovaný	k2eAgInSc4d1
krátký	krátký	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k1gMnSc1
Animated	Animated	k1gMnSc1
Short	Shorta	k1gFnPc2
Subject	Subject	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3
animovaná	animovaný	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
reklama	reklama	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k2eAgInSc1d1
Animated	Animated	k1gInSc1
Television	Television	k1gInSc1
Commercial	Commercial	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Nejlépe	dobře	k6eAd3
animovaná	animovaný	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k2eAgInSc1d1
Animated	Animated	k1gInSc1
Television	Television	k1gInSc1
Production	Production	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3
animovaná	animovaný	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Best	Best	k2eAgInSc1d1
Animated	Animated	k1gInSc1
Video	video	k1gNnSc1
Game	game	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Individuální	individuální	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
</s>
<s>
„	„	k?
<g/>
Animované	animovaný	k2eAgInPc4d1
efekty	efekt	k1gInPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Animated	Animated	k1gInSc1
Effects	Effects	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Animovaná	animovaný	k2eAgFnSc1d1
postava	postava	k1gFnSc1
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Character	Character	k1gInSc1
Animation	Animation	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Animovaná	animovaný	k2eAgFnSc1d1
postava	postava	k1gFnSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Character	Character	k1gInSc1
Animation	Animation	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Design	design	k1gInSc1
postavy	postava	k1gFnSc2
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Character	Character	k1gInSc1
Design	design	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Design	design	k1gInSc1
postavy	postava	k1gFnSc2
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Character	Character	k1gInSc1
Design	design	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Režie	režie	k1gFnSc1
v	v	k7c6
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Directing	Directing	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Režie	režie	k1gFnSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Directing	Directing	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Hudba	hudba	k1gFnSc1
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Music	Music	k1gMnSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Hudba	hudba	k1gFnSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Music	Music	k1gMnSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Produkce	produkce	k1gFnSc1
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Production	Production	k1gInSc1
Design	design	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Produkce	produkce	k1gFnSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Production	Production	k1gInSc1
Design	design	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Výprava	výprava	k1gMnSc1
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Storyboarding	Storyboarding	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Výprava	výprava	k1gMnSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Storyboarding	Storyboarding	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Dabing	dabing	k1gInSc1
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Voice	Voice	k1gMnSc2
Acting	Acting	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Dabing	dabing	k1gInSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Voice	Voice	k1gMnSc2
Acting	Acting	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Scénář	scénář	k1gInSc1
v	v	k7c6
celovečerní	celovečerní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Writing	Writing	k1gInSc1
in	in	k?
a	a	k8xC
Feature	Featur	k1gMnSc5
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Scénář	scénář	k1gInSc1
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Writing	Writing	k1gInSc1
in	in	k?
a	a	k8xC
Television	Television	k1gInSc1
Production	Production	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
poroty	porota	k1gFnSc2
</s>
<s>
„	„	k?
<g/>
Cena	cena	k1gFnSc1
June	jun	k1gMnSc5
Forayové	Forayové	k2eAgMnPc3d1
<g/>
“	“	k?
(	(	kIx(
<g/>
June	jun	k1gMnSc5
Foray	Foray	k1gInPc4
Award	Award	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Cena	cena	k1gFnSc1
Uba	Uba	k1gFnSc1
Iwerkse	Iwerkse	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Ub	Ub	k1gMnSc1
Iwerks	Iwerksa	k1gFnPc2
Award	Award	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Cena	cena	k1gFnSc1
Winsora	Winsora	k1gFnSc1
McCay	McCaa	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Winsor	Winsor	k1gMnSc1
McCay	McCaa	k1gFnSc2
Award	Award	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Zvláštní	zvláštní	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
v	v	k7c6
animaci	animace	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Special	Special	k1gMnSc1
Achievement	Achievement	k1gMnSc1
in	in	k?
Animation	Animation	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Cena	cena	k1gFnSc1
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Certificates	Certificates	k1gMnSc1
of	of	k?
Merit	meritum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Významné	významný	k2eAgFnPc1d1
nominace	nominace	k1gFnPc1
</s>
<s>
Díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
obdržela	obdržet	k5eAaPmAgFnS
nejvíce	nejvíce	k6eAd1,k6eAd3
nominací	nominace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
PočetnominacíČeský	PočetnominacíČeský	k2eAgInSc1d1
názevRokOriginální	názevRokOriginální	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
Úžasňákovi	Úžasňák	k1gMnSc3
<g/>
2004	#num#	k4
<g/>
The	The	k1gFnSc2
Incredibles	Incrediblesa	k1gFnPc2
</s>
<s>
16	#num#	k4
<g/>
Wallace	Wallace	k1gFnSc1
&	&	k?
Gromit	Gromit	k1gInSc1
<g/>
:	:	kIx,
Prokletí	prokletí	k1gNnSc1
králíkodlaka	králíkodlak	k1gMnSc2
<g/>
2005	#num#	k4
<g/>
Wallace	Wallace	k1gFnSc2
&	&	k?
Gromit	Gromit	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Curse	Curse	k1gFnSc2
of	of	k?
the	the	k?
Were-Rabbit	Were-Rabbit	k1gFnSc2
</s>
<s>
16	#num#	k4
<g/>
Kung	Kung	k1gInSc1
Fu	fu	k0
Panda	panda	k1gFnSc1
<g/>
2008	#num#	k4
<g/>
Kung	Kunga	k1gFnPc2
Fu	fu	k0
Panda	panda	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
Jak	jak	k6eAd1
vycvičit	vycvičit	k5eAaPmF
draka	drak	k1gMnSc4
<g/>
2010	#num#	k4
<g/>
How	How	k1gMnSc1
To	to	k9
Train	Train	k2eAgMnSc1d1
Your	Your	k1gMnSc1
Dragon	Dragon	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
Ratatouille	Ratatouille	k1gNnSc1
<g/>
2007	#num#	k4
<g/>
Ratatouille	Ratatouille	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
Zvoník	zvoník	k1gMnSc1
od	od	k7c2
Matky	matka	k1gFnSc2
Boží	boží	k2eAgFnSc2d1
<g/>
1996	#num#	k4
<g/>
WThe	WTh	k1gFnSc2
Hunchback	Hunchbacka	k1gFnPc2
of	of	k?
Notre	Notr	k1gMnSc5
Dame	Damus	k1gMnSc5
</s>
<s>
12	#num#	k4
<g/>
Kung	Kung	k1gInSc1
Fu	fu	k0
Panda	panda	k1gFnSc1
22011	#num#	k4
<g/>
Kung	Kunga	k1gFnPc2
Fu	fu	k0
Panda	panda	k1gFnSc1
2	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
Hledá	hledat	k5eAaImIp3nS
se	se	k3xPyFc4
Nemo	Nemo	k1gMnSc1
<g/>
2003	#num#	k4
<g/>
Finding	Finding	k1gInSc1
Nemo	Nemo	k6eAd1
</s>
<s>
10	#num#	k4
<g/>
Koralína	Koralín	k1gInSc2
a	a	k8xC
svět	svět	k1gInSc1
za	za	k7c7
tajnými	tajný	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
<g/>
2009	#num#	k4
<g/>
Coraline	Coralin	k1gInSc5
</s>
<s>
10	#num#	k4
<g/>
Raubíř	raubíř	k1gMnSc1
Ralf	Ralf	k1gMnSc1
<g/>
2012	#num#	k4
<g/>
Wreck-It	Wreck-Itum	k1gNnPc2
Ralph	Ralpha	k1gFnPc2
</s>
<s>
10	#num#	k4
<g/>
Legenda	legenda	k1gFnSc1
o	o	k7c4
Mulan	Mulan	k1gInSc4
<g/>
1998	#num#	k4
<g/>
Mulan	Mulana	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Annie	Annie	k1gFnSc2
Award	Awarda	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.asifa-hollywood.org/about-us/	http://www.asifa-hollywood.org/about-us/	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Annie	Annie	k1gFnSc2
Award	Award	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Annie	Annie	k1gFnSc2
Award	Awarda	k1gFnPc2
</s>
