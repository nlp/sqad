<s>
Thorium	thorium	k1gNnSc1
</s>
<s>
Thorium	thorium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
6	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
232	#num#	k4
</s>
<s>
Th	Th	k?
</s>
<s>
90	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
kousek	kousek	k1gInSc1
thoria	thorium	k1gNnSc2
v	v	k7c6
zatavené	zatavený	k2eAgFnSc6d1
ampuli	ampule	k1gFnSc6
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Thorium	thorium	k1gNnSc1
<g/>
,	,	kIx,
Th	Th	k1gFnSc1
<g/>
,	,	kIx,
90	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Thorium	thorium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
9,6	9,6	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrobílý	stříbrobílý	k2eAgInSc1d1
kov	kov	k1gInSc1
(	(	kIx(
<g/>
na	na	k7c6
povrchu	povrch	k1gInSc6
obvykle	obvykle	k6eAd1
zčernalý	zčernalý	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-29-1	7440-29-1	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
232,038	232,038	k4
1	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
165	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
179,8	179,8	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Th	Th	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
103	#num#	k4
pm	pm	k?
(	(	kIx(
<g/>
Th	Th	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
95	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
6	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
IV	IV	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
6,95	6,95	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
11,5	11,5	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
20,0	20,0	k4
eV	eV	k?
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
</s>
<s>
28,8	28,8	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
α	α	k1gFnSc1
krychlová	krychlový	k2eAgFnSc1d1
plošně	plošně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
508,43	508,43	k4
pmβ	pmβ	k1gFnSc1
krychlová	krychlový	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
a	a	k8xC
=	=	kIx~
411	#num#	k4
pm	pm	k?
</s>
<s>
Teplota	teplota	k1gFnSc1
změny	změna	k1gFnSc2
modifikace	modifikace	k1gFnSc2
</s>
<s>
1	#num#	k4
400	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
673,15	673,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
11,724	11,724	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
vypočteno	vypočten	k2eAgNnSc1d1
z	z	k7c2
RTG	RTG	kA
dat	datum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
2	#num#	k4
490	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
54,0	54,0	k4
W	W	kA
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
100	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
54,3	54,3	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Součinitel	součinitel	k1gInSc1
délkové	délkový	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
</s>
<s>
125	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
598	#num#	k4
±	±	k?
6	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
53,37	53,37	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1	#num#	k4
750	#num#	k4
±	±	k?
50	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
0	#num#	k4
<g/>
23,15	23,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
4	#num#	k4
200	#num#	k4
±	±	k?
300	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
4	#num#	k4
473,15	473,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
80	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
2	#num#	k4
340	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
0,118	0,118	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
6,67	6,67	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
14	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
Ω	Ω	k6eAd1
</s>
<s>
Teplotní	teplotní	k2eAgMnSc1d1
součinitel	součinitel	k1gMnSc1
elektrického	elektrický	k2eAgInSc2d1
odporu	odpor	k1gInSc2
</s>
<s>
0,003	0,003	k4
8	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Teplota	teplota	k1gFnSc1
přechodu	přechod	k1gInSc2
do	do	k7c2
supravodivého	supravodivý	k2eAgInSc2d1
stavu	stav	k1gInSc2
</s>
<s>
1,38	1,38	k4
K	k	k7c3
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Th	Th	k1gFnPc2
<g/>
4	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Th	Th	k1gFnSc7
<g/>
0	#num#	k4
<g/>
)	)	kIx)
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
susceptibilita	susceptibilita	k1gFnSc1
</s>
<s>
0,57	0,57	k4
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ce	Ce	k?
<g/>
⋏	⋏	k?
</s>
<s>
Aktinium	aktinium	k1gNnSc1
≺	≺	k?
<g/>
Th	Th	k1gMnSc2
<g/>
≻	≻	k?
Protaktinium	protaktinium	k1gNnSc1
</s>
<s>
Thorium	thorium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Th	Th	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druhým	druhý	k4xOgInSc7
prvkem	prvek	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
velmi	velmi	k6eAd1
dlouhému	dlouhý	k2eAgInSc3d1
poločasu	poločas	k1gInSc3
rozpadu	rozpad	k1gInSc2
jader	jádro	k1gNnPc2
thoria	thorium	k1gNnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
tento	tento	k3xDgInSc1
prvek	prvek	k1gInSc1
najít	najít	k5eAaPmF
v	v	k7c6
horninách	hornina	k1gFnPc6
zemské	zemský	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
potenciálním	potenciální	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Thorium	thorium	k1gNnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
slabě	slabě	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
(	(	kIx(
<g/>
zářič	zářič	k1gInSc1
α	α	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
na	na	k7c6
vzduchu	vzduch	k1gInSc6
pomalu	pomalu	k6eAd1
pokrývá	pokrývat	k5eAaImIp3nS
vrstvou	vrstva	k1gFnSc7
našedlého	našedlý	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahřátím	zahřátí	k1gNnSc7
na	na	k7c6
vzduchu	vzduch	k1gInSc6
se	se	k3xPyFc4
kovové	kovový	k2eAgNnSc1d1
thorium	thorium	k1gNnSc1
může	moct	k5eAaImIp3nS
i	i	k9
vznítit	vznítit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžných	běžný	k2eAgFnPc6d1
minerálních	minerální	k2eAgFnPc6d1
kyselinách	kyselina	k1gFnPc6
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
jen	jen	k9
zvolna	zvolna	k6eAd1
<g/>
,	,	kIx,
koncentrovaná	koncentrovaný	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
dusičná	dusičný	k2eAgFnSc1d1
jej	on	k3xPp3gInSc4
pasivuje	pasivovat	k5eAaBmIp3nS
vytvořením	vytvoření	k1gNnSc7
inertní	inertní	k2eAgFnSc2d1
vrstvičky	vrstvička	k1gFnSc2
oxidu	oxid	k1gInSc2
thoričitého	thoričitý	k2eAgInSc2d1
ThO	ThO	k1gFnSc7
<g/>
2	#num#	k4
na	na	k7c6
povrchu	povrch	k1gInSc6
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Th	Th	k1gFnSc2
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
již	již	k9
roku	rok	k1gInSc2
1828	#num#	k4
švédský	švédský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Jöns	Jönsa	k1gFnPc2
Jacob	Jacoba	k1gFnPc2
Berzelius	Berzelius	k1gInSc1
a	a	k8xC
pojmenoval	pojmenovat	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
po	po	k7c6
Thórovi	Thór	k1gMnSc6
<g/>
,	,	kIx,
bohu	bůh	k1gMnSc6
blesku	blesk	k1gInSc2
ve	v	k7c6
skandinávské	skandinávský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Monazitový	monazitový	k2eAgInSc1d1
písek	písek	k1gInSc1
</s>
<s>
Thorium	thorium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
poměrně	poměrně	k6eAd1
silně	silně	k6eAd1
zastoupeno	zastoupit	k5eAaPmNgNnS
<g/>
,	,	kIx,
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
koncentraci	koncentrace	k1gFnSc6
9,6	9,6	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
(	(	kIx(
<g/>
neboli	neboli	k8xC
ppm	ppm	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
obsah	obsah	k1gInSc4
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
udáván	udávat	k5eAaImNgInS
okolo	okolo	k7c2
7	#num#	k4
μ	μ	k1gFnPc2
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
thoria	thorium	k1gNnSc2
na	na	k7c4
500	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
thorium	thorium	k1gNnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
vzácně	vzácně	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
minerálu	minerál	k1gInSc2
thorianitu	thorianit	k1gInSc2
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
ThO	ThO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
thoritu	thorit	k1gInSc2
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
ThSiO	ThSiO	k1gFnPc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
doprovází	doprovázet	k5eAaImIp3nP
prvky	prvek	k1gInPc1
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
a	a	k8xC
nejčastější	častý	k2eAgNnSc1d3
průmyslově	průmyslově	k6eAd1
zpracovávanou	zpracovávaný	k2eAgFnSc7d1
surovinou	surovina	k1gFnSc7
jsou	být	k5eAaImIp3nP
monazitové	monazitový	k2eAgInPc1d1
písky	písek	k1gInPc1
<g/>
,	,	kIx,
směsné	směsný	k2eAgInPc1d1
fosforečnany	fosforečnan	k1gInPc1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
<g/>
,	,	kIx,
Th	Th	k1gFnSc1
<g/>
,	,	kIx,
Nd	Nd	k1gFnSc1
<g/>
,	,	kIx,
Y	Y	kA
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgNnPc6,k3yQgNnPc6,k3yRgNnPc6
je	být	k5eAaImIp3nS
hmotnostní	hmotnostní	k2eAgInSc1d1
podíl	podíl	k1gInSc1
thoria	thorium	k1gNnSc2
až	až	k9
6	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
například	například	k6eAd1
minerál	minerál	k1gInSc4
euxenit	euxenit	k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
Y	Y	kA
<g/>
,	,	kIx,
Ca	ca	kA
<g/>
,	,	kIx,
Ce	Ce	k1gMnSc1
<g/>
,	,	kIx,
U	U	kA
<g/>
,	,	kIx,
Th	Th	k1gFnSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Nb	Nb	k1gFnSc1
<g/>
,	,	kIx,
Ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
Ti	ty	k3xPp2nSc3
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
na	na	k7c4
thorium	thorium	k1gNnSc4
bohatých	bohatý	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
Skandinávii	Skandinávie	k1gFnSc3
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Brazílii	Brazílie	k1gFnSc6
<g/>
,	,	kIx,
Indonésii	Indonésie	k1gFnSc6
a	a	k8xC
Kanadě	Kanada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Při	při	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
thoria	thorium	k1gNnSc2
se	se	k3xPyFc4
rudy	ruda	k1gFnPc1
nejprve	nejprve	k6eAd1
digerují	digerovat	k5eAaImIp3nP
roztokem	roztok	k1gInSc7
louhu	louh	k1gInSc2
a	a	k8xC
vysrážené	vysrážený	k2eAgInPc4d1
nerozpustné	rozpustný	k2eNgInPc4d1
hydroxidy	hydroxid	k1gInPc4
lanthanoidů	lanthanoid	k1gInPc2
a	a	k8xC
thoria	thorium	k1gNnSc2
se	se	k3xPyFc4
oddělí	oddělit	k5eAaPmIp3nP
filtrací	filtrace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejich	jejich	k3xOp3gNnSc6
rozpuštění	rozpuštění	k1gNnSc6
v	v	k7c6
kyselině	kyselina	k1gFnSc6
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1
se	se	k3xPyFc4
postupným	postupný	k2eAgNnSc7d1
snižováním	snižování	k1gNnSc7
pH	ph	kA
z	z	k7c2
roztoku	roztok	k1gInSc2
nejprve	nejprve	k6eAd1
oddělí	oddělit	k5eAaPmIp3nP
hydroxidy	hydroxid	k1gInPc1
thoria	thorium	k1gNnSc2
a	a	k8xC
uranu	uran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnPc4
čistého	čistý	k2eAgNnSc2d1
thoria	thorium	k1gNnSc2
se	se	k3xPyFc4
z	z	k7c2
tohoto	tento	k3xDgInSc2
materiálu	materiál	k1gInSc2
získávají	získávat	k5eAaImIp3nP
po	po	k7c6
rozpuštění	rozpuštění	k1gNnSc6
v	v	k7c6
HCl	HCl	k1gFnSc6
kapalinovou	kapalinový	k2eAgFnSc7d1
extrakcí	extrakce	k1gFnSc7
tributylfosfátem	tributylfosfát	k1gInSc7
nebo	nebo	k8xC
methylisobutylketonem	methylisobutylketon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1
kov	kov	k1gInSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
připravuje	připravovat	k5eAaImIp3nS
elektrochemicky	elektrochemicky	k6eAd1
z	z	k7c2
taveniny	tavenina	k1gFnSc2
směsi	směs	k1gFnSc2
fluoridu	fluorid	k1gInSc2
thoričitého	thoričitý	k2eAgMnSc4d1
ThF	ThF	k1gMnSc4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
kyanidu	kyanid	k1gInSc2
draselného	draselný	k2eAgInSc2d1
KCN	KCN	kA
a	a	k8xC
chloridu	chlorid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
NaCl	NaCl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
získat	získat	k5eAaPmF
elementární	elementární	k2eAgNnSc4d1
thorium	thorium	k1gNnSc4
redukcí	redukce	k1gFnPc2
roztaveného	roztavený	k2eAgInSc2d1
fluoridu	fluorid	k1gInSc2
thoričitého	thoričitý	k2eAgInSc2d1
elementárním	elementární	k2eAgInSc7d1
vápníkem	vápník	k1gInSc7
<g/>
,	,	kIx,
hořčíkem	hořčík	k1gInSc7
nebo	nebo	k8xC
sodíkem	sodík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
řada	řada	k1gFnSc1
izotopů	izotop	k1gInPc2
thoria	thorium	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
pouze	pouze	k6eAd1
s	s	k7c7
izotopem	izotop	k1gInSc7
232	#num#	k4
<g/>
Th	Th	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
mimořádně	mimořádně	k6eAd1
velkým	velký	k2eAgInSc7d1
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
1,40	1,40	k4
<g/>
×	×	k?
<g/>
1010	#num#	k4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
velká	velký	k2eAgFnSc1d1
většina	většina	k1gFnSc1
dalších	další	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
thoria	thorium	k1gNnSc2
<g/>
,	,	kIx,
α	α	k?
zářič	zářič	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
dalších	další	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
se	se	k3xPyFc4
lze	lze	k6eAd1
zmínit	zmínit	k5eAaPmF
například	například	k6eAd1
o	o	k7c4
230	#num#	k4
<g/>
Th	Th	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
75	#num#	k4
400	#num#	k4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
229	#num#	k4
<g/>
Th	Th	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
7	#num#	k4
932	#num#	k4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
228	#num#	k4
<g/>
Th	Th	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
1,912	1,912	k4
<g/>
5	#num#	k4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ostatní	ostatní	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
s	s	k7c7
nukleonovými	nukleonův	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
208	#num#	k4
až	až	k8xS
239	#num#	k4
u	u	k7c2
se	se	k3xPyFc4
rozpadají	rozpadat	k5eAaPmIp3nP,k5eAaImIp3nP
mnohem	mnohem	k6eAd1
rychleji	rychle	k6eAd2
<g/>
:	:	kIx,
</s>
<s>
Izotoppoločas	Izotoppoločas	k1gMnSc1
rozpadudruh	rozpadudruh	k1gMnSc1
rozpaduprodukt	rozpaduprodukt	k1gInSc4
rozpadu	rozpad	k1gInSc2
</s>
<s>
208	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,7	1,7	k4
msα	msα	k?
<g/>
204	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
209	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
2,5	2,5	k4
msα	msα	k?
<g/>
205	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
210	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
16	#num#	k4
msα	msα	k?
/	/	kIx~
ε	ε	k?
<g/>
206	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
210	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
211	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
37	#num#	k4
msα	msα	k?
/	/	kIx~
ε	ε	k?
<g/>
207	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
211	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
212	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
30	#num#	k4
msα	msα	k?
(	(	kIx(
<g/>
99,7	99,7	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
ε	ε	k?
(	(	kIx(
<g/>
0,3	0,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
208	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
212	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
213	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
144	#num#	k4
msα	msα	k?
<g/>
209	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
214	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
87	#num#	k4
msα	msα	k?
<g/>
210	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
215	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,2	1,2	k4
sα	sα	k?
<g/>
211	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
216	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
26,0	26,0	k4
msα	msα	k?
(	(	kIx(
<g/>
99,99	99,99	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
0,01	0,01	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
212	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
216	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
217	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
241	#num#	k4
μ	μ	k?
<g/>
213	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
218	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
117	#num#	k4
nsα	nsα	k?
<g/>
214	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
219	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,05	1,05	k4
μ	μ	k?
<g/>
215	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
220	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
9,7	9,7	k4
μ	μ	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
216	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
220	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
221	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,68	1,68	k4
msα	msα	k?
<g/>
217	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
222	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
2,8	2,8	k4
msα	msα	k?
<g/>
218	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
223	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
0,60	0,60	k4
sα	sα	k?
<g/>
219	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
224	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,04	1,04	k4
sα	sα	k?
<g/>
220	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
225	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
8,75	8,75	k4
minα	minα	k?
(	(	kIx(
<g/>
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
221	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
225	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
226	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
30,57	30,57	k4
minα	minα	k?
<g/>
222	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
227	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
18,697	18,697	k4
dα	dα	k?
<g/>
223	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
228	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,912	1,912	k4
5	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
20O	20O	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
11	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
224	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
</s>
<s>
229	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
7	#num#	k4
932	#num#	k4
rα	rα	k?
<g/>
225	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
230	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
75	#num#	k4
400	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
24	#num#	k4
<g/>
Ne	Ne	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
11	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
≤	≤	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
12	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
226	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
206	#num#	k4
<g/>
Hg	Hg	k1gFnPc2
/	/	kIx~
různé	různý	k2eAgInPc1d1
</s>
<s>
231	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
25,52	25,52	k4
hβ	hβ	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
11	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
231	#num#	k4
<g/>
Pa	Pa	kA
/	/	kIx~
227	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
232	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
1,40	1,40	k4
<g/>
×	×	k?
<g/>
1010	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
1,1	1,1	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
228	#num#	k4
<g/>
Ra	ra	k0
/	/	kIx~
různé	různý	k2eAgInPc4d1
</s>
<s>
233	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
21,83	21,83	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
233	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
234	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
24,10	24,10	k4
dβ	dβ	k?
<g/>
−	−	k?
<g/>
234	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
235	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
7,1	7,1	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
235	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
236	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
37,3	37,3	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
236	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
237	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
4,7	4,7	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
237	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
238	#num#	k4
<g/>
Th	Th	k1gFnSc1
<g/>
9,4	9,4	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
238	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
239	#num#	k4
<g/>
Th	Th	k1gFnPc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
β	β	k?
<g/>
−	−	k?
<g/>
239	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
energetika	energetika	k1gFnSc1
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ThF	ThF	k1gFnSc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
thorium	thorium	k1gNnSc4
hlavní	hlavní	k2eAgNnSc1d1
využití	využití	k1gNnSc1
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
jako	jako	k9
potenciální	potenciální	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
štěpného	štěpný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
jádra	jádro	k1gNnSc2
232	#num#	k4
<g/>
Th	Th	k1gFnSc2
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
α	α	k1gInPc7
a	a	k8xC
nemůže	moct	k5eNaImIp3nS
u	u	k7c2
nich	on	k3xPp3gInPc2
proběhnout	proběhnout	k5eAaPmF
spontánní	spontánní	k2eAgNnSc4d1
štěpení	štěpení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záchytem	záchyt	k1gInSc7
neutronu	neutron	k1gInSc2
se	se	k3xPyFc4
však	však	k9
mohou	moct	k5eAaImIp3nP
měnit	měnit	k5eAaImF
na	na	k7c4
uran	uran	k1gInSc4
233	#num#	k4
<g/>
U	U	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
vynikajícím	vynikající	k2eAgNnSc7d1
jaderným	jaderný	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
a	a	k8xC
silným	silný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
neutronů	neutron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
thorium	thorium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
třikrát	třikrát	k6eAd1
častěji	často	k6eAd2
než	než	k8xS
uran	uran	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pochopitelné	pochopitelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
myšlenka	myšlenka	k1gFnSc1
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
energetické	energetický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
lákavá	lákavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
výzkum	výzkum	k1gInSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
ubírá	ubírat	k5eAaImIp3nS
dvěma	dva	k4xCgInPc7
směry	směr	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thorium	thorium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
přeměňováno	přeměňován	k2eAgNnSc1d1
na	na	k7c4
233	#num#	k4
<g/>
U	U	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
přímo	přímo	k6eAd1
účastní	účastnit	k5eAaImIp3nP
další	další	k2eAgFnPc4d1
štěpné	štěpný	k2eAgFnPc4d1
reakce	reakce	k1gFnPc4
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgNnSc6
prostředí	prostředí	k1gNnSc6
jaderně	jaderna	k1gFnSc3
přeměňuje	přeměňovat	k5eAaImIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
energetického	energetický	k2eAgInSc2d1
výtěžku	výtěžek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
do	do	k7c2
jaderného	jaderný	k2eAgInSc2d1
reaktoru	reaktor	k1gInSc2
vsazován	vsazován	k2eAgInSc4d1
poměrně	poměrně	k6eAd1
nízký	nízký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
thoria	thorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
90	#num#	k4
</s>
<s>
232	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
h	h	k?
</s>
<s>
+	+	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
⟶	⟶	k?
</s>
<s>
90	#num#	k4
</s>
<s>
233	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
h	h	k?
</s>
<s>
→	→	k?
</s>
<s>
22	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
n	n	k0
</s>
<s>
β	β	k?
</s>
<s>
−	−	k?
</s>
<s>
91	#num#	k4
</s>
<s>
233	#num#	k4
</s>
<s>
P	P	kA
</s>
<s>
a	a	k8xC
</s>
<s>
→	→	k?
</s>
<s>
26	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
967	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
β	β	k?
</s>
<s>
−	−	k?
</s>
<s>
92	#num#	k4
</s>
<s>
233	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
232	#num#	k4
<g/>
}	}	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
90	#num#	k4
<g/>
}	}	kIx)
<g/>
Th	Th	k1gFnSc1
<g/>
\	\	kIx~
+	+	kIx~
<g/>
\	\	kIx~
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
n	n	k0
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
\	\	kIx~
<g/>
longrightarrow	longrightarrow	k?
\	\	kIx~
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
90	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
233	#num#	k4
<g/>
}	}	kIx)
<g/>
Th	Th	k1gFnSc1
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
xrightarrow	xrightarrow	k?
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
22,3	22,3	k4
<g/>
\	\	kIx~
min	mina	k1gFnPc2
<g/>
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
^	^	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
91	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
233	#num#	k4
<g/>
}	}	kIx)
<g/>
Pa	Pa	kA
<g/>
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
xrightarrow	xrightarrow	k?
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
26,967	26,967	k4
<g/>
\	\	kIx~
d	d	k?
<g/>
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
92	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
233	#num#	k4
<g/>
}	}	kIx)
<g/>
U	u	k7c2
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
jaderné	jaderný	k2eAgFnSc2d1
přeměny	přeměna	k1gFnSc2
v	v	k7c6
reaktoru	reaktor	k1gInSc6
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
maximálního	maximální	k2eAgNnSc2d1
množství	množství	k1gNnSc2
jader	jádro	k1gNnPc2
233	#num#	k4
<g/>
U	U	kA
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
následně	následně	k6eAd1
oddělena	oddělen	k2eAgNnPc1d1
a	a	k8xC
slouží	sloužit	k5eAaImIp3nP
jako	jako	k9
jaderné	jaderný	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
atomovém	atomový	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
do	do	k7c2
jaderné	jaderný	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
nasazeno	nasazen	k2eAgNnSc4d1
maximální	maximální	k2eAgNnSc4d1
množství	množství	k1gNnSc4
232	#num#	k4
<g/>
Th	Th	k1gFnSc2
a	a	k8xC
přeměna	přeměna	k1gFnSc1
na	na	k7c6
233U	233U	k4
je	být	k5eAaImIp3nS
důležitější	důležitý	k2eAgMnSc1d2
než	než	k8xS
energetický	energetický	k2eAgInSc1d1
výtěžek	výtěžek	k1gInSc1
procesu	proces	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdrojem	zdroj	k1gInSc7
energie	energie	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
až	až	k6eAd1
následné	následný	k2eAgNnSc4d1
jaderné	jaderný	k2eAgNnSc4d1
štěpení	štěpení	k1gNnSc4
233U	233U	k4
v	v	k7c6
dalším	další	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
tohoto	tento	k3xDgInSc2
procesu	proces	k1gInSc2
je	být	k5eAaImIp3nS
nutnost	nutnost	k1gFnSc4
přepracování	přepracování	k1gNnSc2
paliva	palivo	k1gNnSc2
z	z	k7c2
prvního	první	k4xOgInSc2
reaktoru	reaktor	k1gInSc2
na	na	k7c4
čistý	čistý	k2eAgInSc4d1
233	#num#	k4
<g/>
U	U	kA
<g/>
,	,	kIx,
protože	protože	k8xS
produkty	produkt	k1gInPc1
vzniklé	vzniklý	k2eAgInPc1d1
ozařováním	ozařování	k1gNnSc7
thoria	thorium	k1gNnSc2
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
silnými	silný	k2eAgInPc7d1
radioaktivními	radioaktivní	k2eAgInPc7d1
zářiči	zářič	k1gInPc7
a	a	k8xC
separaci	separace	k1gFnSc3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
provádět	provádět	k5eAaImF
za	za	k7c2
zvýšených	zvýšený	k2eAgFnPc2d1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
výhoda	výhoda	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
relativně	relativně	k6eAd1
jednoduché	jednoduchý	k2eAgFnSc6d1
a	a	k8xC
nenáročné	náročný	k2eNgFnSc6d1
kontrole	kontrola	k1gFnSc6
štěpení	štěpení	k1gNnSc2
vzniklého	vzniklý	k2eAgInSc2d1
izotopu	izotop	k1gInSc2
uranu	uran	k1gInSc2
233	#num#	k4
<g/>
U.	U.	kA
</s>
<s>
Výzkum	výzkum	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
využití	využití	k1gNnSc2
thoria	thorium	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
prováděn	provádět	k5eAaImNgInS
především	především	k9
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnPc1
potenciální	potenciální	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
thoria	thorium	k1gNnSc2
patří	patřit	k5eAaImIp3nP
k	k	k7c3
jedněm	jeden	k4xCgFnPc3
z	z	k7c2
největších	veliký	k2eAgInPc2d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
využití	využití	k1gNnSc2
thoria	thorium	k1gNnSc2
pro	pro	k7c4
jadernou	jaderný	k2eAgFnSc4d1
energetiku	energetika	k1gFnSc4
se	se	k3xPyFc4
také	také	k9
dlouhodobě	dlouhodobě	k6eAd1
provádí	provádět	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Ve	v	k7c6
slitinách	slitina	k1gFnPc6
hořčíku	hořčík	k1gInSc2
zlepšují	zlepšovat	k5eAaImIp3nP
malé	malý	k2eAgInPc1d1
přídavky	přídavek	k1gInPc1
thoria	thorium	k1gNnSc2
mechanickou	mechanický	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Neodtavující	odtavující	k2eNgFnPc1d1
se	se	k3xPyFc4
elektrody	elektroda	k1gFnPc1
z	z	k7c2
thoria	thorium	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
slitin	slitina	k1gFnPc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pro	pro	k7c4
obloukové	obloukový	k2eAgNnSc4d1
svařování	svařování	k1gNnSc4
metodou	metoda	k1gFnSc7
TIG	TIG	kA
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sklářském	sklářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
přídavkem	přídavek	k1gInSc7
thoria	thorium	k1gNnSc2
do	do	k7c2
skloviny	sklovina	k1gFnSc2
dociluje	docilovat	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc4
indexu	index	k1gInSc2
lomu	lom	k1gInSc2
a	a	k8xC
snížení	snížení	k1gNnSc4
rozptylu	rozptyl	k1gInSc2
světla	světlo	k1gNnSc2
vyrobeného	vyrobený	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
sklo	sklo	k1gNnSc1
slouží	sloužit	k5eAaImIp3nS
především	především	k9
jako	jako	k9
materiál	materiál	k1gInSc4
v	v	k7c6
optických	optický	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
čočky	čočka	k1gFnPc4
pro	pro	k7c4
filmové	filmový	k2eAgFnPc4d1
kamery	kamera	k1gFnPc4
nebo	nebo	k8xC
vědecké	vědecký	k2eAgInPc4d1
přístroje	přístroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc4
thoričitý	thoričitý	k2eAgInSc4d1
ThO	ThO	k1gFnSc7
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
odolný	odolný	k2eAgInSc4d1
vůči	vůči	k7c3
vysokým	vysoký	k2eAgFnPc3d1
teplotám	teplota	k1gFnPc3
a	a	k8xC
vyrábějí	vyrábět	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
tavicí	tavicí	k2eAgInPc1d1
kelímky	kelímek	k1gInPc1
a	a	k8xC
chemické	chemický	k2eAgNnSc1d1
nádobí	nádobí	k1gNnSc4
určené	určený	k2eAgNnSc4d1
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
agresivními	agresivní	k2eAgInPc7d1
materiály	materiál	k1gInPc7
za	za	k7c2
vysokých	vysoký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxid	oxid	k1gInSc1
thoričitý	thoričitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k6eAd1
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
punčošek	punčoška	k1gFnPc2
v	v	k7c6
plynových	plynový	k2eAgFnPc6d1
lampách	lampa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc4
thoričitý	thoričitý	k2eAgInSc4d1
ThO	ThO	k1gFnSc7
<g/>
2	#num#	k4
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
průmyslový	průmyslový	k2eAgInSc1d1
katalyzátor	katalyzátor	k1gInSc1
v	v	k7c6
chemickém	chemický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
při	při	k7c6
výrobě	výroba	k1gFnSc6
kyseliny	kyselina	k1gFnSc2
dusičné	dusičný	k2eAgFnSc2d1
z	z	k7c2
amoniaku	amoniak	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
výrobě	výroba	k1gFnSc6
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
nebo	nebo	k8xC
při	při	k7c6
krakování	krakování	k1gNnSc6
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jemně	jemně	k6eAd1
rozptýlené	rozptýlený	k2eAgNnSc1d1
kovové	kovový	k2eAgNnSc1d1
thorium	thorium	k1gNnSc1
je	být	k5eAaImIp3nS
po	po	k7c6
zahřátí	zahřátí	k1gNnSc6
na	na	k7c4
vysokou	vysoký	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
pyroforické	pyroforický	k2eAgFnSc3d1
<g/>
,	,	kIx,
shoří	shořet	k5eAaPmIp3nS
jasným	jasný	k2eAgInSc7d1
svítivým	svítivý	k2eAgInSc7d1
plamenem	plamen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Thoriová	thoriový	k2eAgFnSc1d1
rozpadová	rozpadový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
thorium	thorium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
thorium	thorium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
thoriovými	thoriový	k2eAgInPc7d1
a	a	k8xC
uranovými	uranový	k2eAgInPc7d1
reaktory	reaktor	k1gInPc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4185331-3	4185331-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5764	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85134957	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85134957	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
