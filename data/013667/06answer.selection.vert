<s>
Thorium	thorium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Th	Th	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druhým	druhý	k4xOgInSc7
prvkem	prvek	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
</s>