<p>
<s>
Jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
souhrnně	souhrnně	k6eAd1	souhrnně
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
propastí	propast	k1gFnSc7	propast
Macocha	Macocha	k1gFnSc1	Macocha
<g/>
,	,	kIx,	,
obcemi	obec	k1gFnPc7	obec
Sloup	sloup	k1gInSc1	sloup
a	a	k8xC	a
Holštejn	Holštejn	k1gInSc1	Holštejn
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
téměř	téměř	k6eAd1	téměř
45	[number]	k4	45
kilometry	kilometr	k1gInPc4	kilometr
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
jeskynním	jeskynní	k2eAgInSc7d1	jeskynní
systémem	systém	k1gInSc7	systém
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Protékají	protékat	k5eAaImIp3nP	protékat
jím	on	k3xPp3gInSc7	on
tři	tři	k4xCgInPc1	tři
aktivní	aktivní	k2eAgInPc1d1	aktivní
toky	tok	k1gInPc1	tok
<g/>
:	:	kIx,	:
Bílá	bílý	k2eAgFnSc1d1	bílá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Sloupský	Sloupský	k2eAgInSc1d1	Sloupský
potok	potok	k1gInSc1	potok
a	a	k8xC	a
Punkva	Punkva	k1gFnSc1	Punkva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jejich	jejich	k3xOp3gInSc7	jejich
soutokem	soutok	k1gInSc7	soutok
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
Sloupsko-šošůvské	Sloupsko-šošůvský	k2eAgFnPc1d1	Sloupsko-šošůvská
jeskyně	jeskyně	k1gFnPc1	jeskyně
s	s	k7c7	s
propadáním	propadání	k1gNnSc7	propadání
Sloupského	Sloupský	k2eAgInSc2d1	Sloupský
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc2	jeskyně
Nová	nový	k2eAgFnSc1d1	nová
Rasovna	rasovna	k1gFnSc1	rasovna
s	s	k7c7	s
propadáním	propadání	k1gNnSc7	propadání
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc2	jeskyně
Piková	pikový	k2eAgFnSc1d1	Piková
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
Spirálka	spirálka	k1gFnSc1	spirálka
<g/>
,	,	kIx,	,
C	C	kA	C
13	[number]	k4	13
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
propast	propast	k1gFnSc1	propast
Macocha	Macocha	k1gFnSc1	Macocha
a	a	k8xC	a
ve	v	k7c6	v
vývěrové	vývěrový	k2eAgFnSc6d1	vývěrová
části	část	k1gFnSc6	část
Punkevní	punkevní	k2eAgFnSc2d1	punkevní
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objevování	objevování	k1gNnSc6	objevování
jeskynního	jeskynní	k2eAgInSc2d1	jeskynní
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Karel	Karel	k1gMnSc1	Karel
Absolon	Absolon	k1gMnSc1	Absolon
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
a	a	k8xC	a
zpřístupnil	zpřístupnit	k5eAaPmAgInS	zpřístupnit
Punkevní	punkevní	k2eAgFnSc4d1	punkevní
jeskyně	jeskyně	k1gFnPc4	jeskyně
a	a	k8xC	a
Propast	propast	k1gFnSc4	propast
Macochu	Macocha	k1gFnSc4	Macocha
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
prozkoumány	prozkoumán	k2eAgFnPc1d1	prozkoumána
Sloupsko-šošůvské	Sloupsko-šošůvský	k2eAgFnPc1d1	Sloupsko-šošůvská
jeskyně	jeskyně	k1gFnPc1	jeskyně
s	s	k7c7	s
propadáním	propadání	k1gNnSc7	propadání
Punkvy	Punkva	k1gFnSc2	Punkva
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
tok	tok	k1gInSc1	tok
říčky	říčka	k1gFnSc2	říčka
Punkvy	Punkva	k1gFnSc2	Punkva
znamenal	znamenat	k5eAaImAgInS	znamenat
už	už	k6eAd1	už
při	při	k7c6	při
objevu	objev	k1gInSc6	objev
Punkevních	punkevní	k2eAgFnPc2d1	punkevní
jeskyní	jeskyně	k1gFnPc2	jeskyně
předpoklad	předpoklad	k1gInSc1	předpoklad
existence	existence	k1gFnSc2	existence
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
jeskynního	jeskynní	k2eAgInSc2d1	jeskynní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
také	také	k9	také
aktivně	aktivně	k6eAd1	aktivně
hledán	hledat	k5eAaImNgMnS	hledat
a	a	k8xC	a
následně	následně	k6eAd1	následně
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Piková	pikový	k2eAgFnSc1d1	Piková
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
Spirálka	spirálka	k1gFnSc1	spirálka
a	a	k8xC	a
Třináctka	třináctka	k1gFnSc1	třináctka
nedaleko	nedaleko	k7c2	nedaleko
Nové	Nové	k2eAgFnSc2d1	Nové
Rasovny	rasovna	k1gFnSc2	rasovna
byly	být	k5eAaImAgInP	být
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolních	dolní	k2eAgNnPc6d1	dolní
patrech	patro	k1gNnPc6	patro
těchto	tento	k3xDgMnPc2	tento
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
byl	být	k5eAaImAgInS	být
nalezen	nalezen	k2eAgInSc1d1	nalezen
aktivní	aktivní	k2eAgInSc1d1	aktivní
tok	tok	k1gInSc1	tok
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
úspěchu	úspěch	k1gInSc2	úspěch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
amatérská	amatérský	k2eAgFnSc1d1	amatérská
Speleologická	speleologický	k2eAgFnSc1d1	speleologická
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
Plániv	Plániv	k1gFnSc2	Plániv
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Plánivská	Plánivský	k2eAgFnSc1d1	Plánivská
skupina	skupina	k1gFnSc1	skupina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vedená	vedený	k2eAgFnSc1d1	vedená
Milanem	Milan	k1gMnSc7	Milan
Šlechtou	Šlechta	k1gMnSc7	Šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
aktivní	aktivní	k2eAgFnSc2d1	aktivní
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
13	[number]	k4	13
c	c	k0	c
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Třináctka	třináctka	k1gFnSc1	třináctka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
obrátila	obrátit	k5eAaPmAgFnS	obrátit
skupina	skupina	k1gFnSc1	skupina
svoji	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Ostrovské	ostrovský	k2eAgFnSc3d1	Ostrovská
plošině	plošina	k1gFnSc3	plošina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
prokopávat	prokopávat	k5eAaImF	prokopávat
Cigánský	cigánský	k2eAgInSc4d1	cigánský
závrt	závrt	k1gInSc4	závrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
usilovné	usilovný	k2eAgFnSc2d1	usilovná
práce	práce	k1gFnSc2	práce
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
amatérští	amatérský	k2eAgMnPc1d1	amatérský
speleologové	speleolog	k1gMnPc1	speleolog
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
110	[number]	k4	110
m	m	kA	m
aktivního	aktivní	k2eAgInSc2d1	aktivní
toku	tok	k1gInSc2	tok
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
objevili	objevit	k5eAaPmAgMnP	objevit
1,5	[number]	k4	1,5
km	km	kA	km
chodeb	chodba	k1gFnPc2	chodba
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Stará	starý	k2eAgFnSc1d1	stará
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zahájili	zahájit	k5eAaPmAgMnP	zahájit
potápěčský	potápěčský	k2eAgInSc4d1	potápěčský
průzkum	průzkum	k1gInSc4	průzkum
sifonu	sifon	k1gInSc2	sifon
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Povodňové	povodňový	k2eAgFnSc2d1	povodňová
chodby	chodba	k1gFnSc2	chodba
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
objevili	objevit	k5eAaPmAgMnP	objevit
prostory	prostor	k1gInPc7	prostor
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
zvané	zvaný	k2eAgFnPc1d1	zvaná
Nová	nový	k2eAgFnSc1d1	nová
amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1970	[number]	k4	1970
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
při	při	k7c6	při
nenadálé	nenadálý	k2eAgFnSc6d1	nenadálá
povodni	povodeň	k1gFnSc6	povodeň
během	během	k7c2	během
mapování	mapování	k1gNnSc2	mapování
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
amatérské	amatérský	k2eAgFnSc6d1	amatérská
jeskyni	jeskyně	k1gFnSc6	jeskyně
speleologové	speleolog	k1gMnPc1	speleolog
Milan	Milan	k1gMnSc1	Milan
Šlechta	Šlechta	k1gMnSc1	Šlechta
a	a	k8xC	a
Ing.	ing.	kA	ing.
Marko	Marko	k1gMnSc1	Marko
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
tragické	tragický	k2eAgFnSc6d1	tragická
události	událost	k1gFnSc6	událost
byl	být	k5eAaImAgInS	být
další	další	k2eAgInSc1d1	další
průzkum	průzkum	k1gInSc1	průzkum
dočasně	dočasně	k6eAd1	dočasně
zastaven	zastavit	k5eAaPmNgInS	zastavit
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k6eAd1	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
profesionálními	profesionální	k2eAgInPc7d1	profesionální
speleology	speleolog	k1gMnPc4	speleolog
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Geografického	geografický	k2eAgInSc2d1	geografický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Pustého	pustý	k2eAgInSc2d1	pustý
žlebu	žleb	k1gInSc2	žleb
proražena	proražen	k2eAgFnSc1d1	proražena
štola	štola	k1gFnSc1	štola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Amatérské	amatérský	k2eAgFnSc2d1	amatérská
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
výzkum	výzkum	k1gInSc1	výzkum
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
podrobné	podrobný	k2eAgNnSc4d1	podrobné
mapování	mapování	k1gNnSc4	mapování
systému	systém	k1gInSc2	systém
a	a	k8xC	a
potápěčský	potápěčský	k2eAgInSc1d1	potápěčský
průzkum	průzkum	k1gInSc1	průzkum
nejnižších	nízký	k2eAgNnPc2d3	nejnižší
pater	patro	k1gNnPc2	patro
a	a	k8xC	a
neprostupných	prostupný	k2eNgInPc2d1	neprostupný
sifonů	sifon	k1gInPc2	sifon
oddělujících	oddělující	k2eAgInPc2d1	oddělující
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nedávným	dávný	k2eNgInSc7d1	nedávný
úspěchem	úspěch	k1gInSc7	úspěch
jeskyňářů	jeskyňář	k1gMnPc2	jeskyňář
je	být	k5eAaImIp3nS	být
proplutí	proplutí	k1gNnSc4	proplutí
mezi	mezi	k7c7	mezi
Sloupsko-šošůvskými	Sloupsko-šošůvský	k2eAgFnPc7d1	Sloupsko-šošůvská
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
a	a	k8xC	a
Amatérskou	amatérský	k2eAgFnSc7d1	amatérská
jeskyní	jeskyně	k1gFnSc7	jeskyně
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
potápěči	potápěč	k1gMnSc6	potápěč
Janu	Jan	k1gMnSc6	Jan
Sirotkovi	Sirotek	k1gMnSc6	Sirotek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
jeskynního	jeskynní	k2eAgInSc2d1	jeskynní
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
objevenou	objevený	k2eAgFnSc7d1	objevená
částí	část	k1gFnSc7	část
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
byla	být	k5eAaImAgNnP	být
objevena	objevit	k5eAaPmNgNnP	objevit
ze	z	k7c2	z
šachty	šachta	k1gFnSc2	šachta
kopané	kopaná	k1gFnSc2	kopaná
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Cigánského	cigánský	k2eAgInSc2d1	cigánský
závrtu	závrt	k1gInSc2	závrt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
chodba	chodba	k1gFnSc1	chodba
s	s	k7c7	s
aktivním	aktivní	k2eAgNnSc7d1	aktivní
řečištěm	řečiště	k1gNnSc7	řečiště
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
systému	systém	k1gInSc2	systém
přitéká	přitékat	k5eAaImIp3nS	přitékat
100	[number]	k4	100
m	m	kA	m
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
sifonem	sifon	k1gInSc7	sifon
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
C	C	kA	C
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sifon	sifon	k1gInSc1	sifon
byl	být	k5eAaImAgInS	být
potápěči	potápěč	k1gInSc3	potápěč
překonán	překonán	k2eAgInSc1d1	překonán
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Přítoková	přítokový	k2eAgFnSc1d1	přítoková
chodba	chodba	k1gFnSc1	chodba
je	být	k5eAaImIp3nS	být
cestou	cesta	k1gFnSc7	cesta
Holštejnského	Holštejnský	k2eAgInSc2d1	Holštejnský
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
pravostranného	pravostranný	k2eAgInSc2d1	pravostranný
přítoku	přítok	k1gInSc2	přítok
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
od	od	k7c2	od
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
Povodňová	povodňový	k2eAgFnSc1d1	povodňová
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
Povodňovým	povodňový	k2eAgInSc7d1	povodňový
sifonem	sifon	k1gInSc7	sifon
propojena	propojit	k5eAaPmNgFnS	propojit
s	s	k7c7	s
Novou	nový	k2eAgFnSc7d1	nová
Amatérskou	amatérský	k2eAgFnSc7d1	amatérská
jeskyní	jeskyně	k1gFnSc7	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chodba	chodba	k1gFnSc1	chodba
odvádí	odvádět	k5eAaImIp3nS	odvádět
za	za	k7c4	za
povodní	povodní	k2eAgFnSc4d1	povodní
část	část	k1gFnSc4	část
vod	voda	k1gFnPc2	voda
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Amatérské	amatérský	k2eAgFnSc2d1	amatérská
jeskyně	jeskyně	k1gFnSc2	jeskyně
za	za	k7c2	za
běžných	běžný	k2eAgInPc2d1	běžný
vodních	vodní	k2eAgInPc2d1	vodní
stavů	stav	k1gInPc2	stav
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Aktivní	aktivní	k2eAgInSc1d1	aktivní
obtok	obtok	k1gInSc1	obtok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značně	značně	k6eAd1	značně
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
Nová	nový	k2eAgFnSc1d1	nová
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
Staré	Staré	k2eAgFnSc2d1	Staré
Amatérské	amatérský	k2eAgFnSc2d1	amatérská
jeskyně	jeskyně	k1gFnSc2	jeskyně
překonáním	překonání	k1gNnSc7	překonání
Povodňového	povodňový	k2eAgInSc2d1	povodňový
sifonu	sifon	k1gInSc2	sifon
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
Nové	Nové	k2eAgFnSc2d1	Nové
Amatérské	amatérský	k2eAgFnSc2d1	amatérská
jeskyně	jeskyně	k1gFnSc2	jeskyně
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Bludiště	bludiště	k1gNnSc4	bludiště
Milana	Milan	k1gMnSc2	Milan
Šlechty	Šlechta	k1gMnSc2	Šlechta
se	s	k7c7	s
složitým	složitý	k2eAgInSc7d1	složitý
labyrintem	labyrint	k1gInSc7	labyrint
chodeb	chodba	k1gFnPc2	chodba
i	i	k8xC	i
největší	veliký	k2eAgFnSc7d3	veliký
prostorou	prostora	k1gFnSc7	prostora
Amatérské	amatérský	k2eAgFnSc2d1	amatérská
jeskyně	jeskyně	k1gFnSc2	jeskyně
–	–	k?	–
Říceným	řícený	k2eAgInSc7d1	řícený
dómem	dóm	k1gInSc7	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Bludiště	bludiště	k1gNnSc1	bludiště
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
závrtů	závrt	k1gInPc2	závrt
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
–	–	k?	–
Městikáď	Městikáď	k1gFnSc2	Městikáď
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
k	k	k7c3	k
jeskyni	jeskyně	k1gFnSc3	jeskyně
dostat	dostat	k5eAaPmF	dostat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Karel	Karel	k1gMnSc1	Karel
Absolon	Absolon	k1gMnSc1	Absolon
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zbývalo	zbývat	k5eAaImAgNnS	zbývat
k	k	k7c3	k
proniknutí	proniknutí	k1gNnSc3	proniknutí
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
jen	jen	k9	jen
několik	několik	k4yIc1	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodních	spodní	k2eAgInPc6d1	spodní
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
neprobádaných	probádaný	k2eNgNnPc6d1	neprobádané
patrech	patro	k1gNnPc6	patro
jeskyně	jeskyně	k1gFnSc2	jeskyně
pod	pod	k7c7	pod
Bludištěm	bludiště	k1gNnSc7	bludiště
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
Sloupského	Sloupský	k2eAgInSc2d1	Sloupský
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
v	v	k7c4	v
podzemní	podzemní	k2eAgFnSc4d1	podzemní
říčku	říčka	k1gFnSc4	říčka
Punkvu	Punkva	k1gFnSc4	Punkva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bludiště	bludiště	k1gNnSc2	bludiště
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
jeskyně	jeskyně	k1gFnSc1	jeskyně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
–	–	k?	–
Rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
chodba	chodba	k1gFnSc1	chodba
a	a	k8xC	a
Sloupský	Sloupský	k2eAgInSc1d1	Sloupský
koridor	koridor	k1gInSc1	koridor
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Sloupsko-Šošůvským	Sloupsko-Šošůvský	k2eAgFnPc3d1	Sloupsko-Šošůvský
jeskyním	jeskyně	k1gFnPc3	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Koridor	koridor	k1gInSc1	koridor
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
několika	několik	k4yIc7	několik
sifony	sifon	k1gInPc7	sifon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
poslední	poslední	k2eAgNnSc1d1	poslední
podařilo	podařit	k5eAaPmAgNnS	podařit
proplavat	proplavat	k5eAaPmF	proplavat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
možnost	možnost	k1gFnSc4	možnost
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
do	do	k7c2	do
části	část	k1gFnSc2	část
jeskyně	jeskyně	k1gFnSc2	jeskyně
oddělené	oddělený	k2eAgInPc4d1	oddělený
sifony	sifon	k1gInPc4	sifon
proražen	proražen	k2eAgInSc4d1	proražen
umělý	umělý	k2eAgInSc4d1	umělý
vchod	vchod	k1gInSc4	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Staré	Staré	k2eAgFnSc3d1	Staré
Amatérské	amatérský	k2eAgFnSc3d1	amatérská
jeskyni	jeskyně	k1gFnSc3	jeskyně
vede	vést	k5eAaImIp3nS	vést
Dioklasová	Dioklasový	k2eAgFnSc1d1	Dioklasový
chodba	chodba	k1gFnSc1	chodba
a	a	k8xC	a
Chodba	chodba	k1gFnSc1	chodba
samoty	samota	k1gFnSc2	samota
s	s	k7c7	s
Katedrálou	katedrála	k1gFnSc7	katedrála
Jiřího	Jiří	k1gMnSc2	Jiří
Šlechty	Šlechta	k1gMnSc2	Šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
koridory	koridor	k1gInPc1	koridor
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Dómu	dóm	k1gInSc6	dóm
u	u	k7c2	u
Homole	homole	k1gFnSc2	homole
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
u	u	k7c2	u
Bludiště	bludiště	k1gNnSc2	bludiště
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bludiště	bludiště	k1gNnSc2	bludiště
na	na	k7c4	na
jih	jih	k1gInSc4	jih
vede	vést	k5eAaImIp3nS	vést
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
chodba	chodba	k1gFnSc1	chodba
jeskyně	jeskyně	k1gFnSc2	jeskyně
Macošský	macošský	k2eAgInSc4d1	macošský
koridor	koridor	k1gInSc4	koridor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
chodby	chodba	k1gFnSc2	chodba
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
Dómem	dóm	k1gInSc7	dóm
zemních	zemní	k2eAgFnPc2d1	zemní
pyramid	pyramida	k1gFnPc2	pyramida
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
takřka	takřka	k6eAd1	takřka
po	po	k7c6	po
kilometru	kilometr	k1gInSc6	kilometr
dómem	dóm	k1gInSc7	dóm
Ráztoka	Ráztoek	k1gInSc2	Ráztoek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
–	–	k?	–
Západní	západní	k2eAgMnPc1d1	západní
a	a	k8xC	a
Východní	východní	k2eAgFnSc4d1	východní
Macošskou	macošský	k2eAgFnSc4d1	Macošská
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Západní	západní	k2eAgFnSc2d1	západní
větve	větev	k1gFnSc2	větev
je	být	k5eAaImIp3nS	být
proražená	proražený	k2eAgFnSc1d1	proražená
umělá	umělý	k2eAgFnSc1d1	umělá
štola	štola	k1gFnSc1	štola
z	z	k7c2	z
Pustého	pustý	k2eAgInSc2d1	pustý
žlebu	žleb	k1gInSc2	žleb
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
větev	větev	k1gFnSc1	větev
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Javorovou	javorový	k2eAgFnSc7d1	Javorová
a	a	k8xC	a
Spojovací	spojovací	k2eAgFnSc7d1	spojovací
chodbou	chodba	k1gFnSc7	chodba
k	k	k7c3	k
aktivnímu	aktivní	k2eAgInSc3d1	aktivní
toku	tok	k1gInSc3	tok
Punkvy	Punkva	k1gFnSc2	Punkva
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc1d1	východní
větev	větev	k1gFnSc1	větev
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Stupňovitou	stupňovitý	k2eAgFnSc7d1	stupňovitá
chodbou	chodba	k1gFnSc7	chodba
do	do	k7c2	do
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
partií	partie	k1gFnPc2	partie
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
opět	opět	k6eAd1	opět
Aktivní	aktivní	k2eAgNnSc1d1	aktivní
řečiště	řečiště	k1gNnPc1	řečiště
Punkvy	Punkva	k1gFnSc2	Punkva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odtud	odtud	k6eAd1	odtud
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Předmacošským	Předmacošský	k2eAgInSc7d1	Předmacošský
sifonem	sifon	k1gInSc7	sifon
do	do	k7c2	do
Červíkových	červíkův	k2eAgFnPc2d1	červíkův
jeskyní	jeskyně	k1gFnPc2	jeskyně
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
Macocha	Macocha	k1gFnSc1	Macocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
chodeb	chodba	k1gFnPc2	chodba
v	v	k7c6	v
Amatérské	amatérský	k2eAgFnSc6d1	amatérská
jeskyni	jeskyně	k1gFnSc6	jeskyně
leží	ležet	k5eAaImIp3nS	ležet
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
aktivních	aktivní	k2eAgInPc2d1	aktivní
toků	tok	k1gInPc2	tok
Bílé	bílý	k2eAgFnSc2d1	bílá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
Sloupského	Sloupský	k2eAgInSc2d1	Sloupský
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
soutoku	soutok	k1gInSc6	soutok
i	i	k9	i
Punkvy	Punkva	k1gFnPc1	Punkva
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInPc1d1	aktivní
toky	tok	k1gInPc1	tok
se	se	k3xPyFc4	se
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
objevují	objevovat	k5eAaImIp3nP	objevovat
jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
potápěčský	potápěčský	k2eAgInSc4d1	potápěčský
průzkum	průzkum	k1gInSc4	průzkum
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobným	podrobný	k2eAgInSc7d1	podrobný
potápěčským	potápěčský	k2eAgInSc7d1	potápěčský
průzkumem	průzkum	k1gInSc7	průzkum
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
objevům	objev	k1gInPc3	objev
dalších	další	k2eAgFnPc2d1	další
zatopených	zatopený	k2eAgFnPc2d1	zatopená
chodeb	chodba	k1gFnPc2	chodba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
AUDY	AUDY	kA	AUDY
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
:	:	kIx,	:
30	[number]	k4	30
let	léto	k1gNnPc2	léto
od	od	k7c2	od
objevu	objev	k1gInSc2	objev
největšího	veliký	k2eAgInSc2d3	veliký
jeskynního	jeskynní	k2eAgInSc2d1	jeskynní
systému	systém	k1gInSc2	systém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
speleologická	speleologický	k2eAgFnSc1d1	speleologická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
232	[number]	k4	232
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
4721	[number]	k4	4721
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
jeskyni	jeskyně	k1gFnSc6	jeskyně
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
jeskyňářské	jeskyňářský	k2eAgFnSc2d1	jeskyňářská
skupiny	skupina	k1gFnSc2	skupina
Plánivy	Plániva	k1gFnSc2	Plániva
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
mapy	mapa	k1gFnPc1	mapa
jeskyně	jeskyně	k1gFnSc2	jeskyně
</s>
</p>
<p>
<s>
Červíkovy	červíkův	k2eAgFnSc2d1	červíkův
jeskyně	jeskyně	k1gFnSc2	jeskyně
–	–	k?	–
Předmacošský	Předmacošský	k2eAgInSc4d1	Předmacošský
sifon	sifon	k1gInSc4	sifon
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
jeskyňářské	jeskyňářský	k2eAgFnSc2d1	jeskyňářská
skupiny	skupina	k1gFnSc2	skupina
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
sifonu	sifon	k1gInSc2	sifon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevy	objev	k1gInPc1	objev
na	na	k7c6	na
podzemním	podzemní	k2eAgInSc6d1	podzemní
toku	tok	k1gInSc6	tok
Punkvy	Punkva	k1gFnSc2	Punkva
v	v	k7c6	v
Amatérské	amatérský	k2eAgFnSc6d1	amatérská
jeskyni	jeskyně	k1gFnSc6	jeskyně
</s>
</p>
<p>
<s>
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
a	a	k8xC	a
Sloupsko-šošůvské	Sloupsko-šošůvský	k2eAgFnSc2d1	Sloupsko-šošůvská
jeskyně	jeskyně	k1gFnSc2	jeskyně
propojeny	propojen	k2eAgInPc1d1	propojen
12	[number]	k4	12
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
publikace	publikace	k1gFnSc2	publikace
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
30	[number]	k4	30
let	léto	k1gNnPc2	léto
od	od	k7c2	od
objevu	objev	k1gInSc2	objev
největšího	veliký	k2eAgInSc2d3	veliký
jeskynního	jeskynní	k2eAgInSc2d1	jeskynní
systému	systém	k1gInSc2	systém
v	v	k7c6	v
ČR	ČR	kA	ČR
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Plánivy	Plániva	k1gFnPc1	Plániva
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
Plánivské	Plánivský	k2eAgFnSc2d1	Plánivská
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
</s>
</p>
