<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
též	též	k9	též
Otomanská	otomanský	k2eAgFnSc1d1	Otomanská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
možno	možno	k6eAd1	možno
psát	psát	k5eAaImF	psát
i	i	k9	i
malé	malý	k2eAgNnSc4d1	malé
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
říší	říš	k1gFnPc2	říš
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
při	při	k7c6	při
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1299	[number]	k4	1299
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
oblasti	oblast	k1gFnSc6	oblast
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
,	,	kIx,	,
Černomoří	Černomoří	k1gNnSc2	Černomoří
<g/>
,	,	kIx,	,
Blízkého	blízký	k2eAgInSc2d1	blízký
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zcela	zcela	k6eAd1	zcela
islámský	islámský	k2eAgInSc4d1	islámský
charakter	charakter	k1gInSc4	charakter
Božího	boží	k2eAgInSc2d1	boží
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vládli	vládnout	k5eAaImAgMnP	vládnout
sultáni	sultán	k1gMnPc1	sultán
dynastie	dynastie	k1gFnSc2	dynastie
Osmanů	Osman	k1gMnPc2	Osman
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
byla	být	k5eAaImAgFnS	být
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
spojnicí	spojnice	k1gFnSc7	spojnice
mezi	mezi	k7c7	mezi
evropským	evropský	k2eAgMnSc7d1	evropský
a	a	k8xC	a
asijským	asijský	k2eAgInSc7d1	asijský
kontinentem	kontinent	k1gInSc7	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
byla	být	k5eAaImAgNnP	být
dělena	dělit	k5eAaImNgNnP	dělit
na	na	k7c4	na
ejálety	ejálet	k1gInPc4	ejálet
(	(	kIx(	(
<g/>
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vilájety	vilájeta	k1gFnSc2	vilájeta
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c4	na
sandžaky	sandžak	k1gInPc4	sandžak
a	a	k8xC	a
náhije	náhít	k5eAaPmIp3nS	náhít
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rúmský	Rúmský	k2eAgInSc4d1	Rúmský
sultanát	sultanát	k1gInSc4	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
představovaly	představovat	k5eAaImAgFnP	představovat
již	již	k9	již
od	od	k7c2	od
arabské	arabský	k2eAgFnSc2d1	arabská
expanze	expanze	k1gFnSc2	expanze
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
místo	místo	k7c2	místo
střetu	střet	k1gInSc2	střet
mezi	mezi	k7c7	mezi
Byzantskou	byzantský	k2eAgFnSc7d1	byzantská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
muslimskými	muslimský	k2eAgMnPc7d1	muslimský
sousedy	soused	k1gMnPc7	soused
(	(	kIx(	(
<g/>
umajjovský	umajjovský	k2eAgInSc1d1	umajjovský
a	a	k8xC	a
abbásovský	abbásovský	k2eAgInSc1d1	abbásovský
chalífát	chalífát	k1gInSc1	chalífát
<g/>
,	,	kIx,	,
Seldžucká	Seldžucký	k2eAgFnSc1d1	Seldžucká
říše	říše	k1gFnSc1	říše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
následujících	následující	k2eAgNnPc6d1	následující
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Mantzikertu	Mantzikert	k1gInSc2	Mantzikert
získali	získat	k5eAaPmAgMnP	získat
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
tohoto	tento	k3xDgInSc2	tento
poloostrova	poloostrov	k1gInSc2	poloostrov
Seldžukové	Seldžukový	k2eAgInPc4d1	Seldžukový
a	a	k8xC	a
zřídili	zřídit	k5eAaPmAgMnP	zřídit
zde	zde	k6eAd1	zde
Ikonyjský	Ikonyjský	k2eAgInSc4d1	Ikonyjský
nebo	nebo	k8xC	nebo
též	též	k9	též
Rúmský	Rúmský	k2eAgInSc4d1	Rúmský
sultanát	sultanát	k1gInSc4	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Byzantští	byzantský	k2eAgMnPc1d1	byzantský
císaři	císař	k1gMnPc1	císař
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
ztracená	ztracený	k2eAgNnPc4d1	ztracené
území	území	k1gNnPc4	území
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Myriokefala	Myriokefala	k1gFnSc2	Myriokefala
a	a	k8xC	a
po	po	k7c6	po
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
roku	rok	k1gInSc2	rok
1204	[number]	k4	1204
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
Byzanc	Byzanc	k1gFnSc1	Byzanc
dostatečně	dostatečně	k6eAd1	dostatečně
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obnovila	obnovit	k5eAaPmAgFnS	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
dobyl	dobýt	k5eAaPmAgMnS	dobýt
nazpět	nazpět	k6eAd1	nazpět
císař	císař	k1gMnSc1	císař
Michael	Michael	k1gMnSc1	Michael
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Palaiologos	Palaiologos	k1gInSc1	Palaiologos
<g/>
,	,	kIx,	,
obnovená	obnovený	k2eAgFnSc1d1	obnovená
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nepovznesla	povznést	k5eNaPmAgFnS	povznést
na	na	k7c4	na
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
staletích	staletí	k1gNnPc6	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zažíval	zažívat	k5eAaImAgMnS	zažívat
Ikonyjský	Ikonyjský	k2eAgInSc4d1	Ikonyjský
sultanát	sultanát	k1gInSc4	sultanát
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
<g/>
,	,	kIx,	,
mocenský	mocenský	k2eAgInSc4d1	mocenský
i	i	k8xC	i
kulturní	kulturní	k2eAgInSc4d1	kulturní
rozmach	rozmach	k1gInSc4	rozmach
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
nastal	nastat	k5eAaPmAgInS	nastat
vlivem	vlivem	k7c2	vlivem
mongolské	mongolský	k2eAgFnSc2d1	mongolská
invaze	invaze	k1gFnSc2	invaze
úpadek	úpadek	k1gInSc1	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Sultanát	sultanát	k1gInSc1	sultanát
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
mongolských	mongolský	k2eAgMnPc2d1	mongolský
Ílchánů	Ílchán	k1gMnPc2	Ílchán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neváhali	váhat	k5eNaImAgMnP	váhat
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
poměrů	poměr	k1gInPc2	poměr
státu	stát	k1gInSc2	stát
a	a	k8xC	a
udržovali	udržovat	k5eAaImAgMnP	udržovat
nad	nad	k7c7	nad
sultány	sultána	k1gFnPc1	sultána
svoji	svůj	k3xOyFgFnSc4	svůj
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
střetů	střet	k1gInPc2	střet
mezi	mezi	k7c7	mezi
Byzancí	Byzanc	k1gFnSc7	Byzanc
a	a	k8xC	a
muslimy	muslim	k1gMnPc7	muslim
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
nejčastěji	často	k6eAd3	často
s	s	k7c7	s
Araby	Arab	k1gMnPc7	Arab
a	a	k8xC	a
Seldžuky	Seldžuk	k1gMnPc7	Seldžuk
<g/>
)	)	kIx)	)
existovaly	existovat	k5eAaImAgInP	existovat
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
soupeři	soupeř	k1gMnPc7	soupeř
oblasti	oblast	k1gFnSc2	oblast
osazené	osazený	k2eAgInPc1d1	osazený
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podnikaly	podnikat	k5eAaImAgFnP	podnikat
proti	proti	k7c3	proti
území	území	k1gNnSc3	území
nepřítele	nepřítel	k1gMnSc2	nepřítel
občasné	občasný	k2eAgInPc1d1	občasný
nájezdy	nájezd	k1gInPc1	nájezd
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
mongolským	mongolský	k2eAgInSc7d1	mongolský
vpádem	vpád	k1gInSc7	vpád
uteklo	utéct	k5eAaPmAgNnS	utéct
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
usazovalo	usazovat	k5eAaImAgNnS	usazovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Kočovné	kočovný	k2eAgInPc1d1	kočovný
kmeny	kmen	k1gInPc1	kmen
příchozivší	příchozivší	k2eAgInPc1d1	příchozivší
z	z	k7c2	z
východu	východ	k1gInSc2	východ
zde	zde	k6eAd1	zde
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
staletích	staletí	k1gNnPc6	staletí
nárazníková	nárazníkový	k2eAgNnPc4d1	nárazníkové
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
marky	marka	k1gFnPc4	marka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
podnikaly	podnikat	k5eAaImAgFnP	podnikat
výpady	výpad	k1gInPc4	výpad
na	na	k7c4	na
byzantské	byzantský	k2eAgNnSc4d1	byzantské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
moci	moc	k1gFnSc2	moc
Ílchánů	Ílchán	k1gMnPc2	Ílchán
a	a	k8xC	a
byzantských	byzantský	k2eAgMnPc2d1	byzantský
císařů	císař	k1gMnPc2	císař
zavládla	zavládnout	k5eAaPmAgFnS	zavládnout
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
anarchie	anarchie	k1gFnSc2	anarchie
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc1	dvacet
malých	malý	k2eAgInPc2d1	malý
nezávislých	závislý	k2eNgInPc2d1	nezávislý
státečků	státeček	k1gInPc2	státeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přitáhly	přitáhnout	k5eAaPmAgFnP	přitáhnout
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
nové	nový	k2eAgInPc4d1	nový
turecké	turecký	k2eAgInPc4d1	turecký
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
tlak	tlak	k1gInSc1	tlak
Mongolů	mongol	k1gInPc2	mongol
přiměl	přimět	k5eAaPmAgInS	přimět
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
dosavadních	dosavadní	k2eAgNnPc2d1	dosavadní
sídel	sídlo	k1gNnPc2	sídlo
v	v	k7c4	v
Chórásánu	Chórásán	k2eAgFnSc4d1	Chórásán
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
poslední	poslední	k2eAgFnSc4d1	poslední
vlnu	vlna	k1gFnSc4	vlna
kočovných	kočovný	k2eAgInPc2d1	kočovný
tureckých	turecký	k2eAgInPc2d1	turecký
kmenů	kmen	k1gInPc2	kmen
Oguzů	Oguz	k1gInPc2	Oguz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
migrovaly	migrovat	k5eAaImAgInP	migrovat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Oguzové	Oguz	k1gMnPc1	Oguz
přijali	přijmout	k5eAaPmAgMnP	přijmout
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
islám	islám	k1gInSc1	islám
a	a	k8xC	a
znali	znát	k5eAaImAgMnP	znát
arabskou	arabský	k2eAgFnSc4d1	arabská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byla	být	k5eAaImAgFnS	být
přísná	přísný	k2eAgFnSc1d1	přísná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
kmene	kmen	k1gInSc2	kmen
Oguzů	Oguz	k1gInPc2	Oguz
Ertogrul	Ertogrula	k1gFnPc2	Ertogrula
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
ikonyjského	ikonyjský	k2eAgMnSc2d1	ikonyjský
sultána	sultán	k1gMnSc2	sultán
proti	proti	k7c3	proti
Byzanci	Byzanc	k1gFnSc3	Byzanc
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
odměnou	odměna	k1gFnSc7	odměna
území	území	k1gNnSc2	území
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
východiskem	východisko	k1gNnSc7	východisko
k	k	k7c3	k
formování	formování	k1gNnSc3	formování
pozdější	pozdní	k2eAgFnSc2d2	pozdější
mocné	mocný	k2eAgFnSc2d1	mocná
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jejího	její	k3xOp3gMnSc2	její
vlastního	vlastní	k2eAgMnSc2d1	vlastní
zakladatele	zakladatel	k1gMnSc2	zakladatel
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Osman	Osman	k1gMnSc1	Osman
I.	I.	kA	I.
Gází	Gází	k1gMnSc1	Gází
(	(	kIx(	(
<g/>
1299	[number]	k4	1299
<g/>
–	–	k?	–
<g/>
1326	[number]	k4	1326
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
svěřené	svěřený	k2eAgNnSc4d1	svěřené
území	území	k1gNnSc4	území
rychle	rychle	k6eAd1	rychle
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
ve	v	k7c6	v
výboji	výboj	k1gInSc6	výboj
proti	proti	k7c3	proti
Byzanci	Byzanc	k1gFnSc3	Byzanc
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1299	[number]	k4	1299
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
samostatného	samostatný	k2eAgMnSc4d1	samostatný
vládce	vládce	k1gMnSc4	vládce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1303	[number]	k4	1303
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslední	poslední	k2eAgMnSc1d1	poslední
příslušník	příslušník	k1gMnSc1	příslušník
ikonyjské	ikonyjský	k2eAgFnSc2d1	ikonyjský
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
menších	malý	k2eAgMnPc2d2	menší
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
soupeřících	soupeřící	k2eAgInPc2d1	soupeřící
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
seldžučtí	seldžucký	k2eAgMnPc1d1	seldžucký
náčelníci	náčelník	k1gMnPc1	náčelník
poté	poté	k6eAd1	poté
přijali	přijmout	k5eAaPmAgMnP	přijmout
Osmanovu	Osmanův	k2eAgFnSc4d1	Osmanova
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
si	se	k3xPyFc3	se
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
kdysi	kdysi	k6eAd1	kdysi
kočovní	kočovní	k2eAgMnPc1d1	kočovní
Arabové	Arab	k1gMnPc1	Arab
rychle	rychle	k6eAd1	rychle
osvojili	osvojit	k5eAaPmAgMnP	osvojit
mořeplavbu	mořeplavba	k1gFnSc4	mořeplavba
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
podnikat	podnikat	k5eAaImF	podnikat
také	také	k9	také
pirátské	pirátský	k2eAgInPc4d1	pirátský
nájezdy	nájezd	k1gInPc4	nájezd
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
byzantský	byzantský	k2eAgInSc4d1	byzantský
Chios	Chios	k1gInSc4	Chios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osmani	Osman	k1gMnPc1	Osman
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zmocňovali	zmocňovat	k5eAaImAgMnP	zmocňovat
byzantských	byzantský	k2eAgFnPc2d1	byzantská
držav	država	k1gFnPc2	država
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byzantské	byzantský	k2eAgNnSc1d1	byzantské
vojsko	vojsko	k1gNnSc1	vojsko
nebylo	být	k5eNaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
jejich	jejich	k3xOp3gInSc4	jejich
nápor	nápor	k1gInSc4	nápor
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1326	[number]	k4	1326
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Osmanův	Osmanův	k2eAgMnSc1d1	Osmanův
syn	syn	k1gMnSc1	syn
Orhan	Orhan	k1gMnSc1	Orhan
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1326	[number]	k4	1326
<g/>
–	–	k?	–
<g/>
1359	[number]	k4	1359
<g/>
)	)	kIx)	)
byzantské	byzantský	k2eAgNnSc4d1	byzantské
město	město	k1gNnSc4	město
Bursu	bursa	k1gFnSc4	bursa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
rezidencí	rezidence	k1gFnSc7	rezidence
osmanských	osmanský	k2eAgMnPc2d1	osmanský
vládců	vládce	k1gMnPc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nyní	nyní	k6eAd1	nyní
přijali	přijmout	k5eAaPmAgMnP	přijmout
titul	titul	k1gInSc4	titul
sultán	sultána	k1gFnPc2	sultána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Pelekanu	Pelekan	k1gInSc2	Pelekan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1329	[number]	k4	1329
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c4	mezi
Turky	Turek	k1gMnPc4	Turek
a	a	k8xC	a
Byzancí	Byzanc	k1gFnSc7	Byzanc
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
vítězný	vítězný	k2eAgMnSc1d1	vítězný
Orhan	Orhan	k1gMnSc1	Orhan
I.	I.	kA	I.
vymínil	vymínit	k5eAaPmAgMnS	vymínit
placení	placení	k1gNnSc4	placení
ročního	roční	k2eAgInSc2d1	roční
tributu	tribut	k1gInSc2	tribut
za	za	k7c4	za
zbylá	zbylý	k2eAgNnPc4d1	zbylé
byzantská	byzantský	k2eAgNnPc4d1	byzantské
území	území	k1gNnPc4	území
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1331	[number]	k4	1331
padla	padnout	k5eAaImAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Osmanů	Osman	k1gMnPc2	Osman
Nikáia	Nikáius	k1gMnSc2	Nikáius
<g/>
,	,	kIx,	,
1337	[number]	k4	1337
Nikomédie	Nikomédie	k1gFnPc4	Nikomédie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Expanze	expanze	k1gFnSc2	expanze
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1354	[number]	k4	1354
dobyli	dobýt	k5eAaPmAgMnP	dobýt
osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
přístavní	přístavní	k2eAgFnSc4d1	přístavní
pevnost	pevnost	k1gFnSc4	pevnost
Gallipoli	Gallipoli	k1gNnSc2	Gallipoli
(	(	kIx(	(
<g/>
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
se	se	k3xPyFc4	se
zachytili	zachytit	k5eAaPmAgMnP	zachytit
již	již	k6eAd1	již
1352	[number]	k4	1352
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získali	získat	k5eAaPmAgMnP	získat
pevnost	pevnost	k1gFnSc4	pevnost
Cimpe	Cimp	k1gMnSc5	Cimp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
tak	tak	k6eAd1	tak
zakotvili	zakotvit	k5eAaPmAgMnP	zakotvit
na	na	k7c6	na
evropském	evropský	k2eAgNnSc6d1	Evropské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
spolucísařů	spolucísař	k1gMnPc2	spolucísař
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Kantakuzenos	Kantakuzenos	k1gMnSc1	Kantakuzenos
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vykoupit	vykoupit	k5eAaPmF	vykoupit
ji	on	k3xPp3gFnSc4	on
nazpět	nazpět	k6eAd1	nazpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Gallipole	Gallipole	k1gFnSc2	Gallipole
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Osmani	Osman	k1gMnPc1	Osman
dobývání	dobývání	k1gNnSc4	dobývání
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
první	první	k4xOgInPc1	první
útoky	útok	k1gInPc1	útok
vedly	vést	k5eAaImAgInP	vést
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Marici	Marica	k1gFnSc2	Marica
v	v	k7c6	v
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
,	,	kIx,	,
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
největší	veliký	k2eAgFnSc7d3	veliký
zátěží	zátěž	k1gFnSc7	zátěž
pro	pro	k7c4	pro
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
úbytku	úbytek	k1gInSc3	úbytek
domácího	domácí	k2eAgNnSc2d1	domácí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
buď	buď	k8xC	buď
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odvlékáno	odvlékat	k5eAaImNgNnS	odvlékat
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
nebo	nebo	k8xC	nebo
prodáváno	prodáván	k2eAgNnSc1d1	prodáváno
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
zákazům	zákaz	k1gInPc3	zákaz
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
kupovali	kupovat	k5eAaImAgMnP	kupovat
zajatce	zajatec	k1gMnPc4	zajatec
od	od	k7c2	od
Turků	Turek	k1gMnPc2	Turek
především	především	k9	především
obchodníci	obchodník	k1gMnPc1	obchodník
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
a	a	k8xC	a
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
bulharský	bulharský	k2eAgMnSc1d1	bulharský
car	car	k1gMnSc1	car
Ivan	Ivan	k1gMnSc1	Ivan
Alexandr	Alexandr	k1gMnSc1	Alexandr
s	s	k7c7	s
byzantským	byzantský	k2eAgMnSc7d1	byzantský
císařem	císař	k1gMnSc7	císař
Janem	Jan	k1gMnSc7	Jan
V.	V.	kA	V.
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Kantakuzenos	Kantakuzenos	k1gInSc1	Kantakuzenos
byl	být	k5eAaImAgInS	být
1354	[number]	k4	1354
přinucen	přinutit	k5eAaPmNgMnS	přinutit
abdikovat	abdikovat	k5eAaBmF	abdikovat
<g/>
)	)	kIx)	)
defenzívní	defenzívní	k2eAgInSc4d1	defenzívní
spolek	spolek	k1gInSc4	spolek
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Staletí	staletí	k1gNnSc1	staletí
trvající	trvající	k2eAgInPc1d1	trvající
boje	boj	k1gInPc1	boj
o	o	k7c4	o
balkánské	balkánský	k2eAgNnSc4d1	balkánské
území	území	k1gNnSc4	území
a	a	k8xC	a
o	o	k7c4	o
hegemonii	hegemonie	k1gFnSc4	hegemonie
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
však	však	k9	však
spojencům	spojenec	k1gMnPc3	spojenec
nedovolily	dovolit	k5eNaPmAgFnP	dovolit
překonat	překonat	k5eAaPmF	překonat
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
rozpory	rozpor	k1gInPc4	rozpor
a	a	k8xC	a
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
,	,	kIx,	,
zkoordinovat	zkoordinovat	k5eAaPmF	zkoordinovat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
turecké	turecký	k2eAgFnSc3d1	turecká
expanzi	expanze	k1gFnSc3	expanze
účinně	účinně	k6eAd1	účinně
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
důvodů	důvod	k1gInPc2	důvod
nepřistoupilo	přistoupit	k5eNaPmAgNnS	přistoupit
k	k	k7c3	k
bulharsko-byzantskému	bulharskoyzantský	k2eAgInSc3d1	bulharsko-byzantský
spolku	spolek	k1gInSc3	spolek
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
spojenectví	spojenectví	k1gNnSc2	spojenectví
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1364	[number]	k4	1364
došlo	dojít	k5eAaPmAgNnS	dojít
dokonce	dokonce	k9	dokonce
mezi	mezi	k7c7	mezi
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
a	a	k8xC	a
Byzancí	Byzanc	k1gFnSc7	Byzanc
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
o	o	k7c4	o
černomořské	černomořský	k2eAgNnSc4d1	černomořské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Bulharský	bulharský	k2eAgMnSc1d1	bulharský
car	car	k1gMnSc1	car
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
dohodou	dohoda	k1gFnSc7	dohoda
se	s	k7c7	s
sultánem	sultán	k1gMnSc7	sultán
Muradem	Murad	k1gInSc7	Murad
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
přiznal	přiznat	k5eAaPmAgInS	přiznat
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
Turci	Turek	k1gMnPc1	Turek
dobyli	dobýt	k5eAaPmAgMnP	dobýt
v	v	k7c6	v
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
nepochybně	pochybně	k6eNd1	pochybně
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
turecké	turecký	k2eAgInPc4d1	turecký
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
sultána	sultána	k1gFnSc1	sultána
Murada	Murada	k1gFnSc1	Murada
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1359	[number]	k4	1359
<g/>
–	–	k?	–
<g/>
1389	[number]	k4	1389
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
změnil	změnit	k5eAaPmAgInS	změnit
charakter	charakter	k1gInSc1	charakter
výbojů	výboj	k1gInPc2	výboj
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
převážně	převážně	k6eAd1	převážně
kořistnické	kořistnický	k2eAgInPc4d1	kořistnický
nájezdy	nájezd	k1gInPc4	nájezd
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
systematická	systematický	k2eAgFnSc1d1	systematická
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
dobytí	dobytí	k1gNnSc4	dobytí
a	a	k8xC	a
osídlení	osídlení	k1gNnSc4	osídlení
nových	nový	k2eAgNnPc2d1	nové
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
oblehla	oblehnout	k5eAaPmAgNnP	oblehnout
turecká	turecký	k2eAgNnPc1d1	turecké
vojska	vojsko	k1gNnPc1	vojsko
poprvé	poprvé	k6eAd1	poprvé
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
<g/>
,	,	kIx,	,
1361	[number]	k4	1361
získali	získat	k5eAaPmAgMnP	získat
Osmani	Osmaň	k1gFnSc3	Osmaň
Dimotiku	Dimotika	k1gFnSc4	Dimotika
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
Adrianopolis	Adrianopolis	k1gFnSc1	Adrianopolis
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Edirne	Edirn	k1gInSc5	Edirn
<g/>
)	)	kIx)	)
a	a	k8xC	a
přenesli	přenést	k5eAaPmAgMnP	přenést
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
svoji	svůj	k3xOyFgFnSc4	svůj
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Byzanc	Byzanc	k1gFnSc4	Byzanc
nastala	nastat	k5eAaPmAgFnS	nastat
poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
boje	boj	k1gInSc2	boj
s	s	k7c7	s
islámem	islám	k1gInSc7	islám
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
šlo	jít	k5eAaImAgNnS	jít
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
o	o	k7c4	o
samu	sám	k3xTgFnSc4	sám
existenci	existence	k1gFnSc4	existence
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
míru	míra	k1gFnSc4	míra
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
pouze	pouze	k6eAd1	pouze
upadající	upadající	k2eAgFnSc1d1	upadající
starobylá	starobylý	k2eAgFnSc1d1	starobylá
východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
živořící	živořící	k2eAgFnSc1d1	živořící
na	na	k7c6	na
zlomku	zlomek	k1gInSc6	zlomek
svých	svůj	k3xOyFgMnPc2	svůj
bývalých	bývalý	k2eAgMnPc2d1	bývalý
území	území	k1gNnPc1	území
a	a	k8xC	a
rozpadající	rozpadající	k2eAgFnPc1d1	rozpadající
se	se	k3xPyFc4	se
na	na	k7c4	na
poloautonomní	poloautonomní	k2eAgFnPc4d1	poloautonomní
provincie	provincie	k1gFnPc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
expanze	expanze	k1gFnSc1	expanze
představovala	představovat	k5eAaImAgFnS	představovat
nanejvýše	nanejvýše	k6eAd1	nanejvýše
aktuální	aktuální	k2eAgNnSc4d1	aktuální
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
také	také	k9	také
pro	pro	k7c4	pro
decentralizující	decentralizující	k2eAgInPc4d1	decentralizující
se	s	k7c7	s
státy	stát	k1gInPc7	stát
srbský	srbský	k2eAgInSc1d1	srbský
a	a	k8xC	a
bulharský	bulharský	k2eAgInSc1d1	bulharský
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
samy	sám	k3xTgFnPc1	sám
ještě	ještě	k9	ještě
nedávno	nedávno	k6eAd1	nedávno
budovaly	budovat	k5eAaImAgFnP	budovat
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
svoje	svůj	k3xOyFgNnSc4	svůj
mocenské	mocenský	k2eAgNnSc4d1	mocenské
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Balkánské	balkánský	k2eAgFnPc1d1	balkánská
země	zem	k1gFnPc1	zem
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
proti	proti	k7c3	proti
osmanským	osmanský	k2eAgInPc3d1	osmanský
výbojům	výboj	k1gInPc3	výboj
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
obrannou	obranný	k2eAgFnSc4d1	obranná
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
na	na	k7c4	na
případnou	případný	k2eAgFnSc4d1	případná
pomoc	pomoc	k1gFnSc4	pomoc
západních	západní	k2eAgMnPc2d1	západní
křesťanů	křesťan	k1gMnPc2	křesťan
nemohly	moct	k5eNaImAgFnP	moct
spoléhat	spoléhat	k5eAaImF	spoléhat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Naléhavost	naléhavost	k1gFnSc1	naléhavost
turecké	turecký	k2eAgFnSc2d1	turecká
hrozby	hrozba	k1gFnSc2	hrozba
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
podceňovali	podceňovat	k5eAaImAgMnP	podceňovat
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
vládcové	vládce	k1gMnPc1	vládce
dokonce	dokonce	k9	dokonce
neváhali	váhat	k5eNaImAgMnP	váhat
využít	využít	k5eAaPmF	využít
Osmany	Osman	k1gMnPc4	Osman
v	v	k7c6	v
intencích	intence	k1gFnPc6	intence
svých	svůj	k3xOyFgMnPc2	svůj
politických	politický	k2eAgMnPc2d1	politický
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
sjednotit	sjednotit	k5eAaPmF	sjednotit
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
Východ	východ	k1gInSc4	východ
a	a	k8xC	a
Západ	západ	k1gInSc4	západ
proti	proti	k7c3	proti
muslimským	muslimský	k2eAgInPc3d1	muslimský
Turkům	turek	k1gInPc3	turek
na	na	k7c6	na
základě	základ	k1gInSc6	základ
církevní	církevní	k2eAgFnSc2d1	církevní
unie	unie	k1gFnSc2	unie
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
na	na	k7c6	na
náboženských	náboženský	k2eAgInPc6d1	náboženský
protikladech	protiklad	k1gInPc6	protiklad
mezi	mezi	k7c7	mezi
ortodoxními	ortodoxní	k2eAgMnPc7d1	ortodoxní
a	a	k8xC	a
Latiny	Latin	k1gMnPc4	Latin
i	i	k9	i
na	na	k7c6	na
partikulárních	partikulární	k2eAgInPc6d1	partikulární
politických	politický	k2eAgInPc6d1	politický
zájmech	zájem	k1gInPc6	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Vazalským	vazalský	k2eAgInSc7d1	vazalský
státem	stát	k1gInSc7	stát
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1371	[number]	k4	1371
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Marici	Marica	k1gFnSc2	Marica
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k9	také
Byzanc	Byzanc	k1gFnSc1	Byzanc
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
platit	platit	k5eAaImF	platit
tribut	tribut	k1gInSc4	tribut
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
vojenské	vojenský	k2eAgInPc4d1	vojenský
kontingenty	kontingent	k1gInPc4	kontingent
vlastnímu	vlastní	k2eAgMnSc3d1	vlastní
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Pomoci	pomoct	k5eAaPmF	pomoct
sultánu	sultán	k1gMnSc3	sultán
Muradovi	Murada	k1gMnSc3	Murada
I.	I.	kA	I.
musel	muset	k5eAaImAgMnS	muset
například	například	k6eAd1	například
během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
zbylým	zbylý	k2eAgMnPc3d1	zbylý
seldžuckým	seldžucký	k2eAgMnPc3d1	seldžucký
emirátům	emirát	k1gInPc3	emirát
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Manuél	Manuél	k1gMnSc1	Manuél
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1391	[number]	k4	1391
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
jako	jako	k9	jako
rukojmí	rukojmí	k1gNnSc4	rukojmí
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
pevnosti	pevnost	k1gFnSc3	pevnost
Filadelfeia	Filadelfeia	k1gFnSc1	Filadelfeia
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
maloasijských	maloasijský	k2eAgFnPc2d1	maloasijská
byzantských	byzantský	k2eAgFnPc2d1	byzantská
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Osmanští	osmanský	k2eAgMnPc1d1	osmanský
panovníci	panovník	k1gMnPc1	panovník
si	se	k3xPyFc3	se
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
osobovali	osobovat	k5eAaImAgMnP	osobovat
právo	právo	k1gNnSc4	právo
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
obsazování	obsazování	k1gNnSc2	obsazování
císařského	císařský	k2eAgInSc2d1	císařský
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Murad	Murad	k1gInSc1	Murad
I.	I.	kA	I.
začal	začít	k5eAaPmAgInS	začít
přidělovat	přidělovat	k5eAaImF	přidělovat
na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
půdu	půda	k1gFnSc4	půda
svým	svůj	k3xOyFgMnPc3	svůj
vojákům	voják	k1gMnPc3	voják
a	a	k8xC	a
usídlovat	usídlovat	k5eAaImF	usídlovat
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zdejší	zdejší	k2eAgNnSc1d1	zdejší
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
přesídlováno	přesídlovat	k5eAaImNgNnS	přesídlovat
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
těmto	tento	k3xDgFnPc3	tento
tíživým	tíživý	k2eAgFnPc3d1	tíživá
okolnostem	okolnost	k1gFnPc3	okolnost
získal	získat	k5eAaPmAgInS	získat
byzantský	byzantský	k2eAgInSc1d1	byzantský
státeček	státeček	k1gInSc1	státeček
relativně	relativně	k6eAd1	relativně
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
taženích	tažení	k1gNnPc6	tažení
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
pověst	pověst	k1gFnSc4	pověst
nedobytného	dobytný	k2eNgNnSc2d1	nedobytné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
vyhýbala	vyhýbat	k5eAaImAgFnS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
zájmu	zájem	k1gInSc2	zájem
Osmanů	Osman	k1gMnPc2	Osman
stálo	stát	k5eAaImAgNnS	stát
nyní	nyní	k6eAd1	nyní
především	především	k6eAd1	především
bulharské	bulharský	k2eAgNnSc4d1	bulharské
<g/>
,	,	kIx,	,
srbské	srbský	k2eAgNnSc4d1	srbské
a	a	k8xC	a
chorvatské	chorvatský	k2eAgNnSc4d1	Chorvatské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
se	se	k3xPyFc4	se
Muradovi	Muradův	k2eAgMnPc1d1	Muradův
bojovníci	bojovník	k1gMnPc1	bojovník
vypravili	vypravit	k5eAaPmAgMnP	vypravit
proti	proti	k7c3	proti
trnovskému	trnovský	k2eAgNnSc3d1	trnovský
carství	carství	k1gNnSc3	carství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
vzdala	vzdát	k5eAaPmAgFnS	vzdát
Sofie	Sofie	k1gFnSc1	Sofie
(	(	kIx(	(
<g/>
1382	[number]	k4	1382
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1385	[number]	k4	1385
porazili	porazit	k5eAaPmAgMnP	porazit
vládce	vládce	k1gMnSc4	vládce
srbské	srbský	k2eAgFnSc2d1	Srbská
Zety	Zet	k1gMnPc7	Zet
a	a	k8xC	a
poté	poté	k6eAd1	poté
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
padla	padnout	k5eAaImAgFnS	padnout
Niš	Niš	k1gFnSc1	Niš
(	(	kIx(	(
<g/>
1386	[number]	k4	1386
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
získali	získat	k5eAaPmAgMnP	získat
zbývající	zbývající	k2eAgFnSc4d1	zbývající
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
,	,	kIx,	,
pohraničí	pohraničí	k1gNnSc6	pohraničí
Thesálie	Thesálie	k1gFnSc2	Thesálie
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
také	také	k9	také
Soluň	Soluň	k1gFnSc1	Soluň
(	(	kIx(	(
<g/>
1387	[number]	k4	1387
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
tureckou	turecký	k2eAgFnSc7d1	turecká
expanzí	expanze	k1gFnSc7	expanze
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
slovanští	slovanštit	k5eAaImIp3nP	slovanštit
vládci	vládce	k1gMnPc1	vládce
kníže	kníže	k1gMnSc1	kníže
Lazar	Lazar	k1gMnSc1	Lazar
Hrebeljanović	Hrebeljanović	k1gMnSc1	Hrebeljanović
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
1371	[number]	k4	1371
významně	významně	k6eAd1	významně
upevnil	upevnit	k5eAaPmAgInS	upevnit
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
,	,	kIx,	,
bosenský	bosenský	k2eAgMnSc1d1	bosenský
král	král	k1gMnSc1	král
Tvrtko	Tvrtka	k1gFnSc5	Tvrtka
I.	I.	kA	I.
a	a	k8xC	a
bulharský	bulharský	k2eAgMnSc1d1	bulharský
car	car	k1gMnSc1	car
Ivan	Ivan	k1gMnSc1	Ivan
Šišman	Šišman	k1gMnSc1	Šišman
roku	rok	k1gInSc2	rok
1387	[number]	k4	1387
protitureckou	protiturecký	k2eAgFnSc4d1	protiturecká
koalici	koalice	k1gFnSc4	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
porazili	porazit	k5eAaPmAgMnP	porazit
Srbové	Srb	k1gMnPc1	Srb
a	a	k8xC	a
Bosňané	Bosňan	k1gMnPc1	Bosňan
(	(	kIx(	(
<g/>
bulharské	bulharský	k2eAgInPc1d1	bulharský
oddíly	oddíl	k1gInPc1	oddíl
nestačily	stačit	k5eNaBmAgInP	stačit
dorazit	dorazit	k5eAaPmF	dorazit
<g/>
)	)	kIx)	)
Osmany	Osman	k1gMnPc4	Osman
u	u	k7c2	u
Pločniku	Pločnik	k1gInSc2	Pločnik
nad	nad	k7c7	nad
Toplicí	Toplice	k1gFnSc7	Toplice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tureckého	turecký	k2eAgNnSc2d1	turecké
svědectví	svědectví	k1gNnSc2	svědectví
to	ten	k3xDgNnSc1	ten
prý	prý	k9	prý
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
porážka	porážka	k1gFnSc1	porážka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
přimělo	přimět	k5eAaPmAgNnS	přimět
Murada	Murada	k1gFnSc1	Murada
I.	I.	kA	I.
k	k	k7c3	k
mohutné	mohutný	k2eAgFnSc3d1	mohutná
ofenzívě	ofenzíva	k1gFnSc3	ofenzíva
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1388	[number]	k4	1388
zamířilo	zamířit	k5eAaPmAgNnS	zamířit
turecké	turecký	k2eAgNnSc1d1	turecké
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
samotným	samotný	k2eAgMnSc7d1	samotný
sultánem	sultán	k1gMnSc7	sultán
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
obnovit	obnovit	k5eAaPmF	obnovit
poplatnou	poplatný	k2eAgFnSc4d1	poplatná
závislost	závislost	k1gFnSc4	závislost
trnovského	trnovský	k2eAgNnSc2d1	trnovský
carství	carství	k1gNnSc2	carství
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Šišman	Šišman	k1gMnSc1	Šišman
byl	být	k5eAaImAgMnS	být
donucen	donucen	k2eAgMnSc1d1	donucen
k	k	k7c3	k
poslušnosti	poslušnost	k1gFnSc3	poslušnost
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tureckými	turecký	k2eAgInPc7d1	turecký
vazaly	vazal	k1gMnPc4	vazal
stali	stát	k5eAaPmAgMnP	stát
vidinský	vidinský	k2eAgMnSc1d1	vidinský
vládce	vládce	k1gMnSc1	vládce
Ivan	Ivan	k1gMnSc1	Ivan
Stracimir	Stracimir	k1gMnSc1	Stracimir
a	a	k8xC	a
dobrudžský	dobrudžský	k2eAgMnSc1d1	dobrudžský
despota	despota	k1gMnSc1	despota
Ivanko	Ivanka	k1gFnSc5	Ivanka
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
obrovská	obrovský	k2eAgFnSc1d1	obrovská
turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
vypravila	vypravit	k5eAaPmAgFnS	vypravit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Osudová	osudový	k2eAgFnSc1d1	osudová
srážka	srážka	k1gFnSc1	srážka
srbských	srbský	k2eAgInPc2d1	srbský
a	a	k8xC	a
bosenských	bosenský	k2eAgNnPc2d1	bosenské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Bulhaři	Bulhar	k1gMnPc1	Bulhar
<g/>
,	,	kIx,	,
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
,	,	kIx,	,
Valaši	Valach	k1gMnPc1	Valach
a	a	k8xC	a
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
s	s	k7c7	s
Muradovou	Muradový	k2eAgFnSc7d1	Muradový
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
srbskými	srbský	k2eAgMnPc7d1	srbský
vazaly	vazal	k1gMnPc7	vazal
<g/>
,	,	kIx,	,
předurčila	předurčit	k5eAaPmAgFnS	předurčit
osud	osud	k1gInSc4	osud
balkánských	balkánský	k2eAgInPc2d1	balkánský
národů	národ	k1gInPc2	národ
na	na	k7c4	na
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všeobecně	všeobecně	k6eAd1	všeobecně
panující	panující	k2eAgFnSc3d1	panující
představě	představa	k1gFnSc3	představa
o	o	k7c6	o
národní	národní	k2eAgFnSc6d1	národní
tragédii	tragédie	k1gFnSc6	tragédie
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
opředené	opředený	k2eAgFnPc1d1	opředená
mnoha	mnoho	k4c7	mnoho
idealizujícími	idealizující	k2eAgInPc7d1	idealizující
mýty	mýtus	k1gInPc7	mýtus
o	o	k7c6	o
velkosrbské	velkosrbský	k2eAgFnSc6d1	velkosrbská
středověké	středověký	k2eAgFnSc6d1	středověká
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
soudí	soudit	k5eAaImIp3nP	soudit
dnes	dnes	k6eAd1	dnes
historici	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1389	[number]	k4	1389
na	na	k7c6	na
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nerozhodně	rozhodně	k6eNd1	rozhodně
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
utrpěly	utrpět	k5eAaPmAgFnP	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
o	o	k7c4	o
život	život	k1gInSc4	život
přišli	přijít	k5eAaPmAgMnP	přijít
také	také	k9	také
oba	dva	k4xCgMnPc1	dva
vrchní	vrchní	k2eAgMnPc1d1	vrchní
velitelé	velitel	k1gMnPc1	velitel
<g/>
,	,	kIx,	,
Murad	Murad	k1gInSc1	Murad
I.	I.	kA	I.
a	a	k8xC	a
Lazar	Lazar	k1gMnSc1	Lazar
Hrebeljanović	Hrebeljanović	k1gMnSc1	Hrebeljanović
<g/>
.	.	kIx.	.
</s>
<s>
Sultána	sultán	k1gMnSc4	sultán
probodl	probodnout	k5eAaPmAgMnS	probodnout
zřejmě	zřejmě	k6eAd1	zřejmě
ještě	ještě	k9	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
bitvy	bitva	k1gFnSc2	bitva
příslušník	příslušník	k1gMnSc1	příslušník
Lazarovy	Lazarův	k2eAgFnSc2d1	Lazarova
družiny	družina	k1gFnSc2	družina
Miloš	Miloš	k1gMnSc1	Miloš
Obilić	Obilić	k1gMnSc1	Obilić
(	(	kIx(	(
<g/>
Kobilić	Kobilić	k1gMnSc1	Kobilić
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
lstí	lest	k1gFnSc7	lest
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
tureckého	turecký	k2eAgInSc2d1	turecký
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc4	velení
poté	poté	k6eAd1	poté
převzal	převzít	k5eAaPmAgMnS	převzít
Muradův	Muradův	k2eAgMnSc1d1	Muradův
syn	syn	k1gMnSc1	syn
Bajezid	Bajezida	k1gFnPc2	Bajezida
I.	I.	kA	I.
Ildrim	Ildrim	k1gMnSc1	Ildrim
(	(	kIx(	(
<g/>
1389	[number]	k4	1389
<g/>
–	–	k?	–
<g/>
1402	[number]	k4	1402
<g/>
)	)	kIx)	)
a	a	k8xC	a
bitvu	bitva	k1gFnSc4	bitva
dovedl	dovést	k5eAaPmAgMnS	dovést
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Zajatý	zajatý	k2eAgMnSc1d1	zajatý
kníže	kníže	k1gMnSc1	kníže
Lazar	Lazar	k1gMnSc1	Lazar
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
srbští	srbský	k2eAgMnPc1d1	srbský
velmoži	velmož	k1gMnPc1	velmož
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
odvetu	odvet	k1gInSc6	odvet
za	za	k7c4	za
sultánovo	sultánův	k2eAgNnSc4d1	sultánovo
zavraždění	zavraždění	k1gNnSc4	zavraždění
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
příkazu	příkaz	k1gInSc2	příkaz
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Tragické	tragický	k2eAgInPc1d1	tragický
důsledky	důsledek	k1gInPc1	důsledek
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
teprve	teprve	k6eAd1	teprve
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
svoje	svůj	k3xOyFgFnPc4	svůj
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vzápětí	vzápětí	k6eAd1	vzápětí
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
napadeno	napaden	k2eAgNnSc1d1	napadeno
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
donutilo	donutit	k5eAaPmAgNnS	donutit
kněžnu	kněžna	k1gFnSc4	kněžna
vdovu	vdova	k1gFnSc4	vdova
Milicu	Milica	k1gFnSc4	Milica
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vládla	vládnout	k5eAaImAgFnS	vládnout
za	za	k7c4	za
Lazarovy	Lazarův	k2eAgMnPc4d1	Lazarův
nedospělé	dospělý	k2eNgMnPc4d1	nedospělý
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
přijmout	přijmout	k5eAaPmF	přijmout
Bajezidovy	Bajezidův	k2eAgFnPc4d1	Bajezidův
mírové	mírový	k2eAgFnPc4d1	mírová
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vazalským	vazalský	k2eAgInSc7d1	vazalský
státem	stát	k1gInSc7	stát
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
platit	platit	k5eAaImF	platit
Osmanům	Osman	k1gMnPc3	Osman
tribut	tribut	k1gInSc4	tribut
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
jim	on	k3xPp3gMnPc3	on
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
srbských	srbský	k2eAgFnPc2d1	Srbská
měst	město	k1gNnPc2	město
umístěny	umístit	k5eAaPmNgFnP	umístit
turecké	turecký	k2eAgFnPc1d1	turecká
posádky	posádka	k1gFnPc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
hůře	zle	k6eAd2	zle
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Turci	Turek	k1gMnPc1	Turek
se	se	k3xPyFc4	se
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
Srbska	Srbsko	k1gNnSc2	Srbsko
do	do	k7c2	do
vazalské	vazalský	k2eAgFnSc2d1	vazalská
závislosti	závislost	k1gFnSc2	závislost
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
konečné	konečný	k2eAgNnSc4d1	konečné
uspořádání	uspořádání	k1gNnSc4	uspořádání
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1391	[number]	k4	1391
překročila	překročit	k5eAaPmAgFnS	překročit
část	část	k1gFnSc1	část
tureckého	turecký	k2eAgNnSc2d1	turecké
vojska	vojsko	k1gNnSc2	vojsko
Dunaj	Dunaj	k1gInSc1	Dunaj
a	a	k8xC	a
napadla	napadnout	k5eAaPmAgFnS	napadnout
Valašsko	Valašsko	k1gNnSc4	Valašsko
<g/>
.	.	kIx.	.
</s>
<s>
Valašský	valašský	k2eAgMnSc1d1	valašský
kníže	kníže	k1gMnSc1	kníže
Mircea	Mircea	k1gMnSc1	Mircea
I.	I.	kA	I.
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgInS	zavázat
k	k	k7c3	k
poplatné	poplatný	k2eAgFnSc3d1	poplatná
závislosti	závislost	k1gFnSc3	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
Turci	Turek	k1gMnPc1	Turek
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
zde	zde	k6eAd1	zde
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
poraženi	poražen	k2eAgMnPc1d1	poražen
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
příležitosti	příležitost	k1gFnSc3	příležitost
využil	využít	k5eAaPmAgMnS	využít
trnovský	trnovský	k2eAgMnSc1d1	trnovský
car	car	k1gMnSc1	car
Ivan	Ivan	k1gMnSc1	Ivan
Šišman	Šišman	k1gMnSc1	Šišman
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
uherského	uherský	k2eAgMnSc4d1	uherský
krále	král	k1gMnSc4	král
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgInS	vypravit
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
do	do	k7c2	do
Nikopole	Nikopole	k1gFnSc2	Nikopole
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
Bajezid	Bajezid	k1gInSc1	Bajezid
<g/>
,	,	kIx,	,
že	že	k8xS	že
nespolehlivého	spolehlivý	k2eNgMnSc4d1	nespolehlivý
vazala	vazal	k1gMnSc4	vazal
potrestá	potrestat	k5eAaPmIp3nS	potrestat
<g/>
,	,	kIx,	,
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Trnovo	Trnovo	k1gNnSc4	Trnovo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
urputné	urputný	k2eAgFnSc2d1	urputná
obrany	obrana	k1gFnSc2	obrana
padlo	padnout	k5eAaImAgNnS	padnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1393	[number]	k4	1393
sídelní	sídelní	k2eAgInSc4d1	sídelní
město	město	k1gNnSc1	město
druhého	druhý	k4xOgNnSc2	druhý
bulharského	bulharský	k2eAgNnSc2d1	bulharské
carství	carství	k1gNnSc2	carství
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Osmanů	Osman	k1gMnPc2	Osman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
vyrabovali	vyrabovat	k5eAaPmAgMnP	vyrabovat
<g/>
,	,	kIx,	,
zpustošili	zpustošit	k5eAaPmAgMnP	zpustošit
výstavné	výstavný	k2eAgInPc4d1	výstavný
paláce	palác	k1gInPc4	palác
i	i	k8xC	i
kostely	kostel	k1gInPc4	kostel
a	a	k8xC	a
zdecimovali	zdecimovat	k5eAaPmAgMnP	zdecimovat
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jim	on	k3xPp3gMnPc3	on
Bajezid	Bajezid	k1gInSc1	Bajezid
osobně	osobně	k6eAd1	osobně
zaručil	zaručit	k5eAaPmAgInS	zaručit
bezpečí	bezpečí	k1gNnSc4	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
bojlarů	bojlar	k1gInPc2	bojlar
bylo	být	k5eAaImAgNnS	být
povražděno	povražděn	k2eAgNnSc1d1	povražděno
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgFnPc2d1	významná
měšťanských	měšťanský	k2eAgFnPc2d1	měšťanská
rodin	rodina	k1gFnPc2	rodina
přesídleno	přesídlet	k5eAaPmNgNnS	přesídlet
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Zčásti	zčásti	k6eAd1	zčásti
vylidněné	vylidněný	k2eAgNnSc4d1	vylidněné
Trnovo	Trnovo	k1gNnSc4	Trnovo
osídlili	osídlit	k5eAaPmAgMnP	osídlit
turečtí	turecký	k2eAgMnPc1d1	turecký
kolonisté	kolonista	k1gMnPc1	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Bajezid	Bajezid	k1gInSc1	Bajezid
obrátil	obrátit	k5eAaPmAgInS	obrátit
proti	proti	k7c3	proti
Nikopoli	Nikopole	k1gFnSc3	Nikopole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zajal	zajmout	k5eAaPmAgMnS	zajmout
posledního	poslední	k2eAgMnSc4d1	poslední
bulharského	bulharský	k2eAgMnSc4d1	bulharský
cara	car	k1gMnSc4	car
Ivana	Ivan	k1gMnSc4	Ivan
Šišmana	Šišman	k1gMnSc4	Šišman
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
později	pozdě	k6eAd2	pozdě
buď	buď	k8xC	buď
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgMnS	být
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
si	se	k3xPyFc3	se
turečtí	turecký	k2eAgMnPc1d1	turecký
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
podmanili	podmanit	k5eAaPmAgMnP	podmanit
Dobrudžu	Dobrudža	k1gFnSc4	Dobrudža
(	(	kIx(	(
<g/>
1393	[number]	k4	1393
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vidinský	vidinský	k2eAgMnSc1d1	vidinský
vládce	vládce	k1gMnSc1	vládce
Ivan	Ivan	k1gMnSc1	Ivan
Stracimir	Stracimir	k1gMnSc1	Stracimir
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
vpustit	vpustit	k5eAaPmF	vpustit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
turecké	turecký	k2eAgFnSc2d1	turecká
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
chránit	chránit	k5eAaImF	chránit
její	její	k3xOp3gFnPc4	její
hranice	hranice	k1gFnPc4	hranice
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1395	[number]	k4	1395
se	se	k3xPyFc4	se
Mirčovi	Mirč	k1gMnSc3	Mirč
Starému	Starý	k1gMnSc3	Starý
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
s	s	k7c7	s
uherskou	uherský	k2eAgFnSc7d1	uherská
pomocí	pomoc	k1gFnSc7	pomoc
turecké	turecký	k2eAgNnSc1d1	turecké
vojsko	vojsko	k1gNnSc1	vojsko
u	u	k7c2	u
Rovine	Rovin	k1gInSc5	Rovin
(	(	kIx(	(
<g/>
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
zahynul	zahynout	k5eAaPmAgMnS	zahynout
turecký	turecký	k2eAgMnSc1d1	turecký
vazal	vazal	k1gMnSc1	vazal
Marko	Marko	k1gMnSc1	Marko
z	z	k7c2	z
Prilepu	Prilep	k1gInSc2	Prilep
a	a	k8xC	a
Osmani	Osman	k1gMnPc1	Osman
poté	poté	k6eAd1	poté
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
celou	celý	k2eAgFnSc4d1	celá
Makedonii	Makedonie	k1gFnSc4	Makedonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Valašsko	Valašsko	k1gNnSc1	Valašsko
sice	sice	k8xC	sice
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nadále	nadále	k6eAd1	nadále
poplatně	poplatně	k6eAd1	poplatně
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
požívalo	požívat	k5eAaImAgNnS	požívat
zatím	zatím	k6eAd1	zatím
značné	značný	k2eAgFnSc2d1	značná
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Turci	Turek	k1gMnPc1	Turek
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
křesťané	křesťan	k1gMnPc1	křesťan
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
obávat	obávat	k5eAaImF	obávat
stále	stále	k6eAd1	stále
silnější	silný	k2eAgFnSc2d2	silnější
islámské	islámský	k2eAgFnSc2d1	islámská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
sestavil	sestavit	k5eAaPmAgMnS	sestavit
s	s	k7c7	s
požehnáním	požehnání	k1gNnSc7	požehnání
papeže	papež	k1gMnSc2	papež
spojenou	spojený	k2eAgFnSc4d1	spojená
křižáckou	křižácký	k2eAgFnSc4d1	křižácká
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
vojáci	voják	k1gMnPc1	voják
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
proti	proti	k7c3	proti
Osmanům	Osman	k1gMnPc3	Osman
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Nikopole	Nikopole	k1gFnSc2	Nikopole
v	v	k7c6	v
září	září	k1gNnSc6	září
1396	[number]	k4	1396
byla	být	k5eAaImAgNnP	být
Zikmundova	Zikmundův	k2eAgNnPc1d1	Zikmundovo
vojska	vojsko	k1gNnPc1	vojsko
těžce	těžce	k6eAd1	těžce
poražena	poražen	k2eAgNnPc1d1	poraženo
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Zikmund	Zikmund	k1gMnSc1	Zikmund
musel	muset	k5eAaImAgMnS	muset
rychle	rychle	k6eAd1	rychle
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
bojovala	bojovat	k5eAaImAgFnS	bojovat
spousta	spousta	k1gFnSc1	spousta
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
obsazených	obsazený	k2eAgInPc2d1	obsazený
vazalských	vazalský	k2eAgInPc2d1	vazalský
balkánských	balkánský	k2eAgInPc2d1	balkánský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
respektovaná	respektovaný	k2eAgFnSc1d1	respektovaná
mnohonárodní	mnohonárodní	k2eAgFnSc1d1	mnohonárodní
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
turecká	turecký	k2eAgFnSc1d1	turecká
<g/>
)	)	kIx)	)
velmoc	velmoc	k1gFnSc1	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
preferovaným	preferovaný	k2eAgNnSc7d1	preferované
náboženstvím	náboženství	k1gNnSc7	náboženství
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
tolerovaly	tolerovat	k5eAaImAgInP	tolerovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
církve	církev	k1gFnPc1	církev
(	(	kIx(	(
<g/>
pravoslavné	pravoslavný	k2eAgNnSc1d1	pravoslavné
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
judaismus	judaismus	k1gInSc1	judaismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
katolické	katolický	k2eAgFnSc2d1	katolická
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvrdě	tvrdě	k6eAd1	tvrdě
potlačovala	potlačovat	k5eAaImAgFnS	potlačovat
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Osmanské	osmanský	k2eAgNnSc1d1	osmanské
interregnum	interregnum	k1gNnSc1	interregnum
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
sultán	sultána	k1gFnPc2	sultána
Bajezid	Bajezida	k1gFnPc2	Bajezida
I.	I.	kA	I.
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
ji	on	k3xPp3gFnSc4	on
definitivně	definitivně	k6eAd1	definitivně
dobýt	dobýt	k5eAaPmF	dobýt
a	a	k8xC	a
zabrat	zabrat	k5eAaPmF	zabrat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
obléhání	obléhání	k1gNnSc6	obléhání
však	však	k9	však
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
změnit	změnit	k5eAaPmF	změnit
plány	plán	k1gInPc1	plán
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
začal	začít	k5eAaPmAgInS	začít
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
mocný	mocný	k2eAgMnSc1d1	mocný
uzbecký	uzbecký	k2eAgMnSc1d1	uzbecký
vládce	vládce	k1gMnSc1	vládce
s	s	k7c7	s
mongolskými	mongolský	k2eAgInPc7d1	mongolský
kořeny	kořen	k1gInPc7	kořen
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
krutější	krutý	k2eAgMnSc1d2	krutější
bojovník	bojovník	k1gMnSc1	bojovník
Timur	Timur	k1gMnSc1	Timur
Lenk	Lenk	k1gMnSc1	Lenk
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Tamerlán	Tamerlán	k1gInSc1	Tamerlán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bajezid	Bajezid	k1gInSc1	Bajezid
podcenil	podcenit	k5eAaPmAgInS	podcenit
sílu	síla	k1gFnSc4	síla
svého	svůj	k3xOyFgMnSc2	svůj
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
utkal	utkat	k5eAaPmAgMnS	utkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Ankary	Ankara	k1gFnSc2	Ankara
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
poražen	poražen	k2eAgMnSc1d1	poražen
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
sultán	sultán	k1gMnSc1	sultán
byl	být	k5eAaImAgMnS	být
zajat	zajmout	k5eAaPmNgMnS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
nastal	nastat	k5eAaPmAgInS	nastat
kritický	kritický	k2eAgInSc1d1	kritický
okamžik	okamžik	k1gInSc1	okamžik
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
osmanské	osmanský	k2eAgNnSc1d1	osmanské
interregnum	interregnum	k1gNnSc1	interregnum
<g/>
.	.	kIx.	.
</s>
<s>
Bajezid	Bajezid	k1gInSc1	Bajezid
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
uzbeckém	uzbecký	k2eAgNnSc6d1	uzbecké
zajetí	zajetí	k1gNnSc6	zajetí
za	za	k7c2	za
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1403	[number]	k4	1403
<g/>
.	.	kIx.	.
</s>
<s>
Tamerlán	Tamerlán	k1gInSc1	Tamerlán
po	po	k7c6	po
zpustošení	zpustošení	k1gNnSc6	zpustošení
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
své	svůj	k3xOyFgNnSc4	svůj
dobyvatelské	dobyvatelský	k2eAgNnSc4d1	dobyvatelské
úsilí	úsilí	k1gNnSc4	úsilí
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
synů	syn	k1gMnPc2	syn
Bajezida	Bajezid	k1gMnSc2	Bajezid
se	se	k3xPyFc4	se
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
sultánovi	sultánův	k2eAgMnPc1d1	sultánův
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nárokoval	nárokovat	k5eAaImAgInS	nárokovat
trůn	trůn	k1gInSc4	trůn
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nastala	nastat	k5eAaPmAgFnS	nastat
série	série	k1gFnSc1	série
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgMnPc2	tento
bratrů	bratr	k1gMnPc2	bratr
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
zabita	zabit	k2eAgFnSc1d1	zabita
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1413	[number]	k4	1413
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
sultánem	sultán	k1gMnSc7	sultán
Mehmed	Mehmed	k1gMnSc1	Mehmed
I.	I.	kA	I.
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
konsolidovat	konsolidovat	k5eAaBmF	konsolidovat
zničenou	zničený	k2eAgFnSc4d1	zničená
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
této	tento	k3xDgFnSc2	tento
bratrovražedné	bratrovražedný	k2eAgFnSc2d1	bratrovražedná
války	válka	k1gFnSc2	válka
museli	muset	k5eAaImAgMnP	muset
řešit	řešit	k5eAaImF	řešit
ještě	ještě	k6eAd1	ještě
Mehmedovi	Mehmed	k1gMnSc3	Mehmed
nástupci	nástupce	k1gMnSc3	nástupce
Murad	Murad	k1gInSc4	Murad
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
také	také	k9	také
Mehmed	Mehmed	k1gInSc4	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pád	Pád	k1gInSc1	Pád
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
Evropa	Evropa	k1gFnSc1	Evropa
sužovaná	sužovaný	k2eAgFnSc1d1	sužovaná
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozbroji	rozbroj	k1gInPc7	rozbroj
(	(	kIx(	(
<g/>
stoletá	stoletý	k2eAgFnSc1d1	stoletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
papežské	papežský	k2eAgNnSc1d1	papežské
schizma	schizma	k1gNnSc1	schizma
<g/>
,	,	kIx,	,
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
využít	využít	k5eAaPmF	využít
dočasného	dočasný	k2eAgNnSc2d1	dočasné
bezvládí	bezvládí	k1gNnSc2	bezvládí
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
postupně	postupně	k6eAd1	postupně
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
výbojích	výboj	k1gInPc6	výboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1444	[number]	k4	1444
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
polská	polský	k2eAgFnSc1d1	polská
a	a	k8xC	a
maďarská	maďarský	k2eAgNnPc1d1	Maďarské
vojska	vojsko	k1gNnPc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Vladislava	Vladislav	k1gMnSc2	Vladislav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Hunyadiho	Hunyadi	k1gMnSc2	Hunyadi
a	a	k8xC	a
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c4	na
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
však	však	k9	však
poraženi	poražen	k2eAgMnPc1d1	poražen
sultánem	sultán	k1gMnSc7	sultán
Muradem	Murad	k1gInSc7	Murad
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varny	Varna	k1gFnSc2	Varna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
padl	padnout	k5eAaPmAgMnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1453	[number]	k4	1453
sultán	sultán	k1gMnSc1	sultán
Mehmed	Mehmed	k1gMnSc1	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
dobyl	dobýt	k5eAaPmAgMnS	dobýt
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
kdysi	kdysi	k6eAd1	kdysi
všemocné	všemocný	k2eAgFnSc2d1	všemocná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
slabé	slabý	k2eAgFnSc2d1	slabá
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
a	a	k8xC	a
učinil	učinit	k5eAaPmAgInS	učinit
ji	on	k3xPp3gFnSc4	on
svým	svůj	k3xOyFgNnSc7	svůj
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
svoje	svůj	k3xOyFgFnPc4	svůj
državy	država	k1gFnPc4	država
jak	jak	k8xC	jak
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1453	[number]	k4	1453
a	a	k8xC	a
1520	[number]	k4	1520
tak	tak	k6eAd1	tak
získali	získat	k5eAaPmAgMnP	získat
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
pod	pod	k7c4	pod
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
též	též	k9	též
začlenili	začlenit	k5eAaPmAgMnP	začlenit
Valašsko	Valašsko	k1gNnSc4	Valašsko
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
valašský	valašský	k2eAgInSc1d1	valašský
pašalík	pašalík	k1gInSc1	pašalík
<g/>
)	)	kIx)	)
a	a	k8xC	a
Krymský	krymský	k2eAgInSc1d1	krymský
chanát	chanát	k1gInSc1	chanát
<g/>
.	.	kIx.	.
</s>
<s>
Osmanští	osmanský	k2eAgMnPc1d1	osmanský
vládcové	vládce	k1gMnPc1	vládce
také	také	k9	také
upevnili	upevnit	k5eAaPmAgMnP	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
postupným	postupný	k2eAgNnSc7d1	postupné
dobýváním	dobývání	k1gNnSc7	dobývání
zbylých	zbylý	k2eAgInPc2d1	zbylý
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
nezávislých	závislý	k2eNgInPc2d1	nezávislý
tureckých	turecký	k2eAgInPc2d1	turecký
státečků	státeček	k1gInPc2	státeček
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
dobytím	dobytí	k1gNnSc7	dobytí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
posledních	poslední	k2eAgMnPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
–	–	k?	–
Trapezuntu	Trapezunt	k1gInSc2	Trapezunt
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
Selim	Selim	k?	Selim
I.	I.	kA	I.
Hrozný	hrozný	k2eAgInSc1d1	hrozný
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgInS	vládnout
1512	[number]	k4	1512
až	až	k9	až
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
dramaticky	dramaticky	k6eAd1	dramaticky
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
východní	východní	k2eAgFnSc4d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
říše	říš	k1gFnSc2	říš
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
porazil	porazit	k5eAaPmAgInS	porazit
šáha	šáh	k1gMnSc4	šáh
Ismaila	Ismail	k1gMnSc4	Ismail
safíovské	safíovský	k2eAgFnSc2d1	safíovský
Persie	Persie	k1gFnSc2	Persie
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Čaldiránské	Čaldiránský	k2eAgFnSc6d1	Čaldiránský
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhoršení	zhoršení	k1gNnSc6	zhoršení
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Mamlúckým	mamlúcký	k2eAgInSc7d1	mamlúcký
sulatanátem	sulatanát	k1gInSc7	sulatanát
dobyl	dobýt	k5eAaPmAgMnS	dobýt
sultán	sultán	k1gMnSc1	sultán
Selim	Selim	k?	Selim
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
,	,	kIx,	,
Palestinu	Palestina	k1gFnSc4	Palestina
a	a	k8xC	a
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
se	s	k7c7	s
Chalífou	chalífa	k1gMnSc7	chalífa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vůdcem	vůdce	k1gMnSc7	vůdce
celého	celý	k2eAgInSc2d1	celý
muslimského	muslimský	k2eAgInSc2d1	muslimský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
porazila	porazit	k5eAaPmAgFnS	porazit
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
vojska	vojsko	k1gNnSc2	vojsko
perskou	perský	k2eAgFnSc4d1	perská
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
sultán	sultán	k1gMnSc1	sultán
pojistil	pojistit	k5eAaPmAgMnS	pojistit
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
jak	jak	k8xS	jak
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1519	[number]	k4	1519
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
alžírský	alžírský	k2eAgMnSc1d1	alžírský
vládce	vládce	k1gMnSc1	vládce
Chajruddín	Chajruddín	k1gMnSc1	Chajruddín
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
podřídil	podřídit	k5eAaPmAgMnS	podřídit
vládě	vláda	k1gFnSc3	vláda
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velitelem	velitel	k1gMnSc7	velitel
osmanského	osmanský	k2eAgNnSc2d1	osmanské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
smrti	smrt	k1gFnSc2	smrt
Selima	Selimum	k1gNnSc2	Selimum
I.	I.	kA	I.
tedy	tedy	k8xC	tedy
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
ovládala	ovládat	k5eAaImAgFnS	ovládat
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Malou	malý	k2eAgFnSc4d1	malá
Asii	Asie	k1gFnSc4	Asie
<g/>
,	,	kIx,	,
Levantu	Levanta	k1gFnSc4	Levanta
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc4	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Arábie	Arábie	k1gFnSc1	Arábie
a	a	k8xC	a
Alžír	Alžír	k1gInSc1	Alžír
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
přední	přední	k2eAgFnSc7d1	přední
evropskou	evropský	k2eAgFnSc7d1	Evropská
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dále	daleko	k6eAd2	daleko
po	po	k7c4	po
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
i	i	k9	i
politický	politický	k2eAgInSc4d1	politický
vývoj	vývoj	k1gInSc4	vývoj
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Selima	Selimum	k1gNnSc2	Selimum
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Sulejman	Sulejman	k1gMnSc1	Sulejman
I.	I.	kA	I.
zvaný	zvaný	k2eAgInSc1d1	zvaný
Nádherný	nádherný	k2eAgInSc1d1	nádherný
nebo	nebo	k8xC	nebo
též	též	k9	též
Zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
<g/>
.	.	kIx.	.
</s>
<s>
Sulejman	Sulejman	k1gMnSc1	Sulejman
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
osmanských	osmanský	k2eAgMnPc2d1	osmanský
sultánů	sultán	k1gMnPc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
expanzivní	expanzivní	k2eAgFnSc4d1	expanzivní
politiku	politika	k1gFnSc4	politika
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
začlenil	začlenit	k5eAaPmAgMnS	začlenit
do	do	k7c2	do
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dobyl	dobýt	k5eAaPmAgInS	dobýt
Bagdád	Bagdád	k1gInSc4	Bagdád
<g/>
,	,	kIx,	,
Medinu	Medina	k1gFnSc4	Medina
a	a	k8xC	a
Mekku	Mekka	k1gFnSc4	Mekka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
vojska	vojsko	k1gNnSc2	vojsko
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
utonul	utonout	k5eAaPmAgMnS	utonout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
nástupnická	nástupnický	k2eAgFnSc1d1	nástupnická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
stanul	stanout	k5eAaPmAgMnS	stanout
uherský	uherský	k2eAgMnSc1d1	uherský
šlechtic	šlechtic	k1gMnSc1	šlechtic
Jan	Jan	k1gMnSc1	Jan
Zápolský	Zápolský	k2eAgInSc4d1	Zápolský
proti	proti	k7c3	proti
rodu	rod	k1gInSc3	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Osmanů	Osman	k1gMnPc2	Osman
pak	pak	k6eAd1	pak
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Uher	Uhry	k1gFnPc2	Uhry
(	(	kIx(	(
<g/>
osmanská	osmanský	k2eAgNnPc4d1	osmanské
vojska	vojsko	k1gNnPc4	vojsko
pronikla	proniknout	k5eAaPmAgFnS	proniknout
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
až	až	k6eAd1	až
k	k	k7c3	k
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
nepříjemných	příjemný	k2eNgFnPc2d1	nepříjemná
podnebních	podnební	k2eAgFnPc2d1	podnební
podmínek	podmínka	k1gFnPc2	podmínka
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Zápolského	Zápolský	k2eAgMnSc2d1	Zápolský
ale	ale	k8xC	ale
propukly	propuknout	k5eAaPmAgInP	propuknout
boje	boj	k1gInPc1	boj
nanovo	nanovo	k6eAd1	nanovo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburská	habsburský	k2eAgNnPc4d1	habsburské
vojska	vojsko	k1gNnPc4	vojsko
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
,	,	kIx,	,
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
tzv.	tzv.	kA	tzv.
Budínský	budínský	k2eAgInSc4d1	budínský
pašalík	pašalík	k1gInSc4	pašalík
a	a	k8xC	a
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnSc2	vláda
Zápolského	Zápolský	k2eAgMnSc2d1	Zápolský
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Zikmund	Zikmund	k1gMnSc1	Zikmund
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
stal	stát	k5eAaPmAgInS	stát
osmanským	osmanský	k2eAgMnSc7d1	osmanský
vazalem	vazal	k1gMnSc7	vazal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
pak	pak	k6eAd1	pak
osmanská	osmanský	k2eAgNnPc4d1	osmanské
vojska	vojsko	k1gNnPc4	vojsko
dlouho	dlouho	k6eAd1	dlouho
vázala	vázat	k5eAaImAgFnS	vázat
vojenský	vojenský	k2eAgInSc4d1	vojenský
potenciál	potenciál	k1gInSc4	potenciál
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
oslabovala	oslabovat	k5eAaImAgFnS	oslabovat
jejich	jejich	k3xOp3gFnSc4	jejich
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
námořním	námořní	k2eAgFnPc3d1	námořní
základnám	základna	k1gFnPc3	základna
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
i	i	k8xC	i
množství	množství	k1gNnSc6	množství
dalších	další	k2eAgInPc2d1	další
přístavů	přístav	k1gInPc2	přístav
v	v	k7c6	v
Levantě	Levanta	k1gFnSc6	Levanta
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
osmanské	osmanský	k2eAgNnSc1d1	osmanské
loďstvo	loďstvo	k1gNnSc1	loďstvo
stalo	stát	k5eAaPmAgNnS	stát
dominantní	dominantní	k2eAgFnSc7d1	dominantní
silou	síla	k1gFnSc7	síla
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dominancí	dominance	k1gFnSc7	dominance
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
otřásly	otřást	k5eAaPmAgFnP	otřást
už	už	k6eAd1	už
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dvě	dva	k4xCgFnPc4	dva
těžké	těžký	k2eAgInPc1d1	těžký
<g />
.	.	kIx.	.
</s>
<s>
porážky	porážka	k1gFnSc2	porážka
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
neúspěch	neúspěch	k1gInSc4	neúspěch
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Malty	Malta	k1gFnSc2	Malta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jí	on	k3xPp3gFnSc3	on
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
průniku	průnik	k1gInSc3	průnik
do	do	k7c2	do
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
drtivá	drtivý	k2eAgFnSc1d1	drtivá
porážka	porážka	k1gFnSc1	porážka
u	u	k7c2	u
Lepanta	Lepant	k1gMnSc2	Lepant
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukázala	ukázat	k5eAaPmAgFnS	ukázat
její	její	k3xOp3gFnSc4	její
technickou	technický	k2eAgFnSc4d1	technická
zaostalost	zaostalost	k1gFnSc4	zaostalost
v	v	k7c6	v
palných	palný	k2eAgFnPc6d1	palná
zbraních	zbraň	k1gFnPc6	zbraň
a	a	k8xC	a
natrvalo	natrvalo	k6eAd1	natrvalo
podlomila	podlomit	k5eAaPmAgFnS	podlomit
její	její	k3xOp3gFnSc4	její
námořní	námořní	k2eAgFnSc4d1	námořní
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
co	co	k3yQnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
dokázala	dokázat	k5eAaPmAgNnP	dokázat
své	svůj	k3xOyFgNnSc4	svůj
loďstvo	loďstvo	k1gNnSc4	loďstvo
znovu	znovu	k6eAd1	znovu
vybudovat	vybudovat	k5eAaPmF	vybudovat
<g/>
.	.	kIx.	.
</s>
<s>
Osmanské	osmanský	k2eAgFnPc1d1	Osmanská
námořní	námořní	k2eAgFnPc1d1	námořní
síly	síla	k1gFnPc1	síla
také	také	k9	také
dlouho	dlouho	k6eAd1	dlouho
držely	držet	k5eAaImAgFnP	držet
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
prorazit	prorazit	k5eAaPmF	prorazit
portugalskou	portugalský	k2eAgFnSc4d1	portugalská
blokádu	blokáda	k1gFnSc4	blokáda
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
kořením	koření	k1gNnSc7	koření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Sulejmana	Sulejman	k1gMnSc2	Sulejman
se	se	k3xPyFc4	se
též	též	k9	též
značně	značně	k6eAd1	značně
pozvedla	pozvednout	k5eAaPmAgFnS	pozvednout
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
zejména	zejména	k9	zejména
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
množství	množství	k1gNnSc4	množství
mešit	mešita	k1gFnPc2	mešita
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nákladných	nákladný	k2eAgFnPc2d1	nákladná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
tolerantnější	tolerantní	k2eAgNnSc1d2	tolerantnější
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
náboženství	náboženství	k1gNnSc2	náboženství
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
sem	sem	k6eAd1	sem
po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
přicestovalo	přicestovat	k5eAaPmAgNnS	přicestovat
mnoho	mnoho	k4c1	mnoho
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
angažovali	angažovat	k5eAaBmAgMnP	angažovat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
nebo	nebo	k8xC	nebo
pracovali	pracovat	k5eAaImAgMnP	pracovat
jako	jako	k9	jako
státní	státní	k2eAgMnPc1d1	státní
úředníci	úředník	k1gMnPc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
ostatních	ostatní	k2eAgNnPc2d1	ostatní
náboženství	náboženství	k1gNnPc2	náboženství
mohli	moct	k5eAaImAgMnP	moct
svobodně	svobodně	k6eAd1	svobodně
pod	pod	k7c7	pod
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
vládou	vláda	k1gFnSc7	vláda
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
však	však	k9	však
platit	platit	k5eAaImF	platit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
opíral	opírat	k5eAaImAgMnS	opírat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
o	o	k7c4	o
jednotky	jednotka	k1gFnPc4	jednotka
janičářů	janičář	k1gMnPc2	janičář
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
byli	být	k5eAaImAgMnP	být
odvedeni	odveden	k2eAgMnPc1d1	odveden
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
devširme	devširm	k1gInSc5	devširm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
donuceni	donutit	k5eAaPmNgMnP	donutit
konvertovat	konvertovat	k5eAaBmF	konvertovat
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
a	a	k8xC	a
vychováni	vychovat	k5eAaPmNgMnP	vychovat
k	k	k7c3	k
absolutní	absolutní	k2eAgFnSc3d1	absolutní
poslušnosti	poslušnost	k1gFnSc3	poslušnost
a	a	k8xC	a
disciplíně	disciplína	k1gFnSc3	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
této	tento	k3xDgFnSc2	tento
osobní	osobní	k2eAgFnSc2d1	osobní
gardy	garda	k1gFnSc2	garda
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
sipahíjové	sipahíjový	k2eAgFnSc6d1	sipahíjový
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
byla	být	k5eAaImAgFnS	být
udělována	udělován	k2eAgFnSc1d1	udělována
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
půda	půda	k1gFnSc1	půda
a	a	k8xC	a
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
krom	krom	k7c2	krom
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
starali	starat	k5eAaImAgMnP	starat
také	také	k9	také
o	o	k7c4	o
chod	chod	k1gInSc4	chod
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
výběr	výběr	k1gInSc1	výběr
daní	daň	k1gFnPc2	daň
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
přiděleném	přidělený	k2eAgNnSc6d1	přidělené
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
pilířem	pilíř	k1gInSc7	pilíř
sultánovy	sultánův	k2eAgFnPc4d1	sultánova
moci	moc	k1gFnPc4	moc
rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
(	(	kIx(	(
<g/>
divan	divan	k1gInSc1	divan
<g/>
)	)	kIx)	)
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
autoritu	autorita	k1gFnSc4	autorita
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
radu	rada	k1gFnSc4	rada
muslimských	muslimský	k2eAgMnPc2d1	muslimský
teologů	teolog	k1gMnPc2	teolog
(	(	kIx(	(
<g/>
ulema	ulema	k1gMnSc1	ulema
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
snažila	snažit	k5eAaImAgNnP	snažit
strhávat	strhávat	k5eAaImF	strhávat
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
velký	velký	k2eAgMnSc1d1	velký
muftí	muftí	k1gMnSc1	muftí
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
dokonce	dokonce	k9	dokonce
o	o	k7c6	o
způsobilosti	způsobilost	k1gFnSc6	způsobilost
sultána	sultán	k1gMnSc2	sultán
vládnout	vládnout	k5eAaImF	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
vlády	vláda	k1gFnSc2	vláda
Sulejmana	Sulejman	k1gMnSc2	Sulejman
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
příznaky	příznak	k1gInPc4	příznak
stagnace	stagnace	k1gFnSc2	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
jakkoli	jakkoli	k8xS	jakkoli
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Sulejmana	Sulejman	k1gMnSc2	Sulejman
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
pozvolna	pozvolna	k6eAd1	pozvolna
upadat	upadat	k5eAaPmF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sulejmanovi	Sulejman	k1gMnSc6	Sulejman
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
méně	málo	k6eAd2	málo
schopní	schopný	k2eAgMnPc1d1	schopný
sultáni	sultán	k1gMnPc1	sultán
(	(	kIx(	(
<g/>
Selim	Selim	k?	Selim
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Mehmed	Mehmed	k1gMnSc1	Mehmed
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zasahovali	zasahovat	k5eAaImAgMnP	zasahovat
jejich	jejich	k3xOp3gFnPc4	jejich
manželky	manželka	k1gFnPc4	manželka
a	a	k8xC	a
poradci	poradce	k1gMnPc1	poradce
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
vezír	vezír	k1gMnSc1	vezír
Mehmed	Mehmed	k1gMnSc1	Mehmed
Paša	paša	k1gMnSc1	paša
Sokolović	Sokolović	k1gMnSc1	Sokolović
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
postupně	postupně	k6eAd1	postupně
přebírali	přebírat	k5eAaImAgMnP	přebírat
moc	moc	k6eAd1	moc
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
spravované	spravovaný	k2eAgFnPc1d1	spravovaná
sipahíji	sipahí	k6eAd2	sipahí
nevynášely	vynášet	k5eNaImAgFnP	vynášet
tolik	tolik	k6eAd1	tolik
jako	jako	k8xS	jako
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
častému	častý	k2eAgNnSc3d1	časté
střídání	střídání	k1gNnSc3	střídání
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gMnPc1	jejich
správci	správce	k1gMnPc1	správce
nevytvořili	vytvořit	k5eNaPmAgMnP	vytvořit
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
působnosti	působnost	k1gFnSc2	působnost
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
tedy	tedy	k8xC	tedy
zájem	zájem	k1gInSc4	zájem
starat	starat	k5eAaImF	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mnohdy	mnohdy	k6eAd1	mnohdy
tito	tento	k3xDgMnPc1	tento
správcové	správce	k1gMnPc1	správce
využívali	využívat	k5eAaPmAgMnP	využívat
ovládané	ovládaný	k2eAgFnPc4d1	ovládaná
provincie	provincie	k1gFnPc4	provincie
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
osobnímu	osobní	k2eAgNnSc3d1	osobní
obohacení	obohacení	k1gNnSc3	obohacení
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
hlavních	hlavní	k2eAgMnPc2d1	hlavní
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
zámořským	zámořský	k2eAgInPc3d1	zámořský
objevům	objev	k1gInPc3	objev
<g/>
,	,	kIx,	,
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
však	však	k9	však
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
neměla	mít	k5eNaImAgFnS	mít
prakticky	prakticky	k6eAd1	prakticky
žádná	žádný	k3yNgNnPc4	žádný
pozitiva	pozitivum	k1gNnPc4	pozitivum
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pokračujícímu	pokračující	k2eAgNnSc3d1	pokračující
portugalskému	portugalský	k2eAgNnSc3d1	portugalské
embargu	embargo	k1gNnSc3	embargo
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
kořením	koření	k1gNnSc7	koření
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
výnosném	výnosný	k2eAgInSc6d1	výnosný
zdroji	zdroj	k1gInSc6	zdroj
příjmů	příjem	k1gInPc2	příjem
a	a	k8xC	a
situaci	situace	k1gFnSc4	situace
jen	jen	k9	jen
zhoršoval	zhoršovat	k5eAaImAgInS	zhoršovat
příliv	příliv	k1gInSc1	příliv
levného	levný	k2eAgNnSc2d1	levné
stříbra	stříbro	k1gNnSc2	stříbro
z	z	k7c2	z
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
inflaci	inflace	k1gFnSc3	inflace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgInPc4d1	další
zdroje	zdroj	k1gInPc4	zdroj
příjmů	příjem	k1gInPc2	příjem
přicházela	přicházet	k5eAaImAgFnS	přicházet
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
díky	díky	k7c3	díky
systému	systém	k1gInSc3	systém
kapitulací	kapitulace	k1gFnPc2	kapitulace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přenechání	přenechání	k1gNnSc1	přenechání
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
druhem	druh	k1gInSc7	druh
zboží	zboží	k1gNnSc2	zboží
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
obchodníkům	obchodník	k1gMnPc3	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Taktika	taktika	k1gFnSc1	taktika
osmanského	osmanský	k2eAgNnSc2d1	osmanské
vojska	vojsko	k1gNnSc2	vojsko
začala	začít	k5eAaPmAgFnS	začít
zastarávat	zastarávat	k5eAaImF	zastarávat
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
janičáři	janičár	k1gMnPc1	janičár
posilovali	posilovat	k5eAaImAgMnP	posilovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
začali	začít	k5eAaPmAgMnP	začít
obsazovat	obsazovat	k5eAaImF	obsazovat
vysoká	vysoký	k2eAgNnPc1d1	vysoké
úřednická	úřednický	k2eAgNnPc1d1	úřednické
místa	místo	k1gNnPc1	místo
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
byrokracii	byrokracie	k1gFnSc6	byrokracie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
z	z	k7c2	z
původně	původně	k6eAd1	původně
elitních	elitní	k2eAgFnPc2d1	elitní
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
stávala	stávat	k5eAaImAgFnS	stávat
společenská	společenský	k2eAgFnSc1d1	společenská
vrstva	vrstva	k1gFnSc1	vrstva
neváhající	váhající	k2eNgNnSc1d1	neváhající
prosadit	prosadit	k5eAaPmF	prosadit
svoje	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
kohokoli	kdokoli	k3yInSc4	kdokoli
včetně	včetně	k7c2	včetně
sultána	sultán	k1gMnSc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Pokusné	pokusný	k2eAgFnPc4d1	pokusná
reformy	reforma	k1gFnPc4	reforma
některých	některý	k3yIgMnPc2	některý
sultánů	sultán	k1gMnPc2	sultán
janičáři	janičár	k1gMnPc1	janičár
většinou	většinou	k6eAd1	většinou
odmítali	odmítat	k5eAaImAgMnP	odmítat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nepřišli	přijít	k5eNaPmAgMnP	přijít
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
pravomoci	pravomoc	k1gFnSc6	pravomoc
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
neváhali	váhat	k5eNaImAgMnP	váhat
někdy	někdy	k6eAd1	někdy
zavraždit	zavraždit	k5eAaPmF	zavraždit
i	i	k9	i
samotné	samotný	k2eAgFnPc4d1	samotná
sultány	sultána	k1gFnPc4	sultána
a	a	k8xC	a
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
dosadili	dosadit	k5eAaPmAgMnP	dosadit
slabé	slabý	k2eAgMnPc4d1	slabý
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
mohli	moct	k5eAaImAgMnP	moct
lépe	dobře	k6eAd2	dobře
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ale	ale	k8xC	ale
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
musela	muset	k5eAaImAgFnS	muset
čelit	čelit	k5eAaImF	čelit
třem	tři	k4xCgFnPc3	tři
velkým	velký	k2eAgFnPc3d1	velká
říším	říš	k1gFnPc3	říš
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
hranicích	hranice	k1gFnPc6	hranice
-	-	kIx~	-
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
Habsburské	habsburský	k2eAgFnSc3d1	habsburská
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Sáfiovská	Sáfiovská	k1gFnSc1	Sáfiovská
Persie	Persie	k1gFnSc1	Persie
a	a	k8xC	a
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
postupně	postupně	k6eAd1	postupně
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
Mehmed	Mehmed	k1gMnSc1	Mehmed
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
po	po	k7c6	po
několika	několik	k4yIc6	několik
neschopných	schopný	k2eNgMnPc6d1	neschopný
vládcích	vládce	k1gMnPc6	vládce
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
zastavit	zastavit	k5eAaPmF	zastavit
postupný	postupný	k2eAgInSc4d1	postupný
propad	propad	k1gInSc4	propad
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
sultánův	sultánův	k2eAgMnSc1d1	sultánův
velkovezír	velkovezír	k1gMnSc1	velkovezír
Kara	kara	k1gFnSc1	kara
Mustafa	Mustaf	k1gMnSc2	Mustaf
vydal	vydat	k5eAaPmAgInS	vydat
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
zachvátil	zachvátit	k5eAaPmAgInS	zachvátit
Evropu	Evropa	k1gFnSc4	Evropa
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jen	jen	k9	jen
umocnil	umocnit	k5eAaPmAgInS	umocnit
útěk	útěk	k1gInSc1	útěk
Leopolda	Leopolda	k1gFnSc1	Leopolda
I.	I.	kA	I.
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Mustafovi	Mustaf	k1gMnSc3	Mustaf
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
taktika	taktika	k1gFnSc1	taktika
nevyplatila	vyplatit	k5eNaPmAgFnS	vyplatit
–	–	k?	–
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
Vídeň	Vídeň	k1gFnSc4	Vídeň
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
město	město	k1gNnSc1	město
vyhladovět	vyhladovět	k5eAaPmF	vyhladovět
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
však	však	k9	však
jen	jen	k6eAd1	jen
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
císaři	císař	k1gMnSc3	císař
čas	čas	k1gInSc4	čas
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
a	a	k8xC	a
především	především	k6eAd1	především
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sobieski	Sobieski	k1gNnSc1	Sobieski
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
vojska	vojsko	k1gNnSc2	vojsko
Turky	Turek	k1gMnPc4	Turek
rozprášila	rozprášit	k5eAaPmAgFnS	rozprášit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
byla	být	k5eAaImAgFnS	být
dobyta	dobýt	k5eAaPmNgFnS	dobýt
také	také	k9	také
islámská	islámský	k2eAgFnSc1d1	islámská
Ostřihom	Ostřihom	k1gInSc1	Ostřihom
a	a	k8xC	a
Turci	Turek	k1gMnPc1	Turek
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
začít	začít	k5eAaPmF	začít
se	se	k3xPyFc4	se
stahovat	stahovat	k5eAaImF	stahovat
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
sultán	sultán	k1gMnSc1	sultán
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
neúspěších	neúspěch	k1gInPc6	neúspěch
Kary	kara	k1gFnSc2	kara
Mustafy	Mustaf	k1gInPc4	Mustaf
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
uškrtit	uškrtit	k5eAaPmF	uškrtit
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgInPc1d1	evropský
státy	stát	k1gInPc1	stát
hraničící	hraničící	k2eAgInPc1d1	hraničící
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
snažily	snažit	k5eAaImAgFnP	snažit
nabyté	nabytý	k2eAgFnPc4d1	nabytá
převahy	převaha	k1gFnPc4	převaha
využít	využít	k5eAaPmF	využít
<g/>
,	,	kIx,	,
a	a	k8xC	a
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
XI	XI	kA	XI
<g/>
.	.	kIx.	.
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
roku	rok	k1gInSc2	rok
1684	[number]	k4	1684
Svatou	svatý	k2eAgFnSc4d1	svatá
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahájila	zahájit	k5eAaPmAgFnS	zahájit
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
–	–	k?	–
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
bylo	být	k5eAaImAgNnS	být
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
turecké	turecký	k2eAgNnSc1d1	turecké
vojsko	vojsko	k1gNnSc1	vojsko
vytlačeno	vytlačen	k2eAgNnSc1d1	vytlačeno
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
navíc	navíc	k6eAd1	navíc
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
Dalmácii	Dalmácie	k1gFnSc4	Dalmácie
a	a	k8xC	a
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Peloponéský	peloponéský	k2eAgInSc4d1	peloponéský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc3	zem
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
středu	střed	k1gInSc2	střed
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
valili	valit	k5eAaImAgMnP	valit
uprchlíci	uprchlík	k1gMnPc1	uprchlík
<g/>
,	,	kIx,	,
půda	půda	k1gFnSc1	půda
zůstala	zůstat	k5eAaPmAgFnS	zůstat
neobdělána	obdělat	k5eNaPmNgFnS	obdělat
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
situaci	situace	k1gFnSc4	situace
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
prudká	prudký	k2eAgFnSc1d1	prudká
inflace	inflace	k1gFnSc1	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Mehmed	Mehmed	k1gMnSc1	Mehmed
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nastalou	nastalý	k2eAgFnSc4d1	nastalá
situaci	situace	k1gFnSc4	situace
neunesl	unést	k5eNaPmAgMnS	unést
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
rezignovat	rezignovat	k5eAaBmF	rezignovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Sulejmana	Sulejman	k1gMnSc2	Sulejman
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
život	život	k1gInSc4	život
držen	držen	k2eAgInSc4d1	držen
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kafe	kafe	k?	kafe
<g/>
;	;	kIx,	;
praxe	praxe	k1gFnSc1	praxe
povraždit	povraždit	k5eAaPmF	povraždit
sultánovy	sultánův	k2eAgMnPc4d1	sultánův
bratry	bratr	k1gMnPc4	bratr
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	on	k3xPp3gInSc2	on
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
náhodou	náhodou	k6eAd1	náhodou
nepokusili	pokusit	k5eNaPmAgMnP	pokusit
sultanát	sultanát	k1gInSc4	sultanát
uchvátit	uchvátit	k5eAaPmF	uchvátit
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Sulejman	Sulejman	k1gMnSc1	Sulejman
II	II	kA	II
<g/>
.	.	kIx.	.
již	již	k6eAd1	již
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
krátké	krátký	k2eAgFnSc2d1	krátká
vlády	vláda	k1gFnSc2	vláda
zemi	zem	k1gFnSc4	zem
poměrně	poměrně	k6eAd1	poměrně
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
boje	boj	k1gInPc1	boj
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
dalších	další	k2eAgNnPc2d1	další
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
mohutnému	mohutný	k2eAgNnSc3d1	mohutné
posunutí	posunutí	k1gNnSc3	posunutí
hranice	hranice	k1gFnSc2	hranice
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
liga	liga	k1gFnSc1	liga
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
a	a	k8xC	a
situaci	situace	k1gFnSc4	situace
již	již	k6eAd1	již
nezvrátilo	zvrátit	k5eNaPmAgNnS	zvrátit
ani	ani	k8xC	ani
několik	několik	k4yIc1	několik
osmanských	osmanský	k2eAgFnPc2d1	Osmanská
ofenzív	ofenzíva	k1gFnPc2	ofenzíva
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vítězství	vítězství	k1gNnSc1	vítězství
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgMnSc2d1	savojský
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
znepřátelené	znepřátelený	k2eAgFnPc1d1	znepřátelená
strany	strana	k1gFnPc1	strana
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1699	[number]	k4	1699
tzv.	tzv.	kA	tzv.
Karlovický	karlovický	k2eAgInSc4d1	karlovický
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgNnSc2	který
Uhry	Uhry	k1gFnPc4	Uhry
a	a	k8xC	a
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
habsburských	habsburský	k2eAgFnPc6d1	habsburská
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
Benátkám	Benátky	k1gFnPc3	Benátky
připadla	připadnout	k5eAaPmAgFnS	připadnout
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
ustála	ustát	k5eAaPmAgFnS	ustát
i	i	k9	i
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
nepokoje	nepokoj	k1gInPc4	nepokoj
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
vzpoura	vzpoura	k1gFnSc1	vzpoura
janičářů	janičář	k1gMnPc2	janičář
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
krize	krize	k1gFnSc1	krize
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
překonána	překonat	k5eAaPmNgFnS	překonat
reformou	reforma	k1gFnSc7	reforma
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
námořní	námořní	k2eAgFnSc1d1	námořní
flotila	flotila	k1gFnSc1	flotila
sestavená	sestavený	k2eAgFnSc1d1	sestavená
z	z	k7c2	z
moderních	moderní	k2eAgFnPc2d1	moderní
plachetnic	plachetnice	k1gFnPc2	plachetnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
měly	mít	k5eAaImAgFnP	mít
následovat	následovat	k5eAaImF	následovat
ještě	ještě	k9	ještě
hlubší	hluboký	k2eAgFnPc1d2	hlubší
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
sultána	sultán	k1gMnSc2	sultán
Ahmeda	Ahmed	k1gMnSc2	Ahmed
III	III	kA	III
<g/>
.	.	kIx.	.
především	především	k6eAd1	především
vyslala	vyslat	k5eAaPmAgFnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
první	první	k4xOgMnPc4	první
vyslance	vyslanec	k1gMnPc4	vyslanec
do	do	k7c2	do
několika	několik	k4yIc2	několik
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přestaly	přestat	k5eAaPmAgInP	přestat
v	v	k7c6	v
Turcích	turek	k1gInPc6	turek
vidět	vidět	k5eAaImF	vidět
nositele	nositel	k1gMnSc4	nositel
"	"	kIx"	"
<g/>
Božího	boží	k2eAgInSc2d1	boží
hněvu	hněv	k1gInSc2	hněv
<g/>
"	"	kIx"	"
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
brát	brát	k5eAaImF	brát
jako	jako	k8xS	jako
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
jen	jen	k9	jen
u	u	k7c2	u
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
–	–	k?	–
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
začali	začít	k5eAaPmAgMnP	začít
přicházet	přicházet	k5eAaImF	přicházet
evropští	evropský	k2eAgMnPc1d1	evropský
architekti	architekt	k1gMnPc1	architekt
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
muslimských	muslimský	k2eAgFnPc2d1	muslimská
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
tulipánů	tulipán	k1gInPc2	tulipán
(	(	kIx(	(
<g/>
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
exportovány	exportovat	k5eAaBmNgFnP	exportovat
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
natolik	natolik	k6eAd1	natolik
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
módou	móda	k1gFnSc7	móda
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
reforem	reforma	k1gFnPc2	reforma
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaPmNgNnS	nazvat
Obdobím	období	k1gNnSc7	období
tulipánů	tulipán	k1gInPc2	tulipán
(	(	kIx(	(
<g/>
1718	[number]	k4	1718
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
hospodářsky	hospodářsky	k6eAd1	hospodářsky
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
poměrně	poměrně	k6eAd1	poměrně
radikální	radikální	k2eAgFnPc1d1	radikální
změny	změna	k1gFnPc1	změna
však	však	k9	však
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
povstání	povstání	k1gNnSc4	povstání
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
janičářů	janičář	k1gMnPc2	janičář
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
znovu	znovu	k6eAd1	znovu
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
sultána	sultán	k1gMnSc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Mahmud	Mahmud	k1gMnSc1	Mahmud
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
1754	[number]	k4	1754
<g/>
)	)	kIx)	)
však	však	k9	však
nechal	nechat	k5eAaPmAgMnS	nechat
vůdce	vůdce	k1gMnSc4	vůdce
povstání	povstání	k1gNnSc2	povstání
popravit	popravit	k5eAaPmF	popravit
a	a	k8xC	a
v	v	k7c6	v
reformách	reforma	k1gFnPc6	reforma
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
armády	armáda	k1gFnSc2	armáda
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojenských	vojenský	k2eAgMnPc2d1	vojenský
odborníků	odborník	k1gMnPc2	odborník
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
úspěchům	úspěch	k1gInPc3	úspěch
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Persií	Persie	k1gFnSc7	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
problémy	problém	k1gInPc1	problém
však	však	k9	však
byly	být	k5eAaImAgInP	být
vyřešeny	vyřešen	k2eAgInPc1d1	vyřešen
(	(	kIx(	(
<g/>
separatistické	separatistický	k2eAgFnPc1d1	separatistická
tendence	tendence	k1gFnPc1	tendence
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
hladomor	hladomor	k1gInSc1	hladomor
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc4	odpor
janičářů	janičář	k1gInPc2	janičář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
týkaly	týkat	k5eAaImAgInP	týkat
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
rozvoje	rozvoj	k1gInSc2	rozvoj
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
atd.	atd.	kA	atd.
Mustafa	Mustaf	k1gMnSc2	Mustaf
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1757	[number]	k4	1757
<g/>
–	–	k?	–
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
navíc	navíc	k6eAd1	navíc
fatálně	fatálně	k6eAd1	fatálně
přecenil	přecenit	k5eAaPmAgMnS	přecenit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
válku	válka	k1gFnSc4	válka
ruské	ruský	k2eAgFnSc3d1	ruská
carevně	carevna	k1gFnSc3	carevna
Kateřině	Kateřina	k1gFnSc3	Kateřina
Veliké	veliký	k2eAgFnSc2d1	veliká
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
ruské	ruský	k2eAgNnSc4d1	ruské
vojsko	vojsko	k1gNnSc4	vojsko
Turky	turek	k1gInPc4	turek
snadno	snadno	k6eAd1	snadno
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
mírem	mír	k1gInSc7	mír
z	z	k7c2	z
Küčük-Kaynarca	Küčük-Kaynarcum	k1gNnSc2	Küčük-Kaynarcum
(	(	kIx(	(
<g/>
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
Küçük-Kainardži	Küçük-Kainardž	k1gFnSc6	Küçük-Kainardž
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
asi	asi	k9	asi
Küçük-Kajnardža	Küçük-Kajnardža	k1gFnSc1	Küçük-Kajnardža
(	(	kIx(	(
<g/>
=	=	kIx~	=
Malá	malý	k2eAgFnSc1d1	malá
Kajnardža	Kajnardža	k1gFnSc1	Kajnardža
<g/>
))	))	k?	))
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
carevna	carevna	k1gFnSc1	carevna
jisté	jistý	k2eAgFnSc2d1	jistá
pravomoci	pravomoc	k1gFnSc2	pravomoc
ve	v	k7c6	v
Valašsku	Valašsko	k1gNnSc6	Valašsko
a	a	k8xC	a
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
Bospor	Bospor	k1gInSc4	Bospor
a	a	k8xC	a
Dardanely	Dardanely	k1gFnPc4	Dardanely
musely	muset	k5eAaImAgFnP	muset
poskytnout	poskytnout	k5eAaPmF	poskytnout
obchodním	obchodní	k2eAgFnPc3d1	obchodní
lodím	loď	k1gFnPc3	loď
volný	volný	k2eAgInSc4d1	volný
průjezd	průjezd	k1gInSc4	průjezd
a	a	k8xC	a
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
porta	porta	k1gFnSc1	porta
(	(	kIx(	(
<g/>
administrativní	administrativní	k2eAgInSc1d1	administrativní
aparát	aparát	k1gInSc1	aparát
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
porážce	porážka	k1gFnSc6	porážka
se	se	k3xPyFc4	se
sultán	sultán	k1gMnSc1	sultán
Abdulhamid	Abdulhamida	k1gFnPc2	Abdulhamida
I.	I.	kA	I.
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zejména	zejména	k9	zejména
v	v	k7c6	v
reformách	reforma	k1gFnPc6	reforma
vojska	vojsko	k1gNnSc2	vojsko
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
reformy	reforma	k1gFnPc1	reforma
se	se	k3xPyFc4	se
však	však	k9	však
týkaly	týkat	k5eAaImAgFnP	týkat
jen	jen	k9	jen
centra	centrum	k1gNnSc2	centrum
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
v	v	k7c6	v
odlehlejších	odlehlý	k2eAgFnPc6d2	odlehlejší
oblastech	oblast	k1gFnPc6	oblast
naopak	naopak	k6eAd1	naopak
vládla	vládnout	k5eAaImAgFnS	vládnout
anarchie	anarchie	k1gFnSc2	anarchie
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ujímali	ujímat	k5eAaImAgMnP	ujímat
lokální	lokální	k2eAgMnPc1d1	lokální
diktátoři	diktátor	k1gMnPc1	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
začalo	začít	k5eAaPmAgNnS	začít
poprvé	poprvé	k6eAd1	poprvé
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
Gruzii	Gruzie	k1gFnSc4	Gruzie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
(	(	kIx(	(
<g/>
od	od	k7c2	od
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
i	i	k9	i
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Bosnu	Bosna	k1gFnSc4	Bosna
a	a	k8xC	a
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
války	válka	k1gFnPc1	válka
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
a	a	k8xC	a
Palestiny	Palestina	k1gFnSc2	Palestina
však	však	k9	však
jen	jen	k6eAd1	jen
přispěly	přispět	k5eAaPmAgFnP	přispět
ke	k	k7c3	k
stmelení	stmelení	k1gNnSc3	stmelení
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
utvrdily	utvrdit	k5eAaPmAgFnP	utvrdit
sultána	sultána	k1gFnSc1	sultána
v	v	k7c6	v
zavádění	zavádění	k1gNnSc6	zavádění
dalších	další	k2eAgFnPc2d1	další
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Řecká	řecký	k2eAgFnSc1d1	řecká
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
všech	všecek	k3xTgFnPc2	všecek
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
sultán	sultán	k1gMnSc1	sultán
Selim	Selim	k?	Selim
III	III	kA	III
<g/>
.	.	kIx.	.
váhal	váhat	k5eAaImAgInS	váhat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
přiklonit	přiklonit	k5eAaPmF	přiklonit
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
či	či	k8xC	či
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
popudilo	popudit	k5eAaPmAgNnS	popudit
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
násilně	násilně	k6eAd1	násilně
proplout	proplout	k5eAaPmF	proplout
Úžinami	úžina	k1gFnPc7	úžina
a	a	k8xC	a
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
Egypt	Egypt	k1gInSc4	Egypt
od	od	k7c2	od
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
byly	být	k5eAaImAgFnP	být
provázeny	provázet	k5eAaImNgFnP	provázet
separatistickými	separatistický	k2eAgFnPc7d1	separatistická
snahami	snaha	k1gFnPc7	snaha
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
(	(	kIx(	(
<g/>
Ali	Ali	k1gFnSc1	Ali
Paša	paša	k1gMnSc1	paša
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povstáním	povstání	k1gNnSc7	povstání
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
znovu	znovu	k6eAd1	znovu
povstáním	povstání	k1gNnSc7	povstání
protireformních	protireformní	k2eAgInPc2d1	protireformní
janičářů	janičář	k1gInPc2	janičář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Selima	Selim	k1gMnSc4	Selim
svrhli	svrhnout	k5eAaPmAgMnP	svrhnout
a	a	k8xC	a
dosadili	dosadit	k5eAaPmAgMnP	dosadit
Mustafu	Mustaf	k1gInSc3	Mustaf
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
vzápětí	vzápětí	k6eAd1	vzápětí
také	také	k9	také
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
sultánem	sultán	k1gMnSc7	sultán
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Mahmut	Mahmut	k1gMnSc1	Mahmut
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
Mahmut	Mahmut	k1gMnSc1	Mahmut
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
nezbytnosti	nezbytnost	k1gFnSc3	nezbytnost
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sice	sice	k8xC	sice
prováděl	provádět	k5eAaImAgInS	provádět
opatrněji	opatrně	k6eAd2	opatrně
<g/>
,	,	kIx,	,
klid	klid	k1gInSc1	klid
však	však	k9	však
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
nezažívala	zažívat	k5eNaImAgFnS	zažívat
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Turci	Turek	k1gMnPc1	Turek
mírem	mír	k1gInSc7	mír
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
ztratili	ztratit	k5eAaPmAgMnP	ztratit
Besarábii	Besarábie	k1gFnSc3	Besarábie
a	a	k8xC	a
Srbsku	Srbsko	k1gNnSc3	Srbsko
byla	být	k5eAaImAgFnS	být
přislíbena	přislíben	k2eAgFnSc1d1	přislíbena
značná	značný	k2eAgFnSc1d1	značná
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Neklidné	klidný	k2eNgInPc1d1	neklidný
byly	být	k5eAaImAgInP	být
i	i	k9	i
provincie	provincie	k1gFnSc2	provincie
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
potlačit	potlačit	k5eAaPmF	potlačit
srbské	srbský	k2eAgNnSc4d1	srbské
povstání	povstání	k1gNnSc4	povstání
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ali	Ali	k1gMnSc1	Ali
Paša	paša	k1gMnSc1	paša
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
porážce	porážka	k1gFnSc6	porážka
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
podněcoval	podněcovat	k5eAaImAgInS	podněcovat
Řeky	Řek	k1gMnPc4	Řek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
povstali	povstat	k5eAaPmAgMnP	povstat
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
neseno	nést	k5eAaImNgNnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc3	duch
novohelénství	novohelénství	k1gNnSc2	novohelénství
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yIgNnSc3	který
stála	stát	k5eAaImAgFnS	stát
i	i	k9	i
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Mahmut	Mahmut	k1gInSc1	Mahmut
však	však	k9	však
situaci	situace	k1gFnSc4	situace
špatně	špatně	k6eAd1	špatně
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
a	a	k8xC	a
povstání	povstání	k1gNnSc4	povstání
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
pravoslavné	pravoslavný	k2eAgNnSc4d1	pravoslavné
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
proto	proto	k8xC	proto
popravit	popravit	k5eAaPmF	popravit
konstantinopolského	konstantinopolský	k2eAgMnSc4d1	konstantinopolský
patriarchu	patriarcha	k1gMnSc4	patriarcha
a	a	k8xC	a
několik	několik	k4yIc1	několik
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jen	jen	k9	jen
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
věřící	věřící	k1gMnPc1	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
přestával	přestávat	k5eAaImAgMnS	přestávat
situaci	situace	k1gFnSc4	situace
zvládat	zvládat	k5eAaImF	zvládat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
egyptského	egyptský	k2eAgMnSc4d1	egyptský
guvernéra	guvernér	k1gMnSc4	guvernér
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řecké	řecký	k2eAgNnSc1d1	řecké
povstání	povstání	k1gNnSc2	povstání
krutě	krutě	k6eAd1	krutě
potlačil	potlačit	k5eAaPmAgInS	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
vzpouru	vzpoura	k1gFnSc4	vzpoura
janičářů	janičář	k1gMnPc2	janičář
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
Mahmut	Mahmut	k1gInSc1	Mahmut
řešil	řešit	k5eAaImAgInS	řešit
neméně	málo	k6eNd2	málo
radikálně	radikálně	k6eAd1	radikálně
<g/>
:	:	kIx,	:
prostě	prostě	k9	prostě
janičáře	janičář	k1gInPc4	janičář
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
sultánovi	sultánův	k2eAgMnPc1d1	sultánův
konečně	konečně	k6eAd1	konečně
uvolnily	uvolnit	k5eAaPmAgFnP	uvolnit
ruce	ruka	k1gFnPc1	ruka
k	k	k7c3	k
rázným	rázný	k2eAgFnPc3d1	rázná
reformám	reforma	k1gFnPc3	reforma
–	–	k?	–
modernizoval	modernizovat	k5eAaBmAgInS	modernizovat
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
administrativu	administrativa	k1gFnSc4	administrativa
<g/>
,	,	kIx,	,
budoval	budovat	k5eAaImAgMnS	budovat
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
,	,	kIx,	,
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
zrušil	zrušit	k5eAaPmAgInS	zrušit
feudalismus	feudalismus	k1gInSc1	feudalismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
úspěchy	úspěch	k1gInPc1	úspěch
však	však	k9	však
kalily	kalit	k5eAaImAgFnP	kalit
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
velmoci	velmoc	k1gFnPc1	velmoc
–	–	k?	–
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
uznat	uznat	k5eAaPmF	uznat
řeckou	řecký	k2eAgFnSc4d1	řecká
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
intervenci	intervence	k1gFnSc6	intervence
<g/>
,	,	kIx,	,
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Navarina	Navarin	k2eAgInSc2d1	Navarin
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
egyptsko-osmanského	egyptskosmanský	k2eAgNnSc2d1	egyptsko-osmanský
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
drinopolské	drinopolský	k2eAgFnSc3d1	drinopolský
dohodě	dohoda	k1gFnSc3	dohoda
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
ztratila	ztratit	k5eAaPmAgFnS	ztratit
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
(	(	kIx(	(
<g/>
zabrána	zabrat	k5eAaPmNgFnS	zabrat
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
získalo	získat	k5eAaPmAgNnS	získat
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Balkánu	Balkán	k1gInSc2	Balkán
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
demilitarizovanou	demilitarizovaný	k2eAgFnSc7d1	demilitarizovaná
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Řecka	Řecko	k1gNnSc2	Řecko
pak	pak	k6eAd1	pak
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
londýnský	londýnský	k2eAgInSc1d1	londýnský
protokol	protokol	k1gInSc1	protokol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
stál	stát	k5eAaImAgMnS	stát
egyptský	egyptský	k2eAgMnSc1d1	egyptský
guvernér	guvernér	k1gMnSc1	guvernér
Muhammad	Muhammad	k1gInSc4	Muhammad
Alí	Alí	k1gFnSc2	Alí
věrně	věrně	k6eAd1	věrně
po	po	k7c6	po
boku	bok	k1gInSc6	bok
osmanského	osmanský	k2eAgMnSc2d1	osmanský
sultána	sultán	k1gMnSc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
pánu	pán	k1gMnSc3	pán
postavil	postavit	k5eAaPmAgMnS	postavit
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
expandovat	expandovat	k5eAaImF	expandovat
do	do	k7c2	do
Syropalestiny	Syropalestina	k1gFnSc2	Syropalestina
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
jako	jako	k9	jako
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Muhammadův	Muhammadův	k2eAgMnSc1d1	Muhammadův
syn	syn	k1gMnSc1	syn
Ibráhím	Ibráhí	k1gMnPc3	Ibráhí
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
sultán	sultán	k1gMnSc1	sultán
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
své	svůj	k3xOyFgFnSc2	svůj
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
nepřítele	nepřítel	k1gMnSc4	nepřítel
–	–	k?	–
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
britské	britský	k2eAgNnSc1d1	Britské
loďstvo	loďstvo	k1gNnSc1	loďstvo
nebylo	být	k5eNaImAgNnS	být
nablízku	nablízku	k6eAd1	nablízku
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Ibráhíma	Ibráhí	k2eAgFnPc7d1	Ibráhí
z	z	k7c2	z
Anatolie	Anatolie	k1gFnSc2	Anatolie
nakonec	nakonec	k6eAd1	nakonec
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
ruské	ruský	k2eAgNnSc1d1	ruské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Syropalestiny	Syropalestina	k1gFnSc2	Syropalestina
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
osmanské	osmanský	k2eAgNnSc1d1	osmanské
vojsko	vojsko	k1gNnSc1	vojsko
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
vytlačit	vytlačit	k5eAaPmF	vytlačit
ani	ani	k8xC	ani
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pruského	pruský	k2eAgMnSc2d1	pruský
vojenského	vojenský	k2eAgMnSc2d1	vojenský
poradce	poradce	k1gMnSc2	poradce
Helmuta	Helmut	k1gMnSc2	Helmut
von	von	k1gInSc4	von
Moltke	Moltke	k1gNnSc2	Moltke
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ibráhím	Ibráhí	k1gNnSc7	Ibráhí
byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
uznán	uznán	k2eAgMnSc1d1	uznán
guvernérem	guvernér	k1gMnSc7	guvernér
v	v	k7c6	v
Damašku	Damašek	k1gInSc6	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Abdulmecit	Abdulmecit	k1gInSc1	Abdulmecit
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
polní	polní	k2eAgFnSc1d1	polní
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
pokladna	pokladna	k1gFnSc1	pokladna
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
Velmoci	velmoc	k1gFnPc1	velmoc
nakonec	nakonec	k6eAd1	nakonec
donutily	donutit	k5eAaPmAgFnP	donutit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
egyptské	egyptský	k2eAgInPc1d1	egyptský
vojsko	vojsko	k1gNnSc4	vojsko
Syropalestinu	Syropalestin	k1gInSc2	Syropalestin
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
však	však	k9	však
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
Libanonu	Libanon	k1gInSc2	Libanon
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
projevil	projevit	k5eAaPmAgInS	projevit
arabský	arabský	k2eAgInSc1d1	arabský
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Velmoci	velmoc	k1gFnPc1	velmoc
navíc	navíc	k6eAd1	navíc
získaly	získat	k5eAaPmAgFnP	získat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
přežila	přežít	k5eAaPmAgFnS	přežít
jen	jen	k9	jen
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gInSc3	jejich
zásahu	zásah	k1gInSc3	zásah
a	a	k8xC	a
cítily	cítit	k5eAaImAgInP	cítit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
oprávněny	oprávněn	k2eAgFnPc1d1	oprávněna
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
intervenovat	intervenovat	k5eAaImF	intervenovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pod	pod	k7c7	pod
zástěrkou	zástěrka	k1gFnSc7	zástěrka
ochrany	ochrana	k1gFnSc2	ochrana
zájmů	zájem	k1gInPc2	zájem
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
menšiny	menšina	k1gFnPc1	menšina
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgNnSc1d1	aktivní
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k6eAd1	zejména
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mluvilo	mluvit	k5eAaImAgNnS	mluvit
o	o	k7c6	o
"	"	kIx"	"
<g/>
nemocném	mocný	k2eNgMnSc6d1	nemocný
muži	muž	k1gMnSc6	muž
na	na	k7c6	na
Bosporu	Bospor	k1gInSc6	Bospor
<g/>
"	"	kIx"	"
a	a	k8xC	a
osnovalo	osnovat	k5eAaImAgNnS	osnovat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
však	však	k8xC	však
ostatní	ostatní	k2eAgFnPc1d1	ostatní
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
znepokojovaly	znepokojovat	k5eAaImAgInP	znepokojovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
Rusko	Rusko	k1gNnSc1	Rusko
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Valašsko	Valašsko	k1gNnSc4	Valašsko
a	a	k8xC	a
Moldavsko	Moldavsko	k1gNnSc4	Moldavsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
donutilo	donutit	k5eAaPmAgNnS	donutit
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
přijmout	přijmout	k5eAaPmF	přijmout
jeho	jeho	k3xOp3gInPc4	jeho
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
překročilo	překročit	k5eAaPmAgNnS	překročit
osmanské	osmanský	k2eAgNnSc1d1	osmanské
vojsko	vojsko	k1gNnSc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Omara	Omar	k1gMnSc2	Omar
Paši	paša	k1gMnSc2	paša
a	a	k8xC	a
podpořené	podpořený	k2eAgFnSc2d1	podpořená
egyptským	egyptský	k2eAgMnSc7d1	egyptský
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Osmani	Osman	k1gMnPc1	Osman
donutili	donutit	k5eAaPmAgMnP	donutit
ruské	ruský	k2eAgNnSc4d1	ruské
vojsko	vojsko	k1gNnSc4	vojsko
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
však	však	k9	však
Rusko	Rusko	k1gNnSc1	Rusko
potopilo	potopit	k5eAaPmAgNnS	potopit
tureckou	turecký	k2eAgFnSc4d1	turecká
flotilu	flotila	k1gFnSc4	flotila
u	u	k7c2	u
Sinopé	Sinopý	k2eAgFnSc2d1	Sinopý
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc2	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
obracet	obracet	k5eAaImF	obracet
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
flotily	flotila	k1gFnPc1	flotila
vpluly	vplout	k5eAaPmAgFnP	vplout
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
donutily	donutit	k5eAaPmAgInP	donutit
Rusko	Rusko	k1gNnSc4	Rusko
uzavřít	uzavřít	k5eAaPmF	uzavřít
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
mj.	mj.	kA	mj.
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmoci	velmoc	k1gFnPc1	velmoc
nemají	mít	k5eNaImIp3nP	mít
právo	právo	k1gNnSc4	právo
se	se	k3xPyFc4	se
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
vměšovat	vměšovat	k5eAaImF	vměšovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k9	ani
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
"	"	kIx"	"
<g/>
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
"	"	kIx"	"
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgNnSc1d1	Černé
moře	moře	k1gNnSc1	moře
bylo	být	k5eAaImAgNnS	být
demilitarizováno	demilitarizovat	k5eAaBmNgNnS	demilitarizovat
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
kavkazské	kavkazský	k2eAgFnSc2d1	kavkazská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
90	[number]	k4	90
<g/>
%	%	kIx~	%
Čerkesů	Čerkes	k1gMnPc2	Čerkes
etnicky	etnicky	k6eAd1	etnicky
vyčištěno	vyčištěn	k2eAgNnSc1d1	vyčištěno
<g/>
,	,	kIx,	,
vyhnáno	vyhnán	k2eAgNnSc1d1	vyhnáno
z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
uprchlých	uprchlá	k1gFnPc2	uprchlá
do	do	k7c2	do
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
700	[number]	k4	700
000	[number]	k4	000
Čerkesů	Čerkes	k1gMnPc2	Čerkes
usídlených	usídlený	k2eAgMnPc2d1	usídlený
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Abdülmecit	Abdülmecit	k1gMnSc1	Abdülmecit
dále	daleko	k6eAd2	daleko
zaváděl	zavádět	k5eAaImAgMnS	zavádět
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgMnS	podporovat
výstavbu	výstavba	k1gFnSc4	výstavba
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
příchod	příchod	k1gInSc1	příchod
evropských	evropský	k2eAgMnPc2d1	evropský
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
drobení	drobení	k1gNnSc3	drobení
říše	říš	k1gFnSc2	říš
však	však	k9	však
dále	daleko	k6eAd2	daleko
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Spojených	spojený	k2eAgNnPc2d1	spojené
rumunských	rumunský	k2eAgNnPc2d1	rumunské
knížectví	knížectví	k1gNnPc2	knížectví
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzpourami	vzpoura	k1gFnPc7	vzpoura
v	v	k7c6	v
Syropalestině	Syropalestina	k1gFnSc6	Syropalestina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gMnSc2	jeho
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Abdulazize	Abdulazize	k1gFnSc2	Abdulazize
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
nacionalistické	nacionalistický	k2eAgNnSc4d1	nacionalistické
mladoosmanské	mladoosmanský	k2eAgNnSc4d1	mladoosmanský
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
vadilo	vadit	k5eAaImAgNnS	vadit
přílišné	přílišný	k2eAgNnSc1d1	přílišné
přejímání	přejímání	k1gNnSc1	přejímání
západních	západní	k2eAgFnPc2d1	západní
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
sbližování	sbližování	k1gNnSc1	sbližování
se	s	k7c7	s
západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
příliv	příliv	k1gInSc1	příliv
západních	západní	k2eAgFnPc2d1	západní
investic	investice	k1gFnPc2	investice
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
státnímu	státní	k2eAgInSc3d1	státní
bankrotu	bankrot	k1gInSc3	bankrot
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Panslovanské	Panslovanský	k2eAgFnPc1d1	Panslovanský
myšlenky	myšlenka	k1gFnPc1	myšlenka
navíc	navíc	k6eAd1	navíc
podnítily	podnítit	k5eAaPmAgFnP	podnítit
povstání	povstání	k1gNnSc4	povstání
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnSc3d1	Černé
Hoře	hora	k1gFnSc3	hora
a	a	k8xC	a
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
,	,	kIx,	,
sultán	sultán	k1gMnSc1	sultán
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
servility	servilita	k1gFnSc2	servilita
vůči	vůči	k7c3	vůči
Západu	západ	k1gInSc3	západ
a	a	k8xC	a
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rusko-turecká	ruskourecký	k2eAgFnSc1d1	rusko-turecká
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Abdulhamid	Abdulhamid	k1gInSc1	Abdulhamid
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyhovět	vyhovět	k5eAaPmF	vyhovět
liberálním	liberální	k2eAgFnPc3d1	liberální
myšlenkám	myšlenka	k1gFnPc3	myšlenka
<g/>
,	,	kIx,	,
zavázal	zavázat	k5eAaPmAgMnS	zavázat
se	se	k3xPyFc4	se
respektovat	respektovat	k5eAaImF	respektovat
základní	základní	k2eAgNnPc4d1	základní
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
1877	[number]	k4	1877
otevřel	otevřít	k5eAaPmAgInS	otevřít
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Neklid	neklid	k1gInSc1	neklid
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
však	však	k9	však
neustával	ustávat	k5eNaImAgMnS	ustávat
a	a	k8xC	a
do	do	k7c2	do
osmanských	osmanský	k2eAgFnPc2d1	Osmanská
záležitostí	záležitost	k1gFnPc2	záležitost
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
vměšovat	vměšovat	k5eAaImF	vměšovat
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
nakonec	nakonec	k6eAd1	nakonec
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Osmanské	osmanský	k2eAgFnPc4d1	Osmanská
říši	říše	k1gFnSc4	říše
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
válku	válek	k1gInSc2	válek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
až	až	k9	až
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
sultána	sultán	k1gMnSc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
vše	všechen	k3xTgNnSc1	všechen
nasvědčovalo	nasvědčovat	k5eAaImAgNnS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
chce	chtít	k5eAaImIp3nS	chtít
zmocnit	zmocnit	k5eAaPmF	zmocnit
Istanbulu	Istanbul	k1gInSc3	Istanbul
<g/>
,	,	kIx,	,
vyslala	vyslat	k5eAaPmAgFnS	vyslat
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
do	do	k7c2	do
Úžin	úžina	k1gFnPc2	úžina
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vynutily	vynutit	k5eAaPmAgInP	vynutit
uzavření	uzavření	k1gNnSc4	uzavření
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
ze	z	k7c2	z
San	San	k1gFnPc2	San
Stefana	Stefan	k1gMnSc2	Stefan
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byly	být	k5eAaImAgInP	být
dohodnuty	dohodnut	k2eAgInPc1d1	dohodnut
ruské	ruský	k2eAgInPc1d1	ruský
územní	územní	k2eAgInPc1d1	územní
zisky	zisk	k1gInPc1	zisk
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
,	,	kIx,	,
samostatnost	samostatnost	k1gFnSc4	samostatnost
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
autonomie	autonomie	k1gFnSc1	autonomie
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
však	však	k9	však
poškodila	poškodit	k5eAaPmAgFnS	poškodit
zájmy	zájem	k1gInPc4	zájem
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byla	být	k5eAaImAgFnS	být
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
revidována	revidovat	k5eAaImNgFnS	revidovat
na	na	k7c6	na
berlínském	berlínský	k2eAgInSc6d1	berlínský
kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
určil	určit	k5eAaPmAgInS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
sice	sice	k8xC	sice
zůstane	zůstat	k5eAaPmIp3nS	zůstat
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pod	pod	k7c7	pod
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
správou	správa	k1gFnSc7	správa
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
získala	získat	k5eAaPmAgFnS	získat
Kypr	Kypr	k1gInSc4	Kypr
a	a	k8xC	a
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
zavázat	zavázat	k5eAaPmF	zavázat
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
ztratila	ztratit	k5eAaPmAgFnS	ztratit
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
svého	své	k1gNnSc2	své
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
Egypt	Egypt	k1gInSc1	Egypt
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
stal	stát	k5eAaPmAgMnS	stát
de	de	k?	de
facto	facto	k1gNnSc4	facto
britskou	britský	k2eAgFnSc7d1	britská
državou	država	k1gFnSc7	država
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
zoufale	zoufale	k6eAd1	zoufale
hledala	hledat	k5eAaImAgFnS	hledat
oporu	opora	k1gFnSc4	opora
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
nedávno	nedávno	k6eAd1	nedávno
vzniklém	vzniklý	k2eAgNnSc6d1	vzniklé
Německém	německý	k2eAgNnSc6d1	německé
císařství	císařství	k1gNnSc6	císařství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Turkům	Turek	k1gMnPc3	Turek
předalo	předat	k5eAaPmAgNnS	předat
mnohé	mnohé	k1gNnSc4	mnohé
válečné	válečný	k2eAgFnSc2d1	válečná
zkušenosti	zkušenost	k1gFnSc2	zkušenost
i	i	k8xC	i
moderní	moderní	k2eAgNnPc1d1	moderní
děla	dělo	k1gNnPc1	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
drobení	drobení	k1gNnSc2	drobení
říše	říš	k1gFnSc2	říš
však	však	k9	však
zdaleka	zdaleka	k6eAd1	zdaleka
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
kurdské	kurdský	k2eAgInPc1d1	kurdský
oddíly	oddíl	k1gInPc1	oddíl
osmanského	osmanský	k2eAgNnSc2d1	osmanské
vojska	vojsko	k1gNnSc2	vojsko
zmasakrovaly	zmasakrovat	k5eAaPmAgFnP	zmasakrovat
tisíce	tisíc	k4xCgInPc1	tisíc
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
Arménů	Armén	k1gMnPc2	Armén
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
pobouření	pobouření	k1gNnSc4	pobouření
a	a	k8xC	a
britsko-ruskou	britskouský	k2eAgFnSc4d1	britsko-ruská
intervenci	intervence	k1gFnSc4	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
byli	být	k5eAaImAgMnP	být
navíc	navíc	k6eAd1	navíc
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
nespokojeni	spokojit	k5eNaPmNgMnP	spokojit
s	s	k7c7	s
Abdülhamidovou	Abdülhamidový	k2eAgFnSc7d1	Abdülhamidový
despotickou	despotický	k2eAgFnSc7d1	despotická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
také	také	k9	také
v	v	k7c6	v
Syropalestině	Syropalestina	k1gFnSc6	Syropalestina
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zejména	zejména	k9	zejména
po	po	k7c6	po
pogromech	pogrom	k1gInPc6	pogrom
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
přicházelo	přicházet	k5eAaImAgNnS	přicházet
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
vzápětí	vzápětí	k6eAd1	vzápětí
napadáni	napadán	k2eAgMnPc1d1	napadán
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšení	zmenšení	k1gNnSc1	zmenšení
osmanského	osmanský	k2eAgNnSc2d1	osmanské
území	území	k1gNnSc2	území
také	také	k9	také
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
tureckého	turecký	k2eAgInSc2d1	turecký
folklóru	folklór	k1gInSc2	folklór
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
hnutí	hnutí	k1gNnSc1	hnutí
mladoturků	mladoturek	k1gMnPc2	mladoturek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
nerespektovalo	respektovat	k5eNaImAgNnS	respektovat
existenci	existence	k1gFnSc4	existence
netureckých	turecký	k2eNgNnPc2d1	turecký
muslimských	muslimský	k2eAgNnPc2d1	muslimské
etnik	etnikum	k1gNnPc2	etnikum
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Arabové	Arab	k1gMnPc1	Arab
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
pak	pak	k6eAd1	pak
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
etnické	etnický	k2eAgFnPc1d1	etnická
menšiny	menšina	k1gFnPc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Mladoturecké	mladoturecký	k2eAgNnSc1d1	mladoturecký
hnutí	hnutí	k1gNnSc1	hnutí
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
hnutím	hnutí	k1gNnSc7	hnutí
liberálním	liberální	k2eAgNnSc7d1	liberální
a	a	k8xC	a
po	po	k7c6	po
povstání	povstání	k1gNnSc6	povstání
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
obnovení	obnovení	k1gNnSc1	obnovení
konstituce	konstituce	k1gFnSc2	konstituce
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
osmanského	osmanský	k2eAgNnSc2d1	osmanské
panství	panství	k1gNnSc2	panství
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
dále	daleko	k6eAd2	daleko
hroutily	hroutit	k5eAaImAgFnP	hroutit
<g/>
,	,	kIx,	,
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
carství	carství	k1gNnSc1	carství
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
,	,	kIx,	,
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
balkánské	balkánský	k2eAgInPc4d1	balkánský
války	válek	k1gInPc4	válek
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Balkánská	balkánský	k2eAgFnSc1d1	balkánská
liga	liga	k1gFnSc1	liga
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
nakonec	nakonec	k6eAd1	nakonec
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
osmanské	osmanský	k2eAgNnSc4d1	osmanské
území	území	k1gNnSc4	území
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jen	jen	k9	jen
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
Balkánu	Balkán	k1gInSc2	Balkán
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
provázena	provázet	k5eAaImNgFnS	provázet
několika	několik	k4yIc7	několik
dalšími	další	k2eAgInPc7d1	další
převraty	převrat	k1gInPc7	převrat
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Mladoturecká	mladoturecký	k2eAgFnSc1d1	mladoturecká
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoci	pravomoc	k1gFnPc1	pravomoc
nového	nový	k2eAgMnSc2d1	nový
sultána	sultán	k1gMnSc2	sultán
Mehmeda	Mehmed	k1gMnSc2	Mehmed
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
nakonec	nakonec	k6eAd1	nakonec
značně	značně	k6eAd1	značně
okleštěny	okleštit	k5eAaPmNgFnP	okleštit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
mohla	moct	k5eAaImAgFnS	moct
přiklonit	přiklonit	k5eAaPmF	přiklonit
na	na	k7c4	na
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
Británie	Británie	k1gFnSc1	Británie
zabavila	zabavit	k5eAaPmAgFnS	zabavit
nové	nový	k2eAgFnPc4d1	nová
válečné	válečný	k2eAgFnPc4d1	válečná
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Britové	Brit	k1gMnPc1	Brit
pro	pro	k7c4	pro
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
právě	právě	k6eAd1	právě
postavili	postavit	k5eAaPmAgMnP	postavit
<g/>
,	,	kIx,	,
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
se	se	k3xPyFc4	se
Mehmet	Mehmet	k1gInSc1	Mehmet
V.	V.	kA	V.
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
válku	válka	k1gFnSc4	válka
Dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
postupně	postupně	k6eAd1	postupně
otevřela	otevřít	k5eAaPmAgFnS	otevřít
několik	několik	k4yIc4	několik
front	fronta	k1gFnPc2	fronta
<g/>
:	:	kIx,	:
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
a	a	k8xC	a
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
britským	britský	k2eAgMnPc3d1	britský
vojskům	vojsko	k1gNnPc3	vojsko
přidávali	přidávat	k5eAaImAgMnP	přidávat
arabští	arabský	k2eAgMnPc1d1	arabský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vyčerpala	vyčerpat	k5eAaPmAgFnS	vyčerpat
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
zachvátil	zachvátit	k5eAaPmAgInS	zachvátit
hladomor	hladomor	k1gMnSc1	hladomor
a	a	k8xC	a
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
jen	jen	k9	jen
díky	díky	k7c3	díky
německé	německý	k2eAgFnSc3d1	německá
podpoře	podpora	k1gFnSc3	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
další	další	k2eAgFnSc7d1	další
arménskou	arménský	k2eAgFnSc7d1	arménská
genocidou	genocida	k1gFnSc7	genocida
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
sultán	sultán	k1gMnSc1	sultán
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arméni	Armén	k1gMnPc1	Armén
podpoří	podpořit	k5eAaPmIp3nP	podpořit
ruské	ruský	k2eAgMnPc4d1	ruský
útočníky	útočník	k1gMnPc4	útočník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
půlce	půlka	k1gFnSc6	půlka
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
fronty	fronta	k1gFnPc1	fronta
začaly	začít	k5eAaPmAgFnP	začít
hroutit	hroutit	k5eAaImF	hroutit
a	a	k8xC	a
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
proto	proto	k8xC	proto
urychleně	urychleně	k6eAd1	urychleně
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
byl	být	k5eAaImAgInS	být
okupován	okupovat	k5eAaBmNgInS	okupovat
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
získala	získat	k5eAaPmAgFnS	získat
Irák	Irák	k1gInSc4	Irák
<g/>
,	,	kIx,	,
Palestinu	Palestina	k1gFnSc4	Palestina
a	a	k8xC	a
Zajordání	Zajordání	k1gNnSc4	Zajordání
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Sýrii	Sýrie	k1gFnSc4	Sýrie
a	a	k8xC	a
Libanon	Libanon	k1gInSc4	Libanon
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
anatolské	anatolský	k2eAgNnSc1d1	anatolský
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
Mustafa	Mustaf	k1gMnSc2	Mustaf
Kemal	Kemal	k1gMnSc1	Kemal
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vydat	vydat	k5eAaPmF	vydat
Prohlášení	prohlášení	k1gNnPc1	prohlášení
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgInSc4d1	zvolený
parlament	parlament	k1gInSc4	parlament
však	však	k9	však
Britové	Brit	k1gMnPc1	Brit
obsadili	obsadit	k5eAaPmAgMnP	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Kemal	Kemal	k1gMnSc1	Kemal
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Ankary	Ankara	k1gFnSc2	Ankara
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zřídil	zřídit	k5eAaPmAgMnS	zřídit
Velké	velký	k2eAgNnSc4d1	velké
národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Mírové	mírový	k2eAgFnPc1d1	mírová
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgInPc2	který
měli	mít	k5eAaImAgMnP	mít
Arméni	Armén	k1gMnPc1	Armén
získat	získat	k5eAaPmF	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
Kurdistán	Kurdistán	k1gInSc4	Kurdistán
autonomii	autonomie	k1gFnSc4	autonomie
Kemal	Kemal	k1gInSc1	Kemal
nepřijal	přijmout	k5eNaPmAgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začalo	začít	k5eAaPmAgNnS	začít
řecké	řecký	k2eAgNnSc1d1	řecké
vojsko	vojsko	k1gNnSc1	vojsko
zatlačovat	zatlačovat	k5eAaImF	zatlačovat
kemalisty	kemalista	k1gMnPc4	kemalista
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Kemal	Kemal	k1gMnSc1	Kemal
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vytlačení	vytlačení	k1gNnSc3	vytlačení
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
sultanátu	sultanát	k1gInSc2	sultanát
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Velmoci	velmoc	k1gFnPc1	velmoc
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Lausanne	Lausanne	k1gNnSc2	Lausanne
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
nakonec	nakonec	k6eAd1	nakonec
vymezila	vymezit	k5eAaPmAgFnS	vymezit
turecké	turecký	k2eAgFnPc4d1	turecká
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
požadavky	požadavek	k1gInPc4	požadavek
Arménů	Armén	k1gMnPc2	Armén
a	a	k8xC	a
Kurdů	Kurd	k1gMnPc2	Kurd
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
byly	být	k5eAaImAgInP	být
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
okupace	okupace	k1gFnSc1	okupace
Istanbulu	Istanbul	k1gInSc2	Istanbul
poraženými	poražený	k2eAgFnPc7d1	poražená
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
nemuselo	muset	k5eNaImAgNnS	muset
platit	platit	k5eAaImF	platit
válečné	válečný	k2eAgFnPc4d1	válečná
reparace	reparace	k1gFnPc4	reparace
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Pro	pro	k7c4
Turecko	Turecko	k1gNnSc4
úspěšná	úspěšný	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
proti	proti	k7c3
okupačním	okupační	k2eAgFnPc3d1
silám	síla	k1gFnPc3
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
Turecké	turecký	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
srdci	srdce	k1gNnSc6
malé	malý	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
ke	k	k7c3
zrušení	zrušení	k1gNnSc3
osmanské	osmanský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Mustafa	Mustaf	k1gMnSc4	Mustaf
Kemal	Kemal	k1gInSc4	Kemal
jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
turečtina	turečtina	k1gFnSc1	turečtina
byla	být	k5eAaImAgFnS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
oghuzský	oghuzský	k2eAgInSc4d1	oghuzský
turkický	turkický	k2eAgInSc4d1	turkický
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
perštinou	perština	k1gFnSc7	perština
a	a	k8xC	a
arabštinou	arabština	k1gFnSc7	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Osmané	Osman	k1gMnPc1	Osman
měli	mít	k5eAaImAgMnP	mít
několik	několik	k4yIc4	několik
vlivných	vlivný	k2eAgInPc2d1	vlivný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
turečtinu	turečtina	k1gFnSc4	turečtina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mluvila	mluvit	k5eAaImAgFnS	mluvit
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Anatolii	Anatolie	k1gFnSc6	Anatolie
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Albánie	Albánie	k1gFnSc2	Albánie
a	a	k8xC	a
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
;	;	kIx,	;
perštinu	perština	k1gFnSc4	perština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mluvili	mluvit	k5eAaImAgMnP	mluvit
vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
<g/>
;	;	kIx,	;
arabštinu	arabština	k1gFnSc4	arabština
<g/>
,	,	kIx,	,
používanou	používaný	k2eAgFnSc4d1	používaná
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
,	,	kIx,	,
Levantě	Levanta	k1gFnSc6	Levanta
a	a	k8xC	a
částech	část	k1gFnPc6	část
Afrického	africký	k2eAgInSc2d1	africký
rohu	roh	k1gInSc2	roh
<g/>
;	;	kIx,	;
a	a	k8xC	a
somálštinu	somálština	k1gFnSc4	somálština
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
afrického	africký	k2eAgInSc2d1	africký
rohu	roh	k1gInSc2	roh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
stoletích	století	k1gNnPc6	století
se	se	k3xPyFc4	se
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jim	on	k3xPp3gFnPc3	on
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
specifické	specifický	k2eAgFnPc4d1	specifická
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
perština	perština	k1gFnSc1	perština
sloužila	sloužit	k5eAaImAgFnS	sloužit
především	především	k6eAd1	především
jako	jako	k8xS	jako
literární	literární	k2eAgInSc4d1	literární
jazyk	jazyk	k1gInSc4	jazyk
pro	pro	k7c4	pro
vzdělané	vzdělaný	k2eAgMnPc4d1	vzdělaný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
arabština	arabština	k1gFnSc1	arabština
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
během	během	k7c2	během
náboženských	náboženský	k2eAgInPc2d1	náboženský
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Turečtina	turečtina	k1gFnSc1	turečtina
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
variantě	varianta	k1gFnSc6	varianta
byla	být	k5eAaImAgFnS	být
vojenským	vojenský	k2eAgInSc7d1	vojenský
jazykem	jazyk	k1gInSc7	jazyk
i	i	k8xC	i
jazykem	jazyk	k1gInSc7	jazyk
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
již	již	k6eAd1	již
od	od	k7c2	od
raných	raný	k2eAgFnPc2d1	raná
dob	doba	k1gFnPc2	doba
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
oficiálně	oficiálně	k6eAd1	oficiálně
upevnila	upevnit	k5eAaPmAgFnS	upevnit
úřední	úřední	k2eAgInSc4d1	úřední
status	status	k1gInSc4	status
turečtiny	turečtina	k1gFnSc2	turečtina
</s>
