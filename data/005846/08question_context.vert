<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
též	též	k9	též
Otomanská	otomanský	k2eAgFnSc1d1	Otomanská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
možno	možno	k6eAd1	možno
psát	psát	k5eAaImF	psát
i	i	k9	i
malé	malý	k2eAgNnSc4d1	malé
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
říší	říš	k1gFnPc2	říš
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
při	při	k7c6	při
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1299	[number]	k4	1299
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
oblasti	oblast	k1gFnSc6	oblast
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
,	,	kIx,	,
Černomoří	Černomoří	k1gNnSc2	Černomoří
<g/>
,	,	kIx,	,
Blízkého	blízký	k2eAgInSc2d1	blízký
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>

