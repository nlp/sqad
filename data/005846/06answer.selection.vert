<s>
Říše	říše	k1gFnSc1	říše
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1299	[number]	k4	1299
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
oblasti	oblast	k1gFnSc6	oblast
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
,	,	kIx,	,
Černomoří	Černomoří	k1gNnSc2	Černomoří
<g/>
,	,	kIx,	,
Blízkého	blízký	k2eAgInSc2d1	blízký
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
