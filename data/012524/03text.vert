<p>
<s>
Nike	Nike	k1gFnSc1	Nike
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
NYSE	NYSE	kA	NYSE
<g/>
:	:	kIx,	:
NKE	NKE	kA	NKE
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
,	,	kIx,	,
US	US	kA	US
/	/	kIx~	/
<g/>
ˈ	ˈ	k?	ˈ
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
také	také	k9	také
<g/>
,	,	kIx,	,
non-US	non-US	k?	non-US
/	/	kIx~	/
<g/>
ˈ	ˈ	k?	ˈ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
vyrábějící	vyrábějící	k2eAgNnSc4d1	vyrábějící
převážně	převážně	k6eAd1	převážně
sportovní	sportovní	k2eAgNnSc4d1	sportovní
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
s	s	k7c7	s
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Oregonu	Oregon	k1gInSc6	Oregon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fiskálním	fiskální	k2eAgInSc6d1	fiskální
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
firma	firma	k1gFnSc1	firma
obratu	obrat	k1gInSc2	obrat
34,35	[number]	k4	34,35
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
atletem	atlet	k1gMnSc7	atlet
Philipem	Philip	k1gMnSc7	Philip
Knightem	Knight	k1gMnSc7	Knight
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
trenérem	trenér	k1gMnSc7	trenér
Billem	Bill	k1gMnSc7	Bill
Bowermanem	Bowerman	k1gMnSc7	Bowerman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Logo	logo	k1gNnSc1	logo
==	==	k?	==
</s>
</p>
<p>
<s>
Logo	logo	k1gNnSc1	logo
firmy	firma	k1gFnSc2	firma
(	(	kIx(	(
<g/>
swoosh	swoosh	k1gInSc1	swoosh
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
Carolyn	Carolyn	k1gNnSc4	Carolyn
Davidson	Davidsona	k1gFnPc2	Davidsona
<g/>
,	,	kIx,	,
studentkou	studentka	k1gFnSc7	studentka
grafiky	grafika	k1gFnSc2	grafika
na	na	k7c6	na
portlandské	portlandský	k2eAgFnSc6d1	Portlandská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Studentka	studentka	k1gFnSc1	studentka
byla	být	k5eAaImAgFnS	být
placena	platit	k5eAaImNgFnS	platit
od	od	k7c2	od
pracovní	pracovní	k2eAgFnSc2d1	pracovní
hodiny	hodina	k1gFnSc2	hodina
částkou	částka	k1gFnSc7	částka
2	[number]	k4	2
dolary	dolar	k1gInPc7	dolar
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Koncová	koncový	k2eAgFnSc1d1	koncová
cena	cena	k1gFnSc1	cena
loga	logo	k1gNnSc2	logo
a	a	k8xC	a
výplata	výplata	k1gFnSc1	výplata
pro	pro	k7c4	pro
Carolyn	Carolyn	k1gInSc4	Carolyn
za	za	k7c4	za
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
symbolem	symbol	k1gInSc7	symbol
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
35	[number]	k4	35
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
značky	značka	k1gFnSc2	značka
Nike	Nike	k1gFnSc1	Nike
12	[number]	k4	12
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
představuje	představovat	k5eAaImIp3nS	představovat
křídlo	křídlo	k1gNnSc4	křídlo
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
značky	značka	k1gFnSc2	značka
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
řecké	řecký	k2eAgFnSc2d1	řecká
okřídlené	okřídlený	k2eAgFnSc2d1	okřídlená
bohyně	bohyně	k1gFnSc2	bohyně
vítězství	vítězství	k1gNnSc2	vítězství
Níké	Níká	k1gFnSc2	Níká
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
logo	logo	k1gNnSc1	logo
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kasky	Kask	k1gInPc1	Kask
versus	versus	k7c1	versus
Nike	Nike	k1gFnPc2	Nike
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
Mark	Mark	k1gMnSc1	Mark
Kasky	Kaska	k1gFnSc2	Kaska
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Nike	Nike	k1gFnSc4	Nike
<g/>
,	,	kIx,	,
že	že	k8xS	že
lže	lhát	k5eAaImIp3nS	lhát
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
San	San	k1gFnSc2	San
Francisco	Francisco	k6eAd1	Francisco
Examiner	Examiner	k1gInSc4	Examiner
ujišťovala	ujišťovat	k5eAaImAgFnS	ujišťovat
všechny	všechen	k3xTgFnPc4	všechen
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělníci	dělník	k1gMnPc1	dělník
této	tento	k3xDgFnSc2	tento
korporace	korporace	k1gFnSc2	korporace
po	po	k7c6	po
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
těšit	těšit	k5eAaImF	těšit
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
pracovních	pracovní	k2eAgNnPc2d1	pracovní
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
minimální	minimální	k2eAgFnSc1d1	minimální
mzda	mzda	k1gFnSc1	mzda
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
směrnice	směrnice	k1gFnSc2	směrnice
a	a	k8xC	a
rovné	rovný	k2eAgFnPc4d1	rovná
možnosti	možnost	k1gFnPc4	možnost
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kasky	Kasky	k6eAd1	Kasky
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
to	ten	k3xDgNnSc1	ten
nebyla	být	k5eNaImAgFnS	být
pravda	pravda	k1gFnSc1	pravda
–	–	k?	–
audit	audit	k1gInSc4	audit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
odhalil	odhalit	k5eAaPmAgInS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
dělníci	dělník	k1gMnPc1	dělník
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
byli	být	k5eAaImAgMnP	být
rutinně	rutinně	k6eAd1	rutinně
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
chemikáliím	chemikálie	k1gFnPc3	chemikálie
způsobujícím	způsobující	k2eAgInPc3d1	způsobující
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byly	být	k5eAaImAgFnP	být
nezákonné	zákonný	k2eNgFnPc1d1	nezákonná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sama	sám	k3xTgFnSc1	sám
Nike	Nike	k1gFnSc1	Nike
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
<g/>
,	,	kIx,	,
nalezla	nalézt	k5eAaBmAgFnS	nalézt
"	"	kIx"	"
<g/>
důkazy	důkaz	k1gInPc1	důkaz
fyzického	fyzický	k2eAgNnSc2d1	fyzické
a	a	k8xC	a
slovního	slovní	k2eAgNnSc2d1	slovní
zneužívání	zneužívání	k1gNnSc2	zneužívání
a	a	k8xC	a
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
obtěžování	obtěžování	k1gNnSc2	obtěžování
v	v	k7c6	v
devíti	devět	k4xCc3	devět
jejích	její	k3xOp3gFnPc6	její
továrnách	továrna	k1gFnPc6	továrna
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
tisíce	tisíc	k4xCgInPc1	tisíc
zákazníků	zákazník	k1gMnPc2	zákazník
Nike	Nike	k1gFnSc2	Nike
byli	být	k5eAaImAgMnP	být
obelháni	obelhat	k5eAaPmNgMnP	obelhat
a	a	k8xC	a
kupovali	kupovat	k5eAaImAgMnP	kupovat
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
zboží	zboží	k1gNnSc4	zboží
na	na	k7c6	na
základě	základ	k1gInSc6	základ
lživých	lživý	k2eAgInPc2d1	lživý
předpokladů	předpoklad	k1gInPc2	předpoklad
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
Kasky	Kask	k1gInPc7	Kask
korporaci	korporace	k1gFnSc4	korporace
Nike	Nike	k1gFnSc2	Nike
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Kasky	Kaska	k1gFnSc2	Kaska
svůj	svůj	k3xOyFgInSc4	svůj
případ	případ	k1gInSc4	případ
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
vrchní	vrchní	k1gMnSc1	vrchní
kalifornský	kalifornský	k2eAgInSc4d1	kalifornský
soud	soud	k1gInSc4	soud
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nike	Nike	k1gFnSc1	Nike
vskutku	vskutku	k9	vskutku
porušila	porušit	k5eAaPmAgFnS	porušit
zákony	zákon	k1gInPc7	zákon
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
nekalá	kalý	k2eNgFnSc1d1	nekalá
soutěž	soutěž	k1gFnSc1	soutěž
a	a	k8xC	a
lživá	lživý	k2eAgFnSc1d1	lživá
reklama	reklama	k1gFnSc1	reklama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nike	Nike	k1gFnSc1	Nike
se	se	k3xPyFc4	se
ale	ale	k9	ale
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
,	,	kIx,	,
vzala	vzít	k5eAaPmAgFnS	vzít
případ	případ	k1gInSc4	případ
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
se	s	k7c7	s
slyšením	slyšení	k1gNnSc7	slyšení
<g/>
.	.	kIx.	.
</s>
<s>
Nike	Nike	k1gFnSc1	Nike
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
coby	coby	k?	coby
korporátní	korporátní	k2eAgFnSc1d1	korporátní
osoba	osoba	k1gFnSc1	osoba
měla	mít	k5eAaImAgFnS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
<g/>
)	)	kIx)	)
znamenala	znamenat	k5eAaImAgFnS	znamenat
i	i	k9	i
svobodu	svoboda	k1gFnSc4	svoboda
lhát	lhát	k5eAaImF	lhát
<g/>
.	.	kIx.	.
</s>
<s>
Opírala	opírat	k5eAaImAgFnS	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
precedens	precedens	k1gNnSc4	precedens
Southern	Southerna	k1gFnPc2	Southerna
Pacific	Pacifice	k1gFnPc2	Pacifice
Railroad	Railroad	k1gInSc1	Railroad
versus	versus	k7c1	versus
kraj	kraj	k1gInSc1	kraj
Santa	Santo	k1gNnSc2	Santo
Clara	Clara	k1gFnSc1	Clara
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
jistý	jistý	k2eAgMnSc1d1	jistý
John	John	k1gMnSc1	John
Chandler	Chandler	k1gMnSc1	Chandler
Bancroft	Bancroft	k2eAgInSc1d1	Bancroft
Davis	Davis	k1gInSc1	Davis
přisoudil	přisoudit	k5eAaPmAgInS	přisoudit
korporacím	korporace	k1gFnPc3	korporace
status	status	k1gInSc4	status
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Kaskyho	Kaskyze	k6eAd1	Kaskyze
právník	právník	k1gMnSc1	právník
ale	ale	k8xC	ale
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
detailním	detailní	k2eAgInSc7d1	detailní
rozborem	rozbor	k1gInSc7	rozbor
tohoto	tento	k3xDgInSc2	tento
případu	případ	k1gInSc2	případ
a	a	k8xC	a
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
tehdy	tehdy	k6eAd1	tehdy
nevyřkl	vyřknout	k5eNaPmAgInS	vyřknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
korporace	korporace	k1gFnPc1	korporace
byly	být	k5eAaImAgFnP	být
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
precedens	precedens	k1gNnSc1	precedens
je	být	k5eAaImIp3nS	být
mylně	mylně	k6eAd1	mylně
chápán	chápat	k5eAaImNgInS	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Předsedající	předsedající	k2eAgMnSc1d1	předsedající
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Ranquist	Ranquist	k1gInSc1	Ranquist
vyslechl	vyslechnout	k5eAaPmAgInS	vyslechnout
tuto	tento	k3xDgFnSc4	tento
argumentaci	argumentace	k1gFnSc4	argumentace
a	a	k8xC	a
soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ke	k	k7c3	k
slyšení	slyšení	k1gNnSc3	slyšení
tohoto	tento	k3xDgInSc2	tento
případu	případ	k1gInSc2	případ
vůbec	vůbec	k9	vůbec
nemělo	mít	k5eNaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
soudu	soud	k1gInSc2	soud
nižší	nízký	k2eAgFnSc2d2	nižší
instance	instance	k1gFnSc2	instance
nabylo	nabýt	k5eAaPmAgNnS	nabýt
právní	právní	k2eAgNnSc1d1	právní
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
Nike	Nike	k1gFnSc1	Nike
prohrála	prohrát	k5eAaPmAgFnS	prohrát
tento	tento	k3xDgInSc4	tento
případ	případ	k1gInSc4	případ
a	a	k8xC	a
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
se	se	k3xPyFc4	se
s	s	k7c7	s
Kaskym	Kaskym	k1gInSc1	Kaskym
<g/>
,	,	kIx,	,
souhlasíc	souhlasit	k5eAaImSgFnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
daruje	darovat	k5eAaPmIp3nS	darovat
sedmimístné	sedmimístný	k2eAgFnPc4d1	sedmimístná
sumy	suma	k1gFnPc4	suma
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
pracovníkům	pracovník	k1gMnPc3	pracovník
v	v	k7c6	v
textilu	textil	k1gInSc6	textil
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nike	Nike	k1gFnSc2	Nike
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
firmy	firma	k1gFnSc2	firma
</s>
</p>
