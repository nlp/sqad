<s>
Ľudovít	Ľudovít	k5eAaPmF	Ľudovít
Greššo	Greššo	k6eAd1	Greššo
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Zvolen	Zvolen	k1gInSc1	Zvolen
<g/>
,	,	kIx,	,
Uhersko	Uhersko	k1gNnSc1	Uhersko
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
otec	otec	k1gMnSc1	otec
<g/>
:	:	kIx,	:
Július	Július	k1gMnSc1	Július
Greššo	Greššo	k1gMnSc1	Greššo
matka	matka	k1gFnSc1	matka
<g/>
:	:	kIx,	:
Anna	Anna	k1gFnSc1	Anna
rozená	rozený	k2eAgFnSc1d1	rozená
Poliaková	Poliakový	k2eAgFnSc1d1	Poliaková
syn	syn	k1gMnSc1	syn
<g/>
:	:	kIx,	:
Ján	Ján	k1gMnSc1	Ján
Greššo	Greššo	k1gMnSc1	Greššo
Studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Zvolenu	Zvolen	k1gInSc6	Zvolen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
přednosta	přednosta	k1gMnSc1	přednosta
oddělení	oddělení	k1gNnSc2	oddělení
Nemocenské	mocenský	k2eNgFnSc2d1	mocenský
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Podbrezové	Podbrezová	k1gFnSc6	Podbrezová
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
DJGT	DJGT	kA	DJGT
ve	v	k7c6	v
Zvolenu	Zvolen	k1gInSc6	Zvolen
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Divadla	divadlo	k1gNnSc2	divadlo
SNP	SNP	kA	SNP
v	v	k7c6	v
Martině	Martina	k1gFnSc6	Martina
<g/>
,	,	kIx,	,
Krajského	krajský	k2eAgNnSc2d1	krajské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
herec	herec	k1gMnSc1	herec
Divadla	divadlo	k1gNnSc2	divadlo
Andreje	Andrej	k1gMnSc2	Andrej
Bagara	Bagar	k1gMnSc2	Bagar
v	v	k7c6	v
Nitře	Nitra	k1gFnSc6	Nitra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
ve	v	k7c6	v
Zvolenu	Zvolen	k1gInSc6	Zvolen
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
sdružení	sdružení	k1gNnSc6	sdružení
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
,	,	kIx,	,
od	od	k7c2	od
1948	[number]	k4	1948
ve	v	k7c4	v
Sdružení	sdružení	k1gNnSc4	sdružení
Detvan	Detvan	k1gMnSc1	Detvan
ve	v	k7c6	v
Zvolenu	Zvolen	k1gInSc6	Zvolen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
profesionálních	profesionální	k2eAgInPc6d1	profesionální
souborech	soubor	k1gInPc6	soubor
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
jako	jako	k9	jako
zralý	zralý	k2eAgMnSc1d1	zralý
charakterní	charakterní	k2eAgMnSc1d1	charakterní
herec	herec	k1gMnSc1	herec
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
žánrech	žánr	k1gInPc6	žánr
domácího	domácí	k2eAgMnSc2d1	domácí
i	i	k8xC	i
světového	světový	k2eAgInSc2d1	světový
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
jevištních	jevištní	k2eAgFnPc2d1	jevištní
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
filmových	filmový	k2eAgFnPc2d1	filmová
a	a	k8xC	a
televizních	televizní	k2eAgFnPc2d1	televizní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
se	s	k7c7	s
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
i	i	k8xC	i
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
režiséry	režisér	k1gMnPc7	režisér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
i	i	k9	i
v	v	k7c6	v
maďarské	maďarský	k2eAgFnSc6d1	maďarská
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
.	.	kIx.	.
</s>
