<p>
<s>
August	August	k1gMnSc1	August
Friedrich	Friedrich	k1gMnSc1	Friedrich
Piepenhagen	Piepenhagen	k1gInSc1	Piepenhagen
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
Soldin	Soldin	k2eAgInSc1d1	Soldin
u	u	k7c2	u
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Prusku	Prusko	k1gNnSc6	Prusko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Myślibórz	Myślibórz	k1gMnSc1	Myślibórz
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
Praha-Staré	Praha-Starý	k2eAgNnSc1d1	Praha-Staré
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
malíř-krajinář	malířrajinář	k1gMnSc1	malíř-krajinář
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
a	a	k8xC	a
tvořící	tvořící	k2eAgMnSc1d1	tvořící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
Bedřich	Bedřich	k1gMnSc1	Bedřich
Piepenhagen	Piepenhagen	k1gInSc4	Piepenhagen
byl	být	k5eAaImAgMnS	být
malířským	malířský	k2eAgMnSc7d1	malířský
samoukem	samouk	k1gMnSc7	samouk
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
skromných	skromný	k2eAgInPc2d1	skromný
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
vyučil	vyučit	k5eAaPmAgInS	vyučit
se	se	k3xPyFc4	se
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
knoflíkářem	knoflíkář	k1gMnSc7	knoflíkář
a	a	k8xC	a
prýmkařem	prýmkař	k1gMnSc7	prýmkař
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tovaryšském	tovaryšský	k2eAgNnSc6d1	tovaryšský
vandrování	vandrování	k1gNnSc6	vandrování
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
krajinné	krajinný	k2eAgFnSc2d1	krajinná
scenérie	scenérie	k1gFnSc2	scenérie
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
zapůsobily	zapůsobit	k5eAaPmAgInP	zapůsobit
mocným	mocný	k2eAgInSc7d1	mocný
dojmem	dojem	k1gInSc7	dojem
a	a	k8xC	a
přivedly	přivést	k5eAaPmAgFnP	přivést
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
autodidaktickým	autodidaktický	k2eAgInPc3d1	autodidaktický
malířským	malířský	k2eAgInPc3d1	malířský
pokusům	pokus	k1gInPc3	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
u	u	k7c2	u
krajináře	krajinář	k1gMnSc2	krajinář
Johanna	Johann	k1gMnSc2	Johann
Heinricha	Heinrich	k1gMnSc2	Heinrich
Wuesta	Wuest	k1gMnSc2	Wuest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
ho	on	k3xPp3gNnSc4	on
vandrování	vandrování	k1gNnSc1	vandrování
zavedlo	zavést	k5eAaPmAgNnS	zavést
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
zůstal	zůstat	k5eAaPmAgMnS	zůstat
natrvalo	natrvalo	k6eAd1	natrvalo
–	–	k?	–
pracoval	pracovat	k5eAaImAgMnS	pracovat
zde	zde	k6eAd1	zde
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
jako	jako	k8xC	jako
tovaryš	tovaryš	k1gMnSc1	tovaryš
prýmkaře	prýmkař	k1gMnSc2	prýmkař
a	a	k8xC	a
knoflíkáře	knoflíkář	k1gMnSc2	knoflíkář
Jana	Jan	k1gMnSc2	Jan
Risbittera	Risbitter	k1gMnSc2	Risbitter
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
vdovou	vdova	k1gFnSc7	vdova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
přejal	přejmout	k5eAaPmAgMnS	přejmout
i	i	k9	i
knoflíkářskou	knoflíkářský	k2eAgFnSc4d1	knoflíkářský
dílnu	dílna	k1gFnSc4	dílna
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
železných	železný	k2eAgNnPc2d1	železné
vrat	vrata	k1gNnPc2	vrata
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pražským	pražský	k2eAgNnSc7d1	Pražské
prostředím	prostředí	k1gNnSc7	prostředí
záhy	záhy	k6eAd1	záhy
srostl	srůst	k5eAaPmAgInS	srůst
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
a	a	k8xC	a
vyhledávaným	vyhledávaný	k2eAgMnSc7d1	vyhledávaný
výrobcem	výrobce	k1gMnSc7	výrobce
knoflíků	knoflík	k1gMnPc2	knoflík
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
zde	zde	k6eAd1	zde
však	však	k9	však
také	také	k9	také
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
s	s	k7c7	s
malováním	malování	k1gNnSc7	malování
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
malířem	malíř	k1gMnSc7	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
výstavy	výstava	k1gFnSc2	výstava
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
i	i	k9	i
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
měl	mít	k5eAaImAgInS	mít
co	co	k9	co
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uživil	uživit	k5eAaPmAgMnS	uživit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
jeho	jeho	k3xOp3gInSc4	jeho
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
umění	umění	k1gNnSc1	umění
jde	jít	k5eAaImIp3nS	jít
za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řemeslo	řemeslo	k1gNnSc1	řemeslo
ho	on	k3xPp3gMnSc4	on
nalézá	nalézat	k5eAaImIp3nS	nalézat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jeho	jeho	k3xOp3gFnPc4	jeho
dvě	dva	k4xCgFnPc4	dva
souběžné	souběžný	k2eAgFnPc4d1	souběžná
činnosti	činnost	k1gFnPc4	činnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
prodávaným	prodávaný	k2eAgMnSc7d1	prodávaný
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgNnPc2	jenž
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
namaloval	namalovat	k5eAaPmAgMnS	namalovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
a	a	k8xC	a
kupované	kupovaný	k2eAgFnPc1d1	kupovaná
<g/>
.	.	kIx.	.
</s>
<s>
Stýkal	stýkat	k5eAaImAgMnS	stýkat
se	se	k3xPyFc4	se
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
Josefem	Josef	k1gMnSc7	Josef
Navrátilem	Navrátil	k1gMnSc7	Navrátil
a	a	k8xC	a
rakouským	rakouský	k2eAgMnSc7d1	rakouský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Adalbertem	Adalbert	k1gMnSc7	Adalbert
Stifterem	Stifter	k1gMnSc7	Stifter
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
ho	on	k3xPp3gMnSc4	on
pojilo	pojit	k5eAaImAgNnS	pojit
romantické	romantický	k2eAgNnSc1d1	romantické
vidění	vidění	k1gNnSc1	vidění
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc6	on
–	–	k?	–
Charlotta	Charlotta	k1gFnSc1	Charlotta
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
a	a	k8xC	a
Louisa	Louisa	k?	Louisa
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
–	–	k?	–
zdědily	zdědit	k5eAaPmAgFnP	zdědit
jeho	jeho	k3xOp3gInSc4	jeho
malířský	malířský	k2eAgInSc4d1	malířský
talent	talent	k1gInSc4	talent
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k9	rovněž
malířkami-krajinářkami	malířkamirajinářka	k1gFnPc7	malířkami-krajinářka
<g/>
,	,	kIx,	,
pokračovatelkami	pokračovatelka	k1gFnPc7	pokračovatelka
jeho	jeho	k3xOp3gInSc2	jeho
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
statku	statek	k1gInSc6	statek
Jenerálka	jenerálka	k1gFnSc1	jenerálka
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
evangelickém	evangelický	k2eAgInSc6d1	evangelický
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
náhrobek	náhrobek	k1gInSc4	náhrobek
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sochař	sochař	k1gMnSc1	sochař
Tomáš	Tomáš	k1gMnSc1	Tomáš
Seidan	Seidan	k1gMnSc1	Seidan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
hrob	hrob	k1gInSc1	hrob
Augusta	August	k1gMnSc2	August
Piepenhagena	Piepenhagen	k1gMnSc2	Piepenhagen
i	i	k8xC	i
jeho	jeho	k3xOp3gFnPc2	jeho
dcer	dcera	k1gFnPc2	dcera
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Olšanech	Olšany	k1gInPc6	Olšany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Zaměřením	zaměření	k1gNnPc3	zaměření
byl	být	k5eAaImAgMnS	být
čistý	čistý	k2eAgMnSc1d1	čistý
malíř-krajinář	malířrajinář	k1gMnSc1	malíř-krajinář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
umění	umění	k1gNnSc3	umění
dopracoval	dopracovat	k5eAaPmAgMnS	dopracovat
jako	jako	k8xS	jako
autodidakt	autodidakt	k1gMnSc1	autodidakt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
krajiny	krajina	k1gFnPc1	krajina
<g/>
,	,	kIx,	,
romantické	romantický	k2eAgFnPc1d1	romantická
a	a	k8xC	a
náladové	náladový	k2eAgFnPc1d1	náladová
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
pražského	pražský	k2eAgNnSc2d1	Pražské
publika	publikum	k1gNnSc2	publikum
velmi	velmi	k6eAd1	velmi
žádané	žádaný	k2eAgNnSc1d1	žádané
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
nebyly	být	k5eNaImAgInP	být
ovšem	ovšem	k9	ovšem
příliš	příliš	k6eAd1	příliš
drahé	drahý	k2eAgInPc1d1	drahý
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgInP	stát
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
)	)	kIx)	)
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sběratelským	sběratelský	k2eAgInSc7d1	sběratelský
artiklem	artikl	k1gInSc7	artikl
<g/>
.	.	kIx.	.
</s>
<s>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
rovněž	rovněž	k9	rovněž
malíř	malíř	k1gMnSc1	malíř
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
ocenil	ocenit	k5eAaPmAgMnS	ocenit
jako	jako	k8xS	jako
výjimečného	výjimečný	k2eAgMnSc2d1	výjimečný
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
ho	on	k3xPp3gNnSc4	on
"	"	kIx"	"
<g/>
za	za	k7c4	za
nejpověstnějšího	pověstný	k2eAgMnSc4d3	pověstný
krajináře	krajinář	k1gMnSc4	krajinář
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgMnSc4	jaký
znám	znát	k5eAaImIp1nS	znát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
krajinářské	krajinářský	k2eAgNnSc1d1	krajinářské
dílo	dílo	k1gNnSc1	dílo
i	i	k8xC	i
dílo	dílo	k1gNnSc1	dílo
jeho	jeho	k3xOp3gFnPc2	jeho
dcer	dcera	k1gFnPc2	dcera
vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c4	v
malířsky	malířsky	k6eAd1	malířsky
atraktivní	atraktivní	k2eAgNnPc4d1	atraktivní
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
početné	početný	k2eAgFnSc3d1	početná
a	a	k8xC	a
bohaté	bohatý	k2eAgFnSc3d1	bohatá
tvorbě	tvorba	k1gFnSc3	tvorba
otcově	otcův	k2eAgFnSc6d1	otcova
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
jen	jen	k6eAd1	jen
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
v	v	k7c6	v
otcově	otcův	k2eAgInSc6d1	otcův
odkazu	odkaz	k1gInSc6	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nostalgie	nostalgie	k1gFnSc1	nostalgie
<g/>
,	,	kIx,	,
citovost	citovost	k1gFnSc1	citovost
a	a	k8xC	a
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
obliba	obliba	k1gFnSc1	obliba
zřícenin	zřícenina	k1gFnPc2	zřícenina
a	a	k8xC	a
pitoreskních	pitoreskní	k2eAgFnPc2d1	pitoreskní
přírodních	přírodní	k2eAgFnPc2d1	přírodní
scenérií	scenérie	k1gFnPc2	scenérie
s	s	k7c7	s
roklemi	rokle	k1gFnPc7	rokle
<g/>
,	,	kIx,	,
vodopády	vodopád	k1gInPc7	vodopád
<g/>
,	,	kIx,	,
zasazených	zasazený	k2eAgFnPc2d1	zasazená
často	často	k6eAd1	často
do	do	k7c2	do
soumraku	soumrak	k1gInSc2	soumrak
či	či	k8xC	či
podvečerů	podvečer	k1gInPc2	podvečer
<g/>
,	,	kIx,	,
nokturna	nokturno	k1gNnSc2	nokturno
<g/>
,	,	kIx,	,
krajiny	krajina	k1gFnSc2	krajina
před	před	k7c7	před
bouří	bouř	k1gFnSc7	bouř
s	s	k7c7	s
osamělými	osamělý	k2eAgMnPc7d1	osamělý
poutníky	poutník	k1gMnPc7	poutník
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
rysy	rys	k1gInPc4	rys
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
nemalovaných	malovaný	k2eNgFnPc2d1	malovaný
podle	podle	k7c2	podle
reálu	reál	k1gInSc2	reál
<g/>
;	;	kIx,	;
z	z	k7c2	z
empirického	empirický	k2eAgInSc2d1	empirický
věcného	věcný	k2eAgInSc2d1	věcný
popisu	popis	k1gInSc2	popis
vycházel	vycházet	k5eAaImAgMnS	vycházet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
nejen	nejen	k6eAd1	nejen
velmi	velmi	k6eAd1	velmi
výkonný	výkonný	k2eAgMnSc1d1	výkonný
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zdatný	zdatný	k2eAgMnSc1d1	zdatný
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Specifikem	specifikon	k1gNnSc7	specifikon
jeho	jeho	k3xOp3gFnSc2	jeho
obchodní	obchodní	k2eAgFnSc2d1	obchodní
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
,	,	kIx,	,
svědčící	svědčící	k2eAgFnPc1d1	svědčící
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
podnikavosti	podnikavost	k1gFnSc6	podnikavost
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
drobné	drobný	k2eAgFnPc4d1	drobná
studie	studie	k1gFnPc4	studie
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
sestavené	sestavený	k2eAgInPc1d1	sestavený
na	na	k7c6	na
panelech	panel	k1gInPc6	panel
(	(	kIx(	(
<g/>
archy	archa	k1gFnPc1	archa
se	se	k3xPyFc4	se
počtem	počet	k1gInSc7	počet
od	od	k7c2	od
čtyř-pěti	čtyřět	k5eAaImF	čtyř-pět
do	do	k7c2	do
patnácti-dvaceti	patnáctivacet	k5eAaPmF	patnácti-dvacet
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
rozměrů	rozměr	k1gInPc2	rozměr
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
vzorníky	vzorník	k1gInPc1	vzorník
knoflíků	knoflík	k1gMnPc2	knoflík
<g/>
)	)	kIx)	)
předkládal	předkládat	k5eAaImAgMnS	předkládat
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
rozesílal	rozesílat	k5eAaImAgMnS	rozesílat
je	on	k3xPp3gMnPc4	on
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
mohli	moct	k5eAaImAgMnP	moct
vybrat	vybrat	k5eAaPmF	vybrat
motiv	motiv	k1gInSc4	motiv
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
budoucího	budoucí	k2eAgInSc2d1	budoucí
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstava	výstava	k1gFnSc1	výstava
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
Augusta	August	k1gMnSc2	August
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Piepenhagena	Piepenhagen	k1gMnSc2	Piepenhagen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
dcer	dcera	k1gFnPc2	dcera
Louisy	louis	k1gInPc1	louis
a	a	k8xC	a
Charlotty	Charlotta	k1gFnPc1	Charlotta
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
soubornou	souborný	k2eAgFnSc7d1	souborná
výstavou	výstava	k1gFnSc7	výstava
v	v	k7c6	v
Jiřském	jiřský	k2eAgInSc6d1	jiřský
klášteře	klášter	k1gInSc6	klášter
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
díla	dílo	k1gNnSc2	dílo
Augusta	August	k1gMnSc2	August
Piepenhagena	Piepenhagen	k1gMnSc2	Piepenhagen
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Salmovském	Salmovský	k2eAgInSc6d1	Salmovský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
expozice	expozice	k1gFnSc2	expozice
Umění	umění	k1gNnSc1	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
klasicismu	klasicismus	k1gInSc2	klasicismus
k	k	k7c3	k
romantismu	romantismus	k1gInSc3	romantismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
se	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
Risbitterovou	Risbitterův	k2eAgFnSc7d1	Risbitterův
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
Augusta	August	k1gMnSc2	August
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Piepenhagena	Piepenhagen	k1gMnSc2	Piepenhagen
značně	značně	k6eAd1	značně
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
dokladuje	dokladovat	k5eAaImIp3nS	dokladovat
policejní	policejní	k2eAgFnSc1d1	policejní
přihláška	přihláška	k1gFnSc1	přihláška
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželi	manžel	k1gMnPc7	manžel
Augustem	August	k1gMnSc7	August
a	a	k8xC	a
Katharinou	Katharina	k1gMnSc7	Katharina
Piepenhagenovými	Piepenhagenová	k1gFnPc7	Piepenhagenová
žily	žít	k5eAaImAgFnP	žít
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
domácnosti	domácnost	k1gFnSc6	domácnost
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
Kathariny	Katharin	k1gInPc1	Katharin
Piepenhagenové	Piepenhagenová	k1gFnSc2	Piepenhagenová
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
Johann	Johann	k1gInSc1	Johann
-	-	kIx~	-
"	"	kIx"	"
<g/>
Stieftochter	Stieftochter	k1gInSc1	Stieftochter
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Stiefsohn	Stiefsohn	k1gNnSc1	Stiefsohn
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
společné	společný	k2eAgFnPc4d1	společná
dcery	dcera	k1gFnPc4	dcera
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
Charlotta	Charlotta	k1gFnSc1	Charlotta
a	a	k8xC	a
Louisa	Louisa	k?	Louisa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
zapsány	zapsán	k2eAgFnPc1d1	zapsána
čtyři	čtyři	k4xCgFnPc1	čtyři
další	další	k2eAgFnPc1d1	další
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
příbuzenského	příbuzenský	k2eAgInSc2d1	příbuzenský
vztahu	vztah	k1gInSc2	vztah
(	(	kIx(	(
<g/>
pomocnice	pomocnice	k1gFnSc1	pomocnice
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
August	August	k1gMnSc1	August
Piepenhagen	Piepenhagen	k1gInSc1	Piepenhagen
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Macková	Macková	k1gFnSc1	Macková
O.	O.	kA	O.
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
Piepenhagen	Piepenhagen	k1gInSc1	Piepenhagen
<g/>
,	,	kIx,	,
NG	NG	kA	NG
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
</s>
</p>
<p>
<s>
Macková	Macková	k1gFnSc1	Macková
O.	O.	kA	O.
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
Piepenhagen	Piepenhagen	k1gInSc1	Piepenhagen
<g/>
,	,	kIx,	,
Umění	umění	k1gNnSc1	umění
10	[number]	k4	10
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
s.	s.	k?	s.
135-153	[number]	k4	135-153
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
August	August	k1gMnSc1	August
Bedřich	Bedřich	k1gMnSc1	Bedřich
Piepenhagen	Piepenhagen	k1gInSc4	Piepenhagen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
August	August	k1gMnSc1	August
Bedřich	Bedřich	k1gMnSc1	Bedřich
Piepenhagen	Piepenhagen	k1gInSc4	Piepenhagen
</s>
</p>
<p>
<s>
http://artalkweb.wordpress.com/2009/11/05/tz-piepenhagenovi/	[url]	k4	http://artalkweb.wordpress.com/2009/11/05/tz-piepenhagenovi/
</s>
</p>
<p>
<s>
http://www.pozitivni-noviny.cz/cz/clanek-2009120038	[url]	k4	http://www.pozitivni-noviny.cz/cz/clanek-2009120038
</s>
</p>
<p>
<s>
http://ekolist.cz/cz/kultura/clanky/august-bedrich-piepenhagen-chvala-zpustle-krajiny	[url]	k1gFnPc1	http://ekolist.cz/cz/kultura/clanky/august-bedrich-piepenhagen-chvala-zpustle-krajiny
</s>
</p>
<p>
<s>
http://www.galeriekroupa.cz/cs/m-434-piepenhagen-august-bedrich-1791-1868/	[url]	k4	http://www.galeriekroupa.cz/cs/m-434-piepenhagen-august-bedrich-1791-1868/
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
,	,	kIx,	,
Charlotta	Charlotta	k1gFnSc1	Charlotta
a	a	k8xC	a
Louisa	Louisa	k?	Louisa
Piepenhagenovi	Piepenhagenův	k2eAgMnPc1d1	Piepenhagenův
</s>
</p>
<p>
<s>
Hrob	hrob	k1gInSc1	hrob
Augusta	August	k1gMnSc2	August
Friedricha	Friedrich	k1gMnSc2	Friedrich
Pipenhagena	Pipenhagen	k1gMnSc2	Pipenhagen
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Piepenhagen	Piepenhagen	k1gInSc1	Piepenhagen
August	August	k1gMnSc1	August
Bedřich	Bedřich	k1gMnSc1	Bedřich
</s>
</p>
