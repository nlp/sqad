<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
ho	on	k3xPp3gNnSc4	on
vandrování	vandrování	k1gNnSc1	vandrování
zavedlo	zavést	k5eAaPmAgNnS	zavést
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
zůstal	zůstat	k5eAaPmAgMnS	zůstat
natrvalo	natrvalo	k6eAd1	natrvalo
–	–	k?	–
pracoval	pracovat	k5eAaImAgMnS	pracovat
zde	zde	k6eAd1	zde
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
jako	jako	k8xC	jako
tovaryš	tovaryš	k1gMnSc1	tovaryš
prýmkaře	prýmkař	k1gMnSc2	prýmkař
a	a	k8xC	a
knoflíkáře	knoflíkář	k1gMnSc2	knoflíkář
Jana	Jan	k1gMnSc2	Jan
Risbittera	Risbitter	k1gMnSc2	Risbitter
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
vdovou	vdova	k1gFnSc7	vdova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
přejal	přejmout	k5eAaPmAgMnS	přejmout
i	i	k9	i
knoflíkářskou	knoflíkářský	k2eAgFnSc4d1	knoflíkářský
dílnu	dílna	k1gFnSc4	dílna
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
železných	železný	k2eAgNnPc2d1	železné
vrat	vrata	k1gNnPc2	vrata
<g/>
.	.	kIx.	.
</s>
