<s>
Parohy	paroh	k1gInPc1	paroh
nebo	nebo	k8xC	nebo
paroží	paroží	k1gNnSc1	paroží
jsou	být	k5eAaImIp3nP	být
kostěné	kostěný	k2eAgInPc1d1	kostěný
výrůstky	výrůstek	k1gInPc1	výrůstek
bez	bez	k7c2	bez
dutin	dutina	k1gFnPc2	dutina
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
většiny	většina	k1gFnSc2	většina
samců	samec	k1gMnPc2	samec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
sobů	sob	k1gMnPc2	sob
nosí	nosit	k5eAaImIp3nS	nosit
parohy	paroh	k1gInPc1	paroh
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
výrazným	výrazný	k2eAgInSc7d1	výrazný
znakem	znak	k1gInSc7	znak
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc4	paroh
z	z	k7c2	z
jelenovitých	jelenovitý	k2eAgMnPc2d1	jelenovitý
savců	savec	k1gMnPc2	savec
nemá	mít	k5eNaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
srnčík	srnčík	k1gMnSc1	srnčík
(	(	kIx(	(
<g/>
Hydropotes	Hydropotes	k1gMnSc1	Hydropotes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
jelenovitých	jelenovití	k1gMnPc2	jelenovití
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
specifická	specifický	k2eAgNnPc4d1	specifické
paroží	paroží	k1gNnSc4	paroží
<g/>
,	,	kIx,	,
od	od	k7c2	od
největších	veliký	k2eAgMnPc2d3	veliký
u	u	k7c2	u
losů	los	k1gInPc2	los
až	až	k9	až
po	po	k7c4	po
nejmenší	malý	k2eAgFnPc4d3	nejmenší
u	u	k7c2	u
pudu	pud	k1gInSc2	pud
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
paroží	paroží	k1gNnSc2	paroží
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
určující	určující	k2eAgMnSc1d1	určující
pro	pro	k7c4	pro
sociální	sociální	k2eAgNnSc4d1	sociální
zařazení	zařazení	k1gNnSc4	zařazení
mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
vizuálního	vizuální	k2eAgNnSc2d1	vizuální
předvádění	předvádění	k1gNnSc2	předvádění
své	svůj	k3xOyFgFnSc2	svůj
síly	síla	k1gFnSc2	síla
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
zbraň	zbraň	k1gFnSc4	zbraň
k	k	k7c3	k
soubojům	souboj	k1gInPc3	souboj
se	s	k7c7	s
soky	sok	k1gMnPc7	sok
v	v	k7c6	v
období	období	k1gNnSc6	období
říje	říje	k1gFnSc2	říje
a	a	k8xC	a
hrozba	hrozba	k1gFnSc1	hrozba
přirozeným	přirozený	k2eAgMnPc3d1	přirozený
predátorům	predátor	k1gMnPc3	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc1	paroh
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
jelenovitým	jelenovití	k1gMnPc3	jelenovití
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
po	po	k7c6	po
říji	říje	k1gFnSc6	říje
je	on	k3xPp3gInPc4	on
shazují	shazovat	k5eAaImIp3nP	shazovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
periodicky	periodicky	k6eAd1	periodicky
probíhající	probíhající	k2eAgInSc1d1	probíhající
cyklus	cyklus	k1gInSc1	cyklus
nemá	mít	k5eNaImIp3nS	mít
u	u	k7c2	u
vývojově	vývojově	k6eAd1	vývojově
výše	vysoce	k6eAd2	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
živočichů	živočich	k1gMnPc2	živočich
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
růst	růst	k1gInSc1	růst
i	i	k8xC	i
shazování	shazování	k1gNnSc1	shazování
je	být	k5eAaImIp3nS	být
řízeno	řídit	k5eAaImNgNnS	řídit
hormonálními	hormonální	k2eAgFnPc7d1	hormonální
látkami	látka	k1gFnPc7	látka
produkovanými	produkovaný	k2eAgFnPc7d1	produkovaná
žlázami	žláza	k1gFnPc7	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
zralost	zralost	k1gFnSc4	zralost
<g/>
.	.	kIx.	.
</s>
<s>
Produkci	produkce	k1gFnSc4	produkce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
hormonů	hormon	k1gInPc2	hormon
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pro	pro	k7c4	pro
Střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
je	být	k5eAaImIp3nS	být
nejtypičtějším	typický	k2eAgNnSc7d3	nejtypičtější
zvířetem	zvíře	k1gNnSc7	zvíře
s	s	k7c7	s
parohy	paroh	k1gInPc7	paroh
jelen	jelena	k1gFnPc2	jelena
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
další	další	k2eAgInSc1d1	další
popis	popis	k1gInSc1	popis
parohů	paroh	k1gInPc2	paroh
vztažen	vztažen	k2eAgMnSc1d1	vztažen
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
nových	nový	k2eAgInPc2d1	nový
parohů	paroh	k1gInPc2	paroh
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nasazování	nasazování	k1gNnSc1	nasazování
<g/>
,	,	kIx,	,
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
hlavně	hlavně	k9	hlavně
dostatek	dostatek	k1gInSc4	dostatek
růstového	růstový	k2eAgInSc2d1	růstový
hormonu	hormon	k1gInSc2	hormon
somatropinu	somatropin	k1gInSc2	somatropin
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
v	v	k7c6	v
hypofýze	hypofýza	k1gFnSc6	hypofýza
a	a	k8xC	a
současný	současný	k2eAgInSc4d1	současný
útlum	útlum	k1gInSc4	útlum
hormonu	hormon	k1gInSc2	hormon
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
aktivity	aktivita	k1gFnSc2	aktivita
testosteronu	testosteron	k1gInSc2	testosteron
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
<g/>
,	,	kIx,	,
zdatnějších	zdatný	k2eAgMnPc2d2	zdatnější
jelenů	jelen	k1gMnPc2	jelen
tvorba	tvorba	k1gFnSc1	tvorba
paroží	paroží	k1gNnSc4	paroží
začíná	začínat	k5eAaImIp3nS	začínat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
u	u	k7c2	u
mladších	mladý	k2eAgInPc2d2	mladší
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
parohy	paroh	k1gInPc1	paroh
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
výběžků	výběžek	k1gInPc2	výběžek
na	na	k7c4	na
čelní	čelní	k2eAgFnPc4d1	čelní
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
pučnic	pučnice	k1gFnPc2	pučnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
na	na	k7c6	na
pučnicích	pučnice	k1gFnPc6	pučnice
začne	začít	k5eAaPmIp3nS	začít
zmnožovat	zmnožovat	k5eAaImF	zmnožovat
pojivová	pojivový	k2eAgFnSc1d1	pojivová
parožní	parožní	k2eAgFnSc1d1	parožní
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
nových	nový	k2eAgInPc2d1	nový
parohů	paroh	k1gInPc2	paroh
<g/>
,	,	kIx,	,
a	a	k8xC	a
kosti	kost	k1gFnPc1	kost
parohů	paroh	k1gInPc2	paroh
začnou	začít	k5eAaPmIp3nP	začít
pomalu	pomalu	k6eAd1	pomalu
narůstat	narůstat	k5eAaImF	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
parohy	paroh	k1gInPc1	paroh
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
panty	pant	k1gInPc1	pant
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obaleny	obalen	k2eAgInPc1d1	obalen
jemnou	jemný	k2eAgFnSc7d1	jemná
osrstěnou	osrstěný	k2eAgFnSc7d1	osrstěná
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
protkaná	protkaný	k2eAgFnSc1d1	protkaná
nervy	nerv	k1gInPc7	nerv
a	a	k8xC	a
systémem	systém	k1gInSc7	systém
cév	céva	k1gFnPc2	céva
přivádějících	přivádějící	k2eAgFnPc2d1	přivádějící
potřebné	potřebný	k2eAgFnPc4d1	potřebná
živiny	živina	k1gFnPc4	živina
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
parohů	paroh	k1gInPc2	paroh
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
mnoha	mnoho	k4c7	mnoho
pachovými	pachový	k2eAgInPc7d1	pachový
a	a	k8xC	a
mazovými	mazový	k2eAgFnPc7d1	mazová
žlázkami	žlázka	k1gFnPc7	žlázka
s	s	k7c7	s
feromony	feromon	k1gInPc7	feromon
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
kůže	kůže	k1gFnSc1	kůže
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
stadiu	stadion	k1gNnSc3	stadion
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
parohy	paroh	k1gInPc1	paroh
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
lýčí	lýčí	k1gNnSc6	lýčí
<g/>
.	.	kIx.	.
</s>
<s>
Otisky	otisk	k1gInPc1	otisk
hlavních	hlavní	k2eAgFnPc2d1	hlavní
cév	céva	k1gFnPc2	céva
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
trvale	trvale	k6eAd1	trvale
vytlačeny	vytlačit	k5eAaPmNgInP	vytlačit
do	do	k7c2	do
parohů	paroh	k1gInPc2	paroh
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
podélných	podélný	k2eAgFnPc2d1	podélná
rýh	rýha	k1gFnPc2	rýha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nasazování	nasazování	k1gNnSc1	nasazování
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
růstu	růst	k1gInSc2	růst
paroží	paroží	k1gNnSc2	paroží
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
jeleni	jelen	k1gMnPc1	jelen
poměrně	poměrně	k6eAd1	poměrně
skrytě	skrytě	k6eAd1	skrytě
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc1	paroh
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
rohů	roh	k1gInPc2	roh
<g/>
,	,	kIx,	,
přirůstají	přirůstat	k5eAaImIp3nP	přirůstat
na	na	k7c6	na
špici	špice	k1gFnSc6	špice
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohou	moct	k5eNaImIp3nP	moct
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
bázi	báze	k1gFnSc6	báze
dodatečně	dodatečně	k6eAd1	dodatečně
ztloustnout	ztloustnout	k5eAaPmF	ztloustnout
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zdatných	zdatný	k2eAgMnPc2d1	zdatný
jelenů	jelen	k1gMnPc2	jelen
mohou	moct	k5eAaImIp3nP	moct
denně	denně	k6eAd1	denně
v	v	k7c6	v
období	období	k1gNnSc6	období
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
intenzity	intenzita	k1gFnSc2	intenzita
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
přirůst	přirůst	k5eAaPmF	přirůst
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hmotnost	hmotnost	k1gFnSc1	hmotnost
může	moct	k5eAaImIp3nS	moct
denně	denně	k6eAd1	denně
stoupnout	stoupnout	k5eAaPmF	stoupnout
až	až	k9	až
o	o	k7c4	o
150	[number]	k4	150
g.	g.	k?	g.
Měkká	měkký	k2eAgFnSc1d1	měkká
tkáň	tkáň	k1gFnSc1	tkáň
nových	nový	k2eAgInPc2d1	nový
parohů	paroh	k1gInPc2	paroh
se	se	k3xPyFc4	se
během	během	k7c2	během
růstu	růst	k1gInSc2	růst
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
spodu	spod	k1gInSc2	spod
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
chrupavku	chrupavka	k1gFnSc4	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
inkrustována	inkrustován	k2eAgFnSc1d1	inkrustován
minerálními	minerální	k2eAgFnPc7d1	minerální
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
v	v	k7c4	v
osteoblasty	osteoblast	k1gInPc4	osteoblast
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c4	v
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
živé	živý	k2eAgInPc1d1	živý
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jen	jen	k9	jen
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
cm	cm	kA	cm
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
koncích	konec	k1gInPc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
růstu	růst	k1gInSc2	růst
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
paroží	paroží	k1gNnSc2	paroží
bílkoviny	bílkovina	k1gFnSc2	bílkovina
<g/>
,	,	kIx,	,
asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kostnatění	kostnatění	k1gNnSc1	kostnatění
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
ukládáním	ukládání	k1gNnSc7	ukládání
vápenných	vápenný	k2eAgFnPc2d1	vápenná
a	a	k8xC	a
fosforečných	fosforečný	k2eAgFnPc2d1	fosforečná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nasazování	nasazování	k1gNnSc2	nasazování
paroží	paroží	k1gNnSc2	paroží
si	se	k3xPyFc3	se
potřebný	potřebný	k2eAgInSc4d1	potřebný
přísun	přísun	k1gInSc4	přísun
bílkovin	bílkovina	k1gFnPc2	bílkovina
zvířata	zvíře	k1gNnPc4	zvíře
průběžně	průběžně	k6eAd1	průběžně
zajišťuji	zajišťovat	k5eAaImIp1nS	zajišťovat
denní	denní	k2eAgFnSc7d1	denní
pastvou	pastva	k1gFnSc7	pastva
<g/>
,	,	kIx,	,
dospělý	dospělý	k2eAgMnSc1d1	dospělý
jelen	jelen	k1gMnSc1	jelen
spase	spást	k5eAaPmIp3nS	spást
denně	denně	k6eAd1	denně
až	až	k9	až
25	[number]	k4	25
kg	kg	kA	kg
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
spotřeba	spotřeba	k1gFnSc1	spotřeba
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
až	až	k9	až
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
přechodným	přechodný	k2eAgNnSc7d1	přechodné
odbouráním	odbourání	k1gNnSc7	odbourání
minerálů	minerál	k1gInPc2	minerál
z	z	k7c2	z
ostatní	ostatní	k2eAgFnSc2d1	ostatní
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
žeber	žebro	k1gNnPc2	žebro
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
zvedat	zvedat	k5eAaImF	zvedat
aktivita	aktivita	k1gFnSc1	aktivita
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
žláz	žláza	k1gFnPc2	žláza
a	a	k8xC	a
následně	následně	k6eAd1	následně
klesá	klesat	k5eAaImIp3nS	klesat
hladina	hladina	k1gFnSc1	hladina
růstových	růstový	k2eAgInPc2d1	růstový
hormonů	hormon	k1gInPc2	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc1	paroh
přestanou	přestat	k5eAaPmIp3nP	přestat
růst	růst	k1gInSc4	růst
a	a	k8xC	a
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc2	jejich
zkostnatění	zkostnatění	k1gNnSc2	zkostnatění
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
nenabývají	nabývat	k5eNaImIp3nP	nabývat
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
<g/>
,	,	kIx,	,
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
se	se	k3xPyFc4	se
následkem	následkem	k7c2	následkem
ukládání	ukládání	k1gNnSc2	ukládání
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
jejich	jejich	k3xOp3gFnSc4	jejich
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
parohů	paroh	k1gInPc2	paroh
vzniká	vznikat	k5eAaImIp3nS	vznikat
kost	kost	k1gFnSc1	kost
mnohem	mnohem	k6eAd1	mnohem
kompaktnější	kompaktní	k2eAgFnSc1d2	kompaktnější
<g/>
,	,	kIx,	,
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
než	než	k8xS	než
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kostní	kostní	k2eAgFnSc1d1	kostní
tkáň	tkáň	k1gFnSc1	tkáň
více	hodně	k6eAd2	hodně
pórovitá	pórovitý	k2eAgFnSc1d1	pórovitá
<g/>
.	.	kIx.	.
</s>
<s>
Zkostnatělé	zkostnatělý	k2eAgInPc1d1	zkostnatělý
parohy	paroh	k1gInPc1	paroh
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
vápník	vápník	k1gInSc1	vápník
a	a	k8xC	a
pak	pak	k6eAd1	pak
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
fosfor	fosfor	k1gInSc1	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Bílkovinových	Bílkovinových	k2eAgFnSc7d1	Bílkovinových
substancí	substance	k1gFnSc7	substance
bývá	bývat	k5eAaImIp3nS	bývat
asi	asi	k9	asi
35	[number]	k4	35
%	%	kIx~	%
a	a	k8xC	a
podíl	podíl	k1gInSc4	podíl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
zpevňování	zpevňování	k1gNnSc2	zpevňování
paroží	paroží	k1gNnSc2	paroží
jejich	jejich	k3xOp3gInSc1	jejich
kožní	kožní	k2eAgInSc1d1	kožní
obal	obal	k1gInSc1	obal
odumírá	odumírat	k5eAaImIp3nS	odumírat
<g/>
,	,	kIx,	,
usychá	usychat	k5eAaImIp3nS	usychat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
otíráním	otírání	k1gNnSc7	otírání
o	o	k7c4	o
větve	větev	k1gFnPc4	větev
a	a	k8xC	a
kmeny	kmen	k1gInPc4	kmen
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
počínání	počínání	k1gNnSc1	počínání
zvěře	zvěř	k1gFnSc2	zvěř
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vytloukání	vytloukání	k1gNnSc1	vytloukání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vytloukáním	vytloukání	k1gNnSc7	vytloukání
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
začínají	začínat	k5eAaImIp3nP	začínat
původně	původně	k6eAd1	původně
světlé	světlý	k2eAgInPc1d1	světlý
parohy	paroh	k1gInPc1	paroh
přebarvovat	přebarvovat	k5eAaImF	přebarvovat
<g/>
.	.	kIx.	.
</s>
<s>
Nastalo	nastat	k5eAaPmAgNnS	nastat
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
parohy	paroh	k1gInPc1	paroh
přeměnily	přeměnit	k5eAaPmAgInP	přeměnit
v	v	k7c4	v
necitlivou	citlivý	k2eNgFnSc4d1	necitlivá
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
října	říjen	k1gInSc2	říjen
začíná	začínat	k5eAaImIp3nS	začínat
pomalu	pomalu	k6eAd1	pomalu
množství	množství	k1gNnSc1	množství
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
hormonu	hormon	k1gInSc2	hormon
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
růstového	růstový	k2eAgMnSc4d1	růstový
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
shoz	shoz	k1gInSc4	shoz
parohů	paroh	k1gInPc2	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c4	pod
kůži	kůže	k1gFnSc4	kůže
parohu	paroh	k1gInSc2	paroh
v	v	k7c6	v
kosti	kost	k1gFnSc6	kost
pučnice	pučnice	k1gFnSc2	pučnice
je	být	k5eAaImIp3nS	být
resorbována	resorbován	k2eAgFnSc1d1	resorbován
vrstva	vrstva	k1gFnSc1	vrstva
buněk	buňka	k1gFnPc2	buňka
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ztratí	ztratit	k5eAaPmIp3nP	ztratit
parohy	paroh	k1gInPc1	paroh
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
lebkou	lebka	k1gFnSc7	lebka
a	a	k8xC	a
paroh	paroh	k1gInSc1	paroh
odpadá	odpadat	k5eAaImIp3nS	odpadat
buď	buď	k8xC	buď
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vahou	váha	k1gFnSc7	váha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odpadu	odpad	k1gInSc6	odpad
jednoho	jeden	k4xCgInSc2	jeden
se	se	k3xPyFc4	se
zvíře	zvíře	k1gNnSc1	zvíře
snaží	snažit	k5eAaImIp3nS	snažit
urychleně	urychleně	k6eAd1	urychleně
druhý	druhý	k4xOgInSc4	druhý
shodit	shodit	k5eAaPmF	shodit
samo	sám	k3xTgNnSc1	sám
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
hlavy	hlava	k1gFnSc2	hlava
o	o	k7c4	o
hodně	hodně	k6eAd1	hodně
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
;	;	kIx,	;
mohutné	mohutný	k2eAgNnSc1d1	mohutné
paroží	paroží	k1gNnSc1	paroží
jelena	jelen	k1gMnSc2	jelen
váží	vážit	k5eAaImIp3nS	vážit
až	až	k9	až
13	[number]	k4	13
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
pouze	pouze	k6eAd1	pouze
části	část	k1gFnPc1	část
parohů	paroh	k1gInPc2	paroh
ukryté	ukrytý	k2eAgFnPc1d1	ukrytá
pod	pod	k7c7	pod
kůží	kůže	k1gFnSc7	kůže
které	který	k3yQgFnSc2	který
vyrůstaly	vyrůstat	k5eAaImAgFnP	vyrůstat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
pučnice	pučnice	k1gFnSc2	pučnice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pečeť	pečeť	k1gFnSc1	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
parohy	paroh	k1gInPc1	paroh
začnou	začít	k5eAaPmIp3nP	začít
po	po	k7c6	po
shozu	shoz	k1gInSc6	shoz
vyrůstat	vyrůstat	k5eAaImF	vyrůstat
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
až	až	k9	až
deseti	deset	k4xCc6	deset
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
růstem	růst	k1gInSc7	růst
věku	věk	k1gInSc2	věk
jelenovitých	jelenovitý	k2eAgMnPc2d1	jelenovitý
samců	samec	k1gMnPc2	samec
jim	on	k3xPp3gMnPc3	on
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
i	i	k9	i
mohutnější	mohutný	k2eAgInPc4d2	mohutnější
parohy	paroh	k1gInPc4	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
nezáleží	záležet	k5eNaImIp3nS	záležet
jen	jen	k9	jen
na	na	k7c4	na
stáří	stáří	k1gNnSc4	stáří
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
celkovém	celkový	k2eAgInSc6d1	celkový
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
na	na	k7c6	na
vhodnosti	vhodnost	k1gFnSc6	vhodnost
a	a	k8xC	a
množství	množství	k1gNnSc6	množství
dostupného	dostupný	k2eAgNnSc2d1	dostupné
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přestárlých	přestárlý	k2eAgMnPc2d1	přestárlý
jedinců	jedinec	k1gMnPc2	jedinec
však	však	k9	však
schopnost	schopnost	k1gFnSc1	schopnost
vyprodukovat	vyprodukovat	k5eAaPmF	vyprodukovat
množství	množství	k1gNnSc4	množství
parohoviny	parohovina	k1gFnSc2	parohovina
bývá	bývat	k5eAaImIp3nS	bývat
již	již	k6eAd1	již
snížena	snížit	k5eAaPmNgFnS	snížit
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
jejich	jejich	k3xOp3gInPc2	jejich
parohů	paroh	k1gInPc2	paroh
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
paroží	paroží	k1gNnSc2	paroží
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skupin	skupina	k1gFnPc2	skupina
zvěře	zvěř	k1gFnSc2	zvěř
přibližně	přibližně	k6eAd1	přibližně
určit	určit	k5eAaPmF	určit
i	i	k9	i
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
u	u	k7c2	u
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
populací	populace	k1gFnPc2	populace
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
mnohdy	mnohdy	k6eAd1	mnohdy
nesouměřitelné	souměřitelný	k2eNgNnSc1d1	nesouměřitelné
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
paroží	paroží	k1gNnSc1	paroží
má	mít	k5eAaImIp3nS	mít
los	los	k1gInSc1	los
aljašský	aljašský	k2eAgInSc1d1	aljašský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gMnSc1	alces
americanus	americanus	k1gMnSc1	americanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mívá	mívat	k5eAaImIp3nS	mívat
parohy	paroh	k1gInPc4	paroh
o	o	k7c4	o
rozpětí	rozpětí	k1gNnSc4	rozpětí
přes	přes	k7c4	přes
1,6	[number]	k4	1,6
m	m	kA	m
a	a	k8xC	a
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
asi	asi	k9	asi
25	[number]	k4	25
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Paroží	paroží	k1gNnSc1	paroží
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
významné	významný	k2eAgFnPc1d1	významná
lovecké	lovecký	k2eAgFnPc1d1	lovecká
trofeje	trofej	k1gFnPc1	trofej
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
poměřují	poměřovat	k5eAaImIp3nP	poměřovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
sestavovány	sestavován	k2eAgFnPc1d1	sestavována
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
hodnotitelské	hodnotitelský	k2eAgFnSc2d1	hodnotitelská
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
byly	být	k5eAaImAgFnP	být
posuzovány	posuzovat	k5eAaImNgFnP	posuzovat
trofeje	trofej	k1gFnPc1	trofej
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
prodělávaly	prodělávat	k5eAaImAgFnP	prodělávat
různé	různý	k2eAgFnPc4d1	různá
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
platí	platit	k5eAaImIp3nS	platit
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sestavila	sestavit	k5eAaPmAgFnS	sestavit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
zvěře	zvěř	k1gFnSc2	zvěř
(	(	kIx(	(
<g/>
CIC	cic	k1gInSc1	cic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
mohutnost	mohutnost	k1gFnSc4	mohutnost
a	a	k8xC	a
vzhled	vzhled	k1gInSc4	vzhled
paroží	paroží	k1gNnSc2	paroží
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
jejich	jejich	k3xOp3gFnSc1	jejich
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
se	se	k3xPyFc4	se
např.	např.	kA	např.
počet	počet	k1gInSc4	počet
výsad	výsada	k1gFnPc2	výsada
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
lodyh	lodyha	k1gFnPc2	lodyha
<g/>
,	,	kIx,	,
očníků	očník	k1gInPc2	očník
a	a	k8xC	a
opěráků	opěrák	k1gInPc2	opěrák
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
celého	celý	k2eAgNnSc2d1	celé
paroží	paroží	k1gNnSc2	paroží
atd.	atd.	kA	atd.
Už	už	k6eAd1	už
v	v	k7c6	v
pradávných	pradávný	k2eAgFnPc6d1	pradávná
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgInP	sloužit
parohy	paroh	k1gInPc4	paroh
jelenovitých	jelenovití	k1gMnPc2	jelenovití
jako	jako	k8xC	jako
suroviny	surovina	k1gFnSc2	surovina
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
léků	lék	k1gInPc2	lék
proti	proti	k7c3	proti
rozličným	rozličný	k2eAgInPc3d1	rozličný
neduhům	neduh	k1gInPc3	neduh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
léky	lék	k1gInPc1	lék
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
paroží	paroží	k1gNnSc2	paroží
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gInSc2	on
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc1	zvíře
loví	lovit	k5eAaImIp3nP	lovit
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
panty	pant	k1gInPc1	pant
s	s	k7c7	s
kouskem	kousek	k1gInSc7	kousek
lebky	lebka	k1gFnSc2	lebka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lobární	lobární	k2eAgInPc1d1	lobární
panty	pant	k1gInPc1	pant
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
v	v	k7c6	v
příslušném	příslušný	k2eAgNnSc6d1	příslušné
růstovém	růstový	k2eAgNnSc6d1	růstové
období	období	k1gNnSc6	období
uřezávají	uřezávat	k5eAaImIp3nP	uřezávat
parohy	paroh	k1gInPc1	paroh
zvířatům	zvíře	k1gNnPc3	zvíře
chovaným	chovaný	k2eAgFnPc3d1	chovaná
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
vývozci	vývozce	k1gMnPc7	vývozce
pantů	pant	k1gInPc2	pant
jsou	být	k5eAaImIp3nP	být
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Léčebné	léčebný	k2eAgInPc1d1	léčebný
účinky	účinek	k1gInPc1	účinek
preparátů	preparát	k1gInPc2	preparát
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
medicíně	medicína	k1gFnSc6	medicína
všeobecně	všeobecně	k6eAd1	všeobecně
akceptovány	akceptovat	k5eAaBmNgFnP	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc1	paroh
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
stálost	stálost	k1gFnSc4	stálost
materiálu	materiál	k1gInSc2	materiál
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
využívány	využívat	k5eAaImNgInP	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
věcí	věc	k1gFnPc2	věc
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
loveckých	lovecký	k2eAgInPc2d1	lovecký
interiérů	interiér	k1gInPc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Paroží	paroží	k1gNnSc1	paroží
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
užívá	užívat	k5eAaImIp3nS	užívat
na	na	k7c4	na
střenky	střenka	k1gFnPc4	střenka
loveckých	lovecký	k2eAgInPc2d1	lovecký
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
násady	násada	k1gFnPc1	násada
jídelních	jídelní	k2eAgInPc2d1	jídelní
příborů	příbor	k1gInPc2	příbor
<g/>
,	,	kIx,	,
na	na	k7c4	na
lovecké	lovecký	k2eAgFnPc4d1	lovecká
píšťalky	píšťalka	k1gFnPc4	píšťalka
a	a	k8xC	a
vábničky	vábnička	k1gFnPc4	vábnička
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
hlavice	hlavice	k1gFnSc1	hlavice
vycházkových	vycházkový	k2eAgFnPc2d1	vycházková
holí	hole	k1gFnPc2	hole
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyřezáván	vyřezávat	k5eAaImNgInS	vyřezávat
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
miniatur	miniatura	k1gFnPc2	miniatura
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
,	,	kIx,	,
spon	spona	k1gFnPc2	spona
<g/>
,	,	kIx,	,
broží	brož	k1gFnPc2	brož
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
přívěsků	přívěsek	k1gInPc2	přívěsek
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
obor	obor	k1gInSc1	obor
–	–	k?	–
řezba	řezba	k1gFnSc1	řezba
do	do	k7c2	do
paroží	paroží	k1gNnSc2	paroží
<g/>
.	.	kIx.	.
</s>
