<s>
Haná	Haná	k1gFnSc1
Mújás	Mújása	k1gFnPc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ח	ח	k?
מ	מ	k?
<g/>
,	,	kIx,
Chana	Chaen	k2eAgFnSc1d1
Mwais	Mwais	k1gFnSc1
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ح	ح	k?
م	م	k?
<g/>
,	,	kIx,
žil	žít	k5eAaImAgMnS
1913	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1981	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
izraelský	izraelský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
poslanec	poslanec	k1gMnSc1
Knesetu	Kneset	k1gInSc2
za	za	k7c4
stranu	strana	k1gFnSc4
Chadaš	Chadaš	k1gMnSc1
<g/>
.	.	kIx.
</s>