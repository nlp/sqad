<s>
Haná	Haná	k1gFnSc1
Mújás	Mújása	k1gFnPc2
</s>
<s>
Haná	Haná	k1gFnSc1
Mújásח	Mújásח	k1gFnSc2
מ	מ	k?
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Chadaš	Chadaš	k5eAaPmIp2nS
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1913	#num#	k4
RamaOsmanská	RamaOsmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1981	#num#	k4
Izrael	Izrael	k1gMnSc1
Izrael	Izrael	k1gMnSc1
Kneset	Kneset	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Haná	Haná	k1gFnSc1
Mújás	Mújása	k1gFnPc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ח	ח	k?
מ	מ	k?
<g/>
,	,	kIx,
Chana	Chaen	k2eAgFnSc1d1
Mwais	Mwais	k1gFnSc1
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ح	ح	k?
م	م	k?
<g/>
,	,	kIx,
žil	žít	k5eAaImAgMnS
1913	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1981	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
izraelský	izraelský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
poslanec	poslanec	k1gMnSc1
Knesetu	Kneset	k1gInSc2
za	za	k7c4
stranu	strana	k1gFnSc4
Chadaš	Chadaš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
obci	obec	k1gFnSc6
Rama	Ram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Akku	Akkus	k1gInSc6
a	a	k8xC
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Nazaretu	Nazaret	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Patřil	patřit	k5eAaImAgInS
do	do	k7c2
komunity	komunita	k1gFnSc2
izraelských	izraelský	k2eAgMnPc2d1
Arabů	Arab	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
byl	být	k5eAaImAgInS
členem	člen	k1gInSc7
samosprávy	samospráva	k1gFnSc2
v	v	k7c6
obci	obec	k1gFnSc6
Rama	Ram	k1gInSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
i	i	k9
jejím	její	k3xOp3gMnSc7
starostou	starosta	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedal	předsedat	k5eAaImAgMnS
Národnímu	národní	k2eAgInSc3d1
výboru	výbor	k1gInSc2
arabských	arabský	k2eAgFnPc2d1
místních	místní	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
izraelském	izraelský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
zasedl	zasednout	k5eAaPmAgMnS
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgMnPc2
šel	jít	k5eAaImAgMnS
za	za	k7c4
stranu	strana	k1gFnSc4
Chadaš	Chadaš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
členem	člen	k1gMnSc7
parlamentního	parlamentní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
záležitosti	záležitost	k1gFnPc4
vnitra	vnitro	k1gNnSc2
a	a	k8xC
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
výboru	výbor	k1gInSc2
pro	pro	k7c4
ekonomické	ekonomický	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
během	během	k7c2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
v	v	k7c6
únoru	únor	k1gInSc6
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Knesetu	Kneset	k1gInSc6
ho	on	k3xPp3gNnSc2
nahradil	nahradit	k5eAaPmAgInS
Avraham	Avraham	k1gInSc1
Levenbraun	Levenbraun	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Haná	Haná	k1gFnSc1
Mújás	Mújása	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kneset	Kneset	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Kneset	Kneset	k1gInSc1
–	–	k?
Haná	Haná	k1gFnSc1
Mújás	Mújása	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Členové	člen	k1gMnPc1
devátého	devátý	k4xOgInSc2
Knesetu	Kneset	k1gInSc2
zvoleného	zvolený	k2eAgInSc2d1
roku	rok	k1gInSc2
1977	#num#	k4
Likud	Likuda	k1gFnPc2
</s>
<s>
Arens	Arens	k6eAd1
</s>
<s>
Aridor	Aridor	k1gMnSc1
</s>
<s>
Badi	Badi	k1gNnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
</s>
<s>
Begin	Begin	k1gMnSc1
</s>
<s>
Berman	Berman	k1gMnSc1
</s>
<s>
Cipori	Cipori	k6eAd1
</s>
<s>
Dekel	Dekel	k1gMnSc1
</s>
<s>
Doron	Doron	k1gMnSc1
</s>
<s>
Erlich	Erlich	k1gMnSc1
</s>
<s>
Flomin	Flomin	k1gInSc1
</s>
<s>
Gruper	Gruper	k1gMnSc1
</s>
<s>
Hurvic	Hurvic	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi	Raf	k1gFnSc2
–	–	k?
Rešima	Rešima	k1gFnSc1
mamlachtit	mamlachtit	k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Telem	Telum	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Kacav	Kacav	k1gInSc1
</s>
<s>
Kac	Kac	k?
</s>
<s>
Kaufman	Kaufman	k1gMnSc1
</s>
<s>
Ge	Ge	k?
<g/>
'	'	kIx"
<g/>
ula	ula	k?
Kohen	Kohen	k1gInSc1
(	(	kIx(
<g/>
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
Techija	Techij	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Kohen-Avidov	Kohen-Avidov	k1gInSc1
</s>
<s>
Kohen-Orgad	Kohen-Orgad	k6eAd1
</s>
<s>
Kohen	Kohen	k1gInSc1
</s>
<s>
Korfu	Korfu	k1gNnSc1
</s>
<s>
Levy	Levy	k?
</s>
<s>
Lin	Lina	k1gFnPc2
</s>
<s>
Livni	Livn	k1gMnPc1
</s>
<s>
Meron	Meron	k1gMnSc1
</s>
<s>
Milo	milo	k6eAd1
</s>
<s>
Moda	Moda	k1gFnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Nasruddín	Nasruddín	k1gMnSc1
</s>
<s>
Nisim	Nisim	k6eAd1
</s>
<s>
Olmert	Olmert	k1gMnSc1
</s>
<s>
Pat	pat	k1gInSc1
</s>
<s>
Perec	Perec	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi	Raf	k1gFnSc2
–	–	k?
Rešima	Rešima	k1gFnSc1
mamlachtit	mamlachtit	k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
návrat	návrat	k1gInSc1
do	do	k7c2
Likudu	Likud	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Rechtman	Rechtman	k1gMnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Stern	sternum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Rom	Rom	k1gMnSc1
</s>
<s>
Savidor	Savidor	k1gMnSc1
</s>
<s>
Seidel	Seidel	k1gMnSc1
</s>
<s>
Moše	mocha	k1gFnSc3
Šamir	Šamir	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Techija	Techij	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Jicchak	Jicchak	k1gMnSc1
Šamir	Šamir	k1gMnSc1
</s>
<s>
Šarir	Šarir	k1gMnSc1
</s>
<s>
Šilansky	Šilansky	k6eAd1
</s>
<s>
Šostak	Šostak	k6eAd1
</s>
<s>
Šoval	Šoval	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Rafi	Raf	k1gFnSc2
–	–	k?
Rešima	Rešima	k1gFnSc1
mamlachtit	mamlachtit	k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Telem	Telem	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tamir	Tamir	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Weizman	Weizman	k1gMnSc1
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
arach	arach	k1gMnSc1
</s>
<s>
Alon	Alon	k1gNnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Chaša	Chašus	k1gMnSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
<g/>
)	)	kIx)
</s>
<s>
Amar	Amar	k1gMnSc1
</s>
<s>
Amir	Amir	k1gMnSc1
</s>
<s>
Amora	Amor	k1gMnSc4
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Arbeli-Almozlino	Arbeli-Almozlin	k2eAgNnSc1d1
</s>
<s>
Bar	bar	k1gInSc1
Lev	lev	k1gInSc1
</s>
<s>
Baram	Baram	k6eAd1
</s>
<s>
Cadok	Cadok	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Ron	ron	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dajan	Dajan	k1gInSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgNnSc4d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Telem	Telem	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Eban	Eban	k1gMnSc1
</s>
<s>
Ešel	Ešel	k1gMnSc1
</s>
<s>
Feder	Feder	k1gMnSc1
</s>
<s>
Grossman	Grossman	k1gMnSc1
</s>
<s>
Hakohen	Hakohen	k1gInSc1
</s>
<s>
Hadar	Hadar	k1gMnSc1
</s>
<s>
Hilel	Hilel	k1gMnSc1
</s>
<s>
Chariš	Chariš	k5eAaPmIp2nS
</s>
<s>
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akobi	akobi	k6eAd1
</s>
<s>
Jadlin	Jadlin	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Kac	Kac	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Mešel	Mešel	k1gMnSc1
</s>
<s>
Moj	Moj	k?
<g/>
'	'	kIx"
<g/>
al	ala	k1gFnPc2
</s>
<s>
Namir	Namir	k1gMnSc1
</s>
<s>
Navon	Navon	k1gNnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Kac-Oz	Kac-Oz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Peres	Peres	k1gMnSc1
</s>
<s>
Rabin	Rabin	k1gMnSc1
</s>
<s>
Rabinovic	Rabinovice	k1gFnPc2
(	(	kIx(
<g/>
pak	pak	k6eAd1
Herlic	Herlice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Rosolio	Rosolio	k6eAd1
</s>
<s>
Sarid	Sarid	k1gInSc1
</s>
<s>
Šachal	Šachal	k1gMnSc1
</s>
<s>
Speiser	Speiser	k1gMnSc1
</s>
<s>
Talmi	talmi	k2eAgFnSc1d1
</s>
<s>
Zakaj	Zakaj	k1gMnSc1
Daš	Daš	k1gMnSc1
</s>
<s>
Amit	Amit	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Asad	Asad	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Achva	Achvo	k1gNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Telem	Telem	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ataší	Atašet	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Elgrabli	Elgrabnout	k5eAaPmAgMnP
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgNnSc4d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Mifleget	Miflegeta	k1gFnPc2
ha-ichud	ha-ichuda	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Golomb	Golomb	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arach	arach	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Halevy	Haleva	k1gFnPc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Jadin	Jadin	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Jaguri	Jaguri	k6eAd1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
ad	ad	k7c4
<g/>
)	)	kIx)
</s>
<s>
Nof	Nof	k?
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Achva	Achvo	k1gNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Likudu	Likud	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Rubinstein	Rubinstein	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Tamir	Tamir	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaPmIp1nS,k5eAaBmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
pak	pak	k6eAd1
mezi	mezi	k7c4
nezařazené	zařazený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Toledano	Toledana	k1gFnSc5
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Viršubski	Viršubski	k6eAd1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Wertheimer	Wertheimer	k1gMnSc1
(	(	kIx(
<g/>
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Šinuj	Šinuj	k1gFnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pak	pak	k6eAd1
Levy	Levy	k?
<g/>
)	)	kIx)
</s>
<s>
Zorea	Zorea	k1gFnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Elijahu	Elijaha	k1gFnSc4
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Tnu	tnout	k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
a	a	k8xC
demokratit	demokratit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
Achva	Achvo	k1gNnSc2
<g/>
)	)	kIx)
Mafdal	Mafdal	k1gFnSc1
</s>
<s>
Abuchacira	Abuchacira	k6eAd1
</s>
<s>
Avtabi	Avtabi	k6eAd1
</s>
<s>
Ben	Ben	k1gInSc1
Me	Me	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ir	ir	k?
</s>
<s>
Burg	Burg	k1gMnSc1
</s>
<s>
Drukman	Drukman	k1gMnSc1
</s>
<s>
Glass	Glass	k6eAd1
</s>
<s>
Hammer	Hammer	k1gMnSc1
</s>
<s>
Melamed	Melamed	k1gMnSc1
</s>
<s>
Rubin	Rubin	k1gMnSc1
</s>
<s>
Scheinman	Scheinman	k1gMnSc1
</s>
<s>
Stern-Katan	Stern-Katan	k1gInSc1
</s>
<s>
Warhaftig	Warhaftig	k1gMnSc1
Chadaš	Chadaš	k1gMnSc1
</s>
<s>
Biton	Biton	k1gMnSc1
</s>
<s>
Mújás	Mújás	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Levenbraun	Levenbraun	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Túbí	Túbí	k6eAd1
</s>
<s>
Vilner	Vilner	k1gMnSc1
</s>
<s>
Zi	Zi	k?
<g/>
'	'	kIx"
<g/>
ad	ad	k7c4
Agudat	Agudat	k1gFnSc4
Jisra	Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Abramovič	Abramovič	k1gMnSc1
</s>
<s>
Gross	Gross	k1gMnSc1
</s>
<s>
Lorinc	Lorinc	k6eAd1
</s>
<s>
Poruš	porušit	k5eAaPmRp2nS
Machane	Machan	k1gMnSc5
smol	smolit	k5eAaImRp2nS
le-Jisra	le-Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Eli	Eli	k?
<g/>
'	'	kIx"
<g/>
av	av	k?
(	(	kIx(
<g/>
pak	pak	k6eAd1
Avnery	Avnera	k1gFnPc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pak	pak	k6eAd1
Jahjá	Jahjá	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pa	Pa	kA
<g/>
'	'	kIx"
<g/>
il	il	k?
(	(	kIx(
<g/>
pak	pak	k6eAd1
Marci	Marek	k1gMnPc1
<g/>
'	'	kIx"
<g/>
ano	ano	k9
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Mifleget	Miflegeta	k1gFnPc2
ha-ichud	ha-ichuda	k1gFnPc2
<g/>
)	)	kIx)
Šlomcijon	Šlomcijon	k1gInSc1
</s>
<s>
Šaron	Šaron	k1gInSc1
(	(	kIx(
<g/>
sloučeno	sloučit	k5eAaPmNgNnS
s	s	k7c7
Likudem	Likud	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Jicchaki	Jicchak	k1gFnPc1
(	(	kIx(
<g/>
sloučeno	sloučit	k5eAaPmNgNnS
s	s	k7c7
Likud	Likud	k1gInSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
Jisra	Jisra	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
achat	achat	k5eAaImF
<g/>
)	)	kIx)
Pituach	Pituach	k1gInSc1
ve-šalom	ve-šalom	k1gInSc1
</s>
<s>
Flatto-Šaron	Flatto-Šaron	k1gMnSc1
Rac	Rac	k1gMnSc1
</s>
<s>
Aloni	Alon	k1gMnPc1
Liberalim	Liberalim	k1gMnSc1
acma	acma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
im	im	k?
</s>
<s>
Hausner	Hausner	k1gMnSc1
Po	po	k7c4
<g/>
'	'	kIx"
<g/>
alej	alej	k1gFnSc4
Agudat	Agudat	k1gFnSc2
Jisra	Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Kahana	Kahana	k1gFnSc1
Sjednoc	Sjednoc	k1gFnSc1
<g/>
.	.	kIx.
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
kandidátka	kandidátka	k1gFnSc1
</s>
<s>
az-Zuabí	az-Zuabit	k5eAaPmIp3nP
(	(	kIx(
<g/>
pak	pak	k6eAd1
Abú	abú	k1gMnSc1
Rabía	Rabía	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pak	pak	k6eAd1
Muadí	Muadí	k1gNnSc1
<g/>
)	)	kIx)
*	*	kIx~
abecední	abecední	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
podle	podle	k7c2
pozice	pozice	k1gFnSc2
na	na	k7c6
kandidátní	kandidátní	k2eAgFnSc6d1
listině	listina	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
