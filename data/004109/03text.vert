<s>
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1847	[number]	k4	1847
Plánice	plánice	k1gFnSc2	plánice
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
Stádlec	Stádlec	k1gMnSc1	Stádlec
u	u	k7c2	u
Tábora	Tábor	k1gInSc2	Tábor
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
technik	technik	k1gMnSc1	technik
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
vynálezem	vynález	k1gInSc7	vynález
byla	být	k5eAaImAgFnS	být
oblouková	obloukový	k2eAgFnSc1d1	oblouková
lampa	lampa	k1gFnSc1	lampa
se	s	k7c7	s
samočinnou	samočinný	k2eAgFnSc7d1	samočinná
regulací	regulace	k1gFnSc7	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
světelnou	světelný	k2eAgFnSc4d1	světelná
fontánu	fontána	k1gFnSc4	fontána
<g/>
,	,	kIx,	,
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
elektrické	elektrický	k2eAgFnPc4d1	elektrická
tramvaje	tramvaj	k1gFnPc4	tramvaj
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
venkovského	venkovský	k2eAgMnSc2d1	venkovský
ševce	švec	k1gMnSc2	švec
a	a	k8xC	a
posluhovačky	posluhovačka	k1gFnSc2	posluhovačka
v	v	k7c6	v
pošumavském	pošumavský	k2eAgNnSc6d1	pošumavské
městečku	městečko	k1gNnSc6	městečko
Plánice	plánice	k1gFnSc2	plánice
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
jedinou	jediný	k2eAgFnSc4d1	jediná
českou	český	k2eAgFnSc4d1	Česká
reálku	reálka	k1gFnSc4	reálka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
peněz	peníze	k1gInPc2	peníze
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgMnS	moct
složit	složit	k5eAaPmF	složit
maturitu	maturita	k1gFnSc4	maturita
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
nemohl	moct	k5eNaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
zaplatit	zaplatit	k5eAaPmF	zaplatit
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ho	on	k3xPp3gMnSc4	on
profesor	profesor	k1gMnSc1	profesor
Václav	Václav	k1gMnSc1	Václav
Zenger	Zenger	k1gMnSc1	Zenger
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
jeho	jeho	k3xOp3gNnSc4	jeho
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
technické	technický	k2eAgNnSc4d1	technické
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
techniku	technika	k1gFnSc4	technika
(	(	kIx(	(
<g/>
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
mimořádného	mimořádný	k2eAgMnSc4d1	mimořádný
posluchače	posluchač	k1gMnSc4	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
najít	najít	k5eAaPmF	najít
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
soukromé	soukromý	k2eAgNnSc4d1	soukromé
doučování	doučování	k1gNnSc4	doučování
<g/>
,	,	kIx,	,
opisování	opisování	k1gNnSc4	opisování
not	nota	k1gFnPc2	nota
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
výrobou	výroba	k1gFnSc7	výroba
telegrafních	telegrafní	k2eAgFnPc2d1	telegrafní
a	a	k8xC	a
signalizačních	signalizační	k2eAgNnPc2d1	signalizační
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
práce	práce	k1gFnSc2	práce
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
železniční	železniční	k2eAgFnSc4d1	železniční
signalizaci	signalizace	k1gFnSc4	signalizace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
jeho	jeho	k3xOp3gFnSc4	jeho
prvním	první	k4xOgInSc7	první
uznávaným	uznávaný	k2eAgInSc7d1	uznávaný
vynálezem	vynález	k1gInSc7	vynález
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
1000	[number]	k4	1000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
železniční	železniční	k2eAgMnSc1d1	železniční
opravář	opravář	k1gMnSc1	opravář
a	a	k8xC	a
úředník	úředník	k1gMnSc1	úředník
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
jako	jako	k8xS	jako
přednosta	přednosta	k1gMnSc1	přednosta
telegrafního	telegrafní	k2eAgNnSc2d1	telegrafní
oddělení	oddělení	k1gNnSc2	oddělení
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
–	–	k?	–
<g/>
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
blokové	blokový	k2eAgNnSc4d1	Blokové
signalizační	signalizační	k2eAgNnSc4d1	signalizační
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zdokonalené	zdokonalený	k2eAgNnSc4d1	zdokonalené
elektrické	elektrický	k2eAgNnSc4d1	elektrické
návěstidlo	návěstidlo	k1gNnSc4	návěstidlo
<g/>
,	,	kIx,	,
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
ústřední	ústřední	k2eAgNnSc4d1	ústřední
stavění	stavění	k1gNnSc4	stavění
výhybek	výhybka	k1gFnPc2	výhybka
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
blokovací	blokovací	k2eAgNnSc4d1	blokovací
signalizační	signalizační	k2eAgNnSc4d1	signalizační
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znatelně	znatelně	k6eAd1	znatelně
omezilo	omezit	k5eAaPmAgNnS	omezit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
vlakových	vlakový	k2eAgFnPc2d1	vlaková
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc4	peníz
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
vynálezů	vynález	k1gInPc2	vynález
mu	on	k3xPp3gMnSc3	on
umožnily	umožnit	k5eAaPmAgFnP	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
technických	technický	k2eAgFnPc2d1	technická
novinek	novinka	k1gFnPc2	novinka
tu	ten	k3xDgFnSc4	ten
spatřil	spatřit	k5eAaPmAgMnS	spatřit
také	také	k9	také
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
obloukovou	obloukový	k2eAgFnSc4d1	oblouková
lampu	lampa	k1gFnSc4	lampa
ruského	ruský	k2eAgMnSc2d1	ruský
vynálezce	vynálezce	k1gMnSc2	vynálezce
Jabločkova	Jabločkův	k2eAgMnSc2d1	Jabločkův
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
elektrického	elektrický	k2eAgNnSc2d1	elektrické
osvětlení	osvětlení	k1gNnSc2	osvětlení
byla	být	k5eAaImAgFnS	být
fascinující	fascinující	k2eAgFnSc1d1	fascinující
a	a	k8xC	a
předurčila	předurčit	k5eAaPmAgFnS	předurčit
další	další	k2eAgFnSc4d1	další
dráhu	dráha	k1gFnSc4	dráha
jednatřicetiletého	jednatřicetiletý	k2eAgMnSc2d1	jednatřicetiletý
vynálezce	vynálezce	k1gMnSc2	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
řadu	řada	k1gFnSc4	řada
zlepšení	zlepšení	k1gNnSc3	zlepšení
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1880	[number]	k4	1880
patentoval	patentovat	k5eAaBmAgInS	patentovat
podstatně	podstatně	k6eAd1	podstatně
zdokonalenou	zdokonalený	k2eAgFnSc4d1	zdokonalená
obloukovou	obloukový	k2eAgFnSc4d1	oblouková
lampu	lampa	k1gFnSc4	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
velmi	velmi	k6eAd1	velmi
účinných	účinný	k2eAgFnPc2d1	účinná
obloukových	obloukový	k2eAgFnPc2d1	oblouková
lamp	lampa	k1gFnPc2	lampa
bylo	být	k5eAaImAgNnS	být
uhořívání	uhořívání	k1gNnSc4	uhořívání
uhlíkových	uhlíkový	k2eAgFnPc2d1	uhlíková
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nastavovaly	nastavovat	k5eAaImAgFnP	nastavovat
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
různými	různý	k2eAgInPc7d1	různý
regulátory	regulátor	k1gInPc7	regulátor
<g/>
.	.	kIx.	.
</s>
<s>
Křižík	Křižík	k1gMnSc1	Křižík
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgFnPc2	dva
cívek	cívka	k1gFnPc2	cívka
a	a	k8xC	a
kuželových	kuželový	k2eAgNnPc2d1	kuželové
železných	železný	k2eAgNnPc2d1	železné
jader	jádro	k1gNnPc2	jádro
udržovalo	udržovat	k5eAaImAgNnS	udržovat
stálý	stálý	k2eAgInSc4d1	stálý
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
v	v	k7c6	v
lampě	lampa	k1gFnSc6	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgMnS	otevřít
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
dílnu	dílna	k1gFnSc4	dílna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
obloukových	obloukový	k2eAgFnPc2d1	oblouková
lamp	lampa	k1gFnPc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Spřízněnou	spřízněný	k2eAgFnSc4d1	spřízněná
duši	duše	k1gFnSc4	duše
nalezl	nalézt	k5eAaBmAgInS	nalézt
v	v	k7c6	v
plzeňském	plzeňský	k2eAgMnSc6d1	plzeňský
podnikateli	podnikatel	k1gMnSc6	podnikatel
Piettovi	Piett	k1gMnSc6	Piett
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
zavedl	zavést	k5eAaPmAgMnS	zavést
osvětlení	osvětlení	k1gNnSc3	osvětlení
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
papírně	papírna	k1gFnSc6	papírna
a	a	k8xC	a
Piett	Piett	k1gMnSc1	Piett
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
uvést	uvést	k5eAaPmF	uvést
obloukovou	obloukový	k2eAgFnSc4d1	oblouková
lampu	lampa	k1gFnSc4	lampa
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
i	i	k8xC	i
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
obloukovka	obloukovka	k1gFnSc1	obloukovka
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
Edisonově	Edisonův	k2eAgFnSc3d1	Edisonova
žárovce	žárovka	k1gFnSc3	žárovka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
svítila	svítit	k5eAaImAgFnS	svítit
silněji	silně	k6eAd2	silně
než	než	k8xS	než
Edisonova	Edisonův	k2eAgFnSc1d1	Edisonova
žárovka	žárovka	k1gFnSc1	žárovka
–	–	k?	–
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
ocenění	ocenění	k1gNnSc4	ocenění
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Prodejem	prodej	k1gInSc7	prodej
licencí	licence	k1gFnPc2	licence
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
firma	firma	k1gFnSc1	firma
Schuckert	Schuckert	k1gInSc1	Schuckert
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
až	až	k6eAd1	až
5000	[number]	k4	5000
lamp	lampa	k1gFnPc2	lampa
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
si	se	k3xPyFc3	se
starou	starý	k2eAgFnSc4d1	stará
tovární	tovární	k2eAgFnSc4d1	tovární
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
reklamu	reklama	k1gFnSc4	reklama
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
např.	např.	kA	např.
provedl	provést	k5eAaPmAgMnS	provést
bezplatně	bezplatně	k6eAd1	bezplatně
osvětlení	osvětlení	k1gNnSc4	osvětlení
technické	technický	k2eAgFnSc2d1	technická
výstavy	výstava	k1gFnSc2	výstava
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
opět	opět	k6eAd1	opět
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
sklízel	sklízet	k5eAaImAgMnS	sklízet
spíš	spíš	k9	spíš
pocty	pocta	k1gFnPc4	pocta
než	než	k8xS	než
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obdržel	obdržet	k5eAaPmAgMnS	obdržet
první	první	k4xOgFnPc4	první
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c4	na
městské	městský	k2eAgNnSc4d1	Městské
pouliční	pouliční	k2eAgNnSc4d1	pouliční
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
rozzářily	rozzářit	k5eAaPmAgFnP	rozzářit
obloukovky	obloukovka	k1gFnPc1	obloukovka
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
také	také	k9	také
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
postavil	postavit	k5eAaPmAgMnS	postavit
první	první	k4xOgFnSc4	první
městskou	městský	k2eAgFnSc4d1	městská
elektrárnu	elektrárna	k1gFnSc4	elektrárna
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
pro	pro	k7c4	pro
Žižkov	Žižkov	k1gInSc4	Žižkov
<g/>
,	,	kIx,	,
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
vedle	vedle	k7c2	vedle
obloukovek	obloukovka	k1gFnPc2	obloukovka
i	i	k9	i
lustry	lustr	k1gInPc1	lustr
<g/>
,	,	kIx,	,
dynama	dynamo	k1gNnPc1	dynamo
a	a	k8xC	a
elektroinstalační	elektroinstalační	k2eAgInSc1d1	elektroinstalační
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
ovšem	ovšem	k9	ovšem
připravoval	připravovat	k5eAaImAgInS	připravovat
další	další	k2eAgInSc1d1	další
velký	velký	k2eAgInSc1d1	velký
projekt	projekt	k1gInSc1	projekt
–	–	k?	–
pouliční	pouliční	k2eAgFnSc4d1	pouliční
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
dráhu	dráha	k1gFnSc4	dráha
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Získat	získat	k5eAaPmF	získat
zakázku	zakázka	k1gFnSc4	zakázka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
dopravu	doprava	k1gFnSc4	doprava
měly	mít	k5eAaImAgFnP	mít
koňské	koňský	k2eAgFnPc1d1	koňská
dráhy	dráha	k1gFnPc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
však	však	k9	však
našel	najít	k5eAaPmAgMnS	najít
východisko	východisko	k1gNnSc4	východisko
<g/>
:	:	kIx,	:
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
svoji	svůj	k3xOyFgFnSc4	svůj
myšlenku	myšlenka	k1gFnSc4	myšlenka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
Jubilejní	jubilejní	k2eAgFnSc2d1	jubilejní
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgMnS	provést
osvětlení	osvětlení	k1gNnSc4	osvětlení
celé	celý	k2eAgFnSc2d1	celá
výstavy	výstava	k1gFnSc2	výstava
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
trať	trať	k1gFnSc4	trať
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
.	.	kIx.	.
</s>
<s>
Měřila	měřit	k5eAaImAgFnS	měřit
sice	sice	k8xC	sice
jen	jen	k9	jen
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
však	však	k9	však
opravdovou	opravdový	k2eAgFnSc7d1	opravdová
senzací	senzace	k1gFnSc7	senzace
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc4d2	veliký
slávu	sláva	k1gFnSc4	sláva
přinesla	přinést	k5eAaPmAgFnS	přinést
Křižíkovi	Křižík	k1gMnSc3	Křižík
jeho	jeho	k3xOp3gFnSc1	jeho
světelná	světelný	k2eAgFnSc1d1	světelná
fontána	fontána	k1gFnSc1	fontána
na	na	k7c6	na
Výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
triumf	triumf	k1gInSc4	triumf
mu	on	k3xPp3gMnSc3	on
vynesl	vynést	k5eAaPmAgMnS	vynést
nové	nový	k2eAgFnPc4d1	nová
zakázky	zakázka	k1gFnPc4	zakázka
i	i	k8xC	i
bankovní	bankovní	k2eAgInPc4d1	bankovní
úvěry	úvěr	k1gInPc4	úvěr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
zprovoznil	zprovoznit	k5eAaPmAgMnS	zprovoznit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
trati	trať	k1gFnSc2	trať
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
–	–	k?	–
<g/>
Karlín	Karlín	k1gInSc1	Karlín
<g/>
–	–	k?	–
<g/>
Libeň	Libeň	k1gFnSc1	Libeň
<g/>
–	–	k?	–
<g/>
Vysočany	Vysočany	k1gInPc1	Vysočany
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
a	a	k8xC	a
rozvětvení	rozvětvení	k1gNnSc6	rozvětvení
měřila	měřit	k5eAaImAgFnS	měřit
8	[number]	k4	8
km	km	kA	km
a	a	k8xC	a
přepravovala	přepravovat	k5eAaImAgFnS	přepravovat
ročně	ročně	k6eAd1	ročně
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
ji	on	k3xPp3gFnSc4	on
odkoupily	odkoupit	k5eAaPmAgInP	odkoupit
pražské	pražský	k2eAgInPc1d1	pražský
Elektrické	elektrický	k2eAgInPc1d1	elektrický
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
postupně	postupně	k6eAd1	postupně
vybavil	vybavit	k5eAaPmAgInS	vybavit
na	na	k7c4	na
130	[number]	k4	130
českých	český	k2eAgFnPc2d1	Česká
elektráren	elektrárna	k1gFnPc2	elektrárna
svým	svůj	k3xOyFgNnSc7	svůj
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
začal	začít	k5eAaPmAgMnS	začít
vyrůstat	vyrůstat	k5eAaImF	vyrůstat
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
konkurent	konkurent	k1gMnSc1	konkurent
–	–	k?	–
o	o	k7c4	o
patnáct	patnáct	k4xCc4	patnáct
let	let	k1gInSc4	let
mladší	mladý	k2eAgMnSc1d2	mladší
inženýr	inženýr	k1gMnSc1	inženýr
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolben	k2eAgMnSc1d1	Kolben
<g/>
.	.	kIx.	.
</s>
<s>
Nezadržitelně	zadržitelně	k6eNd1	zadržitelně
se	se	k3xPyFc4	se
blížil	blížit	k5eAaImAgInS	blížit
střet	střet	k1gInSc1	střet
koncepcí	koncepce	k1gFnPc2	koncepce
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c4	v
budoucnost	budoucnost	k1gFnSc4	budoucnost
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
Kolben	Kolbna	k1gFnPc2	Kolbna
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgFnSc4d2	veliký
perspektivu	perspektiva	k1gFnSc4	perspektiva
má	mít	k5eAaImIp3nS	mít
proud	proud	k1gInSc1	proud
střídavý	střídavý	k2eAgInSc1d1	střídavý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
konfrontaci	konfrontace	k1gFnSc3	konfrontace
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
soutěže	soutěž	k1gFnSc2	soutěž
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
ústřední	ústřední	k2eAgFnSc2d1	ústřední
pražské	pražský	k2eAgFnSc2d1	Pražská
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
výhodnou	výhodný	k2eAgFnSc4d1	výhodná
zakázku	zakázka	k1gFnSc4	zakázka
získala	získat	k5eAaPmAgFnS	získat
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
firma	firma	k1gFnSc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
obchodní	obchodní	k2eAgFnSc1d1	obchodní
prohra	prohra	k1gFnSc1	prohra
znamenala	znamenat	k5eAaImAgFnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
zakázek	zakázka	k1gFnPc2	zakázka
i	i	k8xC	i
bankovních	bankovní	k2eAgInPc2d1	bankovní
úvěrů	úvěr	k1gInPc2	úvěr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
náročného	náročný	k2eAgInSc2d1	náročný
podniku	podnik	k1gInSc2	podnik
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
první	první	k4xOgFnSc4	první
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
železnici	železnice	k1gFnSc4	železnice
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
<g/>
,	,	kIx,	,
z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
do	do	k7c2	do
Bechyně	Bechyně	k1gFnSc2	Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
problémů	problém	k1gInPc2	problém
už	už	k6eAd1	už
nevybředl	vybřednout	k5eNaPmAgInS	vybřednout
a	a	k8xC	a
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jej	on	k3xPp3gNnSc4	on
banky	banka	k1gFnPc1	banka
donutily	donutit	k5eAaPmAgFnP	donutit
přeměnit	přeměnit	k5eAaPmF	přeměnit
zadluženou	zadlužený	k2eAgFnSc4d1	zadlužená
firmu	firma	k1gFnSc4	firma
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
nesplácení	nesplácení	k1gNnSc4	nesplácení
úvěru	úvěr	k1gInSc2	úvěr
Pražská	pražský	k2eAgFnSc1d1	Pražská
úvěrní	úvěrní	k2eAgFnSc1d1	úvěrní
banka	banka	k1gFnSc1	banka
převzala	převzít	k5eAaPmAgFnS	převzít
jeho	on	k3xPp3gInSc4	on
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Roztrpčený	roztrpčený	k2eAgMnSc1d1	roztrpčený
vynálezce	vynálezce	k1gMnSc1	vynálezce
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
Řád	řád	k1gInSc1	řád
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
Řád	řád	k1gInSc1	řád
železné	železný	k2eAgFnSc2d1	železná
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
císařským	císařský	k2eAgMnSc7d1	císařský
radou	rada	k1gMnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
doživotním	doživotní	k2eAgMnSc7d1	doživotní
členem	člen	k1gMnSc7	člen
Panské	panský	k2eAgFnSc2d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
1906	[number]	k4	1906
obdržel	obdržet	k5eAaPmAgInS	obdržet
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Národ	národ	k1gInSc1	národ
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
"	"	kIx"	"
<g/>
Edisona	Edison	k1gMnSc4	Edison
<g/>
"	"	kIx"	"
nezapomněl	zapomenout	k5eNaPmAgMnS	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
téměř	téměř	k6eAd1	téměř
94	[number]	k4	94
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Stádlci	Stádlec	k1gInSc6	Stádlec
u	u	k7c2	u
Tábora	Tábor	k1gInSc2	Tábor
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
s	s	k7c7	s
poctami	pocta	k1gFnPc7	pocta
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
slavné	slavný	k2eAgFnSc2d1	slavná
české	český	k2eAgFnSc2d1	Česká
osobnosti	osobnost	k1gFnSc2	osobnost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
tiché	tichý	k2eAgFnSc2d1	tichá
manifestace	manifestace	k1gFnSc2	manifestace
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
mnoho	mnoho	k4c1	mnoho
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
městech	město	k1gNnPc6	město
a	a	k8xC	a
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
jeho	jeho	k3xOp3gInSc4	jeho
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
i	i	k8xC	i
stanice	stanice	k1gFnSc1	stanice
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
Křižíkova	Křižíkův	k2eAgFnSc1d1	Křižíkova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
malé	malý	k2eAgNnSc1d1	malé
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
