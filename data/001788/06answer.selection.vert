<s>
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
jako	jako	k8xS	jako
Transylvánie	Transylvánie	k1gFnSc1	Transylvánie
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
Transilvania	Transilvanium	k1gNnSc2	Transilvanium
nebo	nebo	k8xC	nebo
Ardeal	Ardeal	k1gInSc1	Ardeal
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Erdély	Erdéla	k1gFnSc2	Erdéla
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Siebenbürgen	Siebenbürgen	k1gInSc1	Siebenbürgen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
země	země	k1gFnSc1	země
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
