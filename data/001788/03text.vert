<s>
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
jako	jako	k8xS	jako
Transylvánie	Transylvánie	k1gFnSc1	Transylvánie
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
Transilvania	Transilvanium	k1gNnSc2	Transilvanium
nebo	nebo	k8xC	nebo
Ardeal	Ardeal	k1gInSc1	Ardeal
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Erdély	Erdéla	k1gFnSc2	Erdéla
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Siebenbürgen	Siebenbürgen	k1gInSc1	Siebenbürgen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
země	země	k1gFnSc1	země
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Transylvánie	Transylvánie	k1gFnSc1	Transylvánie
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
známá	známý	k2eAgFnSc1d1	známá
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
románu	román	k1gInSc2	román
Drákula	Drákula	k1gMnSc1	Drákula
Brama	brama	k1gFnSc1	brama
Stokera	Stokera	k1gFnSc1	Stokera
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Bran	brána	k1gFnPc2	brána
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
turistický	turistický	k2eAgInSc1d1	turistický
cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
románu	román	k1gInSc6	román
vylíčený	vylíčený	k2eAgInSc1d1	vylíčený
Drákulův	Drákulův	k2eAgInSc1d1	Drákulův
hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
také	také	k6eAd1	také
nazýval	nazývat	k5eAaImAgMnS	nazývat
Bran	brána	k1gFnPc2	brána
a	a	k8xC	a
opravdovému	opravdový	k2eAgInSc3d1	opravdový
hradu	hrad	k1gInSc3	hrad
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
se	se	k3xPyFc4	se
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
švédská	švédský	k2eAgFnSc1d1	švédská
black	black	k6eAd1	black
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
Siebenbürgen	Siebenbürgen	k1gInSc1	Siebenbürgen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
obývali	obývat	k5eAaImAgMnP	obývat
Dákové	Dák	k1gMnPc1	Dák
<g/>
;	;	kIx,	;
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Hunedoara	Hunedoara	k1gFnSc1	Hunedoara
stálo	stát	k5eAaImAgNnS	stát
dácké	dácký	k2eAgNnSc4d1	dácké
středisko	středisko	k1gNnSc4	středisko
Sarmizegetusa	Sarmizegetus	k1gMnSc2	Sarmizegetus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
100	[number]	k4	100
byl	být	k5eAaImAgMnS	být
dácký	dácký	k2eAgMnSc1d1	dácký
král	král	k1gMnSc1	král
Decebalus	Decebalus	k1gMnSc1	Decebalus
poražen	poražen	k2eAgMnSc1d1	poražen
Trajánem	Traján	k1gMnSc7	Traján
a	a	k8xC	a
vítězní	vítězný	k2eAgMnPc1d1	vítězný
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
doly	dol	k1gInPc4	dol
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc4	silnice
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
sídlišť	sídliště	k1gNnPc2	sídliště
(	(	kIx(	(
<g/>
Apulum	Apulum	k1gInSc1	Apulum
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Alba	alba	k1gFnSc1	alba
Iulia	Iulius	k1gMnSc2	Iulius
<g/>
;	;	kIx,	;
Napoca	Napocus	k1gMnSc2	Napocus
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Cluj-Napoca	Cluj-Napoc	k2eAgFnSc1d1	Cluj-Napoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
271	[number]	k4	271
získali	získat	k5eAaPmAgMnP	získat
území	území	k1gNnSc4	území
Vizigóti	Vizigót	k1gMnPc1	Vizigót
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Hunové	Hun	k1gMnPc1	Hun
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Avaři	Avar	k1gMnPc1	Avar
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
tudy	tudy	k6eAd1	tudy
prošla	projít	k5eAaPmAgFnS	projít
výrazná	výrazný	k2eAgFnSc1d1	výrazná
vlna	vlna	k1gFnSc1	vlna
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
migrace	migrace	k1gFnSc2	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dobyli	dobýt	k5eAaPmAgMnP	dobýt
území	území	k1gNnPc2	území
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
získali	získat	k5eAaPmAgMnP	získat
kontrolu	kontrola	k1gFnSc4	kontrola
roku	rok	k1gInSc2	rok
1003	[number]	k4	1003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Štěpán	Štěpán	k1gMnSc1	Štěpán
I.	I.	kA	I.
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
porazil	porazit	k5eAaPmAgMnS	porazit
zdejšího	zdejší	k2eAgMnSc4d1	zdejší
domácího	domácí	k2eAgMnSc4d1	domácí
knížete	kníže	k1gMnSc4	kníže
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Gyula	Gyulo	k1gNnSc2	Gyulo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
zde	zde	k6eAd1	zde
převažovalo	převažovat	k5eAaImAgNnS	převažovat
Romanizované	romanizovaný	k2eAgNnSc1d1	romanizovaný
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
(	(	kIx(	(
<g/>
předci	předek	k1gMnPc1	předek
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
)	)	kIx)	)
a	a	k8xC	a
východ	východ	k1gInSc1	východ
byl	být	k5eAaImAgInS	být
obýván	obývat	k5eAaImNgMnS	obývat
Sékely	Sékel	k1gMnPc7	Sékel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Uherského	uherský	k2eAgInSc2d1	uherský
království	království	k1gNnSc1	království
autonomním	autonomní	k2eAgNnSc7d1	autonomní
knížectvím	knížectví	k1gNnSc7	knížectví
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vojvodou	vojvoda	k1gMnSc7	vojvoda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1211	[number]	k4	1211
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Uherský	uherský	k2eAgMnSc1d1	uherský
Král	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
území	území	k1gNnSc1	území
Burzenland	Burzenlanda	k1gFnPc2	Burzenlanda
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
Rytířů	Rytíř	k1gMnPc2	Rytíř
aby	aby	kYmCp3nP	aby
"	"	kIx"	"
<g/>
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
pohanským	pohanský	k2eAgMnPc3d1	pohanský
<g/>
,	,	kIx,	,
Kumánským	Kumánský	k2eAgMnPc3d1	Kumánský
nájezdníkům	nájezdník	k1gMnPc3	nájezdník
a	a	k8xC	a
zalidnili	zalidnit	k5eAaPmAgMnP	zalidnit
deserta	desert	k1gMnSc2	desert
et	et	k?	et
inhabita	inhabita	k1gMnSc1	inhabita
zdejšího	zdejší	k2eAgInSc2d1	zdejší
kraje	kraj	k1gInSc2	kraj
<g/>
"	"	kIx"	"
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
částečnou	částečný	k2eAgFnSc4d1	částečná
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
ražení	ražení	k1gNnSc2	ražení
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
zahájili	zahájit	k5eAaPmAgMnP	zahájit
politiku	politika	k1gFnSc4	politika
zalidňování	zalidňování	k1gNnSc2	zalidňování
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
osadníci	osadník	k1gMnPc1	osadník
přicházeli	přicházet	k5eAaImAgMnP	přicházet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Porýní	Porýní	k1gNnSc2	Porýní
a	a	k8xC	a
Vestfálska	Vestfálsko	k1gNnSc2	Vestfálsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
Durynska	Durynsko	k1gNnSc2	Durynsko
a	a	k8xC	a
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říkat	říkat	k5eAaImF	říkat
Sedmihradští	sedmihradský	k2eAgMnPc1d1	sedmihradský
Sasové	Sas	k1gMnPc1	Sas
<g/>
.	.	kIx.	.
</s>
<s>
Osadníci	osadník	k1gMnPc1	osadník
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
zde	zde	k6eAd1	zde
mj.	mj.	kA	mj.
města	město	k1gNnSc2	město
Kronstadt	Kronstadt	k1gInSc1	Kronstadt
(	(	kIx(	(
<g/>
Braș	Braș	k1gFnSc1	Braș
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Schäßburg	Schäßburg	k1gMnSc1	Schäßburg
(	(	kIx(	(
<g/>
Sighiș	Sighiș	k1gMnSc1	Sighiș
<g/>
)	)	kIx)	)
či	či	k8xC	či
Hermannstadt	Hermannstadt	k1gMnSc1	Hermannstadt
(	(	kIx(	(
<g/>
Sibiu	Sibius	k1gMnSc6	Sibius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1224	[number]	k4	1224
jim	on	k3xPp3gMnPc3	on
král	král	k1gMnSc1	král
Ondřej	Ondřej	k1gMnSc1	Ondřej
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
Diploma	Diplom	k1gMnSc4	Diplom
Andreanum	Andreanum	k1gInSc1	Andreanum
udělil	udělit	k5eAaPmAgInS	udělit
řadu	řada	k1gFnSc4	řada
výsad	výsada	k1gFnPc2	výsada
a	a	k8xC	a
autonomii	autonomie	k1gFnSc4	autonomie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Sibiňským	sibiňský	k2eAgMnSc7d1	sibiňský
hrabětem	hrabě	k1gMnSc7	hrabě
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgFnPc4d1	obdobná
výsady	výsada	k1gFnPc4	výsada
a	a	k8xC	a
autonomii	autonomie	k1gFnSc4	autonomie
získali	získat	k5eAaPmAgMnP	získat
i	i	k9	i
Sékelové	Sékel	k1gMnPc1	Sékel
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
vládl	vládnout	k5eAaImAgInS	vládnout
Sékelský	Sékelský	k2eAgMnSc1d1	Sékelský
hrabě	hrabě	k1gMnSc1	hrabě
<g/>
.	.	kIx.	.
</s>
<s>
Sékelové	Sékel	k1gMnPc1	Sékel
a	a	k8xC	a
Sasové	Sas	k1gMnPc1	Sas
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
střežit	střežit	k5eAaImF	střežit
zdejší	zdejší	k2eAgInSc4d1	zdejší
úseky	úsek	k1gInPc4	úsek
uherských	uherský	k2eAgFnPc2d1	uherská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
etnika	etnikum	k1gNnPc1	etnikum
zároveň	zároveň	k6eAd1	zároveň
tvořila	tvořit	k5eAaImAgNnP	tvořit
dva	dva	k4xCgInPc4	dva
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
zdejších	zdejší	k2eAgInPc2d1	zdejší
politických	politický	k2eAgInPc2d1	politický
národů	národ	k1gInPc2	národ
-	-	kIx~	-
třetím	třetí	k4xOgInSc7	třetí
byla	být	k5eAaImAgFnS	být
zdejší	zdejší	k2eAgFnSc1d1	zdejší
maďarská	maďarský	k2eAgFnSc1d1	maďarská
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
zde	zde	k6eAd1	zde
vládl	vládnout	k5eAaImAgMnS	vládnout
přímo	přímo	k6eAd1	přímo
sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
vojvoda	vojvoda	k1gMnSc1	vojvoda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
Sékelskému	Sékelský	k2eAgMnSc3d1	Sékelský
a	a	k8xC	a
Sibiňskému	sibiňský	k2eAgMnSc3d1	sibiňský
hraběti	hrabě	k1gMnSc3	hrabě
nadřízen	nadřízen	k2eAgMnSc1d1	nadřízen
jen	jen	k9	jen
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
mincovnictví	mincovnictví	k1gNnSc2	mincovnictví
<g/>
,	,	kIx,	,
vojenství	vojenství	k1gNnSc2	vojenství
a	a	k8xC	a
společných	společný	k2eAgInPc2d1	společný
sněmů	sněm	k1gInPc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
většinoví	většinový	k2eAgMnPc1d1	většinový
Rumuni	Rumun	k1gMnPc1	Rumun
byli	být	k5eAaImAgMnP	být
v	v	k7c4	v
postavení	postavení	k1gNnSc4	postavení
nevolníků	nevolník	k1gMnPc2	nevolník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
započaly	započnout	k5eAaPmAgInP	započnout
nájezdy	nájezd	k1gInPc1	nájezd
Turků	Turek	k1gMnPc2	Turek
do	do	k7c2	do
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
z	z	k7c2	z
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
zdejší	zdejší	k2eAgMnPc1d1	zdejší
Sékelové	Sékel	k1gMnPc1	Sékel
a	a	k8xC	a
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
přemoženi	přemoct	k5eAaPmNgMnP	přemoct
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
včas	včas	k6eAd1	včas
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
právě	právě	k6eAd1	právě
zainteresován	zainteresovat	k5eAaPmNgInS	zainteresovat
v	v	k7c6	v
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zainteresován	zainteresovat	k5eAaPmNgInS	zainteresovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
uherské	uherský	k2eAgFnSc6d1	uherská
politice	politika	k1gFnSc6	politika
<g/>
;	;	kIx,	;
sedmihradské	sedmihradský	k2eAgInPc1d1	sedmihradský
sněmy	sněm	k1gInPc1	sněm
nebyly	být	k5eNaImAgInP	být
svolávány	svolávat	k5eAaImNgInP	svolávat
už	už	k6eAd1	už
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
a	a	k8xC	a
tak	tak	k6eAd1	tak
zde	zde	k6eAd1	zde
nebyl	být	k5eNaImAgMnS	být
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
koordinoval	koordinovat	k5eAaBmAgInS	koordinovat
obranu	obrana	k1gFnSc4	obrana
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
propukaly	propukat	k5eAaImAgInP	propukat
protesty	protest	k1gInPc4	protest
a	a	k8xC	a
vzpoury	vzpoura	k1gFnPc4	vzpoura
maďarských	maďarský	k2eAgMnPc2d1	maďarský
a	a	k8xC	a
rumunských	rumunský	k2eAgMnPc2d1	rumunský
nevolníků	nevolník	k1gMnPc2	nevolník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
snahy	snaha	k1gFnSc2	snaha
zdejší	zdejší	k2eAgFnSc2d1	zdejší
maďarské	maďarský	k2eAgFnSc2d1	maďarská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
Sasů	Sas	k1gMnPc2	Sas
a	a	k8xC	a
Sékelů	Sékel	k1gMnPc2	Sékel
potlačit	potlačit	k5eAaPmF	potlačit
tyto	tento	k3xDgInPc4	tento
protesty	protest	k1gInPc4	protest
a	a	k8xC	a
bránit	bránit	k5eAaImF	bránit
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
před	před	k7c4	před
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
podepsali	podepsat	k5eAaPmAgMnP	podepsat
zástupci	zástupce	k1gMnPc1	zástupce
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
privilegovaných	privilegovaný	k2eAgFnPc2d1	privilegovaná
skupin	skupina	k1gFnPc2	skupina
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1438	[number]	k4	1438
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tzv.	tzv.	kA	tzv.
Unie	unie	k1gFnSc1	unie
tří	tři	k4xCgInPc2	tři
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Unio	Unio	k6eAd1	Unio
Trium	Trium	k1gInSc1	Trium
Nationum	Nationum	k1gInSc1	Nationum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
unie	unie	k1gFnSc1	unie
pak	pak	k6eAd1	pak
hrála	hrát	k5eAaImAgFnS	hrát
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
zdejší	zdejší	k2eAgMnSc1d1	zdejší
rodák	rodák	k1gMnSc1	rodák
Jan	Jan	k1gMnSc1	Jan
Hunyadi	Hunyad	k1gMnPc1	Hunyad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varny	Varna	k1gFnSc2	Varna
(	(	kIx(	(
<g/>
1444	[number]	k4	1444
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
se	se	k3xPyFc4	se
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Zápolským	Zápolský	k2eAgMnSc7d1	Zápolský
stalo	stát	k5eAaPmAgNnS	stát
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Pešti	Pešť	k1gFnSc2	Pešť
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
samostatným	samostatný	k2eAgNnSc7d1	samostatné
knížectvím	knížectví	k1gNnSc7	knížectví
ve	v	k7c6	v
vazalském	vazalský	k2eAgInSc6d1	vazalský
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
i	i	k8xC	i
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
vlastních	vlastní	k2eAgFnPc2d1	vlastní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
včetně	včetně	k7c2	včetně
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
Partium	Partium	k1gNnSc1	Partium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
existovala	existovat	k5eAaImAgFnS	existovat
náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
hrálo	hrát	k5eAaImAgNnS	hrát
úlohu	úloha	k1gFnSc4	úloha
důležitého	důležitý	k2eAgNnSc2d1	důležité
nárazníkového	nárazníkový	k2eAgNnSc2d1	nárazníkové
pásma	pásmo	k1gNnSc2	pásmo
mezi	mezi	k7c7	mezi
impériem	impérium	k1gNnSc7	impérium
rakouských	rakouský	k2eAgMnPc2d1	rakouský
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1599	[number]	k4	1599
-	-	kIx~	-
1600	[number]	k4	1600
bylo	být	k5eAaImAgNnS	být
Sedmihradské	sedmihradský	k2eAgNnSc1d1	sedmihradské
knížectví	knížectví	k1gNnSc1	knížectví
spojeno	spojit	k5eAaPmNgNnS	spojit
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
stát	stát	k1gInSc4	stát
s	s	k7c7	s
Valašskem	Valašsko	k1gNnSc7	Valašsko
a	a	k8xC	a
Moldávií	Moldávie	k1gFnSc7	Moldávie
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Michaela	Michael	k1gMnSc2	Michael
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
získala	získat	k5eAaPmAgFnS	získat
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
tvořilo	tvořit	k5eAaImAgNnS	tvořit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
autonomní	autonomní	k2eAgFnSc4d1	autonomní
korunní	korunní	k2eAgFnSc4d1	korunní
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1765	[number]	k4	1765
povýšila	povýšit	k5eAaPmAgFnS	povýšit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
na	na	k7c4	na
velkoknížectví	velkoknížectví	k1gNnSc4	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
náleželo	náležet	k5eAaImAgNnS	náležet
i	i	k9	i
Partium	Partium	k1gNnSc1	Partium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
poslední	poslední	k2eAgInPc1d1	poslední
zbytky	zbytek	k1gInPc1	zbytek
byly	být	k5eAaImAgInP	být
Uhersku	Uhersko	k1gNnSc6	Uhersko
navráceny	navrácen	k2eAgFnPc1d1	navrácena
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
sepsali	sepsat	k5eAaPmAgMnP	sepsat
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1848	[number]	k4	1848
zdejší	zdejší	k2eAgMnPc1d1	zdejší
Rumuni	Rumun	k1gMnPc1	Rumun
petici	petice	k1gFnSc4	petice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
požadovali	požadovat	k5eAaImAgMnP	požadovat
zrovnoprávnění	zrovnoprávnění	k1gNnSc3	zrovnoprávnění
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
požadavku	požadavek	k1gInSc3	požadavek
však	však	k8xC	však
nebylo	být	k5eNaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
uherským	uherský	k2eAgInSc7d1	uherský
nátlakem	nátlak	k1gInSc7	nátlak
přijat	přijat	k2eAgInSc4d1	přijat
zákon	zákon	k1gInSc4	zákon
vyhlašující	vyhlašující	k2eAgNnSc4d1	vyhlašující
plné	plný	k2eAgNnSc4d1	plné
spojení	spojení	k1gNnSc4	spojení
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
a	a	k8xC	a
rakouská	rakouský	k2eAgFnSc1d1	rakouská
říšská	říšský	k2eAgFnSc1d1	říšská
ústava	ústava	k1gFnSc1	ústava
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1849	[number]	k4	1849
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
a	a	k8xC	a
Uhersko	Uhersko	k1gNnSc1	Uhersko
opět	opět	k6eAd1	opět
plně	plně	k6eAd1	plně
oddělila	oddělit	k5eAaPmAgFnS	oddělit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
a	a	k8xC	a
Uhersko	Uhersko	k1gNnSc1	Uhersko
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
své	své	k1gNnSc4	své
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
instituce	instituce	k1gFnSc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Říjnovým	říjnový	k2eAgInSc7d1	říjnový
diplomem	diplom	k1gInSc7	diplom
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
obnovena	obnoven	k2eAgFnSc1d1	obnovena
autonomie	autonomie	k1gFnSc1	autonomie
obou	dva	k4xCgFnPc2	dva
korunních	korunní	k2eAgFnPc2d1	korunní
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
zbytek	zbytek	k1gInSc4	zbytek
Partia	Partium	k1gNnSc2	Partium
a	a	k8xC	a
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
57	[number]	k4	57
243	[number]	k4	243
km2	km2	k4	km2
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
svolal	svolat	k5eAaPmAgMnS	svolat
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
do	do	k7c2	do
Sibině	Sibiň	k1gFnSc2	Sibiň
Sedmihradský	sedmihradský	k2eAgInSc1d1	sedmihradský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrovnoprávnění	zrovnoprávnění	k1gNnSc3	zrovnoprávnění
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rakousko-uherském	rakouskoherský	k2eAgNnSc6d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
1867	[number]	k4	1867
se	se	k3xPyFc4	se
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
stalo	stát	k5eAaPmAgNnS	stát
částí	část	k1gFnSc7	část
Zalitavska	Zalitavsko	k1gNnSc2	Zalitavsko
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
bylo	být	k5eAaImAgNnS	být
znovuzačleněno	znovuzačlenit	k5eAaPmNgNnS	znovuzačlenit
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zrušením	zrušení	k1gNnSc7	zrušení
jeho	jeho	k3xOp3gFnPc2	jeho
zemských	zemský	k2eAgFnPc2d1	zemská
institucí	instituce	k1gFnPc2	instituce
přestalo	přestat	k5eAaPmAgNnS	přestat
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1867	[number]	k4	1867
existovat	existovat	k5eAaImF	existovat
jako	jako	k8xS	jako
územní	územní	k2eAgFnSc1d1	územní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
území	území	k1gNnSc1	území
nově	nově	k6eAd1	nově
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
15	[number]	k4	15
žup	župa	k1gFnPc2	župa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
uherská	uherský	k2eAgFnSc1d1	uherská
vláda	vláda	k1gFnSc1	vláda
započala	započnout	k5eAaPmAgFnS	započnout
s	s	k7c7	s
maďarizací	maďarizace	k1gFnSc7	maďarizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
sešlo	sejít	k5eAaPmAgNnS	sejít
v	v	k7c4	v
Alba	album	k1gNnPc4	album
Iulii	Iulius	k1gMnPc7	Iulius
1	[number]	k4	1
228	[number]	k4	228
členné	členný	k2eAgNnSc1d1	členné
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
sedmihradských	sedmihradský	k2eAgMnPc2d1	sedmihradský
a	a	k8xC	a
uherských	uherský	k2eAgMnPc2d1	uherský
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgFnPc4d1	vedená
vůdci	vůdce	k1gMnPc7	vůdce
Sedmihradské	sedmihradský	k2eAgFnSc2d1	Sedmihradská
rumunské	rumunský	k2eAgFnSc2d1	rumunská
národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
Sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vydalo	vydat	k5eAaPmAgNnS	vydat
rezoluci	rezoluce	k1gFnSc4	rezoluce
požadující	požadující	k2eAgNnSc1d1	požadující
sjednocení	sjednocení	k1gNnSc1	sjednocení
všech	všecek	k3xTgMnPc2	všecek
Rumunů	Rumun	k1gMnPc2	Rumun
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poté	poté	k6eAd1	poté
schválili	schválit	k5eAaPmAgMnP	schválit
i	i	k9	i
zdejší	zdejší	k2eAgMnPc1d1	zdejší
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
se	se	k3xPyFc4	se
v	v	k7c6	v
Kluži	Kluž	k1gFnSc6	Kluž
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
postavili	postavit	k5eAaPmAgMnP	postavit
zdejší	zdejší	k2eAgMnPc1d1	zdejší
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
svoji	svůj	k3xOyFgFnSc4	svůj
loajalitu	loajalita	k1gFnSc4	loajalita
Uhersku	Uhersko	k1gNnSc3	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Trianonské	trianonský	k2eAgFnSc2d1	Trianonská
smlouvy	smlouva	k1gFnSc2	smlouva
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
Banátu	Banát	k1gInSc2	Banát
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
;	;	kIx,	;
tomu	ten	k3xDgNnSc3	ten
patří	patřit	k5eAaImIp3nS	patřit
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
mezihrou	mezihra	k1gFnSc7	mezihra
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Německa	Německo	k1gNnSc2	Německo
postoupena	postoupen	k2eAgFnSc1d1	postoupena
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Transylvánii	Transylvánie	k1gFnSc6	Transylvánie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
hovoří	hovořit	k5eAaImIp3nP	hovořit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
historické	historický	k2eAgInPc1d1	historický
Sedmihrady	Sedmihrady	k1gInPc1	Sedmihrady
tvoří	tvořit	k5eAaImIp3nP	tvořit
centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
hřebeny	hřeben	k1gInPc7	hřeben
Východních	východní	k2eAgInPc2d1	východní
<g/>
,	,	kIx,	,
Jižních	jižní	k2eAgInPc2d1	jižní
a	a	k8xC	a
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
(	(	kIx(	(
<g/>
Munț	Munț	k1gMnPc1	Munț
Apuseni	Apusen	k2eAgMnPc1d1	Apusen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
dnešními	dnešní	k2eAgFnPc7d1	dnešní
župami	župa	k1gFnPc7	župa
Alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Bistriț	Bistriț	k1gMnSc1	Bistriț
<g/>
,	,	kIx,	,
Braș	Braș	k1gMnSc1	Braș
<g/>
,	,	kIx,	,
Cluj	Cluj	k1gMnSc1	Cluj
<g/>
,	,	kIx,	,
Covasna	Covasna	k1gFnSc1	Covasna
<g/>
,	,	kIx,	,
Harghita	Harghita	k1gFnSc1	Harghita
<g/>
,	,	kIx,	,
Mureș	Mureș	k1gFnSc1	Mureș
<g/>
,	,	kIx,	,
a	a	k8xC	a
Sibiu	Sibius	k1gMnSc3	Sibius
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
patří	patřit	k5eAaImIp3nP	patřit
asi	asi	k9	asi
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
župy	župa	k1gFnSc2	župa
Hunedoara	Hunedoara	k1gFnSc1	Hunedoara
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
župy	župa	k1gFnSc2	župa
Sălaj	Sălaj	k1gFnSc1	Sălaj
<g/>
,	,	kIx,	,
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g />
.	.	kIx.	.
</s>
<s>
župy	župa	k1gFnPc1	župa
Maramureș	Maramureș	k1gFnSc1	Maramureș
<g/>
,	,	kIx,	,
okrajová	okrajový	k2eAgFnSc1d1	okrajová
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
župy	župa	k1gFnSc2	župa
Bacău	Bacăa	k1gFnSc4	Bacăa
<g/>
,	,	kIx,	,
jihozápad	jihozápad	k1gInSc4	jihozápad
župy	župa	k1gFnSc2	župa
Neamț	Neamț	k1gFnSc1	Neamț
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
okrajová	okrajový	k2eAgFnSc1d1	okrajová
část	část	k1gFnSc1	část
župy	župa	k1gFnSc2	župa
Vâlcea	Vâlcea	k1gFnSc1	Vâlcea
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc1	jihozápad
župy	župa	k1gFnSc2	župa
Suceava	Suceava	k1gFnSc1	Suceava
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
zhruba	zhruba	k6eAd1	zhruba
57	[number]	k4	57
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jako	jako	k9	jako
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
či	či	k8xC	či
Transylvánie	Transylvánie	k1gFnSc1	Transylvánie
označuje	označovat	k5eAaImIp3nS	označovat
celá	celý	k2eAgFnSc1d1	celá
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
historické	historický	k2eAgNnSc4d1	historické
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
včetně	včetně	k7c2	včetně
Banátu	Banát	k1gInSc2	Banát
a	a	k8xC	a
regionů	region	k1gInPc2	region
Krišana	Krišan	k1gMnSc2	Krišan
<g/>
,	,	kIx,	,
Satmar	Satmar	k1gInSc1	Satmar
a	a	k8xC	a
Maramureš	Maramureš	k1gFnSc1	Maramureš
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
42	[number]	k4	42
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sikulsko	Sikulsko	k1gNnSc1	Sikulsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Rumuni	Rumun	k1gMnPc1	Rumun
(	(	kIx(	(
<g/>
74,7	[number]	k4	74,7
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
však	však	k9	však
silná	silný	k2eAgFnSc1d1	silná
maďarská	maďarský	k2eAgFnSc1d1	maďarská
menšina	menšina	k1gFnSc1	menšina
(	(	kIx(	(
<g/>
19,6	[number]	k4	19,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
drastickou	drastický	k2eAgFnSc4d1	drastická
rumunizaci	rumunizace	k1gFnSc4	rumunizace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zejména	zejména	k9	zejména
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
za	za	k7c2	za
komunistické	komunistický	k2eAgFnSc2d1	komunistická
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvořili	tvořit	k5eAaImAgMnP	tvořit
okolo	okolo	k7c2	okolo
12	[number]	k4	12
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
nyní	nyní	k6eAd1	nyní
pouze	pouze	k6eAd1	pouze
0,7	[number]	k4	0,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výraznější	výrazný	k2eAgFnSc7d2	výraznější
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Romové	Rom	k1gMnPc1	Rom
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
