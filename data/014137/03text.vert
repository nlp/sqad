<s>
Posedlost	posedlost	k1gFnSc1
(	(	kIx(
<g/>
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
přeložit	přeložit	k5eAaPmF
nebo	nebo	k8xC
jazykově	jazykově	k6eAd1
korigovat	korigovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
cizojazyčný	cizojazyčný	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
přeložit	přeložit	k5eAaPmF
do	do	k7c2
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
strojově	strojově	k6eAd1
přeložený	přeložený	k2eAgInSc1d1
text	text	k1gInSc1
vyžadující	vyžadující	k2eAgFnSc4d1
důkladnou	důkladný	k2eAgFnSc4d1
korekturu	korektura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neodstraňujte	odstraňovat	k5eNaImRp2nP
tuto	tento	k3xDgFnSc4
šablonu	šablona	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
text	text	k1gInSc1
neplní	plnit	k5eNaImIp3nS
minimální	minimální	k2eAgInSc4d1
požadavky	požadavek	k1gInPc4
na	na	k7c4
článek	článek	k1gInSc4
ve	v	k7c6
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
si	se	k3xPyFc3
nevíte	vědět	k5eNaImIp2nP
rady	rada	k1gFnPc4
<g/>
,	,	kIx,
navštivte	navštívit	k5eAaPmRp2nP
prosím	prosit	k5eAaImIp1nS
stránku	stránka	k1gFnSc4
Potřebuji	potřebovat	k5eAaImIp1nS
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
nebude	být	k5eNaImBp3nS
upraven	upraven	k2eAgMnSc1d1
do	do	k7c2
7	#num#	k4
dnů	den	k1gInPc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
článek	článek	k1gInSc1
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc1
příslušná	příslušný	k2eAgFnSc1d1
část	část	k1gFnSc1
smazána	smazán	k2eAgFnSc1d1
v	v	k7c6
procesu	proces	k1gInSc6
odloženého	odložený	k2eAgNnSc2d1
smazání	smazání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Čas	čas	k1gInSc1
označení	označení	k1gNnSc2
je	být	k5eAaImIp3nS
18	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
(	(	kIx(
<g/>
CEST	cesta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
ke	k	k7c3
smazání	smazání	k1gNnSc3
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
CEST	cesta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Pro	pro	k7c4
vkladatele	vkladatel	k1gMnSc4
šablony	šablona	k1gFnSc2
<g/>
:	:	kIx,
Vyhledejte	vyhledat	k5eAaPmRp2nP
v	v	k7c4
historii	historie	k1gFnSc4
článku	článek	k1gInSc2
autora	autor	k1gMnSc2
tohoto	tento	k3xDgInSc2
textu	text	k1gInSc2
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
vložte	vložit	k5eAaPmRp2nP
šablonu	šablona	k1gFnSc4
<g/>
{{	{{	k?
<g/>
subst	subst	k5eAaPmF
<g/>
:	:	kIx,
<g/>
Přeložit	přeložit	k5eAaPmF
autor	autor	k1gMnSc1
<g/>
|	|	kIx~
<g/>
Posedlost	posedlost	k1gFnSc1
(	(	kIx(
<g/>
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
<g/>
)	)	kIx)
<g/>
}}	}}	k?
--	--	k?
<g/>
~~~~	~~~~	k?
</s>
<s>
Posedlost	posedlost	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
seriálu	seriál	k1gInSc2
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
Pův	Pův	k1gFnSc2
<g/>
.	.	kIx.
název	název	k1gInSc1
</s>
<s>
Haunted	Haunted	k1gInSc1
Číslo	číslo	k1gNnSc1
</s>
<s>
řada	řada	k1gFnSc1
1	#num#	k4
</s>
<s>
epizoda	epizoda	k1gFnSc1
7	#num#	k4
Premiéra	premiéra	k1gFnSc1
</s>
<s>
říjen	říjen	k1gInSc1
29	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
Česká	český	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
Tvorba	tvorba	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Kevin	Kevin	k1gMnSc1
Williamson	Williamson	k1gMnSc1
</s>
<s>
Julie	Julie	k1gFnSc1
Plec	plec	k1gFnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Ernest	Ernest	k1gMnSc1
Dickerson	Dickerson	k1gMnSc1
Prod	Prod	k1gMnSc1
<g/>
.	.	kIx.
kód	kód	k1gInSc1
</s>
<s>
2J5006	2J5006	k4
Posloupnost	posloupnost	k1gFnSc1
dílů	díl	k1gInPc2
</s>
<s>
←	←	k?
Předchozí	předchozí	k2eAgFnSc2d1
Zatracené	zatracený	k2eAgFnSc2d1
dušeNásledující	dušeNásledující	k2eAgFnSc2d1
→	→	k?
162	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
Upíří	upíří	k2eAgFnPc4d1
deníkyNěkterá	deníkyNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Posedlost	posedlost	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
Haunted	Haunted	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sedmá	sedmý	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
první	první	k4xOgFnSc2
řady	řada	k1gFnSc2
amerického	americký	k2eAgInSc2d1
televizního	televizní	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
vysílána	vysílán	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
The	The	k1gFnSc2
CW	CW	kA
a	a	k8xC
v	v	k7c6
ČR	ČR	kA
byla	být	k5eAaImAgFnS
vysílána	vysílán	k2eAgFnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
Nova	novum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scénář	scénář	k1gInSc4
epizody	epizoda	k1gFnSc2
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
Andrew	Andrew	k1gMnSc1
Kreisberg	Kreisberg	k1gMnSc1
<g/>
,	,	kIx,
Kevin	Kevin	k1gMnSc1
Williamson	Williamson	k1gMnSc1
a	a	k8xC
Julie	Julie	k1gFnSc1
Plec	plec	k1gFnSc1
<g/>
,	,	kIx,
režíroval	režírovat	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
Ernest	Ernest	k1gMnSc1
Dickerson	Dickerson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Epizoda	epizoda	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Vicki	Vick	k1gMnPc1
(	(	kIx(
Kayla	Kayla	k1gMnSc1
Ewell	Ewell	k1gMnSc1
)	)	kIx)
není	být	k5eNaImIp3nS
schopna	schopen	k2eAgFnSc1d1
ovládat	ovládat	k5eAaImF
její	její	k3xOp3gFnSc4
krvežíznivost	krvežíznivost	k1gFnSc4
a	a	k8xC
zaútočí	zaútočit	k5eAaPmIp3nS
na	na	k7c4
Tylera	Tyler	k1gMnSc4
(	(	kIx(
Michael	Michael	k1gMnSc1
Trevino	Trevin	k2eAgNnSc1d1
)	)	kIx)
u	u	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
auta	auto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
(	(	kIx(
Paul	Paul	k1gMnSc1
Wesley	Weslea	k1gFnSc2
)	)	kIx)
dorazí	dorazit	k5eAaPmIp3nP
právě	právě	k6eAd1
včas	včas	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
zastavil	zastavit	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
Damon	Damon	k1gMnSc1
(	(	kIx(
Ian	Ian	k1gMnSc1
Somerhalder	Somerhalder	k1gMnSc1
)	)	kIx)
přinutí	přinutit	k5eAaPmIp3nS
Tylera	Tylera	k1gFnSc1
zapomenout	zapomenout	k5eAaPmF
na	na	k7c4
všechno	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Všichni	všechen	k3xTgMnPc1
ve	v	k7c6
městě	město	k1gNnSc6
hledají	hledat	k5eAaImIp3nP
zmizelou	zmizelý	k2eAgFnSc4d1
Vicki	Vicke	k1gFnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
Jeremyho	Jeremy	k1gMnSc2
(	(	kIx(
Steven	Steven	k2eAgMnSc1d1
R.	R.	kA
McQueen	McQueen	k1gInSc4
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
chce	chtít	k5eAaImIp3nS
jít	jít	k5eAaImF
za	za	k7c4
školu	škola	k1gFnSc4
a	a	k8xC
připojit	připojit	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
vyhledávacímu	vyhledávací	k2eAgInSc3d1
týmu	tým	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
(	(	kIx(
Nina	Nina	k1gFnSc1
Dobrev	Dobrev	k1gFnSc1
)	)	kIx)
ví	vědět	k5eAaImIp3nS
pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nemůže	moct	k5eNaImIp3nS
mu	on	k3xPp3gMnSc3
ji	on	k3xPp3gFnSc4
odhalit	odhalit	k5eAaPmF
a	a	k8xC
neúspěšně	úspěšně	k6eNd1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc2
snaží	snažit	k5eAaImIp3nS
odradit	odradit	k5eAaPmF
od	od	k7c2
hledání	hledání	k1gNnSc2
Vicki	Vick	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vicki	Vicki	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
domě	dům	k1gInSc6
Salvatorů	Salvator	k1gMnPc2
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
přizpůsobit	přizpůsobit	k5eAaPmF
své	svůj	k3xOyFgFnSc3
nové	nový	k2eAgFnSc3d1
povaze	povaha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
jí	on	k3xPp3gFnSc3
dá	dát	k5eAaPmIp3nS
trochu	trocha	k1gFnSc4
zvířecí	zvířecí	k2eAgFnSc2d1
krve	krev	k1gFnSc2
a	a	k8xC
vysvětlí	vysvětlit	k5eAaPmIp3nS
jí	on	k3xPp3gFnSc7
<g/>
,	,	kIx,
proč	proč	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
neměla	mít	k5eNaImAgFnS
krmit	krmit	k5eAaImF
na	na	k7c6
lidech	člověk	k1gMnPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
s	s	k7c7
ním	on	k3xPp3gNnSc7
Damon	Damon	k1gInSc1
nesouhlasí	souhlasit	k5eNaImIp3nS
a	a	k8xC
přemýšlí	přemýšlet	k5eAaImIp3nS
<g/>
,	,	kIx,
proč	proč	k6eAd1
noviny	novina	k1gFnPc1
vůbec	vůbec	k9
nezmínily	zmínit	k5eNaPmAgFnP
Loganovu	Loganův	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
(	(	kIx(
<g/>
Chris	Chris	k1gFnSc1
Johnson	Johnson	k1gMnSc1
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
přijde	přijít	k5eAaPmIp3nS
zkontrolovat	zkontrolovat	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
jde	jít	k5eAaImIp3nS
s	s	k7c7
Vicki	Vicki	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
informuje	informovat	k5eAaBmIp3nS
Stefana	Stefan	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
celé	celý	k2eAgNnSc1d1
město	město	k1gNnSc1
hledá	hledat	k5eAaImIp3nS
<g/>
,	,	kIx,
včetně	včetně	k7c2
jejího	její	k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
mu	on	k3xPp3gMnSc3
má	mít	k5eAaImIp3nS
říct	říct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
jí	on	k3xPp3gFnSc3
slíbí	slíbit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
Vicki	Vicki	k1gNnSc1
hlídat	hlídat	k5eAaImF
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
nebude	být	k5eNaImBp3nS
moci	moct	k5eAaImF
ovládat	ovládat	k5eAaImF
a	a	k8xC
bude	být	k5eAaImBp3nS
jí	on	k3xPp3gFnSc7
pomáhat	pomáhat	k5eAaImF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jen	jen	k9
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Stefan	Stefan	k1gMnSc1
odejde	odejít	k5eAaPmIp3nS
a	a	k8xC
jde	jít	k5eAaImIp3nS
shánět	shánět	k5eAaImF
další	další	k2eAgFnSc4d1
zvířecí	zvířecí	k2eAgFnSc4d1
krev	krev	k1gFnSc4
pro	pro	k7c4
Vicky	Vicka	k1gFnPc4
a	a	k8xC
Elenu	Elena	k1gFnSc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
nechá	nechat	k5eAaPmIp3nS
samotnou	samotný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívky	dívka	k1gFnSc2
spolu	spolu	k6eAd1
začnou	začít	k5eAaPmIp3nP
mluvit	mluvit	k5eAaImF
o	o	k7c4
Jeremym	Jeremym	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
Elena	Elena	k1gFnSc1
požádá	požádat	k5eAaPmIp3nS
Vicki	Vicke	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
od	od	k7c2
něj	on	k3xPp3gMnSc2
držela	držet	k5eAaImAgFnS
dál	daleko	k6eAd2
<g/>
,	,	kIx,
protože	protože	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
bezpečné	bezpečný	k2eAgNnSc1d1
<g/>
,	,	kIx,
Vicki	Vicki	k1gNnPc7
se	se	k3xPyFc4
rozčílí	rozčílit	k5eAaPmIp3nS
a	a	k8xC
chytne	chytnout	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
za	za	k7c4
krk	krk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekne	říct	k5eAaPmIp3nS
Eleně	Elena	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vídat	vídat	k5eAaImF
s	s	k7c7
kýmkoliv	kdokoliv	k3yInSc7
si	se	k3xPyFc3
přeje	přát	k5eAaImIp3nS
a	a	k8xC
odejde	odejít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Damon	Damon	k1gMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
přijít	přijít	k5eAaPmF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
a	a	k8xC
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
ve	v	k7c4
města	město	k1gNnPc4
ví	vědět	k5eAaImIp3nS
o	o	k7c6
upírech	upír	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaslechne	zaslechnout	k5eAaPmIp3nS
rozhovor	rozhovor	k1gInSc4
starosty	starosta	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
v	v	k7c6
Grillu	Grill	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
osloví	oslovit	k5eAaPmIp3nS
paní	paní	k1gFnSc4
Lockwoodovou	Lockwoodový	k2eAgFnSc4d1
a	a	k8xC
snaží	snažit	k5eAaImIp3nP
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
donutit	donutit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
řekla	říct	k5eAaPmAgFnS
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
nefunguje	fungovat	k5eNaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
má	mít	k5eAaImIp3nS
náramek	náramek	k1gInSc1
se	s	k7c7
sporýšem	sporýš	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podaří	podařit	k5eAaPmIp3nS
zjistit	zjistit	k5eAaPmF
nějaké	nějaký	k3yIgFnPc4
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
během	během	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k8xS
s	s	k7c7
ní	on	k3xPp3gFnSc7
pije	pít	k5eAaImIp3nS
a	a	k8xC
flirtuje	flirtovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Mezitím	mezitím	k6eAd1
je	být	k5eAaImIp3nS
Bonnie	Bonnie	k1gFnSc1
(	(	kIx(
Kat	kat	k1gMnSc1
Graham	Graham	k1gMnSc1
)	)	kIx)
v	v	k7c6
domě	dům	k1gInSc6
své	svůj	k3xOyFgFnSc2
babičky	babička	k1gFnSc2
(	(	kIx(
Jasmine	Jasmin	k1gInSc5
Guy	Guy	k1gMnSc4
)	)	kIx)
a	a	k8xC
učí	učit	k5eAaImIp3nP
se	se	k3xPyFc4
o	o	k7c6
historii	historie	k1gFnSc6
své	svůj	k3xOyFgFnSc2
čarodějnické	čarodějnický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chce	chtít	k5eAaImIp3nS
se	se	k3xPyFc4
naučit	naučit	k5eAaPmF
kouzlit	kouzlit	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
babička	babička	k1gFnSc1
jí	on	k3xPp3gFnSc3
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kouzla	kouzlo	k1gNnPc4
nejsou	být	k5eNaImIp3nP
pro	pro	k7c4
zábavu	zábava	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
jít	jít	k5eAaImF
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Caroline	Carolin	k1gInSc5
(	(	kIx(
Candice	Candice	k1gFnSc1
Accola	Accola	k1gFnSc1
)	)	kIx)
se	se	k3xPyFc4
při	při	k7c6
rozhovoru	rozhovor	k1gInSc6
s	s	k7c7
Bonnie	Bonnie	k1gFnSc1
snaží	snažit	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
si	se	k3xPyFc3
oblékne	obléknout	k5eAaPmIp3nS
na	na	k7c4
halloweenskou	halloweenský	k2eAgFnSc4d1
party	party	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
Bonnie	Bonnie	k1gFnSc1
kostým	kostým	k1gInSc4
čarodějnice	čarodějnice	k1gFnSc2
(	(	kIx(
<g/>
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ji	on	k3xPp3gFnSc4
rozruší	rozrušit	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
Damonův	Damonův	k2eAgInSc1d1
krystal	krystal	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
ho	on	k3xPp3gMnSc4
už	už	k6eAd1
nechce	chtít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Damon	Damon	k1gMnSc1
vezme	vzít	k5eAaPmIp3nS
Vicki	Vicke	k1gFnSc4
na	na	k7c4
chvíli	chvíle	k1gFnSc4
z	z	k7c2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pobavil	pobavit	k5eAaPmAgMnS
i	i	k9
přes	přes	k7c4
Stefanovy	Stefanův	k2eAgFnPc4d1
námitky	námitka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
je	on	k3xPp3gNnPc4
sleduje	sledovat	k5eAaImIp3nS
na	na	k7c4
dvůr	dvůr	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Damon	Damon	k1gMnSc1
ukáže	ukázat	k5eAaPmIp3nS
Vicki	Vicke	k1gFnSc4
některé	některý	k3yIgFnSc2
schopnosti	schopnost	k1gFnSc2
upírů	upír	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
rychle	rychle	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
pohybovat	pohybovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vicky	Vicek	k1gMnPc7
využije	využít	k5eAaPmIp3nS
této	tento	k3xDgFnSc2
schopnosti	schopnost	k1gFnSc2
a	a	k8xC
uteče	utéct	k5eAaPmIp3nS
do	do	k7c2
jejího	její	k3xOp3gInSc2
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
dovnitř	dovnitř	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
schopna	schopen	k2eAgFnSc1d1
<g/>
,	,	kIx,
dokud	dokud	k8xS
ji	on	k3xPp3gFnSc4
Matt	Matt	k1gMnSc1
(	(	kIx(
Zach	Zach	k1gMnSc1
Roerig	Roerig	k1gMnSc1
)	)	kIx)
nepozve	pozvat	k5eNaPmIp3nS
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objeví	objevit	k5eAaPmIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
Stefan	Stefan	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ji	on	k3xPp3gFnSc4
hledá	hledat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
Matt	Matt	k1gMnSc1
ho	on	k3xPp3gNnSc4
požádá	požádat	k5eAaPmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odešel	odejít	k5eAaPmAgMnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
ho	on	k3xPp3gNnSc4
Vicki	Vicki	k1gNnSc4
požádá	požádat	k5eAaPmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gInSc4
nezval	zvát	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Všichni	všechen	k3xTgMnPc1
skončí	skončit	k5eAaPmIp3nP
na	na	k7c4
Halloweenské	Halloweenský	k2eAgFnPc4d1
párty	párty	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
Vicki	Vick	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
je	být	k5eAaImIp3nS
Elena	Elena	k1gFnSc1
naštvaná	naštvaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
tam	tam	k6eAd1
také	také	k9
Jeremy	Jerema	k1gFnSc2
a	a	k8xC
nechce	chtít	k5eNaImIp3nS
Vicki	Vicke	k1gFnSc4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Jeremyho	Jeremy	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
ho	on	k3xPp3gMnSc4
se	s	k7c7
Stefanem	Stefan	k1gMnSc7
zače	začat	k5eAaPmIp3nS
hledat	hledat	k5eAaImF
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
už	už	k6eAd1
je	být	k5eAaImIp3nS
dávno	dávno	k6eAd1
s	s	k7c7
Vicki	Vicki	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
dva	dva	k4xCgMnPc1
se	se	k3xPyFc4
líbají	líbat	k5eAaImIp3nP
a	a	k8xC
Vicki	Vicki	k1gNnSc1
ho	on	k3xPp3gMnSc4
kousne	kousnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ucítí	ucítit	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc4
krev	krev	k1gFnSc4
<g/>
,	,	kIx,
ztratí	ztratit	k5eAaPmIp3nS
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
pokusí	pokusit	k5eAaPmIp3nS
se	se	k3xPyFc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
zaútočit	zaútočit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
tam	tam	k6eAd1
dorazí	dorazit	k5eAaPmIp3nS
včas	včas	k6eAd1
a	a	k8xC
odstrčí	odstrčit	k5eAaPmIp3nP
ji	on	k3xPp3gFnSc4
od	od	k7c2
Jeremyho	Jeremy	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vicki	Vicki	k1gNnSc2
ji	on	k3xPp3gFnSc4
rázem	rázem	k6eAd1
odhodí	odhodit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
přijde	přijít	k5eAaPmIp3nS
a	a	k8xC
požádá	požádat	k5eAaPmIp3nS
Elenu	Elena	k1gFnSc4
a	a	k8xC
Jeremyho	Jeremy	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
schovali	schovat	k5eAaPmAgMnP
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
bude	být	k5eAaImBp3nS
hledat	hledat	k5eAaImF
Vicki	Vicke	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vicki	Vicki	k6eAd1
chytí	chytit	k5eAaPmIp3nS
Elenu	Elena	k1gFnSc4
a	a	k8xC
Jeremyho	Jeremy	k1gMnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhodí	odhodit	k5eAaPmIp3nS
Jeremyho	Jeremy	k1gMnSc4
stranou	stranou	k6eAd1
a	a	k8xC
kousne	kousnout	k5eAaPmIp3nS
Elenu	Elena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
dostane	dostat	k5eAaPmIp3nS
včas	včas	k6eAd1
a	a	k8xC
bodne	bodnout	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
kolíkem	kolík	k1gInSc7
do	do	k7c2
srdce	srdce	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
krmí	krmit	k5eAaImIp3nS
na	na	k7c6
Eleně	Elena	k1gFnSc6
a	a	k8xC
zemře	zemřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Mezitím	mezitím	k6eAd1
Damon	Damon	k1gMnSc1
na	na	k7c6
večírku	večírek	k1gInSc6
potká	potkat	k5eAaPmIp3nS
Bonnie	Bonnie	k1gFnSc1
a	a	k8xC
všimne	všimnout	k5eAaPmIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
na	na	k7c4
sobě	se	k3xPyFc3
jeho	jeho	k3xOp3gInSc1
krystal	krystal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požádá	požádat	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
ho	on	k3xPp3gNnSc4
vrátila	vrátit	k5eAaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
patří	patřit	k5eAaImIp3nS
jemu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
Bonnie	Bonnie	k1gFnSc1
odmítne	odmítnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc2
pokusí	pokusit	k5eAaPmIp3nS
vzít	vzít	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc2
dotkne	dotknout	k5eAaPmIp3nS
<g/>
,	,	kIx,
ho	on	k3xPp3gInSc4
popálí	popálit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
zaskočen	zaskočen	k2eAgMnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
Bonnie	Bonnie	k1gFnSc1
vyděsí	vyděsit	k5eAaPmIp3nS
a	a	k8xC
uteče	utéct	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostane	dostat	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
domu	dům	k1gInSc2
její	její	k3xOp3gFnSc2
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jí	on	k3xPp3gFnSc2
babička	babička	k1gFnSc1
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ten	ten	k3xDgInSc4
křišťálový	křišťálový	k2eAgInSc4d1
náhrdelník	náhrdelník	k1gInSc4
patřil	patřit	k5eAaImAgMnS
jednomu	jeden	k4xCgMnSc3
z	z	k7c2
jejích	její	k3xOp3gMnPc2
nejmocnějších	mocný	k2eAgMnPc2d3
předků	předek	k1gMnPc2
<g/>
;	;	kIx,
ukáže	ukázat	k5eAaPmIp3nS
jí	on	k3xPp3gFnSc7
obrázek	obrázek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
odhalí	odhalit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
patřil	patřit	k5eAaImAgInS
služce	služka	k1gFnSc3
Katherine	Katherin	k1gInSc5
<g/>
,	,	kIx,
Emily	Emil	k1gMnPc7
Bennettové	Bennettová	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
čarodějnice	čarodějnice	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jeremy	Jerema	k1gFnPc4
je	být	k5eAaImIp3nS
zarmoucen	zarmoucen	k2eAgInSc1d1
kvůli	kvůli	k7c3
ztrátě	ztráta	k1gFnSc3
Vicki	Vick	k1gFnSc2
a	a	k8xC
Stefan	Stefan	k1gMnSc1
ho	on	k3xPp3gNnSc4
odvede	odvést	k5eAaPmIp3nS
z	z	k7c2
parkoviště	parkoviště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavolá	zavolat	k5eAaPmIp3nS
Damona	Damona	k1gFnSc1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
dorazí	dorazit	k5eAaPmIp3nS
<g/>
,	,	kIx,
vidí	vidět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Elena	Elena	k1gFnSc1
pláče	plakat	k5eAaImIp3nS
nad	nad	k7c7
Vickiným	Vickin	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
ho	on	k3xPp3gMnSc4
obviňuje	obviňovat	k5eAaImIp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
udeří	udeřit	k5eAaPmIp3nP
ho	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Damon	Damon	k1gMnSc1
jí	on	k3xPp3gFnSc3
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
krvácí	krvácet	k5eAaImIp3nS
a	a	k8xC
musí	muset	k5eAaImIp3nS
odejít	odejít	k5eAaPmF
(	(	kIx(
<g/>
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gNnSc3
nutkání	nutkání	k1gNnSc3
ji	on	k3xPp3gFnSc4
kousnout	kousnout	k5eAaPmF
a	a	k8xC
napít	napít	k5eAaPmF,k5eAaBmF
se	se	k3xPyFc4
její	její	k3xOp3gFnSc2
krve	krev	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Elena	Elena	k1gFnSc1
uposlechne	uposlechnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narazí	narazit	k5eAaPmIp3nS
na	na	k7c4
Matta	Matt	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
hledá	hledat	k5eAaImIp3nS
Vicki	Vicke	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Elena	Elena	k1gFnSc1
mu	on	k3xPp3gMnSc3
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
neviděla	vidět	k5eNaImAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
mu	on	k3xPp3gMnSc3
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
Vicki	Vicki	k1gNnPc1
mrtvá	mrtvý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Elena	Elena	k1gFnSc1
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
domů	dům	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
na	na	k7c6
verandě	veranda	k1gFnSc6
čeká	čekat	k5eAaImIp3nS
Stefan	Stefan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přivedl	přivést	k5eAaPmAgMnS
Jeremyho	Jeremy	k1gMnSc4
domů	domů	k6eAd1
a	a	k8xC
sám	sám	k3xTgMnSc1
je	být	k5eAaImIp3nS
touto	tento	k3xDgFnSc7
ztrátou	ztráta	k1gFnSc7
zdrcen	zdrtit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
zkontroluje	zkontrolovat	k5eAaPmIp3nS
Jeremyho	Jeremy	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
nechápe	chápat	k5eNaImIp3nS
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
přemýšlí	přemýšlet	k5eAaImIp3nS
<g/>
,	,	kIx,
proč	proč	k6eAd1
všichni	všechen	k3xTgMnPc1
kolem	kolem	k7c2
něj	on	k3xPp3gInSc2
musí	muset	k5eAaImIp3nS
zemřít	zemřít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
ho	on	k3xPp3gMnSc4
utěší	utěšit	k5eAaPmIp3nS
a	a	k8xC
vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
za	za	k7c7
Stefanem	Stefan	k1gMnSc7
a	a	k8xC
požádá	požádat	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přiměl	přimět	k5eAaPmAgInS
Jeremyho	Jeremy	k1gMnSc4
zapomenout	zapomenout	k5eAaPmF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
večer	večer	k6eAd1
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stefan	Stefan	k1gMnSc1
jí	on	k3xPp3gFnSc3
vysvětlí	vysvětlit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
protože	protože	k8xS
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nS
zvířaty	zvíře	k1gNnPc7
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
to	ten	k3xDgNnSc1
dělat	dělat	k5eAaImF
správně	správně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objeví	objevit	k5eAaPmIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
Damon	Damon	k1gMnSc1
a	a	k8xC
nabídne	nabídnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
udělá	udělat	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
to	ten	k3xDgNnSc1
opravdu	opravdu	k6eAd1
chce	chtít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elena	Elena	k1gFnSc1
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
chce	chtít	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
řekne	říct	k5eAaPmIp3nS
Damonovi	Damonův	k2eAgMnPc1d1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
říct	říct	k5eAaPmF
Jeremymu	Jeremym	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Damon	Damon	k1gMnSc1
jde	jít	k5eAaImIp3nS
do	do	k7c2
domu	dům	k1gInSc2
přinutit	přinutit	k5eAaPmF
Jeremyho	Jeremy	k1gMnSc4
zapomenout	zapomenout	k5eAaPmF
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Elena	Elena	k1gFnSc1
řekne	říct	k5eAaPmIp3nS
Stefanovi	Stefan	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
přeje	přát	k5eAaImIp3nS
zapomenout	zapomenout	k5eAaPmF
na	na	k7c4
všechno	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
na	na	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
ním	on	k3xPp3gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nemůže	moct	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
nechce	chtít	k5eNaImIp3nS
zapomenout	zapomenout	k5eAaPmF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
k	k	k7c3
němu	on	k3xPp3gNnSc3
cítí	cítit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Damon	Damon	k1gMnSc1
oznámí	oznámit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
udělal	udělat	k5eAaPmAgMnS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
Elena	Elena	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
V	v	k7c6
epizodě	epizoda	k1gFnSc6
"	"	kIx"
<g/>
Posedlost	posedlost	k1gFnSc1
<g/>
"	"	kIx"
lze	lze	k6eAd1
slyšet	slyšet	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
The	The	k1gMnSc1
Weight	Weight	k1gMnSc1
of	of	k?
Us	Us	k1gMnSc1
<g/>
“	“	k?
od	od	k7c2
Sanderse	Sanderse	k1gFnSc2
Bohlke	Bohlk	k1gFnSc2
</s>
<s>
"	"	kIx"
Sleep	Sleep	k1gInSc1
Alone	Alon	k1gInSc5
"	"	kIx"
od	od	k7c2
Bat	Bat	k1gFnSc2
for	forum	k1gNnPc2
Lashes	Lashes	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Time	Time	k1gFnSc1
to	ten	k3xDgNnSc4
Die	Die	k1gFnSc1
<g/>
“	“	k?
od	od	k7c2
The	The	k1gFnSc2
Dodos	Dodosa	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Open	Open	k1gInSc1
Hearts	Hearts	k1gInSc1
<g/>
“	“	k?
od	od	k7c2
The	The	k1gFnSc2
Longcut	Longcut	k1gInSc1
</s>
<s>
"	"	kIx"
To	ten	k3xDgNnSc1
Lose	los	k1gInSc5
My	my	k3xPp1nPc1
Life	Life	k1gNnPc6
"	"	kIx"
od	od	k7c2
White	Whit	k1gMnSc5
Lies	Lies	k1gInSc4
</s>
<s>
„	„	k?
<g/>
No	no	k9
One	One	k1gMnSc1
Sleeps	Sleepsa	k1gFnPc2
When	When	k1gMnSc1
I	i	k9
<g/>
'	'	kIx"
<g/>
m	m	kA
Awake	Awak	k1gMnSc2
<g/>
“	“	k?
od	od	k7c2
The	The	k1gFnSc2
Sounds	Soundsa	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Fading	fading	k1gInSc1
Light	Light	k1gMnSc1
<g/>
“	“	k?
od	od	k7c2
Final	Final	k1gInSc1
Flash	Flash	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Open	Open	k1gInSc1
Arms	Arms	k1gInSc1
<g/>
“	“	k?
od	od	k7c2
Gary	Gara	k1gFnSc2
Go	Go	k1gFnSc2
</s>
<s>
Ohlas	ohlas	k1gInSc1
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
V	v	k7c6
původním	původní	k2eAgNnSc6d1
americkém	americký	k2eAgNnSc6d1
vysílání	vysílání	k1gNnSc6
„	„	k?
<g/>
Posedlost	posedlost	k1gFnSc1
<g/>
“	“	k?
sledovalo	sledovat	k5eAaImAgNnS
4,18	4,18	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
o	o	k7c4
0,30	0,30	k4
více	hodně	k6eAd2
než	než	k8xS
předchozí	předchozí	k2eAgFnSc4d1
epizodu	epizoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Recenze	recenze	k1gFnSc1
</s>
<s>
Lauren	Laurna	k1gFnPc2
Attaway	Attawaa	k1gFnSc2
ze	z	k7c2
Star	Star	kA
Pulse	puls	k1gInSc5
dala	dát	k5eAaPmAgFnS
epizodě	epizoda	k1gFnSc6
hodnocení	hodnocení	k1gNnSc6
A-	A-	k1gFnSc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Vickiina	Vickiin	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
otevřela	otevřít	k5eAaPmAgFnS
mnoho	mnoho	k4c4
potenciálních	potenciální	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Lauren	Laurna	k1gFnPc2
Kalal	Kalal	k1gInSc4
z	z	k7c2
Fandomania	Fandomanium	k1gNnSc2
ohodnotila	ohodnotit	k5eAaPmAgFnS
epizodu	epizoda	k1gFnSc4
s	s	k7c7
4,5	4,5	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
„	„	k?
<g/>
Tato	tento	k3xDgFnSc1
epizoda	epizoda	k1gFnSc1
vás	vy	k3xPp2nPc4
vtáhne	vtáhnout	k5eAaPmIp3nS
a	a	k8xC
nechá	nechat	k5eAaPmIp3nS
vás	vy	k3xPp2nPc4
prosit	prosit	k5eAaImF
o	o	k7c4
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozvedne	pozvednout	k5eAaPmIp3nS
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
z	z	k7c2
provinilého	provinilý	k2eAgNnSc2d1
potěšení	potěšení	k1gNnSc2
na	na	k7c4
televizní	televizní	k2eAgInSc4d1
pořad	pořad	k1gInSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
si	se	k3xPyFc3
nemůžete	moct	k5eNaImIp2nP
nechat	nechat	k5eAaPmF
ujít	ujít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsem	být	k5eAaImIp1nS
znovu	znovu	k6eAd1
investován	investovat	k5eAaBmNgInS
do	do	k7c2
potenciálu	potenciál	k1gInSc2
této	tento	k3xDgFnSc2
show	show	k1gFnSc2
<g/>
,	,	kIx,
teď	teď	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
stabilizovala	stabilizovat	k5eAaBmAgFnS
jako	jako	k9
něco	něco	k3yInSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
přesahuje	přesahovat	k5eAaImIp3nS
obvyklé	obvyklý	k2eAgFnPc4d1
příčky	příčka	k1gFnPc4
seriálů	seriál	k1gInPc2
CW	CW	kA
<g/>
.	.	kIx.
“	“	k?
</s>
<s>
Robin	robin	k2eAgMnSc1d1
Franson	Franson	k1gMnSc1
Pruter	Pruter	k1gMnSc1
ze	z	k7c2
společnosti	společnost	k1gFnSc2
Forced	Forced	k1gInSc1
Viewing	Viewing	k1gInSc1
ohodnotil	ohodnotit	k5eAaPmAgInS
epizodu	epizoda	k1gFnSc4
s	s	k7c7
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
že	že	k8xS
epizoda	epizoda	k1gFnSc1
je	být	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
zklamáním	zklamání	k1gNnSc7
po	po	k7c6
epizodě	epizoda	k1gFnSc6
Zatracené	zatracený	k2eAgFnSc2d1
duše	duše	k1gFnSc2
.	.	kIx.
„	„	k?
<g/>
Tráví	trávit	k5eAaImIp3nS
se	se	k3xPyFc4
málo	málo	k1gNnSc1
času	čas	k1gInSc2
nad	nad	k7c7
rozvíjením	rozvíjení	k1gNnSc7
postav	postava	k1gFnPc2
a	a	k8xC
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
v	v	k7c6
seriálu	seriál	k1gInSc6
budou	být	k5eAaImBp3nP
posouvat	posouvat	k5eAaImF
kupředu	kupředu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
epizoda	epizoda	k1gFnSc1
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
Vickiin	Vickiin	k2eAgInSc4d1
úpadek	úpadek	k1gInSc4
a	a	k8xC
konečný	konečný	k2eAgInSc1d1
zánik	zánik	k1gInSc1
<g/>
.	.	kIx.
“	“	k?
</s>
<s>
Josie	Josius	k1gMnPc4
Kafka	Kafka	k1gMnSc1
z	z	k7c2
Doux	Doux	k1gInSc4
Reviews	Reviewsa	k1gFnPc2
ohodnotila	ohodnotit	k5eAaPmAgFnS
epizodu	epizoda	k1gFnSc4
s	s	k7c7
3,5	3,5	k4
/	/	kIx~
4	#num#	k4
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
báječné	báječný	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
smrt	smrt	k1gFnSc4
Vicky	Vicka	k1gFnSc2
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
byla	být	k5eAaImAgNnP
překvapením	překvapení	k1gNnSc7
<g/>
.	.	kIx.
„	„	k?
<g/>
Zabití	zabití	k1gNnSc2
člena	člen	k1gMnSc2
obsazení	obsazení	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
téměř	téměř	k6eAd1
povinné	povinný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
jsem	být	k5eAaImIp1nS
šokována	šokován	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
udělaly	udělat	k5eAaPmAgInP
tak	tak	k9
rychle	rychle	k6eAd1
<g/>
:	:	kIx,
snadno	snadno	k6eAd1
mohli	moct	k5eAaImAgMnP
vytvořit	vytvořit	k5eAaPmF
mini-oblouk	mini-oblouk	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
Vicki	Vick	k1gInSc6
terorizuje	terorizovat	k5eAaImIp3nS
město	město	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
jsem	být	k5eAaImIp1nS
očekávala	očekávat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
o	o	k7c6
čem	co	k3yInSc6,k3yQnSc6,k3yRnSc6
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
show	show	k1gFnSc1
<g/>
:	:	kIx,
jde	jít	k5eAaImIp3nS
tu	tu	k6eAd1
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
lidé	člověk	k1gMnPc1
reagují	reagovat	k5eAaBmIp3nP
na	na	k7c4
trauma	trauma	k1gNnSc4
<g/>
,	,	kIx,
změny	změna	k1gFnPc4
a	a	k8xC
zármutek	zármutek	k1gInSc4
-	-	kIx~
a	a	k8xC
Vickiina	Vickiin	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
a	a	k8xC
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
přišlo	přijít	k5eAaPmAgNnS
před	před	k7c7
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
vážný	vážný	k2eAgInSc1d1
zlom	zlom	k1gInSc1
pro	pro	k7c4
několik	několik	k4yIc4
našich	náš	k3xOp1gFnPc2
postav	postava	k1gFnPc2
<g/>
.	.	kIx.
“	“	k?
</s>
<s>
Alice	Alice	k1gFnSc1
Jester	Jestra	k1gFnPc2
z	z	k7c2
Blog	Bloga	k1gFnPc2
Critics	Criticsa	k1gFnPc2
zhodnotila	zhodnotit	k5eAaPmAgFnS
epizodu	epizoda	k1gFnSc4
dobře	dobře	k6eAd1
a	a	k8xC
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
téma	téma	k1gNnSc1
upírů	upír	k1gMnPc2
bylo	být	k5eAaImAgNnS
často	často	k6eAd1
vidět	vidět	k5eAaImF
na	na	k7c4
téma	téma	k1gNnSc4
Halloween	Hallowena	k1gFnPc2
<g/>
,	,	kIx,
Upíří	upíří	k2eAgInPc1d1
deníky	deník	k1gInPc1
dokázaly	dokázat	k5eAaPmAgInP
vyniknout	vyniknout	k5eAaPmF
stoupajícím	stoupající	k2eAgNnSc7d1
dramatem	drama	k1gNnSc7
<g/>
.	.	kIx.
„	„	k?
<g/>
Co	co	k8xS
vy	vy	k3xPp2nPc1
víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tak	tak	k9
nějak	nějak	k6eAd1
šťastný	šťastný	k2eAgInSc4d1
konec	konec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
šťastný	šťastný	k2eAgInSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tragický	tragický	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c6
upírech	upír	k1gMnPc6
o	o	k7c6
Halloweenu	Halloween	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
show	show	k1gFnSc1
chytá	chytat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jistě	jistě	k6eAd1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
tam	tam	k6eAd1
nějaké	nějaký	k3yIgFnPc1
dějové	dějový	k2eAgFnPc1d1
mezery	mezera	k1gFnPc1
(	(	kIx(
<g/>
Stefan	Stefan	k1gMnSc1
a	a	k8xC
Elena	Elena	k1gFnSc1
nechali	nechat	k5eAaPmAgMnP
jít	jít	k5eAaImF
Damona	Damon	k1gMnSc4
o	o	k7c6
samotě	samota	k1gFnSc6
s	s	k7c7
Jeremym	Jeremym	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
mu	on	k3xPp3gMnSc3
udělala	udělat	k5eAaPmAgFnS
Vicki	Vicke	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stále	stále	k6eAd1
nový	nový	k2eAgInSc1d1
seriál	seriál	k1gInSc1
začínající	začínající	k2eAgInSc1d1
od	od	k7c2
nuly	nula	k1gFnSc2
<g/>
,	,	kIx,
nechám	nechat	k5eAaPmIp1nS
to	ten	k3xDgNnSc1
zatím	zatím	k6eAd1
jen	jen	k9
sklouznout	sklouznout	k5eAaPmF
<g/>
.	.	kIx.
“	“	k?
</s>
<s>
Popsugar	Popsugar	k1gInSc1
z	z	k7c2
Buzzsugaru	Buzzsugar	k1gInSc2
zhodnotil	zhodnotit	k5eAaPmAgMnS
epizodu	epizoda	k1gFnSc4
dobře	dobře	k6eAd1
a	a	k8xC
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
zatím	zatím	k6eAd1
nejlepší	dobrý	k2eAgFnSc1d3
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
„	„	k?
<g/>
Halloweenské	Halloweenský	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
je	být	k5eAaImIp3nS
ideální	ideální	k2eAgFnSc1d1
pro	pro	k7c4
přehrání	přehrání	k1gNnSc4
Vickiiny	Vickiin	k2eAgFnSc2d1
přeměny	přeměna	k1gFnSc2
na	na	k7c4
upíra	upír	k1gMnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
Bonnie	Bonnie	k1gFnPc4
na	na	k7c6
prozkoumání	prozkoumání	k1gNnSc6
její	její	k3xOp3gFnSc2
tajemné	tajemný	k2eAgFnSc2d1
genealogické	genealogický	k2eAgFnSc2d1
historie	historie	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
odhalení	odhalení	k1gNnSc4
„	„	k?
<g/>
Rady	rada	k1gFnSc2
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Lucia	Lucia	k1gFnSc1
z	z	k7c2
Heroine	heroin	k1gInSc5
TV	TV	kA
také	také	k9
dobře	dobře	k6eAd1
zhodnotila	zhodnotit	k5eAaPmAgFnS
epizodu	epizoda	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
smrt	smrt	k1gFnSc1
Vicky	Vicka	k1gFnSc2
ji	on	k3xPp3gFnSc4
nerozplakala	rozplakat	k5eNaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
nebyla	být	k5eNaImAgFnS
nadšená	nadšený	k2eAgFnSc1d1
z	z	k7c2
přeměny	přeměna	k1gFnSc2
její	její	k3xOp3gFnSc2
postavy	postava	k1gFnSc2
na	na	k7c4
upíra	upír	k1gMnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Existuje	existovat	k5eAaImIp3nS
příliš	příliš	k6eAd1
mnoho	mnoho	k4c1
zápletek	zápletka	k1gFnPc2
z	z	k7c2
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
bych	by	kYmCp1nS
ráda	rád	k2eAgFnSc1d1
viděla	vidět	k5eAaImAgFnS
se	se	k3xPyFc4
rozvíjet	rozvíjet	k5eAaImF
<g/>
,	,	kIx,
než	než	k8xS
půjdu	jít	k5eAaImIp1nS
tak	tak	k6eAd1
daleko	daleko	k6eAd1
mimo	mimo	k7c4
děj	děj	k1gInSc4
s	s	k7c7
drogově	drogově	k6eAd1
závislým	závislý	k2eAgMnSc7d1
novorozeným	novorozený	k2eAgMnSc7d1
upírem	upír	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechápejte	chápat	k5eNaImRp2nP
mě	já	k3xPp1nSc4
špatně	špatně	k6eAd1
-	-	kIx~
nechci	chtít	k5eNaImIp1nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
show	show	k1gNnPc1
byla	být	k5eAaImAgNnP
natočena	natočit	k5eAaBmNgNnP
podle	podle	k7c2
knih	kniha	k1gFnPc2
příliš	příliš	k6eAd1
pečlivě	pečlivě	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
některé	některý	k3yIgInPc4
milníky	milník	k1gInPc4
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yIgFnPc3,k3yQgFnPc3,k3yRgFnPc3
bych	by	kYmCp1nS
se	se	k3xPyFc4
ráda	rád	k2eAgFnSc1d1
dostala	dostat	k5eAaPmAgFnS
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
.	.	kIx.
.	.	kIx.
.	.	kIx.
]	]	kIx)
Z	z	k7c2
pozitivní	pozitivní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
Damona	Damon	k1gMnSc4
skvělá	skvělý	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
my	my	k3xPp1nPc1
jsme	být	k5eAaImIp1nP
opravdu	opravdu	k6eAd1
viděli	vidět	k5eAaImAgMnP
jinou	jiný	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
postavy	postava	k1gFnSc2
<g/>
.	.	kIx.
“	“	k?
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Ewell	Ewell	k1gInSc1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
Michaela	Michael	k1gMnSc4
Ausiella	Ausiell	k1gMnSc4
pro	pro	k7c4
Entertainment	Entertainment	k1gInSc4
Weekly	Weekl	k1gInPc4
prozradila	prozradit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
scénáři	scénář	k1gInSc6
měl	mít	k5eAaImAgMnS
původně	původně	k6eAd1
Jeremy	Jerema	k1gFnPc4
zabít	zabít	k5eAaPmF
její	její	k3xOp3gFnSc4
postavu	postava	k1gFnSc4
(	(	kIx(
<g/>
Vicki	Vicke	k1gFnSc4
<g/>
)	)	kIx)
místo	místo	k1gNnSc4
Stefana	Stefan	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
v	v	k7c6
procesu	proces	k1gInSc6
<g/>
,	,	kIx,
protože	protože	k8xS
Williamson	Williamson	k1gInSc1
a	a	k8xC
Plecová	Plecová	k1gFnSc1
si	se	k3xPyFc3
mysleli	myslet	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
lepší	dobrý	k2eAgNnSc1d2
nechat	nechat	k5eAaPmF
Stefana	Stefan	k1gMnSc4
zabít	zabít	k5eAaPmF
Vicki	Vick	k1gMnSc3
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Haunted	Haunted	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gMnSc5
Vampire	Vampir	k1gMnSc5
Diaries	Diaries	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
