<s desamb="1">
Často	často	k6eAd1
také	také	k9
mohou	moct	k5eAaImIp3nP
ovlivnit	ovlivnit	k5eAaPmF
průběh	průběh	k1gInSc4
nebo	nebo	k8xC
výsledky	výsledek	k1gInPc4
projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
posouzení	posouzení	k1gNnSc4
tohoto	tento	k3xDgNnSc2
ovlivnění	ovlivnění	k1gNnSc2
a	a	k8xC
naplánování	naplánování	k1gNnSc2
strategie	strategie	k1gFnSc2
pro	pro	k7c4
jednání	jednání	k1gNnSc4
se	s	k7c7
zainteresovanými	zainteresovaný	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>