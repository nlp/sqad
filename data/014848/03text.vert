<s>
Analýza	analýza	k1gFnSc1
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
Analýza	analýza	k1gFnSc1
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
stakeholder	stakeholdrat	k5eAaPmRp2nS
analysis	analysis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
postup	postup	k1gInSc4
používaný	používaný	k2eAgInSc4d1
při	při	k7c6
řízení	řízení	k1gNnSc6
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
řešení	řešení	k1gNnSc1
konfliktů	konflikt	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
identifikaci	identifikace	k1gFnSc4
a	a	k8xC
analýzu	analýza	k1gFnSc4
subjektů	subjekt	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
do	do	k7c2
projektu	projekt	k1gInSc2
aktivně	aktivně	k6eAd1
zapojeni	zapojit	k5eAaPmNgMnP
<g/>
,	,	kIx,
nebo	nebo	k8xC
jejich	jejich	k3xOp3gInPc1
zájmy	zájem	k1gInPc1
jsou	být	k5eAaImIp3nP
ovlivněny	ovlivnit	k5eAaPmNgInP
jeho	jeho	k3xOp3gFnSc7
realizací	realizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
také	také	k9
mohou	moct	k5eAaImIp3nP
ovlivnit	ovlivnit	k5eAaPmF
průběh	průběh	k1gInSc4
nebo	nebo	k8xC
výsledky	výsledek	k1gInPc4
projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
posouzení	posouzení	k1gNnSc4
tohoto	tento	k3xDgNnSc2
ovlivnění	ovlivnění	k1gNnSc2
a	a	k8xC
naplánování	naplánování	k1gNnSc2
strategie	strategie	k1gFnSc2
pro	pro	k7c4
jednání	jednání	k1gNnSc4
se	s	k7c7
zainteresovanými	zainteresovaný	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
činnost	činnost	k1gFnSc4
nutnou	nutný	k2eAgFnSc4d1
pro	pro	k7c4
následné	následný	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
metod	metoda	k1gFnPc2
identifikace	identifikace	k1gFnSc2
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
je	být	k5eAaImIp3nS
mapování	mapování	k1gNnSc1
podle	podle	k7c2
otázek	otázka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
chce	chtít	k5eAaImIp3nS
úspěch	úspěch	k1gInSc4
<g/>
(	(	kIx(
<g/>
neúspěch	neúspěch	k1gInSc4
<g/>
)	)	kIx)
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
z	z	k7c2
výsledku	výsledek	k1gInSc2
těžit	těžit	k5eAaImF
<g/>
,	,	kIx,
nebo	nebo	k8xC
jej	on	k3xPp3gNnSc4
naopak	naopak	k6eAd1
výsledek	výsledek	k1gInSc1
poškozuje	poškozovat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
sází	sázet	k5eAaImIp3nS
na	na	k7c4
úspěch	úspěch	k1gInSc4
/	/	kIx~
neúspěch	neúspěch	k1gInSc4
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
disponuje	disponovat	k5eAaBmIp3nS
užívacími	užívací	k2eAgNnPc7d1
nebo	nebo	k8xC
vlastnickými	vlastnický	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
nebude	být	k5eNaImBp3nS
moci	moct	k5eAaImF
velmi	velmi	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
uplatnit	uplatnit	k5eAaPmF
žádné	žádný	k3yNgInPc4
zájmy	zájem	k1gInPc4
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
disponuje	disponovat	k5eAaBmIp3nS
finančními	finanční	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
<g/>
,	,	kIx,
relevantními	relevantní	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
a	a	k8xC
informacemi	informace	k1gFnPc7
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
výsledek	výsledek	k1gInSc4
nutný	nutný	k2eAgInSc1d1
a	a	k8xC
čí	čí	k3xOyQgFnSc2,k3xOyRgFnSc2
podpory	podpora	k1gFnSc2
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
realizaci	realizace	k1gFnSc4
bránit	bránit	k5eAaImF
<g/>
/	/	kIx~
<g/>
zabránit	zabránit	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s>
Zařadit	zařadit	k5eAaPmF
je	on	k3xPp3gFnPc4
vhodné	vhodný	k2eAgFnPc4d1
i	i	k8xC
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
fakticky	fakticky	k6eAd1
kritériím	kritérion	k1gNnPc3
neodpovídají	odpovídat	k5eNaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
identifikováno	identifikován	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
mohly	moct	k5eAaImAgInP
na	na	k7c6
základě	základ	k1gInSc6
nedostatečných	dostatečný	k2eNgFnPc2d1
nebo	nebo	k8xC
nesprávných	správný	k2eNgFnPc2d1
informací	informace	k1gFnPc2
samy	sám	k3xTgMnPc4
řadit	řadit	k5eAaImF
mezi	mezi	k7c4
zainteresované	zainteresovaný	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
K	k	k7c3
popisu	popis	k1gInSc3
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
zvolit	zvolit	k5eAaPmF
kritéria	kritérion	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritéria	kritérion	k1gNnPc1
popisu	popis	k1gInSc6
stran	strana	k1gFnPc2
si	se	k3xPyFc3
volí	volit	k5eAaImIp3nP
ten	ten	k3xDgInSc4
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
provádí	provádět	k5eAaImIp3nS
analýzu	analýza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnými	možný	k2eAgNnPc7d1
kritérii	kritérion	k1gNnPc7
pro	pro	k7c4
jejich	jejich	k3xOp3gInSc4
popis	popis	k1gInSc4
je	být	k5eAaImIp3nS
například	například	k6eAd1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vztah	vztah	k1gInSc1
</s>
<s>
Potenciální	potenciální	k2eAgFnSc1d1
dotčenost	dotčenost	k1gFnSc1
</s>
<s>
Cíle	cíl	k1gInPc1
</s>
<s>
Okruh	okruh	k1gInSc1
působnosti	působnost	k1gFnSc2
</s>
<s>
Zájem	zájem	k1gInSc1
a	a	k8xC
angažovanost	angažovanost	k1gFnSc1
</s>
<s>
Stupeň	stupeň	k1gInSc1
organizace	organizace	k1gFnSc2
</s>
<s>
Kapacity	kapacita	k1gFnPc1
</s>
<s>
Povědomí	povědomí	k1gNnSc1
</s>
<s>
Doporučeným	doporučený	k2eAgInSc7d1
postup	postup	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
soupisu	soupis	k1gInSc2
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
je	být	k5eAaImIp3nS
posoudit	posoudit	k5eAaPmF
a	a	k8xC
zaznamenat	zaznamenat	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
prioritu	priorita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
si	se	k3xPyFc3
uvědomit	uvědomit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jedna	jeden	k4xCgFnSc1
zainteresovaná	zainteresovaný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
několik	několik	k4yIc4
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Matice	matice	k1gFnSc1
vlivu	vliv	k1gInSc2
a	a	k8xC
zájmu	zájem	k1gInSc2
</s>
<s>
Podle	podle	k7c2
vlivu	vliv	k1gInSc2
a	a	k8xC
zájmu	zájem	k1gInSc2
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
zainteresované	zainteresovaný	k2eAgFnPc1d1
strany	strana	k1gFnPc1
do	do	k7c2
následujících	následující	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
kategorie	kategorie	k1gFnPc1
jsou	být	k5eAaImIp3nP
následně	následně	k6eAd1
uplatnitelné	uplatnitelný	k2eAgMnPc4d1
například	například	k6eAd1
při	při	k7c6
volbě	volba	k1gFnSc6
způsobu	způsob	k1gInSc2
řízení	řízení	k1gNnSc4
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Matice	matice	k1gFnSc1
vlivu	vliv	k1gInSc2
a	a	k8xC
zájmu	zájem	k1gInSc2
<g/>
,	,	kIx,
nákres	nákres	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PLAMÍNEK	plamínek	k1gInSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řešení	řešení	k1gNnSc1
konfliktů	konflikt	k1gInPc2
a	a	k8xC
umění	umění	k1gNnSc2
rozhodovat	rozhodovat	k5eAaImF
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Copyright	copyright	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
198	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85794	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Doležal	Doležal	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Máchal	Máchal	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
Lacko	Lacko	k1gNnSc1
<g/>
,	,	kIx,
B.	B.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
:	:	kIx,
Projektový	projektový	k2eAgInSc1d1
management	management	k1gInSc1
podle	podle	k7c2
IPMA	IPMA	kA
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
,	,	kIx,
ISBN	ISBN	kA
978-80-247-4275-51	978-80-247-4275-51	k4
2	#num#	k4
Muro	mura	k1gFnSc5
<g/>
,	,	kIx,
M.	M.	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Pilothafte	Pilothaft	k1gMnSc5
Ermittlung	Ermittlung	k1gInSc1
und	und	k?
Analyse	analysa	k1gFnSc3
von	von	k1gInSc4
Zielgruppen	Zielgruppen	k2eAgInSc4d1
für	für	k?
die	die	k?
Information	Information	k1gInSc1
und	und	k?
Anhörung	Anhörung	k1gInSc1
der	drát	k5eAaImRp2nS
Öffentlichkeit	Öffentlichkeit	k1gInSc4
nach	nach	k1gInSc1
Art	Art	k1gFnSc1
<g/>
.	.	kIx.
14	#num#	k4
EG	ego	k1gNnPc2
Wasserrahmenrichtlinie	Wasserrahmenrichtlinie	k1gFnSc2
in	in	k?
einer	einer	k1gMnSc1
Flussgebietseinheit	Flussgebietseinheit	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umweltbundesamt	Umweltbundesamta	k1gFnPc2
<g/>
,	,	kIx,
Texte	text	k1gInSc5
27	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
R.	R.	kA
Edward	Edward	k1gMnSc1
Freeman	Freeman	k1gMnSc1
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
Moutchnik	Moutchnik	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Stakeholder	Stakeholder	k1gInSc1
management	management	k1gInSc1
and	and	k?
CSR	CSR	kA
<g/>
:	:	kIx,
questions	questions	k1gInSc1
and	and	k?
answers	answers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
UmweltWirtschaftsForum	UmweltWirtschaftsForum	k1gInSc1
<g/>
,	,	kIx,
Springer	Springer	k1gMnSc1
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
Nr	Nr	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
http://link.springer.com/article/10.1007/s00550-013-0266-3	http://link.springer.com/article/10.1007/s00550-013-0266-3	k4
</s>
<s>
Bryson	Bryson	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
<g/>
What	What	k1gInSc1
to	ten	k3xDgNnSc1
do	do	k7c2
when	whena	k1gFnPc2
stakeholder	stakeholder	k1gMnSc1
matter	matter	k1gMnSc1
<g/>
:	:	kIx,
A	a	k9
guide	guide	k6eAd1
to	ten	k3xDgNnSc4
stakeholder	stakeholder	k1gInSc1
identification	identification	k1gInSc1
and	and	k?
analysis	analysis	k1gInSc1
techniques	techniquesa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISSN	ISSN	kA
1471-9045	1471-9045	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Hausmann	Hausmann	k1gMnSc1
<g/>
,	,	kIx,
Hojda	Hojda	k1gMnSc1
<g/>
,	,	kIx,
Řepa	Řepa	k1gMnSc1
-	-	kIx~
Řízení	řízení	k1gNnSc1
projektů	projekt	k1gInPc2
IS	IS	kA
<g/>
,	,	kIx,
1997	#num#	k4
</s>
<s>
Rosenau	Rosenau	k6eAd1
<g/>
,	,	kIx,
M.	M.	kA
-	-	kIx~
Řízení	řízení	k1gNnSc1
projektů	projekt	k1gInPc2
-	-	kIx~
příklady	příklad	k1gInPc1
<g/>
,	,	kIx,
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
praxe	praxe	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
<g/>
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
</s>
<s>
Mitchell	Mitchell	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
K.	K.	kA
<g/>
,	,	kIx,
B.	B.	kA
R.	R.	kA
Agle	Agl	k1gMnSc2
<g/>
,	,	kIx,
and	and	k?
D.J.	D.J.	k1gMnSc1
Wood	Wood	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Toward	Toward	k1gInSc1
a	a	k8xC
Theory	Theor	k1gInPc1
of	of	k?
Stakeholder	Stakeholder	k1gInSc1
Identification	Identification	k1gInSc1
and	and	k?
Salience	Salience	k1gFnSc1
<g/>
:	:	kIx,
Defining	Defining	k1gInSc1
the	the	k?
Principle	Principle	k1gFnSc2
of	of	k?
Who	Who	k1gMnSc1
and	and	k?
What	What	k1gMnSc1
really	realla	k1gFnSc2
Counts	Counts	k1gInSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
in	in	k?
<g/>
:	:	kIx,
Academy	Academa	k1gFnSc2
of	of	k?
Management	management	k1gInSc1
Review	Review	k1gFnSc1
22	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
853	#num#	k4
-	-	kIx~
888	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Fiala	Fiala	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
-	-	kIx~
Řízení	řízení	k1gNnSc1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
Svozilová	Svozilová	k1gFnSc1
<g/>
,	,	kIx,
<g/>
A.	A.	kA
<g/>
:	:	kIx,
Projektový	projektový	k2eAgInSc1d1
management	management	k1gInSc1
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2011	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Řízení	řízení	k1gNnSc1
projektů	projekt	k1gInPc2
</s>
<s>
Řízení	řízení	k1gNnSc1
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
NÁRODNÍ	národní	k2eAgInSc1d1
STANDARD	standard	k1gInSc1
KOMPETENCÍ	kompetence	k1gFnPc2
PROJEKTOVÉHO	projektový	k2eAgNnSc2d1
ŘÍZENÍ	řízení	k1gNnSc2
</s>
<s>
Stránky	stránka	k1gFnPc1
pro	pro	k7c4
projektové	projektový	k2eAgMnPc4d1
manažery	manažer	k1gMnPc4
Archivováno	archivován	k2eAgNnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Portál	portál	k1gInSc1
pro	pro	k7c4
projektové	projektový	k2eAgMnPc4d1
manažery	manažer	k1gMnPc4
</s>
