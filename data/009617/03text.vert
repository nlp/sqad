<p>
<s>
Projekt	projekt	k1gInSc1	projekt
GNU	gnu	k1gNnSc2	gnu
je	být	k5eAaImIp3nS	být
projekt	projekt	k1gInSc1	projekt
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
svobodný	svobodný	k2eAgInSc4d1	svobodný
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
unixového	unixový	k2eAgInSc2d1	unixový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
cíl	cíl	k1gInSc1	cíl
byl	být	k5eAaImAgInS	být
vyvinout	vyvinout	k5eAaPmF	vyvinout
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
se	s	k7c7	s
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
licencí	licence	k1gFnSc7	licence
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádný	žádný	k3yNgInSc1	žádný
kód	kód	k1gInSc1	kód
původního	původní	k2eAgInSc2d1	původní
UNIXu	Unix	k1gInSc2	Unix
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
rekurzivní	rekurzivní	k2eAgFnSc1d1	rekurzivní
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
GNU	gnu	k1gMnSc1	gnu
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Not	nota	k1gFnPc2	nota
Unix	Unix	k1gInSc1	Unix
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
GNU	gnu	k1gMnSc1	gnu
Není	být	k5eNaImIp3nS	být
Unix	Unix	k1gInSc4	Unix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
GNU	gnu	k1gNnSc2	gnu
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
programátor	programátor	k1gMnSc1	programátor
Richard	Richard	k1gMnSc1	Richard
Stallman	Stallman	k1gMnSc1	Stallman
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
softwarový	softwarový	k2eAgInSc1d1	softwarový
vývoj	vývoj	k1gInSc1	vývoj
začal	začít	k5eAaPmAgInS	začít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
projektu	projekt	k1gInSc2	projekt
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
kompletní	kompletní	k2eAgInSc4d1	kompletní
svobodný	svobodný	k2eAgInSc4d1	svobodný
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
naplnit	naplnit	k5eAaPmF	naplnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
chybějící	chybějící	k2eAgFnSc1d1	chybějící
součást	součást	k1gFnSc1	součást
<g/>
,	,	kIx,	,
jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
kernel	kernel	k1gInSc1	kernel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněna	doplnit	k5eAaPmNgFnS	doplnit
nezávisle	závisle	k6eNd1	závisle
vytvořeným	vytvořený	k2eAgNnSc7d1	vytvořené
svobodným	svobodný	k2eAgNnSc7d1	svobodné
jádrem	jádro	k1gNnSc7	jádro
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
takovému	takový	k3xDgInSc3	takový
systému	systém	k1gInSc3	systém
často	často	k6eAd1	často
říká	říkat	k5eAaImIp3nS	říkat
Linux	linux	k1gInSc4	linux
<g/>
,	,	kIx,	,
přesnější	přesný	k2eAgNnSc4d2	přesnější
pojmenování	pojmenování	k1gNnSc4	pojmenování
je	být	k5eAaImIp3nS	být
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
GNU	gnu	k1gNnSc2	gnu
totiž	totiž	k9	totiž
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
jádra	jádro	k1gNnPc4	jádro
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jádro	jádro	k1gNnSc1	jádro
Hurd	hurda	k1gFnPc2	hurda
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
GNU	gnu	k1gNnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Hurd	hurda	k1gFnPc2	hurda
<g/>
)	)	kIx)	)
vyvíjené	vyvíjený	k2eAgFnSc2d1	vyvíjená
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gNnPc2	gnu
nebo	nebo	k8xC	nebo
jádro	jádro	k1gNnSc1	jádro
Solaris	Solaris	k1gFnSc2	Solaris
vyvinuté	vyvinutý	k2eAgFnSc2d1	vyvinutá
společností	společnost	k1gFnSc7	společnost
Sun	Sun	kA	Sun
Microsystems	Microsystemsa	k1gFnPc2	Microsystemsa
(	(	kIx(	(
<g/>
Nexenta	Nexenta	k1gFnSc1	Nexenta
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
řídí	řídit	k5eAaImIp3nS	řídit
chod	chod	k1gInSc4	chod
GNU	gnu	k1gNnSc2	gnu
Projektu	projekt	k1gInSc2	projekt
Free	Fre	k1gFnSc2	Fre
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
GNU	gnu	k1gMnSc1	gnu
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
licencí	licence	k1gFnPc2	licence
GPL	GPL	kA	GPL
a	a	k8xC	a
GFDL	GFDL	kA	GFDL
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
softwaru	software	k1gInSc2	software
a	a	k8xC	a
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
používala	používat	k5eAaImAgFnS	používat
právě	právě	k9	právě
licenci	licence	k1gFnSc4	licence
GFDL	GFDL	kA	GFDL
(	(	kIx(	(
<g/>
než	než	k8xS	než
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
licenci	licence	k1gFnSc4	licence
CC-BY-SA	CC-BY-SA	k1gFnSc2	CC-BY-SA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
licencí	licence	k1gFnPc2	licence
GNU	gnu	k1gNnSc2	gnu
je	být	k5eAaImIp3nS	být
LGPL	LGPL	kA	LGPL
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
chápaná	chápaný	k2eAgFnSc1d1	chápaná
jako	jako	k8xC	jako
knihovní	knihovní	k2eAgFnSc1d1	knihovní
licence	licence	k1gFnSc1	licence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využití	využití	k1gNnSc4	využití
i	i	k9	i
v	v	k7c6	v
softwaru	software	k1gInSc6	software
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
GNU	gnu	k1gNnSc4	gnu
používání	používání	k1gNnSc2	používání
této	tento	k3xDgFnSc2	tento
licence	licence	k1gFnSc2	licence
obecně	obecně	k6eAd1	obecně
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
(	(	kIx(	(
<g/>
...	...	k?	...
prosíme	prosit	k5eAaImIp1nP	prosit
<g/>
,	,	kIx,	,
uvažte	uvázat	k5eAaPmRp2nP	uvázat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
licenci	licence	k1gFnSc4	licence
LGPL	LGPL	kA	LGPL
nenahradit	nahradit	k5eNaPmF	nahradit
GPL	GPL	kA	GPL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
gnu	gnu	k1gNnSc1	gnu
<g/>
"	"	kIx"	"
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
znamená	znamenat	k5eAaImIp3nS	znamenat
též	též	k9	též
"	"	kIx"	"
<g/>
pakůň	pakůň	k1gMnSc1	pakůň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
pakůň	pakůň	k1gMnSc1	pakůň
vybrán	vybrat	k5eAaPmNgMnS	vybrat
maskotem	maskot	k1gInSc7	maskot
projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gNnSc2	gnu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
GNU	gnu	k1gMnSc1	gnu
projekt	projekt	k1gInSc4	projekt
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
<g/>
,	,	kIx,	,
existoval	existovat	k5eAaImAgInS	existovat
pouze	pouze	k6eAd1	pouze
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
Emacs	Emacsa	k1gFnPc2	Emacsa
s	s	k7c7	s
funkcionálním	funkcionální	k2eAgInSc7d1	funkcionální
programovacím	programovací	k2eAgInSc7d1	programovací
jazykem	jazyk	k1gInSc7	jazyk
Lisp	Lisp	k1gMnSc1	Lisp
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
příkazů	příkaz	k1gInPc2	příkaz
editoru	editor	k1gInSc2	editor
<g/>
,	,	kIx,	,
debugger	debugger	k1gInSc1	debugger
<g/>
,	,	kIx,	,
yaac	yaac	k6eAd1	yaac
kompatibilní	kompatibilní	k2eAgInSc4d1	kompatibilní
generátor	generátor	k1gInSc4	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
a	a	k8xC	a
linker	linkra	k1gFnPc2	linkra
(	(	kIx(	(
<g/>
sestavovací	sestavovací	k2eAgInSc1d1	sestavovací
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
GNU	gnu	k1gNnPc2	gnu
systém	systém	k1gInSc1	systém
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
svobodným	svobodný	k2eAgInSc7d1	svobodný
softwarem	software	k1gInSc7	software
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
vyvinout	vyvinout	k5eAaPmF	vyvinout
vlastní	vlastní	k2eAgInSc4d1	vlastní
překladač	překladač	k1gInSc4	překladač
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1987	[number]	k4	1987
se	se	k3xPyFc4	se
nashromáždily	nashromáždit	k5eAaPmAgFnP	nashromáždit
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
projektu	projekt	k1gInSc2	projekt
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
pro	pro	k7c4	pro
assembler	assembler	k1gInSc4	assembler
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
překladač	překladač	k1gInSc1	překladač
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
C	C	kA	C
(	(	kIx(	(
<g/>
GCC	GCC	kA	GCC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
(	(	kIx(	(
<g/>
GNU	gnu	k1gNnSc1	gnu
Emacs	Emacsa	k1gFnPc2	Emacsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
unixové	unixový	k2eAgInPc4d1	unixový
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
ls	ls	k?	ls
<g/>
,	,	kIx,	,
grep	grep	k1gInSc1	grep
<g/>
,	,	kIx,	,
awk	awk	k?	awk
<g/>
,	,	kIx,	,
make	make	k1gFnSc1	make
a	a	k8xC	a
ld	ld	k?	ld
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátečním	počáteční	k2eAgInSc6d1	počáteční
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
také	také	k9	také
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
další	další	k2eAgFnPc4d1	další
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dovést	dovést	k5eAaPmF	dovést
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
vhodného	vhodný	k2eAgNnSc2d1	vhodné
k	k	k7c3	k
ostrému	ostrý	k2eAgNnSc3d1	ostré
nasazení	nasazení	k1gNnSc3	nasazení
(	(	kIx(	(
<g/>
experimentálním	experimentální	k2eAgInSc7d1	experimentální
systémem	systém	k1gInSc7	systém
od	od	k7c2	od
GNU	gnu	k1gNnSc2	gnu
je	být	k5eAaImIp3nS	být
GNU	gnu	k1gNnSc1	gnu
Hurd	hurda	k1gFnPc2	hurda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
GNU	gnu	k1gNnSc4	gnu
nástroje	nástroj	k1gInPc1	nástroj
použity	použit	k2eAgInPc1d1	použit
společně	společně	k6eAd1	společně
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
Linux	Linux	kA	Linux
(	(	kIx(	(
<g/>
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
distribuce	distribuce	k1gFnSc1	distribuce
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
využívat	využívat	k5eAaImF	využívat
GNU	gnu	k1gNnSc4	gnu
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
množství	množství	k1gNnSc1	množství
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používaly	používat	k5eAaImAgFnP	používat
na	na	k7c6	na
ostatních	ostatní	k2eAgInPc6d1	ostatní
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
GNU	gnu	k1gNnSc6	gnu
prostředí	prostředí	k1gNnSc2	prostředí
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
spouštět	spouštět	k5eAaImF	spouštět
unixové	unixový	k2eAgInPc4d1	unixový
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
neztotožňoval	ztotožňovat	k5eNaImAgMnS	ztotožňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
GNU	gnu	k1gNnPc6	gnu
Manifest	manifest	k1gInSc4	manifest
==	==	k?	==
</s>
</p>
<p>
<s>
GNU	gnu	k1gNnSc1	gnu
Manifest	manifest	k1gInSc1	manifest
byl	být	k5eAaImAgInS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
Richardem	Richard	k1gMnSc7	Richard
Stallmanem	Stallman	k1gInSc7	Stallman
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
účasti	účast	k1gFnSc2	účast
ostatních	ostatní	k2eAgMnPc2d1	ostatní
na	na	k7c4	na
GNU	gnu	k1gNnSc4	gnu
Projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
GNU	gnu	k1gNnSc6	gnu
Manifestu	manifest	k1gInSc2	manifest
Stallman	Stallman	k1gMnSc1	Stallman
uvedl	uvést	k5eAaPmAgMnS	uvést
čtyři	čtyři	k4xCgInPc4	čtyři
body	bod	k1gInPc4	bod
svobodného	svobodný	k2eAgInSc2d1	svobodný
vývoje	vývoj	k1gInSc2	vývoj
počítačového	počítačový	k2eAgInSc2d1	počítačový
softwaru	software	k1gInSc2	software
<g/>
:	:	kIx,	:
svobodu	svoboda	k1gFnSc4	svoboda
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
studovat	studovat	k5eAaImF	studovat
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
programu	program	k1gInSc2	program
a	a	k8xC	a
upravovat	upravovat	k5eAaImF	upravovat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
redistribuce	redistribuce	k1gFnSc2	redistribuce
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
upravené	upravený	k2eAgFnPc4d1	upravená
verze	verze	k1gFnPc4	verze
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
naplnění	naplnění	k1gNnSc4	naplnění
těchto	tento	k3xDgInPc2	tento
bodů	bod	k1gInPc2	bod
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
uživatel	uživatel	k1gMnSc1	uživatel
plný	plný	k2eAgInSc4d1	plný
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
zdrojovým	zdrojový	k2eAgInPc3d1	zdrojový
kódům	kód	k1gInPc3	kód
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kód	kód	k1gInSc4	kód
zůstal	zůstat	k5eAaPmAgInS	zůstat
svobodný	svobodný	k2eAgInSc1d1	svobodný
a	a	k8xC	a
poskytovaný	poskytovaný	k2eAgInSc1d1	poskytovaný
veřejnosti	veřejnost	k1gFnSc6	veřejnost
Stallman	Stallman	k1gMnSc1	Stallman
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
GNU	gnu	k1gNnSc4	gnu
General	General	k1gFnSc2	General
Public	publicum	k1gNnPc2	publicum
License	License	k1gFnSc1	License
(	(	kIx(	(
<g/>
GPL	GPL	kA	GPL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
software	software	k1gInSc4	software
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
budoucí	budoucí	k2eAgFnPc4d1	budoucí
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
budou	být	k5eAaImBp3nP	být
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
<g/>
,	,	kIx,	,
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
zdarma	zdarma	k6eAd1	zdarma
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
aktivita	aktivita	k1gFnSc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
GNU	gnu	k1gNnSc2	gnu
projektů	projekt	k1gInPc2	projekt
čistě	čistě	k6eAd1	čistě
technického	technický	k2eAgNnSc2d1	technické
ražení	ražení	k1gNnSc2	ražení
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc1	filozofie
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
jako	jako	k9	jako
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
etická	etický	k2eAgFnSc1d1	etická
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
iniciativa	iniciativa	k1gFnSc1	iniciativa
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vytváření	vytváření	k1gNnSc3	vytváření
softwaru	software	k1gInSc2	software
a	a	k8xC	a
licencí	licence	k1gFnPc2	licence
<g/>
,	,	kIx,	,
GNU	gnu	k1gMnSc1	gnu
projekt	projekt	k1gInSc4	projekt
publikoval	publikovat	k5eAaBmAgMnS	publikovat
řadu	řada	k1gFnSc4	řada
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Richard	Richard	k1gMnSc1	Richard
Stallman	Stallman	k1gMnSc1	Stallman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
GNU	gnu	k1gNnSc2	gnu
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
všech	všecek	k3xTgInPc2	všecek
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
u	u	k7c2	u
každého	každý	k3xTgNnSc2	každý
je	být	k5eAaImIp3nS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
jaký	jaký	k3yRgInSc4	jaký
typ	typ	k1gInSc4	typ
vývojáře	vývojář	k1gMnSc4	vývojář
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
provádět	provádět	k5eAaImF	provádět
potřebné	potřebný	k2eAgInPc4d1	potřebný
úkoly	úkol	k1gInPc4	úkol
dané	daný	k2eAgFnSc2d1	daná
části	část	k1gFnSc2	část
GNU	gnu	k1gNnSc1	gnu
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
znalostí	znalost	k1gFnPc2	znalost
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
projekt	projekt	k1gInSc1	projekt
od	od	k7c2	od
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
znalosti	znalost	k1gFnPc4	znalost
z	z	k7c2	z
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyzýván	vyzýván	k2eAgInSc1d1	vyzýván
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
==	==	k?	==
</s>
</p>
<p>
<s>
GNU	gnu	k1gMnSc1	gnu
projekt	projekt	k1gInSc4	projekt
využívá	využívat	k5eAaPmIp3nS	využívat
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mohou	moct	k5eAaImIp3nP	moct
uživatelé	uživatel	k1gMnPc1	uživatel
volně	volně	k6eAd1	volně
kopírovat	kopírovat	k5eAaImF	kopírovat
<g/>
,	,	kIx,	,
upravovat	upravovat	k5eAaImF	upravovat
a	a	k8xC	a
distribuovat	distribuovat	k5eAaBmF	distribuovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
program	program	k1gInSc1	program
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
individuálním	individuální	k2eAgFnPc3d1	individuální
potřebám	potřeba	k1gFnPc3	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
programátoři	programátor	k1gMnPc1	programátor
získávají	získávat	k5eAaImIp3nP	získávat
svobodný	svobodný	k2eAgInSc4d1	svobodný
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakého	jaký	k3yRgInSc2	jaký
zdroje	zdroj	k1gInSc2	zdroj
jej	on	k3xPp3gInSc4	on
vezmou	vzít	k5eAaPmIp3nP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
programátorům	programátor	k1gMnPc3	programátor
poskytován	poskytovat	k5eAaImNgInS	poskytovat
od	od	k7c2	od
přátel	přítel	k1gMnPc2	přítel
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
zakoupit	zakoupit	k5eAaPmF	zakoupit
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
programátor	programátor	k1gMnSc1	programátor
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Financování	financování	k1gNnSc1	financování
==	==	k?	==
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
financován	financovat	k5eAaBmNgInS	financovat
výnosy	výnos	k1gInPc7	výnos
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
GNU	gnu	k1gNnSc2	gnu
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Copyleft	Copyleftum	k1gNnPc2	Copyleftum
==	==	k?	==
</s>
</p>
<p>
<s>
Copyleft	Copyleft	k1gMnSc1	Copyleft
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
volné	volný	k2eAgNnSc4d1	volné
použití	použití	k1gNnSc4	použití
přes	přes	k7c4	přes
více	hodně	k6eAd2	hodně
programátorů	programátor	k1gMnPc2	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Copyleft	Copyleft	k1gMnSc1	Copyleft
všem	všecek	k3xTgFnPc3	všecek
dává	dávat	k5eAaImIp3nS	dávat
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
<g/>
,	,	kIx,	,
změně	změna	k1gFnSc3	změna
a	a	k8xC	a
redistribuci	redistribuce	k1gFnSc3	redistribuce
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gInPc2	jejich
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
dokud	dokud	k6eAd1	dokud
se	se	k3xPyFc4	se
distribuční	distribuční	k2eAgFnPc1d1	distribuční
podmínky	podmínka	k1gFnPc1	podmínka
nezmění	změnit	k5eNaPmIp3nP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
legálně	legálně	k6eAd1	legálně
získá	získat	k5eAaPmIp3nS	získat
daný	daný	k2eAgInSc4d1	daný
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
program	program	k1gInSc4	program
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GNU	gnu	k1gMnSc1	gnu
projekt	projekt	k1gInSc1	projekt
a	a	k8xC	a
Free	Free	k1gFnSc1	Free
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
FSF	FSF	kA	FSF
<g/>
)	)	kIx)	)
občas	občas	k6eAd1	občas
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
mezi	mezi	k7c7	mezi
silným	silný	k2eAgInSc7d1	silný
a	a	k8xC	a
slabým	slabý	k2eAgInSc7d1	slabý
copyleftem	copyleft	k1gInSc7	copyleft
<g/>
.	.	kIx.	.
</s>
<s>
Slabý	slabý	k2eAgInSc1d1	slabý
copyleft	copyleft	k1gInSc1	copyleft
povoluje	povolovat	k5eAaImIp3nS	povolovat
použití	použití	k1gNnSc4	použití
svobodných	svobodný	k2eAgInPc2d1	svobodný
programů	program	k1gInPc2	program
do	do	k7c2	do
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
samy	sám	k3xTgInPc1	sám
nejsou	být	k5eNaImIp3nP	být
svobodné	svobodný	k2eAgInPc1d1	svobodný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
silný	silný	k2eAgInSc1d1	silný
copyleft	copyleft	k1gInSc1	copyleft
toto	tento	k3xDgNnSc4	tento
použití	použití	k1gNnSc4	použití
striktně	striktně	k6eAd1	striktně
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
GNU	gnu	k1gNnSc2	gnu
Projektů	projekt	k1gInPc2	projekt
je	být	k5eAaImIp3nS	být
distribuována	distribuován	k2eAgFnSc1d1	distribuována
pod	pod	k7c7	pod
silnou	silný	k2eAgFnSc7d1	silná
copyleft	copyleft	k1gInSc1	copyleft
licencí	licence	k1gFnPc2	licence
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
vydávány	vydávat	k5eAaImNgInP	vydávat
pod	pod	k7c7	pod
slabou	slabý	k2eAgFnSc7d1	slabá
copyleft	copyleft	k1gInSc1	copyleft
licencí	licence	k1gFnPc2	licence
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
free	free	k6eAd1	free
software	software	k1gInSc1	software
licenci	licence	k1gFnSc4	licence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
GNU	gnu	k1gMnSc1	gnu
GPL	GPL	kA	GPL
</s>
</p>
<p>
<s>
GNU	gnu	k1gMnSc1	gnu
FDL	FDL	kA	FDL
</s>
</p>
<p>
<s>
GNU	gnu	k1gMnSc1	gnu
LGPL	LGPL	kA	LGPL
</s>
</p>
<p>
<s>
Free	Free	k6eAd1	Free
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
www.gnu.org	www.gnu.org	k1gInSc1	www.gnu.org
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
domovské	domovský	k2eAgFnSc2d1	domovská
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gNnSc2	gnu
</s>
</p>
<p>
<s>
www.gnu.cz	www.gnu.cz	k1gInSc4	www.gnu.cz
–	–	k?	–
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
GNU	gnu	k1gNnSc6	gnu
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
GNU	gnu	k1gMnSc1	gnu
–	–	k?	–
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
programů	program	k1gInPc2	program
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Smysl	smysl	k1gInSc1	smysl
Existence	existence	k1gFnSc1	existence
GNU	gnu	k1gMnSc1	gnu
Linuxu	linux	k1gInSc2	linux
–	–	k?	–
video	video	k1gNnSc1	video
(	(	kIx(	(
<g/>
422	[number]	k4	422
MB	MB	kA	MB
<g/>
)	)	kIx)	)
</s>
</p>
