<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
českých	český	k2eAgNnPc2d1	české
piv	pivo	k1gNnPc2	pivo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
česká	český	k2eAgFnSc1d1	Česká
odrůda	odrůda	k1gFnSc1	odrůda
chmele	chmel	k1gInSc2	chmel
tzv.	tzv.	kA	tzv.
žatecký	žatecký	k2eAgInSc1d1	žatecký
poloraný	poloraný	k2eAgInSc1d1	poloraný
červeňák	červeňák	k1gInSc1	červeňák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c4	mezi
nejkvalitnější	kvalitní	k2eAgInSc4d3	nejkvalitnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
