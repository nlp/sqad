<p>
<s>
Shiyaly	Shiyal	k1gInPc1	Shiyal
Ramamrita	Ramamrita	k1gFnSc1	Ramamrita
Ranganathan	Ranganathan	k1gInSc1	Ranganathan
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1892	[number]	k4	1892
Sirkazhi	Sirkazh	k1gFnSc2	Sirkazh
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1972	[number]	k4	1972
Bangalúr	Bangalúra	k1gFnPc2	Bangalúra
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
indický	indický	k2eAgMnSc1d1	indický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zejména	zejména	k9	zejména
dvojtečkovou	dvojtečkový	k2eAgFnSc4d1	dvojtečková
klasifikaci	klasifikace	k1gFnSc4	klasifikace
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
tak	tak	k6eAd1	tak
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
otcem	otec	k1gMnSc7	otec
indické	indický	k2eAgFnSc2d1	indická
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<s desamb="1">
<p>
<s>
==	==	k?	==
Rané	raný	k2eAgNnSc4d1	rané
období	období	k1gNnSc4	období
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc4	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Shiyaly	Shiyal	k1gInPc1	Shiyal
Ramamrita	Ramamrita	k1gFnSc1	Ramamrita
Ranganathan	Ranganathan	k1gInSc1	Ranganathan
Iyer	Iyer	k1gInSc1	Iyer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1892	[number]	k4	1892
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgNnSc4d1	oficiální
datum	datum	k1gNnSc4	datum
se	se	k3xPyFc4	se
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Shiyaly	Shiyala	k1gFnSc2	Shiyala
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tanjavoor	Tanjavoora	k1gFnPc2	Tanjavoora
<g/>
,	,	kIx,	,
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
státě	stát	k1gInSc6	stát
Madras	madras	k1gInSc1	madras
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
N.	N.	kA	N.
Ramamrity	Ramamrita	k1gFnPc1	Ramamrita
Iyera	Iyera	k1gMnSc1	Iyera
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
a	a	k8xC	a
Seethalakshmi	Seethalaksh	k1gFnPc7	Seethalaksh
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
bráhmánské	bráhmánský	k2eAgFnSc3d1	bráhmánský
komunitě	komunita	k1gFnSc3	komunita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vážená	vážený	k2eAgFnSc1d1	Vážená
<g/>
.	.	kIx.	.
</s>
<s>
Ranganathan	Ranganathan	k1gInSc1	Ranganathan
měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgMnPc4	dva
bratry	bratr	k1gMnPc4	bratr
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
trpěl	trpět	k5eAaImAgMnS	trpět
častými	častý	k2eAgInPc7d1	častý
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
slabou	slabý	k2eAgFnSc7d1	slabá
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
konstitucí	konstituce	k1gFnSc7	konstituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
zemědělec	zemědělec	k1gMnSc1	zemědělec
<g/>
,	,	kIx,	,
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
rýžová	rýžový	k2eAgNnPc4d1	rýžové
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
platil	platit	k5eAaImAgInS	platit
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
vzdělaného	vzdělaný	k2eAgMnSc4d1	vzdělaný
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pravidelně	pravidelně	k6eAd1	pravidelně
předčítal	předčítat	k5eAaImAgMnS	předčítat
a	a	k8xC	a
vykládal	vykládat	k5eAaImAgMnS	vykládat
pasáže	pasáž	k1gFnPc4	pasáž
z	z	k7c2	z
Rámájany	Rámájana	k1gFnSc2	Rámájana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
duchu	duch	k1gMnSc6	duch
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zemřel	zemřít	k5eAaPmAgMnS	zemřít
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Ranganathanovi	Ranganathanův	k2eAgMnPc1d1	Ranganathanův
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
učil	učít	k5eAaPmAgMnS	učít
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
,	,	kIx,	,
projevil	projevit	k5eAaPmAgInS	projevit
hluboký	hluboký	k2eAgInSc1d1	hluboký
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
hinduismu	hinduismus	k1gInSc6	hinduismus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
filozofii	filozofie	k1gFnSc4	filozofie
i	i	k8xC	i
náboženské	náboženský	k2eAgInPc4d1	náboženský
aspekty	aspekt	k1gInPc4	aspekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
zaujetí	zaujetí	k1gNnSc1	zaujetí
jej	on	k3xPp3gNnSc2	on
neopustilo	opustit	k5eNaPmAgNnS	opustit
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Ranganathan	Ranganathan	k1gInSc4	Ranganathan
Sabhanayak	Sabhanayak	k1gMnSc1	Sabhanayak
Mudaliar	Mudaliar	k1gMnSc1	Mudaliar
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hindu	hind	k1gMnSc3	hind
High	High	k1gInSc1	High
School	School	k1gInSc1	School
v	v	k7c6	v
Shiyaly	Shiyala	k1gFnSc2	Shiyala
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Madras	madras	k1gInSc4	madras
Christian	Christian	k1gMnSc1	Christian
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
nejprve	nejprve	k6eAd1	nejprve
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
i	i	k8xC	i
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
matematika	matematik	k1gMnSc2	matematik
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgInS	navázat
zde	zde	k6eAd1	zde
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
E.	E.	kA	E.
B.	B.	kA	B.
Rossem	Ross	k1gMnSc7	Ross
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
i	i	k8xC	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
celkem	celkem	k6eAd1	celkem
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Rukmani	Rukman	k1gMnPc1	Rukman
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
jménem	jméno	k1gNnSc7	jméno
Sarada	Sarada	k1gFnSc1	Sarada
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
svého	svůj	k3xOyFgMnSc4	svůj
jediného	jediný	k2eAgMnSc4d1	jediný
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
později	pozdě	k6eAd2	pozdě
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
i	i	k9	i
Nadaci	nadace	k1gFnSc4	nadace
pro	pro	k7c4	pro
knihovní	knihovní	k2eAgFnSc4d1	knihovní
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
věnoval	věnovat	k5eAaImAgInS	věnovat
své	svůj	k3xOyFgFnPc4	svůj
životní	životní	k2eAgFnPc4d1	životní
úspory	úspora	k1gFnPc4	úspora
-	-	kIx~	-
Sarada	Sarada	k1gFnSc1	Sarada
Ranganathan	Ranganathan	k1gMnSc1	Ranganathan
Endowment	Endowment	k1gInSc1	Endowment
for	forum	k1gNnPc2	forum
Library	Librara	k1gFnSc2	Librara
Science	Science	k1gFnSc1	Science
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studia	studio	k1gNnSc2	studio
získal	získat	k5eAaPmAgInS	získat
oprávnění	oprávnění	k1gNnPc4	oprávnění
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vyučovat	vyučovat	k5eAaImF	vyučovat
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
několika	několik	k4yIc6	několik
státních	státní	k2eAgFnPc6d1	státní
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zlepšit	zlepšit	k5eAaPmF	zlepšit
špatné	špatný	k2eAgNnSc4d1	špatné
postavení	postavení	k1gNnSc4	postavení
učitelské	učitelský	k2eAgFnSc2d1	učitelská
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neshody	neshoda	k1gFnPc1	neshoda
a	a	k8xC	a
nízký	nízký	k2eAgInSc1d1	nízký
plat	plat	k1gInSc1	plat
jej	on	k3xPp3gMnSc4	on
donutily	donutit	k5eAaPmAgFnP	donutit
dráhy	dráha	k1gFnPc1	dráha
učitele	učitel	k1gMnPc4	učitel
zanechat	zanechat	k5eAaPmF	zanechat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k7c2	místo
knihovníka	knihovník	k1gMnSc2	knihovník
Madraské	Madraský	k2eAgFnSc2d1	Madraský
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
zápal	zápal	k1gInSc1	zápal
pro	pro	k7c4	pro
knihovnictví	knihovnictví	k1gNnSc4	knihovnictví
mu	on	k3xPp3gMnSc3	on
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgInS	získat
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
knihovnické	knihovnický	k2eAgNnSc4d1	knihovnické
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
navázal	navázat	k5eAaPmAgInS	navázat
opět	opět	k6eAd1	opět
trvalý	trvalý	k2eAgInSc1d1	trvalý
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
mentorem	mentor	k1gMnSc7	mentor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
<g/>
,	,	kIx,	,
W.	W.	kA	W.
C.	C.	kA	C.
B.	B.	kA	B.
Sawyersem	Sawyers	k1gInSc7	Sawyers
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
systém	systém	k1gInSc1	systém
knihoven	knihovna	k1gFnPc2	knihovna
jej	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
a	a	k8xC	a
toužil	toužit	k5eAaImAgMnS	toužit
pozvednout	pozvednout	k5eAaPmF	pozvednout
úroveň	úroveň	k1gFnSc4	úroveň
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodné	rodný	k2eAgFnSc6d1	rodná
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
klasifikačním	klasifikační	k2eAgInSc6d1	klasifikační
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
dvojtečkovou	dvojtečkový	k2eAgFnSc7d1	dvojtečková
klasifikací	klasifikace	k1gFnSc7	klasifikace
<g/>
"	"	kIx"	"
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgNnSc4d1	ostatní
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
používané	používaný	k2eAgInPc4d1	používaný
systémy	systém	k1gInPc4	systém
jako	jako	k8xS	jako
MDT	MDT	kA	MDT
<g/>
,	,	kIx,	,
DDC	DDC	kA	DDC
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ranganathan	Ranganathan	k1gMnSc1	Ranganathan
chtěl	chtít	k5eAaImAgMnS	chtít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
zejména	zejména	k9	zejména
rozšířit	rozšířit	k5eAaPmF	rozšířit
působnost	působnost	k1gFnSc4	působnost
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
knihy	kniha	k1gFnPc4	kniha
co	co	k9	co
nejširší	široký	k2eAgFnSc3d3	nejširší
čtenářské	čtenářský	k2eAgFnSc3d1	čtenářská
obci	obec	k1gFnSc3	obec
a	a	k8xC	a
pozvednout	pozvednout	k5eAaPmF	pozvednout
knihovní	knihovní	k2eAgFnSc4d1	knihovní
vědu	věda	k1gFnSc4	věda
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
založil	založit	k5eAaPmAgMnS	založit
Madranskou	Madranský	k2eAgFnSc4d1	Madranský
knihovnickou	knihovnický	k2eAgFnSc4d1	knihovnická
asociaci	asociace	k1gFnSc4	asociace
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944-1953	[number]	k4	1944-1953
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
jejího	její	k3xOp3gMnSc2	její
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
také	také	k9	také
zavádět	zavádět	k5eAaImF	zavádět
obor	obor	k1gInSc4	obor
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
do	do	k7c2	do
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
vybudovat	vybudovat	k5eAaPmF	vybudovat
DRTC	DRTC	kA	DRTC
-	-	kIx~	-
Documentation	Documentation	k1gInSc1	Documentation
Research	Research	k1gInSc1	Research
and	and	k?	and
Training	Training	k1gInSc1	Training
Centre	centr	k1gInSc5	centr
v	v	k7c6	v
Bangalore	Bangalor	k1gInSc5	Bangalor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hojně	hojně	k6eAd1	hojně
také	také	k9	také
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
vydal	vydat	k5eAaPmAgMnS	vydat
základní	základní	k2eAgFnSc3d1	základní
koncepci	koncepce	k1gFnSc3	koncepce
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pak	pak	k6eAd1	pak
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dalších	další	k2eAgInPc6d1	další
dílech	díl	k1gInPc6	díl
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
-	-	kIx~	-
Pět	pět	k4xCc4	pět
zákonů	zákon	k1gInPc2	zákon
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
Five	Five	k1gInSc1	Five
laws	laws	k1gInSc1	laws
of	of	k?	of
library	librara	k1gFnSc2	librara
science	science	k1gFnSc2	science
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
kompletní	kompletní	k2eAgFnSc4d1	kompletní
Dvojtečkovou	dvojtečkový	k2eAgFnSc4d1	dvojtečková
klasifikaci	klasifikace	k1gFnSc4	klasifikace
(	(	kIx(	(
<g/>
Colon	colon	k1gNnSc4	colon
classification	classification	k1gInSc4	classification
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Těmto	tento	k3xDgFnPc3	tento
oblastem	oblast	k1gFnPc3	oblast
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
napsal	napsat	k5eAaPmAgMnS	napsat
60	[number]	k4	60
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
2000	[number]	k4	2000
článků	článek	k1gInPc2	článek
a	a	k8xC	a
komentářů	komentář	k1gInPc2	komentář
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
silné	silný	k2eAgFnSc2d1	silná
sítě	síť	k1gFnSc2	síť
knihoven	knihovna	k1gFnPc2	knihovna
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
uplatňoval	uplatňovat	k5eAaImAgMnS	uplatňovat
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
vliv	vliv	k1gInSc4	vliv
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
i	i	k9	i
ve	v	k7c6	v
vládních	vládní	k2eAgInPc6d1	vládní
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Ranganathanovu	Ranganathanův	k2eAgFnSc4d1	Ranganathanův
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c4	o
dvojtečkovou	dvojtečkový	k2eAgFnSc4d1	dvojtečková
klasifikaci	klasifikace	k1gFnSc4	klasifikace
<g/>
,	,	kIx,	,
značný	značný	k2eAgInSc4d1	značný
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
byl	být	k5eAaImAgMnS	být
zván	zvát	k5eAaImNgMnS	zvát
a	a	k8xC	a
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
navštívil	navštívit	k5eAaPmAgMnS	navštívit
USA	USA	kA	USA
a	a	k8xC	a
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
opět	opět	k6eAd1	opět
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
D.	D.	kA	D.
J.	J.	kA	J.
Foskettem	Foskett	k1gMnSc7	Foskett
<g/>
,	,	kIx,	,
B.	B.	kA	B.
I	i	k8xC	i
Palmerem	Palmero	k1gNnSc7	Palmero
a	a	k8xC	a
A.	A.	kA	A.
J.	J.	kA	J.
Wellsem	Wells	k1gMnSc7	Wells
založil	založit	k5eAaPmAgMnS	založit
Skupinu	skupina	k1gFnSc4	skupina
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
klasifikace	klasifikace	k1gFnSc2	klasifikace
(	(	kIx(	(
<g/>
Classification	Classification	k1gInSc1	Classification
research	research	k1gMnSc1	research
group	group	k1gMnSc1	group
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953-1956	[number]	k4	1953-1956
byl	být	k5eAaImAgMnS	být
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1955	[number]	k4	1955
až	až	k9	až
1957	[number]	k4	1957
žil	žíla	k1gFnPc2	žíla
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
<g/>
,	,	kIx,	,
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
konferencí	konference	k1gFnSc7	konference
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgInS	obdržet
také	také	k9	také
mnohá	mnohý	k2eAgNnPc4d1	mnohé
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
International	International	k1gFnPc2	International
Federation	Federation	k1gInSc1	Federation
for	forum	k1gNnPc2	forum
Information	Information	k1gInSc1	Information
and	and	k?	and
Documentation	Documentation	k1gInSc1	Documentation
(	(	kIx(	(
<g/>
FID	FID	kA	FID
<g/>
)	)	kIx)	)
a	a	k8xC	a
indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
mu	on	k3xPp3gMnSc3	on
udělila	udělit	k5eAaPmAgFnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
Národního	národní	k2eAgMnSc2d1	národní
profesora	profesor	k1gMnSc2	profesor
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
práci	práce	k1gFnSc3	práce
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc3	tvorba
nových	nový	k2eAgInPc2d1	nový
konceptů	koncept	k1gInPc2	koncept
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dvojtečkové	dvojtečkový	k2eAgNnSc1d1	dvojtečkové
třídění	třídění	k1gNnSc1	třídění
</s>
</p>
