<s>
Shiyaly	Shiyal	k1gInPc1	Shiyal
Ramamrita	Ramamrita	k1gFnSc1	Ramamrita
Ranganathan	Ranganathan	k1gInSc1	Ranganathan
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1892	[number]	k4	1892
Sirkazhi	Sirkazh	k1gFnSc2	Sirkazh
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1972	[number]	k4	1972
Bangalúr	Bangalúra	k1gFnPc2	Bangalúra
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
indický	indický	k2eAgMnSc1d1	indický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zejména	zejména	k9	zejména
dvojtečkovou	dvojtečkový	k2eAgFnSc4d1	dvojtečková
klasifikaci	klasifikace	k1gFnSc4	klasifikace
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
tak	tak	k6eAd1	tak
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
otcem	otec	k1gMnSc7	otec
indické	indický	k2eAgFnSc2d1	indická
knihovní	knihovní	k2eAgFnSc2d1	knihovní
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
