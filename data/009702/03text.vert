<p>
<s>
Matematika	matematika	k1gFnSc1	matematika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
μ	μ	k?	μ
(	(	kIx(	(
<g/>
mathematikós	mathematikós	k1gInSc1	mathematikós
<g/>
)	)	kIx)	)
=	=	kIx~	=
milující	milující	k2eAgNnSc1d1	milující
poznání	poznání	k1gNnSc1	poznání
<g/>
;	;	kIx,	;
μ	μ	k?	μ
(	(	kIx(	(
<g/>
máthema	máthema	k1gFnSc1	máthema
<g/>
)	)	kIx)	)
=	=	kIx~	=
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
vědění	vědění	k1gNnSc1	vědění
<g/>
,	,	kIx,	,
poznání	poznání	k1gNnSc1	poznání
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
z	z	k7c2	z
formálního	formální	k2eAgNnSc2d1	formální
hlediska	hledisko	k1gNnSc2	hledisko
kvantitou	kvantita	k1gFnSc7	kvantita
<g/>
,	,	kIx,	,
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
prostorem	prostor	k1gInSc7	prostor
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Matematika	matematika	k1gFnSc1	matematika
je	být	k5eAaImIp3nS	být
též	též	k9	též
popisována	popisovat	k5eAaImNgFnS	popisovat
jako	jako	k9	jako
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vytvářením	vytváření	k1gNnSc7	vytváření
abstraktních	abstraktní	k2eAgFnPc2d1	abstraktní
entit	entita	k1gFnPc2	entita
a	a	k8xC	a
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
zákonitých	zákonitý	k2eAgInPc2d1	zákonitý
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matematika	matematika	k1gFnSc1	matematika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
a	a	k8xC	a
budována	budován	k2eAgFnSc1d1	budována
jako	jako	k8xS	jako
exaktní	exaktní	k2eAgFnSc1d1	exaktní
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
exaktnost	exaktnost	k1gFnSc1	exaktnost
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiných	jiný	k2eAgFnPc2d1	jiná
exaktních	exaktní	k2eAgFnPc2d1	exaktní
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
že	že	k8xS	že
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
matematické	matematický	k2eAgInPc1d1	matematický
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
operace	operace	k1gFnPc1	operace
nad	nad	k7c7	nad
nimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
exaktně	exaktně	k6eAd1	exaktně
vytyčeny	vytyčen	k2eAgInPc1d1	vytyčen
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
vágností	vágnost	k1gFnSc7	vágnost
<g/>
)	)	kIx)	)
,	,	kIx,	,
tedy	tedy	k9	tedy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
(	(	kIx(	(
<g/>
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
exaktní	exaktní	k2eAgFnSc6d1	exaktní
vědě	věda	k1gFnSc6	věda
<g/>
)	)	kIx)	)
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
člověk	člověk	k1gMnSc1	člověk
naprosto	naprosto	k6eAd1	naprosto
přesně	přesně	k6eAd1	přesně
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
pochyb	pochyba	k1gFnPc2	pochyba
<g/>
)	)	kIx)	)
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
podstata	podstata	k1gFnSc1	podstata
exaktnosti	exaktnost	k1gFnSc2	exaktnost
této	tento	k3xDgFnSc2	tento
disciplíny	disciplína	k1gFnSc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
matematiky	matematika	k1gFnSc2	matematika
existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
jinak	jinak	k6eAd1	jinak
chápaná	chápaný	k2eAgFnSc1d1	chápaná
exaktnost	exaktnost	k1gFnSc1	exaktnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
exaktnost	exaktnost	k1gFnSc1	exaktnost
použitých	použitý	k2eAgFnPc2d1	použitá
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
výsledků	výsledek	k1gInPc2	výsledek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
exaktní	exaktní	k2eAgNnSc4d1	exaktní
a	a	k8xC	a
neexaktní	exaktní	k2eNgNnSc4d1	neexaktní
řešení	řešení	k1gNnSc4	řešení
<g/>
:	:	kIx,	:
Některé	některý	k3yIgFnPc1	některý
aplikace	aplikace	k1gFnPc1	aplikace
jsou	být	k5eAaImIp3nP	být
řešitelné	řešitelný	k2eAgFnPc1d1	řešitelná
pouze	pouze	k6eAd1	pouze
opuštěním	opuštění	k1gNnSc7	opuštění
přísného	přísný	k2eAgInSc2d1	přísný
a	a	k8xC	a
omezujícího	omezující	k2eAgInSc2d1	omezující
požadavku	požadavek	k1gInSc2	požadavek
exaktnosti	exaktnost	k1gFnSc2	exaktnost
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
matematická	matematický	k2eAgFnSc1d1	matematická
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
exaktním	exaktní	k2eAgMnSc7d1	exaktní
<g/>
)	)	kIx)	)
řešením	řešení	k1gNnSc7	řešení
dané	daný	k2eAgFnSc2d1	daná
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
ale	ale	k8xC	ale
existovat	existovat	k5eAaImF	existovat
posloupnost	posloupnost	k1gFnSc4	posloupnost
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
přesností	přesnost	k1gFnSc7	přesnost
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
však	však	k9	však
exaktně	exaktně	k6eAd1	exaktně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řešením	řešení	k1gNnSc7	řešení
té	ten	k3xDgFnSc2	ten
rovnice	rovnice	k1gFnSc2	rovnice
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
exaktního	exaktní	k2eAgInSc2d1	exaktní
výsledku	výsledek	k1gInSc2	výsledek
(	(	kIx(	(
<g/>
řešení	řešení	k1gNnPc4	řešení
<g/>
)	)	kIx)	)
do	do	k7c2	do
výchozího	výchozí	k2eAgInSc2d1	výchozí
vztahu	vztah	k1gInSc2	vztah
(	(	kIx(	(
<g/>
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
)	)	kIx)	)
dostáváme	dostávat	k5eAaImIp1nP	dostávat
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Neexaktní	exaktní	k2eNgInSc1d1	neexaktní
výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
od	od	k7c2	od
exaktního	exaktní	k2eAgNnSc2d1	exaktní
liší	lišit	k5eAaImIp3nS	lišit
o	o	k7c4	o
"	"	kIx"	"
<g/>
chybu	chyba	k1gFnSc4	chyba
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dosazení	dosazení	k1gNnSc6	dosazení
identitu	identita	k1gFnSc4	identita
nedostaneme	dostat	k5eNaPmIp1nP	dostat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
vlastností	vlastnost	k1gFnSc7	vlastnost
matematiky	matematika	k1gFnSc2	matematika
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
absolutní	absolutní	k2eAgFnSc4d1	absolutní
přesnost	přesnost	k1gFnSc4	přesnost
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
nezpochybnitelnost	nezpochybnitelnost	k1gFnSc4	nezpochybnitelnost
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
matematiku	matematika	k1gFnSc4	matematika
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vědních	vědní	k2eAgFnPc2d1	vědní
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
již	již	k9	již
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
řeckého	řecký	k2eAgMnSc2d1	řecký
matematika	matematik	k1gMnSc2	matematik
Euklida	Euklides	k1gMnSc2	Euklides
Základy	základ	k1gInPc1	základ
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Široké	Široké	k2eAgFnSc3d1	Široké
veřejnosti	veřejnost	k1gFnSc3	veřejnost
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
tzv.	tzv.	kA	tzv.
elementární	elementární	k2eAgFnSc1d1	elementární
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
operováním	operování	k1gNnSc7	operování
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
řešením	řešení	k1gNnSc7	řešení
praktických	praktický	k2eAgFnPc2d1	praktická
úloh	úloha	k1gFnPc2	úloha
<g/>
,	,	kIx,	,
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
rovnic	rovnice	k1gFnPc2	rovnice
a	a	k8xC	a
popisem	popis	k1gInSc7	popis
základních	základní	k2eAgInPc2d1	základní
geometrických	geometrický	k2eAgInPc2d1	geometrický
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
informatice	informatika	k1gFnSc6	informatika
<g/>
,	,	kIx,	,
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
ekonomii	ekonomie	k1gFnSc6	ekonomie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
oborech	obor	k1gInPc6	obor
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívají	využívat	k5eAaPmIp3nP	využívat
výsledky	výsledek	k1gInPc1	výsledek
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
těmito	tento	k3xDgInPc7	tento
obory	obor	k1gInPc7	obor
zpětně	zpětně	k6eAd1	zpětně
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
čistá	čistý	k2eAgFnSc1d1	čistá
matematika	matematika	k1gFnSc1	matematika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pouze	pouze	k6eAd1	pouze
vysoce	vysoce	k6eAd1	vysoce
abstraktními	abstraktní	k2eAgInPc7d1	abstraktní
pojmy	pojem	k1gInPc7	pojem
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
definování	definování	k1gNnSc4	definování
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
motivováno	motivovat	k5eAaBmNgNnS	motivovat
praktickým	praktický	k2eAgInSc7d1	praktický
užitkem	užitek	k1gInSc7	užitek
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
obory	obor	k1gInPc1	obor
čisté	čistý	k2eAgFnSc2d1	čistá
matematiky	matematika	k1gFnSc2	matematika
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
s	s	k7c7	s
logikou	logika	k1gFnSc7	logika
či	či	k8xC	či
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc1	charakteristikon
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
cílů	cíl	k1gInPc2	cíl
matematiky	matematika	k1gFnSc2	matematika
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgFnPc7d1	jiná
vědami	věda	k1gFnPc7	věda
se	se	k3xPyFc4	se
matematika	matematika	k1gFnSc1	matematika
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
mírou	míra	k1gFnSc7	míra
abstrakce	abstrakce	k1gFnSc2	abstrakce
a	a	k8xC	a
přesnosti	přesnost	k1gFnSc2	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
vlastnostem	vlastnost	k1gFnPc3	vlastnost
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
královnu	královna	k1gFnSc4	královna
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
matematický	matematický	k2eAgInSc1d1	matematický
důkaz	důkaz	k1gInSc1	důkaz
je	být	k5eAaImIp3nS	být
nejspolehlivější	spolehlivý	k2eAgInSc1d3	nejspolehlivější
známý	známý	k2eAgInSc1d1	známý
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ověřovat	ověřovat	k5eAaImF	ověřovat
pravdivost	pravdivost	k1gFnSc4	pravdivost
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
považována	považován	k2eAgFnSc1d1	považována
pouze	pouze	k6eAd1	pouze
ta	ten	k3xDgNnPc4	ten
tvrzení	tvrzení	k1gNnPc4	tvrzení
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
matematický	matematický	k2eAgInSc1d1	matematický
důkaz	důkaz	k1gInSc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
pojmy	pojem	k1gInPc1	pojem
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
jednoznačnými	jednoznačný	k2eAgFnPc7d1	jednoznačná
definicemi	definice	k1gFnPc7	definice
z	z	k7c2	z
pojmů	pojem	k1gInPc2	pojem
již	již	k6eAd1	již
zavedených	zavedený	k2eAgFnPc2d1	zavedená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
matematiku	matematika	k1gFnSc4	matematika
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
,	,	kIx,	,
zajišťovaná	zajišťovaný	k2eAgFnSc1d1	zajišťovaná
úplnou	úplný	k2eAgFnSc7d1	úplná
formalizací	formalizace	k1gFnSc7	formalizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
několik	několik	k4yIc4	několik
základních	základní	k2eAgNnPc2d1	základní
tvrzení	tvrzení	k1gNnPc2	tvrzení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
axiomy	axiom	k1gInPc1	axiom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
možné	možný	k2eAgFnPc1d1	možná
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
odvozovacích	odvozovací	k2eAgNnPc2d1	odvozovací
pravidel	pravidlo	k1gNnPc2	pravidlo
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c6	na
logice	logika	k1gFnSc6	logika
odvodit	odvodit	k5eAaPmF	odvodit
další	další	k2eAgNnPc4d1	další
pravdivá	pravdivý	k2eAgNnPc4d1	pravdivé
tvrzení	tvrzení	k1gNnPc4	tvrzení
pomocí	pomocí	k7c2	pomocí
formálních	formální	k2eAgInPc2d1	formální
důkazů	důkaz	k1gInPc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
matematických	matematický	k2eAgInPc2d1	matematický
poznatků	poznatek	k1gInPc2	poznatek
tak	tak	k6eAd1	tak
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
definování	definování	k1gNnSc4	definování
nových	nový	k2eAgInPc2d1	nový
pojmů	pojem	k1gInPc2	pojem
<g/>
,	,	kIx,	,
formulování	formulování	k1gNnSc1	formulování
platných	platný	k2eAgFnPc2d1	platná
vět	věta	k1gFnPc2	věta
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
takových	takový	k3xDgFnPc2	takový
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
je	on	k3xPp3gNnSc4	on
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
pojmy	pojem	k1gInPc7	pojem
staršími	starý	k2eAgInPc7d2	starší
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokazování	dokazování	k1gNnSc3	dokazování
pravdivosti	pravdivost	k1gFnSc2	pravdivost
těchto	tento	k3xDgFnPc2	tento
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Matematické	matematický	k2eAgFnPc1d1	matematická
práce	práce	k1gFnPc1	práce
mají	mít	k5eAaImIp3nP	mít
proto	proto	k8xC	proto
často	často	k6eAd1	často
strukturu	struktura	k1gFnSc4	struktura
"	"	kIx"	"
<g/>
definice	definice	k1gFnSc1	definice
–	–	k?	–
věta	věta	k1gFnSc1	věta
–	–	k?	–
důkaz	důkaz	k1gInSc1	důkaz
<g/>
"	"	kIx"	"
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
doplňujícího	doplňující	k2eAgInSc2d1	doplňující
textu	text	k1gInSc2	text
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
vědních	vědní	k2eAgFnPc6d1	vědní
disciplínách	disciplína	k1gFnPc6	disciplína
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
formulace	formulace	k1gFnSc1	formulace
neověřené	ověřený	k2eNgFnSc2d1	neověřená
hypotézy	hypotéza	k1gFnSc2	hypotéza
-	-	kIx~	-
předpokladu	předpoklad	k1gInSc2	předpoklad
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
dokázání	dokázání	k1gNnSc3	dokázání
či	či	k8xC	či
vyvrácení	vyvrácení	k1gNnSc3	vyvrácení
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
položení	položení	k1gNnSc1	položení
dosud	dosud	k6eAd1	dosud
nezodpovězené	zodpovězený	k2eNgFnPc4d1	nezodpovězená
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
matematikou	matematika	k1gFnSc7	matematika
vytvářených	vytvářený	k2eAgInPc2d1	vytvářený
abstraktních	abstraktní	k2eAgInPc2d1	abstraktní
pojmů	pojem	k1gInPc2	pojem
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
či	či	k8xC	či
snadnějšímu	snadný	k2eAgNnSc3d2	snazší
uchopení	uchopení	k1gNnSc3	uchopení
pojmů	pojem	k1gInPc2	pojem
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
slouží	sloužit	k5eAaImIp3nP	sloužit
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
vědních	vědní	k2eAgInPc6d1	vědní
oborech	obor	k1gInPc6	obor
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc4	nástroj
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
určitých	určitý	k2eAgInPc2d1	určitý
jevů	jev	k1gInPc2	jev
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
idealizovaný	idealizovaný	k2eAgInSc4d1	idealizovaný
model	model	k1gInSc4	model
reálných	reálný	k2eAgInPc2d1	reálný
objektů	objekt	k1gInPc2	objekt
či	či	k8xC	či
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc4d1	další
pak	pak	k6eAd1	pak
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
precizaci	precizace	k1gFnSc4	precizace
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
konceptů	koncept	k1gInPc2	koncept
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
některých	některý	k3yIgFnPc2	některý
disciplín	disciplína	k1gFnPc2	disciplína
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Zákonitosti	zákonitost	k1gFnPc1	zákonitost
objevené	objevený	k2eAgFnPc1d1	objevená
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
pojmy	pojem	k1gInPc7	pojem
lze	lze	k6eAd1	lze
při	při	k7c6	při
vhodné	vhodný	k2eAgFnSc6d1	vhodná
aplikaci	aplikace	k1gFnSc6	aplikace
zpětně	zpětně	k6eAd1	zpětně
přeformulovat	přeformulovat	k5eAaPmF	přeformulovat
jako	jako	k8xC	jako
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
skutečného	skutečný	k2eAgInSc2d1	skutečný
světa	svět	k1gInSc2	svět
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
obecně	obecně	k6eAd1	obecně
platné	platný	k2eAgFnPc4d1	platná
teze	teze	k1gFnPc4	teze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
úkolem	úkol	k1gInSc7	úkol
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
příslušné	příslušný	k2eAgFnPc4d1	příslušná
jiné	jiný	k2eAgFnPc4d1	jiná
disciplíny	disciplína	k1gFnPc4	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyk	jazyk	k1gInSc1	jazyk
matematiky	matematika	k1gFnSc2	matematika
je	být	k5eAaImIp3nS	být
umělý	umělý	k2eAgInSc1d1	umělý
formální	formální	k2eAgInSc1d1	formální
jazyk	jazyk	k1gInSc1	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
matematiky	matematika	k1gFnSc2	matematika
je	být	k5eAaImIp3nS	být
umělý	umělý	k2eAgInSc4d1	umělý
formální	formální	k2eAgInSc4d1	formální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yRgInSc4	který
platí	platit	k5eAaImIp3nS	platit
kategorický	kategorický	k2eAgInSc1d1	kategorický
požadavek	požadavek	k1gInSc1	požadavek
exaktní	exaktní	k2eAgFnSc2d1	exaktní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
vágností	vágnost	k1gFnSc7	vágnost
<g/>
)	)	kIx)	)
interpretace	interpretace	k1gFnPc1	interpretace
všech	všecek	k3xTgFnPc2	všecek
jeho	jeho	k3xOp3gFnPc2	jeho
jazykových	jazykový	k2eAgFnPc2d1	jazyková
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Umělými	umělý	k2eAgInPc7d1	umělý
formálními	formální	k2eAgInPc7d1	formální
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jazyky	jazyk	k1gInPc1	jazyk
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
formálních	formální	k2eAgFnPc2d1	formální
logik	logika	k1gFnPc2	logika
a	a	k8xC	a
programovací	programovací	k2eAgInPc1d1	programovací
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
formální	formální	k2eAgFnSc6d1	formální
logice	logika	k1gFnSc6	logika
použít	použít	k5eAaPmF	použít
přirozený	přirozený	k2eAgInSc4d1	přirozený
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
inherentně	inherentně	k6eAd1	inherentně
vágní	vágní	k2eAgNnSc1d1	vágní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
i	i	k9	i
emocionální	emocionální	k2eAgFnSc3d1	emocionální
interpretaci	interpretace	k1gFnSc3	interpretace
(	(	kIx(	(
<g/>
říkáme	říkat	k5eAaImIp1nP	říkat
jí	on	k3xPp3gFnSc2	on
konotace	konotace	k1gFnSc2	konotace
<g/>
)	)	kIx)	)
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
jazykových	jazykový	k2eAgFnPc2d1	jazyková
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
omylem	omyl	k1gInSc7	omyl
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
učebnicích	učebnice	k1gFnPc6	učebnice
formální	formální	k2eAgFnSc2d1	formální
logiky	logika	k1gFnSc2	logika
nebo	nebo	k8xC	nebo
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
viz	vidět	k5eAaImRp2nS	vidět
reprezentace	reprezentace	k1gFnSc2	reprezentace
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
překročení	překročení	k1gNnSc4	překročení
hranic	hranice	k1gFnPc2	hranice
exaktního	exaktní	k2eAgInSc2d1	exaktní
světa	svět	k1gInSc2	svět
porušením	porušení	k1gNnSc7	porušení
podmínky	podmínka	k1gFnSc2	podmínka
exaktní	exaktní	k2eAgFnSc2d1	exaktní
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hlubší	hluboký	k2eAgNnSc4d2	hlubší
pochopení	pochopení	k1gNnSc4	pochopení
problému	problém	k1gInSc2	problém
<g/>
:	:	kIx,	:
Přirozený	přirozený	k2eAgInSc1d1	přirozený
jazyk	jazyk	k1gInSc1	jazyk
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
exaktního	exaktní	k2eAgInSc2d1	exaktní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
exaktní	exaktní	k2eAgFnSc4d1	exaktní
interpretaci	interpretace	k1gFnSc4	interpretace
svých	svůj	k3xOyFgFnPc2	svůj
jazykových	jazykový	k2eAgFnPc2d1	jazyková
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Napřiklad	Napřiklad	k6eAd1	Napřiklad
pokud	pokud	k8xS	pokud
nějaký	nějaký	k3yIgInSc1	nějaký
objekt	objekt	k1gInSc1	objekt
exaktního	exaktní	k2eAgInSc2d1	exaktní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
veličinu	veličina	k1gFnSc4	veličina
"	"	kIx"	"
<g/>
Rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
tělesa	těleso	k1gNnSc2	těleso
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
<g/>
)	)	kIx)	)
symbolu	symbol	k1gInSc2	symbol
V	V	kA	V
(	(	kIx(	(
<g/>
jednočlenného	jednočlenný	k2eAgInSc2d1	jednočlenný
řetězce	řetězec	k1gInSc2	řetězec
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označíme	označit	k5eAaPmIp1nP	označit
konstrukcí	konstrukce	k1gFnSc7	konstrukce
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
větou	věta	k1gFnSc7	věta
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Marjánka	Marjánka	k1gFnSc1	Marjánka
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
usmívala	usmívat	k5eAaImAgFnS	usmívat
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
tuto	tento	k3xDgFnSc4	tento
větu	věta	k1gFnSc4	věta
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
větu	věta	k1gFnSc4	věta
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
a	a	k8xC	a
přiřazovat	přiřazovat	k5eAaImF	přiřazovat
jí	on	k3xPp3gFnSc7	on
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
význam	význam	k1gInSc1	význam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nutně	nutně	k6eAd1	nutně
jen	jen	k9	jen
jako	jako	k9	jako
řetězec	řetězec	k1gInSc4	řetězec
symbolů	symbol	k1gInPc2	symbol
dostávající	dostávající	k2eAgInSc4d1	dostávající
v	v	k7c6	v
exaktním	exaktní	k2eAgInSc6d1	exaktní
světě	svět	k1gInSc6	svět
nový	nový	k2eAgInSc4d1	nový
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jméno	jméno	k1gNnSc1	jméno
té	ten	k3xDgFnSc2	ten
veličiny	veličina	k1gFnSc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
věta	věta	k1gFnSc1	věta
dostává	dostávat	k5eAaImIp3nS	dostávat
tedy	tedy	k9	tedy
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
symbol	symbol	k1gInSc4	symbol
V.	V.	kA	V.
Přiřazení	přiřazení	k1gNnSc1	přiřazení
významu	význam	k1gInSc6	význam
té	ten	k3xDgFnSc3	ten
větě	věta	k1gFnSc3	věta
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
exaktní	exaktní	k2eAgNnSc1d1	exaktní
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
statutu	statut	k1gInSc3	statut
veličiny	veličina	k1gFnSc2	veličina
jako	jako	k8xS	jako
elementu	element	k1gInSc2	element
exaktního	exaktní	k2eAgInSc2d1	exaktní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
poznamenejme	poznamenat	k5eAaPmRp1nP	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
umělé	umělý	k2eAgInPc1d1	umělý
formální	formální	k2eAgInPc1d1	formální
jazyky	jazyk	k1gInPc1	jazyk
mají	mít	k5eAaImIp3nP	mít
vypovídat	vypovídat	k5eAaPmF	vypovídat
o	o	k7c6	o
znalostech	znalost	k1gFnPc6	znalost
o	o	k7c6	o
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
tak	tak	k9	tak
dít	dít	k5eAaImF	dít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
veličin	veličina	k1gFnPc2	veličina
viz	vidět	k5eAaImRp2nS	vidět
Exaktní	exaktní	k2eAgFnSc1d1	exaktní
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Veličina	veličina	k1gFnSc1	veličina
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
prostředníkem	prostředník	k1gInSc7	prostředník
mezi	mezi	k7c7	mezi
reálným	reálný	k2eAgMnSc7d1	reálný
a	a	k8xC	a
exaktním	exaktní	k2eAgInSc7d1	exaktní
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
matematiky	matematika	k1gFnSc2	matematika
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
především	především	k9	především
potřebou	potřeba	k1gFnSc7	potřeba
řešit	řešit	k5eAaImF	řešit
praktické	praktický	k2eAgFnPc4d1	praktická
úlohy	úloha	k1gFnPc4	úloha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
různé	různý	k2eAgFnPc1d1	různá
obchodní	obchodní	k2eAgFnPc1d1	obchodní
úlohy	úloha	k1gFnPc1	úloha
<g/>
,	,	kIx,	,
vyměřování	vyměřování	k1gNnSc1	vyměřování
a	a	k8xC	a
dělení	dělení	k1gNnSc1	dělení
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
a	a	k8xC	a
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
matematiky	matematika	k1gFnSc2	matematika
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
první	první	k4xOgInPc1	první
abstraktní	abstraktní	k2eAgInPc1d1	abstraktní
matematické	matematický	k2eAgInPc1d1	matematický
pojmy	pojem	k1gInPc1	pojem
–	–	k?	–
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
prodělala	prodělat	k5eAaPmAgFnS	prodělat
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výrazných	výrazný	k2eAgInPc2d1	výrazný
úspěchů	úspěch	k1gInPc2	úspěch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
zejména	zejména	k9	zejména
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
etapou	etapa	k1gFnSc7	etapa
prudkého	prudký	k2eAgInSc2d1	prudký
rozvoje	rozvoj	k1gInSc2	rozvoj
matematiky	matematika	k1gFnSc2	matematika
byl	být	k5eAaImAgInS	být
raný	raný	k2eAgInSc1d1	raný
novověk	novověk	k1gInSc1	novověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
především	především	k6eAd1	především
Descartem	Descart	k1gInSc7	Descart
ustaveny	ustaven	k2eAgInPc4d1	ustaven
základy	základ	k1gInPc4	základ
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
práci	práce	k1gFnSc3	práce
Newtona	Newton	k1gMnSc2	Newton
<g/>
,	,	kIx,	,
Leibnize	Leibnize	k1gFnSc2	Leibnize
<g/>
,	,	kIx,	,
Eulera	Eulero	k1gNnSc2	Eulero
<g/>
,	,	kIx,	,
Gausse	gauss	k1gInSc5	gauss
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
matematiků	matematik	k1gMnPc2	matematik
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zásadních	zásadní	k2eAgInPc2d1	zásadní
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
analýzy	analýza	k1gFnSc2	analýza
zejména	zejména	k9	zejména
položením	položení	k1gNnSc7	položení
základů	základ	k1gInPc2	základ
diferenciálního	diferenciální	k2eAgNnSc2d1	diferenciální
a	a	k8xC	a
integrálního	integrální	k2eAgInSc2d1	integrální
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiným	jiný	k2eAgNnSc7d1	jiné
významným	významný	k2eAgNnSc7d1	významné
obdobím	období	k1gNnSc7	období
dějin	dějiny	k1gFnPc2	dějiny
matematiky	matematika	k1gFnSc2	matematika
byl	být	k5eAaImAgInS	být
přelom	přelom	k1gInSc1	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zkoumání	zkoumání	k1gNnSc4	zkoumání
dokazatelnosti	dokazatelnost	k1gFnSc2	dokazatelnost
tvrzení	tvrzení	k1gNnSc1	tvrzení
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c4	na
solidní	solidní	k2eAgInSc4d1	solidní
a	a	k8xC	a
formální	formální	k2eAgInSc4d1	formální
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
logice	logika	k1gFnSc6	logika
a	a	k8xC	a
zavedením	zavedení	k1gNnSc7	zavedení
axiomatické	axiomatický	k2eAgFnSc2d1	axiomatická
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
dobou	doba	k1gFnSc7	doba
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
též	též	k9	též
zkoumány	zkoumán	k2eAgFnPc4d1	zkoumána
abstraktní	abstraktní	k2eAgFnPc4d1	abstraktní
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jedním	jeden	k4xCgInSc7	jeden
důkazem	důkaz	k1gInSc7	důkaz
ověřit	ověřit	k5eAaPmF	ověřit
matematické	matematický	k2eAgNnSc4d1	matematické
tvrzení	tvrzení	k1gNnSc4	tvrzení
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
skupinu	skupina	k1gFnSc4	skupina
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
tohoto	tento	k3xDgInSc2	tento
trendu	trend	k1gInSc2	trend
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznik	vznik	k1gInSc4	vznik
teorie	teorie	k1gFnSc2	teorie
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
nejobecnější	obecní	k2eAgFnSc4d3	obecní
a	a	k8xC	a
nejabstraktnější	abstraktní	k2eAgFnSc4d3	nejabstraktnější
matematickou	matematický	k2eAgFnSc4d1	matematická
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematické	matematický	k2eAgFnPc1d1	matematická
disciplíny	disciplína	k1gFnPc1	disciplína
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
klasické	klasický	k2eAgFnPc1d1	klasická
disciplíny	disciplína	k1gFnPc1	disciplína
matematiky	matematika	k1gFnSc2	matematika
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
praktických	praktický	k2eAgFnPc2d1	praktická
lidských	lidský	k2eAgFnPc2d1	lidská
potřeb	potřeba	k1gFnPc2	potřeba
–	–	k?	–
potřeby	potřeba	k1gFnSc2	potřeba
počítat	počítat	k5eAaImF	počítat
při	při	k7c6	při
obchodování	obchodování	k1gNnSc6	obchodování
<g/>
,	,	kIx,	,
porozumět	porozumět	k5eAaPmF	porozumět
vztahům	vztah	k1gInPc3	vztah
mezi	mezi	k7c7	mezi
číselně	číselně	k6eAd1	číselně
vyjádřenými	vyjádřený	k2eAgNnPc7d1	vyjádřené
množstvími	množství	k1gNnPc7	množství
<g/>
,	,	kIx,	,
vyměřování	vyměřování	k1gNnSc1	vyměřování
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
předpovídání	předpovídání	k1gNnSc4	předpovídání
astronomických	astronomický	k2eAgInPc2d1	astronomický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
čtyř	čtyři	k4xCgFnPc2	čtyři
potřeb	potřeba	k1gFnPc2	potřeba
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
čtyři	čtyři	k4xCgFnPc4	čtyři
klasické	klasický	k2eAgFnPc4d1	klasická
matematické	matematický	k2eAgFnPc4d1	matematická
disciplíny	disciplína	k1gFnPc4	disciplína
–	–	k?	–
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
aritmetika	aritmetika	k1gFnSc1	aritmetika
<g/>
,	,	kIx,	,
algebra	algebra	k1gFnSc1	algebra
<g/>
,	,	kIx,	,
geometrie	geometrie	k1gFnSc1	geometrie
a	a	k8xC	a
matematická	matematický	k2eAgFnSc1d1	matematická
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
zhruba	zhruba	k6eAd1	zhruba
řečeno	říct	k5eAaPmNgNnS	říct
čtyřmi	čtyři	k4xCgFnPc7	čtyři
základními	základní	k2eAgFnPc7d1	základní
oblastmi	oblast	k1gFnPc7	oblast
zájmu	zájem	k1gInSc2	zájem
matematiky	matematika	k1gFnSc2	matematika
–	–	k?	–
kvantitou	kvantita	k1gFnSc7	kvantita
<g/>
,	,	kIx,	,
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
prostorem	prostor	k1gInSc7	prostor
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
snahám	snaha	k1gFnPc3	snaha
zastřešit	zastřešit	k5eAaPmF	zastřešit
tyto	tento	k3xDgFnPc1	tento
čtyři	čtyři	k4xCgFnPc1	čtyři
disciplíny	disciplína	k1gFnPc1	disciplína
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
matematickou	matematický	k2eAgFnSc7d1	matematická
teorií	teorie	k1gFnSc7	teorie
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
co	co	k9	co
největší	veliký	k2eAgFnSc3d3	veliký
přesnosti	přesnost	k1gFnSc3	přesnost
a	a	k8xC	a
nezpochybnitelnosti	nezpochybnitelnost	k1gFnSc3	nezpochybnitelnost
výsledků	výsledek	k1gInPc2	výsledek
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
několik	několik	k4yIc1	několik
vzájemně	vzájemně	k6eAd1	vzájemně
provázaných	provázaný	k2eAgFnPc2d1	provázaná
disciplín	disciplína	k1gFnPc2	disciplína
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
souhrnně	souhrnně	k6eAd1	souhrnně
základy	základ	k1gInPc4	základ
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
disciplíny	disciplína	k1gFnPc1	disciplína
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
umožnily	umožnit	k5eAaPmAgFnP	umožnit
také	také	k9	také
hlubší	hluboký	k2eAgNnSc4d2	hlubší
propojení	propojení	k1gNnSc4	propojení
matematiky	matematika	k1gFnSc2	matematika
s	s	k7c7	s
filozofií	filozofie	k1gFnSc7	filozofie
či	či	k8xC	či
rozvoj	rozvoj	k1gInSc4	rozvoj
teoretické	teoretický	k2eAgFnSc2d1	teoretická
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
ohromný	ohromný	k2eAgInSc4d1	ohromný
rozvoj	rozvoj	k1gInSc4	rozvoj
disciplíny	disciplína	k1gFnSc2	disciplína
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
důležité	důležitý	k2eAgInPc1d1	důležitý
nástroje	nástroj	k1gInPc1	nástroj
v	v	k7c4	v
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
oborech	obor	k1gInPc6	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kvantita	kvantita	k1gFnSc1	kvantita
===	===	k?	===
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
kvantity	kvantita	k1gFnSc2	kvantita
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgFnSc7d3	nejstarší
oblastí	oblast	k1gFnSc7	oblast
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
počátky	počátek	k1gInPc1	počátek
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
pojmu	pojem	k1gInSc2	pojem
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
následuje	následovat	k5eAaImIp3nS	následovat
vytváření	vytváření	k1gNnSc4	vytváření
základních	základní	k2eAgFnPc2d1	základní
aritmetických	aritmetický	k2eAgFnPc2d1	aritmetická
operací	operace	k1gFnPc2	operace
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
číselného	číselný	k2eAgInSc2d1	číselný
oboru	obor	k1gInSc2	obor
přes	přes	k7c4	přes
čísla	číslo	k1gNnPc4	číslo
celá	celý	k2eAgNnPc4d1	celé
<g/>
,	,	kIx,	,
racionální	racionální	k2eAgNnPc1d1	racionální
<g/>
,	,	kIx,	,
reálná	reálný	k2eAgNnPc1d1	reálné
a	a	k8xC	a
komplexní	komplexní	k2eAgFnSc1d1	komplexní
až	až	k9	až
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
specializovaným	specializovaný	k2eAgInPc3d1	specializovaný
číselným	číselný	k2eAgInPc3d1	číselný
oborům	obor	k1gInPc3	obor
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
hyperkomplexní	hyperkomplexní	k2eAgNnPc1d1	hyperkomplexní
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
kvaterniony	kvaternion	k1gInPc1	kvaternion
<g/>
,	,	kIx,	,
oktoniony	oktonion	k1gInPc1	oktonion
<g/>
,	,	kIx,	,
ordinální	ordinální	k2eAgNnPc1d1	ordinální
a	a	k8xC	a
kardinální	kardinální	k2eAgNnPc1d1	kardinální
čísla	číslo	k1gNnPc1	číslo
nebo	nebo	k8xC	nebo
surreálná	surreálný	k2eAgNnPc4d1	surreálné
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dosud	dosud	k6eAd1	dosud
mnoho	mnoho	k6eAd1	mnoho
snadno	snadno	k6eAd1	snadno
formulovatelných	formulovatelný	k2eAgInPc2d1	formulovatelný
otevřených	otevřený	k2eAgInPc2d1	otevřený
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hypotéza	hypotéza	k1gFnSc1	hypotéza
prvočíselných	prvočíselný	k2eAgFnPc2d1	prvočíselná
dvojic	dvojice	k1gFnPc2	dvojice
nebo	nebo	k8xC	nebo
Goldbachova	Goldbachův	k2eAgFnSc1d1	Goldbachova
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
problém	problém	k1gInSc1	problém
celé	celý	k2eAgFnSc2d1	celá
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
Fermatova	Fermatův	k2eAgFnSc1d1	Fermatova
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
po	po	k7c6	po
350	[number]	k4	350
letech	léto	k1gNnPc6	léto
marných	marný	k2eAgInPc2d1	marný
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
jako	jako	k8xC	jako
množiny	množina	k1gFnSc2	množina
čísel	číslo	k1gNnPc2	číslo
či	či	k8xC	či
funkcí	funkce	k1gFnSc7	funkce
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
jistou	jistý	k2eAgFnSc4d1	jistá
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Abstrahováním	abstrahování	k1gNnSc7	abstrahování
některých	některý	k3yIgFnPc2	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
strukturálních	strukturální	k2eAgFnPc2d1	strukturální
vlastností	vlastnost	k1gFnPc2	vlastnost
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
pojmy	pojem	k1gInPc1	pojem
grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
skupina	skupina	k1gFnSc1	skupina
)	)	kIx)	)
<g/>
,	,	kIx,	,
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
těleso	těleso	k1gNnSc1	těleso
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
těchto	tento	k3xDgInPc2	tento
abstraktních	abstraktní	k2eAgInPc2d1	abstraktní
konceptů	koncept	k1gInPc2	koncept
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
algebra	algebra	k1gFnSc1	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgFnSc1d1	lineární
algebra	algebra	k1gFnSc1	algebra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
tři	tři	k4xCgFnPc1	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
okruhů	okruh	k1gInPc2	okruh
zájmu	zájem	k1gInSc6	zájem
matematiky	matematika	k1gFnSc2	matematika
–	–	k?	–
kvantitu	kvantita	k1gFnSc4	kvantita
<g/>
,	,	kIx,	,
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
a	a	k8xC	a
integrální	integrální	k2eAgInSc1d1	integrální
počet	počet	k1gInSc1	počet
přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
třem	tři	k4xCgNnPc3	tři
okruhům	okruh	k1gInPc3	okruh
i	i	k9	i
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
–	–	k?	–
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prostor	prostor	k1gInSc4	prostor
===	===	k?	===
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
prostoru	prostor	k1gInSc2	prostor
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
geometrií	geometrie	k1gFnPc2	geometrie
–	–	k?	–
konkrétně	konkrétně	k6eAd1	konkrétně
euklidovskou	euklidovský	k2eAgFnSc7d1	euklidovská
<g/>
.	.	kIx.	.
</s>
<s>
Trigonometrie	trigonometrie	k1gFnSc1	trigonometrie
přibírá	přibírat	k5eAaImIp3nS	přibírat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
fenomén	fenomén	k1gInSc1	fenomén
kvantity	kvantita	k1gFnSc2	kvantita
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
tvrzením	tvrzení	k1gNnSc7	tvrzení
této	tento	k3xDgFnSc2	tento
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
geometrie	geometrie	k1gFnSc2	geometrie
je	být	k5eAaImIp3nS	být
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zobecňování	zobecňování	k1gNnSc3	zobecňování
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vícedimenzionálním	vícedimenzionální	k2eAgInPc3d1	vícedimenzionální
prostorům	prostor	k1gInPc3	prostor
<g/>
,	,	kIx,	,
neeuklidovským	euklidovský	k2eNgFnPc3d1	neeuklidovská
geometriím	geometrie	k1gFnPc3	geometrie
a	a	k8xC	a
topologii	topologie	k1gFnSc3	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Uvažováním	uvažování	k1gNnSc7	uvažování
v	v	k7c6	v
kvantitativních	kvantitativní	k2eAgFnPc6d1	kvantitativní
sférách	sféra	k1gFnPc6	sféra
se	se	k3xPyFc4	se
dostáváme	dostávat	k5eAaImIp1nP	dostávat
k	k	k7c3	k
analytické	analytický	k2eAgFnSc3d1	analytická
<g/>
,	,	kIx,	,
diferenciální	diferenciální	k2eAgFnSc3d1	diferenciální
a	a	k8xC	a
algebraické	algebraický	k2eAgFnSc3d1	algebraická
geometrii	geometrie	k1gFnSc3	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
geometrie	geometrie	k1gFnSc1	geometrie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
hladkých	hladký	k2eAgFnPc2d1	hladká
křivek	křivka	k1gFnPc2	křivka
a	a	k8xC	a
ploch	plocha	k1gFnPc2	plocha
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
algebraická	algebraický	k2eAgNnPc4d1	algebraické
pak	pak	k6eAd1	pak
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
reprezentací	reprezentace	k1gFnSc7	reprezentace
množin	množina	k1gFnPc2	množina
kořenů	kořen	k1gInPc2	kořen
polynomů	polynom	k1gInPc2	polynom
více	hodně	k6eAd2	hodně
proměnných	proměnný	k2eAgFnPc2d1	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Topologické	topologický	k2eAgFnPc1d1	topologická
grupy	grupa	k1gFnPc1	grupa
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
fenomény	fenomén	k1gInPc4	fenomén
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
Lieovy	Lieův	k2eAgFnPc1d1	Lieova
grupy	grupa	k1gFnPc1	grupa
přidávají	přidávat	k5eAaImIp3nP	přidávat
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změna	změna	k1gFnSc1	změna
===	===	k?	===
</s>
</p>
<p>
<s>
Pochopení	pochopení	k1gNnSc1	pochopení
a	a	k8xC	a
popis	popis	k1gInSc1	popis
změny	změna	k1gFnSc2	změna
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
snahou	snaha	k1gFnSc7	snaha
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Mocným	mocný	k2eAgInSc7d1	mocný
nástrojem	nástroj	k1gInSc7	nástroj
k	k	k7c3	k
uchopení	uchopení	k1gNnSc3	uchopení
fenoménu	fenomén	k1gInSc2	fenomén
změny	změna	k1gFnSc2	změna
je	být	k5eAaImIp3nS	být
kalkulus	kalkulus	k1gInSc4	kalkulus
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
konceptu	koncept	k1gInSc3	koncept
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
funkcí	funkce	k1gFnPc2	funkce
na	na	k7c6	na
oboru	obor	k1gInSc6	obor
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
reálná	reálný	k2eAgFnSc1d1	reálná
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
disciplínou	disciplína	k1gFnSc7	disciplína
pro	pro	k7c4	pro
komplexní	komplexní	k2eAgInSc4d1	komplexní
případ	případ	k1gInSc4	případ
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgFnSc1d1	komplexní
analýza	analýza	k1gFnSc1	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
i	i	k8xC	i
nejtěžší	těžký	k2eAgInSc1d3	nejtěžší
nevyřešený	vyřešený	k2eNgInSc1d1	nevyřešený
problém	problém	k1gInSc1	problém
současné	současný	k2eAgFnSc2d1	současná
matematiky	matematika	k1gFnSc2	matematika
–	–	k?	–
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionální	funkcionální	k2eAgFnSc1d1	funkcionální
analýza	analýza	k1gFnSc1	analýza
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
přirozeně	přirozeně	k6eAd1	přirozeně
vznikajících	vznikající	k2eAgInPc2d1	vznikající
prostorů	prostor	k1gInPc2	prostor
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
aplikací	aplikace	k1gFnPc2	aplikace
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
je	být	k5eAaImIp3nS	být
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
problematiku	problematika	k1gFnSc4	problematika
změn	změna	k1gFnPc2	změna
kvantitativních	kvantitativní	k2eAgFnPc2d1	kvantitativní
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
složité	složitý	k2eAgInPc1d1	složitý
přírodní	přírodní	k2eAgInPc1d1	přírodní
systémy	systém	k1gInPc1	systém
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
inspirace	inspirace	k1gFnPc1	inspirace
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
dynamických	dynamický	k2eAgInPc2d1	dynamický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Základy	základ	k1gInPc1	základ
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
objasnit	objasnit	k5eAaPmF	objasnit
a	a	k8xC	a
zpřesnit	zpřesnit	k5eAaPmF	zpřesnit
základní	základní	k2eAgInPc4d1	základní
kameny	kámen	k1gInPc4	kámen
matematiky	matematika	k1gFnSc2	matematika
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
disciplínám	disciplína	k1gFnPc3	disciplína
teorie	teorie	k1gFnPc4	teorie
množin	množina	k1gFnPc2	množina
a	a	k8xC	a
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
bývají	bývat	k5eAaImIp3nP	bývat
souhrnně	souhrnně	k6eAd1	souhrnně
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
základy	základ	k1gInPc4	základ
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
základů	základ	k1gInPc2	základ
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
abstraktní	abstraktní	k2eAgFnSc2d1	abstraktní
algebry	algebra	k1gFnSc2	algebra
leží	ležet	k5eAaImIp3nS	ležet
teorie	teorie	k1gFnSc1	teorie
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matematická	matematický	k2eAgFnSc1d1	matematická
logika	logika	k1gFnSc1	logika
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pevný	pevný	k2eAgInSc4d1	pevný
axiomatický	axiomatický	k2eAgInSc4d1	axiomatický
rámec	rámec	k1gInSc4	rámec
celé	celý	k2eAgFnSc2d1	celá
matematice	matematika	k1gFnSc3	matematika
a	a	k8xC	a
svojí	svůj	k3xOyFgFnSc7	svůj
maximální	maximální	k2eAgFnSc7d1	maximální
přesností	přesnost	k1gFnSc7	přesnost
zaštiťuje	zaštiťovat	k5eAaImIp3nS	zaštiťovat
nezpochybnitelnost	nezpochybnitelnost	k1gFnSc1	nezpochybnitelnost
všech	všecek	k3xTgInPc2	všecek
matematických	matematický	k2eAgInPc2d1	matematický
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
důkazu	důkaz	k1gInSc2	důkaz
precizuje	precizovat	k5eAaBmIp3nS	precizovat
a	a	k8xC	a
matematizuje	matematizovat	k5eAaImIp3nS	matematizovat
základní	základní	k2eAgInPc4d1	základní
principy	princip	k1gInPc4	princip
rozumového	rozumový	k2eAgNnSc2d1	rozumové
odvozování	odvozování	k1gNnSc2	odvozování
a	a	k8xC	a
nutného	nutný	k2eAgNnSc2d1	nutné
vyplývání	vyplývání	k1gNnSc2	vyplývání
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
modelů	model	k1gInPc2	model
studuje	studovat	k5eAaImIp3nS	studovat
logické	logický	k2eAgInPc4d1	logický
koncepty	koncept	k1gInPc4	koncept
pomocí	pomocí	k7c2	pomocí
algebraických	algebraický	k2eAgFnPc2d1	algebraická
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgNnSc1d1	formální
studium	studium	k1gNnSc1	studium
aritmetických	aritmetický	k2eAgFnPc2d1	aritmetická
teorií	teorie	k1gFnPc2	teorie
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Robinsonova	Robinsonův	k2eAgFnSc1d1	Robinsonova
či	či	k8xC	či
Peanova	Peanův	k2eAgFnSc1d1	Peanova
aritmetika	aritmetika	k1gFnSc1	aritmetika
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
i	i	k9	i
pro	pro	k7c4	pro
filozofické	filozofický	k2eAgFnPc4d1	filozofická
otázky	otázka	k1gFnPc4	otázka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
hranic	hranice	k1gFnPc2	hranice
deduktivní	deduktivní	k2eAgFnSc2d1	deduktivní
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgFnPc2	tento
otázek	otázka	k1gFnPc2	otázka
je	být	k5eAaImIp3nS	být
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
výsledek	výsledek	k1gInSc1	výsledek
celé	celý	k2eAgFnSc2d1	celá
logiky	logika	k1gFnSc2	logika
–	–	k?	–
Gödelovy	Gödelův	k2eAgFnSc2d1	Gödelova
věty	věta	k1gFnSc2	věta
o	o	k7c6	o
neúplnosti	neúplnost	k1gFnSc6	neúplnost
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
rekurze	rekurze	k1gFnSc2	rekurze
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
teoretické	teoretický	k2eAgInPc4d1	teoretický
základy	základ	k1gInPc4	základ
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
svět	svět	k1gInSc1	svět
matematiky	matematika	k1gFnSc2	matematika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
jiná	jiný	k2eAgFnSc1d1	jiná
matematická	matematický	k2eAgFnSc1d1	matematická
disciplína	disciplína	k1gFnSc1	disciplína
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
obor	obor	k1gInSc1	obor
studia	studio	k1gNnSc2	studio
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c4	na
pochopení	pochopení	k1gNnSc4	pochopení
a	a	k8xC	a
popis	popis	k1gInSc4	popis
fenoménu	fenomén	k1gInSc2	fenomén
nekonečna	nekonečno	k1gNnSc2	nekonečno
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
aktuální	aktuální	k2eAgFnSc6d1	aktuální
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgInSc7d1	slavný
problémem	problém	k1gInSc7	problém
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
byla	být	k5eAaImAgFnS	být
hypotéza	hypotéza	k1gFnSc1	hypotéza
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
,	,	kIx,	,
filozofické	filozofický	k2eAgInPc4d1	filozofický
dopady	dopad	k1gInPc4	dopad
má	mít	k5eAaImIp3nS	mít
otázka	otázka	k1gFnSc1	otázka
axiomu	axiom	k1gInSc2	axiom
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
matematika	matematika	k1gFnSc1	matematika
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
matematika	matematika	k1gFnSc1	matematika
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
oblasti	oblast	k1gFnPc1	oblast
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
studiem	studio	k1gNnSc7	studio
konečných	konečný	k2eAgInPc2d1	konečný
diskrétních	diskrétní	k2eAgInPc2d1	diskrétní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
podobory	podobor	k1gInPc1	podobor
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
velký	velký	k2eAgInSc4d1	velký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
a	a	k8xC	a
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
disciplíny	disciplína	k1gFnPc1	disciplína
jako	jako	k8xS	jako
teorie	teorie	k1gFnPc1	teorie
složitosti	složitost	k1gFnSc2	složitost
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc2	teorie
informace	informace	k1gFnSc2	informace
nebo	nebo	k8xC	nebo
studium	studium	k1gNnSc4	studium
teoretických	teoretický	k2eAgInPc2d1	teoretický
modelů	model	k1gInPc2	model
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
je	být	k5eAaImIp3nS	být
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
složitosti	složitost	k1gFnSc2	složitost
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
časovou	časový	k2eAgFnSc7d1	časová
náročností	náročnost	k1gFnSc7	náročnost
algoritmů	algoritmus	k1gInPc2	algoritmus
zpracovávaných	zpracovávaný	k2eAgInPc2d1	zpracovávaný
v	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
informace	informace	k1gFnSc1	informace
možnostmi	možnost	k1gFnPc7	možnost
efektivního	efektivní	k2eAgNnSc2d1	efektivní
skladování	skladování	k1gNnSc2	skladování
informací	informace	k1gFnPc2	informace
na	na	k7c6	na
záznamových	záznamový	k2eAgNnPc6d1	záznamové
médiích	médium	k1gNnPc6	médium
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
pojmy	pojem	k1gInPc4	pojem
komprese	komprese	k1gFnSc2	komprese
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
entropie	entropie	k1gFnSc2	entropie
apod.	apod.	kA	apod.
Nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
problémem	problém	k1gInSc7	problém
těchto	tento	k3xDgFnPc2	tento
disciplín	disciplína	k1gFnPc2	disciplína
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
problém	problém	k1gInSc1	problém
P	P	kA	P
=	=	kIx~	=
NP	NP	kA	NP
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
součástmi	součást	k1gFnPc7	součást
diskrétní	diskrétní	k2eAgFnSc2d1	diskrétní
matematiky	matematika	k1gFnSc2	matematika
jsou	být	k5eAaImIp3nP	být
kombinatorika	kombinatorika	k1gFnSc1	kombinatorika
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
grafů	graf	k1gInPc2	graf
nebo	nebo	k8xC	nebo
kryptografie	kryptografie	k1gFnSc2	kryptografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
matematika	matematika	k1gFnSc1	matematika
===	===	k?	===
</s>
</p>
<p>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
matematika	matematika	k1gFnSc1	matematika
používá	používat	k5eAaImIp3nS	používat
abstraktní	abstraktní	k2eAgInPc4d1	abstraktní
matematické	matematický	k2eAgInPc4d1	matematický
nástroje	nástroj	k1gInPc4	nástroj
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
praktických	praktický	k2eAgInPc2d1	praktický
problémů	problém	k1gInPc2	problém
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
apod.	apod.	kA	apod.
Statistika	statistik	k1gMnSc2	statistik
používá	používat	k5eAaImIp3nS	používat
teorii	teorie	k1gFnSc4	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
<g/>
,	,	kIx,	,
analýze	analýza	k1gFnSc3	analýza
a	a	k8xC	a
předpovídání	předpovídání	k1gNnSc3	předpovídání
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Numerická	numerický	k2eAgFnSc1d1	numerická
matematika	matematika	k1gFnSc1	matematika
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
zaštiťuje	zaštiťovat	k5eAaImIp3nS	zaštiťovat
počítačové	počítačový	k2eAgFnPc4d1	počítačová
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
metody	metoda	k1gFnPc4	metoda
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
úloh	úloha	k1gFnPc2	úloha
příliš	příliš	k6eAd1	příliš
náročných	náročný	k2eAgFnPc2d1	náročná
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
ji	on	k3xPp3gFnSc4	on
počítačové	počítačový	k2eAgNnSc1d1	počítačové
modelování	modelování	k1gNnSc1	modelování
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
aplikacemi	aplikace	k1gFnPc7	aplikace
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
a	a	k8xC	a
předpovědi	předpověď	k1gFnPc1	předpověď
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
<g/>
,	,	kIx,	,
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
<g/>
,	,	kIx,	,
sociologických	sociologický	k2eAgInPc2d1	sociologický
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgInPc2d1	chemický
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
finanční	finanční	k2eAgFnSc1d1	finanční
matematika	matematika	k1gFnSc1	matematika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
fenoménů	fenomén	k1gInPc2	fenomén
slouží	sloužit	k5eAaImIp3nS	sloužit
často	často	k6eAd1	často
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
teorie	teorie	k1gFnSc2	teorie
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PAVLÍKOVÁ	Pavlíková	k1gFnSc1	Pavlíková
PAVLA	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
SCHMIDT	Schmidt	k1gMnSc1	Schmidt
OSKAR	Oskar	k1gMnSc1	Oskar
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
VŠCHT	VŠCHT	kA	VŠCHT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7080	[number]	k4	7080
<g/>
-	-	kIx~	-
<g/>
615	[number]	k4	615
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MENŠÍK	Menšík	k1gMnSc1	Menšík
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
geometrie	geometrie	k1gFnSc1	geometrie
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
učebné	učebné	k1gNnSc4	učebné
pomůcky	pomůcka	k1gFnSc2	pomůcka
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
a	a	k8xC	a
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
329	[number]	k4	329
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Exaktní	exaktní	k2eAgFnSc1d1	exaktní
</s>
</p>
<p>
<s>
Logika	logika	k1gFnSc1	logika
</s>
</p>
<p>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
Informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
</s>
</p>
<p>
<s>
Matematik	matematik	k1gMnSc1	matematik
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
matematiků	matematik	k1gMnPc2	matematik
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
matematika	matematik	k1gMnSc2	matematik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Mathematika	Mathematika	k1gFnSc1	Mathematika
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Matematika	matematika	k1gFnSc1	matematika
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
matematika	matematika	k1gFnSc1	matematika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Kategorie	kategorie	k1gFnSc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Matematika	matematika	k1gFnSc1	matematika
ve	v	k7c4	v
WikikniháchAnglicko-český	WikikniháchAnglicko-český	k2eAgInSc4d1	WikikniháchAnglicko-český
/	/	kIx~	/
česko-anglický	českonglický	k2eAgInSc1d1	česko-anglický
slovník	slovník	k1gInSc1	slovník
matematické	matematický	k2eAgFnSc2d1	matematická
terminologie	terminologie	k1gFnSc2	terminologie
</s>
</p>
<p>
<s>
Cifrikova	Cifrikův	k2eAgFnSc1d1	Cifrikův
matematika	matematika	k1gFnSc1	matematika
–	–	k?	–
Seminární	seminární	k2eAgFnPc4d1	seminární
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
domácí	domácí	k2eAgInPc4d1	domácí
úkoly	úkol	k1gInPc4	úkol
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
studenta	student	k1gMnSc2	student
UK	UK	kA	UK
PedF	PedF	k1gFnSc1	PedF
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Isibalo	Isibat	k5eAaImAgNnS	Isibat
-	-	kIx~	-
matematický	matematický	k2eAgInSc1d1	matematický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
videoportál	videoportál	k1gInSc1	videoportál
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
výuce	výuka	k1gFnSc6	výuka
matematiky	matematika	k1gFnSc2	matematika
</s>
</p>
