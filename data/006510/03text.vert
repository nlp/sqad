<s>
Star	star	k1gFnSc1	star
Trek	Treka	k1gFnPc2	Treka
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
mediální	mediální	k2eAgFnSc1d1	mediální
řada	řada	k1gFnSc1	řada
a	a	k8xC	a
fikční	fikční	k2eAgInSc1d1	fikční
svět	svět	k1gInSc1	svět
sci-fi	scii	k1gNnSc2	sci-fi
televizních	televizní	k2eAgInPc2d1	televizní
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
komiksů	komiks	k1gInPc2	komiks
i	i	k8xC	i
videoher	videohra	k1gFnPc2	videohra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberr	k1gInPc1	Roddenberr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Hvězdné	hvězdný	k2eAgNnSc1d1	Hvězdné
putování	putování	k1gNnSc1	putování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřekládá	překládat	k5eNaImIp3nS	překládat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Original	Original	k1gMnSc1	Original
Series	Series	k1gMnSc1	Series
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
Původní	původní	k2eAgInSc1d1	původní
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
čítal	čítat	k5eAaImAgInS	čítat
3	[number]	k4	3
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgMnS	představit
divákům	divák	k1gMnPc3	divák
kapitána	kapitán	k1gMnSc2	kapitán
Jamese	Jamese	k1gFnSc2	Jamese
Kirka	Kirka	k1gFnSc1	Kirka
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
posádkou	posádka	k1gFnSc7	posádka
<g/>
,	,	kIx,	,
kterak	kterak	k8xS	kterak
prozkoumávají	prozkoumávat	k5eAaImIp3nP	prozkoumávat
vesmír	vesmír	k1gInSc4	vesmír
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
lodi	loď	k1gFnSc2	loď
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc1	Enterprise
(	(	kIx(	(
<g/>
NCC-	NCC-	k1gFnSc1	NCC-
<g/>
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
příběhy	příběh	k1gInPc1	příběh
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
animovaném	animovaný	k2eAgInSc6d1	animovaný
seriálu	seriál	k1gInSc6	seriál
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Animated	Animated	k1gMnSc1	Animated
Series	Series	k1gMnSc1	Series
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doslovně	doslovně	k6eAd1	doslovně
Animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgInP	být
natočeny	natočen	k2eAgInPc1d1	natočen
čtyři	čtyři	k4xCgInPc1	čtyři
další	další	k2eAgInPc1d1	další
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stavěly	stavět	k5eAaImAgInP	stavět
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
základu	základ	k1gInSc6	základ
<g/>
:	:	kIx,	:
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zasazena	zasadit	k5eAaPmNgFnS	zasadit
do	do	k7c2	do
období	období	k1gNnSc2	období
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
události	událost	k1gFnPc1	událost
původních	původní	k2eAgMnPc2d1	původní
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Seriály	seriál	k1gInPc1	seriál
Star	star	k1gFnSc1	star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Stanice	stanice	k1gFnPc1	stanice
Deep	Deep	k1gInSc4	Deep
Space	Space	k1gFnSc2	Space
Nine	Nin	k1gFnSc2	Nin
a	a	k8xC	a
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Voyager	Voyagra	k1gFnPc2	Voyagra
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
čase	čas	k1gInSc6	čas
jako	jako	k8xC	jako
události	událost	k1gFnSc3	událost
Nové	Nové	k2eAgFnPc1d1	Nové
generace	generace	k1gFnPc1	generace
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
které	který	k3yIgFnSc2	který
navíc	navíc	k6eAd1	navíc
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
4	[number]	k4	4
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Prequelový	Prequelový	k2eAgInSc1d1	Prequelový
seriál	seriál	k1gInSc1	seriál
s	s	k7c7	s
názvem	název	k1gInSc7	název
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Enterprise	Enterprise	k1gFnSc1	Enterprise
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
asi	asi	k9	asi
100	[number]	k4	100
let	léto	k1gNnPc2	léto
před	před	k7c7	před
prvním	první	k4xOgInSc7	první
seriálem	seriál	k1gInSc7	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
Star	star	k1gFnSc4	star
Trek	Treka	k1gFnPc2	Treka
natáčen	natáčen	k2eAgMnSc1d1	natáčen
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
filmového	filmový	k2eAgInSc2d1	filmový
rebootu	reboot	k1gInSc2	reboot
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
tři	tři	k4xCgInPc4	tři
filmy	film	k1gInPc4	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Discovery	Discovera	k1gFnPc1	Discovera
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
děj	děj	k1gInSc1	děj
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zasazen	zasazen	k2eAgInSc4d1	zasazen
do	do	k7c2	do
let	léto	k1gNnPc2	léto
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
původním	původní	k2eAgInSc7d1	původní
seriálem	seriál	k1gInSc7	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pojmem	pojem	k1gInSc7	pojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
mnoho	mnoho	k4c4	mnoho
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
jednoho	jeden	k4xCgMnSc4	jeden
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
31	[number]	k4	31
cen	cena	k1gFnPc2	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
cen	cena	k1gFnPc2	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
široké	široký	k2eAgFnSc3d1	široká
základně	základna	k1gFnSc3	základna
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
trekkies	trekkies	k1gInSc1	trekkies
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
např.	např.	kA	např.
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
nebo	nebo	k8xC	nebo
Whoopi	Whoopi	k1gNnPc1	Whoopi
Goldbergová	Goldbergový	k2eAgNnPc1d1	Goldbergový
<g/>
.	.	kIx.	.
</s>
<s>
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberr	k1gInPc4	Roddenberr
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
probíhala	probíhat	k5eAaImAgFnS	probíhat
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
závod	závod	k1gInSc4	závod
o	o	k7c4	o
dobývání	dobývání	k1gNnSc4	dobývání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
uvést	uvést	k5eAaPmF	uvést
seriál	seriál	k1gInSc4	seriál
o	o	k7c4	o
putování	putování	k1gNnSc4	putování
mezi	mezi	k7c7	mezi
planetami	planeta	k1gFnPc7	planeta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgInS	odehrávat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
není	být	k5eNaImIp3nS	být
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
nezdál	zdát	k5eNaImAgMnS	zdát
moc	moc	k6eAd1	moc
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
Roddenberry	Roddenberra	k1gFnSc2	Roddenberra
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
informován	informovat	k5eAaBmNgInS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
propadák	propadák	k1gInSc4	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
fanoušci	fanoušek	k1gMnPc1	fanoušek
svým	svůj	k3xOyFgNnSc7	svůj
naléháním	naléhání	k1gNnSc7	naléhání
na	na	k7c4	na
televizní	televizní	k2eAgFnSc4d1	televizní
společnost	společnost	k1gFnSc4	společnost
NBC	NBC	kA	NBC
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
život	život	k1gInSc4	život
seriálu	seriál	k1gInSc2	seriál
na	na	k7c4	na
3	[number]	k4	3
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
finálně	finálně	k6eAd1	finálně
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
seriál	seriál	k1gInSc1	seriál
obnoven	obnovit	k5eAaPmNgInS	obnovit
a	a	k8xC	a
Roddenberry	Roddenberra	k1gMnSc2	Roddenberra
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
Star	star	k1gFnSc4	star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Phase	Phase	k1gFnSc1	Phase
II	II	kA	II
(	(	kIx(	(
<g/>
fáze	fáze	k1gFnSc1	fáze
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
ideou	idea	k1gFnSc7	idea
Roddenberryho	Roddenberry	k1gMnSc2	Roddenberry
i	i	k8xC	i
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
seriály	seriál	k1gInPc4	seriál
byla	být	k5eAaImAgFnS	být
nejenom	nejenom	k6eAd1	nejenom
jednotnost	jednotnost	k1gFnSc1	jednotnost
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vynechání	vynechání	k1gNnSc1	vynechání
osobních	osobní	k2eAgFnPc2d1	osobní
hodnot	hodnota	k1gFnPc2	hodnota
jako	jako	k8xS	jako
rasismus	rasismus	k1gInSc1	rasismus
<g/>
,	,	kIx,	,
sexismus	sexismus	k1gInSc1	sexismus
<g/>
,	,	kIx,	,
xenofobie	xenofobie	k1gFnPc1	xenofobie
atp.	atp.	kA	atp.
Proto	proto	k8xC	proto
už	už	k6eAd1	už
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
seriálu	seriál	k1gInSc6	seriál
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Nyota	Nyota	k1gMnSc1	Nyota
Uhura	Uhura	k1gMnSc1	Uhura
coby	coby	k?	coby
zástupkyně	zástupkyně	k1gFnSc2	zástupkyně
černošské	černošský	k2eAgFnSc2d1	černošská
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
sezóny	sezóna	k1gFnSc2	sezóna
Pavel	Pavel	k1gMnSc1	Pavel
Čechov	Čechov	k1gMnSc1	Čechov
jako	jako	k8xS	jako
zřejmý	zřejmý	k2eAgMnSc1d1	zřejmý
zástupce	zástupce	k1gMnSc1	zástupce
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
bořit	bořit	k5eAaImF	bořit
zavedené	zavedený	k2eAgFnPc4d1	zavedená
společenské	společenský	k2eAgFnPc4d1	společenská
bariéry	bariéra	k1gFnPc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
takovým	takový	k3xDgInSc7	takový
mezníkem	mezník	k1gInSc7	mezník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
polibek	polibek	k1gInSc1	polibek
kapitána	kapitán	k1gMnSc2	kapitán
Kirka	Kirek	k1gMnSc2	Kirek
s	s	k7c7	s
Uhurou	Uhura	k1gFnSc7	Uhura
<g/>
,	,	kIx,	,
coby	coby	k?	coby
bělocha	běloch	k1gMnSc2	běloch
s	s	k7c7	s
černoškou	černoška	k1gFnSc7	černoška
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Roddenberry	Roddenberra	k1gFnSc2	Roddenberra
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
The	The	k1gFnSc2	The
Next	Next	k2eAgInSc4d1	Next
Generation	Generation	k1gInSc4	Generation
převzal	převzít	k5eAaPmAgMnS	převzít
Rick	Rick	k1gMnSc1	Rick
Berman	Berman	k1gMnSc1	Berman
<g/>
.	.	kIx.	.
</s>
<s>
TNG	TNG	kA	TNG
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
seriálem	seriál	k1gInSc7	seriál
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
Star	star	k1gFnSc2	star
Treků	Treek	k1gMnPc2	Treek
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Paramount	Paramounta	k1gFnPc2	Paramounta
proto	proto	k8xC	proto
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
seriálu	seriál	k1gInSc6	seriál
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gMnSc1	Space
Nine	Nine	k1gFnSc4	Nine
ještě	ještě	k9	ještě
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
TNG	TNG	kA	TNG
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
popisuje	popisovat	k5eAaImIp3nS	popisovat
budoucnost	budoucnost	k1gFnSc4	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
pohonu	pohon	k1gInSc2	pohon
cestuje	cestovat	k5eAaImIp3nS	cestovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
mimozemskými	mimozemský	k2eAgFnPc7d1	mimozemská
civilizacemi	civilizace	k1gFnPc7	civilizace
a	a	k8xC	a
přes	přes	k7c4	přes
časté	častý	k2eAgInPc4d1	častý
konflikty	konflikt	k1gInPc4	konflikt
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
šířit	šířit	k5eAaImF	šířit
mír	mír	k1gInSc4	mír
a	a	k8xC	a
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
porozumění	porozumění	k1gNnSc4	porozumění
<g/>
.	.	kIx.	.
</s>
<s>
Popsaná	popsaný	k2eAgFnSc1d1	popsaná
je	být	k5eAaImIp3nS	být
také	také	k9	také
"	"	kIx"	"
<g/>
historie	historie	k1gFnSc1	historie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
fiktivní	fiktivní	k2eAgFnPc4d1	fiktivní
události	událost	k1gFnPc4	událost
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
začal	začít	k5eAaPmAgInS	začít
točit	točit	k5eAaImF	točit
<g/>
,	,	kIx,	,
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
připadající	připadající	k2eAgFnSc1d1	připadající
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
uplynula	uplynout	k5eAaPmAgFnS	uplynout
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
tvoří	tvořit	k5eAaImIp3nS	tvořit
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
alternativní	alternativní	k2eAgFnSc3d1	alternativní
historii	historie	k1gFnSc3	historie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
začaly	začít	k5eAaPmAgFnP	začít
"	"	kIx"	"
<g/>
eugenické	eugenický	k2eAgFnPc1d1	eugenická
války	válka	k1gFnPc1	válka
<g/>
"	"	kIx"	"
vedené	vedený	k2eAgNnSc1d1	vedené
geneticky	geneticky	k6eAd1	geneticky
vylepšenými	vylepšený	k2eAgMnPc7d1	vylepšený
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
přes	přes	k7c4	přes
30	[number]	k4	30
000	[number]	k4	000
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
v	v	k7c6	v
metaforách	metafora	k1gFnPc6	metafora
a	a	k8xC	a
alegoriích	alegorie	k1gFnPc6	alegorie
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
mnoha	mnoho	k4c2	mnoho
aspektů	aspekt	k1gInPc2	aspekt
skutečného	skutečný	k2eAgInSc2d1	skutečný
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
)	)	kIx)	)
a	a	k8xC	a
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
epizod	epizoda	k1gFnPc2	epizoda
TOS	TOS	kA	TOS
naráží	narážet	k5eAaImIp3nS	narážet
např.	např.	kA	např.
na	na	k7c4	na
studenou	studený	k2eAgFnSc4d1	studená
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
Klingonská	Klingonský	k2eAgFnSc1d1	Klingonská
říše	říše	k1gFnSc1	říše
představuje	představovat	k5eAaImIp3nS	představovat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Voyager	Voyager	k1gInSc1	Voyager
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
spíše	spíše	k9	spíše
o	o	k7c6	o
univerzálních	univerzální	k2eAgFnPc6d1	univerzální
lidských	lidský	k2eAgFnPc6d1	lidská
hodnotách	hodnota	k1gFnPc6	hodnota
<g/>
,	,	kIx,	,
o	o	k7c6	o
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
i	i	k8xC	i
mezilidských	mezilidský	k2eAgInPc6d1	mezilidský
vztazích	vztah	k1gInPc6	vztah
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
různá	různý	k2eAgNnPc4d1	různé
morální	morální	k2eAgNnPc4d1	morální
či	či	k8xC	či
filosofická	filosofický	k2eAgNnPc4d1	filosofické
poselství	poselství	k1gNnSc4	poselství
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgNnSc7d1	časté
tématem	téma	k1gNnSc7	téma
bývá	bývat	k5eAaImIp3nS	bývat
např.	např.	kA	např.
morální	morální	k2eAgNnSc1d1	morální
dilema	dilema	k1gNnSc1	dilema
(	(	kIx(	(
<g/>
cit	cit	k1gInSc1	cit
vs	vs	k?	vs
<g/>
.	.	kIx.	.
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
etika	etika	k1gFnSc1	etika
vs	vs	k?	vs
<g/>
.	.	kIx.	.
přežití	přežití	k1gNnSc1	přežití
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
vs	vs	k?	vs
<g/>
.	.	kIx.	.
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
poznání	poznání	k1gNnSc6	poznání
vs	vs	k?	vs
<g/>
.	.	kIx.	.
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
respekt	respekt	k1gInSc1	respekt
k	k	k7c3	k
cizím	cizí	k2eAgFnPc3d1	cizí
kulturám	kultura	k1gFnPc3	kultura
vs	vs	k?	vs
<g/>
.	.	kIx.	.
vlastní	vlastní	k2eAgFnSc2d1	vlastní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jednání	jednání	k1gNnSc1	jednání
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
mezních	mezní	k2eAgFnPc6d1	mezní
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
jsou	být	k5eAaImIp3nP	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
i	i	k8xC	i
témata	téma	k1gNnPc4	téma
ve	v	k7c4	v
sci-fi	scii	k1gFnSc4	sci-fi
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
varování	varování	k1gNnSc1	varování
před	před	k7c7	před
nevyzpytatelnými	vyzpytatelný	k2eNgInPc7d1	nevyzpytatelný
důsledky	důsledek	k1gInPc7	důsledek
zneužití	zneužití	k1gNnSc2	zneužití
různých	různý	k2eAgFnPc2d1	různá
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc1	cestování
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
změny	změna	k1gFnPc4	změna
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
bioetika	bioetik	k1gMnSc2	bioetik
atd.	atd.	kA	atd.
Star	Star	kA	Star
Trek	Trek	k1gInSc4	Trek
se	se	k3xPyFc4	se
však	však	k9	však
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
také	také	k9	také
psychologických	psychologický	k2eAgNnPc2d1	psychologické
témat	téma	k1gNnPc2	téma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hledání	hledání	k1gNnPc4	hledání
vlastní	vlastní	k2eAgFnSc2d1	vlastní
identity	identita	k1gFnSc2	identita
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
postava	postava	k1gFnSc1	postava
Sedmá	sedmý	k4xOgFnSc1	sedmý
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
Devíti	devět	k4xCc2	devět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosofických	filosofický	k2eAgNnPc2d1	filosofické
témat	téma	k1gNnPc2	téma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
epizoda	epizoda	k1gFnSc1	epizoda
Voyageru	Voyager	k1gInSc2	Voyager
Přání	přání	k1gNnSc4	přání
zemřít	zemřít	k5eAaPmF	zemřít
má	mít	k5eAaImIp3nS	mít
téma	téma	k1gNnSc4	téma
převzaté	převzatý	k2eAgNnSc4d1	převzaté
z	z	k7c2	z
Čapkovy	Čapkův	k2eAgFnSc2d1	Čapkova
hry	hra	k1gFnSc2	hra
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulos	k1gMnSc1	Makropulos
<g/>
)	)	kIx)	)
i	i	k8xC	i
náboženských	náboženský	k2eAgNnPc2d1	náboženské
témat	téma	k1gNnPc2	téma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
epizody	epizoda	k1gFnSc2	epizoda
Voyageru	Voyager	k1gInSc2	Voyager
Směrnice	směrnice	k1gFnSc2	směrnice
Omega	omega	k1gNnPc2	omega
nebo	nebo	k8xC	nebo
Tělesná	tělesný	k2eAgNnPc4d1	tělesné
pouta	pouto	k1gNnPc4	pouto
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
ras	rasa	k1gFnPc2	rasa
ve	v	k7c6	v
Star	star	k1gFnSc6	star
Treku	Treka	k1gFnSc4	Treka
a	a	k8xC	a
Seznam	seznam	k1gInSc4	seznam
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
Star	Star	kA	Star
Treku	Trek	k1gInSc6	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
obydlených	obydlený	k2eAgFnPc2d1	obydlená
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
ras	rasa	k1gFnPc2	rasa
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
humanoidních	humanoidní	k2eAgFnPc2d1	humanoidní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
série	série	k1gFnSc2	série
patří	patřit	k5eAaImIp3nP	patřit
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
Vulkánci	Vulkánek	k1gMnPc1	Vulkánek
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
rasami	rasa	k1gFnPc7	rasa
tvořící	tvořící	k2eAgFnSc4d1	tvořící
mírumilovnou	mírumilovný	k2eAgFnSc4d1	mírumilovná
Federaci	federace	k1gFnSc4	federace
<g/>
,	,	kIx,	,
zrádní	zrádný	k2eAgMnPc1d1	zrádný
Romulané	Romulaný	k2eAgFnPc1d1	Romulaný
<g/>
,	,	kIx,	,
bojovní	bojovný	k2eAgMnPc1d1	bojovný
Klingoni	Klingon	k1gMnPc1	Klingon
a	a	k8xC	a
civilizace	civilizace	k1gFnPc1	civilizace
kyborgů	kyborg	k1gInPc2	kyborg
–	–	k?	–
Borgové	Borgový	k2eAgFnPc1d1	Borgový
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
technologií	technologie	k1gFnPc2	technologie
ve	v	k7c6	v
Star	Star	kA	Star
Treku	Trek	k1gInSc6	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Star	star	k1gFnSc1	star
Trek	Treka	k1gFnPc2	Treka
popisuje	popisovat	k5eAaImIp3nS	popisovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
desetiletí	desetiletí	k1gNnSc6	desetiletí
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
chronologie	chronologie	k1gFnSc1	chronologie
jsou	být	k5eAaImIp3nP	být
vynalézány	vynalézán	k2eAgFnPc4d1	vynalézán
další	další	k2eAgFnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
(	(	kIx(	(
<g/>
FTL	FTL	kA	FTL
<g/>
)	)	kIx)	)
pohonem	pohon	k1gInSc7	pohon
je	být	k5eAaImIp3nS	být
warp	warp	k1gInSc1	warp
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgFnSc4d1	další
používanou	používaný	k2eAgFnSc4d1	používaná
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
techniku	technika	k1gFnSc4	technika
patří	patřit	k5eAaImIp3nS	patřit
transportér	transportér	k1gInSc1	transportér
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
paprskovými	paprskový	k2eAgFnPc7d1	paprsková
zbraněmi	zbraň	k1gFnPc7	zbraň
(	(	kIx(	(
<g/>
phasery	phaser	k1gInPc1	phaser
a	a	k8xC	a
disruptory	disruptor	k1gInPc1	disruptor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
torpédy	torpédo	k1gNnPc7	torpédo
(	(	kIx(	(
<g/>
fotonovými	fotonový	k2eAgFnPc7d1	fotonová
<g/>
,	,	kIx,	,
plazmovými	plazmový	k2eAgInPc7d1	plazmový
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
)	)	kIx)	)
a	a	k8xC	a
štíty	štít	k1gInPc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Original	Original	k1gMnSc1	Original
Series	Series	k1gMnSc1	Series
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
TOS	TOS	kA	TOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
celou	celý	k2eAgFnSc4d1	celá
existenci	existence	k1gFnSc4	existence
fikčního	fikční	k2eAgInSc2d1	fikční
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc4	první
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
s	s	k7c7	s
názvem	název	k1gInSc7	název
Klec	klec	k1gFnSc1	klec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
společností	společnost	k1gFnSc7	společnost
NBC	NBC	kA	NBC
zamítnut	zamítnut	k2eAgMnSc1d1	zamítnut
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
premiéry	premiéra	k1gFnSc2	premiéra
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seriál	seriál	k1gInSc1	seriál
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
sezóny	sezóna	k1gFnPc4	sezóna
s	s	k7c7	s
dohromady	dohromady	k6eAd1	dohromady
80	[number]	k4	80
dílů	díl	k1gInPc2	díl
po	po	k7c6	po
49	[number]	k4	49
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
kapitán	kapitán	k1gMnSc1	kapitán
James	James	k1gMnSc1	James
Tiberius	Tiberius	k1gMnSc1	Tiberius
Kirk	Kirk	k1gMnSc1	Kirk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
velí	velet	k5eAaImIp3nS	velet
posádce	posádka	k1gFnSc3	posádka
lodi	loď	k1gFnSc2	loď
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc1	Enterprise
(	(	kIx(	(
<g/>
NCC-	NCC-	k1gFnSc1	NCC-
<g/>
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
na	na	k7c6	na
pětileté	pětiletý	k2eAgFnSc6d1	pětiletá
misi	mise	k1gFnSc6	mise
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
neznámý	známý	k2eNgInSc4d1	neznámý
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
navazovat	navazovat	k5eAaImF	navazovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
civilizacemi	civilizace	k1gFnPc7	civilizace
a	a	k8xC	a
hlásat	hlásat	k5eAaImF	hlásat
poslání	poslání	k1gNnSc4	poslání
Spojené	spojený	k2eAgFnSc2d1	spojená
federace	federace	k1gFnSc2	federace
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
vůbec	vůbec	k9	vůbec
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
chtěl	chtít	k5eAaImAgInS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
sci-fi	scii	k1gFnSc7	sci-fi
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
technologický	technologický	k2eAgInSc4d1	technologický
pokrok	pokrok	k1gInSc4	pokrok
lidstva	lidstvo	k1gNnSc2	lidstvo
daleké	daleký	k2eAgFnSc2d1	daleká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
odbourání	odbourání	k1gNnSc4	odbourání
projevů	projev	k1gInPc2	projev
rasismu	rasismus	k1gInSc2	rasismus
<g/>
,	,	kIx,	,
diskriminace	diskriminace	k1gFnSc1	diskriminace
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
negativ	negativum	k1gNnPc2	negativum
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
upravován	upravovat	k5eAaImNgInS	upravovat
a	a	k8xC	a
Roddenberry	Roddenberro	k1gNnPc7	Roddenberro
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
Zemi	zem	k1gFnSc4	zem
pod	pod	k7c4	pod
jednu	jeden	k4xCgFnSc4	jeden
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
lidstvo	lidstvo	k1gNnSc4	lidstvo
samotné	samotný	k2eAgNnSc4d1	samotné
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
spíše	spíše	k9	spíše
národ	národ	k1gInSc1	národ
v	v	k7c6	v
paletě	paleta	k1gFnSc6	paleta
různých	různý	k2eAgFnPc2d1	různá
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
kroky	krok	k1gInPc4	krok
provázely	provázet	k5eAaImAgInP	provázet
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
seriál	seriál	k1gInSc4	seriál
vůbec	vůbec	k9	vůbec
nechtěla	chtít	k5eNaImAgFnS	chtít
a	a	k8xC	a
úspěch	úspěch	k1gInSc4	úspěch
zažil	zažít	k5eAaPmAgMnS	zažít
Roddenberry	Roddenberr	k1gMnPc4	Roddenberr
až	až	k9	až
u	u	k7c2	u
Desilu	Desil	k1gInSc2	Desil
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
však	však	k9	však
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
kritické	kritický	k2eAgFnPc4d1	kritická
poznámky	poznámka	k1gFnPc4	poznámka
po	po	k7c4	po
natočení	natočení	k1gNnSc4	natočení
pilotního	pilotní	k2eAgInSc2d1	pilotní
dílu	díl	k1gInSc2	díl
Klec	klec	k1gFnSc1	klec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
a	a	k8xC	a
podpořen	podpořit	k5eAaPmNgInS	podpořit
dobrým	dobrý	k2eAgInSc7d1	dobrý
rozpočtem	rozpočet	k1gInSc7	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
však	však	k9	však
ustalo	ustat	k5eAaPmAgNnS	ustat
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Desilu	Desil	k1gMnSc3	Desil
bylo	být	k5eAaImAgNnS	být
koupeno	koupen	k2eAgNnSc1d1	koupeno
společností	společnost	k1gFnSc7	společnost
Paramount	Paramounta	k1gFnPc2	Paramounta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
snížila	snížit	k5eAaPmAgFnS	snížit
rozpočet	rozpočet	k1gInSc4	rozpočet
a	a	k8xC	a
sám	sám	k3xTgInSc4	sám
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
od	od	k7c2	od
další	další	k2eAgFnSc2d1	další
tvorby	tvorba	k1gFnSc2	tvorba
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
ukončen	ukončit	k5eAaPmNgInS	ukončit
při	při	k7c6	při
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
80	[number]	k4	80
epizod	epizoda	k1gFnPc2	epizoda
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
sériích	série	k1gFnPc6	série
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
jako	jako	k8xC	jako
chybné	chybný	k2eAgNnSc4d1	chybné
<g/>
,	,	kIx,	,
když	když	k8xS	když
Paramount	Paramount	k1gInSc1	Paramount
prodal	prodat	k5eAaPmAgInS	prodat
vysílací	vysílací	k2eAgNnPc4d1	vysílací
práva	právo	k1gNnPc4	právo
i	i	k9	i
jiným	jiný	k2eAgFnPc3d1	jiná
společnostem	společnost	k1gFnPc3	společnost
a	a	k8xC	a
Star	star	k1gFnSc3	star
Trek	Treka	k1gFnPc2	Treka
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
se	se	k3xPyFc4	se
i	i	k9	i
obnovení	obnovení	k1gNnSc3	obnovení
seriálu	seriál	k1gInSc2	seriál
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
společnosti	společnost	k1gFnSc2	společnost
velmi	velmi	k6eAd1	velmi
nákladné	nákladný	k2eAgFnPc1d1	nákladná
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
dabovali	dabovat	k5eAaBmAgMnP	dabovat
původní	původní	k2eAgMnPc1d1	původní
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Roddenberry	Roddenberra	k1gFnPc4	Roddenberra
původní	původní	k2eAgInSc1d1	původní
seriál	seriál	k1gInSc1	seriál
ohraničil	ohraničit	k5eAaPmAgInS	ohraničit
na	na	k7c4	na
trojici	trojice	k1gFnSc4	trojice
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
–	–	k?	–
kapitán	kapitán	k1gMnSc1	kapitán
James	James	k1gMnSc1	James
T.	T.	kA	T.
Kirk	Kirk	k1gMnSc1	Kirk
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Shatner	Shatner	k1gInSc1	Shatner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
Vulkánec	Vulkánec	k1gMnSc1	Vulkánec
Spock	Spock	k1gMnSc1	Spock
(	(	kIx(	(
<g/>
Leonard	Leonard	k1gMnSc1	Leonard
Nimoy	Nimoa	k1gFnSc2	Nimoa
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
lékař	lékař	k1gMnSc1	lékař
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Leonard	Leonard	k1gMnSc1	Leonard
McCoy	McCoa	k1gFnSc2	McCoa
(	(	kIx(	(
<g/>
DeForest	DeForest	k1gInSc1	DeForest
Kelley	Kellea	k1gFnSc2	Kellea
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
v	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
počtu	počet	k1gInSc6	počet
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
postavy	postav	k1gInPc1	postav
posádky	posádka	k1gFnSc2	posádka
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
taktiku	taktika	k1gFnSc4	taktika
se	se	k3xPyFc4	se
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
až	až	k9	až
u	u	k7c2	u
seriálu	seriál	k1gInSc2	seriál
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
dění	dění	k1gNnSc4	dění
na	na	k7c4	na
více	hodně	k6eAd2	hodně
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
(	(	kIx(	(
<g/>
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Animated	Animated	k1gMnSc1	Animated
Series	Series	k1gMnSc1	Series
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
TAS	tasit	k5eAaPmRp2nS	tasit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
je	být	k5eAaImIp3nS	být
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
sezónách	sezóna	k1gFnPc6	sezóna
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
22	[number]	k4	22
dílů	díl	k1gInPc2	díl
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
30	[number]	k4	30
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
menším	malý	k2eAgFnPc3d2	menší
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
jsou	být	k5eAaImIp3nP	být
totožní	totožný	k2eAgMnPc1d1	totožný
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgInSc4d1	předchozí
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Hlasy	hlas	k1gInPc1	hlas
animovaným	animovaný	k2eAgFnPc3d1	animovaná
postavám	postava	k1gFnPc3	postava
propůjčili	propůjčit	k5eAaPmAgMnP	propůjčit
herci	herec	k1gMnPc7	herec
původní	původní	k2eAgFnSc2d1	původní
posádky	posádka	k1gFnSc2	posádka
z	z	k7c2	z
TOS	TOS	kA	TOS
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
změněnými	změněný	k2eAgInPc7d1	změněný
hlasy	hlas	k1gInPc7	hlas
<g/>
)	)	kIx)	)
zahráli	zahrát	k5eAaPmAgMnP	zahrát
i	i	k8xC	i
role	role	k1gFnPc4	role
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
TNG	TNG	kA	TNG
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgMnS	začít
vznikat	vznikat	k5eAaImF	vznikat
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
7	[number]	k4	7
sezón	sezóna	k1gFnPc2	sezóna
<g/>
,	,	kIx,	,
178	[number]	k4	178
dílů	díl	k1gInPc2	díl
po	po	k7c6	po
45	[number]	k4	45
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
let	léto	k1gNnPc2	léto
po	po	k7c6	po
původním	původní	k2eAgInSc6d1	původní
seriálu	seriál	k1gInSc6	seriál
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2364	[number]	k4	2364
<g/>
-	-	kIx~	-
<g/>
2370	[number]	k4	2370
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
tak	tak	k6eAd1	tak
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
nová	nový	k2eAgFnSc1d1	nová
loď	loď	k1gFnSc1	loď
Enterprise-D	Enterprise-D	k1gFnSc2	Enterprise-D
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
posádkou	posádka	k1gFnSc7	posádka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
velí	velet	k5eAaImIp3nS	velet
kapitán	kapitán	k1gMnSc1	kapitán
Jean-Luc	Jean-Luc	k1gFnSc4	Jean-Luc
Picard	Picarda	k1gFnPc2	Picarda
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Patrick	Patrick	k1gMnSc1	Patrick
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k8xC	i
příslušníky	příslušník	k1gMnPc7	příslušník
mimozemských	mimozemský	k2eAgInPc2d1	mimozemský
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
poradkyně	poradkyně	k1gFnPc4	poradkyně
a	a	k8xC	a
poloviční	poloviční	k2eAgInSc4d1	poloviční
Betazoid	Betazoid	k1gInSc4	Betazoid
Deanna	Deanno	k1gNnSc2	Deanno
Troi	Tro	k1gFnSc2	Tro
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Marina	Marina	k1gFnSc1	Marina
Sirtis	Sirtis	k1gFnSc1	Sirtis
<g/>
)	)	kIx)	)
a	a	k8xC	a
poručík	poručík	k1gMnSc1	poručík
Worf	Worf	k1gMnSc1	Worf
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Michael	Michael	k1gMnSc1	Michael
Dorn	Dorn	k1gMnSc1	Dorn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
Klingon	Klingon	k1gMnSc1	Klingon
ve	v	k7c6	v
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
flotile	flotila	k1gFnSc6	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
posádky	posádka	k1gFnSc2	posádka
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgMnSc1	první
důstojník	důstojník	k1gMnSc1	důstojník
komandér	komandér	k1gMnSc1	komandér
William	William	k1gInSc4	William
Riker	Riker	k1gMnSc1	Riker
(	(	kIx(	(
<g/>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Frakes	Frakes	k1gMnSc1	Frakes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
důstojník	důstojník	k1gMnSc1	důstojník
android	android	k1gInSc1	android
Dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
Brent	Brent	k?	Brent
Spiner	Spiner	k1gInSc1	Spiner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lodní	lodní	k2eAgFnSc1d1	lodní
lékařka	lékařka	k1gFnSc1	lékařka
Beverly	Beverla	k1gFnSc2	Beverla
Crusherová	Crusherová	k1gFnSc1	Crusherová
(	(	kIx(	(
<g/>
Gates	Gates	k1gInSc1	Gates
McFadden	McFaddna	k1gFnPc2	McFaddna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
Geordi	Geord	k1gMnPc1	Geord
La	la	k1gNnSc4	la
Forge	Forg	k1gInSc2	Forg
(	(	kIx(	(
<g/>
LeVar	LeVar	k1gInSc1	LeVar
Burton	Burton	k1gInSc1	Burton
<g/>
)	)	kIx)	)
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
doktory	doktor	k1gMnPc4	doktor
Crusherové	Crusherový	k2eAgInPc1d1	Crusherový
Wesley	Wesley	k1gInPc1	Wesley
(	(	kIx(	(
<g/>
Wil	Wil	k1gFnSc1	Wil
Wheaton	Wheaton	k1gInSc1	Wheaton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
sedm	sedm	k4xCc4	sedm
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
dílu	díl	k1gInSc2	díl
byla	být	k5eAaImAgFnS	být
vysílána	vysílán	k2eAgFnSc1d1	vysílána
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
prezentované	prezentovaný	k2eAgNnSc1d1	prezentované
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
seriálu	seriál	k1gInSc6	seriál
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
jako	jako	k8xS	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
Star	star	k1gFnSc1	star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Stanice	stanice	k1gFnSc1	stanice
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gMnSc1	Space
Nine	Nine	k1gInSc1	Nine
a	a	k8xC	a
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
zpočátku	zpočátku	k6eAd1	zpočátku
řídil	řídit	k5eAaImAgMnS	řídit
tvůrce	tvůrce	k1gMnSc4	tvůrce
Star	Star	kA	Star
Treku	Treka	k1gMnSc4	Treka
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gMnSc2	Roddenberra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dramatický	dramatický	k2eAgInSc4d1	dramatický
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Obdržel	obdržet	k5eAaPmAgMnS	obdržet
také	také	k9	také
cenu	cena	k1gFnSc4	cena
Peabody	Peaboda	k1gFnSc2	Peaboda
Award	Award	k1gInSc4	Award
za	za	k7c4	za
epizodu	epizoda	k1gFnSc4	epizoda
Poslední	poslední	k2eAgFnSc4d1	poslední
sbohem	sbohem	k0	sbohem
(	(	kIx(	(
<g/>
The	The	k1gMnSc4	The
Big	Big	k1gMnSc4	Big
Goodbye	Goodby	k1gMnSc4	Goodby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
Hugo	Hugo	k1gMnSc1	Hugo
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
hrané	hraný	k2eAgNnSc4d1	hrané
představení	představení	k1gNnSc4	představení
získaly	získat	k5eAaPmAgInP	získat
epizody	epizoda	k1gFnPc4	epizoda
Vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Inner	Inner	k1gMnSc1	Inner
Light	Light	k1gMnSc1	Light
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
Všechno	všechen	k3xTgNnSc1	všechen
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
All	All	k1gFnSc1	All
Good	Good	k1gMnSc1	Good
Things	Things	k1gInSc1	Things
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Stanice	stanice	k1gFnPc1	stanice
Deep	Deep	k1gInSc4	Deep
Space	Space	k1gFnSc2	Space
Nine	Nin	k1gInSc2	Nin
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Stanice	stanice	k1gFnSc1	stanice
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gMnSc1	Space
Nine	Nine	k1gInSc1	Nine
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
DS	DS	kA	DS
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
176	[number]	k4	176
dílů	díl	k1gInPc2	díl
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
vysílat	vysílat	k5eAaImF	vysílat
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
dílu	díl	k1gInSc2	díl
byla	být	k5eAaImAgFnS	být
vysílána	vysílán	k2eAgFnSc1d1	vysílána
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
byly	být	k5eAaImAgFnP	být
odvysílány	odvysílat	k5eAaPmNgFnP	odvysílat
jen	jen	k9	jen
první	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
řady	řada	k1gFnPc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
Nové	Nová	k1gFnSc2	Nová
generace	generace	k1gFnSc2	generace
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
bezprostředně	bezprostředně	k6eAd1	bezprostředně
následujících	následující	k2eAgInPc2d1	následující
(	(	kIx(	(
<g/>
2369	[number]	k4	2369
<g/>
-	-	kIx~	-
<g/>
2375	[number]	k4	2375
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
seriál	seriál	k1gInSc4	seriál
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
na	na	k7c6	na
hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
lodi	loď	k1gFnSc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
postavenou	postavený	k2eAgFnSc7d1	postavená
rasou	rasa	k1gFnSc7	rasa
Cardassianů	Cardassian	k1gInPc2	Cardassian
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
Deep	Deep	k1gInSc4	Deep
Space	Spaec	k1gInPc1	Spaec
Nine	Nin	k1gInSc2	Nin
(	(	kIx(	(
<g/>
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
Hluboký	hluboký	k2eAgInSc4d1	hluboký
vesmír	vesmír	k1gInSc4	vesmír
devět	devět	k4xCc4	devět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Terok	Terok	k1gInSc1	Terok
Nor	Nora	k1gFnPc2	Nora
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
planety	planeta	k1gFnSc2	planeta
Bajor	Bajora	k1gFnPc2	Bajora
a	a	k8xC	a
unikátní	unikátní	k2eAgFnSc2d1	unikátní
stabilní	stabilní	k2eAgFnSc2d1	stabilní
červí	červí	k2eAgFnSc2d1	červí
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
60	[number]	k4	60
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
kvadrantu	kvadrant	k1gInSc2	kvadrant
gama	gama	k1gNnSc1	gama
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
sleduje	sledovat	k5eAaImIp3nS	sledovat
osudy	osud	k1gInPc4	osud
posádky	posádka	k1gFnSc2	posádka
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
komandér	komandér	k1gMnSc1	komandér
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
)	)	kIx)	)
Benjamin	Benjamin	k1gMnSc1	Benjamin
Sisko	Sisko	k1gNnSc4	Sisko
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Avery	Aver	k1gInPc4	Aver
Brooks	Brooksa	k1gFnPc2	Brooksa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
důstojníkem	důstojník	k1gMnSc7	důstojník
je	být	k5eAaImIp3nS	být
Bajoranka	Bajoranka	k1gFnSc1	Bajoranka
major	major	k1gMnSc1	major
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
plukovník	plukovník	k1gMnSc1	plukovník
<g/>
)	)	kIx)	)
Kira	Kira	k1gFnSc1	Kira
Nerys	Nerys	k1gInSc1	Nerys
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Nana	Nana	k1gMnSc1	Nana
Visitor	Visitor	k1gMnSc1	Visitor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
posádky	posádka	k1gFnSc2	posádka
jsou	být	k5eAaImIp3nP	být
šéf	šéf	k1gMnSc1	šéf
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
měňavec	měňavec	k1gMnSc1	měňavec
Odo	Odo	k1gMnSc1	Odo
(	(	kIx(	(
<g/>
René	René	k1gMnSc1	René
Auberjonois	Auberjonois	k1gFnSc2	Auberjonois
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
Julian	Julian	k1gMnSc1	Julian
Bashir	Bashir	k1gMnSc1	Bashir
(	(	kIx(	(
<g/>
Alexander	Alexandra	k1gFnPc2	Alexandra
Siddig	Siddig	k1gInSc1	Siddig
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
důstojnice	důstojnice	k1gFnSc1	důstojnice
z	z	k7c2	z
rasy	rasa	k1gFnSc2	rasa
Trillů	Trill	k1gMnPc2	Trill
Jadzia	Jadzia	k1gFnSc1	Jadzia
Dax	Dax	k1gFnSc1	Dax
(	(	kIx(	(
<g/>
Terry	Terr	k1gInPc1	Terr
Farrellová	Farrellová	k1gFnSc1	Farrellová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
komandéra	komandér	k1gMnSc2	komandér
Siska	Sisko	k1gNnSc2	Sisko
Jake	Jak	k1gMnSc2	Jak
(	(	kIx(	(
<g/>
Cirroc	Cirroc	k1gInSc1	Cirroc
Lofton	Lofton	k1gInSc1	Lofton
<g/>
)	)	kIx)	)
a	a	k8xC	a
ferengský	ferengský	k2eAgMnSc1d1	ferengský
barman	barman	k1gMnSc1	barman
Quark	Quark	k1gInSc1	Quark
(	(	kIx(	(
<g/>
Armin	Armin	k1gMnSc1	Armin
Shimerman	Shimerman	k1gMnSc1	Shimerman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
přišli	přijít	k5eAaPmAgMnP	přijít
inženýr	inženýr	k1gMnSc1	inženýr
Miles	Miles	k1gMnSc1	Miles
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k2eAgInSc4d1	Brien
(	(	kIx(	(
<g/>
Colm	Colm	k1gInSc4	Colm
Meaney	Meanea	k1gFnSc2	Meanea
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
ještě	ještě	k6eAd1	ještě
Klingon	Klingon	k1gMnSc1	Klingon
Worf	Worf	k1gMnSc1	Worf
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Dorn	Dorn	k1gMnSc1	Dorn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příchod	příchod	k1gInSc1	příchod
měl	mít	k5eAaImAgInS	mít
pomoci	pomoct	k5eAaPmF	pomoct
zlepšit	zlepšit	k5eAaPmF	zlepšit
sledovanost	sledovanost	k1gFnSc4	sledovanost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
přidělena	přidělen	k2eAgFnSc1d1	přidělena
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
Defiant	Defianta	k1gFnPc2	Defianta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
posádce	posádka	k1gFnSc3	posádka
i	i	k9	i
scenáristům	scenárista	k1gMnPc3	scenárista
nové	nový	k2eAgFnSc2d1	nová
možnosti	možnost	k1gFnSc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
vinou	vinout	k5eAaImIp3nP	vinout
celým	celý	k2eAgInSc7d1	celý
seriálem	seriál	k1gInSc7	seriál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dopady	dopad	k1gInPc4	dopad
cardassijské	cardassijský	k2eAgFnSc2d1	cardassijská
okupace	okupace	k1gFnSc2	okupace
Bajoru	Bajor	k1gInSc2	Bajor
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
Siska	Siska	k1gFnSc1	Siska
jako	jako	k8xC	jako
Vyslance	vyslanec	k1gMnPc4	vyslanec
Proroků	prorok	k1gMnPc2	prorok
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnPc1d1	významná
náboženské	náboženský	k2eAgFnPc1d1	náboženská
osobnosti	osobnost	k1gFnPc1	osobnost
pro	pro	k7c4	pro
Bajorany	Bajorana	k1gFnPc4	Bajorana
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
řadách	řada	k1gFnPc6	řada
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mocností	mocnost	k1gFnSc7	mocnost
z	z	k7c2	z
kvadrantu	kvadrant	k1gInSc2	kvadrant
gama	gama	k1gNnSc1	gama
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Dominion	dominion	k1gNnSc4	dominion
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
seriálů	seriál	k1gInPc2	seriál
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
se	se	k3xPyFc4	se
tak	tak	k9	tak
liší	lišit	k5eAaImIp3nS	lišit
delšími	dlouhý	k2eAgInPc7d2	delší
příběhy	příběh	k1gInPc7	příběh
tvořenými	tvořený	k2eAgInPc7d1	tvořený
několika	několik	k4yIc7	několik
epizodami	epizoda	k1gFnPc7	epizoda
<g/>
,	,	kIx,	,
konfliktem	konflikt	k1gInSc7	konflikt
mezi	mezi	k7c7	mezi
rasami	rasa	k1gFnPc7	rasa
a	a	k8xC	a
náboženskými	náboženský	k2eAgNnPc7d1	náboženské
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
diváci	divák	k1gMnPc1	divák
a	a	k8xC	a
kritici	kritik	k1gMnPc1	kritik
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
které	který	k3yIgFnPc1	který
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
zakázal	zakázat	k5eAaPmAgInS	zakázat
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
seriálu	seriál	k1gInSc6	seriál
a	a	k8xC	a
Nové	Nové	k2eAgFnSc3d1	Nové
generaci	generace	k1gFnSc3	generace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Roddenberry	Roddenberr	k1gInPc4	Roddenberr
byl	být	k5eAaImAgInS	být
o	o	k7c6	o
plánech	plán	k1gInPc6	plán
na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
DS9	DS9	k1gFnSc2	DS9
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
informován	informovat	k5eAaBmNgMnS	informovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
tak	tak	k9	tak
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
seriál	seriál	k1gInSc4	seriál
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
spojovat	spojovat	k5eAaImF	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
i	i	k8xC	i
Stanice	stanice	k1gFnSc1	stanice
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gMnSc1	Space
Nine	Nine	k1gNnPc2	Nine
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc1	několik
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
za	za	k7c4	za
make-up	makep	k1gInSc4	make-up
nebo	nebo	k8xC	nebo
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Voyager	Voyager	k1gInSc1	Voyager
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
VOY	VOY	kA	VOY
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
sezón	sezóna	k1gFnPc2	sezóna
<g/>
,	,	kIx,	,
172	[number]	k4	172
epizod	epizoda	k1gFnPc2	epizoda
po	po	k7c6	po
45	[number]	k4	45
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Voyager	Voyager	k1gInSc4	Voyager
s	s	k7c7	s
kapitánkou	kapitánka	k1gFnSc7	kapitánka
Katheryn	Katheryno	k1gNnPc2	Katheryno
Janeway	Janewaa	k1gMnSc2	Janewaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tohoto	tento	k3xDgInSc2	tento
seriálu	seriál	k1gInSc2	seriál
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
po	po	k7c4	po
Roddenberryho	Roddenberry	k1gMnSc4	Roddenberry
smrti	smrt	k1gFnSc2	smrt
Brannon	Brannon	k1gMnSc1	Brannon
Braga	Braga	k1gFnSc1	Braga
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
směřoval	směřovat	k5eAaImAgInS	směřovat
dost	dost	k6eAd1	dost
odlišně	odlišně	k6eAd1	odlišně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
u	u	k7c2	u
významné	významný	k2eAgFnSc2d1	významná
části	část	k1gFnSc2	část
fanoušků	fanoušek	k1gMnPc2	fanoušek
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
rozhořčení	rozhořčení	k1gNnSc1	rozhořčení
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
jsou	být	k5eAaImIp3nP	být
častá	častý	k2eAgNnPc4d1	časté
narušení	narušení	k1gNnPc4	narušení
kontinuity	kontinuita	k1gFnSc2	kontinuita
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Enterprise	Enterprise	k1gFnSc1	Enterprise
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Enterprise	Enterprise	k1gFnSc1	Enterprise
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
ENT	ENT	kA	ENT
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Brannona	Brannon	k1gMnSc2	Brannon
Bragy	Braga	k1gFnSc2	Braga
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
měla	mít	k5eAaImAgFnS	mít
4	[number]	k4	4
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
98	[number]	k4	98
epizod	epizoda	k1gFnPc2	epizoda
po	po	k7c6	po
45	[number]	k4	45
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
před	před	k7c7	před
všemi	všecek	k3xTgMnPc7	všecek
předchozími	předchozí	k2eAgMnPc7d1	předchozí
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ještě	ještě	k9	ještě
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Enterprise	Enterprise	k1gFnSc1	Enterprise
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc3	označení
NX-	NX-	k1gFnSc7	NX-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Discovery	Discovera	k1gFnPc1	Discovera
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Discovery	Discovera	k1gFnSc2	Discovera
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
následující	následující	k2eAgFnSc2d1	následující
epizody	epizoda	k1gFnSc2	epizoda
budou	být	k5eAaImBp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
online	onlinout	k5eAaPmIp3nS	onlinout
na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
platformě	platforma	k1gFnSc6	platforma
CBS	CBS	kA	CBS
All	All	k1gFnSc4	All
Access	Accessa	k1gFnPc2	Accessa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sezóna	sezóna	k1gFnSc1	sezóna
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
13	[number]	k4	13
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
budou	být	k5eAaImBp3nP	být
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgInPc3d1	předchozí
seriálům	seriál	k1gInPc3	seriál
vzájemně	vzájemně	k6eAd1	vzájemně
dějově	dějově	k6eAd1	dějově
provázány	provázán	k2eAgFnPc1d1	provázána
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
asi	asi	k9	asi
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
před	před	k7c4	před
příběhy	příběh	k1gInPc4	příběh
původní	původní	k2eAgFnSc2d1	původní
"	"	kIx"	"
<g/>
TOSky	Toska	k1gFnSc2	Toska
<g/>
"	"	kIx"	"
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
posádce	posádka	k1gFnSc6	posádka
lodi	loď	k1gFnSc2	loď
USS	USS	kA	USS
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
bude	být	k5eAaImBp3nS	být
žena	žena	k1gFnSc1	žena
–	–	k?	–
první	první	k4xOgMnSc1	první
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
lodi	loď	k1gFnSc2	loď
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
objevit	objevit	k5eAaPmF	objevit
více	hodně	k6eAd2	hodně
mimozemských	mimozemský	k2eAgInPc2d1	mimozemský
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
úplně	úplně	k6eAd1	úplně
nových	nový	k2eAgMnPc2d1	nový
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
Star	star	k1gFnSc4	star
Treku	Trek	k1gInSc2	Trek
neobjevily	objevit	k5eNaPmAgFnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
bude	být	k5eAaImBp3nS	být
také	také	k9	také
homosexuální	homosexuální	k2eAgFnSc1d1	homosexuální
postava	postava	k1gFnSc1	postava
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
seriál	seriál	k1gInSc1	seriál
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
připravoval	připravovat	k5eAaImAgMnS	připravovat
Gene	gen	k1gInSc5	gen
Roddenberry	Roddenberra	k1gFnPc1	Roddenberra
druhý	druhý	k4xOgInSc1	druhý
hraný	hraný	k2eAgInSc1d1	hraný
seriál	seriál	k1gInSc1	seriál
s	s	k7c7	s
názvem	název	k1gInSc7	název
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Phase	Phase	k1gFnSc2	Phase
II	II	kA	II
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
"	"	kIx"	"
<g/>
tosku	toska	k1gFnSc4	toska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vysílání	vysílání	k1gNnSc1	vysílání
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výroby	výroba	k1gFnSc2	výroba
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
realizace	realizace	k1gFnSc1	realizace
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Rekvizity	rekvizita	k1gFnPc1	rekvizita
a	a	k8xC	a
kulisy	kulisa	k1gFnPc1	kulisa
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
celovečerním	celovečerní	k2eAgInSc6d1	celovečerní
snímku	snímek	k1gInSc6	snímek
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
přepracováním	přepracování	k1gNnSc7	přepracování
scénáře	scénář	k1gInSc2	scénář
úvodního	úvodní	k2eAgInSc2d1	úvodní
pilotního	pilotní	k2eAgInSc2d1	pilotní
dvouhodinového	dvouhodinový	k2eAgInSc2d1	dvouhodinový
dílu	díl	k1gInSc2	díl
Phase	Phase	k1gFnSc2	Phase
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zabývat	zabývat	k5eAaImF	zabývat
druhou	druhý	k4xOgFnSc7	druhý
pětiletou	pětiletý	k2eAgFnSc7d1	pětiletá
misí	mise	k1gFnSc7	mise
Enterprise	Enterprise	k1gFnSc2	Enterprise
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
Kirka	Kirek	k1gMnSc2	Kirek
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Phase	Phase	k1gFnSc2	Phase
II	II	kA	II
následně	následně	k6eAd1	následně
využili	využít	k5eAaPmAgMnP	využít
fanoušci	fanoušek	k1gMnPc1	fanoušek
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgInSc4d1	vlastní
produkovaný	produkovaný	k2eAgInSc4d1	produkovaný
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Enterprise	Enterprise	k1gFnSc1	Enterprise
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
režisér	režisér	k1gMnSc1	režisér
Bryan	Bryan	k1gMnSc1	Bryan
Singer	Singer	k1gMnSc1	Singer
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
Christopher	Christophra	k1gFnPc2	Christophra
McQuarrie	McQuarrie	k1gFnSc2	McQuarrie
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Robert	Robert	k1gMnSc1	Robert
Meyer	Meyer	k1gMnSc1	Meyer
Burnett	Burnett	k1gMnSc1	Burnett
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
návrh	návrh	k1gInSc4	návrh
nového	nový	k2eAgInSc2d1	nový
seriálu	seriál	k1gInSc2	seriál
s	s	k7c7	s
názvem	název	k1gInSc7	název
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
poté	poté	k6eAd1	poté
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
Paramountu	Paramounta	k1gFnSc4	Paramounta
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
ale	ale	k8xC	ale
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
realizaci	realizace	k1gFnSc4	realizace
vybralo	vybrat	k5eAaPmAgNnS	vybrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
J.	J.	kA	J.
J.	J.	kA	J.
Abramse	Abrams	k1gMnSc2	Abrams
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
natočil	natočit	k5eAaBmAgMnS	natočit
filmový	filmový	k2eAgMnSc1d1	filmový
reboot	reboot	k1gMnSc1	reboot
celé	celý	k2eAgFnSc2d1	celá
série	série	k1gFnSc2	série
pod	pod	k7c7	pod
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
názvem	název	k1gInSc7	název
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc4	děj
seriálu	seriál	k1gInSc2	seriál
Federation	Federation	k1gInSc1	Federation
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
odehrávat	odehrávat	k5eAaImF	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
600	[number]	k4	600
let	léto	k1gNnPc2	léto
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
z	z	k7c2	z
dějově	dějově	k6eAd1	dějově
nejpozději	pozdě	k6eAd3	pozdě
zasazených	zasazený	k2eAgInPc2d1	zasazený
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
televizní	televizní	k2eAgMnSc1d1	televizní
producent	producent	k1gMnSc1	producent
David	David	k1gMnSc1	David
Foster	Foster	k1gMnSc1	Foster
připravuje	připravovat	k5eAaImIp3nS	připravovat
koncept	koncept	k1gInSc4	koncept
dalšího	další	k2eAgInSc2d1	další
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
realitě	realita	k1gFnSc6	realita
Star	Star	kA	Star
Treku	Treka	k1gMnSc4	Treka
do	do	k7c2	do
po-voyagerovské	pooyagerovský	k2eAgFnSc2d1	po-voyagerovský
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
konec	konec	k1gInSc4	konec
24	[number]	k4	24
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zájmu	zájem	k1gInSc2	zájem
a	a	k8xC	a
realizace	realizace	k1gFnSc1	realizace
jej	on	k3xPp3gMnSc4	on
měla	mít	k5eAaImAgFnS	mít
vysílat	vysílat	k5eAaImF	vysílat
stanice	stanice	k1gFnSc1	stanice
CBS	CBS	kA	CBS
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
oficiálních	oficiální	k2eAgInPc2d1	oficiální
seriálů	seriál	k1gInPc2	seriál
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
příběhů	příběh	k1gInPc2	příběh
natočených	natočený	k2eAgInPc2d1	natočený
amatérsky	amatérsky	k6eAd1	amatérsky
fanoušky	fanoušek	k1gMnPc7	fanoušek
(	(	kIx(	(
<g/>
o	o	k7c4	o
psané	psaný	k2eAgInPc4d1	psaný
fan	fana	k1gFnPc2	fana
fiction	fiction	k1gInSc1	fiction
nemluvě	nemluva	k1gFnSc3	nemluva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
celý	celý	k2eAgInSc1d1	celý
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc4	Trek
<g/>
:	:	kIx,	:
Hidden	Hiddna	k1gFnPc2	Hiddna
Frontier	Frontier	k1gInSc4	Frontier
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
řadami	řada	k1gFnPc7	řada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Phase	Phase	k1gFnSc1	Phase
II	II	kA	II
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
New	New	k1gMnSc1	New
Voyages	Voyages	k1gMnSc1	Voyages
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
seriál	seriál	k1gInSc4	seriál
a	a	k8xC	a
amatérští	amatérský	k2eAgMnPc1d1	amatérský
tvůrci	tvůrce	k1gMnPc1	tvůrce
si	se	k3xPyFc3	se
dali	dát	k5eAaPmAgMnP	dát
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
nahradit	nahradit	k5eAaPmF	nahradit
jeho	jeho	k3xOp3gNnSc4	jeho
nerealizované	realizovaný	k2eNgNnSc4d1	nerealizované
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
neoficiální	neoficiální	k2eAgNnPc1d1	neoficiální
díla	dílo	k1gNnPc1	dílo
nemají	mít	k5eNaImIp3nP	mít
licenci	licence	k1gFnSc4	licence
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Paramount	Paramount	k1gInSc1	Paramount
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
vzniku	vznik	k1gInSc3	vznik
nijak	nijak	k6eAd1	nijak
významně	významně	k6eAd1	významně
nebrání	bránit	k5eNaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
(	(	kIx(	(
<g/>
filmová	filmový	k2eAgFnSc1d1	filmová
série	série	k1gFnSc1	série
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
13	[number]	k4	13
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
šest	šest	k4xCc1	šest
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
seriál	seriál	k1gInSc4	seriál
(	(	kIx(	(
<g/>
TOS	TOS	kA	TOS
<g/>
)	)	kIx)	)
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
posádkou	posádka	k1gFnSc7	posádka
<g/>
:	:	kIx,	:
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
užívaná	užívaný	k2eAgFnSc1d1	užívaná
zkratka	zkratka	k1gFnSc1	zkratka
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
TMP	TMP	kA	TMP
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
II	II	kA	II
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Khanův	Khanův	k2eAgInSc1d1	Khanův
hněv	hněv	k1gInSc1	hněv
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
II	II	kA	II
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Wrath	Wrath	k1gMnSc1	Wrath
of	of	k?	of
Khan	Khan	k1gMnSc1	Khan
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
TWK	TWK	kA	TWK
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
III	III	kA	III
<g/>
:	:	kIx,	:
Pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
Spockovi	Spocek	k1gMnSc6	Spocek
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
III	III	kA	III
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Search	Search	k1gMnSc1	Search
for	forum	k1gNnPc2	forum
Spock	Spock	k1gInSc1	Spock
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
TSS	TSS	kA	TSS
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
IV	IV	kA	IV
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
IV	IV	kA	IV
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Voyage	Voyag	k1gMnSc2	Voyag
Home	Home	k1gNnSc2	Home
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
TVH	TVH	kA	TVH
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treko	k1gNnPc2	Treko
V	v	k7c4	v
<g/>
:	:	kIx,	:
Nejzazší	zadní	k2eAgFnPc4d3	nejzazší
hranice	hranice	k1gFnPc4	hranice
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
<g />
.	.	kIx.	.
</s>
<s>
Frontier	Frontier	k1gInSc1	Frontier
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
TFF	TFF	kA	TFF
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
VI	VI	kA	VI
<g/>
:	:	kIx,	:
Neobjevená	objevený	k2eNgFnSc1d1	neobjevená
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Undiscovered	Undiscovered	k1gMnSc1	Undiscovered
Country	country	k2eAgMnSc1d1	country
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
TUC	TUC	kA	TUC
<g/>
)	)	kIx)	)
Další	další	k2eAgFnPc1d1	další
čtyři	čtyři	k4xCgNnPc1	čtyři
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c6	na
Star	Star	kA	Star
Trek	Trek	k1gInSc4	Trek
<g/>
:	:	kIx,	:
Novou	nový	k2eAgFnSc4d1	nová
generaci	generace	k1gFnSc4	generace
<g/>
:	:	kIx,	:
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Generations	Generations	k1gInSc1	Generations
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
GEN	gen	kA	gen
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
První	první	k4xOgInSc1	první
kontakt	kontakt	k1gInSc1	kontakt
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
First	First	k1gMnSc1	First
Contact	Contact	k1gMnSc1	Contact
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
FC	FC	kA	FC
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Vzpoura	vzpoura	k1gFnSc1	vzpoura
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Insurrection	Insurrection	k1gInSc1	Insurrection
–	–	k?	–
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
INS	INS	kA	INS
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Nemesis	Nemesis	k1gFnSc1	Nemesis
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Nemesis	Nemesis	k1gFnSc1	Nemesis
-	-	kIx~	-
ST	St	kA	St
<g/>
:	:	kIx,	:
<g/>
NEM	NEM	kA	NEM
<g/>
)	)	kIx)	)
Následující	následující	k2eAgInPc1d1	následující
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
období	období	k1gNnSc2	období
TOS	TOS	kA	TOS
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ale	ale	k8xC	ale
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
alternativní	alternativní	k2eAgFnSc4d1	alternativní
realitu	realita	k1gFnSc4	realita
<g/>
:	:	kIx,	:
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Do	do	k7c2	do
temnoty	temnota	k1gFnSc2	temnota
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
Into	Into	k1gNnSc1	Into
Darkness	Darkness	k1gInSc1	Darkness
<g/>
)	)	kIx)	)
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Do	do	k7c2	do
neznáma	neznámo	k1gNnSc2	neznámo
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
;	;	kIx,	;
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
Beyond	Beyond	k1gMnSc1	Beyond
<g/>
)	)	kIx)	)
Tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
chronologii	chronologie	k1gFnSc4	chronologie
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
ve	v	k7c6	v
fiktivní	fiktivní	k2eAgFnSc6d1	fiktivní
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
sloupci	sloupec	k1gInSc6	sloupec
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
základní	základní	k2eAgFnSc2d1	základní
dějové	dějový	k2eAgFnSc2d1	dějová
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
názvů	název	k1gInPc2	název
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
s	s	k7c7	s
časovým	časový	k2eAgInSc7d1	časový
posunem	posun	k1gInSc7	posun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
seriálů	seriál	k1gInPc2	seriál
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
epizod	epizoda	k1gFnPc2	epizoda
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
prvek	prvek	k1gInSc4	prvek
cestování	cestování	k1gNnSc2	cestování
časem	časem	k6eAd1	časem
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
přehlednosti	přehlednost	k1gFnSc2	přehlednost
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
zaneseno	zanesen	k2eAgNnSc1d1	zaneseno
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInPc1d3	nejnovější
filmy	film	k1gInPc1	film
z	z	k7c2	z
alternativní	alternativní	k2eAgFnSc2d1	alternativní
reality	realita	k1gFnSc2	realita
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
bloku	blok	k1gInSc6	blok
sloupců	sloupec	k1gInPc2	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
i	i	k9	i
řada	řada	k1gFnSc1	řada
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Starfleet	Starfleeta	k1gFnPc2	Starfleeta
Command	Command	k1gInSc1	Command
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Armada	Armada	k1gFnSc1	Armada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
knih	kniha	k1gFnPc2	kniha
–	–	k?	–
od	od	k7c2	od
knižních	knižní	k2eAgNnPc2d1	knižní
vydání	vydání	k1gNnPc2	vydání
natočených	natočený	k2eAgFnPc2d1	natočená
epizod	epizoda	k1gFnPc2	epizoda
přes	přes	k7c4	přes
nové	nový	k2eAgFnPc4d1	nová
epizody	epizoda	k1gFnPc4	epizoda
existujících	existující	k2eAgFnPc2d1	existující
sérií	série	k1gFnPc2	série
(	(	kIx(	(
<g/>
především	především	k9	především
původní	původní	k2eAgFnSc2d1	původní
a	a	k8xC	a
Nové	Nové	k2eAgFnSc2d1	Nové
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
ke	k	k7c3	k
zcela	zcela	k6eAd1	zcela
nezávislým	závislý	k2eNgInSc7d1	nezávislý
–	–	k?	–
a	a	k8xC	a
několik	několik	k4yIc4	několik
komiksů	komiks	k1gInPc2	komiks
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
neoficiální	neoficiální	k2eAgNnSc4d1	neoficiální
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
crossover	crossover	k1gInSc1	crossover
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
prolínání	prolínání	k1gNnSc1	prolínání
<g/>
)	)	kIx)	)
s	s	k7c7	s
X-Meny	X-Men	k1gInPc7	X-Men
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Star	Star	kA	Star
Treku	Trek	k1gInSc6	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
Star	Star	kA	Star
Treku	Trek	k1gInSc6	Trek
jsou	být	k5eAaImIp3nP	být
řazené	řazený	k2eAgInPc1d1	řazený
do	do	k7c2	do
několika	několik	k4yIc3	několik
sérií	série	k1gFnPc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Originální	originální	k2eAgFnSc1d1	originální
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
příběhy	příběh	k1gInPc7	příběh
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
příslušné	příslušný	k2eAgInPc4d1	příslušný
televizní	televizní	k2eAgInPc4d1	televizní
seriály	seriál	k1gInPc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
řady	řada	k1gFnPc1	řada
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
odlišitelné	odlišitelný	k2eAgInPc4d1	odlišitelný
i	i	k9	i
formátem	formát	k1gInSc7	formát
a	a	k8xC	a
grafickou	grafický	k2eAgFnSc7d1	grafická
úpravou	úprava	k1gFnSc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byly	být	k5eAaImAgFnP	být
i	i	k9	i
číslované	číslovaný	k2eAgFnPc1d1	číslovaná
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
místo	místo	k7c2	místo
čísel	číslo	k1gNnPc2	číslo
měly	mít	k5eAaImAgFnP	mít
označení	označení	k1gNnSc4	označení
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Knih	kniha	k1gFnPc2	kniha
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hry	hra	k1gFnSc2	hra
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
Star	star	k1gFnPc2	star
Treku	Trek	k1gInSc2	Trek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
poměrně	poměrně	k6eAd1	poměrně
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
řada	řada	k1gFnSc1	řada
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
hry	hra	k1gFnPc4	hra
oficiální	oficiální	k2eAgFnPc4d1	oficiální
(	(	kIx(	(
<g/>
licencované	licencovaný	k2eAgFnPc4d1	licencovaná
<g/>
)	)	kIx)	)
a	a	k8xC	a
hry	hra	k1gFnPc1	hra
od	od	k7c2	od
fanoušků	fanoušek	k1gMnPc2	fanoušek
Star	star	k1gFnSc7	star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existuje	existovat	k5eAaImIp3nS	existovat
před	před	k7c4	před
50	[number]	k4	50
oficiálních	oficiální	k2eAgInPc2d1	oficiální
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
populárním	populární	k2eAgFnPc3d1	populární
hrám	hra	k1gFnPc3	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
patří	patřit	k5eAaImIp3nS	patřit
MMORPG	MMORPG	kA	MMORPG
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
Online	Onlin	k1gInSc5	Onlin
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgNnPc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
známým	známý	k2eAgInSc7d1	známý
přírůstkem	přírůstek	k1gInSc7	přírůstek
do	do	k7c2	do
série	série	k1gFnSc2	série
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
ve	v	k7c6	v
virtuální	virtuální	k2eAgFnSc6d1	virtuální
realitě	realita	k1gFnSc6	realita
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Bridge	Bridge	k1gInSc1	Bridge
Crew	Crew	k1gFnSc2	Crew
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
filmům	film	k1gInPc3	film
a	a	k8xC	a
seriálům	seriál	k1gInPc3	seriál
se	se	k3xPyFc4	se
oficiální	oficiální	k2eAgFnSc2d1	oficiální
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Star	Star	kA	Star
Treku	Treka	k1gFnSc4	Treka
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
kritikách	kritika	k1gFnPc6	kritika
těšily	těšit	k5eAaImAgFnP	těšit
pouze	pouze	k6eAd1	pouze
průměrnému	průměrný	k2eAgInSc3d1	průměrný
až	až	k8xS	až
negativnímu	negativní	k2eAgNnSc3d1	negativní
hodnocení	hodnocení	k1gNnSc3	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
navíc	navíc	k6eAd1	navíc
společnost	společnost	k1gFnSc1	společnost
Activision	Activision	k1gInSc4	Activision
žalovala	žalovat	k5eAaImAgFnS	žalovat
držitele	držitel	k1gMnPc4	držitel
práv	práv	k2eAgInSc4d1	práv
Star	star	k1gInSc4	star
Treku	Trek	k1gInSc2	Trek
<g/>
,	,	kIx,	,
firmu	firma	k1gFnSc4	firma
Viacom	Viacom	k1gInSc4	Viacom
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
nedodržovala	dodržovat	k5eNaImAgFnS	dodržovat
podmínky	podmínka	k1gFnPc4	podmínka
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
rozvojem	rozvoj	k1gInSc7	rozvoj
filmů	film	k1gInPc2	film
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
utlumovala	utlumovat	k5eAaImAgFnS	utlumovat
i	i	k9	i
prodej	prodej	k1gInSc4	prodej
her	hra	k1gFnPc2	hra
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
licencí	licence	k1gFnSc7	licence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
právě	právě	k6eAd1	právě
Activision	Activision	k1gInSc4	Activision
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
řadu	řada	k1gFnSc4	řada
platforem	platforma	k1gFnPc2	platforma
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgFnP	být
vyvíjeny	vyvíjet	k5eAaImNgFnP	vyvíjet
od	od	k7c2	od
8	[number]	k4	8
<g/>
bitových	bitový	k2eAgNnPc2d1	bitové
Atari	Atari	k1gNnPc2	Atari
a	a	k8xC	a
Apple	Apple	kA	Apple
II	II	kA	II
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
počítače	počítač	k1gInPc4	počítač
Amiga	Amig	k1gMnSc2	Amig
<g/>
,	,	kIx,	,
konzole	konzola	k1gFnSc3	konzola
Xbox	Xbox	k1gInSc1	Xbox
<g/>
,	,	kIx,	,
SONY	Sony	kA	Sony
PlayStation	PlayStation	k1gInSc1	PlayStation
až	až	k9	až
po	po	k7c6	po
PC	PC	kA	PC
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
MS-DOS	MS-DOS	k1gFnSc2	MS-DOS
nebo	nebo	k8xC	nebo
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Enterprise	Enterprise	k1gFnSc2	Enterprise
(	(	kIx(	(
<g/>
raketoplán	raketoplán	k1gInSc1	raketoplán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Enterprise	Enterprise	k1gFnSc1	Enterprise
(	(	kIx(	(
<g/>
OV-	OV-	k1gFnSc1	OV-
<g/>
101	[number]	k4	101
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc4	první
americký	americký	k2eAgInSc4d1	americký
raketoplán	raketoplán	k1gInSc4	raketoplán
postavený	postavený	k2eAgInSc4d1	postavený
pro	pro	k7c4	pro
NASA	NASA	kA	NASA
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
jmenovat	jmenovat	k5eAaImF	jmenovat
Constitution	Constitution	k1gInSc4	Constitution
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trekkies	trekkies	k1gMnSc1	trekkies
svou	svůj	k3xOyFgFnSc7	svůj
masovou	masový	k2eAgFnSc7d1	masová
dopisovou	dopisový	k2eAgFnSc7d1	dopisová
kampaní	kampaň	k1gFnSc7	kampaň
přiměli	přimět	k5eAaPmAgMnP	přimět
tvůrce	tvůrce	k1gMnPc4	tvůrce
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
podle	podle	k7c2	podle
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
dokončení	dokončení	k1gNnSc6	dokončení
výroby	výroba	k1gFnSc2	výroba
raketoplánu	raketoplán	k1gInSc2	raketoplán
byli	být	k5eAaImAgMnP	být
přizváni	přizván	k2eAgMnPc1d1	přizván
také	také	k9	také
hlavní	hlavní	k2eAgMnPc1d1	hlavní
představitelé	představitel	k1gMnPc1	představitel
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
posádky	posádka	k1gFnSc2	posádka
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc1	Enterprise
NCC-1701	NCC-1701	k1gFnSc1	NCC-1701
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tvůrcem	tvůrce	k1gMnSc7	tvůrce
Gene	gen	k1gInSc5	gen
Roddenberrym	Roddenberrymum	k1gNnPc2	Roddenberrymum
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
raketoplán	raketoplán	k1gInSc1	raketoplán
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
a	a	k8xC	a
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
-	-	kIx~	-
StarTAC	StarTAC	k1gMnPc2	StarTAC
-	-	kIx~	-
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
Motorola	Motorola	kA	Motorola
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
komunikátorem	komunikátor	k1gInSc7	komunikátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
seriálu	seriál	k1gInSc6	seriál
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
přístroj	přístroj	k1gInSc1	přístroj
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
navržen	navržen	k2eAgInSc4d1	navržen
<g/>
,	,	kIx,	,
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
vůbec	vůbec	k9	vůbec
neexistoval	existovat	k5eNaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
společnost	společnost	k1gFnSc1	společnost
Nokia	Nokia	kA	Nokia
si	se	k3xPyFc3	se
zase	zase	k9	zase
od	od	k7c2	od
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
vypůjčila	vypůjčit	k5eAaPmAgFnS	vypůjčit
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uvedla	uvést	k5eAaPmAgFnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
nový	nový	k2eAgInSc4d1	nový
model	model	k1gInSc4	model
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nokia	Nokia	kA	Nokia
5800	[number]	k4	5800
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
především	především	k9	především
o	o	k7c4	o
marketing	marketing	k1gInSc4	marketing
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
stejnými	stejný	k2eAgInPc7d1	stejný
parametry	parametr	k1gInPc7	parametr
jako	jako	k8xC	jako
Nokia	Nokia	kA	Nokia
5800	[number]	k4	5800
ExpressMusic	ExpressMusice	k1gInPc2	ExpressMusice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
spořičem	spořič	k1gMnSc7	spořič
obrazovky	obrazovka	k1gFnSc2	obrazovka
a	a	k8xC	a
aplikacemi	aplikace	k1gFnPc7	aplikace
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
5800	[number]	k4	5800
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
vydání	vydání	k1gNnSc2	vydání
nejnovějšího	nový	k2eAgInSc2d3	nejnovější
filmu	film	k1gInSc2	film
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
výrobcem	výrobce	k1gMnSc7	výrobce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
jméno	jméno	k1gNnSc4	jméno
seriálu	seriál	k1gInSc2	seriál
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
je	být	k5eAaImIp3nS	být
tchajwanská	tchajwanský	k2eAgFnSc1d1	tchajwanská
společnost	společnost	k1gFnSc1	společnost
High	High	k1gInSc1	High
Tech	Tech	k?	Tech
Computer	computer	k1gInSc1	computer
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
dala	dát	k5eAaPmAgFnS	dát
svému	svůj	k3xOyFgInSc3	svůj
rozklápěcímu	rozklápěcí	k2eAgInSc3d1	rozklápěcí
telefonu	telefon	k1gInSc3	telefon
název	název	k1gInSc4	název
Qtek	Qtek	k1gInSc1	Qtek
HTC	HTC	kA	HTC
Star	Star	kA	Star
Trek	Trek	k1gInSc4	Trek
nebo	nebo	k8xC	nebo
také	také	k9	také
STRTrk	STRTrk	k1gInSc1	STRTrk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Film	film	k1gInSc1	film
Galaxy	Galax	k1gInPc4	Galax
Quest	Quest	k1gFnSc4	Quest
představuje	představovat	k5eAaImIp3nS	představovat
parodii	parodie	k1gFnSc4	parodie
jak	jak	k8xS	jak
na	na	k7c4	na
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
samotný	samotný	k2eAgMnSc1d1	samotný
<g/>
,	,	kIx,	,
tak	tak	k9	tak
komunitu	komunita	k1gFnSc4	komunita
jeho	jeho	k3xOp3gMnPc2	jeho
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Finský	finský	k2eAgInSc1d1	finský
film	film	k1gInSc1	film
a	a	k8xC	a
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Wreck	Wreck	k1gInSc1	Wreck
paroduje	parodovat	k5eAaImIp3nS	parodovat
světy	svět	k1gInPc4	svět
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
a	a	k8xC	a
Babylonu	babylon	k1gInSc2	babylon
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
Raumschiff	Raumschiff	k1gMnSc1	Raumschiff
Surprise	Surprise	k1gFnSc2	Surprise
–	–	k?	–
Periode	Period	k1gInSc5	Period
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
jako	jako	k8xC	jako
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
,	,	kIx,	,
Pátý	pátý	k4xOgInSc1	pátý
element	element	k1gInSc1	element
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
Star	star	k1gFnSc4	star
Trek	Treka	k1gFnPc2	Treka
parodován	parodován	k2eAgMnSc1d1	parodován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
seriálu	seriál	k1gInSc6	seriál
Futurama	Futurama	k?	Futurama
je	být	k5eAaImIp3nS	být
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
parodován	parodován	k2eAgMnSc1d1	parodován
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
napodobením	napodobení	k1gNnSc7	napodobení
názvů	název	k1gInPc2	název
(	(	kIx(	(
<g/>
Kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
člověk	člověk	k1gMnSc1	člověk
nevydal	vydat	k5eNaPmAgMnS	vydat
-	-	kIx~	-
Kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
žádný	žádný	k3yNgMnSc1	žádný
fanoušek	fanoušek	k1gMnSc1	fanoušek
nevydal	vydat	k5eNaPmAgMnS	vydat
<g/>
;	;	kIx,	;
Vyhnání	vyhnání	k1gNnSc1	vyhnání
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
-	-	kIx~	-
Vyhnání	vyhnání	k1gNnPc4	vyhnání
z	z	k7c2	z
Frye	Fry	k1gFnSc2	Fry
<g/>
)	)	kIx)	)
Postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
geecích	geece	k1gFnPc6	geece
a	a	k8xC	a
nerdech	nerd	k1gInPc6	nerd
<g/>
,	,	kIx,	,
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
či	či	k8xC	či
citují	citovat	k5eAaBmIp3nP	citovat
Star	star	k1gFnSc4	star
Trek	Treka	k1gFnPc2	Treka
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
převlékají	převlékat	k5eAaImIp3nP	převlékat
do	do	k7c2	do
kostýmů	kostým	k1gInPc2	kostým
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
samotní	samotný	k2eAgMnPc1d1	samotný
herci	herec	k1gMnPc1	herec
ze	z	k7c2	z
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
<g/>
:	:	kIx,	:
v	v	k7c6	v
několika	několik	k4yIc6	několik
epizodách	epizoda	k1gFnPc6	epizoda
hraje	hrát	k5eAaImIp3nS	hrát
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
Wil	Wil	k1gFnSc7	Wil
Wheaton	Wheaton	k1gInSc1	Wheaton
<g/>
,	,	kIx,	,
v	v	k7c6	v
cameo	cameo	k6eAd1	cameo
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
George	George	k1gFnSc4	George
Takei	Take	k1gFnSc2	Take
<g/>
,	,	kIx,	,
LeVar	LeVar	k1gMnSc1	LeVar
Burton	Burton	k1gInSc1	Burton
a	a	k8xC	a
Brent	Brent	k?	Brent
Spiner	Spiner	k1gInSc1	Spiner
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
Sev	Sev	k1gMnSc1	Sev
Trek	Trek	k1gMnSc1	Trek
paroduje	parodovat	k5eAaImIp3nS	parodovat
klišé	klišé	k1gNnSc4	klišé
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
krátkými	krátký	k2eAgInPc7d1	krátký
stripy	strip	k1gInPc7	strip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
parodie	parodie	k1gFnSc1	parodie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
textové	textový	k2eAgFnSc2d1	textová
hry	hra	k1gFnSc2	hra
zvané	zvaný	k2eAgFnSc2d1	zvaná
Blektrek	Blektrek	k1gMnSc1	Blektrek
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Trekkies	Trekkies	k1gInSc1	Trekkies
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
části	část	k1gFnSc2	část
spadající	spadající	k2eAgFnSc2d1	spadající
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
posádce	posádka	k1gFnSc3	posádka
s	s	k7c7	s
kapitánem	kapitán	k1gMnSc7	kapitán
Kirkem	Kirek	k1gMnSc7	Kirek
<g/>
,	,	kIx,	,
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
mnoho	mnoho	k4c4	mnoho
televizních	televizní	k2eAgMnPc2d1	televizní
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
rázem	rázem	k6eAd1	rázem
stali	stát	k5eAaPmAgMnP	stát
fanoušci	fanoušek	k1gMnPc1	fanoušek
tohoto	tento	k3xDgNnSc2	tento
sci-fi	scii	k1gNnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celá	celý	k2eAgNnPc4d1	celé
společenství	společenství	k1gNnPc4	společenství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
trekkies	trekkies	k1gInSc1	trekkies
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trekkies	Trekkies	k1gMnSc1	Trekkies
velmi	velmi	k6eAd1	velmi
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
skutečný	skutečný	k2eAgInSc4d1	skutečný
svět	svět	k1gInSc4	svět
svými	svůj	k3xOyFgInPc7	svůj
hlasy	hlas	k1gInPc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dopisové	dopisový	k2eAgFnSc3d1	dopisová
kampani	kampaň	k1gFnSc3	kampaň
se	se	k3xPyFc4	se
trekkies	trekkies	k1gMnSc1	trekkies
podařilo	podařit	k5eAaPmAgNnS	podařit
původní	původní	k2eAgInSc4d1	původní
seriál	seriál	k1gInSc4	seriál
prodloužit	prodloužit	k5eAaPmF	prodloužit
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
2	[number]	k4	2
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
NBC	NBC	kA	NBC
chtěla	chtít	k5eAaImAgFnS	chtít
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
základna	základna	k1gFnSc1	základna
trekkies	trekkiesa	k1gFnPc2	trekkiesa
tak	tak	k9	tak
začala	začít	k5eAaPmAgFnS	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
i	i	k9	i
seriál	seriál	k1gInSc1	seriál
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
změna	změna	k1gFnSc1	změna
vůči	vůči	k7c3	vůči
zavedeným	zavedený	k2eAgNnPc3d1	zavedené
pravidlům	pravidlo	k1gNnPc3	pravidlo
universa	universum	k1gNnSc2	universum
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
strany	strana	k1gFnSc2	strana
ostře	ostro	k6eAd1	ostro
kritizována	kritizován	k2eAgFnSc1d1	kritizována
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
silnou	silný	k2eAgFnSc7d1	silná
zbraní	zbraň	k1gFnSc7	zbraň
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejedné	nejeden	k4xCyIgFnSc2	nejeden
společnosti	společnost	k1gFnSc2	společnost
daly	dát	k5eAaPmAgInP	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
přehlížet	přehlížet	k5eAaImF	přehlížet
<g/>
.	.	kIx.	.
</s>
<s>
Trekkies	Trekkies	k1gInSc1	Trekkies
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
fanoušci	fanoušek	k1gMnPc1	fanoušek
Star	star	k1gFnSc2	star
Wars	Wars	k1gInSc1	Wars
nebo	nebo	k8xC	nebo
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
brány	brána	k1gFnPc1	brána
pořádají	pořádat	k5eAaImIp3nP	pořádat
svá	svůj	k3xOyFgNnPc4	svůj
setkání	setkání	k1gNnPc4	setkání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zejména	zejména	k9	zejména
v	v	k7c6	v
USA	USA	kA	USA
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
průvody	průvod	k1gInPc4	průvod
lidí	člověk	k1gMnPc2	člověk
převlečených	převlečený	k2eAgFnPc2d1	převlečená
za	za	k7c4	za
Klingony	Klingon	k1gInPc4	Klingon
<g/>
,	,	kIx,	,
Borgy	Borga	k1gFnPc4	Borga
<g/>
,	,	kIx,	,
Vulkánce	Vulkánka	k1gFnSc6	Vulkánka
apod.	apod.	kA	apod.
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
Trekfest	Trekfest	k1gFnSc4	Trekfest
a	a	k8xC	a
CzechTREK	CzechTREK	k1gFnSc4	CzechTREK
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Star	star	k1gFnSc2	star
Trek	Trek	k1gInSc1	Trek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Memory	Memor	k1gInPc1	Memor
Alpha	Alpha	k1gFnSc1	Alpha
(	(	kIx(	(
<g/>
Wiki	Wiki	k1gNnSc1	Wiki
<g/>
)	)	kIx)	)
Memory	Memor	k1gInPc4	Memor
Alpha	Alpha	k1gFnSc1	Alpha
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
Memory	Memora	k1gFnSc2	Memora
Alpha	Alpha	k1gFnSc1	Alpha
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
Star	star	k1gInSc4	star
Treku	Trek	k1gInSc2	Trek
český	český	k2eAgInSc4d1	český
oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
nového	nový	k2eAgInSc2d1	nový
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
JJ	JJ	kA	JJ
Abramse	Abramse	k1gFnSc2	Abramse
CZ	CZ	kA	CZ
Kontinuum	kontinuum	k1gNnSc1	kontinuum
<g/>
,	,	kIx,	,
kontinuum	kontinuum	k1gNnSc1	kontinuum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
fan	fana	k1gFnPc2	fana
klub	klub	k1gInSc1	klub
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fanouškovský	fanouškovský	k2eAgInSc1d1	fanouškovský
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Hidden	Hiddna	k1gFnPc2	Hiddna
Frontier	Frontier	k1gMnSc1	Frontier
</s>
