<s>
První	první	k4xOgInSc1	první
seriál	seriál	k1gInSc1	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Hvězdné	hvězdný	k2eAgNnSc1d1	Hvězdné
putování	putování	k1gNnSc1	putování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřekládá	překládat	k5eNaImIp3nS	překládat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Original	Original	k1gMnSc1	Original
Series	Series	k1gMnSc1	Series
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
Původní	původní	k2eAgInSc1d1	původní
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
čítal	čítat	k5eAaImAgInS	čítat
3	[number]	k4	3
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
