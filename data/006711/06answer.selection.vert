<s>
Emitované	emitovaný	k2eAgInPc1d1	emitovaný
elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
fotoelektrony	fotoelektron	k1gInPc1	fotoelektron
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
uvolňování	uvolňování	k1gNnSc1	uvolňování
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
fotoelektrická	fotoelektrický	k2eAgFnSc1d1	fotoelektrická
emise	emise	k1gFnSc1	emise
(	(	kIx(	(
<g/>
fotoemise	fotoemise	k1gFnSc1	fotoemise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
