<s>
Gerontologie	gerontologie	k1gFnSc1	gerontologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
gerón	gerón	k1gMnSc1	gerón
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
p.	p.	k?	p.
gerontos	gerontos	k1gInSc1	gerontos
=	=	kIx~	=
starý	starý	k2eAgMnSc1d1	starý
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
logos	logos	k1gInSc1	logos
=	=	kIx~	=
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc1	souhrn
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c4	o
stárnutí	stárnutí	k1gNnSc4	stárnutí
a	a	k8xC	a
stáří	stáří	k1gNnSc4	stáří
<g/>
.	.	kIx.	.
</s>
