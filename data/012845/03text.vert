<p>
<s>
Gerontologie	gerontologie	k1gFnSc1	gerontologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
gerón	gerón	k1gMnSc1	gerón
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
p.	p.	k?	p.
gerontos	gerontos	k1gInSc1	gerontos
=	=	kIx~	=
starý	starý	k2eAgMnSc1d1	starý
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
logos	logos	k1gInSc1	logos
=	=	kIx~	=
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc1	souhrn
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c4	o
stárnutí	stárnutí	k1gNnSc4	stárnutí
a	a	k8xC	a
stáří	stáří	k1gNnSc4	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
gerontologie	gerontologie	k1gFnSc1	gerontologie
profiluje	profilovat	k5eAaImIp3nS	profilovat
také	také	k9	také
jako	jako	k9	jako
interdisciplinární	interdisciplinární	k2eAgInSc1d1	interdisciplinární
obor	obor	k1gInSc1	obor
v	v	k7c6	v
pregraduálním	pregraduální	k2eAgNnSc6d1	pregraduální
i	i	k8xC	i
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
gerontologie	gerontologie	k1gFnSc2	gerontologie
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Ilja	Ilja	k1gMnSc1	Ilja
Iljič	Iljič	k1gMnSc1	Iljič
Mečnikov	Mečnikov	k1gInSc1	Mečnikov
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Louise	Louis	k1gMnSc2	Louis
Pasteura	Pasteur	k1gMnSc2	Pasteur
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
průkopníkům	průkopník	k1gMnPc3	průkopník
oboru	obor	k1gInSc2	obor
patřili	patřit	k5eAaImAgMnP	patřit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
USA	USA	kA	USA
M.	M.	kA	M.
Rubner	Rubnra	k1gFnPc2	Rubnra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
USA	USA	kA	USA
R.	R.	kA	R.
Pearl	Pearla	k1gFnPc2	Pearla
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
A.	A.	kA	A.
A.	A.	kA	A.
Bogomolec	Bogomolec	k1gInSc4	Bogomolec
či	či	k8xC	či
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
V.	V.	kA	V.
Korenčevský	Korenčevský	k2eAgMnSc1d1	Korenčevský
a	a	k8xC	a
B.	B.	kA	B.
W.	W.	kA	W.
S.	S.	kA	S.
MacKenzie	MacKenzie	k1gFnSc1	MacKenzie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
gerontologie	gerontologie	k1gFnSc2	gerontologie
==	==	k?	==
</s>
</p>
<p>
<s>
Gerontologie	gerontologie	k1gFnSc1	gerontologie
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
3	[number]	k4	3
problémových	problémový	k2eAgInPc2d1	problémový
okruhů	okruh	k1gInPc2	okruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Gerontologie	gerontologie	k1gFnSc1	gerontologie
experimentální	experimentální	k2eAgFnSc1d1	experimentální
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
a	a	k8xC	a
jak	jak	k6eAd1	jak
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
stárnou	stárnout	k5eAaImIp3nP	stárnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
především	především	k9	především
na	na	k7c6	na
molekulární	molekulární	k2eAgFnSc6d1	molekulární
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Metodicky	metodicky	k6eAd1	metodicky
cenné	cenný	k2eAgFnPc1d1	cenná
jsou	být	k5eAaImIp3nP	být
institucionální	institucionální	k2eAgFnPc1d1	institucionální
studie	studie	k1gFnPc1	studie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gerontologie	gerontologie	k1gFnSc1	gerontologie
sociální	sociální	k2eAgFnSc1d1	sociální
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vztahem	vztah	k1gInSc7	vztah
starého	starý	k2eAgMnSc2d1	starý
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
včetně	včetně	k7c2	včetně
fenoménu	fenomén	k1gInSc2	fenomén
stárnutí	stárnutí	k1gNnSc1	stárnutí
populace	populace	k1gFnSc2	populace
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
aspekty	aspekt	k1gInPc4	aspekt
demografické	demografický	k2eAgInPc4d1	demografický
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
<g/>
,	,	kIx,	,
politické	politický	k2eAgInPc4d1	politický
<g/>
,	,	kIx,	,
sociologické	sociologický	k2eAgInPc4d1	sociologický
<g/>
,	,	kIx,	,
psychologické	psychologický	k2eAgInPc4d1	psychologický
<g/>
,	,	kIx,	,
etické	etický	k2eAgInPc4d1	etický
<g/>
,	,	kIx,	,
právní	právní	k2eAgInPc4d1	právní
<g/>
,	,	kIx,	,
urbanistické	urbanistický	k2eAgInPc4d1	urbanistický
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zakladatelskou	zakladatelský	k2eAgFnSc4d1	zakladatelská
monografii	monografie	k1gFnSc4	monografie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
kniha	kniha	k1gFnSc1	kniha
G.	G.	kA	G.
S.	S.	kA	S.
Halla	Hall	k1gMnSc2	Hall
Senescence	Senescenec	k1gMnSc2	Senescenec
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Half	halfa	k1gFnPc2	halfa
of	of	k?	of
the	the	k?	the
Life	Life	k1gInSc1	Life
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Appleton	Appleton	k1gInSc1	Appleton
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
518	[number]	k4	518
p.	p.	k?	p.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gerontologie	gerontologie	k1gFnSc1	gerontologie
klinická	klinický	k2eAgFnSc1d1	klinická
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
geriatrie	geriatrie	k1gFnSc1	geriatrie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zdravotním	zdravotní	k2eAgMnSc7d1	zdravotní
a	a	k8xC	a
funkčním	funkční	k2eAgInSc7d1	funkční
stavem	stav	k1gInSc7	stav
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
<g/>
,	,	kIx,	,
zdravím	zdravit	k5eAaImIp1nS	zdravit
podmíněnou	podmíněný	k2eAgFnSc7d1	podmíněná
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
starých	starý	k2eAgMnPc2d1	starý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
diagnostikování	diagnostikování	k1gNnSc2	diagnostikování
a	a	k8xC	a
léčení	léčení	k1gNnSc2	léčení
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
viz	vidět	k5eAaImRp2nS	vidět
geriatrie	geriatrie	k1gFnPc5	geriatrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
S.	S.	kA	S.
<g/>
N.	N.	kA	N.
Austad	Austad	k1gInSc4	Austad
<g/>
:	:	kIx,	:
Proč	proč	k6eAd1	proč
stárneme	stárnout	k5eAaImIp1nP	stárnout
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Why	Why	k1gMnSc2	Why
we	we	k?	we
age	age	k?	age
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Wiley	Wilea	k1gFnSc2	Wilea
and	and	k?	and
Sons	Sons	k1gInSc1	Sons
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kalvach	Kalvach	k1gInSc1	Kalvach
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Zadák	zadák	k1gInSc1	zadák
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Jirák	Jirák	k1gMnSc1	Jirák
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Zavázalová	Zavázalová	k1gFnSc1	Zavázalová
H.	H.	kA	H.
<g/>
,	,	kIx,	,
Sucharda	Sucharda	k1gMnSc1	Sucharda
P.	P.	kA	P.
(	(	kIx(	(
<g/>
editoři	editor	k1gMnPc1	editor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Geriatrie	Geriatrie	k1gFnSc1	Geriatrie
a	a	k8xC	a
gerontologie	gerontologie	k1gFnSc1	gerontologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
864	[number]	k4	864
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Haškovcová	Haškovcový	k2eAgFnSc1d1	Haškovcová
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Fenomén	fenomén	k1gInSc1	fenomén
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
407	[number]	k4	407
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Zych	Zych	k1gMnSc1	Zych
Adam	Adam	k1gMnSc1	Adam
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Leksykon	Leksykon	k1gInSc1	Leksykon
gerontologii	gerontologie	k1gFnSc4	gerontologie
<g/>
,	,	kIx,	,
Oficyna	Oficyen	k2eAgFnSc1d1	Oficyna
Wydawnicza	Wydawnicza	k1gFnSc1	Wydawnicza
"	"	kIx"	"
<g/>
Impuls	impuls	k1gInSc1	impuls
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Kraków	Kraków	k1gFnSc1	Kraków
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
280	[number]	k4	280
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gerontologie	gerontologie	k1gFnSc2	gerontologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gerontologie	gerontologie	k1gFnSc2	gerontologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
