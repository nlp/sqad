<s>
Mlži	mlž	k1gMnPc1	mlž
(	(	kIx(	(
<g/>
Bivalvia	Bivalvia	k1gFnSc1	Bivalvia
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
bezobratlí	bezobratlý	k2eAgMnPc1d1	bezobratlý
vodní	vodní	k2eAgMnPc1d1	vodní
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
řazení	řazený	k2eAgMnPc1d1	řazený
do	do	k7c2	do
kmene	kmen	k1gInSc2	kmen
měkkýšů	měkkýš	k1gMnPc2	měkkýš
(	(	kIx(	(
<g/>
Mollusca	Mollusca	k1gMnSc1	Mollusca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
kolem	kolem	k7c2	kolem
30	[number]	k4	30
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
mlžů	mlž	k1gMnPc2	mlž
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
tak	tak	k9	tak
obecně	obecně	k6eAd1	obecně
známí	známý	k2eAgMnPc1d1	známý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
mořských	mořský	k2eAgNnPc2d1	mořské
a	a	k8xC	a
sladkovodních	sladkovodní	k2eAgNnPc2d1	sladkovodní
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ústřice	ústřice	k1gFnPc4	ústřice
<g/>
,	,	kIx,	,
slávky	slávka	k1gFnPc4	slávka
<g/>
,	,	kIx,	,
srdcovky	srdcovka	k1gFnPc4	srdcovka
<g/>
,	,	kIx,	,
škeble	škeble	k1gFnPc4	škeble
a	a	k8xC	a
perlorodky	perlorodka	k1gFnPc4	perlorodka
<g/>
.	.	kIx.	.
</s>
<s>
Mlži	mlž	k1gMnPc1	mlž
jsou	být	k5eAaImIp3nP	být
měkkýši	měkkýš	k1gMnPc1	měkkýš
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
bilaterální	bilaterální	k2eAgFnSc7d1	bilaterální
symetrií	symetrie	k1gFnSc7	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
obvykle	obvykle	k6eAd1	obvykle
silně	silně	k6eAd1	silně
laterálně	laterálně	k6eAd1	laterálně
zploštělí	zploštělý	k2eAgMnPc1d1	zploštělý
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
tvoří	tvořit	k5eAaImIp3nS	tvořit
obvykle	obvykle	k6eAd1	obvykle
dva	dva	k4xCgInPc4	dva
laloky	lalok	k1gInPc4	lalok
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
jednu	jeden	k4xCgFnSc4	jeden
lasturu	lastura	k1gFnSc4	lastura
<g/>
.	.	kIx.	.
</s>
<s>
Lastury	lastura	k1gFnPc1	lastura
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
vazy	vaz	k1gInPc7	vaz
a	a	k8xC	a
silnými	silný	k2eAgInPc7d1	silný
svaly	sval	k1gInPc7	sval
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
je	být	k5eAaImIp3nS	být
vnitřek	vnitřek	k1gInSc1	vnitřek
lastury	lastura	k1gFnSc2	lastura
spojen	spojit	k5eAaPmNgInS	spojit
pouze	pouze	k6eAd1	pouze
třemi	tři	k4xCgInPc7	tři
otvory	otvor	k1gInPc7	otvor
<g/>
,	,	kIx,	,
přijímacím	přijímací	k2eAgNnSc6d1	přijímací
<g/>
,	,	kIx,	,
vyvrhovacím	vyvrhovací	k2eAgNnSc6d1	vyvrhovací
a	a	k8xC	a
otvorem	otvor	k1gInSc7	otvor
pro	pro	k7c4	pro
vysouvání	vysouvání	k1gNnSc4	vysouvání
svalnaté	svalnatý	k2eAgFnSc2d1	svalnatá
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
redukována	redukován	k2eAgFnSc1d1	redukována
–	–	k?	–
nenese	nést	k5eNaImIp3nS	nést
ani	ani	k8xC	ani
oči	oko	k1gNnPc4	oko
či	či	k8xC	či
radulu	radula	k1gFnSc4	radula
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
oči	oko	k1gNnPc1	oko
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
těla	tělo	k1gNnSc2	tělo
vyvinout	vyvinout	k5eAaPmF	vyvinout
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
statocystu	statocysta	k1gFnSc4	statocysta
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
schránky	schránka	k1gFnSc2	schránka
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
plášťová	plášťový	k2eAgFnSc1d1	plášťová
dutina	dutina	k1gFnSc1	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Dýchají	dýchat	k5eAaImIp3nP	dýchat
pomocí	pomocí	k7c2	pomocí
žaber	žábry	k1gFnPc2	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
tělní	tělní	k2eAgFnSc1d1	tělní
tekutina	tekutina	k1gFnSc1	tekutina
(	(	kIx(	(
<g/>
krev	krev	k1gFnSc1	krev
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
hemolymfa	hemolymf	k1gMnSc4	hemolymf
<g/>
"	"	kIx"	"
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
barvivo	barvivo	k1gNnSc1	barvivo
hemocyanin	hemocyanina	k1gFnPc2	hemocyanina
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
molekuly	molekula	k1gFnPc1	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
měď	měď	k1gFnSc4	měď
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
gangliová	gangliový	k2eAgFnSc1d1	gangliová
<g/>
.	.	kIx.	.
</s>
<s>
Mlži	mlž	k1gMnPc1	mlž
mají	mít	k5eAaImIp3nP	mít
čichové	čichový	k2eAgNnSc4d1	čichové
ústrojí	ústrojí	k1gNnSc4	ústrojí
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
orientují	orientovat	k5eAaBmIp3nP	orientovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
hmatové	hmatový	k2eAgFnPc1d1	hmatová
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Vylučovací	vylučovací	k2eAgFnSc4d1	vylučovací
soustavu	soustava	k1gFnSc4	soustava
představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
nefridií	nefridie	k1gFnPc2	nefridie
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
nohy	noha	k1gFnSc2	noha
se	se	k3xPyFc4	se
mlži	mlž	k1gMnPc1	mlž
dokážou	dokázat	k5eAaPmIp3nP	dokázat
zahrabat	zahrabat	k5eAaPmF	zahrabat
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přichytí	přichytit	k5eAaPmIp3nS	přichytit
k	k	k7c3	k
podkladu	podklad	k1gInSc3	podklad
pomocí	pomoc	k1gFnPc2	pomoc
byssových	byssový	k2eAgFnPc2d1	byssová
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mlži	mlž	k1gMnPc1	mlž
dokážou	dokázat	k5eAaPmIp3nP	dokázat
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Mlži	mlž	k1gMnPc1	mlž
jsou	být	k5eAaImIp3nP	být
filtrátoři	filtrátor	k1gMnPc1	filtrátor
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
částečkami	částečka	k1gFnPc7	částečka
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zachytí	zachytit	k5eAaPmIp3nP	zachytit
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
žábrách	žábry	k1gFnPc6	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Mlži	mlž	k1gMnPc1	mlž
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
gonochoristé	gonochorista	k1gMnPc1	gonochorista
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
nepřímý	přímý	k2eNgInSc4d1	nepřímý
vývin	vývin	k1gInSc4	vývin
<g/>
,	,	kIx,	,
oplození	oplození	k1gNnSc1	oplození
je	být	k5eAaImIp3nS	být
vnější	vnější	k2eAgNnSc1d1	vnější
<g/>
.	.	kIx.	.
</s>
<s>
Mlži	mlž	k1gMnPc1	mlž
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
larev	larva	k1gFnPc2	larva
<g/>
.	.	kIx.	.
</s>
<s>
Veliger	Veliger	k1gInSc1	Veliger
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
a	a	k8xC	a
glochidie	glochidie	k1gFnSc1	glochidie
je	být	k5eAaImIp3nS	být
sladkovodní	sladkovodní	k2eAgFnSc1d1	sladkovodní
a	a	k8xC	a
cizopasí	cizopasit	k5eAaImIp3nS	cizopasit
na	na	k7c6	na
rybách	ryba	k1gFnPc6	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
mlžů	mlž	k1gMnPc2	mlž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
málokteří	málokterý	k3yIgMnPc1	málokterý
dva	dva	k4xCgMnPc4	dva
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
shodnou	shodnout	k5eAaPmIp3nP	shodnout
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
r.	r.	kA	r.
2015	[number]	k4	2015
analyzující	analyzující	k2eAgInSc4d1	analyzující
transkriptom	transkriptom	k1gInSc4	transkriptom
mlžů	mlž	k1gMnPc2	mlž
nabízí	nabízet	k5eAaImIp3nS	nabízet
následující	následující	k2eAgInSc1d1	následující
fylogenetický	fylogenetický	k2eAgInSc1d1	fylogenetický
strom	strom	k1gInSc1	strom
mlžů	mlž	k1gMnPc2	mlž
<g/>
:	:	kIx,	:
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
jen	jen	k9	jen
příklady	příklad	k1gInPc1	příklad
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
:	:	kIx,	:
Sladkovodní	sladkovodní	k2eAgFnSc1d1	sladkovodní
<g/>
:	:	kIx,	:
Perlorodka	perlorodka	k1gFnSc1	perlorodka
říční	říční	k2eAgFnSc1d1	říční
(	(	kIx(	(
<g/>
Palaeoheterodonta	Palaeoheterodonta	k1gFnSc1	Palaeoheterodonta
<g/>
)	)	kIx)	)
Velevrub	velevrub	k1gMnSc1	velevrub
malířský	malířský	k2eAgMnSc1d1	malířský
(	(	kIx(	(
<g/>
Palaeoheterodonta	Palaeoheterodonta	k1gMnSc1	Palaeoheterodonta
<g/>
)	)	kIx)	)
Škeble	škeble	k1gFnSc1	škeble
rybničná	rybničný	k2eAgFnSc1d1	rybničná
(	(	kIx(	(
<g/>
Palaeoheterodonta	Palaeoheterodonta	k1gFnSc1	Palaeoheterodonta
<g/>
)	)	kIx)	)
Slávička	Slávička	k1gFnSc1	Slávička
mnohotvárná	mnohotvárný	k2eAgFnSc1d1	mnohotvárná
Mořští	mořský	k2eAgMnPc1d1	mořský
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Srdcovka	srdcovka	k1gFnSc1	srdcovka
jedlá	jedlý	k2eAgFnSc1d1	jedlá
(	(	kIx(	(
<g/>
Imparidentia	Imparidentia	k1gFnSc1	Imparidentia
<g/>
)	)	kIx)	)
Střenka	střenka	k1gFnSc1	střenka
jedlá	jedlý	k2eAgFnSc1d1	jedlá
(	(	kIx(	(
<g/>
Imparidentia	Imparidentia	k1gFnSc1	Imparidentia
<g/>
)	)	kIx)	)
Šášeň	Šášeň	k?	Šášeň
lodní	lodní	k2eAgNnSc1d1	lodní
(	(	kIx(	(
<g/>
Imparidentia	Imparidentia	k1gFnSc1	Imparidentia
<g/>
)	)	kIx)	)
Ústřice	ústřice	k1gFnSc1	ústřice
jedlá	jedlý	k2eAgFnSc1d1	jedlá
(	(	kIx(	(
<g/>
Pteriomorpha	Pteriomorpha	k1gFnSc1	Pteriomorpha
<g/>
)	)	kIx)	)
Slávka	Slávka	k1gFnSc1	Slávka
jedlá	jedlý	k2eAgFnSc1d1	jedlá
(	(	kIx(	(
<g/>
Pteriomorpha	Pteriomorpha	k1gFnSc1	Pteriomorpha
<g/>
)	)	kIx)	)
Perlotvorka	Perlotvorka	k1gFnSc1	Perlotvorka
mořská	mořský	k2eAgFnSc1d1	mořská
(	(	kIx(	(
<g/>
Pteriomorpha	Pteriomorpha	k1gFnSc1	Pteriomorpha
<g/>
)	)	kIx)	)
Hřebenatka	hřebenatka	k1gFnSc1	hřebenatka
jakubská	jakubský	k2eAgFnSc1d1	Jakubská
(	(	kIx(	(
<g/>
Pteriomorpha	Pteriomorpha	k1gFnSc1	Pteriomorpha
<g/>
)	)	kIx)	)
Zéva	zéva	k1gFnSc1	zéva
obrovská	obrovský	k2eAgFnSc1d1	obrovská
Hřebenatka	hřebenatka	k1gFnSc1	hřebenatka
islandská	islandský	k2eAgFnSc1d1	islandská
</s>
