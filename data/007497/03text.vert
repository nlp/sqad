<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1645	[number]	k4	1645
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
událost	událost	k1gFnSc1	událost
poslední	poslední	k2eAgFnSc2d1	poslední
fáze	fáze	k1gFnSc2	fáze
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
500	[number]	k4	500
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
asi	asi	k9	asi
tisícovkou	tisícovka	k1gFnSc7	tisícovka
příslušníků	příslušník	k1gMnPc2	příslušník
městské	městský	k2eAgFnSc2d1	městská
milice	milice	k1gFnSc2	milice
<g/>
,	,	kIx,	,
ubránila	ubránit	k5eAaPmAgFnS	ubránit
město	město	k1gNnSc4	město
proti	proti	k7c3	proti
asi	asi	k9	asi
28	[number]	k4	28
tisícům	tisíc	k4xCgInPc3	tisíc
vojáků	voják	k1gMnPc2	voják
generála	generál	k1gMnSc2	generál
Lennarta	Lennart	k1gMnSc2	Lennart
Torstensona	Torstenson	k1gMnSc2	Torstenson
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
obránců	obránce	k1gMnPc2	obránce
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
skvělou	skvělý	k2eAgFnSc4d1	skvělá
kariéru	kariéra	k1gFnSc4	kariéra
jejich	jejich	k3xOp3gMnSc2	jejich
velitele	velitel	k1gMnSc2	velitel
Louise	Louis	k1gMnSc2	Louis
Raduita	Raduit	k1gMnSc2	Raduit
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
zaměstnal	zaměstnat	k5eAaPmAgInS	zaměstnat
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
čtvrt	čtvrt	k1gFnSc4	čtvrt
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
zbytečném	zbytečný	k2eAgNnSc6d1	zbytečné
a	a	k8xC	a
nákladném	nákladný	k2eAgNnSc6d1	nákladné
úsilí	úsilí	k1gNnSc6	úsilí
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
švédskou	švédský	k2eAgFnSc4d1	švédská
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
též	též	k9	též
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
nakonec	nakonec	k6eAd1	nakonec
definitivně	definitivně	k6eAd1	definitivně
stalo	stát	k5eAaPmAgNnS	stát
metropolí	metropol	k1gFnSc7	metropol
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
dobyté	dobytý	k2eAgFnSc2d1	dobytá
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
učinil	učinit	k5eAaPmAgMnS	učinit
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgMnS	zřídit
zde	zde	k6eAd1	zde
své	svůj	k3xOyFgNnSc4	svůj
místodržitelství	místodržitelství	k1gNnSc4	místodržitelství
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Královský	královský	k2eAgInSc1d1	královský
tribunál	tribunál	k1gInSc1	tribunál
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
sídlem	sídlo	k1gNnSc7	sídlo
všech	všecek	k3xTgInPc2	všecek
hlavních	hlavní	k2eAgInPc2d1	hlavní
královských	královský	k2eAgInPc2d1	královský
i	i	k8xC	i
zemských	zemský	k2eAgInPc2d1	zemský
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
švédského	švédský	k2eAgInSc2d1	švédský
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
předních	přední	k2eAgFnPc2d1	přední
osobností	osobnost	k1gFnPc2	osobnost
města	město	k1gNnSc2	město
neměla	mít	k5eNaImAgNnP	mít
dost	dost	k6eAd1	dost
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vojenským	vojenský	k2eAgMnSc7d1	vojenský
velitelem	velitel	k1gMnSc7	velitel
města	město	k1gNnSc2	město
byl	být	k5eAaImAgMnS	být
císařem	císař	k1gMnSc7	císař
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
sedmatřicetiletý	sedmatřicetiletý	k2eAgMnSc1d1	sedmatřicetiletý
plukovník	plukovník	k1gMnSc1	plukovník
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
Jean	Jean	k1gMnSc1	Jean
Louis	Louis	k1gMnSc1	Louis
Raduit	Raduit	k1gMnSc1	Raduit
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
důvěrou	důvěra	k1gFnSc7	důvěra
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
protestantem	protestant	k1gMnSc7	protestant
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
bojoval	bojovat	k5eAaImAgMnS	bojovat
ve	v	k7c6	v
švédském	švédský	k2eAgNnSc6d1	švédské
vojsku	vojsko	k1gNnSc6	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
události	událost	k1gFnPc1	událost
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
volba	volba	k1gFnSc1	volba
byla	být	k5eAaImAgFnS	být
správná	správný	k2eAgFnSc1d1	správná
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
města	město	k1gNnSc2	město
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
hejtmanem	hejtman	k1gMnSc7	hejtman
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
<g/>
,	,	kIx,	,
zpevnění	zpevnění	k1gNnSc4	zpevnění
a	a	k8xC	a
vybudování	vybudování	k1gNnSc4	vybudování
nového	nový	k2eAgNnSc2d1	nové
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
stály	stát	k5eAaImAgFnP	stát
600	[number]	k4	600
kroků	krok	k1gInPc2	krok
od	od	k7c2	od
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zbourány	zbourat	k5eAaPmNgInP	zbourat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
prohloubeny	prohlouben	k2eAgInPc1d1	prohlouben
staré	starý	k2eAgInPc1d1	starý
příkopy	příkop	k1gInPc1	příkop
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
nové	nový	k2eAgFnPc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
zásobování	zásobování	k1gNnSc2	zásobování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byl	být	k5eAaImAgInS	být
zbudován	zbudován	k2eAgInSc1d1	zbudován
mlýn	mlýn	k1gInSc1	mlýn
na	na	k7c4	na
volský	volský	k2eAgInSc4d1	volský
potah	potah	k1gInSc4	potah
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgFnP	být
vyčištěny	vyčistit	k5eAaPmNgFnP	vyčistit
všechny	všechen	k3xTgFnPc1	všechen
studny	studna	k1gFnPc1	studna
<g/>
.	.	kIx.	.
</s>
<s>
Krytou	krytý	k2eAgFnSc7d1	krytá
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
strada	strada	k1gFnSc1	strada
coperta	coperta	k1gFnSc1	coperta
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
spojil	spojit	k5eAaPmAgMnS	spojit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
město	město	k1gNnSc4	město
se	s	k7c7	s
Špilberkem	Špilberek	k1gInSc7	Špilberek
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
rozhodností	rozhodnost	k1gFnSc7	rozhodnost
a	a	k8xC	a
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
obrany	obrana	k1gFnSc2	obrana
města	město	k1gNnSc2	město
si	se	k3xPyFc3	se
velitel	velitel	k1gMnSc1	velitel
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
brzy	brzy	k6eAd1	brzy
získal	získat	k5eAaPmAgMnS	získat
vážnost	vážnost	k1gFnSc4	vážnost
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
nedostatek	nedostatek	k1gInSc1	nedostatek
zbraní	zbraň	k1gFnPc2	zbraň
odstraňoval	odstraňovat	k5eAaImAgInS	odstraňovat
usilovnou	usilovný	k2eAgFnSc7d1	usilovná
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obránců	obránce	k1gMnPc2	obránce
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
také	také	k9	také
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
475	[number]	k4	475
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
426	[number]	k4	426
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
přesto	přesto	k8xC	přesto
odhodláni	odhodlán	k2eAgMnPc1d1	odhodlán
čelit	čelit	k5eAaImF	čelit
osmnáctinásobné	osmnáctinásobný	k2eAgFnSc3d1	osmnáctinásobný
převaze	převaha	k1gFnSc3	převaha
obléhajících	obléhající	k2eAgMnPc2d1	obléhající
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Brna	Brno	k1gNnSc2	Brno
trvalo	trvat	k5eAaImAgNnS	trvat
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1645	[number]	k4	1645
až	až	k9	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Švédové	Švéd	k1gMnPc1	Švéd
odtáhli	odtáhnout	k5eAaPmAgMnP	odtáhnout
–	–	k?	–
obrovské	obrovský	k2eAgNnSc4d1	obrovské
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
,	,	kIx,	,
odvaha	odvaha	k1gFnSc1	odvaha
a	a	k8xC	a
hrdinství	hrdinství	k1gNnSc1	hrdinství
obránců	obránce	k1gMnPc2	obránce
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
korunováno	korunovat	k5eAaBmNgNnS	korunovat
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Zásobování	zásobování	k1gNnSc1	zásobování
potravinami	potravina	k1gFnPc7	potravina
-	-	kIx~	-
zákaz	zákaz	k1gInSc1	zákaz
prodeje	prodej	k1gInSc2	prodej
dobytka	dobytek	k1gInSc2	dobytek
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
magistrátu	magistrát	k1gInSc2	magistrát
<g/>
,	,	kIx,	,
píce	píce	k1gFnSc1	píce
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
koně	kůň	k1gMnSc2	kůň
byla	být	k5eAaImAgFnS	být
vydávána	vydávat	k5eAaPmNgFnS	vydávat
na	na	k7c4	na
denní	denní	k2eAgInPc4d1	denní
příděly	příděl	k1gInPc4	příděl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
projevovat	projevovat	k5eAaImF	projevovat
nedostatek	nedostatek	k1gInSc1	nedostatek
ledku	ledek	k1gInSc2	ledek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
také	také	k9	také
zásobování	zásobování	k1gNnSc1	zásobování
vázlo	váznout	k5eAaImAgNnS	váznout
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc4	problém
ale	ale	k8xC	ale
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
Švédové	Švédové	k2eAgInSc4d1	Švédové
–	–	k?	–
nedostatek	nedostatek	k1gInSc4	nedostatek
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgInPc4d1	vojenský
neúspěchy	neúspěch	k1gInPc4	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
pokus	pokus	k1gInSc1	pokus
Torstensona	Torstenson	k1gMnSc2	Torstenson
o	o	k7c6	o
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Brňané	Brňan	k1gMnPc1	Brňan
nechtěli	chtít	k5eNaImAgMnP	chtít
nic	nic	k3yNnSc1	nic
slyšet	slyšet	k5eAaImF	slyšet
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
výsměch	výsměch	k1gInSc4	výsměch
Švédům	Švéd	k1gMnPc3	Švéd
na	na	k7c6	na
hradbách	hradba	k1gFnPc6	hradba
vyhrávala	vyhrávat	k5eAaImAgFnS	vyhrávat
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
vlály	vlát	k5eAaImAgInP	vlát
prapory	prapor	k1gInPc1	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
brněnští	brněnský	k2eAgMnPc1d1	brněnský
podnikli	podniknout	k5eAaPmAgMnP	podniknout
opět	opět	k6eAd1	opět
výpad	výpad	k1gInSc4	výpad
do	do	k7c2	do
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
tábora	tábor	k1gInSc2	tábor
a	a	k8xC	a
Švédy	švéda	k1gFnSc2	švéda
překvapili	překvapit	k5eAaPmAgMnP	překvapit
<g/>
.	.	kIx.	.
</s>
<s>
Obráncům	obránce	k1gMnPc3	obránce
města	město	k1gNnSc2	město
vydatně	vydatně	k6eAd1	vydatně
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
měšťana	měšťan	k1gMnSc2	měšťan
Jana	Jan	k1gMnSc2	Jan
Staffa	Staff	k1gMnSc2	Staff
také	také	k9	také
setnina	setnina	k1gFnSc1	setnina
studentů	student	k1gMnPc2	student
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
vycvičit	vycvičit	k5eAaPmF	vycvičit
rektor	rektor	k1gMnSc1	rektor
koleje	kolej	k1gFnSc2	kolej
Martin	Martin	k1gMnSc1	Martin
Středa	středa	k1gFnSc1	středa
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
ač	ač	k8xS	ač
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
při	při	k7c6	při
pomoci	pomoc	k1gFnSc6	pomoc
obraně	obrana	k1gFnSc6	obrana
Brna	Brno	k1gNnSc2	Brno
byli	být	k5eAaImAgMnP	být
jednotní	jednotný	k2eAgMnPc1d1	jednotný
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
vojsko	vojsko	k1gNnSc1	vojsko
sedmihradského	sedmihradský	k2eAgNnSc2d1	sedmihradské
knížete	kníže	k1gNnSc2wR	kníže
Jiřího	Jiří	k1gMnSc2	Jiří
Rákocziho	Rákoczi	k1gMnSc2	Rákoczi
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
se	s	k7c7	s
Švédy	Švéd	k1gMnPc7	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1645	[number]	k4	1645
vyslal	vyslat	k5eAaPmAgMnS	vyslat
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
posly	posel	k1gMnPc4	posel
k	k	k7c3	k
arcivévodovi	arcivévoda	k1gMnSc3	arcivévoda
Leopoldovi	Leopold	k1gMnSc3	Leopold
Vilémovi	Vilém	k1gMnSc3	Vilém
s	s	k7c7	s
podrobnou	podrobný	k2eAgFnSc7d1	podrobná
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
celém	celý	k2eAgNnSc6d1	celé
obléhání	obléhání	k1gNnSc6	obléhání
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
přímluvu	přímluva	k1gFnSc4	přímluva
a	a	k8xC	a
doporučení	doporučení	k1gNnSc4	doporučení
císařské	císařský	k2eAgFnSc2d1	císařská
milosti	milost	k1gFnSc2	milost
při	při	k7c6	při
náhradě	náhrada	k1gFnSc6	náhrada
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgInS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
Linci	Linec	k1gInSc6	Linec
resoluci	resoluce	k1gFnSc4	resoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
řadu	řada	k1gFnSc4	řada
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
ohodnoceny	ohodnocen	k2eAgFnPc1d1	ohodnocena
zásluhy	zásluha	k1gFnPc1	zásluha
brněnských	brněnský	k2eAgMnPc2d1	brněnský
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
materiálovou	materiálový	k2eAgFnSc7d1	materiálová
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
měšťané	měšťan	k1gMnPc1	měšťan
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
osvobozeni	osvobodit	k5eAaPmNgMnP	osvobodit
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
přirážek	přirážka	k1gFnPc2	přirážka
<g/>
.	.	kIx.	.
</s>
<s>
Obci	obec	k1gFnSc3	obec
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
finanční	finanční	k2eAgFnSc1d1	finanční
výpomoc	výpomoc	k1gFnSc1	výpomoc
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
třicet	třicet	k4xCc4	třicet
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
[	[	kIx(	[
<g/>
určitě	určitě	k6eAd1	určitě
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
formou	forma	k1gFnSc7	forma
ročního	roční	k2eAgInSc2d1	roční
odpočtu	odpočet	k1gInSc2	odpočet
tří	tři	k4xCgInPc2	tři
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatý	k1gInPc2	zlatý
z	z	k7c2	z
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
Brnu	Brno	k1gNnSc3	Brno
byl	být	k5eAaImAgInS	být
také	také	k9	také
později	pozdě	k6eAd2	pozdě
vylepšen	vylepšen	k2eAgInSc1d1	vylepšen
městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1647	[number]	k4	1647
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
císař	císař	k1gMnSc1	císař
měšťany	měšťan	k1gMnPc7	měšťan
od	od	k7c2	od
placení	placení	k1gNnSc2	placení
cla	clo	k1gNnSc2	clo
<g/>
,	,	kIx,	,
mýta	mýto	k1gNnSc2	mýto
a	a	k8xC	a
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
poplatků	poplatek	k1gInPc2	poplatek
ze	z	k7c2	z
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Brnu	Brno	k1gNnSc3	Brno
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
uděleno	udělen	k2eAgNnSc1d1	uděleno
právo	právo	k1gNnSc1	právo
konat	konat	k5eAaImF	konat
koňské	koňský	k2eAgInPc4d1	koňský
a	a	k8xC	a
dobytčí	dobytčí	k2eAgInPc4d1	dobytčí
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
obecných	obecný	k2eAgFnPc2d1	obecná
odměn	odměna	k1gFnPc2	odměna
byli	být	k5eAaImAgMnP	být
osobně	osobně	k6eAd1	osobně
vyznamenáni	vyznamenán	k2eAgMnPc1d1	vyznamenán
a	a	k8xC	a
odměněni	odměněn	k2eAgMnPc1d1	odměněn
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
brněnští	brněnský	k2eAgMnPc1d1	brněnský
měšťané	měšťan	k1gMnPc1	měšťan
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
studenti	student	k1gMnPc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Četní	četný	k2eAgMnPc1d1	četný
tovaryši	tovaryš	k1gMnPc1	tovaryš
<g/>
,	,	kIx,	,
měšťanští	měšťanský	k2eAgMnPc1d1	měšťanský
pomocníci	pomocník	k1gMnPc1	pomocník
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
brněnských	brněnský	k2eAgNnPc2d1	brněnské
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
měšťanská	měšťanský	k2eAgNnPc1d1	měšťanské
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
osvobozeni	osvobodit	k5eAaPmNgMnP	osvobodit
od	od	k7c2	od
povinných	povinný	k2eAgInPc2d1	povinný
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
byli	být	k5eAaImAgMnP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nejlépe	dobře	k6eAd3	dobře
oceněni	oceněn	k2eAgMnPc1d1	oceněn
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
města	město	k1gNnSc2	město
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
posádky	posádka	k1gFnSc2	posádka
na	na	k7c4	na
Špilberku	Špilberka	k1gFnSc4	Špilberka
Ogilvy	Ogilva	k1gFnSc2	Ogilva
<g/>
.	.	kIx.	.
</s>
<s>
Raduit	Raduit	k1gMnSc1	Raduit
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
hraběte	hrabě	k1gMnSc4	hrabě
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
darem	dar	k1gInSc7	dar
od	od	k7c2	od
města	město	k1gNnSc2	město
Schwanzův	Schwanzův	k2eAgInSc4d1	Schwanzův
palác	palác	k1gInSc4	palác
na	na	k7c6	na
Dolním	dolní	k2eAgInSc6d1	dolní
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
čekala	čekat	k5eAaImAgFnS	čekat
ho	on	k3xPp3gNnSc4	on
další	další	k2eAgFnSc1d1	další
skvělá	skvělý	k2eAgFnSc1d1	skvělá
vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obléháním	obléhání	k1gNnSc7	obléhání
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
nejznámější	známý	k2eAgFnSc1d3	nejznámější
brněnská	brněnský	k2eAgFnSc1d1	brněnská
pověst	pověst	k1gFnSc1	pověst
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
zvoní	zvonit	k5eAaImIp3nS	zvonit
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
poledne	poledne	k1gNnSc2	poledne
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Švédové	Švéd	k1gMnPc1	Švéd
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
obléhali	obléhat	k5eAaImAgMnP	obléhat
město	město	k1gNnSc4	město
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
útoky	útok	k1gInPc1	útok
ani	ani	k8xC	ani
snaha	snaha	k1gFnSc1	snaha
město	město	k1gNnSc1	město
vyhladovět	vyhladovět	k5eAaPmF	vyhladovět
nevedly	vést	k5eNaImAgInP	vést
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
podnikl	podniknout	k5eAaPmAgMnS	podniknout
velitel	velitel	k1gMnSc1	velitel
švédského	švédský	k2eAgNnSc2d1	švédské
vojska	vojsko	k1gNnSc2	vojsko
Torstenson	Torstenson	k1gMnSc1	Torstenson
generální	generální	k2eAgInSc4d1	generální
útok	útok	k1gInSc4	útok
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
město	město	k1gNnSc1	město
nepadne	padnout	k5eNaPmIp3nS	padnout
do	do	k7c2	do
poledne	poledne	k1gNnSc2	poledne
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
obléhání	obléhání	k1gNnSc1	obléhání
skončí	skončit	k5eAaPmIp3nS	skončit
a	a	k8xC	a
odtáhne	odtáhnout	k5eAaPmIp3nS	odtáhnout
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
donést	donést	k5eAaPmF	donést
Brňanům	Brňan	k1gMnPc3	Brňan
a	a	k8xC	a
když	když	k9	když
byli	být	k5eAaImAgMnP	být
útočníky	útočník	k1gMnPc7	útočník
zle	zle	k6eAd1	zle
tísněni	tísněn	k2eAgMnPc1d1	tísněn
a	a	k8xC	a
poledne	poledne	k1gNnSc4	poledne
ještě	ještě	k9	ještě
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
napadlo	napadnout	k5eAaPmAgNnS	napadnout
kohosi	kdosi	k3yInSc4	kdosi
začít	začít	k5eAaPmF	začít
zvonit	zvonit	k5eAaImF	zvonit
poledne	poledne	k1gNnSc4	poledne
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
dříve	dříve	k6eAd2	dříve
<g/>
;	;	kIx,	;
generál	generál	k1gMnSc1	generál
Torstenson	Torstenson	k1gMnSc1	Torstenson
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
dostát	dostát	k5eAaPmF	dostát
svému	svůj	k3xOyFgNnSc3	svůj
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
a	a	k8xC	a
obléhání	obléhání	k1gNnSc4	obléhání
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
bylo	být	k5eAaImAgNnS	být
zachráněno	zachráněn	k2eAgNnSc1d1	zachráněno
a	a	k8xC	a
na	na	k7c4	na
připomínku	připomínka	k1gFnSc4	připomínka
této	tento	k3xDgFnSc3	tento
události	událost	k1gFnSc3	událost
zvoní	zvonit	k5eAaImIp3nS	zvonit
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
zvony	zvon	k1gInPc7	zvon
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
poledne	poledne	k1gNnSc2	poledne
už	už	k9	už
v	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Brňany	Brňan	k1gMnPc4	Brňan
ještě	ještě	k6eAd1	ještě
příznivější	příznivý	k2eAgMnSc1d2	příznivější
<g/>
:	:	kIx,	:
poslední	poslední	k2eAgMnSc1d1	poslední
generální	generální	k2eAgMnSc1d1	generální
šturm	šturm	k?	šturm
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
trval	trvat	k5eAaImAgMnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
a	a	k8xC	a
kompletně	kompletně	k6eAd1	kompletně
odražen	odražen	k2eAgInSc1d1	odražen
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Torstenson	Torstenson	k1gInSc1	Torstenson
pro	pro	k7c4	pro
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
času	čas	k1gInSc2	čas
obléhání	obléhání	k1gNnSc2	obléhání
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgInPc2d1	poslední
osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
obležení	obležení	k1gNnPc2	obležení
se	se	k3xPyFc4	se
už	už	k6eAd1	už
švédské	švédský	k2eAgFnPc1d1	švédská
jednotky	jednotka	k1gFnPc1	jednotka
stahovaly	stahovat	k5eAaImAgFnP	stahovat
a	a	k8xC	a
odvážely	odvážet	k5eAaImAgFnP	odvážet
děla	dělo	k1gNnSc2	dělo
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
už	už	k9	už
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
útoku	útok	k1gInSc3	útok
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
opustili	opustit	k5eAaPmAgMnP	opustit
poslední	poslední	k2eAgMnPc1d1	poslední
švédští	švédský	k2eAgMnPc1d1	švédský
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Němčík	Němčík	k1gMnSc1	Němčík
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
:	:	kIx,	:
Švédové	Švédová	k1gFnSc2	Švédová
před	před	k7c7	před
Brnem	Brno	k1gNnSc7	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Bpress	Bpress	k1gInSc1	Bpress
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1995	[number]	k4	1995
Matějek	Matějka	k1gFnPc2	Matějka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
:	:	kIx,	:
Švédové	Švéd	k1gMnPc1	Švéd
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Acta	Acta	k6eAd1	Acta
Musei	museum	k1gNnPc7	museum
Moraviae	Moravia	k1gFnSc2	Moravia
<g/>
,	,	kIx,	,
Scientiae	Scientiae	k1gNnSc1	Scientiae
sociales	socialesa	k1gFnPc2	socialesa
<g/>
,	,	kIx,	,
73	[number]	k4	73
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
s.	s.	k?	s.
127	[number]	k4	127
<g/>
-	-	kIx~	-
<g/>
161	[number]	k4	161
<g/>
.	.	kIx.	.
</s>
<s>
Matějek	Matějka	k1gFnPc2	Matějka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
:	:	kIx,	:
Švédové	Švéd	k1gMnPc1	Švéd
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Acta	Acta	k6eAd1	Acta
Musei	museum	k1gNnPc7	museum
Moraviae	Moravia	k1gFnSc2	Moravia
<g/>
,	,	kIx,	,
Scientiae	Scientiae	k1gNnSc1	Scientiae
sociales	socialesa	k1gFnPc2	socialesa
<g/>
,	,	kIx,	,
75	[number]	k4	75
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
s.	s.	k?	s.
141	[number]	k4	141
<g/>
-	-	kIx~	-
<g/>
172	[number]	k4	172
<g/>
.	.	kIx.	.
</s>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
FUČÍK	Fučík	k1gMnSc1	Fučík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
HAVEL	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
císařským	císařský	k2eAgInSc7d1	císařský
praporem	prapor	k1gInSc7	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
habsburské	habsburský	k2eAgFnSc2d1	habsburská
armády	armáda	k1gFnSc2	armáda
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
555	[number]	k4	555
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902745	[number]	k4	902745
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
město	město	k1gNnSc4	město
Švédové	Švédová	k1gFnSc2	Švédová
na	na	k7c6	na
Kraví	kraví	k2eAgFnSc6d1	kraví
hoře	hora	k1gFnSc6	hora
Švédové	Švédová	k1gFnSc2	Švédová
před	před	k7c7	před
Brnem	Brno	k1gNnSc7	Brno
</s>
