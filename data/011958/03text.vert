<p>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
Lhotka	Lhotka	k1gFnSc1	Lhotka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
Hradčovic	Hradčovice	k1gFnPc2	Hradčovice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
rozhledny	rozhledna	k1gFnSc2	rozhledna
je	být	k5eAaImIp3nS	být
ocelová	ocelový	k2eAgFnSc1d1	ocelová
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
35	[number]	k4	35
m.	m.	k?	m.
Zastřešená	zastřešený	k2eAgFnSc1d1	zastřešená
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
plošina	plošina	k1gFnSc1	plošina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
18	[number]	k4	18
m	m	kA	m
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
96	[number]	k4	96
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhledny	rozhledna	k1gFnSc2	rozhledna
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
a	a	k8xC	a
Chřiby	Chřiby	k1gInPc1	Chřiby
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rozhledna	rozhledna	k1gFnSc1	rozhledna
Lhotka	Lhotka	k1gFnSc1	Lhotka
u	u	k7c2	u
Hradčovic	Hradčovice	k1gFnPc2	Hradčovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://rozhledny.wz.cz/index2roz.htm	[url]	k6eAd1	http://rozhledny.wz.cz/index2roz.htm
</s>
</p>
