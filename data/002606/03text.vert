<s>
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
dvojicí	dvojice	k1gFnSc7	dvojice
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
a	a	k8xC	a
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
známější	známý	k2eAgFnSc1d2	známější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
až	až	k6eAd1	až
jako	jako	k8xC	jako
čtveřice	čtveřice	k1gFnSc1	čtveřice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
Flürem	Flür	k1gMnSc7	Flür
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Bartosem	Bartos	k1gMnSc7	Bartos
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
složení	složení	k1gNnSc1	složení
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Hilpert	Hilpert	k1gMnSc1	Hilpert
<g/>
,	,	kIx,	,
Henning	Henning	k1gInSc1	Henning
Schmitz	Schmitz	k1gInSc1	Schmitz
a	a	k8xC	a
Falk	Falk	k1gInSc1	Falk
Grieffenhagen	Grieffenhagen	k1gInSc1	Grieffenhagen
<g/>
.	.	kIx.	.
</s>
<s>
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
düsseldorfské	düsseldorfský	k2eAgFnSc6d1	düsseldorfská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
potkávají	potkávat	k5eAaImIp3nP	potkávat
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
a	a	k8xC	a
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
coby	coby	k?	coby
studenti	student	k1gMnPc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
sám	sám	k3xTgInSc4	sám
Hütter	Hütter	k1gInSc4	Hütter
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
interview	interview	k1gNnPc2	interview
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
tak	tak	k9	tak
trochu	trochu	k6eAd1	trochu
za	za	k7c4	za
outsidery	outsider	k1gMnPc4	outsider
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
říkat	říkat	k5eAaImF	říkat
Mister	mister	k1gMnPc1	mister
Kling	Kling	k1gMnSc1	Kling
a	a	k8xC	a
Mister	mister	k1gMnSc1	mister
Klang	Klang	k1gMnSc1	Klang
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
projevovali	projevovat	k5eAaImAgMnP	projevovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
avantgardní	avantgardní	k2eAgNnSc4d1	avantgardní
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
třemi	tři	k4xCgInPc7	tři
hudebníky	hudebník	k1gMnPc7	hudebník
zakládají	zakládat	k5eAaImIp3nP	zakládat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
skupinu	skupina	k1gFnSc4	skupina
Organisation	Organisation	k1gInSc1	Organisation
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
marně	marně	k6eAd1	marně
hledají	hledat	k5eAaImIp3nP	hledat
hudebního	hudební	k2eAgMnSc4d1	hudební
vydavatele	vydavatel	k1gMnSc4	vydavatel
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
podaří	podařit	k5eAaPmIp3nS	podařit
sepsat	sepsat	k5eAaPmF	sepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
společností	společnost	k1gFnSc7	společnost
RCA	RCA	kA	RCA
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
vydávají	vydávat	k5eAaImIp3nP	vydávat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnSc6	první
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Tone	tonout	k5eAaImIp3nS	tonout
Float	Float	k2eAgMnSc1d1	Float
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
Hütter	Hüttrum	k1gNnPc2	Hüttrum
se	s	k7c7	s
Schneiderem	Schneider	k1gMnSc7	Schneider
zakládají	zakládat	k5eAaImIp3nP	zakládat
svou	svůj	k3xOyFgFnSc4	svůj
skupinu	skupina	k1gFnSc4	skupina
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hütterem	Hütter	k1gMnSc7	Hütter
a	a	k8xC	a
Schneiderem	Schneider	k1gMnSc7	Schneider
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
kytarista	kytarista	k1gMnSc1	kytarista
Klaus	Klaus	k1gMnSc1	Klaus
Dinger	Dinger	k1gMnSc1	Dinger
a	a	k8xC	a
Andreas	Andreas	k1gMnSc1	Andreas
Hohmann	Hohmann	k1gMnSc1	Hohmann
coby	coby	k?	coby
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
perkuse	perkuse	k1gFnPc4	perkuse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
si	se	k3xPyFc3	se
Hütter	Hütter	k1gMnSc1	Hütter
se	s	k7c7	s
Schneiderem	Schneider	k1gMnSc7	Schneider
zakládají	zakládat	k5eAaImIp3nP	zakládat
a	a	k8xC	a
vybavují	vybavovat	k5eAaImIp3nP	vybavovat
své	svůj	k3xOyFgInPc4	svůj
Kling	Kling	k1gInSc4	Kling
Klang	Klang	k1gInSc4	Klang
Studio	studio	k1gNnSc1	studio
v	v	k7c6	v
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
první	první	k4xOgFnSc7	první
eponymní	eponymnět	k5eAaPmIp3nS	eponymnět
album	album	k1gNnSc1	album
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
vychází	vycházet	k5eAaImIp3nS	vycházet
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
pilotní	pilotní	k2eAgFnSc1d1	pilotní
skladba	skladba	k1gFnSc1	skladba
Ruckzuck	Ruckzucka	k1gFnPc2	Ruckzucka
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
oficiální	oficiální	k2eAgFnSc7d1	oficiální
znělkou	znělka	k1gFnSc7	znělka
televizního	televizní	k2eAgInSc2d1	televizní
magazínu	magazín	k1gInSc2	magazín
ZDF	ZDF	kA	ZDF
"	"	kIx"	"
<g/>
Kennzeichen	Kennzeichen	k1gInSc1	Kennzeichen
D	D	kA	D
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
vystoupení	vystoupení	k1gNnPc2	vystoupení
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
skupinu	skupina	k1gFnSc4	skupina
opustí	opustit	k5eAaPmIp3nS	opustit
Dinger	Dinger	k1gInSc1	Dinger
<g/>
,	,	kIx,	,
Hohmann	Hohmann	k1gNnSc1	Hohmann
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
přichází	přicházet	k5eAaImIp3nS	přicházet
kytarista	kytarista	k1gMnSc1	kytarista
Michael	Michael	k1gMnSc1	Michael
Rother	Rothra	k1gFnPc2	Rothra
a	a	k8xC	a
basista	basista	k1gMnSc1	basista
Eberhardt	Eberhardt	k1gMnSc1	Eberhardt
Krannemann	Krannemann	k1gMnSc1	Krannemann
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
tedy	tedy	k9	tedy
tvoří	tvořit	k5eAaImIp3nP	tvořit
trojice	trojice	k1gFnSc2	trojice
Schneider-Rother-Krannemann	Schneider-Rother-Krannemanna	k1gFnPc2	Schneider-Rother-Krannemanna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
Hütter	Hütter	k1gInSc1	Hütter
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
vydává	vydávat	k5eAaPmIp3nS	vydávat
album	album	k1gNnSc4	album
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
prodávají	prodávat	k5eAaImIp3nP	prodávat
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
skladbu	skladba	k1gFnSc4	skladba
Autobahn	Autobahna	k1gFnPc2	Autobahna
firmě	firma	k1gFnSc3	firma
Phonogram	Phonogram	k1gInSc4	Phonogram
za	za	k7c4	za
úsměvných	úsměvný	k2eAgInPc2d1	úsměvný
2000	[number]	k4	2000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
s	s	k7c7	s
pocitem	pocit	k1gInSc7	pocit
výhodného	výhodný	k2eAgInSc2d1	výhodný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
připojuje	připojovat	k5eAaImIp3nS	připojovat
Klaus	Klaus	k1gMnSc1	Klaus
Roeder	Roeder	k1gMnSc1	Roeder
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vychází	vycházet	k5eAaImIp3nS	vycházet
ještě	ještě	k9	ještě
album	album	k1gNnSc1	album
Ralf	Ralf	k1gMnSc1	Ralf
und	und	k?	und
Florian	Florian	k1gMnSc1	Florian
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
první	první	k4xOgFnSc4	první
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
desku	deska	k1gFnSc4	deska
Autobahn	Autobahna	k1gFnPc2	Autobahna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
velmi	velmi	k6eAd1	velmi
futuristickým	futuristický	k2eAgInSc7d1	futuristický
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
znamenala	znamenat	k5eAaImAgFnS	znamenat
velký	velký	k2eAgInSc4d1	velký
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
skladbě	skladba	k1gFnSc6	skladba
Autobahn	Autobahn	k1gMnSc1	Autobahn
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dálnice	dálnice	k1gFnSc1	dálnice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ozývají	ozývat	k5eAaImIp3nP	ozývat
zvuky	zvuk	k1gInPc7	zvuk
klapnutí	klapnutí	k1gNnSc2	klapnutí
dveří	dveře	k1gFnPc2	dveře
od	od	k7c2	od
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
startování	startování	k1gNnSc2	startování
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
kolemjedoucích	kolemjedoucí	k2eAgNnPc2d1	kolemjedoucí
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
na	na	k7c4	na
magnetofon	magnetofon	k1gInSc4	magnetofon
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
Volkswagenu	volkswagen	k1gInSc6	volkswagen
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
vzletnými	vzletný	k2eAgInPc7d1	vzletný
a	a	k8xC	a
roztažnými	roztažný	k2eAgInPc7d1	roztažný
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
hudebními	hudební	k2eAgFnPc7d1	hudební
odborníky	odborník	k1gMnPc7	odborník
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
předchůdkyni	předchůdkyně	k1gFnSc4	předchůdkyně
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
trance	tranec	k1gInSc2	tranec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
koncertní	koncertní	k2eAgMnSc1d1	koncertní
tour	tour	k1gMnSc1	tour
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
připojili	připojit	k5eAaPmAgMnP	připojit
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Flür	Flür	k1gMnSc1	Flür
a	a	k8xC	a
Karl	Karl	k1gMnSc1	Karl
Bartos	Bartos	k1gMnSc1	Bartos
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
setrvali	setrvat	k5eAaPmAgMnP	setrvat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zaznamenávala	zaznamenávat	k5eAaImAgFnS	zaznamenávat
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
obsadila	obsadit	k5eAaPmAgFnS	obsadit
skladba	skladba	k1gFnSc1	skladba
Autobahn	Autobahn	k1gInSc1	Autobahn
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
větší	veliký	k2eAgInSc1d2	veliký
úspěch	úspěch	k1gInSc1	úspěch
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgInS	přijít
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Radio-Aktivität	Radio-Aktivitäta	k1gFnPc2	Radio-Aktivitäta
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Radioactivity	Radioactivita	k1gFnPc4	Radioactivita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
kromě	kromě	k7c2	kromě
tématu	téma	k1gNnSc2	téma
využití	využití	k1gNnSc2	využití
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
energie	energie	k1gFnSc2	energie
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
hold	hold	k1gInSc4	hold
objevení	objevení	k1gNnSc2	objevení
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
Radio-Aktivität	Radio-Aktivitäta	k1gFnPc2	Radio-Aktivitäta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
letním	letní	k2eAgInSc7d1	letní
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc1	jeho
prodejnost	prodejnost	k1gFnSc1	prodejnost
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
magické	magický	k2eAgFnSc2d1	magická
sedmimístné	sedmimístný	k2eAgFnSc2d1	sedmimístná
cifry	cifra	k1gFnSc2	cifra
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
albem	album	k1gNnSc7	album
Trans	trans	k1gInSc4	trans
Europa	Europa	k1gFnSc1	Europa
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Trans-Europe	Trans-Europ	k1gInSc5	Trans-Europ
Express	express	k1gInSc1	express
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgInSc1d1	obsahující
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
hit	hit	k1gInSc1	hit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k8xC	i
videoklip	videoklip	k1gInSc1	videoklip
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
nechala	nechat	k5eAaPmAgFnS	nechat
zcela	zcela	k6eAd1	zcela
zjevně	zjevně	k6eAd1	zjevně
inspirovat	inspirovat	k5eAaBmF	inspirovat
filmem	film	k1gInSc7	film
Metropolis	Metropolis	k1gFnSc2	Metropolis
Fritze	Fritze	k1gFnSc2	Fritze
Langa	Lang	k1gMnSc2	Lang
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
rovněž	rovněž	k9	rovněž
pochází	pocházet	k5eAaImIp3nS	pocházet
hit	hit	k1gInSc1	hit
Schaufensterpuppen	Schaufensterpuppen	k2eAgInSc1d1	Schaufensterpuppen
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Showroom	Showroom	k1gInSc1	Showroom
Dummies	Dummiesa	k1gFnPc2	Dummiesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
poprvé	poprvé	k6eAd1	poprvé
transformují	transformovat	k5eAaBmIp3nP	transformovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
neživých	živý	k2eNgFnPc2d1	neživá
figurín	figurína	k1gFnPc2	figurína
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
-	-	kIx~	-
pobídnuti	pobídnut	k2eAgMnPc1d1	pobídnut
úspěchem	úspěch	k1gInSc7	úspěch
předešlého	předešlý	k2eAgNnSc2d1	předešlé
alba	album	k1gNnSc2	album
-	-	kIx~	-
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgFnSc4d1	další
studiovou	studiový	k2eAgFnSc4d1	studiová
nahrávku	nahrávka	k1gFnSc4	nahrávka
Die	Die	k1gMnSc1	Die
Mensch	Mensch	k1gMnSc1	Mensch
Maschine	Maschin	k1gInSc5	Maschin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc5	The
Man-Machine	Man-Machin	k1gMnSc5	Man-Machin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hity	hit	k1gInPc4	hit
jako	jako	k8xC	jako
Die	Die	k1gFnPc4	Die
Roboter	Robotrum	k1gNnPc2	Robotrum
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Robots	Robotsa	k1gFnPc2	Robotsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neonlichts	Neonlichts	k1gInSc1	Neonlichts
(	(	kIx(	(
<g/>
Neon	neon	k1gInSc1	neon
Lights	Lights	k1gInSc1	Lights
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ještě	ještě	k9	ještě
známější	známý	k2eAgFnSc1d2	známější
Das	Das	k1gFnSc1	Das
Model	modla	k1gFnPc2	modla
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zdolává	zdolávat	k5eAaImIp3nS	zdolávat
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
příčku	příčka	k1gFnSc4	příčka
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
cover	cover	k1gMnSc1	cover
verzi	verze	k1gFnSc4	verze
skladby	skladba	k1gFnSc2	skladba
Das	Das	k1gMnSc1	Das
Modell	Modell	k1gMnSc1	Modell
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Man-Machine	Man-Machin	k1gInSc5	Man-Machin
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
albem	album	k1gNnSc7	album
Computer	computer	k1gInSc1	computer
World	World	k1gInSc1	World
představuje	představovat	k5eAaImIp3nS	představovat
naprostý	naprostý	k2eAgInSc4d1	naprostý
vrchol	vrchol	k1gInSc4	vrchol
tvorby	tvorba	k1gFnSc2	tvorba
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
Computerwelt	Computerwelt	k1gInSc4	Computerwelt
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Computer	computer	k1gInSc1	computer
World	Worlda	k1gFnPc2	Worlda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
Taschenrechner	Taschenrechner	k1gInSc1	Taschenrechner
(	(	kIx(	(
<g/>
Pocket	Pocket	k1gMnSc1	Pocket
Calculator	Calculator	k1gMnSc1	Calculator
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
kromě	kromě	k7c2	kromě
anglické	anglický	k2eAgFnSc2d1	anglická
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
verze	verze	k1gFnSc2	verze
vydán	vydat	k5eAaPmNgInS	vydat
také	také	k9	také
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
francouzské	francouzský	k2eAgNnSc1d1	francouzské
(	(	kIx(	(
<g/>
Mini	mini	k2eAgMnSc1d1	mini
Calculateur	Calculateur	k1gMnSc1	Calculateur
<g/>
)	)	kIx)	)
a	a	k8xC	a
japonské	japonský	k2eAgFnPc1d1	japonská
(	(	kIx(	(
<g/>
Dentaku	Dentak	k1gMnSc6	Dentak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
skupina	skupina	k1gFnSc1	skupina
často	často	k6eAd1	často
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
vystoupeních	vystoupení	k1gNnPc6	vystoupení
za	za	k7c4	za
verzi	verze	k1gFnSc4	verze
anglickou	anglický	k2eAgFnSc4d1	anglická
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
hrají	hrát	k5eAaImIp3nP	hrát
italskou	italský	k2eAgFnSc4d1	italská
verzi	verze	k1gFnSc4	verze
Mini	mini	k2eAgFnSc2d1	mini
Calcolatore	Calcolator	k1gMnSc5	Calcolator
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
Picollo	Picolla	k1gMnSc5	Picolla
Calcolatore	Calcolator	k1gMnSc5	Calcolator
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
polskou	polský	k2eAgFnSc4d1	polská
Mini	mini	k2eAgFnSc4d1	mini
Kalkulator	Kalkulator	k1gMnSc1	Kalkulator
a	a	k8xC	a
v	v	k7c6	v
rusku	rusko	k1gNnSc6	rusko
verzi	verze	k1gFnSc4	verze
ruskou	ruský	k2eAgFnSc4d1	ruská
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
klasikami	klasika	k1gFnPc7	klasika
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
skladby	skladba	k1gFnPc1	skladba
Heimcomputer	Heimcomputer	k1gInSc1	Heimcomputer
(	(	kIx(	(
<g/>
Home	Home	k1gInSc1	Home
Computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
či	či	k8xC	či
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
Computer	computer	k1gInSc1	computer
World	Worldo	k1gNnPc2	Worldo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
zremixovaných	zremixovaný	k2eAgFnPc6d1	zremixovaná
tanečních	taneční	k2eAgFnPc6d1	taneční
verzích	verze	k1gFnPc6	verze
jako	jako	k8xC	jako
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
rovněž	rovněž	k9	rovněž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
singl	singl	k1gInSc1	singl
Computerliebe	Computerlieb	k1gInSc5	Computerlieb
(	(	kIx(	(
<g/>
Computer	computer	k1gInSc1	computer
Love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
melodii	melodie	k1gFnSc4	melodie
použila	použít	k5eAaPmAgFnS	použít
skupina	skupina	k1gFnSc1	skupina
Coldplay	Coldplaa	k1gMnSc2	Coldplaa
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
skladby	skladba	k1gFnSc2	skladba
Talk	Talka	k1gFnPc2	Talka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
B-strana	Btrana	k1gFnSc1	B-strana
k	k	k7c3	k
singlu	singl	k1gInSc3	singl
Computer	computer	k1gInSc1	computer
Love	lov	k1gInSc5	lov
vyšla	vyjít	k5eAaPmAgFnS	vyjít
starší	starší	k1gMnPc4	starší
skladba	skladba	k1gFnSc1	skladba
The	The	k1gFnSc2	The
Model	modla	k1gFnPc2	modla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsadila	obsadit	k5eAaPmAgFnS	obsadit
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jediným	jediný	k2eAgInSc7d1	jediný
singlem	singl	k1gInSc7	singl
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabodovala	zabodovat	k5eAaPmAgFnS	zabodovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Computer	computer	k1gInSc1	computer
World	Worldo	k1gNnPc2	Worldo
následovala	následovat	k5eAaImAgFnS	následovat
světová	světový	k2eAgFnSc1d1	světová
koncertní	koncertní	k2eAgFnSc1d1	koncertní
tour	tour	k1gInSc1	tour
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yQgFnSc2	který
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
procestovali	procestovat	k5eAaPmAgMnP	procestovat
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zemí	zem	k1gFnPc2	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc4	Tour
byla	být	k5eAaImAgFnS	být
pojata	pojmout	k5eAaPmNgFnS	pojmout
velmi	velmi	k6eAd1	velmi
propracovaně	propracovaně	k6eAd1	propracovaně
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
techniky	technika	k1gFnSc2	technika
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
za	za	k7c7	za
hudebníky	hudebník	k1gMnPc7	hudebník
byly	být	k5eAaImAgFnP	být
promítány	promítán	k2eAgInPc4d1	promítán
videoklipy	videoklip	k1gInPc4	videoklip
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
skladbám	skladba	k1gFnPc3	skladba
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
navíc	navíc	k6eAd1	navíc
používali	používat	k5eAaImAgMnP	používat
malá	malý	k2eAgNnPc4d1	malé
zvuková	zvukový	k2eAgNnPc4d1	zvukové
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
posílali	posílat	k5eAaImAgMnP	posílat
lidem	člověk	k1gMnPc3	člověk
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
mohli	moct	k5eAaImAgMnP	moct
hrát	hrát	k5eAaImF	hrát
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
videoklipu	videoklip	k1gInSc6	videoklip
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
Pocket	Pocket	k1gMnSc1	Pocket
Calculator	Calculator	k1gMnSc1	Calculator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Computer	computer	k1gInSc1	computer
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
byla	být	k5eAaImAgFnS	být
první	první	k4xOgInSc4	první
koncertní	koncertní	k2eAgInSc4d1	koncertní
tour	tour	k1gInSc4	tour
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
sice	sice	k8xC	sice
skupina	skupina	k1gFnSc1	skupina
připravovala	připravovat	k5eAaImAgFnS	připravovat
album	album	k1gNnSc4	album
Techno	Techno	k6eAd1	Techno
Pop	pop	k1gMnSc1	pop
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
frontman	frontman	k1gMnSc1	frontman
kapely	kapela	k1gFnSc2	kapela
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
-	-	kIx~	-
nadšený	nadšený	k2eAgMnSc1d1	nadšený
cyklista	cyklista	k1gMnSc1	cyklista
-	-	kIx~	-
měl	mít	k5eAaImAgInS	mít
těžkou	těžký	k2eAgFnSc4d1	těžká
nehodu	nehoda	k1gFnSc4	nehoda
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
kómatu	kóma	k1gNnSc6	kóma
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
hotovo	hotov	k2eAgNnSc4d1	hotovo
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
alba	album	k1gNnSc2	album
vyšel	vyjít	k5eAaPmAgInS	vyjít
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
singl	singl	k1gInSc1	singl
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgInSc4d1	nadcházející
závod	závod	k1gInSc4	závod
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
a	a	k8xC	a
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
remixy	remix	k1gInPc4	remix
této	tento	k3xDgFnSc2	tento
skladby	skladba	k1gFnSc2	skladba
na	na	k7c6	na
samostatném	samostatný	k2eAgInSc6d1	samostatný
singlu	singl	k1gInSc6	singl
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
však	však	k9	však
uváděly	uvádět	k5eAaImAgInP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
Techno	Techen	k2eAgNnSc1d1	Techno
Pop	pop	k1gInSc4	pop
naopak	naopak	k6eAd1	naopak
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
sérii	série	k1gFnSc6	série
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
jako	jako	k9	jako
promo	promo	k6eAd1	promo
materiál	materiál	k1gInSc4	materiál
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
několika	několik	k4yIc3	několik
rádiovým	rádiový	k2eAgFnPc3d1	rádiová
stanicím	stanice	k1gFnPc3	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
demo	demo	k2eAgFnPc1d1	demo
nahrávky	nahrávka	k1gFnPc1	nahrávka
skladeb	skladba	k1gFnPc2	skladba
Techno	Techno	k6eAd1	Techno
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
Object	Objecta	k1gFnPc2	Objecta
a	a	k8xC	a
The	The	k1gFnPc2	The
Telephone	Telephon	k1gInSc5	Telephon
Call	Calla	k1gFnPc2	Calla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
až	až	k6eAd1	až
na	na	k7c4	na
posledně	posledně	k6eAd1	posledně
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
několika	několik	k4yIc6	několik
neoficiálních	oficiální	k2eNgFnPc6d1	neoficiální
kompilacích	kompilace	k1gFnPc6	kompilace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
zpoždění	zpoždění	k1gNnSc2	zpoždění
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
analogového	analogový	k2eAgInSc2d1	analogový
záznamu	záznam	k1gInSc2	záznam
na	na	k7c4	na
digitální	digitální	k2eAgInSc4d1	digitální
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Electric	Electrice	k1gInPc2	Electrice
Cafe	Caf	k1gFnSc2	Caf
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
např.	např.	kA	např.
skladbu	skladba	k1gFnSc4	skladba
Music	Musice	k1gFnPc2	Musice
Non	Non	k1gFnSc1	Non
Stop	stop	k1gInSc1	stop
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zakončovací	zakončovací	k2eAgFnSc7d1	zakončovací
skladbou	skladba	k1gFnSc7	skladba
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
hudebníci	hudebník	k1gMnPc1	hudebník
odcházeli	odcházet	k5eAaImAgMnP	odcházet
po	po	k7c6	po
svých	svůj	k3xOyFgNnPc6	svůj
sólech	sólo	k1gNnPc6	sólo
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Electric	Electrice	k1gInPc2	Electrice
Cafe	Cafe	k1gFnPc2	Cafe
byl	být	k5eAaImAgInS	být
Der	drát	k5eAaImRp2nS	drát
Telefon	telefon	k1gInSc1	telefon
Anruf	Anruf	k1gInSc4	Anruf
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Telephone	Telephon	k1gMnSc5	Telephon
Call	Callum	k1gNnPc2	Callum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
zpívá	zpívat	k5eAaImIp3nS	zpívat
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
Karl	Karla	k1gFnPc2	Karla
Bartos	Bartosa	k1gFnPc2	Bartosa
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgInP	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
taneční	taneční	k2eAgFnSc6d1	taneční
hitparádě	hitparáda	k1gFnSc6	hitparáda
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
Electric	Electrice	k1gFnPc2	Electrice
Cafe	Cafe	k1gFnSc1	Cafe
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
posledním	poslední	k2eAgInSc7d1	poslední
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
opouští	opouštět	k5eAaImIp3nS	opouštět
skupinu	skupina	k1gFnSc4	skupina
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Flür	Flür	k1gMnSc1	Flür
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Karl	Karl	k1gMnSc1	Karl
Bartos	Bartos	k1gMnSc1	Bartos
<g/>
.	.	kIx.	.
</s>
<s>
Bartos	Bartos	k1gMnSc1	Bartos
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
vadilo	vadit	k5eAaImAgNnS	vadit
"	"	kIx"	"
<g/>
šnečí	šnečí	k2eAgNnSc4d1	šnečí
tempo	tempo	k1gNnSc4	tempo
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgMnSc7	který
Ralf	Ralf	k1gMnSc1	Ralf
a	a	k8xC	a
Florian	Florian	k1gMnSc1	Florian
připravovali	připravovat	k5eAaImAgMnP	připravovat
alba	album	k1gNnPc4	album
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dennodenně	dennodenně	k6eAd1	dennodenně
oddávali	oddávat	k5eAaImAgMnP	oddávat
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
své	svůj	k3xOyFgFnSc6	svůj
cyklistické	cyklistický	k2eAgFnSc6d1	cyklistická
vášni	vášeň	k1gFnSc6	vášeň
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
skládali	skládat	k5eAaImAgMnP	skládat
hudbu	hudba	k1gFnSc4	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bartos	Bartos	k1gMnSc1	Bartos
měl	mít	k5eAaImAgMnS	mít
de	de	k?	de
facto	facto	k1gNnSc4	facto
pravdu	pravda	k1gFnSc4	pravda
-	-	kIx~	-
příprava	příprava	k1gFnSc1	příprava
tehdy	tehdy	k6eAd1	tehdy
posledního	poslední	k2eAgNnSc2d1	poslední
alba	album	k1gNnSc2	album
Electric	Electric	k1gMnSc1	Electric
Café	café	k1gNnSc1	café
zabrala	zabrat	k5eAaPmAgFnS	zabrat
skupině	skupina	k1gFnSc6	skupina
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
časová	časový	k2eAgFnSc1d1	časová
perioda	perioda	k1gFnSc1	perioda
mezi	mezi	k7c7	mezi
následujícími	následující	k2eAgNnPc7d1	následující
alby	album	k1gNnPc7	album
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
prodlužovala	prodlužovat	k5eAaImAgFnS	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Flüra	Flür	k1gInSc2	Flür
a	a	k8xC	a
Bartose	Bartosa	k1gFnSc3	Bartosa
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
Fritz	Fritz	k1gMnSc1	Fritz
Hilpert	Hilpert	k1gMnSc1	Hilpert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
Hütter	Hütter	k1gMnSc1	Hütter
<g/>
,	,	kIx,	,
Schneider	Schneider	k1gMnSc1	Schneider
a	a	k8xC	a
Hilpert	Hilpert	k1gMnSc1	Hilpert
remixují	remixovat	k5eAaImIp3nP	remixovat
a	a	k8xC	a
modernizují	modernizovat	k5eAaBmIp3nP	modernizovat
staré	starý	k2eAgInPc4d1	starý
singly	singl	k1gInPc4	singl
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
pak	pak	k6eAd1	pak
vychází	vycházet	k5eAaImIp3nS	vycházet
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gFnSc2	The
Mix	mix	k1gInSc1	mix
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
jakési	jakýsi	k3yIgNnSc4	jakýsi
vylepšené	vylepšený	k2eAgNnSc4d1	vylepšené
výběrové	výběrový	k2eAgNnSc4d1	výběrové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
objeví	objevit	k5eAaPmIp3nS	objevit
Portugalec	Portugalec	k1gMnSc1	Portugalec
Fernando	Fernanda	k1gFnSc5	Fernanda
Abrantes	Abrantes	k1gInSc4	Abrantes
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tour	toura	k1gFnPc2	toura
k	k	k7c3	k
The	The	k1gFnPc3	The
Mix	mix	k1gInSc4	mix
se	se	k3xPyFc4	se
však	však	k9	však
Abrantes	Abrantes	k1gInSc1	Abrantes
s	s	k7c7	s
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
rozloučí	rozloučit	k5eAaPmIp3nP	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
jím	on	k3xPp3gMnSc7	on
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
špatná	špatný	k2eAgFnSc1d1	špatná
možnost	možnost	k1gFnSc1	možnost
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
důvod	důvod	k1gInSc1	důvod
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Abrantes	Abrantes	k1gInSc1	Abrantes
zcela	zcela	k6eAd1	zcela
nezapadal	zapadat	k5eNaPmAgInS	zapadat
do	do	k7c2	do
"	"	kIx"	"
<g/>
robotické	robotický	k2eAgFnSc2d1	robotická
<g/>
"	"	kIx"	"
image	image	k1gFnSc2	image
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
mával	mávat	k5eAaImAgInS	mávat
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
tak	tak	k6eAd1	tak
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Abrantese	Abrantese	k1gFnPc1	Abrantese
můžeme	moct	k5eAaImIp1nP	moct
coby	coby	k?	coby
robota	robota	k1gFnSc1	robota
spatřit	spatřit	k5eAaPmF	spatřit
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
zremixovanému	zremixovaný	k2eAgInSc3d1	zremixovaný
singlu	singl	k1gInSc3	singl
The	The	k1gFnSc2	The
Robots	Robotsa	k1gFnPc2	Robotsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
přichází	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
Henning	Henning	k1gInSc1	Henning
Schmitz	Schmitz	k1gInSc1	Schmitz
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
kapela	kapela	k1gFnSc1	kapela
také	také	k9	také
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
odehrála	odehrát	k5eAaPmAgFnS	odehrát
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Paláci	palác	k1gInSc6	palác
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
koncertem	koncert	k1gInSc7	koncert
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
albu	album	k1gNnSc3	album
The	The	k1gFnSc2	The
Mix	mix	k1gInSc1	mix
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
přes	přes	k7c4	přes
30	[number]	k4	30
zastavení	zastavení	k1gNnPc2	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
Benelux	Benelux	k1gInSc4	Benelux
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
devadesátá	devadesátý	k4xOgNnPc4	devadesátý
léta	léto	k1gNnPc4	léto
skupina	skupina	k1gFnSc1	skupina
žádný	žádný	k3yNgInSc4	žádný
nový	nový	k2eAgInSc4d1	nový
hudební	hudební	k2eAgInSc4d1	hudební
materiál	materiál	k1gInSc4	materiál
nevydala	vydat	k5eNaPmAgFnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
rozsáhle	rozsáhle	k6eAd1	rozsáhle
technicky	technicky	k6eAd1	technicky
modernizovali	modernizovat	k5eAaBmAgMnP	modernizovat
své	své	k1gNnSc4	své
Kling	Klinga	k1gFnPc2	Klinga
Klang	Klanga	k1gFnPc2	Klanga
Studio	studio	k1gNnSc4	studio
v	v	k7c6	v
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
skupina	skupina	k1gFnSc1	skupina
zprovozňuje	zprovozňovat	k5eAaImIp3nS	zprovozňovat
své	svůj	k3xOyFgFnPc4	svůj
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
www.kraftwerk.com	www.kraftwerk.com	k1gInSc4	www.kraftwerk.com
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevují	objevovat	k5eAaImIp3nP	objevovat
strohé	strohý	k2eAgFnPc4d1	strohá
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
aktuální	aktuální	k2eAgInSc4d1	aktuální
tour	tour	k1gInSc4	tour
apod.	apod.	kA	apod.
Další	další	k2eAgFnSc1d1	další
aktivita	aktivita	k1gFnSc1	aktivita
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
až	až	k8xS	až
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Tribal	Tribal	k1gInSc1	Tribal
Gathering	Gathering	k1gInSc1	Gathering
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
odehráli	odehrát	k5eAaPmAgMnP	odehrát
novou	nový	k2eAgFnSc4d1	nová
neznámou	známý	k2eNgFnSc4d1	neznámá
skladbu	skladba	k1gFnSc4	skladba
a	a	k8xC	a
text	text	k1gInSc4	text
skladby	skladba	k1gFnSc2	skladba
Computer	computer	k1gInSc1	computer
World	World	k1gInSc1	World
byl	být	k5eAaImAgInS	být
mírně	mírně	k6eAd1	mírně
pozměněn	pozměnit	k5eAaPmNgInS	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
poté	poté	k6eAd1	poté
odehráli	odehrát	k5eAaPmAgMnP	odehrát
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
koncerty	koncert	k1gInPc4	koncert
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Linci	Linec	k1gInSc6	Linec
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
Karlsruhe	Karlsruhe	k1gFnSc6	Karlsruhe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krom	krom	k7c2	krom
nové	nový	k2eAgFnSc2d1	nová
skladby	skladba	k1gFnSc2	skladba
zahráli	zahrát	k5eAaPmAgMnP	zahrát
ještě	ještě	k6eAd1	ještě
dvě	dva	k4xCgFnPc4	dva
další	další	k2eAgFnPc4d1	další
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
nové	nový	k2eAgFnPc1d1	nová
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
alba	album	k1gNnSc2	album
skupiny	skupina	k1gFnSc2	skupina
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
byly	být	k5eAaImAgFnP	být
předány	předat	k5eAaPmNgInP	předat
za	za	k7c2	za
přísného	přísný	k2eAgNnSc2d1	přísné
utajení	utajení	k1gNnSc2	utajení
pobočce	pobočka	k1gFnSc6	pobočka
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
EMI	EMI	kA	EMI
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
přísném	přísný	k2eAgNnSc6d1	přísné
utajení	utajení	k1gNnSc6	utajení
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
EMI	EMI	kA	EMI
mohli	moct	k5eAaImAgMnP	moct
skladby	skladba	k1gFnPc4	skladba
slyšet	slyšet	k5eAaImF	slyšet
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
půli	půle	k1gFnSc6	půle
roku	rok	k1gInSc2	rok
vyjde	vyjít	k5eAaPmIp3nS	vyjít
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
a	a	k8xC	a
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
firmy	firma	k1gFnSc2	firma
Doepfer	Doepfra	k1gFnPc2	Doepfra
se	se	k3xPyFc4	se
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
objevila	objevit	k5eAaPmAgFnS	objevit
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
zahájí	zahájit	k5eAaPmIp3nS	zahájit
novou	nova	k1gFnSc7	nova
tour	toura	k1gFnPc2	toura
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čtyřiadvacet	čtyřiadvacet	k4xCc4	čtyřiadvacet
hodin	hodina	k1gFnPc2	hodina
však	však	k9	však
informace	informace	k1gFnSc1	informace
zmizela	zmizet	k5eAaPmAgFnS	zmizet
a	a	k8xC	a
vedení	vedení	k1gNnSc3	vedení
firmy	firma	k1gFnSc2	firma
Doepfer	Doepfer	k1gInSc1	Doepfer
situaci	situace	k1gFnSc4	situace
vysvětlilo	vysvětlit	k5eAaPmAgNnS	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
dni	den	k1gInSc6	den
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
rozmyslela	rozmyslet	k5eAaPmAgFnS	rozmyslet
<g/>
.	.	kIx.	.
</s>
<s>
Připravované	připravovaný	k2eAgNnSc1d1	připravované
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
tedy	tedy	k9	tedy
nikdy	nikdy	k6eAd1	nikdy
nespatřilo	spatřit	k5eNaPmAgNnS	spatřit
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
ony	onen	k3xDgInPc1	onen
tři	tři	k4xCgInPc1	tři
naživo	naživo	k1gNnSc1	naživo
zahrané	zahraný	k2eAgFnPc1d1	zahraná
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1998	[number]	k4	1998
vychází	vycházet	k5eAaImIp3nS	vycházet
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
společnosti	společnost	k1gFnSc2	společnost
Concert	Concert	k1gInSc1	Concert
Classics	Classics	k1gInSc1	Classics
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
živé	živý	k2eAgNnSc1d1	živé
vystoupení	vystoupení	k1gNnSc1	vystoupení
skupiny	skupina	k1gFnSc2	skupina
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oficiální	oficiální	k2eAgInSc4d1	oficiální
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tracklist	tracklist	k1gInSc1	tracklist
alba	album	k1gNnSc2	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
grafické	grafický	k2eAgNnSc4d1	grafické
řešení	řešení	k1gNnSc4	řešení
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
působí	působit	k5eAaImIp3nS	působit
poněkud	poněkud	k6eAd1	poněkud
levně	levně	k6eAd1	levně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1998	[number]	k4	1998
se	se	k3xPyFc4	se
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
zastávkami	zastávka	k1gFnPc7	zastávka
poctili	poctít	k5eAaPmAgMnP	poctít
i	i	k9	i
Španělsko	Španělsko	k1gNnSc4	Španělsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
turné	turné	k1gNnSc2	turné
ukončili	ukončit	k5eAaPmAgMnP	ukončit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Červencový	červencový	k2eAgInSc4d1	červencový
washingtonský	washingtonský	k2eAgInSc4d1	washingtonský
koncert	koncert	k1gInSc4	koncert
však	však	k9	však
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
zrušit	zrušit	k5eAaPmF	zrušit
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgNnSc3d1	špatné
počasí	počasí	k1gNnSc3	počasí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pódium	pódium	k1gNnSc4	pódium
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
blesky	blesk	k1gInPc4	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
vydávají	vydávat	k5eAaImIp3nP	vydávat
třicetisekundový	třicetisekundový	k2eAgInSc4d1	třicetisekundový
jingle	jingle	k1gInSc4	jingle
k	k	k7c3	k
nadcházející	nadcházející	k2eAgFnSc3d1	nadcházející
výstavě	výstava	k1gFnSc3	výstava
Expo	Expo	k1gNnSc1	Expo
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
Expo	Expo	k1gNnSc1	Expo
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pronesena	pronesen	k2eAgNnPc1d1	proneseno
hlasem	hlasem	k6eAd1	hlasem
zdeformovaným	zdeformovaný	k2eAgMnSc7d1	zdeformovaný
přes	přes	k7c4	přes
vocoder	vocoder	k1gInSc4	vocoder
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
<g/>
,	,	kIx,	,
německém	německý	k2eAgNnSc6d1	německé
<g/>
,	,	kIx,	,
francouzském	francouzský	k2eAgNnSc6d1	francouzské
<g/>
,	,	kIx,	,
španělském	španělský	k2eAgNnSc6d1	španělské
<g/>
,	,	kIx,	,
ruském	ruský	k2eAgMnSc6d1	ruský
a	a	k8xC	a
japonském	japonský	k2eAgInSc6d1	japonský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
však	však	k9	však
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
velký	velký	k2eAgInSc4d1	velký
rozruch	rozruch	k1gInSc4	rozruch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
skupina	skupina	k1gFnSc1	skupina
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
půlminutový	půlminutový	k2eAgInSc4d1	půlminutový
počin	počin	k1gInSc4	počin
dostala	dostat	k5eAaPmAgFnS	dostat
400	[number]	k4	400
000	[number]	k4	000
DM	dm	kA	dm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
tedy	tedy	k8xC	tedy
skupina	skupina	k1gFnSc1	skupina
vydává	vydávat	k5eAaImIp3nS	vydávat
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
singl	singl	k1gInSc4	singl
Expo	Expo	k1gNnSc1	Expo
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
vychází	vycházet	k5eAaImIp3nS	vycházet
singl	singl	k1gInSc1	singl
s	s	k7c7	s
remixy	remix	k1gInPc7	remix
titulní	titulní	k2eAgFnSc2d1	titulní
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
skupina	skupina	k1gFnSc1	skupina
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlá	hodlat	k5eAaImIp3nS	hodlat
vydat	vydat	k5eAaPmF	vydat
nové	nový	k2eAgNnSc4d1	nové
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
cyjklistického	cyjklistický	k2eAgInSc2d1	cyjklistický
závodu	závod	k1gInSc2	závod
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc4	Franc
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2002	[number]	k4	2002
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
miniturné	miniturný	k2eAgFnPc4d1	miniturný
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
několik	několik	k4yIc1	několik
zastávek	zastávka	k1gFnPc2	zastávka
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
pak	pak	k6eAd1	pak
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
skupina	skupina	k1gFnSc1	skupina
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
albem	album	k1gNnSc7	album
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
Soundtracks	Soundtracks	k1gInSc4	Soundtracks
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
obsadila	obsadit	k5eAaPmAgFnS	obsadit
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
předcházel	předcházet	k5eAaImAgInS	předcházet
singl	singl	k1gInSc1	singl
Elektrokardiogramm	Elektrokardiogramm	k1gInSc1	Elektrokardiogramm
vydaný	vydaný	k2eAgInSc1d1	vydaný
však	však	k9	však
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
promo-verzi	promoerze	k1gFnSc6	promo-verze
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
17	[number]	k4	17
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
posledního	poslední	k2eAgNnSc2d1	poslední
alba	album	k1gNnSc2	album
mají	mít	k5eAaImIp3nP	mít
světu	svět	k1gInSc3	svět
co	co	k3yQnSc4	co
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
takového	takový	k3xDgNnSc2	takový
futuristického	futuristický	k2eAgNnSc2d1	futuristické
vizionářství	vizionářství	k1gNnSc2	vizionářství
jako	jako	k8xC	jako
desky	deska	k1gFnSc2	deska
z	z	k7c2	z
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
důležitý	důležitý	k2eAgInSc1d1	důležitý
článek	článek	k1gInSc1	článek
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
využívá	využívat	k5eAaImIp3nS	využívat
rozvláčných	rozvláčný	k2eAgInPc2d1	rozvláčný
kovových	kovový	k2eAgInPc2d1	kovový
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
také	také	k9	také
ústřední	ústřední	k2eAgNnSc4d1	ústřední
téma	téma	k1gNnSc4	téma
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
cyklistického	cyklistický	k2eAgInSc2d1	cyklistický
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
věcí	věc	k1gFnPc2	věc
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojených	spojený	k2eAgInPc2d1	spojený
(	(	kIx(	(
<g/>
životospráva	životospráva	k1gFnSc1	životospráva
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgNnSc4d1	sportovní
soustředění	soustředění	k1gNnSc4	soustředění
a	a	k8xC	a
využití	využití	k1gNnSc4	využití
moderních	moderní	k2eAgInPc2d1	moderní
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
technik	technika	k1gFnPc2	technika
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cyklistiky	cyklistika	k1gFnSc2	cyklistika
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
originálně	originálně	k6eAd1	originálně
a	a	k8xC	a
neotřele	otřele	k6eNd1	otřele
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
singl	singl	k1gInSc1	singl
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
videoklip	videoklip	k1gInSc1	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vychází	vycházet	k5eAaImIp3nS	vycházet
další	další	k2eAgInSc4d1	další
singl	singl	k1gInSc4	singl
Aerodynamik	aerodynamika	k1gFnPc2	aerodynamika
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
skupina	skupina	k1gFnSc1	skupina
udělala	udělat	k5eAaPmAgFnS	udělat
strhující	strhující	k2eAgNnSc4d1	strhující
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
světové	světový	k2eAgInPc4d1	světový
tour	tour	k1gInSc4	tour
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
poctili	poctít	k5eAaPmAgMnP	poctít
svou	svůj	k3xOyFgFnSc7	svůj
návštěvou	návštěva	k1gFnSc7	návštěva
i	i	k9	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
vydali	vydat	k5eAaPmAgMnP	vydat
svou	svůj	k3xOyFgFnSc4	svůj
remasterovanou	remasterovaný	k2eAgFnSc4d1	remasterovaná
edici	edice	k1gFnSc4	edice
The	The	k1gMnSc2	The
Catalogue	Catalogu	k1gMnSc2	Catalogu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
pouhých	pouhý	k2eAgInPc2d1	pouhý
1000	[number]	k4	1000
ks	ks	kA	ks
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
vydávají	vydávat	k5eAaImIp3nP	vydávat
dvoudiskové	dvoudiskový	k2eAgNnSc4d1	dvoudiskové
album	album	k1gNnSc4	album
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
název	název	k1gInSc1	název
Minimum-Maximum	Minimum-Maximum	k1gInSc1	Minimum-Maximum
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Dostupný	dostupný	k2eAgMnSc1d1	dostupný
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
discích	disk	k1gInPc6	disk
CD	CD	kA	CD
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
s	s	k7c7	s
obrazovým	obrazový	k2eAgInSc7d1	obrazový
záznamem	záznam	k1gInSc7	záznam
na	na	k7c4	na
dvou	dva	k4xCgInPc2	dva
DVD	DVD	kA	DVD
nebo	nebo	k8xC	nebo
SACD	SACD	kA	SACD
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
turné	turné	k1gNnSc6	turné
i	i	k8xC	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc4	svůj
setlistu	setlista	k1gMnSc4	setlista
zařadila	zařadit	k5eAaPmAgFnS	zařadit
i	i	k8xC	i
skladby	skladba	k1gFnSc2	skladba
Showroom	Showroom	k1gInSc1	Showroom
Dummies	Dummies	k1gMnSc1	Dummies
a	a	k8xC	a
Computerlove	Computerlov	k1gInSc5	Computerlov
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
několika	několik	k4yIc6	několik
festivalech	festival	k1gInPc6	festival
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Summer	Summer	k1gMnSc1	Summer
of	of	k?	of
Love	lov	k1gInSc5	lov
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
naposled	naposled	k6eAd1	naposled
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
koncertní	koncertní	k2eAgFnSc6d1	koncertní
sestavě	sestava	k1gFnSc6	sestava
zařazen	zařadit	k5eAaPmNgMnS	zařadit
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
poslední	poslední	k2eAgInSc1d1	poslední
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc4	Franc
vyšel	vyjít	k5eAaPmAgMnS	vyjít
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
remixy	remix	k1gInPc4	remix
skladeb	skladba	k1gFnPc2	skladba
Aerodynamik	aerodynamika	k1gFnPc2	aerodynamika
/	/	kIx~	/
La	la	k0	la
Forme	Form	k1gMnSc5	Form
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zremixovala	zremixovat	k5eAaImAgFnS	zremixovat
skupina	skupina	k1gFnSc1	skupina
Hot	hot	k0	hot
Chip	Chip	k1gMnSc1	Chip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
zakládá	zakládat	k5eAaImIp3nS	zakládat
svůj	svůj	k3xOyFgInSc4	svůj
profil	profil	k1gInSc4	profil
na	na	k7c6	na
serveru	server	k1gInSc6	server
Myspace	Myspace	k1gFnSc2	Myspace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
opět	opět	k6eAd1	opět
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
započali	započnout	k5eAaPmAgMnP	započnout
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehráli	odehrát	k5eAaPmAgMnP	odehrát
4	[number]	k4	4
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncertní	koncertní	k2eAgFnSc6d1	koncertní
sestavě	sestava	k1gFnSc6	sestava
však	však	k9	však
chyběl	chybět	k5eAaImAgMnS	chybět
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
Stefan	Stefan	k1gMnSc1	Stefan
Pfaffe	Pfaff	k1gInSc5	Pfaff
<g/>
,	,	kIx,	,
technický	technický	k2eAgMnSc1d1	technický
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
technickou	technický	k2eAgFnSc4d1	technická
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
videoprojekci	videoprojekce	k1gFnSc4	videoprojekce
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interview	interview	k1gNnSc2	interview
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
pro	pro	k7c4	pro
stránky	stránka	k1gFnPc4	stránka
NZherald	NZheralda	k1gFnPc2	NZheralda
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
frontman	frontman	k1gMnSc1	frontman
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
<g/>
,	,	kIx,	,
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
momentálně	momentálně	k6eAd1	momentálně
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
důvod	důvod	k1gInSc4	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
absence	absence	k1gFnSc2	absence
rovněž	rovněž	k9	rovněž
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schneider	Schneider	k1gMnSc1	Schneider
nesnáší	snášet	k5eNaImIp3nS	snášet
přesuny	přesun	k1gInPc4	přesun
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
vystoupeními	vystoupení	k1gNnPc7	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
však	však	k8xC	však
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
několika	několik	k4yIc7	několik
zastávkami	zastávka	k1gFnPc7	zastávka
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Global	globat	k5eAaImAgMnS	globat
Gathering	Gathering	k1gInSc4	Gathering
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
pět	pět	k4xCc1	pět
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
tour	tour	k1gInSc4	tour
ukončili	ukončit	k5eAaPmAgMnP	ukončit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
prosince	prosinec	k1gInSc2	prosinec
dvěma	dva	k4xCgInPc7	dva
koncerty	koncert	k1gInPc7	koncert
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Hilpert	Hilpert	k1gMnSc1	Hilpert
<g/>
,	,	kIx,	,
prodělal	prodělat	k5eAaPmAgMnS	prodělat
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Hilpert	Hilpert	k1gMnSc1	Hilpert
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
zotavil	zotavit	k5eAaPmAgMnS	zotavit
a	a	k8xC	a
již	již	k6eAd1	již
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
odehráli	odehrát	k5eAaPmAgMnP	odehrát
další	další	k2eAgMnPc1d1	další
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
normální	normální	k2eAgFnSc6d1	normální
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
i	i	k8xC	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc4	Tour
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
předskokany	předskokan	k1gMnPc4	předskokan
skupině	skupina	k1gFnSc3	skupina
Radiohead	Radiohead	k1gInSc4	Radiohead
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
turné	turné	k1gNnSc2	turné
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
září	září	k1gNnSc6	září
několika	několik	k4yIc7	několik
koncerty	koncert	k1gInPc7	koncert
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
festivalovými	festivalový	k2eAgNnPc7d1	festivalové
vystoupeními	vystoupení	k1gNnPc7	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Wolfsburgu	Wolfsburg	k1gInSc6	Wolfsburg
<g/>
,	,	kIx,	,
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
Manchesteru	Manchester	k1gInSc6	Manchester
a	a	k8xC	a
v	v	k7c6	v
dánském	dánský	k2eAgNnSc6d1	dánské
Randers	Randersa	k1gFnPc2	Randersa
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
3D	[number]	k4	3D
videoprojekcí	videoprojekce	k1gFnPc2	videoprojekce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
divákům	divák	k1gMnPc3	divák
byly	být	k5eAaImAgInP	být
rozdány	rozdat	k5eAaPmNgInP	rozdat
speciální	speciální	k2eAgFnPc4d1	speciální
3D	[number]	k4	3D
brýle	brýle	k1gFnPc4	brýle
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Remasterovaná	Remasterovaný	k2eAgFnSc1d1	Remasterovaná
edice	edice	k1gFnSc1	edice
alb	album	k1gNnPc2	album
skupiny	skupina	k1gFnSc2	skupina
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Der	drát	k5eAaImRp2nS	drát
Katalog	katalog	k1gInSc1	katalog
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
The	The	k1gFnSc1	The
Catalogue	Catalogue	k1gFnSc1	Catalogue
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Předcházelo	předcházet	k5eAaImAgNnS	předcházet
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
vydání	vydání	k1gNnSc3	vydání
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
remasterovaných	remasterovaný	k2eAgFnPc2d1	remasterovaná
alb	alba	k1gFnPc2	alba
(	(	kIx(	(
<g/>
Autobahn	Autobahna	k1gFnPc2	Autobahna
<g/>
,	,	kIx,	,
Radioactivity	Radioactivita	k1gFnSc2	Radioactivita
<g/>
,	,	kIx,	,
Trans-Europe	Trans-Europ	k1gInSc5	Trans-Europ
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
Machine	Machin	k1gInSc5	Machin
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
World	Worlda	k1gFnPc2	Worlda
<g/>
,	,	kIx,	,
Electric	Electrice	k1gInPc2	Electrice
Cafe	Cafe	k1gNnSc2	Cafe
-	-	kIx~	-
vydané	vydaný	k2eAgInPc4d1	vydaný
pod	pod	k7c4	pod
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Techno	Techno	k6eAd1	Techno
Pop	pop	k1gInSc1	pop
<g/>
"	"	kIx"	"
a	a	k8xC	a
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
formátech	formát	k1gInPc6	formát
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
×	×	k?	×
samostatné	samostatný	k2eAgFnSc2d1	samostatná
CD	CD	kA	CD
<g/>
,	,	kIx,	,
Box	box	k1gInSc1	box
set	set	k1gInSc1	set
8	[number]	k4	8
CD	CD	kA	CD
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
×	×	k?	×
vinyl	vinyl	k1gInSc1	vinyl
LP	LP	kA	LP
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgInSc4d1	digitální
download	download	k1gInSc4	download
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Catalogue	Catalogue	k1gInSc1	Catalogue
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
promo	promo	k6eAd1	promo
verzi	verze	k1gFnSc6	verze
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
1000	[number]	k4	1000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
tři	tři	k4xCgNnPc1	tři
nekomerční	komerční	k2eNgNnPc1d1	nekomerční
alba	album	k1gNnPc1	album
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1971	[number]	k4	1971
až	až	k6eAd1	až
1973	[number]	k4	1973
(	(	kIx(	(
<g/>
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
,	,	kIx,	,
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
2	[number]	k4	2
a	a	k8xC	a
Ralf	Ralf	k1gMnSc1	Ralf
und	und	k?	und
Florian	Florian	k1gMnSc1	Florian
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
vydány	vydat	k5eAaPmNgInP	vydat
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
remasterované	remasterovaný	k2eAgFnSc6d1	remasterovaná
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
poskytnutých	poskytnutý	k2eAgInPc2d1	poskytnutý
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
2009	[number]	k4	2009
pro	pro	k7c4	pro
média	médium	k1gNnPc4	médium
několikrát	několikrát	k6eAd1	několikrát
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
vydá	vydat	k5eAaPmIp3nS	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Hütterových	Hütterův	k2eAgNnPc2d1	Hütterův
slov	slovo	k1gNnPc2	slovo
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
znovu	znovu	k6eAd1	znovu
Hütter	Hütter	k1gMnSc1	Hütter
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
portál	portál	k1gInSc4	portál
BillBoard	billboard	k1gInSc4	billboard
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
však	však	k9	však
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příprava	příprava	k1gFnSc1	příprava
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
zvukovým	zvukový	k2eAgMnSc7d1	zvukový
technikem	technik	k1gMnSc7	technik
Normanem	Norman	k1gMnSc7	Norman
Fairbanksem	Fairbanks	k1gMnSc7	Fairbanks
vydala	vydat	k5eAaPmAgFnS	vydat
aplikaci	aplikace	k1gFnSc4	aplikace
Kling	Kling	k1gInSc1	Kling
Klang	Klang	k1gMnSc1	Klang
Machine	Machin	k1gInSc5	Machin
No	no	k9	no
<g/>
1	[number]	k4	1
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
iPady	iPad	k1gMnPc4	iPad
<g/>
,	,	kIx,	,
iPody	iPod	k1gMnPc4	iPod
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
a	a	k8xC	a
aktuálního	aktuální	k2eAgInSc2d1	aktuální
času	čas	k1gInSc2	čas
generuje	generovat	k5eAaImIp3nS	generovat
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
výstava	výstava	k1gFnSc1	výstava
3	[number]	k4	3
<g/>
D-Video-Installation	D-Video-Installation	k1gInSc1	D-Video-Installation
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
náplní	náplň	k1gFnSc7	náplň
jsou	být	k5eAaImIp3nP	být
3D	[number]	k4	3D
animace	animace	k1gFnPc4	animace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
používají	používat	k5eAaImIp3nP	používat
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
výstavy	výstava	k1gFnSc2	výstava
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
i	i	k8xC	i
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
"	"	kIx"	"
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
3D	[number]	k4	3D
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
obdobné	obdobný	k2eAgFnPc1d1	obdobná
výstavy	výstava	k1gFnPc1	výstava
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
New	New	k1gFnSc3	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
započetím	započetí	k1gNnSc7	započetí
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
výstavy	výstava	k1gFnSc2	výstava
se	s	k7c7	s
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
tři	tři	k4xCgFnPc4	tři
3D	[number]	k4	3D
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
Alte	alt	k1gInSc5	alt
Kongresshalle	Kongresshalle	k1gInSc1	Kongresshalle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
odehráli	odehrát	k5eAaPmAgMnP	odehrát
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ultra	ultra	k2eAgFnPc2d1	ultra
Music	Musice	k1gFnPc2	Musice
Festivalu	festival	k1gInSc3	festival
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
až	až	k9	až
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
skupina	skupina	k1gFnSc1	skupina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
osm	osm	k4xCc4	osm
3D	[number]	k4	3D
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
MoMA	MoMA	k1gFnSc1	MoMA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
zahráli	zahrát	k5eAaPmAgMnP	zahrát
všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
byl	být	k5eAaImAgInS	být
daný	daný	k2eAgInSc4d1	daný
koncert	koncert	k1gInSc4	koncert
věnován	věnovat	k5eAaImNgInS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
koncerty	koncert	k1gInPc7	koncert
až	až	k9	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
MoMA	MoMA	k1gMnPc2	MoMA
PS1	PS1	k1gFnPc2	PS1
probíhala	probíhat	k5eAaImAgFnS	probíhat
výstava	výstava	k1gFnSc1	výstava
Retrospective	Retrospectiv	k1gInSc5	Retrospectiv
1	[number]	k4	1
2	[number]	k4	2
3	[number]	k4	3
4	[number]	k4	4
5	[number]	k4	5
6	[number]	k4	6
7	[number]	k4	7
8	[number]	k4	8
mapující	mapující	k2eAgInPc1d1	mapující
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
40	[number]	k4	40
let	léto	k1gNnPc2	léto
tvorby	tvorba	k1gFnSc2	tvorba
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
newyorských	newyorský	k2eAgInPc2d1	newyorský
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
výstavy	výstava	k1gFnSc2	výstava
EMI	EMI	kA	EMI
vydalo	vydat	k5eAaPmAgNnS	vydat
limitovanou	limitovaný	k2eAgFnSc4d1	limitovaná
řadu	řada	k1gFnSc4	řada
edice	edice	k1gFnSc2	edice
The	The	k1gMnSc2	The
Catalogue	Catalogu	k1gMnSc2	Catalogu
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
přebalu	přebal	k1gInSc6	přebal
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
2	[number]	k4	2
tisíce	tisíc	k4xCgInSc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
zrušený	zrušený	k2eAgInSc4d1	zrušený
koncert	koncert	k1gInSc4	koncert
Björk	Björk	k1gInSc4	Björk
na	na	k7c4	na
Sónar	Sónar	k1gInSc4	Sónar
Festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
brazilském	brazilský	k2eAgMnSc6d1	brazilský
Sao	Sao	k1gMnSc6	Sao
Paulu	Paul	k1gMnSc6	Paul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
také	také	k9	také
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Tokyu	Tokyus	k1gInSc6	Tokyus
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
Göteborgu	Göteborg	k1gInSc6	Göteborg
a	a	k8xC	a
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Curychu	Curych	k1gInSc6	Curych
předvedli	předvést	k5eAaPmAgMnP	předvést
3D	[number]	k4	3D
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Ultra	ultra	k2eAgMnSc1d1	ultra
Music	Music	k1gMnSc1	Music
Festival	festival	k1gInSc1	festival
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
vystoupit	vystoupit	k5eAaPmF	vystoupit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
osm	osm	k4xCc4	osm
retrospektivních	retrospektivní	k2eAgFnPc2d1	retrospektivní
3D	[number]	k4	3D
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
zahráli	zahrát	k5eAaPmAgMnP	zahrát
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
diskografii	diskografie	k1gFnSc4	diskografie
naživo	naživo	k1gNnSc4	naživo
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
již	již	k6eAd1	již
nevystupoval	vystupovat	k5eNaImAgMnS	vystupovat
Stefan	Stefan	k1gMnSc1	Stefan
Pfaffe	Pfaff	k1gMnSc5	Pfaff
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Falk	Falk	k1gInSc1	Falk
Grieffenhagen	Grieffenhagen	k1gInSc1	Grieffenhagen
<g/>
,	,	kIx,	,
technik	technik	k1gMnSc1	technik
zajišťující	zajišťující	k2eAgFnSc2d1	zajišťující
vizuální	vizuální	k2eAgFnSc2d1	vizuální
projekce	projekce	k1gFnSc2	projekce
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
provázeny	provázet	k5eAaImNgInP	provázet
výstavou	výstava	k1gFnSc7	výstava
fotografií	fotografia	k1gFnPc2	fotografia
fotografa	fotograf	k1gMnSc2	fotograf
Petera	Peter	k1gMnSc2	Peter
Boettchera	Boettcher	k1gMnSc2	Boettcher
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
obrázkové	obrázkový	k2eAgFnSc6d1	obrázková
knize	kniha	k1gFnSc6	kniha
Kraftwerk-Roboter	Kraftwerk-Robotra	k1gFnPc2	Kraftwerk-Robotra
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
německé	německý	k2eAgFnSc2d1	německá
verze	verze	k1gFnSc2	verze
boxu	box	k1gInSc2	box
remasterovaných	remasterovaný	k2eAgFnPc2d1	remasterovaná
alb	alba	k1gFnPc2	alba
Der	drát	k5eAaImRp2nS	drát
Katalog	katalog	k1gInSc4	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
osmice	osmice	k1gFnSc1	osmice
retrospektivních	retrospektivní	k2eAgInPc2d1	retrospektivní
koncertů	koncert	k1gInPc2	koncert
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
také	také	k9	také
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
galerii	galerie	k1gFnSc6	galerie
Tate	Tate	k1gNnSc2	Tate
Modern	Moderna	k1gFnPc2	Moderna
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
i	i	k8xC	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
až	až	k8xS	až
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
festivalech	festival	k1gInPc6	festival
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
čtyři	čtyři	k4xCgInPc4	čtyři
koncerty	koncert	k1gInPc4	koncert
ve	v	k7c6	v
stylovém	stylový	k2eAgNnSc6d1	stylové
konferenčním	konferenční	k2eAgNnSc6d1	konferenční
centru	centrum	k1gNnSc6	centrum
Evoluon	Evoluona	k1gFnPc2	Evoluona
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Eindhovenu	Eindhoven	k2eAgFnSc4d1	Eindhoven
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
počet	počet	k1gInSc1	počet
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
padesátku	padesátka	k1gFnSc4	padesátka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
koncertování	koncertování	k1gNnSc4	koncertování
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
hodlají	hodlat	k5eAaImIp3nP	hodlat
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
taky	taky	k6eAd1	taky
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Bažant	Bažant	k1gMnSc1	Bažant
Pohoda	pohoda	k1gFnSc1	pohoda
Festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Trenčíně	Trenčín	k1gInSc6	Trenčín
<g/>
)	)	kIx)	)
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
svých	svůj	k3xOyFgInPc2	svůj
osm	osm	k4xCc4	osm
retrospektivních	retrospektivní	k2eAgInPc2d1	retrospektivní
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odprezentují	odprezentovat	k5eAaImIp3nP	odprezentovat
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
představí	představit	k5eAaPmIp3nS	představit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
Burgtheateru	Burgtheater	k1gInSc6	Burgtheater
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
si	se	k3xPyFc3	se
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
převezme	převzít	k5eAaPmIp3nS	převzít
ocenění	ocenění	k1gNnSc4	ocenění
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
práci	práce	k1gFnSc4	práce
skupiny	skupina	k1gFnSc2	skupina
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
např.	např.	kA	např.
s	s	k7c7	s
Beatles	Beatles	k1gFnSc7	Beatles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
sestava	sestava	k1gFnSc1	sestava
Ralf	Ralf	k1gMnSc1	Ralf
Hütter	Hütter	k1gMnSc1	Hütter
-	-	kIx~	-
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
-	-	kIx~	-
souč	souč	k6eAd1	souč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
;	;	kIx,	;
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
skupiny	skupina	k1gFnSc2	skupina
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
bicí	bicí	k2eAgMnSc1d1	bicí
Fritz	Fritz	k1gMnSc1	Fritz
Hilpert	Hilpert	k1gMnSc1	Hilpert
-	-	kIx~	-
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
-	-	kIx~	-
souč	souč	k6eAd1	souč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
,	,	kIx,	,
webmaster	webmaster	k1gInSc1	webmaster
<g />
.	.	kIx.	.
</s>
<s>
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
stránek	stránka	k1gFnPc2	stránka
skupiny	skupina	k1gFnSc2	skupina
Henning	Henning	k1gInSc1	Henning
Schmitz	Schmitz	k1gMnSc1	Schmitz
-	-	kIx~	-
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
-	-	kIx~	-
souč	souč	k6eAd1	souč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
Falk	Falka	k1gFnPc2	Falka
Grieffenhagen	Grieffenhagen	k1gInSc1	Grieffenhagen
-	-	kIx~	-
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
-	-	kIx~	-
souč	souč	k6eAd1	souč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
vizuální	vizuální	k2eAgFnSc1d1	vizuální
projekce	projekce	k1gFnSc1	projekce
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
tvorba	tvorba	k1gFnSc1	tvorba
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
hlavní	hlavní	k2eAgMnPc1d1	hlavní
členové	člen	k1gMnPc1	člen
Stefan	Stefan	k1gMnSc1	Stefan
Pfaffe	Pfaff	k1gInSc5	Pfaff
-	-	kIx~	-
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
souč	souč	k1gFnSc1	souč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
vizuální	vizuální	k2eAgFnSc1d1	vizuální
projekce	projekce	k1gFnSc1	projekce
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
Florian	Florian	k1gMnSc1	Florian
Schneider	Schneider	k1gMnSc1	Schneider
-	-	kIx~	-
Esleben	Esleben	k2eAgMnSc1d1	Esleben
-	-	kIx~	-
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
;	;	kIx,	;
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
skupiny	skupina	k1gFnSc2	skupina
housle	housle	k1gFnPc4	housle
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Flür	Flür	k1gMnSc1	Flür
-	-	kIx~	-
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
-	-	kIx~	-
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
elektronické	elektronický	k2eAgFnPc4d1	elektronická
<g />
.	.	kIx.	.
</s>
<s>
bicí	bicí	k2eAgMnSc1d1	bicí
Karl	Karl	k1gMnSc1	Karl
Bartos	Bartos	k1gMnSc1	Bartos
-	-	kIx~	-
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
elektronické	elektronický	k2eAgInPc1d1	elektronický
bicí	bicí	k2eAgInPc1d1	bicí
Externí	externí	k2eAgMnSc1d1	externí
pracovník	pracovník	k1gMnSc1	pracovník
Emil	Emil	k1gMnSc1	Emil
Schult	Schult	k1gMnSc1	Schult
-	-	kIx~	-
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
-	-	kIx~	-
souč	souč	k6eAd1	souč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
tvořící	tvořící	k2eAgInPc4d1	tvořící
obaly	obal	k1gInPc4	obal
CD	CD	kA	CD
a	a	k8xC	a
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
skupiny	skupina	k1gFnPc4	skupina
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
vedlejší	vedlejší	k2eAgMnPc1d1	vedlejší
členové	člen	k1gMnPc1	člen
Fernando	Fernanda	k1gFnSc5	Fernanda
Abrantes	Abrantes	k1gInSc4	Abrantes
-	-	kIx~	-
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
elektronické	elektronický	k2eAgInPc1d1	elektronický
bicí	bicí	k2eAgMnSc1d1	bicí
Klaus	Klaus	k1gMnSc1	Klaus
Röder	Röder	k1gMnSc1	Röder
-	-	kIx~	-
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnPc4d1	elektrická
housle	housle	k1gFnPc4	housle
Plato	Plato	k1gMnSc1	Plato
Kostic	kostice	k1gFnPc2	kostice
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
Peter	Peter	k1gMnSc1	Peter
Schmidt	Schmidt	k1gMnSc1	Schmidt
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
Michael	Michael	k1gMnSc1	Michael
Rother	Rothra	k1gFnPc2	Rothra
-	-	kIx~	-
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Houschäng	Houschäng	k1gMnSc1	Houschäng
Néjadepour	Néjadepour	k1gMnSc1	Néjadepour
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Klaus	Klaus	k1gMnSc1	Klaus
Dinger	Dinger	k1gMnSc1	Dinger
-	-	kIx~	-
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
bicí	bicí	k2eAgFnSc2d1	bicí
Charly	Charla	k1gFnSc2	Charla
Weiss	Weiss	k1gMnSc1	Weiss
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
Andreas	Andreas	k1gMnSc1	Andreas
Hohmann	Hohmann	k1gMnSc1	Hohmann
-	-	kIx~	-
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
Eberhard	Eberhard	k1gMnSc1	Eberhard
Kranemann	Kranemann	k1gMnSc1	Kranemann
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
světoznámých	světoznámý	k2eAgMnPc2d1	světoznámý
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
Depeche	Depeche	k1gFnSc1	Depeche
Mode	modus	k1gInSc5	modus
<g/>
,	,	kIx,	,
OMD	OMD	kA	OMD
(	(	kIx(	(
<g/>
Orchestral	Orchestral	k1gMnSc1	Orchestral
Manoeuvres	Manoeuvres	k1gMnSc1	Manoeuvres
in	in	k?	in
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Human	Human	k1gMnSc1	Human
League	Leagu	k1gFnSc2	Leagu
<g/>
,	,	kIx,	,
Gary	Gara	k1gFnSc2	Gara
Numan	Numana	k1gFnPc2	Numana
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obalu	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
Computer	computer	k1gInSc1	computer
World	Worldo	k1gNnPc2	Worldo
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
počítač	počítač	k1gInSc1	počítač
CASIO	CASIO	kA	CASIO
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
těchto	tento	k3xDgMnPc2	tento
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
monitorech	monitor	k1gInPc6	monitor
se	se	k3xPyFc4	se
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
určité	určitý	k2eAgFnSc2d1	určitá
kombinace	kombinace	k1gFnSc2	kombinace
kláves	klávesa	k1gFnPc2	klávesa
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
KRAFTWERK	KRAFTWERK	kA	KRAFTWERK
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
CASIO	CASIO	kA	CASIO
také	také	k9	také
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
s	s	k7c7	s
ozvučenými	ozvučený	k2eAgNnPc7d1	ozvučené
tlačítky	tlačítko	k1gNnPc7	tlačítko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
stistknuty	stistknut	k2eAgFnPc4d1	stistknut
určitá	určitý	k2eAgNnPc4d1	určité
tlačítka	tlačítko	k1gNnPc4	tlačítko
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zahrát	zahrát	k5eAaPmF	zahrát
melodie	melodie	k1gFnPc1	melodie
několika	několik	k4yIc2	několik
sladeb	sladba	k1gFnPc2	sladba
skupiny	skupina	k1gFnSc2	skupina
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
singl	singl	k1gInSc4	singl
Kohoutek-Kometenmelodie	Kohoutek-Kometenmelodie	k1gFnSc2	Kohoutek-Kometenmelodie
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
obou	dva	k4xCgFnPc2	dva
verzí	verze	k1gFnPc2	verze
na	na	k7c6	na
albu	album	k1gNnSc6	album
Autobahn	Autobahna	k1gFnPc2	Autobahna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
českém	český	k2eAgMnSc6d1	český
astronomovi	astronom	k1gMnSc6	astronom
Luboši	Luboš	k1gMnSc6	Luboš
Kohoutkovi	Kohoutek	k1gMnSc6	Kohoutek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1973	[number]	k4	1973
objevil	objevit	k5eAaPmAgInS	objevit
kometu	kometa	k1gFnSc4	kometa
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
E	E	kA	E
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
skupina	skupina	k1gFnSc1	skupina
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Hanse	Hans	k1gMnSc2	Hans
Zimmera	Zimmer	k1gMnSc2	Zimmer
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlivky	vysvětlivka	k1gFnPc1	vysvětlivka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
-	-	kIx~	-
<g/>
"	"	kIx"	"
=	=	kIx~	=
Neumístil	umístit	k5eNaPmAgMnS	umístit
se	se	k3xPyFc4	se
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
/	/	kIx~	/
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
=	=	kIx~	=
v	v	k7c6	v
oné	onen	k3xDgFnSc6	onen
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
zemi	zem	k1gFnSc6	zem
nevydáno	vydán	k2eNgNnSc1d1	nevydáno
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
=	=	kIx~	=
první	první	k4xOgMnSc1	první
místo	místo	k7c2	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
tučně	tučně	k6eAd1	tučně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
první	první	k4xOgFnSc1	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
kniha	kniha	k1gFnSc1	kniha
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
88	[number]	k4	88
<g/>
-stránkovou	tránkový	k2eAgFnSc4d1	-stránkový
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
turné	turné	k1gNnSc6	turné
2002	[number]	k4	2002
až	až	k9	až
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
luxusní	luxusní	k2eAgFnSc1d1	luxusní
edice	edice	k1gFnSc1	edice
alba	alba	k1gFnSc1	alba
Minimum-Maximum	Minimum-Maximum	k1gInSc4	Minimum-Maximum
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
"	"	kIx"	"
<g/>
Notebook	notebook	k1gInSc1	notebook
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
-	-	kIx~	-
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
:	:	kIx,	:
3D	[number]	k4	3D
2013	[number]	k4	2013
-	-	kIx~	-
Kraftwerk-Roboter	Kraftwerk-Roboter	k1gInSc1	Kraftwerk-Roboter
Některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
videoklipů	videoklip	k1gInPc2	videoklip
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
nikdy	nikdy	k6eAd1	nikdy
své	svůj	k3xOyFgInPc4	svůj
videoklipy	videoklip	k1gInPc4	videoklip
nevydali	vydat	k5eNaPmAgMnP	vydat
oficiální	oficiální	k2eAgFnSc7d1	oficiální
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
záznamy	záznam	k1gInPc1	záznam
šíří	šířit	k5eAaImIp3nP	šířit
většinou	většina	k1gFnSc7	většina
nelegálně	legálně	k6eNd1	legálně
v	v	k7c4	v
obvykle	obvykle	k6eAd1	obvykle
ne	ne	k9	ne
moc	moc	k6eAd1	moc
uspokojující	uspokojující	k2eAgFnSc3d1	uspokojující
kvalitě	kvalita	k1gFnSc3	kvalita
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
-	-	kIx~	-
Radioactivity	Radioactivita	k1gFnSc2	Radioactivita
1976	[number]	k4	1976
-	-	kIx~	-
Antenna	Antenn	k1gInSc2	Antenn
1977	[number]	k4	1977
-	-	kIx~	-
Trans-Europe	Trans-Europ	k1gInSc5	Trans-Europ
Express	express	k1gInSc1	express
-	-	kIx~	-
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
německá	německý	k2eAgFnSc1d1	německá
<g/>
/	/	kIx~	/
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
1978	[number]	k4	1978
-	-	kIx~	-
The	The	k1gFnSc1	The
Robots	Robots	k1gInSc1	Robots
1979	[number]	k4	1979
-	-	kIx~	-
Neonlights	Neonlights	k1gInSc1	Neonlights
1982	[number]	k4	1982
-	-	kIx~	-
Showroom	Showroom	k1gInSc1	Showroom
Dummies	Dummies	k1gInSc1	Dummies
1983	[number]	k4	1983
-	-	kIx~	-
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
-	-	kIx~	-
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
německá	německý	k2eAgFnSc1d1	německá
<g/>
/	/	kIx~	/
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
videu	video	k1gNnSc6	video
vystiupují	vystiupovat	k5eAaBmIp3nP	vystiupovat
členové	člen	k1gMnPc1	člen
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
jedoucí	jedoucí	k2eAgMnPc4d1	jedoucí
na	na	k7c6	na
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
-	-	kIx~	-
Music	Music	k1gMnSc1	Music
Non	Non	k1gFnSc2	Non
Stop	stop	k1gInSc1	stop
1986	[number]	k4	1986
-	-	kIx~	-
The	The	k1gMnSc5	The
Telephone	Telephon	k1gMnSc5	Telephon
Call	Call	k1gInSc1	Call
-	-	kIx~	-
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
německá	německý	k2eAgFnSc1d1	německá
<g/>
/	/	kIx~	/
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
1991	[number]	k4	1991
-	-	kIx~	-
The	The	k1gFnSc1	The
Robots	Robotsa	k1gFnPc2	Robotsa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
version	version	k1gInSc1	version
<g/>
)	)	kIx)	)
-	-	kIx~	-
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
německá	německý	k2eAgFnSc1d1	německá
<g/>
/	/	kIx~	/
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
1993	[number]	k4	1993
-	-	kIx~	-
Sellafield	Sellafield	k1gInSc1	Sellafield
2	[number]	k4	2
-	-	kIx~	-
video-shot	videohot	k1gInSc4	video-shot
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
REACT	REACT	kA	REACT
organizovanou	organizovaný	k2eAgFnSc7d1	organizovaná
společností	společnost	k1gFnSc7	společnost
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
-	-	kIx~	-
Luton	Luton	k1gNnSc1	Luton
Hoo	Hoo	k1gFnPc2	Hoo
-	-	kIx~	-
záznam	záznam	k1gInSc4	záznam
pořízený	pořízený	k2eAgInSc4d1	pořízený
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
Luton	Luton	k1gNnSc4	Luton
Hoo	Hoo	k1gFnSc2	Hoo
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
byl	být	k5eAaImAgInS	být
propagačně	propagačně	k6eAd1	propagačně
vysílán	vysílat	k5eAaImNgInS	vysílat
televizními	televizní	k2eAgFnPc7d1	televizní
stanicemi	stanice	k1gFnPc7	stanice
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
-	-	kIx~	-
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc4	Franc
-	-	kIx~	-
jiný	jiný	k2eAgInSc4d1	jiný
videoklip	videoklip	k1gInSc4	videoklip
-	-	kIx~	-
sestřihy	sestřih	k1gInPc4	sestřih
ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
záznamů	záznam	k1gInPc2	záznam
ze	z	k7c2	z
závodů	závod	k1gInPc2	závod
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
-	-	kIx~	-
Expo	Expo	k1gNnSc1	Expo
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Rolando	Rolanda	k1gFnSc5	Rolanda
Remix	Remix	k1gInSc1	Remix
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Tour	Tour	k1gInSc1	Tour
<g />
.	.	kIx.	.
</s>
<s>
de	de	k?	de
France	Franc	k1gMnPc4	Franc
2003	[number]	k4	2003
2005	[number]	k4	2005
-	-	kIx~	-
Minimum	minimum	k1gNnSc1	minimum
Maximum	maximum	k1gNnSc1	maximum
-	-	kIx~	-
promo	promo	k6eAd1	promo
video	video	k1gNnSc1	video
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
na	na	k7c4	na
Myspace	Myspace	k1gFnPc4	Myspace
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
České	český	k2eAgFnPc1d1	Česká
neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Neoficiální	neoficiální	k2eAgFnPc1d1	neoficiální
stránky	stránka	k1gFnPc1	stránka
1	[number]	k4	1
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Neoficiální	oficiální	k2eNgFnPc4d1	neoficiální
stránky	stránka	k1gFnPc4	stránka
2	[number]	k4	2
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Neoficiální	oficiální	k2eNgFnPc4d1	neoficiální
stránky	stránka	k1gFnPc4	stránka
3	[number]	k4	3
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Minimum-Maximum	Minimum-Maximum	k1gInSc1	Minimum-Maximum
Notebook	notebook	k1gInSc1	notebook
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Listings	Listings	k1gInSc1	Listings
of	of	k?	of
all	all	k?	all
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
albums	albumsa	k1gFnPc2	albumsa
Music	Music	k1gMnSc1	Music
Sampler	Sampler	k1gMnSc1	Sampler
</s>
