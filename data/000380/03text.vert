<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Emil	Emil	k1gMnSc1	Emil
Frida	Frida	k1gMnSc1	Frida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1853	[number]	k4	1853
Louny	Louny	k1gInPc4	Louny
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1912	[number]	k4	1912
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
103	[number]	k4	103
v	v	k7c6	v
Lounech	Louny	k1gInPc6	Louny
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
Jakuba	Jakub	k1gMnSc2	Jakub
Fridy	Frida	k1gFnSc2	Frida
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Kolářové	Kolářová	k1gFnSc2	Kolářová
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
po	po	k7c6	po
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
do	do	k7c2	do
Slaného	Slaný	k1gInSc2	Slaný
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Frida	Frida	k1gMnSc1	Frida
žil	žít	k5eAaImAgMnS	žít
do	do	k7c2	do
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
<g/>
,	,	kIx,	,
faráře	farář	k1gMnSc2	farář
Antonína	Antonín	k1gMnSc2	Antonín
Koláře	Kolář	k1gMnSc2	Kolář
v	v	k7c6	v
Ovčárech	Ovčáry	k1gInPc6	Ovčáry
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
první	první	k4xOgInPc4	první
ročníky	ročník	k1gInPc4	ročník
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
(	(	kIx(	(
<g/>
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
<g/>
)	)	kIx)	)
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc4	gymnázium
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Slaném	Slaný	k1gInSc6	Slaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
spolužákem	spolužák	k1gMnSc7	spolužák
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
Třebízský	Třebízský	k1gMnSc1	Třebízský
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
maturoval	maturovat	k5eAaBmAgMnS	maturovat
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
<g/>
.	.	kIx.	.
</s>
<s>
Veden	veden	k2eAgInSc1d1	veden
strýcovým	strýcův	k2eAgInSc7d1	strýcův
příkladem	příklad	k1gInSc7	příklad
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
do	do	k7c2	do
pražského	pražský	k2eAgInSc2d1	pražský
Arcibiskupského	arcibiskupský	k2eAgInSc2d1	arcibiskupský
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
ale	ale	k8xC	ale
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
dějepis	dějepis	k1gInSc4	dějepis
<g/>
,	,	kIx,	,
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
románskou	románský	k2eAgFnSc4d1	románská
filologii	filologie	k1gFnSc4	filologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
studií	studie	k1gFnPc2	studie
učil	učít	k5eAaPmAgMnS	učít
češtinu	čeština	k1gFnSc4	čeština
francouzského	francouzský	k2eAgMnSc2d1	francouzský
historika	historik	k1gMnSc2	historik
Ernesta	Ernest	k1gMnSc4	Ernest
Denise	Denisa	k1gFnSc6	Denisa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
zprostředkoval	zprostředkovat	k5eAaPmAgInS	zprostředkovat
první	první	k4xOgInPc4	první
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
literární	literární	k2eAgFnPc1d1	literární
práce	práce	k1gFnPc1	práce
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
tisku	tisk	k1gInSc2	tisk
zprostředkovala	zprostředkovat	k5eAaPmAgFnS	zprostředkovat
redaktorka	redaktorka	k1gFnSc1	redaktorka
Sofie	Sofia	k1gFnSc2	Sofia
Podlipská	Podlipský	k2eAgFnSc1d1	Podlipská
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
podle	podle	k7c2	podle
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
korespondence	korespondence	k1gFnSc2	korespondence
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Podlipská	Podlipský	k2eAgFnSc1d1	Podlipská
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc1d2	starší
a	a	k8xC	a
moudře	moudřit	k5eAaImSgInS	moudřit
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
životnímu	životní	k2eAgInSc3d1	životní
vztahu	vztah	k1gInSc3	vztah
i	i	k8xC	i
sňatku	sňatek	k1gInSc3	sňatek
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
svou	svůj	k3xOyFgFnSc7	svůj
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
patnáctiletou	patnáctiletý	k2eAgFnSc4d1	patnáctiletá
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Ludmilu	Ludmila	k1gFnSc4	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
navázal	navázat	k5eAaPmAgInS	navázat
přátelství	přátelství	k1gNnSc3	přátelství
např.	např.	kA	např.
se	s	k7c7	s
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Winterem	Winter	k1gMnSc7	Winter
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Václavem	Václav	k1gMnSc7	Václav
Sládkem	Sládek	k1gMnSc7	Sládek
a	a	k8xC	a
Aloisem	Alois	k1gMnSc7	Alois
Jiráskem	Jirásek	k1gMnSc7	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Lumírovců	lumírovec	k1gMnPc2	lumírovec
(	(	kIx(	(
<g/>
Jirásek	Jirásek	k1gMnSc1	Jirásek
záhy	záhy	k6eAd1	záhy
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
Ruchovcům	ruchovec	k1gMnPc3	ruchovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
literát	literát	k1gMnSc1	literát
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
užíval	užívat	k5eAaImAgInS	užívat
slova	slovo	k1gNnPc1	slovo
pábit	pábit	k5eAaBmF	pábit
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
odcházel	odcházet	k5eAaImAgMnS	odcházet
zakouřit	zakouřit	k5eAaPmF	zakouřit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
později	pozdě	k6eAd2	pozdě
posunul	posunout	k5eAaPmAgMnS	posunout
dále	daleko	k6eAd2	daleko
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
(	(	kIx(	(
<g/>
Pábitelé	pábitel	k1gMnPc1	pábitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
tajemník	tajemník	k1gMnSc1	tajemník
a	a	k8xC	a
vychovatel	vychovatel	k1gMnSc1	vychovatel
synů	syn	k1gMnPc2	syn
hraběcí	hraběcí	k2eAgFnSc2d1	hraběcí
rodiny	rodina	k1gFnSc2	rodina
Montecuccoli-Laderchi	Montecuccoli-Laderchi	k1gNnSc2	Montecuccoli-Laderchi
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Meranu	Meran	k1gInSc6	Meran
nad	nad	k7c7	nad
Panarem	Panar	k1gInSc7	Panar
u	u	k7c2	u
Modeny	Modena	k1gFnSc2	Modena
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Livornu	Livorno	k1gNnSc6	Livorno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
učitelském	učitelský	k2eAgInSc6d1	učitelský
ústavu	ústav	k1gInSc6	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
Panské	panský	k2eAgFnSc6d1	Panská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
vyučování	vyučování	k1gNnSc1	vyučování
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
nebavilo	bavit	k5eNaImAgNnS	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
hraběte	hrabě	k1gMnSc2	hrabě
Montecucoliho	Montecucoli	k1gMnSc2	Montecucoli
u	u	k7c2	u
Leopolda	Leopold	k1gMnSc2	Leopold
Lva	Lev	k1gMnSc2	Lev
Thuna-Hohensteina	Thuna-Hohenstein	k2eAgMnSc2d1	Thuna-Hohenstein
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
jmenován	jmenován	k2eAgMnSc1d1	jmenován
tajemníkem	tajemník	k1gMnSc7	tajemník
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
české	český	k2eAgFnSc6d1	Česká
polytechnice	polytechnika	k1gFnSc6	polytechnika
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
moderních	moderní	k2eAgFnPc2d1	moderní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Dvořákem	Dvořák	k1gMnSc7	Dvořák
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
povýšil	povýšit	k5eAaPmAgInS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
jej	on	k3xPp3gMnSc4	on
členem	člen	k1gMnSc7	člen
Panské	panský	k2eAgFnSc2d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
zde	zde	k6eAd1	zde
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
a	a	k8xC	a
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
požadavek	požadavek	k1gInSc4	požadavek
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
hlasovacího	hlasovací	k2eAgNnSc2d1	hlasovací
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
nesmírně	smírně	k6eNd1	smírně
činný	činný	k2eAgMnSc1d1	činný
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
polského	polský	k2eAgNnSc2d1	polské
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
také	také	k9	také
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
mnoha	mnoho	k4c2	mnoho
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
evropské	evropský	k2eAgFnSc2d1	Evropská
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
císařem	císař	k1gMnSc7	císař
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
byl	být	k5eAaImAgMnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Pseudonym	pseudonym	k1gInSc1	pseudonym
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
spolužáka	spolužák	k1gMnSc2	spolužák
z	z	k7c2	z
klatovského	klatovský	k2eAgNnSc2d1	Klatovské
gymnázia	gymnázium	k1gNnSc2	gymnázium
Josefa	Josef	k1gMnSc2	Josef
Thomayera	Thomayer	k1gMnSc2	Thomayer
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
studenti	student	k1gMnPc1	student
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
navštívili	navštívit	k5eAaPmAgMnP	navštívit
rozkvetlé	rozkvetlý	k2eAgNnSc4d1	rozkvetlé
údolí	údolí	k1gNnSc4	údolí
řeky	řeka	k1gFnSc2	řeka
Vrchlice	vrchlice	k1gFnSc2	vrchlice
u	u	k7c2	u
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
jarní	jarní	k2eAgFnSc7d1	jarní
scenérií	scenérie	k1gFnSc7	scenérie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
pseudonym	pseudonym	k1gInSc4	pseudonym
dříve	dříve	k6eAd2	dříve
používal	používat	k5eAaImAgMnS	používat
kutnohorský	kutnohorský	k2eAgMnSc1d1	kutnohorský
básník	básník	k1gMnSc1	básník
Josef	Josef	k1gMnSc1	Josef
Jelínek	Jelínek	k1gMnSc1	Jelínek
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Vrchlice	vrchlice	k1gFnSc2	vrchlice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
vytesal	vytesat	k5eAaPmAgMnS	vytesat
kutnohorský	kutnohorský	k2eAgMnSc1d1	kutnohorský
sochař	sochař	k1gMnSc1	sochař
Josef	Josef	k1gMnSc1	Josef
Chvojan	Chvojan	k1gMnSc1	Chvojan
do	do	k7c2	do
pískovcové	pískovcový	k2eAgFnSc2d1	pískovcová
skály	skála	k1gFnSc2	skála
Vrchlického	Vrchlického	k2eAgInSc1d1	Vrchlického
reliéf	reliéf	k1gInSc1	reliéf
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
přes	přes	k7c4	přes
12	[number]	k4	12
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kolem	kolem	k7c2	kolem
270	[number]	k4	270
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
přes	přes	k7c4	přes
80	[number]	k4	80
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
a	a	k8xC	a
50	[number]	k4	50
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
básnické	básnický	k2eAgFnPc1d1	básnická
sbírky	sbírka	k1gFnPc1	sbírka
a	a	k8xC	a
dramata	drama	k1gNnPc1	drama
jsou	být	k5eAaImIp3nP	být
vrcholnou	vrcholný	k2eAgFnSc7d1	vrcholná
ukázkou	ukázka	k1gFnSc7	ukázka
formální	formální	k2eAgFnSc2d1	formální
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
poměrně	poměrně	k6eAd1	poměrně
čtivé	čtivý	k2eAgFnPc1d1	čtivá
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vše	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Pozdějšími	pozdní	k2eAgFnPc7d2	pozdější
generacemi	generace	k1gFnPc7	generace
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
generací	generace	k1gFnSc7	generace
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Vrchlického	Vrchlického	k2eAgFnSc1d1	Vrchlického
tvorba	tvorba	k1gFnSc1	tvorba
často	často	k6eAd1	často
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
a	a	k8xC	a
odsuzována	odsuzovat	k5eAaImNgFnS	odsuzovat
za	za	k7c4	za
mnohomluvnost	mnohomluvnost	k1gFnSc4	mnohomluvnost
<g/>
,	,	kIx,	,
patos	patos	k1gInSc4	patos
<g/>
,	,	kIx,	,
samoúčelnou	samoúčelný	k2eAgFnSc4d1	samoúčelná
rétoriku	rétorika	k1gFnSc4	rétorika
a	a	k8xC	a
šroubovanou	šroubovaný	k2eAgFnSc4d1	šroubovaná
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
dílu	dílo	k1gNnSc3	dílo
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
například	například	k6eAd1	například
také	také	k9	také
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
starší	starší	k1gMnSc1	starší
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
Vrchlického	Vrchlický	k1gMnSc4	Vrchlický
uznával	uznávat	k5eAaImAgMnS	uznávat
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
dobrého	dobrý	k2eAgMnSc4d1	dobrý
překladatele	překladatel	k1gMnSc4	překladatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
F.	F.	kA	F.
X.	X.	kA	X.
Šalda	Šalda	k1gMnSc1	Šalda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
básníků	básník	k1gMnPc2	básník
jako	jako	k8xS	jako
např.	např.	kA	např.
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Blatný	blatný	k2eAgMnSc1d1	blatný
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
kvalitu	kvalita	k1gFnSc4	kvalita
jeho	on	k3xPp3gNnSc2	on
básnického	básnický	k2eAgNnSc2d1	básnické
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
svým	svůj	k3xOyFgMnPc3	svůj
ideově	ideově	k6eAd1	ideově
bohatým	bohatý	k2eAgInSc7d1	bohatý
dílem	díl	k1gInSc7	díl
připravil	připravit	k5eAaPmAgInS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
symbolismus	symbolismus	k1gInSc4	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
vrcholnému	vrcholný	k2eAgNnSc3d1	vrcholné
evropskému	evropský	k2eAgNnSc3d1	Evropské
básnictví	básnictví	k1gNnSc3	básnictví
a	a	k8xC	a
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
ztělesňovala	ztělesňovat	k5eAaImAgFnS	ztělesňovat
harmonický	harmonický	k2eAgInSc4d1	harmonický
ideál	ideál	k1gInSc4	ideál
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
historickými	historický	k2eAgInPc7d1	historický
náměty	námět	k1gInPc7	námět
<g/>
,	,	kIx,	,
vývojem	vývoj	k1gInSc7	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
mj.	mj.	kA	mj.
z	z	k7c2	z
Legendy	legenda	k1gFnSc2	legenda
věků	věk	k1gInPc2	věk
od	od	k7c2	od
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
mapování	mapování	k1gNnSc6	mapování
historie	historie	k1gFnSc2	historie
nacházel	nacházet	k5eAaImAgMnS	nacházet
největší	veliký	k2eAgFnSc4d3	veliký
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
lyrice	lyrika	k1gFnSc6	lyrika
<g/>
.	.	kIx.	.
</s>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
-	-	kIx~	-
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Sofii	Sofia	k1gFnSc4	Sofia
Podlipské	Podlipský	k2eAgNnSc1d1	Podlipské
Z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
-	-	kIx~	-
především	především	k9	především
milostná	milostný	k2eAgFnSc1d1	milostná
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
;	;	kIx,	;
básně	báseň	k1gFnPc1	báseň
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
příběh	příběh	k1gInSc1	příběh
-	-	kIx~	-
z	z	k7c2	z
pocitů	pocit	k1gInPc2	pocit
nenávratné	návratný	k2eNgFnSc2d1	nenávratná
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
všeho	všecek	k3xTgNnSc2	všecek
opět	opět	k6eAd1	opět
probleskuje	probleskovat	k5eAaImIp3nS	probleskovat
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc1d1	spočívající
ve	v	k7c6	v
šťastných	šťastný	k2eAgFnPc6d1	šťastná
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
<g/>
,	,	kIx,	,
zklamání	zklamání	k1gNnSc6	zklamání
a	a	k8xC	a
konečného	konečný	k2eAgNnSc2d1	konečné
uklidnění	uklidnění	k1gNnSc2	uklidnění
v	v	k7c6	v
resignaci	resignace	k1gFnSc6	resignace
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
;	;	kIx,	;
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
popisů	popis	k1gInPc2	popis
svých	svůj	k3xOyFgInPc2	svůj
pocitů	pocit	k1gInPc2	pocit
využívá	využívat	k5eAaPmIp3nS	využívat
básník	básník	k1gMnSc1	básník
přírodní	přírodní	k2eAgFnSc4d1	přírodní
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejčastějším	častý	k2eAgInPc3d3	nejčastější
motivům	motiv	k1gInPc3	motiv
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
propast	propast	k1gFnSc1	propast
Rok	rok	k1gInSc1	rok
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
-	-	kIx~	-
přírodní	přírodní	k2eAgFnSc1d1	přírodní
a	a	k8xC	a
milostná	milostný	k2eAgFnSc1d1	milostná
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
dojmy	dojem	k1gInPc1	dojem
z	z	k7c2	z
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
Dědictví	dědictví	k1gNnSc4	dědictví
Tantalovo	Tantalův	k2eAgNnSc4d1	Tantalovo
Brevíř	brevíř	k1gInSc4	brevíř
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
Eklogy	ekloga	k1gFnSc2	ekloga
a	a	k8xC	a
písně	píseň	k1gFnSc2	píseň
-	-	kIx~	-
anticky	anticky	k6eAd1	anticky
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
<g />
.	.	kIx.	.
</s>
<s>
přírodní	přírodní	k2eAgFnSc1d1	přírodní
a	a	k8xC	a
milostná	milostný	k2eAgFnSc1d1	milostná
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
melodické	melodický	k2eAgFnPc1d1	melodická
a	a	k8xC	a
bohatě	bohatě	k6eAd1	bohatě
využívají	využívat	k5eAaImIp3nP	využívat
eufonie	eufonie	k1gFnPc1	eufonie
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgInP	psát
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dialogu	dialog	k1gInSc2	dialog
Poutí	poutí	k1gNnSc4	poutí
k	k	k7c3	k
Eldorádu	Eldorádo	k1gNnSc3	Eldorádo
-	-	kIx~	-
milostná	milostný	k2eAgFnSc1d1	milostná
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc1d1	přírodní
lyrika	lyrika	k1gFnSc1	lyrika
Dojmy	dojem	k1gInPc4	dojem
a	a	k8xC	a
rozmary	rozmar	k1gInPc4	rozmar
-	-	kIx~	-
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
lidem	člověk	k1gMnPc3	člověk
Hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
Moje	můj	k3xOp1gFnSc1	můj
sonáta	sonáta	k1gFnSc1	sonáta
Zlatý	zlatý	k2eAgInSc4d1	zlatý
prach	prach	k1gInSc4	prach
Okna	okno	k1gNnSc2	okno
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
-	-	kIx~	-
psáno	psát	k5eAaImNgNnS	psát
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
(	(	kIx(	(
<g/>
jednak	jednak	k8xC	jednak
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
-	-	kIx~	-
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
-	-	kIx~	-
probíhal	probíhat	k5eAaImAgInS	probíhat
spor	spor	k1gInSc1	spor
s	s	k7c7	s
ruchovci	ruchovec	k1gMnPc7	ruchovec
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
procházel	procházet	k5eAaImAgMnS	procházet
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
manželskou	manželský	k2eAgFnSc7d1	manželská
krizí	krize	k1gFnSc7	krize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
<g/>
,	,	kIx,	,
rezignace	rezignace	k1gFnSc1	rezignace
<g/>
,	,	kIx,	,
samota	samota	k1gFnSc1	samota
<g/>
,	,	kIx,	,
formalistní	formalistní	k2eAgFnSc1d1	formalistní
-	-	kIx~	-
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
různé	různý	k2eAgFnPc4d1	různá
<g />
.	.	kIx.	.
</s>
<s>
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
<g/>
,	,	kIx,	,
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
<g/>
;	;	kIx,	;
báseň	báseň	k1gFnSc4	báseň
Za	za	k7c4	za
trochu	trocha	k1gFnSc4	trocha
lásky	láska	k1gFnSc2	láska
Písně	píseň	k1gFnSc2	píseň
poutníka	poutník	k1gMnSc2	poutník
-	-	kIx~	-
psáno	psát	k5eAaImNgNnS	psát
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xC	jako
Okna	okno	k1gNnPc1	okno
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
Pavučiny	pavučina	k1gFnSc2	pavučina
-	-	kIx~	-
psáno	psát	k5eAaImNgNnS	psát
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xC	jako
Okna	okno	k1gNnPc1	okno
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
Korálové	korálový	k2eAgInPc4d1	korálový
ostrovy	ostrov	k1gInPc4	ostrov
Strom	strom	k1gInSc1	strom
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
Co	co	k3yInSc1	co
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
<g/>
)	)	kIx)	)
-	-	kIx~	-
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
<g />
.	.	kIx.	.
</s>
<s>
lyrika	lyrika	k1gFnSc1	lyrika
Co	co	k3yRnSc4	co
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
haluzích	haluz	k1gFnPc6	haluz
(	(	kIx(	(
<g/>
Co	co	k3yInSc1	co
zní	znět	k5eAaImIp3nS	znět
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
<g/>
)	)	kIx)	)
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc1	vlast
-	-	kIx~	-
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
lyrika	lyrika	k1gFnSc1	lyrika
Mýty	mýtus	k1gInPc1	mýtus
-	-	kIx~	-
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Prokopovi	Prokop	k1gMnSc6	Prokop
Sonety	sonet	k1gInPc4	sonet
samotáře	samotář	k1gMnSc2	samotář
-	-	kIx~	-
přírodní	přírodní	k2eAgFnSc1d1	přírodní
lyrika	lyrika	k1gFnSc1	lyrika
Démon	démon	k1gMnSc1	démon
láska	láska	k1gFnSc1	láska
Dni	den	k1gInSc6	den
a	a	k8xC	a
noci	noc	k1gFnSc6	noc
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
a	a	k8xC	a
reflexivní	reflexivní	k2eAgFnSc1d1	reflexivní
lyrika	lyrika	k1gFnSc1	lyrika
psaná	psaný	k2eAgFnSc1d1	psaná
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
a	a	k8xC	a
melodickou	melodický	k2eAgFnSc7d1	melodická
písňovou	písňový	k2eAgFnSc7d1	písňová
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
tematicky	tematicky	k6eAd1	tematicky
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
,	,	kIx,	,
uvažující	uvažující	k2eAgFnSc1d1	uvažující
o	o	k7c6	o
základní	základní	k2eAgFnSc6d1	základní
životní	životní	k2eAgFnSc6d1	životní
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
přírodní	přírodní	k2eAgFnSc4d1	přírodní
metaforiku	metaforika	k1gFnSc4	metaforika
<g/>
.	.	kIx.	.
</s>
<s>
Skvrny	skvrna	k1gFnPc1	skvrna
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
Píseň	píseň	k1gFnSc4	píseň
klasů	klas	k1gInPc2	klas
-	-	kIx~	-
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
klasy	klasa	k1gFnPc1	klasa
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
životě	život	k1gInSc6	život
chudých	chudý	k1gMnPc2	chudý
venkovských	venkovský	k2eAgMnPc2d1	venkovský
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
popisuje	popisovat	k5eAaImIp3nS	popisovat
zde	zde	k6eAd1	zde
jejich	jejich	k3xOp3gFnSc4	jejich
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
Modlitba	modlitba	k1gFnSc1	modlitba
na	na	k7c6	na
Řípu	Říp	k1gInSc6	Říp
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
básni	báseň	k1gFnSc6	báseň
se	se	k3xPyFc4	se
sedláci	sedlák	k1gMnPc1	sedlák
modlí	modlit	k5eAaImIp3nP	modlit
k	k	k7c3	k
dědu	děd	k1gMnSc3	děd
Čechovi	Čech	k1gMnSc3	Čech
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dal	dát	k5eAaPmAgMnS	dát
otcům	otec	k1gMnPc3	otec
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
dětem	dítě	k1gFnPc3	dítě
dobrý	dobrý	k2eAgInSc4d1	dobrý
život	život	k1gInSc4	život
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
aby	aby	kYmCp3nS	aby
svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
ochránil	ochránit	k5eAaPmAgMnS	ochránit
českou	český	k2eAgFnSc4d1	Česká
zemi	zem	k1gFnSc4	zem
Meč	meč	k1gInSc1	meč
Damoklův	Damoklův	k2eAgInSc1d1	Damoklův
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
;	;	kIx,	;
hořká	hořký	k2eAgFnSc1d1	hořká
reflexivní	reflexivní	k2eAgFnSc1d1	reflexivní
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
pochmurně	pochmurně	k6eAd1	pochmurně
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
blížící	blížící	k2eAgFnPc4d1	blížící
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
častá	častý	k2eAgFnSc1d1	častá
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
sebeironizace	sebeironizace	k1gFnSc2	sebeironizace
<g/>
;	;	kIx,	;
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
posmrtně	posmrtně	k6eAd1	posmrtně
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
básní	báseň	k1gFnPc2	báseň
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
náčrtků	náčrtek	k1gInPc2	náčrtek
Zlomky	zlomek	k1gInPc4	zlomek
epopeje	epopeje	k1gFnSc1	epopeje
-	-	kIx~	-
cyklus	cyklus	k1gInSc1	cyklus
epických	epický	k2eAgFnPc2d1	epická
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
lyricko	lyricko	k1gNnSc1	lyricko
epických	epický	k2eAgFnPc2d1	epická
básní	báseň	k1gFnPc2	báseň
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
téma	téma	k1gNnSc1	téma
-	-	kIx~	-
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
vývoj	vývoj	k1gInSc1	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
s	s	k7c7	s
historickými	historický	k2eAgInPc7d1	historický
náměty	námět	k1gInPc7	námět
<g/>
,	,	kIx,	,
optimistický	optimistický	k2eAgInSc1d1	optimistický
náhled	náhled	k1gInSc1	náhled
<g/>
,	,	kIx,	,
lidstvo	lidstvo	k1gNnSc1	lidstvo
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
k	k	k7c3	k
humanitě	humanita	k1gFnSc3	humanita
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
:	:	kIx,	:
Duch	duch	k1gMnSc1	duch
a	a	k8xC	a
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Selské	selský	k2eAgFnPc1d1	selská
balady	balada	k1gFnPc1	balada
<g/>
,	,	kIx,	,
Bar	bar	k1gInSc1	bar
Kochba	Kochba	k1gMnSc1	Kochba
Duch	duch	k1gMnSc1	duch
a	a	k8xC	a
svět	svět	k1gInSc1	svět
-	-	kIx~	-
kolísání	kolísání	k1gNnSc1	kolísání
mezi	mezi	k7c7	mezi
<g />
.	.	kIx.	.
</s>
<s>
optimismem	optimismus	k1gInSc7	optimismus
a	a	k8xC	a
pesimismem	pesimismus	k1gInSc7	pesimismus
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
zde	zde	k6eAd1	zde
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
rozum	rozum	k1gInSc1	rozum
<g/>
,	,	kIx,	,
starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
optimismu	optimismus	k1gInSc2	optimismus
<g/>
;	;	kIx,	;
volá	volat	k5eAaImIp3nS	volat
po	po	k7c6	po
harmonii	harmonie	k1gFnSc6	harmonie
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
,	,	kIx,	,
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
lidské	lidský	k2eAgNnSc4d1	lidské
poznání	poznání	k1gNnSc4	poznání
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
rozumu	rozum	k1gInSc2	rozum
Selské	selský	k2eAgInPc4d1	selský
ballady	ballad	k1gInPc4	ballad
-	-	kIx~	-
po	po	k7c6	po
formální	formální	k2eAgFnSc6d1	formální
stránce	stránka	k1gFnSc6	stránka
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
a	a	k8xC	a
melodické	melodický	k2eAgFnSc2d1	melodická
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
vykazující	vykazující	k2eAgFnSc1d1	vykazující
návaznost	návaznost	k1gFnSc1	návaznost
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
českou	český	k2eAgFnSc4d1	Česká
lidovou	lidový	k2eAgFnSc4d1	lidová
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
selské	selský	k2eAgFnSc2d1	selská
epiky	epika	k1gFnSc2	epika
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nevolnické	nevolnický	k2eAgFnSc2d1	nevolnická
bídy	bída	k1gFnSc2	bída
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
příběhů	příběh	k1gInPc2	příběh
lidových	lidový	k2eAgMnPc2d1	lidový
hrdinů	hrdina	k1gMnPc2	hrdina
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
<g/>
,	,	kIx,	,
Matyáš	Matyáš	k1gMnSc1	Matyáš
Ulický	Ulický	k1gMnSc1	Ulický
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
naturalistické	naturalistický	k2eAgInPc1d1	naturalistický
až	až	k8xS	až
drastické	drastický	k2eAgInPc1d1	drastický
popisy	popis	k1gInPc1	popis
vrchnostenské	vrchnostenský	k2eAgFnSc2d1	vrchnostenská
krutosti	krutost	k1gFnSc2	krutost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
básně	báseň	k1gFnPc4	báseň
Ballada	Ballada	k1gFnSc1	Ballada
vánoční	vánoční	k2eAgFnSc1d1	vánoční
<g/>
,	,	kIx,	,
Matyáš	Matyáš	k1gMnSc1	Matyáš
Ulický	Ulický	k1gMnSc1	Ulický
<g/>
)	)	kIx)	)
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
výjevy	výjev	k1gInPc1	výjev
dostli	dostle	k1gFnSc4	dostle
brutální	brutální	k2eAgFnSc2d1	brutální
msty	msta	k1gFnSc2	msta
poddaných	poddaný	k1gMnPc2	poddaný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
báseň	báseň	k1gFnSc1	báseň
Selské	selský	k2eAgNnSc4d1	selské
kázání	kázání	k1gNnSc4	kázání
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
Lorecký	Lorecký	k2eAgMnSc1d1	Lorecký
ze	z	k7c2	z
Lkouše	Lkouše	k1gFnSc2	Lkouše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
historicky	historicky	k6eAd1	historicky
málo	málo	k6eAd1	málo
přesné	přesný	k2eAgFnPc1d1	přesná
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
si	se	k3xPyFc3	se
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
upravuje	upravovat	k5eAaImIp3nS	upravovat
a	a	k8xC	a
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc4d1	Nové
zlomky	zlomek	k1gInPc4	zlomek
epopeje	epopeje	k1gFnSc1	epopeje
Kytka	kytka	k1gFnSc1	kytka
balad	balada	k1gFnPc2	balada
Fresky	freska	k1gFnSc2	freska
a	a	k8xC	a
gobelíny	gobelín	k1gInPc4	gobelín
Epické	epický	k2eAgFnSc2d1	epická
básně	báseň	k1gFnSc2	báseň
-	-	kIx~	-
poetické	poetický	k2eAgFnPc1d1	poetická
momentky	momentka	k1gFnPc1	momentka
ze	z	k7c2	z
slavných	slavný	k2eAgInPc2d1	slavný
příběhů	příběh	k1gInPc2	příběh
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
původní	původní	k2eAgFnSc2d1	původní
<g/>
,	,	kIx,	,
zasazené	zasazený	k2eAgFnSc2d1	zasazená
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
reálií	reálie	k1gFnPc2	reálie
(	(	kIx(	(
<g/>
častí	častit	k5eAaImIp3nS	častit
jsou	být	k5eAaImIp3nP	být
mj.	mj.	kA	mj.
uherští	uherský	k2eAgMnPc1d1	uherský
Cikáni	cikán	k1gMnPc1	cikán
<g/>
,	,	kIx,	,
severská	severský	k2eAgFnSc1d1	severská
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
antická	antický	k2eAgFnSc1d1	antická
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
<g />
.	.	kIx.	.
</s>
<s>
sbírky	sbírka	k1gFnPc4	sbírka
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
mnoha	mnoho	k4c2	mnoho
kratších	krátký	k2eAgFnPc2d2	kratší
romancí	romance	k1gFnPc2	romance
a	a	k8xC	a
balad	balada	k1gFnPc2	balada
také	také	k9	také
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
básnická	básnický	k2eAgFnSc1d1	básnická
povídka	povídka	k1gFnSc1	povídka
Satanela	Satanela	k1gFnSc1	Satanela
<g/>
,	,	kIx,	,
zasazená	zasazený	k2eAgFnSc1d1	zasazená
do	do	k7c2	do
středověkého	středověký	k2eAgNnSc2d1	středověké
Španělska	Španělsko	k1gNnSc2	Španělsko
Perspektivy	perspektiva	k1gFnSc2	perspektiva
-	-	kIx~	-
tématem	téma	k1gNnSc7	téma
převážně	převážně	k6eAd1	převážně
epických	epický	k2eAgFnPc2d1	epická
či	či	k8xC	či
dialogických	dialogický	k2eAgFnPc2d1	dialogická
básní	báseň	k1gFnPc2	báseň
eklogického	eklogický	k2eAgInSc2d1	eklogický
charakteru	charakter	k1gInSc2	charakter
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
události	událost	k1gFnPc1	událost
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
světových	světový	k2eAgNnPc2d1	světové
náboženství	náboženství	k1gNnPc2	náboženství
(	(	kIx(	(
<g/>
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
zoroastrismus	zoroastrismus	k1gInSc1	zoroastrismus
<g/>
)	)	kIx)	)
i	i	k9	i
mytologií	mytologie	k1gFnPc2	mytologie
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
rituálně	rituálně	k6eAd1	rituálně
stylisovaný	stylisovaný	k2eAgInSc1d1	stylisovaný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
básně	báseň	k1gFnPc1	báseň
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
mýtu	mýtus	k1gInSc2	mýtus
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jazykově	jazykově	k6eAd1	jazykově
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgInPc1d1	složitý
Mythy	mythus	k1gInPc1	mythus
(	(	kIx(	(
<g/>
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
sv.	sv.	kA	sv.
Prokopu	prokopat	k5eAaPmIp1nS	prokopat
<g/>
,	,	kIx,	,
Kříž	Kříž	k1gMnSc1	Kříž
Božetěchův	Božetěchův	k2eAgMnSc1d1	Božetěchův
<g/>
)	)	kIx)	)
Panteon	panteon	k1gInSc1	panteon
Hlasy	hlas	k1gInPc1	hlas
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Jeho	jeho	k3xOp3gFnPc1	jeho
povídky	povídka	k1gFnPc1	povídka
jsou	být	k5eAaImIp3nP	být
ironické	ironický	k2eAgFnPc1d1	ironická
a	a	k8xC	a
sentimentální	sentimentální	k2eAgFnPc1d1	sentimentální
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgInPc4d1	barevný
střepy	střep	k1gInPc4	střep
-	-	kIx~	-
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
autobiografických	autobiografický	k2eAgInPc2d1	autobiografický
prvků	prvek	k1gInPc2	prvek
Loutky	loutka	k1gFnSc2	loutka
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
<g/>
,	,	kIx,	,
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
nenaplněné	naplněný	k2eNgFnPc4d1	nenaplněná
lásky	láska	k1gFnPc4	láska
Povídky	povídka	k1gFnSc2	povídka
ironické	ironický	k2eAgFnPc1d1	ironická
a	a	k8xC	a
sentimentální	sentimentální	k2eAgFnPc1d1	sentimentální
Jeho	jeho	k3xOp3gFnPc1	jeho
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
pro	pro	k7c4	pro
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
trilogie	trilogie	k1gFnSc1	trilogie
-	-	kIx~	-
historické	historický	k2eAgNnSc1d1	historické
drama	drama	k1gNnSc1	drama
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
prvních	první	k4xOgMnPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
Drahomíra	drahomíra	k1gMnSc1	drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
Bratři	bratr	k1gMnPc1	bratr
Knížata	kníže	k1gMnPc1wR	kníže
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
V	v	k7c6	v
sudu	sud	k1gInSc6	sud
Diogenově	Diogenův	k2eAgInSc6d1	Diogenův
Soud	soud	k1gInSc1	soud
lásky	láska	k1gFnSc2	láska
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
-	-	kIx~	-
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
námět	námět	k1gInSc4	námět
mylnou	mylný	k2eAgFnSc4d1	mylná
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zakázal	zakázat	k5eAaPmAgInS	zakázat
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
přítomnost	přítomnost	k1gFnSc1	přítomnost
žen	žena	k1gFnPc2	žena
<g/>
;	;	kIx,	;
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
režisérem	režisér	k1gMnSc7	režisér
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Podskalským	podskalský	k2eAgMnSc7d1	podskalský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
Noc	noc	k1gFnSc4	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
)	)	kIx)	)
Smrt	smrt	k1gFnSc1	smrt
Odyssea	Odyssea	k1gFnSc1	Odyssea
Hippodamie	Hippodamie	k1gFnSc1	Hippodamie
-	-	kIx~	-
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
,	,	kIx,	,
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
ji	on	k3xPp3gFnSc4	on
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fibich	Fibich	k1gMnSc1	Fibich
Námluvy	námluva	k1gFnSc2	námluva
Pelopovy	Pelopův	k2eAgFnSc2d1	Pelopův
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
Smír	smír	k1gInSc4	smír
Tantalův	Tantalův	k2eAgInSc4d1	Tantalův
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Smrt	smrt	k1gFnSc1	smrt
Hippodamie	Hippodamie	k1gFnSc1	Hippodamie
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Svatá	svatý	k2eAgFnSc1d1	svatá
Ludmila	Ludmila	k1gFnSc1	Ludmila
-	-	kIx~	-
zhudebněna	zhudebněn	k2eAgFnSc1d1	zhudebněna
jako	jako	k8xS	jako
oratorium	oratorium	k1gNnSc1	oratorium
Antonínem	Antonín	k1gMnSc7	Antonín
Dvořákem	Dvořák	k1gMnSc7	Dvořák
Básnické	básnický	k2eAgInPc4d1	básnický
profily	profil	k1gInPc4	profil
francouzské	francouzský	k2eAgInPc4d1	francouzský
O	o	k7c4	o
poezii	poezie	k1gFnSc4	poezie
Jana	Jan	k1gMnSc2	Jan
Nerudy	neruda	k1gFnSc2	neruda
Poezie	poezie	k1gFnSc1	poezie
francouzská	francouzský	k2eAgFnSc1d1	francouzská
nové	nový	k2eAgFnPc4d1	nová
doby	doba	k1gFnPc4	doba
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
korespondence	korespondence	k1gFnSc1	korespondence
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
Ludmilou	Ludmila	k1gFnSc7	Ludmila
Podlipskou	Podlipský	k2eAgFnSc4d1	Podlipská
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
jeho	jeho	k3xOp3gInSc2	jeho
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
zde	zde	k6eAd1	zde
filosofické	filosofický	k2eAgFnPc4d1	filosofická
a	a	k8xC	a
literární	literární	k2eAgFnPc4d1	literární
úvahy	úvaha	k1gFnPc4	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Uveřejněna	uveřejněn	k2eAgFnSc1d1	uveřejněna
byla	být	k5eAaImAgFnS	být
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
jako	jako	k8xC	jako
literární	literární	k2eAgNnSc1d1	literární
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
článků	článek	k1gInPc2	článek
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
Máj	máj	k1gInSc1	máj
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
českým	český	k2eAgMnPc3d1	český
překladatelům	překladatel	k1gMnPc3	překladatel
jak	jak	k8xS	jak
rozsahem	rozsah	k1gInSc7	rozsah
a	a	k8xC	a
časovým	časový	k2eAgNnSc7d1	časové
rozpětím	rozpětí	k1gNnSc7	rozpětí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
koncepcí	koncepce	k1gFnSc7	koncepce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
překlad	překlad	k1gInSc1	překlad
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
osobité	osobitý	k2eAgNnSc4d1	osobité
tvůrčí	tvůrčí	k2eAgNnSc4d1	tvůrčí
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
prosadit	prosadit	k5eAaPmF	prosadit
vedle	vedle	k7c2	vedle
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
však	však	k9	však
tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
setřel	setřít	k5eAaPmAgInS	setřít
osobitost	osobitost	k1gFnSc4	osobitost
a	a	k8xC	a
nuance	nuance	k1gFnPc4	nuance
původního	původní	k2eAgNnSc2d1	původní
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgInS	překládat
z	z	k7c2	z
18	[number]	k4	18
národních	národní	k2eAgFnPc2d1	národní
literatur	literatura	k1gFnPc2	literatura
-	-	kIx~	-
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
(	(	kIx(	(
<g/>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
<g/>
,	,	kIx,	,
Edmond	Edmond	k1gMnSc1	Edmond
Rostand	Rostand	k1gInSc1	Rostand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnSc2	italština
(	(	kIx(	(
<g/>
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighier	k1gFnSc2	Alighier
<g/>
,	,	kIx,	,
Francesco	Francesco	k6eAd1	Francesco
Petrarca	Petrarc	k2eAgFnSc1d1	Petrarc
<g/>
,	,	kIx,	,
Torquato	Torquat	k2eAgNnSc1d1	Torquato
Tasso	Tassa	k1gFnSc5	Tassa
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
španělštiny	španělština	k1gFnPc1	španělština
(	(	kIx(	(
<g/>
Pedro	Pedro	k1gNnSc1	Pedro
Calderón	Calderón	k1gMnSc1	Calderón
de	de	k?	de
la	la	k1gNnSc2	la
Barca	Barca	k1gFnSc1	Barca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angličtiny	angličtina	k1gFnSc2	angličtina
(	(	kIx(	(
<g/>
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Gordon	Gordon	k1gMnSc1	Gordon
Byron	Byron	k1gMnSc1	Byron
<g/>
,	,	kIx,	,
Percy	Perc	k2eAgFnPc1d1	Perca
Bysshe	Byssh	k1gFnPc1	Byssh
Shelley	Shellea	k1gFnSc2	Shellea
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
,	,	kIx,	,
Walt	Walt	k1gMnSc1	Walt
Whitman	Whitman	k1gMnSc1	Whitman
<g/>
)	)	kIx)	)
a	a	k8xC	a
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
z	z	k7c2	z
polštiny	polština	k1gFnSc2	polština
(	(	kIx(	(
<g/>
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnSc2	maďarština
(	(	kIx(	(
<g/>
Sándor	Sándor	k1gInSc1	Sándor
Petőfi	Petőf	k1gFnSc2	Petőf
<g/>
,	,	kIx,	,
János	János	k1gMnSc1	János
Arany	Arana	k1gFnSc2	Arana
<g/>
,	,	kIx,	,
Imre	Imr	k1gFnSc2	Imr
Madách	Mady	k1gInPc6	Mady
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
norštiny	norština	k1gFnSc2	norština
(	(	kIx(	(
<g/>
Henrik	Henrik	k1gMnSc1	Henrik
Ibsen	Ibsen	k1gMnSc1	Ibsen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
především	především	k9	především
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
klasickou	klasický	k2eAgFnSc7d1	klasická
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
moderní	moderní	k2eAgFnSc1d1	moderní
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
k	k	k7c3	k
překladům	překlad	k1gInPc3	překlad
vybíral	vybírat	k5eAaImAgMnS	vybírat
náročná	náročný	k2eAgNnPc1d1	náročné
a	a	k8xC	a
kvalitní	kvalitní	k2eAgNnPc1d1	kvalitní
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
Faust	Faust	k1gFnSc1	Faust
<g/>
,	,	kIx,	,
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
<g/>
,	,	kIx,	,
Andersenovy	Andersenův	k2eAgFnPc1d1	Andersenova
pohádky	pohádka	k1gFnPc1	pohádka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
studentskou	studentský	k2eAgFnSc7d1	studentská
láskou	láska	k1gFnSc7	láska
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
dcera	dcera	k1gFnSc1	dcera
jeho	jeho	k3xOp3gFnSc2	jeho
bytné	bytná	k1gFnSc2	bytná
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
-	-	kIx~	-
K.	K.	kA	K.
Martínková	Martínková	k1gFnSc1	Martínková
<g/>
;	;	kIx,	;
psal	psát	k5eAaImAgMnS	psát
jí	on	k3xPp3gFnSc2	on
básně	báseň	k1gFnSc2	báseň
a	a	k8xC	a
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
na	na	k7c6	na
Filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
však	však	k9	však
jejich	jejich	k3xOp3gInSc4	jejich
kontakt	kontakt	k1gInSc4	kontakt
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
na	na	k7c6	na
prázdninách	prázdniny	k1gFnPc6	prázdniny
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
příbuzné	příbuzný	k2eAgFnSc2d1	příbuzná
Marie	Maria	k1gFnSc2	Maria
Potměšilové	potměšil	k1gMnPc1	potměšil
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc4	přednost
jeho	jeho	k3xOp3gMnSc3	jeho
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
Bedřichovi	Bedřich	k1gMnSc3	Bedřich
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
platonický	platonický	k2eAgInSc1d1	platonický
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
inspirací	inspirace	k1gFnSc7	inspirace
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
sbírky	sbírka	k1gFnSc2	sbírka
Sny	sen	k1gInPc1	sen
o	o	k7c4	o
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Meranu	Meran	k1gInSc6	Meran
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
osamělý	osamělý	k2eAgMnSc1d1	osamělý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stýskalo	stýskat	k5eAaImAgNnS	stýskat
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
neúnavně	únavně	k6eNd1	únavně
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
překládal	překládat	k5eAaImAgMnS	překládat
z	z	k7c2	z
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
s	s	k7c7	s
guvernantkou	guvernantka	k1gFnSc7	guvernantka
Charlottou	Charlotta	k1gFnSc7	Charlotta
P.	P.	kA	P.
(	(	kIx(	(
<g/>
celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vztah	vztah	k1gInSc1	vztah
však	však	k9	však
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
erotický	erotický	k2eAgInSc1d1	erotický
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zdaleka	zdaleka	k6eAd1	zdaleka
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
roli	role	k1gFnSc4	role
sehrála	sehrát	k5eAaPmAgFnS	sehrát
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
Sofie	Sofia	k1gFnSc2	Sofia
Podlipská	Podlipský	k2eAgFnSc1d1	Podlipská
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Karoliny	Karolinum	k1gNnPc7	Karolinum
Světlé	světlý	k2eAgFnSc6d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
znal	znát	k5eAaImAgMnS	znát
nejdříve	dříve	k6eAd3	dříve
její	její	k3xOp3gFnSc2	její
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
jen	jen	k9	jen
letmo	letmo	k6eAd1	letmo
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
parku	park	k1gInSc6	park
veltruského	veltruský	k2eAgInSc2d1	veltruský
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Seznámili	seznámit	k5eAaPmAgMnP	seznámit
se	se	k3xPyFc4	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Umělecké	umělecký	k2eAgFnSc6d1	umělecká
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
depresivním	depresivní	k2eAgNnSc6d1	depresivní
rozpoložení	rozpoložení	k1gNnSc6	rozpoložení
v	v	k7c6	v
Meranu	Meran	k1gInSc6	Meran
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
i	i	k9	i
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
talentu	talent	k1gInSc6	talent
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
Podlipské	Podlipský	k2eAgFnSc2d1	Podlipská
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
dopis	dopis	k1gInSc4	dopis
<g/>
;	;	kIx,	;
čilá	čilý	k2eAgFnSc1d1	čilá
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Vrchlického	Vrchlický	k1gMnSc4	Vrchlický
velmi	velmi	k6eAd1	velmi
povzbuzující	povzbuzující	k2eAgFnSc1d1	povzbuzující
korespondence	korespondence	k1gFnSc1	korespondence
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
hluboký	hluboký	k2eAgInSc1d1	hluboký
vztah	vztah	k1gInSc1	vztah
a	a	k8xC	a
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
se	se	k3xPyFc4	se
do	do	k7c2	do
Sofie	Sofia	k1gFnSc2	Sofia
Podlipské	Podlipský	k2eAgFnSc2d1	Podlipská
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
starší	starší	k1gMnPc4	starší
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
-	-	kIx~	-
již	již	k6eAd1	již
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
vdova	vdova	k1gFnSc1	vdova
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
rovněž	rovněž	k9	rovněž
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
si	se	k3xPyFc3	se
však	však	k9	však
vědoma	vědom	k2eAgFnSc1d1	vědoma
velkého	velký	k2eAgInSc2d1	velký
věkového	věkový	k2eAgInSc2d1	věkový
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
obracela	obracet	k5eAaImAgFnS	obracet
jeho	jeho	k3xOp3gFnSc4	jeho
pozornost	pozornost	k1gFnSc4	pozornost
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Ludmile	Ludmila	k1gFnSc3	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
připravit	připravit	k5eAaPmF	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1879	[number]	k4	1879
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c4	v
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
miloval	milovat	k5eAaImAgInS	milovat
matku	matka	k1gFnSc4	matka
v	v	k7c6	v
mladším	mladý	k2eAgMnSc6d2	mladší
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
půvabném	půvabný	k2eAgNnSc6d1	půvabné
-	-	kIx~	-
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
však	však	k9	však
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
nedosahovala	dosahovat	k5eNaImAgFnS	dosahovat
intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
výše	výše	k1gFnSc1	výše
matčiny	matčin	k2eAgFnSc2d1	matčina
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
rozuměl	rozumět	k5eAaImAgMnS	rozumět
spíše	spíše	k9	spíše
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
nežli	nežli	k8xS	nežli
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Ludmila	Ludmila	k1gFnSc1	Ludmila
nicméně	nicméně	k8xC	nicméně
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ideálem	ideál	k1gInSc7	ideál
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
(	(	kIx(	(
<g/>
dcery	dcera	k1gFnPc1	dcera
Milada	Milada	k1gFnSc1	Milada
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vnímal	vnímat	k5eAaImAgInS	vnímat
jako	jako	k9	jako
vrcholně	vrcholně	k6eAd1	vrcholně
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
tvořil	tvořit	k5eAaImAgMnS	tvořit
dramata	drama	k1gNnPc4	drama
<g/>
,	,	kIx,	,
překládal	překládat	k5eAaImAgMnS	překládat
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
a	a	k8xC	a
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k8xC	však
manželství	manželství	k1gNnSc2	manželství
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
hluboká	hluboký	k2eAgFnSc1d1	hluboká
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Ludmila	Ludmila	k1gFnSc1	Ludmila
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
pražského	pražský	k2eAgMnSc2d1	pražský
herce	herec	k1gMnSc2	herec
Jakuba	Jakub	k1gMnSc2	Jakub
Seiferta	Seifert	k1gMnSc2	Seifert
a	a	k8xC	a
udržovala	udržovat	k5eAaImAgFnS	udržovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
snad	snad	k9	snad
osm	osm	k4xCc4	osm
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
poměr	poměra	k1gFnPc2	poměra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nezůstal	zůstat	k5eNaPmAgInS	zůstat
pražské	pražský	k2eAgFnPc4d1	Pražská
společnosti	společnost	k1gFnPc4	společnost
utajen	utajen	k2eAgMnSc1d1	utajen
a	a	k8xC	a
také	také	k6eAd1	také
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
-	-	kIx~	-
Seifert	Seifert	k1gMnSc1	Seifert
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
dvou	dva	k4xCgFnPc2	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
Ludmiliných	Ludmilin	k2eAgFnPc2d1	Ludmilina
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
dlouho	dlouho	k6eAd1	dlouho
nic	nic	k3yNnSc1	nic
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
až	až	k8xS	až
ho	on	k3xPp3gMnSc4	on
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
zpravila	zpravit	k5eAaPmAgFnS	zpravit
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Emma	Emma	k1gFnSc1	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgNnSc7	ten
otřesen	otřesen	k2eAgMnSc1d1	otřesen
<g/>
,	,	kIx,	,
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
psala	psát	k5eAaImAgFnS	psát
Emma	Emma	k1gFnSc1	Emma
později	pozdě	k6eAd2	pozdě
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bratr	bratr	k1gMnSc1	bratr
tehdáž	tehdáž	k?	tehdáž
strašně	strašně	k6eAd1	strašně
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
,	,	kIx,	,
netřeba	netřeba	k6eAd1	netřeba
dokládat	dokládat	k5eAaImF	dokládat
<g/>
.	.	kIx.	.
</s>
<s>
Přede	před	k7c7	před
mnou	já	k3xPp1nSc7	já
a	a	k8xC	a
přáteli	přítel	k1gMnPc7	přítel
se	se	k3xPyFc4	se
držel	držet	k5eAaImAgInS	držet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
svěřoval	svěřovat	k5eAaImAgInS	svěřovat
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
zaplakal	zaplakat	k5eAaPmAgMnS	zaplakat
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
ženu	hnát	k5eAaImIp1nS	hnát
ani	ani	k8xC	ani
nezabil	zabít	k5eNaPmAgMnS	zabít
ani	ani	k8xC	ani
nevyhnal	vyhnat	k5eNaPmAgMnS	vyhnat
-	-	kIx~	-
naoko	naoko	k6eAd1	naoko
vše	všechen	k3xTgNnSc1	všechen
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
při	při	k7c6	při
starém	staré	k1gNnSc6	staré
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
jí	jíst	k5eAaImIp3nS	jíst
neodpustil	odpustit	k5eNaPmAgMnS	odpustit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
však	však	k9	však
přijal	přijmout	k5eAaPmAgMnS	přijmout
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Evou	Eva	k1gFnSc7	Eva
měl	mít	k5eAaImAgInS	mít
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
<g/>
,	,	kIx,	,
láskyplný	láskyplný	k2eAgInSc1d1	láskyplný
vztah	vztah	k1gInSc1	vztah
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
napsala	napsat	k5eAaPmAgFnS	napsat
knihu	kniha	k1gFnSc4	kniha
idylických	idylický	k2eAgFnPc2d1	idylická
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
Dětství	dětství	k1gNnPc2	dětství
s	s	k7c7	s
Vrchlickým	Vrchlický	k1gMnSc7	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
Ludmila	Ludmila	k1gFnSc1	Ludmila
Vrchlickému	Vrchlický	k1gMnSc3	Vrchlický
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
otcem	otec	k1gMnSc7	otec
jejích	její	k3xOp3gFnPc2	její
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
Evy	Eva	k1gFnSc2	Eva
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
listy	list	k1gInPc1	list
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
uschoval	uschovat	k5eAaPmAgMnS	uschovat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
lékař	lékař	k1gMnSc1	lékař
Josef	Josef	k1gMnSc1	Josef
Thomayer	Thomayer	k1gMnSc1	Thomayer
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obálce	obálka	k1gFnSc6	obálka
předal	předat	k5eAaPmAgMnS	předat
Národnímu	národní	k2eAgNnSc3d1	národní
muzeu	muzeum	k1gNnSc3	muzeum
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
;	;	kIx,	;
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
Památníku	památník	k1gInSc6	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
Vrchlického	Vrchlického	k2eAgNnSc4d1	Vrchlického
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
všestranně	všestranně	k6eAd1	všestranně
slavené	slavený	k2eAgNnSc1d1	slavené
<g/>
,	,	kIx,	,
podrobováno	podrobován	k2eAgNnSc1d1	podrobováno
kritice	kritika	k1gFnSc3	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
(	(	kIx(	(
<g/>
F.	F.	kA	F.
X.	X.	kA	X.
Šalda	Šalda	k1gMnSc1	Šalda
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
období	období	k1gNnSc4	období
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
překonat	překonat	k5eAaPmF	překonat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Karlou	Karla	k1gFnSc7	Karla
Bezdíčkovou	Bezdíčková	k1gFnSc7	Bezdíčková
<g/>
,	,	kIx,	,
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
sbírku	sbírka	k1gFnSc4	sbírka
Kvítí	kvítí	k1gNnSc2	kvítí
Pertidy	Pertida	k1gFnSc2	Pertida
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
učitelka	učitelka	k1gFnSc1	učitelka
moderních	moderní	k2eAgInPc2d1	moderní
tanců	tanec	k1gInPc2	tanec
Justina	Justina	k1gFnSc1	Justina
Vondroušová	Vondroušová	k1gFnSc1	Vondroušová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
Vrchlického	Vrchlický	k1gMnSc4	Vrchlický
začala	začít	k5eAaPmAgFnS	začít
trápit	trápit	k5eAaImF	trápit
nemoc	nemoc	k1gFnSc4	nemoc
-	-	kIx~	-
měl	mít	k5eAaImAgMnS	mít
úporné	úporný	k2eAgFnPc4d1	úporná
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
prodělal	prodělat	k5eAaPmAgMnS	prodělat
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
viděl	vidět	k5eAaImAgMnS	vidět
dvojmo	dvojmo	k6eAd1	dvojmo
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
<s>
Léčil	léčit	k5eAaImAgInS	léčit
se	se	k3xPyFc4	se
v	v	k7c6	v
sanatoriích	sanatorium	k1gNnPc6	sanatorium
ve	v	k7c6	v
Stupčicích	Stupčice	k1gFnPc6	Stupčice
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
istrijské	istrijský	k2eAgFnSc6d1	istrijská
Opatiji	Opatiji	k1gFnSc6	Opatiji
a	a	k8xC	a
v	v	k7c6	v
Domažlicích	Domažlice	k1gFnPc6	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
jej	on	k3xPp3gMnSc4	on
mrtvice	mrtvice	k1gFnSc1	mrtvice
postihla	postihnout	k5eAaPmAgFnS	postihnout
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
polovinu	polovina	k1gFnSc4	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
v	v	k7c6	v
pouhých	pouhý	k2eAgNnPc6d1	pouhé
59	[number]	k4	59
letech	léto	k1gNnPc6	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
lékařské	lékařský	k2eAgFnSc2d1	lékařská
zprávy	zpráva	k1gFnSc2	zpráva
následkem	následkem	k7c2	následkem
sklerózy	skleróza	k1gFnSc2	skleróza
mozkových	mozkový	k2eAgFnPc2d1	mozková
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
