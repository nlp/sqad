<s>
Vakcína	vakcína	k1gFnSc1
Sanofi	Sanof	k1gFnSc2
<g/>
–	–	kIx~
<g/>
GlaxoSmithKline	GlaxoSmithKline	k1gInSc1
proti	proti	k7c3
covidu-	covid	k1gInSc3
<g/>
19	#num#	k4
francouzské	francouzský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Sanofi	Sanof	k1gFnSc1
Pasteur	Pasteura	k1gFnSc1
a	a	k8xC
britské	britský	k2eAgFnSc2d1
nadnárodní	nadnárodní	k2eAgFnSc2d1
korporace	korporace	k1gFnSc2
GlaxoSmithKline	GlaxoSmithKline	k1gInSc1
(	(	kIx(
<g/>
GSK	GSK	kA
<g/>
)	)	kIx)
s	s	k7c7
pracovním	pracovní	k2eAgInSc7d1
názvem	název	k1gInSc7
(	(	kIx(
<g/>
kódovým	kódový	k2eAgNnSc7d1
označením	označení	k1gNnSc7
<g/>
)	)	kIx)
VAT00002	VAT00002	k1gMnSc1
je	být	k5eAaImIp3nS
kandidátní	kandidátní	k2eAgFnSc7d1
vakcínou	vakcína	k1gFnSc7
proti	proti	k7c3
onemocnění	onemocnění	k1gNnSc3
covid-	covid	k1gInSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>