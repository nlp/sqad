<s>
Vakcína	vakcína	k1gFnSc1
Sanofi	Sanof	k1gFnSc2
<g/>
–	–	k?
<g/>
GlaxoSmithKline	GlaxoSmithKlin	k1gInSc5
proti	proti	k7c3
covidu-	covidu-	k?
<g/>
19	#num#	k4
</s>
<s>
Vakcína	vakcína	k1gFnSc1
Sanofi	Sanof	k1gFnSc2
<g/>
–	–	kIx~
<g/>
GlaxoSmithKline	GlaxoSmithKline	k1gInSc1
proti	proti	k7c3
covidu-	covid	k1gInSc3
<g/>
19	#num#	k4
francouzské	francouzský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Sanofi	Sanof	k1gFnSc1
Pasteur	Pasteura	k1gFnSc1
a	a	k8xC
britské	britský	k2eAgFnSc2d1
nadnárodní	nadnárodní	k2eAgFnSc2d1
korporace	korporace	k1gFnSc2
GlaxoSmithKline	GlaxoSmithKline	k1gInSc1
(	(	kIx(
<g/>
GSK	GSK	kA
<g/>
)	)	kIx)
s	s	k7c7
pracovním	pracovní	k2eAgInSc7d1
názvem	název	k1gInSc7
(	(	kIx(
<g/>
kódovým	kódový	k2eAgNnSc7d1
označením	označení	k1gNnSc7
<g/>
)	)	kIx)
VAT00002	VAT00002	k1gMnSc1
je	být	k5eAaImIp3nS
kandidátní	kandidátní	k2eAgFnSc7d1
vakcínou	vakcína	k1gFnSc7
proti	proti	k7c3
onemocnění	onemocnění	k1gNnSc3
covid-	covid	k1gInSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
technologii	technologie	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
rekombinantních	rekombinantní	k2eAgInPc6d1
proteinech	protein	k1gInPc6
od	od	k7c2
Sanofi	Sanof	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2020	#num#	k4
vláda	vláda	k1gFnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
podepsala	podepsat	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
60	#num#	k4
milionů	milion	k4xCgInPc2
dávek	dávka	k1gFnPc2
vakcíny	vakcína	k1gFnSc2
na	na	k7c4
covid-	covid-	k?
<g/>
19	#num#	k4
vyvinutých	vyvinutý	k2eAgFnPc2d1
společnostmi	společnost	k1gFnPc7
GSK	GSK	kA
a	a	k8xC
Sanofi	Sanof	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
předběžné	předběžný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
nákupu	nákup	k1gInSc6
mezi	mezi	k7c7
EU	EU	kA
a	a	k8xC
Sanofi	Sanofi	k1gNnSc7
<g/>
–	–	k?
<g/>
GSK	GSK	kA
(	(	kIx(
<g/>
smlouva	smlouva	k1gFnSc1
ze	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
EU	EU	kA
pořídit	pořídit	k5eAaPmF
až	až	k9
300	#num#	k4
milionů	milion	k4xCgInPc2
dávek	dávka	k1gFnPc2
očkovací	očkovací	k2eAgFnSc2d1
látky	látka	k1gFnSc2
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
prokáže	prokázat	k5eAaPmIp3nS
bezpečnost	bezpečnost	k1gFnSc1
a	a	k8xC
účinnost	účinnost	k1gFnSc1
vakcíny	vakcína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společnost	společnost	k1gFnSc1
rovněž	rovněž	k6eAd1
souhlasila	souhlasit	k5eAaImAgFnS
s	s	k7c7
dohodou	dohoda	k1gFnSc7
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
o	o	k7c6
produkci	produkce	k1gFnSc6
100	#num#	k4
milionů	milion	k4xCgInPc2
dávek	dávka	k1gFnPc2
vakcíny	vakcína	k1gFnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
2,1	2,1	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zpráv	zpráva	k1gFnPc2
CNBC	CNBC	kA
v	v	k7c6
rámci	rámec	k1gInSc6
2,1	2,1	k4
miliardového	miliardový	k2eAgInSc2d1
vládního	vládní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
USA	USA	kA
jde	jít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
750	#num#	k4
milionů	milion	k4xCgInPc2
na	na	k7c4
podporu	podpora	k1gFnSc4
Sanofi	Sanof	k1gFnSc2
<g/>
–	–	k?
<g/>
GSK	GSK	kA
při	při	k7c6
vývoji	vývoj	k1gInSc6
vakcín	vakcína	k1gFnPc2
a	a	k8xC
klinických	klinický	k2eAgNnPc6d1
studiích	studio	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Klinický	klinický	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
</s>
<s>
Fáze	fáze	k1gFnSc1
I	i	k9
</s>
<s>
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
konkurencí	konkurence	k1gFnSc7
Sanofi	Sanof	k1gFnSc2
plánovala	plánovat	k5eAaImAgFnS
zahájit	zahájit	k5eAaPmF
klinické	klinický	k2eAgNnSc4d1
testování	testování	k1gNnSc4
Fáze	fáze	k1gFnSc2
1	#num#	k4
až	až	k8xS
v	v	k7c6
září	září	k1gNnSc6
2020	#num#	k4
a	a	k8xC
schválení	schválení	k1gNnSc4
pro	pro	k7c4
nouzové	nouzový	k2eAgNnSc4d1
použití	použití	k1gNnSc4
očekává	očekávat	k5eAaImIp3nS
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Klinické	klinický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
byly	být	k5eAaImAgFnP
odloženy	odložit	k5eAaPmNgFnP
v	v	k7c6
prosinci	prosinec	k1gInSc6
2020	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
vakcína	vakcína	k1gFnSc1
nevyvolala	vyvolat	k5eNaPmAgFnS
dostatečnou	dostatečný	k2eAgFnSc4d1
imunitní	imunitní	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
u	u	k7c2
lidí	člověk	k1gMnPc2
nad	nad	k7c4
50	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
kvůli	kvůli	k7c3
nedostatečné	dostatečný	k2eNgFnSc3d1
koncentraci	koncentrace	k1gFnSc3
antigenu	antigen	k1gInSc2
ve	v	k7c6
vakcíně	vakcína	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
2020	#num#	k4
předpoklad	předpoklad	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
uvedení	uvedení	k1gNnSc3
vakcíny	vakcína	k1gFnSc2
až	až	k9
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Sanofi	Sanof	k1gFnSc2
<g/>
–	–	k?
<g/>
GSK	GSK	kA
COVID-19	COVID-19	k1gFnSc2
vaccine	vaccinout	k5eAaPmIp3nS
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Očkovací	očkovací	k2eAgFnSc2d1
látky	látka	k1gFnSc2
–	–	k?
strategie	strategie	k1gFnSc2
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vakcína	vakcína	k1gFnSc1
Sanofi	Sanof	k1gFnSc2
a	a	k8xC
GSK	GSK	kA
musí	muset	k5eAaImIp3nS
projít	projít	k5eAaPmF
dalším	další	k2eAgNnSc7d1
testováním	testování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
Noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vakcína	vakcína	k1gFnSc1
Sanofi	Sanof	k1gFnSc2
a	a	k8xC
GSK	GSK	kA
nabírá	nabírat	k5eAaImIp3nS
nečekané	čekaný	k2eNgNnSc4d1
zpoždění	zpoždění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jízdenky	jízdenka	k1gFnPc4
ven	ven	k6eAd1
z	z	k7c2
pandemie	pandemie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgFnPc7
vakcínami	vakcína	k1gFnPc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
Evropa	Evropa	k1gFnSc1
bránit	bránit	k5eAaImF
proti	proti	k7c3
covidu-	covidu-	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-12	2020-11-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
https://www.avcr.cz/cs/veda-a-vyzkum/aktuality/Prehledne-Ctyri-typy-vakcin-proti-nemoci-covid-19/	https://www.avcr.cz/cs/veda-a-vyzkum/aktuality/Prehledne-Ctyri-typy-vakcin-proti-nemoci-covid-19/	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Pandemie	pandemie	k1gFnSc1
covidu-	covidu-	k?
<g/>
19	#num#	k4
Covid-	Covid-	k1gFnSc1
<g/>
19	#num#	k4
(	(	kIx(
<g/>
onemocnění	onemocnění	k1gNnPc2
<g/>
)	)	kIx)
•	•	k?
SARS-CoV-	SARS-CoV-	k1gFnSc7
<g/>
2	#num#	k4
(	(	kIx(
<g/>
virus	virus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
listopad	listopad	k1gInSc1
<g/>
–	–	k?
<g/>
prosinec	prosinec	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
leden	leden	k1gInSc1
</s>
<s>
únor	únor	k1gInSc1
</s>
<s>
Lokace	lokace	k1gFnSc1
Asie	Asie	k1gFnSc2
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Tržnice	tržnice	k1gFnSc1
Chua-nan	Chua-nana	k1gFnPc2
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
průběh	průběh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Vatikán	Vatikán	k1gInSc1
Ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
stát	stát	k1gInSc1
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Diamond	Diamond	k1gInSc1
Princess	Princessa	k1gFnPc2
</s>
<s>
Grand	grand	k1gMnSc1
Princess	Princessa	k1gFnPc2
</s>
<s>
USS	USS	kA
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
</s>
<s>
Dopady	dopad	k1gInPc1
</s>
<s>
recese	recese	k1gFnSc1
</s>
<s>
sport	sport	k1gInSc1
</s>
<s>
věda	věda	k1gFnSc1
a	a	k8xC
technologie	technologie	k1gFnSc1
</s>
<s>
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
</s>
<s>
Instituce	instituce	k1gFnSc1
Střediska	středisko	k1gNnSc2
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
nemocí	nemoc	k1gFnPc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Nemocnice	nemocnice	k1gFnSc2
</s>
<s>
Nemocnice	nemocnice	k1gFnSc1
Chuo-šen-šan	Chuo-šen-šana	k1gFnPc2
</s>
<s>
Nemocnice	nemocnice	k1gFnSc1
Lej-šen-šan	Lej-šen-šana	k1gFnPc2
Organizace	organizace	k1gFnSc2
</s>
<s>
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
zdravotní	zdravotní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
(	(	kIx(
<g/>
Čína	Čína	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Wuchanský	Wuchanský	k2eAgInSc1d1
institut	institut	k1gInSc1
virologie	virologie	k1gFnSc2
</s>
<s>
Problémy	problém	k1gInPc1
Problémy	problém	k1gInPc1
a	a	k8xC
reakce	reakce	k1gFnPc1
</s>
<s>
evakuace	evakuace	k1gFnSc1
</s>
<s>
lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
</s>
<s>
dezinformace	dezinformace	k1gFnSc1
</s>
<s>
globální	globální	k2eAgInSc1d1
stav	stav	k1gInSc1
zdravotní	zdravotní	k2eAgFnSc2d1
nouze	nouze	k1gFnSc2
</s>
<s>
omezení	omezení	k1gNnSc1
cestování	cestování	k1gNnSc2
</s>
<s>
xenofobie	xenofobie	k1gFnSc1
a	a	k8xC
rasismus	rasismus	k1gInSc1
Léčba	léčba	k1gFnSc1
</s>
<s>
testování	testování	k1gNnSc1
nemoci	nemoc	k1gFnSc2
</s>
<s>
vakcína	vakcína	k1gFnSc1
(	(	kIx(
<g/>
tozinameran	tozinameran	k1gInSc1
[	[	kIx(
<g/>
BioNTech	BioNTech	k1gInSc1
a	a	k8xC
Pfizer	Pfizer	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
mRNA-	mRNA-	k?
<g/>
1273	#num#	k4
[	[	kIx(
<g/>
Moderna	Moderna	k1gFnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
AZD1222	AZD1222	k1gFnSc1
[	[	kIx(
<g/>
Oxford	Oxford	k1gInSc1
a	a	k8xC
AstraZeneca	AstraZeneca	k1gFnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Sputnik	sputnik	k1gInSc1
V	V	kA
<g/>
,	,	kIx,
Ad	ad	k7c4
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
COV	COV	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
S	s	k7c7
[	[	kIx(
<g/>
Johnson	Johnson	k1gMnSc1
&	&	k?
Johnson	Johnson	k1gMnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Remdesivir	Remdesivir	k1gMnSc1
</s>
<s>
Lidé	člověk	k1gMnPc1
Lékaři	lékař	k1gMnPc1
a	a	k8xC
oznamovatelé	oznamovatel	k1gMnPc1
</s>
<s>
Aj	aj	kA
Fen	fena	k1gFnPc2
</s>
<s>
Li	li	k8xS
Wen-liang	Wen-lianga	k1gFnPc2
Politici	politik	k1gMnPc1
a	a	k8xC
úředníci	úředník	k1gMnPc1
</s>
<s>
Tedros	Tedrosa	k1gFnPc2
Adhanom	Adhanom	k1gInSc1
Ghebreyesus	Ghebreyesus	k1gMnSc1
(	(	kIx(
<g/>
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
WHO	WHO	kA
<g/>
)	)	kIx)
</s>
<s>
Michael	Michael	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Ryan	Ryan	k1gMnSc1
(	(	kIx(
<g/>
výkonný	výkonný	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
programu	program	k1gInSc2
WHO	WHO	kA
pro	pro	k7c4
mimořádné	mimořádný	k2eAgFnPc4d1
události	událost	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
zdraví	zdraví	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Li	li	k8xS
Kche-čchiang	Kche-čchiang	k1gMnSc1
(	(	kIx(
<g/>
premiér	premiér	k1gMnSc1
<g/>
)	)	kIx)
Itálie	Itálie	k1gFnSc1
</s>
<s>
Giuseppe	Giusepp	k1gMnSc5
Conte	Cont	k1gMnSc5
(	(	kIx(
<g/>
premiér	premiér	k1gMnSc1
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Roberto	Roberta	k1gFnSc5
Speranza	Speranz	k1gMnSc4
(	(	kIx(
<g/>
ministr	ministr	k1gMnSc1
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Gaetano	Gaetana	k1gFnSc5
Manfredi	Manfred	k1gMnPc1
(	(	kIx(
<g/>
ministr	ministr	k1gMnSc1
univerzit	univerzita	k1gFnPc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Čchen	Čchen	k2eAgInSc1d1
Čchiou-š	Čchiou-š	k1gInSc1
<g/>
'	'	kIx"
</s>
<s>
Údaje	údaj	k1gInPc1
a	a	k8xC
statistiky	statistika	k1gFnPc1
Celosvětové	celosvětový	k2eAgFnPc1d1
</s>
<s>
podle	podle	k7c2
zemí	zem	k1gFnPc2
Asie	Asie	k1gFnSc2
</s>
<s>
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
tabulka	tabulka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
Evropa	Evropa	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
tabulka	tabulka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
(	(	kIx(
<g/>
graf	graf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Vatikán	Vatikán	k1gInSc1
(	(	kIx(
<g/>
graf	graf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kategorie	kategorie	k1gFnSc1
•	•	k?
nemoc	nemoc	k1gFnSc1
X	X	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
