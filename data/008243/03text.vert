<p>
<s>
Wang	Wang	k1gInSc1	Wang
Šu-jen	Šuna	k1gFnPc2	Šu-jna
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Wáng	Wánga	k1gFnPc2	Wánga
Shū	Shū	k1gMnPc2	Shū
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
王	王	k?	王
<g/>
;	;	kIx,	;
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1974	[number]	k4	1974
Fu-šun	Fu-šun	k1gInSc1	Fu-šun
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
čínská	čínský	k2eAgFnSc1d1	čínská
zápasnice	zápasnice	k1gFnSc1	zápasnice
–	–	k?	–
judistka	judistka	k1gFnSc1	judistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
judem	judo	k1gNnSc7	judo
začínala	začínat	k5eAaImAgFnS	začínat
ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
Fu-šunu	Fu-šun	k1gInSc6	Fu-šun
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
na	na	k7c4	na
vysokoškolská	vysokoškolský	k2eAgNnPc4d1	vysokoškolské
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Připravovala	připravovat	k5eAaImAgFnS	připravovat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Wu	Wu	k1gFnSc2	Wu
Č	Č	kA	Č
<g/>
'	'	kIx"	'
<g/>
-sina	in	k1gMnSc2	-sin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
reprezentaci	reprezentace	k1gFnSc6	reprezentace
se	se	k3xPyFc4	se
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
v	v	k7c6	v
lehké	lehký	k2eAgFnSc6d1	lehká
váze	váha	k1gFnSc6	váha
do	do	k7c2	do
57	[number]	k4	57
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
nominována	nominován	k2eAgFnSc1d1	nominována
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
mistryně	mistryně	k1gFnSc1	mistryně
asie	asie	k1gFnSc1	asie
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1996	[number]	k4	1996
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Wang	Wanga	k1gFnPc2	Wanga
Šu-jen	Šuna	k1gFnPc2	Šu-jna
na	na	k7c6	na
judoinside	judoinsid	k1gInSc5	judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
