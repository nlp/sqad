<s>
Filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
poroty	porota	k1gFnSc2	porota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
České	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
