<s>
Dysprosium	Dysprosium	k1gNnSc1
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
10	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Dy	Dy	kA
</s>
<s>
66	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
dysprosium	dysprosium	k1gNnSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc1
<g/>
,	,	kIx,
Dy	Dy	k1gFnSc1
<g/>
,	,	kIx,
66	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Dysprosium	Dysprosium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7429-91-6	7429-91-6	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
162,500	162,500	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
10	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,22	1,22	k4
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
8,540	8,540	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
;	;	kIx,
<g/>
Hustota	hustota	k1gFnSc1
při	při	k7c6
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnSc2
<g/>
:	:	kIx,
8,37	8,37	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1	#num#	k4
407	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
680,15	680,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2	#num#	k4
567	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
840,15	840,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Terbium	terbium	k1gNnSc1
≺	≺	k?
<g/>
Dy	Dy	k1gMnSc2
<g/>
≻	≻	k?
Holmium	holmium	k1gNnSc1
</s>
<s>
⋎	⋎	k?
<g/>
Cf	Cf	k1gFnSc1
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Dy	Dy	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Dysprosium	Dysprosium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
měkký	měkký	k2eAgInSc1d1
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
přechodný	přechodný	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
člen	člen	k1gInSc1
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
využití	využití	k1gNnSc2
při	při	k7c6
výrobě	výroba	k1gFnSc6
speciálních	speciální	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
pro	pro	k7c4
jadernou	jaderný	k2eAgFnSc4d1
energetiku	energetika	k1gFnSc4
a	a	k8xC
při	při	k7c6
výrobě	výroba	k1gFnSc6
laserů	laser	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc1
je	být	k5eAaImIp3nS
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
přechodný	přechodný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc1
</s>
<s>
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
dysprosium	dysprosium	k1gNnSc4
méně	málo	k6eAd2
reaktivní	reaktivní	k2eAgInPc4d1
než	než	k8xS
předchozí	předchozí	k2eAgInPc4d1
prvky	prvek	k1gInPc4
ze	z	k7c2
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
suchém	suchý	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
stálé	stálý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
vlhkém	vlhký	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
se	se	k3xPyFc4
pomalu	pomalu	k6eAd1
pokrývá	pokrývat	k5eAaImIp3nS
vrstvičkou	vrstvička	k1gFnSc7
oxidu	oxid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snadno	snadno	k6eAd1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
v	v	k7c6
běžných	běžný	k2eAgFnPc6d1
minerálních	minerální	k2eAgFnPc6d1
kyselinách	kyselina	k1gFnPc6
za	za	k7c2
vývoje	vývoj	k1gInSc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Dy	Dy	k1gFnSc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnPc4
Dy	Dy	k1gFnSc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
vykazují	vykazovat	k5eAaImIp3nP
vlastnosti	vlastnost	k1gFnPc4
podobné	podobný	k2eAgFnPc4d1
sloučeninám	sloučenina	k1gFnPc3
ostatních	ostatní	k2eAgInPc2d1
lanthanoidů	lanthanoid	k1gInPc2
a	a	k8xC
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
prvky	prvek	k1gInPc4
tvoří	tvořit	k5eAaImIp3nS
například	například	k6eAd1
vysoce	vysoce	k6eAd1
stabilní	stabilní	k2eAgInPc1d1
oxidy	oxid	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
nereagují	reagovat	k5eNaBmIp3nP
s	s	k7c7
vodou	voda	k1gFnSc7
a	a	k8xC
jen	jen	k9
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
se	se	k3xPyFc4
redukují	redukovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
solí	sůl	k1gFnPc2
anorganických	anorganický	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
jsou	být	k5eAaImIp3nP
důležité	důležitý	k2eAgInPc1d1
především	především	k9
fluoridy	fluorid	k1gInPc1
a	a	k8xC
fosforečnany	fosforečnan	k1gInPc1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
nerozpustnost	nerozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
separaci	separace	k1gFnSc3
lanthanoidů	lanthanoid	k1gInPc2
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
kovových	kovový	k2eAgInPc2d1
iontů	ion	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dysprosité	Dysprositý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
mají	mít	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
žlutou	žlutý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc4
objevil	objevit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1886	#num#	k4
francouzský	francouzský	k2eAgInSc1d1
chemik	chemik	k1gMnSc1
Paul-Emile	Paul-Emila	k1gFnSc3
Lecoq	Lecoq	k1gFnSc3
de	de	k?
Boisbaudran	Boisbaudran	k1gInSc4
jako	jako	k8xS,k8xC
nečistotu	nečistota	k1gFnSc4
ve	v	k7c6
zkoumaném	zkoumaný	k2eAgInSc6d1
oxidu	oxid	k1gInSc6
erbitém	erbitý	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elementární	elementární	k2eAgFnSc1d1
čisté	čistý	k2eAgNnSc4d1
dysprosium	dysprosium	k1gNnSc4
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
teprve	teprve	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
užitím	užití	k1gNnSc7
techniky	technika	k1gFnSc2
ionexové	ionexový	k2eAgFnSc2d1
separace	separace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Dysprosium	Dysprosium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
obsaženo	obsáhnout	k5eAaPmNgNnS
v	v	k7c6
koncentraci	koncentrace	k1gFnSc6
přibližně	přibližně	k6eAd1
3	#num#	k4
–	–	k?
4,5	4,5	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
,	,	kIx,
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
obsahu	obsah	k1gInSc6
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
údaje	údaj	k1gInPc1
chybí	chybit	k5eAaPmIp3nP,k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
dysprosia	dysprosium	k1gNnSc2
na	na	k7c4
100	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
dysprosium	dysprosium	k1gNnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistují	existovat	k5eNaImIp3nP
však	však	k9
ani	ani	k9
minerály	minerál	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
by	by	kYmCp3nS
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
lanthanoidy	lanthanoida	k1gFnPc1
(	(	kIx(
<g/>
prvky	prvek	k1gInPc1
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
)	)	kIx)
vyskytovaly	vyskytovat	k5eAaImAgInP
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
minerály	minerál	k1gInPc4
směsné	směsný	k2eAgInPc4d1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
obsahují	obsahovat	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgInPc1
prvky	prvek	k1gInPc1
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
patří	patřit	k5eAaImIp3nS
monazity	monazit	k1gInPc4
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
<g/>
,	,	kIx,
Th	Th	k1gFnSc1
<g/>
,	,	kIx,
Nd	Nd	k1gFnSc1
<g/>
,	,	kIx,
Y	Y	kA
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
4	#num#	k4
a	a	k8xC
xenotim	xenotim	k1gInSc1
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
fosforečnany	fosforečnan	k1gInPc1
lanthanoidů	lanthanoid	k1gInPc2
,	,	kIx,
dále	daleko	k6eAd2
bastnäsity	bastnäsit	k1gInPc1
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
<g/>
,	,	kIx,
Y	Y	kA
<g/>
)	)	kIx)
<g/>
CO	co	k8xS
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
F	F	kA
<g/>
–	–	k?
směsné	směsný	k2eAgInPc4d1
flourouhličitany	flourouhličitan	k1gInPc4
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
a	a	k8xC
např.	např.	kA
minerál	minerál	k1gInSc1
euxenit	euxenit	k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
<g/>
Ce	Ce	k1gMnSc1
<g/>
,	,	kIx,
<g/>
U	U	kA
<g/>
,	,	kIx,
<g/>
Th	Th	k1gFnSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Nb	Nb	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
<g/>
Ti	ty	k3xPp2nSc3
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
těchto	tento	k3xDgFnPc2
rud	ruda	k1gFnPc2
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
ve	v	k7c4
Skandinávii	Skandinávie	k1gFnSc4
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
a	a	k8xC
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
jsou	být	k5eAaImIp3nP
i	i	k9
fosfátové	fosfátový	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
–	–	k?
apatity	apatit	k1gInPc1
z	z	k7c2
poloostrova	poloostrov	k1gInSc2
Kola	kolo	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
ohlášen	ohlásit	k5eAaPmNgInS,k5eAaImNgInS
nález	nález	k1gInSc1
ložiska	ložisko	k1gNnSc2
bohatého	bohatý	k2eAgNnSc2d1
na	na	k7c6
yttrium	yttrium	k1gNnSc4
<g/>
,	,	kIx,
dysprosium	dysprosium	k1gNnSc4
<g/>
,	,	kIx,
europium	europium	k1gNnSc4
a	a	k8xC
terbium	terbium	k1gNnSc4
poblíž	poblíž	k7c2
japonského	japonský	k2eAgInSc2d1
ostrůvku	ostrůvek	k1gInSc2
Minamitori	Minamitor	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
1	#num#	k4
850	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Tokia	Tokio	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezené	omezený	k2eAgFnSc3d1
dostupnosti	dostupnost	k1gFnSc3
hrozí	hrozit	k5eAaImIp3nS
v	v	k7c6
nejbližších	blízký	k2eAgInPc6d3
letech	let	k1gInPc6
kritický	kritický	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
zdrojů	zdroj	k1gInPc2
prvku	prvek	k1gInSc2
pro	pro	k7c4
technologické	technologický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výše	vysoce	k6eAd2
uvedený	uvedený	k2eAgInSc1d1
nález	nález	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgInPc2d1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnPc1
rudy	ruda	k1gFnPc1
nejprve	nejprve	k6eAd1
louží	loužit	k5eAaImIp3nP
směsí	směs	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
a	a	k8xC
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1
a	a	k8xC
ze	z	k7c2
vzniklého	vzniklý	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
solí	solit	k5eAaImIp3nS
se	s	k7c7
přídavkem	přídavek	k1gInSc7
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
vysráží	vysrážet	k5eAaPmIp3nS
hydroxidy	hydroxid	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Separace	separace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
řadou	řada	k1gFnSc7
různých	různý	k2eAgInPc2d1
postupů	postup	k1gInPc2
–	–	k?
kapalinovou	kapalinový	k2eAgFnSc7d1
extrakcí	extrakce	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c4
použití	použití	k1gNnSc4
ionexových	ionexový	k2eAgFnPc2d1
kolon	kolona	k1gFnPc2
nebo	nebo	k8xC
selektivním	selektivní	k2eAgNnSc7d1
srážením	srážení	k1gNnSc7
nerozpustných	rozpustný	k2eNgFnPc2d1
komplexních	komplexní	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
čistého	čistý	k2eAgInSc2d1
kovu	kov	k1gInSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
provádí	provádět	k5eAaImIp3nS
redukcí	redukce	k1gFnSc7
oxidu	oxid	k1gInSc2
dysprosia	dysprosium	k1gNnSc2
Dy	Dy	k1gFnSc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
elementárním	elementární	k2eAgInSc7d1
vápníkem	vápník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Dy	Dy	k?
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
+	+	kIx~
3	#num#	k4
Ca	ca	kA
→	→	k?
2	#num#	k4
Dy	Dy	k1gFnPc2
+	+	kIx~
3	#num#	k4
CaO	CaO	k1gFnPc2
</s>
<s>
Použití	použití	k1gNnSc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
gadolinium	gadolinium	k1gNnSc4
<g/>
,	,	kIx,
vykazuje	vykazovat	k5eAaImIp3nS
dysprosium	dysprosium	k1gNnSc4
vysoký	vysoký	k2eAgInSc1d1
účinný	účinný	k2eAgInSc1d1
průřez	průřez	k1gInSc1
pro	pro	k7c4
záchyt	záchyt	k1gInSc4
tepelných	tepelný	k2eAgInPc2d1
neutronů	neutron	k1gInPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
slitiny	slitina	k1gFnSc2
s	s	k7c7
niklem	nikl	k1gInSc7
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
používaným	používaný	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
moderátorových	moderátorův	k2eAgFnPc2d1
tyčí	tyč	k1gFnPc2
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasunutím	zasunutí	k1gNnSc7
těchto	tento	k3xDgFnPc2
tyčí	tyč	k1gFnPc2
do	do	k7c2
nitra	nitro	k1gNnSc2
reaktoru	reaktor	k1gInSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
poklesu	pokles	k1gInSc3
neutronového	neutronový	k2eAgInSc2d1
toku	tok	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
zpomalení	zpomalení	k1gNnSc4
štěpné	štěpný	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Světelné	světelný	k2eAgFnPc1d1
výbojky	výbojka	k1gFnPc1
plněné	plněný	k2eAgFnSc2d1
halogenidy	halogenida	k1gFnSc2
dysprosia	dysprosia	k1gFnSc1
jsou	být	k5eAaImIp3nP
zdrojem	zdroj	k1gInSc7
velmi	velmi	k6eAd1
intenzivního	intenzivní	k2eAgNnSc2d1
světelného	světelný	k2eAgNnSc2d1
záření	záření	k1gNnSc2
a	a	k8xC
nacházejí	nacházet	k5eAaImIp3nP
proto	proto	k8xC
uplatnění	uplatnění	k1gNnSc4
především	především	k9
ve	v	k7c6
filmařském	filmařský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
speciálních	speciální	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
s	s	k7c7
požadavky	požadavek	k1gInPc7
na	na	k7c4
mohutný	mohutný	k2eAgInSc4d1
světelný	světelný	k2eAgInSc4d1
tok	tok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
vanadem	vanad	k1gInSc7
se	se	k3xPyFc4
dysprosium	dysprosium	k1gNnSc1
uplatňuje	uplatňovat	k5eAaImIp3nS
při	při	k7c6
výrobě	výroba	k1gFnSc6
laserů	laser	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
.	.	kIx.
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gMnSc1
tremendous	tremendous	k1gMnSc1
potential	potential	k1gMnSc1
of	of	k?
deep-sea	deep-sea	k1gMnSc1
mud	mud	k?
as	as	k1gInSc1
a	a	k8xC
source	source	k1gMnSc1
of	of	k?
rare-earth	rare-earth	k1gInSc1
elements	elementsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2018	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Energy	Energ	k1gInPc4
Department	department	k1gInSc1
Releases	Releases	k1gMnSc1
New	New	k1gMnSc1
Critical	Critical	k1gMnSc1
Materials	Materials	k1gInSc4
Strategy	Stratega	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2010	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1993	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dysprosium	Dysprosium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
dysprosium	dysprosium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4150953-5	4150953-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85040345	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85040345	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
