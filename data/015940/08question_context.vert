<s>
Ledňáčkovití	Ledňáčkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alcedinidae	Alcedinidae	k1gNnSc7
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
čeleď	čeleď	k1gFnSc4
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc4
druhy	druh	k1gMnPc4
jsou	být	k5eAaImIp3nP
rozšířené	rozšířený	k2eAgFnPc1d1
hlavně	hlavně	k9
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>