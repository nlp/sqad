<s>
Ledňáčkovití	Ledňáčkovitý	k2eAgMnPc1d1
</s>
<s>
Ledňáčkovití	Ledňáčkovitý	k2eAgMnPc1d1
Ledňáček	ledňáček	k1gMnSc1
říční	říční	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
letci	letec	k1gMnSc3
(	(	kIx(
<g/>
Neognathae	Neognathae	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
srostloprstí	srostloprstý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Coraciiiformes	Coraciiiformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
ledňáčkovití	ledňáčkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alcedinidae	Alcedinidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
Rafinesque	Rafinesque	k1gNnSc7
<g/>
,	,	kIx,
1815	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ledňáčkovití	Ledňáčkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alcedinidae	Alcedinidae	k1gNnSc7
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
čeleď	čeleď	k1gFnSc4
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc4
druhy	druh	k1gMnPc4
jsou	být	k5eAaImIp3nP
rozšířené	rozšířený	k2eAgFnPc1d1
hlavně	hlavně	k9
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
žije	žít	k5eAaImIp3nS
pouze	pouze	k6eAd1
ledňáček	ledňáček	k1gMnSc1
říční	říční	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
silnější	silný	k2eAgNnSc4d2
tělo	tělo	k1gNnSc4
<g/>
,	,	kIx,
krátký	krátký	k2eAgInSc4d1
ocas	ocas	k1gInSc4
a	a	k8xC
silný	silný	k2eAgInSc4d1
tupý	tupý	k2eAgInSc4d1
zobák	zobák	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rody	rod	k1gInPc1
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Actenoides	Actenoides	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Caridonax	Caridonax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Cittura	Cittura	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňák	Ledňák	k1gInSc1
(	(	kIx(
<g/>
Crytoceyx	Crytoceyx	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňák	Ledňák	k1gInSc1
(	(	kIx(
<g/>
Dacelo	Dacela	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Halcyon	Halcyon	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Lacedo	Lacedo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Melidora	Melidora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Pelargopsis	Pelargopsis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Syma	Syma	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Tanysiptera	Tanysipter	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Todirhampus	Todirhampus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Rybařík	rybařík	k1gMnSc1
(	(	kIx(
<g/>
Ceryle	Ceryl	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Rybařík	rybařík	k1gMnSc1
(	(	kIx(
<g/>
Chloroceryle	Chloroceryl	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Rybařík	rybařík	k1gMnSc1
(	(	kIx(
<g/>
Megaceryle	Megaceryl	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Alcedo	Alcedo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1
(	(	kIx(
<g/>
Ceyx	Ceyx	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Ledňáčci	ledňáček	k1gMnPc1
mají	mít	k5eAaImIp3nP
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
jíst	jíst	k5eAaImF
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
korýše	korýš	k1gMnPc4
<g/>
,	,	kIx,
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
malé	malý	k2eAgMnPc4d1
plazy	plaz	k1gMnPc4
<g/>
,	,	kIx,
obojživelníky	obojživelník	k1gMnPc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnPc4
pulce	pulec	k1gMnPc4
i	i	k8xC
malé	malý	k2eAgMnPc4d1
savce	savec	k1gMnPc4
a	a	k8xC
malé	malý	k2eAgMnPc4d1
ptáky	pták	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známí	známý	k1gMnPc1
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
pro	pro	k7c4
lov	lov	k1gInSc4
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
slétávají	slétávat	k5eAaImIp3nP
střemhlav	střemhlav	k6eAd1
do	do	k7c2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
chytí	chytit	k5eAaPmIp3nS
rybu	ryba	k1gFnSc4
a	a	k8xC
následně	následně	k6eAd1
s	s	k7c7
ní	on	k3xPp3gFnSc7
odletí	odletět	k5eAaPmIp3nP
zpátky	zpátky	k6eAd1
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
seděli	sedět	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kořist	kořist	k1gFnSc1
usmrtí	usmrtit	k5eAaPmIp3nS
údery	úder	k1gInPc4
o	o	k7c4
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Všichni	všechen	k3xTgMnPc1
ledňáčci	ledňáček	k1gMnPc1
hnízdí	hnízdit	k5eAaImIp3nP
v	v	k7c6
hlubokých	hluboký	k2eAgFnPc6d1
zemních	zemní	k2eAgFnPc6d1
norách	nora	k1gFnPc6
vyhrabaných	vyhrabaný	k2eAgInPc2d1
ve	v	k7c6
svislých	svislý	k2eAgFnPc6d1
hlinitých	hlinitý	k2eAgFnPc6d1
nebo	nebo	k8xC
písčitých	písčitý	k2eAgFnPc6d1
stěnách	stěna	k1gFnPc6
<g/>
,	,	kIx,
často	často	k6eAd1
u	u	k7c2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jejich	jejich	k3xOp3gInSc4
konec	konec	k1gInSc4
kladou	klást	k5eAaImIp3nP
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
druhu	druh	k1gInSc6
od	od	k7c2
2	#num#	k4
do	do	k7c2
7	#num#	k4
kulovitých	kulovitý	k2eAgInPc2d1
<g/>
,	,	kIx,
leskle	leskle	k6eAd1
bílých	bílý	k2eAgNnPc2d1
vajíček	vajíčko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Chad	Chad	k1gMnSc1
Eliason	Eliason	k1gMnSc1
<g/>
,	,	kIx,
Jenna	Jenn	k1gMnSc2
M.	M.	kA
McCullough	McCullough	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
J.	J.	kA
Andersen	Andersen	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Shannon	Shannon	k1gMnSc1
J.	J.	kA
Hackett	Hackett	k1gMnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accelerated	Accelerated	k1gMnSc1
brain	brain	k1gMnSc1
shape	shapat	k5eAaPmIp3nS
evolution	evolution	k1gInSc4
is	is	k?
associated	associated	k1gInSc1
with	with	k1gInSc1
rapid	rapid	k1gInSc1
diversification	diversification	k1gInSc1
in	in	k?
an	an	k?
avian	avian	k1gInSc1
radiation	radiation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Naturalist	Naturalist	k1gMnSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1086/713664	https://doi.org/10.1086/713664	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
</s>
