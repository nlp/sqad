<s>
Matěj	Matěj	k1gMnSc1	Matěj
Hollan	Hollan	k1gMnSc1	Hollan
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1984	[number]	k4	1984
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
občanský	občanský	k2eAgMnSc1d1	občanský
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
,	,	kIx,	,
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
3	[number]	k4	3
<g/>
.	.	kIx.	.
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
<g/>
)	)	kIx)	)
a	a	k8xC	a
Městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
politického	politický	k2eAgNnSc2d1	politické
hnutí	hnutí	k1gNnSc2	hnutí
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
Hollan	Hollan	k1gMnSc1	Hollan
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
ekologických	ekologický	k2eAgMnPc2d1	ekologický
aktivistů	aktivista	k1gMnPc2	aktivista
Jana	Jan	k1gMnSc2	Jan
Hollana	Hollan	k1gMnSc2	Hollan
a	a	k8xC	a
Yvonny	Yvonna	k1gFnSc2	Yvonna
Gaillyové	Gaillyová	k1gFnSc2	Gaillyová
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Biskupské	biskupský	k2eAgNnSc4d1	Biskupské
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
obor	obor	k1gInSc1	obor
muzikologie	muzikologie	k1gFnSc2	muzikologie
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
předsedou	předseda	k1gMnSc7	předseda
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Brnění	brnění	k1gNnSc2	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
známým	známý	k1gMnSc7	známý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgFnPc7	svůj
aktivitami	aktivita	k1gFnPc7	aktivita
za	za	k7c4	za
regulaci	regulace	k1gFnSc4	regulace
hazardu	hazard	k1gInSc2	hazard
a	a	k8xC	a
některými	některý	k3yIgNnPc7	některý
kontroverzními	kontroverzní	k2eAgNnPc7d1	kontroverzní
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
o	o	k7c4	o
podzemní	podzemní	k2eAgFnPc4d1	podzemní
garáže	garáž	k1gFnPc4	garáž
pod	pod	k7c7	pod
Zelným	zelný	k2eAgInSc7d1	zelný
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
spor	spor	k1gInSc1	spor
o	o	k7c4	o
odsun	odsun	k1gInSc4	odsun
brněnského	brněnský	k2eAgNnSc2d1	brněnské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
koalice	koalice	k1gFnSc2	koalice
Nádraží	nádraží	k1gNnPc2	nádraží
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
podporuje	podporovat	k5eAaImIp3nS	podporovat
zachování	zachování	k1gNnSc4	zachování
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
podobu	podoba	k1gFnSc4	podoba
brněnského	brněnský	k2eAgInSc2d1	brněnský
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Publikuje	publikovat	k5eAaBmIp3nS	publikovat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
jak	jak	k8xS	jak
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
portálu	portál	k1gInSc6	portál
iHNed	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
satirického	satirický	k2eAgInSc2d1	satirický
portálu	portál	k1gInSc2	portál
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Facebooku	Facebook	k1gInSc2	Facebook
ostře	ostro	k6eAd1	ostro
vymezoval	vymezovat	k5eAaImAgMnS	vymezovat
proti	proti	k7c3	proti
kandidátům	kandidát	k1gMnPc3	kandidát
Miloši	Miloš	k1gMnSc3	Miloš
Zemanovi	Zeman	k1gMnSc3	Zeman
a	a	k8xC	a
Janu	Jan	k1gMnSc3	Jan
Fischerovi	Fischer	k1gMnSc3	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
agitační	agitační	k2eAgNnSc1d1	agitační
video	video	k1gNnSc1	video
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
odstranit	odstranit	k5eAaPmF	odstranit
Zemana	Zeman	k1gMnSc4	Zeman
s	s	k7c7	s
Fischerem	Fischer	k1gMnSc7	Fischer
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
vidělo	vidět	k5eAaImAgNnS	vidět
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
během	během	k7c2	během
týdne	týden	k1gInSc2	týden
přes	přes	k7c4	přes
360	[number]	k4	360
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
zablokován	zablokován	k2eAgInSc4d1	zablokován
facebookový	facebookový	k2eAgInSc4d1	facebookový
účet	účet	k1gInSc4	účet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
sám	sám	k3xTgMnSc1	sám
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
právě	právě	k9	právě
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
předvolebními	předvolební	k2eAgFnPc7d1	předvolební
aktivitami	aktivita	k1gFnPc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
písemném	písemný	k2eAgNnSc6d1	písemné
prokázání	prokázání	k1gNnSc6	prokázání
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
vlastní	vlastní	k2eAgInSc1d1	vlastní
účet	účet	k1gInSc1	účet
není	být	k5eNaImIp3nS	být
falešný	falešný	k2eAgInSc1d1	falešný
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
účet	účet	k1gInSc1	účet
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cenu	cena	k1gFnSc4	cena
Františka	František	k1gMnSc2	František
Kriegla	Kriegl	k1gMnSc2	Kriegl
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
uděluje	udělovat	k5eAaImIp3nS	udělovat
Nadace	nadace	k1gFnSc1	nadace
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
za	za	k7c4	za
občanskou	občanský	k2eAgFnSc4d1	občanská
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Matěje	Matěj	k1gMnSc2	Matěj
Hollana	Hollan	k1gMnSc2	Hollan
ocenila	ocenit	k5eAaPmAgNnP	ocenit
"	"	kIx"	"
<g/>
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
občanského	občanský	k2eAgInSc2d1	občanský
aktivismu	aktivismus	k1gInSc2	aktivismus
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
nelegálnímu	legální	k2eNgInSc3d1	nelegální
hazardu	hazard	k1gInSc3	hazard
a	a	k8xC	a
korupci	korupce	k1gFnSc3	korupce
a	a	k8xC	a
prosazování	prosazování	k1gNnSc6	prosazování
svobody	svoboda	k1gFnSc2	svoboda
informací	informace	k1gFnPc2	informace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
zelených	zelená	k1gFnPc2	zelená
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
Strany	strana	k1gFnSc2	strana
zelených	zelené	k1gNnPc2	zelené
byl	být	k5eAaImAgInS	být
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Bursíkem	Bursík	k1gMnSc7	Bursík
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
nové	nový	k2eAgFnSc2d1	nová
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
LES	les	k1gInSc1	les
(	(	kIx(	(
<g/>
Liberálně	liberálně	k6eAd1	liberálně
ekologická	ekologický	k2eAgFnSc1d1	ekologická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
politické	politický	k2eAgNnSc4d1	politické
hnutí	hnutí	k1gNnSc4	hnutí
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
základech	základ	k1gInPc6	základ
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
satirického	satirický	k2eAgInSc2d1	satirický
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
komunální	komunální	k2eAgFnPc4d1	komunální
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lídrem	lídr	k1gMnSc7	lídr
subjektu	subjekt	k1gInSc2	subjekt
"	"	kIx"	"
<g/>
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Pirátů	pirát	k1gMnPc2	pirát
<g/>
"	"	kIx"	"
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
kandidátem	kandidát	k1gMnSc7	kandidát
uskupení	uskupení	k1gNnSc4	uskupení
na	na	k7c4	na
post	post	k1gInSc4	post
brněnského	brněnský	k2eAgMnSc2d1	brněnský
primátora	primátor	k1gMnSc2	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k8xS	jako
lídr	lídr	k1gMnSc1	lídr
politického	politický	k2eAgNnSc2d1	politické
hnutí	hnutí	k1gNnSc2	hnutí
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
obou	dva	k4xCgFnPc2	dva
kandidátek	kandidátka	k1gFnPc2	kandidátka
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
volebnímu	volební	k2eAgInSc3d1	volební
úspěchu	úspěch	k1gInSc3	úspěch
hnutí	hnutí	k1gNnSc2	hnutí
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
obou	dva	k4xCgInPc2	dva
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
náměstkem	náměstek	k1gMnSc7	náměstek
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
současně	současně	k6eAd1	současně
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
3	[number]	k4	3
<g/>
.	.	kIx.	.
náměstkem	náměstek	k1gMnSc7	náměstek
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
měl	mít	k5eAaImAgMnS	mít
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
gesci	gesce	k1gFnSc6	gesce
místo	místo	k7c2	místo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
časovou	časový	k2eAgFnSc7d1	časová
vytížeností	vytíženost	k1gFnSc7	vytíženost
spjatou	spjatý	k2eAgFnSc7d1	spjatá
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
náměstka	náměstek	k1gMnSc2	náměstek
primátora	primátor	k1gMnSc2	primátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
radě	rada	k1gFnSc6	rada
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
zastupitel	zastupitel	k1gMnSc1	zastupitel
zvolený	zvolený	k2eAgMnSc1d1	zvolený
za	za	k7c4	za
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
Michal	Michal	k1gMnSc1	Michal
Doležel	Doležel	k1gMnSc1	Doležel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníčku	deníček	k1gInSc6	deníček
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
psal	psát	k5eAaImAgInS	psát
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
výstavy	výstava	k1gFnSc2	výstava
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
vícero	vícero	k1gNnSc1	vícero
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
dílem	dílem	k6eAd1	dílem
zplynovali	zplynovat	k5eAaPmAgMnP	zplynovat
<g/>
,	,	kIx,	,
dílem	dílem	k6eAd1	dílem
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
výrok	výrok	k1gInSc1	výrok
pobouřil	pobouřit	k5eAaPmAgInS	pobouřit
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
strhla	strhnout	k5eAaPmAgFnS	strhnout
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
ostrá	ostrý	k2eAgFnSc1d1	ostrá
debata	debata	k1gFnSc1	debata
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
ho	on	k3xPp3gInSc4	on
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
omluvě	omluva	k1gFnSc3	omluva
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dle	dle	k7c2	dle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
Češi	česat	k5eAaImIp1nS	česat
nikoho	nikdo	k3yNnSc4	nikdo
do	do	k7c2	do
plynu	plyn	k1gInSc2	plyn
neposlali	poslat	k5eNaPmAgMnP	poslat
<g/>
.	.	kIx.	.
</s>
<s>
Hollan	Hollan	k1gMnSc1	Hollan
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
omluvit	omluvit	k5eAaPmF	omluvit
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
za	za	k7c4	za
zkratkovitost	zkratkovitost	k1gFnSc4	zkratkovitost
svého	svůj	k3xOyFgInSc2	svůj
výroku	výrok	k1gInSc2	výrok
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Češi	Čech	k1gMnPc1	Čech
nemohli	moct	k5eNaImAgMnP	moct
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
dozorci	dozorce	k1gMnSc3	dozorce
v	v	k7c6	v
koncentráku	koncentrák	k1gInSc6	koncentrák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakkoli	jakkoli	k8xS	jakkoli
byli	být	k5eAaImAgMnP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
národy	národ	k1gInPc7	národ
rozdmýchávači	rozdmýchávač	k1gMnPc7	rozdmýchávač
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
soudu	soud	k1gInSc6	soud
přímou	přímý	k2eAgFnSc4d1	přímá
vinu	vina	k1gFnSc4	vina
na	na	k7c6	na
romském	romský	k2eAgInSc6d1	romský
holokaustu	holokaust	k1gInSc6	holokaust
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
zplynování	zplynování	k1gNnSc1	zplynování
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ho	on	k3xPp3gMnSc4	on
Češi	Čech	k1gMnPc1	Čech
nevykonali	vykonat	k5eNaPmAgMnP	vykonat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
video	video	k1gNnSc1	video
z	z	k7c2	z
Hollanovy	Hollanův	k2eAgFnSc2d1	Hollanův
návštěvy	návštěva	k1gFnSc2	návštěva
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
bičovat	bičovat	k5eAaImF	bičovat
důtkami	důtka	k1gFnPc7	důtka
a	a	k8xC	a
svojí	svojit	k5eAaImIp3nP	svojit
"	"	kIx"	"
<g/>
trýznitelce	trýznitelka	k1gFnSc6	trýznitelka
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
anglickými	anglický	k2eAgNnPc7d1	anglické
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
I	i	k9	i
want	want	k1gInSc1	want
more	mor	k1gInSc5	mor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
víc	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
říká	říkat	k5eAaImIp3nS	říkat
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
rány	rána	k1gFnPc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
hnutí	hnutí	k1gNnSc4	hnutí
Žít	žít	k5eAaImF	žít
Brno	Brno	k1gNnSc4	Brno
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
sloganu	slogan	k1gInSc2	slogan
učinilo	učinit	k5eAaPmAgNnS	učinit
recesistickou	recesistický	k2eAgFnSc4d1	recesistická
reklamu	reklama	k1gFnSc4	reklama
na	na	k7c6	na
webu	web	k1gInSc6	web
iwantmore	iwantmor	k1gInSc5	iwantmor
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
podzimními	podzimní	k2eAgFnPc7d1	podzimní
krajskými	krajský	k2eAgFnPc7d1	krajská
a	a	k8xC	a
senátními	senátní	k2eAgFnPc7d1	senátní
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
média	médium	k1gNnSc2	médium
zabývala	zabývat	k5eAaImAgFnS	zabývat
incidentem	incident	k1gInSc7	incident
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Strany	strana	k1gFnSc2	strana
zelených	zelené	k1gNnPc2	zelené
Matějem	Matěj	k1gMnSc7	Matěj
Stropnickým	stropnický	k2eAgMnSc7d1	stropnický
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInPc4	jejich
názory	názor	k1gInPc4	názor
na	na	k7c4	na
imigraci	imigrace	k1gFnSc4	imigrace
vyháněla	vyhánět	k5eAaImAgFnS	vyhánět
z	z	k7c2	z
brněnského	brněnský	k2eAgInSc2d1	brněnský
hostince	hostinec	k1gInSc2	hostinec
U	u	k7c2	u
Poutníka	poutník	k1gMnSc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Hollan	Hollan	k1gMnSc1	Hollan
tam	tam	k6eAd1	tam
poté	poté	k6eAd1	poté
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
sezval	sezvat	k5eAaPmAgMnS	sezvat
příznivce	příznivec	k1gMnPc4	příznivec
k	k	k7c3	k
debatování	debatování	k1gNnSc3	debatování
nad	nad	k7c7	nad
pivem	pivo	k1gNnSc7	pivo
<g/>
,	,	kIx,	,
provozovatel	provozovatel	k1gMnSc1	provozovatel
však	však	k9	však
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
podnik	podnik	k1gInSc4	podnik
mimořádně	mimořádně	k6eAd1	mimořádně
zavřel	zavřít	k5eAaPmAgMnS	zavřít
<g/>
.	.	kIx.	.
</s>
<s>
Desítky	desítka	k1gFnPc1	desítka
radikálních	radikální	k2eAgMnPc2d1	radikální
krajně	krajně	k6eAd1	krajně
pravicových	pravicový	k2eAgMnPc2d1	pravicový
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
rovněž	rovněž	k9	rovněž
chystali	chystat	k5eAaImAgMnP	chystat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
prošly	projít	k5eAaPmAgFnP	projít
městem	město	k1gNnSc7	město
k	k	k7c3	k
Hollanovu	Hollanův	k2eAgNnSc3d1	Hollanův
bydlišti	bydliště	k1gNnSc3	bydliště
<g/>
.	.	kIx.	.
</s>
<s>
Primátor	primátor	k1gMnSc1	primátor
Brna	Brno	k1gNnSc2	Brno
Petr	Petr	k1gMnSc1	Petr
Vokřál	Vokřál	k1gMnSc1	Vokřál
to	ten	k3xDgNnSc4	ten
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
trestnou	trestný	k2eAgFnSc4d1	trestná
výpravu	výprava	k1gFnSc4	výprava
bandy	banda	k1gFnSc2	banda
rváčů	rváč	k1gMnPc2	rváč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
Hollan	Hollan	k1gMnSc1	Hollan
jakožto	jakožto	k8xS	jakožto
náměstek	náměstek	k1gMnSc1	náměstek
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
přistižen	přistižen	k2eAgInSc1d1	přistižen
revizory	revizor	k1gMnPc7	revizor
při	při	k7c6	při
nočním	noční	k2eAgInSc6d1	noční
zátahu	zátah	k1gInSc6	zátah
na	na	k7c4	na
černé	černý	k2eAgMnPc4d1	černý
pasažéry	pasažér	k1gMnPc4	pasažér
v	v	k7c6	v
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
platnou	platný	k2eAgFnSc4d1	platná
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
,	,	kIx,	,
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
amnestií	amnestie	k1gFnSc7	amnestie
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
pořídil	pořídit	k5eAaPmAgMnS	pořídit
čtvrtletní	čtvrtletní	k2eAgFnSc4d1	čtvrtletní
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matěj	Matěj	k1gMnSc1	Matěj
Hollan	Hollany	k1gInPc2	Hollany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Matěj	Matěj	k1gMnSc1	Matěj
Hollan	Hollan	k1gMnSc1	Hollan
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Krásný	krásný	k2eAgInSc1d1	krásný
ztráty	ztráta	k1gFnPc4	ztráta
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Matěj	Matěj	k1gMnSc1	Matěj
Hollan	Hollan	k1gMnSc1	Hollan
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Hydepark	Hydepark	k1gInSc1	Hydepark
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Blog	Blog	k1gInSc1	Blog
Matěje	Matěj	k1gMnSc2	Matěj
Hollana	Hollana	k1gFnSc1	Hollana
na	na	k7c4	na
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Blog	Blog	k1gInSc1	Blog
Matěje	Matěj	k1gMnSc2	Matěj
Hollana	Hollan	k1gMnSc2	Hollan
na	na	k7c4	na
Respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Matěj	Matěj	k1gMnSc1	Matěj
Hollan	Hollan	k1gMnSc1	Hollan
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Bém	Bém	k1gMnSc1	Bém
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Pro	pro	k7c4	pro
a	a	k8xC	a
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
