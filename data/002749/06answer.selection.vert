<s>
Elbrus	Elbrus	k1gInSc1	Elbrus
(	(	kIx(	(
<g/>
balkarsky	balkarsky	k6eAd1	balkarsky
М	М	k?	М
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Э	Э	k?	Э
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Mount	Mount	k1gMnSc1	Mount
Elbrus	Elbrus	k1gMnSc1	Elbrus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
5642	[number]	k4	5642
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
horou	hora	k1gFnSc7	hora
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
