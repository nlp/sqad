<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
nazývána	nazýván	k2eAgFnSc1d1	nazývána
též	též	k9	též
Latinská	latinský	k2eAgFnSc1d1	Latinská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
často	často	k6eAd1	často
označovaná	označovaný	k2eAgFnSc1d1	označovaná
nepřesně	přesně	k6eNd1	přesně
jako	jako	k8xS	jako
katolická	katolický	k2eAgFnSc1d1	katolická
(	(	kIx(	(
<g/>
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnSc1d1	univerzální
<g/>
)	)	kIx)	)
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
třiadvaceti	třiadvacet	k4xCc2	třiadvacet
autonomních	autonomní	k2eAgFnPc2d1	autonomní
katolických	katolický	k2eAgFnPc2d1	katolická
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
západní	západní	k2eAgFnSc7d1	západní
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
