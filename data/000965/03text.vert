<s>
Antoine	Antoinout	k5eAaPmIp3nS	Antoinout
Marie	Marie	k1gFnSc1	Marie
Roger	Rogero	k1gNnPc2	Rogero
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnSc2	Saint-Exupéra
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Antoine	Antoin	k1gInSc5	Antoin
Marie	Maria	k1gFnSc2	Maria
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Roger	Roger	k1gMnSc1	Roger
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
de	de	k?	de
Saint	Saint	k1gMnSc1	Saint
Exupéry	Exupéra	k1gFnSc2	Exupéra
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
Lyon	Lyon	k1gInSc1	Lyon
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
také	také	k9	také
za	za	k7c4	za
filosofa	filosof	k1gMnSc4	filosof
a	a	k8xC	a
humanistu	humanista	k1gMnSc4	humanista
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
Země	zem	k1gFnPc1	zem
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
Citadela	citadela	k1gFnSc1	citadela
nebo	nebo	k8xC	nebo
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnSc2	Saint-Exupéra
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
inspektorem	inspektor	k1gMnSc7	inspektor
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
de	de	k?	de
Fonscolombe	Fonscolomb	k1gInSc5	Fonscolomb
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dvou	dva	k4xCgFnPc2	dva
dcer	dcera	k1gFnPc2	dcera
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Antoine	Antoin	k1gInSc5	Antoin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
přibyl	přibýt	k5eAaPmAgMnS	přibýt
bratr	bratr	k1gMnSc1	bratr
François	François	k1gFnSc2	François
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
Gabriely	Gabriela	k1gFnSc2	Gabriela
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnSc2	Saint-Exupéra
<g/>
,	,	kIx,	,
Antoinův	Antoinův	k2eAgMnSc1d1	Antoinův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
děti	dítě	k1gFnPc1	dítě
měly	mít	k5eAaImAgFnP	mít
šťastné	šťastný	k2eAgNnSc4d1	šťastné
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
trávily	trávit	k5eAaImAgFnP	trávit
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
u	u	k7c2	u
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
u	u	k7c2	u
její	její	k3xOp3gFnSc2	její
pratety	prateta	k1gFnSc2	prateta
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Saint-Maurice-de-Rémens	Saint-Mauricee-Rémens	k1gInSc1	Saint-Maurice-de-Rémens
<g/>
.	.	kIx.	.
</s>
<s>
Rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
park	park	k1gInSc1	park
zdejšího	zdejší	k2eAgInSc2d1	zdejší
zámku	zámek	k1gInSc2	zámek
se	se	k3xPyFc4	se
vryl	vrýt	k5eAaPmAgMnS	vrýt
všem	všecek	k3xTgFnPc3	všecek
dětem	dítě	k1gFnPc3	dítě
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
na	na	k7c4	na
šťastné	šťastný	k2eAgFnPc4d1	šťastná
chvíle	chvíle	k1gFnPc4	chvíle
zde	zde	k6eAd1	zde
prožité	prožitý	k2eAgFnPc4d1	prožitá
vzpomínal	vzpomínat	k5eAaImAgInS	vzpomínat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
velmi	velmi	k6eAd1	velmi
rád	rád	k6eAd1	rád
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
útočištěm	útočiště	k1gNnSc7	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
Sluníčko	sluníčko	k1gNnSc4	sluníčko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
modré	modré	k1gNnSc1	modré
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
blonďaté	blonďatý	k2eAgInPc4d1	blonďatý
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
docházel	docházet	k5eAaImAgInS	docházet
do	do	k7c2	do
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
psal	psát	k5eAaImAgMnS	psát
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
"	"	kIx"	"
<g/>
vynalézáním	vynalézání	k1gNnSc7	vynalézání
<g/>
"	"	kIx"	"
a	a	k8xC	a
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
létající	létající	k2eAgFnPc4d1	létající
piloty	pilota	k1gFnPc4	pilota
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úderem	úder	k1gInSc7	úder
válečného	válečný	k2eAgInSc2d1	válečný
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
paní	paní	k1gFnSc1	paní
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnSc2	Saint-Exupéra
stala	stát	k5eAaPmAgFnS	stát
ošetřovatelkou	ošetřovatelka	k1gFnSc7	ošetřovatelka
v	v	k7c6	v
Amberieu	Amberieum	k1gNnSc6	Amberieum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
studovali	studovat	k5eAaImAgMnP	studovat
oba	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
ve	v	k7c6	v
Fribourgu	Fribourg	k1gInSc6	Fribourg
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
složil	složit	k5eAaPmAgMnS	složit
maturitu	maturita	k1gFnSc4	maturita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgMnS	připravovat
na	na	k7c6	na
složení	složení	k1gNnSc6	složení
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c4	na
námořní	námořní	k2eAgFnSc4d1	námořní
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
u	u	k7c2	u
zkoušek	zkouška	k1gFnPc2	zkouška
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
velkého	velký	k2eAgInSc2d1	velký
zájmu	zájem	k1gInSc2	zájem
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
a	a	k8xC	a
bouřlivě	bouřlivě	k6eAd1	bouřlivě
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
v	v	k7c6	v
kavárnách	kavárna	k1gFnPc6	kavárna
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
příbuzné	příbuzná	k1gFnSc3	příbuzná
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
se	s	k7c7	s
spisovatelskou	spisovatelský	k2eAgFnSc7d1	spisovatelská
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgInS	seznámit
se	se	k3xPyFc4	se
také	také	k9	také
s	s	k7c7	s
Louisou	Louisý	k2eAgFnSc7d1	Louisý
Vilmorinovou	Vilmorinová	k1gFnSc7	Vilmorinová
a	a	k8xC	a
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
byl	být	k5eAaImAgInS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
k	k	k7c3	k
letectvu	letectvo	k1gNnSc3	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
letectví	letectví	k1gNnSc2	letectví
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
výcvik	výcvik	k1gInSc4	výcvik
na	na	k7c4	na
pilota	pilot	k1gMnSc4	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
diplom	diplom	k1gInSc4	diplom
civilního	civilní	k2eAgNnSc2d1	civilní
i	i	k8xC	i
vojenského	vojenský	k2eAgMnSc2d1	vojenský
pilota	pilot	k1gMnSc2	pilot
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
těžce	těžce	k6eAd1	těžce
zraněn	zranit	k5eAaPmNgMnS	zranit
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Louisa	Louisa	k?	Louisa
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
si	se	k3xPyFc3	se
nepřály	přát	k5eNaImAgFnP	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
rozchodu	rozchod	k1gInSc3	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
obchodní	obchodní	k2eAgMnSc1d1	obchodní
zástupce	zástupce	k1gMnSc1	zástupce
automobilové	automobilový	k2eAgFnSc2d1	automobilová
firmy	firma	k1gFnSc2	firma
Sauer	Saura	k1gFnPc2	Saura
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
úspěšně	úspěšně	k6eAd1	úspěšně
debutoval	debutovat	k5eAaBmAgInS	debutovat
svou	svůj	k3xOyFgFnSc7	svůj
povídkou	povídka	k1gFnSc7	povídka
Letec	letec	k1gMnSc1	letec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
letecké	letecký	k2eAgFnSc3d1	letecká
společnosti	společnost	k1gFnSc3	společnost
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
musel	muset	k5eAaImAgMnS	muset
začínat	začínat	k5eAaImF	začínat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
rád	rád	k6eAd1	rád
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
prý	prý	k9	prý
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
zkušenostem	zkušenost	k1gFnPc3	zkušenost
vracel	vracet	k5eAaImAgMnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
stanice	stanice	k1gFnSc2	stanice
Juba	Jub	k1gInSc2	Jub
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
velkorysou	velkorysý	k2eAgFnSc7d1	velkorysá
povahou	povaha	k1gFnSc7	povaha
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zlepšit	zlepšit	k5eAaPmF	zlepšit
místní	místní	k2eAgInPc4d1	místní
napjaté	napjatý	k2eAgInPc4d1	napjatý
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Třináct	třináct	k4xCc1	třináct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
zde	zde	k6eAd1	zde
prožil	prožít	k5eAaPmAgMnS	prožít
<g/>
,	,	kIx,	,
zužitkoval	zužitkovat	k5eAaPmAgMnS	zužitkovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Kurýr	kurýr	k1gMnSc1	kurýr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
(	(	kIx(	(
<g/>
také	také	k9	také
Pošta	pošta	k1gFnSc1	pošta
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
nápisu	nápis	k1gInSc2	nápis
na	na	k7c6	na
pytlích	pytel	k1gInPc6	pytel
s	s	k7c7	s
poštou	pošta	k1gFnSc7	pošta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
se	se	k3xPyFc4	se
nezastavila	zastavit	k5eNaPmAgFnS	zastavit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
kamaráda	kamarád	k1gMnSc4	kamarád
H.	H.	kA	H.
Guillaumeta	Guillaumet	k1gMnSc4	Guillaumet
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
při	při	k7c6	při
přeletu	přelet	k1gInSc6	přelet
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
knihy	kniha	k1gFnSc2	kniha
Noční	noční	k2eAgInSc4d1	noční
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1931	[number]	k4	1931
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
významný	významný	k2eAgInSc1d1	významný
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Consuelou	Consuela	k1gFnSc7	Consuela
Suncin	Suncin	k1gMnSc1	Suncin
de	de	k?	de
Sandoval	Sandoval	k1gFnSc1	Sandoval
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poznal	poznat	k5eAaPmAgMnS	poznat
koncem	koncem	k7c2	koncem
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Noční	noční	k2eAgFnSc1d1	noční
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vyzdvihující	vyzdvihující	k2eAgFnSc4d1	vyzdvihující
sílu	síla	k1gFnSc4	síla
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
Rytířem	Rytíř	k1gMnSc7	Rytíř
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
a	a	k8xC	a
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
členem	člen	k1gMnSc7	člen
literárních	literární	k2eAgInPc2d1	literární
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
čelil	čelit	k5eAaImAgMnS	čelit
často	často	k6eAd1	často
finančním	finanční	k2eAgInPc3d1	finanční
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
si	se	k3xPyFc3	se
i	i	k8xC	i
reportážemi	reportáž	k1gFnPc7	reportáž
<g/>
,	,	kIx,	,
např.	např.	kA	např.
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgMnS	vyslat
jako	jako	k9	jako
reportér	reportér	k1gMnSc1	reportér
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
zuřila	zuřit	k5eAaImAgFnS	zuřit
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Prévotem	Prévot	k1gMnSc7	Prévot
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
dálkový	dálkový	k2eAgInSc4d1	dálkový
let	let	k1gInSc4	let
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
-	-	kIx~	-
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
letecké	letecký	k2eAgFnSc3d1	letecká
havárii	havárie	k1gFnSc3	havárie
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
četné	četný	k2eAgFnPc4d1	četná
zlomeniny	zlomenina	k1gFnPc4	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Země	zem	k1gFnSc2	zem
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
Velkou	velký	k2eAgFnSc4d1	velká
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
lékaři	lékař	k1gMnSc3	lékař
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
neschopného	schopný	k2eNgMnSc4d1	neschopný
válečných	válečný	k2eAgInPc2d1	válečný
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
usiloval	usilovat	k5eAaImAgMnS	usilovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
aktivně	aktivně	k6eAd1	aktivně
zapojit	zapojit	k5eAaPmF	zapojit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědků	svědek	k1gMnPc2	svědek
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
Groupe	Group	k1gInSc5	Group
de	de	k?	de
reconnaissance	reconnaissanec	k1gInPc4	reconnaissanec
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejodvážnějších	odvážný	k2eAgMnPc2d3	nejodvážnější
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
nasazením	nasazení	k1gNnSc7	nasazení
a	a	k8xC	a
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
dopisů	dopis	k1gInPc2	dopis
je	být	k5eAaImIp3nS	být
však	však	k9	však
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgMnS	nést
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
slávu	sláva	k1gFnSc4	sláva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
hrdinství	hrdinství	k1gNnSc4	hrdinství
letců	letec	k1gMnPc2	letec
popisoval	popisovat	k5eAaImAgMnS	popisovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
paradoxně	paradoxně	k6eAd1	paradoxně
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
zrádce	zrádce	k1gMnPc4	zrádce
ideálů	ideál	k1gInPc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
Válečný	válečný	k2eAgMnSc1d1	válečný
pilot	pilot	k1gMnSc1	pilot
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
fenomenální	fenomenální	k2eAgInSc1d1	fenomenální
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
mu	on	k3xPp3gInSc3	on
také	také	k9	také
zazlívali	zazlívat	k5eAaImAgMnP	zazlívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
de	de	k?	de
Gaullem	Gaull	k1gInSc7	Gaull
<g/>
,	,	kIx,	,
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
rozhlasovým	rozhlasový	k2eAgInSc7d1	rozhlasový
projevem	projev	k1gInSc7	projev
"	"	kIx"	"
<g/>
Francie	Francie	k1gFnSc1	Francie
především	především	k9	především
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
poštval	poštvat	k5eAaPmAgMnS	poštvat
většinu	většina	k1gFnSc4	většina
Francouzů	Francouz	k1gMnPc2	Francouz
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
vyšel	vyjít	k5eAaPmAgInS	vyjít
Dopis	dopis	k1gInSc1	dopis
rukojmímu	rukojmí	k1gMnSc3	rukojmí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
duchovní	duchovní	k2eAgFnPc4d1	duchovní
hodnoty	hodnota	k1gFnPc4	hodnota
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
předsudky	předsudek	k1gInPc4	předsudek
a	a	k8xC	a
proti	proti	k7c3	proti
rasismu	rasismus	k1gInSc3	rasismus
staví	stavit	k5eAaBmIp3nS	stavit
úctu	úcta	k1gFnSc4	úcta
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
vyšla	vyjít	k5eAaPmAgFnS	vyjít
rovněž	rovněž	k9	rovněž
proslulá	proslulý	k2eAgFnSc1d1	proslulá
filosofická	filosofický	k2eAgFnSc1d1	filosofická
pohádka	pohádka	k1gFnSc1	pohádka
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
překročil	překročit	k5eAaPmAgMnS	překročit
třiačtyřicetiletou	třiačtyřicetiletý	k2eAgFnSc4d1	třiačtyřicetiletá
věkovou	věkový	k2eAgFnSc4d1	věková
hranici	hranice	k1gFnSc4	hranice
válečného	válečný	k2eAgMnSc4d1	válečný
pilota	pilot	k1gMnSc4	pilot
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
povolení	povolení	k1gNnSc4	povolení
létat	létat	k5eAaImF	létat
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
typu	typ	k1gInSc6	typ
letadla	letadlo	k1gNnSc2	letadlo
Lockheed	Lockheed	k1gInSc4	Lockheed
F-5	F-5	k1gFnSc2	F-5
(	(	kIx(	(
<g/>
průzkumná	průzkumný	k2eAgFnSc1d1	průzkumná
verze	verze	k1gFnSc1	verze
dvoumotorové	dvoumotorový	k2eAgFnSc2d1	dvoumotorová
stíhačky	stíhačka	k1gFnSc2	stíhačka
P-38	P-38	k1gMnSc1	P-38
Lightning	Lightning	k1gInSc1	Lightning
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
průzkumných	průzkumný	k2eAgInPc6d1	průzkumný
letech	let	k1gInPc6	let
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgInS	dostat
kvůli	kvůli	k7c3	kvůli
nehodě	nehoda	k1gFnSc3	nehoda
zákaz	zákaz	k1gInSc1	zákaz
dalších	další	k2eAgInPc2d1	další
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgInS	odjet
tedy	tedy	k9	tedy
do	do	k7c2	do
Alžíru	Alžír	k1gInSc2	Alžír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtěl	chtít	k5eAaImAgMnS	chtít
dokončit	dokončit	k5eAaPmF	dokončit
Citadelu	citadela	k1gFnSc4	citadela
<g/>
,	,	kIx,	,
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
tvořit	tvořit	k5eAaImF	tvořit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
souhrn	souhrn	k1gInSc4	souhrn
jeho	jeho	k3xOp3gFnPc2	jeho
morálních	morální	k2eAgFnPc2d1	morální
a	a	k8xC	a
filosofických	filosofický	k2eAgFnPc2d1	filosofická
zásad	zásada	k1gFnPc2	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
generála	generál	k1gMnSc2	generál
dostal	dostat	k5eAaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
pěti	pět	k4xCc2	pět
dalších	další	k2eAgFnPc2d1	další
akcí	akce	k1gFnPc2	akce
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgMnS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
francouzským	francouzský	k2eAgNnSc7d1	francouzské
vedením	vedení	k1gNnSc7	vedení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
limit	limit	k1gInSc1	limit
však	však	k9	však
vysoce	vysoce	k6eAd1	vysoce
překročil	překročit	k5eAaPmAgMnS	překročit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
na	na	k7c4	na
devět	devět	k4xCc4	devět
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
z	z	k7c2	z
Korsiky	Korsika	k1gFnSc2	Korsika
k	k	k7c3	k
letu	let	k1gInSc3	let
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Desítky	desítka	k1gFnPc1	desítka
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
diskuze	diskuze	k1gFnPc1	diskuze
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vlastně	vlastně	k9	vlastně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgFnP	být
trosky	troska	k1gFnPc1	troska
jeho	on	k3xPp3gInSc2	on
letounu	letoun	k1gInSc2	letoun
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
poblíž	poblíž	k7c2	poblíž
francouzského	francouzský	k2eAgInSc2d1	francouzský
přístavu	přístav	k1gInSc2	přístav
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
stíhač	stíhač	k1gMnSc1	stíhač
německé	německý	k2eAgFnSc2d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
Horst	Horst	k1gMnSc1	Horst
Rippert	Rippert	k1gInSc1	Rippert
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
knize	kniha	k1gFnSc6	kniha
autorů	autor	k1gMnPc2	autor
J.	J.	kA	J.
Pradela	Pradela	k1gFnSc1	Pradela
a	a	k8xC	a
L.	L.	kA	L.
Vanrella	Vanrella	k1gFnSc1	Vanrella
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saint-Exupéryho	Saint-Exupéry	k1gMnSc4	Saint-Exupéry
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
právě	právě	k6eAd1	právě
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Historikovi	historikův	k2eAgMnPc1d1	historikův
Lino	Lina	k1gFnSc5	Lina
von	von	k1gInSc1	von
Gartzenovi	Gartzen	k1gMnSc3	Gartzen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pátral	pátrat	k5eAaImAgMnS	pátrat
po	po	k7c6	po
okolnostech	okolnost	k1gFnPc6	okolnost
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
který	který	k3yRgMnSc1	který
Ripperta	Rippert	k1gMnSc4	Rippert
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můžete	moct	k5eAaImIp2nP	moct
teď	teď	k6eAd1	teď
přestat	přestat	k5eAaPmF	přestat
pátrat	pátrat	k5eAaImF	pátrat
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
Exupéryho	Exupéry	k1gMnSc4	Exupéry
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvnitř	uvnitř	k6eAd1	uvnitř
sedí	sedit	k5eAaImIp3nS	sedit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mých	můj	k3xOp1gMnPc2	můj
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
bych	by	kYmCp1nS	by
nestřílel	střílet	k5eNaImAgMnS	střílet
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Rippertovo	Rippertův	k2eAgNnSc1d1	Rippertův
tvrzení	tvrzení	k1gNnSc1	tvrzení
není	být	k5eNaImIp3nS	být
podloženo	podložit	k5eAaPmNgNnS	podložit
dobovými	dobový	k2eAgInPc7d1	dobový
dokumenty	dokument	k1gInPc7	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
nárokovaných	nárokovaný	k2eAgInPc6d1	nárokovaný
sestřelech	sestřel	k1gInPc6	sestřel
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Francie	Francie	k1gFnSc2	Francie
jsou	být	k5eAaImIp3nP	být
kompletně	kompletně	k6eAd1	kompletně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Spojenecké	spojenecký	k2eAgInPc4d1	spojenecký
záznamy	záznam	k1gInPc4	záznam
odposlechů	odposlech	k1gInPc2	odposlech
německého	německý	k2eAgInSc2d1	německý
rádiového	rádiový	k2eAgInSc2d1	rádiový
provozu	provoz	k1gInSc2	provoz
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
letecké	letecký	k2eAgFnSc2d1	letecká
činnosti	činnost	k1gFnSc2	činnost
obou	dva	k4xCgFnPc2	dva
válčících	válčící	k2eAgFnPc2d1	válčící
stran	strana	k1gFnPc2	strana
nad	nad	k7c7	nad
jižní	jižní	k2eAgFnSc7d1	jižní
Francií	Francie	k1gFnSc7	Francie
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
boji	boj	k1gInSc3	boj
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nadále	nadále	k6eAd1	nadále
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pád	pád	k1gInSc1	pád
jeho	jeho	k3xOp3gInSc2	jeho
stroje	stroj	k1gInSc2	stroj
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
mohla	moct	k5eAaImAgFnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
technická	technický	k2eAgFnSc1d1	technická
závada	závada	k1gFnSc1	závada
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
indispozice	indispozice	k1gFnSc1	indispozice
<g/>
.	.	kIx.	.
</s>
<s>
Letec	letec	k1gMnSc1	letec
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Aviateur	Aviateur	k1gMnSc1	Aviateur
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Kurýr	kurýr	k1gMnSc1	kurýr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
(	(	kIx(	(
<g/>
Courrier	Courrier	k1gInSc4	Courrier
Sud	suda	k1gFnPc2	suda
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Noční	noční	k2eAgInSc4d1	noční
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
Vol	vol	k6eAd1	vol
de	de	k?	de
nuit	nuit	k1gMnSc1	nuit
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
Země	zem	k1gFnPc1	zem
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Terre	Terr	k1gInSc5	Terr
des	des	k1gNnPc1	des
hommes	hommes	k1gMnSc1	hommes
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
Válečný	válečný	k2eAgInSc1d1	válečný
<g />
.	.	kIx.	.
</s>
<s>
pilot	pilot	k1gMnSc1	pilot
(	(	kIx(	(
<g/>
Pilot	pilot	k1gMnSc1	pilot
de	de	k?	de
guerre	guerr	k1gMnSc5	guerr
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Petit	petit	k1gInSc4	petit
Prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Dopis	dopis	k1gInSc1	dopis
rukojmímu	rukojmí	k1gMnSc3	rukojmí
(	(	kIx(	(
<g/>
Lettre	Lettr	k1gInSc5	Lettr
à	à	k?	à
un	un	k?	un
otage	otage	k1gFnPc2	otage
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
Citadela	citadela	k1gFnSc1	citadela
(	(	kIx(	(
<g/>
Citadelle	Citadelle	k1gNnSc1	Citadelle
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokončeno	dokončen	k2eNgNnSc1d1	nedokončeno
Moje	můj	k3xOp1gFnSc1	můj
planeta	planeta	k1gFnSc1	planeta
Let	let	k1gInSc4	let
do	do	k7c2	do
Arrasu	Arras	k1gInSc2	Arras
</s>
