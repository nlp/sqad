<s>
Antoine	Antoinout	k5eAaPmIp3nS	Antoinout
Marie	Marie	k1gFnSc1	Marie
Roger	Rogero	k1gNnPc2	Rogero
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnSc2	Saint-Exupéra
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Antoine	Antoin	k1gInSc5	Antoin
Marie	Maria	k1gFnSc2	Maria
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Roger	Roger	k1gMnSc1	Roger
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
de	de	k?	de
Saint	Saint	k1gMnSc1	Saint
Exupéry	Exupéra	k1gFnSc2	Exupéra
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
Lyon	Lyon	k1gInSc1	Lyon
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
také	také	k9	také
za	za	k7c4	za
filosofa	filosof	k1gMnSc4	filosof
a	a	k8xC	a
humanistu	humanista	k1gMnSc4	humanista
<g/>
.	.	kIx.	.
</s>
