<p>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
vata	vata	k1gFnSc1	vata
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
cukrové	cukrový	k2eAgFnSc2d1	cukrová
sladkosti	sladkost	k1gFnSc2	sladkost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
typickou	typický	k2eAgFnSc7d1	typická
lehkou	lehký	k2eAgFnSc7d1	lehká
<g/>
,	,	kIx,	,
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
hmotou	hmota	k1gFnSc7	hmota
o	o	k7c6	o
různé	různý	k2eAgFnSc6d1	různá
barvě	barva	k1gFnSc6	barva
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
použitém	použitý	k2eAgNnSc6d1	Použité
barvivu	barvivo	k1gNnSc6	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
i	i	k9	i
na	na	k7c4	na
dotyk	dotyk	k1gInSc4	dotyk
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
vatě	vata	k1gFnSc3	vata
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
roztavením	roztavení	k1gNnSc7	roztavení
krystalického	krystalický	k2eAgInSc2d1	krystalický
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
karamel	karamel	k1gInSc4	karamel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vháněn	vhánět	k5eAaImNgInS	vhánět
do	do	k7c2	do
bubnu	buben	k1gInSc2	buben
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
a	a	k8xC	a
zpátky	zpátky	k6eAd1	zpátky
cukernatí	cukernatět	k5eAaImIp3nS	cukernatět
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
slabá	slabý	k2eAgNnPc1d1	slabé
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
namotávají	namotávat	k5eAaImIp3nP	namotávat
na	na	k7c4	na
špejli	špejle	k1gFnSc4	špejle
a	a	k8xC	a
podávají	podávat	k5eAaImIp3nP	podávat
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
pouťovou	pouťový	k2eAgFnSc4d1	pouťová
sladkost	sladkost	k1gFnSc4	sladkost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
u	u	k7c2	u
stánků	stánek	k1gInPc2	stánek
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
barva	barva	k1gFnSc1	barva
cukrové	cukrový	k2eAgFnSc2d1	cukrová
vaty	vata	k1gFnSc2	vata
je	být	k5eAaImIp3nS	být
růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zakoupit	zakoupit	k5eAaPmF	zakoupit
i	i	k9	i
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cukrová	cukrový	k2eAgFnSc1d1	cukrová
vata	vata	k1gFnSc1	vata
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
cukrová	cukrový	k2eAgFnSc1d1	cukrová
vata	vata	k1gFnSc1	vata
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
vata	vata	k1gFnSc1	vata
podporuje	podporovat	k5eAaImIp3nS	podporovat
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
</s>
</p>
