<s>
Arachnofobie	Arachnofobie	k1gFnSc1	Arachnofobie
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
fobie	fobie	k1gFnSc1	fobie
<g/>
,	,	kIx,	,
úzkostí	úzkost	k1gFnSc7	úzkost
provázený	provázený	k2eAgInSc1d1	provázený
abnormální	abnormální	k2eAgInSc1d1	abnormální
strach	strach	k1gInSc1	strach
z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
fobie	fobie	k1gFnPc1	fobie
obvykle	obvykle	k6eAd1	obvykle
provázející	provázející	k2eAgFnPc4d1	provázející
neurózy	neuróza	k1gFnPc4	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
arachnofobiků	arachnofobik	k1gMnPc2	arachnofobik
často	často	k6eAd1	často
připadají	připadat	k5eAaPmIp3nP	připadat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
iracionální	iracionální	k2eAgMnPc1d1	iracionální
(	(	kIx(	(
<g/>
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
trpícímu	trpící	k2eAgNnSc3d1	trpící
samotnému	samotný	k2eAgNnSc3d1	samotné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
arachnofobií	arachnofobie	k1gFnSc7	arachnofobie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
necítí	cítit	k5eNaImIp3nS	cítit
dobře	dobře	k6eAd1	dobře
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
pavouci	pavouk	k1gMnPc1	pavouk
nebo	nebo	k8xC	nebo
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
známky	známka	k1gFnPc1	známka
jejich	jejich	k3xOp3gFnSc2	jejich
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
vidí	vidět	k5eAaImIp3nP	vidět
pavouka	pavouk	k1gMnSc4	pavouk
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
ani	ani	k9	ani
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepřekonají	překonat	k5eNaPmIp3nP	překonat
panický	panický	k2eAgInSc4d1	panický
šok	šok	k1gInSc4	šok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
tuto	tento	k3xDgFnSc4	tento
fobii	fobie	k1gFnSc4	fobie
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
cítit	cítit	k5eAaImF	cítit
ponížení	ponížení	k1gNnSc4	ponížení
<g/>
,	,	kIx,	,
když	když	k8xS	když
taková	takový	k3xDgFnSc1	takový
situace	situace	k1gFnSc1	situace
nastane	nastat	k5eAaPmIp3nS	nastat
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
přihlížejících	přihlížející	k2eAgMnPc2d1	přihlížející
lidí	člověk	k1gMnPc2	člověk
nebo	nebo	k8xC	nebo
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
tohoto	tento	k3xDgInSc2	tento
chorobného	chorobný	k2eAgInSc2d1	chorobný
strachu	strach	k1gInSc2	strach
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
traumatický	traumatický	k2eAgInSc4d1	traumatický
zážitek	zážitek	k1gInSc4	zážitek
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
nápodoba	nápodoba	k1gFnSc1	nápodoba
chování	chování	k1gNnSc2	chování
rodičů	rodič	k1gMnPc2	rodič
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
bojí	bát	k5eAaImIp3nS	bát
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vysoce	vysoce	k6eAd1	vysoce
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
bát	bát	k5eAaImF	bát
taky	taky	k6eAd1	taky
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Při	při	k7c6	při
záchvatu	záchvat	k1gInSc6	záchvat
arachnofobie	arachnofobie	k1gFnSc2	arachnofobie
se	se	k3xPyFc4	se
dotyčného	dotyčný	k2eAgInSc2d1	dotyčný
začínají	začínat	k5eAaImIp3nP	začínat
projevovat	projevovat	k5eAaImF	projevovat
tyto	tento	k3xDgInPc4	tento
příznaky	příznak	k1gInPc4	příznak
<g/>
:	:	kIx,	:
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc4	odpor
<g />
.	.	kIx.	.
</s>
<s>
panika	panika	k1gFnSc1	panika
bušení	bušení	k1gNnSc2	bušení
srdce	srdce	k1gNnSc2	srdce
obtíže	obtíž	k1gFnSc2	obtíž
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
návaly	nával	k1gInPc7	nával
horka	horko	k1gNnSc2	horko
a	a	k8xC	a
chladu	chlad	k1gInSc2	chlad
cítí	cítit	k5eAaImIp3nS	cítit
se	se	k3xPyFc4	se
malátný	malátný	k2eAgInSc1d1	malátný
a	a	k8xC	a
ochromený	ochromený	k2eAgInSc1d1	ochromený
pocení	pocení	k1gNnSc3	pocení
sucho	sucho	k6eAd1	sucho
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
třas	třas	k1gInSc1	třas
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
člověka	člověk	k1gMnSc2	člověk
trpícího	trpící	k2eAgInSc2d1	trpící
arachnofobií	arachnofobie	k1gFnSc7	arachnofobie
pavouk	pavouk	k1gMnSc1	pavouk
<g/>
,	,	kIx,	,
daný	daný	k2eAgMnSc1d1	daný
jedinec	jedinec	k1gMnSc1	jedinec
často	často	k6eAd1	často
jedná	jednat	k5eAaImIp3nS	jednat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
pavouka	pavouk	k1gMnSc2	pavouk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jít	jít	k5eAaImF	jít
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
místnosti	místnost	k1gFnSc2	místnost
<g/>
)	)	kIx)	)
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
vyvýšené	vyvýšený	k2eAgNnSc4d1	vyvýšené
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stůl	stůl	k1gInSc4	stůl
<g/>
)	)	kIx)	)
Cítí	cítit	k5eAaImIp3nS	cítit
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
Musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
pavouka	pavouk	k1gMnSc4	pavouk
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
věděl	vědět	k5eAaImAgInS	vědět
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nP	by
ho	on	k3xPp3gMnSc4	on
nejradši	rád	k6eAd3	rád
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pavouka	pavouk	k1gMnSc4	pavouk
ztratí	ztratit	k5eAaPmIp3nS	ztratit
z	z	k7c2	z
dohledu	dohled	k1gInSc2	dohled
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
obavě	obava	k1gFnSc3	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
a	a	k8xC	a
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
nemohl	moct	k5eNaImAgMnS	moct
včas	včas	k6eAd1	včas
zareagovat	zareagovat	k5eAaPmF	zareagovat
<g/>
.	.	kIx.	.
</s>
<s>
Pavouka	pavouk	k1gMnSc4	pavouk
většinou	většinou	k6eAd1	většinou
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
–	–	k?	–
zabít	zabít	k5eAaPmF	zabít
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
vyhodit	vyhodit	k5eAaPmF	vyhodit
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
hodit	hodit	k5eAaPmF	hodit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
něco	něco	k6eAd1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mu	on	k3xPp3gMnSc3	on
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
volný	volný	k2eAgInSc1d1	volný
pohyb	pohyb	k1gInSc1	pohyb
např.	např.	kA	např.
krabičku	krabička	k1gFnSc4	krabička
<g/>
,	,	kIx,	,
skleničku	sklenička	k1gFnSc4	sklenička
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
léčen	léčit	k5eAaImNgMnS	léčit
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
z	z	k7c2	z
obecných	obecný	k2eAgFnPc2d1	obecná
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
specifické	specifický	k2eAgFnPc4d1	specifická
fobie	fobie	k1gFnPc4	fobie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
expozice	expozice	k1gFnSc1	expozice
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
psychoterapeuta	psychoterapeut	k1gMnSc2	psychoterapeut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
