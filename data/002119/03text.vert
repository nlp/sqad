<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
К	К	k?	К
<g/>
,	,	kIx,	,
Kyjiv	Kyjiva	k1gFnPc2	Kyjiva
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
,	,	kIx,	,
Kijev	Kijev	k1gFnSc1	Kijev
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc1d1	správní
středisko	středisko	k1gNnSc1	středisko
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dněpr	Dněpr	k1gInSc4	Dněpr
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
rozlohu	rozloha	k1gFnSc4	rozloha
827	[number]	k4	827
km2	km2	k4	km2
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
aglomerace	aglomerace	k1gFnSc2	aglomerace
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
3,3	[number]	k4	3,3
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
centrum	centrum	k1gNnSc1	centrum
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
křižovatka	křižovatka	k1gFnSc1	křižovatka
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
elektrotechnického	elektrotechnický	k2eAgInSc2d1	elektrotechnický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
starobylost	starobylost	k1gFnSc4	starobylost
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazýván	k2eAgInSc1d1	nazýván
"	"	kIx"	"
<g/>
matkou	matka	k1gFnSc7	matka
ruských	ruský	k2eAgNnPc2d1	ruské
měst	město	k1gNnPc2	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
Kyj	kyj	k1gInSc1	kyj
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Kyjův	Kyjův	k2eAgMnSc1d1	Kyjův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Srovnej	srovnat	k5eAaPmRp2nS	srovnat
např.	např.	kA	např.
Kyjov	Kyjov	k1gInSc4	Kyjov
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Kijów	Kijów	k1gFnSc1	Kijów
<g/>
,	,	kIx,	,
jidiš	jidiš	k1gNnSc1	jidiš
<g/>
:	:	kIx,	:
ק	ק	k?	ק
[	[	kIx(	[
<g/>
kijav	kijav	k1gInSc1	kijav
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Kiew	Kiew	k1gFnSc1	Kiew
<g/>
.	.	kIx.	.
</s>
<s>
Kyjevský	kyjevský	k2eAgInSc1d1	kyjevský
znak	znak	k1gInSc1	znak
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
archanděla	archanděl	k1gMnSc2	archanděl
Michaela	Michael	k1gMnSc2	Michael
držícího	držící	k2eAgMnSc2d1	držící
plamenný	plamenný	k2eAgInSc4d1	plamenný
meč	meč	k1gInSc4	meč
a	a	k8xC	a
štít	štít	k1gInSc4	štít
na	na	k7c6	na
azurovém	azurový	k2eAgNnSc6d1	azurové
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
zobrazen	zobrazit	k5eAaPmNgMnS	zobrazit
svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
bojující	bojující	k2eAgMnSc1d1	bojující
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
zůstal	zůstat	k5eAaPmAgInS	zůstat
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
osídlení	osídlení	k1gNnSc1	osídlení
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kyjeva	Kyjev	k1gInSc2	Kyjev
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
keramika	keramika	k1gFnSc1	keramika
typu	typ	k1gInSc2	typ
Korčak	Korčak	k1gInSc1	Korčak
a	a	k8xC	a
byzantské	byzantský	k2eAgFnSc2d1	byzantská
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
doloženy	doložen	k2eAgFnPc1d1	doložena
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přes	přes	k7c4	přes
strategickou	strategický	k2eAgFnSc4d1	strategická
polohu	poloha	k1gFnSc4	poloha
žádné	žádný	k3yNgNnSc1	žádný
hradiště	hradiště	k1gNnSc1	hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
se	se	k3xPyFc4	se
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Zámkové	zámkový	k2eAgFnSc2d1	zámková
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Dětinek	Dětinek	k1gInSc4	Dětinek
a	a	k8xC	a
Starokyjevské	Starokyjevský	k2eAgFnPc4d1	Starokyjevský
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
Kyjeva	Kyjev	k1gInSc2	Kyjev
mluví	mluvit	k5eAaImIp3nS	mluvit
také	také	k9	také
pověst	pověst	k1gFnSc1	pověst
uváděná	uváděný	k2eAgFnSc1d1	uváděná
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
arménským	arménský	k2eAgMnSc7d1	arménský
historikem	historik	k1gMnSc7	historik
Zenobem	Zenob	k1gMnSc7	Zenob
Glakem	Glaek	k1gMnSc7	Glaek
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
Pověsti	pověst	k1gFnSc6	pověst
dávných	dávný	k2eAgNnPc2d1	dávné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
bratrů	bratr	k1gMnPc2	bratr
Kyje	kyj	k1gInSc2	kyj
<g/>
,	,	kIx,	,
Ščeka	Ščeek	k1gInSc2	Ščeek
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
)	)	kIx)	)
a	a	k8xC	a
Choriva	Choriva	k1gFnSc1	Choriva
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
sestrou	sestra	k1gFnSc7	sestra
jménem	jméno	k1gNnSc7	jméno
Lybeď	Lybeď	k1gFnSc7	Lybeď
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Poljanů	Poljan	k1gInPc2	Poljan
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
pahorku	pahorek	k1gInSc6	pahorek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Boričev	Boričev	k1gMnSc1	Boričev
<g/>
,	,	kIx,	,
Ščekovina	Ščekovina	k1gFnSc1	Ščekovina
<g/>
,	,	kIx,	,
Chorevica	Chorevica	k1gMnSc1	Chorevica
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
pak	pak	k6eAd1	pak
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
po	po	k7c6	po
nejstarším	starý	k2eAgInSc7d3	nejstarší
Kyjev	Kyjev	k1gInSc4	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
866	[number]	k4	866
podle	podle	k7c2	podle
Pověsti	pověst	k1gFnSc2	pověst
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
860	[number]	k4	860
podle	podle	k7c2	podle
byzantských	byzantský	k2eAgInPc2d1	byzantský
pramenů	pramen	k1gInPc2	pramen
Kyjev	Kyjev	k1gInSc4	Kyjev
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
varjagští	varjagský	k2eAgMnPc1d1	varjagský
bojovníci	bojovník	k1gMnPc1	bojovník
Askold	Askold	k1gMnSc1	Askold
a	a	k8xC	a
Dir	Dir	k1gMnPc1	Dir
a	a	k8xC	a
vládli	vládnout	k5eAaImAgMnP	vládnout
zde	zde	k6eAd1	zde
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
882	[number]	k4	882
kdy	kdy	k6eAd1	kdy
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
varjagem	varjag	k1gMnSc7	varjag
<g/>
,	,	kIx,	,
novgorodským	novgorodský	k2eAgMnSc7d1	novgorodský
knížetem	kníže	k1gMnSc7	kníže
Olegem	Oleg	k1gMnSc7	Oleg
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
už	už	k6eAd1	už
Kyjev	Kyjev	k1gInSc1	Kyjev
stal	stát	k5eAaPmAgInS	stát
opevněným	opevněný	k2eAgNnSc7d1	opevněné
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
politickým	politický	k2eAgNnSc7d1	politické
centrem	centrum	k1gNnSc7	centrum
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
Byzantskou	byzantský	k2eAgFnSc7d1	byzantská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
nálezy	nález	k1gInPc4	nález
tří	tři	k4xCgInPc2	tři
hradišť	hradiště	k1gNnPc2	hradiště
<g/>
,	,	kIx,	,
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
Starokyjevské	Starokyjevský	k2eAgFnSc2d1	Starokyjevský
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
Zámkové	zámkový	k2eAgFnSc6d1	zámková
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
na	na	k7c6	na
Lysé	Lysé	k2eAgFnSc6d1	Lysé
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
kdy	kdy	k6eAd1	kdy
kníže	kníže	k1gMnSc1	kníže
Vladimír	Vladimír	k1gMnSc1	Vladimír
I.	I.	kA	I.
přijal	přijmout	k5eAaPmAgMnS	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
989	[number]	k4	989
<g/>
-	-	kIx~	-
<g/>
996	[number]	k4	996
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
tzv.	tzv.	kA	tzv.
Desátkový	desátkový	k2eAgInSc1d1	desátkový
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Bohorodičky	Bohorodička	k1gFnSc2	Bohorodička
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
předchozí	předchozí	k2eAgFnSc2d1	předchozí
pohanské	pohanský	k2eAgFnSc2d1	pohanská
svatyně	svatyně	k1gFnSc2	svatyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestavět	přestavět	k5eAaPmF	přestavět
knížecí	knížecí	k2eAgNnSc4d1	knížecí
sídlo	sídlo	k1gNnSc4	sídlo
po	po	k7c6	po
byzantském	byzantský	k2eAgInSc6d1	byzantský
vzoru	vzor	k1gInSc6	vzor
<g/>
,	,	kIx,	,
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Moudrý	moudrý	k2eAgMnSc1d1	moudrý
obehnal	obehnat	k5eAaPmAgMnS	obehnat
Kyjev	Kyjev	k1gInSc4	Kyjev
mohutným	mohutný	k2eAgInSc7d1	mohutný
valem	val	k1gInSc7	val
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
měl	mít	k5eAaImAgMnS	mít
mnoho	mnoho	k4c1	mnoho
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
bojarských	bojarský	k2eAgInPc2d1	bojarský
dvorců	dvorec	k1gInPc2	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
Řemesla	řemeslo	k1gNnPc1	řemeslo
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgInP	soustředit
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
zvaném	zvaný	k2eAgNnSc6d1	zvané
Podol	Podol	k1gInSc1	Podol
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
také	také	k9	také
opevněno	opevněn	k2eAgNnSc1d1	opevněno
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
stál	stát	k5eAaImAgInS	stát
významný	významný	k2eAgInSc1d1	významný
klášter	klášter	k1gInSc1	klášter
Kyjevskopečerská	Kyjevskopečerský	k2eAgFnSc1d1	Kyjevskopečerská
lávra	lávra	k1gFnSc1	lávra
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ruský	ruský	k2eAgInSc1d1	ruský
stát	stát	k1gInSc1	stát
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
Kyjev	Kyjev	k1gInSc1	Kyjev
začal	začít	k5eAaPmAgInS	začít
ztrácet	ztrácet	k5eAaImF	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
politický	politický	k2eAgMnSc1d1	politický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
<g/>
.	.	kIx.	.
</s>
<s>
Souviselo	souviset	k5eAaImAgNnS	souviset
to	ten	k3xDgNnSc1	ten
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
také	také	k9	také
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rusům	Rus	k1gMnPc3	Rus
uzavíraly	uzavírat	k5eAaImAgFnP	uzavírat
možnosti	možnost	k1gFnPc4	možnost
obchodovat	obchodovat	k5eAaImF	obchodovat
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stepích	step	k1gFnPc6	step
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
se	se	k3xPyFc4	se
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
neustále	neustále	k6eAd1	neustále
kočovníci	kočovník	k1gMnPc1	kočovník
a	a	k8xC	a
Byzanc	Byzanc	k1gFnSc1	Byzanc
získala	získat	k5eAaPmAgFnS	získat
nové	nový	k2eAgMnPc4d1	nový
obchodní	obchodní	k2eAgMnPc4d1	obchodní
partnery	partner	k1gMnPc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
věnovat	věnovat	k5eAaPmF	věnovat
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
usadila	usadit	k5eAaPmAgFnS	usadit
se	se	k3xPyFc4	se
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
se	se	k3xPyFc4	se
v	v	k7c4	v
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
výhodou	výhoda	k1gFnSc7	výhoda
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
transakce	transakce	k1gFnPc4	transakce
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
<g/>
,	,	kIx,	,
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
nyní	nyní	k6eAd1	nyní
svoji	svůj	k3xOyFgFnSc4	svůj
prioritu	priorita	k1gFnSc4	priorita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
krajní	krajní	k2eAgFnSc1d1	krajní
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
snadná	snadný	k2eAgFnSc1d1	snadná
přístupnost	přístupnost	k1gFnSc1	přístupnost
ze	z	k7c2	z
stepí	step	k1gFnPc2	step
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
staly	stát	k5eAaPmAgInP	stát
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
odsouván	odsouvat	k5eAaImNgInS	odsouvat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
rodícími	rodící	k2eAgMnPc7d1	rodící
se	se	k3xPyFc4	se
novými	nový	k2eAgNnPc7d1	nové
centry	centrum	k1gNnPc7	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
sídlili	sídlit	k5eAaImAgMnP	sídlit
rurikovští	rurikovský	k2eAgMnPc1d1	rurikovský
vládci	vládce	k1gMnPc1	vládce
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
patřil	patřit	k5eAaImAgMnS	patřit
Vladimir	Vladimir	k1gMnSc1	Vladimir
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1154	[number]	k4	1154
dobyl	dobýt	k5eAaPmAgInS	dobýt
Kyjev	Kyjev	k1gInSc4	Kyjev
vladimirsko-suzdalský	vladimirskouzdalský	k2eAgMnSc1d1	vladimirsko-suzdalský
kníže	kníže	k1gMnSc1	kníže
Jurij	Jurij	k1gMnSc1	Jurij
Dolgorukij	Dolgorukij	k1gMnSc1	Dolgorukij
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
velikého	veliký	k2eAgMnSc2d1	veliký
knížete	kníže	k1gMnSc2	kníže
kyjevského	kyjevský	k2eAgMnSc2d1	kyjevský
a	a	k8xC	a
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Andrej	Andrej	k1gMnSc1	Andrej
Bogoljubskij	Bogoljubskij	k1gMnSc1	Bogoljubskij
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
a	a	k8xC	a
zpustošení	zpustošení	k1gNnSc4	zpustošení
Kyjeva	Kyjev	k1gInSc2	Kyjev
roku	rok	k1gInSc2	rok
1169	[number]	k4	1169
sídlit	sídlit	k5eAaImF	sídlit
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Rusi	Rus	k1gFnSc2	Rus
a	a	k8xC	a
Kyjev	Kyjev	k1gInSc1	Kyjev
svěřil	svěřit	k5eAaPmAgInS	svěřit
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
bratrovi	bratr	k1gMnSc3	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
rok	rok	k1gInSc4	rok
1169	[number]	k4	1169
považován	považován	k2eAgInSc4d1	považován
za	za	k7c4	za
definitivní	definitivní	k2eAgInSc4d1	definitivní
konec	konec	k1gInSc4	konec
kyjevského	kyjevský	k2eAgInSc2d1	kyjevský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
město	město	k1gNnSc4	město
zpustošila	zpustošit	k5eAaPmAgFnS	zpustošit
mongolská	mongolský	k2eAgNnPc1d1	mongolské
vojska	vojsko	k1gNnPc4	vojsko
chána	chán	k1gMnSc2	chán
Bátúa	Bátúus	k1gMnSc2	Bátúus
a	a	k8xC	a
zničila	zničit	k5eAaPmAgFnS	zničit
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgFnPc2d1	významná
architektonických	architektonický	k2eAgFnPc2d1	architektonická
i	i	k8xC	i
písemných	písemný	k2eAgFnPc2d1	písemná
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
slavný	slavný	k2eAgInSc1d1	slavný
Desátkový	desátkový	k2eAgInSc1d1	desátkový
chrám	chrám	k1gInSc1	chrám
založený	založený	k2eAgInSc1d1	založený
Vladimírem	Vladimír	k1gMnSc7	Vladimír
I.	I.	kA	I.
nebo	nebo	k8xC	nebo
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
bránu	brána	k1gFnSc4	brána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1299	[number]	k4	1299
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
kyjevský	kyjevský	k2eAgMnSc1d1	kyjevský
metropolita	metropolita	k1gMnSc1	metropolita
do	do	k7c2	do
Vladimiru	Vladimir	k1gInSc2	Vladimir
<g/>
.	.	kIx.	.
</s>
<s>
Papežský	papežský	k2eAgMnSc1d1	papežský
vyslanec	vyslanec	k1gMnSc1	vyslanec
k	k	k7c3	k
mongolskému	mongolský	k2eAgMnSc3d1	mongolský
chánu	chán	k1gMnSc3	chán
Giovanni	Giovanň	k1gMnSc3	Giovanň
Carpini	Carpin	k2eAgMnPc1d1	Carpin
zanechal	zanechat	k5eAaPmAgInS	zanechat
roku	rok	k1gInSc2	rok
1245	[number]	k4	1245
svědectví	svědectví	k1gNnSc2	svědectví
o	o	k7c6	o
Kyjevě	Kyjev	k1gInSc6	Kyjev
zničeném	zničený	k2eAgInSc6d1	zničený
při	při	k7c6	při
mongolském	mongolský	k2eAgInSc6d1	mongolský
nájezdu	nájezd	k1gInSc6	nájezd
<g/>
:	:	kIx,	:
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1363	[number]	k4	1363
náležel	náležet	k5eAaImAgInS	náležet
Kyjev	Kyjev	k1gInSc1	Kyjev
k	k	k7c3	k
litevskému	litevský	k2eAgNnSc3d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc3	velkoknížectví
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
jako	jako	k8xS	jako
středisko	středisko	k1gNnSc1	středisko
Kyjevského	kyjevský	k2eAgNnSc2d1	Kyjevské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
)	)	kIx)	)
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Lublinské	lublinský	k2eAgFnSc2d1	Lublinská
unie	unie	k1gFnSc2	unie
k	k	k7c3	k
Republice	republika	k1gFnSc6	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1497	[number]	k4	1497
měl	mít	k5eAaImAgInS	mít
Kyjev	Kyjev	k1gInSc1	Kyjev
samosprávu	samospráva	k1gFnSc4	samospráva
podle	podle	k7c2	podle
magdeburského	magdeburský	k2eAgInSc2d1	magdeburský
způsobu	způsob	k1gInSc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kozáckých	kozácký	k2eAgNnPc6d1	kozácké
povstáních	povstání	k1gNnPc6	povstání
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Kyjev	Kyjev	k1gInSc1	Kyjev
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
ruskou	ruský	k2eAgFnSc4d1	ruská
správu	správa	k1gFnSc4	správa
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc3	jeho
postoupení	postoupení	k1gNnSc3	postoupení
Rusku	Rusko	k1gNnSc3	Rusko
definitivně	definitivně	k6eAd1	definitivně
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Věčný	věčný	k2eAgInSc1d1	věčný
mír	mír	k1gInSc1	mír
roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
byl	být	k5eAaImAgInS	být
Kyjev	Kyjev	k1gInSc1	Kyjev
střediskem	středisko	k1gNnSc7	středisko
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
sv.	sv.	kA	sv.
Vladimíra	Vladimír	k1gMnSc2	Vladimír
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Ševčenkova	Ševčenkův	k2eAgFnSc1d1	Ševčenkova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
ustavila	ustavit	k5eAaPmAgFnS	ustavit
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
Centrální	centrální	k2eAgFnSc1d1	centrální
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgNnPc2	dva
letech	let	k1gInPc6	let
bojů	boj	k1gInPc2	boj
však	však	k8xC	však
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
bolševici	bolševik	k1gMnPc1	bolševik
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Kyjev	Kyjev	k1gInSc1	Kyjev
sovětský	sovětský	k2eAgInSc1d1	sovětský
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
opět	opět	k6eAd1	opět
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
proletářštějšího	proletářský	k2eAgInSc2d2	proletářský
<g/>
"	"	kIx"	"
Charkova	Charkov	k1gInSc2	Charkov
<g/>
)	)	kIx)	)
alespoň	alespoň	k9	alespoň
metropolí	metropol	k1gFnPc2	metropol
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
Němci	Němec	k1gMnSc3	Němec
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
dobyli	dobýt	k5eAaPmAgMnP	dobýt
i	i	k8xC	i
Kyjev	Kyjev	k1gInSc4	Kyjev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dopustili	dopustit	k5eAaPmAgMnP	dopustit
nejkrutějších	krutý	k2eAgInPc2d3	nejkrutější
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
-	-	kIx~	-
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
29	[number]	k4	29
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
svezly	svézt	k5eAaPmAgFnP	svézt
do	do	k7c2	do
rokle	rokle	k1gFnSc2	rokle
Babí	babí	k2eAgInSc1d1	babí
Jar	jar	k1gInSc1	jar
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
přes	přes	k7c4	přes
33	[number]	k4	33
tisíc	tisíc	k4xCgInPc2	tisíc
kyjevských	kyjevský	k2eAgMnPc2d1	kyjevský
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
postřílely	postřílet	k5eAaPmAgInP	postřílet
je	on	k3xPp3gFnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dobývání	dobývání	k1gNnSc6	dobývání
a	a	k8xC	a
okupaci	okupace	k1gFnSc6	okupace
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
i	i	k9	i
např.	např.	kA	např.
slovenské	slovenský	k2eAgFnSc2d1	slovenská
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
divize	divize	k1gFnSc1	divize
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
bylo	být	k5eAaImAgNnS	být
těžce	těžce	k6eAd1	těžce
zničené	zničený	k2eAgNnSc1d1	zničené
město	město	k1gNnSc1	město
vysvobozeno	vysvobodit	k5eAaPmNgNnS	vysvobodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
jednotkami	jednotka	k1gFnPc7	jednotka
1	[number]	k4	1
<g/>
.	.	kIx.	.
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
frontu	front	k1gInSc2	front
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
do	do	k7c2	do
města	město	k1gNnSc2	město
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
jednotky	jednotka	k1gFnPc1	jednotka
1	[number]	k4	1
<g/>
.	.	kIx.	.
československé	československý	k2eAgFnSc2d1	Československá
samostatné	samostatný	k2eAgFnSc2d1	samostatná
brigády	brigáda	k1gFnSc2	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
Kyjev	Kyjev	k1gInSc1	Kyjev
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
země	zem	k1gFnSc2	zem
zapsal	zapsat	k5eAaPmAgMnS	zapsat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
masové	masový	k2eAgFnPc4d1	masová
demonstrace	demonstrace	k1gFnPc4	demonstrace
proti	proti	k7c3	proti
zfalšovaným	zfalšovaný	k2eAgInPc3d1	zfalšovaný
výsledkům	výsledek	k1gInPc3	výsledek
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
probíhaly	probíhat	k5eAaImAgFnP	probíhat
masové	masový	k2eAgInPc4d1	masový
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc4d1	zvaný
Euromajdan	Euromajdan	k1gInSc4	Euromajdan
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
prezidenta	prezident	k1gMnSc2	prezident
Viktora	Viktor	k1gMnSc2	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Kyjeva	Kyjev	k1gInSc2	Kyjev
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
pahorcích	pahorek	k1gInPc6	pahorek
nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
(	(	kIx(	(
<g/>
západním	západní	k2eAgInSc7d1	západní
<g/>
)	)	kIx)	)
břehem	břeh	k1gInSc7	břeh
Dněpru	Dněpr	k1gInSc2	Dněpr
<g/>
;	;	kIx,	;
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
nové	nový	k2eAgFnPc1d1	nová
čtvrti	čtvrt	k1gFnPc1	čtvrt
také	také	k9	také
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
Darnycja	Darnycj	k2eAgFnSc1d1	Darnycj
<g/>
)	)	kIx)	)
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
také	také	k9	také
vzdálenější	vzdálený	k2eAgFnPc1d2	vzdálenější
pravobřežní	pravobřežní	k2eAgFnPc1d1	pravobřežní
čtvrti	čtvrt	k1gFnPc1	čtvrt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Oboloň	Oboloň	k1gFnSc1	Oboloň
<g/>
,	,	kIx,	,
Podol	Podol	k1gInSc1	Podol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
členitý	členitý	k2eAgInSc1d1	členitý
<g/>
,	,	kIx,	,
lesnaté	lesnatý	k2eAgInPc1d1	lesnatý
pahorky	pahorek	k1gInPc1	pahorek
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděleny	oddělit	k5eAaPmNgInP	oddělit
zaříznutými	zaříznutý	k2eAgNnPc7d1	zaříznuté
údolíčky	údolíčko	k1gNnPc7	údolíčko
<g/>
,	,	kIx,	,
sbíhajícími	sbíhající	k2eAgNnPc7d1	sbíhající
se	se	k3xPyFc4	se
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Dněpru	Dněpr	k1gInSc2	Dněpr
u	u	k7c2	u
Kyjeva	Kyjev	k1gInSc2	Kyjev
zleva	zleva	k6eAd1	zleva
ústí	ústit	k5eAaImIp3nS	ústit
řeka	řeka	k1gFnSc1	řeka
Desna	Desna	k1gFnSc1	Desna
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
Dněpr	Dněpr	k1gInSc4	Dněpr
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
velká	velký	k2eAgFnSc1d1	velká
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
kontinentální	kontinentální	k2eAgInPc1d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
je	být	k5eAaImIp3nS	být
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
přicházejí	přicházet	k5eAaImIp3nP	přicházet
oblevy	obleva	k1gFnPc1	obleva
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
všechen	všechen	k3xTgInSc4	všechen
sníh	sníh	k1gInSc1	sníh
roztaje	roztát	k5eAaPmIp3nS	roztát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
mrazivé	mrazivý	k2eAgInPc4d1	mrazivý
dny	den	k1gInPc4	den
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
-20	-20	k4	-20
<g/>
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c6	po
ránu	ráno	k1gNnSc6	ráno
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
méně	málo	k6eAd2	málo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
začne	začít	k5eAaPmIp3nS	začít
vítr	vítr	k1gInSc1	vítr
foukat	foukat	k5eAaImF	foukat
z	z	k7c2	z
východu	východ	k1gInSc2	východ
nebo	nebo	k8xC	nebo
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
byla	být	k5eAaImAgFnS	být
-32,2	-32,2	k4	-32,2
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
jaro	jaro	k1gNnSc4	jaro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
půli	půle	k1gFnSc4	půle
března	březen	k1gInSc2	březen
do	do	k7c2	do
půli	půle	k1gFnSc3	půle
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
srážkových	srážkový	k2eAgInPc2d1	srážkový
rekordů	rekord	k1gInPc2	rekord
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
horko	horko	k1gNnSc1	horko
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
Dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
půlka	půlka	k1gFnSc1	půlka
podzimu	podzim	k1gInSc2	podzim
bývá	bývat	k5eAaImIp3nS	bývat
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
září	září	k1gNnSc4	září
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
deštivé	deštivý	k2eAgNnSc1d1	deštivé
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
napršelo	napršet	k5eAaPmAgNnS	napršet
153	[number]	k4	153
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
norma	norma	k1gFnSc1	norma
srážek	srážka	k1gFnPc2	srážka
pro	pro	k7c4	pro
září	září	k1gNnSc4	září
je	být	k5eAaImIp3nS	být
58	[number]	k4	58
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
39,9	[number]	k4	39,9
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
může	moct	k5eAaImIp3nS	moct
suché	suchý	k2eAgNnSc1d1	suché
počasí	počasí	k1gNnSc1	počasí
také	také	k9	také
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
Kyjevem	Kyjev	k1gInSc7	Kyjev
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
a	a	k8xC	a
slunečno	slunečno	k6eAd1	slunečno
teploty	teplota	k1gFnPc1	teplota
mohou	moct	k5eAaImIp3nP	moct
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
spíše	spíše	k9	spíše
přichází	přicházet	k5eAaImIp3nS	přicházet
deštivé	deštivý	k2eAgNnSc1d1	deštivé
počasí	počasí	k1gNnSc1	počasí
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
první	první	k4xOgInSc4	první
sníh	sníh	k1gInSc4	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
může	moct	k5eAaImIp3nS	moct
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Taras	taras	k1gInSc1	taras
Ševčenko	Ševčenka	k1gFnSc5	Ševčenka
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
umělec	umělec	k1gMnSc1	umělec
Julija	Julijum	k1gNnSc2	Julijum
Džymová	Džymová	k1gFnSc1	Džymová
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
biatlonistka	biatlonistka	k1gFnSc1	biatlonistka
Oleksandr	Oleksandr	k1gInSc1	Oleksandr
Šovkovskyj	Šovkovskyj	k1gFnSc1	Šovkovskyj
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
Michail	Michail	k1gMnSc1	Michail
Těreščenko	Těreščenka	k1gFnSc5	Těreščenka
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
filozof	filozof	k1gMnSc1	filozof
Michail	Michail	k1gMnSc1	Michail
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
a	a	k8xC	a
sovětský	sovětský	k2eAgMnSc1d1	sovětský
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
románu	román	k1gInSc2	román
Mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
Markétka	Markétka	k1gFnSc1	Markétka
Dmitrij	Dmitrij	k1gFnSc2	Dmitrij
Bogrov	Bogrov	k1gInSc1	Bogrov
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgMnSc1d1	policejní
udavač	udavač	k1gMnSc1	udavač
<g/>
,	,	kIx,	,
vrah	vrah	k1gMnSc1	vrah
Pjotra	Pjotr	k1gMnSc2	Pjotr
Stolypina	Stolypin	k2eAgNnSc2d1	Stolypin
Milla	Millo	k1gNnSc2	Millo
Jovovich	Jovovicha	k1gFnPc2	Jovovicha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
Kazimir	Kazimir	k1gMnSc1	Kazimir
Malevič	Malevič	k1gMnSc1	Malevič
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Golda	Golda	k1gMnSc1	Golda
Meirová	Meirová	k1gFnSc1	Meirová
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministerská	ministerský	k2eAgFnSc1d1	ministerská
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Izraele	Izrael	k1gInSc2	Izrael
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nosov	Nosov	k1gInSc1	Nosov
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Igor	Igor	k1gMnSc1	Igor
Sikorskij	Sikorskij	k1gMnSc1	Sikorskij
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Žofie	Žofie	k1gFnSc2	Žofie
(	(	kIx(	(
<g/>
С	С	k?	С
с	с	k?	с
<g/>
)	)	kIx)	)
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
knížetem	kníže	k1gMnSc7	kníže
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Moudrým	moudrý	k2eAgMnSc7d1	moudrý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Ondřeje	Ondřej	k1gMnSc2	Ondřej
(	(	kIx(	(
<g/>
А	А	k?	А
ц	ц	k?	ц
<g/>
)	)	kIx)	)
Kyjevskopečerská	Kyjevskopečerský	k2eAgFnSc1d1	Kyjevskopečerská
lávra	lávra	k1gFnSc1	lávra
(	(	kIx(	(
<g/>
К	К	k?	К
л	л	k?	л
<g/>
)	)	kIx)	)
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
(	(	kIx(	(
<g/>
М	М	k?	М
З	З	k?	З
с	с	k?	с
<g/>
)	)	kIx)	)
socha	socha	k1gFnSc1	socha
Matka	matka	k1gFnSc1	matka
Vlast	vlast	k1gFnSc1	vlast
Zlatá	zlatá	k1gFnSc1	zlatá
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
З	З	k?	З
в	в	k?	в
<g/>
)	)	kIx)	)
Katedrála	katedrál	k1gMnSc2	katedrál
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Muzeum	muzeum	k1gNnSc4	muzeum
Michaila	Michail	k1gMnSc4	Michail
Bulgakova	Bulgakův	k2eAgInSc2d1	Bulgakův
Kyjev	Kyjev	k1gInSc1	Kyjev
je	být	k5eAaImIp3nS	být
rozdělen	rozdělen	k2eAgInSc1d1	rozdělen
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
10	[number]	k4	10
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
rajónů	rajón	k1gInPc2	rajón
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Darnyckyj	Darnyckyj	k1gFnSc1	Darnyckyj
(	(	kIx(	(
<g/>
Д	Д	k?	Д
<g/>
)	)	kIx)	)
Desňanskyj	Desňanskyj	k1gMnSc1	Desňanskyj
(	(	kIx(	(
<g/>
Д	Д	k?	Д
<g/>
)	)	kIx)	)
Dniprovskyj	Dniprovskyj	k1gMnSc1	Dniprovskyj
(	(	kIx(	(
<g/>
Д	Д	k?	Д
<g/>
)	)	kIx)	)
Holosijivskyj	Holosijivskyj	k1gMnSc1	Holosijivskyj
(	(	kIx(	(
<g/>
Г	Г	k?	Г
<g/>
)	)	kIx)	)
Oboloňskyj	Oboloňskyj	k1gMnSc1	Oboloňskyj
(	(	kIx(	(
<g/>
О	О	k?	О
<g/>
)	)	kIx)	)
Pečerskyj	Pečerskyj	k1gMnSc1	Pečerskyj
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
Podilskyj	Podilskyj	k1gMnSc1	Podilskyj
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
Solomenskyj	Solomenskyj	k1gMnSc1	Solomenskyj
(	(	kIx(	(
<g/>
С	С	k?	С
<g/>
'	'	kIx"	'
<g/>
я	я	k?	я
<g/>
)	)	kIx)	)
Svjatošynskyj	Svjatošynskyj	k1gMnSc1	Svjatošynskyj
(	(	kIx(	(
<g/>
С	С	k?	С
<g/>
)	)	kIx)	)
Ševčenkivskyj	Ševčenkivskyj	k1gMnSc1	Ševčenkivskyj
(	(	kIx(	(
<g/>
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
Lvova	Lvov	k1gInSc2	Lvov
či	či	k8xC	či
Charkova	Charkov	k1gInSc2	Charkov
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tudy	tudy	k6eAd1	tudy
procházela	procházet	k5eAaImAgFnS	procházet
cesta	cesta	k1gFnSc1	cesta
tzv.	tzv.	kA	tzv.
Cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
Varjagů	Varjag	k1gMnPc2	Varjag
k	k	k7c3	k
Řekům	Řek	k1gMnPc3	Řek
(	(	kIx(	(
<g/>
Iz	Iz	k1gMnSc1	Iz
Varjag	Varjag	k1gMnSc1	Varjag
v	v	k7c6	v
Greki	Grek	k1gInSc6	Grek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Dněpr	Dněpr	k1gInSc1	Dněpr
hlavní	hlavní	k2eAgInSc1d1	hlavní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
tepnou	tepna	k1gFnSc7	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
<g/>
;	;	kIx,	;
osobní	osobní	k2eAgFnSc1d1	osobní
doprava	doprava	k1gFnSc1	doprava
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
turistické	turistický	k2eAgFnPc4d1	turistická
a	a	k8xC	a
rekreační	rekreační	k2eAgFnPc4d1	rekreační
vyjížďky	vyjížďka	k1gFnPc4	vyjížďka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
<g/>
;	;	kIx,	;
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
několikamilionovému	několikamilionový	k2eAgNnSc3d1	několikamilionové
městu	město	k1gNnSc3	město
slouží	sloužit	k5eAaImIp3nS	sloužit
vlastní	vlastní	k2eAgFnSc1d1	vlastní
síť	síť	k1gFnSc1	síť
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
trolejbusů	trolejbus	k1gInPc2	trolejbus
i	i	k8xC	i
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
je	být	k5eAaImIp3nS	být
Kyjiv-Pasažyrskyj	Kyjiv-Pasažyrskyj	k1gFnSc1	Kyjiv-Pasažyrskyj
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Boryspil	Boryspil	k1gFnSc2	Boryspil
<g/>
,	,	kIx,	,
blíže	blízce	k6eAd2	blízce
městu	město	k1gNnSc3	město
leží	ležet	k5eAaImIp3nS	ležet
menší	malý	k2eAgNnSc1d2	menší
letiště	letiště	k1gNnSc1	letiště
Žuljany	Žuljana	k1gFnSc2	Žuljana
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Baku	Baku	k1gNnSc1	Baku
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
Tbilisi	Tbilisi	k1gNnSc2	Tbilisi
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
Kišiněv	Kišiněv	k1gFnSc2	Kišiněv
<g/>
,	,	kIx,	,
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Manila	Manila	k1gFnSc1	Manila
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
Tampere	Tamper	k1gInSc5	Tamper
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
</s>
