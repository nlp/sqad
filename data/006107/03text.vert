<s>
Úd	úd	k1gInSc1	úd
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
psáno	psát	k5eAaImNgNnS	psát
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
určitým	určitý	k2eAgInSc7d1	určitý
členem	člen	k1gInSc7	člen
jako	jako	k8xS	jako
al-úd	al-úd	k6eAd1	al-úd
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
ud	ud	k?	ud
<g/>
,	,	kIx,	,
ʻ	ʻ	k?	ʻ
<g/>
,	,	kIx,	,
oud	oud	k1gInSc1	oud
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
též	též	k9	též
út	út	k?	út
či	či	k8xC	či
elaúd	elaúd	k1gInSc1	elaúd
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
arabský	arabský	k2eAgInSc1d1	arabský
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
drnkací	drnkací	k2eAgInSc1d1	drnkací
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
krkem	krk	k1gInSc7	krk
o	o	k7c6	o
hmatníku	hmatník	k1gInSc6	hmatník
bez	bez	k7c2	bez
pražců	pražec	k1gInPc2	pražec
<g/>
,	,	kIx,	,
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
skříň	skříň	k1gFnSc1	skříň
má	mít	k5eAaImIp3nS	mít
vyduté	vydutý	k2eAgNnSc4d1	vyduté
dno	dno	k1gNnSc4	dno
s	s	k7c7	s
rovným	rovný	k2eAgNnSc7d1	rovné
víkem	víko	k1gNnSc7	víko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přímý	přímý	k2eAgMnSc1d1	přímý
předchůdce	předchůdce	k1gMnSc1	předchůdce
loutny	loutna	k1gFnSc2	loutna
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
nástroje	nástroj	k1gInSc2	nástroj
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
ʿ	ʿ	k?	ʿ
(	(	kIx(	(
<g/>
ع	ع	k?	ع
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
též	též	k9	též
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
označení	označení	k1gNnSc1	označení
turecké	turecký	k2eAgFnSc2d1	turecká
varianty	varianta	k1gFnSc2	varianta
nástroje	nástroj	k1gInPc1	nástroj
jako	jako	k8xS	jako
ud	ud	k?	ud
nebo	nebo	k8xC	nebo
ut	ut	k?	ut
a	a	k8xC	a
řecké	řecký	k2eAgNnSc1d1	řecké
jako	jako	k8xS	jako
úti	úti	k?	úti
(	(	kIx(	(
<g/>
ο	ο	k?	ο
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Perská	perský	k2eAgFnSc1d1	perská
varianta	varianta	k1gFnSc1	varianta
údu	úd	k1gInSc2	úd
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
barbat	barbat	k5eAaImF	barbat
(	(	kIx(	(
<g/>
ب	ب	k?	ب
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
názvu	název	k1gInSc2	název
nástroje	nástroj	k1gInSc2	nástroj
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
určitým	určitý	k2eAgInSc7d1	určitý
členem	člen	k1gInSc7	člen
al-ʿ	al-ʿ	k?	al-ʿ
pochází	pocházet	k5eAaImIp3nS	pocházet
též	též	k9	též
označení	označení	k1gNnSc1	označení
loutny	loutna	k1gFnSc2	loutna
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
českého	český	k2eAgInSc2d1	český
loutna	loutna	k1gFnSc1	loutna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
etymologie	etymologie	k1gFnSc1	etymologie
názvu	název	k1gInSc2	název
se	se	k3xPyFc4	se
povětšinou	povětšinou	k6eAd1	povětšinou
udává	udávat	k5eAaImIp3nS	udávat
metonymie	metonymie	k1gFnSc1	metonymie
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
ʿ	ʿ	k?	ʿ
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
možný	možný	k2eAgInSc1d1	možný
je	být	k5eAaImIp3nS	být
též	též	k9	též
původ	původ	k1gInSc4	původ
z	z	k7c2	z
perského	perský	k2eAgInSc2d1	perský
rud	ruda	k1gFnPc2	ruda
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
struna	struna	k1gFnSc1	struna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
