<s>
Úd	úd	k1gInSc1	úd
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
psáno	psát	k5eAaImNgNnS	psát
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
určitým	určitý	k2eAgInSc7d1	určitý
členem	člen	k1gInSc7	člen
jako	jako	k8xS	jako
al-úd	al-úd	k6eAd1	al-úd
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
ud	ud	k?	ud
<g/>
,	,	kIx,	,
ʻ	ʻ	k?	ʻ
<g/>
,	,	kIx,	,
oud	oud	k1gInSc1	oud
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
též	též	k9	též
út	út	k?	út
či	či	k8xC	či
elaúd	elaúd	k1gInSc1	elaúd
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
arabský	arabský	k2eAgInSc1d1	arabský
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
