<s>
Těstoviny	těstovina	k1gFnPc1	těstovina
jsou	být	k5eAaImIp3nP	být
potravinový	potravinový	k2eAgInSc4d1	potravinový
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
nekynutého	kynutý	k2eNgMnSc2d1	nekynutý
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
nekypřeného	kypřený	k2eNgNnSc2d1	kypřený
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
z	z	k7c2	z
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
přídavných	přídavný	k2eAgFnPc2d1	přídavná
látek	látka	k1gFnPc2	látka
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
obarvení	obarvení	k1gNnSc3	obarvení
nebo	nebo	k8xC	nebo
ochucení	ochucení	k1gNnSc3	ochucení
<g/>
.	.	kIx.	.
</s>
<s>
Těstoviny	těstovina	k1gFnPc1	těstovina
se	se	k3xPyFc4	se
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
vařit	vařit	k5eAaImF	vařit
v	v	k7c6	v
osolené	osolený	k2eAgFnSc6d1	osolená
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
jeden	jeden	k4xCgInSc4	jeden
litr	litr	k1gInSc4	litr
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
100	[number]	k4	100
g	g	kA	g
těstovin	těstovina	k1gFnPc2	těstovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
těstoviny	těstovina	k1gFnPc4	těstovina
vaječné	vaječný	k2eAgFnPc4d1	vaječná
a	a	k8xC	a
bezvaječné	bezvaječný	k2eAgFnPc4d1	bezvaječná
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
těstovinami	těstovina	k1gFnPc7	těstovina
semolinovými	semolinový	k2eAgFnPc7d1	semolinová
<g/>
,	,	kIx,	,
celozrnnými	celozrnný	k2eAgFnPc7d1	celozrnná
pšeničnými	pšeničný	k2eAgFnPc7d1	pšeničná
nebo	nebo	k8xC	nebo
špaldovými	špaldový	k2eAgFnPc7d1	špaldová
<g/>
,	,	kIx,	,
ječnými	ječný	k2eAgFnPc7d1	Ječná
nebo	nebo	k8xC	nebo
žitnými	žitný	k2eAgFnPc7d1	Žitná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
zdravé	zdravý	k2eAgFnSc2d1	zdravá
výživy	výživa	k1gFnSc2	výživa
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
také	také	k9	také
těstoviny	těstovina	k1gFnPc1	těstovina
z	z	k7c2	z
bezlepkové	bezlepkový	k2eAgFnSc2d1	bezlepková
mouky	mouka	k1gFnSc2	mouka
-	-	kIx~	-
pohankové	pohankový	k2eAgFnSc2d1	pohanková
<g/>
,	,	kIx,	,
kukuřičné	kukuřičný	k2eAgFnSc2d1	kukuřičná
nebo	nebo	k8xC	nebo
rýžové	rýžový	k2eAgFnSc2d1	rýžová
<g/>
.	.	kIx.	.
</s>
<s>
Těstoviny	těstovina	k1gFnPc1	těstovina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
ve	v	k7c6	v
známé	známý	k2eAgFnSc6d1	známá
Apiciově	Apiciův	k2eAgFnSc6d1	Apiciův
knize	kniha	k1gFnSc6	kniha
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
císaře	císař	k1gMnSc2	císař
Tiberia	Tiberium	k1gNnSc2	Tiberium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
příprava	příprava	k1gFnSc1	příprava
sekaného	sekaný	k2eAgNnSc2d1	sekané
masa	maso	k1gNnSc2	maso
nebo	nebo	k8xC	nebo
ryb	ryba	k1gFnPc2	ryba
obložených	obložený	k2eAgFnPc2d1	obložená
těstovinami	těstovina	k1gFnPc7	těstovina
"	"	kIx"	"
<g/>
lasagne	lasagnout	k5eAaPmIp3nS	lasagnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
historický	historický	k2eAgInSc1d1	historický
záznam	záznam	k1gInSc1	záznam
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dvanáctého	dvanáctý	k4xOgNnSc2	dvanáctý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Gugliemo	Gugliema	k1gFnSc5	Gugliema
di	di	k?	di
Malavelle	Malavelle	k1gFnSc1	Malavelle
popisuje	popisovat	k5eAaImIp3nS	popisovat
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
podávaly	podávat	k5eAaImAgFnP	podávat
macarrones	macarrones	k1gInSc4	macarrones
sen	sen	k1gInSc1	sen
logana	logana	k1gFnSc1	logana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
těstoviny	těstovina	k1gFnPc1	těstovina
v	v	k7c6	v
omáčce	omáčka	k1gFnSc6	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
objevuje	objevovat	k5eAaImIp3nS	objevovat
těstovina	těstovina	k1gFnSc1	těstovina
Jacopore	Jacopor	k1gInSc5	Jacopor
da	da	k?	da
Todi	Tod	k1gMnSc3	Tod
<g/>
.	.	kIx.	.
</s>
<s>
Tortellini	Tortellin	k2eAgMnPc1d1	Tortellin
<g/>
:	:	kIx,	:
těstovinové	těstovinový	k2eAgFnPc4d1	těstovinová
taštičky	taštička	k1gFnPc4	taštička
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
prstýnku	prstýnek	k1gInSc2	prstýnek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
maso	maso	k1gNnSc1	maso
nebo	nebo	k8xC	nebo
sýrová	sýrový	k2eAgFnSc1d1	sýrová
náplň	náplň	k1gFnSc1	náplň
<g/>
.	.	kIx.	.
</s>
<s>
Tortellini	Tortellin	k2eAgMnPc1d1	Tortellin
[	[	kIx(	[
<g/>
čti	číst	k5eAaImRp2nS	číst
tortelíny	tortelína	k1gFnSc2	tortelína
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
italského	italský	k2eAgNnSc2d1	italské
slova	slovo	k1gNnSc2	slovo
tortellino	tortellin	k2eAgNnSc1d1	tortellin
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc4d1	malý
koláček	koláček	k1gInSc4	koláček
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
italské	italský	k2eAgFnPc1d1	italská
semolinové	semolinový	k2eAgFnPc1d1	semolinová
těstoviny	těstovina	k1gFnPc1	těstovina
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kroužku	kroužek	k1gInSc2	kroužek
neboli	neboli	k8xC	neboli
prstýnku	prstýnek	k1gInSc2	prstýnek
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
náplněmi	náplň	k1gFnPc7	náplň
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
provincie	provincie	k1gFnSc2	provincie
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
.	.	kIx.	.
</s>
<s>
Vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
tortellini	tortellin	k2eAgMnPc1d1	tortellin
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
masovou	masový	k2eAgFnSc7d1	masová
<g/>
,	,	kIx,	,
sýrovou	sýrový	k2eAgFnSc7d1	sýrová
<g/>
,	,	kIx,	,
houbovou	houbový	k2eAgFnSc7d1	houbová
nebo	nebo	k8xC	nebo
ricotta	ricotta	k1gFnSc1	ricotta
<g/>
/	/	kIx~	/
<g/>
špenát	špenát	k1gInSc1	špenát
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgMnPc1d1	tradiční
výrobci	výrobce	k1gMnPc1	výrobce
tortellin	tortellina	k1gFnPc2	tortellina
jsou	být	k5eAaImIp3nP	být
Pastifico	Pastifico	k6eAd1	Pastifico
Bolognese	Bolognese	k1gFnSc1	Bolognese
a	a	k8xC	a
Azienda	Azienda	k1gFnSc1	Azienda
Cerlacchia	Cerlacchia	k1gFnSc1	Cerlacchia
<g/>
.	.	kIx.	.
</s>
<s>
Ravioly	Raviola	k1gFnPc1	Raviola
<g/>
:	:	kIx,	:
těstovinové	těstovinový	k2eAgFnPc1d1	těstovinová
taštičky	taštička	k1gFnPc1	taštička
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čtverečků	čtvereček	k1gInPc2	čtvereček
<g/>
.	.	kIx.	.
</s>
<s>
Ravioli	Ravioli	k6eAd1	Ravioli
[	[	kIx(	[
<g/>
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
italského	italský	k2eAgNnSc2d1	italské
slova	slovo	k1gNnSc2	slovo
raviolo	raviola	k1gFnSc5	raviola
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc7d1	pocházející
ze	z	k7c2	z
slovesa	sloveso	k1gNnSc2	sloveso
riavolgere	riavolgrat	k5eAaPmIp3nS	riavolgrat
"	"	kIx"	"
<g/>
zabalit	zabalit	k5eAaPmF	zabalit
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
italské	italský	k2eAgFnPc1d1	italská
semolinové	semolinový	k2eAgFnPc1d1	semolinová
těstoviny	těstovina	k1gFnPc1	těstovina
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
čtverečku	čtvereček	k1gInSc2	čtvereček
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
taštičky	taštička	k1gFnPc1	taštička
<g/>
,	,	kIx,	,
s	s	k7c7	s
masovou	masový	k2eAgFnSc7d1	masová
náplní	náplň	k1gFnSc7	náplň
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
italský	italský	k2eAgInSc4d1	italský
region	region	k1gInSc4	region
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
Emilia	Emilius	k1gMnSc2	Emilius
Romagna	Romagn	k1gMnSc2	Romagn
<g/>
.	.	kIx.	.
</s>
<s>
Cappelleti	Cappellet	k5eAaPmF	Cappellet
<g/>
:	:	kIx,	:
plněné	plněný	k2eAgFnPc1d1	plněná
těstoviny	těstovina	k1gFnPc1	těstovina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
malých	malý	k2eAgInPc2d1	malý
kloboučků	klobouček	k1gInPc2	klobouček
<g/>
.	.	kIx.	.
</s>
<s>
Canelloni	Canelloň	k1gFnSc3	Canelloň
<g/>
:	:	kIx,	:
závitky	závitka	k1gFnPc1	závitka
z	z	k7c2	z
nudlového	nudlový	k2eAgNnSc2d1	nudlové
těsta	těsto	k1gNnSc2	těsto
plněné	plněný	k2eAgInPc1d1	plněný
různými	různý	k2eAgFnPc7d1	různá
náplněmi	náplň	k1gFnPc7	náplň
<g/>
.	.	kIx.	.
</s>
<s>
Cannelloni	Cannellon	k1gMnPc1	Cannellon
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
kanelóny	kanelóna	k1gFnSc2	kanelóna
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
těstoviny	těstovina	k1gFnPc1	těstovina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
dutého	dutý	k2eAgInSc2d1	dutý
válečku	váleček	k1gInSc2	váleček
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
většinou	většinou	k6eAd1	většinou
sýrem	sýr	k1gInSc7	sýr
<g/>
,	,	kIx,	,
ricottou	ricotta	k1gFnSc7	ricotta
<g/>
,	,	kIx,	,
špenátem	špenát	k1gInSc7	špenát
nebo	nebo	k8xC	nebo
mletým	mletý	k2eAgNnSc7d1	mleté
masem	maso	k1gNnSc7	maso
a	a	k8xC	a
zapékají	zapékat	k5eAaImIp3nP	zapékat
v	v	k7c6	v
troubě	trouba	k1gFnSc6	trouba
s	s	k7c7	s
bešamelovou	bešamelový	k2eAgFnSc7d1	bešamelová
nebo	nebo	k8xC	nebo
rajčatovou	rajčatový	k2eAgFnSc7d1	rajčatová
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Canelloni	Canellon	k1gMnPc1	Canellon
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
předvaření	předvařený	k2eAgMnPc1d1	předvařený
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
cca	cca	kA	cca
8	[number]	k4	8
-	-	kIx~	-
10	[number]	k4	10
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
cca	cca	kA	cca
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Spaghetti	spaghetti	k1gInPc1	spaghetti
<g/>
:	:	kIx,	:
špagety	špagety	k1gFnPc1	špagety
<g/>
.	.	kIx.	.
</s>
<s>
Spaghettini	Spaghettin	k2eAgMnPc1d1	Spaghettin
<g/>
:	:	kIx,	:
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgFnPc4d1	tenká
špagety	špagety	k1gFnPc4	špagety
<g/>
.	.	kIx.	.
</s>
<s>
Bucatini	Bucatin	k2eAgMnPc1d1	Bucatin
<g/>
:	:	kIx,	:
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
špagety	špagety	k1gFnPc4	špagety
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
duté	dutý	k2eAgFnSc2d1	dutá
<g/>
.	.	kIx.	.
</s>
<s>
Maccheroni	Maccheron	k1gMnPc1	Maccheron
<g/>
:	:	kIx,	:
široké	široký	k2eAgFnSc2d1	široká
roury	roura	k1gFnSc2	roura
<g/>
.	.	kIx.	.
</s>
<s>
Linguine	Linguinout	k5eAaPmIp3nS	Linguinout
<g/>
:	:	kIx,	:
tenké	tenký	k2eAgFnPc1d1	tenká
<g/>
,	,	kIx,	,
placaté	placatý	k2eAgFnPc1d1	placatá
špagety	špagety	k1gFnPc1	špagety
<g/>
.	.	kIx.	.
</s>
<s>
Fettuccine	Fettuccinout	k5eAaPmIp3nS	Fettuccinout
<g/>
:	:	kIx,	:
široké	široký	k2eAgFnPc1d1	široká
nudle	nudle	k1gFnPc1	nudle
(	(	kIx(	(
<g/>
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
tagliatelle	tagliatelle	k1gFnSc1	tagliatelle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tagliatelle	Tagliatelle	k1gNnSc1	Tagliatelle
<g/>
:	:	kIx,	:
široké	široký	k2eAgFnPc1d1	široká
nudle	nudle	k1gFnPc1	nudle
<g/>
.	.	kIx.	.
</s>
<s>
Pappardelle	Pappardelle	k1gFnSc1	Pappardelle
<g/>
:	:	kIx,	:
do	do	k7c2	do
klubíček	klubíčko	k1gNnPc2	klubíčko
stočené	stočený	k2eAgInPc4d1	stočený
asi	asi	k9	asi
1	[number]	k4	1
cm	cm	kA	cm
široké	široký	k2eAgFnSc2d1	široká
nudle	nudle	k1gFnSc2	nudle
<g/>
.	.	kIx.	.
</s>
<s>
Penne	Pennout	k5eAaPmIp3nS	Pennout
<g/>
:	:	kIx,	:
široké	široký	k2eAgFnPc1d1	široká
kratší	krátký	k2eAgFnPc1d2	kratší
rourky	rourka	k1gFnPc1	rourka
se	s	k7c7	s
zkosenými	zkosený	k2eAgInPc7d1	zkosený
konci	konec	k1gInPc7	konec
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
též	též	k9	též
makaronky	makaronek	k1gInPc4	makaronek
<g/>
.	.	kIx.	.
</s>
<s>
Pizzoccheri	Pizzoccheri	k6eAd1	Pizzoccheri
<g/>
:	:	kIx,	:
rovné	rovný	k2eAgFnPc4d1	rovná
nudle	nudle	k1gFnPc4	nudle
z	z	k7c2	z
80	[number]	k4	80
%	%	kIx~	%
pohankové	pohankový	k2eAgFnSc2d1	pohanková
mouky	mouka	k1gFnSc2	mouka
<g/>
.	.	kIx.	.
</s>
<s>
Rigatoni	Rigaton	k1gMnPc1	Rigaton
<g/>
:	:	kIx,	:
podobné	podobný	k2eAgNnSc1d1	podobné
penne	pennout	k5eAaPmIp3nS	pennout
<g/>
,	,	kIx,	,
jen	jen	k9	jen
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
s	s	k7c7	s
rovnými	rovný	k2eAgInPc7d1	rovný
konci	konec	k1gInPc7	konec
<g/>
.	.	kIx.	.
</s>
<s>
Lasagne	Lasagnout	k5eAaPmIp3nS	Lasagnout
<g/>
:	:	kIx,	:
asi	asi	k9	asi
2,5	[number]	k4	2,5
cm	cm	kA	cm
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
8	[number]	k4	8
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
těstovinové	těstovinový	k2eAgInPc4d1	těstovinový
pláty	plát	k1gInPc4	plát
<g/>
.	.	kIx.	.
</s>
<s>
Farfalle	Farfalle	k1gNnSc1	Farfalle
<g/>
:	:	kIx,	:
těstoviny	těstovina	k1gFnPc1	těstovina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
mašliček	mašlička	k1gFnPc2	mašlička
-	-	kIx~	-
motýlků	motýlek	k1gInPc2	motýlek
<g/>
.	.	kIx.	.
</s>
<s>
Fusilli	Fusille	k1gFnSc4	Fusille
<g/>
:	:	kIx,	:
vřetena	vřeteno	k1gNnSc2	vřeteno
<g/>
.	.	kIx.	.
</s>
<s>
Gnocchi	Gnocchi	k1gNnSc1	Gnocchi
<g/>
:	:	kIx,	:
noky	nok	k1gInPc1	nok
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
z	z	k7c2	z
bramborového	bramborový	k2eAgNnSc2d1	bramborové
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Radiatori	Radiatori	k1gNnSc1	Radiatori
<g/>
:	:	kIx,	:
tvar	tvar	k1gInSc1	tvar
noků	nok	k1gInPc2	nok
s	s	k7c7	s
žebrováním	žebrování	k1gNnSc7	žebrování
<g/>
.	.	kIx.	.
</s>
<s>
Cannelloni	Cannellon	k1gMnPc1	Cannellon
<g/>
:	:	kIx,	:
těstovinové	těstovinový	k2eAgFnPc4d1	těstovinová
trubičky	trubička	k1gFnPc4	trubička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
sýrem	sýr	k1gInSc7	sýr
<g/>
,	,	kIx,	,
špenátem	špenát	k1gInSc7	špenát
apod.	apod.	kA	apod.
Orecchiette	Orecchiett	k1gInSc5	Orecchiett
<g/>
:	:	kIx,	:
těstoviny	těstovina	k1gFnPc1	těstovina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
'	'	kIx"	'
<g/>
oušek	ouško	k1gNnPc2	ouško
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Cavatelli	Cavatell	k1gMnPc1	Cavatell
<g/>
:	:	kIx,	:
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
centimetry	centimetr	k1gInPc4	centimetr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
těstoviny	těstovina	k1gFnSc2	těstovina
<g/>
.	.	kIx.	.
</s>
<s>
Casarecce	Casarecce	k1gFnPc1	Casarecce
<g/>
:	:	kIx,	:
domácí	domácí	k2eAgFnPc1d1	domácí
těstoviny	těstovina	k1gFnPc1	těstovina
svinuté	svinutý	k2eAgFnPc1d1	svinutá
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
písmene	písmeno	k1gNnSc2	písmeno
S.	S.	kA	S.
Rotelle	Rotelle	k1gFnSc2	Rotelle
<g/>
:	:	kIx,	:
těstoviny	těstovina	k1gFnPc1	těstovina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kola	kolo	k1gNnSc2	kolo
od	od	k7c2	od
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Spätzle	Spätzle	k6eAd1	Spätzle
<g/>
:	:	kIx,	:
špecle	špecle	k6eAd1	špecle
Stelline	Stellin	k1gInSc5	Stellin
<g/>
:	:	kIx,	:
malé	malý	k2eAgFnPc1d1	malá
hvězdičky	hvězdička	k1gFnPc1	hvězdička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xC	jako
zavářka	zavářka	k1gFnSc1	zavářka
(	(	kIx(	(
<g/>
do	do	k7c2	do
polévek	polévka	k1gFnPc2	polévka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Peperini	Peperin	k1gMnPc1	Peperin
<g/>
:	:	kIx,	:
malé	malý	k2eAgFnPc4d1	malá
kuličky	kulička	k1gFnPc4	kulička
<g/>
/	/	kIx~	/
<g/>
kostičky	kostička	k1gFnPc4	kostička
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
nejmenší	malý	k2eAgInSc4d3	nejmenší
typ	typ	k1gInSc4	typ
polévkových	polévkový	k2eAgFnPc2d1	polévková
nudlí	nudle	k1gFnPc2	nudle
<g/>
.	.	kIx.	.
</s>
<s>
Alfabetti	Alfabetti	k1gNnSc1	Alfabetti
<g/>
:	:	kIx,	:
malé	malé	k1gNnSc1	malé
písmenka	písmenko	k1gNnSc2	písmenko
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
zejména	zejména	k9	zejména
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Anellini	Anellin	k2eAgMnPc1d1	Anellin
<g/>
:	:	kIx,	:
malé	malý	k2eAgInPc1d1	malý
kroužky	kroužek	k1gInPc1	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Capellini	Capellin	k2eAgMnPc1d1	Capellin
tagliati	tagliat	k1gMnPc1	tagliat
<g/>
:	:	kIx,	:
nalámané	nalámaný	k2eAgFnPc4d1	nalámaná
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgFnPc4d1	tenká
špagety	špagety	k1gFnPc4	špagety
<g/>
.	.	kIx.	.
</s>
<s>
Conchigliette	Conchigliette	k5eAaPmIp2nP	Conchigliette
<g/>
:	:	kIx,	:
mořské	mořský	k2eAgFnPc1d1	mořská
ulity	ulita	k1gFnPc1	ulita
<g/>
.	.	kIx.	.
</s>
<s>
Farfalline	Farfallin	k1gInSc5	Farfallin
<g/>
:	:	kIx,	:
farfalle	farfall	k1gMnPc4	farfall
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
