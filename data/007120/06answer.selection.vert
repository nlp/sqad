<s>
Těstoviny	těstovina	k1gFnPc1	těstovina
jsou	být	k5eAaImIp3nP	být
potravinový	potravinový	k2eAgInSc4d1	potravinový
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
nekynutého	kynutý	k2eNgMnSc2d1	nekynutý
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
nekypřeného	kypřený	k2eNgNnSc2d1	kypřený
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
