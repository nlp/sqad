<p>
<s>
Pětiseteurovka	Pětiseteurovka	k1gFnSc1	Pětiseteurovka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bankovka	bankovka	k1gFnSc1	bankovka
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
500	[number]	k4	500
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
€	€	k?	€
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
eurobankovkami	eurobankovka	k1gFnPc7	eurobankovka
bankovkou	bankovka	k1gFnSc7	bankovka
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
hodnotou	hodnota	k1gFnSc7	hodnota
a	a	k8xC	a
také	také	k9	také
největší	veliký	k2eAgFnSc7d3	veliký
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdobena	zdobit	k5eAaImNgFnS	zdobit
architektonickými	architektonický	k2eAgInPc7d1	architektonický
motivy	motiv	k1gInPc7	motiv
a	a	k8xC	a
laděna	laděn	k2eAgFnSc1d1	laděna
do	do	k7c2	do
růžové	růžový	k2eAgFnSc2d1	růžová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pětiseteurovky	Pětiseteurovka	k1gFnPc1	Pětiseteurovka
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
eurovými	eurův	k2eAgFnPc7d1	eurův
bankovkami	bankovka	k1gFnPc7	bankovka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
je	být	k5eAaImIp3nS	být
doprovázaly	doprovázat	k5eAaPmAgFnP	doprovázat
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
používány	používat	k5eAaImNgFnP	používat
především	především	k9	především
zločinci	zločinec	k1gMnSc3	zločinec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
legální	legální	k2eAgInPc1d1	legální
obchody	obchod	k1gInPc1	obchod
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výši	výše	k1gFnSc6	výše
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
realizují	realizovat	k5eAaBmIp3nP	realizovat
bezhotovostně	bezhotovostně	k6eAd1	bezhotovostně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvody	důvod	k1gInPc1	důvod
je	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
přestaly	přestat	k5eAaPmAgInP	přestat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
poskytovat	poskytovat	k5eAaImF	poskytovat
směnárny	směnárna	k1gFnPc4	směnárna
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
ministři	ministr	k1gMnPc1	ministr
financí	finance	k1gFnPc2	finance
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
Evropské	evropský	k2eAgFnSc3d1	Evropská
centrální	centrální	k2eAgFnSc3d1	centrální
bance	banka	k1gFnSc3	banka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dostupnost	dostupnost	k1gFnSc1	dostupnost
pětiseteurovek	pětiseteurovka	k1gFnPc2	pětiseteurovka
omezila	omezit	k5eAaPmAgFnS	omezit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pětiseteurovka	pětiseteurovka	k1gFnSc1	pětiseteurovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pětiseteurovka	pětiseteurovka	k1gFnSc1	pětiseteurovka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Britské	britský	k2eAgFnSc2d1	britská
směnárny	směnárna	k1gFnSc2	směnárna
přestávají	přestávat	k5eAaImIp3nP	přestávat
prodávat	prodávat	k5eAaImF	prodávat
bankovky	bankovka	k1gFnPc4	bankovka
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
500	[number]	k4	500
eur	euro	k1gNnPc2	euro
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
