<s>
Pětiseteurovky	Pětiseteurovka	k1gFnPc1	Pětiseteurovka
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
eurovými	eurův	k2eAgFnPc7d1	eurův
bankovkami	bankovka	k1gFnPc7	bankovka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
je	být	k5eAaImIp3nS	být
doprovázaly	doprovázat	k5eAaImAgFnP	doprovázat
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
používány	používat	k5eAaImNgFnP	používat
především	především	k9	především
zločinci	zločinec	k1gMnSc3	zločinec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
legální	legální	k2eAgInPc1d1	legální
obchody	obchod	k1gInPc1	obchod
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výši	výše	k1gFnSc6	výše
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
realizují	realizovat	k5eAaBmIp3nP	realizovat
bezhotovostně	bezhotovostně	k6eAd1	bezhotovostně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvody	důvod	k1gInPc1	důvod
je	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
přestaly	přestat	k5eAaPmAgInP	přestat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
poskytovat	poskytovat	k5eAaImF	poskytovat
směnárny	směnárna	k1gFnPc4	směnárna
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
