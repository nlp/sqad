<s desamb="1">
Je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mála	málo	k4c2
kytaristů	kytarista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
hrají	hrát	k5eAaImIp3nP
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
na	na	k7c4
kytaru	kytara	k1gFnSc4
určenou	určený	k2eAgFnSc4d1
pro	pro	k7c4
praváky	pravák	k1gMnPc4
(	(	kIx(
<g/>
se	s	k7c7
svisle	svisle	k6eAd1
prohozenými	prohozený	k2eAgFnPc7d1
strunami	struna	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>