<s>
Doyle	Doyle	k6eAd1
Bramhall	Bramhall	k1gInSc1
II	II	kA
</s>
<s>
Doyle	Doyle	k6eAd1
Bramhall	Bramhall	k1gInSc4
II	II	kA
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1968	#num#	k4
(	(	kIx(
<g/>
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Austin	Austin	k1gInSc1
Žánry	žánr	k1gInPc5
</s>
<s>
rock	rock	k1gInSc1
<g/>
,	,	kIx,
blues	blues	k1gFnSc1
a	a	k8xC
Americana	Americana	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
elektrická	elektrický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
a	a	k8xC
hlas	hlas	k1gInSc4
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Geffen	Geffen	k2eAgInSc1d1
RecordsConcord	RecordsConcord	k1gInSc1
RecordsProvogue	RecordsProvogu	k1gFnSc2
RecordsRCA	RecordsRCA	k1gMnSc2
Records	Recordsa	k1gFnPc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Doyle	Doyle	k6eAd1
Bramhall	Bramhall	k1gInSc1
Web	web	k1gInSc1
</s>
<s>
db	db	kA
<g/>
2	#num#	k4
<g/>
music	musice	k1gInPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Doyle	Doyle	k6eAd1
Bramhall	Bramhall	k1gInSc1
II	II	kA
(	(	kIx(
<g/>
*	*	kIx~
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1968	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
bluesový	bluesový	k2eAgMnSc1d1
a	a	k8xC
blues-rockový	blues-rockový	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
písní	píseň	k1gFnPc2
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
své	svůj	k3xOyFgFnSc2
kapely	kapela	k1gFnSc2
Smokestack	Smokestacka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mála	málo	k4c2
kytaristů	kytarista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
hrají	hrát	k5eAaImIp3nP
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
na	na	k7c4
kytaru	kytara	k1gFnSc4
určenou	určený	k2eAgFnSc4d1
pro	pro	k7c4
praváky	pravák	k1gMnPc4
(	(	kIx(
<g/>
se	s	k7c7
svisle	svisle	k6eAd1
prohozenými	prohozený	k2eAgFnPc7d1
strunami	struna	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
až	až	k9
2009	#num#	k4
byl	být	k5eAaImAgInS
také	také	k9
druhým	druhý	k4xOgMnSc7
kytaristou	kytarista	k1gMnSc7
ve	v	k7c6
skupině	skupina	k1gFnSc6
Erica	Ericus	k1gMnSc4
Claptona	Clapton	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
texaském	texaský	k2eAgInSc6d1
Dallasu	Dallas	k1gInSc6
na	na	k7c4
Vánoce	Vánoce	k1gFnPc4
roku	rok	k1gInSc2
1968	#num#	k4
jako	jako	k8xC,k8xS
syn	syn	k1gMnSc1
hudebníka	hudebník	k1gMnSc2
<g/>
,	,	kIx,
skladatele	skladatel	k1gMnSc2
a	a	k8xC
zpěváka	zpěvák	k1gMnSc2
Doyle	Doyl	k1gMnSc2
Bramhalla	Bramhall	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
přátelil	přátelit	k5eAaImAgMnS
se	s	k7c7
Stevem	Steve	k1gMnSc7
Rayem	Ray	k1gMnSc7
a	a	k8xC
Jimmiem	Jimmius	k1gMnSc7
Vaughanovými	Vaughanová	k1gFnPc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc7
kapelou	kapela	k1gFnSc7
Fabulous	Fabulous	k1gInSc4
Thunderbirds	Thunderbirdsa	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
později	pozdě	k6eAd2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
Doylovi	Doylův	k2eAgMnPc1d1
bylo	být	k5eAaImAgNnS
šestnáct	šestnáct	k4xCc4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
jezdil	jezdit	k5eAaImAgMnS
a	a	k8xC
hrál	hrát	k5eAaImAgMnS
jako	jako	k9
druhý	druhý	k4xOgMnSc1
kytarista	kytarista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
se	se	k3xPyFc4
Stevie	Stevie	k1gFnSc1
Ray	Ray	k1gFnSc2
Vaughanem	Vaughan	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnPc7
přáteli	přítel	k1gMnPc7
vytvořili	vytvořit	k5eAaPmAgMnP
skupinu	skupina	k1gFnSc4
Arc	Arc	k1gFnSc3
Angels	Angelsa	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
spolu	spolu	k6eAd1
nahráli	nahrát	k5eAaPmAgMnP,k5eAaBmAgMnP
jedno	jeden	k4xCgNnSc4
menší	malý	k2eAgNnSc4d2
album	album	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
rozdělili	rozdělit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
upsal	upsat	k5eAaPmAgMnS
nahrávací	nahrávací	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
RCA	RCA	kA
Records	Records	k1gInSc1
a	a	k8xC
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
na	na	k7c6
albu	album	k1gNnSc6
Jellycream	Jellycream	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
rukou	ruka	k1gFnPc2
Erica	Ericus	k1gMnSc2
Claptona	Clapton	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
jím	on	k3xPp3gNnSc7
byl	být	k5eAaImAgInS
natolik	natolik	k6eAd1
nadšen	nadšen	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
ním	on	k3xPp3gNnSc7
přiletěl	přiletět	k5eAaPmAgInS
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
spolu	spolu	k6eAd1
uspořádali	uspořádat	k5eAaPmAgMnP
několik	několik	k4yIc4
společných	společný	k2eAgNnPc2d1
vystoupení	vystoupení	k1gNnPc2
<g/>
,	,	kIx,
nahráli	nahrát	k5eAaBmAgMnP,k5eAaPmAgMnP
několik	několik	k4yIc4
písniček	písnička	k1gFnPc2
a	a	k8xC
udržují	udržovat	k5eAaImIp3nP
těsné	těsný	k2eAgNnSc4d1
přátelství	přátelství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eric	Eric	k1gInSc1
Clapton	Clapton	k1gInSc4
o	o	k7c6
něm	on	k3xPp3gInSc6
opakovaně	opakovaně	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
velkého	velký	k2eAgMnSc4d1
a	a	k8xC
nedoceněného	doceněný	k2eNgMnSc4d1
hudebníka	hudebník	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1999	#num#	k4
a	a	k8xC
2000	#num#	k4
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
jako	jako	k9
kytarista	kytarista	k1gMnSc1
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
turné	turné	k1gNnSc4
Rogera	Roger	k1gMnSc2
Waterse	Waterse	k1gFnSc2
In	In	k1gMnSc1
the	the	k?
Flesh	Flesh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
znovu	znovu	k6eAd1
zformoval	zformovat	k5eAaPmAgInS
skupinu	skupina	k1gFnSc4
Doyle	Doyle	k1gNnSc2
Bramhall	Bramhalla	k1gFnPc2
II	II	kA
&	&	k?
Smokestack	Smokestack	k1gInSc1
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
vydal	vydat	k5eAaPmAgMnS
třetí	třetí	k4xOgNnSc4
své	své	k1gNnSc4
větší	veliký	k2eAgNnSc1d2
album	album	k1gNnSc1
Welcome	Welcom	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
postoje	postoj	k1gInPc4
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1996	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
byl	být	k5eAaImAgMnS
ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
Susannah	Susannah	k1gMnSc1
Melvoinovou	Melvoinová	k1gFnSc7
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
Indii	Indie	k1gFnSc4
a	a	k8xC
Elle	Elle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dokumentu	dokument	k1gInSc6
Before	Befor	k1gInSc5
the	the	k?
Music	Musice	k1gInPc2
Dies	Diesa	k1gFnPc2
se	se	k3xPyFc4
jako	jako	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
respondentů	respondent	k1gMnPc2
svěřil	svěřit	k5eAaPmAgMnS
s	s	k7c7
frustrací	frustrace	k1gFnSc7
z	z	k7c2
hudebního	hudební	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
ryze	ryze	k6eAd1
obchodními	obchodní	k2eAgInPc7d1
motivy	motiv	k1gInPc7
<g/>
,	,	kIx,
odtržením	odtržení	k1gNnSc7
marketingových	marketingový	k2eAgMnPc2d1
záměrů	záměr	k1gInPc2
od	od	k7c2
podstaty	podstata	k1gFnSc2
skutečné	skutečný	k2eAgFnSc2d1
a	a	k8xC
dobré	dobrý	k2eAgFnSc2d1
muziky	muzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2012	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
udržoval	udržovat	k5eAaImAgInS
partnerský	partnerský	k2eAgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
herečkou	herečka	k1gFnSc7
Renée	René	k1gMnPc4
Zellwegerovou	Zellwegerová	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
se	s	k7c7
skupinou	skupina	k1gFnSc7
Arc	Arc	k1gFnSc2
Angels	Angelsa	k1gFnPc2
</s>
<s>
Arc	Arc	k?
Angels	Angels	k1gInSc1
(	(	kIx(
<g/>
Geffen	Geffen	k2eAgInSc1d1
Records	Records	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Living	Living	k1gInSc1
in	in	k?
a	a	k8xC
Dream	Dream	k1gInSc1
(	(	kIx(
<g/>
Mark	Mark	k1gMnSc1
I	i	k8xC
Music	Music	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
sólová	sólový	k2eAgNnPc1d1
</s>
<s>
Doyle	Doyle	k6eAd1
Bramhall	Bramhall	k1gInSc1
II	II	kA
(	(	kIx(
<g/>
Geffen	Geffen	k2eAgInSc1d1
Records	Records	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jellycream	Jellycream	k1gInSc1
(	(	kIx(
<g/>
RCA	RCA	kA
Records	Records	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Welcome	Welcom	k1gInSc5
(	(	kIx(
<g/>
RCA	RCA	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Doyle	Doyle	k1gNnSc2
Bramhall	Bramhalla	k1gFnPc2
II	II	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ABC	ABC	kA
News	News	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renée	René	k1gMnPc4
Zellweger	Zellweger	k1gMnSc1
Opens	Opensa	k1gFnPc2
Up	Up	k1gMnSc1
About	About	k1gMnSc1
Boyfriend	Boyfriend	k1gMnSc1
<g/>
.	.	kIx.
abcnews	abcnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
go	go	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Renee	Renee	k1gInSc1
Zellweger	Zellweger	k1gInSc1
<g/>
:	:	kIx,
My	my	k3xPp1nPc1
Boyfriend	Boyfriend	k1gInSc4
Is	Is	k1gFnPc4
'	'	kIx"
<g/>
a	a	k8xC
Very	Vera	k1gFnSc2
Special	Special	k1gMnSc1
Person	persona	k1gFnPc2
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jonathan	Jonathan	k1gMnSc1
Van	vana	k1gFnPc2
Meter	metro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renée	René	k1gMnPc4
Zellweger	Zellwegra	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Lost	Lost	k1gMnSc1
Decade	Decad	k1gInSc5
<g/>
.	.	kIx.
www.vulture.com	www.vulture.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vulture	Vultur	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Doyle	Doyle	k1gNnSc1
Bramhall	Bramhallum	k1gNnPc2
II	II	kA
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Doyle	Doyle	k1gNnSc1
Bramhall	Bramhallum	k1gNnPc2
II	II	kA
–	–	k?
fanouškovská	fanouškovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gInSc1
Waters	Waters	k1gInSc1
&	&	k?
Doyle	Doyle	k1gInSc1
Bramhall	Bramhall	k1gMnSc1
II	II	kA
–	–	k?
Comfortably	Comfortably	k1gMnSc1
Numb	Numb	k1gMnSc1
<g/>
,	,	kIx,
YouTube	YouTub	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
111367	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
136407803	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1454	#num#	k4
5704	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2001057174	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
107485419	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2001057174	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
