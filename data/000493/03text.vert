<s>
Angelologie	Angelologie	k1gFnSc1	Angelologie
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
ἄ	ἄ	k?	ἄ
-	-	kIx~	-
Angelos	Angelos	k1gMnSc1	Angelos
<g/>
,	,	kIx,	,
posel	posel	k1gMnSc1	posel
a	a	k8xC	a
λ	λ	k?	λ
-	-	kIx~	-
logie	logie	k1gFnSc1	logie
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starobylá	starobylý	k2eAgFnSc1d1	starobylá
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
andělích	andělí	k2eAgFnPc6d1	andělí
<g/>
.	.	kIx.	.
</s>
<s>
Henoch	Henoch	k1gMnSc1	Henoch
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
potomků	potomek	k1gMnPc2	potomek
Adamových	Adamová	k1gFnPc2	Adamová
žijících	žijící	k2eAgFnPc2d1	žijící
před	před	k7c7	před
potopou	potopa	k1gFnSc7	potopa
a	a	k8xC	a
pradědečkem	pradědeček	k1gMnSc7	pradědeček
Noeho	Noe	k1gMnSc2	Noe
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaly	dochovat	k5eAaPmAgInP	dochovat
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
dvě	dva	k4xCgNnPc1	dva
tzv.	tzv.	kA	tzv.
knihy	kniha	k1gFnPc4	kniha
Henochovy	Henochův	k2eAgFnPc4d1	Henochova
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
Henoch	Henoch	k1gInSc1	Henoch
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Staroslověnský	staroslověnský	k2eAgInSc1d1	staroslověnský
Henoch	Henoch	k1gInSc1	Henoch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Henochova	Henochův	k2eAgFnSc1d1	Henochova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
prostředí	prostředí	k1gNnSc6	prostředí
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
vlivný	vlivný	k2eAgMnSc1d1	vlivný
mystik	mystik	k1gMnSc1	mystik
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Pseudo-Dionýsios	Pseudo-Dionýsios	k1gMnSc1	Pseudo-Dionýsios
Areopagita	Areopagita	k1gMnSc1	Areopagita
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spise	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
O	o	k7c6	o
nebeské	nebeský	k2eAgFnSc6d1	nebeská
hierarchii	hierarchie	k1gFnSc6	hierarchie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Peri	peri	k1gFnSc1	peri
tés	tés	k?	tés
úranias	úranias	k1gMnSc1	úranias
hierarchias	hierarchias	k1gMnSc1	hierarchias
/	/	kIx~	/
De	De	k?	De
caelesti	caelest	k1gFnSc2	caelest
hierarchia	hierarchium	k1gNnSc2	hierarchium
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
-	-	kIx~	-
zčásti	zčásti	k6eAd1	zčásti
na	na	k7c6	na
základě	základ	k1gInSc6	základ
starších	starý	k2eAgFnPc2d2	starší
tradic	tradice	k1gFnPc2	tradice
-	-	kIx~	-
devět	devět	k4xCc1	devět
hierarchických	hierarchický	k2eAgInPc2d1	hierarchický
stupňů	stupeň	k1gInPc2	stupeň
andělů	anděl	k1gMnPc2	anděl
<g/>
,	,	kIx,	,
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
andělských	andělský	k2eAgMnPc2d1	andělský
chórů	chór	k1gInPc2	chór
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
triád	triáda	k1gFnPc2	triáda
či	či	k8xC	či
sfér	sféra	k1gFnPc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
trojici	trojice	k1gFnSc4	trojice
podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
učení	učení	k1gNnSc2	učení
tvoří	tvořit	k5eAaImIp3nP	tvořit
chóry	chór	k1gInPc4	chór
serafínů	serafín	k1gMnPc2	serafín
<g/>
,	,	kIx,	,
cherubínů	cherubín	k1gMnPc2	cherubín
a	a	k8xC	a
trůnů	trůn	k1gInPc2	trůn
<g/>
,	,	kIx,	,
prostřední	prostřední	k2eAgFnSc4d1	prostřední
triádu	triáda	k1gFnSc4	triáda
tvoří	tvořit	k5eAaImIp3nS	tvořit
panstva	panstvo	k1gNnSc2	panstvo
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sféře	sféra	k1gFnSc6	sféra
jsou	být	k5eAaImIp3nP	být
chóry	chór	k1gInPc1	chór
označené	označený	k2eAgFnSc2d1	označená
jako	jako	k8xS	jako
knížectva	knížectvo	k1gNnSc2	knížectvo
<g/>
,	,	kIx,	,
archandělé	archanděl	k1gMnPc1	archanděl
a	a	k8xC	a
andělé	anděl	k1gMnPc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Trithemius	Trithemius	k1gMnSc1	Trithemius
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1509	[number]	k4	1509
napsal	napsat	k5eAaBmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
De	De	k?	De
septem	septum	k1gNnSc7	septum
secundeis	secundeis	k1gFnSc2	secundeis
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
De	De	k?	De
septum	septum	k1gNnSc4	septum
secundeis	secundeis	k1gFnSc2	secundeis
(	(	kIx(	(
<g/>
O	o	k7c6	o
sedmi	sedm	k4xCc6	sedm
duších	duše	k1gFnPc6	duše
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
řídí	řídit	k5eAaImIp3nP	řídit
oběhy	oběh	k1gInPc4	oběh
nebeských	nebeský	k2eAgFnPc2d1	nebeská
sfér	sféra	k1gFnPc2	sféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
podává	podávat	k5eAaImIp3nS	podávat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
sedmi	sedm	k4xCc6	sedm
planetárních	planetární	k2eAgFnPc6d1	planetární
inteligencích	inteligence	k1gFnPc6	inteligence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
boha	bůh	k1gMnSc2	bůh
vládnou	vládnout	k5eAaImIp3nP	vládnout
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
věnoval	věnovat	k5eAaImAgMnS	věnovat
císaři	císař	k1gMnSc3	císař
Maxmiliánovi	Maxmilián	k1gMnSc3	Maxmilián
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slavný	slavný	k2eAgMnSc1d1	slavný
císaři	císař	k1gMnPc1	císař
<g/>
!	!	kIx.	!
</s>
<s>
Je	být	k5eAaImIp3nS	být
názorem	názor	k1gInSc7	názor
mnoha	mnoho	k4c2	mnoho
osobností	osobnost	k1gFnPc2	osobnost
starých	starý	k2eAgInPc2d1	starý
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
stvořený	stvořený	k2eAgInSc1d1	stvořený
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
Inteligence	inteligence	k1gFnSc2	inteligence
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
a	a	k8xC	a
usměrňován	usměrňovat	k5eAaImNgInS	usměrňovat
druhotnými	druhotný	k2eAgFnPc7d1	druhotná
inteligencemi	inteligence	k1gFnPc7	inteligence
<g/>
...	...	k?	...
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
sedmi	sedm	k4xCc7	sedm
duchy	duch	k1gMnPc7	duch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ustanoveni	ustanoven	k2eAgMnPc1d1	ustanoven
vládci	vládce	k1gMnPc1	vládce
sedmi	sedm	k4xCc2	sedm
planet	planeta	k1gFnPc2	planeta
<g/>
...	...	k?	...
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
<g/>
,	,	kIx,	,
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
vládne	vládnout	k5eAaImIp3nS	vládnout
světu	svět	k1gInSc3	svět
354	[number]	k4	354
let	léto	k1gNnPc2	léto
a	a	k8xC	a
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
po	po	k7c6	po
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
se	se	k3xPyFc4	se
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
zajímal	zajímat	k5eAaImAgMnS	zajímat
také	také	k9	také
Abano	Abano	k6eAd1	Abano
nebo	nebo	k8xC	nebo
Abraham	Abraham	k1gMnSc1	Abraham
ibn	ibn	k?	ibn
Ezra	Ezra	k1gMnSc1	Ezra
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
planet	planeta	k1gFnPc2	planeta
na	na	k7c4	na
život	život	k1gInSc4	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
i	i	k9	i
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
proroctvích	proroctví	k1gNnPc6	proroctví
se	se	k3xPyFc4	se
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
vyšších	vysoký	k2eAgFnPc2d2	vyšší
inteligencí	inteligence	k1gFnPc2	inteligence
opíral	opírat	k5eAaImAgMnS	opírat
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
proroctvích	proroctví	k1gNnPc6	proroctví
i	i	k8xC	i
Nostradamus	Nostradamus	k1gInSc4	Nostradamus
<g/>
.	.	kIx.	.
</s>
