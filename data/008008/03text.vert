<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
(	(	kIx(	(
<g/>
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
angličtině	angličtina	k1gFnSc6	angličtina
též	též	k9	též
buck	buck	k6eAd1	buck
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgFnSc1d1	oficiální
měna	měna	k1gFnSc1	měna
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
i	i	k8xC	i
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
amerických	americký	k2eAgNnPc2d1	americké
teritorií	teritorium	k1gNnPc2	teritorium
v	v	k7c6	v
Tichomořských	tichomořský	k2eAgInPc6d1	tichomořský
ostrovech	ostrov	k1gInPc6	ostrov
si	se	k3xPyFc3	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
nepřijaly	přijmout	k5eNaPmAgFnP	přijmout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
pevně	pevně	k6eAd1	pevně
vázánu	vázán	k2eAgFnSc4d1	vázána
na	na	k7c4	na
kurs	kurs	k1gInSc4	kurs
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
měna	měna	k1gFnSc1	měna
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
transakcích	transakce	k1gFnPc6	transakce
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
také	také	k9	také
široce	široko	k6eAd1	široko
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
bankovní	bankovní	k2eAgFnSc1d1	bankovní
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
téměř	téměř	k6eAd1	téměř
700	[number]	k4	700
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gNnSc4	on
mnohé	mnohé	k1gNnSc4	mnohé
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
drží	držet	k5eAaImIp3nS	držet
společně	společně	k6eAd1	společně
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
jako	jako	k8xS	jako
rezervu	rezerva	k1gFnSc4	rezerva
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
zaveden	zaveden	k2eAgInSc1d1	zaveden
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1785	[number]	k4	1785
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1792	[number]	k4	1792
byl	být	k5eAaImAgInS	být
redefinován	redefinovat	k5eAaImNgInS	redefinovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výnosu	výnos	k1gInSc2	výnos
Mint	Mint	k1gMnSc1	Mint
Act	Act	k1gMnSc1	Act
(	(	kIx(	(
<g/>
též	též	k9	též
Coinage	Coinage	k1gFnSc1	Coinage
Act	Act	k1gFnSc1	Act
of	of	k?	of
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gMnSc4	on
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
minci	mince	k1gFnSc4	mince
specifikované	specifikovaný	k2eAgFnSc2d1	specifikovaná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
ražený	ražený	k2eAgInSc1d1	ražený
státní	státní	k2eAgFnSc7d1	státní
mincovnou	mincovna	k1gFnSc7	mincovna
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dolarem	dolar	k1gInSc7	dolar
definoval	definovat	k5eAaBmAgInS	definovat
i	i	k9	i
zlaté	zlatý	k2eAgFnPc4d1	zlatá
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
eagly	eagl	k1gInPc7	eagl
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotu	hodnota	k1gFnSc4	hodnota
dolaru	dolar	k1gInSc2	dolar
spjal	spjal	k1gInSc4	spjal
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
<g/>
:	:	kIx,	:
1	[number]	k4	1
USD	USD	kA	USD
=	=	kIx~	=
371	[number]	k4	371
až	až	k9	až
416	[number]	k4	416
grainů	grain	k1gInPc2	grain
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
10	[number]	k4	10
USD	USD	kA	USD
=	=	kIx~	=
247	[number]	k4	247
až	až	k9	až
270	[number]	k4	270
grainů	grain	k1gInPc2	grain
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozsah	rozsah	k1gInSc1	rozsah
hmotností	hmotnost	k1gFnPc2	hmotnost
daných	daný	k2eAgInPc2d1	daný
kovů	kov	k1gInPc2	kov
závisel	záviset	k5eAaImAgInS	záviset
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
ryzostí	ryzost	k1gFnSc7	ryzost
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgFnPc1d1	stříbrná
mince	mince	k1gFnPc1	mince
měly	mít	k5eAaImAgFnP	mít
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
zosobnění	zosobnění	k1gNnSc2	zosobnění
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Liberty	Libert	k1gInPc1	Libert
<g/>
)	)	kIx)	)
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
LIBERTY	LIBERTY	kA	LIBERTY
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
zlaté	zlatý	k2eAgFnPc1d1	zlatá
mince	mince	k1gFnPc1	mince
měly	mít	k5eAaImAgFnP	mít
vyražený	vyražený	k2eAgInSc4d1	vyražený
znak	znak	k1gInSc4	znak
amerického	americký	k2eAgMnSc2d1	americký
orla	orel	k1gMnSc2	orel
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Eagle	Eagle	k1gInSc1	Eagle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Coinage	Coinage	k1gFnSc1	Coinage
Act	Act	k1gFnSc4	Act
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mince	mince	k1gFnPc4	mince
jiných	jiný	k2eAgInPc2d1	jiný
objemů	objem	k1gInPc2	objem
(	(	kIx(	(
<g/>
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
)	)	kIx)	)
–	–	k?	–
dvoudolar	dvoudolar	k1gInSc1	dvoudolar
(	(	kIx(	(
<g/>
double	double	k2eAgInSc4d1	double
dollar	dollar	k1gInSc4	dollar
nebo	nebo	k8xC	nebo
double	double	k2eAgNnSc4d1	double
eagle	eagle	k1gNnSc4	eagle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
půldolar	půldolar	k1gMnSc1	půldolar
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
fiftycent	fiftycent	k1gInSc1	fiftycent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtvrtdolar	čtvrtdolar	k1gInSc1	čtvrtdolar
(	(	kIx(	(
<g/>
quarter	quarter	k1gInSc1	quarter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
desetník	desetník	k1gInSc1	desetník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dime	dimat	k5eAaPmIp3nS	dimat
<g/>
)	)	kIx)	)
a	a	k8xC	a
pěticent	pěticent	k1gInSc1	pěticent
(	(	kIx(	(
<g/>
five	fivat	k5eAaPmIp3nS	fivat
cents	cents	k1gInSc1	cents
<g/>
,	,	kIx,	,
nickel	nickel	k1gInSc1	nickel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zlatého	zlatý	k2eAgInSc2d1	zlatý
standardu	standard	k1gInSc2	standard
hmotnostní	hmotnostní	k2eAgInPc1d1	hmotnostní
poměry	poměr	k1gInPc1	poměr
upraveny	upravit	k5eAaPmNgInP	upravit
na	na	k7c4	na
23,2	[number]	k4	23,2
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
23,22	[number]	k4	23,22
grainů	grain	k1gInPc2	grain
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
dolar	dolar	k1gInSc1	dolar
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
unce	unce	k1gFnSc1	unce
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Svázání	svázání	k1gNnSc1	svázání
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
se	s	k7c7	s
stříbrem	stříbro	k1gNnSc7	stříbro
trvalo	trvalo	k1gNnSc4	trvalo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
pojil	pojit	k5eAaImAgInS	pojit
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
ražení	ražení	k1gNnSc3	ražení
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
mincí	mince	k1gFnPc2	mince
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
(	(	kIx(	(
<g/>
s	s	k7c7	s
přechodným	přechodný	k2eAgNnSc7d1	přechodné
obdobím	období	k1gNnSc7	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
stříbro	stříbro	k1gNnSc1	stříbro
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
ze	z	k7c2	z
čtvrťáků	čtvrťák	k1gInPc2	čtvrťák
a	a	k8xC	a
desetníků	desetník	k1gInPc2	desetník
a	a	k8xC	a
z	z	k7c2	z
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
i	i	k8xC	i
z	z	k7c2	z
půldolarů	půldolar	k1gInPc2	půldolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
mince	mince	k1gFnPc1	mince
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
staženy	stáhnout	k5eAaPmNgInP	stáhnout
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Gold	Gold	k1gMnSc1	Gold
seizure	seizur	k1gMnSc5	seizur
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
zlata	zlato	k1gNnSc2	zlato
za	za	k7c4	za
dolar	dolar	k1gInSc4	dolar
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
13,71	[number]	k4	13,71
grainů	grain	k1gInPc2	grain
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
35	[number]	k4	35
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
troyskou	troyský	k2eAgFnSc4d1	troyská
unci	unce	k1gFnSc4	unce
<g/>
)	)	kIx)	)
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
standard	standard	k1gInSc4	standard
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
tzv.	tzv.	kA	tzv.
Brettonwoodský	Brettonwoodský	k2eAgInSc1d1	Brettonwoodský
měnový	měnový	k2eAgInSc1d1	měnový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hrál	hrát	k5eAaImAgInS	hrát
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
roli	role	k1gFnSc4	role
světové	světový	k2eAgFnSc2d1	světová
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
který	který	k3yIgInSc1	který
cizím	cizit	k5eAaImIp1nS	cizit
vládám	vláda	k1gFnPc3	vláda
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
jeho	jeho	k3xOp3gFnSc4	jeho
směnitelnost	směnitelnost	k1gFnSc4	směnitelnost
za	za	k7c4	za
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ztratil	ztratit	k5eAaPmAgInS	ztratit
dolar	dolar	k1gInSc4	dolar
i	i	k8xC	i
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
statut	statut	k1gInSc1	statut
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
petroměny	petroměn	k2eAgFnPc1d1	petroměn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1975	[number]	k4	1975
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
do	do	k7c2	do
éry	éra	k1gFnSc2	éra
tzv.	tzv.	kA	tzv.
plovoucích	plovoucí	k2eAgInPc2d1	plovoucí
(	(	kIx(	(
<g/>
měnových	měnový	k2eAgInPc2d1	měnový
<g/>
)	)	kIx)	)
kurzů	kurz	k1gInPc2	kurz
na	na	k7c6	na
měnových	měnový	k2eAgInPc6d1	měnový
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Nominální	nominální	k2eAgFnSc1d1	nominální
hodnota	hodnota	k1gFnSc1	hodnota
vůči	vůči	k7c3	vůči
troyské	troyský	k2eAgFnSc3d1	troyská
unci	unce	k1gFnSc3	unce
zlata	zlato	k1gNnSc2	zlato
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
z	z	k7c2	z
42,22	[number]	k4	42,22
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
1700	[number]	k4	1700
USD	USD	kA	USD
<g/>
/	/	kIx~	/
<g/>
unci	unce	k1gFnSc4	unce
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
kupní	kupní	k2eAgFnSc1d1	kupní
síla	síla	k1gFnSc1	síla
dolaru	dolar	k1gInSc2	dolar
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
Federálního	federální	k2eAgInSc2d1	federální
rezervního	rezervní	k2eAgInSc2d1	rezervní
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
cca	cca	kA	cca
4	[number]	k4	4
centy	cent	k1gInPc7	cent
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
dolar	dolar	k1gInSc1	dolar
historicky	historicky	k6eAd1	historicky
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
jáchymovského	jáchymovský	k2eAgInSc2d1	jáchymovský
tolaru	tolar	k1gInSc2	tolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Joachimsthal	Joachimsthal	k1gMnSc1	Joachimsthal
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
razily	razit	k5eAaImAgFnP	razit
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
mince	mince	k1gFnPc4	mince
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
jedné	jeden	k4xCgFnSc2	jeden
unce	unce	k1gFnSc2	unce
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
lidově	lidově	k6eAd1	lidově
řikalo	řikat	k5eAaPmAgNnS	řikat
Joachimsthalers	Joachimsthalers	k1gInSc4	Joachimsthalers
(	(	kIx(	(
<g/>
Jáchymovské	jáchymovský	k2eAgInPc4d1	jáchymovský
tolary	tolar	k1gInPc4	tolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jen	jen	k9	jen
thalers	thalers	k6eAd1	thalers
(	(	kIx(	(
<g/>
tolary	tolar	k1gInPc1	tolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
komolením	komolení	k1gNnSc7	komolení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
slovo	slovo	k1gNnSc1	slovo
dolar	dolar	k1gInSc1	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
označuje	označovat	k5eAaImIp3nS	označovat
symbolem	symbol	k1gInSc7	symbol
písmene	písmeno	k1gNnSc2	písmeno
S	s	k7c7	s
přeškrtnutého	přeškrtnutý	k2eAgMnSc4d1	přeškrtnutý
jednou	jednou	k6eAd1	jednou
či	či	k8xC	či
dvěma	dva	k4xCgFnPc7	dva
svislými	svislý	k2eAgFnPc7d1	svislá
čarami	čára	k1gFnPc7	čára
–	–	k?	–
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
podle	podle	k7c2	podle
ISO	ISO	kA	ISO
4217	[number]	k4	4217
je	být	k5eAaImIp3nS	být
USD	USD	kA	USD
<g/>
;	;	kIx,	;
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc4	označení
US	US	kA	US
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
pomocné	pomocný	k2eAgInPc1d1	pomocný
kódy	kód	k1gInPc1	kód
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
na	na	k7c6	na
burzách	burza	k1gFnPc6	burza
–	–	k?	–
USS	USS	kA	USS
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
zaplacený	zaplacený	k2eAgInSc4d1	zaplacený
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
dollar	dollar	k1gInSc1	dollar
same	same	k1gInSc1	same
day	day	k?	day
<g/>
)	)	kIx)	)
a	a	k8xC	a
USN	USN	kA	USN
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
zaplacený	zaplacený	k2eAgInSc4d1	zaplacený
příští	příští	k2eAgInSc4d1	příští
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
dollar	dollar	k1gInSc1	dollar
next	next	k1gInSc1	next
day	day	k?	day
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozlišení	rozlišení	k1gNnSc1	rozlišení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
potřeby	potřeba	k1gFnSc2	potřeba
opatření	opatření	k1gNnSc2	opatření
proti	proti	k7c3	proti
změnám	změna	k1gFnPc3	změna
kurzu	kurz	k1gInSc2	kurz
této	tento	k3xDgFnSc2	tento
světové	světový	k2eAgFnSc2d1	světová
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
je	být	k5eAaImIp3nS	být
zákonnou	zákonný	k2eAgFnSc7d1	zákonná
měnou	měna	k1gFnSc7	měna
v	v	k7c6	v
několika	několik	k4yIc6	několik
nezávislých	závislý	k2eNgInPc6d1	nezávislý
státech	stát	k1gInPc6	stát
a	a	k8xC	a
závislých	závislý	k2eAgNnPc6d1	závislé
územích	území	k1gNnPc6	území
<g/>
:	:	kIx,	:
Nezávislé	závislý	k2eNgInPc1d1	nezávislý
státy	stát	k1gInPc1	stát
Palau	Palaus	k1gInSc2	Palaus
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
Marshallovy	Marshallův	k2eAgInPc4d1	Marshallův
ostrovy	ostrov	k1gInPc4	ostrov
Salvador	Salvador	k1gMnSc1	Salvador
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
používá	používat	k5eAaImIp3nS	používat
dolarové	dolarový	k2eAgFnPc4d1	dolarová
bankovky	bankovka	k1gFnPc4	bankovka
i	i	k8xC	i
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souběžně	souběžně	k6eAd1	souběžně
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
mince	mince	k1gFnPc4	mince
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
používá	používat	k5eAaImIp3nS	používat
dolarové	dolarový	k2eAgFnPc4d1	dolarová
bankovky	bankovka	k1gFnPc4	bankovka
i	i	k8xC	i
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souběžně	souběžně	k6eAd1	souběžně
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
mince	mince	k1gFnSc1	mince
Panama	panama	k1gNnSc2	panama
má	mít	k5eAaImIp3nS	mít
<g />
.	.	kIx.	.
</s>
<s>
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
balbou	balbý	k2eAgFnSc4d1	balbý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
směnný	směnný	k2eAgInSc1d1	směnný
kurs	kurs	k1gInSc1	kurs
k	k	k7c3	k
dolaru	dolar	k1gInSc3	dolar
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
dolar	dolar	k1gInSc1	dolar
je	být	k5eAaImIp3nS	být
uznán	uznat	k5eAaPmNgInS	uznat
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
měna	měna	k1gFnSc1	měna
Kambodža	Kambodža	k1gFnSc1	Kambodža
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
riel	riel	k1gInSc4	riel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
drobné	drobný	k2eAgFnPc1d1	drobná
a	a	k8xC	a
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
je	být	k5eAaImIp3nS	být
vyměňována	vyměňován	k2eAgFnSc1d1	vyměňována
za	za	k7c4	za
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
natolik	natolik	k6eAd1	natolik
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
,	,	kIx,	,
že	že	k8xS	že
tamní	tamní	k2eAgFnSc1d1	tamní
vláda	vláda	k1gFnSc1	vláda
přijala	přijmout	k5eAaPmAgFnS	přijmout
opatření	opatření	k1gNnSc4	opatření
pozastavující	pozastavující	k2eAgFnSc4d1	pozastavující
platnost	platnost	k1gFnSc4	platnost
zimbabwského	zimbabwský	k2eAgInSc2d1	zimbabwský
dolaru	dolar	k1gInSc2	dolar
a	a	k8xC	a
umožňující	umožňující	k2eAgNnSc4d1	umožňující
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgNnSc4d1	oficiální
platidlo	platidlo	k1gNnSc4	platidlo
používat	používat	k5eAaImF	používat
několik	několik	k4yIc4	několik
světových	světový	k2eAgFnPc2d1	světová
měn	měna	k1gFnPc2	měna
včetně	včetně	k7c2	včetně
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgNnPc1d1	Britské
zámořská	zámořský	k2eAgNnPc1d1	zámořské
teritoria	teritorium	k1gNnPc1	teritorium
Britské	britský	k2eAgNnSc1d1	Britské
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
Turks	Turksa	k1gFnPc2	Turksa
a	a	k8xC	a
Caicos	Caicosa	k1gFnPc2	Caicosa
Autonomní	autonomní	k2eAgInPc1d1	autonomní
státy	stát	k1gInPc1	stát
volně	volně	k6eAd1	volně
přidružené	přidružený	k2eAgFnSc2d1	přidružená
k	k	k7c3	k
USA	USA	kA	USA
Americká	americký	k2eAgFnSc1d1	americká
Samoa	Samoa	k1gFnSc1	Samoa
Portoriko	Portoriko	k1gNnSc1	Portoriko
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
Guam	Guama	k1gFnPc2	Guama
Severní	severní	k2eAgFnSc2d1	severní
Mariany	Mariana	k1gFnSc2	Mariana
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
správní	správní	k2eAgInPc1d1	správní
obvody	obvod	k1gInPc1	obvod
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
Bonaire	Bonair	k1gInSc5	Bonair
Bonaire	Bonair	k1gInSc5	Bonair
Saba	Saba	k1gMnSc1	Saba
Saba	Sabum	k1gNnSc2	Sabum
Svatý	svatý	k1gMnSc1	svatý
Eustach	Eustach	k1gMnSc1	Eustach
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
měří	měřit	k5eAaImIp3nS	měřit
množství	množství	k1gNnSc2	množství
svého	svůj	k3xOyFgNnSc2	svůj
oběživa	oběživo	k1gNnSc2	oběživo
následujícími	následující	k2eAgInPc7d1	následující
indexy	index	k1gInPc7	index
(	(	kIx(	(
<g/>
řidčeji	řídce	k6eAd2	řídce
zvané	zvaný	k2eAgInPc1d1	zvaný
"	"	kIx"	"
<g/>
monetární	monetární	k2eAgInPc1d1	monetární
agregáty	agregát	k1gInPc1	agregát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
M0	M0	k1gFnSc1	M0
=	=	kIx~	=
veškeré	veškerý	k3xTgNnSc1	veškerý
fyzické	fyzický	k2eAgNnSc1d1	fyzické
oběživo	oběživo	k1gNnSc1	oběživo
+	+	kIx~	+
peníze	peníz	k1gInPc1	peníz
na	na	k7c6	na
účtech	účet	k1gInPc6	účet
Centrální	centrální	k2eAgFnPc1d1	centrální
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
proměněny	proměněn	k2eAgFnPc1d1	proměněna
ve	v	k7c4	v
fyzické	fyzický	k2eAgNnSc4d1	fyzické
oběživo	oběživo	k1gNnSc4	oběživo
<g/>
.	.	kIx.	.
</s>
<s>
M1	M1	k4	M1
=	=	kIx~	=
ty	ten	k3xDgFnPc1	ten
části	část	k1gFnPc1	část
M	M	kA	M
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
drženy	držet	k5eAaImNgFnP	držet
v	v	k7c6	v
trezorech	trezor	k1gInPc6	trezor
+	+	kIx~	+
objem	objem	k1gInSc1	objem
transakčních	transakční	k2eAgInPc2d1	transakční
depozitních	depozitní	k2eAgInPc2d1	depozitní
účtů	účet	k1gInPc2	účet
(	(	kIx(	(
<g/>
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgInP	soustředit
například	například	k6eAd1	například
peníze	peníz	k1gInPc1	peníz
pro	pro	k7c4	pro
platbu	platba	k1gFnSc4	platba
šeky	šek	k1gInPc4	šek
<g/>
)	)	kIx)	)
M2	M2	k1gFnSc1	M2
=	=	kIx~	=
M1	M1	k1gFnSc1	M1
+	+	kIx~	+
většina	většina	k1gFnSc1	většina
spořících	spořící	k2eAgInPc2d1	spořící
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
účtů	účet	k1gInPc2	účet
trhu	trh	k1gInSc6	trh
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
malých	malý	k2eAgInPc2d1	malý
termínovaných	termínovaný	k2eAgInPc2d1	termínovaný
vkladů	vklad	k1gInPc2	vklad
a	a	k8xC	a
vkladových	vkladový	k2eAgInPc2d1	vkladový
certifikátů	certifikát	k1gInPc2	certifikát
do	do	k7c2	do
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
M3	M3	k4	M3
=	=	kIx~	=
M2	M2	k1gFnPc4	M2
+	+	kIx~	+
ostatní	ostatní	k2eAgInPc4d1	ostatní
vkladové	vkladový	k2eAgInPc4d1	vkladový
certifikáty	certifikát	k1gInPc4	certifikát
<g/>
,	,	kIx,	,
vklady	vklad	k1gInPc4	vklad
eurodolarů	eurodolar	k1gInPc2	eurodolar
a	a	k8xC	a
skupní	skupnit	k5eAaPmIp3nP	skupnit
dohody	dohoda	k1gFnPc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
M3	M3	k1gFnSc2	M3
dává	dávat	k5eAaImIp3nS	dávat
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
obrázek	obrázek	k1gInSc4	obrázek
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
amerického	americký	k2eAgNnSc2d1	americké
oběživa	oběživo	k1gNnSc2	oběživo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
přestat	přestat	k5eAaPmF	přestat
jej	on	k3xPp3gMnSc4	on
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc4d1	oficiální
důvod	důvod	k1gInSc4	důvod
byl	být	k5eAaImAgMnS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zjišťování	zjišťování	k1gNnSc1	zjišťování
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
stojí	stát	k5eAaImIp3nP	stát
mnoho	mnoho	k4c4	mnoho
peněz	peníze	k1gInPc2	peníze
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
komentátoři	komentátor	k1gMnPc1	komentátor
však	však	k9	však
dávají	dávat	k5eAaImIp3nP	dávat
zákaz	zákaz	k1gInSc4	zákaz
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
uvažovaným	uvažovaný	k2eAgNnSc7d1	uvažované
otevřením	otevření	k1gNnSc7	otevření
íránské	íránský	k2eAgFnSc2d1	íránská
ropné	ropný	k2eAgFnSc2d1	ropná
burzy	burza	k1gFnSc2	burza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
éru	éra	k1gFnSc4	éra
obchodování	obchodování	k1gNnSc2	obchodování
s	s	k7c7	s
důležitými	důležitý	k2eAgFnPc7d1	důležitá
fosilními	fosilní	k2eAgFnPc7d1	fosilní
palivy	palivo	k1gNnPc7	palivo
striktně	striktně	k6eAd1	striktně
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nP	by
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
jistě	jistě	k6eAd1	jistě
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
devalvaci	devalvace	k1gFnSc3	devalvace
dolaru	dolar	k1gInSc2	dolar
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
-	-	kIx~	-
nakonec	nakonec	k6eAd1	nakonec
neuskutečněné	uskutečněný	k2eNgNnSc1d1	neuskutečněné
-	-	kIx~	-
otevření	otevření	k1gNnSc1	otevření
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c4	před
ukončení	ukončení	k1gNnSc4	ukončení
zveřejňování	zveřejňování	k1gNnSc2	zveřejňování
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
dolar	dolar	k1gInSc1	dolar
"	"	kIx"	"
<g/>
kryt	kryt	k1gInSc1	kryt
<g/>
"	"	kIx"	"
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gInSc6	jeho
hodnota	hodnota	k1gFnSc1	hodnota
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
množství	množství	k1gNnSc6	množství
zlata	zlato	k1gNnSc2	zlato
vázána	vázán	k2eAgFnSc1d1	vázána
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yRgFnPc4	který
některé	některý	k3yIgFnPc4	některý
evropské	evropský	k2eAgFnPc4d1	Evropská
země	zem	k1gFnPc4	zem
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
finanční	finanční	k2eAgFnPc4d1	finanční
krize	krize	k1gFnPc4	krize
a	a	k8xC	a
hyperinflaci	hyperinflace	k1gFnSc4	hyperinflace
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
celosvětově	celosvětově	k6eAd1	celosvětově
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Brettonwoodskou	Brettonwoodský	k2eAgFnSc7d1	Brettonwoodská
dohodou	dohoda	k1gFnSc7	dohoda
se	se	k3xPyFc4	se
z	z	k7c2	z
dolaru	dolar	k1gInSc2	dolar
stala	stát	k5eAaPmAgFnS	stát
oficiální	oficiální	k2eAgFnSc1d1	oficiální
světová	světový	k2eAgFnSc1d1	světová
rezervní	rezervní	k2eAgFnSc1d1	rezervní
měna	měna	k1gFnSc1	měna
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
měny	měna	k1gFnPc1	měna
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
byly	být	k5eAaImAgFnP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
<g/>
.	.	kIx.	.
</s>
<s>
Měnový	měnový	k2eAgInSc1d1	měnový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
Bretton	Bretton	k1gInSc4	Bretton
Woods	Woods	k1gInSc1	Woods
ustanovený	ustanovený	k2eAgInSc1d1	ustanovený
a	a	k8xC	a
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
variantou	varianta	k1gFnSc7	varianta
zlatého	zlatý	k2eAgInSc2d1	zlatý
standardu	standard	k1gInSc2	standard
<g/>
,	,	kIx,	,
trval	trvat	k5eAaImAgInS	trvat
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
přestal	přestat	k5eAaPmAgMnS	přestat
platit	platit	k5eAaImF	platit
princip	princip	k1gInSc4	princip
krytí	krytí	k1gNnSc4	krytí
dolaru	dolar	k1gInSc2	dolar
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
tato	tento	k3xDgFnSc1	tento
měna	měna	k1gFnSc1	měna
skokový	skokový	k2eAgInSc4d1	skokový
pokles	pokles	k1gInSc4	pokles
zájmu	zájem	k1gInSc2	zájem
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
časově	časově	k6eAd1	časově
kryl	krýt	k5eAaImAgMnS	krýt
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
ropným	ropný	k2eAgInSc7d1	ropný
šokem	šok	k1gInSc7	šok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nixonova	Nixonův	k2eAgFnSc1d1	Nixonova
administrativa	administrativa	k1gFnSc1	administrativa
hledala	hledat	k5eAaImAgFnS	hledat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
žádanost	žádanost	k1gFnSc1	žádanost
a	a	k8xC	a
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
národní	národní	k2eAgFnSc4d1	národní
měnu	měna	k1gFnSc4	měna
upevnit	upevnit	k5eAaPmF	upevnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
OPECu	OPECus	k1gInSc2	OPECus
(	(	kIx(	(
<g/>
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
předcházely	předcházet	k5eAaImAgFnP	předcházet
bilaterální	bilaterální	k2eAgFnPc1d1	bilaterální
dohody	dohoda	k1gFnPc1	dohoda
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
stal	stát	k5eAaPmAgInS	stát
jedinou	jediný	k2eAgFnSc7d1	jediná
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yQgMnPc4	který
byla	být	k5eAaImAgFnS	být
obchodována	obchodován	k2eAgFnSc1d1	obchodována
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
fosilní	fosilní	k2eAgNnPc1d1	fosilní
paliva	palivo	k1gNnPc1	palivo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
petrodolar	petrodolar	k1gInSc1	petrodolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
většinu	většina	k1gFnSc4	většina
zemí	zem	k1gFnPc2	zem
nutilo	nutit	k5eAaImAgNnS	nutit
opatřit	opatřit	k5eAaPmF	opatřit
si	se	k3xPyFc3	se
a	a	k8xC	a
spravovat	spravovat	k5eAaImF	spravovat
dolarové	dolarový	k2eAgFnPc4d1	dolarová
rezervy	rezerva	k1gFnPc4	rezerva
k	k	k7c3	k
nakupování	nakupování	k1gNnSc3	nakupování
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
pohledu	pohled	k1gInSc2	pohled
jejich	jejich	k3xOp3gInSc4	jejich
rezervní	rezervní	k2eAgFnSc1d1	rezervní
měna	měna	k1gFnSc1	měna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
tak	tak	k6eAd1	tak
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jím	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ropných	ropný	k2eAgFnPc6d1	ropná
burzách	burza	k1gFnPc6	burza
zaplaceno	zaplatit	k5eAaPmNgNnS	zaplatit
zemím	zem	k1gFnPc3	zem
vyvážejícím	vyvážející	k2eAgFnPc3d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
většinou	většinou	k6eAd1	většinou
investovaly	investovat	k5eAaBmAgInP	investovat
za	za	k7c4	za
služby	služba	k1gFnPc4	služba
nebo	nebo	k8xC	nebo
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k9	i
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
zemí	zem	k1gFnPc2	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
udržení	udržení	k1gNnSc6	udržení
nebo	nebo	k8xC	nebo
expanzi	expanze	k1gFnSc3	expanze
své	svůj	k3xOyFgFnSc2	svůj
ropné	ropný	k2eAgFnSc2d1	ropná
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ropná	ropný	k2eAgNnPc1d1	ropné
naleziště	naleziště	k1gNnPc1	naleziště
vyčerpávají	vyčerpávat	k5eAaImIp3nP	vyčerpávat
a	a	k8xC	a
ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
,	,	kIx,	,
především	především	k9	především
ale	ale	k8xC	ale
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
zvyšující	zvyšující	k2eAgInSc1d1	zvyšující
objem	objem	k1gInSc1	objem
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
uvolňování	uvolňování	k1gNnSc1	uvolňování
dluhopisů	dluhopis	k1gInPc2	dluhopis
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
americké	americký	k2eAgFnSc2d1	americká
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
fakticky	fakticky	k6eAd1	fakticky
snižují	snižovat	k5eAaImIp3nP	snižovat
počet	počet	k1gInSc4	počet
emitovaných	emitovaný	k2eAgInPc2d1	emitovaný
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
jeho	jeho	k3xOp3gNnSc1	jeho
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
coby	coby	k?	coby
měně	měna	k1gFnSc6	měna
nutné	nutný	k2eAgNnSc1d1	nutné
k	k	k7c3	k
nákupu	nákup	k1gInSc3	nákup
potřebné	potřebný	k2eAgFnSc2d1	potřebná
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
poptávky	poptávka	k1gFnSc2	poptávka
si	se	k3xPyFc3	se
americká	americký	k2eAgFnSc1d1	americká
státní	státní	k2eAgFnSc1d1	státní
pokladna	pokladna	k1gFnSc1	pokladna
mohla	moct	k5eAaImAgFnS	moct
pravidelně	pravidelně	k6eAd1	pravidelně
dovolit	dovolit	k5eAaPmF	dovolit
emitovat	emitovat	k5eAaBmF	emitovat
nové	nový	k2eAgInPc4d1	nový
dolary	dolar	k1gInPc4	dolar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
do	do	k7c2	do
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
oběhu	oběh	k1gInSc2	oběh
zařadila	zařadit	k5eAaPmAgFnS	zařadit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
nakoupila	nakoupit	k5eAaPmAgFnS	nakoupit
produkty	produkt	k1gInPc4	produkt
nebo	nebo	k8xC	nebo
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
také	také	k9	také
důvod	důvod	k1gInSc1	důvod
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
nevyrovnané	vyrovnaný	k2eNgFnSc2d1	nevyrovnaná
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
importu	import	k1gInSc2	import
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
rozdíl	rozdíl	k1gInSc1	rozdíl
činí	činit	k5eAaImIp3nS	činit
nominálně	nominálně	k6eAd1	nominálně
cca	cca	kA	cca
470	[number]	k4	470
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
světového	světový	k2eAgInSc2d1	světový
statutu	statut	k1gInSc2	statut
dolaru	dolar	k1gInSc2	dolar
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
americkou	americký	k2eAgFnSc4d1	americká
ekonomiku	ekonomika	k1gFnSc4	ekonomika
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
před	před	k7c4	před
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
"	"	kIx"	"
<g/>
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
"	"	kIx"	"
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
saldo	saldo	k1gNnSc4	saldo
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
diskurzu	diskurz	k1gInSc6	diskurz
posledních	poslední	k2eAgFnPc2d1	poslední
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
a	a	k8xC	a
petrodolarový	petrodolarový	k2eAgInSc1d1	petrodolarový
cyklus	cyklus	k1gInSc1	cyklus
byl	být	k5eAaImAgInS	být
narušen	narušit	k5eAaPmNgInS	narušit
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
platbě	platba	k1gFnSc6	platba
za	za	k7c4	za
ropu	ropa	k1gFnSc4	ropa
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
měně	měna	k1gFnSc6	měna
než	než	k8xS	než
americkém	americký	k2eAgInSc6d1	americký
dolaru	dolar	k1gInSc6	dolar
poprvé	poprvé	k6eAd1	poprvé
začal	začít	k5eAaPmAgInS	začít
hovořit	hovořit	k5eAaImF	hovořit
Írán	Írán	k1gInSc1	Írán
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
začal	začít	k5eAaPmAgInS	začít
za	za	k7c4	za
ropu	ropa	k1gFnSc4	ropa
přijímat	přijímat	k5eAaImF	přijímat
jinou	jiný	k2eAgFnSc4d1	jiná
měnu	měna	k1gFnSc4	měna
než	než	k8xS	než
dolary	dolar	k1gInPc4	dolar
(	(	kIx(	(
<g/>
euro	euro	k1gNnSc1	euro
<g/>
,	,	kIx,	,
za	za	k7c4	za
program	program	k1gInSc4	program
Food	Food	k1gInSc4	Food
for	forum	k1gNnPc2	forum
Oil	Oil	k1gMnSc2	Oil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Saddám	Saddám	k1gMnSc1	Saddám
Hussain	Hussain	k1gMnSc1	Hussain
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
byl	být	k5eAaImAgInS	být
prodej	prodej	k1gInSc1	prodej
irácké	irácký	k2eAgFnSc2d1	irácká
ropy	ropa	k1gFnSc2	ropa
"	"	kIx"	"
<g/>
přepnut	přepnut	k2eAgMnSc1d1	přepnut
<g/>
"	"	kIx"	"
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
míře	míra	k1gFnSc6	míra
Írán	Írán	k1gInSc1	Írán
započaly	započnout	k5eAaPmAgFnP	započnout
trend	trend	k1gInSc4	trend
diverzifikace	diverzifikace	k1gFnSc2	diverzifikace
rezervní	rezervní	k2eAgFnSc2d1	rezervní
měny	měna	k1gFnSc2	měna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
inicializovaly	inicializovat	k5eAaImAgFnP	inicializovat
a	a	k8xC	a
podporovaly	podporovat	k5eAaImAgFnP	podporovat
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
nahrazení	nahrazení	k1gNnSc6	nahrazení
dolaru	dolar	k1gInSc2	dolar
něčím	něčí	k3xOyIgNnSc7	něčí
jiným	jiný	k2eAgMnSc7d1	jiný
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
<g/>
:	:	kIx,	:
jinou	jiný	k2eAgFnSc7d1	jiná
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
krytou	krytý	k2eAgFnSc4d1	krytá
komoditním	komoditní	k2eAgInSc7d1	komoditní
košem	koš	k1gInSc7	koš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
bilaterálně	bilaterálně	k6eAd1	bilaterálně
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
zeměmi	zem	k1gFnPc7	zem
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
)	)	kIx)	)
i	i	k8xC	i
významnými	významný	k2eAgInPc7d1	významný
soukromými	soukromý	k2eAgInPc7d1	soukromý
subjekty	subjekt	k1gInPc7	subjekt
o	o	k7c6	o
nákupu	nákup	k1gInSc6	nákup
ropy	ropa	k1gFnSc2	ropa
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
lokální	lokální	k2eAgFnSc4d1	lokální
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
patrně	patrně	k6eAd1	patrně
zašel	zajít	k5eAaPmAgInS	zajít
nejdále	daleko	k6eAd3	daleko
–	–	k?	–
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgInSc3d1	původní
plánu	plán	k1gInSc3	plán
<g/>
)	)	kIx)	)
otevřel	otevřít	k5eAaPmAgInS	otevřít
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
ropnou	ropný	k2eAgFnSc4d1	ropná
burzu	burza	k1gFnSc4	burza
obchodující	obchodující	k2eAgFnSc4d1	obchodující
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
a	a	k8xC	a
deriváty	derivát	k1gInPc1	derivát
s	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
explicitně	explicitně	k6eAd1	explicitně
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
při	při	k7c6	při
platbách	platba	k1gFnPc6	platba
<g/>
,	,	kIx,	,
zbavil	zbavit	k5eAaPmAgMnS	zbavit
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
85	[number]	k4	85
%	%	kIx~	%
dolarů	dolar	k1gInPc2	dolar
coby	coby	k?	coby
rezervní	rezervní	k2eAgFnSc2d1	rezervní
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
zbavil	zbavit	k5eAaPmAgMnS	zbavit
se	se	k3xPyFc4	se
bezprostředních	bezprostřední	k2eAgFnPc2d1	bezprostřední
transakcí	transakce	k1gFnPc2	transakce
s	s	k7c7	s
americkými	americký	k2eAgFnPc7d1	americká
bankami	banka	k1gFnPc7	banka
a	a	k8xC	a
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
mnoho	mnoho	k4c4	mnoho
kontraktů	kontrakt	k1gInPc2	kontrakt
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
íránské	íránský	k2eAgFnSc2d1	íránská
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
měnách	měna	k1gFnPc6	měna
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
vystoupení	vystoupení	k1gNnSc1	vystoupení
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
zemí	zem	k1gFnPc2	zem
proti	proti	k7c3	proti
privilegiu	privilegium	k1gNnSc3	privilegium
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
motivovala	motivovat	k5eAaBmAgFnS	motivovat
další	další	k2eAgInPc4d1	další
státy	stát	k1gInPc4	stát
a	a	k8xC	a
spustila	spustit	k5eAaPmAgFnS	spustit
změnu	změna	k1gFnSc4	změna
paradigmatu	paradigma	k1gNnSc2	paradigma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
má	mít	k5eAaImIp3nS	mít
takovou	takový	k3xDgFnSc4	takový
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
budou	být	k5eAaImBp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
tento	tento	k3xDgInSc4	tento
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
<g/>
)	)	kIx)	)
trend	trend	k1gInSc4	trend
zvrátit	zvrátit	k5eAaPmF	zvrátit
a	a	k8xC	a
opět	opět	k6eAd1	opět
nastolit	nastolit	k5eAaPmF	nastolit
petrodolarový	petrodolarový	k2eAgInSc4d1	petrodolarový
cyklus	cyklus	k1gInSc4	cyklus
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
podle	podle	k7c2	podle
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
"	"	kIx"	"
<g/>
běží	běžet	k5eAaImIp3nS	běžet
<g/>
"	"	kIx"	"
jen	jen	k9	jen
na	na	k7c4	na
68	[number]	k4	68
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Faktorem	faktor	k1gMnSc7	faktor
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
transformaci	transformace	k1gFnSc6	transformace
je	být	k5eAaImIp3nS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
sesazení	sesazení	k1gNnSc1	sesazení
dolaru	dolar	k1gInSc2	dolar
z	z	k7c2	z
privilegované	privilegovaný	k2eAgFnSc2d1	privilegovaná
pozice	pozice	k1gFnSc2	pozice
petroměny	petroměn	k2eAgFnPc1d1	petroměn
nakloněno	naklonit	k5eAaPmNgNnS	naklonit
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
<g/>
,	,	kIx,	,
volil	volit	k5eAaImAgInS	volit
Federální	federální	k2eAgInSc1d1	federální
rezervní	rezervní	k2eAgInSc4d1	rezervní
systém	systém	k1gInSc4	systém
taktiku	taktika	k1gFnSc4	taktika
masívního	masívní	k2eAgInSc2d1	masívní
tisku	tisk	k1gInSc2	tisk
nových	nový	k2eAgFnPc2d1	nová
bankovek	bankovka	k1gFnPc2	bankovka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
distribuce	distribuce	k1gFnSc1	distribuce
do	do	k7c2	do
peněžního	peněžní	k2eAgInSc2d1	peněžní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
inflaci	inflace	k1gFnSc4	inflace
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
s	s	k7c7	s
reálnou	reálný	k2eAgFnSc7d1	reálná
hrozbou	hrozba	k1gFnSc7	hrozba
rozběhnutí	rozběhnutí	k1gNnSc2	rozběhnutí
hyperinflace	hyperinflace	k1gFnPc1	hyperinflace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
okolností	okolnost	k1gFnSc7	okolnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tomu	ten	k3xDgMnSc3	ten
nenasvědčuje	nasvědčovat	k5eNaImIp3nS	nasvědčovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2000	[number]	k4	2000
a	a	k8xC	a
2007	[number]	k4	2007
ztratil	ztratit	k5eAaPmAgInS	ztratit
cca	cca	kA	cca
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
své	svůj	k3xOyFgFnSc2	svůj
hodnoty	hodnota	k1gFnSc2	hodnota
vůči	vůči	k7c3	vůči
euru	euro	k1gNnSc3	euro
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
státy	stát	k1gInPc1	stát
přehodnotily	přehodnotit	k5eAaPmAgInP	přehodnotit
pozici	pozice	k1gFnSc3	pozice
dolaru	dolar	k1gInSc2	dolar
coby	coby	k?	coby
rezervní	rezervní	k2eAgFnSc2d1	rezervní
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
diverzifikovat	diverzifikovat	k5eAaImF	diverzifikovat
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
změně	změna	k1gFnSc6	změna
systému	systém	k1gInSc2	systém
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
státní	státní	k2eAgFnSc7d1	státní
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
světovou	světový	k2eAgFnSc7d1	světová
rezervní	rezervní	k2eAgFnSc7d1	rezervní
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
v	v	k7c6	v
Davosu	Davos	k1gInSc6	Davos
Vladimir	Vladimir	k1gInSc1	Vladimir
Putin	putin	k2eAgInSc1d1	putin
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
guvernér	guvernér	k1gMnSc1	guvernér
čínské	čínský	k2eAgFnSc2d1	čínská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
Čou	Čou	k1gMnSc1	Čou
Siao-čchuan	Siao-čchuan	k1gMnSc1	Siao-čchuan
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vytvoření	vytvoření	k1gNnSc4	vytvoření
světové	světový	k2eAgFnSc2d1	světová
rezervní	rezervní	k2eAgFnSc2d1	rezervní
měny	měna	k1gFnSc2	měna
namísto	namísto	k7c2	namísto
dolaru	dolar	k1gInSc2	dolar
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
áklady	áklada	k1gFnPc1	áklada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
svět	svět	k1gInSc1	svět
nese	nést	k5eAaImIp3nS	nést
za	za	k7c4	za
existující	existující	k2eAgInSc4d1	existující
měnový	měnový	k2eAgInSc4d1	měnový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
možná	možná	k6eAd1	možná
předčily	předčit	k5eAaBmAgInP	předčit
jeho	jeho	k3xOp3gInPc1	jeho
zisky	zisk	k1gInPc1	zisk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
OSN	OSN	kA	OSN
ke	k	k7c3	k
Konferenci	konference	k1gFnSc3	konference
o	o	k7c6	o
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc6	rozvoj
(	(	kIx(	(
<g/>
UNCTAD	UNCTAD	kA	UNCTAD
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
finanční	finanční	k2eAgInSc1d1	finanční
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dolar	dolar	k1gInSc1	dolar
jedinou	jediný	k2eAgFnSc7d1	jediná
rezervní	rezervní	k2eAgFnSc7d1	rezervní
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
projít	projít	k5eAaPmF	projít
celkovým	celkový	k2eAgNnSc7d1	celkové
přehodnocením	přehodnocení	k1gNnSc7	přehodnocení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
deník	deník	k1gInSc1	deník
The	The	k1gFnPc2	The
Independent	independent	k1gMnSc1	independent
přinesl	přinést	k5eAaPmAgMnS	přinést
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
jednáních	jednání	k1gNnPc6	jednání
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
států	stát	k1gInPc2	stát
OPEC	opéct	k5eAaPmRp2nSwK	opéct
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
o	o	k7c4	o
nahrazení	nahrazení	k1gNnSc4	nahrazení
dolaru	dolar	k1gInSc2	dolar
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
světové	světový	k2eAgFnSc2d1	světová
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sesazením	sesazení	k1gNnSc7	sesazení
dolaru	dolar	k1gInSc2	dolar
se	se	k3xPyFc4	se
ztotožnili	ztotožnit	k5eAaPmAgMnP	ztotožnit
i	i	k9	i
významní	významný	k2eAgMnPc1d1	významný
ekonomové	ekonom	k1gMnPc1	ekonom
ze	z	k7c2	z
samotných	samotný	k2eAgInPc2d1	samotný
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
jako	jako	k8xC	jako
Josef	Josef	k1gMnSc1	Josef
Stiglitz	Stiglitz	k1gMnSc1	Stiglitz
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Volker	Volker	k1gMnSc1	Volker
nebo	nebo	k8xC	nebo
George	Georg	k1gMnSc4	Georg
Soros	Soros	k1gInSc4	Soros
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
či	či	k8xC	či
ředitel	ředitel	k1gMnSc1	ředitel
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Zoelick	Zoelick	k1gMnSc1	Zoelick
<g/>
.	.	kIx.	.
</s>
<s>
Kroky	krok	k1gInPc4	krok
k	k	k7c3	k
sesazení	sesazení	k1gNnSc3	sesazení
dolaru	dolar	k1gInSc2	dolar
coby	coby	k?	coby
obchodní	obchodní	k2eAgFnSc2d1	obchodní
měny	měna	k1gFnSc2	měna
podnikají	podnikat	k5eAaImIp3nP	podnikat
i	i	k9	i
země	zem	k1gFnPc1	zem
BRICS	BRICS	kA	BRICS
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
nasazení	nasazení	k1gNnSc6	nasazení
svých	svůj	k3xOyFgFnPc2	svůj
národních	národní	k2eAgFnPc2d1	národní
měn	měna	k1gFnPc2	měna
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
obchodu	obchod	k1gInSc6	obchod
tak	tak	k6eAd1	tak
v	v	k7c6	v
úvěrech	úvěr	k1gInPc6	úvěr
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
společné	společný	k2eAgFnSc2d1	společná
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Zhou	Zhou	k6eAd1	Zhou
Xiaochuan	Xiaochuan	k1gMnSc1	Xiaochuan
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
statut	statut	k1gInSc4	statut
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc4d1	dobrý
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nyní	nyní	k6eAd1	nyní
zmatený	zmatený	k2eAgInSc1d1	zmatený
svět	svět	k1gInSc1	svět
byl	být	k5eAaImAgInS	být
přebudován	přebudován	k2eAgMnSc1d1	přebudován
a	a	k8xC	a
deamerikanizován	deamerikanizován	k2eAgMnSc1d1	deamerikanizován
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
čínské	čínský	k2eAgFnSc2d1	čínská
tiskové	tiskový	k2eAgFnSc2d1	tisková
agentury	agentura	k1gFnSc2	agentura
Xinhua	Xinhua	k1gFnSc1	Xinhua
mezi	mezi	k7c7	mezi
návrhy	návrh	k1gInPc7	návrh
Číny	Čína	k1gFnSc2	Čína
patří	patřit	k5eAaImIp3nP	patřit
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
nahradila	nahradit	k5eAaPmAgFnS	nahradit
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnPc1	mince
jsou	být	k5eAaImIp3nP	být
raženy	razit	k5eAaImNgFnP	razit
v	v	k7c6	v
nominálních	nominální	k2eAgFnPc6d1	nominální
hodnotách	hodnota	k1gFnPc6	hodnota
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
50	[number]	k4	50
centů	cent	k1gInPc2	cent
a	a	k8xC	a
1	[number]	k4	1
dolar	dolar	k1gInSc1	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
bankovky	bankovka	k1gFnPc1	bankovka
dolaru	dolar	k1gInSc2	dolar
mají	mít	k5eAaImIp3nP	mít
hodnoty	hodnota	k1gFnSc2	hodnota
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
100	[number]	k4	100
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Amero	Amero	k1gNnSc1	Amero
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
soudobně	soudobně	k6eAd1	soudobně
se	se	k3xPyFc4	se
vznikem	vznik	k1gInSc7	vznik
eura	euro	k1gNnSc2	euro
začaly	začít	k5eAaPmAgInP	začít
některé	některý	k3yIgFnPc4	některý
americké	americký	k2eAgFnPc4d1	americká
think-tanky	thinkanka	k1gFnPc4	think-tanka
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
koncepci	koncepce	k1gFnSc4	koncepce
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
měnové	měnový	k2eAgFnSc6d1	měnová
unii	unie	k1gFnSc6	unie
nejdříve	dříve	k6eAd3	dříve
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
eventuálně	eventuálně	k6eAd1	eventuálně
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
2007	[number]	k4	2007
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
předběžná	předběžný	k2eAgNnPc1d1	předběžné
jednání	jednání	k1gNnPc1	jednání
hlav	hlava	k1gFnPc2	hlava
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
nést	nést	k5eAaImF	nést
jméno	jméno	k1gNnSc4	jméno
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
měna	měna	k1gFnSc1	měna
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgNnPc4d1	nazváno
Amero	Amero	k1gNnSc4	Amero
<g/>
.	.	kIx.	.
</s>
