<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
(	(	kIx(	(
<g/>
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
angličtině	angličtina	k1gFnSc6	angličtina
též	též	k9	též
buck	buck	k6eAd1	buck
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgFnSc1d1	oficiální
měna	měna	k1gFnSc1	měna
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
i	i	k8xC	i
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
amerických	americký	k2eAgNnPc2d1	americké
teritorií	teritorium	k1gNnPc2	teritorium
v	v	k7c6	v
Tichomořských	tichomořský	k2eAgInPc6d1	tichomořský
ostrovech	ostrov	k1gInPc6	ostrov
si	se	k3xPyFc3	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
nepřijaly	přijmout	k5eNaPmAgFnP	přijmout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
pevně	pevně	k6eAd1	pevně
vázánu	vázán	k2eAgFnSc4d1	vázána
na	na	k7c4	na
kurs	kurs	k1gInSc4	kurs
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
