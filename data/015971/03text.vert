<s>
César	César	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
francouzské	francouzský	k2eAgFnSc3d1
filmové	filmový	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
římském	římský	k2eAgInSc6d1
vojevůdci	vojevůdce	k1gMnSc3
a	a	k8xC
konzulovi	konzul	k1gMnSc3
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
César	César	k1gMnSc1
soška	soška	k1gFnSc1
CésaraUdělováno	CésaraUdělován	k2eAgNnSc1d1
</s>
<s>
za	za	k7c4
nejlepší	dobrý	k2eAgInPc4d3
filmové	filmový	k2eAgInPc4d1
počiny	počin	k1gInPc4
ve	v	k7c6
francouzské	francouzský	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
Datum	datum	k1gNnSc1
</s>
<s>
únor	únor	k1gInSc1
/	/	kIx~
březen	březen	k1gInSc1
Místo	místo	k7c2
</s>
<s>
Olympia	Olympia	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
2021	#num#	k4
<g/>
)	)	kIx)
Země	zem	k1gFnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
Uděluje	udělovat	k5eAaImIp3nS
</s>
<s>
Académie	Académie	k1gFnSc1
des	des	k1gNnSc2
Arts	Artsa	k1gFnPc2
et	et	k?
Techniques	Techniques	k1gMnSc1
du	du	k?
Cinéma	Cinéma	k1gFnSc1
Moderátoři	moderátor	k1gMnPc5
</s>
<s>
Marina	Marina	k1gFnSc1
Foï	Foï	k1gFnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
První	první	k4xOgInSc1
ročník	ročník	k1gInSc1
</s>
<s>
1976	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.academie-cinema.org	www.academie-cinema.org	k1gInSc1
Televizní	televizní	k2eAgInSc1d1
přenos	přenos	k1gInSc1
TV	TV	kA
stanice	stanice	k1gFnSc2
</s>
<s>
France	Franc	k1gMnSc2
2	#num#	k4
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
Canal	Canal	k1gInSc1
<g/>
+	+	kIx~
(	(	kIx(
<g/>
od	od	k7c2
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
César	César	k1gMnSc1
je	být	k5eAaImIp3nS
francouzské	francouzský	k2eAgNnSc4d1
filmové	filmový	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
uděluje	udělovat	k5eAaImIp3nS
Filmová	filmový	k2eAgFnSc1d1
umělecká	umělecký	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
’	’	k?
<g/>
Académie	Académie	k1gFnSc2
des	des	k1gNnSc2
Arts	Artsa	k1gFnPc2
et	et	k?
Techniques	Techniques	k1gMnSc1
du	du	k?
Cinéma	Cinéma	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
udělování	udělování	k1gNnSc2
je	být	k5eAaImIp3nS
zviditelnění	zviditelnění	k1gNnSc4
nejlepších	dobrý	k2eAgInPc2d3
francouzských	francouzský	k2eAgInPc2d1
filmů	film	k1gInPc2
v	v	k7c6
očích	oko	k1gNnPc6
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
distributorů	distributor	k1gMnPc2
a	a	k8xC
přispět	přispět	k5eAaPmF
tak	tak	k6eAd1
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
snahou	snaha	k1gFnSc7
je	být	k5eAaImIp3nS
pravdivě	pravdivě	k6eAd1
zobrazovat	zobrazovat	k5eAaImF
francouzskou	francouzský	k2eAgFnSc4d1
filmovou	filmový	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
a	a	k8xC
propagovat	propagovat	k5eAaImF
evropské	evropský	k2eAgInPc4d1
filmy	film	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
světové	světový	k2eAgFnSc2d1
konkurence	konkurence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
předávání	předávání	k1gNnSc1
Césarů	César	k1gMnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
prezidentem	prezident	k1gMnSc7
byl	být	k5eAaImAgMnS
Jean	Jean	k1gMnSc1
Gabin	Gabin	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvacet	dvacet	k4xCc4
pět	pět	k4xCc4
bronzových	bronzový	k2eAgFnPc2d1
sošek	soška	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
každá	každý	k3xTgFnSc1
váží	vážit	k5eAaImIp3nS
3	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
uděleno	udělen	k2eAgNnSc1d1
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
na	na	k7c6
přelomu	přelom	k1gInSc6
února	únor	k1gInSc2
a	a	k8xC
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Trofej	trofej	k1gFnSc1
převzala	převzít	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
César	César	k1gMnSc1
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
sochařském	sochařský	k2eAgMnSc6d1
tvůrci	tvůrce	k1gMnSc6
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
byl	být	k5eAaImAgMnS
César	César	k1gMnSc1
Baldaccini	Baldaccin	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
aktérem	aktér	k1gMnSc7
ve	v	k7c6
výběru	výběr	k1gInSc6
a	a	k8xC
oceňování	oceňování	k1gNnSc6
francouzské	francouzský	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
uvedené	uvedený	k2eAgFnSc2d1
do	do	k7c2
kin	kino	k1gNnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
je	být	k5eAaImIp3nS
Filmová	filmový	k2eAgFnSc1d1
umělecká	umělecký	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
instituce	instituce	k1gFnSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
George	Georg	k1gMnSc2
Cravenna	Cravenn	k1gMnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
400	#num#	k4
členů	člen	k1gMnPc2
z	z	k7c2
řad	řada	k1gFnPc2
filmových	filmový	k2eAgMnPc2d1
profesionálů	profesionál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
César	César	k1gMnSc1
je	být	k5eAaImIp3nS
francouzskou	francouzský	k2eAgFnSc7d1
obdobou	obdoba	k1gFnSc7
americké	americký	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
Oscar	Oscara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Televizní	televizní	k2eAgInSc1d1
přenos	přenos	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
zajišťuje	zajišťovat	k5eAaImIp3nS
stanice	stanice	k1gFnSc1
Canal	Canal	k1gInSc1
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceremoniál	ceremoniál	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
proběhl	proběhnout	k5eAaPmAgMnS
v	v	k7c6
pařížské	pařížský	k2eAgFnSc6d1
Olympii	Olympia	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc4d3
počet	počet	k1gInSc4
sedmi	sedm	k4xCc2
Césarů	César	k1gMnPc2
z	z	k7c2
třinácti	třináct	k4xCc2
nominací	nominace	k1gFnPc2
získalo	získat	k5eAaPmAgNnS
komediální	komediální	k2eAgNnSc1d1
drama	drama	k1gNnSc1
Adieu	adieu	k0
les	les	k1gInSc1
cons	cons	k1gInSc4
(	(	kIx(
<g/>
Sbohem	sbohem	k0
<g/>
,	,	kIx,
blbci	blbec	k1gMnPc5
<g/>
)	)	kIx)
režiséra	režisér	k1gMnSc2
Alberta	Albert	k1gMnSc2
Dupontela	Dupontel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
sošku	soška	k1gFnSc4
také	také	k9
odnesl	odnést	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Josiane	Josianout	k5eAaPmIp3nS
Balasko	Balasko	k1gNnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Auteuil	Auteuil	k1gMnSc1
<g/>
,	,	kIx,
Catherine	Catherin	k1gInSc5
Deneuve	Deneuev	k1gFnSc2
a	a	k8xC
Karin	Karina	k1gFnPc2
Viardová	Viardový	k2eAgFnSc1d1
na	na	k7c4
Césarech	César	k1gMnPc6
200045	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
udílení	udílení	k1gNnSc2
Césarů	César	k1gMnPc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenu	cena	k1gFnSc4
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
film	film	k1gInSc4
obdržela	obdržet	k5eAaPmAgFnS
adaptace	adaptace	k1gFnSc1
Hugových	Hugových	k2eAgMnPc2d1
Bídníků	bídník	k1gMnPc2
<g/>
,	,	kIx,
režírovaná	režírovaný	k2eAgFnSc1d1
Ladji	Ladje	k1gFnSc4
Lyem	Lyem	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roman	Roman	k1gMnSc1
Polański	Polańske	k1gFnSc4
proměnil	proměnit	k5eAaPmAgMnS
ve	v	k7c6
vítězství	vítězství	k1gNnSc6
i	i	k9
pátou	pátý	k4xOgFnSc4
nominaci	nominace	k1gFnSc4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
nejlepšího	dobrý	k2eAgMnSc2d3
režiséra	režisér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
protest	protest	k1gInSc4
vůči	vůči	k7c3
jeho	jeho	k3xOp3gNnSc3
ocenění	ocenění	k1gNnSc3
opustilo	opustit	k5eAaPmAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
večera	večer	k1gInSc2
sál	sát	k5eAaImAgMnS
několik	několik	k4yIc4
hereček	herečka	k1gFnPc2
včetně	včetně	k7c2
Adè	Adè	k1gFnSc2
Haenelové	Haenelová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
obviněný	obviněný	k1gMnSc1
Polański	Polański	k1gNnSc4
ze	z	k7c2
znásilnění	znásilnění	k1gNnSc2
nezletilé	zletilý	k2eNgFnPc1d1,k2eAgFnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
,	,	kIx,
již	již	k6eAd1
před	před	k7c7
ceremoniálem	ceremoniál	k1gInSc7
avizoval	avizovat	k5eAaBmAgMnS
svou	svůj	k3xOyFgFnSc4
nepřítomnost	nepřítomnost	k1gFnSc4
během	během	k7c2
večera	večer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
kritici	kritik	k1gMnPc1
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
nemorální	morální	k2eNgNnSc4d1
aplaudovat	aplaudovat	k5eAaImF
takto	takto	k6eAd1
obviněnému	obviněný	k1gMnSc3
umělci	umělec	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Producent	producent	k1gMnSc1
Polańského	Polańský	k2eAgInSc2d1
snímku	snímek	k1gInSc2
Žaluji	žalovat	k5eAaImIp1nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
oceněného	oceněný	k2eAgInSc2d1
třemi	tři	k4xCgMnPc7
Césary	César	k1gMnPc7
<g/>
,	,	kIx,
odvolal	odvolat	k5eAaPmAgInS
celý	celý	k2eAgInSc1d1
štáb	štáb	k1gInSc1
před	před	k7c7
vlastním	vlastní	k2eAgNnSc7d1
předáváním	předávání	k1gNnSc7
sošek	soška	k1gFnPc2
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
výroku	výrok	k1gInSc3
ministra	ministr	k1gMnSc2
kultury	kultura	k1gFnSc2
Francka	Francek	k1gMnSc2
Riestera	Riester	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
cena	cena	k1gFnSc1
pro	pro	k7c4
Polańského	Polańský	k1gMnSc4
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
špatným	špatný	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
Polańského	Polańského	k2eAgFnSc2d1
kauzy	kauza	k1gFnSc2
rezignovalo	rezignovat	k5eAaBmAgNnS
již	již	k6eAd1
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
před	před	k7c7
vyhlášením	vyhlášení	k1gNnSc7
všech	všecek	k3xTgInPc2
21	#num#	k4
členů	člen	k1gInPc2
vedení	vedení	k1gNnSc2
filmové	filmový	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
cenu	cena	k1gFnSc4
udílí	udílet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgNnPc4
sta	sto	k4xCgNnPc4
francouzských	francouzský	k2eAgInPc2d1
umělců	umělec	k1gMnPc2
v	v	k7c6
otevřeném	otevřený	k2eAgInSc6d1
dopise	dopis	k1gInSc6
vyzvalo	vyzvat	k5eAaPmAgNnS
k	k	k7c3
„	„	k?
<g/>
hluboké	hluboký	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
<g/>
“	“	k?
akademie	akademie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
čítala	čítat	k5eAaImAgFnS
na	na	k7c4
4	#num#	k4
700	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
signatáře	signatář	k1gMnPc4
se	se	k3xPyFc4
zařadili	zařadit	k5eAaPmAgMnP
herec	herec	k1gMnSc1
Omar	Omar	k1gMnSc1
Sy	Sy	k1gMnSc1
i	i	k8xC
režisérka	režisérka	k1gFnSc1
Céline	Célin	k1gInSc5
Sciammaová	Sciammaový	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předávání	předávání	k1gNnSc4
cen	cena	k1gFnPc2
46	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc6
Césarů	César	k1gMnPc2
v	v	k7c6
březnu	březen	k1gInSc6
2021	#num#	k4
proběhlo	proběhnout	k5eAaPmAgNnS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
covidu-	covidu-	k?
<g/>
19	#num#	k4
jen	jen	k9
za	za	k7c2
účasti	účast	k1gFnSc2
nominovaných	nominovaný	k2eAgMnPc2d1
a	a	k8xC
vítězů	vítěz	k1gMnPc2
v	v	k7c6
pařížské	pařížský	k2eAgFnSc6d1
Olympii	Olympia	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herečka	herečka	k1gFnSc1
Corinne	Corinn	k1gInSc5
Masierová	Masierová	k1gFnSc1
předávající	předávající	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
pro	pro	k7c4
nejlepší	dobrý	k2eAgInPc4d3
kostýmy	kostým	k1gInPc4
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
pódium	pódium	k1gNnSc4
v	v	k7c6
kostýmu	kostým	k1gInSc6
osla	osel	k1gMnSc4
potřísněného	potřísněný	k2eAgMnSc2d1
krví	krvit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
převleku	převlek	k1gInSc2
i	i	k8xC
šatů	šat	k1gInPc2
se	se	k3xPyFc4
vysvlékla	vysvléknout	k5eAaPmAgFnS
a	a	k8xC
na	na	k7c6
nahém	nahý	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
odhalila	odhalit	k5eAaPmAgFnS
sdělení	sdělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrudník	hrudník	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
prohlášení	prohlášení	k1gNnSc4
„	„	k?
<g/>
no	no	k0
culture	cultur	k1gMnSc5
<g/>
,	,	kIx,
no	no	k9
future	futur	k1gMnSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
bez	bez	k7c2
kultury	kultura	k1gFnSc2
není	být	k5eNaImIp3nS
budoucnost	budoucnost	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápisem	nápis	k1gInSc7
na	na	k7c6
zádech	záda	k1gNnPc6
„	„	k?
<g/>
Vrať	vrátit	k5eAaPmRp2nS
nám	my	k3xPp1nPc3
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
Jeane	Jean	k1gMnSc5
<g/>
,	,	kIx,
<g/>
“	“	k?
apelovala	apelovat	k5eAaImAgFnS
na	na	k7c4
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
Jeana	Jean	k1gMnSc4
Castexe	Castex	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upozornila	upozornit	k5eAaPmAgFnS
tím	ten	k3xDgNnSc7
na	na	k7c4
kulturu	kultura	k1gFnSc4
strádající	strádající	k2eAgFnSc4d1
pro	pro	k7c4
koronavirová	koronavirový	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
uzavřených	uzavřený	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
a	a	k8xC
kin	kino	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostatečný	dostatečný	k2eNgInSc4d1
přístup	přístup	k1gInSc4
francouzského	francouzský	k2eAgInSc2d1
kabinetu	kabinet	k1gInSc2
k	k	k7c3
pandemii	pandemie	k1gFnSc3
odsoudila	odsoudit	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
úvodu	úvod	k1gInSc6
průvodkyně	průvodkyně	k1gFnSc2
večerem	večer	k1gInSc7
<g/>
,	,	kIx,
komička	komička	k1gFnSc1
Marina	Marina	k1gFnSc1
Foï	Foï	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
také	také	k9
překvapilo	překvapit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
ministryně	ministryně	k1gFnSc1
kultury	kultura	k1gFnSc2
Roselyne	Roselyn	k1gInSc5
Bachelotová	Bachelotový	k2eAgFnSc1d1
našla	najít	k5eAaPmAgFnS
během	během	k7c2
krize	krize	k1gFnSc2
čas	čas	k1gInSc1
k	k	k7c3
napsání	napsání	k1gNnSc3
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyhlašované	vyhlašovaný	k2eAgFnPc1d1
kategorie	kategorie	k1gFnPc1
</s>
<s>
Ceny	cena	k1gFnPc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
film	film	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
filmový	filmový	k2eAgInSc1d1
debut	debut	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
zahraniční	zahraniční	k2eAgInSc4d1
film	film	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
režisér	režisér	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
</s>
<s>
Nejslibnější	slibní	k2eAgMnSc1d3
herec	herec	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
</s>
<s>
Nejslibnější	slibní	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
původní	původní	k2eAgInSc4d1
scénář	scénář	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
adaptace	adaptace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
kamera	kamera	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
zvuk	zvuk	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
střih	střih	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3
kostýmy	kostým	k1gInPc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc4d3
dekorace	dekorace	k1gFnPc4
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
filmová	filmový	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
dokument	dokument	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
krátkometrážní	krátkometrážní	k2eAgInSc4d1
film	film	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
krátkometrážní	krátkometrážní	k2eAgInSc1d1
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
</s>
<s>
Čestný	čestný	k2eAgMnSc1d1
César	César	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
César	César	k1gMnSc1
des	des	k1gNnSc2
Césars	Césarsa	k1gFnPc2
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prix	Prix	k1gInSc1
Daniel	Daniel	k1gMnSc1
Toscan	Toscan	k1gMnSc1
du	du	k?
Plantier	Plantier	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trophée	Trophée	k6eAd1
César	César	k1gMnSc1
&	&	k?
Techniques	Techniques	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Médaille	Médaille	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prix	Prix	k1gInSc4
spécial	spéciat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
César	César	k1gMnSc1
&	&	k?
Techniques	Techniques	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prix	Prix	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
innovation	innovation	k1gInSc1
César	César	k1gMnSc1
&	&	k?
Techniques	Techniques	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
César	César	k1gMnSc1
du	du	k?
public	publicum	k1gNnPc2
(	(	kIx(
<g/>
od	od	k7c2
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
César	César	k1gMnSc1
des	des	k1gNnSc2
lycéens	lycéensa	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bývalé	bývalý	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
původní	původní	k2eAgInSc1d1
scénář	scénář	k1gInSc1
nebo	nebo	k8xC
adaptace	adaptace	k1gFnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
frankofonní	frankofonní	k2eAgInSc1d1
film	film	k1gInSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
plakát	plakát	k1gInSc4
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
film	film	k1gInSc4
z	z	k7c2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
producent	producent	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
krátkometrážní	krátkometrážní	k2eAgInSc4d1
dokument	dokument	k1gInSc4
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
krátkometrážní	krátkometrážní	k2eAgFnSc1d1
fikce	fikce	k1gFnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nominace	nominace	k1gFnSc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
podle	podle	k7c2
počtu	počet	k1gInSc2
</s>
<s>
Filmy	film	k1gInPc1
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
Césary	César	k1gMnPc7
</s>
<s>
Čestný	čestný	k2eAgMnSc1d1
César	César	k1gMnSc1
pro	pro	k7c4
Jeana	Jean	k1gMnSc2
Rocheforta	Rocheforta	k1gFnSc1
a	a	k8xC
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
Daniel	Daniel	k1gMnSc1
Prévost	Prévost	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
Pierre	Pierr	k1gInSc5
Deladonchamps	Deladonchamps	k1gInSc1
<g/>
,	,	kIx,
Nejslibnější	slibní	k2eAgMnSc1d3
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
Nejlepší	dobrý	k2eAgMnPc1d3
herec	herec	k1gMnSc1
Omar	Omar	k1gMnSc1
Sy	Sy	k1gMnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
Michael	Michaela	k1gFnPc2
Douglas	Douglas	k1gInSc4
s	s	k7c7
druhým	druhý	k4xOgInSc7
Čestným	čestný	k2eAgInSc7d1
Césarem	César	k1gMnSc7
2016	#num#	k4
<g/>
Isabelle	Isabelle	k1gFnSc1
Huppertová	Huppertová	k1gFnSc1
s	s	k7c7
druhým	druhý	k4xOgMnSc7
Césarem	César	k1gMnSc7
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
Adè	Adè	k1gNnSc2
Haenel	Haenela	k1gFnPc2
<g/>
,	,	kIx,
Nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
Režisér	režisér	k1gMnSc1
Paul	Paul	k1gMnSc1
Verhoeven	Verhoeven	k2eAgMnSc1d1
s	s	k7c7
Césarem	César	k1gMnSc7
pro	pro	k7c4
nejlepší	dobrý	k2eAgInSc4d3
film	film	k1gInSc4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Filmy	film	k1gInPc1
s	s	k7c7
nejvíce	nejvíce	k6eAd1,k6eAd3
Césary	César	k1gMnPc7
</s>
<s>
Césarů	César	k1gMnPc2
</s>
<s>
název	název	k1gInSc1
filmu	film	k1gInSc2
</s>
<s>
nominací	nominace	k1gFnSc7
</s>
<s>
rok	rok	k1gInSc4
</s>
<s>
10	#num#	k4
</s>
<s>
Cyrano	Cyrano	k1gMnSc1
z	z	k7c2
Bergeracu	Bergeracus	k1gInSc2
</s>
<s>
13	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
metro	metro	k1gNnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Prorok	prorok	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Tlukot	tlukot	k1gInSc1
mého	můj	k3xOp1gNnSc2
srdce	srdce	k1gNnSc2
se	se	k3xPyFc4
zastavil	zastavit	k5eAaPmAgInS
</s>
<s>
10	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Adieu	adieu	k0
les	les	k1gInSc1
cons	cons	k1gInSc4
</s>
<s>
13	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
Stará	starý	k2eAgFnSc1d1
známá	známý	k2eAgFnSc1d1
písnička	písnička	k1gFnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
Všechna	všechen	k3xTgNnPc1
jitra	jitro	k1gNnPc1
světa	svět	k1gInSc2
</s>
<s>
11	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
Pianista	pianista	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
Timbuktu	Timbukt	k1gInSc3
</s>
<s>
8	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
Séraphine	Séraphinout	k5eAaPmIp3nS
</s>
<s>
9	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
Prozřetelnost	prozřetelnost	k1gFnSc1
</s>
<s>
8	#num#	k4
</s>
<s>
1977	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
120	#num#	k4
BPM	BPM	kA
</s>
<s>
13	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
Thérè	Thérè	k6eAd1
</s>
<s>
10	#num#	k4
</s>
<s>
1986	#num#	k4
</s>
<s>
Umělec	umělec	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Ahoj	ahoj	k0
<g/>
,	,	kIx,
tajtrlíku	tajtrlíku	k?
<g/>
!	!	kIx.
</s>
<s>
12	#num#	k4
</s>
<s>
1984	#num#	k4
</s>
<s>
Camille	Camille	k6eAd1
Claudelová	Claudelový	k2eAgFnSc1d1
</s>
<s>
12	#num#	k4
</s>
<s>
1988	#num#	k4
</s>
<s>
Královna	královna	k1gFnSc1
Margot	Margota	k1gFnPc2
</s>
<s>
12	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
Příliš	příliš	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
zásnuby	zásnuba	k1gFnPc1
</s>
<s>
12	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
Na	na	k7c4
shledanou	shledaná	k1gFnSc4
tam	tam	k6eAd1
nahoře	nahoře	k6eAd1
</s>
<s>
12	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
Příliš	příliš	k6eAd1
krásná	krásný	k2eAgFnSc5d1
</s>
<s>
11	#num#	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
Můj	můj	k3xOp1gInSc1
růžový	růžový	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
11	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
Láska	láska	k1gFnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
Kluci	kluk	k1gMnPc1
a	a	k8xC
Guillaume	Guillaum	k1gInSc5
<g/>
,	,	kIx,
ke	k	k7c3
stolu	stol	k1gInSc3
<g/>
!	!	kIx.
</s>
<s>
10	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
Stav	stav	k1gInSc1
po	po	k7c6
46	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filmy	film	k1gInPc1
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
nominacemi	nominace	k1gFnPc7
</s>
<s>
Filmy	film	k1gInPc1
s	s	k7c7
nejvíce	nejvíce	k6eAd1,k6eAd3
Césary	César	k1gMnPc7
</s>
<s>
Nominací	nominace	k1gFnSc7
</s>
<s>
název	název	k1gInSc1
filmu	film	k1gInSc2
</s>
<s>
rok	rok	k1gInSc4
</s>
<s>
Césarů	César	k1gMnPc2
</s>
<s>
14	#num#	k4
<g/>
Milostné	milostný	k2eAgFnPc4d1
historky	historka	k1gFnPc4
<g/>
20211	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
Cyrano	Cyrano	k1gMnSc1
z	z	k7c2
Bergeracu	Bergeracus	k1gInSc2
<g/>
199010	#num#	k4
</s>
<s>
Prorok	prorok	k1gMnSc1
<g/>
20099	#num#	k4
</s>
<s>
Adieu	adieu	k0
les	les	k1gInSc4
cons	cons	k1gInSc1
<g/>
20217	#num#	k4
</s>
<s>
120	#num#	k4
BPM20186	BPM20186	k1gFnSc1
</s>
<s>
Na	na	k7c4
shledanou	shledaná	k1gFnSc4
tam	tam	k6eAd1
nahoře	nahoře	k6eAd1
<g/>
20185	#num#	k4
</s>
<s>
Amélie	Amélie	k1gFnSc1
z	z	k7c2
Montmartru	Montmartr	k1gInSc2
<g/>
20014	#num#	k4
</s>
<s>
Podzemka	podzemka	k1gFnSc1
<g/>
19853	#num#	k4
</s>
<s>
Polisse	Polisse	k6eAd1
<g/>
20122	#num#	k4
</s>
<s>
Znovu	znovu	k6eAd1
zamilovaná	zamilovaná	k1gFnSc1
<g/>
20130	#num#	k4
</s>
<s>
Léto	léto	k1gNnSc1
8520210	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
Poslední	poslední	k2eAgInSc4d1
metro	metro	k1gNnSc1
<g/>
198010	#num#	k4
</s>
<s>
Stará	starý	k2eAgFnSc1d1
známá	známý	k2eAgFnSc1d1
písnička	písnička	k1gFnSc1
<g/>
19977	#num#	k4
</s>
<s>
Ahoj	ahoj	k0
<g/>
,	,	kIx,
tajtrlíku	tajtrlíku	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
19845	#num#	k4
</s>
<s>
Camille	Camilla	k1gFnSc3
Claudelová	Claudelová	k1gFnSc1
<g/>
19885	#num#	k4
</s>
<s>
Královna	královna	k1gFnSc1
Margot	Margot	k1gInSc1
<g/>
19945	#num#	k4
</s>
<s>
Příliš	příliš	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
zásnuby	zásnuba	k1gFnPc1
<g/>
20045	#num#	k4
</s>
<s>
Nevinné	vinný	k2eNgFnPc1d1
krutosti	krutost	k1gFnPc1
<g/>
19964	#num#	k4
</s>
<s>
Bídníci	bídník	k1gMnPc1
<g/>
20204	#num#	k4
</s>
<s>
Žaluji	žalovat	k5eAaImIp1nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
20203	#num#	k4
</s>
<s>
Ministr	ministr	k1gMnSc1
<g/>
20123	#num#	k4
</s>
<s>
8	#num#	k4
žen	žena	k1gFnPc2
<g/>
20020	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
Všechna	všechen	k3xTgNnPc1
jitra	jitro	k1gNnPc1
světa	svět	k1gInSc2
<g/>
19917	#num#	k4
</s>
<s>
Příliš	příliš	k6eAd1
krásná	krásný	k2eAgFnSc1d1
<g/>
19895	#num#	k4
</s>
<s>
Můj	můj	k3xOp1gInSc1
růžový	růžový	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
20075	#num#	k4
</s>
<s>
Marguerite	Marguerit	k1gInSc5
<g/>
20164	#num#	k4
</s>
<s>
Všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
mě	já	k3xPp1nSc4
mají	mít	k5eAaImIp3nP
rádi	rád	k2eAgMnPc1d1
<g/>
,	,	kIx,
pojedou	pojet	k5eAaPmIp3nP,k5eAaImIp3nP
vlakem	vlak	k1gInSc7
<g/>
19983	#num#	k4
</s>
<s>
O	o	k7c6
bozích	bůh	k1gMnPc6
a	a	k8xC
lidech	člověk	k1gMnPc6
<g/>
20103	#num#	k4
</s>
<s>
Tenkrát	tenkrát	k6eAd1
podruhé	podruhé	k6eAd1
<g/>
20203	#num#	k4
</s>
<s>
Nelly	Nella	k1gFnPc1
a	a	k8xC
pan	pan	k1gMnSc1
Arnaud	Arnaud	k1gInSc4
<g/>
19952	#num#	k4
</s>
<s>
Elle	Elle	k6eAd1
<g/>
20172	#num#	k4
</s>
<s>
Tajemství	tajemství	k1gNnSc1
<g/>
20071	#num#	k4
</s>
<s>
À	À	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
origine	originout	k5eAaPmIp3nS
<g/>
20091	#num#	k4
</s>
<s>
Frantz	Frantz	k1gInSc1
<g/>
20171	#num#	k4
</s>
<s>
Tři	tři	k4xCgFnPc4
vzpomínky	vzpomínka	k1gFnPc4
<g/>
20161	#num#	k4
</s>
<s>
Stav	stav	k1gInSc1
po	po	k7c6
46	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Režiséři	režisér	k1gMnPc1
s	s	k7c7
více	hodně	k6eAd2
Césary	César	k1gMnPc7
</s>
<s>
Režiséři	režisér	k1gMnPc1
s	s	k7c7
dvěma	dva	k4xCgFnPc7
a	a	k8xC
více	hodně	k6eAd2
Césary	César	k1gMnPc7
</s>
<s>
Césarů	César	k1gMnPc2
</s>
<s>
režisér	režisér	k1gMnSc1
</s>
<s>
nominací	nominace	k1gFnSc7
</s>
<s>
5	#num#	k4
</s>
<s>
Roman	Roman	k1gMnSc1
Polański	Polańsk	k1gFnSc2
<g/>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Alain	Alain	k2eAgInSc1d1
Resnais	Resnais	k1gInSc1
<g/>
8	#num#	k4
</s>
<s>
Jacques	Jacques	k1gMnSc1
Audiard	Audiard	k1gMnSc1
<g/>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Bertrand	Bertrand	k1gInSc1
Tavernier	Tavernier	k1gInSc1
<g/>
7	#num#	k4
</s>
<s>
Jean-Jacques	Jean-Jacques	k1gMnSc1
Annaud	Annaud	k1gMnSc1
<g/>
4	#num#	k4
</s>
<s>
Claude	Claude	k6eAd1
Sautet	Sautet	k1gInSc1
<g/>
4	#num#	k4
</s>
<s>
Abdellatif	Abdellatif	k1gInSc1
Kechiche	Kechich	k1gFnSc2
<g/>
3	#num#	k4
</s>
<s>
Albert	Albert	k1gMnSc1
Dupontel	Dupontel	k1gMnSc1
<g/>
3	#num#	k4
</s>
<s>
Stav	stav	k1gInSc1
po	po	k7c6
46	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Herci	herec	k1gMnPc1
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
nominacemi	nominace	k1gFnPc7
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
nominací	nominace	k1gFnPc2
v	v	k7c6
kategoriiNejlepší	kategoriiNejlepší	k1gNnSc6
herec	herec	k1gMnSc1
/	/	kIx~
Nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
</s>
<s>
nominací	nominace	k1gFnSc7
</s>
<s>
herec	herec	k1gMnSc1
</s>
<s>
Césarů	César	k1gMnPc2
</s>
<s>
17	#num#	k4
</s>
<s>
Gérard	Gérard	k1gMnSc1
Depardieu	Depardieus	k1gInSc2
</s>
<s>
2	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Isabelle	Isabelle	k6eAd1
Huppertová	Huppertový	k2eAgFnSc1d1
</s>
<s>
2	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
Catherine	Catherin	k1gInSc5
Deneuve	Deneuv	k1gInSc5
</s>
<s>
2	#num#	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Auteuil	Auteuil	k1gMnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Karin	Karina	k1gFnPc2
Viardová	Viardová	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Nathalie	Nathalie	k1gFnSc1
Baye	Bay	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
Catherine	Catherin	k1gInSc5
Frotová	Frotový	k2eAgFnSc1d1
</s>
<s>
2	#num#	k4
</s>
<s>
Miou-Miou	Miou-Miou	k6eAd1
</s>
<s>
1	#num#	k4
</s>
<s>
Fabrice	fabrika	k1gFnSc3
Luchini	Luchin	k1gMnPc1
</s>
<s>
1	#num#	k4
</s>
<s>
Juliette	Juliette	k5eAaPmIp2nP
Binocheová	Binocheová	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Isabelle	Isabelle	k6eAd1
Adjaniová	Adjaniový	k2eAgFnSc1d1
</s>
<s>
51	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dominique	Dominique	k1gFnSc1
Blancová	Blancová	k1gFnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
François	François	k1gFnSc1
Cluzet	Cluzet	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
Sandrine	Sandrin	k1gInSc5
Kiberlainová	Kiberlainová	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Michel	Michel	k1gMnSc1
Serrault	Serrault	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Jean-Hugues	Jean-Hugues	k1gMnSc1
Anglade	Anglad	k1gInSc5
</s>
<s>
1	#num#	k4
</s>
<s>
Emmanuelle	Emmanuella	k1gFnSc3
Béart	Béart	k1gInSc4
</s>
<s>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
André	André	k1gMnSc1
Dussollier	Dussollier	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Marion	Marion	k1gInSc1
Cotillard	Cotillarda	k1gFnPc2
</s>
<s>
2	#num#	k4
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP
Gainsbourgová	Gainsbourgový	k2eAgFnSc1d1
</s>
<s>
1	#num#	k4
</s>
<s>
Adè	Adè	k1gFnSc1
Haenel	Haenela	k1gFnPc2
</s>
<s>
2	#num#	k4
</s>
<s>
Noémie	Noémie	k1gFnSc1
Lvovsky	lvovsky	k6eAd1
</s>
<s>
0	#num#	k4
</s>
<s>
Jean-Pierre	Jean-Pierr	k1gMnSc5
Marielle	Mariell	k1gMnSc5
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
)	)	kIx)
–	–	k?
rekordní	rekordní	k2eAgInSc4d1
počet	počet	k1gInSc4
Césarů	César	k1gMnPc2
v	v	k7c6
herecké	herecký	k2eAgFnSc6d1
kategoriiStav	kategoriiStat	k5eAaPmDgInS
po	po	k7c6
46	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hudební	hudební	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
s	s	k7c7
více	hodně	k6eAd2
nominacemi	nominace	k1gFnPc7
a	a	k8xC
Césarem	César	k1gMnSc7
</s>
<s>
Césarů	César	k1gMnPc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
</s>
<s>
nominací	nominace	k1gFnSc7
</s>
<s>
3	#num#	k4
</s>
<s>
Georges	Georges	k1gMnSc1
Delerue	Deleru	k1gInSc2
<g/>
8	#num#	k4
</s>
<s>
Alexandre	Alexandr	k1gInSc5
Desplat	Desple	k1gNnPc2
<g/>
8	#num#	k4
</s>
<s>
Bruno	Bruno	k1gMnSc1
Coulais	Coulais	k1gFnSc2
<g/>
7	#num#	k4
</s>
<s>
Michel	Michel	k1gMnSc1
Portal	Portal	k1gMnSc1
<g/>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Cosma	Cosma	k1gNnSc1
<g/>
6	#num#	k4
</s>
<s>
Tony	Tony	k1gMnSc1
Gatlif	Gatlif	k1gMnSc1
<g/>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Philippe	Philipp	k1gMnSc5
Sarde	Sard	k1gMnSc5
<g/>
11	#num#	k4
</s>
<s>
Éric	Éric	k1gFnSc1
Serra	Serr	k1gInSc2
<g/>
6	#num#	k4
</s>
<s>
Stav	stav	k1gInSc1
po	po	k7c6
46	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítězové	vítěz	k1gMnPc1
„	„	k?
<g/>
velké	velký	k2eAgFnSc2d1
pětky	pětka	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
Vítězné	vítězný	k2eAgInPc1d1
filmy	film	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
obdržely	obdržet	k5eAaPmAgInP
Césara	César	k1gMnSc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
pěti	pět	k4xCc6
nejprestižnějších	prestižní	k2eAgFnPc6d3
kategoriích	kategorie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
:	:	kIx,
Poslední	poslední	k2eAgNnSc1d1
metro	metro	k1gNnSc1
</s>
<s>
nejlepší	dobrý	k2eAgInSc1d3
film	film	k1gInSc1
<g/>
:	:	kIx,
Poslední	poslední	k2eAgNnSc1d1
metro	metro	k1gNnSc1
</s>
<s>
nejlepší	dobrý	k2eAgMnSc1d3
režisér	režisér	k1gMnSc1
<g/>
:	:	kIx,
François	François	k1gInSc1
Truffaut	Truffaut	k2eAgInSc1d1
</s>
<s>
nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
Gérard	Gérard	k1gInSc1
Depardieu	Depardieus	k1gInSc2
</s>
<s>
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
<g/>
:	:	kIx,
Catherine	Catherin	k1gInSc5
Deneuve	Deneuv	k1gInSc5
</s>
<s>
nejlepší	dobrý	k2eAgInSc4d3
scénář	scénář	k1gInSc4
<g/>
:	:	kIx,
Suzanne	Suzann	k1gMnSc5
Schiffman	Schiffman	k1gMnSc1
a	a	k8xC
François	François	k1gFnSc1
Truffaut	Truffaut	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
:	:	kIx,
Láska	láska	k1gFnSc1
</s>
<s>
nejlepší	dobrý	k2eAgInSc1d3
film	film	k1gInSc1
<g/>
:	:	kIx,
Láska	láska	k1gFnSc1
</s>
<s>
nejlepší	dobrý	k2eAgMnSc1d3
režisér	režisér	k1gMnSc1
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Haneke	Hanek	k1gFnSc2
</s>
<s>
nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
Jean-Louis	Jean-Louis	k1gFnSc1
Trintignant	Trintignanta	k1gFnPc2
</s>
<s>
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
<g/>
:	:	kIx,
Emmanuelle	Emmanuella	k1gFnSc3
Riva	Rivum	k1gNnSc2
</s>
<s>
nejlepší	dobrý	k2eAgInSc1d3
scénář	scénář	k1gInSc1
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Haneke	Hanek	k1gFnSc2
</s>
<s>
Přehled	přehled	k1gInSc1
ročníků	ročník	k1gInPc2
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
datum	datum	k1gNnSc1
</s>
<s>
místo	místo	k7c2
</s>
<s>
přenos	přenos	k1gInSc1
</s>
<s>
prezident	prezident	k1gMnSc1
</s>
<s>
moderátor	moderátor	k1gMnSc1
</s>
<s>
nejlepší	dobrý	k2eAgInSc1d3
film	film	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1976	#num#	k4
</s>
<s>
Palais	Palais	k1gInSc1
des	des	k1gNnSc2
Congrè	Congrè	k1gFnPc2
</s>
<s>
France	Franc	k1gMnSc4
2	#num#	k4
</s>
<s>
Jean	Jean	k1gMnSc1
Gabin	Gabin	k1gMnSc1
</s>
<s>
Pierre	Pierr	k1gMnSc5
Tchernia	Tchernium	k1gNnPc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
puška	puška	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1977	#num#	k4
</s>
<s>
Salle	Salle	k1gFnSc1
Pleyel	Pleyela	k1gFnPc2
</s>
<s>
Lino	Lina	k1gFnSc5
Ventura	Ventura	kA
</s>
<s>
Pan	Pan	k1gMnSc1
Klein	Klein	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1978	#num#	k4
</s>
<s>
Jeanne	Jeannout	k5eAaImIp3nS,k5eAaPmIp3nS
Moreau	Moreaa	k1gFnSc4
</s>
<s>
Prozřetelnost	prozřetelnost	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1979	#num#	k4
</s>
<s>
Charles	Charles	k1gMnSc1
Vanel	Vanel	k1gMnSc1
</s>
<s>
Peníze	peníz	k1gInPc1
těch	ten	k3xDgMnPc2
druhých	druhý	k4xOgMnPc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1980	#num#	k4
</s>
<s>
Jean	Jean	k1gMnSc1
Marais	Marais	k1gFnSc2
</s>
<s>
Tess	Tess	k6eAd1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1981	#num#	k4
</s>
<s>
Palais	Palais	k1gInSc1
des	des	k1gNnSc2
Congrè	Congrè	k1gFnPc2
</s>
<s>
Yves	Yves	k6eAd1
Montand	Montand	k1gInSc1
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
metro	metro	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1982	#num#	k4
</s>
<s>
Salle	Salle	k1gFnSc1
Pleyel	Pleyela	k1gFnPc2
</s>
<s>
Orson	Orson	k1gMnSc1
Welles	Welles	k1gMnSc1
</s>
<s>
Jacques	Jacques	k1gMnSc1
MartinPierre	MartinPierr	k1gInSc5
Tchernia	Tchernium	k1gNnPc1
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
ohneň	ohneň	k1gFnSc4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1983	#num#	k4
</s>
<s>
Le	Le	k?
Grand	grand	k1gMnSc1
Rex	Rex	k1gMnSc1
</s>
<s>
Catherine	Catherin	k1gInSc5
Deneuve	Deneuev	k1gFnPc4
</s>
<s>
Jean-Claude	Jean-Claude	k6eAd1
Brialy	Brial	k1gInPc1
</s>
<s>
Práskač	práskač	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1984	#num#	k4
</s>
<s>
Théâtre	Théâtr	k1gMnSc5
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Empire	empir	k1gInSc5
</s>
<s>
Gene	gen	k1gInSc5
Kelly	Kella	k1gMnSc2
</s>
<s>
Našim	náš	k3xOp1gMnPc3
láskám	láskat	k5eAaImIp1nS
a	a	k8xC
Tančírna	tančírna	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1985	#num#	k4
</s>
<s>
Simone	Simon	k1gMnSc5
Signoretová	Signoretový	k2eAgFnSc1d1
</s>
<s>
Prohnilí	prohnilý	k2eAgMnPc1d1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1986	#num#	k4
</s>
<s>
Palais	Palais	k1gInSc1
des	des	k1gNnSc2
Congrè	Congrè	k1gFnPc2
</s>
<s>
Madeleine	Madeleine	k1gFnSc1
RenaudJean-Louis	RenaudJean-Louis	k1gFnSc2
Barrault	Barraulta	k1gFnPc2
</s>
<s>
Michel	Michel	k1gMnSc1
Drucker	Drucker	k1gMnSc1
</s>
<s>
Tři	tři	k4xCgMnPc1
muži	muž	k1gMnPc1
a	a	k8xC
nemluvně	nemluvně	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1987	#num#	k4
</s>
<s>
Sean	Sean	k1gMnSc1
Connery	Conner	k1gMnPc7
</s>
<s>
Thérè	Thérè	k6eAd1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1988	#num#	k4
</s>
<s>
Miloš	Miloš	k1gMnSc1
Forman	Forman	k1gMnSc1
</s>
<s>
Michel	Michelit	k5eAaPmRp2nS
DruckerJane	DruckerJan	k1gMnSc5
Birkinová	Birkinový	k2eAgNnPc5d1
</s>
<s>
Na	na	k7c4
shledanou	shledaná	k1gFnSc4
<g/>
,	,	kIx,
chlapci	chlapec	k1gMnPc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1989	#num#	k4
</s>
<s>
Théâtre	Théâtr	k1gMnSc5
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Empire	empir	k1gInSc5
</s>
<s>
Peter	Peter	k1gMnSc1
Ustinov	Ustinov	k1gInSc4
</s>
<s>
Camille	Camille	k6eAd1
Claudelová	Claudelový	k2eAgFnSc1d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
</s>
<s>
Théâtre	Théâtr	k1gMnSc5
des	des	k1gNnSc6
Champs-Élysées	Champs-Élysées	k1gMnSc1
</s>
<s>
Kirk	Kirk	k1gMnSc1
Douglas	Douglas	k1gMnSc1
</s>
<s>
È	È	k5eAaPmIp3nS
Ruggiéri	Ruggiére	k1gFnSc4
</s>
<s>
Příliš	příliš	k6eAd1
krásná	krásný	k2eAgFnSc5d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
</s>
<s>
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Bohringer	Bohringer	k1gMnSc1
</s>
<s>
Cyrano	Cyrano	k1gMnSc1
z	z	k7c2
Bergeracu	Bergeracus	k1gInSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1992	#num#	k4
</s>
<s>
Palais	Palais	k1gInSc1
des	des	k1gNnSc2
Congrè	Congrè	k1gFnPc2
</s>
<s>
Michè	Michè	k6eAd1
Morgan	morgan	k1gMnSc1
</s>
<s>
Frédéric	Frédéric	k1gMnSc1
Mitterrand	Mitterranda	k1gFnPc2
</s>
<s>
Všechna	všechen	k3xTgNnPc1
jitra	jitro	k1gNnPc1
světa	svět	k1gInSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1993	#num#	k4
</s>
<s>
Théâtre	Théâtr	k1gMnSc5
des	des	k1gNnPc6
Champs-Élysées	Champs-Élyséesa	k1gFnPc2
</s>
<s>
Marcello	Marcella	k1gMnSc5
Mastroianni	Mastroianň	k1gMnSc5
</s>
<s>
Noci	noc	k1gFnPc1
šelem	šelma	k1gFnPc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
</s>
<s>
Canal	Canal	k1gInSc1
<g/>
+	+	kIx~
</s>
<s>
Gérard	Gérard	k1gMnSc1
Depardieu	Depardieus	k1gInSc2
</s>
<s>
Fabrice	fabrika	k1gFnSc3
LuchiniClémentine	LuchiniClémentin	k1gInSc5
Célarié	Célariý	k2eAgNnSc1d1
</s>
<s>
Smoking	smoking	k1gInSc1
<g/>
/	/	kIx~
<g/>
No	no	k9
Smoking	smoking	k1gInSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1995	#num#	k4
</s>
<s>
Palais	Palais	k1gInSc1
des	des	k1gNnSc2
Congrè	Congrè	k1gFnPc2
</s>
<s>
Alain	Alain	k1gMnSc1
Delon	Delon	k1gMnSc1
</s>
<s>
Divoké	divoký	k2eAgNnSc1d1
rákosí	rákosí	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1996	#num#	k4
</s>
<s>
Théâtre	Théâtr	k1gMnSc5
des	des	k1gNnPc1
Champs-Élysées	Champs-Élysées	k1gInSc1
</s>
<s>
Philippe	Philippat	k5eAaPmIp3nS
Noiret	Noiret	k1gInSc1
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
de	de	k?
Caunes	Caunes	k1gInSc1
</s>
<s>
Nenávist	nenávist	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1997	#num#	k4
</s>
<s>
Annie	Annie	k1gFnSc1
Girardotová	Girardotový	k2eAgFnSc1d1
</s>
<s>
Nevinné	vinný	k2eNgFnPc4d1
krutosti	krutost	k1gFnPc4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1998	#num#	k4
</s>
<s>
Juliette	Juliette	k5eAaPmIp2nP
Binocheová	Binocheová	k1gFnSc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
známá	známý	k2eAgFnSc1d1
písnička	písnička	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
</s>
<s>
Isabelle	Isabelle	k6eAd1
Huppertová	Huppertový	k2eAgFnSc1d1
</s>
<s>
Vysněný	vysněný	k2eAgInSc1d1
život	život	k1gInSc1
andělů	anděl	k1gMnPc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2000	#num#	k4
</s>
<s>
Alain	Alain	k1gMnSc1
Delon	Delon	k1gMnSc1
</s>
<s>
Alain	Alain	k1gMnSc1
Chabat	Chabat	k1gMnSc1
</s>
<s>
Venuše	Venuše	k1gFnSc1
<g/>
,	,	kIx,
salon	salon	k1gInSc1
krásy	krása	k1gFnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2001	#num#	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Auteuil	Auteuil	k1gMnSc1
</s>
<s>
Édouard	Édouard	k1gMnSc1
Baer	Baer	k1gMnSc1
</s>
<s>
Někdo	někdo	k3yInSc1
to	ten	k3xDgNnSc4
rád	rád	k6eAd1
jinak	jinak	k6eAd1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
</s>
<s>
Théâtre	Théâtr	k1gMnSc5
du	du	k?
Châtelet	Châtelet	k1gInSc4
</s>
<s>
Nathalie	Nathalie	k1gFnSc1
Baye	Bay	k1gFnSc2
</s>
<s>
Amélie	Amélie	k1gFnSc1
z	z	k7c2
Montmartru	Montmartr	k1gInSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2003	#num#	k4
</s>
<s>
bez	bez	k7c2
prezidenta	prezident	k1gMnSc2
</s>
<s>
Géraldine	Géraldinout	k5eAaPmIp3nS
Pailhas	Pailhas	k1gInSc1
</s>
<s>
Pianista	pianista	k1gMnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
</s>
<s>
Fanny	Fanna	k1gFnPc1
Ardant	Ardanta	k1gFnPc2
</s>
<s>
Gad	Gad	k?
Elmaleh	Elmaleh	k1gInSc1
</s>
<s>
Invaze	invaze	k1gFnSc1
barbarů	barbar	k1gMnPc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
</s>
<s>
Isabelle	Isabelle	k6eAd1
Adjaniová	Adjaniový	k2eAgFnSc1d1
</s>
<s>
Únik	únik	k1gInSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2006	#num#	k4
</s>
<s>
Carole	Carole	k1gFnSc1
Bouquet	bouquet	k1gNnSc2
</s>
<s>
Valérie	Valérie	k1gFnSc1
Lemercierová	Lemercierová	k1gFnSc1
</s>
<s>
Tlukot	tlukot	k1gInSc1
mého	můj	k3xOp1gNnSc2
srdce	srdce	k1gNnSc2
se	se	k3xPyFc4
zastavil	zastavit	k5eAaPmAgInS
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
</s>
<s>
Claude	Claude	k6eAd1
Brasseur	Brasseur	k1gMnSc1
</s>
<s>
Lady	lady	k1gFnSc1
Chatterleyová	Chatterleyová	k1gFnSc1
</s>
<s>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2008	#num#	k4
</s>
<s>
Jean	Jean	k1gMnSc1
Rochefort	Rochefort	k1gInSc4
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
de	de	k?
Caunes	Caunes	k1gInSc1
</s>
<s>
Kuskus	kuskus	k1gMnSc1
</s>
<s>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP
Gainsbourgová	Gainsbourgový	k2eAgFnSc1d1
</s>
<s>
Séraphine	Séraphinout	k5eAaPmIp3nS
</s>
<s>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Marion	Marion	k1gInSc1
Cotillard	Cotillarda	k1gFnPc2
</s>
<s>
Valérie	Valérie	k1gFnSc1
LemercierováGad	LemercierováGad	k1gInSc1
Elmaleh	Elmaleh	k1gMnSc1
</s>
<s>
Prorok	prorok	k1gMnSc1
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
</s>
<s>
Jodie	Jodie	k1gFnSc1
Fosterová	Fosterová	k1gFnSc1
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
de	de	k?
Caunes	Caunes	k1gInSc1
</s>
<s>
O	o	k7c6
bozích	bůh	k1gMnPc6
a	a	k8xC
lidech	člověk	k1gMnPc6
</s>
<s>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
</s>
<s>
Guillaume	Guillaum	k1gInSc5
Canet	Canet	k1gInSc4
</s>
<s>
Umělec	umělec	k1gMnSc1
</s>
<s>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
Jamel	Jamel	k1gInSc1
Debbouze	Debbouze	k1gFnSc2
</s>
<s>
Láska	láska	k1gFnSc1
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
François	François	k1gFnSc1
Cluzet	Cluzet	k1gFnSc2
</s>
<s>
Cécile	Cécile	k6eAd1
de	de	k?
France	Franc	k1gMnSc4
</s>
<s>
Kluci	kluk	k1gMnPc1
a	a	k8xC
Guillaume	Guillaum	k1gInSc5
<g/>
,	,	kIx,
ke	k	k7c3
stolu	stol	k1gInSc3
<g/>
!	!	kIx.
</s>
<s>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
</s>
<s>
Dany	Dana	k1gFnPc1
Boon	Boona	k1gFnPc2
</s>
<s>
Édouard	Édouard	k1gMnSc1
Baer	Baer	k1gMnSc1
</s>
<s>
Timbuktu	Timbukt	k1gInSc3
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
</s>
<s>
Claude	Claude	k6eAd1
Lelouch	Lelouch	k1gInSc1
</s>
<s>
Florence	Florenc	k1gFnPc1
Forestiová	Forestiový	k2eAgFnSc5d1
</s>
<s>
Fatima	Fatima	k1gFnSc1
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
</s>
<s>
Salle	Salle	k1gFnSc1
Pleyel	Pleyela	k1gFnPc2
</s>
<s>
bez	bez	k7c2
prezidenta	prezident	k1gMnSc2
</s>
<s>
Jérôme	Jérômat	k5eAaPmIp3nS
Commandeur	Commandeur	k1gMnSc1
</s>
<s>
Elle	Elle	k6eAd1
</s>
<s>
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
</s>
<s>
Vanessa	Vanessa	k1gFnSc1
Paradis	Paradis	k1gFnSc2
</s>
<s>
Manu	manout	k5eAaImIp1nS
Payet	Payet	k1gInSc1
</s>
<s>
120	#num#	k4
BPM	BPM	kA
</s>
<s>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
</s>
<s>
Kristin	Kristin	k2eAgMnSc1d1
Scott	Scott	k1gMnSc1
Thomas	Thomas	k1gMnSc1
</s>
<s>
Kad	Kad	k?
Merad	Merad	k1gInSc1
</s>
<s>
Střídavá	střídavý	k2eAgFnSc1d1
péče	péče	k1gFnSc1
</s>
<s>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
Sandrine	Sandrin	k1gInSc5
Kiberlainová	Kiberlainový	k2eAgNnPc4d1
</s>
<s>
Florence	Florenc	k1gFnPc1
Forestiová	Forestiový	k2eAgFnSc5d1
</s>
<s>
Bídníci	bídník	k1gMnPc1
</s>
<s>
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
</s>
<s>
Olympia	Olympia	k1gFnSc1
</s>
<s>
Roschdy	Roschdy	k6eAd1
Zem	zem	k1gFnSc4
</s>
<s>
Marina	Marina	k1gFnSc1
Foï	Foï	k1gFnSc1
</s>
<s>
Adieu	adieu	k0
les	les	k1gInSc1
cons	cons	k1gInSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
César	César	k1gMnSc1
Award	Award	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
César	César	k1gMnSc1
du	du	k?
cinéma	cinéma	k1gFnSc1
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Martin	Martina	k1gFnPc2
Balucha	Balucha	k1gFnSc1
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
‚	‚	k?
<g/>
Vrať	vrátit	k5eAaPmRp2nS
nám	my	k3xPp1nPc3
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
Jeane	Jean	k1gMnSc5
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Herečka	herečka	k1gFnSc1
Masierová	Masierová	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
cenách	cena	k1gFnPc6
César	César	k1gMnSc1
svlékla	svléknout	k5eAaPmAgFnS
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
uzavření	uzavření	k1gNnSc3
kin	kino	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
<g/>
,	,	kIx,
2021-03-19	2021-03-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HÁJKOVÁ	Hájková	k1gFnSc1
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skandál	skandál	k1gInSc1
na	na	k7c6
Césarech	César	k1gMnPc6
<g/>
:	:	kIx,
Polanski	Polansk	k1gInSc6
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
,	,	kIx,
francouzské	francouzský	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
na	na	k7c4
protest	protest	k1gInSc4
odešly	odejít	k5eAaPmAgFnP
ze	z	k7c2
sálu	sál	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TVGuru	TVGur	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-03-01	2020-03-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polanski	Polansk	k1gInSc6
vyhrál	vyhrát	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
César	César	k1gMnSc1
<g/>
,	,	kIx,
sál	sál	k1gInSc1
na	na	k7c4
protest	protest	k1gInSc4
opustilo	opustit	k5eAaPmAgNnS
několik	několik	k4yIc1
hereček	herečka	k1gFnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-29	2020-02-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
bal	bal	k1gInSc1
<g/>
,	,	kIx,
PaS	pas	k1gInSc1
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
aféře	aféra	k1gFnSc3
kolem	kolem	k7c2
novinky	novinka	k1gFnSc2
Romana	Roman	k1gMnSc4
Polanského	Polanský	k1gMnSc4
odstoupila	odstoupit	k5eAaPmAgFnS
francouzská	francouzský	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-14	2020-02-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Francouzská	francouzský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
předávání	předávání	k1gNnSc6
cen	cena	k1gFnPc2
svlékla	svléknout	k5eAaPmAgFnS
do	do	k7c2
naha	naho	k1gNnSc2
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
uzavření	uzavření	k1gNnSc3
kin	kino	k1gNnPc2
|	|	kIx~
Žena	žena	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žena	žena	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
magazín	magazín	k1gInSc1
pro	pro	k7c4
ženy	žena	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-13	2021-03-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
César	César	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
César	César	k1gMnSc1
–	–	k?
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Francie	Francie	k1gFnSc1
</s>
