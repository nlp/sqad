<s>
César	César	k1gMnSc1
je	být	k5eAaImIp3nS
francouzské	francouzský	k2eAgNnSc4d1
filmové	filmový	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
uděluje	udělovat	k5eAaImIp3nS
Filmová	filmový	k2eAgFnSc1d1
umělecká	umělecký	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
’	’	k?
<g/>
Académie	Académie	k1gFnSc2
des	des	k1gNnSc2
Arts	Artsa	k1gFnPc2
et	et	k?
Techniques	Techniques	k1gMnSc1
du	du	k?
Cinéma	Cinéma	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>