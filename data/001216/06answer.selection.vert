<s>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
(	(	kIx(	(
<g/>
původním	původní	k2eAgNnSc7d1	původní
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
Augustín	Augustína	k1gFnPc2	Augustína
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1913	[number]	k4	1913
Dúbravka	Dúbravka	k1gFnSc1	Dúbravka
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
Bratislavy	Bratislava	k1gFnSc2	Bratislava
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
