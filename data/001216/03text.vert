<s>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
(	(	kIx(	(
<g/>
původním	původní	k2eAgNnSc7d1	původní
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
Augustín	Augustína	k1gFnPc2	Augustína
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1913	[number]	k4	1913
Dúbravka	Dúbravka	k1gFnSc1	Dúbravka
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
Bratislavy	Bratislava	k1gFnSc2	Bratislava
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
Nikodéma	Nikodém	k1gMnSc2	Nikodém
Husáka	Husák	k1gMnSc2	Husák
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Fratričové	Fratrič	k1gMnPc1	Fratrič
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
křestní	křestní	k2eAgNnSc1d1	křestní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Augustín	Augustín	k1gInSc4	Augustín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
v	v	k7c6	v
kamenolomu	kamenolom	k1gInSc6	kamenolom
a	a	k8xC	a
sezonní	sezonní	k2eAgMnSc1d1	sezonní
zemědělský	zemědělský	k2eAgMnSc1d1	zemědělský
dělník	dělník	k1gMnSc1	dělník
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
těžší	těžký	k2eAgNnSc4d2	těžší
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
rolník	rolník	k1gMnSc1	rolník
<g/>
;	;	kIx,	;
krátce	krátce	k6eAd1	krátce
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
post	post	k1gInSc4	post
starosty	starosta	k1gMnSc2	starosta
Dúbravky	Dúbravka	k1gFnSc2	Dúbravka
<g/>
.	.	kIx.	.
</s>
<s>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
svoji	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
nepoznal	poznat	k5eNaPmAgMnS	poznat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
pobýval	pobývat	k5eAaImAgMnS	pobývat
Husák	Husák	k1gMnSc1	Husák
v	v	k7c6	v
bratislavských	bratislavský	k2eAgInPc6d1	bratislavský
internátech	internát	k1gInPc6	internát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
studentském	studentský	k2eAgNnSc6d1	studentské
hnutí	hnutí	k1gNnSc6	hnutí
poznal	poznat	k5eAaPmAgMnS	poznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
svoji	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
první	první	k4xOgFnSc4	první
manželku	manželka	k1gFnSc4	manželka
Magdu	Magda	k1gFnSc4	Magda
Lokvencovou	Lokvencový	k2eAgFnSc4d1	Lokvencová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
syny	syn	k1gMnPc7	syn
Vladimíra	Vladimír	k1gMnSc4	Vladimír
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jána	Ján	k1gMnSc4	Ján
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
manželství	manželství	k1gNnSc1	manželství
skončilo	skončit	k5eAaPmAgNnS	skončit
rozvodem	rozvod	k1gInSc7	rozvod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Magda	Magda	k1gFnSc1	Magda
Husáková	Husáková	k1gFnSc1	Husáková
Lokvencová	Lokvencový	k2eAgFnSc1d1	Lokvencová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
ženy	žena	k1gFnSc2	žena
se	se	k3xPyFc4	se
Husák	Husák	k1gMnSc1	Husák
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
s	s	k7c7	s
Vierou	Vierý	k2eAgFnSc7d1	Vierý
Millerovou	Millerová	k1gFnSc7	Millerová
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Čáslavskou	Čáslavská	k1gFnSc7	Čáslavská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
deset	deset	k4xCc4	deset
roků	rok	k1gInPc2	rok
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Viera	Viera	k6eAd1	Viera
Husáková	Husáková	k1gFnSc1	Husáková
zahynula	zahynout	k5eAaPmAgFnS	zahynout
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1977	[number]	k4	1977
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
vrtulníku	vrtulník	k1gInSc2	vrtulník
na	na	k7c6	na
bratislavském	bratislavský	k2eAgNnSc6d1	Bratislavské
letišti	letiště	k1gNnSc6	letiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgMnSc7d1	dnešní
Grösslingově	Grösslingově	k1gMnSc7	Grösslingově
<g/>
)	)	kIx)	)
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c7	mezi
premianty	premiant	k1gMnPc7	premiant
<g/>
.	.	kIx.	.
</s>
<s>
Maturoval	maturovat	k5eAaBmAgMnS	maturovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Komunistického	komunistický	k2eAgInSc2d1	komunistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
student	student	k1gMnSc1	student
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
promován	promovat	k5eAaBmNgInS	promovat
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
JUDr.	JUDr.	kA	JUDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
práv	práv	k2eAgMnSc1d1	práv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
právnický	právnický	k2eAgMnSc1d1	právnický
koncipient	koncipient	k1gMnSc1	koncipient
v	v	k7c6	v
advokátní	advokátní	k2eAgFnSc6d1	advokátní
kanceláři	kancelář	k1gFnSc6	kancelář
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Clementise	Clementise	k1gFnSc2	Clementise
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
uváděl	uvádět	k5eAaImAgMnS	uvádět
i	i	k9	i
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
CSc.	CSc.	kA	CSc.
(	(	kIx(	(
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc4d1	vědecký
titul	titul	k1gInSc4	titul
kandidáta	kandidát	k1gMnSc2	kandidát
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
československé	československý	k2eAgFnSc2d1	Československá
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
Husák	Husák	k1gMnSc1	Husák
odpůrcem	odpůrce	k1gMnSc7	odpůrce
Tisova	Tisovo	k1gNnSc2	Tisovo
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
místopředsedou	místopředseda	k1gMnSc7	místopředseda
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
významným	významný	k2eAgMnSc7d1	významný
členem	člen	k1gMnSc7	člen
tehdy	tehdy	k6eAd1	tehdy
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Sboru	sbor	k1gInSc2	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
zastával	zastávat	k5eAaImAgMnS	zastávat
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
post	posta	k1gFnPc2	posta
pověřence	pověřenec	k1gMnSc2	pověřenec
vnitra	vnitro	k1gNnSc2	vnitro
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Štefánikem	Štefánik	k1gMnSc7	Štefánik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
léta	léto	k1gNnSc2	léto
1944	[number]	k4	1944
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
i	i	k9	i
Husákův	Husákův	k2eAgInSc1d1	Husákův
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Slovensko	Slovensko	k1gNnSc1	Slovensko
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
národů	národ	k1gInPc2	národ
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
zdůvodňoval	zdůvodňovat	k5eAaImAgMnS	zdůvodňovat
Husák	Husák	k1gMnSc1	Husák
dopis	dopis	k1gInSc4	dopis
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
jen	jen	k9	jen
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
tlak	tlak	k1gInSc4	tlak
na	na	k7c6	na
čs	čs	kA	čs
<g/>
.	.	kIx.	.
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uznala	uznat	k5eAaPmAgFnS	uznat
slovenské	slovenský	k2eAgInPc4d1	slovenský
národní	národní	k2eAgInPc4d1	národní
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
tedy	tedy	k9	tedy
již	již	k6eAd1	již
tenkrát	tenkrát	k6eAd1	tenkrát
zastávat	zastávat	k5eAaImF	zastávat
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
svazku	svazek	k1gInSc6	svazek
značnou	značný	k2eAgFnSc4d1	značná
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
politice	politika	k1gFnSc6	politika
setrval	setrvat	k5eAaPmAgMnS	setrvat
i	i	k9	i
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
a	a	k8xC	a
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
byl	být	k5eAaImAgInS	být
opětovně	opětovně	k6eAd1	opětovně
pověřencem	pověřenec	k1gMnSc7	pověřenec
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
na	na	k7c4	na
post	post	k1gInSc4	post
pověřence	pověřenec	k1gMnSc2	pověřenec
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
poválečných	poválečný	k2eAgFnPc6d1	poválečná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
třetí	třetí	k4xOgFnSc2	třetí
pražské	pražský	k2eAgFnSc2d1	Pražská
dohody	dohoda	k1gFnSc2	dohoda
však	však	k9	však
všechna	všechen	k3xTgNnPc1	všechen
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
podléhala	podléhat	k5eAaImAgFnS	podléhat
schválení	schválení	k1gNnSc4	schválení
celostátní	celostátní	k2eAgFnSc2d1	celostátní
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Husák	Husák	k1gMnSc1	Husák
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prakticky	prakticky	k6eAd1	prakticky
slovenské	slovenský	k2eAgFnSc2d1	slovenská
autonomní	autonomní	k2eAgFnSc2d1	autonomní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
Husák	Husák	k1gMnSc1	Husák
navíc	navíc	k6eAd1	navíc
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
nátlakovými	nátlakový	k2eAgFnPc7d1	nátlaková
akcemi	akce	k1gFnPc7	akce
některé	některý	k3yIgFnSc6	některý
přední	přední	k2eAgFnSc6d1	přední
nekomunistické	komunistický	k2eNgFnSc6d1	nekomunistická
pověřence	pověřenka	k1gFnSc6	pověřenka
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
potom	potom	k6eAd1	potom
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pořádkové	pořádkový	k2eAgFnSc2d1	pořádková
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
Sboru	sbor	k1gInSc2	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
-	-	kIx~	-
SNB	SNB	kA	SNB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
StB	StB	k1gMnSc1	StB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
navíc	navíc	k6eAd1	navíc
krátce	krátce	k6eAd1	krátce
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
post	post	k1gInSc4	post
pověřence	pověřenec	k1gMnSc2	pověřenec
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
Husák	Husák	k1gMnSc1	Husák
zpočátku	zpočátku	k6eAd1	zpočátku
politicky	politicky	k6eAd1	politicky
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgInS	zůstat
předsedou	předseda	k1gMnSc7	předseda
i	i	k8xC	i
poúnorového	poúnorový	k2eAgNnSc2d1	poúnorové
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
a	a	k8xC	a
zprvu	zprvu	k6eAd1	zprvu
také	také	k9	také
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
nastal	nastat	k5eAaPmAgInS	nastat
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Clementisem	Clementis	k1gInSc7	Clementis
<g/>
,	,	kIx,	,
Lacem	Lac	k1gInSc7	Lac
Novomeským	Novomeský	k2eAgInSc7d1	Novomeský
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalšími	další	k2eAgFnPc7d1	další
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
buržoazního	buržoazní	k2eAgInSc2d1	buržoazní
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1950	[number]	k4	1950
ztratil	ztratit	k5eAaPmAgMnS	ztratit
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
Sboru	sbor	k1gInSc2	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1951	[number]	k4	1951
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
buržoazními	buržoazní	k2eAgMnPc7d1	buržoazní
nacionalisty	nacionalista	k1gMnPc7	nacionalista
<g/>
,	,	kIx,	,
souzenými	souzený	k2eAgFnPc7d1	souzená
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
,	,	kIx,	,
sabotáž	sabotáž	k1gFnSc1	sabotáž
a	a	k8xC	a
vyzvědačství	vyzvědačství	k1gNnSc1	vyzvědačství
<g/>
,	,	kIx,	,
<g/>
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
k	k	k7c3	k
doživotnímu	doživotní	k2eAgInSc3d1	doživotní
trestu	trest	k1gInSc3	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odsouzení	odsouzení	k1gNnSc6	odsouzení
čekal	čekat	k5eAaImAgInS	čekat
na	na	k7c4	na
přesun	přesun	k1gInSc4	přesun
do	do	k7c2	do
věznice	věznice	k1gFnSc2	věznice
v	v	k7c6	v
Leopoldově	Leopoldův	k2eAgInSc6d1	Leopoldův
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
věznici	věznice	k1gFnSc6	věznice
StB	StB	k1gFnSc2	StB
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Arcibiskupského	arcibiskupský	k2eAgNnSc2d1	Arcibiskupské
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Praze-Bubenči	Praze-Bubenči	k1gFnSc6	Praze-Bubenči
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
nedoznali	doznat	k5eNaPmAgMnP	doznat
žádnou	žádný	k3yNgFnSc4	žádný
vinu	vina	k1gFnSc4	vina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
mučení	mučení	k1gNnSc6	mučení
nakonec	nakonec	k6eAd1	nakonec
odvolal	odvolat	k5eAaPmAgInS	odvolat
své	svůj	k3xOyFgNnSc4	svůj
přiznání	přiznání	k1gNnSc4	přiznání
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
příprava	příprava	k1gFnSc1	příprava
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Rozsudek	rozsudek	k1gInSc1	rozsudek
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
byl	být	k5eAaImAgMnS	být
vynesen	vynést	k5eAaPmNgMnS	vynést
až	až	k6eAd1	až
po	po	k7c6	po
Stalinově	Stalinův	k2eAgFnSc6d1	Stalinova
a	a	k8xC	a
Gottwaldově	Gottwaldův	k2eAgFnSc6d1	Gottwaldova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
také	také	k9	také
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Husák	Husák	k1gMnSc1	Husák
unikl	uniknout	k5eAaPmAgMnS	uniknout
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
amnestii	amnestie	k1gFnSc6	amnestie
prezidenta	prezident	k1gMnSc2	prezident
Antonína	Antonín	k1gMnSc2	Antonín
Novotného	Novotný	k1gMnSc2	Novotný
Husák	Husák	k1gMnSc1	Husák
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
plně	plně	k6eAd1	plně
rehabilitován	rehabilitovat	k5eAaBmNgInS	rehabilitovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
Husák	Husák	k1gMnSc1	Husák
zpočátku	zpočátku	k6eAd1	zpočátku
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
význačných	význačný	k2eAgFnPc2d1	význačná
osobností	osobnost	k1gFnPc2	osobnost
reformního	reformní	k2eAgInSc2d1	reformní
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
stoupenec	stoupenec	k1gMnSc1	stoupenec
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
místopředsedou	místopředseda	k1gMnSc7	místopředseda
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
iniciátorů	iniciátor	k1gMnPc2	iniciátor
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
federativním	federativní	k2eAgNnSc6d1	federativní
uspořádání	uspořádání	k1gNnSc6	uspořádání
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moskevských	moskevský	k2eAgNnPc6d1	moskevské
jednáních	jednání	k1gNnPc6	jednání
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
československé	československý	k2eAgFnSc2d1	Československá
delegace	delegace	k1gFnSc2	delegace
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
prezidenta	prezident	k1gMnSc2	prezident
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Husák	Husák	k1gMnSc1	Husák
pragmatický	pragmatický	k2eAgInSc4d1	pragmatický
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tím	ten	k3xDgMnSc7	ten
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
posléze	posléze	k6eAd1	posléze
dal	dát	k5eAaPmAgMnS	dát
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Leonid	Leonida	k1gFnPc2	Leonida
Brežněv	Brežněv	k1gMnSc1	Brežněv
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
ostatními	ostatní	k2eAgMnPc7d1	ostatní
komunistickými	komunistický	k2eAgMnPc7d1	komunistický
politiky	politik	k1gMnPc7	politik
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
před	před	k7c7	před
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Dubčekem	Dubček	k1gInSc7	Dubček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1969	[number]	k4	1969
se	se	k3xPyFc4	se
Husák	Husák	k1gMnSc1	Husák
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
tajemníkem	tajemník	k1gMnSc7	tajemník
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
prezidenta	prezident	k1gMnSc2	prezident
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
volbách	volba	k1gFnPc6	volba
nového	nový	k2eAgMnSc2d1	nový
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
příštích	příští	k2eAgNnPc2d1	příští
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
prezidentem	prezident	k1gMnSc7	prezident
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Husákova	Husákův	k2eAgNnSc2d1	Husákovo
vedení	vedení	k1gNnSc2	vedení
došlo	dojít	k5eAaPmAgNnS	dojít
počátkem	počátkem	k7c2	počátkem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
normalizaci	normalizace	k1gFnSc4	normalizace
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
personální	personální	k2eAgFnPc1d1	personální
čistky	čistka	k1gFnPc1	čistka
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
odvětvích	odvětví	k1gNnPc6	odvětví
společenského	společenský	k2eAgInSc2d1	společenský
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
elit	elita	k1gFnPc2	elita
a	a	k8xC	a
odborníků	odborník	k1gMnPc2	odborník
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
do	do	k7c2	do
státních	státní	k2eAgFnPc2d1	státní
a	a	k8xC	a
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
funkcí	funkce	k1gFnPc2	funkce
často	často	k6eAd1	často
bezcharakterní	bezcharakterní	k2eAgMnPc1d1	bezcharakterní
kariéristé	kariérista	k1gMnPc1	kariérista
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
úpadku	úpadek	k1gInSc3	úpadek
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
kritériem	kritérion	k1gNnSc7	kritérion
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stávala	stávat	k5eAaImAgFnS	stávat
schopnost	schopnost	k1gFnSc4	schopnost
přetvářky	přetvářka	k1gFnSc2	přetvářka
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
občanské	občanský	k2eAgFnSc2d1	občanská
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
cokoliv	cokoliv	k3yInSc4	cokoliv
udělat	udělat	k5eAaPmF	udělat
pro	pro	k7c4	pro
ideály	ideál	k1gInPc4	ideál
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
československá	československý	k2eAgFnSc1d1	Československá
společnost	společnost	k1gFnSc1	společnost
hlásila	hlásit	k5eAaImAgFnS	hlásit
koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
umírněně	umírněně	k6eAd1	umírněně
protestovat	protestovat	k5eAaBmF	protestovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostávali	dostávat	k5eAaImAgMnP	dostávat
do	do	k7c2	do
izolace	izolace	k1gFnSc2	izolace
od	od	k7c2	od
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jejich	jejich	k3xOp3gNnSc4	jejich
úsilí	úsilí	k1gNnSc4	úsilí
často	často	k6eAd1	často
chápala	chápat	k5eAaImAgFnS	chápat
jen	jen	k9	jen
jako	jako	k9	jako
zbytečnou	zbytečný	k2eAgFnSc4d1	zbytečná
provokaci	provokace	k1gFnSc4	provokace
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
víceméně	víceméně	k9	víceméně
"	"	kIx"	"
<g/>
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
"	"	kIx"	"
zúčastňovala	zúčastňovat	k5eAaImAgFnS	zúčastňovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
naprosto	naprosto	k6eAd1	naprosto
nesvobodných	svobodný	k2eNgFnPc2d1	nesvobodná
a	a	k8xC	a
zmanipulovaných	zmanipulovaný	k2eAgFnPc2d1	zmanipulovaná
"	"	kIx"	"
<g/>
voleb	volba	k1gFnPc2	volba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
ochotní	ochotný	k2eAgMnPc1d1	ochotný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zachování	zachování	k1gNnSc2	zachování
svého	svůj	k3xOyFgNnSc2	svůj
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
postavení	postavení	k1gNnSc2	postavení
podepsat	podepsat	k5eAaPmF	podepsat
prakticky	prakticky	k6eAd1	prakticky
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
prohlášení	prohlášení	k1gNnSc4	prohlášení
odsuzující	odsuzující	k2eAgMnPc1d1	odsuzující
či	či	k8xC	či
podporující	podporující	k2eAgMnPc1d1	podporující
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
výhradně	výhradně	k6eAd1	výhradně
podle	podle	k7c2	podle
vůle	vůle	k1gFnSc2	vůle
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Husákova	Husákův	k2eAgFnSc1d1	Husákova
normalizace	normalizace	k1gFnSc1	normalizace
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
navenek	navenek	k6eAd1	navenek
mj.	mj.	kA	mj.
i	i	k9	i
opětným	opětný	k2eAgNnSc7d1	opětné
odstraňováním	odstraňování	k1gNnSc7	odstraňování
pomníků	pomník	k1gInPc2	pomník
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
obnovených	obnovený	k2eAgFnPc2d1	obnovená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
-	-	kIx~	-
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Husák	Husák	k1gMnSc1	Husák
byl	být	k5eAaImAgMnS	být
trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
nositelem	nositel	k1gMnSc7	nositel
titulu	titul	k1gInSc2	titul
Hrdina	Hrdina	k1gMnSc1	Hrdina
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
hvězdou	hvězda	k1gFnSc7	hvězda
Hrdiny	Hrdina	k1gMnSc2	Hrdina
ČSSR	ČSSR	kA	ČSSR
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
a	a	k8xC	a
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
a	a	k8xC	a
titulu	titul	k1gInSc2	titul
Hrdina	Hrdina	k1gMnSc1	Hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
Husákův	Husákův	k2eAgInSc1d1	Husákův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgInSc7d1	silný
kuřákem	kuřák	k1gInSc7	kuřák
(	(	kIx(	(
<g/>
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
cigaretou	cigareta	k1gFnSc7	cigareta
objevoval	objevovat	k5eAaImAgInS	objevovat
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
vězněním	věznění	k1gNnSc7	věznění
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
zdraví	zdraví	k1gNnSc4	zdraví
citelně	citelně	k6eAd1	citelně
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovka	cukrovka	k1gFnSc1	cukrovka
omezovala	omezovat	k5eAaImAgFnS	omezovat
možnost	možnost	k1gFnSc4	možnost
operovat	operovat	k5eAaImF	operovat
šedý	šedý	k2eAgInSc4d1	šedý
zákal	zákal	k1gInSc4	zákal
(	(	kIx(	(
<g/>
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
celoživotní	celoživotní	k2eAgNnSc1d1	celoživotní
těžkou	těžký	k2eAgFnSc7d1	těžká
krátkozrakostí	krátkozrakost	k1gFnSc7	krátkozrakost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
postoupil	postoupit	k5eAaPmAgMnS	postoupit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
číst	číst	k5eAaImF	číst
jen	jen	k9	jen
na	na	k7c6	na
čtecím	čtecí	k2eAgInSc6d1	čtecí
stroji	stroj	k1gInSc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
operaci	operace	k1gFnSc4	operace
(	(	kIx(	(
<g/>
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
poměrně	poměrně	k6eAd1	poměrně
těžký	těžký	k2eAgInSc1d1	těžký
zákrok	zákrok	k1gInSc1	zákrok
<g/>
)	)	kIx)	)
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc4d1	silná
brýle	brýle	k1gFnPc4	brýle
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
později	pozdě	k6eAd2	pozdě
nosil	nosit	k5eAaImAgMnS	nosit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zrak	zrak	k1gInSc1	zrak
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
přijatelných	přijatelný	k2eAgFnPc2d1	přijatelná
mezí	mez	k1gFnPc2	mez
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
funkce	funkce	k1gFnSc2	funkce
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Miloš	Miloš	k1gMnSc1	Miloš
Jakeš	Jakeš	k1gMnSc1	Jakeš
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
však	však	k9	však
vykonával	vykonávat	k5eAaImAgInS	vykonávat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadových	listopadový	k2eAgFnPc6d1	listopadová
událostech	událost	k1gFnPc6	událost
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
Husák	Husák	k1gMnSc1	Husák
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Čalfovu	Čalfův	k2eAgFnSc4d1	Čalfova
"	"	kIx"	"
<g/>
vládu	vláda	k1gFnSc4	vláda
národního	národní	k2eAgNnSc2d1	národní
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
"	"	kIx"	"
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadových	listopadový	k2eAgInPc6d1	listopadový
dnech	den	k1gInPc6	den
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
československých	československý	k2eAgMnPc2d1	československý
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
situaci	situace	k1gFnSc4	situace
řešit	řešit	k5eAaImF	řešit
násilím	násilí	k1gNnSc7	násilí
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
tak	tak	k6eAd1	tak
ke	k	k7c3	k
klidnému	klidný	k2eAgInSc3d1	klidný
průběhu	průběh	k1gInSc3	průběh
převratu	převrat	k1gInSc2	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
pak	pak	k9	pak
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
,	,	kIx,	,
polozapomenutá	polozapomenutý	k2eAgFnSc1d1	polozapomenutá
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc2	jeho
pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vlády	vláda	k1gFnSc2	vláda
Ján	Ján	k1gMnSc1	Ján
Čarnogurský	Čarnogurský	k2eAgMnSc1d1	Čarnogurský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
kritiku	kritika	k1gFnSc4	kritika
především	především	k6eAd1	především
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k9	jako
gesto	gesto	k1gNnSc1	gesto
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
Husákovým	Husákův	k2eAgInPc3d1	Husákův
nacionálním	nacionální	k2eAgInPc3d1	nacionální
postojům	postoj	k1gInPc3	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
součást	součást	k1gFnSc4	součást
eskalace	eskalace	k1gFnSc2	eskalace
problémů	problém	k1gInPc2	problém
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc7	Slovák
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Husák	Husák	k1gMnSc1	Husák
často	často	k6eAd1	často
hovořil	hovořit	k5eAaImAgMnS	hovořit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
husákovština	husákovština	k1gFnSc1	husákovština
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
narozené	narozený	k2eAgFnPc1d1	narozená
během	během	k7c2	během
začátku	začátek	k1gInSc2	začátek
Husákovy	Husákův	k2eAgFnSc2d1	Husákova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
publicistickém	publicistický	k2eAgInSc6d1	publicistický
slangu	slang	k1gInSc6	slang
označují	označovat	k5eAaImIp3nP	označovat
Husákovy	Husákův	k2eAgFnPc1d1	Husákova
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Žižkovskému	žižkovský	k2eAgMnSc3d1	žižkovský
vysílači	vysílač	k1gMnSc3	vysílač
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Husákův	Husákův	k2eAgInSc1d1	Husákův
prst	prst	k1gInSc1	prst
<g/>
.	.	kIx.	.
</s>
<s>
Husákovo	Husákův	k2eAgNnSc1d1	Husákovo
ticho	ticho	k1gNnSc1	ticho
-	-	kIx~	-
Těšnovský	Těšnovský	k2eAgInSc1d1	Těšnovský
silniční	silniční	k2eAgInSc1d1	silniční
tunel	tunel	k1gInSc1	tunel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
zahloubením	zahloubení	k1gNnSc7	zahloubení
nábřežní	nábřežní	k2eAgFnSc2d1	nábřežní
komunikace	komunikace	k1gFnSc2	komunikace
před	před	k7c7	před
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
budovou	budova	k1gFnSc7	budova
sídla	sídlo	k1gNnSc2	sídlo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
Osobnosti	osobnost	k1gFnSc6	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
255	[number]	k4	255
<g/>
.	.	kIx.	.
</s>
<s>
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
:	:	kIx,	:
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
688	[number]	k4	688
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
537	[number]	k4	537
<g/>
-	-	kIx~	-
<g/>
538	[number]	k4	538
<g/>
.	.	kIx.	.
</s>
<s>
MACHÁČEK	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Slezský	slezský	k2eAgInSc1d1	slezský
sborník	sborník	k1gInSc1	sborník
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
110	[number]	k4	110
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
270	[number]	k4	270
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
6833	[number]	k4	6833
<g/>
.	.	kIx.	.
</s>
<s>
MACHÁČEK	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Veľmi	Veľ	k1gFnPc7	Veľ
vehementne	vehementnout	k5eAaPmIp3nS	vehementnout
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
srdcom	srdcom	k1gInSc4	srdcom
<g/>
,	,	kIx,	,
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
dušou	duša	k1gFnSc7	duša
som	soma	k1gFnPc2	soma
sa	sa	k?	sa
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
vrhol	vrhol	k1gInSc1	vrhol
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
o	o	k7c4	o
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
-	-	kIx~	-
Otázky	otázka	k1gFnPc1	otázka
-	-	kIx~	-
Problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1804	[number]	k4	1804
<g/>
-	-	kIx~	-
<g/>
1132	[number]	k4	1132
<g/>
.	.	kIx.	.
</s>
<s>
MACHÁČEK	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgFnPc1d1	slovenská
dějiny	dějiny	k1gFnPc1	dějiny
teprve	teprve	k6eAd1	teprve
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mládí	mládí	k1gNnSc1	mládí
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
418	[number]	k4	418
<g/>
-	-	kIx~	-
<g/>
5129	[number]	k4	5129
<g/>
.	.	kIx.	.
</s>
<s>
ŠTEFANICA	ŠTEFANICA	kA	ŠTEFANICA
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vybrané	vybraná	k1gFnSc3	vybraná
právne	právnout	k5eAaPmIp3nS	právnout
aspekty	aspekt	k1gInPc4	aspekt
procesu	proces	k1gInSc2	proces
G.	G.	kA	G.
HUSÁK	Husák	k1gMnSc1	Husák
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
<g/>
|	|	kIx~	|
<g/>
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Spisy	spis	k1gInPc1	spis
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
MU	MU	kA	MU
č.	č.	k?	č.
449	[number]	k4	449
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
teoretická	teoretický	k2eAgFnSc1d1	teoretická
<g/>
,	,	kIx,	,
Edice	edice	k1gFnSc1	edice
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
s.	s.	k?	s.
346	[number]	k4	346
<g/>
-	-	kIx~	-
<g/>
362	[number]	k4	362
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
6381	[number]	k4	6381
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
MACHÁČEK	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
ke	k	k7c3	k
komunistickému	komunistický	k2eAgNnSc3d1	komunistické
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
112	[number]	k4	112
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
227	[number]	k4	227
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
<s>
MACHÁČEK	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
slovenského	slovenský	k2eAgInSc2d1	slovenský
buržoazního	buržoazní	k2eAgInSc2d1	buržoazní
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
<g/>
http://www.sezimovo-usti.eu/prilohapdf/2012_machacek.pdf	[url]	k1gMnSc1	http://www.sezimovo-usti.eu/prilohapdf/2012_machacek.pdf
Ján	Ján	k1gMnSc1	Ján
Štefanica	Štefanica	k1gMnSc1	Štefanica
<g/>
:	:	kIx,	:
Trestnoprávne	Trestnoprávne	k1gFnSc1	Trestnoprávne
aspekty	aspekt	k1gInPc4	aspekt
obžaloby	obžaloba	k1gFnSc2	obžaloba
a	a	k8xC	a
rozsudku	rozsudek	k1gInSc2	rozsudek
v	v	k7c6	v
prípade	prípást	k5eAaPmIp3nS	prípást
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
/	/	kIx~	/
Kinčok	Kinčok	k1gInSc1	Kinčok
<g/>
,	,	kIx,	,
Branislav	Branislav	k1gMnSc1	Branislav
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pamäti	pamäť	k1gFnSc2	pamäť
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
s.	s.	k?	s.
374	[number]	k4	374
<g/>
-	-	kIx~	-
<g/>
397	[number]	k4	397
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
978-80-89335-76-3	[number]	k4	978-80-89335-76-3
Husákovy	Husákův	k2eAgFnPc1d1	Husákova
děti	dítě	k1gFnPc1	dítě
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
na	na	k7c6	na
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
stránkách	stránka	k1gFnPc6	stránka
Hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Gustáv	Gustáva	k1gFnPc2	Gustáva
<g />
.	.	kIx.	.
</s>
<s>
Husák	Husák	k1gMnSc1	Husák
na	na	k7c6	na
webu	web	k1gInSc6	web
Totalita	totalita	k1gFnSc1	totalita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
-	-	kIx~	-
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Kubal	Kubal	k1gInSc4	Kubal
<g/>
)	)	kIx)	)
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
-	-	kIx~	-
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc4d1	historický
magazín	magazín	k1gInSc4	magazín
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Triumf	triumf	k1gInSc4	triumf
i	i	k8xC	i
pád	pád	k1gInSc4	pád
<g/>
,	,	kIx,	,
o	o	k7c6	o
Gustávu	Gustáv	k1gMnSc6	Gustáv
Husákovi	Husák	k1gMnSc6	Husák
na	na	k7c6	na
webu	web	k1gInSc6	web
Obrys-Kmen	Obrys-Kmen	k1gInSc4	Obrys-Kmen
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k6eAd1	plus
Potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
bystré	bystrý	k2eAgFnPc4d1	bystrá
hlavy	hlava	k1gFnPc4	hlava
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
málo	málo	k6eAd1	málo
známý	známý	k2eAgInSc1d1	známý
normalizační	normalizační	k2eAgInSc1d1	normalizační
projev	projev	k1gInSc1	projev
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
ve	v	k7c6	v
Škodě	škoda	k1gFnSc6	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
http://www.rozhlas.cz/zpravy/historie/_zprava/husak-si-pujcoval-na-cigarety-a-penize-nevracel-vzpomina-jeho-spolupracovnik--1159694	[url]	k4	http://www.rozhlas.cz/zpravy/historie/_zprava/husak-si-pujcoval-na-cigarety-a-penize-nevracel-vzpomina-jeho-spolupracovnik--1159694
</s>
