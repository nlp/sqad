<s>
Amputace	amputace	k1gFnSc1	amputace
je	být	k5eAaImIp3nS	být
odstranění	odstranění	k1gNnSc4	odstranění
periferní	periferní	k2eAgFnSc2d1	periferní
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
od	od	k7c2	od
celku	celek	k1gInSc2	celek
úrazem	úraz	k1gInSc7	úraz
nebo	nebo	k8xC	nebo
chirurgicky	chirurgicky	k6eAd1	chirurgicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
amputace	amputace	k1gFnSc1	amputace
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
rukou	ruka	k1gFnSc7	ruka
či	či	k8xC	či
nohou	noha	k1gFnSc7	noha
praktikována	praktikován	k2eAgFnSc1d1	praktikována
jako	jako	k8xS	jako
trest	trest	k1gInSc4	trest
u	u	k7c2	u
odsouzených	odsouzený	k2eAgMnPc2d1	odsouzený
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
a	a	k8xC	a
náboženstvích	náboženství	k1gNnPc6	náboženství
se	se	k3xPyFc4	se
drobnější	drobný	k2eAgFnSc1d2	drobnější
amputace	amputace	k1gFnSc1	amputace
či	či	k8xC	či
zmrzačení	zmrzačení	k1gNnSc1	zmrzačení
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
z	z	k7c2	z
rituálních	rituální	k2eAgInPc2d1	rituální
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Amputace	amputace	k1gFnSc1	amputace
varlat	varle	k1gNnPc2	varle
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kastrace	kastrace	k1gFnSc1	kastrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chirurgii	chirurgie	k1gFnSc6	chirurgie
se	se	k3xPyFc4	se
amputace	amputace	k1gFnSc1	amputace
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zmírnění	zmírnění	k1gNnSc2	zmírnění
či	či	k8xC	či
odstranění	odstranění	k1gNnSc2	odstranění
bolesti	bolest	k1gFnSc2	bolest
nebo	nebo	k8xC	nebo
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zamezení	zamezení	k1gNnSc2	zamezení
šíření	šíření	k1gNnSc2	šíření
infekce	infekce	k1gFnSc1	infekce
(	(	kIx(	(
<g/>
gangréna	gangréna	k1gFnSc1	gangréna
<g/>
)	)	kIx)	)
či	či	k8xC	či
metastáz	metastáza	k1gFnPc2	metastáza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgMnPc2	některý
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ještěrky	ještěrka	k1gFnSc2	ještěrka
<g/>
)	)	kIx)	)
amputované	amputovaný	k2eAgFnSc2d1	amputovaná
periferní	periferní	k2eAgFnSc2d1	periferní
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
nedorůstají	dorůstat	k5eNaImIp3nP	dorůstat
a	a	k8xC	a
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
následky	následek	k1gInPc4	následek
lze	lze	k6eAd1	lze
napravit	napravit	k5eAaPmF	napravit
jen	jen	k9	jen
transplantací	transplantace	k1gFnSc7	transplantace
nebo	nebo	k8xC	nebo
umělou	umělý	k2eAgFnSc7d1	umělá
protézou	protéza	k1gFnSc7	protéza
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
doložené	doložený	k2eAgFnPc1d1	doložená
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
chirurgické	chirurgický	k2eAgFnPc1d1	chirurgická
amputace	amputace	k1gFnPc1	amputace
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
kosterních	kosterní	k2eAgInPc2d1	kosterní
nálezů	nález	k1gInPc2	nález
datovány	datovat	k5eAaImNgFnP	datovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
zhruba	zhruba	k6eAd1	zhruba
7000	[number]	k4	7000
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
Vedrovice	Vedrovice	k1gFnSc1	Vedrovice
v	v	k7c6	v
ČR	ČR	kA	ČR
7500	[number]	k4	7500
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
6700	[number]	k4	6700
až	až	k9	až
6900	[number]	k4	6900
roků	rok	k1gInPc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
amputace	amputace	k1gFnSc2	amputace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
dojem	dojem	k1gInSc4	dojem
končetiny	končetina	k1gFnSc2	končetina
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
končetiny	končetina	k1gFnSc2	končetina
amputované	amputovaný	k2eAgFnSc2d1	amputovaná
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
fantomová	fantomový	k2eAgFnSc1d1	fantomová
končetina	končetina	k1gFnSc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Amputation	Amputation	k1gInSc1	Amputation
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
amputace	amputace	k1gFnSc2	amputace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
amputace	amputace	k1gFnSc2	amputace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
