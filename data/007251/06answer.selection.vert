<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgMnPc2	některý
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ještěrky	ještěrka	k1gFnSc2	ještěrka
<g/>
)	)	kIx)	)
amputované	amputovaný	k2eAgFnSc2d1	amputovaná
periferní	periferní	k2eAgFnSc2d1	periferní
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
nedorůstají	dorůstat	k5eNaImIp3nP	dorůstat
a	a	k8xC	a
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
následky	následek	k1gInPc4	následek
lze	lze	k6eAd1	lze
napravit	napravit	k5eAaPmF	napravit
jen	jen	k9	jen
transplantací	transplantace	k1gFnSc7	transplantace
nebo	nebo	k8xC	nebo
umělou	umělý	k2eAgFnSc7d1	umělá
protézou	protéza	k1gFnSc7	protéza
<g/>
.	.	kIx.	.
</s>
