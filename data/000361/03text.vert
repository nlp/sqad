<s>
Alan	Alan	k1gMnSc1	Alan
Mathison	Mathison	k1gMnSc1	Mathison
Turing	Turing	k1gInSc4	Turing
<g/>
,	,	kIx,	,
OBE	Ob	k1gInSc5	Ob
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1912	[number]	k4	1912
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
britský	britský	k2eAgMnSc1d1	britský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
<g/>
,	,	kIx,	,
kryptoanalytik	kryptoanalytik	k1gMnSc1	kryptoanalytik
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc4	Turing
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malého	malý	k2eAgMnSc4d1	malý
Alana	Alan	k1gMnSc4	Alan
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nevzali	vzít	k5eNaPmAgMnP	vzít
<g/>
,	,	kIx,	,
vychovávaly	vychovávat	k5eAaImAgInP	vychovávat
ho	on	k3xPp3gInSc4	on
chůvy	chůva	k1gFnPc1	chůva
a	a	k8xC	a
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
ani	ani	k8xC	ani
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dětství	dětství	k1gNnSc6	dětství
nevykazoval	vykazovat	k5eNaImAgMnS	vykazovat
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
průměrným	průměrný	k2eAgMnSc7d1	průměrný
žákem	žák	k1gMnSc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Bavily	bavit	k5eAaImAgFnP	bavit
ho	on	k3xPp3gMnSc4	on
šachy	šach	k1gInPc4	šach
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
zvlášť	zvlášť	k6eAd1	zvlášť
dobrým	dobrý	k2eAgMnSc7d1	dobrý
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
Alan	Alan	k1gMnSc1	Alan
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Sherborne	Sherborn	k1gInSc5	Sherborn
<g/>
,	,	kIx,	,
ochromila	ochromit	k5eAaPmAgFnS	ochromit
Británii	Británie	k1gFnSc4	Británie
devítidenní	devítidenní	k2eAgFnSc1d1	devítidenní
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
stávka	stávka	k1gFnSc1	stávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Alan	Alan	k1gMnSc1	Alan
vzal	vzít	k5eAaPmAgMnS	vzít
kolo	kolo	k1gNnSc4	kolo
a	a	k8xC	a
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
dojel	dojet	k5eAaPmAgInS	dojet
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Christopherem	Christopher	k1gMnSc7	Christopher
Morcomem	Morcom	k1gMnSc7	Morcom
<g/>
,	,	kIx,	,
bavili	bavit	k5eAaImAgMnP	bavit
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
o	o	k7c6	o
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
novinkách	novinka	k1gFnPc6	novinka
a	a	k8xC	a
prováděli	provádět	k5eAaImAgMnP	provádět
vlastní	vlastní	k2eAgInPc4d1	vlastní
pokusy	pokus	k1gInPc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Morcomova	Morcomův	k2eAgFnSc1d1	Morcomův
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
Alana	Alan	k1gMnSc2	Alan
těžce	těžce	k6eAd1	těžce
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
až	až	k9	až
1934	[number]	k4	1934
studoval	studovat	k5eAaImAgInS	studovat
Turing	Turing	k1gInSc1	Turing
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c6	na
King	Kinga	k1gFnPc2	Kinga
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
v	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
koleje	kolej	k1gFnSc2	kolej
(	(	kIx(	(
<g/>
fellow	fellow	k?	fellow
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
své	svůj	k3xOyFgFnSc2	svůj
disertace	disertace	k1gFnSc2	disertace
o	o	k7c6	o
centrální	centrální	k2eAgFnSc6d1	centrální
limitní	limitní	k2eAgFnSc6d1	limitní
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Turingovy	Turingův	k2eAgFnPc1d1	Turingova
největší	veliký	k2eAgFnPc1d3	veliký
vědecké	vědecký	k2eAgFnPc1d1	vědecká
zásluhy	zásluha	k1gFnPc1	zásluha
tkví	tkvět	k5eAaImIp3nP	tkvět
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
článku	článek	k1gInSc6	článek
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
Computable	Computable	k1gMnSc1	Computable
Numbers	Numbersa	k1gFnPc2	Numbersa
<g/>
,	,	kIx,	,
with	witha	k1gFnPc2	witha
an	an	k?	an
Application	Application	k1gInSc1	Application
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Entscheidungsproblem	Entscheidungsprobl	k1gInSc7	Entscheidungsprobl
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pojem	pojem	k1gInSc4	pojem
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
teoretického	teoretický	k2eAgInSc2d1	teoretický
modelu	model	k1gInSc2	model
obecného	obecný	k2eAgInSc2d1	obecný
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnSc2	zastavení
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
není	být	k5eNaImIp3nS	být
rozhodnutelný	rozhodnutelný	k2eAgInSc1d1	rozhodnutelný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Churchovy-Turingovy	Churchovy-Turingův	k2eAgFnSc2d1	Churchovy-Turingův
teze	teze	k1gFnSc2	teze
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
toto	tento	k3xDgNnSc4	tento
zjištění	zjištění	k1gNnSc4	zjištění
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c6	na
Hilbertem	Hilbert	k1gInSc7	Hilbert
formulovaný	formulovaný	k2eAgInSc1d1	formulovaný
tzv.	tzv.	kA	tzv.
Entscheidungsproblem	Entscheidungsprobl	k1gMnSc7	Entscheidungsprobl
neboli	neboli	k8xC	neboli
problém	problém	k1gInSc4	problém
rozhodnutelnosti	rozhodnutelnost	k1gFnSc2	rozhodnutelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
a	a	k8xC	a
1938	[number]	k4	1938
studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Alonza	Alonz	k1gMnSc2	Alonz
Churche	Church	k1gMnSc2	Church
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
zde	zde	k6eAd1	zde
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Turing	Turing	k1gInSc1	Turing
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c4	v
Bletchley	Bletchley	k1gInPc4	Bletchley
Parku	park	k1gInSc2	park
luštili	luštit	k5eAaImAgMnP	luštit
německé	německý	k2eAgInPc4d1	německý
tajné	tajný	k2eAgInPc4d1	tajný
kódy	kód	k1gInPc4	kód
šifrované	šifrovaný	k2eAgInPc4d1	šifrovaný
stroji	stroj	k1gInSc6	stroj
Enigma	enigma	k1gFnSc1	enigma
a	a	k8xC	a
Tunny	Tunny	k?	Tunny
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
úsilí	úsilí	k1gNnSc1	úsilí
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
a	a	k8xC	a
Angličané	Angličan	k1gMnPc1	Angličan
měli	mít	k5eAaImAgMnP	mít
po	po	k7c4	po
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
války	válka	k1gFnSc2	válka
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
"	"	kIx"	"
<g/>
tajné	tajný	k2eAgFnSc2d1	tajná
<g/>
"	"	kIx"	"
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
ovšem	ovšem	k9	ovšem
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
nemohl	moct	k5eNaImAgMnS	moct
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
porušil	porušit	k5eAaPmAgMnS	porušit
státní	státní	k2eAgNnSc4d1	státní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Turing	Turing	k1gInSc4	Turing
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
inteligentních	inteligentní	k2eAgInPc2d1	inteligentní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
myšlenky	myšlenka	k1gFnSc2	myšlenka
tzv.	tzv.	kA	tzv.
Turingova	Turingův	k2eAgInSc2d1	Turingův
testu	test	k1gInSc2	test
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
můžeme	moct	k5eAaImIp1nP	moct
stroj	stroj	k1gInSc1	stroj
považovat	považovat	k5eAaImF	považovat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
odlišit	odlišit	k5eAaPmF	odlišit
jeho	on	k3xPp3gInSc4	on
výstup	výstup	k1gInSc4	výstup
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jeho	jeho	k3xOp3gFnPc4	jeho
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
)	)	kIx)	)
od	od	k7c2	od
výstupu	výstup	k1gInSc2	výstup
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
myšlenky	myšlenka	k1gFnPc1	myšlenka
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
využity	využít	k5eAaPmNgInP	využít
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
prvních	první	k4xOgMnPc2	první
počítačů	počítač	k1gMnPc2	počítač
řízených	řízený	k2eAgMnPc2d1	řízený
programem	program	k1gInSc7	program
uloženým	uložený	k2eAgInSc7d1	uložený
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
počítače	počítač	k1gInPc1	počítač
Turing	Turing	k1gInSc1	Turing
prakticky	prakticky	k6eAd1	prakticky
využíval	využívat	k5eAaImAgInS	využívat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
teoretickém	teoretický	k2eAgNnSc6d1	teoretické
vysvětlení	vysvětlení	k1gNnSc6	vysvětlení
morfogeneze	morfogeneze	k1gFnSc2	morfogeneze
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
Turingově	Turingově	k1gFnSc4	Turingově
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
málo	málo	k1gNnSc1	málo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1952	[number]	k4	1952
se	se	k3xPyFc4	se
Turing	Turing	k1gInSc1	Turing
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
devatenáctiletým	devatenáctiletý	k2eAgMnSc7d1	devatenáctiletý
nezaměstnaným	nezaměstnaný	k1gMnSc7	nezaměstnaný
Arnoldem	Arnold	k1gMnSc7	Arnold
Murrayem	Murray	k1gMnSc7	Murray
a	a	k8xC	a
pozval	pozvat	k5eAaPmAgInS	pozvat
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
sobě	se	k3xPyFc3	se
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byl	být	k5eAaImAgMnS	být
dům	dům	k1gInSc4	dům
vykraden	vykraden	k2eAgInSc4d1	vykraden
<g/>
.	.	kIx.	.
</s>
<s>
Murray	Murra	k1gMnPc4	Murra
sdělil	sdělit	k5eAaPmAgMnS	sdělit
Turingovi	Turing	k1gMnSc3	Turing
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlodějem	zloděj	k1gMnSc7	zloděj
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jeho	jeho	k3xOp3gMnSc1	jeho
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
a	a	k8xC	a
Turing	Turing	k1gInSc1	Turing
nahlásil	nahlásit	k5eAaPmAgInS	nahlásit
vloupání	vloupání	k1gNnSc3	vloupání
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Murrayem	Murrayum	k1gNnSc7	Murrayum
měl	mít	k5eAaImAgInS	mít
sexuální	sexuální	k2eAgInSc1d1	sexuální
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
sexuálního	sexuální	k2eAgInSc2d1	sexuální
deliktu	delikt	k1gInSc2	delikt
(	(	kIx(	(
<g/>
gross	grossit	k5eAaPmRp2nS	grossit
indecency	indecenca	k1gFnSc2	indecenca
<g/>
)	)	kIx)	)
a	a	k8xC	a
čelil	čelit	k5eAaImAgMnS	čelit
soudnímu	soudní	k2eAgNnSc3d1	soudní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
odepřen	odepřen	k2eAgInSc4d1	odepřen
další	další	k2eAgInSc4d1	další
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
utajovaným	utajovaný	k2eAgFnPc3d1	utajovaná
informacím	informace	k1gFnPc3	informace
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
šifrování	šifrování	k1gNnSc4	šifrování
ve	v	k7c6	v
Vládním	vládní	k2eAgNnSc6d1	vládní
komunikačním	komunikační	k2eAgNnSc6d1	komunikační
centru	centrum	k1gNnSc6	centrum
(	(	kIx(	(
<g/>
GCHQ	GCHQ	kA	GCHQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
si	se	k3xPyFc3	se
ale	ale	k9	ale
udržel	udržet	k5eAaPmAgMnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Turing	Turing	k1gInSc1	Turing
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
volit	volit	k5eAaImF	volit
mezi	mezi	k7c7	mezi
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
dvouletým	dvouletý	k2eAgMnSc7d1	dvouletý
<g/>
)	)	kIx)	)
vězením	vězení	k1gNnSc7	vězení
a	a	k8xC	a
probací	probací	k1gFnSc1	probací
-	-	kIx~	-
podmíněným	podmíněný	k2eAgNnSc7d1	podmíněné
prominutím	prominutí	k1gNnSc7	prominutí
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c4	na
podstoupení	podstoupení	k1gNnSc4	podstoupení
hormonální	hormonální	k2eAgFnSc2d1	hormonální
"	"	kIx"	"
<g/>
léčby	léčba	k1gFnSc2	léčba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
organo-therapic	organoherapic	k1gMnSc1	organo-therapic
treatment	treatment	k1gMnSc1	treatment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
možnost	možnost	k1gFnSc4	možnost
<g/>
:	:	kIx,	:
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
dostával	dostávat	k5eAaImAgMnS	dostávat
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
libida	libido	k1gNnSc2	libido
dávky	dávka	k1gFnSc2	dávka
syntetického	syntetický	k2eAgInSc2d1	syntetický
ženského	ženský	k2eAgInSc2d1	ženský
hormonu	hormon	k1gInSc2	hormon
estrogenu	estrogen	k1gInSc2	estrogen
<g/>
.	.	kIx.	.
</s>
<s>
Estrogen	estrogen	k1gInSc1	estrogen
navíc	navíc	k6eAd1	navíc
běžně	běžně	k6eAd1	běžně
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
gynekomastii	gynekomastie	k1gFnSc4	gynekomastie
(	(	kIx(	(
<g/>
růst	růst	k1gInSc4	růst
prsů	prs	k1gInPc2	prs
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nevítaný	vítaný	k2eNgInSc1d1	nevítaný
příklad	příklad	k1gInSc1	příklad
morfogeneze	morfogeneze	k1gFnSc2	morfogeneze
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
zabýval	zabývat	k5eAaImAgInS	zabývat
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1954	[number]	k4	1954
Turing	Turing	k1gInSc1	Turing
zemřel	zemřít	k5eAaPmAgInS	zemřít
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
kyanidem	kyanid	k1gInSc7	kyanid
draselným	draselný	k2eAgInSc7d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
napuštěno	napuštěn	k2eAgNnSc4d1	napuštěno
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc2	který
trochu	trochu	k6eAd1	trochu
snědl	sníst	k5eAaPmAgMnS	sníst
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
kyanidu	kyanid	k1gInSc2	kyanid
v	v	k7c6	v
jablku	jablko	k1gNnSc6	jablko
nebyla	být	k5eNaImAgFnS	být
testována	testovat	k5eAaImNgFnS	testovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
kyanid	kyanid	k1gInSc1	kyanid
určen	určit	k5eAaPmNgInS	určit
až	až	k6eAd1	až
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
stanoviska	stanovisko	k1gNnSc2	stanovisko
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byly	být	k5eAaImAgFnP	být
odmítnuty	odmítnut	k2eAgFnPc4d1	odmítnuta
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
náhodě	náhoda	k1gFnSc6	náhoda
(	(	kIx(	(
<g/>
neopatrné	opatrný	k2eNgNnSc1d1	neopatrné
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
o	o	k7c6	o
vraždě	vražda	k1gFnSc6	vražda
(	(	kIx(	(
<g/>
politické	politický	k2eAgInPc1d1	politický
<g/>
,	,	kIx,	,
špionážní	špionážní	k2eAgInPc1d1	špionážní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
se	se	k3xPyFc4	se
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Gordon	Gordon	k1gMnSc1	Gordon
Brown	Brown	k1gMnSc1	Brown
jménem	jméno	k1gNnSc7	jméno
vlády	vláda	k1gFnSc2	vláda
omluvil	omluvit	k5eAaPmAgInS	omluvit
Alanu	Alan	k1gMnSc3	Alan
Turingovi	Turing	k1gMnSc3	Turing
za	za	k7c2	za
příkoří	příkoří	k1gNnSc2	příkoří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
pro	pro	k7c4	pro
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Omluvu	omluva	k1gFnSc4	omluva
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
list	list	k1gInSc1	list
The	The	k1gMnSc2	The
Daily	Daila	k1gMnSc2	Daila
Telegraph	Telegraph	k1gMnSc1	Telegraph
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jménem	jméno	k1gNnSc7	jméno
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
všech	všecek	k3xTgInPc2	všecek
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
díky	díky	k7c3	díky
Alanově	Alanův	k2eAgFnSc3d1	Alanova
práci	práce	k1gFnSc3	práce
žijí	žít	k5eAaImIp3nP	žít
svobodně	svobodně	k6eAd1	svobodně
<g/>
,	,	kIx,	,
říkám	říkat	k5eAaImIp1nS	říkat
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc1	ten
líto	líto	k6eAd1	líto
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
jste	být	k5eAaImIp2nP	být
si	se	k3xPyFc3	se
něco	něco	k3yInSc1	něco
lepšího	dobrý	k2eAgNnSc2d2	lepší
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
Brown	Brown	k1gMnSc1	Brown
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
přehnané	přehnaný	k2eAgNnSc1d1	přehnané
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
jeho	on	k3xPp3gNnSc2	on
mimořádného	mimořádný	k2eAgNnSc2d1	mimořádné
přispění	přispění	k1gNnSc2	přispění
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
dějiny	dějiny	k1gFnPc1	dějiny
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
velice	velice	k6eAd1	velice
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
za	za	k7c4	za
co	co	k3yRnSc4	co
mu	on	k3xPp3gMnSc3	on
musíme	muset	k5eAaImIp1nP	muset
být	být	k5eAaImF	být
vděčni	vděčen	k2eAgMnPc1d1	vděčen
<g/>
,	,	kIx,	,
staví	stavit	k5eAaBmIp3nS	stavit
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
hroznějšího	hrozný	k2eAgNnSc2d2	hroznější
světla	světlo	k1gNnSc2	světlo
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
jednalo	jednat	k5eAaImAgNnS	jednat
tak	tak	k9	tak
nelidsky	lidsky	k6eNd1	lidsky
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
udělila	udělit	k5eAaPmAgFnS	udělit
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alanu	Alan	k1gMnSc3	Alan
Turingovi	Turing	k1gMnSc3	Turing
královskou	královský	k2eAgFnSc4d1	královská
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Grayling	Grayling	k1gInSc1	Grayling
k	k	k7c3	k
milosti	milost	k1gFnSc3	milost
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
genialita	genialita	k1gFnSc1	genialita
pomohla	pomoct	k5eAaPmAgFnS	pomoct
ukončit	ukončit	k5eAaPmF	ukončit
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
zachránila	zachránit	k5eAaPmAgFnS	zachránit
tisíce	tisíc	k4xCgInPc4	tisíc
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pozdější	pozdní	k2eAgInSc1d2	pozdější
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
zastíněn	zastínit	k5eAaPmNgInS	zastínit
jeho	jeho	k3xOp3gInPc7	jeho
odsouzením	odsouzení	k1gNnSc7	odsouzení
za	za	k7c4	za
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rozsudek	rozsudek	k1gInSc4	rozsudek
bychom	by	kYmCp1nP	by
nyní	nyní	k6eAd1	nyní
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nespravedlivý	spravedlivý	k2eNgInSc4d1	nespravedlivý
a	a	k8xC	a
diskriminační	diskriminační	k2eAgInSc4d1	diskriminační
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
rozsudek	rozsudek	k1gInSc4	rozsudek
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
.	.	kIx.	.
</s>
<s>
Turing	Turing	k1gInSc1	Turing
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
být	být	k5eAaImF	být
uznáván	uznávat	k5eAaImNgInS	uznávat
za	za	k7c4	za
jeho	jeho	k3xOp3gInPc4	jeho
přínosy	přínos	k1gInPc4	přínos
ve	v	k7c6	v
válečném	válečný	k2eAgNnSc6d1	válečné
tažení	tažení	k1gNnSc6	tažení
a	a	k8xC	a
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
o	o	k7c6	o
počítačích	počítač	k1gInPc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Milost	milost	k1gFnSc1	milost
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
je	být	k5eAaImIp3nS	být
adekvátní	adekvátní	k2eAgInSc4d1	adekvátní
hold	hold	k1gInSc4	hold
tomuto	tento	k3xDgMnSc3	tento
skvělému	skvělý	k2eAgMnSc3d1	skvělý
muži	muž	k1gMnSc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Alana	Alan	k1gMnSc2	Alan
Turinga	Turing	k1gMnSc2	Turing
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
udílena	udílen	k2eAgFnSc1d1	udílena
Turingova	Turingův	k2eAgFnSc1d1	Turingova
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
informatických	informatický	k2eAgNnPc2d1	informatické
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
