<s>
Druhá	druhý	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Počáteční	počáteční	k2eAgInSc1d1
bulharský	bulharský	k2eAgInSc1d1
plán	plán	k1gInSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
-	-	kIx~
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1913	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Balkán	Balkán	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Porážka	porážka	k1gFnSc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
,	,	kIx,
bukurešťská	bukurešťský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
1913	#num#	k4
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
Černá	černat	k5eAaImIp3nS
HoraOsmanská	HoraOsmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Michail	Michait	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
Savov	Savov	k1gInSc1
Vasil	Vasil	k1gMnSc1
Kutinčev	Kutinčev	k1gMnSc1
<g/>
,	,	kIx,
Nikola	Nikola	k1gMnSc1
Ivanov	Ivanov	k1gInSc1
<g/>
,	,	kIx,
Radko	Radka	k1gFnSc5
Dimitriev	Dimitriev	k1gFnSc1
<g/>
,	,	kIx,
Stiliyan	Stiliyan	k1gMnSc1
Kovačev	Kovačev	k1gFnSc2
Stefan	Stefan	k1gMnSc1
Tošev	Tošev	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
I.	I.	kA
Radomir	Radomir	k1gMnSc1
Putnik	Putnik	k1gMnSc1
<g/>
,	,	kIx,
Petar	Petar	k1gMnSc1
Bojović	Bojović	k1gMnSc1
<g/>
,	,	kIx,
Stepa	Stepa	k1gFnSc1
Stepanović	Stepanović	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
Korunní	korunní	k2eAgMnSc1d1
Princ	princ	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Alexandru	Alexandr	k1gMnSc3
Averescu	Averesc	k1gMnSc3
Konstantin	Konstantin	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Dousmanis	Dousmanis	k1gFnSc2
Mehmed	Mehmed	k1gMnSc1
V.	V.	kA
Enver	Enver	k1gMnSc1
Paša	paša	k1gMnSc1
Ahmet	Ahmet	k1gMnSc1
Izzet	Izzet	k1gMnSc1
Paša	paša	k1gMnSc1
Nikola	Nikola	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Princ	princ	k1gMnSc1
Danilo	danit	k5eAaImAgNnS
Janko	Janko	k1gMnSc1
Vukotić	Vukotić	k1gMnPc2
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
500	#num#	k4
221	#num#	k4
<g/>
–	–	k?
<g/>
576	#num#	k4
878	#num#	k4
</s>
<s>
348	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
330	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
148	#num#	k4
000	#num#	k4
12	#num#	k4
802	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
255	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
1	#num#	k4
093	#num#	k4
802	#num#	k4
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
7	#num#	k4
583	#num#	k4
padlých	padlý	k2eAgInPc2d1
<g/>
9	#num#	k4
694	#num#	k4
pohřešovaných	pohřešovaný	k2eAgInPc2d1
<g/>
42	#num#	k4
911	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
3	#num#	k4
049	#num#	k4
zesnulých	zesnulá	k1gFnPc2
<g/>
140	#num#	k4
ks	ks	kA
dělostřelectva	dělostřelectvo	k1gNnSc2
zničeno	zničit	k5eAaPmNgNnS
nebo	nebo	k8xC
zajatoCelkem	zajatoCelko	k1gNnSc7
<g/>
:	:	kIx,
65	#num#	k4
927	#num#	k4
mrtvých	mrtvý	k1gMnPc2
nebo	nebo	k8xC
zraněných	zraněný	k1gMnPc2
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
9	#num#	k4
000	#num#	k4
padlých	padlý	k2eAgInPc2d1
<g/>
36	#num#	k4
000	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
5	#num#	k4
000	#num#	k4
zemřelých	zemřelý	k1gMnPc2
na	na	k7c4
nemoc	nemoc	k1gFnSc4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
5	#num#	k4
851	#num#	k4
zabito	zabít	k5eAaPmNgNnS
v	v	k7c6
boji	boj	k1gInSc6
<g/>
23	#num#	k4
847	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
v	v	k7c6
boji	boj	k1gInSc6
<g/>
188	#num#	k4
<g/>
pohřešovaných	pohřešovaná	k1gFnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
Černá	Černá	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Hora	hora	k1gFnSc1
<g/>
:	:	kIx,
<g/>
240	#num#	k4
padlých	padlý	k1gMnPc2
<g/>
961	#num#	k4
zraněných	zraněný	k1gMnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Rumunsko	Rumunsko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
malé	malý	k2eAgFnSc2d1
bojové	bojový	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
<g/>
6	#num#	k4
000	#num#	k4
zemřelých	zemřelý	k1gMnPc2
na	na	k7c4
nemoc	nemoc	k1gFnSc4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
:	:	kIx,
<g/>
malé	malý	k2eAgFnSc2d1
bojové	bojový	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
<g/>
4	#num#	k4
000	#num#	k4
zemřelých	zemřelý	k1gMnPc2
na	na	k7c4
nemoc	nemoc	k1gFnSc4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
<g/>
~	~	kIx~
<g/>
76	#num#	k4
000	#num#	k4
ztrát	ztráta	k1gFnPc2
v	v	k7c6
boji	boj	k1gInSc6
<g/>
~	~	kIx~
<g/>
91	#num#	k4
000	#num#	k4
celkových	celkový	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
vypukla	vypuknout	k5eAaPmAgFnS
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válce	válka	k1gFnSc6
bojovalo	bojovat	k5eAaImAgNnS
Bulharsko	Bulharsko	k1gNnSc1
proti	proti	k7c3
Osmanské	osmanský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
a	a	k8xC
svým	svůj	k3xOyFgMnPc3
bývalým	bývalý	k2eAgMnPc3d1
spojencům	spojenec	k1gMnPc3
z	z	k7c2
Balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
:	:	kIx,
Srbsku	Srbsko	k1gNnSc6
<g/>
,	,	kIx,
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
Černé	Černá	k1gFnSc6
Hoře	hora	k1gFnSc6
a	a	k8xC
Rumunsku	Rumunsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
uspořádat	uspořádat	k5eAaPmF
územní	územní	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
zemí	zem	k1gFnPc2
Balkánskeho	Balkánský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
války	válka	k1gFnSc2
</s>
<s>
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1913	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1913	#num#	k4
podle	podle	k7c2
starého	starý	k2eAgInSc2d1
juliánského	juliánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
<g/>
)	)	kIx)
po	po	k7c6
podpisu	podpis	k1gInSc6
Londýnské	londýnský	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
konečným	konečný	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
první	první	k4xOgFnSc2
balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
otázka	otázka	k1gFnSc1
rozdělení	rozdělení	k1gNnSc2
území	území	k1gNnSc2
mezi	mezi	k7c7
spojenci	spojenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
dny	den	k1gInPc4
po	po	k7c6
podpisu	podpis	k1gInSc6
smlouvy	smlouva	k1gFnSc2
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
Řecko	Řecko	k1gNnSc4
(	(	kIx(
<g/>
později	pozdě	k6eAd2
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
přidala	přidat	k5eAaPmAgFnS
i	i	k9
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
)	)	kIx)
podepsaly	podepsat	k5eAaPmAgFnP
tajnou	tajný	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
namířenou	namířený	k2eAgFnSc4d1
proti	proti	k7c3
Bulharsku	Bulharsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
zeměmi	zem	k1gFnPc7
balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
postupně	postupně	k6eAd1
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS
novou	nový	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
tzv.	tzv.	kA
"	"	kIx"
<g/>
Velké	velký	k2eAgFnPc1d1
Síly	síla	k1gFnPc1
<g/>
"	"	kIx"
zastoupeny	zastoupit	k5eAaPmNgFnP
Rakousko-Uherskem	Rakousko-Uhersko	k1gNnSc7
a	a	k8xC
Německem	Německo	k1gNnSc7
Bulharsko	Bulharsko	k1gNnSc1
povzbuzovaly	povzbuzovat	k5eAaImAgInP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
iniciátorem	iniciátor	k1gInSc7
vzniku	vznik	k1gInSc2
vojenského	vojenský	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
,	,	kIx,
zničilo	zničit	k5eAaPmAgNnS
Balkánský	balkánský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
a	a	k8xC
oslabilo	oslabit	k5eAaPmAgNnS
vliv	vliv	k1gInSc4
Ruska	Rusko	k1gNnSc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
však	však	k9
upozorňovalo	upozorňovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
první	první	k4xOgFnSc6
začne	začít	k5eAaPmIp3nS
válku	válek	k1gInSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
bude	být	k5eAaImBp3nS
nést	nést	k5eAaImF
i	i	k9
následky	následek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Jestliže	jestliže	k8xS
Bulharsko	Bulharsko	k1gNnSc1
první	první	k4xOgFnSc4
balkánskou	balkánský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
vyhrálo	vyhrát	k5eAaPmAgNnS
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
se	se	k3xPyFc4
dožadovalo	dožadovat	k5eAaImAgNnS
přehodnocení	přehodnocení	k1gNnSc1
závěrů	závěr	k1gInPc2
bulharsko-srbské	bulharsko-srbský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
podepsané	podepsaný	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
a	a	k8xC
začalo	začít	k5eAaPmAgNnS
okupovat	okupovat	k5eAaBmF
sporné	sporný	k2eAgNnSc1d1
a	a	k8xC
nesporné	sporný	k2eNgNnSc1d1
území	území	k1gNnSc1
Makedonie	Makedonie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
žila	žít	k5eAaImAgFnS
bulharská	bulharský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakékoliv	jakýkoliv	k3yIgInPc4
požadavky	požadavek	k1gInPc4
bývalých	bývalý	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
Bulharska	Bulharsko	k1gNnSc2
byly	být	k5eAaImAgFnP
však	však	k9
nesplnitelné	splnitelný	k2eNgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
právě	právě	k9
Bulharsko	Bulharsko	k1gNnSc1
se	se	k3xPyFc4
do	do	k7c2
velké	velký	k2eAgFnSc2d1
míry	míra	k1gFnSc2
zasloužilo	zasloužit	k5eAaPmAgNnS
o	o	k7c4
své	svůj	k3xOyFgNnSc4
vítězství	vítězství	k1gNnSc4
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc1d1
balkánské	balkánský	k2eAgFnPc1d1
země	zem	k1gFnPc1
tedy	tedy	k9
neměly	mít	k5eNaImAgFnP
mít	mít	k5eAaImF
žádný	žádný	k3yNgInSc4
nárok	nárok	k1gInSc4
na	na	k7c4
zisk	zisk	k1gInSc4
území	území	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
Srbští	srbský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
během	během	k7c2
války	válka	k1gFnSc2
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
uvádí	uvádět	k5eAaImIp3nS
data	datum	k1gNnPc4
v	v	k7c6
juliánském	juliánský	k2eAgInSc6d1
kalendáři	kalendář	k1gInSc6
(	(	kIx(
<g/>
tedy	tedy	k9
o	o	k7c4
13	#num#	k4
dní	den	k1gInPc2
nazpět	nazpět	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bulharský	bulharský	k2eAgMnSc1d1
car	car	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
napadl	napadnout	k5eAaPmAgInS
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
přinutit	přinutit	k5eAaPmF
bývalé	bývalý	k2eAgMnPc4d1
spojence	spojenec	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mu	on	k3xPp3gMnSc3
vrátili	vrátit	k5eAaPmAgMnP
území	území	k1gNnSc4
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
vyhlásil	vyhlásit	k5eAaPmAgMnS
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1913	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc1
rozkaz	rozkaz	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
s	s	k7c7
odůvodněním	odůvodnění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
země	země	k1gFnSc1
může	moct	k5eAaImIp3nS
ocitnout	ocitnout	k5eAaPmF
v	v	k7c6
politické	politický	k2eAgFnSc6d1
izolaci	izolace	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
Řecko	Řecko	k1gNnSc4
začaly	začít	k5eAaPmAgFnP
postupnou	postupný	k2eAgFnSc4d1
ofenzívu	ofenzíva	k1gFnSc4
proti	proti	k7c3
Bulharsku	Bulharsko	k1gNnSc3
na	na	k7c6
celé	celý	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
čtvrtá	čtvrtý	k4xOgFnSc1
a	a	k8xC
pátá	pátý	k4xOgFnSc1
bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Michaila	Michail	k1gMnSc2
Savov	Savov	k1gInSc4
srbská	srbský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
porazila	porazit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
chystala	chystat	k5eAaImAgFnS
na	na	k7c4
útok	útok	k1gInSc4
proti	proti	k7c3
Řecku	Řecko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
války	válka	k1gFnSc2
přidala	přidat	k5eAaPmAgFnS
i	i	k9
první	první	k4xOgFnSc1
a	a	k8xC
třetí	třetí	k4xOgFnSc1
bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
začala	začít	k5eAaPmAgFnS
operovat	operovat	k5eAaImF
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pomáhala	pomáhat	k5eAaImAgFnS
svým	svůj	k3xOyFgMnPc3
kolegům	kolega	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompletní	kompletní	k2eAgFnSc1d1
bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
zaútočit	zaútočit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecká	řecký	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
se	se	k3xPyFc4
ocitla	ocitnout	k5eAaPmAgFnS
na	na	k7c6
pokraji	pokraj	k1gInSc6
krachu	krach	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
zavolala	zavolat	k5eAaPmAgFnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
srbskou	srbský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
také	také	k6eAd1
nemohla	moct	k5eNaImAgFnS
nic	nic	k3yNnSc4
udělat	udělat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgMnSc1d1
řecký	řecký	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
byl	být	k5eAaImAgMnS
proto	proto	k8xC
nucen	nucen	k2eAgMnSc1d1
prosit	prosit	k5eAaImF
o	o	k7c4
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
války	válka	k1gFnSc2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
okupací	okupace	k1gFnPc2
bulharských	bulharský	k2eAgNnPc2d1
území	území	k1gNnPc2
i	i	k8xC
měst	město	k1gNnPc2
Balčik	Balčika	k1gFnPc2
a	a	k8xC
Tutrakan	Tutrakana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
jezdecké	jezdecký	k2eAgFnPc1d1
divize	divize	k1gFnPc1
přešly	přejít	k5eAaPmAgFnP
Dunaj	Dunaj	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
spojily	spojit	k5eAaPmAgFnP
se	s	k7c7
srbskou	srbský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
napadat	napadat	k5eAaBmF,k5eAaPmF,k5eAaImF
Sofii	Sofia	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
okupaci	okupace	k1gFnSc3
své	svůj	k3xOyFgFnPc4
někdejší	někdejší	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
a	a	k8xC
pevnosti	pevnost	k1gFnPc4
Odrin	Odrina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
Bulharska	Bulharsko	k1gNnSc2
byla	být	k5eAaImAgFnS
vážná	vážný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
vláda	vláda	k1gFnSc1
Stojana	Stojana	k1gFnSc1
Daneva	Daneva	k1gFnSc1
podala	podat	k5eAaPmAgFnS
demisi	demise	k1gFnSc4
a	a	k8xC
na	na	k7c6
postu	post	k1gInSc6
premiéra	premiér	k1gMnSc2
ho	on	k3xPp3gMnSc4
vystřídal	vystřídat	k5eAaPmAgInS
Vasil	Vasil	k1gInSc1
Radoslavov	Radoslavovo	k1gNnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
vláda	vláda	k1gFnSc1
v	v	k7c6
zemi	zem	k1gFnSc6
skončila	skončit	k5eAaPmAgFnS
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
nucena	nutit	k5eAaImNgFnS
kapitulovat	kapitulovat	k5eAaBmF
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
nebyla	být	k5eNaImAgFnS
poražena	porazit	k5eAaPmNgFnS
a	a	k8xC
Vasil	Vasil	k1gInSc1
Radoslavov	Radoslavov	k1gInSc1
byl	být	k5eAaImAgInS
nucen	nucen	k2eAgMnSc1d1
žádat	žádat	k5eAaImF
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Výsledek	výsledek	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
Výsledkem	výsledek	k1gInSc7
Druhé	druhý	k4xOgFnSc2
balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
bukurešťská	bukurešťský	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
podepsaná	podepsaný	k2eAgFnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1913	#num#	k4
(	(	kIx(
<g/>
Bulharsko	Bulharsko	k1gNnSc1
ji	on	k3xPp3gFnSc4
podepsalo	podepsat	k5eAaPmAgNnS
již	již	k6eAd1
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
garantovala	garantovat	k5eAaBmAgFnS
ztrátu	ztráta	k1gFnSc4
všech	všecek	k3xTgNnPc2
území	území	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
Bulharsko	Bulharsko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
v	v	k7c6
první	první	k4xOgFnSc6
balkánské	balkánský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Řecko	Řecko	k1gNnSc1
si	se	k3xPyFc3
mezi	mezi	k7c7
sebou	se	k3xPyFc7
rozdělili	rozdělit	k5eAaPmAgMnP
část	část	k1gFnSc4
území	území	k1gNnSc2
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharsku	Bulharsko	k1gNnSc6
zůstala	zůstat	k5eAaPmAgFnS
pouze	pouze	k6eAd1
Pirinská	Pirinský	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
a	a	k8xC
část	část	k1gFnSc1
Thrákie	Thrákie	k1gFnSc2
s	s	k7c7
přístupem	přístup	k1gInSc7
k	k	k7c3
Egejskému	egejský	k2eAgNnSc3d1
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumunsko	Rumunsko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
oblast	oblast	k1gFnSc4
jižní	jižní	k2eAgFnSc2d1
Dobrudže	Dobrudža	k1gFnSc2
a	a	k8xC
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
výsledku	výsledek	k1gInSc2
války	válka	k1gFnSc2
vrátila	vrátit	k5eAaPmAgFnS
Bulharsku	Bulharsko	k1gNnSc3
část	část	k1gFnSc1
západní	západní	k2eAgFnSc1d1
Thrákie	Thrákie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Cařihradská	cařihradský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1913	#num#	k4
muselo	muset	k5eAaImAgNnS
Bulharsko	Bulharsko	k1gNnSc1
podepsat	podepsat	k5eAaPmF
i	i	k9
cařihradskou	cařihradský	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
urovnaly	urovnat	k5eAaPmAgInP
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
Bulharskem	Bulharsko	k1gNnSc7
a	a	k8xC
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
po	po	k7c6
její	její	k3xOp3gFnSc6
okupaci	okupace	k1gFnSc6
Východní	východní	k2eAgFnSc1d1
Thrákie	Thrákie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hranice	hranice	k1gFnSc1
na	na	k7c6
Balkánském	balkánský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
po	po	k7c6
druhé	druhý	k4xOgFnSc6
balkánské	balkánský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
</s>
<s>
Mapa	mapa	k1gFnSc1
Srbska	Srbsko	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
Bulharska	Bulharsko	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Hall	Halla	k1gFnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
117	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Edward	Edward	k1gMnSc1
J.	J.	kA
Erickson	Erickson	k1gMnSc1
<g/>
,	,	kIx,
Defeat	Defeat	k2eAgMnSc1d1
in	in	k?
Detail	detail	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Ottoman	Ottoman	k1gMnSc1
Army	Arma	k1gFnSc2
in	in	k?
the	the	k?
Balkans	Balkansa	k1gFnPc2
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
,	,	kIx,
Westport	Westport	k1gInSc1
<g/>
,	,	kIx,
Praeger	Praeger	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
323	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.bulgarianartillery.it/Bulgarian%20Artillery%201/T_OOB/Troops%20losses_1912-13.htm1	http://www.bulgarianartillery.it/Bulgarian%20Artillery%201/T_OOB/Troops%20losses_1912-13.htm1	k4
2	#num#	k4
Hall	Halla	k1gFnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
135	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Calculation	Calculation	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hellenic	Hellenice	k1gFnPc2
Army	Arma	k1gFnSc2
General	General	k1gMnSc1
Staff	Staff	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Greek	Greek	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
..	..	k?
<g/>
↑	↑	k?
Hall	Hall	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
118	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hall	Hall	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
119	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
První	první	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Druhá	druhý	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
