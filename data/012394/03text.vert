<p>
<s>
Sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
(	(	kIx(	(
<g/>
Tyto	tento	k3xDgInPc1	tento
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
sovy	sova	k1gFnSc2	sova
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sovovitých	sovovití	k1gMnPc2	sovovití
(	(	kIx(	(
<g/>
Tytonidae	Tytonidae	k1gNnSc1	Tytonidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nápadný	nápadný	k2eAgInSc4d1	nápadný
hlavně	hlavně	k9	hlavně
výrazným	výrazný	k2eAgInSc7d1	výrazný
srdcovitým	srdcovitý	k2eAgInSc7d1	srdcovitý
závojem	závoj	k1gInSc7	závoj
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
druh	druh	k1gInSc4	druh
sovy	sova	k1gFnSc2	sova
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
řadu	řada	k1gFnSc4	řada
biotopů	biotop	k1gInPc2	biotop
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
alba	album	k1gNnSc2	album
Scopoli	Scopole	k1gFnSc4	Scopole
<g/>
,	,	kIx,	,
1769	[number]	k4	1769
</s>
</p>
<p>
<s>
Lechusa	Lechus	k1gMnSc4	Lechus
stirtoni	stirton	k1gMnPc1	stirton
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
sova	sova	k1gFnSc1	sova
s	s	k7c7	s
dlouhýma	dlouhý	k2eAgFnPc7d1	dlouhá
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
g.	g.	k?	g.
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
šedá	šedý	k2eAgFnSc1d1	šedá
a	a	k8xC	a
okrová	okrový	k2eAgFnSc1d1	okrová
<g/>
,	,	kIx,	,
zespodu	zespodu	k6eAd1	zespodu
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
poddruhu	poddruh	k1gInSc6	poddruh
buď	buď	k8xC	buď
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgNnPc1d1	bílé
nebo	nebo	k8xC	nebo
žlutooranžová	žlutooranžový	k2eAgNnPc1d1	žlutooranžové
(	(	kIx(	(
<g/>
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
poddruh	poddruh	k1gInSc4	poddruh
T.	T.	kA	T.
a.	a.	k?	a.
guttata	guttata	k1gFnSc1	guttata
má	mít	k5eAaImIp3nS	mít
spodinu	spodina	k1gFnSc4	spodina
žlutooranžovou	žlutooranžový	k2eAgFnSc4d1	žlutooranžová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
obličejový	obličejový	k2eAgInSc1d1	obličejový
závoj	závoj	k1gInSc1	závoj
je	být	k5eAaImIp3nS	být
světlý	světlý	k2eAgInSc1d1	světlý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
typicky	typicky	k6eAd1	typicky
srdcovitý	srdcovitý	k2eAgInSc1d1	srdcovitý
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
bývají	bývat	k5eAaImIp3nP	bývat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
světlejší	světlý	k2eAgNnSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Létá	létat	k5eAaImIp3nS	létat
houpavě	houpavě	k6eAd1	houpavě
<g/>
,	,	kIx,	,
elegantně	elegantně	k6eAd1	elegantně
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
neslyšně	slyšně	k6eNd1	slyšně
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
volně	volně	k6eAd1	volně
svěšenýma	svěšený	k2eAgFnPc7d1	svěšená
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlasový	hlasový	k2eAgInSc4d1	hlasový
projev	projev	k1gInSc4	projev
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
drsným	drsný	k2eAgMnPc3d1	drsný
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
popsatelným	popsatelný	k2eAgInSc7d1	popsatelný
křikem	křik	k1gInSc7	křik
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
samci	samec	k1gMnPc1	samec
mohou	moct	k5eAaImIp3nP	moct
opakovaně	opakovaně	k6eAd1	opakovaně
vydávat	vydávat	k5eAaImF	vydávat
také	také	k9	také
táhlý	táhlý	k2eAgInSc4d1	táhlý
<g/>
,	,	kIx,	,
vrnivý	vrnivý	k2eAgInSc4d1	vrnivý
zvuk	zvuk	k1gInSc4	zvuk
připomínající	připomínající	k2eAgNnSc1d1	připomínající
volání	volání	k1gNnSc1	volání
lelka	lelek	k1gMnSc2	lelek
lesního	lesní	k2eAgMnSc2d1	lesní
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
se	se	k3xPyFc4	se
ozývají	ozývat	k5eAaImIp3nP	ozývat
hlasitým	hlasitý	k2eAgNnPc3d1	hlasité
táhlým	táhlý	k2eAgNnPc3d1	táhlé
syčením	syčení	k1gNnPc3	syčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
početnost	početnost	k1gFnSc1	početnost
==	==	k?	==
</s>
</p>
<p>
<s>
Sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
má	mít	k5eAaImIp3nS	mít
kosmopolitní	kosmopolitní	k2eAgInSc4d1	kosmopolitní
typ	typ	k1gInSc4	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
chybí	chybit	k5eAaPmIp3nS	chybit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
a	a	k8xC	a
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
deštných	deštný	k2eAgInPc6d1	deštný
pralesích	prales	k1gInPc6	prales
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
populace	populace	k1gFnSc2	populace
obývá	obývat	k5eAaImIp3nS	obývat
teplejší	teplý	k2eAgInSc1d2	teplejší
oblasti	oblast	k1gFnSc3	oblast
<g/>
,	,	kIx,	,
k	k	k7c3	k
severu	sever	k1gInSc3	sever
nejvýše	vysoce	k6eAd3	vysoce
proniká	pronikat	k5eAaImIp3nS	pronikat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
k	k	k7c3	k
60	[number]	k4	60
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stálý	stálý	k2eAgInSc4d1	stálý
a	a	k8xC	a
přelétavý	přelétavý	k2eAgInSc4d1	přelétavý
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
často	často	k6eAd1	často
podniká	podnikat	k5eAaImIp3nS	podnikat
potulky	potulka	k1gFnPc4	potulka
a	a	k8xC	a
přesídlování	přesídlování	k1gNnPc4	přesídlování
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
značně	značně	k6eAd1	značně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Těžištěm	těžiště	k1gNnSc7	těžiště
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
50	[number]	k4	50
400	[number]	k4	400
–	–	k?	–
90	[number]	k4	90
500	[number]	k4	500
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
20	[number]	k4	20
000	[number]	k4	000
–	–	k?	–
60	[number]	k4	60
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
její	její	k3xOp3gFnSc1	její
početnost	početnost	k1gFnSc1	početnost
prudce	prudko	k6eAd1	prudko
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
;	;	kIx,	;
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
%	%	kIx~	%
pokles	pokles	k1gInSc1	pokles
stavů	stav	k1gInPc2	stav
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ve	v	k7c6	v
13	[number]	k4	13
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
<g/>
%	%	kIx~	%
v	v	k7c6	v
7	[number]	k4	7
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
již	již	k6eAd1	již
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
do	do	k7c2	do
400	[number]	k4	400
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Její	její	k3xOp3gFnSc4	její
početnost	početnost	k1gFnSc4	početnost
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
výrazným	výrazný	k2eAgInPc3d1	výrazný
početním	početní	k2eAgInPc3d1	početní
výkyvům	výkyv	k1gInPc3	výkyv
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
jen	jen	k6eAd1	jen
obtížně	obtížně	k6eAd1	obtížně
<g/>
,	,	kIx,	,
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
však	však	k9	však
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
hnízdící	hnízdící	k2eAgInSc4d1	hnízdící
druh	druh	k1gInSc4	druh
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
v	v	k7c6	v
58	[number]	k4	58
%	%	kIx~	%
kvadrátů	kvadrát	k1gInPc2	kvadrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
jen	jen	k6eAd1	jen
v	v	k7c6	v
50	[number]	k4	50
%	%	kIx~	%
kvadrátů	kvadrát	k1gInPc2	kvadrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
700	[number]	k4	700
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
na	na	k7c4	na
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavně	hlavně	k9	hlavně
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
drobní	drobný	k2eAgMnPc1d1	drobný
savci	savec	k1gMnPc1	savec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
4965	[number]	k4	4965
obratlovců	obratlovec	k1gMnPc2	obratlovec
nalezených	nalezený	k2eAgInPc2d1	nalezený
ve	v	k7c6	v
237	[number]	k4	237
vývržcích	vývržek	k1gInPc6	vývržek
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
Slovenska	Slovensko	k1gNnSc2	Slovensko
bylo	být	k5eAaImAgNnS	být
94,6	[number]	k4	94,6
%	%	kIx~	%
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
5,2	[number]	k4	5,2
%	%	kIx~	%
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
0,2	[number]	k4	0,2
%	%	kIx~	%
obojživelníků	obojživelník	k1gMnPc2	obojživelník
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
výrazně	výrazně	k6eAd1	výrazně
dominoval	dominovat	k5eAaImAgMnS	dominovat
hraboš	hraboš	k1gMnSc1	hraboš
polní	polní	k2eAgMnSc1d1	polní
<g/>
,	,	kIx,	,
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
vrabec	vrabec	k1gMnSc1	vrabec
domácí	domácí	k1gMnSc1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
výsledky	výsledek	k1gInPc4	výsledek
ukázal	ukázat	k5eAaPmAgInS	ukázat
i	i	k9	i
rozbor	rozbor	k1gInSc1	rozbor
vývržků	vývržek	k1gInPc2	vývržek
ze	z	k7c2	z
severovýchodních	severovýchodní	k2eAgFnPc2d1	severovýchodní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
97,7	[number]	k4	97,7
%	%	kIx~	%
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
hraboš	hraboš	k1gMnSc1	hraboš
polní	polní	k2eAgMnSc1d1	polní
představoval	představovat	k5eAaImAgMnS	představovat
73,3	[number]	k4	73,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1,7	[number]	k4	1,7
%	%	kIx~	%
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
0,6	[number]	k4	0,6
%	%	kIx~	%
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
skokan	skokan	k1gMnSc1	skokan
<g/>
.	.	kIx.	.
<g/>
Loví	lovit	k5eAaImIp3nS	lovit
během	během	k7c2	během
soumraku	soumrak	k1gInSc2	soumrak
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
krmení	krmení	k1gNnSc1	krmení
mláďat	mládě	k1gNnPc2	mládě
občas	občas	k6eAd1	občas
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
za	za	k7c2	za
pomalého	pomalý	k2eAgInSc2d1	pomalý
letu	let	k1gInSc2	let
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
nebo	nebo	k8xC	nebo
vsedě	vsedě	k6eAd1	vsedě
z	z	k7c2	z
pozorovatelny	pozorovatelna	k1gFnSc2	pozorovatelna
a	a	k8xC	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
vynikajícímu	vynikající	k2eAgInSc3d1	vynikající
sluchu	sluch	k1gInSc3	sluch
ji	on	k3xPp3gFnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přesně	přesně	k6eAd1	přesně
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
i	i	k9	i
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
tmě	tma	k1gFnSc6	tma
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
požírá	požírat	k5eAaImIp3nS	požírat
vcelku	vcelku	k6eAd1	vcelku
<g/>
,	,	kIx,	,
srsti	srst	k1gFnSc3	srst
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc6	peří
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nestravitelných	stravitelný	k2eNgFnPc2d1	nestravitelná
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
sovy	sova	k1gFnPc4	sova
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
jejich	jejich	k3xOp3gNnSc7	jejich
vyvrhováním	vyvrhování	k1gNnSc7	vyvrhování
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
šišticovitých	šišticovitý	k2eAgInPc2d1	šišticovitý
vývržků	vývržek	k1gInPc2	vývržek
<g/>
.	.	kIx.	.
</s>
<s>
Vývržky	vývržek	k1gInPc1	vývržek
sovy	sova	k1gFnSc2	sova
pálené	pálená	k1gFnSc2	pálená
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnPc1d1	tmavá
a	a	k8xC	a
díky	díky	k7c3	díky
lesklému	lesklý	k2eAgInSc3d1	lesklý
povrchu	povrch	k1gInSc3	povrch
jakoby	jakoby	k8xS	jakoby
nalakované	nalakovaný	k2eAgFnSc3d1	nalakovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
trvalé	trvalý	k2eAgInPc4d1	trvalý
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
věrné	věrný	k2eAgFnPc1d1	věrná
místu	místo	k1gNnSc3	místo
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
nebo	nebo	k8xC	nebo
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
se	se	k3xPyFc4	se
během	během	k7c2	během
něj	on	k3xPp3gMnSc2	on
ozývají	ozývat	k5eAaImIp3nP	ozývat
pronikavými	pronikavý	k2eAgInPc7d1	pronikavý
chraplavými	chraplavý	k2eAgInPc7d1	chraplavý
skřeky	skřek	k1gInPc7	skřek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
k	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
vyhledávala	vyhledávat	k5eAaImAgFnS	vyhledávat
stromové	stromový	k2eAgFnPc4d1	stromová
dutiny	dutina	k1gFnPc4	dutina
a	a	k8xC	a
skály	skála	k1gFnPc4	skála
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
areálu	areál	k1gInSc2	areál
se	se	k3xPyFc4	se
však	však	k9	však
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
synantropním	synantropní	k2eAgInSc7d1	synantropní
druhem	druh	k1gInSc7	druh
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
tak	tak	k9	tak
většina	většina	k1gFnSc1	většina
populace	populace	k1gFnSc2	populace
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
věžích	věž	k1gFnPc6	věž
kostelů	kostel	k1gInPc2	kostel
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
stodolách	stodola	k1gFnPc6	stodola
<g/>
.	.	kIx.	.
</s>
<s>
Ochotně	ochotně	k6eAd1	ochotně
přijímá	přijímat	k5eAaImIp3nS	přijímat
i	i	k9	i
vhodné	vhodný	k2eAgFnPc4d1	vhodná
hnízdní	hnízdní	k2eAgFnPc4d1	hnízdní
budky	budka	k1gFnPc4	budka
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
nestaví	stavit	k5eNaImIp3nS	stavit
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
vejce	vejce	k1gNnPc4	vejce
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
podklad	podklad	k1gInSc4	podklad
nebo	nebo	k8xC	nebo
do	do	k7c2	do
vývržkové	vývržkový	k2eAgFnSc2d1	vývržkový
drti	drť	k1gFnSc2	drť
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
a	a	k8xC	a
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
hnízdění	hnízdění	k1gNnSc2	hnízdění
silně	silně	k6eAd1	silně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dostupnosti	dostupnost	k1gFnSc6	dostupnost
hlavní	hlavní	k2eAgFnSc2d1	hlavní
potravy	potrava	k1gFnSc2	potrava
–	–	k?	–
hraboše	hraboš	k1gMnSc2	hraboš
polního	polní	k2eAgMnSc2d1	polní
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	on	k3xPp3gNnSc2	on
přemnožení	přemnožení	k1gNnSc2	přemnožení
bývají	bývat	k5eAaImIp3nP	bývat
snůšky	snůška	k1gFnPc1	snůška
početnější	početní	k2eAgFnPc1d2	početnější
a	a	k8xC	a
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
potravní	potravní	k2eAgFnPc4d1	potravní
nabídky	nabídka	k1gFnPc4	nabídka
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nP	muset
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
hnízdění	hnízdění	k1gNnSc1	hnízdění
začíná	začínat	k5eAaImIp3nS	začínat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnPc1	druhý
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
nebo	nebo	k8xC	nebo
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
snůšce	snůška	k1gFnSc6	snůška
bývá	bývat	k5eAaImIp3nS	bývat
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
početnější	početní	k2eAgFnPc1d2	početnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
oválná	oválný	k2eAgNnPc1d1	oválné
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
39,7	[number]	k4	39,7
×	×	k?	×
30,7	[number]	k4	30,7
mm	mm	kA	mm
<g/>
,	,	kIx,	,
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
je	on	k3xPp3gMnPc4	on
snáší	snášet	k5eAaImIp3nP	snášet
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
2	[number]	k4	2
i	i	k8xC	i
více	hodně	k6eAd2	hodně
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
sezení	sezení	k1gNnSc2	sezení
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nP	sedit
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
samec	samec	k1gInSc1	samec
přináší	přinášet	k5eAaImIp3nS	přinášet
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
krmena	krmen	k2eAgNnPc1d1	krmeno
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
vzletnosti	vzletnost	k1gFnPc1	vzletnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
60	[number]	k4	60
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospělá	dospělý	k2eAgNnPc1d1	dospělé
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
1	[number]	k4	1
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
sovy	sova	k1gFnSc2	sova
pálené	pálená	k1gFnSc2	pálená
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
72	[number]	k4	72
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
známý	známý	k2eAgInSc1d1	známý
věk	věk	k1gInSc1	věk
okroužkovaného	okroužkovaný	k2eAgMnSc2d1	okroužkovaný
jedince	jedinec	k1gMnSc2	jedinec
je	být	k5eAaImIp3nS	být
17	[number]	k4	17
let	léto	k1gNnPc2	léto
a	a	k8xC	a
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
úbytku	úbytek	k1gInSc2	úbytek
sovy	sova	k1gFnSc2	sova
pálené	pálená	k1gFnSc2	pálená
jsou	být	k5eAaImIp3nP	být
výrazné	výrazný	k2eAgFnPc4d1	výrazná
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
vhodných	vhodný	k2eAgNnPc2d1	vhodné
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
a	a	k8xC	a
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
úmrtnosti	úmrtnost	k1gFnSc3	úmrtnost
pravidelně	pravidelně	k6eAd1	pravidelně
dochází	docházet	k5eAaImIp3nS	docházet
během	během	k7c2	během
krutých	krutý	k2eAgFnPc2d1	krutá
sněžných	sněžný	k2eAgFnPc2d1	sněžná
zim	zima	k1gFnPc2	zima
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgFnPc3d1	jiná
sovám	sova	k1gFnPc3	sova
náchylnější	náchylný	k2eAgFnSc2d2	náchylnější
kvůli	kvůli	k7c3	kvůli
nižší	nízký	k2eAgFnSc3d2	nižší
schopnosti	schopnost	k1gFnSc3	schopnost
hromadit	hromadit	k5eAaImF	hromadit
zásobní	zásobní	k2eAgInSc4d1	zásobní
tuk	tuk	k1gInSc4	tuk
a	a	k8xC	a
špatným	špatný	k2eAgFnPc3d1	špatná
izolačním	izolační	k2eAgFnPc3d1	izolační
schopnostem	schopnost	k1gFnPc3	schopnost
opeření	opeření	k1gNnSc2	opeření
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
výrazně	výrazně	k6eAd1	výrazně
přibývá	přibývat	k5eAaImIp3nS	přibývat
i	i	k9	i
počet	počet	k1gInSc1	počet
ptáků	pták	k1gMnPc2	pták
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
sovy	sova	k1gFnSc2	sova
pálené	pálená	k1gFnSc2	pálená
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
automobily	automobil	k1gInPc7	automobil
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
70	[number]	k4	70
%	%	kIx~	%
a	a	k8xC	a
např.	např.	kA	např.
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
ročně	ročně	k6eAd1	ročně
zahyne	zahynout	k5eAaPmIp3nS	zahynout
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
tak	tak	k9	tak
často	často	k6eAd1	často
obětí	oběť	k1gFnSc7	oběť
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
příkopy	příkop	k1gInPc1	příkop
a	a	k8xC	a
silniční	silniční	k2eAgInPc1d1	silniční
náspy	násep	k1gInPc1	násep
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
stanoviště	stanoviště	k1gNnPc4	stanoviště
hlodavců	hlodavec	k1gMnPc2	hlodavec
a	a	k8xC	a
pro	pro	k7c4	pro
sovu	sova	k1gFnSc4	sova
pálenou	pálený	k2eAgFnSc4d1	pálená
tudíž	tudíž	k8xC	tudíž
představují	představovat	k5eAaImIp3nP	představovat
lákavou	lákavý	k2eAgFnSc4d1	lákavá
potravní	potravní	k2eAgFnSc4d1	potravní
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
úhynů	úhyn	k1gInPc2	úhyn
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
používání	používání	k1gNnSc1	používání
chemických	chemický	k2eAgInPc2d1	chemický
přípravků	přípravek	k1gInPc2	přípravek
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
chlorovaných	chlorovaný	k2eAgInPc2d1	chlorovaný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
sloužících	sloužící	k2eAgFnPc6d1	sloužící
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
hrozby	hrozba	k1gFnPc4	hrozba
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
nástrahy	nástraha	k1gFnPc1	nástraha
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
uvíznutí	uvíznutí	k1gNnSc1	uvíznutí
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
pády	pád	k1gInPc4	pád
do	do	k7c2	do
komínů	komín	k1gInPc2	komín
<g/>
,	,	kIx,	,
šachet	šachta	k1gFnPc2	šachta
a	a	k8xC	a
vertikálně	vertikálně	k6eAd1	vertikálně
stojících	stojící	k2eAgFnPc2d1	stojící
rour	roura	k1gFnPc2	roura
či	či	k8xC	či
utonutí	utonutí	k1gNnPc2	utonutí
v	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
s	s	k7c7	s
tekutinami	tekutina	k1gFnPc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
171	[number]	k4	171
okroužkovaných	okroužkovaný	k2eAgMnPc2d1	okroužkovaný
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
ČR	ČR	kA	ČR
byly	být	k5eAaImAgFnP	být
nejčastějšími	častý	k2eAgFnPc7d3	nejčastější
příčinami	příčina	k1gFnPc7	příčina
úhynu	úhyn	k1gInSc2	úhyn
srážka	srážka	k1gFnSc1	srážka
s	s	k7c7	s
automobilem	automobil	k1gInSc7	automobil
či	či	k8xC	či
vlakem	vlak	k1gInSc7	vlak
(	(	kIx(	(
<g/>
53	[number]	k4	53
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
utonutí	utonutí	k1gNnSc1	utonutí
nebo	nebo	k8xC	nebo
vyhladovění	vyhladovění	k1gNnSc1	vyhladovění
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
do	do	k7c2	do
nádrží	nádrž	k1gFnPc2	nádrž
na	na	k7c4	na
melasu	melasa	k1gFnSc4	melasa
či	či	k8xC	či
siláž	siláž	k1gFnSc4	siláž
<g/>
,	,	kIx,	,
šachet	šachta	k1gFnPc2	šachta
<g/>
,	,	kIx,	,
rour	roura	k1gFnPc2	roura
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
29	[number]	k4	29
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zvláště	zvláště	k6eAd1	zvláště
chráněná	chráněný	k2eAgFnSc1d1	chráněná
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
aktivní	aktivní	k2eAgFnSc2d1	aktivní
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
i	i	k9	i
vyvěšování	vyvěšování	k1gNnSc1	vyvěšování
hnízdních	hnízdní	k2eAgFnPc2d1	hnízdní
budek	budka	k1gFnPc2	budka
ve	v	k7c6	v
vhodných	vhodný	k2eAgFnPc6d1	vhodná
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zemědělských	zemědělský	k2eAgInPc6d1	zemědělský
objektech	objekt	k1gInPc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
instalaci	instalace	k1gFnSc6	instalace
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
ptáků	pták	k1gMnPc2	pták
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
kuny	kuna	k1gFnPc1	kuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělý	umělý	k2eAgInSc4d1	umělý
chov	chov	k1gInSc4	chov
==	==	k?	==
</s>
</p>
<p>
<s>
Sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
chována	chován	k2eAgFnSc1d1	chována
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
českých	český	k2eAgFnPc6d1	Česká
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ZOO	zoo	k1gNnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
28	[number]	k4	28
poddruhů	poddruh	k1gInPc2	poddruh
sovy	sova	k1gFnSc2	sova
pálené	pálená	k1gFnSc2	pálená
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
první	první	k4xOgMnPc1	první
4	[number]	k4	4
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
středomořská	středomořský	k2eAgFnSc1d1	středomořská
(	(	kIx(	(
<g/>
Tyto	tento	k3xDgFnPc4	tento
alba	alba	k1gFnSc1	alba
alba	alba	k1gFnSc1	alba
<g/>
)	)	kIx)	)
–	–	k?	–
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
včetně	včetně	k7c2	včetně
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
od	od	k7c2	od
Maroka	Maroko	k1gNnSc2	Maroko
po	po	k7c4	po
Egypt	Egypt	k1gInSc4	Egypt
</s>
</p>
<p>
<s>
s.	s.	k?	s.
p.	p.	k?	p.
středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
(	(	kIx(	(
<g/>
T.	T.	kA	T.
a.	a.	k?	a.
guttata	guttata	k1gFnSc1	guttata
<g/>
)	)	kIx)	)
–	–	k?	–
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
severnější	severní	k2eAgFnSc1d2	severnější
část	část	k1gFnSc1	část
Evropy	Evropa	k1gFnSc2	Evropa
</s>
</p>
<p>
<s>
s.	s.	k?	s.
p.	p.	k?	p.
sardinská	sardinský	k2eAgFnSc1d1	sardinská
(	(	kIx(	(
<g/>
T.	T.	kA	T.
a.	a.	k?	a.
ernesti	ernest	k1gFnSc2	ernest
<g/>
)	)	kIx)	)
–	–	k?	–
Sardinie	Sardinie	k1gFnSc1	Sardinie
a	a	k8xC	a
Korsika	Korsika	k1gFnSc1	Korsika
</s>
</p>
<p>
<s>
s.	s.	k?	s.
p.	p.	k?	p.
předoasijská	předoasijský	k2eAgFnSc1d1	předoasijský
(	(	kIx(	(
<g/>
T.	T.	kA	T.
a.	a.	k?	a.
erlangeri	erlanger	k1gFnSc2	erlanger
<g/>
)	)	kIx)	)
–	–	k?	–
Kréta	Kréta	k1gFnSc1	Kréta
a	a	k8xC	a
menší	malý	k2eAgInPc1d2	menší
řecké	řecký	k2eAgInPc1d1	řecký
ostrovy	ostrov	k1gInPc1	ostrov
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
také	také	k9	také
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Sinajského	sinajský	k2eAgInSc2d1	sinajský
poloostrova	poloostrov	k1gInSc2	poloostrov
po	po	k7c4	po
Írán	Írán	k1gInSc4	Írán
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
schmitzi	schmitze	k1gFnSc6	schmitze
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
gracilirostris	gracilirostris	k1gInSc1	gracilirostris
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
dedorta	dedorta	k1gFnSc1	dedorta
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
affinis	affinis	k1gInSc1	affinis
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
poensis	poensis	k1gInSc1	poensis
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
thomensis	thomensis	k1gInSc1	thomensis
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
stertens	stertens	k1gInSc1	stertens
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
deroepstorffi	deroepstorffi	k6eAd1	deroepstorffi
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
javanica	javanica	k1gMnSc1	javanica
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
sumbaensis	sumbaensis	k1gInSc1	sumbaensis
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
meeki	meek	k1gFnSc2	meek
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
delicatula	delicatula	k1gFnSc1	delicatula
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
crassirostris	crassirostris	k1gInSc1	crassirostris
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
interposita	interposita	k1gFnSc1	interposita
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
pratincola	pratincola	k1gFnSc1	pratincola
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
guatemalae	guatemalae	k1gInSc1	guatemalae
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
bondi	bond	k1gMnPc1	bond
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
furcata	furcata	k1gFnSc1	furcata
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
niveicauda	niveicauda	k1gMnSc1	niveicauda
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
bargei	bargei	k6eAd1	bargei
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
punctatissima	punctatissima	k1gFnSc1	punctatissima
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
contempta	contempta	k1gMnSc1	contempta
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
hellmayri	hellmayri	k6eAd1	hellmayri
</s>
</p>
<p>
<s>
T.	T.	kA	T.
a.	a.	k?	a.
tuidara	tuidara	k1gFnSc1	tuidara
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sova	sova	k1gFnSc1	sova
pálená	pálený	k2eAgFnSc1d1	pálená
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Tyto	tyt	k2eAgNnSc1d1	tyto
alba	album	k1gNnPc1	album
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
http://www.biolib.cz/cz/taxon/id8753/	[url]	k4	http://www.biolib.cz/cz/taxon/id8753/
</s>
</p>
<p>
<s>
https://web.archive.org/web/20130725165302/http://www.birdlife.cz/wpimages/video/ptak_roku_1997.pdf	[url]	k1gMnSc1	https://web.archive.org/web/20130725165302/http://www.birdlife.cz/wpimages/video/ptak_roku_1997.pdf
</s>
</p>
<p>
<s>
http://www.naturfoto.cz/sova-palena-fotografie-291.html	[url]	k1gMnSc1	http://www.naturfoto.cz/sova-palena-fotografie-291.html
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
sova	sova	k1gFnSc1	sova
pálená	pálená	k1gFnSc1	pálená
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sova	sova	k1gFnSc1	sova
pálená	pálený	k2eAgFnSc1d1	pálená
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Sova	sova	k1gFnSc1	sova
pálená	pálený	k2eAgFnSc1d1	pálená
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Příroda	příroda	k1gFnSc1	příroda
</s>
</p>
