<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
druh	druh	k1gInSc4	druh
sovy	sova	k1gFnSc2	sova
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
řadu	řada	k1gFnSc4	řada
biotopů	biotop	k1gInPc2	biotop
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
