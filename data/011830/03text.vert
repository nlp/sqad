<p>
<s>
Organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
čistá	čistý	k2eAgFnSc1d1	čistá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
molekuly	molekula	k1gFnPc1	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
atomy	atom	k1gInPc1	atom
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
především	především	k9	především
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
známých	známý	k2eAgFnPc2d1	známá
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
registrován	registrovat	k5eAaBmNgMnS	registrovat
v	v	k7c6	v
Beilsteinově	Beilsteinův	k2eAgFnSc6d1	Beilsteinův
databázi	databáze	k1gFnSc6	databáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
oxidy	oxid	k1gInPc1	oxid
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
uhličitá	uhličitý	k2eAgFnSc1d1	uhličitá
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
soli	sůl	k1gFnPc4	sůl
-	-	kIx~	-
uhličitany	uhličitan	k1gInPc4	uhličitan
a	a	k8xC	a
hydrogenuhličitany	hydrogenuhličitan	k1gInPc4	hydrogenuhličitan
<g/>
,	,	kIx,	,
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
(	(	kIx(	(
<g/>
karbidy	karbid	k1gInPc7	karbid
<g/>
)	)	kIx)	)
a	a	k8xC	a
nekovy	nekov	k1gInPc1	nekov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sirouhlík	sirouhlík	k1gInSc1	sirouhlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
anorganické	anorganický	k2eAgFnPc4d1	anorganická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
mezi	mezi	k7c7	mezi
anorganickými	anorganický	k2eAgFnPc7d1	anorganická
a	a	k8xC	a
organickými	organický	k2eAgFnPc7d1	organická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dichlorid	dichlorid	k1gInSc4	dichlorid
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
čili	čili	k8xC	čili
fosgen	fosgen	k1gInSc1	fosgen
<g/>
,	,	kIx,	,
kyanidy	kyanid	k1gInPc1	kyanid
a	a	k8xC	a
diamid	diamid	k1gInSc1	diamid
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
čili	čili	k8xC	čili
močovina	močovina	k1gFnSc1	močovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
organický	organický	k2eAgMnSc1d1	organický
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
vymezení	vymezení	k1gNnSc2	vymezení
oboru	obor	k1gInSc2	obor
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
jako	jako	k8xC	jako
chemie	chemie	k1gFnSc2	chemie
přírodních	přírodní	k2eAgFnPc2d1	přírodní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
veškerý	veškerý	k3xTgInSc1	veškerý
známý	známý	k2eAgInSc1d1	známý
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
sloučeninách	sloučenina	k1gFnPc6	sloučenina
uhlíku	uhlík	k1gInSc2	uhlík
založen	založit	k5eAaPmNgInS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dnes	dnes	k6eAd1	dnes
známých	známý	k2eAgFnPc2d1	známá
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
však	však	k9	však
syntetických	syntetický	k2eAgInPc2d1	syntetický
(	(	kIx(	(
<g/>
třebaže	třebaže	k8xS	třebaže
připravených	připravený	k2eAgInPc2d1	připravený
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
neznamená	znamenat	k5eNaImIp3nS	znamenat
nutně	nutně	k6eAd1	nutně
biologický	biologický	k2eAgInSc1d1	biologický
původ	původ	k1gInSc1	původ
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
methan	methan	k1gInSc4	methan
<g/>
/	/	kIx~	/
<g/>
ethanová	ethanový	k2eAgNnPc4d1	ethanový
jezera	jezero	k1gNnPc4	jezero
a	a	k8xC	a
organický	organický	k2eAgInSc4d1	organický
smog	smog	k1gInSc4	smog
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
Saturnově	Saturnův	k2eAgInSc6d1	Saturnův
měsíci	měsíc	k1gInSc6	měsíc
Titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
detekovány	detekován	k2eAgInPc1d1	detekován
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
americkým	americký	k2eAgNnSc7d1	americké
vozítkem	vozítko	k1gNnSc7	vozítko
Curiosity	Curiosita	k1gFnSc2	Curiosita
operující	operující	k2eAgFnSc2d1	operující
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Gale	Gale	k1gMnSc1	Gale
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planetky	planetka	k1gFnSc2	planetka
ʻ	ʻ	k?	ʻ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kategorie	kategorie	k1gFnPc1	kategorie
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Třídění	třídění	k1gNnSc1	třídění
v	v	k7c6	v
systému	systém	k1gInSc6	systém
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
základních	základní	k2eAgNnPc2d1	základní
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
podle	podle	k7c2	podle
struktury	struktura	k1gFnSc2	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgNnSc7	první
hlediskem	hledisko	k1gNnSc7	hledisko
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
skeletu	skelet	k1gInSc2	skelet
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
rozumíme	rozumět	k5eAaImIp1nP	rozumět
sled	sled	k1gInSc4	sled
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
přímo	přímo	k6eAd1	přímo
vázaných	vázaný	k2eAgInPc2d1	vázaný
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
buď	buď	k8xC	buď
otevřené	otevřený	k2eAgInPc4d1	otevřený
řetězce	řetězec	k1gInPc4	řetězec
<g/>
,	,	kIx,	,
nerozvětvené	rozvětvený	k2eNgInPc4d1	nerozvětvený
nebo	nebo	k8xC	nebo
větvené	větvený	k2eAgInPc4d1	větvený
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
řetězce	řetězec	k1gInPc4	řetězec
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
smyček	smyčka	k1gFnPc2	smyčka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
cykly	cyklus	k1gInPc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
cyklů	cyklus	k1gInPc2	cyklus
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
sloučeniny	sloučenina	k1gFnSc2	sloučenina
několik	několik	k4yIc4	několik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
velkých	velký	k2eAgFnPc2d1	velká
tříd	třída	k1gFnPc2	třída
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
podtřídami	podtřída	k1gFnPc7	podtřída
<g/>
,	,	kIx,	,
vytvářenými	vytvářený	k2eAgInPc7d1	vytvářený
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
cyklů	cyklus	k1gInPc2	cyklus
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
necyklické	cyklický	k2eNgFnSc2d1	necyklická
(	(	kIx(	(
<g/>
též	též	k9	též
acyklické	acyklický	k2eAgFnSc6d1	acyklická
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
cyklické	cyklický	k2eAgFnPc1d1	cyklická
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
monocyklické	monocyklický	k2eAgFnPc1d1	monocyklický
(	(	kIx(	(
<g/>
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
cyklem	cyklus	k1gInSc7	cyklus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
bicyklické	bicyklický	k2eAgFnPc1d1	bicyklický
(	(	kIx(	(
<g/>
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
cykly	cyklus	k1gInPc7	cyklus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tricyklické	tricyklický	k2eAgFnPc1d1	tricyklická
(	(	kIx(	(
<g/>
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
cykly	cyklus	k1gInPc7	cyklus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
polycyklické	polycyklický	k2eAgFnPc1d1	polycyklický
(	(	kIx(	(
<g/>
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
cykly	cyklus	k1gInPc7	cyklus
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
řetěz	řetěz	k1gInSc4	řetěz
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
sice	sice	k8xC	sice
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
do	do	k7c2	do
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
cyklu	cyklus	k1gInSc6	cyklus
je	být	k5eAaImIp3nS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
jiným	jiný	k2eAgInSc7d1	jiný
atomem	atom	k1gInSc7	atom
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
nazýváme	nazývat	k5eAaImIp1nP	nazývat
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
sloučeninami	sloučenina	k1gFnPc7	sloučenina
heterocyklickými	heterocyklický	k2eAgFnPc7d1	heterocyklická
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
"	"	kIx"	"
<g/>
heteros	heterosa	k1gFnPc2	heterosa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
cizí	cizí	k2eAgFnSc1d1	cizí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
cyklických	cyklický	k2eAgFnPc2d1	cyklická
sloučenin	sloučenina	k1gFnPc2	sloučenina
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
šestičlenný	šestičlenný	k2eAgInSc4d1	šestičlenný
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc4d1	tvořený
šesti	šest	k4xCc6	šest
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
(	(	kIx(	(
<g/>
formálně	formálně	k6eAd1	formálně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
atomy	atom	k1gInPc7	atom
střídají	střídat	k5eAaImIp3nP	střídat
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
a	a	k8xC	a
dvojné	dvojný	k2eAgFnPc1d1	dvojná
vazby	vazba	k1gFnPc1	vazba
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
aromatický	aromatický	k2eAgInSc1d1	aromatický
cyklus	cyklus	k1gInSc1	cyklus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
elektronů	elektron	k1gInPc2	elektron
ve	v	k7c6	v
vazbách	vazba	k1gFnPc6	vazba
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
uhlíkatých	uhlíkatý	k2eAgFnPc2d1	uhlíkatá
cyklických	cyklický	k2eAgFnPc2d1	cyklická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
do	do	k7c2	do
speciální	speciální	k2eAgFnSc2d1	speciální
třídy	třída	k1gFnSc2	třída
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
aromatické	aromatický	k2eAgFnPc1d1	aromatická
<g/>
;	;	kIx,	;
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
nejjednodušším	jednoduchý	k2eAgMnSc7d3	nejjednodušší
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
uhlovodík	uhlovodík	k1gInSc1	uhlovodík
benzen	benzen	k1gInSc1	benzen
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádný	žádný	k3yNgInSc4	žádný
aromatický	aromatický	k2eAgInSc4d1	aromatický
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sloučenin	sloučenina	k1gFnPc2	sloučenina
necyklických	cyklický	k2eNgFnPc2d1	necyklická
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
alifatické	alifatický	k2eAgFnPc1d1	alifatický
sloučeniny	sloučenina	k1gFnPc1	sloučenina
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
podle	podle	k7c2	podle
funkčních	funkční	k2eAgFnPc2d1	funkční
skupin	skupina	k1gFnPc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgInSc7	druhý
hlavním	hlavní	k2eAgInSc7d1	hlavní
hlediskem	hledisko	k1gNnSc7	hledisko
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
určitých	určitý	k2eAgFnPc2d1	určitá
funkčních	funkční	k2eAgFnPc2d1	funkční
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
příslušným	příslušný	k2eAgInSc7d1	příslušný
sloučeninám	sloučenina	k1gFnPc3	sloučenina
dávají	dávat	k5eAaImIp3nP	dávat
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
nejběžnějších	běžný	k2eAgFnPc2d3	nejběžnější
tříd	třída	k1gFnPc2	třída
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
podle	podle	k7c2	podle
obsažených	obsažený	k2eAgFnPc2d1	obsažená
funkčních	funkční	k2eAgFnPc2d1	funkční
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
molekulách	molekula	k1gFnPc6	molekula
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
-	-	kIx~	-
kromě	kromě	k7c2	kromě
uhlíku	uhlík	k1gInSc2	uhlík
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
žádné	žádný	k3yNgFnPc4	žádný
specifické	specifický	k2eAgFnPc4d1	specifická
funkční	funkční	k2eAgFnPc4d1	funkční
skupiny	skupina	k1gFnPc4	skupina
</s>
</p>
<p>
<s>
nasycené	nasycený	k2eAgInPc1d1	nasycený
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
-	-	kIx~	-
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
násobné	násobný	k2eAgFnPc4d1	násobná
vazby	vazba	k1gFnPc4	vazba
</s>
</p>
<p>
<s>
nenasycené	nasycený	k2eNgInPc1d1	nenasycený
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
násobné	násobný	k2eAgFnPc1d1	násobná
vazby	vazba	k1gFnPc1	vazba
</s>
</p>
<p>
<s>
alkeny	alkena	k1gFnPc1	alkena
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dvojné	dvojný	k2eAgFnPc1d1	dvojná
vazby	vazba	k1gFnPc1	vazba
</s>
</p>
<p>
<s>
alkyny	alkyna	k1gFnPc1	alkyna
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
trojné	trojný	k2eAgFnPc1d1	trojná
vazby	vazba	k1gFnPc1	vazba
</s>
</p>
<p>
<s>
kombinované	kombinovaný	k2eAgInPc1d1	kombinovaný
nenasycené	nasycený	k2eNgInPc1d1	nenasycený
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jak	jak	k6eAd1	jak
dvojné	dvojný	k2eAgFnPc1d1	dvojná
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
trojné	trojný	k2eAgFnPc1d1	trojná
vazby	vazba	k1gFnPc1	vazba
</s>
</p>
<p>
<s>
aromatické	aromatický	k2eAgInPc1d1	aromatický
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jeden	jeden	k4xCgMnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
aromatických	aromatický	k2eAgInPc2d1	aromatický
cyklů	cyklus	k1gInPc2	cyklus
</s>
</p>
<p>
<s>
halogenderiváty	halogenderiváta	k1gFnPc1	halogenderiváta
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
místo	místo	k7c2	místo
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
atom	atom	k1gInSc1	atom
halogenu	halogen	k1gInSc2	halogen
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
,	,	kIx,	,
Br	br	k0	br
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
I	i	k9	i
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hydroxylové	hydroxylový	k2eAgInPc1d1	hydroxylový
deriváty	derivát	k1gInPc1	derivát
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
hydroxylových	hydroxylový	k2eAgFnPc2d1	hydroxylová
skupin	skupina	k1gFnPc2	skupina
OH	OH	kA	OH
</s>
</p>
<p>
<s>
alkoholy	alkohol	k1gInPc1	alkohol
-	-	kIx~	-
hydroxyl	hydroxyl	k1gInSc1	hydroxyl
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
alifatickou	alifatický	k2eAgFnSc4d1	alifatický
část	část	k1gFnSc4	část
molekuly	molekula	k1gFnSc2	molekula
</s>
</p>
<p>
<s>
fenoly	fenol	k1gInPc1	fenol
-	-	kIx~	-
hydroxyl	hydroxyl	k1gInSc1	hydroxyl
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
aromatickou	aromatický	k2eAgFnSc4d1	aromatická
část	část	k1gFnSc4	část
molekuly	molekula	k1gFnSc2	molekula
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
dvojně	dvojně	k6eAd1	dvojně
vázaným	vázaný	k2eAgInSc7d1	vázaný
kyslíkem	kyslík	k1gInSc7	kyslík
</s>
</p>
<p>
<s>
aldehydy	aldehyd	k1gInPc1	aldehyd
-	-	kIx~	-
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
tentýž	týž	k3xTgInSc4	týž
atom	atom	k1gInSc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
vázán	vázat	k5eAaImNgInS	vázat
i	i	k8xC	i
atom	atom	k1gInSc1	atom
vodíku	vodík	k1gInSc2	vodík
</s>
</p>
<p>
<s>
ketony	keton	k1gInPc1	keton
-	-	kIx~	-
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
tentýž	týž	k3xTgInSc4	týž
atom	atom	k1gInSc4	atom
uhlíku	uhlík	k1gInSc2	uhlík
vázán	vázán	k2eAgInSc4d1	vázán
žádný	žádný	k3yNgInSc4	žádný
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
</s>
</p>
<p>
<s>
karboxylové	karboxylový	k2eAgFnPc1d1	karboxylová
kyseliny	kyselina	k1gFnPc1	kyselina
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
-COOH	-COOH	k?	-COOH
</s>
</p>
<p>
<s>
estery	ester	k1gInPc1	ester
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
COO-	COO-	k1gFnSc2	COO-
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
dvojmocnou	dvojmocný	k2eAgFnSc4d1	dvojmocná
síru	síra	k1gFnSc4	síra
</s>
</p>
<p>
<s>
thioly	thiola	k1gFnPc1	thiola
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
funkční	funkční	k2eAgFnSc7d1	funkční
skupinou	skupina	k1gFnSc7	skupina
-SH	-SH	k?	-SH
</s>
</p>
<p>
<s>
thioaldehydy	thioaldehyd	k1gInPc1	thioaldehyd
a	a	k8xC	a
thioketony	thioketon	k1gInPc1	thioketon
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
funkční	funkční	k2eAgFnSc7d1	funkční
skupinou	skupina	k1gFnSc7	skupina
=	=	kIx~	=
<g/>
S	s	k7c7	s
</s>
</p>
<p>
<s>
thiokarboxylové	thiokarboxylový	k2eAgFnPc1d1	thiokarboxylový
kyseliny	kyselina	k1gFnPc1	kyselina
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
funkční	funkční	k2eAgFnSc7d1	funkční
skupinou	skupina	k1gFnSc7	skupina
-CSOH	-CSOH	k?	-CSOH
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc2d1	obsahující
dusíkový	dusíkový	k2eAgInSc4d1	dusíkový
atom	atom	k1gInSc4	atom
</s>
</p>
<p>
<s>
aminy	amin	k1gInPc1	amin
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
-NH	-NH	k?	-NH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
-NH-	-NH-	k?	-NH-
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
-N	-N	k?	-N
<g/>
<	<	kIx(	<
</s>
</p>
<p>
<s>
nitrily	nitril	k1gInPc1	nitril
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
-C	-C	k?	-C
<g/>
≡	≡	k?	≡
<g/>
N	N	kA	N
</s>
</p>
<p>
<s>
amidy	amid	k1gInPc1	amid
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
-CONH	-CONH	k?	-CONH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
-CONH-	-CONH-	k?	-CONH-
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
-CON	-CON	k?	-CON
<g/>
<	<	kIx(	<
</s>
</p>
<p>
<s>
a	a	k8xC	a
j.	j.	k?	j.
<g/>
Shora	shora	k6eAd1	shora
uvedený	uvedený	k2eAgInSc4d1	uvedený
výčet	výčet	k1gInSc4	výčet
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jen	jen	k9	jen
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
a	a	k8xC	a
nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
třídy	třída	k1gFnPc1	třída
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
molekule	molekula	k1gFnSc6	molekula
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
nacházet	nacházet	k5eAaImF	nacházet
více	hodně	k6eAd2	hodně
různých	různý	k2eAgFnPc2d1	různá
funkčních	funkční	k2eAgFnPc2d1	funkční
skupin	skupina	k1gFnPc2	skupina
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
shora	shora	k6eAd1	shora
uvedené	uvedený	k2eAgNnSc1d1	uvedené
schéma	schéma	k1gNnSc1	schéma
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c4	na
nespočet	nespočet	k1gInSc4	nespočet
různých	různý	k2eAgFnPc2d1	různá
možných	možný	k2eAgFnPc2d1	možná
kombinaci	kombinace	k1gFnSc3	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
současně	současně	k6eAd1	současně
mezi	mezi	k7c4	mezi
karboxylové	karboxylový	k2eAgFnPc4d1	karboxylová
kyseliny	kyselina	k1gFnPc4	kyselina
a	a	k8xC	a
mezi	mezi	k7c7	mezi
aminy	amin	k1gInPc7	amin
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
podle	podle	k7c2	podle
chemické	chemický	k2eAgFnSc2d1	chemická
podobnosti	podobnost	k1gFnSc2	podobnost
===	===	k?	===
</s>
</p>
<p>
<s>
Třetím	třetí	k4xOgNnSc7	třetí
hlediskem	hledisko	k1gNnSc7	hledisko
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
chemická	chemický	k2eAgFnSc1d1	chemická
podobnost	podobnost	k1gFnSc1	podobnost
určitých	určitý	k2eAgFnPc2d1	určitá
skupin	skupina	k1gFnPc2	skupina
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
původem	původ	k1gInSc7	původ
v	v	k7c6	v
přírodních	přírodní	k2eAgInPc6d1	přírodní
materiálech	materiál	k1gInPc6	materiál
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
specifickém	specifický	k2eAgInSc6d1	specifický
fyziologickém	fyziologický	k2eAgInSc6d1	fyziologický
či	či	k8xC	či
jiném	jiný	k2eAgInSc6d1	jiný
účinku	účinek	k1gInSc6	účinek
na	na	k7c4	na
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
případy	případ	k1gInPc7	případ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
následující	následující	k2eAgFnSc2d1	následující
kategorie	kategorie	k1gFnSc2	kategorie
látek	látka	k1gFnPc2	látka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
terpeny	terpen	k1gInPc1	terpen
</s>
</p>
<p>
<s>
monoterpeny	monoterpen	k2eAgFnPc1d1	monoterpen
</s>
</p>
<p>
<s>
seskviterpeny	seskviterpen	k2eAgFnPc1d1	seskviterpen
</s>
</p>
<p>
<s>
diterpeny	diterpen	k2eAgFnPc1d1	diterpen
</s>
</p>
<p>
<s>
triterpeny	triterpen	k2eAgFnPc1d1	triterpen
</s>
</p>
<p>
<s>
steroidy	steroid	k1gInPc1	steroid
</s>
</p>
<p>
<s>
alkaloidy	alkaloid	k1gInPc1	alkaloid
</s>
</p>
<p>
<s>
sacharidy	sacharid	k1gInPc1	sacharid
(	(	kIx(	(
<g/>
cukry	cukr	k1gInPc1	cukr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
monosacharidy	monosacharid	k1gInPc1	monosacharid
</s>
</p>
<p>
<s>
disacharidy	disacharid	k1gInPc1	disacharid
</s>
</p>
<p>
<s>
trisacharidy	trisacharid	k1gInPc1	trisacharid
</s>
</p>
<p>
<s>
oligosacharidy	oligosacharid	k1gInPc1	oligosacharid
</s>
</p>
<p>
<s>
polysacharidy	polysacharid	k1gInPc1	polysacharid
</s>
</p>
<p>
<s>
peptidy	peptid	k1gInPc1	peptid
</s>
</p>
<p>
<s>
dipeptidy	dipeptida	k1gFnPc1	dipeptida
</s>
</p>
<p>
<s>
tripeptidy	tripeptida	k1gFnPc1	tripeptida
</s>
</p>
<p>
<s>
oligopeptidy	oligopeptida	k1gFnPc1	oligopeptida
</s>
</p>
<p>
<s>
polypeptidy	polypeptid	k1gInPc1	polypeptid
</s>
</p>
<p>
<s>
vitaminy	vitamin	k1gInPc1	vitamin
</s>
</p>
<p>
<s>
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
</s>
</p>
<p>
<s>
apod.	apod.	kA	apod.
<g/>
Ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
výčet	výčet	k1gInSc1	výčet
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
se	se	k3xPyFc4	se
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
používají	používat	k5eAaImIp3nP	používat
systematické	systematický	k2eAgInPc1d1	systematický
názvy	název	k1gInPc1	název
a	a	k8xC	a
triviální	triviální	k2eAgInPc1d1	triviální
názvy	název	k1gInPc1	název
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejich	jejich	k3xOp3gFnSc1	jejich
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
nazvat	nazvat	k5eAaPmF	nazvat
semisystematické	semisystematický	k2eAgInPc4d1	semisystematický
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systematické	systematický	k2eAgInPc1d1	systematický
názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Systematické	systematický	k2eAgNnSc1d1	systematické
názvosloví	názvosloví	k1gNnSc1	názvosloví
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
zejména	zejména	k9	zejména
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
substitučního	substituční	k2eAgInSc2d1	substituční
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
tvorby	tvorba	k1gFnSc2	tvorba
názvu	název	k1gInSc2	název
vždy	vždy	k6eAd1	vždy
počíná	počínat	k5eAaImIp3nS	počínat
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
skeletu	skelet	k1gInSc2	skelet
tvořeného	tvořený	k2eAgInSc2d1	tvořený
nepřerušenou	přerušený	k2eNgFnSc7d1	nepřerušená
řadou	řada	k1gFnSc7	řada
vzájemně	vzájemně	k6eAd1	vzájemně
vázaných	vázaný	k2eAgInPc2d1	vázaný
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
odvodíme	odvodit	k5eAaPmIp1nP	odvodit
kořen	kořen	k1gInSc4	kořen
názvu	název	k1gInSc2	název
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
příslušného	příslušný	k2eAgInSc2d1	příslušný
uhlovodíku	uhlovodík	k1gInSc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
modifikujeme	modifikovat	k5eAaBmIp1nP	modifikovat
pomocí	pomocí	k7c2	pomocí
přípon	přípona	k1gFnPc2	přípona
definujících	definující	k2eAgFnPc2d1	definující
nejprve	nejprve	k6eAd1	nejprve
násobné	násobný	k2eAgFnPc4d1	násobná
vazby	vazba	k1gFnPc4	vazba
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
další	další	k2eAgFnPc4d1	další
přípony	přípona	k1gFnPc4	přípona
<g/>
,	,	kIx,	,
ukazující	ukazující	k2eAgInPc4d1	ukazující
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
funkčních	funkční	k2eAgFnPc2d1	funkční
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc4	přítomnost
některých	některý	k3yIgFnPc2	některý
jiných	jiný	k2eAgFnPc2d1	jiná
funkčních	funkční	k2eAgFnPc2d1	funkční
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
přípony	přípona	k1gFnPc1	přípona
<g/>
,	,	kIx,	,
tak	tak	k9	tak
předpony	předpona	k1gFnPc1	předpona
popisující	popisující	k2eAgFnSc2d1	popisující
funkční	funkční	k2eAgFnSc2d1	funkční
skupiny	skupina	k1gFnSc2	skupina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
doplněny	doplnit	k5eAaPmNgInP	doplnit
latinskými	latinský	k2eAgFnPc7d1	Latinská
číslovkami	číslovka	k1gFnPc7	číslovka
<g/>
,	,	kIx,	,
udávajícími	udávající	k2eAgFnPc7d1	udávající
počet	počet	k1gInSc1	počet
té	ten	k3xDgFnSc2	ten
které	který	k3yRgFnSc2	který
funkční	funkční	k2eAgFnSc2d1	funkční
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
arabských	arabský	k2eAgFnPc2d1	arabská
číslic	číslice	k1gFnPc2	číslice
pak	pak	k6eAd1	pak
určujeme	určovat	k5eAaImIp1nP	určovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
se	se	k3xPyFc4	se
která	který	k3yIgFnSc1	který
funkční	funkční	k2eAgFnSc1d1	funkční
skupina	skupina	k1gFnSc1	skupina
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dalších	další	k2eAgInPc2d1	další
grafických	grafický	k2eAgInPc2d1	grafický
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
písmen	písmeno	k1gNnPc2	písmeno
latinské	latinský	k2eAgFnSc2d1	Latinská
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
k	k	k7c3	k
bližšímu	blízký	k2eAgNnSc3d2	bližší
určení	určení	k1gNnSc3	určení
prostorového	prostorový	k2eAgNnSc2d1	prostorové
uspořádání	uspořádání	k1gNnSc2	uspořádání
molekul	molekula	k1gFnPc2	molekula
dané	daný	k2eAgFnSc2d1	daná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Triviální	triviální	k2eAgInPc1d1	triviální
názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
obchodní	obchodní	k2eAgFnSc6d1	obchodní
praxi	praxe	k1gFnSc6	praxe
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
obecně	obecně	k6eAd1	obecně
známé	známý	k2eAgFnPc4d1	známá
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
používá	používat	k5eAaImIp3nS	používat
triviálních	triviální	k2eAgInPc2d1	triviální
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
názvů	název	k1gInPc2	název
historických	historický	k2eAgInPc2d1	historický
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
však	však	k9	však
neodrážejí	odrážet	k5eNaImIp3nP	odrážet
ani	ani	k8xC	ani
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
strukturu	struktura	k1gFnSc4	struktura
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
těmito	tento	k3xDgInPc7	tento
názvy	název	k1gInPc7	název
označují	označovat	k5eAaImIp3nP	označovat
zejména	zejména	k9	zejména
látky	látka	k1gFnPc1	látka
přírodního	přírodní	k2eAgInSc2d1	přírodní
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
izolované	izolovaný	k2eAgInPc1d1	izolovaný
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgMnPc2	který
by	by	kYmCp3nP	by
jejich	jejich	k3xOp3gInPc1	jejich
systematické	systematický	k2eAgInPc1d1	systematický
názvy	název	k1gInPc1	název
byly	být	k5eAaImAgInP	být
příliš	příliš	k6eAd1	příliš
složité	složitý	k2eAgInPc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
známé	známý	k2eAgInPc1d1	známý
pod	pod	k7c7	pod
triviálním	triviální	k2eAgInSc7d1	triviální
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
názvu	název	k1gInSc3	název
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc1	přednost
před	před	k7c7	před
názvem	název	k1gInSc7	název
systematickým	systematický	k2eAgInSc7d1	systematický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
triviálně	triviálně	k6eAd1	triviálně
glycerin	glycerin	k1gInSc1	glycerin
místo	místo	k7c2	místo
systematického	systematický	k2eAgMnSc2d1	systematický
propantriol	propantriol	k1gInSc1	propantriol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Semisystematické	Semisystematický	k2eAgInPc1d1	Semisystematický
názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
názvů	název	k1gInPc2	název
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
semitriviální	semitriviální	k2eAgInPc4d1	semitriviální
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
předchozích	předchozí	k2eAgInPc2d1	předchozí
dvou	dva	k4xCgInPc2	dva
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	s	k7c7	s
dvojím	dvojí	k4xRgInSc7	dvojí
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
pomocí	pomocí	k7c2	pomocí
přípony	přípona	k1gFnSc2	přípona
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
molekula	molekula	k1gFnSc1	molekula
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
připojí	připojit	k5eAaPmIp3nS	připojit
se	se	k3xPyFc4	se
přípona	přípona	k1gFnSc1	přípona
<g/>
,	,	kIx,	,
identifikující	identifikující	k2eAgInSc1d1	identifikující
základní	základní	k2eAgInSc4d1	základní
chemický	chemický	k2eAgInSc4d1	chemický
charakter	charakter	k1gInSc4	charakter
dané	daný	k2eAgFnSc2d1	daná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
semitriviálního	semitriviální	k2eAgInSc2d1	semitriviální
názvu	název	k1gInSc2	název
glycerol	glycerol	k1gInSc4	glycerol
místo	místo	k7c2	místo
plně	plně	k6eAd1	plně
triviálního	triviální	k2eAgInSc2d1	triviální
glycerin	glycerin	k1gInSc4	glycerin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zdůraznila	zdůraznit	k5eAaPmAgFnS	zdůraznit
přítomnost	přítomnost	k1gFnSc1	přítomnost
hydroxylových	hydroxylový	k2eAgFnPc2d1	hydroxylová
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
,	,	kIx,	,
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
pro	pro	k7c4	pro
alkoholy	alkohol	k1gInPc4	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
používaném	používaný	k2eAgMnSc6d1	používaný
u	u	k7c2	u
derivátů	derivát	k1gInPc2	derivát
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
odvozených	odvozený	k2eAgFnPc2d1	odvozená
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
)	)	kIx)	)
výchozí	výchozí	k2eAgFnPc1d1	výchozí
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přírodní	přírodní	k2eAgFnPc4d1	přírodní
látky	látka	k1gFnPc4	látka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
použijeme	použít	k5eAaPmIp1nP	použít
triviálního	triviální	k2eAgInSc2d1	triviální
názvu	název	k1gInSc2	název
této	tento	k3xDgFnSc2	tento
původní	původní	k2eAgFnSc2d1	původní
sloučeniny	sloučenina	k1gFnSc2	sloučenina
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
prostředků	prostředek	k1gInPc2	prostředek
systematického	systematický	k2eAgNnSc2d1	systematické
názvosloví	názvosloví	k1gNnSc2	názvosloví
zaznamenáme	zaznamenat	k5eAaPmIp1nP	zaznamenat
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
struktuře	struktura	k1gFnSc6	struktura
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
přípravy	příprava	k1gFnSc2	příprava
derivátu	derivát	k1gInSc2	derivát
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
atomu	atom	k1gInSc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
navázaných	navázaný	k2eAgInPc2d1	navázaný
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
atomu	atom	k1gInSc6	atom
uhlíku	uhlík	k1gInSc2	uhlík
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
uhlík	uhlík	k1gInSc4	uhlík
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
primární	primární	k2eAgFnSc7d1	primární
(	(	kIx(	(
<g/>
3	[number]	k4	3
navázané	navázaný	k2eAgInPc1d1	navázaný
atomy	atom	k1gInPc1	atom
H	H	kA	H
-	-	kIx~	-
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgMnSc1d1	sekundární
(	(	kIx(	(
<g/>
2	[number]	k4	2
atomy	atom	k1gInPc7	atom
H	H	kA	H
-	-	kIx~	-
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
R	R	kA	R
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
terciární	terciární	k2eAgFnSc1d1	terciární
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
atom	atom	k1gInSc1	atom
H	H	kA	H
-	-	kIx~	-
CHR	chr	k0	chr
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvartérní	kvartérní	k2eAgFnSc1d1	kvartérní
(	(	kIx(	(
<g/>
žádný	žádný	k3yNgInSc4	žádný
atom	atom	k1gInSc4	atom
H	H	kA	H
-	-	kIx~	-
CR	cr	k0	cr
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
atomu	atom	k1gInSc2	atom
C	C	kA	C
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
svojí	svůj	k3xOyFgFnSc7	svůj
reaktivitou	reaktivita	k1gFnSc7	reaktivita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
radikálové	radikálový	k2eAgFnSc6d1	radikálová
substituci	substituce	k1gFnSc6	substituce
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
terciární	terciární	k2eAgInSc1d1	terciární
uhlík	uhlík	k1gInSc1	uhlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
vazeb	vazba	k1gFnPc2	vazba
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vazby	vazba	k1gFnPc4	vazba
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
podívat	podívat	k5eAaPmF	podívat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Násobnost	násobnost	k1gFnSc1	násobnost
-	-	kIx~	-
vazby	vazba	k1gFnPc1	vazba
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
,	,	kIx,	,
dvojné	dvojný	k2eAgInPc1d1	dvojný
nebo	nebo	k8xC	nebo
trojné	trojný	k2eAgInPc1d1	trojný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polarita	polarita	k1gFnSc1	polarita
-	-	kIx~	-
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
mezi	mezi	k7c7	mezi
atomem	atom	k1gInSc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
vazba	vazba	k1gFnSc1	vazba
kovalentní	kovalentní	k2eAgFnSc1d1	kovalentní
nepolární	polární	k2eNgFnSc1d1	nepolární
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
atomem	atom	k1gInSc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
elektronegativního	elektronegativní	k2eAgInSc2d1	elektronegativní
prvku	prvek	k1gInSc2	prvek
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
halogen	halogen	k1gInSc1	halogen
apod.	apod.	kA	apod.
vzniká	vznikat	k5eAaImIp3nS	vznikat
kovalentní	kovalentní	k2eAgFnSc1d1	kovalentní
polární	polární	k2eAgFnSc1d1	polární
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
většinou	většinou	k6eAd1	většinou
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
dané	daný	k2eAgFnSc2d1	daná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
<g/>
Chemická	chemický	k2eAgFnSc1d1	chemická
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
svojí	svůj	k3xOyFgFnSc7	svůj
</s>
</p>
<p>
<s>
délkou	délka	k1gFnSc7	délka
–	–	k?	–
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
násobností	násobnost	k1gFnSc7	násobnost
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
vazby	vazba	k1gFnSc2	vazba
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
</s>
</p>
<p>
<s>
energii	energie	k1gFnSc4	energie
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
vazby	vazba	k1gFnSc2	vazba
(	(	kIx(	(
<g/>
disociační	disociační	k2eAgFnSc2d1	disociační
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
–	–	k?	–
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
násobností	násobnost	k1gFnSc7	násobnost
stoupá	stoupat	k5eAaImIp3nS	stoupat
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Organická	organický	k2eAgFnSc1d1	organická
látka	látka	k1gFnSc1	látka
</s>
</p>
