<s>
Organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
čistá	čistý	k2eAgFnSc1d1	čistá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
molekuly	molekula	k1gFnPc1	molekula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
