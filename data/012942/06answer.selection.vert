<s>
Obloha	obloha	k1gFnSc1	obloha
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
molekuly	molekula	k1gFnPc1	molekula
vzduchu	vzduch	k1gInSc2	vzduch
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
proti	proti	k7c3	proti
očím	oko	k1gNnPc3	oko
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
nejvíce	nejvíce	k6eAd1	nejvíce
právě	právě	k6eAd1	právě
modrou	modrý	k2eAgFnSc7d1	modrá
<g/>
.	.	kIx.	.
</s>
