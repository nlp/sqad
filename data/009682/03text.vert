<p>
<s>
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mara	k1gFnSc1	Mara
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc4d1	vodní
dílo	dílo	k1gNnSc4	dílo
u	u	k7c2	u
Liptovského	liptovský	k2eAgMnSc2d1	liptovský
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Váh	Váh	k1gInSc4	Váh
pod	pod	k7c7	pod
Tatrami	Tatra	k1gFnPc7	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
leží	ležet	k5eAaImIp3nS	ležet
přesně	přesně	k6eAd1	přesně
mezi	mezi	k7c7	mezi
Liptovskou	liptovský	k2eAgFnSc7d1	Liptovská
Teplou	Teplá	k1gFnSc7	Teplá
a	a	k8xC	a
Liptovským	liptovský	k2eAgInSc7d1	liptovský
Mikulášem	mikuláš	k1gInSc7	mikuláš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
;	;	kIx,	;
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
její	její	k3xOp3gFnSc2	její
výstavby	výstavba	k1gFnSc2	výstavba
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
měst	město	k1gNnPc2	město
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Váhu	Váh	k1gInSc2	Váh
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
.	.	kIx.	.
hráz	hráz	k1gFnSc1	hráz
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
53	[number]	k4	53
m	m	kA	m
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
nádrže	nádrž	k1gFnSc2	nádrž
je	být	k5eAaImIp3nS	být
45	[number]	k4	45
m	m	kA	m
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2	[number]	k4	2
700	[number]	k4	700
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objemem	objem	k1gInSc7	objem
vody	voda	k1gFnSc2	voda
360	[number]	k4	360
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
je	být	k5eAaImIp3nS	být
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mar	k2eAgFnSc1d1	Mara
největší	veliký	k2eAgFnSc7d3	veliký
nádrží	nádrž	k1gFnSc7	nádrž
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Váh	Váh	k1gInSc4	Váh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
bylo	být	k5eAaImAgNnS	být
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
7	[number]	k4	7
liptovských	liptovský	k2eAgFnPc2d1	Liptovská
vesnic	vesnice	k1gFnPc2	vesnice
úplně	úplně	k6eAd1	úplně
(	(	kIx(	(
<g/>
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mara	k1gFnSc1	Mara
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Stará	starý	k2eAgFnSc1d1	stará
<g/>
)	)	kIx)	)
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Sielnica	Sielnica	k1gFnSc1	Sielnica
<g/>
,	,	kIx,	,
Paludza	Paludza	k1gFnSc1	Paludza
<g/>
,	,	kIx,	,
Parížovce	Parížovec	k1gInPc1	Parížovec
<g/>
,	,	kIx,	,
Sokolče	sokolče	k1gNnSc1	sokolče
<g/>
,	,	kIx,	,
Vrbie	Vrbie	k1gFnSc1	Vrbie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
pak	pak	k6eAd1	pak
částečně	částečně	k6eAd1	částečně
(	(	kIx(	(
<g/>
Bobrovník	Bobrovník	k1gMnSc1	Bobrovník
<g/>
,	,	kIx,	,
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Trnovec	Trnovec	k1gMnSc1	Trnovec
<g/>
,	,	kIx,	,
Benice	Benice	k1gFnSc1	Benice
-	-	kIx~	-
část	část	k1gFnSc1	část
Čemice	Čemice	k1gFnSc2	Čemice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
nádrže	nádrž	k1gFnPc1	nádrž
nacházejí	nacházet	k5eAaImIp3nP	nacházet
velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgNnPc4d1	známé
rekreační	rekreační	k2eAgNnPc4d1	rekreační
centra	centrum	k1gNnPc4	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poloostrov	poloostrov	k1gInSc1	poloostrov
Ratkovo	Ratkův	k2eAgNnSc1d1	Ratkovo
s	s	k7c7	s
přilehlými	přilehlý	k2eAgFnPc7d1	přilehlá
vodami	voda	k1gFnPc7	voda
je	být	k5eAaImIp3nS	být
chráněným	chráněný	k2eAgNnSc7d1	chráněné
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historická	historický	k2eAgFnSc1d1	historická
osada	osada	k1gFnSc1	osada
==	==	k?	==
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
přehradní	přehradní	k2eAgFnSc7d1	přehradní
zdí	zeď	k1gFnSc7	zeď
Liptovské	liptovský	k2eAgFnSc2d1	Liptovská
Mary	Mary	k1gFnSc2	Mary
<g/>
,	,	kIx,	,
2	[number]	k4	2
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Bobrovník	Bobrovník	k1gInSc1	Bobrovník
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
úpatí	úpatí	k1gNnSc6	úpatí
vrchu	vrch	k1gInSc2	vrch
Úložisko	Úložisko	k1gNnSc1	Úložisko
(	(	kIx(	(
<g/>
741,7	[number]	k4	741,7
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
sa	sa	k?	sa
nachází	nacházet	k5eAaImIp3nS	nacházet
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
Havránok	Havránok	k1gInSc4	Havránok
náležející	náležející	k2eAgInSc1d1	náležející
púchovské	púchovský	k2eAgFnSc3d1	Púchovská
kultuře	kultura	k1gFnSc3	kultura
doby	doba	k1gFnSc2	doba
laténské	laténský	k2eAgNnSc1d1	laténské
-	-	kIx~	-
opevnění	opevnění	k1gNnSc1	opevnění
osady	osada	k1gFnSc2	osada
Kotinů	Kotin	k1gInPc2	Kotin
s	s	k7c7	s
druidskou	druidský	k2eAgFnSc7d1	druidská
svatyní	svatyně	k1gFnSc7	svatyně
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
i	i	k8xC	i
archeologický	archeologický	k2eAgInSc1d1	archeologický
skanzen	skanzen	k1gInSc1	skanzen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Panorama	panorama	k1gNnSc1	panorama
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mara	k1gFnSc1	Mara
(	(	kIx(	(
<g/>
priehrada	priehrada	k1gFnSc1	priehrada
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mara	k1gFnSc1	Mara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mara	k1gFnSc1	Mara
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Liptovská	liptovský	k2eAgFnSc1d1	Liptovská
Mara	Mara	k1gFnSc1	Mara
<g/>
:	:	kIx,	:
kostelík	kostelík	k1gInSc1	kostelík
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
