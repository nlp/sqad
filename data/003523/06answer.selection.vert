<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
mezi	mezi	k7c4	mezi
Světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
