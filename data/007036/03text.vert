<s>
Müsli	müsli	k1gNnSc1	müsli
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Birchermüesli	Birchermüesli	k1gFnSc1	Birchermüesli
nebo	nebo	k8xC	nebo
Müesli	Müesli	k1gFnSc1	Müesli
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
Müsli	müsli	k1gNnSc1	müsli
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
̥	̥	k?	̥
<g/>
li	li	k8xS	li
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pokrmu	pokrm	k1gInSc2	pokrm
z	z	k7c2	z
obilnin	obilnina	k1gFnPc2	obilnina
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
jako	jako	k8xS	jako
snídaně	snídaně	k1gFnSc1	snídaně
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tepelně	tepelně	k6eAd1	tepelně
neupravených	upravený	k2eNgFnPc2d1	neupravená
ovesných	ovesný	k2eAgFnPc2d1	ovesná
vloček	vločka	k1gFnPc2	vločka
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
je	on	k3xPp3gInPc4	on
jedí	jíst	k5eAaImIp3nP	jíst
též	též	k9	též
jako	jako	k9	jako
lehkou	lehký	k2eAgFnSc4d1	lehká
večeři	večeře	k1gFnSc4	večeře
<g/>
;	;	kIx,	;
pojmem	pojem	k1gInSc7	pojem
Birchermüesli	Birchermüesli	k1gFnSc2	Birchermüesli
complet	complet	k5eAaPmF	complet
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozumí	rozumět	k5eAaImIp3nS	rozumět
müsli	müsli	k1gNnSc1	müsli
<g/>
,	,	kIx,	,
chléb	chléb	k1gInSc1	chléb
s	s	k7c7	s
máslem	máslo	k1gNnSc7	máslo
a	a	k8xC	a
káva	káva	k1gFnSc1	káva
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vlákninu	vláknina	k1gFnSc4	vláknina
Müsli	müsli	k1gNnSc2	müsli
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
hlavních	hlavní	k2eAgFnPc6d1	hlavní
podobách	podoba	k1gFnPc6	podoba
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
a	a	k8xC	a
suché	suchý	k2eAgFnPc1d1	suchá
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
druh	druh	k1gInSc1	druh
müsli	müsli	k1gNnSc2	müsli
je	být	k5eAaImIp3nS	být
sypká	sypký	k2eAgFnSc1d1	sypká
směs	směs	k1gFnSc1	směs
zejména	zejména	k9	zejména
ovesných	ovesný	k2eAgFnPc2d1	ovesná
vloček	vločka	k1gFnPc2	vločka
a	a	k8xC	a
kousků	kousek	k1gInPc2	kousek
různého	různý	k2eAgNnSc2d1	různé
sušeného	sušený	k2eAgNnSc2d1	sušené
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
ořechů	ořech	k1gInPc2	ořech
a	a	k8xC	a
semínek	semínko	k1gNnPc2	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
obilné	obilný	k2eAgFnPc4d1	obilná
vločky	vločka	k1gFnPc4	vločka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pšeničné	pšeničný	k2eAgFnPc1d1	pšeničná
či	či	k8xC	či
žitné	žitný	k2eAgFnPc1d1	Žitná
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgNnSc1d1	Suché
müsli	müsli	k1gNnSc1	müsli
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
běžně	běžně	k6eAd1	běžně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
průmyslově	průmyslově	k6eAd1	průmyslově
balených	balený	k2eAgFnPc2d1	balená
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
ráda	rád	k2eAgFnSc1d1	ráda
připravuje	připravovat	k5eAaImIp3nS	připravovat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
müsli	müsli	k1gNnSc2	müsli
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
s	s	k7c7	s
medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
různým	různý	k2eAgNnSc7d1	různé
kořením	koření	k1gNnSc7	koření
či	či	k8xC	či
čokoládou	čokoláda	k1gFnSc7	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgNnSc1d1	Suché
müsli	müsli	k1gNnSc1	müsli
lze	lze	k6eAd1	lze
pohodlně	pohodlně	k6eAd1	pohodlně
skladovat	skladovat	k5eAaImF	skladovat
po	po	k7c4	po
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
příprava	příprava	k1gFnSc1	příprava
je	být	k5eAaImIp3nS	být
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
je	být	k5eAaImIp3nS	být
smísit	smísit	k5eAaPmF	smísit
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
jogurtem	jogurt	k1gInSc7	jogurt
či	či	k8xC	či
ovocnou	ovocný	k2eAgFnSc7d1	ovocná
šťávou	šťáva	k1gFnSc7	šťáva
a	a	k8xC	a
případně	případně	k6eAd1	případně
kousky	kousek	k1gInPc1	kousek
čerstvého	čerstvý	k2eAgNnSc2d1	čerstvé
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
müsli	müsli	k1gNnSc2	müsli
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
čerstvě	čerstvě	k6eAd1	čerstvě
připravená	připravený	k2eAgFnSc1d1	připravená
směs	směs	k1gFnSc1	směs
ovesných	ovesný	k2eAgFnPc2d1	ovesná
vloček	vločka	k1gFnPc2	vločka
<g/>
,	,	kIx,	,
předem	předem	k6eAd1	předem
namočených	namočený	k2eAgMnPc2d1	namočený
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
ovocné	ovocný	k2eAgFnSc6d1	ovocná
šťávě	šťáva	k1gFnSc6	šťáva
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jemno	jemno	k1gNnSc4	jemno
nastrouhaného	nastrouhaný	k2eAgNnSc2d1	nastrouhané
nebo	nebo	k8xC	nebo
rozmixovaného	rozmixovaný	k2eAgNnSc2d1	rozmixované
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
obvyklé	obvyklý	k2eAgFnPc4d1	obvyklá
přísady	přísada	k1gFnPc4	přísada
patří	patřit	k5eAaImIp3nS	patřit
na	na	k7c4	na
kousky	kousek	k1gInPc4	kousek
nakrájené	nakrájený	k2eAgInPc4d1	nakrájený
nebo	nebo	k8xC	nebo
strouhané	strouhaný	k2eAgNnSc4d1	strouhané
čerstvé	čerstvý	k2eAgNnSc4d1	čerstvé
ovoce	ovoce	k1gNnSc4	ovoce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInPc1d1	lesní
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
hrozny	hrozen	k1gInPc1	hrozen
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sušené	sušený	k2eAgNnSc4d1	sušené
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jogurt	jogurt	k1gInSc1	jogurt
<g/>
,	,	kIx,	,
smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
kondenzované	kondenzovaný	k2eAgNnSc1d1	kondenzované
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
tvaroh	tvaroh	k1gInSc1	tvaroh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
citronová	citronový	k2eAgFnSc1d1	citronová
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
,	,	kIx,	,
strouhané	strouhaný	k2eAgInPc1d1	strouhaný
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
semínka	semínko	k1gNnPc1	semínko
<g/>
,	,	kIx,	,
koření	kořenit	k5eAaImIp3nP	kořenit
(	(	kIx(	(
<g/>
především	především	k9	především
skořice	skořice	k1gFnSc1	skořice
<g/>
)	)	kIx)	)
a	a	k8xC	a
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
čerstvého	čerstvý	k2eAgNnSc2d1	čerstvé
müsli	müsli	k1gNnSc2	müsli
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
sráží	srážet	k5eAaImIp3nS	srážet
působením	působení	k1gNnSc7	působení
kyselin	kyselina	k1gFnPc2	kyselina
z	z	k7c2	z
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Müesli	Müesnout	k5eAaPmAgMnP	Müesnout
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
Maximilian	Maximilian	k1gMnSc1	Maximilian
Bircher-Benner	Bircher-Benner	k1gMnSc1	Bircher-Benner
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strava	strava	k1gFnSc1	strava
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
čerstvé	čerstvý	k2eAgNnSc4d1	čerstvé
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
tvořila	tvořit	k5eAaImAgFnS	tvořit
stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
část	část	k1gFnSc1	část
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
byl	být	k5eAaImAgInS	být
podobně	podobně	k6eAd1	podobně
"	"	kIx"	"
<g/>
podivný	podivný	k2eAgInSc1d1	podivný
pokrm	pokrm	k1gInSc1	pokrm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jemu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
podali	podat	k5eAaPmAgMnP	podat
během	během	k7c2	během
túry	túra	k1gFnSc2	túra
ve	v	k7c6	v
švýcarských	švýcarský	k2eAgFnPc6d1	švýcarská
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Bircher-Benner	Bircher-Benner	k1gMnSc1	Bircher-Benner
sám	sám	k3xTgMnSc1	sám
jídlo	jídlo	k1gNnSc4	jídlo
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Spys	Spys	k1gInSc1	Spys
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
podoba	podoba	k1gFnSc1	podoba
německého	německý	k2eAgInSc2d1	německý
"	"	kIx"	"
<g/>
die	die	k?	die
Speise	Speise	k1gFnSc1	Speise
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
pokrm	pokrm	k1gInSc1	pokrm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
Müesli	Müesli	k1gFnSc2	Müesli
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
švýcarského	švýcarský	k2eAgNnSc2d1	švýcarské
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Mues	Mues	k1gInSc1	Mues
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
Mus	Musa	k1gFnPc2	Musa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kaše	kaše	k1gFnSc2	kaše
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
müsli	müsli	k1gNnSc1	müsli
oblíbeno	oblíbit	k5eAaPmNgNnS	oblíbit
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rostoucího	rostoucí	k2eAgInSc2d1	rostoucí
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
zdravou	zdravý	k2eAgFnSc4d1	zdravá
a	a	k8xC	a
vegetariánskou	vegetariánský	k2eAgFnSc4d1	vegetariánská
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
<s>
Aşure	Aşur	k1gMnSc5	Aşur
</s>
