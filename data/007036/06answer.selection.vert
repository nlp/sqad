<s>
Müesli	Müesnout	k5eAaPmAgMnP	Müesnout
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
Maximilian	Maximilian	k1gMnSc1	Maximilian
Bircher-Benner	Bircher-Benner	k1gMnSc1	Bircher-Benner
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strava	strava	k1gFnSc1	strava
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
čerstvé	čerstvý	k2eAgNnSc4d1	čerstvé
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
tvořila	tvořit	k5eAaImAgFnS	tvořit
stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
část	část	k1gFnSc1	část
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
