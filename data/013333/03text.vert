<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1640	[number]	k4	1640
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1705	[number]	k4	1705
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
původem	původ	k1gInSc7	původ
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
syn	syn	k1gMnSc1	syn
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
Španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1658	[number]	k4	1658
<g/>
–	–	k?	–
<g/>
1705	[number]	k4	1705
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1657	[number]	k4	1657
<g/>
–	–	k?	–
<g/>
1705	[number]	k4	1705
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Leopoldova	Leopoldův	k2eAgFnSc1d1	Leopoldova
výchova	výchova	k1gFnSc1	výchova
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nástupnické	nástupnický	k2eAgFnSc6d1	nástupnická
linii	linie	k1gFnSc6	linie
byl	být	k5eAaImAgMnS	být
Leopold	Leopold	k1gMnSc1	Leopold
až	až	k9	až
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gMnSc4	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
měl	mít	k5eAaImAgMnS	mít
jako	jako	k9	jako
budoucí	budoucí	k2eAgMnSc1d1	budoucí
duchovní	duchovní	k1gMnSc1	duchovní
následovat	následovat	k5eAaImF	následovat
kariéru	kariéra	k1gFnSc4	kariéra
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
<g/>
,	,	kIx,	,
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Leopolda	Leopold	k1gMnSc2	Leopold
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
biskupem	biskup	k1gMnSc7	biskup
a	a	k8xC	a
velmistrem	velmistr	k1gMnSc7	velmistr
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1654	[number]	k4	1654
však	však	k9	však
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
k	k	k7c3	k
samostatné	samostatný	k2eAgFnSc3d1	samostatná
vládě	vláda	k1gFnSc3	vláda
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
Leopolda	Leopold	k1gMnSc4	Leopold
se	se	k3xPyFc4	se
rázem	rázem	k6eAd1	rázem
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgInSc3	ten
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
zaměření	zaměření	k1gNnSc2	zaměření
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
mladého	mladý	k2eAgMnSc2d1	mladý
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
hodiny	hodina	k1gFnPc1	hodina
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
diplomacie	diplomacie	k1gFnSc2	diplomacie
a	a	k8xC	a
ustoupit	ustoupit	k5eAaPmF	ustoupit
musely	muset	k5eAaImAgFnP	muset
teologie	teologie	k1gFnSc1	teologie
<g/>
,	,	kIx,	,
morálka	morálka	k1gFnSc1	morálka
a	a	k8xC	a
církevní	církevní	k2eAgFnSc1d1	církevní
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Leopoldovy	Leopoldův	k2eAgFnSc2d1	Leopoldova
korunovace	korunovace	k1gFnSc2	korunovace
==	==	k?	==
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zajistil	zajistit	k5eAaPmAgInS	zajistit
uherský	uherský	k2eAgInSc1d1	uherský
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
trůn	trůn	k1gInSc1	trůn
(	(	kIx(	(
<g/>
Leopoldova	Leopoldův	k2eAgFnSc1d1	Leopoldova
uherská	uherský	k2eAgFnSc1d1	uherská
korunovace	korunovace	k1gFnSc1	korunovace
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1655	[number]	k4	1655
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1656	[number]	k4	1656
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neúspěchem	neúspěch	k1gInSc7	neúspěch
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
Ferdinandova	Ferdinandův	k2eAgFnSc1d1	Ferdinandova
snaha	snaha	k1gFnSc1	snaha
zajistit	zajistit	k5eAaPmF	zajistit
Leopoldovi	Leopold	k1gMnSc3	Leopold
korunu	koruna	k1gFnSc4	koruna
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
a	a	k8xC	a
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
mu	on	k3xPp3gMnSc3	on
tak	tak	k9	tak
nástupnictví	nástupnictví	k1gNnSc3	nástupnictví
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
hodnosti	hodnost	k1gFnSc6	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
především	především	k9	především
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
vlivu	vliv	k1gInSc2	vliv
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
protivníky	protivník	k1gMnPc7	protivník
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Švédové	Švéd	k1gMnPc1	Švéd
a	a	k8xC	a
duchovní	duchovní	k2eAgMnPc1d1	duchovní
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápas	zápas	k1gInSc1	zápas
o	o	k7c4	o
římský	římský	k2eAgInSc4d1	římský
trůn	trůn	k1gInSc4	trůn
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
letech	let	k1gInPc6	let
1656	[number]	k4	1656
<g/>
–	–	k?	–
<g/>
1658	[number]	k4	1658
byl	být	k5eAaImAgMnS	být
Leopoldovou	Leopoldová	k1gFnSc4	Leopoldová
první	první	k4xOgFnSc7	první
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
akcí	akce	k1gFnSc7	akce
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skoro	skoro	k6eAd1	skoro
ročním	roční	k2eAgNnSc6d1	roční
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
vůbec	vůbec	k9	vůbec
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
volba	volba	k1gFnSc1	volba
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
skončil	skončit	k5eAaPmAgMnS	skončit
jeho	jeho	k3xOp3gNnSc7	jeho
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
sice	sice	k8xC	sice
nenašli	najít	k5eNaPmAgMnP	najít
protikandidáta	protikandidát	k1gMnSc4	protikandidát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovlivňovali	ovlivňovat	k5eAaImAgMnP	ovlivňovat
na	na	k7c6	na
dvorech	dvůr	k1gInPc6	dvůr
rýnských	rýnský	k2eAgMnPc2d1	rýnský
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
sestavení	sestavení	k1gNnSc2	sestavení
Leopoldovy	Leopoldův	k2eAgFnSc2d1	Leopoldova
volební	volební	k2eAgFnSc2d1	volební
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
například	například	k6eAd1	například
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Leopoldovi	Leopold	k1gMnSc3	Leopold
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
podporu	podpora	k1gFnSc4	podpora
Španělů	Španěl	k1gMnPc2	Španěl
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgInSc7	ten
musel	muset	k5eAaImAgMnS	muset
nakonec	nakonec	k6eAd1	nakonec
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
souhlasit	souhlasit	k5eAaImF	souhlasit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
si	se	k3xPyFc3	se
Francie	Francie	k1gFnSc1	Francie
svou	svůj	k3xOyFgFnSc7	svůj
spoluúčastí	spoluúčast	k1gFnSc7	spoluúčast
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
tzv.	tzv.	kA	tzv.
Rýnského	rýnský	k2eAgInSc2d1	rýnský
spolku	spolek	k1gInSc2	spolek
zabezpečila	zabezpečit	k5eAaPmAgFnS	zabezpečit
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
záležitosti	záležitost	k1gFnSc6	záležitost
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1658	[number]	k4	1658
byl	být	k5eAaImAgMnS	být
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
nakonec	nakonec	k6eAd1	nakonec
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
dómu	dóm	k1gInSc6	dóm
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
korunován	korunován	k2eAgInSc4d1	korunován
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
nepříliš	příliš	k6eNd1	příliš
zběhlý	zběhlý	k2eAgMnSc1d1	zběhlý
Leopold	Leopold	k1gMnSc1	Leopold
zprvu	zprvu	k6eAd1	zprvu
přenechal	přenechat	k5eAaPmAgMnS	přenechat
státní	státní	k2eAgFnPc4d1	státní
záležitosti	záležitost	k1gFnPc4	záležitost
zkušeným	zkušený	k2eAgMnPc3d1	zkušený
poradcům	poradce	k1gMnPc3	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
prvním	první	k4xOgMnSc7	první
ministrem	ministr	k1gMnSc7	ministr
svého	svůj	k3xOyFgInSc2	svůj
někdejšího	někdejší	k2eAgInSc2d1	někdejší
vychovatele	vychovatel	k1gMnPc4	vychovatel
Porziu	Porzius	k1gMnSc6	Porzius
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
Johann	Johann	k1gMnSc1	Johann
Weikhard	Weikhard	k1gMnSc1	Weikhard
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Auerspergu	Auersperg	k1gInSc2	Auersperg
(	(	kIx(	(
<g/>
1615	[number]	k4	1615
<g/>
–	–	k?	–
<g/>
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
dvorní	dvorní	k2eAgFnSc2d1	dvorní
rady	rada	k1gFnSc2	rada
Václav	Václav	k1gMnSc1	Václav
Eusebius	Eusebius	k1gMnSc1	Eusebius
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Auersperg	Auersperg	k1gInSc1	Auersperg
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
ministr	ministr	k1gMnSc1	ministr
sesazen	sesadit	k5eAaPmNgMnS	sesadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1674	[number]	k4	1674
byl	být	k5eAaImAgMnS	být
také	také	k9	také
kníže	kníže	k1gMnSc1	kníže
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
zbaven	zbavit	k5eAaPmNgMnS	zbavit
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
totiž	totiž	k9	totiž
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
císaře	císař	k1gMnSc2	císař
budovali	budovat	k5eAaImAgMnP	budovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
určoval	určovat	k5eAaImAgMnS	určovat
císař	císař	k1gMnSc1	císař
směr	směr	k1gInSc4	směr
politiky	politika	k1gFnSc2	politika
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
ministra	ministr	k1gMnSc2	ministr
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Kancléř	kancléř	k1gMnSc1	kancléř
Johann	Johann	k1gMnSc1	Johann
Paul	Paul	k1gMnSc1	Paul
Hocher	Hochra	k1gFnPc2	Hochra
(	(	kIx(	(
<g/>
1616	[number]	k4	1616
<g/>
–	–	k?	–
<g/>
1683	[number]	k4	1683
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
nástupci	nástupce	k1gMnPc1	nástupce
byli	být	k5eAaImAgMnP	být
zástupci	zástupce	k1gMnPc1	zástupce
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
diplomatický	diplomatický	k2eAgMnSc1d1	diplomatický
pomocník	pomocník	k1gMnSc1	pomocník
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
byl	být	k5eAaImAgInS	být
Franz	Franz	k1gInSc1	Franz
von	von	k1gInSc1	von
Lisola	Lisola	k1gFnSc1	Lisola
<g/>
.	.	kIx.	.
</s>
<s>
Neustálým	neustálý	k2eAgInSc7d1	neustálý
problémem	problém	k1gInSc7	problém
císařství	císařství	k1gNnSc2	císařství
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
dvorské	dvorský	k2eAgFnSc2d1	dvorská
komory	komora	k1gFnSc2	komora
Jiří	Jiří	k1gMnSc1	Jiří
Ludvík	Ludvík	k1gMnSc1	Ludvík
ze	z	k7c2	z
Sinzendorfu	Sinzendorf	k1gInSc2	Sinzendorf
byl	být	k5eAaImAgMnS	být
zbaven	zbaven	k2eAgMnSc1d1	zbaven
funkce	funkce	k1gFnPc4	funkce
kvůli	kvůli	k7c3	kvůli
zpronevěře	zpronevěra	k1gFnSc3	zpronevěra
<g/>
.	.	kIx.	.
</s>
<s>
Stabilizace	stabilizace	k1gFnPc4	stabilizace
financí	finance	k1gFnPc2	finance
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
za	za	k7c4	za
Gundakera	Gundaker	k1gMnSc4	Gundaker
hraběte	hrabě	k1gMnSc4	hrabě
Starhemberga	Starhemberg	k1gMnSc4	Starhemberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říšské	říšský	k2eAgFnSc6d1	říšská
politice	politika	k1gFnSc6	politika
hráli	hrát	k5eAaImAgMnP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
říšský	říšský	k2eAgMnSc1d1	říšský
vicekancléř	vicekancléř	k1gMnSc1	vicekancléř
Leopold	Leopold	k1gMnSc1	Leopold
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc4	von
Königsegg-Rothenfels	Königsegg-Rothenfelsa	k1gFnPc2	Königsegg-Rothenfelsa
a	a	k8xC	a
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
Wilderich	Wilderich	k1gInSc1	Wilderich
von	von	k1gInSc1	von
Walderdorff	Walderdorff	k1gInSc1	Walderdorff
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Tajná	tajný	k2eAgFnSc1d1	tajná
rada	rada	k1gFnSc1	rada
kvůli	kvůli	k7c3	kvůli
značnému	značný	k2eAgInSc3d1	značný
počtu	počet	k1gInSc3	počet
členů	člen	k1gMnPc2	člen
stala	stát	k5eAaPmAgFnS	stát
víceméně	víceméně	k9	víceméně
nefunkční	funkční	k2eNgFnSc1d1	nefunkční
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
Leopold	Leopold	k1gMnSc1	Leopold
zřídit	zřídit	k5eAaPmF	zřídit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
převážně	převážně	k6eAd1	převážně
vněpolitické	vněpolitický	k2eAgNnSc4d1	vněpolitický
poradní	poradní	k2eAgNnSc4d1	poradní
grémium	grémium	k1gNnSc4	grémium
<g/>
,	,	kIx,	,
tajnou	tajný	k2eAgFnSc4d1	tajná
konferenci	konference	k1gFnSc4	konference
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Leopolda	Leopold	k1gMnSc2	Leopold
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
císařských	císařský	k2eAgNnPc2d1	císařské
vyslanectví	vyslanectví	k1gNnPc2	vyslanectví
u	u	k7c2	u
dvorů	dvůr	k1gInPc2	dvůr
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
říšských	říšský	k2eAgInPc2d1	říšský
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
říšských	říšský	k2eAgInPc2d1	říšský
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
hráli	hrát	k5eAaImAgMnP	hrát
císařský	císařský	k2eAgMnSc1d1	císařský
vrchní	vrchní	k1gMnSc1	vrchní
komisař	komisař	k1gMnSc1	komisař
a	a	k8xC	a
rakouské	rakouský	k2eAgNnSc1d1	rakouské
vyslanectví	vyslanectví	k1gNnSc1	vyslanectví
při	při	k7c6	při
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgNnSc1d1	pozitivní
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Říšská	říšský	k2eAgFnSc1d1	říšská
dvorská	dvorský	k2eAgFnSc1d1	dvorská
kancelář	kancelář	k1gFnSc1	kancelář
a	a	k8xC	a
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
dvorská	dvorský	k2eAgFnSc1d1	dvorská
kancelář	kancelář	k1gFnSc1	kancelář
spolu	spolu	k6eAd1	spolu
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
spolupracovaly	spolupracovat	k5eAaImAgInP	spolupracovat
a	a	k8xC	a
neztrácely	ztrácet	k5eNaImAgInP	ztrácet
tudíž	tudíž	k8xC	tudíž
čas	čas	k1gInSc1	čas
boji	boj	k1gInSc3	boj
o	o	k7c4	o
kompetence	kompetence	k1gFnPc4	kompetence
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
habsbursko-turecké	habsburskourecký	k2eAgFnSc2d1	habsbursko-turecký
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
na	na	k7c4	na
slabě	slabě	k6eAd1	slabě
bráněnou	bráněný	k2eAgFnSc4d1	bráněná
Moravu	Morava	k1gFnSc4	Morava
Turci	Turek	k1gMnPc1	Turek
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
krymsko-tatarští	krymskoatarský	k2eAgMnPc1d1	krymsko-tatarský
spojenci	spojenec	k1gMnPc1	spojenec
a	a	k8xC	a
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
tehdy	tehdy	k6eAd1	tehdy
odvlekli	odvléct	k5eAaPmAgMnP	odvléct
tisíce	tisíc	k4xCgInPc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
===	===	k?	===
</s>
</p>
<p>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
byly	být	k5eAaImAgFnP	být
zatíženy	zatížit	k5eAaPmNgInP	zatížit
vysokými	vysoký	k2eAgFnPc7d1	vysoká
daněmi	daň	k1gFnPc7	daň
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
přecházely	přecházet	k5eAaImAgFnP	přecházet
z	z	k7c2	z
majitelů	majitel	k1gMnPc2	majitel
půdy	půda	k1gFnSc2	půda
na	na	k7c4	na
sedláky	sedlák	k1gMnPc4	sedlák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
udeřily	udeřit	k5eAaPmAgFnP	udeřit
epidemie	epidemie	k1gFnPc1	epidemie
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
rekatolizační	rekatolizační	k2eAgFnSc1d1	rekatolizační
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1679	[number]	k4	1679
přijel	přijet	k5eAaPmAgMnS	přijet
císař	císař	k1gMnSc1	císař
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgInS	uniknout
před	před	k7c7	před
morovou	morový	k2eAgFnSc7d1	morová
epidemií	epidemie	k1gFnSc7	epidemie
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
předáno	předat	k5eAaPmNgNnS	předat
mnoho	mnoho	k4c4	mnoho
proseb	prosba	k1gFnPc2	prosba
a	a	k8xC	a
stížností	stížnost	k1gFnPc2	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
císařově	císařův	k2eAgInSc6d1	císařův
odjezdu	odjezd	k1gInSc6	odjezd
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
stěžovatelů	stěžovatel	k1gMnPc2	stěžovatel
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1680	[number]	k4	1680
vyústily	vyústit	k5eAaPmAgFnP	vyústit
do	do	k7c2	do
velikého	veliký	k2eAgNnSc2d1	veliké
selského	selský	k2eAgNnSc2d1	selské
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zachvátilo	zachvátit	k5eAaPmAgNnS	zachvátit
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
násilí	násilí	k1gNnSc2	násilí
znovu	znovu	k6eAd1	znovu
nastolen	nastolen	k2eAgInSc1d1	nastolen
provizorní	provizorní	k2eAgInSc1d1	provizorní
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
účastníků	účastník	k1gMnPc2	účastník
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
nebo	nebo	k8xC	nebo
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
k	k	k7c3	k
nuceným	nucený	k2eAgInPc3d1	nucený
pracím	prací	k2eAgInPc3d1	prací
nebo	nebo	k8xC	nebo
žaláři	žalář	k1gInSc3	žalář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ovšem	ovšem	k9	ovšem
císař	císař	k1gMnSc1	císař
Leopold	Leopold	k1gMnSc1	Leopold
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
reagoval	reagovat	k5eAaBmAgMnS	reagovat
vydáním	vydání	k1gNnSc7	vydání
prvního	první	k4xOgInSc2	první
robotního	robotní	k2eAgInSc2d1	robotní
patentu	patent	k1gInSc2	patent
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
(	(	kIx(	(
<g/>
Pardubitzer	Pardubitzer	k1gMnSc1	Pardubitzer
Pragmatica	Pragmatica	k1gMnSc1	Pragmatica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
definoval	definovat	k5eAaBmAgMnS	definovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
poddanými	poddaná	k1gFnPc7	poddaná
a	a	k8xC	a
vrchností	vrchnost	k1gFnSc7	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
bylo	být	k5eAaImAgNnS	být
omezení	omezení	k1gNnSc1	omezení
zatížení	zatížení	k1gNnSc2	zatížení
robotou	robota	k1gFnSc7	robota
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
však	však	k9	však
byl	být	k5eAaImAgInS	být
českými	český	k2eAgMnPc7d1	český
pány	pan	k1gMnPc7	pan
často	často	k6eAd1	často
porušován	porušován	k2eAgInSc1d1	porušován
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
nebyla	být	k5eNaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
projevily	projevit	k5eAaPmAgInP	projevit
vysoké	vysoký	k2eAgInPc1d1	vysoký
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
v	v	k7c6	v
době	doba	k1gFnSc6	doba
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
značně	značně	k6eAd1	značně
přetěžovaly	přetěžovat	k5eAaImAgFnP	přetěžovat
rozpočet	rozpočet	k1gInSc4	rozpočet
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Leopolda	Leopolda	k1gFnSc1	Leopolda
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
vnějších	vnější	k2eAgInPc2d1	vnější
i	i	k8xC	i
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1678	[number]	k4	1678
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
čarodějnickým	čarodějnický	k2eAgInPc3d1	čarodějnický
procesům	proces	k1gInPc3	proces
na	na	k7c6	na
losinském	losinský	k2eAgNnSc6d1	losinské
panství	panství	k1gNnSc6	panství
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neustálé	neustálý	k2eAgFnPc1d1	neustálá
války	válka	k1gFnPc1	válka
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
jej	on	k3xPp3gInSc2	on
nutily	nutit	k5eAaImAgInP	nutit
udržovat	udržovat	k5eAaImF	udržovat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
opakovaně	opakovaně	k6eAd1	opakovaně
porazila	porazit	k5eAaPmAgFnS	porazit
Osmanské	osmanský	k2eAgInPc4d1	osmanský
Turky	turek	k1gInPc4	turek
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
(	(	kIx(	(
<g/>
1683	[number]	k4	1683
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
sérii	série	k1gFnSc6	série
dalších	další	k2eAgNnPc2d1	další
drtivých	drtivý	k2eAgNnPc2d1	drtivé
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
byly	být	k5eAaImAgFnP	být
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Nagyharsány	Nagyharsán	k2eAgInPc4d1	Nagyharsán
(	(	kIx(	(
<g/>
1687	[number]	k4	1687
<g/>
)	)	kIx)	)
a	a	k8xC	a
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zenty	Zenta	k1gFnSc2	Zenta
(	(	kIx(	(
<g/>
1697	[number]	k4	1697
<g/>
)	)	kIx)	)
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
problémů	problém	k1gInPc2	problém
monarchie	monarchie	k1gFnSc2	monarchie
byla	být	k5eAaImAgFnS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgFnP	být
obsazeny	obsadit	k5eAaPmNgInP	obsadit
Turky	turek	k1gInPc1	turek
<g/>
.	.	kIx.	.
</s>
<s>
Tamější	tamější	k2eAgFnSc1d1	tamější
šlechta	šlechta	k1gFnSc1	šlechta
během	během	k7c2	během
Leopoldovy	Leopoldův	k2eAgFnSc2d1	Leopoldova
vlády	vláda	k1gFnSc2	vláda
několikrát	několikrát	k6eAd1	několikrát
povstala	povstat	k5eAaPmAgFnS	povstat
a	a	k8xC	a
spojence	spojenec	k1gMnPc4	spojenec
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
sedmihradských	sedmihradský	k2eAgNnPc6d1	sedmihradské
knížatech	kníže	k1gNnPc6	kníže
a	a	k8xC	a
nezřídka	nezřídka	k6eAd1	nezřídka
také	také	k9	také
v	v	k7c6	v
Turcích	turek	k1gInPc6	turek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
nebyl	být	k5eNaImAgMnS	být
rozeným	rozený	k2eAgMnSc7d1	rozený
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nevedl	vést	k5eNaImAgMnS	vést
vojska	vojsko	k1gNnSc2	vojsko
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
věnoval	věnovat	k5eAaImAgInS	věnovat
vedení	vedení	k1gNnSc4	vedení
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
X.	X.	kA	X.
našel	najít	k5eAaPmAgMnS	najít
spojence	spojenec	k1gMnPc4	spojenec
v	v	k7c6	v
sedmihradském	sedmihradský	k2eAgNnSc6d1	sedmihradské
knížeti	kníže	k1gNnSc6wR	kníže
Jiřím	Jiří	k1gMnSc7	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákóczim	Rákóczim	k6eAd1	Rákóczim
<g/>
,	,	kIx,	,
odbojném	odbojný	k2eAgMnSc6d1	odbojný
vazalovi	vazal	k1gMnSc6	vazal
uherské	uherský	k2eAgFnSc2d1	uherská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Prozíravě	prozíravě	k6eAd1	prozíravě
přijal	přijmout	k5eAaPmAgMnS	přijmout
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
také	také	k9	také
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgMnSc2d1	savojský
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
předtím	předtím	k6eAd1	předtím
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
svými	svůj	k3xOyFgFnPc7	svůj
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
proporcemi	proporce	k1gFnPc7	proporce
nepůsobil	působit	k5eNaImAgMnS	působit
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
úspěchy	úspěch	k1gInPc7	úspěch
jako	jako	k8xS	jako
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
akceptovat	akceptovat	k5eAaBmF	akceptovat
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc2	jeho
nástupce	nástupce	k1gMnSc2	nástupce
Filipa	Filip	k1gMnSc2	Filip
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc7d1	Anjý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Karla	Karel	k1gMnSc4	Karel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
válkám	válka	k1gFnPc3	válka
o	o	k7c6	o
dědictví	dědictví	k1gNnSc6	dědictví
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
,	,	kIx,	,
největšímu	veliký	k2eAgInSc3d3	veliký
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
konfliktu	konflikt	k1gInSc3	konflikt
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
Leopold	Leopolda	k1gFnPc2	Leopolda
po	po	k7c6	po
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
určoval	určovat	k5eAaImAgMnS	určovat
směřování	směřování	k1gNnSc1	směřování
politiky	politika	k1gFnSc2	politika
víceméně	víceméně	k9	víceméně
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
válečné	válečný	k2eAgFnSc6d1	válečná
straně	strana	k1gFnSc6	strana
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kriegspartei	Kriegsparte	k1gMnPc1	Kriegsparte
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgMnSc2d1	savojský
a	a	k8xC	a
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
zatlačit	zatlačit	k5eAaPmF	zatlačit
Leopolda	Leopolda	k1gFnSc1	Leopolda
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gNnSc1	jeho
životní	životní	k2eAgNnSc1d1	životní
heslo	heslo	k1gNnSc1	heslo
znělo	znět	k5eAaImAgNnS	znět
<g/>
:	:	kIx,	:
consilio	consilio	k6eAd1	consilio
et	et	k?	et
industria	industrium	k1gNnSc2	industrium
=	=	kIx~	=
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
pílí	píle	k1gFnSc7	píle
[	[	kIx(	[
<g/>
k	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
cíli	cíl	k1gInSc3	cíl
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Střety	střet	k1gInPc1	střet
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
monarchie	monarchie	k1gFnSc1	monarchie
srážela	srážet	k5eAaImAgFnS	srážet
s	s	k7c7	s
rozpínavostí	rozpínavost	k1gFnSc7	rozpínavost
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
císař	císař	k1gMnSc1	císař
méně	málo	k6eAd2	málo
úspěchů	úspěch	k1gInPc2	úspěch
než	než	k8xS	než
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Monarchie	monarchie	k1gFnSc1	monarchie
se	se	k3xPyFc4	se
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
spojenci	spojenec	k1gMnPc1	spojenec
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
roku	rok	k1gInSc2	rok
1679	[number]	k4	1679
<g/>
,	,	kIx,	,
vyzněl	vyznět	k5eAaImAgInS	vyznět
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
územní	územní	k2eAgInSc4d1	územní
zisky	zisk	k1gInPc4	zisk
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
rakouských	rakouský	k2eAgMnPc2d1	rakouský
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
Devítiletá	devítiletý	k2eAgFnSc1d1	devítiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1688	[number]	k4	1688
<g/>
–	–	k?	–
<g/>
1697	[number]	k4	1697
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
válka	válka	k1gFnSc1	válka
Augšpurské	augšpurský	k2eAgFnSc2d1	augšpurská
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
falcké	falcký	k2eAgNnSc4d1	falcké
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
Velké	velký	k2eAgFnSc2d1	velká
aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
mír	mír	k1gInSc4	mír
z	z	k7c2	z
Rijkswijku	Rijkswijka	k1gFnSc4	Rijkswijka
(	(	kIx(	(
<g/>
1697	[number]	k4	1697
<g/>
)	)	kIx)	)
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Leopoldovi	Leopold	k1gMnSc3	Leopold
I.	I.	kA	I.
jeho	jeho	k3xOp3gFnSc7	jeho
územní	územní	k2eAgFnPc4d1	územní
ztráty	ztráta	k1gFnPc4	ztráta
z	z	k7c2	z
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
střetu	střet	k1gInSc2	střet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
boji	boj	k1gInSc3	boj
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
Leopoldovy	Leopoldův	k2eAgFnSc2d1	Leopoldova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
bratranec	bratranec	k1gMnSc1	bratranec
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Leopold	Leopold	k1gMnSc1	Leopold
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
akceptovat	akceptovat	k5eAaBmF	akceptovat
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc2	jeho
nástupce	nástupce	k1gMnSc2	nástupce
Filipa	Filip	k1gMnSc2	Filip
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc7d1	Anjý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Karla	Karel	k1gMnSc4	Karel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
konfliktu	konflikt	k1gInSc3	konflikt
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
válkám	válka	k1gFnPc3	válka
o	o	k7c6	o
dědictví	dědictví	k1gNnSc6	dědictví
španělské	španělský	k2eAgNnSc1d1	španělské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
některým	některý	k3yIgInPc3	některý
dílčím	dílčí	k2eAgInPc3d1	dílčí
neúspěchům	neúspěch	k1gInPc3	neúspěch
Leopoldovy	Leopoldův	k2eAgFnSc2d1	Leopoldova
vlády	vláda	k1gFnSc2	vláda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
panování	panování	k1gNnSc4	panování
mezi	mezi	k7c4	mezi
skutečné	skutečný	k2eAgFnPc4d1	skutečná
evropské	evropský	k2eAgFnPc4d1	Evropská
velmoci	velmoc	k1gFnPc4	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc1d1	habsburský
byl	být	k5eAaImAgInS	být
druhým	druhý	k4xOgNnSc7	druhý
nejdéle	dlouho	k6eAd3	dlouho
vládnoucím	vládnoucí	k2eAgMnSc7d1	vládnoucí
panovníkem	panovník	k1gMnSc7	panovník
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgInS	vládnout
48	[number]	k4	48
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
pochován	pochován	k2eAgInSc1d1	pochován
pod	pod	k7c7	pod
skvostným	skvostný	k2eAgInSc7d1	skvostný
barokním	barokní	k2eAgInSc7d1	barokní
náhrobkem	náhrobek	k1gInSc7	náhrobek
v	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
kryptě	krypta	k1gFnSc6	krypta
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc4d1	latinský
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
První	první	k4xOgMnSc1	první
mezi	mezi	k7c4	mezi
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tolika	tolik	k4xDc2	tolik
podivuhodných	podivuhodný	k2eAgNnPc2d1	podivuhodné
vítězství	vítězství	k1gNnPc2	vítězství
a	a	k8xC	a
samotné	samotný	k2eAgInPc4d1	samotný
Turky	turek	k1gInPc4	turek
donutil	donutit	k5eAaPmAgMnS	donutit
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
;	;	kIx,	;
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rakvi	rakev	k1gFnSc6	rakev
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
<g/>
,	,	kIx,	,
se	s	k7c7	s
souhlasným	souhlasný	k2eAgNnSc7d1	souhlasné
nadšením	nadšení	k1gNnSc7	nadšení
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
zvaný	zvaný	k2eAgInSc1d1	zvaný
Veliký	veliký	k2eAgInSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
je	být	k5eAaImIp3nS	být
datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
i	i	k8xC	i
věk	věk	k1gInSc1	věk
uveden	uvést	k5eAaPmNgInS	uvést
chybně	chybně	k6eAd1	chybně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zesnul	zesnout	k5eAaPmAgInS	zesnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgInPc1d1	stavební
plány	plán	k1gInPc1	plán
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Leopoldovou	Leopoldův	k2eAgFnSc7d1	Leopoldova
snahou	snaha	k1gFnSc7	snaha
bylo	být	k5eAaImAgNnS	být
učinit	učinit	k5eAaImF	učinit
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
dvůr	dvůr	k1gInSc4	dvůr
co	co	k9	co
nejatraktivnějším	atraktivní	k2eAgMnSc7d3	nejatraktivnější
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
sám	sám	k3xTgMnSc1	sám
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ambiciózní	ambiciózní	k2eAgInSc4d1	ambiciózní
stavební	stavební	k2eAgInSc4d1	stavební
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
barokním	barokní	k2eAgNnSc7d1	barokní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
nového	nový	k2eAgInSc2d1	nový
schönbrunnského	schönbrunnský	k2eAgInSc2d1	schönbrunnský
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Leopoldinské	leopoldinský	k2eAgNnSc4d1	leopoldinský
křídlo	křídlo	k1gNnSc4	křídlo
Hofburgu	Hofburg	k1gInSc2	Hofburg
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k8xC	i
základy	základ	k1gInPc1	základ
barokní	barokní	k2eAgFnSc2d1	barokní
přestavby	přestavba	k1gFnSc2	přestavba
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
dílem	dílo	k1gNnSc7	dílo
Leopoldovým	Leopoldův	k2eAgNnSc7d1	Leopoldovo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
nechal	nechat	k5eAaPmAgMnS	nechat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vztyčit	vztyčit	k5eAaPmF	vztyčit
sloup	sloup	k1gInSc4	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
jako	jako	k8xS	jako
připomínku	připomínka	k1gFnSc4	připomínka
překonané	překonaný	k2eAgFnSc2d1	překonaná
morové	morový	k2eAgFnSc2d1	morová
rány	rána	k1gFnSc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
sloupu	sloup	k1gInSc2	sloup
je	být	k5eAaImIp3nS	být
také	také	k9	také
socha	socha	k1gFnSc1	socha
modlícího	modlící	k2eAgNnSc2d1	modlící
se	se	k3xPyFc4	se
Leopolda	Leopolda	k1gFnSc1	Leopolda
ve	v	k7c6	v
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předlohou	předloha	k1gFnSc7	předloha
podobných	podobný	k2eAgInPc2d1	podobný
pomníků	pomník	k1gInPc2	pomník
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1703	[number]	k4	1703
schválil	schválit	k5eAaPmAgMnS	schválit
založení	založení	k1gNnSc4	založení
Vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
Diaria	Diarium	k1gNnSc2	Diarium
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
Wiener	Wienero	k1gNnPc2	Wienero
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
<g/>
,	,	kIx,	,
započaly	započnout	k5eAaPmAgFnP	započnout
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
Linienwallu	Linienwallo	k1gNnSc6	Linienwallo
(	(	kIx(	(
<g/>
lehké	lehký	k2eAgFnPc1d1	lehká
hradby	hradba	k1gFnPc1	hradba
a	a	k8xC	a
opevnění	opevnění	k1gNnSc1	opevnění
mezi	mezi	k7c7	mezi
předměstím	předměstí	k1gNnSc7	předměstí
a	a	k8xC	a
okolím	okolí	k1gNnSc7	okolí
kolem	kolem	k7c2	kolem
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
trať	trať	k1gFnSc1	trať
městského	městský	k2eAgInSc2d1	městský
rychlovlaku	rychlovlak	k1gInSc2	rychlovlak
(	(	kIx(	(
<g/>
ulic	ulice	k1gFnPc2	ulice
Wiener	Wiener	k1gInSc1	Wiener
Gürtelu	Gürtel	k1gInSc2	Gürtel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
byl	být	k5eAaImAgMnS	být
jazykově	jazykově	k6eAd1	jazykově
velmi	velmi	k6eAd1	velmi
nadaný	nadaný	k2eAgMnSc1d1	nadaný
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
latiny	latina	k1gFnSc2	latina
hovořil	hovořit	k5eAaImAgInS	hovořit
také	také	k9	také
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejoblíbenějším	oblíbený	k2eAgMnSc7d3	nejoblíbenější
jazykem	jazyk	k1gMnSc7	jazyk
však	však	k9	však
byla	být	k5eAaImAgFnS	být
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
dvorního	dvorní	k2eAgMnSc2d1	dvorní
knihovníka	knihovník	k1gMnSc2	knihovník
Petera	Peter	k1gMnSc2	Peter
Lambecka	Lambecko	k1gNnSc2	Lambecko
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
sbírku	sbírka	k1gFnSc4	sbírka
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
starožitností	starožitnost	k1gFnPc2	starožitnost
a	a	k8xC	a
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
podporoval	podporovat	k5eAaImAgInS	podporovat
zakládání	zakládání	k1gNnSc4	zakládání
univerzit	univerzita	k1gFnPc2	univerzita
(	(	kIx(	(
<g/>
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
či	či	k8xC	či
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
také	také	k9	také
podporoval	podporovat	k5eAaImAgMnS	podporovat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
Akademie	akademie	k1gFnSc2	akademie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
vědce	vědec	k1gMnSc2	vědec
a	a	k8xC	a
filosofa	filosof	k1gMnSc2	filosof
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
Leibnize	Leibnize	k1gFnSc1	Leibnize
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
přírodovědně-výzkumné	přírodovědněýzkumný	k2eAgFnPc4d1	přírodovědně-výzkumný
společnosti	společnost	k1gFnSc2	společnost
Leopoldina	Leopoldin	k2eAgFnSc1d1	Leopoldina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
založil	založit	k5eAaPmAgMnS	založit
Collegium	Collegium	k1gNnSc4	Collegium
pro	pro	k7c4	pro
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
merkantilismem	merkantilismus	k1gInSc7	merkantilismus
zval	zvát	k5eAaImAgInS	zvát
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
dvůr	dvůr	k1gInSc4	dvůr
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
kameralisty	kameralista	k1gMnPc4	kameralista
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
merkantilistických	merkantilistický	k2eAgFnPc2d1	merkantilistická
myšlenek	myšlenka	k1gFnPc2	myšlenka
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Leopold	Leopold	k1gMnSc1	Leopold
se	se	k3xPyFc4	se
také	také	k9	také
věnoval	věnovat	k5eAaPmAgMnS	věnovat
alchymii	alchymie	k1gFnSc3	alchymie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgFnSc1d1	hudební
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
milovníkem	milovník	k1gMnSc7	milovník
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
také	také	k9	také
velmi	velmi	k6eAd1	velmi
nadaným	nadaný	k2eAgMnSc7d1	nadaný
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
několik	několik	k4yIc4	několik
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
svůj	svůj	k3xOyFgInSc4	svůj
komorní	komorní	k2eAgInSc4d1	komorní
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
pěstoval	pěstovat	k5eAaImAgMnS	pěstovat
především	především	k6eAd1	především
italskou	italský	k2eAgFnSc4d1	italská
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
italskou	italský	k2eAgFnSc4d1	italská
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Johanna	Johann	k1gMnSc4	Johann
Heinricha	Heinrich	k1gMnSc4	Heinrich
Schmelzera	Schmelzer	k1gMnSc4	Schmelzer
jako	jako	k8xC	jako
prvního	první	k4xOgMnSc2	první
neitalského	italský	k2eNgMnSc2d1	neitalský
skladatele	skladatel	k1gMnSc2	skladatel
císařským	císařský	k2eAgMnSc7d1	císařský
dvorním	dvorní	k2eAgMnSc7d1	dvorní
kapelníkem	kapelník	k1gMnSc7	kapelník
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
hrála	hrát	k5eAaImAgFnS	hrát
italština	italština	k1gFnSc1	italština
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
duchovními	duchovní	k2eAgInPc7d1	duchovní
náměty	námět	k1gInPc7	námět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1659	[number]	k4	1659
nechal	nechat	k5eAaPmAgMnS	nechat
zřídit	zřídit	k5eAaPmF	zřídit
dvorní	dvorní	k2eAgNnSc4d1	dvorní
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
Hoftheater	Hoftheater	k1gInSc1	Hoftheater
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
několikrát	několikrát	k6eAd1	několikrát
obnovováno	obnovován	k2eAgNnSc1d1	obnovováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
dobrého	dobrý	k2eAgMnSc4d1	dobrý
skladatele	skladatel	k1gMnSc4	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
přes	přes	k7c4	přes
230	[number]	k4	230
různých	různý	k2eAgFnPc2d1	různá
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
od	od	k7c2	od
menších	malý	k2eAgFnPc2d2	menší
duchovních	duchovní	k2eAgFnPc2d1	duchovní
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
oratorií	oratorium	k1gNnPc2	oratorium
přes	přes	k7c4	přes
balety	balet	k1gInPc4	balet
až	až	k9	až
po	po	k7c4	po
německé	německý	k2eAgFnPc4d1	německá
zpěvohry	zpěvohra	k1gFnPc4	zpěvohra
(	(	kIx(	(
<g/>
singspiely	singspiel	k1gInPc4	singspiel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hudebně	hudebně	k6eAd1	hudebně
velmi	velmi	k6eAd1	velmi
hodnotná	hodnotný	k2eAgNnPc1d1	hodnotné
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
jeho	jeho	k3xOp3gFnPc4	jeho
skladby	skladba	k1gFnPc4	skladba
k	k	k7c3	k
oficiu	oficium	k1gNnSc3	oficium
za	za	k7c4	za
zemřelé	zemřelý	k2eAgFnPc4d1	zemřelá
<g/>
,	,	kIx,	,
provozované	provozovaný	k2eAgFnPc4d1	provozovaná
ještě	ještě	k6eAd1	ještě
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
skladby	skladba	k1gFnPc4	skladba
patří	patřit	k5eAaImIp3nS	patřit
jeho	jeho	k3xOp3gNnSc1	jeho
sepolkra	sepolkra	k1gFnSc1	sepolkra
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejznámější	známý	k2eAgFnSc4d3	nejznámější
je	být	k5eAaImIp3nS	být
Il	Il	k1gFnSc4	Il
Lutto	Lutto	k1gNnSc4	Lutto
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
universo	universa	k1gFnSc5	universa
(	(	kIx(	(
<g/>
Smutek	smutek	k1gInSc1	smutek
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1688	[number]	k4	1688
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
ovšem	ovšem	k9	ovšem
také	také	k9	také
mnoho	mnoho	k6eAd1	mnoho
vynikající	vynikající	k2eAgFnSc2d1	vynikající
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
operách	opera	k1gFnPc6	opera
italského	italský	k2eAgMnSc2d1	italský
skladatele	skladatel	k1gMnSc2	skladatel
Antonia	Antonio	k1gMnSc2	Antonio
Draghiho	Draghi	k1gMnSc2	Draghi
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejpilnějších	pilný	k2eAgMnPc2d3	nejpilnější
skladatelů	skladatel	k1gMnPc2	skladatel
–	–	k?	–
snad	snad	k9	snad
nejplodnějším	plodný	k2eAgMnSc7d3	nejplodnější
skladatelem	skladatel	k1gMnSc7	skladatel
hudebně	hudebně	k6eAd1	hudebně
dramatických	dramatický	k2eAgNnPc2d1	dramatické
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
nejméně	málo	k6eAd3	málo
4	[number]	k4	4
premiérované	premiérovaný	k2eAgFnSc2d1	premiérovaná
opery	opera	k1gFnSc2	opera
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1687	[number]	k4	1687
<g/>
/	/	kIx~	/
<g/>
88	[number]	k4	88
–	–	k?	–
Il	Il	k1gMnSc1	Il
marito	marit	k2eAgNnSc1d1	marit
ama	ama	k?	ama
più	più	k?	più
/	/	kIx~	/
Manžel	manžel	k1gMnSc1	manžel
miluje	milovat	k5eAaImIp3nS	milovat
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
byl	být	k5eAaImAgMnS	být
ženat	ženat	k2eAgMnSc1d1	ženat
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Tereza	Tereza	k1gFnSc1	Tereza
Španělská	španělský	k2eAgFnSc1d1	španělská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnSc7	jeho
sestřenicí	sestřenice	k1gFnSc7	sestřenice
i	i	k8xC	i
neteří	neteř	k1gFnSc7	neteř
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c4	za
Leopolda	Leopold	k1gMnSc4	Leopold
provdala	provdat	k5eAaPmAgFnS	provdat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
<g/>
.	.	kIx.	.
</s>
<s>
Porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gNnSc3	on
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
1667	[number]	k4	1667
<g/>
–	–	k?	–
<g/>
1668	[number]	k4	1668
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Antonie	Antonie	k1gFnSc2	Antonie
(	(	kIx(	(
<g/>
1669	[number]	k4	1669
<g/>
–	–	k?	–
<g/>
1692	[number]	k4	1692
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1685	[number]	k4	1685
bavorský	bavorský	k2eAgMnSc1d1	bavorský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Max	Max	k1gMnSc1	Max
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
manželky	manželka	k1gFnPc4	manželka
Henrietty	Henrietta	k1gFnSc2	Henrietta
Adély	Adéla	k1gFnSc2	Adéla
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Leopold	Leopold	k1gMnSc1	Leopold
(	(	kIx(	(
<g/>
1670	[number]	k4	1670
<g/>
–	–	k?	–
<g/>
1670	[number]	k4	1670
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Antonie	Antonie	k1gFnSc1	Antonie
(	(	kIx(	(
<g/>
1672	[number]	k4	1672
<g/>
–	–	k?	–
<g/>
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
<g/>
Druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
stala	stát	k5eAaPmAgFnS	stát
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Klaudie	Klaudie	k1gFnSc1	Klaudie
Felicitas	Felicitas	k1gFnSc1	Felicitas
Tyrolská	tyrolský	k2eAgFnSc1d1	tyrolská
(	(	kIx(	(
<g/>
sestřenice	sestřenice	k1gFnSc1	sestřenice
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zemřely	zemřít	k5eAaPmAgFnP	zemřít
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Marie	Marie	k1gFnSc1	Marie
Sofie	Sofie	k1gFnSc1	Sofie
(	(	kIx(	(
<g/>
1674	[number]	k4	1674
<g/>
–	–	k?	–
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
Klementina	Klementina	k1gFnSc1	Klementina
(	(	kIx(	(
<g/>
1675	[number]	k4	1675
<g/>
–	–	k?	–
<g/>
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
<g/>
Roku	rok	k1gInSc2	rok
1676	[number]	k4	1676
se	se	k3xPyFc4	se
Leopold	Leopold	k1gMnSc1	Leopold
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
oženil	oženit	k5eAaPmAgMnS	oženit
po	po	k7c6	po
třetí	třetí	k4xOgFnSc3	třetí
<g/>
,	,	kIx,	,
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Eleonorou	Eleonora	k1gFnSc7	Eleonora
Magdalenou	Magdalena	k1gFnSc7	Magdalena
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
falckého	falcký	k2eAgMnSc2d1	falcký
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Filipa	Filip	k1gMnSc2	Filip
Viléma	Vilém	k1gMnSc2	Vilém
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Alžběty	Alžběta	k1gFnSc2	Alžběta
Amálie	Amálie	k1gFnSc2	Amálie
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
tyto	tento	k3xDgFnPc4	tento
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1678	[number]	k4	1678
<g/>
–	–	k?	–
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1699	[number]	k4	1699
Amálie	Amálie	k1gFnSc1	Amálie
Vilemína	Vilemína	k1gFnSc1	Vilemína
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
brunšvického	brunšvický	k2eAgInSc2d1	brunšvický
vévodského	vévodský	k2eAgInSc2d1	vévodský
páru	pár	k1gInSc2	pár
Jana	Jan	k1gMnSc4	Jan
Fridricha	Fridrich	k1gMnSc4	Fridrich
a	a	k8xC	a
Benedikty	benedikt	k1gInPc4	benedikt
Falcké	falcký	k2eAgInPc4d1	falcký
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kristýna	Kristýna	k1gFnSc1	Kristýna
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
<g/>
–	–	k?	–
<g/>
1679	[number]	k4	1679
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
1680	[number]	k4	1680
<g/>
–	–	k?	–
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
místodržitelka	místodržitelka	k1gFnSc1	místodržitelka
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
1682	[number]	k4	1682
<g/>
–	–	k?	–
<g/>
1684	[number]	k4	1684
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Josefa	Josefa	k1gFnSc1	Josefa
(	(	kIx(	(
<g/>
1683	[number]	k4	1683
<g/>
–	–	k?	–
<g/>
1754	[number]	k4	1754
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1708	[number]	k4	1708
portugalský	portugalský	k2eAgMnSc1d1	portugalský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Petra	Petr	k1gMnSc2	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
Sofie	Sofia	k1gFnSc2	Sofia
Falcké	falcký	k2eAgFnSc2d1	Falcká
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1684	[number]	k4	1684
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1696	[number]	k4	1696
zámek	zámek	k1gInSc1	zámek
Ebersdorf	Ebersdorf	k1gInSc4	Ebersdorf
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1685	[number]	k4	1685
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
∞	∞	k?	∞
1708	[number]	k4	1708
Alžběta	Alžběta	k1gFnSc1	Alžběta
Kristýna	Kristýna	k1gFnSc1	Kristýna
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1	Brunšvicko-Wolfenbüttelský
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
brunšvického	brunšvický	k2eAgInSc2d1	brunšvický
vévodského	vévodský	k2eAgInSc2d1	vévodský
páru	pár	k1gInSc2	pár
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Rudolfa	Rudolf	k1gMnSc2	Rudolf
a	a	k8xC	a
Kristýny	Kristýna	k1gFnSc2	Kristýna
Luisy	Luisa	k1gFnPc4	Luisa
Öttingenské	Öttingenský	k2eAgFnPc4d1	Öttingenský
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1687	[number]	k4	1687
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1703	[number]	k4	1703
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Magdalena	Magdalena	k1gFnSc1	Magdalena
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1689	[number]	k4	1689
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1743	[number]	k4	1743
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Markéta	Markéta	k1gFnSc1	Markéta
(	(	kIx(	(
<g/>
1690	[number]	k4	1690
<g/>
–	–	k?	–
<g/>
1691	[number]	k4	1691
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
947	[number]	k4	947
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
KUBEŠ	Kubeš	k1gMnSc1	Kubeš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
cesta	cesta	k1gFnSc1	cesta
Leopolda	Leopolda	k1gFnSc1	Leopolda
I.	I.	kA	I.
za	za	k7c7	za
říšskou	říšský	k2eAgFnSc7d1	říšská
korunou	koruna	k1gFnSc7	koruna
(	(	kIx(	(
<g/>
1657	[number]	k4	1657
<g/>
–	–	k?	–
<g/>
1658	[number]	k4	1658
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
a	a	k8xC	a
korunovace	korunovace	k1gFnPc1	korunovace
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc2d1	římská
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
409	[number]	k4	409
<g/>
-	-	kIx~	-
<g/>
419	[number]	k4	419
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
barokního	barokní	k2eAgMnSc2d1	barokní
Habsburka	Habsburk	k1gMnSc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
245	[number]	k4	245
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
141	[number]	k4	141
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
IX	IX	kA	IX
<g/>
.	.	kIx.	.
1683	[number]	k4	1683
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
860	[number]	k4	860
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
-	-	kIx~	-
<g/>
1705	[number]	k4	1705
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
141	[number]	k4	141
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WINKELBAUER	WINKELBAUER	kA	WINKELBAUER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1522	[number]	k4	1522
-	-	kIx~	-
1699	[number]	k4	1699
:	:	kIx,	:
Ständefreiheit	Ständefreiheit	k1gInSc1	Ständefreiheit
und	und	k?	und
Fürstenmacht	Fürstenmacht	k1gInSc1	Fürstenmacht
;	;	kIx,	;
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
Untertanen	Untertanen	k1gInSc1	Untertanen
des	des	k1gNnSc1	des
Hauses	Hauses	k1gInSc1	Hauses
Habsburg	Habsburg	k1gMnSc1	Habsburg
im	im	k?	im
konfessionellen	konfessionellen	k2eAgMnSc1d1	konfessionellen
Zeitalter	Zeitalter	k1gMnSc1	Zeitalter
<g/>
.	.	kIx.	.
</s>
<s>
Teil	Teil	k1gInSc1	Teil
1	[number]	k4	1
<g/>
..	..	k?	..
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
621	[number]	k4	621
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3528	[number]	k4	3528
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WINKELBAUER	WINKELBAUER	kA	WINKELBAUER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1522	[number]	k4	1522
-	-	kIx~	-
1699	[number]	k4	1699
:	:	kIx,	:
Ständefreiheit	Ständefreiheit	k1gInSc1	Ständefreiheit
und	und	k?	und
Fürstenmacht	Fürstenmacht	k1gInSc1	Fürstenmacht
;	;	kIx,	;
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
Untertanen	Untertanen	k1gInSc1	Untertanen
des	des	k1gNnSc1	des
Hauses	Hauses	k1gInSc1	Hauses
Habsburg	Habsburg	k1gMnSc1	Habsburg
im	im	k?	im
konfessionellen	konfessionellen	k2eAgMnSc1d1	konfessionellen
Zeitalter	Zeitalter	k1gMnSc1	Zeitalter
<g/>
.	.	kIx.	.
</s>
<s>
Teil	Teil	k1gInSc1	Teil
2	[number]	k4	2
<g/>
..	..	k?	..
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
567	[number]	k4	567
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3987	[number]	k4	3987
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
panovníků	panovník	k1gMnPc2	panovník
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Leopold	Leopolda	k1gFnPc2	Leopolda
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Leopold	Leopolda	k1gFnPc2	Leopolda
I.	I.	kA	I.
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
