<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1985	[number]	k4	1985
Valašské	valašský	k2eAgNnSc4d1	Valašské
Meziříčí	Meziříčí	k1gNnSc4	Meziříčí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
profesionální	profesionální	k2eAgInSc4d1	profesionální
okruh	okruh	k1gInSc4	okruh
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tenisem	tenis	k1gInSc7	tenis
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
výrazněji	výrazně	k6eAd2	výrazně
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazil	porazit	k5eAaPmAgMnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Švýcara	Švýcar	k1gMnSc4	Švýcar
Rogera	Roger	k1gMnSc4	Roger
Federera	Federer	k1gMnSc4	Federer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
získal	získat	k5eAaPmAgInS	získat
třináct	třináct	k4xCc4	třináct
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
turnaji	turnaj	k1gInSc6	turnaj
série	série	k1gFnSc1	série
Masters	Masters	k1gInSc1	Masters
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
Masters	Masters	k1gInSc1	Masters
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
finále	finále	k1gNnSc4	finále
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
a	a	k8xC	a
semifinále	semifinále	k1gNnSc6	semifinále
na	na	k7c6	na
French	Fren	k1gFnPc6	Fren
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
</s>
<s>
Probojoval	probojovat	k5eAaPmAgInS	probojovat
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
jediného	jediný	k2eAgNnSc2d1	jediné
grandslamového	grandslamový	k2eAgNnSc2d1	grandslamové
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
nestačil	stačit	k5eNaBmAgMnS	stačit
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Rafaela	Rafael	k1gMnSc2	Rafael
Nadala	nadat	k5eAaPmAgNnP	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
2011	[number]	k4	2011
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
v	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
Jo-Wilfriedu	Jo-Wilfried	k1gMnSc3	Jo-Wilfried
Tsongovi	Tsong	k1gMnSc3	Tsong
<g/>
.	.	kIx.	.
</s>
<s>
Účastí	účast	k1gFnSc7	účast
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtyřkou	čtyřka	k1gFnSc7	čtyřka
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
a	a	k8xC	a
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gInSc4	Open
2014	[number]	k4	2014
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgInS	zařadit
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Ivana	Ivan	k1gMnSc4	Ivan
Lendla	Lendla	k1gMnSc4	Lendla
jako	jako	k8xC	jako
druhý	druhý	k4xOgMnSc1	druhý
český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dokázal	dokázat	k5eAaPmAgMnS	dokázat
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
dvouhry	dvouhra	k1gFnSc2	dvouhra
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
čtyřech	čtyři	k4xCgNnPc6	čtyři
Grand	grand	k1gMnSc1	grand
Slamech	slam	k1gInPc6	slam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
pro	pro	k7c4	pro
dvouhru	dvouhra	k1gFnSc4	dvouhra
byl	být	k5eAaImAgInS	být
nejvýše	nejvýše	k6eAd1	nejvýše
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Čech	Čech	k1gMnSc1	Čech
na	na	k7c6	na
takto	takto	k6eAd1	takto
vysoké	vysoký	k2eAgFnSc6d1	vysoká
pozici	pozice	k1gFnSc6	pozice
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
opustil	opustit	k5eAaPmAgMnS	opustit
Petr	Petr	k1gMnSc1	Petr
Korda	Korda	k1gMnSc1	Korda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
pak	pak	k6eAd1	pak
nejvýše	nejvýše	k6eAd1	nejvýše
figuroval	figurovat	k5eAaImAgMnS	figurovat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
na	na	k7c4	na
54	[number]	k4	54
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
ho	on	k3xPp3gNnSc4	on
trénoval	trénovat	k5eAaImAgMnS	trénovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Štěpánkův	Štěpánkův	k2eAgMnSc1d1	Štěpánkův
kouč	kouč	k1gMnSc1	kouč
Tomáš	Tomáš	k1gMnSc1	Tomáš
Krupa	krupa	k1gFnSc1	krupa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
do	do	k7c2	do
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
ho	on	k3xPp3gInSc4	on
vedl	vést	k5eAaImAgMnS	vést
Venezuelan	Venezuelan	k1gMnSc1	Venezuelan
Daniel	Daniel	k1gMnSc1	Daniel
Vallverdu	Vallverd	k1gMnSc3	Vallverd
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dříve	dříve	k6eAd2	dříve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Andym	Andymum	k1gNnPc2	Andymum
Murraym	Murraymum	k1gNnPc2	Murraymum
<g/>
.	.	kIx.	.
</s>
<s>
Berdych	Berdych	k1gMnSc1	Berdych
rozchod	rozchod	k1gInSc4	rozchod
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
facebookovém	facebookový	k2eAgInSc6d1	facebookový
profilu	profil	k1gInSc6	profil
<g/>
,	,	kIx,	,
když	když	k8xS	když
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
nejsem	být	k5eNaImIp1nS	být
tenista	tenista	k1gMnSc1	tenista
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
teprve	teprve	k6eAd1	teprve
začínal	začínat	k5eAaImAgMnS	začínat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musím	muset	k5eAaImIp1nS	muset
jednat	jednat	k5eAaImF	jednat
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
když	když	k8xS	když
cítím	cítit	k5eAaImIp1nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebuju	potřebovat	k5eAaImIp1nS	potřebovat
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Za	za	k7c4	za
český	český	k2eAgInSc4d1	český
daviscupový	daviscupový	k2eAgInSc4d1	daviscupový
tým	tým	k1gInSc4	tým
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
zahrál	zahrát	k5eAaPmAgMnS	zahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Thajsku	Thajsko	k1gNnSc3	Thajsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvouhru	dvouhra	k1gFnSc4	dvouhra
nad	nad	k7c7	nad
Udomčokem	Udomčok	k1gInSc7	Udomčok
a	a	k8xC	a
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Novákem	Novák	k1gMnSc7	Novák
také	také	k9	také
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
dvaceti	dvacet	k4xCc3	dvacet
osmi	osm	k4xCc2	osm
mezistátním	mezistátní	k2eAgNnPc3d1	mezistátní
utkáním	utkání	k1gNnSc7	utkání
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
jedničkou	jednička	k1gFnSc7	jednička
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Davisův	Davisův	k2eAgInSc4d1	Davisův
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgInS	porazit
Španělsko	Španělsko	k1gNnSc4	Španělsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Výhrou	výhra	k1gFnSc7	výhra
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
se	se	k3xPyFc4	se
s	s	k7c7	s
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
stali	stát	k5eAaPmAgMnP	stát
historicky	historicky	k6eAd1	historicky
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
českou	český	k2eAgFnSc7d1	Česká
dvojicí	dvojice	k1gFnSc7	dvojice
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
2013	[number]	k4	2013
dokázal	dokázat	k5eAaPmAgInS	dokázat
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
týmem	tým	k1gInSc7	tým
"	"	kIx"	"
<g/>
salátovou	salátový	k2eAgFnSc4d1	salátová
mísu	mísa	k1gFnSc4	mísa
<g/>
"	"	kIx"	"
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgInSc4	první
bod	bod	k1gInSc4	bod
proti	proti	k7c3	proti
Dušanu	Dušan	k1gMnSc3	Dušan
Lajovićovi	Lajovića	k1gMnSc3	Lajovića
a	a	k8xC	a
s	s	k7c7	s
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
následně	následně	k6eAd1	následně
zvládli	zvládnout	k5eAaPmAgMnP	zvládnout
i	i	k9	i
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Berdych	Berdych	k1gMnSc1	Berdych
si	se	k3xPyFc3	se
tak	tak	k9	tak
připsal	připsat	k5eAaPmAgMnS	připsat
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc7	Kvitová
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Hopman	Hopman	k1gMnSc1	Hopman
Cup	cup	k1gInSc4	cup
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejoblíbenější	oblíbený	k2eAgNnSc1d3	nejoblíbenější
uvádí	uvádět	k5eAaImIp3nS	uvádět
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
údery	úder	k1gInPc1	úder
patří	patřit	k5eAaImIp3nP	patřit
forhend	forhend	k1gInSc1	forhend
a	a	k8xC	a
servis	servis	k1gInSc1	servis
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
jej	on	k3xPp3gMnSc4	on
trénoval	trénovat	k5eAaImAgMnS	trénovat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yRgMnPc3	který
ukončil	ukončit	k5eAaPmAgMnS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Navrátil	Navrátil	k1gMnSc1	Navrátil
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k7c2	místo
šéftrenéra	šéftrenér	k1gMnSc2	šéftrenér
TK	TK	kA	TK
Agrofert	Agrofert	k1gInSc1	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
je	být	k5eAaImIp3nS	být
i	i	k9	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
profesionálního	profesionální	k2eAgInSc2d1	profesionální
tenisu	tenis	k1gInSc2	tenis
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
odehrál	odehrát	k5eAaPmAgInS	odehrát
tři	tři	k4xCgInPc4	tři
challengery	challenger	k1gInPc4	challenger
a	a	k8xC	a
několik	několik	k4yIc4	několik
turnajů	turnaj	k1gInPc2	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Futures	Futuresa	k1gFnPc2	Futuresa
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
událostech	událost	k1gFnPc6	událost
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
Czech	Czecha	k1gFnPc2	Czecha
Republic	Republice	k1gFnPc2	Republice
F5	F5	k1gMnPc2	F5
porazil	porazit	k5eAaPmAgMnS	porazit
Pavla	Pavel	k1gMnSc4	Pavel
Šnobela	Šnobel	k1gMnSc4	Šnobel
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
F8	F8	k1gFnSc2	F8
pak	pak	k6eAd1	pak
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Ladislava	Ladislav	k1gMnSc4	Ladislav
Chramostu	Chramosta	k1gMnSc4	Chramosta
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Futures	Futures	k1gInSc1	Futures
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
i	i	k9	i
čtyřher	čtyřhra	k1gFnPc2	čtyřhra
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
výsledku	výsledek	k1gInSc2	výsledek
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
s	s	k7c7	s
Lukášem	Lukáš	k1gMnSc7	Lukáš
Dlouhým	Dlouhý	k1gMnSc7	Dlouhý
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
F9	F9	k1gFnSc2	F9
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
probojovali	probojovat	k5eAaPmAgMnP	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
německému	německý	k2eAgInSc3d1	německý
páru	pár	k1gInSc2	pár
Petzschner	Petzschnra	k1gFnPc2	Petzschnra
a	a	k8xC	a
Stadler	Stadler	k1gInSc1	Stadler
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sezóně	sezóna	k1gFnSc6	sezóna
odehrál	odehrát	k5eAaPmAgMnS	odehrát
několik	několik	k4yIc4	několik
turnajů	turnaj	k1gInPc2	turnaj
nižších	nízký	k2eAgFnPc2d2	nižší
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Future	Futur	k1gMnSc5	Futur
F5	F5	k1gMnSc4	F5
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Petera	Petera	k1gMnSc1	Petera
Clarka	Clark	k1gInSc2	Clark
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
kategorii	kategorie	k1gFnSc6	kategorie
Challenger	Challenger	k1gMnSc1	Challenger
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvě	dva	k4xCgFnPc4	dva
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
zdolal	zdolat	k5eAaPmAgMnS	zdolat
domácího	domácí	k2eAgMnSc4d1	domácí
tenistu	tenista	k1gMnSc4	tenista
Juliana	Julian	k1gMnSc2	Julian
Knowleho	Knowle	k1gMnSc2	Knowle
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
,	,	kIx,	,
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
v	v	k7c6	v
maďarském	maďarský	k2eAgInSc6d1	maďarský
Burdaösu	Burdaös	k1gInSc6	Burdaös
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgMnS	porazit
Bulhara	Bulhar	k1gMnSc4	Bulhar
Ivayla	Ivayla	k1gMnSc7	Ivayla
Tarykova	Tarykov	k1gInSc2	Tarykov
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
sice	sice	k8xC	sice
prohrál	prohrát	k5eAaPmAgMnS	prohrát
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Roko	Roko	k1gMnSc1	Roko
Karanušićem	Karanušić	k1gMnSc7	Karanušić
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
postoupil	postoupit	k5eAaPmAgInS	postoupit
jako	jako	k9	jako
šťastný	šťastný	k2eAgMnSc1d1	šťastný
poražený	poražený	k1gMnSc1	poražený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
fázi	fáze	k1gFnSc6	fáze
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Němce	Němec	k1gMnSc4	Němec
Tomase	Tomasa	k1gFnSc3	Tomasa
Behrenda	Behrenda	k1gFnSc1	Behrenda
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dějství	dějství	k1gNnSc6	dějství
ale	ale	k8xC	ale
nestačil	stačit	k5eNaBmAgMnS	stačit
na	na	k7c4	na
Juana	Juan	k1gMnSc4	Juan
Ignacio	Ignacio	k6eAd1	Ignacio
Chelu	Chel	k1gInSc2	Chel
a	a	k8xC	a
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mu	on	k3xPp3gMnSc3	on
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
premiérové	premiérový	k2eAgInPc4d1	premiérový
turnajové	turnajový	k2eAgInPc4d1	turnajový
triumfy	triumf	k1gInPc4	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Levinským	Levinský	k2eAgMnSc7d1	Levinský
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
challenger	challenger	k1gInSc4	challenger
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
porazili	porazit	k5eAaPmAgMnP	porazit
švédskou	švédský	k2eAgFnSc4d1	švédská
dvojici	dvojice	k1gFnSc4	dvojice
Simon	Simona	k1gFnPc2	Simona
Aspelin	Aspelin	k2eAgMnSc1d1	Aspelin
a	a	k8xC	a
Johan	Johan	k1gMnSc1	Johan
Landsberg	Landsberg	k1gMnSc1	Landsberg
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Navrátilem	Navrátil	k1gMnSc7	Navrátil
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c4	na
události	událost	k1gFnPc4	událost
kategorie	kategorie	k1gFnSc2	kategorie
Futures	Futuresa	k1gFnPc2	Futuresa
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
challengeru	challenger	k1gInSc6	challenger
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgNnSc6d1	rozhodující
utkání	utkání	k1gNnSc6	utkání
zdolali	zdolat	k5eAaPmAgMnP	zdolat
argentinský	argentinský	k2eAgInSc4d1	argentinský
pár	pár	k1gInSc4	pár
Martín	Martín	k1gMnSc1	Martín
García	García	k1gMnSc1	García
a	a	k8xC	a
Sebastián	Sebastián	k1gMnSc1	Sebastián
Prieto	Prieto	k1gNnSc4	Prieto
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
po	po	k7c6	po
dovršení	dovršení	k1gNnSc6	dovršení
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
poprvé	poprvé	k6eAd1	poprvé
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
v	v	k7c6	v
Davis	Davis	k1gFnSc6	Davis
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Thajsku	Thajsko	k1gNnSc3	Thajsko
nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Novákem	Novák	k1gMnSc7	Novák
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
porazili	porazit	k5eAaPmAgMnP	porazit
dvojici	dvojice	k1gFnSc4	dvojice
Paradorn	Paradorna	k1gFnPc2	Paradorna
Srichaphan	Srichaphana	k1gFnPc2	Srichaphana
a	a	k8xC	a
Danai	Dana	k1gFnSc2	Dana
Udomčoke	Udomčok	k1gInSc2	Udomčok
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
za	za	k7c2	za
rozhodnutého	rozhodnutý	k2eAgInSc2d1	rozhodnutý
stavu	stav	k1gInSc2	stav
ještě	ještě	k6eAd1	ještě
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvouhru	dvouhra	k1gFnSc4	dvouhra
nad	nad	k7c7	nad
Danaiem	Danaius	k1gMnSc7	Danaius
Udomčokem	Udomčok	k1gMnSc7	Udomčok
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posun	posun	k1gInSc4	posun
z	z	k7c2	z
šesté	šestý	k4xOgFnSc2	šestý
stovky	stovka	k1gFnSc2	stovka
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
až	až	k9	až
na	na	k7c4	na
103	[number]	k4	103
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
poprvé	poprvé	k6eAd1	poprvé
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
anketu	anketa	k1gFnSc4	anketa
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kanár	kanár	k1gInSc4	kanár
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Postup	postup	k1gInSc4	postup
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP.	atp.	kA	atp.
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2004	[number]	k4	2004
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
věnoval	věnovat	k5eAaPmAgInS	věnovat
challengerům	challenger	k1gMnPc3	challenger
a	a	k8xC	a
událostem	událost	k1gFnPc3	událost
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
challengerech	challenger	k1gInPc6	challenger
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Besançonu	Besançon	k1gInSc6	Besançon
<g/>
,	,	kIx,	,
Weidenu	Weiden	k1gInSc6	Weiden
a	a	k8xC	a
Braunschweigu	Braunschweig	k1gInSc6	Braunschweig
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
začal	začít	k5eAaPmAgInS	začít
neúspěšným	úspěšný	k2eNgInSc7d1	neúspěšný
pokusem	pokus	k1gInSc7	pokus
se	se	k3xPyFc4	se
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Aucklandu	Auckland	k1gInSc6	Auckland
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
podařilo	podařit	k5eAaPmAgNnS	podařit
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
na	na	k7c4	na
úvodní	úvodní	k2eAgInSc4d1	úvodní
grandslam	grandslam	k1gInSc4	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
Australian	Australiana	k1gFnPc2	Australiana
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
Francouze	Francouz	k1gMnSc4	Francouz
Nicolase	Nicolasa	k1gFnSc6	Nicolasa
Mahuta	Mahut	k1gMnSc2	Mahut
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
však	však	k9	však
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
čtvrtého	čtvrtý	k4xOgMnSc4	čtvrtý
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
Andreho	Andre	k1gMnSc4	Andre
Agassiho	Agassi	k1gMnSc4	Agassi
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
hladce	hladko	k6eAd1	hladko
prohrál	prohrát	k5eAaPmAgMnS	prohrát
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
opět	opět	k6eAd1	opět
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
jako	jako	k9	jako
v	v	k7c6	v
loňském	loňský	k2eAgInSc6d1	loňský
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Felicianu	Felician	k1gMnSc3	Felician
Lópezovi	López	k1gMnSc3	López
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
na	na	k7c4	na
Španělsko	Španělsko	k1gNnSc4	Španělsko
nestačila	stačit	k5eNaBmAgFnS	stačit
a	a	k8xC	a
po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
do	do	k7c2	do
baráže	baráž	k1gFnSc2	baráž
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
také	také	k9	také
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
grandslamech	grandslam	k1gInPc6	grandslam
však	však	k9	však
nepřešel	přejít	k5eNaPmAgInS	přejít
přes	přes	k7c4	přes
úvodní	úvodní	k2eAgNnSc4d1	úvodní
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Thajci	Thajce	k1gMnSc3	Thajce
Paradornu	Paradorn	k1gInSc2	Paradorn
Srichaphanovi	Srichaphan	k1gMnSc3	Srichaphan
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
porazil	porazit	k5eAaPmAgMnS	porazit
Francouz	Francouz	k1gMnSc1	Francouz
Julien	Julien	k1gInSc4	Julien
Benneteau	Benneteaus	k1gInSc2	Benneteaus
výsledkem	výsledek	k1gInSc7	výsledek
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odjel	odjet	k5eAaPmAgMnS	odjet
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
do	do	k7c2	do
Atén	Atény	k1gFnPc2	Atény
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
Floriana	Florian	k1gMnSc4	Florian
Mayera	Mayer	k1gMnSc4	Mayer
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
soutěže	soutěž	k1gFnSc2	soutěž
překvapil	překvapit	k5eAaPmAgInS	překvapit
tenisový	tenisový	k2eAgInSc1d1	tenisový
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k9	jako
74	[number]	k4	74
<g/>
.	.	kIx.	.
hráč	hráč	k1gMnSc1	hráč
světa	svět	k1gInSc2	svět
zdolal	zdolat	k5eAaPmAgMnS	zdolat
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Švýcara	Švýcar	k1gMnSc4	Švýcar
Rogera	Roger	k1gMnSc4	Roger
Federera	Federer	k1gMnSc4	Federer
ve	v	k7c6	v
třísetové	třísetový	k2eAgFnSc6d1	třísetová
bitvě	bitva	k1gFnSc6	bitva
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
přešel	přejít	k5eAaPmAgInS	přejít
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
dějství	dějství	k1gNnSc6	dějství
přes	přes	k7c4	přes
patnáctého	patnáctý	k4xOgMnSc4	patnáctý
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
Tommy	Tomma	k1gFnSc2	Tomma
Robreda	Robreda	k1gMnSc1	Robreda
výsledkem	výsledek	k1gInSc7	výsledek
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ho	on	k3xPp3gNnSc4	on
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
nenasazený	nasazený	k2eNgMnSc1d1	nenasazený
Američan	Američan	k1gMnSc1	Američan
Taylor	Taylor	k1gMnSc1	Taylor
Dent	Dent	k1gMnSc1	Dent
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
sadách	sada	k1gFnPc6	sada
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
pětisetové	pětisetový	k2eAgFnSc6d1	pětisetová
bitvě	bitva	k1gFnSc6	bitva
Švéda	Švéd	k1gMnSc2	Švéd
Jonase	Jonasa	k1gFnSc6	Jonasa
Björkmana	Björkman	k1gMnSc2	Björkman
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nestačil	stačit	k5eNaBmAgMnS	stačit
Fin	Fin	k1gMnSc1	Fin
Tuomas	Tuomas	k1gMnSc1	Tuomas
Ketola	Ketola	k1gFnSc1	Ketola
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
zdolal	zdolat	k5eAaPmAgMnS	zdolat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
utkání	utkání	k1gNnSc6	utkání
odehrál	odehrát	k5eAaPmAgInS	odehrát
další	další	k2eAgInSc4d1	další
pětisetový	pětisetový	k2eAgInSc4d1	pětisetový
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
nad	nad	k7c7	nad
Rusem	Rus	k1gMnSc7	Rus
Michailem	Michail	k1gMnSc7	Michail
Južným	Južný	k1gMnSc7	Južný
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
už	už	k6eAd1	už
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Tommyho	Tommy	k1gMnSc4	Tommy
Haase	Haas	k1gMnSc4	Haas
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
baráže	baráž	k1gFnSc2	baráž
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
proti	proti	k7c3	proti
týmu	tým	k1gInSc3	tým
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
Berdych	Berdych	k1gInSc1	Berdych
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Ramóna	Ramón	k1gMnSc2	Ramón
Delgada	Delgada	k1gFnSc1	Delgada
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgInS	pomoct
tak	tak	k9	tak
k	k	k7c3	k
jednoznačné	jednoznačný	k2eAgFnSc3d1	jednoznačná
výhře	výhra	k1gFnSc3	výhra
Česka	Česko	k1gNnSc2	Česko
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
turnaj	turnaj	k1gInSc4	turnaj
do	do	k7c2	do
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
sedmého	sedmý	k4xOgMnSc4	sedmý
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
Španěla	Španěl	k1gMnSc4	Španěl
Davida	David	k1gMnSc2	David
Ferrera	Ferrer	k1gMnSc2	Ferrer
po	po	k7c6	po
setech	set	k1gInPc6	set
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
poté	poté	k6eAd1	poté
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
i	i	k9	i
nad	nad	k7c7	nad
Italem	Ital	k1gMnSc7	Ital
Filippem	Filipp	k1gInSc7	Filipp
Volandrim	Volandrim	k1gInSc1	Volandrim
dvakrát	dvakrát	k6eAd1	dvakrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
slavil	slavit	k5eAaImAgInS	slavit
zisk	zisk	k1gInSc1	zisk
prvního	první	k4xOgInSc2	první
titulu	titul	k1gInSc2	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
triumfu	triumf	k1gInSc6	triumf
se	se	k3xPyFc4	se
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
poprvé	poprvé	k6eAd1	poprvé
posunul	posunout	k5eAaPmAgInS	posunout
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
padesátky	padesátka	k1gFnSc2	padesátka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kanár	kanár	k1gInSc4	kanár
vyneslo	vynést	k5eAaPmAgNnS	vynést
prvenství	prvenství	k1gNnSc1	prvenství
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Postup	postup	k1gInSc4	postup
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP.	atp.	kA	atp.
Sezónu	sezóna	k1gFnSc4	sezóna
nezačal	začít	k5eNaPmAgInS	začít
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
lednových	lednový	k2eAgFnPc6d1	lednová
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
Adelaide	Adelaid	k1gInSc5	Adelaid
a	a	k8xC	a
Sydney	Sydney	k1gNnSc4	Sydney
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
hned	hned	k6eAd1	hned
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
neprošel	projít	k5eNaPmAgInS	projít
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
lepší	dobrý	k2eAgInSc1d2	lepší
výsledek	výsledek	k1gInSc1	výsledek
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rumunem	Rumun	k1gMnSc7	Rumun
Andreiem	Andreius	k1gMnSc7	Andreius
Pavlem	Pavel	k1gMnSc7	Pavel
probojovali	probojovat	k5eAaPmAgMnP	probojovat
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
rakousko-německé	rakouskoěmecký	k2eAgFnSc3d1	rakousko-německá
dvojici	dvojice	k1gFnSc3	dvojice
Jürgen	Jürgen	k1gInSc4	Jürgen
Melzer	Melzero	k1gNnPc2	Melzero
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Waske	Waske	k1gNnSc4	Waske
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
<g/>
,	,	kIx,	,
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
a	a	k8xC	a
Dubaji	Dubaj	k1gFnSc6	Dubaj
pokaždé	pokaždé	k6eAd1	pokaždé
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
jmenovaném	jmenovaný	k2eAgInSc6d1	jmenovaný
turnaji	turnaj	k1gInSc6	turnaj
odehrál	odehrát	k5eAaPmAgInS	odehrát
debl	debl	k1gInSc1	debl
s	s	k7c7	s
Němcem	Němec	k1gMnSc7	Němec
Nicolasem	Nicolas	k1gMnSc7	Nicolas
Kieferem	Kiefer	k1gMnSc7	Kiefer
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
až	až	k8xS	až
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
od	od	k7c2	od
švédsko-francouzského	švédskorancouzský	k2eAgInSc2d1	švédsko-francouzský
páru	pár	k1gInSc2	pár
Jonas	Jonas	k1gMnSc1	Jonas
Björkman	Björkman	k1gMnSc1	Björkman
a	a	k8xC	a
Fabrice	fabrika	k1gFnSc3	fabrika
Santoro	Santora	k1gFnSc5	Santora
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
v	v	k7c6	v
Davis	Davis	k1gFnSc6	Davis
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
nepodal	podat	k5eNaPmAgMnS	podat
proti	proti	k7c3	proti
Argentině	Argentina	k1gFnSc3	Argentina
přesvědčivý	přesvědčivý	k2eAgInSc4d1	přesvědčivý
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
páteční	páteční	k2eAgFnSc6d1	páteční
dvouhře	dvouhra	k1gFnSc6	dvouhra
přehrál	přehrát	k5eAaPmAgInS	přehrát
Guillermo	Guillerma	k1gFnSc5	Guillerma
Coria	Corius	k1gMnSc4	Corius
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
celkově	celkově	k6eAd1	celkově
prohrála	prohrát	k5eAaPmAgFnS	prohrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
opět	opět	k6eAd1	opět
sehrát	sehrát	k5eAaPmF	sehrát
utkání	utkání	k1gNnSc4	utkání
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
hrál	hrát	k5eAaImAgInS	hrát
turnaj	turnaj	k1gInSc1	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
Masters	Masters	k1gInSc4	Masters
1000	[number]	k4	1000
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc1	Wells
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
postoupil	postoupit	k5eAaPmAgMnS	postoupit
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
soutěže	soutěž	k1gFnSc2	soutěž
porazil	porazit	k5eAaPmAgMnS	porazit
devatenáctého	devatenáctý	k4xOgMnSc4	devatenáctý
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
Chorvata	Chorvat	k1gMnSc4	Chorvat
Maria	Mario	k1gMnSc4	Mario
Ančiće	Ančić	k1gMnSc4	Ančić
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
utkání	utkání	k1gNnSc6	utkání
však	však	k9	však
již	již	k6eAd1	již
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
dalšího	další	k2eAgMnSc4d1	další
Chorvata	Chorvat	k1gMnSc4	Chorvat
Ivana	Ivan	k1gMnSc4	Ivan
Ljubičiće	Ljubičić	k1gMnSc4	Ljubičić
a	a	k8xC	a
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
turnajích	turnaj	k1gInPc6	turnaj
ATP	atp	kA	atp
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
singlu	singl	k1gInSc6	singl
dokázal	dokázat	k5eAaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
Francouze	Francouz	k1gMnSc4	Francouz
Michaëla	Michaël	k1gMnSc4	Michaël
Llodru	Llodr	k1gInSc2	Llodr
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgFnPc4d1	zbylá
dvě	dva	k4xCgFnPc4	dva
dvouhry	dvouhra	k1gFnPc4	dvouhra
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejdříve	dříve	k6eAd3	dříve
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Argentince	Argentinec	k1gMnSc4	Argentinec
Guillerma	Guillerm	k1gMnSc4	Guillerm
Cañ	Cañ	k1gMnSc4	Cañ
a	a	k8xC	a
poté	poté	k6eAd1	poté
Chilana	Chilan	k1gMnSc4	Chilan
Nicoláse	Nicolás	k1gMnSc4	Nicolás
Massúa	Massúus	k1gMnSc4	Massúus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Novákem	Novák	k1gMnSc7	Novák
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
francouzskému	francouzský	k2eAgInSc3d1	francouzský
páru	pár	k1gInSc2	pár
Arnaud	Arnauda	k1gFnPc2	Arnauda
Clément	Clément	k1gMnSc1	Clément
a	a	k8xC	a
Michaël	Michaël	k1gMnSc1	Michaël
Llodra	Llodra	k1gFnSc1	Llodra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgNnSc1d1	další
utkání	utkání	k1gNnSc1	utkání
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Juan	Juan	k1gMnSc1	Juan
Ignacio	Ignacio	k1gMnSc1	Ignacio
Chela	Chela	k1gMnSc1	Chela
a	a	k8xC	a
Gastón	Gastón	k1gMnSc1	Gastón
Gaudio	Gaudio	k1gMnSc1	Gaudio
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
chilskou	chilský	k2eAgFnSc7d1	chilská
dvojicí	dvojice	k1gFnSc7	dvojice
Hermes	Hermes	k1gMnSc1	Hermes
Gamonal	Gamonal	k1gMnSc1	Gamonal
a	a	k8xC	a
Adrián	Adrián	k1gMnSc1	Adrián
García	Garcíus	k1gMnSc2	Garcíus
dokázali	dokázat	k5eAaPmAgMnP	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
červené	červený	k2eAgFnSc6d1	červená
skupině	skupina	k1gFnSc6	skupina
na	na	k7c6	na
posledním	poslední	k2eAgNnSc6d1	poslední
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gMnSc1	Open
zdolal	zdolat	k5eAaPmAgMnS	zdolat
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
Američana	Američan	k1gMnSc2	Američan
Jeffa	Jeff	k1gMnSc2	Jeff
Morrisna	Morrisn	k1gMnSc2	Morrisn
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
však	však	k9	však
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c6	na
Argentince	Argentinka	k1gFnSc6	Argentinka
Davida	David	k1gMnSc2	David
Nalbandiana	Nalbandian	k1gMnSc2	Nalbandian
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deblu	debl	k1gInSc6	debl
se	se	k3xPyFc4	se
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Tomáše	Tomáš	k1gMnSc2	Tomáš
Zíba	Zíbum	k1gNnSc2	Zíbum
probojoval	probojovat	k5eAaPmAgMnS	probojovat
také	také	k9	také
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
porazil	porazit	k5eAaPmAgMnS	porazit
ruský	ruský	k2eAgInSc4d1	ruský
pár	pár	k1gInSc4	pár
Igor	Igor	k1gMnSc1	Igor
Andrejev	Andrejev	k1gMnSc1	Andrejev
a	a	k8xC	a
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Davyděnko	Davyděnka	k1gFnSc5	Davyděnka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
následném	následný	k2eAgInSc6d1	následný
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
prošel	projít	k5eAaPmAgMnS	projít
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
dějství	dějství	k1gNnSc2	dějství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
svého	svůj	k3xOyFgMnSc2	svůj
olympijského	olympijský	k2eAgMnSc2d1	olympijský
přemožitele	přemožitel	k1gMnSc2	přemožitel
Taylora	Taylor	k1gMnSc2	Taylor
Denta	Dent	k1gMnSc2	Dent
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc1d1	následující
týden	týden	k1gInSc1	týden
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
první	první	k4xOgInSc4	první
větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c4	na
Swedish	Swedish	k1gInSc4	Swedish
Open	Open	k1gNnSc4	Open
zahrál	zahrát	k5eAaPmAgInS	zahrát
finále	finále	k1gNnSc4	finále
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
jej	on	k3xPp3gNnSc2	on
porazila	porazit	k5eAaPmAgFnS	porazit
světová	světový	k2eAgFnSc1d1	světová
trojka	trojka	k1gFnSc1	trojka
Španěl	Španěl	k1gMnSc1	Španěl
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
postoupil	postoupit	k5eAaPmAgInS	postoupit
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
Rusem	Rus	k1gMnSc7	Rus
Davyděnkem	Davyděnek	k1gMnSc7	Davyděnek
po	po	k7c6	po
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
událostech	událost	k1gFnPc6	událost
US	US	kA	US
Open	Open	k1gMnSc1	Open
Series	Series	k1gMnSc1	Series
měl	mít	k5eAaImAgMnS	mít
výsledky	výsledek	k1gInPc4	výsledek
poměrně	poměrně	k6eAd1	poměrně
dobré	dobrý	k2eAgInPc4d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
přemohl	přemoct	k5eAaPmAgMnS	přemoct
domácí	domácí	k1gMnSc1	domácí
tenista	tenista	k1gMnSc1	tenista
James	James	k1gMnSc1	James
Blake	Blake	k1gInSc4	Blake
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Canada	Canada	k1gFnSc1	Canada
Cupu	cup	k1gInSc2	cup
a	a	k8xC	a
Cincinnati	Cincinnati	k1gMnSc1	Cincinnati
Open	Open	k1gMnSc1	Open
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
nepřešel	přejít	k5eNaPmAgMnS	přejít
přes	přes	k7c4	přes
třetí	třetí	k4xOgFnSc4	třetí
fázi	fáze	k1gFnSc4	fáze
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
turnajová	turnajový	k2eAgFnSc1d1	turnajová
sedmička	sedmička	k1gFnSc1	sedmička
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgFnSc1d1	domácí
Andre	Andr	k1gInSc5	Andr
Agassi	Agass	k1gMnSc3	Agass
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zářijovém	zářijový	k2eAgInSc6d1	zářijový
grandslamu	grandslam	k1gInSc6	grandslam
opět	opět	k6eAd1	opět
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
ve	v	k7c6	v
Světové	světový	k2eAgFnSc6d1	světová
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
Česko	Česko	k1gNnSc1	Česko
utkalo	utkat	k5eAaPmAgNnS	utkat
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Berdych	Berdych	k1gMnSc1	Berdych
dokázal	dokázat	k5eAaPmAgMnS	dokázat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
pětisetových	pětisetový	k2eAgFnPc6d1	pětisetová
bitvách	bitva	k1gFnPc6	bitva
získat	získat	k5eAaPmF	získat
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
porazil	porazit	k5eAaPmAgMnS	porazit
Tommyho	Tommy	k1gMnSc4	Tommy
Haase	Haas	k1gMnSc4	Haas
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
3	[number]	k4	3
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
nedělním	nedělní	k2eAgInSc6d1	nedělní
duelu	duel	k1gInSc6	duel
zdolal	zdolat	k5eAaPmAgMnS	zdolat
také	také	k9	také
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Kiefera	Kiefero	k1gNnSc2	Kiefero
po	po	k7c6	po
sadách	sada	k1gFnPc6	sada
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
zápasy	zápas	k1gInPc4	zápas
však	však	k9	však
Češi	Čech	k1gMnPc1	Čech
prohráli	prohrát	k5eAaPmAgMnP	prohrát
a	a	k8xC	a
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
do	do	k7c2	do
první	první	k4xOgFnSc2	první
skupiny	skupina	k1gFnSc2	skupina
euroasijské	euroasijský	k2eAgFnSc2d1	euroasijská
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
Palermu	Palermo	k1gNnSc6	Palermo
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
s	s	k7c7	s
Britem	Brit	k1gMnSc7	Brit
Andy	Anda	k1gFnSc2	Anda
Murrayem	Murrayem	k1gInSc1	Murrayem
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
turnajem	turnaj	k1gInSc7	turnaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
odehrál	odehrát	k5eAaPmAgMnS	odehrát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
premiérový	premiérový	k2eAgInSc1d1	premiérový
zisk	zisk	k1gInSc1	zisk
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
Masters	Masters	k1gInSc4	Masters
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
po	po	k7c6	po
grandslamu	grandslam	k1gInSc2	grandslam
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
titulem	titul	k1gInSc7	titul
nejprve	nejprve	k6eAd1	nejprve
přehrál	přehrát	k5eAaPmAgInS	přehrát
krajana	krajan	k1gMnSc4	krajan
Jiřího	Jiří	k1gMnSc4	Jiří
Nováka	Novák	k1gMnSc4	Novák
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
dvojku	dvojka	k1gFnSc4	dvojka
Guillerma	Guillerm	k1gMnSc2	Guillerm
Coria	Corius	k1gMnSc2	Corius
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
přešel	přejít	k5eAaPmAgMnS	přejít
přes	přes	k7c4	přes
třináctého	třináctý	k4xOgMnSc4	třináctý
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
Španěla	Španěl	k1gMnSc4	Španěl
Juana	Juan	k1gMnSc4	Juan
Carlose	Carlosa	k1gFnSc6	Carlosa
Ferreru	Ferrer	k1gInSc6	Ferrer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
pak	pak	k6eAd1	pak
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
Argentince	Argentinec	k1gMnSc4	Argentinec
Gastóna	Gastón	k1gMnSc2	Gastón
Gaudia	gaudium	k1gNnSc2	gaudium
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
nepovolil	povolit	k5eNaPmAgMnS	povolit
ani	ani	k8xC	ani
jedinou	jediný	k2eAgFnSc4d1	jediná
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
jedničkou	jednička	k1gFnSc7	jednička
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
udolal	udolat	k5eAaPmAgMnS	udolat
šestého	šestý	k4xOgMnSc2	šestý
nasazeného	nasazený	k2eAgMnSc2d1	nasazený
Ivana	Ivan	k1gMnSc2	Ivan
Ljubičiće	Ljubičić	k1gMnSc2	Ljubičić
z	z	k7c2	z
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
v	v	k7c6	v
pěti	pět	k4xCc6	pět
sadách	sada	k1gFnPc6	sada
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2006	[number]	k4	2006
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
Next	Next	k2eAgInSc4d1	Next
Generation	Generation	k1gInSc4	Generation
Adelaide	Adelaid	k1gMnSc5	Adelaid
International	International	k1gMnSc5	International
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ovšem	ovšem	k9	ovšem
brzy	brzy	k6eAd1	brzy
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Sydney	Sydney	k1gNnSc4	Sydney
<g/>
,	,	kIx,	,
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
<g/>
,	,	kIx,	,
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
a	a	k8xC	a
Dubaji	Dubaj	k1gInSc6	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc1	Open
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Federerem	Federer	k1gMnSc7	Federer
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
na	na	k7c4	na
Gerry	Gerr	k1gInPc4	Gerr
Weber	Weber	k1gMnSc1	Weber
Open	Open	k1gInSc4	Open
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
finále	finále	k1gNnSc2	finále
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
prohrál	prohrát	k5eAaPmAgInS	prohrát
opět	opět	k6eAd1	opět
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Federerem	Federer	k1gMnSc7	Federer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tétýž	tétýž	k6eAd1	tétýž
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
na	na	k7c6	na
zbylých	zbylý	k2eAgInPc6d1	zbylý
dvou	dva	k4xCgInPc6	dva
grandslamech	grandslam	k1gInPc6	grandslam
<g/>
,	,	kIx,	,
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
a	a	k8xC	a
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
taktéž	taktéž	k?	taktéž
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Lucií	Lucie	k1gFnSc7	Lucie
Šafářovou	Šafářová	k1gFnSc7	Šafářová
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
také	také	k6eAd1	také
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
své	svůj	k3xOyFgNnSc4	svůj
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
umístění	umístění	k1gNnSc4	umístění
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
grandslamu	grandslam	k1gInSc2	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
tehdy	tehdy	k6eAd1	tehdy
3	[number]	k4	3
<g/>
.	.	kIx.	.
hráč	hráč	k1gMnSc1	hráč
světa	svět	k1gInSc2	svět
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Davyděnko	Davyděnka	k1gFnSc5	Davyděnka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
následujících	následující	k2eAgFnPc6d1	následující
brzkých	brzký	k2eAgFnPc6d1	brzká
prohrách	prohra	k1gFnPc6	prohra
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
Rotterdamu	Rotterdam	k1gInSc2	Rotterdam
<g/>
,	,	kIx,	,
Dubaji	Dubaje	k1gFnSc4	Dubaje
<g/>
,	,	kIx,	,
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
a	a	k8xC	a
Miami	Miami	k1gNnSc2	Miami
se	se	k3xPyFc4	se
na	na	k7c6	na
antukovém	antukový	k2eAgInSc6d1	antukový
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gMnSc6	Carl
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
ovšem	ovšem	k9	ovšem
opustil	opustit	k5eAaPmAgMnS	opustit
již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
Guillermo	Guillerma	k1gFnSc5	Guillerma
García-López	García-Lópeza	k1gFnPc2	García-Lópeza
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
získal	získat	k5eAaPmAgInS	získat
svůj	svůj	k3xOyFgInSc4	svůj
třetí	třetí	k4xOgInSc4	třetí
tiul	tiul	k1gInSc4	tiul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
ATP	atp	kA	atp
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Tomáše	Tomáš	k1gMnSc4	Tomáš
Berdycha	Berdych	k1gMnSc4	Berdych
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Zahájil	zahájit	k5eAaPmAgMnS	zahájit
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnSc5	International
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgInS	zahrát
si	se	k3xPyFc3	se
také	také	k6eAd1	také
finále	finále	k1gNnSc4	finále
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
French	Fren	k1gFnPc6	Fren
Open	Openo	k1gNnPc2	Openo
došel	dojít	k5eAaPmAgInS	dojít
až	až	k6eAd1	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
a	a	k8xC	a
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
ale	ale	k8xC	ale
porazil	porazit	k5eAaPmAgMnS	porazit
i	i	k9	i
Novaka	Novak	k1gMnSc4	Novak
Djokoviče	Djokovič	k1gMnSc4	Djokovič
(	(	kIx(	(
<g/>
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
)	)	kIx)	)
či	či	k8xC	či
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
(	(	kIx(	(
<g/>
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepovedlo	povést	k5eNaPmAgNnS	povést
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
grandslamového	grandslamový	k2eAgNnSc2d1	grandslamové
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
US	US	kA	US
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
opustil	opustit	k5eAaPmAgMnS	opustit
již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2012	[number]	k4	2012
zahájil	zahájit	k5eAaPmAgInS	zahájit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
účastí	účastí	k1gNnSc2	účastí
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
po	po	k7c6	po
boku	bok	k1gInSc6	bok
světové	světový	k2eAgFnSc2d1	světová
dvojky	dvojka	k1gFnSc2	dvojka
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozice	pozice	k1gFnPc4	pozice
prvního	první	k4xOgInSc2	první
nasazeného	nasazený	k2eAgInSc2d1	nasazený
týmu	tým	k1gInSc2	tým
24	[number]	k4	24
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
turnaje	turnaj	k1gInSc2	turnaj
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
mezistátního	mezistátní	k2eAgInSc2d1	mezistátní
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
postoupil	postoupit	k5eAaPmAgMnS	postoupit
Čech	Čech	k1gMnSc1	Čech
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
a	a	k8xC	a
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
svůj	svůj	k3xOyFgInSc4	svůj
výsledek	výsledek	k1gInSc4	výsledek
z	z	k7c2	z
posledního	poslední	k2eAgInSc2d1	poslední
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
již	již	k6eAd1	již
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
světovou	světový	k2eAgFnSc4d1	světová
dvojku	dvojka	k1gFnSc4	dvojka
<g/>
,	,	kIx,	,
Rafaela	Rafaela	k1gFnSc1	Rafaela
Nadala	nadat	k5eAaPmAgFnS	nadat
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
vyrovnaných	vyrovnaný	k2eAgInPc6d1	vyrovnaný
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
australského	australský	k2eAgNnSc2d1	Australské
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
čekaly	čekat	k5eAaImAgFnP	čekat
dva	dva	k4xCgInPc4	dva
halové	halový	k2eAgInPc4d1	halový
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
turnaji	turnaj	k1gInPc7	turnaj
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Montpellier	Montpellier	k1gInSc1	Montpellier
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgInS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přemohl	přemoct	k5eAaPmAgMnS	přemoct
Francouze	Francouz	k1gMnSc4	Francouz
Gaëla	Gaëlo	k1gNnSc2	Gaëlo
Monfilse	Monfilse	k1gFnSc2	Monfilse
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
tak	tak	k9	tak
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
halovém	halový	k2eAgInSc6d1	halový
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
český	český	k2eAgMnSc1d1	český
hráč	hráč	k1gMnSc1	hráč
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
prohrál	prohrát	k5eAaPmAgMnS	prohrát
jasně	jasně	k6eAd1	jasně
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
s	s	k7c7	s
Argentincem	Argentinec	k1gMnSc7	Argentinec
Del	Del	k1gMnSc7	Del
Potrem	Potr	k1gMnSc7	Potr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
turnajích	turnaj	k1gInPc6	turnaj
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
prohrál	prohrát	k5eAaPmAgInS	prohrát
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
již	již	k6eAd1	již
chystal	chystat	k5eAaImAgMnS	chystat
na	na	k7c4	na
daviscupové	daviscupový	k2eAgNnSc4d1	daviscupové
střetnutí	střetnutí	k1gNnSc4	střetnutí
českého	český	k2eAgInSc2d1	český
výběru	výběr	k1gInSc2	výběr
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dokázal	dokázat	k5eAaPmAgInS	dokázat
opanovat	opanovat	k5eAaPmF	opanovat
jak	jak	k6eAd1	jak
obě	dva	k4xCgFnPc1	dva
dvouhry	dvouhra	k1gFnPc1	dvouhra
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
také	také	k9	také
společně	společně	k6eAd1	společně
s	s	k7c7	s
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
i	i	k8xC	i
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
Česku	Česko	k1gNnSc6	Česko
postup	postup	k1gInSc4	postup
do	do	k7c2	do
zářijového	zářijový	k2eAgNnSc2d1	zářijové
semifinále	semifinále	k1gNnSc2	semifinále
proti	proti	k7c3	proti
Argentině	Argentina	k1gFnSc3	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
evropském	evropský	k2eAgInSc6d1	evropský
antukovém	antukový	k2eAgInSc6d1	antukový
turnaji	turnaj	k1gInSc6	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gMnSc6	Carl
došel	dojít	k5eAaPmAgInS	dojít
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c4	v
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
porazil	porazit	k5eAaPmAgMnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
čtyřku	čtyřka	k1gFnSc4	čtyřka
Andyho	Andy	k1gMnSc2	Andy
Murrayho	Murray	k1gMnSc2	Murray
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Semifinálový	semifinálový	k2eAgInSc1d1	semifinálový
zápas	zápas	k1gInSc1	zápas
se	s	k7c7	s
Srbem	Srb	k1gMnSc7	Srb
Djokovićem	Djoković	k1gMnSc7	Djoković
již	již	k6eAd1	již
Berdych	Berdycha	k1gFnPc2	Berdycha
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
skončil	skončit	k5eAaPmAgMnS	skončit
český	český	k2eAgMnSc1d1	český
hráč	hráč	k1gMnSc1	hráč
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
antukáři	antukář	k1gMnSc3	antukář
současnosti	současnost	k1gFnSc2	současnost
a	a	k8xC	a
pozdějšímu	pozdní	k2eAgMnSc3d2	pozdější
vítězi	vítěz	k1gMnSc3	vítěz
turnaje	turnaj	k1gInSc2	turnaj
Nadalovi	Nadal	k1gMnSc3	Nadal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
modré	modrý	k2eAgFnSc6d1	modrá
antuce	antuka	k1gFnSc6	antuka
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startoval	startovat	k5eAaBmAgInS	startovat
jako	jako	k9	jako
šestý	šestý	k4xOgMnSc1	šestý
nasazený	nasazený	k2eAgMnSc1d1	nasazený
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
porazit	porazit	k5eAaPmF	porazit
Andersona	Anderson	k1gMnSc4	Anderson
<g/>
,	,	kIx,	,
Monfilse	Monfils	k1gMnSc4	Monfils
<g/>
,	,	kIx,	,
Verdasca	Verdascus	k1gMnSc4	Verdascus
a	a	k8xC	a
Del	Del	k1gMnSc4	Del
Potra	Potr	k1gMnSc4	Potr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
jej	on	k3xPp3gNnSc2	on
udolala	udolat	k5eAaPmAgFnS	udolat
světová	světový	k2eAgFnSc1d1	světová
trojka	trojka	k1gFnSc1	trojka
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
a	a	k8xC	a
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
grandslamem	grandslam	k1gInSc7	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
French	French	k1gInSc1	French
Open	Open	k1gInSc1	Open
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
Berdych	Berdych	k1gMnSc1	Berdych
společně	společně	k6eAd1	společně
s	s	k7c7	s
krajanem	krajan	k1gMnSc7	krajan
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
oplatili	oplatit	k5eAaPmAgMnP	oplatit
porážku	porážka	k1gFnSc4	porážka
z	z	k7c2	z
daviscupového	daviscupový	k2eAgNnSc2d1	daviscupové
střetnutí	střetnutí	k1gNnSc2	střetnutí
tenisté	tenista	k1gMnPc1	tenista
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gMnSc1	Open
byl	být	k5eAaImAgMnS	být
sedmým	sedmý	k4xOgMnSc7	sedmý
nasazeným	nasazený	k2eAgMnSc7d1	nasazený
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
prošel	projít	k5eAaPmAgMnS	projít
první	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
kola	kolo	k1gNnPc4	kolo
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
,	,	kIx,	,
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
však	však	k9	však
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
Jihoafričanem	Jihoafričan	k1gMnSc7	Jihoafričan
Andersonem	Anderson	k1gMnSc7	Anderson
až	až	k8xS	až
v	v	k7c6	v
pěti	pět	k4xCc6	pět
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInPc7	jeho
síly	síla	k1gFnPc1	síla
Argentinec	Argentinec	k1gMnSc1	Argentinec
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Halle	Hall	k1gInSc6	Hall
<g/>
,	,	kIx,	,
přípravném	přípravný	k2eAgInSc6d1	přípravný
turnaji	turnaj	k1gInSc6	turnaj
před	před	k7c7	před
Wimbledonem	Wimbledon	k1gInSc7	Wimbledon
<g/>
,	,	kIx,	,
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
wimbledonským	wimbledonský	k2eAgInSc7d1	wimbledonský
turnajem	turnaj	k1gInSc7	turnaj
se	se	k3xPyFc4	se
však	však	k9	však
překvapivě	překvapivě	k6eAd1	překvapivě
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrál	prohrát	k5eAaPmAgMnS	prohrát
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
tiebreacích	tiebreace	k1gFnPc6	tiebreace
s	s	k7c7	s
Lotyšem	Lotyš	k1gMnSc7	Lotyš
Ernestsem	Ernests	k1gMnSc7	Ernests
Gulbisem	Gulbis	k1gInSc7	Gulbis
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
londýnský	londýnský	k2eAgInSc1d1	londýnský
olympijský	olympijský	k2eAgInSc1d1	olympijský
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
právě	právě	k9	právě
na	na	k7c6	na
dvorcích	dvorec	k1gInPc6	dvorec
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
nezastihl	zastihnout	k5eNaPmAgMnS	zastihnout
v	v	k7c6	v
optimální	optimální	k2eAgFnSc6d1	optimální
pohodě	pohoda	k1gFnSc6	pohoda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Belgičanem	Belgičan	k1gMnSc7	Belgičan
Darcisem	Darcis	k1gInSc7	Darcis
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c6	na
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Berdych	Berdych	k1gInSc4	Berdych
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
turnajích	turnaj	k1gInPc6	turnaj
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
série	série	k1gFnSc1	série
Masters	Masters	k1gInSc1	Masters
1000	[number]	k4	1000
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
i	i	k8xC	i
v	v	k7c6	v
Cincinatti	Cincinatť	k1gFnSc6	Cincinatť
skončil	skončit	k5eAaPmAgInS	skončit
již	již	k6eAd1	již
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c6	na
méně	málo	k6eAd2	málo
dotovaném	dotovaný	k2eAgInSc6d1	dotovaný
turnaji	turnaj	k1gInSc6	turnaj
ve	v	k7c4	v
Winston	Winston	k1gInSc4	Winston
Salemu	Salem	k1gInSc2	Salem
dokázal	dokázat	k5eAaPmAgMnS	dokázat
dojít	dojít	k5eAaPmF	dojít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
Američanem	Američan	k1gMnSc7	Američan
Isnerem	Isner	k1gMnSc7	Isner
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
třetího	třetí	k4xOgNnSc2	třetí
setu	set	k1gInSc6	set
tři	tři	k4xCgInPc4	tři
mečboly	mečbol	k1gInPc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
grandslam	grandslam	k1gInSc4	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows	Meadows	k1gInSc1	Meadows
jej	on	k3xPp3gMnSc4	on
zastihl	zastihnout	k5eAaPmAgInS	zastihnout
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nasazená	nasazený	k2eAgFnSc1d1	nasazená
šestka	šestka	k1gFnSc1	šestka
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
přehrál	přehrát	k5eAaPmAgMnS	přehrát
Belgičana	Belgičan	k1gMnSc4	Belgičan
Goffina	Goffin	k1gMnSc4	Goffin
<g/>
,	,	kIx,	,
následovali	následovat	k5eAaImAgMnP	následovat
Estonec	Estonec	k1gMnSc1	Estonec
Zopp	Zopp	k1gMnSc1	Zopp
<g/>
,	,	kIx,	,
Američan	Američan	k1gMnSc1	Američan
Querrey	Querrea	k1gFnSc2	Querrea
a	a	k8xC	a
Španěl	Španěly	k1gInPc2	Španěly
Almagro	Almagro	k1gNnSc1	Almagro
<g/>
.	.	kIx.	.
</s>
<s>
Set	set	k1gInSc1	set
ztratil	ztratit	k5eAaPmAgInS	ztratit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Querrym	Querrym	k1gInSc4	Querrym
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
ho	on	k3xPp3gInSc4	on
čekal	čekat	k5eAaImAgMnS	čekat
první	první	k4xOgMnSc1	první
nasazený	nasazený	k2eAgMnSc1d1	nasazený
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
<g/>
,	,	kIx,	,
Švýcar	Švýcar	k1gMnSc1	Švýcar
Federer	Federer	k1gMnSc1	Federer
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
po	po	k7c6	po
setech	set	k1gInPc6	set
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
postaral	postarat	k5eAaPmAgMnS	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
překvapení	překvapení	k1gNnSc4	překvapení
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
pak	pak	k6eAd1	pak
za	za	k7c2	za
větrného	větrný	k2eAgNnSc2d1	větrné
počasí	počasí	k1gNnSc2	počasí
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
turnajové	turnajový	k2eAgFnSc3d1	turnajová
trojce	trojka	k1gFnSc3	trojka
Murraymu	Murraym	k1gInSc2	Murraym
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Tomáše	Tomáš	k1gMnSc2	Tomáš
Berdycha	Berdych	k1gMnSc2	Berdych
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
Berdych	Berdych	k1gMnSc1	Berdych
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
smlouvy	smlouva	k1gFnSc2	smlouva
u	u	k7c2	u
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
Nike	Nike	k1gFnSc2	Nike
domluvil	domluvit	k5eAaPmAgInS	domluvit
na	na	k7c6	na
úzké	úzký	k2eAgFnSc6d1	úzká
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
švédskou	švédský	k2eAgFnSc7d1	švédská
oděvní	oděvní	k2eAgFnSc7d1	oděvní
společností	společnost	k1gFnSc7	společnost
H	H	kA	H
<g/>
&	&	k?	&
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
oblékat	oblékat	k5eAaImF	oblékat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
kurtu	kurt	k1gInSc6	kurt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
turnaj	turnaj	k1gInSc1	turnaj
odehrál	odehrát	k5eAaPmAgInS	odehrát
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
Čennaj	Čennaj	k1gInSc1	Čennaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
nasazená	nasazený	k2eAgFnSc1d1	nasazená
jednička	jednička	k1gFnSc1	jednička
nestačil	stačit	k5eNaBmAgInS	stačit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
na	na	k7c4	na
Španěla	Španěl	k1gMnSc4	Španěl
Roberto	Roberta	k1gFnSc5	Roberta
Bautistu	Bautista	k1gMnSc4	Bautista
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
padl	padnout	k5eAaImAgInS	padnout
s	s	k7c7	s
Novakem	Novak	k1gMnSc7	Novak
Djokovičem	Djokovič	k1gMnSc7	Djokovič
po	po	k7c6	po
setech	set	k1gInPc6	set
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
února	únor	k1gInSc2	únor
pak	pak	k6eAd1	pak
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Česko	Česko	k1gNnSc4	Česko
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
den	den	k1gInSc1	den
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
úvodní	úvodní	k2eAgFnSc4d1	úvodní
dvouhru	dvouhra	k1gFnSc4	dvouhra
proti	proti	k7c3	proti
Henrimu	Henrim	k1gMnSc3	Henrim
Laaksonenovi	Laaksonen	k1gMnSc3	Laaksonen
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
pak	pak	k6eAd1	pak
odehrál	odehrát	k5eAaPmAgInS	odehrát
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
zápas	zápas	k1gInSc1	zápas
tenisové	tenisový	k2eAgFnSc2d1	tenisová
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
zápas	zápas	k1gInSc1	zápas
Davis	Davis	k1gFnSc2	Davis
Cupu	cupat	k5eAaImIp1nS	cupat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
s	s	k7c7	s
Lukášem	Lukáš	k1gMnSc7	Lukáš
Rosolem	Rosol	k1gMnSc7	Rosol
porazil	porazit	k5eAaPmAgMnS	porazit
švýcarské	švýcarský	k2eAgMnPc4d1	švýcarský
hráče	hráč	k1gMnPc4	hráč
Stanislase	Stanislas	k1gInSc6	Stanislas
Wawrinku	Wawrink	k1gInSc6	Wawrink
a	a	k8xC	a
Marca	Marc	k2eAgMnSc4d1	Marc
Chiudinelliho	Chiudinelli	k1gMnSc4	Chiudinelli
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
po	po	k7c6	po
7	[number]	k4	7
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
2	[number]	k4	2
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
zápas	zápas	k1gInSc4	zápas
daviscupové	daviscupový	k2eAgFnSc2d1	daviscupová
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
tenisový	tenisový	k2eAgInSc4d1	tenisový
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
dva	dva	k4xCgInPc1	dva
turnaje	turnaj	k1gInPc1	turnaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
domácí	domácí	k2eAgMnPc4d1	domácí
hráče	hráč	k1gMnPc4	hráč
Jo-Wilfrieda	Jo-Wilfried	k1gMnSc2	Jo-Wilfried
Tsongu	Tsong	k1gInSc2	Tsong
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
prohrál	prohrát	k5eAaPmAgInS	prohrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
v	v	k7c6	v
Dubaji	Dubaj	k1gInSc6	Dubaj
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
prohrál	prohrát	k5eAaPmAgMnS	prohrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
turnajích	turnaj	k1gInPc6	turnaj
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
nejprve	nejprve	k6eAd1	nejprve
nestačil	stačit	k5eNaBmAgInS	stačit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
na	na	k7c4	na
pozdějšího	pozdní	k2eAgMnSc4d2	pozdější
vítěze	vítěz	k1gMnSc4	vítěz
Rafaela	Rafael	k1gMnSc4	Rafael
Nadala	nadat	k5eAaPmAgFnS	nadat
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Francouze	Francouz	k1gMnSc4	Francouz
Richarda	Richard	k1gMnSc4	Richard
Gasqueta	Gasquet	k1gMnSc4	Gasquet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
antukovém	antukový	k2eAgInSc6d1	antukový
turnaji	turnaj	k1gInSc6	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carlo	k1gNnSc6	Carlo
nestačil	stačit	k5eNaBmAgInS	stačit
ve	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
na	na	k7c4	na
italského	italský	k2eAgMnSc4d1	italský
tenistu	tenista	k1gMnSc4	tenista
Fabia	fabia	k1gFnSc1	fabia
Fogniniho	Fognini	k1gMnSc2	Fognini
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
antukovém	antukový	k2eAgInSc6d1	antukový
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
došel	dojít	k5eAaPmAgInS	dojít
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
Španěl	Španěl	k1gMnSc1	Španěl
Tommy	Tomma	k1gFnSc2	Tomma
Robredo	Robredo	k1gNnSc1	Robredo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
došel	dojít	k5eAaPmAgInS	dojít
až	až	k6eAd1	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
dvou	dva	k4xCgInPc2	dva
turnajů	turnaj	k1gInPc2	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
Masters	Masters	k1gInSc4	Masters
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
ho	on	k3xPp3gInSc4	on
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
Švýcar	Švýcar	k1gMnSc1	Švýcar
Stan	stan	k1gInSc4	stan
Wawrinka	Wawrinka	k1gFnSc1	Wawrinka
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6-4	[number]	k4	6-4
a	a	k8xC	a
4-6	[number]	k4	4-6
a	a	k8xC	a
na	na	k7c4	na
italskému	italský	k2eAgInSc3d1	italský
turnaji	turnaj	k1gInSc3	turnaj
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Rafaela	Rafael	k1gMnSc4	Rafael
Nadala	nadat	k5eAaPmAgFnS	nadat
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
grandslamu	grandslam	k1gInSc2	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
jako	jako	k9	jako
pátý	pátý	k4xOgMnSc1	pátý
nasazený	nasazený	k2eAgMnSc1d1	nasazený
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
ho	on	k3xPp3gNnSc4	on
čekal	čekat	k5eAaImAgMnS	čekat
těžký	těžký	k2eAgMnSc1d1	těžký
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgMnSc1d1	domácí
hráč	hráč	k1gMnSc1	hráč
Gaël	Gaëla	k1gFnPc2	Gaëla
Monfils	Monfils	k1gInSc1	Monfils
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
mu	on	k3xPp3gNnSc3	on
nakonec	nakonec	k6eAd1	nakonec
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
po	po	k7c6	po
pěti	pět	k4xCc6	pět
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7-6	[number]	k4	7-6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
změnil	změnit	k5eAaPmAgInS	změnit
přípravný	přípravný	k2eAgInSc1d1	přípravný
turnaj	turnaj	k1gInSc1	turnaj
na	na	k7c4	na
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
německého	německý	k2eAgNnSc2d1	německé
Halle	Halle	k1gNnSc2	Halle
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c6	na
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c4	v
Queens	Queens	k1gInSc4	Queens
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
nasazené	nasazený	k2eAgFnSc2d1	nasazená
dvojky	dvojka	k1gFnSc2	dvojka
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
loňského	loňský	k2eAgMnSc4d1	loňský
vítěze	vítěz	k1gMnSc4	vítěz
Chorvata	Chorvat	k1gMnSc2	Chorvat
Marina	Marina	k1gFnSc1	Marina
Čiliće	Čiliće	k1gFnSc1	Čiliće
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
jako	jako	k9	jako
sedmý	sedmý	k4xOgMnSc1	sedmý
nasazený	nasazený	k2eAgMnSc1d1	nasazený
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
porazit	porazit	k5eAaPmF	porazit
Martina	Martin	k1gMnSc2	Martin
Kližana	Kližan	k1gMnSc2	Kližan
<g/>
,	,	kIx,	,
Daniela	Daniel	k1gMnSc2	Daniel
Brandse	Brands	k1gMnSc2	Brands
<g/>
,	,	kIx,	,
Kevina	Kevin	k1gMnSc2	Kevin
Andersona	Anderson	k1gMnSc2	Anderson
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bernarda	Bernard	k1gMnSc4	Bernard
Tomice	Tomic	k1gMnSc4	Tomic
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
jedničku	jednička	k1gFnSc4	jednička
Novaka	Novak	k1gMnSc2	Novak
Djokoviće	Djoković	k1gMnSc2	Djoković
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
prohrál	prohrát	k5eAaPmAgInS	prohrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
4-6	[number]	k4	4-6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
nasazené	nasazený	k2eAgFnSc2d1	nasazená
jedničky	jednička	k1gFnSc2	jednička
startoval	startovat	k5eAaBmAgInS	startovat
také	také	k9	také
na	na	k7c6	na
švédském	švédský	k2eAgInSc6d1	švédský
antukovém	antukový	k2eAgInSc6d1	antukový
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Bå	Bå	k1gFnSc6	Bå
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
síly	síla	k1gFnPc4	síla
Thiemo	Thiema	k1gFnSc5	Thiema
de	de	k?	de
Bakker	Bakkra	k1gFnPc2	Bakkra
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
startoval	startovat	k5eAaBmAgInS	startovat
jako	jako	k9	jako
nasazená	nasazený	k2eAgFnSc1d1	nasazená
šestka	šestka	k1gFnSc1	šestka
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Cincinnatti	Cincinnatť	k1gFnSc6	Cincinnatť
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokázal	dokázat	k5eAaPmAgMnS	dokázat
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
porazit	porazit	k5eAaPmF	porazit
olympijského	olympijský	k2eAgMnSc4d1	olympijský
vítěze	vítěz	k1gMnSc4	vítěz
a	a	k8xC	a
wimbledonského	wimbledonský	k2eAgMnSc4d1	wimbledonský
šampiona	šampion	k1gMnSc4	šampion
Andyho	Andy	k1gMnSc2	Andy
Murrayho	Murray	k1gMnSc2	Murray
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
však	však	k9	však
po	po	k7c6	po
boji	boj	k1gInSc6	boj
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
španěla	španěl	k1gMnSc4	španěl
Rafaela	Rafael	k1gMnSc4	Rafael
Nadala	nadat	k5eAaPmAgFnS	nadat
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
se	se	k3xPyFc4	se
však	však	k9	však
posunul	posunout	k5eAaPmAgInS	posunout
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
odsunul	odsunout	k5eAaPmAgInS	odsunout
švýcara	švýcar	k1gMnSc4	švýcar
Rogera	Roger	k1gMnSc4	Roger
Federera	Federer	k1gMnSc4	Federer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
US	US	kA	US
Open	Open	k1gInSc1	Open
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
coby	coby	k?	coby
nasazená	nasazený	k2eAgFnSc1d1	nasazená
pětka	pětka	k1gFnSc1	pětka
<g/>
,	,	kIx,	,
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
však	však	k9	však
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Švýcara	Švýcar	k1gMnSc4	Švýcar
Stanislase	Stanislasa	k1gFnSc6	Stanislasa
Wawrinku	Wawrinka	k1gFnSc4	Wawrinka
a	a	k8xC	a
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
si	se	k3xPyFc3	se
tak	tak	k9	tak
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
bilanci	bilance	k1gFnSc4	bilance
s	s	k7c7	s
desátým	desátý	k4xOgMnSc7	desátý
hráčem	hráč	k1gMnSc7	hráč
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
k	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
na	na	k7c4	na
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
k	k	k7c3	k
semifinálovému	semifinálový	k2eAgNnSc3d1	semifinálové
utkání	utkání	k1gNnSc3	utkání
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
proti	proti	k7c3	proti
Argentině	Argentina	k1gFnSc3	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
postoupil	postoupit	k5eAaPmAgInS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
a	a	k8xC	a
Berdych	Berdych	k1gInSc1	Berdych
týmu	tým	k1gInSc2	tým
přispěl	přispět	k5eAaPmAgInS	přispět
bodem	bod	k1gInSc7	bod
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
a	a	k8xC	a
půl	půl	k6eAd1	půl
bodem	bod	k1gInSc7	bod
ze	z	k7c2	z
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
opět	opět	k6eAd1	opět
s	s	k7c7	s
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ale	ale	k9	ale
v	v	k7c6	v
loňském	loňský	k2eAgNnSc6d1	loňské
semifinále	semifinále	k1gNnSc6	semifinále
odehrál	odehrát	k5eAaPmAgInS	odehrát
o	o	k7c4	o
zápas	zápas	k1gInSc4	zápas
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
neobhájil	obhájit	k5eNaPmAgInS	obhájit
potřebné	potřebný	k2eAgInPc4d1	potřebný
body	bod	k1gInPc4	bod
a	a	k8xC	a
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
ATP.	atp.	kA	atp.
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
turnaji	turnaj	k1gInSc6	turnaj
asijského	asijský	k2eAgNnSc2d1	asijské
turné	turné	k1gNnSc2	turné
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c6	na
Milose	Milosa	k1gFnSc6	Milosa
Raonice	Raonice	k1gFnSc2	Raonice
a	a	k8xC	a
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
a	a	k8xC	a
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turnaji	turnaj	k1gInSc6	turnaj
série	série	k1gFnSc2	série
Masters	Mastersa	k1gFnPc2	Mastersa
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
skončil	skončit	k5eAaPmAgInS	skončit
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
argentince	argentinka	k1gFnSc6	argentinka
Nicoláse	Nicoláse	k1gFnSc1	Nicoláse
Almagra	Almagr	k1gInSc2	Almagr
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Ivu	Ivo	k1gMnSc3	Ivo
Karlovićovi	Karlovića	k1gMnSc3	Karlovića
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
výsledkem	výsledek	k1gInSc7	výsledek
si	se	k3xPyFc3	se
však	však	k9	však
zajistil	zajistit	k5eAaPmAgMnS	zajistit
4	[number]	k4	4
<g/>
.	.	kIx.	.
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
vstupoval	vstupovat	k5eAaImAgMnS	vstupovat
jako	jako	k8xC	jako
pátý	pátý	k4xOgMnSc1	pátý
nasazený	nasazený	k2eAgMnSc1d1	nasazený
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
neoblíbeným	oblíbený	k2eNgInSc7d1	neoblíbený
Stanislasem	Stanislas	k1gInSc7	Stanislas
Wawrinkou	Wawrinka	k1gFnSc7	Wawrinka
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
sice	sice	k8xC	sice
porazil	porazit	k5eAaPmAgMnS	porazit
Davida	David	k1gMnSc4	David
Ferrera	Ferrero	k1gNnSc2	Ferrero
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
prohře	prohra	k1gFnSc6	prohra
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
jedničkou	jednička	k1gFnSc7	jednička
Rafaelem	Rafael	k1gMnSc7	Rafael
Nadalem	Nadal	k1gMnSc7	Nadal
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
turnajem	turnaj	k1gInSc7	turnaj
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
tak	tak	k6eAd1	tak
spadl	spadnout	k5eAaPmAgInS	spadnout
na	na	k7c4	na
7	[number]	k4	7
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
víkend	víkend	k1gInSc1	víkend
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
finále	finále	k1gNnSc1	finále
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Srby	Srb	k1gMnPc7	Srb
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
i	i	k9	i
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
vítěz	vítěz	k1gMnSc1	vítěz
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
Novak	Novak	k1gMnSc1	Novak
Djoković	Djoković	k1gMnSc1	Djoković
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sice	sice	k8xC	sice
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
obě	dva	k4xCgFnPc4	dva
své	svůj	k3xOyFgFnSc2	svůj
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbylé	zbylý	k2eAgInPc1d1	zbylý
duely	duel	k1gInPc1	duel
patřily	patřit	k5eAaImAgInP	patřit
Čechům	Čech	k1gMnPc3	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Berdych	Berdych	k1gMnSc1	Berdych
porazil	porazit	k5eAaPmAgMnS	porazit
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
srbskou	srbský	k2eAgFnSc4d1	Srbská
dvojku	dvojka	k1gFnSc4	dvojka
Dušana	Dušan	k1gMnSc2	Dušan
Lajoviće	Lajović	k1gMnSc2	Lajović
hladce	hladko	k6eAd1	hladko
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ke	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
bodu	bod	k1gInSc3	bod
pak	pak	k6eAd1	pak
pomohl	pomoct	k5eAaPmAgMnS	pomoct
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
porazili	porazit	k5eAaPmAgMnP	porazit
srbský	srbský	k2eAgInSc4d1	srbský
pár	pár	k1gInSc4	pár
Ilija	Ilij	k2eAgFnSc1d1	Ilija
Bozoljac	Bozoljac	k1gFnSc1	Bozoljac
<g/>
,	,	kIx,	,
Nenad	Nenad	k1gInSc1	Nenad
Zimonjić	Zimonjić	k1gFnSc1	Zimonjić
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
sice	sice	k8xC	sice
Berdych	Berdych	k1gMnSc1	Berdych
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
Djokovićem	Djoković	k1gInSc7	Djoković
hladce	hladko	k6eAd1	hladko
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
a	a	k8xC	a
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
bod	bod	k1gInSc1	bod
však	však	k9	však
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
získal	získat	k5eAaPmAgMnS	získat
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
tak	tak	k6eAd1	tak
slavil	slavit	k5eAaImAgMnS	slavit
obhajobu	obhajoba	k1gFnSc4	obhajoba
nejcennější	cenný	k2eAgFnSc2d3	nejcennější
týmové	týmový	k2eAgFnSc2d1	týmová
tenisové	tenisový	k2eAgFnSc2d1	tenisová
trofeje	trofej	k1gFnSc2	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
sezónu	sezóna	k1gFnSc4	sezóna
zahájil	zahájit	k5eAaPmAgMnS	zahájit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Chorvatu	Chorvat	k1gMnSc3	Chorvat
Ivu	Ivo	k1gMnSc3	Ivo
Karlovićovi	Karlovića	k1gMnSc3	Karlovića
dvakrát	dvakrát	k6eAd1	dvakrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Hájkem	Hájek	k1gMnSc7	Hájek
však	však	k9	však
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazili	porazit	k5eAaPmAgMnP	porazit
deblové	deblový	k2eAgMnPc4d1	deblový
specialisty	specialista	k1gMnPc4	specialista
Peyu	Peyus	k1gInSc2	Peyus
se	s	k7c7	s
Soaresem	Soares	k1gInSc7	Soares
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zamířil	zamířit	k5eAaPmAgMnS	zamířit
na	na	k7c4	na
exhibiční	exhibiční	k2eAgInSc4d1	exhibiční
turnaj	turnaj	k1gInSc4	turnaj
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Keiu	Kei	k1gMnSc3	Kei
Nišikorovi	Nišikor	k1gMnSc3	Nišikor
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
semifinále	semifinále	k1gNnSc2	semifinále
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
Nedovjesova	Nedovjesův	k2eAgMnSc2d1	Nedovjesův
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
De	De	k?	De
Schepperem	Schepper	k1gInSc7	Schepper
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Džumhura	Džumhur	k1gMnSc2	Džumhur
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
Kevina	Kevin	k2eAgMnSc4d1	Kevin
Andresona	Andreson	k1gMnSc4	Andreson
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Jihoafričanově	Jihoafričanův	k2eAgNnSc6d1	Jihoafričanův
vyřazení	vyřazení	k1gNnSc6	vyřazení
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
ztratil	ztratit	k5eAaPmAgMnS	ztratit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Ferrerem	Ferrer	k1gMnSc7	Ferrer
a	a	k8xC	a
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vítězem	vítěz	k1gMnSc7	vítěz
Stanislasem	Stanislasma	k1gFnPc2	Stanislasma
Wawrinkou	Wawrinký	k2eAgFnSc4d1	Wawrinký
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
souboj	souboj	k1gInSc4	souboj
pro	pro	k7c4	pro
Berdycha	Berdych	k1gMnSc4	Berdych
znamenal	znamenat	k5eAaImAgMnS	znamenat
prohru	prohra	k1gFnSc4	prohra
poměrem	poměr	k1gInSc7	poměr
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
první	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yRgNnSc4	který
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
postoupil	postoupit	k5eAaPmAgMnS	postoupit
výsledkem	výsledek	k1gInSc7	výsledek
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
páteční	páteční	k2eAgFnSc6d1	páteční
dvouhře	dvouhra	k1gFnSc6	dvouhra
si	se	k3xPyFc3	se
Berdych	Berdych	k1gMnSc1	Berdych
poradil	poradit	k5eAaPmAgMnS	poradit
s	s	k7c7	s
Igorem	Igor	k1gMnSc7	Igor
Sijslingem	Sijsling	k1gInSc7	Sijsling
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
však	však	k9	však
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
nestačil	stačit	k5eNaBmAgMnS	stačit
na	na	k7c4	na
Haaseho	Haase	k1gMnSc4	Haase
<g/>
.	.	kIx.	.
</s>
<s>
Sobotní	sobotní	k2eAgFnSc4d1	sobotní
vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
nakonec	nakonec	k6eAd1	nakonec
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Berdych	Berdych	k1gMnSc1	Berdych
se	s	k7c7	s
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
<g/>
,	,	kIx,	,
když	když	k8xS	když
zdolali	zdolat	k5eAaPmAgMnP	zdolat
pár	pár	k4xCyI	pár
Rojer	Rojra	k1gFnPc2	Rojra
a	a	k8xC	a
Haase	Haase	k1gFnSc1	Haase
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nedělního	nedělní	k2eAgInSc2d1	nedělní
singlu	singl	k1gInSc2	singl
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
náhradník	náhradník	k1gMnSc1	náhradník
Thiem	Thi	k1gMnSc7	Thi
De	De	k?	De
Bakker	Bakker	k1gMnSc1	Bakker
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Berdych	Berdych	k1gMnSc1	Berdych
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
přehrál	přehrát	k5eAaPmAgInS	přehrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
mezistátního	mezistátní	k2eAgNnSc2d1	mezistátní
utkání	utkání	k1gNnSc2	utkání
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
již	již	k6eAd1	již
v	v	k7c6	v
Davisově	Davisův	k2eAgInSc6d1	Davisův
poháru	pohár	k1gInSc6	pohár
nenastoupí	nastoupit	k5eNaPmIp3nS	nastoupit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
individuální	individuální	k2eAgFnSc4d1	individuální
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
formu	forma	k1gFnSc4	forma
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
na	na	k7c4	na
Rotterdam	Rotterdam	k1gInSc4	Rotterdam
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
Andreasem	Andreas	k1gInSc7	Andreas
Seppim	Seppim	k1gInSc1	Seppim
dvakrát	dvakrát	k6eAd1	dvakrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
přehrál	přehrát	k5eAaPmAgInS	přehrát
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Mahuta	Mahut	k1gMnSc2	Mahut
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c6	o
semifinále	semifinále	k1gNnSc6	semifinále
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nestačil	stačit	k5eNaBmAgInS	stačit
Polák	polák	k1gInSc1	polák
Jerzy	Jerza	k1gFnSc2	Jerza
Janowicz	Janowicza	k1gFnPc2	Janowicza
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtyřkou	čtyřka	k1gFnSc7	čtyřka
hráčů	hráč	k1gMnPc2	hráč
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
raketě	raketa	k1gFnSc6	raketa
Lotyš	Lotyš	k1gMnSc1	Lotyš
Ernests	Ernestsa	k1gFnPc2	Ernestsa
Gulbis	Gulbis	k1gFnSc4	Gulbis
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šestnácti	šestnáct	k4xCc6	šestnáct
měsících	měsíc	k1gInPc6	měsíc
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
singlové	singlový	k2eAgNnSc4d1	singlové
finále	finále	k1gNnSc4	finále
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
poradil	poradit	k5eAaPmAgMnS	poradit
s	s	k7c7	s
Chorvatem	Chorvat	k1gMnSc7	Chorvat
Marinem	Marin	k1gMnSc7	Marin
Čilićem	Čilić	k1gMnSc7	Čilić
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
posun	posun	k1gInSc4	posun
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
před	před	k7c4	před
Andyho	Andy	k1gMnSc4	Andy
Murrayho	Murray	k1gMnSc4	Murray
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgInPc1d1	dobrý
výkony	výkon	k1gInPc1	výkon
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
také	také	k9	také
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
přehrál	přehrát	k5eAaPmAgMnS	přehrát
kvalifikanta	kvalifikant	k1gMnSc4	kvalifikant
Copila	Copil	k1gMnSc4	Copil
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
pak	pak	k6eAd1	pak
porazil	porazit	k5eAaPmAgMnS	porazit
Sergeje	Sergej	k1gMnSc4	Sergej
Stachovského	Stachovský	k2eAgMnSc2d1	Stachovský
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
jej	on	k3xPp3gMnSc4	on
čekal	čekat	k5eAaImAgInS	čekat
těžší	těžký	k2eAgInSc1d2	těžší
úkol	úkol	k1gInSc1	úkol
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Francouze	Francouz	k1gMnPc4	Francouz
Tsongy	Tsong	k1gInPc7	Tsong
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zdolat	zdolat	k5eAaPmF	zdolat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
rozjetého	rozjetý	k2eAgMnSc4d1	rozjetý
Němce	Němec	k1gMnSc4	Němec
Philippa	Philipp	k1gMnSc4	Philipp
Kohlschreibera	Kohlschreiber	k1gMnSc4	Kohlschreiber
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgMnSc3	jenž
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
koncovky	koncovka	k1gFnPc4	koncovka
obou	dva	k4xCgFnPc2	dva
sad	sada	k1gFnPc2	sada
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
však	však	k9	však
nenašel	najít	k5eNaPmAgInS	najít
recept	recept	k1gInSc1	recept
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
osmičku	osmička	k1gFnSc4	osmička
Rogera	Roger	k1gMnSc2	Roger
Federera	Federer	k1gMnSc2	Federer
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
po	po	k7c6	po
třísetové	třísetový	k2eAgFnSc6d1	třísetová
bitvě	bitva	k1gFnSc6	bitva
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
výhodu	výhod	k1gInSc2	výhod
prolomeného	prolomený	k2eAgNnSc2d1	prolomené
podání	podání	k1gNnSc2	podání
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
gamů	game	k1gInPc2	game
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vnásledně	vnásledně	k6eAd1	vnásledně
pondělní	pondělní	k2eAgFnSc6d1	pondělní
klasifikaci	klasifikace	k1gFnSc6	klasifikace
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
kariérní	kariérní	k2eAgNnSc4d1	kariérní
maximum	maximum	k1gNnSc4	maximum
<g/>
,	,	kIx,	,
když	když	k8xS	když
figuroval	figurovat	k5eAaImAgInS	figurovat
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
rozehrál	rozehrát	k5eAaPmAgInS	rozehrát
lednovým	lednový	k2eAgInSc7d1	lednový
turnajem	turnaj	k1gInSc7	turnaj
Qatar	Qatar	k1gMnSc1	Qatar
ExxonMobil	ExxonMobil	k1gMnSc1	ExxonMobil
Open	Open	k1gMnSc1	Open
v	v	k7c4	v
Dauhá	Dauhý	k2eAgNnPc4d1	Dauhý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
nasazený	nasazený	k2eAgMnSc1d1	nasazený
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Davidu	David	k1gMnSc3	David
Ferrerovi	Ferrer	k1gMnSc3	Ferrer
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
..	..	k?	..
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
přes	přes	k7c4	přes
třetího	třetí	k4xOgMnSc4	třetí
hráče	hráč	k1gMnSc4	hráč
světa	svět	k1gInSc2	svět
Rafaela	Rafaela	k1gFnSc1	Rafaela
Nadala	nadat	k5eAaPmAgFnS	nadat
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Ukončil	ukončit	k5eAaPmAgMnS	ukončit
tak	tak	k9	tak
sérii	série	k1gFnSc4	série
sedmnácti	sedmnáct	k4xCc2	sedmnáct
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
proher	prohra	k1gFnPc2	prohra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
když	když	k8xS	když
naposledy	naposledy	k6eAd1	naposledy
předtím	předtím	k6eAd1	předtím
Španěla	Španěl	k1gMnSc4	Španěl
porazil	porazit	k5eAaPmAgInS	porazit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Madrid	Madrid	k1gInSc1	Madrid
Masters	Masters	k1gInSc1	Masters
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
snížil	snížit	k5eAaPmAgMnS	snížit
pasivní	pasivní	k2eAgFnSc4d1	pasivní
bilanci	bilance	k1gFnSc4	bilance
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
na	na	k7c4	na
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
ze	z	k7c2	z
zápasu	zápas	k1gInSc2	zápas
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	poražen	k2eAgMnSc1d1	poražen
i	i	k9	i
po	po	k7c6	po
osmnácté	osmnáctý	k4xOgFnSc6	osmnáctý
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
to	ten	k3xDgNnSc4	ten
nový	nový	k2eAgInSc4d1	nový
mužský	mužský	k2eAgInSc4d1	mužský
rekord	rekord	k1gInSc4	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS	nadat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
utržil	utržit	k5eAaPmAgInS	utržit
třetího	třetí	k4xOgNnSc2	třetí
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
své	svůj	k3xOyFgFnSc2	svůj
grandslamové	grandslamový	k2eAgFnSc2d1	grandslamová
kariéry	kariéra	k1gFnSc2	kariéra
a	a	k8xC	a
prvního	první	k4xOgInSc2	první
od	od	k7c2	od
wimbledonského	wimbledonský	k2eAgNnSc2d1	wimbledonské
finále	finále	k1gNnSc2	finále
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Berdych	Berdych	k1gInSc1	Berdych
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
zahrál	zahrát	k5eAaPmAgMnS	zahrát
10	[number]	k4	10
es	es	k1gNnPc4	es
<g/>
,	,	kIx,	,
46	[number]	k4	46
vítězných	vítězný	k2eAgInPc2d1	vítězný
úderů	úder	k1gInPc2	úder
<g/>
,	,	kIx,	,
21	[number]	k4	21
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
proměnil	proměnit	k5eAaPmAgMnS	proměnit
pět	pět	k4xCc1	pět
z	z	k7c2	z
deseti	deset	k4xCc2	deset
breakových	breakový	k2eAgFnPc2d1	breaková
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
nedopustil	dopustit	k5eNaPmAgMnS	dopustit
se	se	k3xPyFc4	se
žádné	žádný	k3yNgFnSc2	žádný
dvojchyby	dvojchyba	k1gFnSc2	dvojchyba
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
naopak	naopak	k6eAd1	naopak
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
3	[number]	k4	3
esa	eso	k1gNnSc2	eso
<g/>
,	,	kIx,	,
6	[number]	k4	6
dvojchyb	dvojchyba	k1gFnPc2	dvojchyba
<g/>
,	,	kIx,	,
24	[number]	k4	24
winnerů	winner	k1gMnPc2	winner
<g/>
,	,	kIx,	,
26	[number]	k4	26
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
nabídek	nabídka	k1gFnPc2	nabídka
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
neprolomila	prolomit	k5eNaPmAgFnS	prolomit
podání	podání	k1gNnSc3	podání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
semifinále	semifinále	k1gNnSc6	semifinále
ho	on	k3xPp3gMnSc4	on
čekal	čekat	k5eAaImAgMnS	čekat
Brit	Brit	k1gMnSc1	Brit
Andy	Anda	k1gFnSc2	Anda
Murray	Murraa	k1gFnSc2	Murraa
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
svěřenec	svěřenec	k1gMnSc1	svěřenec
Berdychova	Berdychův	k2eAgMnSc2d1	Berdychův
kouče	kouč	k1gMnSc2	kouč
Daniela	Daniel	k1gMnSc2	Daniel
Vallverdeho	Vallverde	k1gMnSc2	Vallverde
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
hráč	hráč	k1gMnSc1	hráč
měl	mít	k5eAaImAgMnS	mít
před	před	k7c7	před
duelem	duel	k1gInSc7	duel
kladnou	kladný	k2eAgFnSc4d1	kladná
bilanci	bilance	k1gFnSc4	bilance
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
se	s	k7c7	s
skórem	skór	k1gInSc7	skór
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
když	když	k8xS	když
poslední	poslední	k2eAgInSc1d1	poslední
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Cincinnati	Cincinnati	k1gFnPc3	Cincinnati
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
začal	začít	k5eAaPmAgInS	začít
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
tenistu	tenista	k1gMnSc4	tenista
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
napínavé	napínavý	k2eAgFnSc6d1	napínavá
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
hře	hra	k1gFnSc6	hra
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
set	set	k1gInSc4	set
nejtěsnějším	těsný	k2eAgInSc7d3	nejtěsnější
poměrem	poměr	k1gInSc7	poměr
míčů	míč	k1gInPc2	míč
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Murrayho	Murrayze	k6eAd1	Murrayze
však	však	k9	však
ztracený	ztracený	k2eAgInSc4d1	ztracený
set	set	k1gInSc4	set
nesrazil	srazit	k5eNaPmAgInS	srazit
<g/>
,	,	kIx,	,
ba	ba	k9	ba
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
set	set	k1gInSc4	set
jasně	jasně	k6eAd1	jasně
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
a	a	k8xC	a
za	za	k7c4	za
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
uštědřil	uštědřit	k5eAaPmAgMnS	uštědřit
Berdychovy	Berdychův	k2eAgFnPc1d1	Berdychova
kanára	kanár	k1gMnSc4	kanár
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
3	[number]	k4	3
<g/>
.	.	kIx.	.
set	set	k1gInSc4	set
díky	díky	k7c3	díky
jednomu	jeden	k4xCgInSc3	jeden
využitému	využitý	k2eAgInSc3d1	využitý
brejku	brejk	k1gInSc2	brejk
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
britský	britský	k2eAgMnSc1d1	britský
tenista	tenista	k1gMnSc1	tenista
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
blízko	blízko	k7c2	blízko
postupu	postup	k1gInSc2	postup
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
finálového	finálový	k2eAgNnSc2d1	finálové
utkání	utkání	k1gNnSc2	utkání
na	na	k7c6	na
australském	australský	k2eAgInSc6d1	australský
grandslamu	grandslam	k1gInSc6	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
setu	set	k1gInSc2	set
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Berdych	Berdych	k1gInSc1	Berdych
herně	herna	k1gFnSc3	herna
zlepšoval	zlepšovat	k5eAaImAgInS	zlepšovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
nevyužil	využít	k5eNaPmAgMnS	využít
ani	ani	k9	ani
jen	jen	k9	jen
z	z	k7c2	z
nabídnutých	nabídnutý	k2eAgInPc2d1	nabídnutý
dvou	dva	k4xCgInPc2	dva
brejkbolů	brejkbol	k1gInPc2	brejkbol
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
podepsal	podepsat	k5eAaPmAgMnS	podepsat
ortel	ortel	k1gInSc4	ortel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
a	a	k8xC	a
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
nejprve	nejprve	k6eAd1	nejprve
udělal	udělat	k5eAaPmAgInS	udělat
dvojchybou	dvojchyba	k1gFnSc7	dvojchyba
a	a	k8xC	a
následně	následně	k6eAd1	následně
zkazil	zkazit	k5eAaPmAgMnS	zkazit
bekhendový	bekhendový	k2eAgInSc4d1	bekhendový
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
svoje	svůj	k3xOyFgNnSc4	svůj
podání	podání	k1gNnSc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Murray	Murraa	k1gFnPc1	Murraa
pak	pak	k6eAd1	pak
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
servisu	servis	k1gInSc6	servis
duel	duel	k1gInSc1	duel
dokončil	dokončit	k5eAaPmAgMnS	dokončit
čistou	čistý	k2eAgFnSc7d1	čistá
hrou	hra	k1gFnSc7	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozápasové	pozápasový	k2eAgFnSc6d1	pozápasová
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
litoval	litovat	k5eAaImAgMnS	litovat
Berdych	Berdych	k1gMnSc1	Berdych
prohry	prohra	k1gFnSc2	prohra
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
opravdu	opravdu	k6eAd1	opravdu
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
a	a	k8xC	a
bez	bez	k7c2	bez
nálady	nálada	k1gFnSc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
hrozně	hrozně	k6eAd1	hrozně
zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
tenhle	tenhle	k3xDgInSc4	tenhle
zápas	zápas	k1gInSc4	zápas
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Musím	muset	k5eAaImIp1nS	muset
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
silnější	silný	k2eAgFnSc1d2	silnější
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
přijel	přijet	k5eAaPmAgInS	přijet
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
ročník	ročník	k1gInSc4	ročník
šenčenského	šenčenský	k2eAgNnSc2d1	šenčenský
ATP	atp	kA	atp
Shenzhen	Shenzhen	k2eAgInSc4d1	Shenzhen
Open	Open	k1gInSc4	Open
v	v	k7c6	v
roli	role	k1gFnSc6	role
světové	světový	k2eAgFnSc2d1	světová
pětky	pětka	k1gFnSc2	pětka
a	a	k8xC	a
nejvýše	vysoce	k6eAd3	vysoce
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
hráče	hráč	k1gMnSc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
favorila	favoril	k1gMnSc2	favoril
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
pavoukem	pavouk	k1gMnSc7	pavouk
neztratil	ztratit	k5eNaPmAgMnS	ztratit
žádný	žádný	k3yNgInSc4	žádný
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gMnSc1	Tour
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
prostějovským	prostějovský	k2eAgMnSc7d1	prostějovský
oddílovým	oddílový	k2eAgMnSc7d1	oddílový
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
Jiřím	Jiří	k1gMnSc7	Jiří
Veselým	Veselý	k1gMnSc7	Veselý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
odebral	odebrat	k5eAaPmAgInS	odebrat
pět	pět	k4xCc4	pět
gamů	game	k1gInPc2	game
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
třetího	třetí	k4xOgMnSc4	třetí
nasazeného	nasazený	k2eAgMnSc4d1	nasazený
Španěla	Španěl	k1gMnSc4	Španěl
Tommyho	Tommy	k1gMnSc4	Tommy
Robreda	Robred	k1gMnSc4	Robred
a	a	k8xC	a
v	v	k7c6	v
odloženém	odložený	k2eAgNnSc6d1	odložené
pondělním	pondělní	k2eAgNnSc6d1	pondělní
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
španělskou	španělský	k2eAgFnSc4d1	španělská
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
čtyřku	čtyřka	k1gFnSc4	čtyřka
Guillerma	Guillermum	k1gNnSc2	Guillermum
Garcíu-Lópeze	Garcíu-Lópeze	k1gFnSc2	Garcíu-Lópeze
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
si	se	k3xPyFc3	se
tak	tak	k9	tak
připsal	připsat	k5eAaPmAgMnS	připsat
premiérové	premiérový	k2eAgNnSc4d1	premiérové
turnajové	turnajový	k2eAgNnSc4d1	turnajové
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamenalo	znamenat	k5eAaImAgNnS	znamenat
jedenáctý	jedenáctý	k4xOgInSc4	jedenáctý
singlový	singlový	k2eAgInSc4d1	singlový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
daviscupový	daviscupový	k2eAgInSc4d1	daviscupový
tým	tým	k1gInSc4	tým
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
odehrál	odehrát	k5eAaPmAgInS	odehrát
28	[number]	k4	28
mezistátní	mezistátní	k2eAgNnPc1d1	mezistátní
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
bilancí	bilance	k1gFnSc7	bilance
48	[number]	k4	48
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
16	[number]	k4	16
proher	prohra	k1gFnPc2	prohra
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
a	a	k8xC	a
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
zahrál	zahrát	k5eAaPmAgMnS	zahrát
finále	finále	k1gNnSc7	finále
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hájek	Hájek	k1gMnSc1	Hájek
a	a	k8xC	a
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
tehdy	tehdy	k6eAd1	tehdy
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
domácímu	domácí	k2eAgInSc3d1	domácí
Španělsku	Španělsko	k1gNnSc6	Španělsko
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitový	k2eAgFnSc7d1	Kvitová
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolali	zdolat	k5eAaPmAgMnP	zdolat
Francii	Francie	k1gFnSc4	Francie
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
členem	člen	k1gMnSc7	člen
vítězného	vítězný	k2eAgInSc2d1	vítězný
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
Davis	Davis	k1gFnSc6	Davis
Cupu	cup	k1gInSc2	cup
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Iva	Ivo	k1gMnSc2	Ivo
Mináře	Minář	k1gMnSc2	Minář
<g/>
,	,	kIx,	,
Lukáše	Lukáš	k1gMnSc2	Lukáš
Rosola	Rosol	k1gMnSc2	Rosol
a	a	k8xC	a
Radka	Radek	k1gMnSc2	Radek
Štěpánka	Štěpánek	k1gMnSc2	Štěpánek
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
finále	finále	k1gNnSc6	finále
porazili	porazit	k5eAaPmAgMnP	porazit
obhájce	obhájce	k1gMnSc1	obhájce
titulu	titul	k1gInSc2	titul
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
zopakoval	zopakovat	k5eAaPmAgInS	zopakovat
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
lídra	lídr	k1gMnSc2	lídr
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Hájkem	Hájek	k1gMnSc7	Hájek
<g/>
,	,	kIx,	,
Rosolem	Rosol	k1gMnSc7	Rosol
a	a	k8xC	a
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
<g/>
,	,	kIx,	,
přehráli	přehrát	k5eAaPmAgMnP	přehrát
v	v	k7c6	v
bělehradském	bělehradský	k2eAgNnSc6d1	Bělehradské
finále	finále	k1gNnSc6	finále
Srbsko	Srbsko	k1gNnSc4	Srbsko
opět	opět	k6eAd1	opět
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
přítelem	přítel	k1gMnSc7	přítel
české	český	k2eAgFnSc2d1	Česká
tenistky	tenistka	k1gFnSc2	tenistka
Lucie	Lucie	k1gFnSc2	Lucie
Šafářové	Šafářová	k1gFnSc2	Šafářová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc7	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
stala	stát	k5eAaPmAgFnS	stát
zlínská	zlínský	k2eAgFnSc1d1	zlínská
modelka	modelka	k1gFnSc1	modelka
Ester	Ester	k1gFnSc1	Ester
Sátorová	Sátorová	k1gFnSc1	Sátorová
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
tenistka	tenistka	k1gFnSc1	tenistka
TO	to	k9	to
Napajedla	Napajedla	k1gNnPc4	Napajedla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lednovém	lednový	k2eAgInSc6d1	lednový
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gNnSc1	Open
2015	[number]	k4	2015
pár	pár	k4xCyI	pár
oznámil	oznámit	k5eAaPmAgMnS	oznámit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
následoval	následovat	k5eAaImAgInS	následovat
sňatek	sňatek	k1gInSc1	sňatek
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gMnSc3	Carl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Střední	střední	k2eAgFnSc4d1	střední
odbornou	odborný	k2eAgFnSc4d1	odborná
školu	škola	k1gFnSc4	škola
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
fotovoltaické	fotovoltaický	k2eAgFnSc2d1	fotovoltaická
elektrárny	elektrárna	k1gFnSc2	elektrárna
Lužany	Lužana	k1gFnSc2	Lužana
<g/>
.	.	kIx.	.
</s>
