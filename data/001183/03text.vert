<s>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
I	i	k9	i
<g/>
,	,	kIx,	,
Robot	robot	k1gMnSc1	robot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
devíti	devět	k4xCc2	devět
sci-fi	scii	k1gFnPc2	sci-fi
povídek	povídka	k1gFnPc2	povídka
od	od	k7c2	od
Isaaca	Isaac	k1gInSc2	Isaac
Asimova	Asimův	k2eAgInSc2d1	Asimův
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
publikován	publikovat	k5eAaBmNgInS	publikovat
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Gnome	Gnom	k1gInSc5	Gnom
Press	Press	k1gInSc1	Press
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
5	[number]	k4	5
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
objevovaly	objevovat	k5eAaImAgFnP	objevovat
v	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
časopisech	časopis	k1gInPc6	časopis
Super	super	k2eAgFnSc1d1	super
Science	Science	k1gFnSc1	Science
Stories	Stories	k1gInSc4	Stories
a	a	k8xC	a
Astounding	Astounding	k1gInSc4	Astounding
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
až	až	k9	až
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
povídky	povídka	k1gFnPc1	povídka
čteny	číst	k5eAaImNgFnP	číst
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
sdílejí	sdílet	k5eAaImIp3nP	sdílet
motiv	motiv	k1gInSc4	motiv
interakce	interakce	k1gFnSc2	interakce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
robotů	robot	k1gInPc2	robot
a	a	k8xC	a
morality	moralita	k1gFnSc2	moralita
a	a	k8xC	a
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nS	tvořit
příběh	příběh	k1gInSc1	příběh
Asimovy	Asimův	k2eAgFnSc2d1	Asimova
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
historie	historie	k1gFnSc2	historie
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
povídkách	povídka	k1gFnPc6	povídka
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
doktorka	doktorka	k1gFnSc1	doktorka
Susan	Susan	k1gInSc1	Susan
Calvinová	Calvinová	k1gFnSc1	Calvinová
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
robopsycholožka	robopsycholožka	k1gFnSc1	robopsycholožka
Národní	národní	k2eAgFnSc2d1	národní
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
robotů	robot	k1gInPc2	robot
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
US	US	kA	US
Robots	Robotsa	k1gFnPc2	Robotsa
and	and	k?	and
Mechanical	Mechanical	k1gMnSc1	Mechanical
Men	Men	k1gMnSc1	Men
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předního	přední	k2eAgMnSc2d1	přední
výrobce	výrobce	k1gMnSc2	výrobce
robotů	robot	k1gMnPc2	robot
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Lhář	lhář	k1gMnSc1	lhář
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
povídek	povídka	k1gFnPc2	povídka
Asimov	Asimovo	k1gNnPc2	Asimovo
napsal	napsat	k5eAaPmAgInS	napsat
rámcový	rámcový	k2eAgInSc1d1	rámcový
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
povídky	povídka	k1gFnPc4	povídka
představuje	představovat	k5eAaImIp3nS	představovat
jako	jako	k9	jako
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
doktorky	doktorka	k1gFnSc2	doktorka
Calvinové	Calvinová	k1gFnSc2	Calvinová
během	během	k7c2	během
interview	interview	k1gNnSc2	interview
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
životní	životní	k2eAgFnSc4d1	životní
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
robotů	robot	k1gInPc2	robot
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
robopsychologie	robopsychologie	k1gFnSc2	robopsychologie
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odstranění	odstranění	k1gNnSc3	odstranění
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
též	též	k9	též
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídku	povídka	k1gFnSc4	povídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgInP	objevit
Asimovovy	Asimovův	k2eAgInPc1d1	Asimovův
tři	tři	k4xCgInPc1	tři
zákony	zákon	k1gInPc1	zákon
robotiky	robotika	k1gFnSc2	robotika
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
honěnou	honěná	k1gFnSc4	honěná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zde	zde	k6eAd1	zde
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Gregory	Gregor	k1gMnPc4	Gregor
Powell	Powell	k1gMnSc1	Powell
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Donovan	Donovan	k1gMnSc1	Donovan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
testují	testovat	k5eAaImIp3nP	testovat
prototypy	prototyp	k1gInPc4	prototyp
robotů	robot	k1gInPc2	robot
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
příčiny	příčina	k1gFnPc4	příčina
jejich	jejich	k3xOp3gFnPc2	jejich
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednovém	lednový	k2eAgNnSc6d1	lednové
čísle	číslo	k1gNnSc6	číslo
sci-fi	scii	k1gFnSc2	sci-fi
časopisu	časopis	k1gInSc2	časopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
Amazing	Amazing	k1gInSc4	Amazing
Stories	Stories	k1gInSc4	Stories
vyšla	vyjít	k5eAaPmAgFnS	vyjít
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
<g/>
"	"	kIx"	"
o	o	k7c6	o
sympatickém	sympatický	k2eAgMnSc6d1	sympatický
robotovi	robot	k1gMnSc6	robot
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
s	s	k7c7	s
pseudonymem	pseudonym	k1gInSc7	pseudonym
Eando	Eando	k1gNnSc1	Eando
Binder	Binder	k1gMnSc1	Binder
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Isaaca	Isaaca	k1gFnSc1	Isaaca
Asimova	Asimův	k2eAgFnSc1d1	Asimova
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
četl	číst	k5eAaImAgMnS	číst
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1938	[number]	k4	1938
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Astounding	Astounding	k1gInSc1	Astounding
Science	Science	k1gFnSc2	Science
Fiction	Fiction	k1gInSc1	Fiction
povídku	povídka	k1gFnSc4	povídka
"	"	kIx"	"
<g/>
Helen	Helena	k1gFnPc2	Helena
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Loy	Loy	k1gFnPc6	Loy
<g/>
"	"	kIx"	"
od	od	k7c2	od
Lestera	Lester	k1gMnSc2	Lester
Del	Del	k1gMnSc2	Del
Reye	Rey	k1gMnSc2	Rey
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
námětem	námět	k1gInSc7	námět
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
Asimov	Asimov	k1gInSc1	Asimov
pustil	pustit	k5eAaPmAgInS	pustit
do	do	k7c2	do
psaní	psaní	k1gNnSc2	psaní
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
povídky	povídka	k1gFnSc2	povídka
o	o	k7c6	o
robotech	robot	k1gInPc6	robot
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
mu	on	k3xPp3gMnSc3	on
2	[number]	k4	2
týdny	týden	k1gInPc7	týden
<g/>
,	,	kIx,	,
než	než	k8xS	než
ji	on	k3xPp3gFnSc4	on
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	s	k7c7	s
"	"	kIx"	"
<g/>
Robbie	Robbie	k1gFnPc4	Robbie
<g/>
"	"	kIx"	"
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
"	"	kIx"	"
<g/>
Helen	Helena	k1gFnPc2	Helena
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Loy	Loy	k1gFnPc6	Loy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Frederik	Frederik	k1gMnSc1	Frederik
Pohl	Pohl	k1gMnSc1	Pohl
doporučil	doporučit	k5eAaPmAgMnS	doporučit
redaktorovi	redaktor	k1gMnSc3	redaktor
Astounding	Astounding	k1gInSc4	Astounding
Science	Scienec	k1gInSc2	Scienec
Fiction	Fiction	k1gInSc4	Fiction
Johnu	John	k1gMnSc3	John
W.	W.	kA	W.
Campbellovi	Campbell	k1gMnSc3	Campbell
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
netiskl	tisknout	k5eNaImAgInS	tisknout
<g/>
.	.	kIx.	.
</s>
<s>
Otiskl	otisknout	k5eAaPmAgMnS	otisknout
ji	on	k3xPp3gFnSc4	on
až	až	k6eAd1	až
samotný	samotný	k2eAgMnSc1d1	samotný
Pohl	Pohl	k1gMnSc1	Pohl
v	v	k7c6	v
září	září	k1gNnSc6	září
1940	[number]	k4	1940
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
sci-fi	scii	k1gFnSc2	sci-fi
časopisu	časopis	k1gInSc2	časopis
Super	super	k2eAgInSc2d1	super
Science	Scienec	k1gInSc2	Scienec
Stories	Storiesa	k1gFnPc2	Storiesa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pod	pod	k7c7	pod
odlišným	odlišný	k2eAgInSc7d1	odlišný
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Strange	Strange	k1gInSc1	Strange
Playfellow	Playfellow	k1gFnSc2	Playfellow
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Rozum	rozum	k1gInSc1	rozum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Campbell	Campbell	k1gMnSc1	Campbell
přijal	přijmout	k5eAaPmAgMnS	přijmout
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1940	[number]	k4	1940
a	a	k8xC	a
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
časopisu	časopis	k1gInSc6	časopis
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
byl	být	k5eAaImAgInS	být
nadšen	nadchnout	k5eAaPmNgInS	nadchnout
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
napsal	napsat	k5eAaBmAgMnS	napsat
další	další	k2eAgFnSc4d1	další
povídku	povídka	k1gFnSc4	povídka
"	"	kIx"	"
<g/>
Lhář	lhář	k1gMnSc1	lhář
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Campbell	Campbell	k1gInSc1	Campbell
otiskl	otisknout	k5eAaPmAgInS	otisknout
hned	hned	k6eAd1	hned
v	v	k7c6	v
příštím	příští	k2eAgNnSc6d1	příští
květnovém	květnový	k2eAgNnSc6d1	květnové
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
napsal	napsat	k5eAaBmAgInS	napsat
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimovo	k1gNnPc2	Asimovo
pro	pro	k7c4	pro
Astounding	Astounding	k1gInSc4	Astounding
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
další	další	k2eAgFnSc2d1	další
povídky	povídka	k1gFnSc2	povídka
o	o	k7c6	o
robotech	robot	k1gInPc6	robot
-	-	kIx~	-
"	"	kIx"	"
<g/>
Chyť	chytit	k5eAaPmRp2nS	chytit
toho	ten	k3xDgMnSc4	ten
králíka	králík	k1gMnSc2	králík
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Únik	únik	k1gInSc1	únik
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Escape	Escap	k1gMnSc5	Escap
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Campbell	Campbell	k1gMnSc1	Campbell
ji	on	k3xPp3gFnSc4	on
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Paradoxical	Paradoxical	k1gMnSc5	Paradoxical
Escape	Escap	k1gMnSc5	Escap
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
2	[number]	k4	2
roky	rok	k1gInPc7	rok
předtím	předtím	k6eAd1	předtím
již	již	k6eAd1	již
otiskl	otisknout	k5eAaPmAgInS	otisknout
povídku	povídka	k1gFnSc4	povídka
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Konflikt	konflikt	k1gInSc1	konflikt
nikoli	nikoli	k9	nikoli
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Frederika	Frederik	k1gMnSc2	Frederik
Pohla	Pohl	k1gMnSc2	Pohl
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
literárního	literární	k2eAgMnSc2d1	literární
agenta	agent	k1gMnSc2	agent
Isaaca	Isaacus	k1gMnSc2	Isaacus
Asimova	Asimův	k2eAgMnSc2d1	Asimův
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
sestavit	sestavit	k5eAaPmF	sestavit
knížka	knížka	k1gFnSc1	knížka
z	z	k7c2	z
již	již	k6eAd1	již
vydaných	vydaný	k2eAgFnPc2d1	vydaná
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Doubleday	Doubledaa	k1gFnSc2	Doubledaa
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
nemělo	mít	k5eNaImAgNnS	mít
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projevilo	projevit	k5eAaPmAgNnS	projevit
ho	on	k3xPp3gNnSc4	on
malé	malý	k2eAgNnSc1d1	malé
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Gnome	Gnom	k1gInSc5	Gnom
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Asimov	Asimov	k1gInSc4	Asimov
chtěl	chtít	k5eAaImAgMnS	chtít
titul	titul	k1gInSc4	titul
Mind	Mind	k1gMnSc1	Mind
and	and	k?	and
Iron	iron	k1gInSc1	iron
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
si	se	k3xPyFc3	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
název	název	k1gInSc4	název
I	i	k9	i
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
(	(	kIx(	(
<g/>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
autorovy	autorův	k2eAgInPc4d1	autorův
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
povídka	povídka	k1gFnSc1	povídka
od	od	k7c2	od
Eando	Eando	k1gNnSc4	Eando
Bindera	Binder	k1gMnSc2	Binder
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spisovateli	spisovatel	k1gMnSc3	spisovatel
vyšla	vyjít	k5eAaPmAgFnS	vyjít
(	(	kIx(	(
<g/>
tou	ten	k3xDgFnSc7	ten
první	první	k4xOgInSc1	první
byl	být	k5eAaImAgInS	být
román	román	k1gInSc4	román
Oblázek	oblázek	k1gInSc4	oblázek
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezpůsobila	způsobit	k5eNaPmAgFnS	způsobit
sice	sice	k8xC	sice
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
žádný	žádný	k3yNgInSc4	žádný
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
jistě	jistě	k6eAd1	jistě
se	se	k3xPyFc4	se
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
prodávala	prodávat	k5eAaImAgNnP	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
5	[number]	k4	5
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
i	i	k9	i
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
úpravě	úprava	k1gFnSc6	úprava
pro	pro	k7c4	pro
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
v	v	k7c6	v
levnější	levný	k2eAgFnSc6d2	levnější
vázané	vázaný	k2eAgFnSc6d1	vázaná
edici	edice	k1gFnSc6	edice
<g/>
,	,	kIx,	,
také	také	k9	také
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
Isaaca	Isaacus	k1gMnSc2	Isaacus
Asimova	Asimův	k2eAgMnSc2d1	Asimův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Robbie	Robbie	k1gFnSc1	Robbie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Robbie	Robbie	k1gFnSc1	Robbie
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Strange	Strange	k1gFnSc1	Strange
Playfellow	Playfellow	k1gFnSc1	Playfellow
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
Westonovi	Weston	k1gMnSc3	Weston
pořídí	pořídit	k5eAaPmIp3nS	pořídit
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Glorii	Gloria	k1gFnSc3	Gloria
chůvu	chůva	k1gFnSc4	chůva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
nemluvící	mluvící	k2eNgMnSc1d1	nemluvící
robot	robot	k1gMnSc1	robot
Robbie	Robbie	k1gFnSc2	Robbie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Glorie	Glorie	k1gFnSc1	Glorie
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc3	její
matce	matka	k1gFnSc3	matka
se	se	k3xPyFc4	se
přestane	přestat	k5eAaPmIp3nS	přestat
líbit	líbit	k5eAaImF	líbit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
honěnou	honěná	k1gFnSc4	honěná
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Runaround	Runaround	k1gInSc1	Runaround
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Cycle	Cycle	k1gNnSc1	Cycle
Fermé	Fermý	k2eAgNnSc1d1	Fermé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mike	Mike	k1gFnSc1	Mike
Donovan	Donovan	k1gMnSc1	Donovan
a	a	k8xC	a
Greg	Greg	k1gMnSc1	Greg
Powell	Powell	k1gMnSc1	Powell
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
testovat	testovat	k5eAaImF	testovat
nové	nový	k2eAgFnPc4d1	nová
roboty	robota	k1gFnPc4	robota
<g/>
.	.	kIx.	.
</s>
<s>
Potřebují	potřebovat	k5eAaImIp3nP	potřebovat
ale	ale	k9	ale
selen	selen	k1gInSc4	selen
a	a	k8xC	a
tak	tak	k9	tak
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vyšlou	vyslat	k5eAaPmIp3nP	vyslat
robota	robot	k1gMnSc2	robot
Rychlíka	Rychlík	k1gMnSc2	Rychlík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
selenovému	selenový	k2eAgNnSc3d1	selenový
jezírku	jezírko	k1gNnSc3	jezírko
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
běhat	běhat	k5eAaImF	běhat
kolem	kolem	k6eAd1	kolem
dokola	dokola	k6eAd1	dokola
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
zákony	zákon	k1gInPc4	zákon
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Rozum	rozum	k1gInSc1	rozum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Reason	Reason	k1gInSc1	Reason
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mike	Mike	k1gFnSc1	Mike
Donovan	Donovan	k1gMnSc1	Donovan
a	a	k8xC	a
Greg	Greg	k1gMnSc1	Greg
Powell	Powell	k1gMnSc1	Powell
zkonstruují	zkonstruovat	k5eAaPmIp3nP	zkonstruovat
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
robota	robot	k1gMnSc2	robot
CHTR-	CHTR-	k1gFnSc2	CHTR-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dokáže	dokázat	k5eAaPmIp3nS	dokázat
samostatně	samostatně	k6eAd1	samostatně
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
.	.	kIx.	.
</s>
<s>
Robot	robot	k1gMnSc1	robot
nemůže	moct	k5eNaImIp3nS	moct
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
stvořili	stvořit	k5eAaPmAgMnP	stvořit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
méně	málo	k6eAd2	málo
inteligentní	inteligentní	k2eAgFnSc2d1	inteligentní
bytosti	bytost	k1gFnSc2	bytost
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Dedukce	dedukce	k1gFnSc1	dedukce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Chyť	chytit	k5eAaPmRp2nS	chytit
toho	ten	k3xDgMnSc4	ten
králíka	králík	k1gMnSc2	králík
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Catch	Catch	k1gMnSc1	Catch
That	That	k1gMnSc1	That
Rabbit	Rabbit	k1gMnSc1	Rabbit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
Greg	Greg	k1gMnSc1	Greg
Powell	Powell	k1gMnSc1	Powell
a	a	k8xC	a
Mike	Mike	k1gInSc1	Mike
Donovan	Donovan	k1gMnSc1	Donovan
mají	mít	k5eAaImIp3nP	mít
vyladit	vyladit	k5eAaPmF	vyladit
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
robota	robot	k1gMnSc2	robot
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
robotů	robot	k1gInPc2	robot
<g/>
.	.	kIx.	.
</s>
<s>
Roboti	robot	k1gMnPc1	robot
přestávají	přestávat	k5eAaImIp3nP	přestávat
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gInPc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nesleduje	sledovat	k5eNaImIp3nS	sledovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
podivně	podivně	k6eAd1	podivně
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Lhář	lhář	k1gMnSc1	lhář
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Liar	Liar	k1gInSc1	Liar
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
robotovi	robot	k1gMnSc6	robot
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
umí	umět	k5eAaImIp3nS	umět
číst	číst	k5eAaImF	číst
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gInSc1	Susan
Calvinová	Calvinová	k1gFnSc1	Calvinová
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
rozluštit	rozluštit	k5eAaPmF	rozluštit
tuto	tento	k3xDgFnSc4	tento
záhadu	záhada	k1gFnSc4	záhada
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Malý	malý	k2eAgInSc1d1	malý
ztracený	ztracený	k2eAgInSc1d1	ztracený
robot	robot	k1gInSc1	robot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
Lost	Lost	k1gMnSc1	Lost
Robot	robot	k1gMnSc1	robot
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
Susan	Susan	k1gInSc1	Susan
Calvinová	Calvinová	k1gFnSc1	Calvinová
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Bogert	Bogert	k1gMnSc1	Bogert
přilétají	přilétat	k5eAaImIp3nP	přilétat
na	na	k7c6	na
Hyperzákladnu	Hyperzákladnu	k1gFnSc6	Hyperzákladnu
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
vyřešit	vyřešit	k5eAaPmF	vyřešit
záhadu	záhada	k1gFnSc4	záhada
zmizení	zmizení	k1gNnSc2	zmizení
robota	robot	k1gMnSc2	robot
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
to	ten	k3xDgNnSc4	ten
dostal	dostat	k5eAaPmAgMnS	dostat
<g />
.	.	kIx.	.
</s>
<s>
příkazem	příkaz	k1gInSc7	příkaz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Únik	únik	k1gInSc1	únik
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Escape	Escap	k1gInSc5	Escap
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Paradoxical	Paradoxical	k1gMnSc5	Paradoxical
Escape	Escap	k1gMnSc5	Escap
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
americkou	americký	k2eAgFnSc4d1	americká
společnost	společnost	k1gFnSc4	společnost
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
robotů	robot	k1gInPc2	robot
se	se	k3xPyFc4	se
obrátí	obrátit	k5eAaPmIp3nS	obrátit
konkurence	konkurence	k1gFnSc1	konkurence
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zadali	zadat	k5eAaPmAgMnP	zadat
svému	svůj	k1gMnSc3	svůj
robotovi	robot	k1gMnSc3	robot
přepočet	přepočet	k1gInSc4	přepočet
rovnic	rovnice	k1gFnPc2	rovnice
pro	pro	k7c4	pro
motor	motor	k1gInSc4	motor
pro	pro	k7c4	pro
interstellární	interstellární	k2eAgInPc4d1	interstellární
lety	let	k1gInPc4	let
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
vlastní	vlastní	k2eAgInSc1d1	vlastní
superpočítač	superpočítač	k1gInSc1	superpočítač
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
se	s	k7c7	s
zákony	zákon	k1gInPc7	zákon
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Evidence	evidence	k1gFnSc1	evidence
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
volební	volební	k2eAgFnSc6d1	volební
kampani	kampaň	k1gFnSc6	kampaň
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
starosty	starosta	k1gMnSc2	starosta
nařkne	nařknout	k5eAaPmIp3nS	nařknout
Francis	Francis	k1gFnSc4	Francis
Quinn	Quinna	k1gFnPc2	Quinna
svého	svůj	k3xOyFgMnSc2	svůj
rivala	rival	k1gMnSc2	rival
Stephena	Stephen	k1gMnSc2	Stephen
Byerleye	Byerley	k1gMnSc2	Byerley
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
robot	robot	k1gMnSc1	robot
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Byerleym	Byerleym	k1gInSc4	Byerleym
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgInS	dokázat
opak	opak	k1gInSc1	opak
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Konflikt	konflikt	k1gInSc1	konflikt
nikoli	nikoli	k9	nikoli
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Evitable	Evitable	k1gMnSc1	Evitable
Conflict	Conflict	k1gMnSc1	Conflict
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Conflict	Conflict	k2eAgMnSc1d1	Conflict
Evitable	Evitable	k1gMnSc1	Evitable
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
robopsycholožka	robopsycholožka	k1gFnSc1	robopsycholožka
Susan	Susan	k1gInSc1	Susan
Calvinová	Calvinová	k1gFnSc1	Calvinová
je	být	k5eAaImIp3nS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
k	k	k7c3	k
Stephenu	Stepheno	k1gNnSc3	Stepheno
Byerleymu	Byerleym	k1gInSc2	Byerleym
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stroje	stroj	k1gInPc1	stroj
řídící	řídící	k2eAgFnSc4d1	řídící
světovou	světový	k2eAgFnSc4d1	světová
ekonomiku	ekonomika	k1gFnSc4	ekonomika
nefungují	fungovat	k5eNaImIp3nP	fungovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mají	mít	k5eAaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
-	-	kIx~	-
vědeckofantastický	vědeckofantastický	k2eAgInSc1d1	vědeckofantastický
film	film	k1gInSc1	film
USA	USA	kA	USA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
volně	volně	k6eAd1	volně
zpracovaný	zpracovaný	k2eAgMnSc1d1	zpracovaný
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
Alex	Alex	k1gMnSc1	Alex
Proyas	Proyas	k1gMnSc1	Proyas
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Will	Will	k1gMnSc1	Will
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Bridget	Bridget	k1gMnSc1	Bridget
Moynahan	Moynahan	k1gMnSc1	Moynahan
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
Greenwood	Greenwood	k1gInSc1	Greenwood
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Tudyk	Tudyk	k1gMnSc1	Tudyk
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Cromwell	Cromwell	k1gMnSc1	Cromwell
<g/>
,	,	kIx,	,
Shia	Shia	k1gMnSc1	Shia
LaBeouf	LaBeouf	k1gMnSc1	LaBeouf
<g/>
,	,	kIx,	,
Chi	chi	k0	chi
McBride	McBrid	k1gInSc5	McBrid
<g/>
,	,	kIx,	,
Aaron	Aaron	k1gMnSc1	Aaron
Douglas	Douglas	k1gMnSc1	Douglas
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
Tiffany	Tiffana	k1gFnPc1	Tiffana
Lyndall-Knight	Lyndall-Knight	k1gInSc1	Lyndall-Knight
<g/>
,	,	kIx,	,
Emily	Emil	k1gMnPc4	Emil
Tennant	Tennanta	k1gFnPc2	Tennanta
<g/>
,	,	kIx,	,
Terry	Terra	k1gFnSc2	Terra
Chen	Chena	k1gFnPc2	Chena
<g/>
.	.	kIx.	.
</s>
