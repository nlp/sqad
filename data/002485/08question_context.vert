<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
En	En	k1gMnSc1	En
attendant	attendant	k1gMnSc1	attendant
Godot	Godot	k1gMnSc1	Godot
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
Godot	Godot	k1gInSc1	Godot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
klasické	klasický	k2eAgNnSc4d1	klasické
absurdní	absurdní	k2eAgNnSc4d1	absurdní
drama	drama	k1gNnSc4	drama
Samuela	Samuel	k1gMnSc2	Samuel
Becketta	Beckett	k1gMnSc2	Beckett
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
napsal	napsat	k5eAaBmAgMnS	napsat
hru	hra	k1gFnSc4	hra
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
uvedena	uveden	k2eAgFnSc1d1	uvedena
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
v	v	k7c6	v
Théâtre	Théâtr	k1gInSc5	Théâtr
de	de	k?	de
Babylone	Babylon	k1gInSc5	Babylon
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
