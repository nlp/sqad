<s>
Hirose	Hirosa	k1gFnSc3	Hirosa
Heidžiró	Heidžiró	k1gMnSc1	Heidžiró
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
広	広	k?	広
平	平	k?	平
<g/>
,	,	kIx,	,
Hepburnův	Hepburnův	k2eAgInSc1d1	Hepburnův
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Heijirō	Heijirō	k1gFnSc1	Heijirō
Hirose	Hirosa	k1gFnSc6	Hirosa
<g/>
;	;	kIx,	;
1865	[number]	k4	1865
–	–	k?	–
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
profesionální	profesionální	k2eAgMnSc1d1	profesionální
japonský	japonský	k2eAgMnSc1d1	japonský
hráč	hráč	k1gMnSc1	hráč
go	go	k?	go
organizace	organizace	k1gFnSc1	organizace
Hoenša	Hoenša	k1gFnSc1	Hoenša
a	a	k8xC	a
Nihon	Nihon	k1gNnSc1	Nihon
Ki-in	Kina	k1gFnPc2	Ki-ina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
udělily	udělit	k5eAaPmAgFnP	udělit
5	[number]	k4	5
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
