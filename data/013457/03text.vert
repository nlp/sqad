<p>
<s>
Hirose	Hirosa	k1gFnSc3	Hirosa
Heidžiró	Heidžiró	k1gMnSc1	Heidžiró
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
広	広	k?	広
平	平	k?	平
<g/>
,	,	kIx,	,
Hepburnův	Hepburnův	k2eAgInSc1d1	Hepburnův
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Heijirō	Heijirō	k1gFnSc1	Heijirō
Hirose	Hirosa	k1gFnSc6	Hirosa
<g/>
;	;	kIx,	;
1865	[number]	k4	1865
–	–	k?	–
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
profesionální	profesionální	k2eAgMnSc1d1	profesionální
japonský	japonský	k2eAgMnSc1d1	japonský
hráč	hráč	k1gMnSc1	hráč
go	go	k?	go
organizace	organizace	k1gFnSc1	organizace
Hoenša	Hoenša	k1gFnSc1	Hoenša
a	a	k8xC	a
Nihon	Nihon	k1gNnSc1	Nihon
Ki-in	Kina	k1gFnPc2	Ki-ina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
udělily	udělit	k5eAaPmAgFnP	udělit
5	[number]	k4	5
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
<s>
Hirosemu	Hirosemat	k5eAaPmIp1nS	Hirosemat
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
Iwamota	Iwamot	k1gMnSc2	Iwamot
Kaora	Kaor	k1gMnSc2	Kaor
a	a	k8xC	a
Kotó	Kotó	k1gMnSc2	Kotó
Šina	šinout	k5eAaImSgMnS	šinout
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k1gNnPc7	další
Hiroseho	Hirose	k1gMnSc2	Hirose
učedníky	učedník	k1gMnPc7	učedník
byli	být	k5eAaImAgMnP	být
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Cujamori	Cujamor	k1gMnPc1	Cujamor
Icuró	Icuró	k1gFnSc2	Icuró
<g/>
,	,	kIx,	,
Iida	Iida	k1gFnSc1	Iida
Harudži	Harudž	k1gFnSc3	Harudž
a	a	k8xC	a
Sakaguči	Sakaguč	k1gFnSc3	Sakaguč
Cunedžiró	Cunedžiró	k1gFnSc3	Cunedžiró
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
získal	získat	k5eAaPmAgInS	získat
3	[number]	k4	3
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
získal	získat	k5eAaPmAgInS	získat
4	[number]	k4	4
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
Jubango	Jubango	k6eAd1	Jubango
s	s	k7c7	s
Išii	Išii	k1gNnSc7	Išii
Sendžim	Sendžima	k1gFnPc2	Sendžima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
získal	získat	k5eAaPmAgInS	získat
5	[number]	k4	5
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
Jubango	Jubango	k6eAd1	Jubango
s	s	k7c7	s
Izawou	Izawa	k1gFnSc7	Izawa
Genkičim	Genkičima	k1gFnPc2	Genkičima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
získal	získat	k5eAaPmAgInS	získat
6	[number]	k4	6
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
čínským	čínský	k2eAgMnSc7d1	čínský
premiérem	premiér	k1gMnSc7	premiér
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pátým	pátý	k4xOgMnSc7	pátý
prezidentem	prezident	k1gMnSc7	prezident
organizace	organizace	k1gFnSc2	organizace
Hoenša	Hoenš	k1gInSc2	Hoenš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
získal	získat	k5eAaPmAgInS	získat
7	[number]	k4	7
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
odešel	odejít	k5eAaPmAgMnS	odejít
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hirose	Hirosa	k1gFnSc3	Hirosa
Heijirō	Heijirō	k1gMnPc3	Heijirō
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hráčů	hráč	k1gMnPc2	hráč
go	go	k?	go
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Sensei	Sensee	k1gFnSc4	Sensee
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Library	Librar	k1gInPc7	Librar
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
</s>
</p>
