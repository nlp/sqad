<p>
<s>
FK	FK	kA	FK
Baník	baník	k1gMnSc1	baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Dubňanech	Dubňan	k1gMnPc6	Dubňan
na	na	k7c6	na
Hodonínsku	Hodonínsko	k1gNnSc6	Hodonínsko
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
SK	Sk	kA	Sk
Moravia	Moravium	k1gNnSc2	Moravium
Dubňany	Dubňan	k1gMnPc7	Dubňan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
hraje	hrát	k5eAaImIp3nS	hrát
I.	I.	kA	I.
A	a	k8xC	a
třídu	třída	k1gFnSc4	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
klubu	klub	k1gInSc2	klub
je	být	k5eAaImIp3nS	být
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
15	[number]	k4	15
ročnících	ročník	k1gInPc6	ročník
Přeboru	přebor	k1gInSc2	přebor
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
–	–	k?	–
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
/	/	kIx~	/
<g/>
84	[number]	k4	84
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
čtvrtoligovém	čtvrtoligový	k2eAgInSc6d1	čtvrtoligový
ročníku	ročník	k1gInSc6	ročník
1977	[number]	k4	1977
<g/>
/	/	kIx~	/
<g/>
78	[number]	k4	78
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
se	se	k3xPyFc4	se
po	po	k7c6	po
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
probojoval	probojovat	k5eAaPmAgMnS	probojovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
krajské	krajský	k2eAgFnSc2d1	krajská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
ihned	ihned	k6eAd1	ihned
ovšem	ovšem	k9	ovšem
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
odchovancem	odchovanec	k1gMnSc7	odchovanec
klubu	klub	k1gInSc2	klub
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Dupal	Dupal	k1gMnSc1	Dupal
<g/>
,	,	kIx,	,
prvoligový	prvoligový	k2eAgMnSc1d1	prvoligový
hráč	hráč	k1gMnSc1	hráč
Zlína	Zlín	k1gInSc2	Zlín
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
trenér	trenér	k1gMnSc1	trenér
především	především	k6eAd1	především
francouzských	francouzský	k2eAgNnPc2d1	francouzské
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Trénoval	trénovat	k5eAaImAgMnS	trénovat
i	i	k9	i
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
kariéry	kariéra	k1gFnSc2	kariéra
zde	zde	k6eAd1	zde
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
František	František	k1gMnSc1	František
Kordula	Kordula	k1gFnSc1	Kordula
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
dorostenecký	dorostenecký	k2eAgInSc4d1	dorostenecký
titul	titul	k1gInSc4	titul
s	s	k7c7	s
Ratíškovicemi	Ratíškovice	k1gFnPc7	Ratíškovice
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
a	a	k8xC	a
prvoligový	prvoligový	k2eAgInSc4d1	prvoligový
s	s	k7c7	s
pražskou	pražský	k2eAgFnSc7d1	Pražská
Duklou	Dukla	k1gFnSc7	Dukla
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
/	/	kIx~	/
<g/>
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
–	–	k?	–
SK	Sk	kA	Sk
Moravia	Moravium	k1gNnPc1	Moravium
Dubňany	Dubňan	k1gMnPc4	Dubňan
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Moravia	Moravium	k1gNnSc2	Moravium
Dubňany	Dubňan	k1gMnPc7	Dubňan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
–	–	k?	–
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
–	–	k?	–
DSO	DSO	kA	DSO
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
(	(	kIx(	(
<g/>
Dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
organisace	organisace	k1gFnSc1	organisace
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
–	–	k?	–
TJ	tj	kA	tj
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc7	Dubňan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
FK	FK	kA	FK
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
(	(	kIx(	(
<g/>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc7	Dubňan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sezona	sezona	k1gFnSc1	sezona
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
70	[number]	k4	70
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezoně	sezona	k1gFnSc6	sezona
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
Dubňanech	Dubňan	k1gMnPc6	Dubňan
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
župní	župní	k2eAgFnSc1d1	župní
(	(	kIx(	(
<g/>
krajská	krajský	k2eAgFnSc1d1	krajská
<g/>
)	)	kIx)	)
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kádru	kádr	k1gInSc6	kádr
A-mužstva	Aužstvo	k1gNnSc2	A-mužstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Miroslav	Miroslav	k1gMnSc1	Miroslav
Frejka	Frejka	k1gFnSc1	Frejka
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Josef	Josef	k1gMnSc1	Josef
Dohnálek	Dohnálek	k1gMnSc1	Dohnálek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Filipovič	Filipovič	k1gMnSc1	Filipovič
<g/>
,	,	kIx,	,
Štefan	Štefan	k1gMnSc1	Štefan
Foldváry	Foldvár	k1gInPc4	Foldvár
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Harca	Harca	k1gMnSc1	Harca
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hoch	hoch	k1gMnSc1	hoch
(	(	kIx(	(
<g/>
brankář	brankář	k1gMnSc1	brankář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ilčík	Ilčík	k1gMnSc1	Ilčík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Ilčík	Ilčík	k1gMnSc1	Ilčík
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kordula	Kordula	k1gFnSc1	Kordula
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Příkazský	Příkazský	k2eAgMnSc1d1	Příkazský
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Skřivánek	Skřivánek	k1gMnSc1	Skřivánek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Teichl	Teichl	k1gMnSc1	Teichl
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vaculík	Vaculík	k1gMnSc1	Vaculík
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Vyškovský	vyškovský	k2eAgMnSc1d1	vyškovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
II	II	kA	II
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
–	–	k?	–
VII	VII	kA	VII
<g/>
.	.	kIx.	.
okrsek	okrsek	k1gInSc1	okrsek
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
:	:	kIx,	:
I.	I.	kA	I.
B	B	kA	B
třída	třída	k1gFnSc1	třída
BZMŽF	BZMŽF	kA	BZMŽF
–	–	k?	–
III	III	kA	III
<g/>
.	.	kIx.	.
okrsek	okrsek	k1gInSc1	okrsek
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
:	:	kIx,	:
I.	I.	kA	I.
B	B	kA	B
třída	třída	k1gFnSc1	třída
BZMŽF	BZMŽF	kA	BZMŽF
–	–	k?	–
IV	IV	kA	IV
<g/>
.	.	kIx.	.
okrsek	okrsek	k1gInSc1	okrsek
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
I.	I.	kA	I.
A	a	k9	a
třída	třída	k1gFnSc1	třída
Jihomoravské	jihomoravský	k2eAgFnSc2d1	Jihomoravská
oblasti	oblast	k1gFnSc2	oblast
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
župní	župní	k2eAgInSc1d1	župní
přebor	přebor	k1gInSc1	přebor
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
:	:	kIx,	:
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
I.	I.	kA	I.
A	a	k8xC	a
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
Jihomoravská	jihomoravský	k2eAgFnSc1d1	Jihomoravská
krajská	krajský	k2eAgFnSc1d1	krajská
soutěž	soutěž	k1gFnSc1	soutěž
I.	I.	kA	I.
třídy	třída	k1gFnPc1	třída
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
I.	I.	kA	I.
A	a	k8xC	a
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
I.	I.	kA	I.
B	B	kA	B
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgInSc1d1	okresní
přebor	přebor	k1gInSc1	přebor
Hodonínska	Hodonínsko	k1gNnSc2	Hodonínsko
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
I.	I.	kA	I.
B	B	kA	B
třída	třída	k1gFnSc1	třída
Středomoravské	středomoravský	k2eAgFnSc2d1	Středomoravská
župy	župa	k1gFnSc2	župa
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
I.	I.	kA	I.
A	a	k8xC	a
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
I.	I.	kA	I.
B	B	kA	B
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
I.	I.	kA	I.
A	a	k8xC	a
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Přebor	přebor	k1gInSc1	přebor
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
–	–	k?	–
:	:	kIx,	:
I.	I.	kA	I.
A	a	k8xC	a
třída	třída	k1gFnSc1	třída
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
BJednotlivé	BJednotlivý	k2eAgInPc4d1	BJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	z	k7c2	z
–	–	k?	–
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
–	–	k?	–
výhry	výhra	k1gFnPc1	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
–	–	k?	–
remízy	remíza	k1gFnPc4	remíza
<g/>
,	,	kIx,	,
P	P	kA	P
–	–	k?	–
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
–	–	k?	–
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
–	–	k?	–
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
−	−	k?	−
–	–	k?	–
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
–	–	k?	–
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc4d1	červené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
–	–	k?	–
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
69	[number]	k4	69
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
reorganizaci	reorganizace	k1gFnSc3	reorganizace
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
70	[number]	k4	70
<g/>
:	:	kIx,	:
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezoně	sezona	k1gFnSc6	sezona
byly	být	k5eAaImAgInP	být
body	bod	k1gInPc1	bod
udělovány	udělovat	k5eAaImNgInP	udělovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
3	[number]	k4	3
body	bod	k1gInPc4	bod
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
branek	branka	k1gFnPc2	branka
<g/>
,	,	kIx,	,
2	[number]	k4	2
body	bod	k1gInPc1	bod
za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
1	[number]	k4	1
bod	bod	k1gInSc1	bod
za	za	k7c4	za
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
gólovou	gólový	k2eAgFnSc4d1	gólová
remízu	remíza	k1gFnSc4	remíza
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc4	žádný
bod	bod	k1gInSc4	bod
za	za	k7c4	za
remízu	remíza	k1gFnSc4	remíza
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
/	/	kIx~	/
<g/>
72	[number]	k4	72
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc1	zrušení
žup	župa	k1gFnPc2	župa
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc4	návrat
krajů	kraj	k1gInPc2	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
/	/	kIx~	/
<g/>
77	[number]	k4	77
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
/	/	kIx~	/
<g/>
81	[number]	k4	81
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
83	[number]	k4	83
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
krajských	krajský	k2eAgFnPc2d1	krajská
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
/	/	kIx~	/
<g/>
86	[number]	k4	86
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
krajských	krajský	k2eAgFnPc2d1	krajská
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc1	zrušení
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
žup	župa	k1gFnPc2	župa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
<g/>
:	:	kIx,	:
Baníku	Baník	k1gInSc2	Baník
Dubňany	Dubňan	k1gMnPc4	Dubňan
byly	být	k5eAaImAgInP	být
odečteny	odečíst	k5eAaPmNgInP	odečíst
3	[number]	k4	3
body	bod	k1gInPc1	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc1	zrušení
žup	župa	k1gFnPc2	župa
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc4	návrat
krajů	kraj	k1gInPc2	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
FK	FK	kA	FK
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc7	Dubňan
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
==	==	k?	==
</s>
</p>
<p>
<s>
FK	FK	kA	FK
Baník	baník	k1gMnSc1	baník
Dubňany	Dubňan	k1gMnPc7	Dubňan
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
rezervním	rezervní	k2eAgInSc7d1	rezervní
týmem	tým	k1gInSc7	tým
Dubňan	Dubňana	k1gFnPc2	Dubňana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
v	v	k7c6	v
okresních	okresní	k2eAgFnPc6d1	okresní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
hrál	hrát	k5eAaImAgMnS	hrát
Okresní	okresní	k2eAgFnSc4d1	okresní
soutěž	soutěž	k1gFnSc4	soutěž
Hodonínska	Hodonínsko	k1gNnSc2	Hodonínsko
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
ukončil	ukončit	k5eAaPmAgInS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
===	===	k?	===
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Základní	základní	k2eAgFnSc1d1	základní
třída	třída	k1gFnSc1	třída
Hodonínska	Hodonínsko	k1gNnSc2	Hodonínsko
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgFnSc2d1	okresní
soutěž	soutěž	k1gFnSc4	soutěž
Hodonínska	Hodonínsko	k1gNnSc2	Hodonínsko
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Základní	základní	k2eAgFnSc1d1	základní
třída	třída	k1gFnSc1	třída
Hodonínska	Hodonínsko	k1gNnSc2	Hodonínsko
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgFnSc2d1	okresní
soutěž	soutěž	k1gFnSc4	soutěž
Hodonínska	Hodonínsko	k1gNnSc2	Hodonínsko
–	–	k?	–
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
AJednotlivé	AJednotlivý	k2eAgInPc4d1	AJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	z	k7c2	z
–	–	k?	–
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
–	–	k?	–
výhry	výhra	k1gFnPc1	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
–	–	k?	–
remízy	remíza	k1gFnPc4	remíza
<g/>
,	,	kIx,	,
P	P	kA	P
–	–	k?	–
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
–	–	k?	–
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
–	–	k?	–
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
−	−	k?	−
–	–	k?	–
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
–	–	k?	–
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc4d1	červené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
–	–	k?	–
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
fotbalu	fotbal	k1gInSc2	fotbal
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
a	a	k8xC	a
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
–	–	k?	–
Dáme	dát	k5eAaPmIp1nP	dát
góla	góla	k6eAd1	góla
<g/>
,	,	kIx,	,
dáme	dát	k5eAaPmIp1nP	dát
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
FK	FK	kA	FK
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
978-80-239-9259-5	[number]	k4	978-80-239-9259-5
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
FK	FK	kA	FK
Baník	Baník	k1gInSc1	Baník
Dubňany	Dubňan	k1gMnPc7	Dubňan
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
