<s>
Tandemový	tandemový	k2eAgInSc1d1
paragliding	paragliding	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tandemový	tandemový	k2eAgInSc1d1
paragliding	paragliding	k1gInSc1
</s>
<s>
Tandemový	tandemový	k2eAgInSc1d1
paragliding	paragliding	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
vyzkoušet	vyzkoušet	k5eAaPmF
si	se	k3xPyFc3
paragliding	paragliding	k1gInSc4
všem	všecek	k3xTgMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
nechtějí	chtít	k5eNaImIp3nP
létat	létat	k5eAaImF
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
jen	jen	k9
chtějí	chtít	k5eAaImIp3nP
zažít	zažít	k5eAaPmF
výjimečný	výjimečný	k2eAgInSc4d1
zážitek	zážitek	k1gInSc4
<g/>
,	,	kIx,
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
let	léto	k1gNnPc2
na	na	k7c6
paraglidingovém	paraglidingový	k2eAgInSc6d1
kluzáku	kluzák	k1gInSc6
bezpochyby	bezpochyby	k6eAd1
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
letu	let	k1gInSc2
tandemový	tandemový	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
řídí	řídit	k5eAaImIp3nS
<g/>
,	,	kIx,
pasažér	pasažér	k1gMnSc1
sedí	sedit	k5eAaImIp3nS
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
sedačce	sedačka	k1gFnSc6
před	před	k7c7
pilotem	pilot	k1gMnSc7
a	a	k8xC
užívá	užívat	k5eAaImIp3nS
si	se	k3xPyFc3
volný	volný	k2eAgInSc4d1
let	let	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
tandemového	tandemový	k2eAgInSc2d1
paraglidingu	paragliding	k1gInSc2
</s>
<s>
Při	při	k7c6
tandemovém	tandemový	k2eAgInSc6d1
paraglidingu	paragliding	k1gInSc6
je	být	k5eAaImIp3nS
bezpečnosti	bezpečnost	k1gFnPc4
věnována	věnován	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
pro	pro	k7c4
"	"	kIx"
<g/>
single	singl	k1gInSc5
<g/>
"	"	kIx"
paragliding	paragliding	k1gInSc4
se	se	k3xPyFc4
nabízejí	nabízet	k5eAaImIp3nP
kluzáky	kluzák	k1gInPc1
od	od	k7c2
velice	velice	k6eAd1
bezpečných	bezpečný	k2eAgInPc2d1
<g/>
,	,	kIx,
školních	školní	k2eAgInPc2d1
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
výkonné	výkonný	k2eAgInPc4d1
závodní	závodní	k2eAgInPc4d1
kluzáky	kluzák	k1gInPc4
s	s	k7c7
relativně	relativně	k6eAd1
malou	malý	k2eAgFnSc7d1
stabilitou	stabilita	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
tandemový	tandemový	k2eAgInSc4d1
paragliding	paragliding	k1gInSc4
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
pouze	pouze	k6eAd1
kluzáky	kluzák	k1gInPc4
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
stabilitou	stabilita	k1gFnSc7
za	za	k7c2
všech	všecek	k3xTgFnPc2
okolností	okolnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kolapsu	kolaps	k1gInSc3
tandemového	tandemový	k2eAgInSc2d1
kluzáku	kluzák	k1gInSc2
při	při	k7c6
letu	let	k1gInSc6
v	v	k7c6
nevhodných	vhodný	k2eNgFnPc6d1
povětrnostních	povětrnostní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tandemový	tandemový	k2eAgInSc1d1
kluzák	kluzák	k1gInSc1
schopen	schopen	k2eAgInSc1d1
se	se	k3xPyFc4
velice	velice	k6eAd1
rychle	rychle	k6eAd1
sám	sám	k3xTgInSc4
zregenerovat	zregenerovat	k5eAaPmF
a	a	k8xC
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
letu	let	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tandemové	tandemový	k2eAgInPc1d1
kluzáky	kluzák	k1gInPc1
procházejí	procházet	k5eAaImIp3nP
před	před	k7c7
uvedením	uvedení	k1gNnSc7
na	na	k7c4
trh	trh	k1gInSc4
důkladnými	důkladný	k2eAgInPc7d1
bezpečnostními	bezpečnostní	k2eAgInPc7d1
testy	test	k1gInPc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
povinně	povinně	k6eAd1
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
kontrolovány	kontrolován	k2eAgMnPc4d1
výrobcem	výrobce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Tandemový	tandemový	k2eAgInSc1d1
kluzák	kluzák	k1gInSc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
pasivní	pasivní	k2eAgFnSc7d1
bezpečností	bezpečnost	k1gFnSc7
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
část	část	k1gFnSc1
bezpečného	bezpečný	k2eAgInSc2d1
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
druhou	druhý	k4xOgFnSc4
jsou	být	k5eAaImIp3nP
tandemoví	tandemový	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
let	let	k1gInSc4
provádějí	provádět	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
tandemovým	tandemový	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
zkušeným	zkušený	k2eAgMnSc7d1
<g/>
,	,	kIx,
dlouholetým	dlouholetý	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
a	a	k8xC
složit	složit	k5eAaPmF
teoretické	teoretický	k2eAgFnPc4d1
i	i	k8xC
praktické	praktický	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
u	u	k7c2
inspektora	inspektor	k1gMnSc2
paraglidingu	paragliding	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musí	muset	k5eAaImIp3nS
předvést	předvést	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
tandemový	tandemový	k2eAgInSc1d1
kluzák	kluzák	k1gInSc1
dokonale	dokonale	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
<g/>
,	,	kIx,
dokáže	dokázat	k5eAaPmIp3nS
bezpečně	bezpečně	k6eAd1
odstartovat	odstartovat	k5eAaPmF
i	i	k8xC
přistát	přistát	k5eAaImF,k5eAaPmF
a	a	k8xC
má	mít	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
potřebné	potřebný	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
z	z	k7c2
meteorologie	meteorologie	k1gFnSc2
<g/>
,	,	kIx,
aerodynamiky	aerodynamika	k1gFnSc2
a	a	k8xC
leteckých	letecký	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
poté	poté	k6eAd1
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
vydána	vydán	k2eAgFnSc1d1
Licence	licence	k1gFnSc1
tandemového	tandemový	k2eAgMnSc2d1
pilota	pilot	k1gMnSc2
od	od	k7c2
Letecké	letecký	k2eAgFnSc2d1
amatérské	amatérský	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odlišnosti	odlišnost	k1gFnPc1
tandemového	tandemový	k2eAgInSc2d1
paraglidingu	paragliding	k1gInSc2
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
tandemového	tandemový	k2eAgInSc2d1
kluzáku	kluzák	k1gInSc2
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
stejná	stejný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
"	"	kIx"
<g/>
single	singl	k1gInSc5
<g/>
"	"	kIx"
kluzáku	kluzák	k1gInSc2
<g/>
,	,	kIx,
tandemový	tandemový	k2eAgInSc1d1
kluzák	kluzák	k1gInSc1
má	mít	k5eAaImIp3nS
ale	ale	k8xC
přibližně	přibližně	k6eAd1
2	#num#	k4
<g/>
x	x	k?
větší	veliký	k2eAgFnSc4d2
plochu	plocha	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
musí	muset	k5eAaImIp3nS
nést	nést	k5eAaImF
váhu	váha	k1gFnSc4
pilota	pilot	k1gMnSc2
i	i	k8xC
pasažéra	pasažér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tandemový	tandemový	k2eAgInSc1d1
kluzák	kluzák	k1gInSc1
nemá	mít	k5eNaImIp3nS
systém	systém	k1gInSc4
pro	pro	k7c4
zrychlení	zrychlení	k1gNnSc4
letu	let	k1gInSc2
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
single	singl	k1gInSc5
<g/>
"	"	kIx"
kluzák	kluzák	k1gInSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
speed	speed	k1gInSc1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
změnu	změna	k1gFnSc4
rychlosti	rychlost	k1gFnSc2
letu	let	k1gInSc2
je	být	k5eAaImIp3nS
vybaven	vybaven	k2eAgInSc4d1
tzv.	tzv.	kA
trimy	trima	k1gFnSc2
na	na	k7c6
popruzích	popruh	k1gInPc6
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
pilot	pilot	k1gMnSc1
stahuje	stahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
povoluje	povolovat	k5eAaImIp3nS
určité	určitý	k2eAgFnPc4d1
řady	řada	k1gFnPc4
šňůr	šňůra	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
mění	měnit	k5eAaImIp3nS
geometrii	geometrie	k1gFnSc4
kluzáku	kluzák	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
"	"	kIx"
<g/>
single	singl	k1gInSc5
<g/>
"	"	kIx"
kluzáku	kluzák	k1gInSc2
se	se	k3xPyFc4
popruhy	popruh	k1gInPc1
vedoucí	vedoucí	k2eAgInPc1d1
od	od	k7c2
padáku	padák	k1gInSc2
připnou	připnout	k5eAaPmIp3nP
přímo	přímo	k6eAd1
do	do	k7c2
karabiny	karabina	k1gFnSc2
sedačky	sedačka	k1gFnSc2
pilota	pilota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
tandemového	tandemový	k2eAgInSc2d1
kluzáku	kluzák	k1gInSc2
se	se	k3xPyFc4
popruhy	popruha	k1gFnPc1
připnou	připnout	k5eAaPmIp3nP
napřed	napřed	k6eAd1
k	k	k7c3
tzv.	tzv.	kA
rozpěrce	rozpěrka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
jedním	jeden	k4xCgInSc7
koncem	konec	k1gInSc7
připne	připnout	k5eAaPmIp3nS
do	do	k7c2
karabiny	karabina	k1gFnSc2
sedačky	sedačka	k1gFnSc2
pilota	pilot	k1gMnSc2
a	a	k8xC
druhým	druhý	k4xOgInSc7
koncem	konec	k1gInSc7
do	do	k7c2
karabiny	karabina	k1gFnSc2
sedačky	sedačka	k1gFnSc2
pasažéra	pasažér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
rozpěrky	rozpěrka	k1gFnSc2
je	být	k5eAaImIp3nS
výškové	výškový	k2eAgNnSc4d1
vyvážení	vyvážení	k1gNnSc4
pilota	pilot	k1gMnSc2
a	a	k8xC
pasažéra	pasažér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Jestliže	jestliže	k8xS
u	u	k7c2
"	"	kIx"
<g/>
single	singl	k1gInSc5
<g/>
"	"	kIx"
paraglidingu	paragliding	k1gInSc6
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
používá	používat	k5eAaImIp3nS
"	"	kIx"
<g/>
křížový	křížový	k2eAgInSc1d1
start	start	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tj.	tj.	kA
čelem	čelo	k1gNnSc7
k	k	k7c3
padáku	padák	k1gInSc3
<g/>
,	,	kIx,
u	u	k7c2
tandemu	tandem	k1gInSc2
se	se	k3xPyFc4
většinou	většinou	k6eAd1
používá	používat	k5eAaImIp3nS
čelní	čelní	k2eAgInSc4d1
start	start	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
křížový	křížový	k2eAgInSc1d1
start	start	k1gInSc1
je	být	k5eAaImIp3nS
obtížněji	obtížně	k6eAd2
proveditelný	proveditelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tandemový	tandemový	k2eAgInSc1d1
kluzák	kluzák	k1gInSc1
může	moct	k5eAaImIp3nS
startovat	startovat	k5eAaBmF
buď	buď	k8xC
rozběhem	rozběh	k1gInSc7
z	z	k7c2
kopce	kopec	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
ze	z	k7c2
země	zem	k1gFnSc2
pomocí	pomocí	k7c2
odvijáku	odviják	k1gInSc2
či	či	k8xC
navijáku	naviják	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průběh	průběh	k1gInSc4
letu	let	k1gInSc2
a	a	k8xC
přistání	přistání	k1gNnSc2
je	být	k5eAaImIp3nS
shodný	shodný	k2eAgInSc1d1
se	s	k7c7
"	"	kIx"
<g/>
single	singl	k1gInSc5
<g/>
"	"	kIx"
paraglidingem	paragliding	k1gInSc7
<g/>
.	.	kIx.
</s>
