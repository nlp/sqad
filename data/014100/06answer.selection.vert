<s>
Tandemový	tandemový	k2eAgInSc1d1
paragliding	paragliding	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
vyzkoušet	vyzkoušet	k5eAaPmF
si	se	k3xPyFc3
paragliding	paragliding	k1gInSc4
všem	všecek	k3xTgMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
nechtějí	chtít	k5eNaImIp3nP
létat	létat	k5eAaImF
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
jen	jen	k9
chtějí	chtít	k5eAaImIp3nP
zažít	zažít	k5eAaPmF
výjimečný	výjimečný	k2eAgInSc4d1
zážitek	zážitek	k1gInSc4
<g/>
,	,	kIx,
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
let	léto	k1gNnPc2
na	na	k7c6
paraglidingovém	paraglidingový	k2eAgInSc6d1
kluzáku	kluzák	k1gInSc6
bezpochyby	bezpochyby	k6eAd1
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>