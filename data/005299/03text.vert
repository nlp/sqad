<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgNnSc4d1	oficiální
londýnské	londýnský	k2eAgNnSc4d1	Londýnské
sídlo	sídlo	k1gNnSc4	sídlo
britského	britský	k2eAgMnSc2d1	britský
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
královská	královský	k2eAgFnSc1d1	královská
pracovna	pracovna	k1gFnSc1	pracovna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
Buckinghamský	buckinghamský	k2eAgInSc4d1	buckinghamský
palác	palác	k1gInSc4	palác
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
používaný	používaný	k2eAgInSc4d1	používaný
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
vyjádření	vyjádření	k1gNnSc2	vyjádření
pocházející	pocházející	k2eAgNnSc4d1	pocházející
od	od	k7c2	od
členů	člen	k1gInPc2	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
londýnské	londýnský	k2eAgFnSc2d1	londýnská
rezidence	rezidence	k1gFnSc2	rezidence
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
i	i	k9	i
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
akcí	akce	k1gFnPc2	akce
státního	státní	k2eAgInSc2d1	státní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
akcí	akce	k1gFnPc2	akce
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
dvorem	dvůr	k1gInSc7	dvůr
<g/>
,	,	kIx,	,
místem	místo	k1gNnSc7	místo
oficiálních	oficiální	k2eAgInPc2d1	oficiální
uvítání	uvítání	k1gNnSc4	uvítání
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
národního	národní	k2eAgNnSc2d1	národní
veselí	veselí	k1gNnSc2	veselí
a	a	k8xC	a
krizí	krize	k1gFnPc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
domem	dům	k1gInSc7	dům
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgNnSc6	jenž
existují	existovat	k5eAaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Goring	Goring	k1gInSc1	Goring
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
pro	pro	k7c4	pro
lorda	lord	k1gMnSc4	lord
Goringa	Goring	k1gMnSc4	Goring
asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1633	[number]	k4	1633
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
centrum	centrum	k1gNnSc4	centrum
současného	současný	k2eAgInSc2d1	současný
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1703	[number]	k4	1703
pro	pro	k7c4	pro
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Buckinghamu	Buckingham	k1gInSc2	Buckingham
a	a	k8xC	a
Normanby	Normanba	k1gFnSc2	Normanba
<g/>
.	.	kIx.	.
</s>
<s>
Buckingham	Buckingham	k6eAd1	Buckingham
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
Williamem	William	k1gInSc7	William
Windem	Windo	k1gNnSc7	Windo
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
velký	velký	k2eAgInSc1d1	velký
třípatrový	třípatrový	k2eAgInSc1d1	třípatrový
centrální	centrální	k2eAgInSc1d1	centrální
blok	blok	k1gInSc1	blok
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
menšími	malý	k2eAgFnPc7d2	menší
bočními	boční	k2eAgFnPc7d1	boční
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Buckinghamův	Buckinghamův	k2eAgInSc1d1	Buckinghamův
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
prodán	prodat	k5eAaPmNgMnS	prodat
jeho	jeho	k3xOp3gMnSc7	jeho
potomkem	potomek	k1gMnSc7	potomek
<g/>
,	,	kIx,	,
sirem	sir	k1gMnSc7	sir
Charlesem	Charles	k1gMnSc7	Charles
Sheffieldem	Sheffield	k1gMnSc7	Sheffield
<g/>
,	,	kIx,	,
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
sloužit	sloužit	k5eAaImF	sloužit
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgInPc4d1	soukromý
účely	účel	k1gInPc4	účel
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
královy	králův	k2eAgFnPc1d1	králova
manželky	manželka	k1gFnPc1	manželka
Charlotte	Charlott	k1gMnSc5	Charlott
<g/>
,	,	kIx,	,
než	než	k8xS	než
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgFnPc4d1	oficiální
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
sídlem	sídlo	k1gNnSc7	sídlo
nadále	nadále	k6eAd1	nadále
zůstal	zůstat	k5eAaPmAgMnS	zůstat
St	St	kA	St
James	James	k1gMnSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Palace	Palace	k1gFnSc1	Palace
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
dům	dům	k1gInSc1	dům
rozšířit	rozšířit	k5eAaPmF	rozšířit
a	a	k8xC	a
používat	používat	k5eAaImF	používat
ho	on	k3xPp3gMnSc4	on
dále	daleko	k6eAd2	daleko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
St.	st.	kA	st.
James	James	k1gInSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Palace	Palace	k1gFnSc2	Palace
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
tento	tento	k3xDgInSc1	tento
dům	dům	k1gInSc1	dům
na	na	k7c4	na
plně	plně	k6eAd1	plně
vybavený	vybavený	k2eAgInSc4d1	vybavený
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
tím	ten	k3xDgNnSc7	ten
architekta	architekt	k1gMnSc2	architekt
Johna	John	k1gMnSc4	John
Nashe	Nash	k1gMnSc4	Nash
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
palác	palác	k1gInSc1	palác
tvořený	tvořený	k2eAgInSc1d1	tvořený
třemi	tři	k4xCgNnPc7	tři
křídly	křídlo	k1gNnPc7	křídlo
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
Buckinghamským	buckinghamský	k2eAgInSc7d1	buckinghamský
domem	dům	k1gInSc7	dům
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc1d1	použit
kámen	kámen	k1gInSc1	kámen
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Bath	Batha	k1gFnPc2	Batha
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyzdobena	vyzdobit	k5eAaPmNgFnS	vyzdobit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
francouzského	francouzský	k2eAgInSc2d1	francouzský
neoklasicistického	neoklasicistický	k2eAgInSc2d1	neoklasicistický
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
základ	základ	k1gInSc4	základ
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
palác	palác	k1gInSc1	palác
dodnes	dodnes	k6eAd1	dodnes
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
východního	východní	k2eAgNnSc2d1	východní
křídla	křídlo	k1gNnSc2	křídlo
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Mall	Mall	k1gMnSc1	Mall
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
postaven	postavit	k5eAaPmNgInS	postavit
triumfální	triumfální	k2eAgInSc1d1	triumfální
Mramorový	mramorový	k2eAgInSc1d1	mramorový
oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
tohoto	tento	k3xDgInSc2	tento
oblouku	oblouk	k1gInSc2	oblouk
činily	činit	k5eAaImAgFnP	činit
34	[number]	k4	34
450	[number]	k4	450
£	£	k?	£
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
královský	královský	k2eAgInSc1d1	královský
vstup	vstup	k1gInSc1	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
postavit	postavit	k5eAaPmF	postavit
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vrcholu	vrchol	k1gInSc6	vrchol
svoji	svůj	k3xOyFgFnSc4	svůj
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
jezdeckou	jezdecký	k2eAgFnSc4d1	jezdecká
sochu	socha	k1gFnSc4	socha
ale	ale	k8xC	ale
protože	protože	k8xS	protože
zemřel	zemřít	k5eAaPmAgMnS	zemřít
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
dokončením	dokončení	k1gNnSc7	dokončení
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
parlament	parlament	k1gInSc1	parlament
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
umístění	umístění	k1gNnSc6	umístění
na	na	k7c4	na
Trafalgarské	Trafalgarský	k2eAgNnSc4d1	Trafalgarské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výzdobu	výzdoba	k1gFnSc4	výzdoba
interiéru	interiér	k1gInSc2	interiér
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Charles	Charles	k1gMnSc1	Charles
Long	Long	k1gMnSc1	Long
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
jasně	jasně	k6eAd1	jasně
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
imitace	imitace	k1gFnPc4	imitace
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
růžových	růžový	k2eAgInPc2d1	růžový
kaménků	kamének	k1gInPc2	kamének
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlysovanými	vlysovaný	k2eAgInPc7d1	vlysovaný
panely	panel	k1gInPc7	panel
na	na	k7c6	na
stropech	strop	k1gInPc6	strop
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
a	a	k8xC	a
barvité	barvitý	k2eAgFnPc1d1	barvitá
a	a	k8xC	a
silně	silně	k6eAd1	silně
pozlacené	pozlacený	k2eAgFnPc1d1	pozlacená
státní	státní	k2eAgFnPc1d1	státní
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
komnaty	komnata	k1gFnPc1	komnata
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Viléma	Vilém	k1gMnSc2	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jiřího	Jiří	k1gMnSc2	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
narůstající	narůstající	k2eAgInPc1d1	narůstající
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
dostavbu	dostavba	k1gFnSc4	dostavba
paláce	palác	k1gInSc2	palác
setkaly	setkat	k5eAaPmAgInP	setkat
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kritikou	kritika	k1gFnSc7	kritika
britského	britský	k2eAgInSc2d1	britský
parlamentu	parlament	k1gInSc2	parlament
i	i	k8xC	i
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ukončil	ukončit	k5eAaPmAgInS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Nashem	Nash	k1gInSc7	Nash
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
dalšími	další	k2eAgFnPc7d1	další
pracemi	práce	k1gFnPc7	práce
Eduarda	Eduard	k1gMnSc2	Eduard
Blora	Blor	k1gMnSc2	Blor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
více	hodně	k6eAd2	hodně
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
umírněnému	umírněný	k2eAgInSc3d1	umírněný
vkusu	vkus	k1gInSc3	vkus
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
méně	málo	k6eAd2	málo
idealistický	idealistický	k2eAgMnSc1d1	idealistický
architekt	architekt	k1gMnSc1	architekt
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
finance	finance	k1gFnPc4	finance
ponechal	ponechat	k5eAaPmAgMnS	ponechat
to	ten	k3xDgNnSc4	ten
co	co	k9	co
již	již	k6eAd1	již
Nash	Nash	k1gMnSc1	Nash
dokončil	dokončit	k5eAaPmAgMnS	dokončit
ale	ale	k9	ale
další	další	k2eAgFnSc3d1	další
dostavbě	dostavba	k1gFnSc3	dostavba
dal	dát	k5eAaPmAgInS	dát
více	hodně	k6eAd2	hodně
solidní	solidní	k2eAgInSc1d1	solidní
a	a	k8xC	a
méně	málo	k6eAd2	málo
pitoreskní	pitoreskní	k2eAgInSc4d1	pitoreskní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
pořádali	pořádat	k5eAaImAgMnP	pořádat
státní	státní	k2eAgFnSc4d1	státní
a	a	k8xC	a
královské	královský	k2eAgFnSc2d1	královská
akce	akce	k1gFnSc2	akce
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
zde	zde	k6eAd1	zde
nebydleli	bydlet	k5eNaImAgMnP	bydlet
<g/>
,	,	kIx,	,
preferujíce	preferovat	k5eAaImSgMnP	preferovat
Clarence	Clarence	k1gFnPc4	Clarence
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
postavit	postavit	k5eAaPmF	postavit
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
činily	činit	k5eAaImAgFnP	činit
719	[number]	k4	719
000	[number]	k4	000
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Vilém	Vilém	k1gMnSc1	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
jako	jako	k8xS	jako
nové	nový	k2eAgNnSc1d1	nové
sídlo	sídlo	k1gNnSc1	sídlo
parlamentu	parlament	k1gInSc2	parlament
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
a	a	k8xC	a
Westminsterský	Westminsterský	k2eAgInSc4d1	Westminsterský
palác	palác	k1gInSc4	palác
byl	být	k5eAaImAgInS	být
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
<g/>
.	.	kIx.	.
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
královským	královský	k2eAgNnSc7d1	královské
sídlem	sídlo	k1gNnSc7	sídlo
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
státní	státní	k2eAgFnPc1d1	státní
komnaty	komnata	k1gFnPc1	komnata
hýřily	hýřit	k5eAaImAgFnP	hýřit
zlatem	zlato	k1gNnSc7	zlato
a	a	k8xC	a
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
služební	služební	k2eAgFnPc1d1	služební
místnosti	místnost	k1gFnPc1	místnost
neoplývaly	oplývat	k5eNaImAgFnP	oplývat
přílišným	přílišný	k2eAgInSc7d1	přílišný
luxusem	luxus	k1gInSc7	luxus
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
komíny	komín	k1gInPc1	komín
kouřily	kouřit	k5eAaImAgInP	kouřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohně	oheň	k1gInPc4	oheň
v	v	k7c6	v
krbech	krb	k1gInPc6	krb
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
uhašeny	uhašen	k2eAgFnPc1d1	uhašena
a	a	k8xC	a
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
bylo	být	k5eAaImAgNnS	být
chladno	chladno	k6eAd1	chladno
<g/>
.	.	kIx.	.
</s>
<s>
Ventilace	ventilace	k1gFnSc1	ventilace
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
chatrná	chatrný	k2eAgFnSc1d1	chatrná
<g/>
,	,	kIx,	,
že	že	k8xS	že
interiér	interiér	k1gInSc1	interiér
zapáchal	zapáchat	k5eAaImAgInS	zapáchat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
personál	personál	k1gInSc1	personál
byl	být	k5eAaImAgInS	být
nedbalý	dbalý	k2eNgMnSc1d1	nedbalý
a	a	k8xC	a
líný	líný	k2eAgMnSc1d1	líný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
Viktorie	Viktoria	k1gFnSc2	Viktoria
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Albertem	Albert	k1gMnSc7	Albert
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c6	na
reorganizaci	reorganizace	k1gFnSc6	reorganizace
chodu	chod	k1gInSc2	chod
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
odstranění	odstranění	k1gNnSc2	odstranění
jeho	jeho	k3xOp3gInPc2	jeho
nedostatků	nedostatek	k1gInPc2	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgInPc4d1	související
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
křídlo	křídlo	k1gNnSc1	křídlo
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Mall	Mall	k1gInSc4	Mall
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
Viktorie	Viktoria	k1gFnSc2	Viktoria
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Albertem	Albert	k1gMnSc7	Albert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
manželé	manžel	k1gMnPc1	manžel
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
malý	malý	k2eAgInSc1d1	malý
pro	pro	k7c4	pro
dvorní	dvorní	k2eAgInSc4d1	dvorní
život	život	k1gInSc4	život
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
rozrůstající	rozrůstající	k2eAgFnSc4d1	rozrůstající
se	se	k3xPyFc4	se
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
nového	nový	k2eAgNnSc2d1	nové
křídla	křídlo	k1gNnSc2	křídlo
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Eduarda	Eduard	k1gMnSc2	Eduard
Blora	Blor	k1gMnSc2	Blor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
dvoranu	dvorana	k1gFnSc4	dvorana
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
dostavby	dostavba	k1gFnSc2	dostavba
je	být	k5eAaImIp3nS	být
i	i	k9	i
balkón	balkón	k1gInSc1	balkón
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
zdravili	zdravit	k5eAaImAgMnP	zdravit
členové	člen	k1gMnPc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
veřejnost	veřejnost	k1gFnSc1	veřejnost
při	při	k7c6	při
význačných	význačný	k2eAgFnPc6d1	význačná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
postaven	postavit	k5eAaPmNgInS	postavit
taneční	taneční	k2eAgInSc1d1	taneční
sál	sál	k1gInSc1	sál
a	a	k8xC	a
doplněny	doplněn	k2eAgFnPc1d1	doplněna
některé	některý	k3yIgFnPc1	některý
státní	státní	k2eAgFnPc1d1	státní
komnaty	komnata	k1gFnPc1	komnata
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Nashova	Nashův	k2eAgMnSc2d1	Nashův
žáka	žák	k1gMnSc2	žák
Jamese	Jamese	k1gFnSc1	Jamese
Pennethorna	Pennethorna	k1gFnSc1	Pennethorna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
předčasnou	předčasný	k2eAgFnSc7d1	předčasná
smrtí	smrt	k1gFnSc7	smrt
prince	princ	k1gMnSc2	princ
Alberta	Albert	k1gMnSc2	Albert
královna	královna	k1gFnSc1	královna
Viktorie	Viktorie	k1gFnSc1	Viktorie
ráda	rád	k2eAgFnSc1d1	ráda
pořádala	pořádat	k5eAaImAgFnS	pořádat
společenské	společenský	k2eAgFnPc4d1	společenská
taneční	taneční	k1gFnPc4	taneční
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
akce	akce	k1gFnPc1	akce
a	a	k8xC	a
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
hráli	hrát	k5eAaImAgMnP	hrát
významní	významný	k2eAgMnPc1d1	významný
hudebníci	hudebník	k1gMnPc1	hudebník
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zde	zde	k6eAd1	zde
třikrát	třikrát	k6eAd1	třikrát
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
Felix	Felix	k1gMnSc1	Felix
Mendelsson	Mendelsson	k1gMnSc1	Mendelsson
Bartholdy	Bartholda	k1gFnSc2	Bartholda
a	a	k8xC	a
Johann	Johanna	k1gFnPc2	Johanna
Strauss	Straussa	k1gFnPc2	Straussa
mladší	mladý	k2eAgFnSc1d2	mladší
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Straussova	Straussův	k2eAgFnSc1d1	Straussova
Polka	Polka	k1gFnSc1	Polka
pro	pro	k7c4	pro
Alici	Alice	k1gFnSc4	Alice
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
předvedena	předveden	k2eAgFnSc1d1	předvedena
na	na	k7c4	na
poctu	pocta	k1gFnSc4	pocta
královniny	královnin	k2eAgFnSc2d1	Královnina
dcery	dcera	k1gFnSc2	dcera
princezny	princezna	k1gFnSc2	princezna
Alice	Alice	k1gFnSc2	Alice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgInP	konat
časté	častý	k2eAgInPc1d1	častý
maškarní	maškarní	k2eAgInPc1d1	maškarní
bály	bál	k1gInPc1	bál
i	i	k8xC	i
běžné	běžný	k2eAgFnSc2d1	běžná
královské	královský	k2eAgFnSc2d1	královská
ceremonie	ceremonie	k1gFnSc2	ceremonie
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
nechala	nechat	k5eAaPmAgFnS	nechat
přemístit	přemístit	k5eAaPmF	přemístit
Mramorový	mramorový	k2eAgInSc4d1	mramorový
oblouk	oblouk	k1gInSc4	oblouk
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dodnes	dodnes	k6eAd1	dodnes
do	do	k7c2	do
Hyde	Hyd	k1gInSc2	Hyd
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
prince	princa	k1gFnSc6	princa
Alberta	Alberta	k1gFnSc1	Alberta
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
a	a	k8xC	a
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
používala	používat	k5eAaImAgFnS	používat
Windsorský	windsorský	k2eAgInSc4d1	windsorský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
Balmoralský	Balmoralský	k2eAgInSc4d1	Balmoralský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
Osborne	Osborn	k1gInSc5	Osborn
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
používán	používat	k5eAaImNgInS	používat
jen	jen	k6eAd1	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
byla	být	k5eAaImAgFnS	být
královna	královna	k1gFnSc1	královna
po	po	k7c6	po
nátlaku	nátlak	k1gInSc6	nátlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
nucena	nutit	k5eAaImNgFnS	nutit
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
vyhýbala	vyhýbat	k5eAaImAgFnS	vyhýbat
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
Buckinghamskému	buckinghamský	k2eAgInSc3d1	buckinghamský
paláci	palác	k1gInSc3	palác
a	a	k8xC	a
dvorní	dvorní	k2eAgFnSc2d1	dvorní
záležitosti	záležitost	k1gFnSc2	záležitost
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
na	na	k7c6	na
Windsorském	windsorský	k2eAgInSc6d1	windsorský
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Eduarda	Eduard	k1gMnSc2	Eduard
VII	VII	kA	VII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
palác	palác	k1gInSc1	palác
začal	začít	k5eAaPmAgInS	začít
znovu	znovu	k6eAd1	znovu
používat	používat	k5eAaImF	používat
jako	jako	k8xC	jako
královské	královský	k2eAgNnSc1d1	královské
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
nechali	nechat	k5eAaPmAgMnP	nechat
vyzdobit	vyzdobit	k5eAaPmF	vyzdobit
taneční	taneční	k2eAgInSc1d1	taneční
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
vstupní	vstupní	k2eAgFnSc4d1	vstupní
halu	hala	k1gFnSc4	hala
<g/>
,	,	kIx,	,
mramorovou	mramorový	k2eAgFnSc4d1	mramorová
síň	síň	k1gFnSc4	síň
<g/>
,	,	kIx,	,
vestibul	vestibul	k1gInSc1	vestibul
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Belle	bell	k1gInSc5	bell
epoque	epoqu	k1gMnPc4	epoqu
s	s	k7c7	s
převažujícími	převažující	k2eAgFnPc7d1	převažující
barvami	barva	k1gFnPc7	barva
smetanově	smetanově	k6eAd1	smetanově
žlutou	žlutý	k2eAgFnSc4d1	žlutá
a	a	k8xC	a
zlatou	zlatý	k2eAgFnSc4d1	zlatá
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc1	vzhled
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
uchován	uchovat	k5eAaPmNgInS	uchovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
významné	významný	k2eAgFnPc1d1	významná
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
byly	být	k5eAaImAgFnP	být
prováděny	provádět	k5eAaImNgFnP	provádět
za	za	k7c2	za
krále	král	k1gMnSc4	král
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
sir	sir	k1gMnSc1	sir
Aston	Aston	k1gMnSc1	Aston
Webb	Webb	k1gMnSc1	Webb
upravil	upravit	k5eAaPmAgMnS	upravit
původní	původní	k2eAgFnSc4d1	původní
Blorovu	Blorův	k2eAgFnSc4d1	Blorův
fasádu	fasáda	k1gFnSc4	fasáda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připomínala	připomínat	k5eAaImAgFnS	připomínat
Lyme	Lyme	k1gNnSc4	Lyme
Park	park	k1gInSc1	park
v	v	k7c6	v
Cheshire	Cheshir	k1gInSc5	Cheshir
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fasáda	fasáda	k1gFnSc1	fasáda
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
vhodné	vhodný	k2eAgNnSc4d1	vhodné
pozadí	pozadí	k1gNnSc4	pozadí
k	k	k7c3	k
památníku	památník	k1gInSc3	památník
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
serióznější	seriózní	k2eAgFnSc4d2	serióznější
povahu	povaha	k1gFnSc4	povaha
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
spíše	spíše	k9	spíše
místem	místem	k6eAd1	místem
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
aktivit	aktivita	k1gFnPc2	aktivita
než	než	k8xS	než
plesů	ples	k1gInPc2	ples
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
radovánek	radovánka	k1gFnPc2	radovánka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
královna	královna	k1gFnSc1	královna
Marie	Marie	k1gFnSc1	Marie
byla	být	k5eAaImAgFnS	být
znalkyní	znalkyně	k1gFnSc7	znalkyně
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
zajímala	zajímat	k5eAaImAgFnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
královské	královský	k2eAgFnPc4d1	královská
sbírky	sbírka	k1gFnPc4	sbírka
nábytku	nábytek	k1gInSc2	nábytek
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
udržované	udržovaný	k2eAgFnPc1d1	udržovaná
a	a	k8xC	a
doplňované	doplňovaný	k2eAgFnPc1d1	doplňovaná
<g/>
.	.	kIx.	.
</s>
<s>
Postarala	postarat	k5eAaPmAgFnS	postarat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
výzdobu	výzdoba	k1gFnSc4	výzdoba
modrého	modrý	k2eAgInSc2d1	modrý
salónu	salón	k1gInSc2	salón
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
místnosti	místnost	k1gFnSc6	místnost
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
21	[number]	k4	21
m	m	kA	m
dříve	dříve	k6eAd2	dříve
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xS	jako
jižní	jižní	k2eAgInSc4d1	jižní
salón	salón	k1gInSc4	salón
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
Nashových	Nashův	k2eAgFnPc2d1	Nashova
kazetových	kazetový	k2eAgFnPc2d1	kazetová
výzdob	výzdoba	k1gFnPc2	výzdoba
stropů	strop	k1gInPc2	strop
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Olwena	Olwen	k1gMnSc2	Olwen
Hedleye	Hedley	k1gMnSc2	Hedley
ještě	ještě	k6eAd1	ještě
nádhernější	nádherný	k2eAgFnSc1d2	nádhernější
než	než	k8xS	než
v	v	k7c6	v
trůnním	trůnní	k2eAgInSc6d1	trůnní
sále	sál	k1gInSc6	sál
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
měl	mít	k5eAaImAgInS	mít
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
19	[number]	k4	19
státních	státní	k2eAgFnPc2d1	státní
komnat	komnata	k1gFnPc2	komnata
<g/>
,	,	kIx,	,
52	[number]	k4	52
ložnic	ložnice	k1gFnPc2	ložnice
<g/>
,	,	kIx,	,
188	[number]	k4	188
ložnic	ložnice	k1gFnPc2	ložnice
pro	pro	k7c4	pro
personál	personál	k1gInSc4	personál
<g/>
,	,	kIx,	,
92	[number]	k4	92
kanceláří	kancelář	k1gFnPc2	kancelář
a	a	k8xC	a
78	[number]	k4	78
koupelen	koupelna	k1gFnPc2	koupelna
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zdá	zdát	k5eAaPmIp3nS	zdát
jako	jako	k9	jako
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
některými	některý	k3yIgNnPc7	některý
jinými	jiný	k2eAgNnPc7d1	jiné
královskými	královský	k2eAgNnPc7d1	královské
sídly	sídlo	k1gNnPc7	sídlo
například	například	k6eAd1	například
Carskoje	Carskoj	k1gInPc1	Carskoj
selo	selo	k1gNnSc1	selo
<g/>
,	,	kIx,	,
papežovu	papežův	k2eAgInSc3d1	papežův
paláci	palác	k1gInSc3	palác
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nebo	nebo	k8xC	nebo
královským	královský	k2eAgInSc7d1	královský
palácem	palác	k1gInSc7	palác
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
přečkal	přečkat	k5eAaPmAgInS	přečkat
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
domov	domov	k1gInSc4	domov
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
<g/>
.	.	kIx.	.
</s>
<s>
Cennější	cenný	k2eAgInPc1d2	cennější
exponáty	exponát	k1gInPc1	exponát
byly	být	k5eAaImAgInP	být
přestěhovány	přestěhovat	k5eAaPmNgInP	přestěhovat
do	do	k7c2	do
Windsorského	windsorský	k2eAgInSc2d1	windsorský
zámku	zámek	k1gInSc2	zámek
ale	ale	k8xC	ale
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
dále	daleko	k6eAd2	daleko
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
palác	palác	k1gInSc1	palác
sedmkrát	sedmkrát	k6eAd1	sedmkrát
terčem	terč	k1gInSc7	terč
náletů	nálet	k1gInPc2	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
leteckých	letecký	k2eAgFnPc2d1	letecká
pum	puma	k1gFnPc2	puma
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
do	do	k7c2	do
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
čtvercového	čtvercový	k2eAgNnSc2d1	čtvercové
nádvoří	nádvoří	k1gNnSc2	nádvoří
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
rozbito	rozbít	k5eAaPmNgNnS	rozbít
mnoho	mnoho	k4c1	mnoho
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
velké	velký	k2eAgFnPc1d1	velká
škody	škoda	k1gFnPc1	škoda
nebyly	být	k5eNaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
poškozením	poškození	k1gNnSc7	poškození
paláce	palác	k1gInSc2	palác
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc1	zničení
palácové	palácový	k2eAgFnSc2d1	palácová
kaple	kaple	k1gFnSc2	kaple
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
centrem	centrum	k1gNnSc7	centrum
britských	britský	k2eAgFnPc2d1	britská
oslav	oslava	k1gFnPc2	oslava
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
princezna	princezna	k1gFnSc1	princezna
Alžběta	Alžběta	k1gFnSc1	Alžběta
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
se	s	k7c7	s
začerněnými	začerněný	k2eAgNnPc7d1	začerněné
okny	okno	k1gNnPc7	okno
zdravili	zdravit	k5eAaImAgMnP	zdravit
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
shromáždění	shromáždění	k1gNnSc2	shromáždění
na	na	k7c4	na
Mallu	Malla	k1gFnSc4	Malla
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
místnosti	místnost	k1gFnPc1	místnost
paláce	palác	k1gInSc2	palác
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
poschodí	poschodí	k1gNnSc6	poschodí
(	(	kIx(	(
<g/>
piano	piano	k6eAd1	piano
nobile	nobile	k1gMnSc1	nobile
<g/>
)	)	kIx)	)
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
obráceném	obrácený	k2eAgNnSc6d1	obrácené
k	k	k7c3	k
zahradám	zahrada	k1gFnPc3	zahrada
přilehlým	přilehlý	k2eAgFnPc3d1	přilehlá
k	k	k7c3	k
paláci	palác	k1gInSc3	palác
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
souboru	soubor	k1gInSc2	soubor
vyzdobených	vyzdobený	k2eAgFnPc2d1	vyzdobená
státních	státní	k2eAgFnPc2d1	státní
komnat	komnata	k1gFnPc2	komnata
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
salón	salón	k1gInSc4	salón
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výrazný	výrazný	k2eAgInSc1d1	výrazný
oblouk	oblouk	k1gInSc1	oblouk
tvoří	tvořit	k5eAaImIp3nS	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
fasády	fasáda	k1gFnSc2	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
této	tento	k3xDgFnSc2	tento
komnaty	komnata	k1gFnSc2	komnata
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
modrý	modrý	k2eAgInSc1d1	modrý
salón	salón	k1gInSc1	salón
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
salón	salón	k1gInSc1	salón
<g/>
.	.	kIx.	.
</s>
<s>
Spojnici	spojnice	k1gFnSc4	spojnice
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
místností	místnost	k1gFnPc2	místnost
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
obrazová	obrazový	k2eAgFnSc1d1	obrazová
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
50	[number]	k4	50
m.	m.	k?	m.
V	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
obrazy	obraz	k1gInPc1	obraz
význačných	význačný	k2eAgMnPc2d1	význačný
malířů	malíř	k1gMnPc2	malíř
například	například	k6eAd1	například
Rembrandta	Rembrandt	k1gInSc2	Rembrandt
<g/>
,	,	kIx,	,
van	van	k1gInSc1	van
Dycka	Dycko	k1gNnSc2	Dycko
<g/>
,	,	kIx,	,
Rubense	Rubense	k1gFnSc2	Rubense
<g/>
,	,	kIx,	,
Vermeera	Vermeero	k1gNnSc2	Vermeero
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
místnosti	místnost	k1gFnPc4	místnost
přístupné	přístupný	k2eAgFnPc4d1	přístupná
z	z	k7c2	z
galerie	galerie	k1gFnSc2	galerie
jsou	být	k5eAaImIp3nP	být
trůnní	trůnní	k2eAgInSc4d1	trůnní
sál	sál	k1gInSc4	sál
a	a	k8xC	a
zelený	zelený	k2eAgInSc4d1	zelený
salón	salón	k1gInSc4	salón
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
salón	salón	k1gInSc1	salón
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
obrovský	obrovský	k2eAgInSc1d1	obrovský
předpokoj	předpokoj	k1gInSc1	předpokoj
trůnního	trůnní	k2eAgInSc2d1	trůnní
sálu	sál	k1gInSc2	sál
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ceremoniální	ceremoniální	k2eAgFnSc2d1	ceremoniální
trasy	trasa	k1gFnSc2	trasa
ze	z	k7c2	z
strážní	strážní	k2eAgFnSc2d1	strážní
komnaty	komnata	k1gFnSc2	komnata
nad	nad	k7c7	nad
hlavním	hlavní	k2eAgNnSc7d1	hlavní
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
strážní	strážní	k2eAgFnSc6d1	strážní
komnatě	komnata	k1gFnSc6	komnata
stojí	stát	k5eAaImIp3nS	stát
bílá	bílý	k2eAgFnSc1d1	bílá
mramorová	mramorový	k2eAgFnSc1d1	mramorová
socha	socha	k1gFnSc1	socha
prince	princ	k1gMnSc2	princ
Alberta	Albert	k1gMnSc2	Albert
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
oděvu	oděv	k1gInSc6	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
významné	významný	k2eAgFnPc1d1	významná
místnosti	místnost	k1gFnPc1	místnost
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgFnPc1d1	používána
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgFnPc4d1	oficiální
a	a	k8xC	a
ceremoniální	ceremoniální	k2eAgFnPc4d1	ceremoniální
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
státními	státní	k2eAgFnPc7d1	státní
komnatami	komnata	k1gFnPc7	komnata
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
trochu	trochu	k6eAd1	trochu
menších	malý	k2eAgFnPc2d2	menší
místností	místnost	k1gFnPc2	místnost
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k8xC	jako
polostátní	polostátní	k2eAgFnPc4d1	polostátní
komnaty	komnata	k1gFnPc4	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
místnosti	místnost	k1gFnPc1	místnost
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
oficiální	oficiální	k2eAgFnPc4d1	oficiální
příležitosti	příležitost	k1gFnPc4	příležitost
–	–	k?	–
obědy	oběd	k1gInPc4	oběd
a	a	k8xC	a
soukromé	soukromý	k2eAgFnPc4d1	soukromá
audience	audience	k1gFnPc4	audience
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
vyzdobeny	vyzdobit	k5eAaPmNgInP	vyzdobit
a	a	k8xC	a
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgMnPc6d1	jednotlivý
hostech	host	k1gMnPc6	host
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
komnata	komnata	k1gFnSc1	komnata
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyzdobena	vyzdobit	k5eAaPmNgFnS	vyzdobit
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
návštěvy	návštěva	k1gFnSc2	návštěva
ruského	ruský	k2eAgMnSc2d1	ruský
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
I.	I.	kA	I.
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
místností	místnost	k1gFnPc2	místnost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obloukový	obloukový	k2eAgInSc4d1	obloukový
salón	salón	k1gInSc4	salón
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
prochází	procházet	k5eAaImIp3nP	procházet
tisíce	tisíc	k4xCgInPc1	tisíc
hostů	host	k1gMnPc2	host
na	na	k7c4	na
zahradní	zahradní	k2eAgFnPc4d1	zahradní
slavnosti	slavnost	k1gFnPc4	slavnost
pořádané	pořádaný	k2eAgFnPc4d1	pořádaná
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
soukromé	soukromý	k2eAgInPc4d1	soukromý
účely	účel	k1gInPc4	účel
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
místností	místnost	k1gFnPc2	místnost
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1847	[number]	k4	1847
až	až	k6eAd1	až
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Blore	Blor	k1gInSc5	Blor
budoval	budovat	k5eAaImAgMnS	budovat
nové	nový	k2eAgNnSc4d1	nové
východní	východní	k2eAgNnSc4d1	východní
křídlo	křídlo	k1gNnSc4	křídlo
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
orientální	orientální	k2eAgFnSc4d1	orientální
výzdobu	výzdoba	k1gFnSc4	výzdoba
zařízení	zařízení	k1gNnSc2	zařízení
Brightonského	Brightonský	k2eAgInSc2d1	Brightonský
pavilónu	pavilón	k1gInSc2	pavilón
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
jídelna	jídelna	k1gFnSc1	jídelna
a	a	k8xC	a
čínská	čínský	k2eAgFnSc1d1	čínská
jídelna	jídelna	k1gFnSc1	jídelna
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
nábytkem	nábytek	k1gInSc7	nábytek
z	z	k7c2	z
jídelny	jídelna	k1gFnSc2	jídelna
a	a	k8xC	a
hudebního	hudební	k2eAgInSc2d1	hudební
salónu	salón	k1gInSc2	salón
Brightonského	Brightonský	k2eAgInSc2d1	Brightonský
pavilónu	pavilón	k1gInSc2	pavilón
<g/>
.	.	kIx.	.
</s>
<s>
Žlutý	žlutý	k2eAgInSc1d1	žlutý
salón	salón	k1gInSc1	salón
je	být	k5eAaImIp3nS	být
vyzdoben	vyzdoben	k2eAgInSc1d1	vyzdoben
tapetami	tapeta	k1gFnPc7	tapeta
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
pro	pro	k7c4	pro
Brightonský	Brightonský	k2eAgInSc4d1	Brightonský
pavilón	pavilón	k1gInSc4	pavilón
<g/>
,	,	kIx,	,
a	a	k8xC	a
obložení	obložení	k1gNnSc1	obložení
krbu	krb	k1gInSc2	krb
doplněné	doplněná	k1gFnSc2	doplněná
okřídlenými	okřídlený	k2eAgInPc7d1	okřídlený
draky	drak	k1gInPc7	drak
podle	podle	k7c2	podle
evropské	evropský	k2eAgFnSc2d1	Evropská
představy	představa	k1gFnSc2	představa
o	o	k7c6	o
čínském	čínský	k2eAgInSc6d1	čínský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
středu	střed	k1gInSc6	střed
tohoto	tento	k3xDgNnSc2	tento
křídla	křídlo	k1gNnSc2	křídlo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
známý	známý	k2eAgInSc1d1	známý
balkón	balkón	k1gInSc1	balkón
za	za	k7c7	za
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
dveřmi	dveře	k1gFnPc7	dveře
centrální	centrální	k2eAgFnSc2d1	centrální
komnaty	komnata	k1gFnSc2	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
salón	salón	k1gInSc1	salón
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyzdoben	vyzdobit	k5eAaPmNgInS	vyzdobit
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
stylu	styl	k1gInSc6	styl
i	i	k8xC	i
když	když	k8xS	když
lakované	lakovaný	k2eAgFnPc1d1	lakovaná
dveře	dveře	k1gFnPc1	dveře
byly	být	k5eAaImAgFnP	být
přeneseny	přenést	k5eAaPmNgFnP	přenést
z	z	k7c2	z
Brightonu	Brighton	k1gInSc2	Brighton
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
křídle	křídlo	k1gNnSc6	křídlo
nachází	nacházet	k5eAaImIp3nS	nacházet
obrovská	obrovský	k2eAgFnSc1d1	obrovská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
skromně	skromně	k6eAd1	skromně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
hlavní	hlavní	k2eAgFnSc1d1	hlavní
chodba	chodba	k1gFnSc1	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
zrcadlovými	zrcadlový	k2eAgFnPc7d1	zrcadlová
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
skříňkami	skříňka	k1gFnPc7	skříňka
s	s	k7c7	s
čínskými	čínský	k2eAgFnPc7d1	čínská
porcelánovými	porcelánový	k2eAgFnPc7d1	porcelánová
pagodami	pagoda	k1gFnPc7	pagoda
a	a	k8xC	a
jiným	jiný	k2eAgInSc7d1	jiný
orientálním	orientální	k2eAgInSc7d1	orientální
nábytkem	nábytek	k1gInSc7	nábytek
z	z	k7c2	z
Brightonu	Brighton	k1gInSc2	Brighton
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvy	návštěva	k1gFnPc1	návštěva
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
bývají	bývat	k5eAaImIp3nP	bývat
ubytovány	ubytovat	k5eAaPmNgFnP	ubytovat
v	v	k7c6	v
části	část	k1gFnSc6	část
komnat	komnata	k1gFnPc2	komnata
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k8xC	jako
belgické	belgický	k2eAgFnPc1d1	belgická
komnaty	komnata	k1gFnPc1	komnata
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
severního	severní	k2eAgNnSc2d1	severní
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
komnaty	komnata	k1gFnPc4	komnata
vybavené	vybavený	k2eAgFnPc4d1	vybavená
kupolovými	kupolový	k2eAgInPc7d1	kupolový
světlíky	světlík	k1gInPc7	světlík
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vyzdobeny	vyzdobit	k5eAaPmNgFnP	vyzdobit
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
návštěvy	návštěva	k1gFnSc2	návštěva
Albertova	Albertův	k2eAgMnSc2d1	Albertův
strýce	strýc	k1gMnSc2	strýc
Leopolda	Leopold	k1gMnSc2	Leopold
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc2	první
belgického	belgický	k2eAgMnSc2d1	belgický
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
místnostech	místnost	k1gFnPc6	místnost
bydlel	bydlet	k5eAaImAgMnS	bydlet
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
VII	VII	kA	VII
<g/>
.	.	kIx.	.
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
krátkého	krátký	k2eAgNnSc2d1	krátké
panování	panování	k1gNnSc2	panování
<g/>
.	.	kIx.	.
</s>
<s>
Uvádění	uvádění	k1gNnSc1	uvádění
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
povýšení	povýšení	k1gNnSc4	povýšení
do	do	k7c2	do
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
významné	významný	k2eAgFnPc1d1	významná
ceremonie	ceremonie	k1gFnPc1	ceremonie
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgInSc6d1	viktoriánský
tanečním	taneční	k2eAgInSc6d1	taneční
sále	sál	k1gInSc6	sál
<g/>
,	,	kIx,	,
vybudovaném	vybudovaný	k2eAgInSc6d1	vybudovaný
roku	rok	k1gInSc6	rok
1854	[number]	k4	1854
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
37	[number]	k4	37
m	m	kA	m
na	na	k7c4	na
20	[number]	k4	20
m	m	kA	m
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
místnost	místnost	k1gFnSc1	místnost
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgInS	nahradit
trůnní	trůnní	k2eAgInSc1d1	trůnní
sál	sál	k1gInSc1	sál
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
významu	význam	k1gInSc6	význam
a	a	k8xC	a
použití	použití	k1gNnSc6	použití
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
královna	královna	k1gFnSc1	královna
nesedí	sedit	k5eNaImIp3nS	sedit
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgNnSc6d1	vyvýšené
místě	místo	k1gNnSc6	místo
u	u	k7c2	u
trůnu	trůn	k1gInSc2	trůn
pod	pod	k7c7	pod
obrovským	obrovský	k2eAgInSc7d1	obrovský
sametovým	sametový	k2eAgInSc7d1	sametový
baldachýnem	baldachýn	k1gInSc7	baldachýn
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
recepce	recepce	k1gFnSc1	recepce
se	se	k3xPyFc4	se
také	také	k9	také
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
tanečním	taneční	k2eAgInSc6d1	taneční
sále	sál	k1gInSc6	sál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
formální	formální	k2eAgFnPc1d1	formální
večeře	večeře	k1gFnPc1	večeře
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
návštěvy	návštěva	k1gFnSc2	návštěva
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
pozvaných	pozvaný	k2eAgFnPc2d1	pozvaná
osob	osoba	k1gFnPc2	osoba
bývá	bývat	k5eAaImIp3nS	bývat
okolo	okolo	k7c2	okolo
150	[number]	k4	150
a	a	k8xC	a
večeře	večeře	k1gFnSc1	večeře
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
na	na	k7c6	na
zlatém	zlatý	k1gInSc6	zlatý
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
formální	formální	k2eAgFnSc1d1	formální
večeře	večeře	k1gFnSc1	večeře
je	být	k5eAaImIp3nS	být
pořádána	pořádat	k5eAaImNgFnS	pořádat
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
pokaždé	pokaždé	k6eAd1	pokaždé
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
královna	královna	k1gFnSc1	královna
hostí	hostit	k5eAaImIp3nS	hostit
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
diplomaty	diplomat	k1gMnPc4	diplomat
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
jsou	být	k5eAaImIp3nP	být
zpřístupněny	zpřístupněn	k2eAgFnPc1d1	zpřístupněna
a	a	k8xC	a
používány	používán	k2eAgFnPc1d1	používána
všechny	všechen	k3xTgFnPc1	všechen
státní	státní	k2eAgFnPc1d1	státní
komnaty	komnata	k1gFnPc1	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgFnPc1d1	společenská
akce	akce	k1gFnPc1	akce
menšího	malý	k2eAgInSc2d2	menší
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
přijetí	přijetí	k1gNnSc4	přijetí
nových	nový	k2eAgMnPc2d1	nový
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnPc1d2	menší
recepce	recepce	k1gFnPc1	recepce
a	a	k8xC	a
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
radou	rada	k1gFnSc7	rada
jsou	být	k5eAaImIp3nP	být
pořádány	pořádat	k5eAaImNgInP	pořádat
v	v	k7c6	v
komnatě	komnata	k1gFnSc6	komnata
1844	[number]	k4	1844
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
podobnou	podobný	k2eAgFnSc4d1	podobná
akci	akce	k1gFnSc4	akce
pozván	pozván	k2eAgInSc4d1	pozván
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
hostů	host	k1gMnPc2	host
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
využit	využít	k5eAaPmNgInS	využít
hudební	hudební	k2eAgInSc1d1	hudební
salón	salón	k1gInSc1	salón
nebo	nebo	k8xC	nebo
státní	státní	k2eAgFnSc1d1	státní
jídelna	jídelna	k1gFnSc1	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
akcí	akce	k1gFnPc2	akce
tvoří	tvořit	k5eAaImIp3nS	tvořit
stráž	stráž	k1gFnSc1	stráž
příslušníci	příslušník	k1gMnPc1	příslušník
královské	královský	k2eAgFnSc2d1	královská
stráže	stráž	k1gFnSc2	stráž
(	(	kIx(	(
<g/>
Yeomen	Yeomen	k2eAgInSc1d1	Yeomen
Warder	Warder	k1gInSc1	Warder
<g/>
)	)	kIx)	)
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
starobylých	starobylý	k2eAgFnPc6d1	starobylá
uniformách	uniforma	k1gFnPc6	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
akcí	akce	k1gFnSc7	akce
pořádanou	pořádaný	k2eAgFnSc7d1	pořádaná
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
je	být	k5eAaImIp3nS	být
zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
bývá	bývat	k5eAaImIp3nS	bývat
pozváno	pozvat	k5eAaPmNgNnS	pozvat
až	až	k9	až
9	[number]	k4	9
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
hosté	host	k1gMnPc1	host
shromáždí	shromáždět	k5eAaImIp3nP	shromáždět
<g/>
,	,	kIx,	,
zahraje	zahrát	k5eAaPmIp3nS	zahrát
vojenská	vojenský	k2eAgFnSc1d1	vojenská
kapela	kapela	k1gFnSc1	kapela
národní	národní	k2eAgFnSc4d1	národní
hymnu	hymna	k1gFnSc4	hymna
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
vyjde	vyjít	k5eAaPmIp3nS	vyjít
z	z	k7c2	z
obloukového	obloukový	k2eAgInSc2d1	obloukový
salónu	salón	k1gInSc2	salón
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
prochází	procházet	k5eAaImIp3nS	procházet
zahradou	zahrada	k1gFnSc7	zahrada
a	a	k8xC	a
pozve	pozvat	k5eAaPmIp3nS	pozvat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
soukromého	soukromý	k2eAgInSc2d1	soukromý
stanu	stanout	k5eAaPmIp1nS	stanout
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
předem	předem	k6eAd1	předem
vybráni	vybrat	k5eAaPmNgMnP	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
ve	v	k7c4	v
všední	všední	k2eAgInPc4d1	všední
dny	den	k1gInPc4	den
sídlem	sídlo	k1gNnSc7	sídlo
královny	královna	k1gFnSc2	královna
a	a	k8xC	a
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
asi	asi	k9	asi
450	[number]	k4	450
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
zahradní	zahradní	k2eAgFnPc4d1	zahradní
slavnosti	slavnost	k1gFnPc4	slavnost
<g/>
,	,	kIx,	,
audience	audience	k1gFnPc4	audience
a	a	k8xC	a
recepce	recepce	k1gFnPc4	recepce
pozváno	pozván	k2eAgNnSc1d1	pozváno
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
také	také	k9	také
místem	místo	k1gNnSc7	místo
střídání	střídání	k1gNnSc2	střídání
stráží	strážit	k5eAaImIp3nP	strážit
<g/>
,	,	kIx,	,
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
velkému	velký	k2eAgInSc3d1	velký
zajmu	zajmout	k5eAaPmIp1nS	zajmout
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
50	[number]	k4	50
výročí	výročí	k1gNnSc2	výročí
panování	panování	k1gNnSc2	panování
současné	současný	k2eAgFnSc2d1	současná
královny	královna	k1gFnSc2	královna
se	se	k3xPyFc4	se
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
konaly	konat	k5eAaImAgInP	konat
koncerty	koncert	k1gInPc1	koncert
populární	populární	k2eAgFnSc2d1	populární
a	a	k8xC	a
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
kdy	kdy	k6eAd1	kdy
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
měli	mít	k5eAaImAgMnP	mít
štěstí	štěstí	k1gNnSc4	štěstí
v	v	k7c6	v
losování	losování	k1gNnSc6	losování
o	o	k7c4	o
vstupenky	vstupenka	k1gFnPc4	vstupenka
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pozváni	pozvat	k5eAaPmNgMnP	pozvat
do	do	k7c2	do
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
<g/>
.	.	kIx.	.
</s>
<s>
Zpřístupnění	zpřístupnění	k1gNnSc1	zpřístupnění
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
významnou	významný	k2eAgFnSc7d1	významná
změnou	změna	k1gFnSc7	změna
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Prostředky	prostředek	k1gInPc1	prostředek
ze	z	k7c2	z
vstupného	vstupné	k1gNnSc2	vstupné
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
státních	státní	k2eAgFnPc2d1	státní
komnat	komnata	k1gFnPc2	komnata
Windsorského	windsorský	k2eAgInSc2d1	windsorský
zámku	zámek	k1gInSc2	zámek
zničených	zničený	k2eAgMnPc2d1	zničený
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
léto	léto	k1gNnSc1	léto
v	v	k7c6	v
období	období	k1gNnSc6	období
srpen	srpen	k1gInSc4	srpen
až	až	k8xS	až
září	září	k1gNnSc4	září
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
přístupné	přístupný	k2eAgNnSc1d1	přístupné
západní	západní	k2eAgNnSc1d1	západní
křídlo	křídlo	k1gNnSc1	křídlo
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
prohlídek	prohlídka	k1gFnPc2	prohlídka
tvoří	tvořit	k5eAaImIp3nS	tvořit
průvodcovskou	průvodcovský	k2eAgFnSc4d1	průvodcovská
službu	služba	k1gFnSc4	služba
asi	asi	k9	asi
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
50.000	[number]	k4	50.000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
palác	palác	k1gInSc1	palác
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
navštíví	navštívit	k5eAaPmIp3nS	navštívit
hrad	hrad	k1gInSc4	hrad
ročně	ročně	k6eAd1	ročně
cca	cca	kA	cca
360.000	[number]	k4	360.000
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
–	–	k?	–
354.568	[number]	k4	354.568
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
–	–	k?	–
358.941	[number]	k4	358.941
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
–	–	k?	–
364.273	[number]	k4	364.273
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
běžného	běžný	k2eAgInSc2d1	běžný
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
nepatří	patřit	k5eNaImIp3nS	patřit
královně	královna	k1gFnSc3	královna
<g/>
.	.	kIx.	.
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc4d1	buckinghamský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
Windsorský	windsorský	k2eAgInSc4d1	windsorský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
jsou	být	k5eAaImIp3nP	být
státním	státní	k2eAgInSc7d1	státní
majetkem	majetek	k1gInSc7	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
palácem	palác	k1gInSc7	palác
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
zahrady	zahrada	k1gFnPc1	zahrada
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
parku	park	k1gInSc2	park
–	–	k?	–
největší	veliký	k2eAgFnPc4d3	veliký
soukromé	soukromý	k2eAgFnPc4d1	soukromá
zahrady	zahrada	k1gFnPc4	zahrada
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
původního	původní	k2eAgInSc2d1	původní
vzhledu	vzhled	k1gInSc2	vzhled
zahrad	zahrada	k1gFnPc2	zahrada
byl	být	k5eAaImAgInS	být
Capability	Capabilita	k1gFnSc2	Capabilita
Brown	Brown	k1gInSc1	Brown
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahrady	zahrada	k1gFnPc1	zahrada
prošly	projít	k5eAaPmAgFnP	projít
úpravami	úprava	k1gFnPc7	úprava
Williama	William	k1gMnSc2	William
Townsenda	Townsend	k1gMnSc2	Townsend
Ailtona	Ailton	k1gMnSc2	Ailton
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Nashe	Nash	k1gMnSc2	Nash
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgNnSc1d1	umělé
jezero	jezero	k1gNnSc1	jezero
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
napájeno	napájet	k5eAaImNgNnS	napájet
ze	z	k7c2	z
Serpentinového	serpentinový	k2eAgNnSc2d1	serpentinový
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
Hyde	Hyde	k1gFnSc6	Hyde
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
i	i	k9	i
zahrada	zahrada	k1gFnSc1	zahrada
plná	plný	k2eAgFnSc1d1	plná
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvzácnějších	vzácný	k2eAgInPc2d3	nejvzácnější
exponátů	exponát	k1gInPc2	exponát
je	být	k5eAaImIp3nS	být
váza	váza	k1gFnSc1	váza
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
obrovská	obrovský	k2eAgFnSc1d1	obrovská
urna	urna	k1gFnSc1	urna
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
kararského	kararský	k2eAgInSc2d1	kararský
mramoru	mramor	k1gInSc2	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Urnu	urna	k1gFnSc4	urna
nechal	nechat	k5eAaPmAgMnS	nechat
vytesat	vytesat	k5eAaPmF	vytesat
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
ale	ale	k8xC	ale
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jen	jen	k9	jen
hrubě	hrubě	k6eAd1	hrubě
otesána	otesán	k2eAgFnSc1d1	otesána
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
porážce	porážka	k1gFnSc6	porážka
ji	on	k3xPp3gFnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
dokončit	dokončit	k5eAaPmF	dokončit
princ	princ	k1gMnSc1	princ
regent	regent	k1gMnSc1	regent
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
paláce	palác	k1gInSc2	palác
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
královské	královský	k2eAgFnPc1d1	královská
garáže	garáž	k1gFnPc1	garáž
a	a	k8xC	a
stáje	stáj	k1gFnPc1	stáj
<g/>
,	,	kIx,	,
navržené	navržený	k2eAgInPc1d1	navržený
rovněž	rovněž	k6eAd1	rovněž
architektem	architekt	k1gMnSc7	architekt
Johnem	John	k1gMnSc7	John
Nashem	Nash	k1gInSc7	Nash
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěn	umístit	k5eAaPmNgInS	umístit
i	i	k9	i
zlatý	zlatý	k1gInSc1	zlatý
královsky	královsky	k6eAd1	královsky
kočár	kočár	k1gInSc4	kočár
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rokokový	rokokový	k2eAgInSc1d1	rokokový
kočár	kočár	k1gInSc1	kočár
s	s	k7c7	s
malovanými	malovaný	k2eAgInPc7d1	malovaný
panely	panel	k1gInPc7	panel
od	od	k7c2	od
G.	G.	kA	G.
B.	B.	kA	B.
Ciprianiho	Cipriani	k1gMnSc2	Cipriani
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Chambers	Chambers	k1gInSc1	Chambers
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
parlamentu	parlament	k1gInSc2	parlament
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
panovníky	panovník	k1gMnPc4	panovník
používán	používán	k2eAgInSc1d1	používán
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
nebo	nebo	k8xC	nebo
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
výročí	výročí	k1gNnSc2	výročí
korunovace	korunovace	k1gFnSc2	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
stájích	stáj	k1gFnPc6	stáj
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ustájeni	ustájen	k2eAgMnPc1d1	ustájen
koně	kůň	k1gMnPc1	kůň
používaní	používaný	k2eAgMnPc1d1	používaný
pro	pro	k7c4	pro
tažení	tažení	k1gNnSc4	tažení
kočárů	kočár	k1gInPc2	kočár
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Důstojník	důstojník	k1gMnSc1	důstojník
královské	královský	k2eAgFnSc2d1	královská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyvěšují	vyvěšovat	k5eAaImIp3nP	vyvěšovat
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
vlajkou	vlajka	k1gFnSc7	vlajka
vyvěšovanou	vyvěšovaný	k2eAgFnSc7d1	vyvěšovaná
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
královská	královský	k2eAgFnSc1d1	královská
standarta	standarta	k1gFnSc1	standarta
–	–	k?	–
oficiální	oficiální	k2eAgFnSc1d1	oficiální
vlajka	vlajka	k1gFnSc1	vlajka
vládnoucího	vládnoucí	k2eAgMnSc2d1	vládnoucí
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
panovník	panovník	k1gMnSc1	panovník
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
smutku	smutek	k1gInSc2	smutek
nebyla	být	k5eNaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
vyvěšen	vyvěšen	k2eAgInSc4d1	vyvěšen
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
případem	případ	k1gInSc7	případ
byla	být	k5eAaImAgFnS	být
doba	doba	k1gFnSc1	doba
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
předchozího	předchozí	k2eAgMnSc2d1	předchozí
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
vlajka	vlajka	k1gFnSc1	vlajka
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
příslušníka	příslušník	k1gMnSc2	příslušník
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Diany	Diana	k1gFnSc2	Diana
<g/>
,	,	kIx,	,
princezny	princezna	k1gFnSc2	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
veřejnost	veřejnost	k1gFnSc1	veřejnost
přijala	přijmout	k5eAaPmAgFnS	přijmout
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nad	nad	k7c7	nad
palácem	palác	k1gInSc7	palác
není	být	k5eNaImIp3nS	být
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
vlajka	vlajka	k1gFnSc1	vlajka
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Balmoralském	Balmoralský	k2eAgInSc6d1	Balmoralský
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nad	nad	k7c7	nad
palácem	palác	k1gInSc7	palác
nevlála	vlát	k5eNaImAgFnS	vlát
žádná	žádný	k3yNgFnSc1	žádný
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
porušit	porušit	k5eAaPmF	porušit
protokol	protokol	k1gInSc4	protokol
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
vyvěsit	vyvěsit	k5eAaPmF	vyvěsit
v	v	k7c4	v
den	den	k1gInSc4	den
pohřbu	pohřeb	k1gInSc2	pohřeb
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
Union	union	k1gInSc1	union
Jack	Jack	k1gInSc1	Jack
<g/>
)	)	kIx)	)
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
vyvěšována	vyvěšován	k2eAgFnSc1d1	vyvěšována
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
královna	královna	k1gFnSc1	královna
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zemře	zemřít	k5eAaPmIp3nS	zemřít
člen	člen	k1gMnSc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
státního	státní	k2eAgInSc2d1	státní
smutku	smutek	k1gInSc2	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
spojení	spojení	k1gNnSc1	spojení
–	–	k?	–
metro	metro	k1gNnSc1	metro
–	–	k?	–
Green	Green	k2eAgInSc1d1	Green
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
St	St	kA	St
James	James	k1gMnSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Park	park	k1gInSc4	park
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Buckinghamský	buckinghamský	k2eAgInSc4d1	buckinghamský
palác	palác	k1gInSc4	palác
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
Galerie	galerie	k1gFnSc2	galerie
Buckinghamský	buckinghamský	k2eAgInSc1d1	buckinghamský
palác	palác	k1gInSc1	palác
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
