<s>
Významným	významný	k2eAgInSc7d1	významný
symbolem	symbol	k1gInSc7	symbol
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
i	i	k9	i
kimono	kimono	k1gNnSc1	kimono
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgInSc1d1	tradiční
japonský	japonský	k2eAgInSc1d1	japonský
oděv	oděv	k1gInSc1	oděv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
viděný	viděný	k2eAgInSc1d1	viděný
na	na	k7c6	na
umělkyních	umělkyně	k1gFnPc6	umělkyně
zvaných	zvaný	k2eAgFnPc2d1	zvaná
gejša	gejša	k1gFnSc1	gejša
(	(	kIx(	(
<g/>
společnice	společnice	k1gFnPc1	společnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
baví	bavit	k5eAaImIp3nP	bavit
tradičními	tradiční	k2eAgInPc7d1	tradiční
tanci	tanec	k1gInPc7	tanec
<g/>
,	,	kIx,	,
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
konverzací	konverzace	k1gFnSc7	konverzace
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
běžný	běžný	k2eAgInSc1d1	běžný
oděv	oděv	k1gInSc1	oděv
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nenosí	nosit	k5eNaImIp3nP	nosit
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
(	(	kIx(	(
<g/>
v	v	k7c6	v
rodinách	rodina	k1gFnPc6	rodina
či	či	k8xC	či
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
ctících	ctící	k2eAgMnPc2d1	ctící
tradici	tradice	k1gFnSc4	tradice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
příležitostech	příležitost	k1gFnPc6	příležitost
(	(	kIx(	(
<g/>
svatba	svatba	k1gFnSc1	svatba
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
</s>
