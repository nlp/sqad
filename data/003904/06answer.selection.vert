<s>
Irsko	Irsko	k1gNnSc1	Irsko
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
provincií	provincie	k1gFnPc2	provincie
–	–	k?	–
Connacht	Connachta	k1gFnPc2	Connachta
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Connachta	Connacht	k1gMnSc2	Connacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leinster	Leinster	k1gMnSc1	Leinster
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Laighin	Laighin	k2eAgInSc1d1	Laighin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Munster	Munster	k1gMnSc1	Munster
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
an	an	k?	an
Mhumhain	Mhumhain	k1gInSc1	Mhumhain
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ulster	Ulster	k1gInSc4	Ulster
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Uladh	Uladh	k1gInSc1	Uladh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
existovala	existovat	k5eAaImAgFnS	existovat
provincie	provincie	k1gFnSc1	provincie
Meath	Meath	k1gMnSc1	Meath
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
an	an	k?	an
Mhí	Mhí	k1gFnSc1	Mhí
<g/>
)	)	kIx)	)
–	–	k?	–
tzv.	tzv.	kA	tzv.
Střední	střední	k2eAgNnSc1d1	střední
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
