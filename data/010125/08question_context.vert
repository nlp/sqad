<s>
K2	K2	k4	K2
(	(	kIx(	(
<g/>
baltsky	baltsky	k6eAd1	baltsky
Čhogori	Čhogori	k1gNnSc1	Čhogori
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Čchokori	Čchokor	k1gMnPc1	Čchokor
<g/>
,	,	kIx,	,
urdsky	urdsky	k6eAd1	urdsky
ک	ک	k?	ک
ٹ	ٹ	k?	ٹ
<g/>
,	,	kIx,	,
transliterováno	transliterován	k2eAgNnSc4d1	transliterován
Ke	k	k7c3	k
tū	tū	k?	tū
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Lambá	Lambý	k2eAgFnSc1d1	Lambý
Pahár	Pahár	k1gInSc1	Pahár
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
乔	乔	k?	乔
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc4d1	oficiální
přepis	přepis	k1gInSc4	přepis
Qogir	Qogir	k1gMnSc1	Qogir
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gMnSc1	pinyin
Qiáogē	Qiáogē	k1gMnSc1	Qiáogē
Fē	Fē	k1gMnSc1	Fē
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Čchiao-ke	Čchiao	k1gFnSc2	Čchiao-k
<g/>
-li	i	k?	-li
feng	feng	k1gInSc1	feng
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
pod	pod	k7c7	pod
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
Mount	Mount	k1gInSc1	Mount
Godwin-Austen	Godwin-Austen	k2eAgInSc1d1	Godwin-Austen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Karákóram	Karákóram	k1gInSc1	Karákóram
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
pákistánskou	pákistánský	k2eAgFnSc7d1	pákistánská
částí	část	k1gFnSc7	část
Kašmíru	Kašmír	k1gInSc2	Kašmír
a	a	k8xC	a
čínskou	čínský	k2eAgFnSc7d1	čínská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
Sin-ťiang	Sin-ťianga	k1gFnPc2	Sin-ťianga
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
8611	[number]	k4	8611
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>

