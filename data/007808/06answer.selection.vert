<s>
Poprvé	poprvé	k6eAd1	poprvé
sice	sice	k8xC	sice
získal	získat	k5eAaPmAgMnS	získat
filmovou	filmový	k2eAgFnSc4d1	filmová
roli	role	k1gFnSc4	role
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
v	v	k7c6	v
němém	němý	k2eAgInSc6d1	němý
filmu	film	k1gInSc6	film
Přemysla	Přemysl	k1gMnSc2	Přemysl
Pražského	pražský	k2eAgInSc2d1	pražský
Neznámá	známý	k2eNgFnSc1d1	neznámá
kráska	kráska	k1gFnSc1	kráska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
jen	jen	k9	jen
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
roličkách	rolička	k1gFnPc6	rolička
<g/>
.	.	kIx.	.
</s>
