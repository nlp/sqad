<s>
Lichess	Lichess	k6eAd1
</s>
<s>
Lichess	Lichess	k1gInSc1
je	být	k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
a	a	k8xC
otevřený	otevřený	k2eAgInSc1d1
internetový	internetový	k2eAgInSc1d1
šachový	šachový	k2eAgInSc1d1
server	server	k1gInSc1
provozován	provozovat	k5eAaImNgInS
stejnojmennou	stejnojmenný	k2eAgFnSc7d1
neziskovou	ziskový	k2eNgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
platformě	platforma	k1gFnSc6
může	moct	k5eAaImIp3nS
hrát	hrát	k5eAaImF
šachy	šach	k1gInPc4
kdokoliv	kdokoliv	k3yInSc1
bez	bez	k7c2
jakékoliv	jakýkoliv	k3yIgFnSc2
registrace	registrace	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
registrace	registrace	k1gFnSc1
mj.	mj.	kA
umožňuje	umožňovat	k5eAaImIp3nS
hrát	hrát	k5eAaImF
hodnocené	hodnocený	k2eAgFnPc4d1
partie	partie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc1
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
placené	placený	k2eAgFnPc4d1
reklamy	reklama	k1gFnPc4
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc4
funkce	funkce	k1gFnPc4
jsou	být	k5eAaImIp3nP
zdarma	zdarma	k6eAd1
a	a	k8xC
financován	financován	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
z	z	k7c2
dobrovolných	dobrovolný	k2eAgInPc2d1
darů	dar	k1gInPc2
jeho	jeho	k3xOp3gMnPc2
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
a	a	k8xC
hlavní	hlavní	k2eAgMnSc1d1
programátor	programátor	k1gMnSc1
Lichess	Lichessa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lichess	Lichess	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
francouzským	francouzský	k2eAgInSc7d1
programátorem	programátor	k1gInSc7
Thibaultem	Thibault	k1gInSc7
Duplessisem	Duplessis	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Software	software	k1gInSc1
a	a	k8xC
design	design	k1gInSc4
stránky	stránka	k1gFnSc2
jsou	být	k5eAaImIp3nP
open	open	k1gMnSc1
source	source	k1gMnSc1
a	a	k8xC
pod	pod	k7c7
licencí	licence	k1gFnSc7
AGPL	AGPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
oficiální	oficiální	k2eAgFnSc1d1
mobilní	mobilní	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
Lichess	Lichess	k1gFnPc2
pro	pro	k7c4
zařízení	zařízení	k1gNnSc4
Android	android	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
iOS	iOS	k?
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2020	#num#	k4
byl	být	k5eAaImAgMnS
na	na	k7c6
žebříčku	žebříček	k1gInSc6
Alexa	Alexa	k1gMnSc1
na	na	k7c6
1780	#num#	k4
místě	místo	k1gNnSc6
a	a	k8xC
podle	podle	k7c2
statistik	statistika	k1gFnPc2
většina	většina	k1gFnSc1
návštěvníků	návštěvník	k1gMnPc2
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
<g/>
,	,	kIx,
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
popularity	popularita	k1gFnSc2
šachových	šachový	k2eAgInPc2d1
serverů	server	k1gInPc2
je	být	k5eAaImIp3nS
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
za	za	k7c7
serverem	server	k1gInSc7
Chess	Chess	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lichess	Lichess	k6eAd1
pravidelně	pravidelně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
turnaj	turnaj	k1gInSc4
pro	pro	k7c4
titulované	titulovaný	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
s	s	k7c7
peněžními	peněžní	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
a	a	k8xC
každé	každý	k3xTgInPc4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
tzv.	tzv.	kA
šachový	šachový	k2eAgInSc4d1
maraton	maraton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Financování	financování	k1gNnSc1
serverů	server	k1gInPc2
a	a	k8xC
programátora	programátor	k1gMnSc2
je	být	k5eAaImIp3nS
zajištěno	zajistit	k5eAaPmNgNnS
dary	dar	k1gInPc1
uživatelů	uživatel	k1gMnPc2
Lichess	Lichessa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Funkce	funkce	k1gFnSc1
</s>
<s>
Lichess	Lichess	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
hrát	hrát	k5eAaImF
živé	živý	k2eAgInPc4d1
i	i	k8xC
korespondenční	korespondenční	k2eAgInPc4d1
šachy	šach	k1gInPc4
proti	proti	k7c3
jiným	jiný	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
sekce	sekce	k1gFnSc1
pro	pro	k7c4
tréninik	tréninik	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
základy	základ	k1gInPc1
šachu	šach	k1gInSc2
<g/>
,	,	kIx,
taktické	taktický	k2eAgFnPc1d1
úlohy	úloha	k1gFnPc1
<g/>
,	,	kIx,
šachová	šachový	k2eAgFnSc1d1
videotéka	videotéka	k1gFnSc1
<g/>
,	,	kIx,
průzkumník	průzkumník	k1gMnSc1
zahájení	zahájení	k1gNnSc2
<g/>
,	,	kIx,
prostor	prostor	k1gInSc4
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
šachových	šachový	k2eAgFnPc2d1
studií	studie	k1gFnPc2
a	a	k8xC
analýza	analýza	k1gFnSc1
šachových	šachový	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
také	také	k9
sekce	sekce	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
mohou	moct	k5eAaImIp3nP
šachoví	šachový	k2eAgMnPc1d1
trenéři	trenér	k1gMnPc1
nabízet	nabízet	k5eAaImF
své	svůj	k3xOyFgFnPc4
služby	služba	k1gFnPc4
ostatním	ostatní	k2eAgMnPc3d1
uživatelům	uživatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Lichess	Lichess	k6eAd1
má	mít	k5eAaImIp3nS
zabudované	zabudovaný	k2eAgFnPc4d1
následující	následující	k2eAgFnPc4d1
šachové	šachový	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Žravé	žravý	k2eAgInPc1d1
šachy	šach	k1gInPc1
</s>
<s>
Atomové	atomový	k2eAgInPc1d1
šachy	šach	k1gInPc1
</s>
<s>
Chess	Chess	k6eAd1
<g/>
960	#num#	k4
</s>
<s>
Crazyhouse	Crazyhouse	k6eAd1
</s>
<s>
Horde	Horde	k6eAd1
</s>
<s>
Král	Král	k1gMnSc1
do	do	k7c2
středu	střed	k1gInSc2
šachovnice	šachovnice	k1gFnSc2
</s>
<s>
Závod	závod	k1gInSc1
králů	král	k1gMnPc2
</s>
<s>
Třikrát	třikrát	k6eAd1
šach	šach	k1gInSc1
</s>
<s>
Lichess	Lichess	k6eAd1
jako	jako	k9
první	první	k4xOgFnSc1
nabízela	nabízet	k5eAaImAgFnS
pomoc	pomoc	k1gFnSc1
pro	pro	k7c4
zrakově	zrakově	k6eAd1
postižené	postižený	k1gMnPc4
při	při	k7c6
hraní	hraní	k1gNnSc6
šachu	šach	k1gInSc2
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Lze	lze	k6eAd1
také	také	k9
hrát	hrát	k5eAaImF
proti	proti	k7c3
šachovému	šachový	k2eAgInSc3d1
motoru	motor	k1gInSc3
Stockfish	Stockfish	k1gInSc4
či	či	k8xC
analyzovat	analyzovat	k5eAaImF
pozice	pozice	k1gFnPc4
pomocí	pomocí	k7c2
téhož	týž	k3xTgInSc2
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc4
také	také	k9
nabízí	nabízet	k5eAaImIp3nS
knihu	kniha	k1gFnSc4
zahájení	zahájení	k1gNnSc2
s	s	k7c7
partiemi	partie	k1gFnPc7
titulovaných	titulovaný	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
FIDE	FIDE	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
registrované	registrovaný	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
používá	používat	k5eAaImIp3nS
Lichess	Lichess	k1gInSc1
systém	systém	k1gInSc1
hodnocení	hodnocení	k1gNnSc6
Glicko-	Glicko-	k1gFnSc2
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Lichess	Lichessa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DUPLESSIS	DUPLESSIS	kA
<g/>
,	,	kIx,
Thibault	Thibault	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
is	is	k?
lichess	lichess	k6eAd1
free	freat	k5eAaPmIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichessa	k1gFnPc2
<g/>
,	,	kIx,
2014-07-02	2014-07-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Lichess	Lichessa	k1gFnPc2
features	featuresa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lichess	Lichess	k1gInSc1
Patron	patron	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GRAVAGNA	GRAVAGNA	kA
<g/>
,	,	kIx,
Pierre	Pierr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carnet	Carnet	k1gMnSc1
d	d	k?
<g/>
’	’	k?
<g/>
échecs	échecs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libération	Libération	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
About	About	k2eAgInSc4d1
lichess	lichess	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
License	License	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
lichess	lichess	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexa	Alexa	k1gMnSc1
Internet	Internet	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chess	Chess	k1gInSc1
Links	Links	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chess	Chess	k1gInSc1
Links	Links	k1gInSc4
and	and	k?
Websites	Websites	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
lichess	lichess	k1gInSc1
costs	costs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lichess	Lichess	k1gInSc1
variants	variants	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FAQ	FAQ	kA
Ratings	Ratings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichess	Lichess	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Blog	Blog	k1gInSc1
lichess	lichess	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
