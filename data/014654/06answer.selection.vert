<s>
Lichess	Lichess	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
francouzským	francouzský	k2eAgInSc7d1
programátorem	programátor	k1gInSc7
Thibaultem	Thibault	k1gInSc7
Duplessisem	Duplessis	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Software	software	k1gInSc1
a	a	k8xC
design	design	k1gInSc4
stránky	stránka	k1gFnSc2
jsou	být	k5eAaImIp3nP
open	open	k1gMnSc1
source	source	k1gMnSc1
a	a	k8xC
pod	pod	k7c7
licencí	licence	k1gFnSc7
AGPL	AGPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
oficiální	oficiální	k2eAgFnSc1d1
mobilní	mobilní	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
Lichess	Lichess	k1gFnPc2
pro	pro	k7c4
zařízení	zařízení	k1gNnSc4
Android	android	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
iOS	iOS	k?
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>