<p>
<s>
Uzi	Uzi	k?	Uzi
Bar	bar	k1gInSc1	bar
<g/>
'	'	kIx"	'
<g/>
am	am	k?	am
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ע	ע	k?	ע
ב	ב	k?	ב
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
izraelské	izraelský	k2eAgFnSc2d1	izraelská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Uzi	Uzi	k?	Uzi
Baram	Baram	k1gInSc1	Baram
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
britské	britský	k2eAgFnSc2d1	britská
mandátní	mandátní	k2eAgFnSc2d1	mandátní
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Moše	mocha	k1gFnSc3	mocha
Bar	bar	k1gInSc1	bar
<g/>
'	'	kIx"	'
<g/>
am	am	k?	am
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
za	za	k7c4	za
strany	strana	k1gFnPc4	strana
Mapaj	Mapaj	k1gFnSc4	Mapaj
a	a	k8xC	a
Ma	Ma	k1gFnSc4	Ma
<g/>
'	'	kIx"	'
<g/>
arach	arach	k1gInSc4	arach
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Grazia	Grazium	k1gNnSc2	Grazium
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
syrském	syrský	k2eAgInSc6d1	syrský
Aleppu	Alepp	k1gInSc6	Alepp
<g/>
.	.	kIx.	.
</s>
<s>
Baram	Baram	k1gInSc1	Baram
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
v	v	k7c6	v
jeruzalémské	jeruzalémský	k2eAgFnSc6d1	Jeruzalémská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Nachlaot	Nachlaota	k1gFnPc2	Nachlaota
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
obor	obor	k1gInSc1	obor
politologie	politologie	k1gFnSc1	politologie
a	a	k8xC	a
sociologie	sociologie	k1gFnSc1	sociologie
na	na	k7c6	na
Hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
během	během	k7c2	během
studií	studie	k1gFnPc2	studie
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
studentské	studentský	k2eAgFnSc2d1	studentská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Baram	Baram	k1gInSc1	Baram
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
Knesetu	Kneset	k1gInSc2	Kneset
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Ma	Ma	k1gFnSc2	Ma
<g/>
'	'	kIx"	'
<g/>
arach	arach	k1gMnSc1	arach
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c4	o
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
přišel	přijít	k5eAaPmAgMnS	přijít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
zvolen	zvolen	k2eAgMnSc1d1	zvolen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
a	a	k8xC	a
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Ma	Ma	k1gMnSc1	Ma
<g/>
'	'	kIx"	'
<g/>
arach	arach	k1gMnSc1	arach
transformoval	transformovat	k5eAaBmAgMnS	transformovat
ve	v	k7c4	v
Stranu	strana	k1gFnSc4	strana
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
posledních	poslední	k2eAgFnPc6d1	poslední
zmíněných	zmíněný	k2eAgFnPc6d1	zmíněná
volbách	volba	k1gFnPc6	volba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1992	[number]	k4	1992
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
turismu	turismus	k1gInSc2	turismus
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Jicchaka	Jicchak	k1gMnSc2	Jicchak
Rabina	Rabin	k1gMnSc2	Rabin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k9	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
ve	v	k7c4	v
funkci	funkce	k1gFnSc4	funkce
nahradil	nahradit	k5eAaPmAgMnS	nahradit
David	David	k1gMnSc1	David
Libaj	Libaj	k1gMnSc1	Libaj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
vraždě	vražda	k1gFnSc6	vražda
premiéra	premiér	k1gMnSc2	premiér
Rabina	Rabin	k1gMnSc2	Rabin
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
premiérem	premiér	k1gMnSc7	premiér
Šimon	Šimon	k1gMnSc1	Šimon
Peres	Peres	k1gMnSc1	Peres
<g/>
,	,	kIx,	,
Bar	bar	k1gInSc1	bar
<g/>
'	'	kIx"	'
<g/>
amovi	amoev	k1gFnSc6	amoev
byl	být	k5eAaImAgMnS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
ponechán	ponechat	k5eAaPmNgInS	ponechat
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgInS	udržet
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
však	však	k9	však
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
strana	strana	k1gFnSc1	strana
Likud	Likud	k1gInSc1	Likud
a	a	k8xC	a
Bar	bar	k1gInSc1	bar
<g/>
'	'	kIx"	'
<g/>
am	am	k?	am
tak	tak	k6eAd1	tak
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
ministerskou	ministerský	k2eAgFnSc4d1	ministerská
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
za	za	k7c4	za
levicovou	levicový	k2eAgFnSc4d1	levicová
alianci	aliance	k1gFnSc4	aliance
Jeden	jeden	k4xCgInSc4	jeden
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2001	[number]	k4	2001
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Efi	Efi	k1gMnSc7	Efi
Ošajou	Ošaja	k1gMnSc7	Ošaja
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Uzi	Uzi	k1gFnSc2	Uzi
Baram	Baram	k1gInSc1	Baram
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kneset	Kneset	k1gInSc1	Kneset
–	–	k?	–
Uzi	Uzi	k1gFnSc1	Uzi
Bar	bar	k1gInSc1	bar
<g/>
'	'	kIx"	'
<g/>
am	am	k?	am
</s>
</p>
