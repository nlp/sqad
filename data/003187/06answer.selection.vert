<s>
Metallice	Metallice	k1gFnSc1	Metallice
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
mezi	mezi	k7c4	mezi
40	[number]	k4	40
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
s	s	k7c7	s
albem	album	k1gNnSc7	album
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
s	s	k7c7	s
albem	album	k1gNnSc7	album
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc2	justice
for	forum	k1gNnPc2	forum
All	All	k1gMnPc2	All
dobyla	dobýt	k5eAaPmAgFnS	dobýt
šesté	šestý	k4xOgNnSc1	šestý
místo	místo	k1gNnSc1	místo
<g/>
;	;	kIx,	;
alba	alba	k1gFnSc1	alba
kapel	kapela	k1gFnPc2	kapela
Megadeth	Megadeth	k1gInSc4	Megadeth
a	a	k8xC	a
Anthrax	Anthrax	k1gInSc4	Anthrax
měli	mít	k5eAaImAgMnP	mít
rovněž	rovněž	k9	rovněž
alba	album	k1gNnSc2	album
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
