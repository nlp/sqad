<s>
Havana	Havana	k1gFnSc1	Havana
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
de	de	k?	de
La	la	k1gNnSc1	la
Habana	Habana	k1gFnSc1	Habana
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zkracováno	zkracovat	k5eAaImNgNnS	zkracovat
na	na	k7c4	na
La	la	k1gNnSc4	la
Habana	Haban	k1gMnSc2	Haban
<g/>
,	,	kIx,	,
IPA	IPA	kA	IPA
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
la	la	k1gNnSc1	la
aˈ	aˈ	k?	aˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Kuby	Kuba	k1gFnSc2	Kuba
přezdívané	přezdívaný	k2eAgFnSc2d1	přezdívaná
jako	jako	k8xS	jako
Paříž	Paříž	k1gFnSc4	Paříž
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
