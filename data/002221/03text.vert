<s>
Havana	Havana	k1gFnSc1	Havana
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
de	de	k?	de
La	la	k1gNnSc1	la
Habana	Habana	k1gFnSc1	Habana
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zkracováno	zkracovat	k5eAaImNgNnS	zkracovat
na	na	k7c4	na
La	la	k1gNnSc4	la
Habana	Haban	k1gMnSc2	Haban
<g/>
,	,	kIx,	,
IPA	IPA	kA	IPA
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
la	la	k1gNnSc1	la
aˈ	aˈ	k?	aˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Kuby	Kuba	k1gFnSc2	Kuba
přezdívané	přezdívaný	k2eAgFnSc2d1	přezdívaná
jako	jako	k8xS	jako
Paříž	Paříž	k1gFnSc4	Paříž
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
2,2	[number]	k4	2,2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
160	[number]	k4	160
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
floridského	floridský	k2eAgMnSc2d1	floridský
Key	Key	k1gMnSc2	Key
Westu	West	k1gInSc2	West
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Havana	havana	k1gNnSc2	havana
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
15	[number]	k4	15
kubánských	kubánský	k2eAgFnPc2d1	kubánská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
201	[number]	k4	201
610	[number]	k4	610
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světových	světový	k2eAgFnPc2d1	světová
památek	památka	k1gFnPc2	památka
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
Havana	Havana	k1gFnSc1	Havana
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Evropany	Evropan	k1gMnPc4	Evropan
prozkoumán	prozkoumán	k2eAgInSc1d1	prozkoumán
roku	rok	k1gInSc2	rok
1509	[number]	k4	1509
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1515	[number]	k4	1515
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
současné	současný	k2eAgNnSc1d1	současné
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
zakladatelem	zakladatel	k1gMnSc7	zakladatel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
conquistador	conquistador	k1gMnSc1	conquistador
Diego	Diego	k1gMnSc1	Diego
Velázquez	Velázquez	k1gMnSc1	Velázquez
de	de	k?	de
Cuéllar	Cuéllar	k1gMnSc1	Cuéllar
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
Havany	Havana	k1gFnSc2	Havana
byly	být	k5eAaImAgFnP	být
vysílány	vysílán	k2eAgFnPc1d1	vysílána
další	další	k2eAgFnPc1d1	další
dobyvatelské	dobyvatelský	k2eAgFnPc1d1	dobyvatelská
výpravy	výprava	k1gFnPc1	výprava
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
koutů	kout	k1gInPc2	kout
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Havana	havana	k1gNnSc2	havana
vzkvétala	vzkvétat	k5eAaImAgFnS	vzkvétat
jako	jako	k9	jako
centrum	centrum	k1gNnSc4	centrum
koloniální	koloniální	k2eAgFnSc2d1	koloniální
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
<g/>
;	;	kIx,	;
dokonce	dokonce	k9	dokonce
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
jako	jako	k8xS	jako
Paříž	Paříž	k1gFnSc1	Paříž
Antil	Antily	k1gFnPc2	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
dnes	dnes	k6eAd1	dnes
známých	známý	k2eAgFnPc2d1	známá
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
přibyly	přibýt	k5eAaPmAgInP	přibýt
různé	různý	k2eAgInPc1d1	různý
hotely	hotel	k1gInPc1	hotel
a	a	k8xC	a
herny	herna	k1gFnPc1	herna
<g/>
,	,	kIx,	,
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
zločinnost	zločinnost	k1gFnSc4	zločinnost
<g/>
,	,	kIx,	,
prosperitu	prosperita	k1gFnSc4	prosperita
však	však	k9	však
přinášeli	přinášet	k5eAaImAgMnP	přinášet
američtí	americký	k2eAgMnPc1d1	americký
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
sem	sem	k6eAd1	sem
jezdily	jezdit	k5eAaImAgInP	jezdit
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
prvního	první	k4xOgMnSc2	první
výročí	výročí	k1gNnSc1	výročí
vypálení	vypálení	k1gNnSc1	vypálení
Lidic	Lidice	k1gInPc2	Lidice
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
havanské	havanský	k2eAgNnSc1d1	havanské
náměstí	náměstí	k1gNnSc1	náměstí
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
"	"	kIx"	"
<g/>
Plaza	plaz	k1gMnSc4	plaz
Lidice	Lidice	k1gInPc1	Lidice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
havanském	havanský	k2eAgInSc6d1	havanský
hotelu	hotel	k1gInSc6	hotel
"	"	kIx"	"
<g/>
Hotel	hotel	k1gInSc1	hotel
Nacional	Nacional	k1gFnSc2	Nacional
<g/>
"	"	kIx"	"
založena	založen	k2eAgFnSc1d1	založena
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
letecká	letecký	k2eAgFnSc1d1	letecká
asociace	asociace	k1gFnSc1	asociace
IATA	IATA	kA	IATA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
nábřežních	nábřežní	k2eAgFnPc6d1	nábřežní
komunikacích	komunikace	k1gFnPc6	komunikace
Havany	Havana	k1gFnSc2	Havana
jezdily	jezdit	k5eAaImAgInP	jezdit
závody	závod	k1gInPc1	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingway	k1gInPc4	Hemingway
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
20	[number]	k4	20
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
co	co	k9	co
přišel	přijít	k5eAaPmAgInS	přijít
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
získala	získat	k5eAaPmAgFnS	získat
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
rychle	rychle	k6eAd1	rychle
budovat	budovat	k5eAaImF	budovat
moderní	moderní	k2eAgFnSc2d1	moderní
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zapříčiněná	zapříčiněný	k2eAgFnSc1d1	zapříčiněná
rozpadem	rozpad	k1gInSc7	rozpad
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hospodářského	hospodářský	k2eAgMnSc2d1	hospodářský
partnera	partner	k1gMnSc2	partner
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
stavu	stav	k1gInSc2	stav
všech	všecek	k3xTgFnPc2	všecek
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
nových	nový	k2eAgNnPc2d1	nové
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
historicky	historicky	k6eAd1	historicky
chráněných	chráněný	k2eAgFnPc2d1	chráněná
<g/>
.	.	kIx.	.
</s>
<s>
Havana	Havana	k1gFnSc1	Havana
je	být	k5eAaImIp3nS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
celé	celý	k2eAgFnSc2d1	celá
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
významný	významný	k2eAgInSc4d1	významný
přístav	přístav	k1gInSc4	přístav
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Jose	Jos	k1gMnSc2	Jos
Martího	Martí	k1gMnSc2	Martí
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
sem	sem	k6eAd1	sem
i	i	k9	i
Carretera	Carreter	k1gMnSc4	Carreter
Central	Central	k1gMnSc1	Central
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
kubánská	kubánský	k2eAgFnSc1d1	kubánská
silnice	silnice	k1gFnSc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
i	i	k9	i
železniční	železniční	k2eAgNnSc1d1	železniční
napojení	napojení	k1gNnSc1	napojení
<g/>
,	,	kIx,	,
na	na	k7c4	na
karibské	karibský	k2eAgInPc4d1	karibský
poměry	poměr	k1gInPc4	poměr
však	však	k9	však
poněkud	poněkud	k6eAd1	poněkud
kvalitnější	kvalitní	k2eAgNnSc1d2	kvalitnější
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
předměstské	předměstský	k2eAgFnSc2d1	předměstská
železnice	železnice	k1gFnSc2	železnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
obstarávají	obstarávat	k5eAaImIp3nP	obstarávat
pouze	pouze	k6eAd1	pouze
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
drážní	drážní	k2eAgInPc1d1	drážní
druhy	druh	k1gInPc1	druh
dopravy	doprava	k1gFnSc2	doprava
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
špatné	špatný	k2eAgFnSc3d1	špatná
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
situaci	situace	k1gFnSc3	situace
Kuby	Kuba	k1gFnSc2	Kuba
byly	být	k5eAaImAgFnP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
speciální	speciální	k2eAgInPc1d1	speciální
návěsové	návěsový	k2eAgInPc1d1	návěsový
autobusy	autobus	k1gInPc1	autobus
(	(	kIx(	(
<g/>
Camello	Camello	k1gNnSc1	Camello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
pomalu	pomalu	k6eAd1	pomalu
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
modernějšími	moderní	k2eAgFnPc7d2	modernější
vozidly	vozidlo	k1gNnPc7	vozidlo
postavenými	postavený	k2eAgFnPc7d1	postavená
podle	podle	k7c2	podle
konstrukce	konstrukce	k1gFnSc2	konstrukce
vozů	vůz	k1gInPc2	vůz
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
nákup	nákup	k1gInSc1	nákup
nových	nový	k2eAgInPc2d1	nový
autobusů	autobus	k1gInPc2	autobus
z	z	k7c2	z
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
