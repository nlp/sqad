<p>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
(	(	kIx(	(
<g/>
též	též	k9	též
Ganymedes	Ganymedes	k1gMnSc1	Ganymedes
nebo	nebo	k8xC	nebo
z	z	k7c2	z
angl.	angl.	k?	angl.
Ganymede	Ganymed	k1gMnSc5	Ganymed
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Titanem	titan	k1gInSc7	titan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Io	Io	k1gFnSc7	Io
<g/>
,	,	kIx,	,
Europou	Europa	k1gFnSc7	Europa
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
jen	jen	k9	jen
polovičku	polovička	k1gFnSc4	polovička
její	její	k3xOp3gFnSc2	její
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
nejhmotnějším	hmotný	k2eAgInSc7d3	hmotný
měsícem	měsíc	k1gInSc7	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
2,01	[number]	k4	2,01
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgInSc4d2	hmotnější
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
5	[number]	k4	5
262	[number]	k4	262
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
1,07	[number]	k4	1,07
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
7,15	[number]	k4	7,15
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
Ganymed	Ganymed	k1gMnSc1	Ganymed
obíhal	obíhat	k5eAaImAgMnS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
místo	místo	k6eAd1	místo
okolo	okolo	k7c2	okolo
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
měsíci	měsíc	k1gInPc7	měsíc
Europa	Europ	k1gMnSc2	Europ
a	a	k8xC	a
Io	Io	k1gMnSc2	Io
je	být	k5eAaImIp3nS	být
Ganymed	Ganymed	k1gMnSc1	Ganymed
v	v	k7c6	v
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
vůči	vůči	k7c3	vůči
Jupiteru	Jupiter	k1gMnSc3	Jupiter
má	mít	k5eAaImIp3nS	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ganymedův	Ganymedův	k2eAgInSc1d1	Ganymedův
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
silikátovými	silikátový	k2eAgFnPc7d1	silikátová
horninami	hornina	k1gFnPc7	hornina
a	a	k8xC	a
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
planet	planeta	k1gFnPc2	planeta
plně	plně	k6eAd1	plně
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tekuté	tekutý	k2eAgNnSc4d1	tekuté
jádro	jádro	k1gNnSc4	jádro
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
obsahem	obsah	k1gInSc7	obsah
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
km	km	kA	km
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Ganymedu	Ganymed	k1gMnSc6	Ganymed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oceán	oceán	k1gInSc1	oceán
tvořený	tvořený	k2eAgInSc1d1	tvořený
slanou	slaný	k2eAgFnSc7d1	slaná
tekutou	tekutý	k2eAgFnSc7d1	tekutá
vodou	voda	k1gFnSc7	voda
mezi	mezi	k7c7	mezi
vrstvami	vrstva	k1gFnPc7	vrstva
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgInPc7	dva
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
typy	typ	k1gInPc7	typ
<g/>
:	:	kIx,	:
tmavými	tmavý	k2eAgFnPc7d1	tmavá
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
posetými	posetý	k2eAgInPc7d1	posetý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
okolo	okolo	k7c2	okolo
4	[number]	k4	4
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
mladšími	mladý	k2eAgFnPc7d2	mladší
<g/>
,	,	kIx,	,
světlejšími	světlý	k2eAgFnPc7d2	světlejší
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
křížem	křížem	k6eAd1	křížem
krážem	krážem	k6eAd1	krážem
protkané	protkaný	k2eAgFnPc1d1	protkaná
prasklinami	prasklina	k1gFnPc7	prasklina
a	a	k8xC	a
trhlinami	trhlina	k1gFnPc7	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
světlejších	světlý	k2eAgFnPc2d2	světlejší
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
četnost	četnost	k1gFnSc4	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
řídká	řídký	k2eAgFnSc1d1	řídká
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
těchto	tento	k3xDgFnPc2	tento
světlejších	světlý	k2eAgFnPc2d2	světlejší
oblastí	oblast	k1gFnPc2	oblast
nebyl	být	k5eNaImAgInS	být
zatím	zatím	k6eAd1	zatím
přesně	přesně	k6eAd1	přesně
geologicky	geologicky	k6eAd1	geologicky
vysvětlen	vysvětlen	k2eAgMnSc1d1	vysvětlen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
tektonickými	tektonický	k2eAgInPc7d1	tektonický
procesy	proces	k1gInPc7	proces
způsobovanými	způsobovaný	k2eAgInPc7d1	způsobovaný
slapovým	slapový	k2eAgNnSc7d1	slapové
zahříváním	zahřívání	k1gNnSc7	zahřívání
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
známý	známý	k2eAgInSc4d1	známý
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
byla	být	k5eAaImAgFnS	být
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořená	tvořený	k2eAgFnSc1d1	tvořená
konvekcí	konvekce	k1gFnSc7	konvekce
probíhající	probíhající	k2eAgFnSc6d1	probíhající
uvnitř	uvnitř	k7c2	uvnitř
tekutého	tekutý	k2eAgNnSc2d1	tekuté
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
překryta	překrýt	k5eAaPmNgFnS	překrýt
silným	silný	k2eAgNnSc7d1	silné
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k8xC	i
spojena	spojit	k5eAaPmNgFnS	spojit
pomocí	pomoc	k1gFnSc7	pomoc
otevřených	otevřený	k2eAgFnPc2d1	otevřená
siločar	siločára	k1gFnPc2	siločára
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
denně	denně	k6eAd1	denně
obdrží	obdržet	k5eAaPmIp3nS	obdržet
dávku	dávka	k1gFnSc4	dávka
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
okolo	okolo	k7c2	okolo
8	[number]	k4	8
Remů	Remus	k1gMnPc2	Remus
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
slabou	slabý	k2eAgFnSc4d1	slabá
kyslíkovou	kyslíkový	k2eAgFnSc4d1	kyslíková
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
molekulami	molekula	k1gFnPc7	molekula
O	O	kA	O
<g/>
,	,	kIx,	,
O2	O2	k1gFnSc2	O2
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Atomární	atomární	k2eAgInSc1d1	atomární
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
jen	jen	k9	jen
menšinová	menšinový	k2eAgFnSc1d1	menšinová
složka	složka	k1gFnSc1	složka
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
ionosféra	ionosféra	k1gFnSc1	ionosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
objevil	objevit	k5eAaPmAgMnS	objevit
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc6	Galilei
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
pozorování	pozorování	k1gNnSc2	pozorování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měsíc	měsíc	k1gInSc4	měsíc
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jiný	jiný	k2eAgMnSc1d1	jiný
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
dle	dle	k7c2	dle
postavy	postava	k1gFnSc2	postava
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
Ganyméda	Ganyméd	k1gMnSc2	Ganyméd
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
milencem	milenec	k1gMnSc7	milenec
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
číšníkem	číšník	k1gMnSc7	číšník
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
měsíc	měsíc	k1gInSc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
sondami	sonda	k1gFnPc7	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
upřesnily	upřesnit	k5eAaPmAgFnP	upřesnit
jeho	jeho	k3xOp3gFnSc4	jeho
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
mise	mise	k1gFnSc1	mise
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
objevila	objevit	k5eAaPmAgFnS	objevit
podzemní	podzemní	k2eAgInSc4d1	podzemní
oceán	oceán	k1gInSc4	oceán
a	a	k8xC	a
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2020	[number]	k4	2020
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
vyslání	vyslání	k1gNnSc2	vyslání
sondy	sonda	k1gFnSc2	sonda
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Systo	k1gNnSc7	Systo
Mission	Mission	k1gInSc1	Mission
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zkoumat	zkoumat	k5eAaImF	zkoumat
magnetická	magnetický	k2eAgNnPc4d1	magnetické
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
podpovrchové	podpovrchový	k2eAgInPc4d1	podpovrchový
oceány	oceán	k1gInPc4	oceán
Ganymedu	Ganymed	k1gMnSc6	Ganymed
a	a	k8xC	a
Europy	Europ	k1gInPc4	Europ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
původ	původ	k1gInSc1	původ
měsíce	měsíc	k1gInSc2	měsíc
==	==	k?	==
</s>
</p>
<p>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
v	v	k7c6	v
akrečním	akreční	k2eAgInSc6d1	akreční
disku	disk	k1gInSc6	disk
obklopujícím	obklopující	k2eAgNnSc7d1	obklopující
Jupiter	Jupiter	k1gMnSc1	Jupiter
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
akrece	akrece	k1gFnSc1	akrece
Ganymedu	Ganymed	k1gMnSc3	Ganymed
trvala	trvat	k5eAaImAgFnS	trvat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
potřebných	potřebný	k2eAgMnPc2d1	potřebný
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Callista	Callista	k1gMnSc1	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mlhovina	mlhovina	k1gFnSc1	mlhovina
obklopující	obklopující	k2eAgFnSc1d1	obklopující
Jupiter	Jupiter	k1gInSc4	Jupiter
byla	být	k5eAaImAgFnS	být
chudá	chudý	k2eAgFnSc1d1	chudá
na	na	k7c4	na
plyny	plyn	k1gInPc4	plyn
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
Galileových	Galileův	k2eAgMnPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
vysvětlovalo	vysvětlovat	k5eAaImAgNnS	vysvětlovat
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
akrece	akrece	k1gFnSc2	akrece
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Ganymed	Ganymed	k1gMnSc1	Ganymed
vznikal	vznikat	k5eAaImAgMnS	vznikat
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
mlhovina	mlhovina	k1gFnSc1	mlhovina
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
,	,	kIx,	,
vysvětlovalo	vysvětlovat	k5eAaImAgNnS	vysvětlovat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
kratší	krátký	k2eAgNnSc1d2	kratší
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
právě	právě	k6eAd1	právě
s	s	k7c7	s
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
relativně	relativně	k6eAd1	relativně
rychlá	rychlý	k2eAgFnSc1d1	rychlá
formace	formace	k1gFnSc1	formace
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplo	teplo	k1gNnSc1	teplo
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
akrecí	akrece	k1gFnSc7	akrece
nestihlo	stihnout	k5eNaPmAgNnS	stihnout
vyzářit	vyzářit	k5eAaPmF	vyzářit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
diferenciaci	diferenciace	k1gFnSc3	diferenciace
oddělující	oddělující	k2eAgFnSc3d1	oddělující
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
horniny	hornina	k1gFnSc2	hornina
a	a	k8xC	a
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
se	se	k3xPyFc4	se
usadily	usadit	k5eAaPmAgFnP	usadit
uprostřed	uprostřed	k7c2	uprostřed
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vznik	vznik	k1gInSc4	vznik
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
Ganymed	Ganymed	k1gMnSc1	Ganymed
odlišný	odlišný	k2eAgMnSc1d1	odlišný
od	od	k7c2	od
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
akrece	akrece	k1gFnSc1	akrece
probíhala	probíhat	k5eAaImAgFnS	probíhat
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
akreční	akreční	k2eAgNnSc1d1	akreční
teplo	teplo	k1gNnSc1	teplo
bylo	být	k5eAaImAgNnS	být
vyzářeno	vyzářit	k5eAaPmNgNnS	vyzářit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
nedošlo	dojít	k5eNaPmAgNnS	dojít
u	u	k7c2	u
něho	on	k3xPp3gInSc2	on
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
diferenciaci	diferenciace	k1gFnSc4	diferenciace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
velké	velký	k2eAgInPc4d1	velký
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
oba	dva	k4xCgMnPc4	dva
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
poblíž	poblíž	k6eAd1	poblíž
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zformování	zformování	k1gNnSc6	zformování
si	se	k3xPyFc3	se
Ganymed	Ganymed	k1gMnSc1	Ganymed
podržel	podržet	k5eAaPmAgMnS	podržet
teplo	teplo	k1gNnSc4	teplo
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
akrecí	akrece	k1gFnPc2	akrece
a	a	k8xC	a
diferenciací	diferenciace	k1gFnPc2	diferenciace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
uvolňoval	uvolňovat	k5eAaImAgMnS	uvolňovat
do	do	k7c2	do
ledového	ledový	k2eAgInSc2d1	ledový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k6eAd1	teplo
se	se	k3xPyFc4	se
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
šířilo	šířit	k5eAaImAgNnS	šířit
konvekcí	konvekce	k1gFnSc7	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
do	do	k7c2	do
tepelné	tepelný	k2eAgFnSc2d1	tepelná
bilance	bilance	k1gFnSc2	bilance
přidalo	přidat	k5eAaPmAgNnS	přidat
teplo	teplo	k1gNnSc1	teplo
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
rozpadem	rozpad	k1gInSc7	rozpad
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
teplotu	teplota	k1gFnSc4	teplota
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
diferenciaci	diferenciace	k1gFnSc3	diferenciace
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
železné	železný	k2eAgNnSc1d1	železné
a	a	k8xC	a
sulfidoželeznaté	sulfidoželeznatý	k2eAgNnSc1d1	sulfidoželeznatý
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
plášť	plášť	k1gInSc1	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
diferenciovaným	diferenciovaný	k2eAgNnSc7d1	diferenciované
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
rozpad	rozpad	k1gInSc4	rozpad
a	a	k8xC	a
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
teplo	teplo	k1gNnSc4	teplo
uvnitř	uvnitř	k7c2	uvnitř
Callisto	Callista	k1gMnSc5	Callista
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
konvekční	konvekční	k2eAgInPc4d1	konvekční
proudy	proud	k1gInPc4	proud
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
ledové	ledový	k2eAgFnSc6d1	ledová
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
chladným	chladný	k2eAgNnSc7d1	chladné
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
efektivně	efektivně	k6eAd1	efektivně
chladly	chladnout	k5eAaImAgFnP	chladnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
tavení	tavení	k1gNnSc3	tavení
ledu	led	k1gInSc2	led
v	v	k7c6	v
globálním	globální	k2eAgNnSc6d1	globální
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
tedy	tedy	k9	tedy
k	k	k7c3	k
vážnější	vážní	k2eAgFnSc3d2	vážnější
diferenciaci	diferenciace	k1gFnSc3	diferenciace
<g/>
.	.	kIx.	.
</s>
<s>
Konvektivní	Konvektivní	k2eAgInPc1d1	Konvektivní
pohyby	pohyb	k1gInPc1	pohyb
na	na	k7c4	na
Callisto	Callista	k1gMnSc5	Callista
vedly	vést	k5eAaImAgFnP	vést
jen	jen	k9	jen
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
led	led	k1gInSc1	led
a	a	k8xC	a
horniny	hornina	k1gFnPc1	hornina
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
jen	jen	k6eAd1	jen
místně	místně	k6eAd1	místně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ganymed	Ganymed	k1gMnSc1	Ganymed
chladne	chladnout	k5eAaImIp3nS	chladnout
jen	jen	k9	jen
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
z	z	k7c2	z
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
pláště	plášť	k1gInSc2	plášť
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
existenci	existence	k1gFnSc4	existence
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
pomalé	pomalý	k2eAgNnSc1d1	pomalé
chlazení	chlazení	k1gNnSc1	chlazení
tekutého	tekutý	k2eAgMnSc2d1	tekutý
Fe-FeS	Fe-FeS	k1gMnSc2	Fe-FeS
jádra	jádro	k1gNnSc2	jádro
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
konvekci	konvekce	k1gFnSc4	konvekce
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc1	vznik
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
další	další	k2eAgFnSc1d1	další
teorie	teorie	k1gFnSc2	teorie
vysvětlující	vysvětlující	k2eAgInPc1d1	vysvětlující
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
Callisto	Callista	k1gMnSc5	Callista
a	a	k8xC	a
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
četnosti	četnost	k1gFnPc4	četnost
dopadů	dopad	k1gInPc2	dopad
těles	těleso	k1gNnPc2	těleso
na	na	k7c4	na
povrchy	povrch	k1gInPc4	povrch
měsíců	měsíc	k1gInPc2	měsíc
způsobených	způsobený	k2eAgFnPc2d1	způsobená
gravitací	gravitace	k1gFnPc2	gravitace
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Ganymed	Ganymed	k1gMnSc1	Ganymed
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
než	než	k8xS	než
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
častěji	často	k6eAd2	často
vystaven	vystavit	k5eAaPmNgInS	vystavit
impaktům	impakt	k1gInPc3	impakt
cizích	cizí	k2eAgNnPc2d1	cizí
těles	těleso	k1gNnPc2	těleso
o	o	k7c6	o
vyšších	vysoký	k2eAgFnPc6d2	vyšší
rychlostech	rychlost	k1gFnPc6	rychlost
v	v	k7c6	v
období	období	k1gNnSc6	období
velkého	velký	k2eAgNnSc2d1	velké
bombardování	bombardování	k1gNnSc2	bombardování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
mělo	mít	k5eAaImAgNnS	mít
způsobit	způsobit	k5eAaPmF	způsobit
roztavení	roztavení	k1gNnSc1	roztavení
povrchu	povrch	k1gInSc2	povrch
Ganymedu	Ganymed	k1gMnSc3	Ganymed
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
dostalo	dostat	k5eAaPmAgNnS	dostat
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nemohlo	moct	k5eNaImAgNnS	moct
rychle	rychle	k6eAd1	rychle
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Stavba	stavba	k1gFnSc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
Ganymedu	Ganymed	k1gMnSc6	Ganymed
je	být	k5eAaImIp3nS	být
1,936	[number]	k4	1,936
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
zastoupení	zastoupení	k1gNnSc1	zastoupení
přibližně	přibližně	k6eAd1	přibližně
stejného	stejný	k2eAgInSc2d1	stejný
dílu	díl	k1gInSc2	díl
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
<g/>
Hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
zlomek	zlomek	k1gInSc1	zlomek
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
46	[number]	k4	46
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nepatrně	nepatrně	k6eAd1	nepatrně
méně	málo	k6eAd2	málo
než	než	k8xS	než
u	u	k7c2	u
Callista	Callista	k1gMnSc1	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
ledu	led	k1gInSc6	led
budou	být	k5eAaImBp3nP	být
nacházet	nacházet	k5eAaImF	nacházet
i	i	k9	i
další	další	k2eAgFnPc4d1	další
příměsi	příměs	k1gFnPc4	příměs
jako	jako	k8xS	jako
čpavek	čpavek	k1gInSc4	čpavek
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
složení	složení	k1gNnSc4	složení
horninového	horninový	k2eAgInSc2d1	horninový
pláště	plášť	k1gInSc2	plášť
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
chondritů	chondrit	k1gInPc2	chondrit
typu	typ	k1gInSc2	typ
L	L	kA	L
či	či	k8xC	či
LL	LL	kA	LL
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
chondritů	chondrit	k1gInPc2	chondrit
typu	typ	k1gInSc2	typ
H	H	kA	H
liší	lišit	k5eAaImIp3nP	lišit
především	především	k6eAd1	především
menším	malý	k2eAgNnSc7d2	menší
zastoupením	zastoupení	k1gNnSc7	zastoupení
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
vyskytujícím	vyskytující	k2eAgMnSc7d1	vyskytující
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
jen	jen	k9	jen
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
železa	železo	k1gNnSc2	železo
metalického	metalický	k2eAgNnSc2d1	metalické
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
poměr	poměr	k1gInSc1	poměr
železa	železo	k1gNnSc2	železo
vůči	vůči	k7c3	vůči
křemičitanům	křemičitan	k1gInPc3	křemičitan
je	být	k5eAaImIp3nS	být
1,05	[number]	k4	1,05
až	až	k9	až
1,27	[number]	k4	1,27
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
1,8	[number]	k4	1,8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Albedo	Albedo	k1gNnSc1	Albedo
Ganymedu	Ganymed	k1gMnSc6	Ganymed
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
43	[number]	k4	43
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
přítomný	přítomný	k2eAgInSc1d1	přítomný
všude	všude	k6eAd1	všude
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
s	s	k7c7	s
hmotnostním	hmotnostní	k2eAgNnSc7d1	hmotnostní
zastoupením	zastoupení	k1gNnSc7	zastoupení
50	[number]	k4	50
až	až	k8xS	až
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
zastoupení	zastoupení	k1gNnSc4	zastoupení
ledu	led	k1gInSc2	led
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgNnSc2d1	celé
tělesa	těleso	k1gNnSc2	těleso
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
silných	silný	k2eAgFnPc2d1	silná
absorpčních	absorpční	k2eAgFnPc2d1	absorpční
čar	čára	k1gFnPc2	čára
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1,04	[number]	k4	1,04
<g/>
,	,	kIx,	,
1,25	[number]	k4	1,25
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
<g/>
,	,	kIx,	,
2,0	[number]	k4	2,0
a	a	k8xC	a
3,0	[number]	k4	3,0
mikrometru	mikrometr	k1gInSc2	mikrometr
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
vodnímu	vodní	k2eAgInSc3d1	vodní
ledu	led	k1gInSc3	led
<g/>
.	.	kIx.	.
</s>
<s>
Popraskaný	popraskaný	k2eAgInSc4d1	popraskaný
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
jasnější	jasný	k2eAgFnSc1d2	jasnější
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
ledu	led	k1gInSc2	led
než	než	k8xS	než
tmavší	tmavý	k2eAgFnSc2d2	tmavší
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
snímků	snímek	k1gInPc2	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
,	,	kIx,	,
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
pořízených	pořízený	k2eAgInPc2d1	pořízený
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pozemních	pozemní	k2eAgNnPc2d1	pozemní
pozorování	pozorování	k1gNnPc2	pozorování
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
přítomnost	přítomnost	k1gFnSc1	přítomnost
i	i	k9	i
jiných	jiný	k2eAgFnPc2d1	jiná
sloučenin	sloučenina	k1gFnPc2	sloučenina
než	než	k8xS	než
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
dikyanu	dikyan	k1gInSc2	dikyan
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
taktéž	taktéž	k?	taktéž
objevil	objevit	k5eAaPmAgInS	objevit
síran	síran	k1gInSc4	síran
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
(	(	kIx(	(
<g/>
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
síran	síran	k1gInSc1	síran
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Objevené	objevený	k2eAgFnPc1d1	objevená
soli	sůl	k1gFnPc1	sůl
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
<g/>
Povrch	povrch	k6eAd1wR	povrch
Ganymedu	Ganymed	k1gMnSc3	Ganymed
je	být	k5eAaImIp3nS	být
asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
<g/>
,	,	kIx,	,
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
polokoule	polokoule	k1gFnSc1	polokoule
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
oběhu	oběh	k1gInSc2	oběh
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
než	než	k8xS	než
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opačné	opačný	k2eAgInPc1d1	opačný
než	než	k8xS	než
u	u	k7c2	u
Callista	Callista	k1gMnSc1	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
obohacena	obohatit	k5eAaPmNgFnS	obohatit
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
rozložení	rozložení	k1gNnSc3	rozložení
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
symetrické	symetrický	k2eAgNnSc1d1	symetrické
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
oblastí	oblast	k1gFnPc2	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyl	být	k5eNaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Impaktní	Impaktní	k2eAgInPc1d1	Impaktní
krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
jednoho	jeden	k4xCgInSc2	jeden
<g/>
)	)	kIx)	)
neukazují	ukazovat	k5eNaImIp3nP	ukazovat
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
obohacení	obohacení	k1gNnSc2	obohacení
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
z	z	k7c2	z
Callista	Callista	k1gMnSc1	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ganymed	Ganymed	k1gMnSc1	Ganymed
své	svůj	k3xOyFgFnPc4	svůj
zásoby	zásoba	k1gFnPc4	zásoba
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
vyčerpal	vyčerpat	k5eAaPmAgInS	vyčerpat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ganymed	Ganymed	k1gMnSc1	Ganymed
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
diferencovaný	diferencovaný	k2eAgInSc1d1	diferencovaný
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sulfidy	sulfid	k1gInPc4	sulfid
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
vnějšího	vnější	k2eAgInSc2d1	vnější
ledového	ledový	k2eAgInSc2d1	ledový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
podporován	podporovat	k5eAaImNgInS	podporovat
nízkou	nízký	k2eAgFnSc7d1	nízká
hodnotou	hodnota	k1gFnSc7	hodnota
bezrozměrného	bezrozměrný	k2eAgInSc2d1	bezrozměrný
momentu	moment	k1gInSc2	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
–	–	k?	–
0,3105	[number]	k4	0,3105
±	±	k?	±
0,0028	[number]	k4	0,0028
–	–	k?	–
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
změřena	změřit	k5eAaPmNgFnS	změřit
během	během	k7c2	během
přeletů	přelet	k1gInPc2	přelet
sondy	sonda	k1gFnPc4	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
má	mít	k5eAaImIp3nS	mít
Ganymed	Ganymed	k1gMnSc1	Ganymed
nejnižší	nízký	k2eAgInSc4d3	nejnižší
moment	moment	k1gInSc4	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
pevných	pevný	k2eAgNnPc2d1	pevné
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnPc4	existence
tekutého	tekutý	k2eAgNnSc2d1	tekuté
<g/>
,	,	kIx,	,
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
bohatého	bohatý	k2eAgNnSc2d1	bohaté
jádra	jádro	k1gNnSc2	jádro
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
existenci	existence	k1gFnSc4	existence
vlastního	vlastní	k2eAgNnSc2d1	vlastní
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Ganymedu	Ganymed	k1gMnSc3	Ganymed
naměřeného	naměřený	k2eAgInSc2d1	naměřený
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Konvekce	konvekce	k1gFnSc1	konvekce
tekutého	tekutý	k2eAgNnSc2d1	tekuté
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
elektricky	elektricky	k6eAd1	elektricky
vodivé	vodivý	k2eAgNnSc1d1	vodivé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejpřijímanější	přijímaný	k2eAgInSc1d3	přijímaný
model	model	k1gInSc1	model
vysvětlující	vysvětlující	k2eAgInSc4d1	vysvětlující
vznik	vznik	k1gInSc4	vznik
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Určení	určení	k1gNnSc1	určení
přesné	přesný	k2eAgFnSc2d1	přesná
tloušťky	tloušťka	k1gFnSc2	tloušťka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
uvnitř	uvnitř	k6eAd1	uvnitř
Ganymedu	Ganymed	k1gMnSc3	Ganymed
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
poměru	poměr	k1gInSc6	poměr
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
silikátech	silikát	k1gInPc6	silikát
(	(	kIx(	(
<g/>
zastoupení	zastoupení	k1gNnSc4	zastoupení
olivínu	olivín	k1gInSc2	olivín
a	a	k8xC	a
pyroxenu	pyroxen	k1gInSc2	pyroxen
<g/>
)	)	kIx)	)
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
poloměr	poloměr	k1gInSc4	poloměr
700	[number]	k4	700
až	až	k9	až
900	[number]	k4	900
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
800	[number]	k4	800
až	až	k9	až
1000	[number]	k4	1000
kilometrů	kilometr	k1gInPc2	kilometr
mocný	mocný	k2eAgMnSc1d1	mocný
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vnější	vnější	k2eAgInSc1d1	vnější
ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
silikátový	silikátový	k2eAgInSc4d1	silikátový
plášť	plášť	k1gInSc4	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
5,5	[number]	k4	5,5
až	až	k9	až
6	[number]	k4	6
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
silikátový	silikátový	k2eAgInSc1d1	silikátový
plášť	plášť	k1gInSc1	plášť
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
3,4	[number]	k4	3,4
až	až	k9	až
3,6	[number]	k4	3,6
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
modely	model	k1gInPc1	model
vysvětlující	vysvětlující	k2eAgInSc1d1	vysvětlující
vznik	vznik	k1gInSc4	vznik
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
požadují	požadovat	k5eAaImIp3nP	požadovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
kapalného	kapalný	k2eAgNnSc2d1	kapalné
jádra	jádro	k1gNnSc2	jádro
tvořeného	tvořený	k2eAgInSc2d1	tvořený
čistým	čistý	k2eAgNnSc7d1	čisté
železem	železo	k1gNnSc7	železo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
poměrem	poměr	k1gInSc7	poměr
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
takovéhoto	takovýto	k3xDgNnSc2	takovýto
jádra	jádro	k1gNnSc2	jádro
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgInS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
500	[number]	k4	500
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
Ganymedu	Ganymed	k1gMnSc3	Ganymed
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mezi	mezi	k7c7	mezi
1500	[number]	k4	1500
až	až	k9	až
1700	[number]	k4	1700
K	K	kA	K
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přes	přes	k7c4	přes
100	[number]	k4	100
barů	bar	k1gInPc2	bar
(	(	kIx(	(
<g/>
10	[number]	k4	10
Gpa	Gpa	k1gFnPc2	Gpa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povrch	povrch	k1gInSc1	povrch
==	==	k?	==
</s>
</p>
<p>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
měl	mít	k5eAaImAgMnS	mít
složitou	složitý	k2eAgFnSc4d1	složitá
geologickou	geologický	k2eAgFnSc4d1	geologická
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnPc4	údolí
<g/>
,	,	kIx,	,
krátery	kráter	k1gInPc1	kráter
a	a	k8xC	a
toky	tok	k1gInPc1	tok
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
světlými	světlý	k2eAgFnPc7d1	světlá
a	a	k8xC	a
tmavými	tmavý	k2eAgFnPc7d1	tmavá
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
liší	lišit	k5eAaImIp3nP	lišit
stářím	stáří	k1gNnSc7	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
hustě	hustě	k6eAd1	hustě
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
velice	velice	k6eAd1	velice
dávno	dávno	k6eAd1	dávno
<g/>
.	.	kIx.	.
</s>
<s>
Zabírají	zabírat	k5eAaImIp3nP	zabírat
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
světlé	světlý	k2eAgFnPc1d1	světlá
oblasti	oblast	k1gFnPc1	oblast
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
nižší	nízký	k2eAgFnSc4d2	nižší
četnost	četnost	k1gFnSc4	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
jsou	být	k5eAaImIp3nP	být
protkány	protkat	k5eAaPmNgFnP	protkat
množstvím	množství	k1gNnSc7	množství
trhlin	trhlina	k1gFnPc2	trhlina
a	a	k8xC	a
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
nejspíše	nejspíše	k9	nejspíše
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jíly	jíl	k1gInPc4	jíl
a	a	k8xC	a
organické	organický	k2eAgInPc4d1	organický
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
napovědět	napovědět	k5eAaPmF	napovědět
více	hodně	k6eAd2	hodně
o	o	k7c6	o
tělesech	těleso	k1gNnPc6	těleso
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
měsíc	měsíc	k1gInSc1	měsíc
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
akrece	akrece	k1gFnSc2	akrece
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
jsou	být	k5eAaImIp3nP	být
vybírána	vybírán	k2eAgNnPc4d1	vybíráno
jména	jméno	k1gNnPc4	jméno
vesměs	vesměs	k6eAd1	vesměs
z	z	k7c2	z
mytologií	mytologie	k1gFnPc2	mytologie
kultur	kultura	k1gFnPc2	kultura
úrodného	úrodný	k2eAgInSc2d1	úrodný
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
od	od	k7c2	od
Egypta	Egypt	k1gInSc2	Egypt
po	po	k7c6	po
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
krátery	kráter	k1gInPc1	kráter
mají	mít	k5eAaImIp3nP	mít
jména	jméno	k1gNnPc4	jméno
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
<g/>
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
měsíce	měsíc	k1gInPc1	měsíc
přijmou	přijmout	k5eAaPmIp3nP	přijmout
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
množství	množství	k1gNnSc1	množství
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nemá	mít	k5eNaImIp3nS	mít
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
teplo	teplo	k6eAd1	teplo
zachycovala	zachycovat	k5eAaImAgFnS	zachycovat
<g/>
.	.	kIx.	.
</s>
<s>
Ganymedův	Ganymedův	k2eAgInSc1d1	Ganymedův
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
7	[number]	k4	7
pozemských	pozemský	k2eAgInPc2d1	pozemský
dní	den	k1gInPc2	den
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
k	k	k7c3	k
vykonání	vykonání	k1gNnSc3	vykonání
oběhu	oběh	k1gInSc2	oběh
okolo	okolo	k7c2	okolo
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
teploty	teplota	k1gFnPc1	teplota
od	od	k7c2	od
70	[number]	k4	70
K	K	kA	K
do	do	k7c2	do
152	[number]	k4	152
K.	K.	kA	K.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
===	===	k?	===
</s>
</p>
<p>
<s>
Tepelný	tepelný	k2eAgInSc1d1	tepelný
mechanismus	mechanismus	k1gInSc1	mechanismus
potřebný	potřebný	k2eAgInSc1d1	potřebný
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
rozpraskaného	rozpraskaný	k2eAgInSc2d1	rozpraskaný
terénu	terén	k1gInSc2	terén
povrchu	povrch	k1gInSc2	povrch
Ganymedu	Ganymed	k1gMnSc6	Ganymed
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
nezodpovězená	zodpovězený	k2eNgFnSc1d1	nezodpovězená
otázka	otázka	k1gFnSc1	otázka
planetologie	planetologie	k1gFnSc2	planetologie
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
názor	názor	k1gInSc1	názor
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
projev	projev	k1gInSc1	projev
přírodních	přírodní	k2eAgInPc2d1	přírodní
tektonických	tektonický	k2eAgInPc2d1	tektonický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
hrál	hrát	k5eAaImAgInS	hrát
kryovulkanismus	kryovulkanismus	k1gInSc1	kryovulkanismus
jen	jen	k9	jen
minimální	minimální	k2eAgMnSc1d1	minimální
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
nějakou	nějaký	k3yIgFnSc4	nějaký
<g/>
)	)	kIx)	)
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
způsobily	způsobit	k5eAaPmAgFnP	způsobit
napětí	napětí	k1gNnSc4	napětí
v	v	k7c6	v
Ganymedově	Ganymedův	k2eAgFnSc6d1	Ganymedův
ledové	ledový	k2eAgFnSc6d1	ledová
litosféře	litosféra	k1gFnSc6	litosféra
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
gravitační	gravitační	k2eAgFnSc2d1	gravitační
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
Jupiterem	Jupiter	k1gInSc7	Jupiter
vedoucí	vedoucí	k1gFnSc2	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prošel	projít	k5eAaPmAgInS	projít
nestabilními	stabilní	k2eNgFnPc7d1	nestabilní
dráhovými	dráhový	k2eAgFnPc7d1	dráhová
rezonancemi	rezonance	k1gFnPc7	rezonance
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
pnutí	pnutí	k1gNnSc1	pnutí
na	na	k7c4	na
led	led	k1gInSc4	led
mohlo	moct	k5eAaImAgNnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
zahřátí	zahřátí	k1gNnSc1	zahřátí
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
napnout	napnout	k5eAaPmF	napnout
litosféru	litosféra	k1gFnSc4	litosféra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
popraskání	popraskání	k1gNnSc3	popraskání
a	a	k8xC	a
sérii	série	k1gFnSc3	série
výzdvihů	výzdvih	k1gInPc2	výzdvih
a	a	k8xC	a
poklesů	pokles	k1gInPc2	pokles
částí	část	k1gFnPc2	část
litosféry	litosféra	k1gFnSc2	litosféra
a	a	k8xC	a
přetvoření	přetvoření	k1gNnSc2	přetvoření
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
starého	starý	k2eAgInSc2d1	starý
tmavého	tmavý	k2eAgInSc2d1	tmavý
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Popraskaný	popraskaný	k2eAgInSc4d1	popraskaný
povrch	povrch	k1gInSc4	povrch
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
taktéž	taktéž	k?	taktéž
procesy	proces	k1gInPc7	proces
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
formováním	formování	k1gNnSc7	formování
jádra	jádro	k1gNnSc2	jádro
částečně	částečně	k6eAd1	částečně
ohřívaného	ohřívaný	k2eAgMnSc2d1	ohřívaný
slapovými	slapový	k2eAgInPc7d1	slapový
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
mírné	mírný	k2eAgNnSc1d1	mírné
zvětšení	zvětšení	k1gNnSc1	zvětšení
Ganymedu	Ganymed	k1gMnSc3	Ganymed
o	o	k7c4	o
1	[number]	k4	1
až	až	k9	až
6	[number]	k4	6
%	%	kIx~	%
vlivem	vlivem	k7c2	vlivem
fázových	fázový	k2eAgFnPc2d1	fázová
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
ledu	led	k1gInSc6	led
a	a	k8xC	a
teplotní	teplotní	k2eAgFnPc1d1	teplotní
roztažnosti	roztažnost	k1gFnPc1	roztažnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následného	následný	k2eAgInSc2d1	následný
vývoje	vývoj	k1gInSc2	vývoj
by	by	kYmCp3nS	by
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
stoupala	stoupat	k5eAaImAgFnS	stoupat
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
plumy	pluma	k1gFnSc2	pluma
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
nárůst	nárůst	k1gInSc4	nárůst
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
tektonické	tektonický	k2eAgFnSc2d1	tektonická
deformace	deformace	k1gFnSc2	deformace
litosféry	litosféra	k1gFnSc2	litosféra
<g/>
.	.	kIx.	.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
rozpady	rozpad	k1gInPc1	rozpad
minerálů	minerál	k1gInPc2	minerál
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hlavním	hlavní	k2eAgInSc7d1	hlavní
energetickým	energetický	k2eAgInSc7d1	energetický
zdrojem	zdroj	k1gInSc7	zdroj
tepla	teplo	k1gNnSc2	teplo
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
tloušťku	tloušťka	k1gFnSc4	tloušťka
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
výstřednost	výstřednost	k1gFnSc1	výstřednost
dráhy	dráha	k1gFnSc2	dráha
o	o	k7c4	o
řád	řád	k1gInSc4	řád
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
)	)	kIx)	)
teplo	teplo	k6eAd1	teplo
získávané	získávaný	k2eAgInPc4d1	získávaný
slapovými	slapový	k2eAgInPc7d1	slapový
procesy	proces	k1gInPc7	proces
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
z	z	k7c2	z
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
rozpadů	rozpad	k1gInPc2	rozpad
<g/>
.	.	kIx.	.
<g/>
Impaktní	Impaktní	k2eAgInPc4d1	Impaktní
krátery	kráter	k1gInPc4	kráter
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
typech	typ	k1gInPc6	typ
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
četnější	četný	k2eAgFnPc1d2	četnější
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tmavých	tmavý	k2eAgFnPc6d1	tmavá
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
formovány	formován	k2eAgInPc1d1	formován
nárazy	náraz	k1gInPc1	náraz
cizích	cizí	k2eAgNnPc2d1	cizí
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Světlejší	světlý	k2eAgInSc1d2	světlejší
popraskaný	popraskaný	k2eAgInSc1d1	popraskaný
terén	terén	k1gInSc1	terén
je	být	k5eAaImIp3nS	být
krátery	kráter	k1gInPc4	kráter
poset	posít	k5eAaPmNgMnS	posít
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
impakty	impakt	k1gInPc1	impakt
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vývoji	vývoj	k1gInSc6	vývoj
podepsaly	podepsat	k5eAaPmAgInP	podepsat
jen	jen	k6eAd1	jen
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vrchoviny	vrchovina	k1gFnPc1	vrchovina
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
světlé	světlý	k2eAgFnPc1d1	světlá
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
o	o	k7c4	o
kolik	kolik	k4yRc4	kolik
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
mohl	moct	k5eAaImAgMnS	moct
zažít	zažít	k5eAaPmF	zažít
období	období	k1gNnSc4	období
pozdního	pozdní	k2eAgNnSc2d1	pozdní
těžkého	těžký	k2eAgNnSc2d1	těžké
bombardování	bombardování	k1gNnSc2	bombardování
před	před	k7c7	před
3,5	[number]	k4	3,5
až	až	k9	až
5	[number]	k4	5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
by	by	kYmCp3nS	by
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
překrývají	překrývat	k5eAaImIp3nP	překrývat
a	a	k8xC	a
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
i	i	k9	i
systémy	systém	k1gInPc1	systém
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgMnPc1d2	mladší
než	než	k8xS	než
praskliny	prasklina	k1gFnPc1	prasklina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
relativně	relativně	k6eAd1	relativně
mladé	mladý	k2eAgInPc4d1	mladý
krátery	kráter	k1gInPc4	kráter
s	s	k7c7	s
příčně	příčně	k6eAd1	příčně
se	se	k3xPyFc4	se
rozbíhajícími	rozbíhající	k2eAgInPc7d1	rozbíhající
paprsky	paprsek	k1gInPc7	paprsek
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
jsou	být	k5eAaImIp3nP	být
plošší	plochý	k2eAgInPc1d2	plošší
než	než	k8xS	než
krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
ledovou	ledový	k2eAgFnSc7d1	ledová
kůrou	kůra	k1gFnSc7	kůra
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozpustit	rozpustit	k5eAaPmF	rozpustit
a	a	k8xC	a
tak	tak	k6eAd1	tak
krátery	kráter	k1gInPc4	kráter
zarovnávat	zarovnávat	k5eAaImF	zarovnávat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
kráterů	kráter	k1gInPc2	kráter
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gInPc4	jejich
bývalé	bývalý	k2eAgInPc4d1	bývalý
okraje	okraj	k1gInPc4	okraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snadno	snadno	k6eAd1	snadno
rozpoznatelný	rozpoznatelný	k2eAgInSc1d1	rozpoznatelný
útvar	útvar	k1gInSc1	útvar
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
je	být	k5eAaImIp3nS	být
temná	temný	k2eAgFnSc1d1	temná
planina	planina	k1gFnSc1	planina
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Galileo	Galilea	k1gFnSc5	Galilea
Regio	Regia	k1gMnSc5	Regia
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
série	série	k1gFnSc1	série
soustředně	soustředně	k6eAd1	soustředně
se	se	k3xPyFc4	se
sbíhajících	sbíhající	k2eAgFnPc2d1	sbíhající
prasklin	prasklina	k1gFnPc2	prasklina
či	či	k8xC	či
brázd	brázda	k1gFnPc2	brázda
<g/>
,	,	kIx,	,
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
zřejmě	zřejmě	k6eAd1	zřejmě
během	během	k7c2	během
nějakého	nějaký	k3yIgNnSc2	nějaký
období	období	k1gNnSc2	období
geologické	geologický	k2eAgFnSc2d1	geologická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
polární	polární	k2eAgFnSc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořeny	tvořit	k5eAaImNgFnP	tvořit
zmrzlou	zmrzlý	k2eAgFnSc7d1	zmrzlá
vodou	voda	k1gFnSc7	voda
zasahující	zasahující	k2eAgInSc1d1	zasahující
až	až	k6eAd1	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
40	[number]	k4	40
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
i	i	k8xC	i
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Čepičky	čepička	k1gFnPc1	čepička
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
a	a	k8xC	a
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
získaných	získaný	k2eAgInPc2d1	získaný
sondou	sonda	k1gFnSc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
jejich	jejich	k3xOp3gInSc7	jejich
vznikem	vznik	k1gInSc7	vznik
bombardování	bombardování	k1gNnSc2	bombardování
ledu	led	k1gInSc2	led
plazmatem	plazma	k1gNnSc7	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
vlastní	vlastní	k2eAgNnSc1d1	vlastní
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
a	a	k8xC	a
důsledkem	důsledek	k1gInSc7	důsledek
jeho	jeho	k3xOp3gFnSc2	jeho
přítomnosti	přítomnost	k1gFnSc2	přítomnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblasti	oblast	k1gFnPc1	oblast
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
jsou	být	k5eAaImIp3nP	být
nabitými	nabitý	k2eAgFnPc7d1	nabitá
částicemi	částice	k1gFnPc7	částice
pocházejícími	pocházející	k2eAgFnPc7d1	pocházející
z	z	k7c2	z
Jupiteru	Jupiter	k1gInSc2	Jupiter
bombardovány	bombardovat	k5eAaImNgInP	bombardovat
mnohem	mnohem	k6eAd1	mnohem
intenzivněji	intenzivně	k6eAd2	intenzivně
<g/>
.	.	kIx.	.
</s>
<s>
Nárazy	náraz	k1gInPc1	náraz
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
rozprašování	rozprašování	k1gNnSc3	rozprašování
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
teplotními	teplotní	k2eAgInPc7d1	teplotní
vlivy	vliv	k1gInPc7	vliv
oddělí	oddělit	k5eAaPmIp3nS	oddělit
světlý	světlý	k2eAgInSc1d1	světlý
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
od	od	k7c2	od
tmavších	tmavý	k2eAgInPc2d2	tmavší
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
usazovat	usazovat	k5eAaImF	usazovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
důvodem	důvod	k1gInSc7	důvod
vzniku	vznik	k1gInSc2	vznik
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krátery	kráter	k1gInPc4	kráter
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgInPc4d1	světlý
a	a	k8xC	a
tmavé	tmavý	k2eAgInPc4d1	tmavý
pruhy	pruh	k1gInPc4	pruh
===	===	k?	===
</s>
</p>
<p>
<s>
Povrch	povrch	k7c2wR	povrch
měsíce	měsíc	k1gInSc2	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
četné	četný	k2eAgInPc4d1	četný
impaktové	impaktový	k2eAgInPc4d1	impaktový
krátery	kráter	k1gInPc4	kráter
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
systémy	systém	k1gInPc4	systém
jasných	jasný	k2eAgInPc2d1	jasný
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
postrádající	postrádající	k2eAgInPc1d1	postrádající
systémy	systém	k1gInPc1	systém
paprsků	paprsek	k1gInPc2	paprsek
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
starší	starý	k2eAgInPc1d2	starší
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	on	k3xPp3gMnPc4	on
mají	mít	k5eAaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgInPc1d1	světlý
pruhy	pruh	k1gInPc1	pruh
křižují	křižovat	k5eAaImIp3nP	křižovat
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
směrech	směr	k1gInPc6	směr
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
spletitý	spletitý	k2eAgInSc4d1	spletitý
systém	systém	k1gInSc4	systém
střídavých	střídavý	k2eAgInPc2d1	střídavý
přímočarých	přímočarý	k2eAgInPc2d1	přímočarý
světlých	světlý	k2eAgInPc2d1	světlý
a	a	k8xC	a
tmavých	tmavý	k2eAgInPc2d1	tmavý
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
deformace	deformace	k1gFnPc4	deformace
vrstvy	vrstva	k1gFnSc2	vrstva
ledové	ledový	k2eAgFnSc2d1	ledová
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systém	systém	k1gInSc1	systém
souřadnic	souřadnice	k1gFnPc2	souřadnice
===	===	k?	===
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
kráteru	kráter	k1gInSc2	kráter
Anat	Anat	k1gMnSc1	Anat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
určení	určení	k1gNnSc6	určení
souřadného	souřadný	k2eAgInSc2d1	souřadný
systému	systém	k1gInSc2	systém
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
128	[number]	k4	128
<g/>
°	°	k?	°
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
a	a	k8xC	a
ionosféra	ionosféra	k1gFnSc1	ionosféra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
tým	tým	k1gInSc1	tým
astronomů	astronom	k1gMnPc2	astronom
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
USA	USA	kA	USA
pracující	pracující	k1gMnSc1	pracující
na	na	k7c6	na
indonéské	indonéský	k2eAgFnSc6d1	Indonéská
observatoři	observatoř	k1gFnSc6	observatoř
Bosscha	Bossch	k1gMnSc2	Bossch
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
objev	objev	k1gInSc4	objev
slabé	slabý	k2eAgFnSc2d1	slabá
atmosféry	atmosféra	k1gFnSc2	atmosféra
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
během	během	k7c2	během
zákrytu	zákryt	k1gInSc2	zákryt
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
odhadly	odhadnout	k5eAaPmAgFnP	odhadnout
na	na	k7c4	na
1	[number]	k4	1
μ	μ	k?	μ
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
zákryt	zákryt	k1gInSc4	zákryt
hvězdy	hvězda	k1gFnSc2	hvězda
κ	κ	k?	κ
Centauri	Centaur	k1gFnSc2	Centaur
během	během	k7c2	během
jejího	její	k3xOp3gInSc2	její
letu	let	k1gInSc2	let
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
během	během	k7c2	během
zákrytu	zákryt	k1gInSc2	zákryt
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
dalekém	daleký	k2eAgNnSc6d1	daleké
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
spektru	spektrum	k1gNnSc6	spektrum
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
200	[number]	k4	200
nm	nm	k?	nm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zaručilo	zaručit	k5eAaPmAgNnS	zaručit
citlivější	citlivý	k2eAgNnSc1d2	citlivější
měření	měření	k1gNnSc1	měření
než	než	k8xS	než
pozorování	pozorování	k1gNnSc1	pozorování
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
nezjistil	zjistit	k5eNaPmAgInS	zjistit
žádnou	žádný	k3yNgFnSc4	žádný
přítomnost	přítomnost	k1gFnSc4	přítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
částic	částice	k1gFnPc2	částice
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
určil	určit	k5eAaPmAgMnS	určit
na	na	k7c4	na
1,5e+9	[number]	k4	1,5e+9
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
atmosférickému	atmosférický	k2eAgInSc3d1	atmosférický
tlaku	tlak	k1gInSc3	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
méně	málo	k6eAd2	málo
než	než	k8xS	než
2,5e-5	[number]	k4	2,5e-5
μ	μ	k?	μ
<g/>
;	;	kIx,	;
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
pět	pět	k4xCc4	pět
řádů	řád	k1gInPc2	řád
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgNnPc1d2	starší
měření	měření	k1gNnPc1	měření
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
optimistické	optimistický	k2eAgFnPc1d1	optimistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
pozoroval	pozorovat	k5eAaImAgInS	pozorovat
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
slabou	slabý	k2eAgFnSc4d1	slabá
kyslíkovou	kyslíkový	k2eAgFnSc4d1	kyslíková
atmosféru	atmosféra	k1gFnSc4	atmosféra
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgFnSc6d1	podobná
atmosféře	atmosféra	k1gFnSc6	atmosféra
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Teleskop	teleskop	k1gInSc1	teleskop
objevil	objevit	k5eAaPmAgInS	objevit
slabé	slabý	k2eAgNnSc4d1	slabé
světelné	světelný	k2eAgNnSc4d1	světelné
záření	záření	k1gNnSc4	záření
atmosféry	atmosféra	k1gFnSc2	atmosféra
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
tzv.	tzv.	kA	tzv.
airglow	airglow	k?	airglow
<g/>
)	)	kIx)	)
atomů	atom	k1gInPc2	atom
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
dalekém	daleký	k2eAgNnSc6d1	daleké
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
záření	záření	k1gNnSc6	záření
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
130,4	[number]	k4	130,4
nm	nm	k?	nm
a	a	k8xC	a
135,6	[number]	k4	135,6
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Světelné	světelný	k2eAgNnSc1d1	světelné
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
když	když	k8xS	když
molekulární	molekulární	k2eAgInSc1d1	molekulární
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
disociován	disociovat	k5eAaBmNgInS	disociovat
srážkou	srážka	k1gFnSc7	srážka
s	s	k7c7	s
elektronem	elektron	k1gInSc7	elektron
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důkaz	důkaz	k1gInSc4	důkaz
neutrální	neutrální	k2eAgFnSc2d1	neutrální
atmosféry	atmosféra	k1gFnSc2	atmosféra
složené	složený	k2eAgFnPc1d1	složená
primárně	primárně	k6eAd1	primárně
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
částic	částice	k1gFnPc2	částice
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
okolo	okolo	k7c2	okolo
1,2	[number]	k4	1,2
až	až	k9	až
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
e	e	k0	e
<g/>
8	[number]	k4	8
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
odpovídajíc	odpovídat	k5eAaImSgFnS	odpovídat
atmosférickému	atmosférický	k2eAgInSc3d1	atmosférický
tlaku	tlak	k1gInSc3	tlak
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
0,2	[number]	k4	0,2
až	až	k8xS	až
1,2e−	1,2e−	k?	1,2e−
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hodnoty	hodnota	k1gFnPc1	hodnota
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
hornímu	horní	k2eAgInSc3d1	horní
limitu	limit	k1gInSc3	limit
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
naměřily	naměřit	k5eAaBmAgInP	naměřit
sondy	sonda	k1gFnPc4	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
nemusí	muset	k5eNaImIp3nS	muset
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
ale	ale	k9	ale
být	být	k5eAaImF	být
důkazem	důkaz	k1gInSc7	důkaz
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozpadem	rozpad	k1gInSc7	rozpad
vodních	vodní	k2eAgFnPc2d1	vodní
molekul	molekula	k1gFnPc2	molekula
vázaných	vázaný	k2eAgFnPc2d1	vázaná
v	v	k7c6	v
ledu	led	k1gInSc6	led
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
vlivem	vliv	k1gInSc7	vliv
radiace	radiace	k1gFnSc2	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vodík	vodík	k1gInSc1	vodík
lehčí	lehký	k2eAgInSc1d2	lehčí
než	než	k8xS	než
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
snáze	snadno	k6eAd2	snadno
unikne	uniknout	k5eAaPmIp3nS	uniknout
gravitačnímu	gravitační	k2eAgNnSc3d1	gravitační
působení	působení	k1gNnSc3	působení
Ganymedu	Ganymed	k1gMnSc3	Ganymed
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
není	být	k5eNaImIp3nS	být
prostorově	prostorově	k6eAd1	prostorově
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
teleskop	teleskop	k1gInSc1	teleskop
pozoroval	pozorovat	k5eAaImAgInS	pozorovat
dvě	dva	k4xCgFnPc4	dva
zářící	zářící	k2eAgFnPc4d1	zářící
oblasti	oblast	k1gFnPc4	oblast
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
okolo	okolo	k7c2	okolo
50	[number]	k4	50
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
otevřenými	otevřený	k2eAgFnPc7d1	otevřená
a	a	k8xC	a
zavřenými	zavřený	k2eAgFnPc7d1	zavřená
silokřivkami	silokřivka	k1gFnPc7	silokřivka
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Zářící	zářící	k2eAgFnPc1d1	zářící
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
způsobené	způsobený	k2eAgFnPc4d1	způsobená
pohybem	pohyb	k1gInSc7	pohyb
zachyceného	zachycený	k2eAgNnSc2d1	zachycené
plazmatu	plazma	k1gNnSc2	plazma
podél	podél	k7c2	podél
otevřených	otevřený	k2eAgFnPc2d1	otevřená
siločar	siločára	k1gFnPc2	siločára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
neutrální	neutrální	k2eAgFnSc2d1	neutrální
atmosféry	atmosféra	k1gFnSc2	atmosféra
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
existovat	existovat	k5eAaImF	existovat
ionosféra	ionosféra	k1gFnSc1	ionosféra
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
molekuly	molekula	k1gFnPc1	molekula
kyslíku	kyslík	k1gInSc2	kyslík
jsou	být	k5eAaImIp3nP	být
ionizované	ionizovaný	k2eAgInPc1d1	ionizovaný
dopady	dopad	k1gInPc1	dopad
energeticky	energeticky	k6eAd1	energeticky
nabitých	nabitý	k2eAgInPc2d1	nabitý
elektronů	elektron	k1gInPc2	elektron
přicházejících	přicházející	k2eAgInPc2d1	přicházející
z	z	k7c2	z
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
a	a	k8xC	a
sluneční	sluneční	k2eAgInSc1d1	sluneční
extrémně	extrémně	k6eAd1	extrémně
ultrafialovou	ultrafialový	k2eAgFnSc7d1	ultrafialová
radiací	radiace	k1gFnSc7	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existence	existence	k1gFnSc1	existence
ionosféry	ionosféra	k1gFnSc2	ionosféra
Ganymedu	Ganymed	k1gMnSc3	Ganymed
je	být	k5eAaImIp3nS	být
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jeho	jeho	k3xOp3gFnSc2	jeho
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
měření	měření	k1gNnPc1	měření
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
našly	najít	k5eAaPmAgFnP	najít
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
hustotu	hustota	k1gFnSc4	hustota
elektronů	elektron	k1gInPc2	elektron
poblíž	poblíž	k7c2	poblíž
měsíce	měsíc	k1gInSc2	měsíc
naznačující	naznačující	k2eAgFnSc4d1	naznačující
existenci	existence	k1gFnSc4	existence
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
neobjevily	objevit	k5eNaPmAgFnP	objevit
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
elektronů	elektron	k1gInPc2	elektron
poblíž	poblíž	k7c2	poblíž
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
2500	[number]	k4	2500
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
ale	ale	k9	ale
vlastnosti	vlastnost	k1gFnPc1	vlastnost
hypotetické	hypotetický	k2eAgFnPc1d1	hypotetická
ionosféry	ionosféra	k1gFnPc1	ionosféra
nebyly	být	k5eNaImAgFnP	být
detailněji	detailně	k6eAd2	detailně
určeny	určit	k5eAaPmNgFnP	určit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
důkazem	důkaz	k1gInSc7	důkaz
existence	existence	k1gFnSc2	existence
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
atmosféry	atmosféra	k1gFnSc2	atmosféra
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
spektroskopických	spektroskopický	k2eAgNnPc2d1	spektroskopické
měření	měření	k1gNnPc2	měření
plynů	plyn	k1gInPc2	plyn
zachycených	zachycený	k2eAgInPc2d1	zachycený
v	v	k7c6	v
ledu	led	k1gInSc6	led
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
ozónu	ozón	k1gInSc2	ozón
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
spektroskopické	spektroskopický	k2eAgFnPc1d1	spektroskopická
analýzy	analýza	k1gFnPc1	analýza
odhalily	odhalit	k5eAaPmAgFnP	odhalit
dimery	dimer	k1gInPc4	dimer
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
dvouatomový	dvouatomový	k2eAgInSc1d1	dvouatomový
kyslík	kyslík	k1gInSc1	kyslík
<g/>
)	)	kIx)	)
v	v	k7c6	v
absorpčních	absorpční	k2eAgFnPc6d1	absorpční
čarách	čára	k1gFnPc6	čára
molekulárního	molekulární	k2eAgInSc2d1	molekulární
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
absorpce	absorpce	k1gFnSc1	absorpce
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc4	kyslík
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kandidátem	kandidát	k1gMnSc7	kandidát
jsou	být	k5eAaImIp3nP	být
molekuly	molekula	k1gFnPc1	molekula
kyslíku	kyslík	k1gInSc2	kyslík
zachyceného	zachycený	k2eAgInSc2d1	zachycený
v	v	k7c6	v
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
absorpčních	absorpční	k2eAgInPc2d1	absorpční
pásů	pás	k1gInPc2	pás
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
šířce	šířka	k1gFnSc6	šířka
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c4	na
albedu	albeda	k1gMnSc4	albeda
povrchu	povrch	k1gInSc2	povrch
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
klesat	klesat	k5eAaImF	klesat
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
šířkou	šířka	k1gFnSc7	šířka
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
O3	O3	k1gFnSc1	O3
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
opačný	opačný	k2eAgInSc4d1	opačný
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorní	laboratorní	k2eAgInPc1d1	laboratorní
výsledky	výsledek	k1gInPc1	výsledek
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
O2	O2	k1gFnSc1	O2
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
shlukovat	shlukovat	k5eAaImF	shlukovat
a	a	k8xC	a
bublat	bublat	k5eAaImF	bublat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
se	se	k3xPyFc4	se
v	v	k7c6	v
ledu	led	k1gInSc6	led
na	na	k7c6	na
relativně	relativně	k6eAd1	relativně
teplém	teplý	k2eAgInSc6d1	teplý
povrchu	povrch	k1gInSc6	povrch
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
100	[number]	k4	100
K.	K.	kA	K.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c6	na
Europe	Europ	k1gInSc5	Europ
byl	být	k5eAaImAgInS	být
sodík	sodík	k1gInSc1	sodík
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
se	se	k3xPyFc4	se
při	při	k7c6	při
podobném	podobný	k2eAgNnSc6d1	podobné
hledání	hledání	k1gNnSc6	hledání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
nenašel	najít	k5eNaPmAgMnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
byl	být	k5eAaImAgInS	být
přinejmenším	přinejmenším	k6eAd1	přinejmenším
13	[number]	k4	13
krát	krát	k6eAd1	krát
méně	málo	k6eAd2	málo
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
okolo	okolo	k7c2	okolo
Ganymedu	Ganymed	k1gMnSc3	Ganymed
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
relativnímu	relativní	k2eAgInSc3d1	relativní
nedostatku	nedostatek	k1gInSc3	nedostatek
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
odrazí	odrazit	k5eAaPmIp3nP	odrazit
energeticky	energeticky	k6eAd1	energeticky
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
atomární	atomární	k2eAgInSc4d1	atomární
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
až	až	k9	až
3000	[number]	k4	3000
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hustota	hustota	k1gFnSc1	hustota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,5e+4	[number]	k4	1,5e+4
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
==	==	k?	==
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
provedla	provést	k5eAaPmAgFnS	provést
šest	šest	k4xCc4	šest
těsných	těsný	k2eAgInPc2d1	těsný
průletů	průlet	k1gInPc2	průlet
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1995	[number]	k4	1995
až	až	k9	až
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
objevila	objevit	k5eAaPmAgFnS	objevit
trvalé	trvalý	k2eAgNnSc4d1	trvalé
magnetické	magnetický	k2eAgNnSc4d1	magnetické
dipólové	dipólový	k2eAgNnSc4d1	dipólový
pole	pole	k1gNnSc4	pole
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
na	na	k7c4	na
Jupiterovu	Jupiterův	k2eAgFnSc4d1	Jupiterova
působení	působení	k1gNnSc4	působení
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
měsícem	měsíc	k1gInSc7	měsíc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgMnSc4	který
bylo	být	k5eAaImAgNnS	být
vlastní	vlastní	k2eAgNnSc1d1	vlastní
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
neindukované	indukovaný	k2eNgNnSc1d1	indukovaný
polem	pole	k1gNnSc7	pole
planety	planeta	k1gFnSc2	planeta
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
kolem	kolem	k6eAd1	kolem
Ganymedu	Ganymed	k1gMnSc3	Ganymed
lze	lze	k6eAd1	lze
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
přiblížení	přiblížení	k1gNnSc6	přiblížení
považovat	považovat	k5eAaImF	považovat
za	za	k7c2	za
složení	složení	k1gNnSc2	složení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
dipólového	dipólový	k2eAgNnSc2d1	dipólový
pole	pole	k1gNnSc2	pole
Ganymedu	Ganymed	k1gMnSc3	Ganymed
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
lze	lze	k6eAd1	lze
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
obíhajícího	obíhající	k2eAgInSc2d1	obíhající
Ganymedu	Ganymed	k1gMnSc3	Ganymed
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
homogenní	homogenní	k2eAgFnSc4d1	homogenní
<g/>
,	,	kIx,	,
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
magnetické	magnetický	k2eAgFnSc2d1	magnetická
indukce	indukce	k1gFnSc2	indukce
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
nT	nT	k?	nT
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
oběhu	oběh	k1gInSc2	oběh
měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
poněkud	poněkud	k6eAd1	poněkud
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
naměřených	naměřený	k2eAgNnPc2d1	naměřené
dat	datum	k1gNnPc2	datum
skutečného	skutečný	k2eAgNnSc2d1	skutečné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
pak	pak	k6eAd1	pak
vycházejí	vycházet	k5eAaImIp3nP	vycházet
hodnoty	hodnota	k1gFnPc1	hodnota
vlastního	vlastní	k2eAgNnSc2d1	vlastní
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
:	:	kIx,	:
hodnota	hodnota	k1gFnSc1	hodnota
magnetického	magnetický	k2eAgInSc2d1	magnetický
momentu	moment	k1gInSc2	moment
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1,3	[number]	k4	1,3
<g/>
×	×	k?	×
<g/>
1020	[number]	k4	1020
A	A	kA	A
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
třikrát	třikrát	k6eAd1	třikrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
například	například	k6eAd1	například
magnetický	magnetický	k2eAgInSc4d1	magnetický
moment	moment	k1gInSc4	moment
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
magnetického	magnetický	k2eAgInSc2d1	magnetický
dipólu	dipól	k1gInSc2	dipól
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
odchýlen	odchýlit	k5eAaPmNgInS	odchýlit
od	od	k7c2	od
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
o	o	k7c4	o
úhel	úhel	k1gInSc4	úhel
přibližně	přibližně	k6eAd1	přibližně
176	[number]	k4	176
<g/>
°	°	k?	°
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
téměř	téměř	k6eAd1	téměř
opačný	opačný	k2eAgInSc1d1	opačný
směr	směr	k1gInSc1	směr
než	než	k8xS	než
magnetický	magnetický	k2eAgInSc1d1	magnetický
dipól	dipól	k1gInSc1	dipól
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
severní	severní	k2eAgMnSc1d1	severní
<g/>
"	"	kIx"	"
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
rovinou	rovina	k1gFnSc7	rovina
na	na	k7c4	na
24	[number]	k4	24
<g/>
°	°	k?	°
délky	délka	k1gFnSc2	délka
Ganymedu	Ganymed	k1gMnSc3	Ganymed
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
poledník	poledník	k1gInSc1	poledník
0	[number]	k4	0
<g/>
°	°	k?	°
směřuje	směřovat	k5eAaImIp3nS	směřovat
vlivem	vliv	k1gInSc7	vliv
vázané	vázaný	k2eAgFnSc2d1	vázaná
rotace	rotace	k1gFnSc2	rotace
vždy	vždy	k6eAd1	vždy
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
severní	severní	k2eAgMnSc1d1	severní
<g/>
"	"	kIx"	"
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
"	"	kIx"	"
<g/>
odvrácené	odvrácený	k2eAgMnPc4d1	odvrácený
<g/>
"	"	kIx"	"
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
oběžnému	oběžný	k2eAgInSc3d1	oběžný
pohybu	pohyb	k1gInSc3	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
indukce	indukce	k1gFnSc1	indukce
vlastního	vlastní	k2eAgNnSc2d1	vlastní
pole	pole	k1gNnSc2	pole
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Ganymedu	Ganymed	k1gMnSc3	Ganymed
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
přibližně	přibližně	k6eAd1	přibližně
750	[number]	k4	750
nT	nT	k?	nT
<g/>
,	,	kIx,	,
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
asi	asi	k9	asi
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
1440	[number]	k4	1440
nT	nT	k?	nT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Struktura	struktura	k1gFnSc1	struktura
výsledného	výsledný	k2eAgNnSc2d1	výsledné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
planetárních	planetární	k2eAgFnPc2d1	planetární
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
orientaci	orientace	k1gFnSc3	orientace
obklopujícího	obklopující	k2eAgNnSc2d1	obklopující
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
Ganymedu	Ganymed	k1gMnSc3	Ganymed
(	(	kIx(	(
<g/>
do	do	k7c2	do
cca	cca	kA	cca
30	[number]	k4	30
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
)	)	kIx)	)
indukční	indukční	k2eAgInPc4d1	indukční
čáry	čár	k1gInPc4	čár
vystupující	vystupující	k2eAgInPc4d1	vystupující
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
opět	opět	k6eAd1	opět
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
(	(	kIx(	(
<g/>
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k9	tak
oblast	oblast	k1gFnSc4	oblast
vlastní	vlastní	k2eAgFnSc2d1	vlastní
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
navázané	navázaný	k2eAgInPc1d1	navázaný
na	na	k7c4	na
indukční	indukční	k2eAgFnPc4d1	indukční
čáry	čára	k1gFnPc4	čára
Jupiterova	Jupiterův	k2eAgNnSc2d1	Jupiterovo
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
je	být	k5eAaImIp3nS	být
4-5	[number]	k4	4-5
poloměrů	poloměr	k1gInPc2	poloměr
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
i	i	k9	i
existence	existence	k1gFnSc1	existence
magnetopauzy	magnetopauza	k1gFnSc2	magnetopauza
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgInPc1d2	lepší
modely	model	k1gInPc1	model
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
navíc	navíc	k6eAd1	navíc
plazma	plazma	k1gNnSc4	plazma
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
Ganymed	Ganymed	k1gMnSc1	Ganymed
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
,	,	kIx,	,
a	a	k8xC	a
započítávají	započítávat	k5eAaImIp3nP	započítávat
tak	tak	k6eAd1	tak
do	do	k7c2	do
modelu	model	k1gInSc2	model
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
i	i	k8xC	i
magnetohydrodynamické	magnetohydrodynamický	k2eAgInPc4d1	magnetohydrodynamický
vlivy	vliv	k1gInPc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
schopny	schopen	k2eAgFnPc1d1	schopna
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Země	zem	k1gFnSc2	zem
není	být	k5eNaImIp3nS	být
magnetopauza	magnetopauza	k1gFnSc1	magnetopauza
u	u	k7c2	u
Ganymedu	Ganymed	k1gMnSc3	Ganymed
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
rázovou	rázový	k2eAgFnSc7d1	rázová
vlnou	vlna	k1gFnSc7	vlna
–	–	k?	–
plazma	plazma	k1gFnSc1	plazma
spolurotující	spolurotující	k2eAgFnSc1d1	spolurotující
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
má	mít	k5eAaImIp3nS	mít
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
Ganymedu	Ganymed	k1gMnSc3	Ganymed
rychlost	rychlost	k1gFnSc4	rychlost
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Alfvénova	Alfvénův	k2eAgFnSc1d1	Alfvénův
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
poloviční	poloviční	k2eAgFnSc1d1	poloviční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vlastní	vlastní	k2eAgFnSc2d1	vlastní
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
zachycovány	zachycován	k2eAgFnPc1d1	zachycována
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
radiační	radiační	k2eAgInPc1d1	radiační
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
Ganymedu	Ganymed	k1gMnSc3	Ganymed
může	moct	k5eAaImIp3nS	moct
plazma	plazma	k1gNnSc4	plazma
z	z	k7c2	z
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
ionosféry	ionosféra	k1gFnSc2	ionosféra
podél	podél	k7c2	podél
magnetických	magnetický	k2eAgFnPc2d1	magnetická
indukčních	indukční	k2eAgFnPc2d1	indukční
čar	čára	k1gFnPc2	čára
vstupovat	vstupovat	k5eAaImF	vstupovat
až	až	k9	až
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
skutečně	skutečně	k6eAd1	skutečně
byly	být	k5eAaImAgFnP	být
pozorovány	pozorován	k2eAgMnPc4d1	pozorován
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
teleskopem	teleskop	k1gInSc7	teleskop
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgInPc4d1	těžký
ionty	ion	k1gInPc4	ion
dopadající	dopadající	k2eAgInPc4d1	dopadající
až	až	k9	až
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
mají	mít	k5eAaImIp3nP	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
energii	energie	k1gFnSc4	energie
k	k	k7c3	k
vyrážení	vyrážení	k1gNnSc3	vyrážení
atomů	atom	k1gInPc2	atom
ze	z	k7c2	z
struktury	struktura	k1gFnSc2	struktura
ledu	led	k1gInSc2	led
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tím	ten	k3xDgNnSc7	ten
jeho	jeho	k3xOp3gInSc2	jeho
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
ztmavnutí	ztmavnutí	k1gNnSc1	ztmavnutí
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ganymed	Ganymed	k1gMnSc1	Ganymed
má	mít	k5eAaImIp3nS	mít
diferencovanou	diferencovaný	k2eAgFnSc4d1	diferencovaná
strukturu	struktura	k1gFnSc4	struktura
s	s	k7c7	s
objemným	objemný	k2eAgNnSc7d1	objemné
kovovým	kovový	k2eAgNnSc7d1	kovové
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
generované	generovaný	k2eAgNnSc1d1	generované
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
–	–	k?	–
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
pohybu	pohyb	k1gInSc2	pohyb
vodivých	vodivý	k2eAgMnPc2d1	vodivý
materiálů	materiál	k1gInPc2	materiál
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniká	vznikat	k5eAaImIp3nS	vznikat
konvekčním	konvekční	k2eAgInSc7d1	konvekční
pohybem	pohyb	k1gInSc7	pohyb
uvnitř	uvnitř	k7c2	uvnitř
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
magnetohydrodynamické	magnetohydrodynamický	k2eAgNnSc4d1	magnetohydrodynamický
dynamo	dynamo	k1gNnSc4	dynamo
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgFnPc1d1	jistá
pochybnosti	pochybnost	k1gFnPc1	pochybnost
u	u	k7c2	u
předpokladů	předpoklad	k1gInPc2	předpoklad
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgNnPc4d1	podobné
tělesa	těleso	k1gNnPc4	těleso
vlastní	vlastní	k2eAgNnPc4d1	vlastní
pole	pole	k1gNnPc4	pole
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
výzkumy	výzkum	k1gInPc1	výzkum
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
měsíce	měsíc	k1gInSc2	měsíc
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
natolik	natolik	k6eAd1	natolik
vychladlé	vychladlý	k2eAgNnSc1d1	vychladlé
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tekutý	tekutý	k2eAgInSc1d1	tekutý
pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
již	již	k6eAd1	již
zaniklé	zaniklý	k2eAgNnSc1d1	zaniklé
<g/>
.	.	kIx.	.
</s>
<s>
Navrženým	navržený	k2eAgNnSc7d1	navržené
východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc4d1	podobné
zdůvodnění	zdůvodnění	k1gNnSc4	zdůvodnění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
popraskaného	popraskaný	k2eAgInSc2d1	popraskaný
povrchu	povrch	k1gInSc2	povrch
–	–	k?	–
slapové	slapový	k2eAgInPc1d1	slapový
jevy	jev	k1gInPc1	jev
by	by	kYmCp3nP	by
dostatečně	dostatečně	k6eAd1	dostatečně
zahřívaly	zahřívat	k5eAaImAgInP	zahřívat
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
bránily	bránit	k5eAaImAgFnP	bránit
tak	tak	k6eAd1	tak
jádru	jádro	k1gNnSc6	jádro
vychladnout	vychladnout	k5eAaPmF	vychladnout
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
remanentní	remanentní	k2eAgFnSc1d1	remanentní
magnetizace	magnetizace	k1gFnSc1	magnetizace
křemičitanových	křemičitanový	k2eAgFnPc2d1	křemičitanová
hornin	hornina	k1gFnPc2	hornina
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
silným	silný	k2eAgNnSc7d1	silné
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
generovaným	generovaný	k2eAgNnSc7d1	generované
magnetohydrodynamickým	magnetohydrodynamický	k2eAgNnSc7d1	magnetohydrodynamický
dynamem	dynamo	k1gNnSc7	dynamo
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
má	mít	k5eAaImIp3nS	mít
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Callisto	Callista	k1gMnSc5	Callista
a	a	k8xC	a
Europa	Europ	k1gMnSc4	Europ
<g/>
,	,	kIx,	,
také	také	k9	také
indukované	indukovaný	k2eAgNnSc1d1	indukované
dipólové	dipólový	k2eAgNnSc1d1	dipólový
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
řád	řád	k1gInSc4	řád
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
vlastní	vlastní	k2eAgNnSc1d1	vlastní
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
převládající	převládající	k2eAgFnSc1d1	převládající
orientace	orientace	k1gFnSc1	orientace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
radiálním	radiální	k2eAgInSc6d1	radiální
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
nebo	nebo	k8xC	nebo
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rovníku	rovník	k1gInSc2	rovník
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jeho	jeho	k3xOp3gFnSc1	jeho
magnetická	magnetický	k2eAgFnSc1d1	magnetická
indukce	indukce	k1gFnSc1	indukce
hodnoty	hodnota	k1gFnSc2	hodnota
až	až	k9	až
60	[number]	k4	60
nT	nT	k?	nT
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
vodivostí	vodivost	k1gFnSc7	vodivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
rotace	rotace	k1gFnSc1	rotace
==	==	k?	==
</s>
</p>
<p>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
obíhá	obíhat	k5eAaImIp3nS	obíhat
Jupiter	Jupiter	k1gMnSc1	Jupiter
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
070	[number]	k4	070
400	[number]	k4	400
km	km	kA	km
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Galileovými	Galileův	k2eAgInPc7d1	Galileův
měsíci	měsíc	k1gInPc7	měsíc
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
od	od	k7c2	od
Jupitera	Jupiter	k1gMnSc2	Jupiter
druhý	druhý	k4xOgInSc4	druhý
nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
(	(	kIx(	(
<g/>
po	po	k7c6	po
Callisto	Callista	k1gMnSc5	Callista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
mu	on	k3xPp3gMnSc3	on
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
má	mít	k5eAaImIp3nS	mít
Ganymed	Ganymed	k1gMnSc1	Ganymed
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
stále	stále	k6eAd1	stále
přivrácen	přivrátit	k5eAaPmNgInS	přivrátit
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
výstřední	výstřední	k2eAgFnSc1d1	výstřední
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
nakloněná	nakloněný	k2eAgFnSc1d1	nakloněná
k	k	k7c3	k
Jupiterovu	Jupiterův	k2eAgInSc3d1	Jupiterův
rovníku	rovník	k1gInSc3	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Výstřednost	výstřednost	k1gFnSc1	výstřednost
(	(	kIx(	(
<g/>
excentricita	excentricita	k1gFnSc1	excentricita
<g/>
)	)	kIx)	)
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
naklonění	naklonění	k1gNnPc4	naklonění
(	(	kIx(	(
<g/>
inklinace	inklinace	k1gFnSc1	inklinace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kvaziperiodicky	kvaziperiodicky	k6eAd1	kvaziperiodicky
mění	měnit	k5eAaImIp3nS	měnit
vlivem	vliv	k1gInSc7	vliv
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
rušení	rušení	k1gNnSc2	rušení
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
měřítku	měřítko	k1gNnSc6	měřítko
staletí	staletí	k1gNnPc2	staletí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
excentricita	excentricita	k1gFnSc1	excentricita
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
0,0009	[number]	k4	0,0009
<g/>
–	–	k?	–
<g/>
0,0022	[number]	k4	0,0022
a	a	k8xC	a
inklinace	inklinace	k1gFnPc1	inklinace
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
0,05	[number]	k4	0,05
<g/>
–	–	k?	–
<g/>
0,32	[number]	k4	0,32
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oběžné	oběžný	k2eAgFnPc1d1	oběžná
změny	změna	k1gFnPc1	změna
současně	současně	k6eAd1	současně
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sklon	sklon	k1gInSc4	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
(	(	kIx(	(
<g/>
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
rotační	rotační	k2eAgFnSc7d1	rotační
a	a	k8xC	a
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
osou	osa	k1gFnSc7	osa
<g/>
)	)	kIx)	)
mění	měnit	k5eAaImIp3nS	měnit
mezi	mezi	k7c7	mezi
0	[number]	k4	0
až	až	k9	až
0,33	[number]	k4	0,33
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
a	a	k8xC	a
Ganymed	Ganymed	k1gMnSc1	Ganymed
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
4	[number]	k4	4
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
oběhu	oběh	k1gInSc2	oběh
Ganymeda	Ganymed	k1gMnSc2	Ganymed
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Europa	Europa	k1gFnSc1	Europa
dvakrát	dvakrát	k6eAd1	dvakrát
a	a	k8xC	a
Io	Io	k1gFnSc1	Io
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
konjunkce	konjunkce	k1gFnSc1	konjunkce
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Io	Io	k1gFnSc2	Io
nastává	nastávat	k5eAaImIp3nS	nastávat
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Io	Io	k1gFnSc1	Io
nejblíže	blízce	k6eAd3	blízce
Jupiteru	Jupiter	k1gInSc2	Jupiter
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
perijovium	perijovium	k1gNnSc4	perijovium
<g/>
)	)	kIx)	)
a	a	k8xC	a
Europa	Europa	k1gFnSc1	Europa
nejdále	daleko	k6eAd3	daleko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
apojovium	apojovium	k1gNnSc1	apojovium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
konjunkce	konjunkce	k1gFnSc1	konjunkce
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Ganymeda	Ganymed	k1gMnSc4	Ganymed
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Europa	Europa	k1gFnSc1	Europa
v	v	k7c6	v
perijoviu	perijovium	k1gNnSc6	perijovium
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
poměry	poměr	k1gInPc1	poměr
oběžných	oběžný	k2eAgFnPc2d1	oběžná
dob	doba	k1gFnPc2	doba
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Laplaceova	Laplaceův	k2eAgFnSc1d1	Laplaceova
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
)	)	kIx)	)
také	také	k9	také
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
konjunkce	konjunkce	k1gFnPc1	konjunkce
trojité	trojitý	k2eAgFnSc2d1	trojitá
<g/>
.	.	kIx.	.
<g/>
Současná	současný	k2eAgFnSc1d1	současná
Laplaceova	Laplaceův	k2eAgFnSc1d1	Laplaceova
rezonance	rezonance	k1gFnSc1	rezonance
již	již	k6eAd1	již
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
více	hodně	k6eAd2	hodně
zvýšit	zvýšit	k5eAaPmF	zvýšit
výstřednost	výstřednost	k1gFnSc4	výstřednost
dráhy	dráha	k1gFnSc2	dráha
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
excentricita	excentricita	k1gFnSc1	excentricita
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližné	přibližný	k2eAgFnPc4d1	přibližná
hodnoty	hodnota	k1gFnPc4	hodnota
0,0013	[number]	k4	0,0013
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
z	z	k7c2	z
dávné	dávný	k2eAgFnSc2d1	dávná
historie	historie	k1gFnSc2	historie
satelitu	satelit	k1gInSc2	satelit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvyšování	zvyšování	k1gNnSc1	zvyšování
výstřednosti	výstřednost	k1gFnSc2	výstřednost
dráhy	dráha	k1gFnSc2	dráha
ještě	ještě	k9	ještě
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
však	však	k9	však
současně	současně	k6eAd1	současně
poněkud	poněkud	k6eAd1	poněkud
matoucí	matoucí	k2eAgFnSc1d1	matoucí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
rezonance	rezonance	k1gFnSc1	rezonance
již	již	k6eAd1	již
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
narušena	narušit	k5eAaPmNgFnS	narušit
vlivem	vlivem	k7c2	vlivem
slapové	slapový	k2eAgFnSc2d1	slapová
disipace	disipace	k1gFnSc2	disipace
uvnitř	uvnitř	k6eAd1	uvnitř
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
nárůstu	nárůst	k1gInSc3	nárůst
výstřednosti	výstřednost	k1gFnSc2	výstřednost
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
nanejvýš	nanejvýš	k6eAd1	nanejvýš
před	před	k7c7	před
několika	několik	k4yIc7	několik
stovkami	stovka	k1gFnPc7	stovka
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
výstřednost	výstřednost	k1gFnSc1	výstřednost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Ganymedu	Ganymed	k1gMnSc3	Ganymed
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
–	–	k?	–
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
0,0013	[number]	k4	0,0013
–	–	k?	–
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
slapové	slapový	k2eAgNnSc1d1	slapové
zahřívání	zahřívání	k1gNnSc1	zahřívání
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
však	však	k9	však
Ganymed	Ganymed	k1gMnSc1	Ganymed
mohl	moct	k5eAaImAgMnS	moct
projít	projít	k5eAaPmF	projít
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rezonancemi	rezonance	k1gFnPc7	rezonance
podobnými	podobný	k2eAgFnPc7d1	podobná
rezonanci	rezonance	k1gFnSc3	rezonance
Laplaceově	Laplaceův	k2eAgFnSc6d1	Laplaceova
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgInPc3	jenž
byla	být	k5eAaImAgFnS	být
výstřednost	výstřednost	k1gFnSc1	výstřednost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
až	až	k9	až
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
0,01	[number]	k4	0,01
<g/>
–	–	k?	–
<g/>
0,02	[number]	k4	0,02
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
významné	významný	k2eAgNnSc4d1	významné
slapové	slapový	k2eAgNnSc4d1	slapové
zahřívání	zahřívání	k1gNnSc4	zahřívání
vnitřku	vnitřek	k1gInSc2	vnitřek
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zvrásněný	zvrásněný	k2eAgInSc1d1	zvrásněný
terén	terén	k1gInSc1	terén
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
i	i	k9	i
více	hodně	k6eAd2	hodně
takových	takový	k3xDgFnPc2	takový
episod	episoda	k1gFnPc2	episoda
<g/>
.	.	kIx.	.
<g/>
Původ	původ	k1gInSc1	původ
Laplaceovy	Laplaceův	k2eAgFnSc2d1	Laplaceova
rezonance	rezonance	k1gFnSc2	rezonance
mezi	mezi	k7c7	mezi
měsíci	měsíc	k1gInPc7	měsíc
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
a	a	k8xC	a
Ganymed	Ganymed	k1gMnSc1	Ganymed
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
hypotéz	hypotéza	k1gFnPc2	hypotéza
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ho	on	k3xPp3gMnSc4	on
hledat	hledat	k5eAaImF	hledat
již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
se	se	k3xPyFc4	se
však	však	k9	však
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
formace	formace	k1gFnSc1	formace
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
mohly	moct	k5eAaImAgFnP	moct
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
sledu	sled	k1gInSc6	sled
<g/>
:	:	kIx,	:
Slapové	slapový	k2eAgNnSc1d1	slapové
působení	působení	k1gNnSc1	působení
mezi	mezi	k7c7	mezi
Io	Io	k1gMnSc7	Io
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nárůst	nárůst	k1gInSc4	nárůst
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Io	Io	k1gMnSc1	Io
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
rezonance	rezonance	k1gFnSc2	rezonance
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
s	s	k7c7	s
Europou	Europa	k1gFnSc7	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
na	na	k7c6	na
Europu	Europ	k1gInSc6	Europ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vlivem	vliv	k1gInSc7	vliv
rezonance	rezonance	k1gFnSc2	rezonance
narůstala	narůstat	k5eAaImAgFnS	narůstat
i	i	k9	i
její	její	k3xOp3gFnSc1	její
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
Europa	Europa	k1gFnSc1	Europa
nedostala	dostat	k5eNaPmAgFnS	dostat
do	do	k7c2	do
rezonance	rezonance	k1gFnSc2	rezonance
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
s	s	k7c7	s
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
synchronizaci	synchronizace	k1gFnSc3	synchronizace
konjunkcí	konjunkce	k1gFnSc7	konjunkce
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
uzamčení	uzamčení	k1gNnSc3	uzamčení
v	v	k7c6	v
Laplaceově	Laplaceův	k2eAgFnSc6d1	Laplaceova
rezonanci	rezonance	k1gFnSc6	rezonance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objev	k1gInSc4	objev
a	a	k8xC	a
pojmenování	pojmenování	k1gNnSc4	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1610	[number]	k4	1610
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gInPc7	Galile
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
nově	nově	k6eAd1	nově
zkonstruovaným	zkonstruovaný	k2eAgInSc7d1	zkonstruovaný
dalekohledem	dalekohled	k1gInSc7	dalekohled
tři	tři	k4xCgInPc1	tři
světelné	světelný	k2eAgInPc1d1	světelný
zdroje	zdroj	k1gInPc1	zdroj
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
opakovaného	opakovaný	k2eAgNnSc2d1	opakované
pozorování	pozorování	k1gNnSc2	pozorování
druhého	druhý	k4xOgMnSc2	druhý
večera	večer	k1gInSc2	večer
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
pohnuly	pohnout	k5eAaPmAgInP	pohnout
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1610	[number]	k4	1610
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
předpokládanou	předpokládaný	k2eAgFnSc4d1	předpokládaná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Galileo	Galilea	k1gFnSc5	Galilea
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
údajné	údajný	k2eAgFnPc1d1	údajná
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
objeviteli	objevitel	k1gMnSc6	objevitel
mu	on	k3xPp3gMnSc3	on
připadlo	připadnout	k5eAaPmAgNnS	připadnout
právo	právo	k1gNnSc4	právo
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
Medicejské	Medicejský	k2eAgInPc1d1	Medicejský
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
<g/>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Nicolas-Claude	Nicolas-Claud	k1gInSc5	Nicolas-Claud
Fabri	Fabr	k1gFnPc1	Fabr
de	de	k?	de
Peiresc	Peiresc	k1gFnSc1	Peiresc
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Medicejských	Medicejský	k2eAgInPc2d1	Medicejský
měsíců	měsíc	k1gInPc2	měsíc
zavedl	zavést	k5eAaPmAgInS	zavést
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
měsíce	měsíc	k1gInPc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
před	před	k7c7	před
Galileem	Galileus	k1gMnSc7	Galileus
<g/>
,	,	kIx,	,
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
původně	původně	k6eAd1	původně
pojmenování	pojmenování	k1gNnSc4	pojmenování
"	"	kIx"	"
<g/>
Saturn	Saturn	k1gInSc1	Saturn
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jupiter	Jupiter	k1gInSc1	Jupiter
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Venuše	Venuše	k1gFnSc1	Venuše
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Merkur	Merkur	k1gInSc1	Merkur
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
bylo	být	k5eAaImAgNnS	být
zamítnuto	zamítnout	k5eAaPmNgNnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
Johana	Johan	k1gMnSc4	Johan
Keplera	Kepler	k1gMnSc4	Kepler
Marius	Marius	k1gMnSc1	Marius
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
pokusil	pokusit	k5eAaPmAgInS	pokusit
navrhnout	navrhnout	k5eAaPmF	navrhnout
jiná	jiný	k2eAgNnPc4d1	jiné
pojmenování	pojmenování	k1gNnPc4	pojmenování
pro	pro	k7c4	pro
měsíce	měsíc	k1gInPc4	měsíc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
a	a	k8xC	a
i	i	k9	i
další	další	k2eAgNnPc4d1	další
jména	jméno	k1gNnPc4	jméno
pro	pro	k7c4	pro
Galileovo	Galileův	k2eAgNnSc4d1	Galileovo
měsíce	měsíc	k1gInPc4	měsíc
upadlo	upadnout	k5eAaPmAgNnS	upadnout
v	v	k7c4	v
zapomnění	zapomnění	k1gNnSc4	zapomnění
po	po	k7c4	po
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
názvům	název	k1gInPc3	název
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívější	dřívější	k2eAgFnSc6d1	dřívější
astronomické	astronomický	k2eAgFnSc6d1	astronomická
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
Ganymed	Ganymed	k1gMnSc1	Ganymed
uváděn	uváděn	k2eAgMnSc1d1	uváděn
jako	jako	k8xS	jako
římská	římský	k2eAgFnSc1d1	římská
číslice	číslice	k1gFnSc1	číslice
III	III	kA	III
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyjadřovalo	vyjadřovat	k5eAaImAgNnS	vyjadřovat
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
třetí	třetí	k4xOgInSc4	třetí
měsíc	měsíc	k1gInSc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
začalo	začít	k5eAaPmAgNnS	začít
opět	opět	k6eAd1	opět
používat	používat	k5eAaImF	používat
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
společně	společně	k6eAd1	společně
Kepler	Kepler	k1gMnSc1	Kepler
a	a	k8xC	a
Marius	Marius	k1gMnSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgInSc7d1	jediný
měsícem	měsíc	k1gInSc7	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nese	nést	k5eAaImIp3nS	nést
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
také	také	k9	také
ony	onen	k3xDgInPc1	onen
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc4	jméno
milenek	milenka	k1gFnPc2	milenka
boha	bůh	k1gMnSc4	bůh
Dia	Dia	k1gMnSc4	Dia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průzkum	průzkum	k1gInSc1	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
sond	sonda	k1gFnPc2	sonda
letících	letící	k2eAgMnPc2d1	letící
či	či	k8xC	či
obíhajících	obíhající	k2eAgMnPc2d1	obíhající
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
detailně	detailně	k6eAd1	detailně
zkoumalo	zkoumat	k5eAaImAgNnS	zkoumat
i	i	k9	i
měsíc	měsíc	k1gInSc4	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
systém	systém	k1gInSc4	systém
navštívila	navštívit	k5eAaPmAgFnS	navštívit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc4d1	americký
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
následovaný	následovaný	k2eAgInSc1d1	následovaný
Pioneerem	Pioneero	k1gNnSc7	Pioneero
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Pioneery	Pioneer	k1gInPc1	Pioneer
o	o	k7c6	o
Ganymedu	Ganymed	k1gMnSc6	Ganymed
mnoho	mnoho	k6eAd1	mnoho
informací	informace	k1gFnPc2	informace
nezískaly	získat	k5eNaPmAgInP	získat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
soustavou	soustava	k1gFnSc7	soustava
proletěla	proletět	k5eAaPmAgFnS	proletět
dvojice	dvojice	k1gFnSc1	dvojice
amerických	americký	k2eAgFnPc2d1	americká
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Průlet	průlet	k1gInSc1	průlet
Voyagerů	Voyager	k1gMnPc2	Voyager
pomohl	pomoct	k5eAaPmAgInS	pomoct
určit	určit	k5eAaPmF	určit
průměr	průměr	k1gInSc1	průměr
měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ganymed	Ganymed	k1gMnSc1	Ganymed
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
měsíc	měsíc	k1gInSc4	měsíc
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvrátilo	vyvrátit	k5eAaPmAgNnS	vyvrátit
předchozí	předchozí	k2eAgInSc4d1	předchozí
opačný	opačný	k2eAgInSc4d1	opačný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Ukázaly	ukázat	k5eAaPmAgFnP	ukázat
také	také	k9	také
povrch	povrch	k6eAd1wR	povrch
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
trhlinami	trhlina	k1gFnPc7	trhlina
a	a	k8xC	a
prasklinami	prasklina	k1gFnPc7	prasklina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
navedena	navést	k5eAaPmNgNnP	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1996	[number]	k4	1996
až	až	k8xS	až
2000	[number]	k4	2000
provedla	provést	k5eAaPmAgFnS	provést
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
těsných	těsný	k2eAgInPc2d1	těsný
průletů	průlet	k1gInPc2	průlet
kolem	kolem	k7c2	kolem
Ganymedu	Ganymed	k1gMnSc6	Ganymed
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podrobně	podrobně	k6eAd1	podrobně
ho	on	k3xPp3gMnSc4	on
zmapovat	zmapovat	k5eAaPmF	zmapovat
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
průlety	průlet	k1gInPc4	průlet
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
G	G	kA	G
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
G28	G28	k1gFnSc1	G28
a	a	k8xC	a
G	G	kA	G
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nejtěsnějšího	těsný	k2eAgInSc2d3	nejtěsnější
průletu	průlet	k1gInSc2	průlet
G2	G2	k1gFnSc2	G2
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
pouze	pouze	k6eAd1	pouze
264	[number]	k4	264
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Průlet	průlet	k1gInSc1	průlet
G1	G1	k1gFnSc2	G1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
přinesl	přinést	k5eAaPmAgInS	přinést
objev	objev	k1gInSc1	objev
magnetické	magnetický	k2eAgFnSc2d1	magnetická
pole	pole	k1gFnSc2	pole
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
ohlášeno	ohlášen	k2eAgNnSc1d1	ohlášeno
objevení	objevení	k1gNnSc1	objevení
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
odeslala	odeslat	k5eAaPmAgFnS	odeslat
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
spektroskopických	spektroskopický	k2eAgInPc2d1	spektroskopický
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
složky	složka	k1gFnSc2	složka
netvořené	tvořený	k2eNgFnSc2d1	tvořený
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
proletěla	proletět	k5eAaPmAgFnS	proletět
kolem	kolem	k6eAd1	kolem
Ganymedu	Ganymed	k1gMnSc3	Ganymed
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
vyhotovila	vyhotovit	k5eAaPmAgFnS	vyhotovit
mapu	mapa	k1gFnSc4	mapa
topografie	topografie	k1gFnSc2	topografie
a	a	k8xC	a
složení	složení	k1gNnSc2	složení
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2020	[number]	k4	2020
je	být	k5eAaImIp3nS	být
naplánován	naplánován	k2eAgInSc4d1	naplánován
start	start	k1gInSc4	start
mise	mise	k1gFnSc2	mise
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
evropské	evropský	k2eAgFnSc2d1	Evropská
ESA	eso	k1gNnPc1	eso
a	a	k8xC	a
americké	americký	k2eAgInPc1d1	americký
NASA	NASA	kA	NASA
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
měsíce	měsíc	k1gInPc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
agentury	agentura	k1gFnSc2	agentura
společně	společně	k6eAd1	společně
prohlásily	prohlásit	k5eAaPmAgInP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
dostane	dostat	k5eAaPmIp3nS	dostat
prioritu	priorita	k1gFnSc4	priorita
před	před	k7c7	před
misí	mise	k1gFnSc7	mise
Titan	titan	k1gInSc4	titan
Saturn	Saturn	k1gInSc1	Saturn
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
mise	mise	k1gFnSc1	mise
soupeřit	soupeřit	k5eAaImF	soupeřit
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
projekty	projekt	k1gInPc7	projekt
ESA	eso	k1gNnSc2	eso
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mise	mise	k1gFnSc1	mise
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
modulu	modul	k1gInSc2	modul
Jupiter	Jupiter	k1gInSc1	Jupiter
Europa	Europa	k1gFnSc1	Europa
Orbiter	Orbiter	k1gMnSc1	Orbiter
<g/>
,	,	kIx,	,
evropského	evropský	k2eAgInSc2d1	evropský
modulu	modul	k1gInSc2	modul
Jupiter	Jupiter	k1gMnSc1	Jupiter
Ganymede	Ganymed	k1gMnSc5	Ganymed
Orbiter	Orbitra	k1gFnPc2	Orbitra
a	a	k8xC	a
japonského	japonský	k2eAgInSc2d1	japonský
Jupiter	Jupiter	k1gMnSc1	Jupiter
Magnetospheric	Magnetospherice	k1gFnPc2	Magnetospherice
Orbiter	Orbiter	k1gMnSc1	Orbiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
návrhy	návrh	k1gInPc1	návrh
sond	sonda	k1gFnPc2	sonda
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
koncept	koncept	k1gInSc1	koncept
sondy	sonda	k1gFnSc2	sonda
Jupiter	Jupiter	k1gMnSc1	Jupiter
Icy	Icy	k1gMnSc1	Icy
Moons	Moonsa	k1gFnPc2	Moonsa
Orbiter	Orbiter	k1gMnSc1	Orbiter
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
získávat	získávat	k5eAaImF	získávat
energii	energie	k1gFnSc4	energie
pomocí	pomocí	k7c2	pomocí
štěpení	štěpení	k1gNnSc2	štěpení
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zrušena	zrušit	k5eAaPmNgFnS	zrušit
pro	pro	k7c4	pro
škrty	škrt	k1gInPc4	škrt
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
neuskutečněná	uskutečněný	k2eNgFnSc1d1	neuskutečněná
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
sonda	sonda	k1gFnSc1	sonda
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
The	The	k1gFnSc1	The
Grandeur	Grandeura	k1gFnPc2	Grandeura
of	of	k?	of
Ganymede	Ganymed	k1gMnSc5	Ganymed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ganymede	Ganymed	k1gMnSc5	Ganymed
(	(	kIx(	(
<g/>
moon	moono	k1gNnPc2	moono
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČEMAN	ČEMAN	kA	ČEMAN
<g/>
,	,	kIx,	,
Róbert	Róbert	k1gInSc1	Róbert
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc4	vesmír
1	[number]	k4	1
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Mapa	mapa	k1gFnSc1	mapa
Slovakia	Slovakia	k1gFnSc1	Slovakia
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
8067	[number]	k4	8067
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
72	[number]	k4	72
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GREGERSEN	GREGERSEN	kA	GREGERSEN
<g/>
,	,	kIx,	,
Erik	Erik	k1gMnSc1	Erik
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Outer	Outer	k1gMnSc1	Outer
Solar	Solar	k1gMnSc1	Solar
System	Syst	k1gMnSc7	Syst
<g/>
:	:	kIx,	:
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
Uranus	Uranus	k1gMnSc1	Uranus
<g/>
,	,	kIx,	,
Neptune	Neptun	k1gMnSc5	Neptun
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Dwarf	Dwarf	k1gInSc1	Dwarf
Planets	Planets	k1gInSc1	Planets
<g/>
.	.	kIx.	.
</s>
<s>
Britannica	Britannica	k1gMnSc1	Britannica
Educational	Educational	k1gMnSc1	Educational
Pub	Pub	k1gMnSc1	Pub
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
61530	[number]	k4	61530
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
109	[number]	k4	109
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ganymed	Ganymed	k1gMnSc1	Ganymed
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Solarviews	Solarviews	k1gInSc1	Solarviews
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Ganymede	Ganymed	k1gMnSc5	Ganymed
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
