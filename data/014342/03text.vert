<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
</s>
<s>
Charles	Charles	k1gMnSc1
Lutwidge	Lutwidg	k1gFnSc2
Dodgson	Dodgson	k1gMnSc1
Lewis	Lewis	k1gFnPc2
Carroll	Carroll	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
<g/>
,	,	kIx,
<g/>
foto	foto	k1gNnSc1
<g/>
:	:	kIx,
Oscar	Oscar	k1gInSc1
Gustave	Gustav	k1gMnSc5
RejlanderRodné	RejlanderRodný	k2eAgInPc5d1
jméno	jméno	k1gNnSc4
</s>
<s>
Charles	Charles	k1gMnSc1
Lutwidge	Lutwidg	k1gInSc2
Dodgson	Dodgson	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1832	#num#	k4
Daresbury	Daresbura	k1gFnSc2
<g/>
,	,	kIx,
Cheshire	Cheshir	k1gMnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1898	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Guildfordu	Guildforda	k1gMnSc4
<g/>
,	,	kIx,
Surrey	Surre	k1gMnPc4
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
zápal	zápal	k1gInSc1
plic	plíce	k1gFnPc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Surrey	Surrea	k1gFnPc1
Pseudonym	pseudonym	k1gInSc1
</s>
<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrollum	k1gNnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
logik	logik	k1gMnSc1
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
jáhen	jáhen	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
dětské	dětský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
deníků	deník	k1gInPc2
<g/>
,	,	kIx,
romanopisec	romanopisec	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
autobiografie	autobiografie	k1gFnSc2
a	a	k8xC
filozof	filozof	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Angličané	Angličan	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Christ	Christ	k1gInSc1
ChurchRugby	ChurchRugba	k1gFnSc2
SchoolOxfordská	SchoolOxfordský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
dětská	dětský	k2eAgFnSc1d1
<g/>
,	,	kIx,
fantasy	fantas	k1gInPc1
<g/>
,	,	kIx,
nonsens	nonsens	k1gNnPc1
<g/>
,	,	kIx,
odborná	odborný	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
poezie	poezie	k1gFnSc1
Témata	téma	k1gNnPc4
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
<g/>
,	,	kIx,
Za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
a	a	k8xC
co	co	k9
tam	tam	k6eAd1
Alenka	Alenka	k1gFnSc1
našla	najít	k5eAaPmAgFnS
<g/>
,	,	kIx,
Lovení	lovení	k1gNnSc4
Snárka	Snárek	k1gMnSc2
<g/>
,	,	kIx,
Žvahlav	Žvahlav	k1gMnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Charles	Charles	k1gMnSc1
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Frances	Frances	k1gMnSc1
Jane	Jan	k1gMnSc5
Lutwidge	Lutwidge	k1gNnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Caroline	Carolin	k1gInSc5
Hume	Hume	k1gNnPc7
Dodgson	Dodgson	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Louisa	Louisa	k?
Fletcher	Fletchra	k1gFnPc2
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
Lucy	Luca	k1gFnSc2
Dodgson	Dodgsona	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Wilfred	Wilfred	k1gMnSc1
Longley	Longlea	k1gFnSc2
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Frances	Frances	k1gMnSc1
Jane	Jan	k1gMnSc5
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Skeffington	Skeffington	k1gInSc1
Hume	Hume	k1gFnSc1
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Edwin	Edwin	k1gMnSc1
H.	H.	kA
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Henrietta	Henrietta	k1gFnSc1
Harrington	Harrington	k1gInSc1
Dodgson	Dodgson	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Mary	Mary	k1gFnSc1
Charlotte	Charlott	k1gInSc5
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Margaret	Margareta	k1gFnPc2
Anne	Ann	k1gMnSc2
Ashley	Ashlea	k1gMnSc2
Dodgson	Dodgson	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Charles	Charles	k1gMnSc1
Lutwidge	Lutwidg	k1gFnSc2
Dodgson	Dodgson	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1832	#num#	k4
Daresbury	Daresbura	k1gFnSc2
<g/>
,	,	kIx,
Cheshire	Cheshir	k1gInSc5
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1898	#num#	k4
Guildford	Guildforda	k1gFnPc2
<g/>
,	,	kIx,
Surrey	Surrea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Lewis	Lewis	k1gInSc1
Carroll	Carrolla	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
anglický	anglický	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
logik	logik	k1gMnSc1
<g/>
,	,	kIx,
učenec	učenec	k1gMnSc1
<g/>
,	,	kIx,
anglikánský	anglikánský	k2eAgMnSc1d1
diakon	diakon	k1gMnSc1
a	a	k8xC
fotograf	fotograf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
nejznámější	známý	k2eAgFnSc7d3
knihou	kniha	k1gFnSc7
je	být	k5eAaImIp3nS
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říše	k1gFnSc6
divů	div	k1gInPc2
a	a	k8xC
její	její	k3xOp3gNnSc4
následné	následný	k2eAgNnSc4d1
pokračování	pokračování	k1gNnSc1
Za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
a	a	k8xC
co	co	k3yQ
tam	tam	k6eAd1
Alenka	Alenka	k1gFnSc1
našla	najít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračování	pokračování	k1gNnSc1
Alenčiných	Alenčin	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
Alenka	Alenka	k1gFnSc1
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
Through	Through	k1gInSc1
the	the	k?
Looking-Glass	Looking-Glass	k1gInSc1
and	and	k?
What	What	k1gInSc1
Alice	Alice	k1gFnSc1
Found	Found	k1gMnSc1
There	Ther	k1gInSc5
<g/>
)	)	kIx)
poprvé	poprvé	k6eAd1
vyšlo	vyjít	k5eAaPmAgNnS
roku	rok	k1gInSc3
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
dílo	dílo	k1gNnSc1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
obsahuje	obsahovat	k5eAaImIp3nS
slavnou	slavný	k2eAgFnSc4d1
báseň	báseň	k1gFnSc4
Žvahlav	Žvahlav	k1gFnSc2
(	(	kIx(
<g/>
Tlachapoud	Tlachapoud	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
Carrollův	Carrollův	k2eAgInSc4d1
smysl	smysl	k1gInSc4
pro	pro	k7c4
hravost	hravost	k1gFnSc4
a	a	k8xC
fantazii	fantazie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7
třetím	třetí	k4xOgInSc7
nejproslulejším	proslulý	k2eAgInSc7d3
dílem	díl	k1gInSc7
ve	v	k7c6
světě	svět	k1gInSc6
po	po	k7c6
obou	dva	k4xCgFnPc6
Alenkách	Alenka	k1gFnPc6
je	být	k5eAaImIp3nS
alegorická	alegorický	k2eAgFnSc1d1
báseň	báseň	k1gFnSc1
The	The	k1gFnSc2
Hunting	Hunting	k1gInSc1
of	of	k?
the	the	k?
Snark	Snark	k1gInSc1
(	(	kIx(
<g/>
Lovení	lovení	k1gNnSc1
Snárka	Snárk	k1gInSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
Carrollovým	Carrollův	k2eAgInSc7d1
románem	román	k1gInSc7
byla	být	k5eAaImAgFnS
kniha	kniha	k1gFnSc1
Sylvie	Sylvie	k1gFnSc2
and	and	k?
Bruno	Bruno	k1gMnSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
svým	svůj	k3xOyFgNnSc7
jménem	jméno	k1gNnSc7
publikoval	publikovat	k5eAaBmAgMnS
odborné	odborný	k2eAgInPc4d1
texty	text	k1gInPc4
z	z	k7c2
oblasti	oblast	k1gFnSc2
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Předkové	předek	k1gMnPc1
</s>
<s>
Rodina	rodina	k1gFnSc1
Dodgsonů	Dodgson	k1gMnPc2
pocházela	pocházet	k5eAaImAgFnS
převážně	převážně	k6eAd1
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
Anglie	Anglie	k1gFnSc2
a	a	k8xC
částečně	částečně	k6eAd1
z	z	k7c2
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předkové	předek	k1gMnPc1
Charlese	Charles	k1gMnSc2
Dodgsona	Dodgson	k1gMnSc2
byli	být	k5eAaImAgMnP
většinou	většinou	k6eAd1
lidé	člověk	k1gMnPc1
konzervativní	konzervativní	k2eAgMnPc1d1
a	a	k8xC
činní	činný	k2eAgMnPc1d1
v	v	k7c6
anglikánské	anglikánský	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
pradědeček	pradědeček	k1gMnSc1
Charles	Charles	k1gMnSc1
Dodgson	Dodgson	k1gMnSc1
byl	být	k5eAaImAgMnS
anglikánským	anglikánský	k2eAgMnSc7d1
biskupem	biskup	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dědeček	dědeček	k1gMnSc1
<g/>
,	,	kIx,
rovněž	rovněž	k9
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
důstojníkem	důstojník	k1gMnSc7
a	a	k8xC
zahynul	zahynout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
jeho	jeho	k3xOp3gMnPc1
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
byli	být	k5eAaImAgMnP
ještě	ještě	k6eAd1
malé	malý	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Starší	starý	k2eAgMnPc1d2
z	z	k7c2
těchto	tento	k3xDgMnPc2
synů	syn	k1gMnPc2
–	–	k?
opět	opět	k6eAd1
Charles	Charles	k1gMnSc1
Dodgson	Dodgson	k1gMnSc1
–	–	k?
byl	být	k5eAaImAgMnS
Carrollův	Carrollův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrátil	Navrátil	k1gMnSc1
se	se	k3xPyFc4
k	k	k7c3
druhé	druhý	k4xOgFnSc3
rodinné	rodinný	k2eAgFnSc3d1
profesi	profes	k1gFnSc3
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
duchovním	duchovní	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštěvoval	navštěvovat	k5eAaImAgMnS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Rugby	rugby	k1gNnSc2
School	Schoola	k1gFnPc2
a	a	k8xC
potom	potom	k6eAd1
kolej	kolej	k1gFnSc1
Christ	Christ	k1gMnSc1
Church	Church	k1gMnSc1
na	na	k7c6
Oxfordské	oxfordský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
talent	talent	k1gInSc4
pro	pro	k7c4
matematiku	matematika	k1gFnSc4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
bakalářské	bakalářský	k2eAgInPc4d1
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
dobrý	dobrý	k2eAgInSc4d1
začátek	začátek	k1gInSc4
vynikající	vynikající	k2eAgFnSc2d1
akademické	akademický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
ale	ale	k9
v	v	k7c6
roce	rok	k1gInSc6
1827	#num#	k4
oženil	oženit	k5eAaPmAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
sestřenicí	sestřenice	k1gFnSc7
a	a	k8xC
uchýlil	uchýlit	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
ústraní	ústraní	k1gNnSc2
jako	jako	k8xS,k8xC
venkovský	venkovský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charlesův	Charlesův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
aktivní	aktivní	k2eAgInSc4d1
a	a	k8xC
velmi	velmi	k6eAd1
konzervativní	konzervativní	k2eAgFnSc2d1
duchovní	duchovní	k2eAgFnSc2d1
anglikánské	anglikánský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgInS
arcidiákonem	arcidiákon	k1gInSc7
Richmondu	Richmond	k1gInSc2
(	(	kIx(
<g/>
Archdeacon	Archdeacon	k1gInSc1
of	of	k?
Richmond	Richmond	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Zapojoval	zapojovat	k5eAaImAgMnS
se	se	k3xPyFc4
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
významně	významně	k6eAd1
<g/>
)	)	kIx)
do	do	k7c2
ostrých	ostrý	k2eAgFnPc2d1
náboženských	náboženský	k2eAgFnPc2d1
polemik	polemika	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
rozdělovaly	rozdělovat	k5eAaImAgFnP
anglikánskou	anglikánský	k2eAgFnSc4d1
církev	církev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
anglikánské	anglikánský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
vést	vést	k5eAaImF
i	i	k9
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladý	mladý	k1gMnSc1
Charles	Charles	k1gMnSc1
si	se	k3xPyFc3
ale	ale	k9
k	k	k7c3
otcovým	otcův	k2eAgFnPc3d1
hodnotám	hodnota	k1gFnPc3
a	a	k8xC
celé	celý	k2eAgFnSc3d1
anglikánské	anglikánský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
vytvořil	vytvořit	k5eAaPmAgInS
rozporuplný	rozporuplný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Kostelní	kostelní	k2eAgFnSc1d1
věž	věž	k1gFnSc1
v	v	k7c4
Daresbury	Daresbura	k1gFnPc4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Dodgson	Dodgson	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
na	na	k7c6
malé	malý	k2eAgFnSc6d1
faře	fara	k1gFnSc6
v	v	k7c4
Daresbury	Daresbur	k1gInPc4
v	v	k7c6
hrabství	hrabství	k1gNnSc6
Cheshire	Cheshir	k1gInSc5
jako	jako	k8xC,k8xS
nejstarší	starý	k2eAgMnSc1d3
chlapec	chlapec	k1gMnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
už	už	k9
třetí	třetí	k4xOgNnSc4
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Následovalo	následovat	k5eAaImAgNnS
osm	osm	k4xCc1
dalších	další	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
;	;	kIx,
všechny	všechen	k3xTgFnPc1
(	(	kIx(
<g/>
sedm	sedm	k4xCc4
dívek	dívka	k1gFnPc2
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
chlapci	chlapec	k1gMnPc1
<g/>
)	)	kIx)
přežily	přežít	k5eAaPmAgFnP
až	až	k9
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
bylo	být	k5eAaImAgNnS
Charlesovi	Charles	k1gMnSc6
jedenáct	jedenáct	k4xCc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
přestěhoval	přestěhovat	k5eAaPmAgMnS
se	se	k3xPyFc4
spolu	spolu	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
do	do	k7c2
vesnice	vesnice	k1gFnSc2
Croft-on-Tees	Croft-on-Teesa	k1gFnPc2
v	v	k7c6
hrabství	hrabství	k1gNnSc6
Yorkshire	Yorkshir	k1gMnSc5
<g/>
,	,	kIx,
kam	kam	k6eAd1
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
přeložen	přeložen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rugby	rugby	k1gNnSc1
School	Schoola	k1gFnPc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Během	během	k7c2
raného	raný	k2eAgNnSc2d1
období	období	k1gNnSc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
vzdělávali	vzdělávat	k5eAaImAgMnP
rodiče	rodič	k1gMnSc4
Charlese	Charles	k1gMnSc4
doma	doma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
„	„	k?
<g/>
čtenářský	čtenářský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
rodina	rodina	k1gFnSc1
uchovala	uchovat	k5eAaPmAgFnS
<g/>
,	,	kIx,
vypovídá	vypovídat	k5eAaPmIp3nS,k5eAaImIp3nS
o	o	k7c6
předčasně	předčasně	k6eAd1
rozvinutém	rozvinutý	k2eAgInSc6d1
intelektu	intelekt	k1gInSc6
–	–	k?
v	v	k7c6
sedmi	sedm	k4xCc6
letech	léto	k1gNnPc6
přečetl	přečíst	k5eAaPmAgMnS
Bunyanovu	Bunyanův	k2eAgFnSc4d1
Poutníkovu	poutníkův	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
z	z	k7c2
tohoto	tento	k3xDgInSc2
světa	svět	k1gInSc2
do	do	k7c2
světa	svět	k1gInSc2
budoucího	budoucí	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnPc7
sourozenci	sourozenec	k1gMnPc7
trpěl	trpět	k5eAaImAgMnS
koktavostí	koktavost	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
často	často	k6eAd1
ovlivňovala	ovlivňovat	k5eAaImAgFnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
společenský	společenský	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvanácti	dvanáct	k4xCc2
letech	let	k1gInPc6
byl	být	k5eAaImAgInS
poslán	poslat	k5eAaPmNgInS
na	na	k7c4
malou	malý	k2eAgFnSc4d1
soukromou	soukromý	k2eAgFnSc4d1
školu	škola	k1gFnSc4
poblíž	poblíž	k7c2
Richmondu	Richmond	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zdál	zdát	k5eAaImAgInS
být	být	k5eAaImF
šťastný	šťastný	k2eAgMnSc1d1
a	a	k8xC
spokojený	spokojený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1845	#num#	k4
ale	ale	k8xC
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
Rugby	rugby	k1gNnSc4
School	Schoola	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
očividně	očividně	k6eAd1
spokojený	spokojený	k2eAgMnSc1d1
méně	málo	k6eAd2
<g/>
,	,	kIx,
protože	protože	k8xS
po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
co	co	k9
ze	z	k7c2
školy	škola	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
<g/>
,	,	kIx,
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nemohu	moct	k5eNaImIp1nS
říci	říct	k5eAaPmF
…	…	k?
že	že	k8xS
by	by	kYmCp3nS
mě	já	k3xPp1nSc4
cokoli	cokoli	k3yInSc4
na	na	k7c6
zemi	zem	k1gFnSc6
přinutilo	přinutit	k5eAaPmAgNnS
prožít	prožít	k5eAaPmF
si	se	k3xPyFc3
ty	ten	k3xDgInPc4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
znovu	znovu	k6eAd1
…	…	k?
Mohu	moct	k5eAaImIp1nS
upřímně	upřímně	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
kdybych	kdyby	kYmCp1nS
byl	být	k5eAaImAgMnS
býval	bývat	k5eAaImAgMnS
<g/>
…	…	k?
měl	mít	k5eAaImAgInS
v	v	k7c6
noci	noc	k1gFnSc6
klid	klid	k1gInSc4
<g/>
,	,	kIx,
těžkosti	těžkost	k1gFnPc4
každodenního	každodenní	k2eAgInSc2d1
života	život	k1gInSc2
by	by	kYmCp3nP
se	se	k3xPyFc4
snášely	snášet	k5eAaImAgFnP
mnohem	mnohem	k6eAd1
lépe	dobře	k6eAd2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
žák	žák	k1gMnSc1
ale	ale	k8xC
vynikal	vynikat	k5eAaImAgMnS
s	s	k7c7
očividnou	očividný	k2eAgFnSc7d1
lehkostí	lehkost	k1gFnSc7
<g/>
.	.	kIx.
„	„	k?
<g/>
Od	od	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
jsem	být	k5eAaImIp1nS
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Rugby	rugby	k1gNnSc2
<g/>
,	,	kIx,
jsem	být	k5eAaImIp1nS
neučil	učit	k5eNaImAgMnS,k5eNaPmAgMnS
slibnějšího	slibní	k2eAgMnSc4d2
chlapce	chlapec	k1gMnSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
<g/>
“	“	k?
poznamenal	poznamenat	k5eAaPmAgMnS
R.	R.	kA
B.	B.	kA
Mayor	Mayor	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oxford	Oxford	k1gInSc1
</s>
<s>
Meadow	Meadow	k?
Building	Building	k1gInSc1
<g/>
,	,	kIx,
Christ	Christ	k1gMnSc1
Church	Church	k1gMnSc1
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rugby	rugby	k1gNnSc4
School	Schoola	k1gFnPc2
opustil	opustit	k5eAaPmAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1849	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
následuje	následovat	k5eAaImIp3nS
určitá	určitý	k2eAgFnSc1d1
přestávka	přestávka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdo	nikdo	k3yNnSc1
ale	ale	k8xC
pořádně	pořádně	k6eAd1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
vlastně	vlastně	k9
dělal	dělat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
přestávce	přestávka	k1gFnSc6
šel	jít	k5eAaImAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
1851	#num#	k4
do	do	k7c2
Oxfordu	Oxford	k1gInSc2
na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
kolej	kolej	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
navštěvoval	navštěvovat	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
–	–	k?
Christ	Christ	k1gMnSc1
Church	Church	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pouhých	pouhý	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
dnech	den	k1gInPc6
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
musel	muset	k5eAaImAgInS
jet	jet	k5eAaImF
zase	zase	k9
zpátky	zpátky	k6eAd1
-	-	kIx~
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
ve	v	k7c6
čtyřiceti	čtyřicet	k4xCc6
sedmi	sedm	k4xCc6
letech	léto	k1gNnPc6
zemřela	zemřít	k5eAaPmAgFnS
na	na	k7c4
„	„	k?
<g/>
zápal	zápal	k1gInSc4
mozku	mozek	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
meningitida	meningitida	k1gFnSc1
nebo	nebo	k8xC
mrtvice	mrtvice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
akademické	akademický	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
se	se	k3xPyFc4
střídaly	střídat	k5eAaImAgFnP
nadšené	nadšený	k2eAgFnPc1d1
naděje	naděje	k1gFnPc1
a	a	k8xC
neodolatelné	odolatelný	k2eNgInPc1d1
zábavy	zábav	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
vždy	vždy	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
tvrdě	tvrdě	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
neobyčejně	obyčejně	k6eNd1
nadaný	nadaný	k2eAgMnSc1d1
a	a	k8xC
úspěchů	úspěch	k1gInPc2
dosahoval	dosahovat	k5eAaImAgMnS
snadno	snadno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
matematický	matematický	k2eAgInSc1d1
talent	talent	k1gInSc1
mu	on	k3xPp3gMnSc3
nakonec	nakonec	k6eAd1
zajistil	zajistit	k5eAaPmAgMnS
místo	místo	k1gNnSc4
učitele	učitel	k1gMnSc2
na	na	k7c4
Christ	Christ	k1gInSc4
Church	Churcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
práce	práce	k1gFnSc1
dobře	dobře	k6eAd1
placená	placený	k2eAgFnSc1d1
<g/>
,	,	kIx,
Dodgsona	Dodgsona	k1gFnSc1
nudila	nudit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
bylo	být	k5eAaImAgNnS
starších	starý	k2eAgMnPc2d2
a	a	k8xC
bohatších	bohatý	k2eAgMnPc2d2
než	než	k8xS
on	on	k3xPp3gMnSc1
a	a	k8xC
málokoho	málokdo	k3yInSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
studium	studium	k1gNnSc1
zajímalo	zajímat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
své	svůj	k3xOyFgNnSc4
počáteční	počáteční	k2eAgNnSc4d1
znechucení	znechucení	k1gNnSc4
ale	ale	k8xC
Dodgson	Dodgson	k1gNnSc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
funkcích	funkce	k1gFnPc6
na	na	k7c4
Christ	Christ	k1gInSc4
Church	Church	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1867	#num#	k4
mu	on	k3xPp3gMnSc3
jeho	jeho	k3xOp3gFnSc6
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
kolega	kolega	k1gMnSc1
a	a	k8xC
teolog	teolog	k1gMnSc1
Henry	Henry	k1gMnSc1
Liddon	Liddon	k1gMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
společnou	společný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
hned	hned	k6eAd1
o	o	k7c4
týden	týden	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestovali	cestovat	k5eAaImAgMnP
přes	přes	k7c4
Brusel	Brusel	k1gInSc4
<g/>
,	,	kIx,
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Gdaňsk	Gdaňsk	k1gInSc1
a	a	k8xC
Kaliningrad	Kaliningrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
měsíce	měsíc	k1gInSc2
navštívili	navštívit	k5eAaPmAgMnP
Petrohrad	Petrohrad	k1gInSc4
<g/>
,	,	kIx,
Moskvu	Moskva	k1gFnSc4
<g/>
,	,	kIx,
pravoslavný	pravoslavný	k2eAgInSc4d1
klášter	klášter	k1gInSc4
Trojicko-sergijevská	Trojicko-sergijevský	k2eAgFnSc1d1
lávra	lávra	k1gFnSc1
v	v	k7c6
Sergijev	Sergijev	k1gFnSc6
Posadu	posad	k1gInSc2
a	a	k8xC
Nižnij	Nižnij	k1gFnSc2
Novgorod	Novgorod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domů	dům	k1gInPc2
se	se	k3xPyFc4
vraceli	vracet	k5eAaImAgMnP
přes	přes	k7c4
Varšavu	Varšava	k1gFnSc4
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gFnSc4
<g/>
,	,	kIx,
Drážďany	Drážďany	k1gInPc1
<g/>
,	,	kIx,
Lipsko	Lipsko	k1gNnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
a	a	k8xC
Calais	Calais	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
dojmy	dojem	k1gInPc4
z	z	k7c2
cesty	cesta	k1gFnSc2
popsal	popsat	k5eAaPmAgMnS
v	v	k7c6
cestopise	cestopis	k1gInSc6
Russian	Russian	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
publikován	publikovat	k5eAaBmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
jeho	jeho	k3xOp3gFnSc4
jedinou	jediný	k2eAgFnSc4d1
zahraniční	zahraniční	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozdější	pozdní	k2eAgNnPc1d2
léta	léto	k1gNnPc1
</s>
<s>
Během	během	k7c2
zbývajících	zbývající	k2eAgNnPc2d1
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
zůstal	zůstat	k5eAaPmAgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
život	život	k1gInSc4
i	i	k9
přes	přes	k7c4
vzrůstající	vzrůstající	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
a	a	k8xC
slávu	sláva	k1gFnSc4
téměř	téměř	k6eAd1
nezměněn	změnit	k5eNaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1881	#num#	k4
učil	učít	k5eAaPmAgInS,k5eAaImAgInS
v	v	k7c4
Christ	Christ	k1gInSc4
Church	Churcha	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
poslední	poslední	k2eAgInSc1d1
dvoudílný	dvoudílný	k2eAgInSc1d1
román	román	k1gInSc1
Sylvie	Sylvie	k1gFnSc2
a	a	k8xC
Bruno	Bruno	k1gMnSc1
byl	být	k5eAaImAgMnS
vydán	vydat	k5eAaPmNgMnS
po	po	k7c6
dílech	dílo	k1gNnPc6
v	v	k7c6
letech	léto	k1gNnPc6
1889	#num#	k4
a	a	k8xC
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
neobvyklá	obvyklý	k2eNgFnSc1d1
komplikovanost	komplikovanost	k1gFnSc1
a	a	k8xC
očividná	očividný	k2eAgFnSc1d1
zmatenost	zmatenost	k1gFnSc1
většinu	většina	k1gFnSc4
čtenářů	čtenář	k1gMnPc2
vyvedla	vyvést	k5eAaPmAgFnS
z	z	k7c2
konceptu	koncept	k1gInSc2
a	a	k8xC
román	román	k1gInSc4
neměl	mít	k5eNaImAgMnS
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hrob	hrob	k1gInSc1
Lewise	Lewise	k1gFnSc2
Carrolla	Carroll	k1gMnSc2
v	v	k7c6
Guildfordu	Guildford	k1gInSc6
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1898	#num#	k4
na	na	k7c4
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
po	po	k7c6
chřipce	chřipka	k1gFnSc6
v	v	k7c6
domě	dům	k1gInSc6
své	svůj	k3xOyFgFnSc2
sestry	sestra	k1gFnSc2
v	v	k7c6
Guildfordu	Guildford	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
pohřben	pohřbít	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literární	literární	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
</s>
<s>
Rané	raný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
</s>
<s>
Už	už	k6eAd1
v	v	k7c6
raném	raný	k2eAgInSc6d1
věku	věk	k1gInSc6
psal	psát	k5eAaImAgInS
Dodgson	Dodgson	k1gInSc1
poezii	poezie	k1gFnSc4
a	a	k8xC
povídky	povídka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
s	s	k7c7
průměrným	průměrný	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
posílal	posílat	k5eAaImAgMnS
do	do	k7c2
různých	různý	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1854	#num#	k4
<g/>
–	–	k?
<g/>
1856	#num#	k4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnPc1
díla	dílo	k1gNnPc1
objevila	objevit	k5eAaPmAgNnP
jak	jak	k6eAd1
v	v	k7c6
celostátních	celostátní	k2eAgFnPc6d1
publikacích	publikace	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
The	The	k1gFnSc1
Comix	Comix	k1gInSc1
a	a	k8xC
The	The	k1gFnSc1
Train	Traina	k1gFnPc2
<g/>
)	)	kIx)
tak	tak	k9
v	v	k7c6
menších	malý	k2eAgInPc6d2
časopisech	časopis	k1gInPc6
jako	jako	k8xC,k8xS
Whitby	Whitba	k1gFnPc1
Gazette	Gazett	k1gInSc5
a	a	k8xC
Oxford	Oxford	k1gInSc4
Critic	Critice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Dodgsonových	Dodgsonův	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
byla	být	k5eAaImAgFnS
humorná	humorný	k2eAgFnSc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
satirická	satirický	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnPc1
normy	norma	k1gFnPc1
a	a	k8xC
ambice	ambice	k1gFnPc1
byly	být	k5eAaImAgFnP
náročné	náročný	k2eAgFnPc1d1
<g/>
.	.	kIx.
„	„	k?
<g/>
Nemyslím	myslet	k5eNaImIp1nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
něco	něco	k3yInSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
opravdu	opravdu	k6eAd1
stálo	stát	k5eAaImAgNnS
za	za	k7c4
zveřejnění	zveřejnění	k1gNnSc4
v	v	k7c6
pořádné	pořádný	k2eAgFnSc6d1
publikaci	publikace	k1gFnSc6
(	(	kIx(
<g/>
do	do	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
nepočítám	počítat	k5eNaImIp1nS
Whitby	Whitba	k1gFnPc1
Gazette	Gazett	k1gInSc5
a	a	k8xC
Oxonian	Oxonian	k1gMnSc1
Advertiser	Advertiser	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
nebojím	bát	k5eNaImIp1nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
tomu	ten	k3xDgNnSc3
tak	tak	k9
bude	být	k5eAaImBp3nS
navždy	navždy	k6eAd1
<g/>
,	,	kIx,
<g/>
“	“	k?
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
v	v	k7c6
červenci	červenec	k1gInSc6
1855	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
poprvé	poprvé	k6eAd1
zveřejnil	zveřejnit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
dílo	dílo	k1gNnSc4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ho	on	k3xPp3gMnSc4
později	pozdě	k6eAd2
proslavilo	proslavit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romantická	romantický	k2eAgFnSc1d1
báseň	báseň	k1gFnSc1
nazvaná	nazvaný	k2eAgFnSc1d1
„	„	k?
<g/>
Solitude	Solitud	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Osamělost	osamělost	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
časopise	časopis	k1gInSc6
The	The	k1gMnSc1
Train	Train	k1gMnSc1
jako	jako	k8xS,k8xC
tvorba	tvorba	k1gFnSc1
„	„	k?
<g/>
Lewise	Lewise	k1gFnSc2
Carrolla	Carroll	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pseudonym	pseudonym	k1gInSc1
byl	být	k5eAaImAgInS
hříčkou	hříčka	k1gFnSc7
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
pravým	pravý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
:	:	kIx,
Lewis	Lewis	k1gFnSc2
je	být	k5eAaImIp3nS
poangličtěná	poangličtěný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
jména	jméno	k1gNnSc2
Ludovicus	Ludovicus	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
latinská	latinský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
jména	jméno	k1gNnSc2
Lutwidge	Lutwidg	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Carroll	Carroll	k1gInSc1
je	být	k5eAaImIp3nS
poangličtěná	poangličtěný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
jména	jméno	k1gNnSc2
Carolus	Carolus	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
latinská	latinský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
jména	jméno	k1gNnSc2
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
</s>
<s>
George	Georg	k1gMnSc4
Richmond	Richmond	k1gMnSc1
<g/>
:	:	kIx,
Děkan	děkan	k1gMnSc1
Henry	Henry	k1gMnSc1
George	George	k1gFnPc2
Liddell	Liddell	k1gMnSc1
<g/>
,	,	kIx,
kresba	kresba	k1gFnSc1
pastelem	pastel	k1gInSc7
<g/>
,	,	kIx,
1858	#num#	k4
</s>
<s>
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
,	,	kIx,
1856	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
kolej	kolej	k1gFnSc4
Christ	Christ	k1gMnSc1
Church	Church	k1gMnSc1
přijel	přijet	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
děkan	děkan	k1gMnSc1
Henry	Henry	k1gMnSc1
Liddell	Liddell	k1gMnSc1
a	a	k8xC
přivedl	přivést	k5eAaPmAgMnS
s	s	k7c7
sebou	se	k3xPyFc7
svou	svůj	k3xOyFgFnSc4
mladou	mladý	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnPc4
všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
významné	významný	k2eAgFnPc4d1
role	role	k1gFnPc4
v	v	k7c6
Dodgsonově	Dodgsonův	k2eAgInSc6d1
životě	život	k1gInSc6
a	a	k8xC
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
výrazně	výrazně	k6eAd1
ovlivnili	ovlivnit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnSc4
spisovatelskou	spisovatelský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgson	Dodgson	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
blízkým	blízký	k2eAgMnSc7d1
přítelem	přítel	k1gMnSc7
Liddellovy	Liddellův	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
Loriny	Lorina	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gFnPc2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
tří	tři	k4xCgFnPc2
sester	sestra	k1gFnPc2
<g/>
:	:	kIx,
Loriny	Lorina	k1gFnPc1
<g/>
,	,	kIx,
Edith	Edith	k1gInSc1
a	a	k8xC
Alice	Alice	k1gFnSc1
Liddellových	Liddellových	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
svou	svůj	k3xOyFgFnSc4
„	„	k?
<g/>
Alenku	Alenka	k1gFnSc4
<g/>
“	“	k?
odvodil	odvodit	k5eAaPmAgMnS
od	od	k7c2
Alice	Alice	k1gFnSc2
Liddellové	Liddellová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napovídá	napovídat	k5eAaBmIp3nS
tomu	ten	k3xDgMnSc3
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
verše	verš	k1gInPc1
akrostichu	akrostich	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
Za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
tvoří	tvořit	k5eAaImIp3nS
její	její	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
a	a	k8xC
v	v	k7c6
textech	text	k1gInPc6
obou	dva	k4xCgFnPc2
knih	kniha	k1gFnPc2
je	být	k5eAaImIp3nS
skryto	skryt	k2eAgNnSc1d1
množství	množství	k1gNnSc1
zběžných	zběžný	k2eAgFnPc2d1
zmínek	zmínka	k1gFnPc2
o	o	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgson	Dodgson	k1gMnSc1
sám	sám	k3xTgMnSc1
ale	ale	k8xC
v	v	k7c6
pozdějším	pozdní	k2eAgInSc6d2
životě	život	k1gInSc6
opakovaně	opakovaně	k6eAd1
popíral	popírat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
jeho	jeho	k3xOp3gFnSc1
„	„	k?
<g/>
malá	malý	k2eAgFnSc1d1
hrdinka	hrdinka	k1gFnSc1
<g/>
“	“	k?
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
skutečném	skutečný	k2eAgNnSc6d1
dítěti	dítě	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Svá	svůj	k3xOyFgNnPc4
díla	dílo	k1gNnPc4
často	často	k6eAd1
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
dívkám	dívka	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
znal	znát	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
jména	jméno	k1gNnPc4
vkládal	vkládat	k5eAaImAgMnS
do	do	k7c2
akrostichů	akrostich	k1gInPc2
na	na	k7c6
začátku	začátek	k1gInSc6
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
Gertrudy	Gertruda	k1gFnSc2
Chatawayové	Chatawayové	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
formě	forma	k1gFnSc6
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
předmluvě	předmluva	k1gFnSc6
<g/>
/	/	kIx~
<g/>
věnování	věnování	k1gNnSc1
k	k	k7c3
básni	báseň	k1gFnSc3
The	The	k1gFnSc2
Hunting	Hunting	k1gInSc1
of	of	k?
the	the	k?
Snark	Snark	k1gInSc1
(	(	kIx(
<g/>
Lovení	lovení	k1gNnSc1
Snárka	Snárk	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
přesto	přesto	k8xC
nikdy	nikdy	k6eAd1
nikdo	nikdo	k3yNnSc1
nepředpokládal	předpokládat	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
některá	některý	k3yIgFnSc1
postava	postava	k1gFnSc1
v	v	k7c6
příběhu	příběh	k1gInSc6
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
založena	založit	k5eAaPmNgFnS
na	na	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
přes	přes	k7c4
nedostatečné	dostatečný	k2eNgFnPc4d1
informace	informace	k1gFnPc4
(	(	kIx(
<g/>
Dodgsonovy	Dodgsonův	k2eAgInPc1d1
deníky	deník	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1858	#num#	k4
<g/>
–	–	k?
<g/>
1862	#num#	k4
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
)	)	kIx)
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
být	být	k5eAaImF
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
přátelství	přátelství	k1gNnSc1
s	s	k7c7
rodinou	rodina	k1gFnSc7
Liddellových	Liddellová	k1gFnPc2
bylo	být	k5eAaImAgNnS
koncem	koncem	k7c2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
důležitou	důležitý	k2eAgFnSc4d1
součástí	součást	k1gFnSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvykl	zvyknout	k5eAaPmAgMnS
si	se	k3xPyFc3
brát	brát	k5eAaImF
děti	dítě	k1gFnPc4
(	(	kIx(
<g/>
nejdřív	dříve	k6eAd3
chlapce	chlapec	k1gMnSc2
Harryho	Harry	k1gMnSc2
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
dívky	dívka	k1gFnPc1
<g/>
)	)	kIx)
na	na	k7c4
výlety	výlet	k1gInPc1
loďkou	loďka	k1gFnSc7
do	do	k7c2
blízkých	blízký	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
Nuneham	Nuneham	k1gInSc1
Courtenay	Courtenaa	k1gFnPc1
a	a	k8xC
Godstow	Godstow	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Právě	právě	k9
při	při	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
těchto	tento	k3xDgFnPc2
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
vzal	vzít	k5eAaPmAgMnS
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
Robinsonem	Robinson	k1gMnSc7
Duckworthem	Duckworth	k1gInSc7
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1862	#num#	k4
dívky	dívka	k1gFnPc4
na	na	k7c4
výlet	výlet	k1gInSc4
po	po	k7c6
Temži	Temže	k1gFnSc6
<g/>
,	,	kIx,
vymyslel	vymyslet	k5eAaPmAgMnS
Dodgson	Dodgson	k1gMnSc1
základ	základ	k1gInSc4
příběhu	příběh	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
stal	stát	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
prvním	první	k4xOgMnSc6
a	a	k8xC
největším	veliký	k2eAgInSc7d3
komerčním	komerční	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atmosféru	atmosféra	k1gFnSc4
tohoto	tento	k3xDgNnSc2
odpoledne	odpoledne	k1gNnSc2
zachycuje	zachycovat	k5eAaImIp3nS
v	v	k7c6
úvodní	úvodní	k2eAgFnSc6d1
básni	báseň	k1gFnSc6
Alenky	Alenka	k1gFnSc2
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
onom	onen	k3xDgNnSc6
„	„	k?
<g/>
golden	goldno	k1gNnPc2
afternoon	afternoon	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
zlatém	zlatý	k2eAgNnSc6d1
odpoledni	odpoledne	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívky	dívka	k1gFnPc1
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
označeny	označit	k5eAaPmNgInP
jmény	jméno	k1gNnPc7
Prima	primo	k1gNnSc2
<g/>
,	,	kIx,
Secunda	Secunda	k1gFnSc1
a	a	k8xC
Tertia	Tertia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loďka	loďka	k1gFnSc1
plula	plout	k5eAaImAgFnS
po	po	k7c6
Temži	Temže	k1gFnSc6
až	až	k9
k	k	k7c3
Godstow	Godstow	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
deníku	deník	k1gInSc6
si	se	k3xPyFc3
Carroll	Carroll	k1gMnSc1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pak	pak	k6eAd1
si	se	k3xPyFc3
dali	dát	k5eAaPmAgMnP
na	na	k7c6
břehu	břeh	k1gInSc6
čaj	čaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpět	zpět	k6eAd1
do	do	k7c2
Christ	Christ	k1gMnSc1
Church	Churcha	k1gFnPc2
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
až	až	k9
po	po	k7c4
čtvrt	čtvrt	k1gFnSc4
na	na	k7c4
devět	devět	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
muži	muž	k1gMnPc1
vzali	vzít	k5eAaPmAgMnP
dívky	dívka	k1gFnPc4
do	do	k7c2
Carrollova	Carrollův	k2eAgInSc2d1
bytu	byt	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
prohlédly	prohlédnout	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnSc4
sbírku	sbírka	k1gFnSc4
fotografií	fotografia	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
dívky	dívka	k1gFnPc1
se	se	k3xPyFc4
vrátily	vrátit	k5eAaPmAgFnP
domů	domů	k6eAd1
před	před	k7c7
devátou	devátý	k4xOgFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgInSc3
záznamu	záznam	k1gInSc3
připsal	připsat	k5eAaPmAgMnS
Dodgson	Dodgson	k1gMnSc1
o	o	k7c6
sedm	sedm	k4xCc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
poznámku	poznámka	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Při	při	k7c6
této	tento	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
jsem	být	k5eAaImIp1nS
jim	on	k3xPp3gMnPc3
vyprávěl	vyprávět	k5eAaImAgMnS
pohádku	pohádka	k1gFnSc4
o	o	k7c6
Alenčiných	Alenčin	k2eAgNnPc6d1
dobrodružstvích	dobrodružství	k1gNnPc6
v	v	k7c6
podzemí	podzemí	k1gNnSc6
<g/>
…	…	k?
<g/>
“	“	k?
Alice	Alice	k1gFnSc1
Liddellová	Liddellová	k1gFnSc1
Dodgsona	Dodgsona	k1gFnSc1
prosila	prosít	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pohádku	pohádka	k1gFnSc4
sepsal	sepsat	k5eAaPmAgMnS
a	a	k8xC
ten	ten	k3xDgInSc4
jí	jíst	k5eAaImIp3nS
nakonec	nakonec	k6eAd1
(	(	kIx(
<g/>
po	po	k7c6
mnoha	mnoho	k4c6
odkladech	odklad	k1gInPc6
<g/>
)	)	kIx)
v	v	k7c6
listopadu	listopad	k1gInSc6
1864	#num#	k4
dal	dát	k5eAaPmAgInS
ilustrovaný	ilustrovaný	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
nazvaný	nazvaný	k2eAgInSc1d1
Alenčina	Alenčin	k2eAgNnSc2d1
dobrodružství	dobrodružství	k1gNnSc3
v	v	k7c6
podzemní	podzemní	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
zárodek	zárodek	k1gInSc1
příběhu	příběh	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Carroll	Carroll	k1gInSc1
ještě	ještě	k9
později	pozdě	k6eAd2
přepracoval	přepracovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
Johna	John	k1gMnSc2
Tenniela	Tenniel	k1gMnSc2
ke	k	k7c3
knize	kniha	k1gFnSc3
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
</s>
<s>
Ještě	ještě	k6eAd1
před	před	k7c7
tím	ten	k3xDgNnSc7
četla	číst	k5eAaImAgFnS
Dodgsonův	Dodgsonův	k2eAgInSc4d1
nedokončený	dokončený	k2eNgInSc4d1
rukopis	rukopis	k1gInSc4
rodina	rodina	k1gFnSc1
přítele	přítel	k1gMnSc2
a	a	k8xC
učitele	učitel	k1gMnSc2
George	Georg	k1gMnSc2
MacDonalda	Macdonald	k1gMnSc2
a	a	k8xC
nadšení	nadšení	k1gNnSc1
MacDonaldových	Macdonaldových	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
povzbudilo	povzbudit	k5eAaPmAgNnS
Dodgsona	Dodgsona	k1gFnSc1
k	k	k7c3
uveřejnění	uveřejnění	k1gNnSc3
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
nedokončený	dokončený	k2eNgInSc1d1
rukopis	rukopis	k1gInSc1
nakladateli	nakladatel	k1gMnSc3
Macmillanovi	Macmillan	k1gMnSc3
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
zalíbil	zalíbit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
byly	být	k5eAaImAgFnP
zamítnuty	zamítnut	k2eAgInPc4d1
možné	možný	k2eAgInPc4d1
alternativní	alternativní	k2eAgInPc4d1
názvy	název	k1gInPc4
„	„	k?
<g/>
Alenka	Alenka	k1gFnSc1
mezi	mezi	k7c7
skřítky	skřítek	k1gMnPc7
<g/>
“	“	k?
(	(	kIx(
<g/>
Alice	Alice	k1gFnSc1
Among	Among	k1gInSc1
the	the	k?
Fairies	Fairies	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
Alenčina	Alenčin	k2eAgFnSc1d1
zlatá	zlatý	k2eAgFnSc1d1
hodina	hodina	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Alice	Alice	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Golden	Goldno	k1gNnPc2
Hour	Hour	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
dílo	dílo	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1865	#num#	k4
vydáno	vydat	k5eAaPmNgNnS
jako	jako	k9
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
pod	pod	k7c7
literárním	literární	k2eAgInSc7d1
pseudonymem	pseudonym	k1gInSc7
Lewis	Lewis	k1gFnSc2
Carroll	Carroll	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Dodgson	Dodgson	k1gInSc1
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgInS
asi	asi	k9
o	o	k7c4
devět	devět	k4xCc4
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Ilustrace	ilustrace	k1gFnPc1
byly	být	k5eAaImAgFnP
tentokrát	tentokrát	k6eAd1
Johna	John	k1gMnSc4
Tenniela	Tenniel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgson	Dodgson	k1gNnSc1
si	se	k3xPyFc3
očividně	očividně	k6eAd1
myslel	myslet	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vydaná	vydaný	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
potřebuje	potřebovat	k5eAaImIp3nS
dovednosti	dovednost	k1gFnSc3
profesionálního	profesionální	k2eAgMnSc2d1
umělce	umělec	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ohromný	ohromný	k2eAgInSc4d1
komerční	komerční	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
první	první	k4xOgFnSc2
knihy	kniha	k1gFnSc2
o	o	k7c6
Alence	Alenka	k1gFnSc6
v	v	k7c6
mnoha	mnoho	k4c2
ohledech	ohled	k1gInPc6
změnil	změnit	k5eAaPmAgInS
Dodgsonův	Dodgsonův	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Sláva	Sláva	k1gFnSc1
jeho	jeho	k3xOp3gFnPc2
alter	altra	k1gFnPc2
ega	ego	k1gNnSc2
„	„	k?
<g/>
Lewis	Lewis	k1gFnPc2
Carroll	Carroll	k1gMnSc1
<g/>
“	“	k?
brzy	brzy	k6eAd1
obletěla	obletět	k5eAaPmAgFnS
celý	celý	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgsona	Dodgsona	k1gFnSc1
zaplavily	zaplavit	k5eAaPmAgInP
dopisy	dopis	k1gInPc4
obdivovatelů	obdivovatel	k1gMnPc2
a	a	k8xC
někdy	někdy	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostávalo	dostávat	k5eAaImAgNnS
nevítané	vítaný	k2eNgFnSc3d1
pozornosti	pozornost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
začal	začít	k5eAaPmAgInS
vydělávat	vydělávat	k5eAaImF
značné	značný	k2eAgFnPc4d1
sumy	suma	k1gFnPc4
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
příjem	příjem	k1gInSc1
ale	ale	k9
nevyužil	využít	k5eNaPmAgInS
k	k	k7c3
opuštění	opuštění	k1gNnSc3
svého	svůj	k3xOyFgInSc2
zdánlivě	zdánlivě	k6eAd1
neoblíbeného	oblíbený	k2eNgNnSc2d1
místa	místo	k1gNnSc2
v	v	k7c4
Christ	Christ	k1gInSc4
Church	Church	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
vyšlo	vyjít	k5eAaPmAgNnS
pokračování	pokračování	k1gNnSc1
–	–	k?
Za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
a	a	k8xC
co	co	k9
tam	tam	k6eAd1
Alenka	Alenka	k1gFnSc1
našla	najít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
poněkud	poněkud	k6eAd1
temnější	temný	k2eAgFnSc6d2
atmosféře	atmosféra	k1gFnSc6
se	se	k3xPyFc4
zrcadlily	zrcadlit	k5eAaImAgFnP
změny	změna	k1gFnPc1
v	v	k7c6
Dodgsonově	Dodgsonův	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedávná	dávný	k2eNgFnSc1d1
otcova	otcův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
ho	on	k3xPp3gMnSc4
uvrhla	uvrhnout	k5eAaPmAgFnS
do	do	k7c2
deprese	deprese	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
trvat	trvat	k5eAaImF
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
na	na	k7c6
obálce	obálka	k1gFnSc6
prvního	první	k4xOgNnSc2
vydání	vydání	k1gNnSc2
The	The	k1gFnPc2
Nursery	Nursera	k1gFnSc2
"	"	kIx"
<g/>
Alice	Alice	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1889	#num#	k4
vydal	vydat	k5eAaPmAgInS
Dodgson	Dodgson	k1gInSc1
zkrácenou	zkrácený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
Alenky	Alenka	k1gFnSc2
The	The	k1gFnSc2
Nursery	Nursera	k1gFnSc2
"	"	kIx"
<g/>
Alice	Alice	k1gFnSc2
<g/>
"	"	kIx"
přizpůsobenou	přizpůsobený	k2eAgFnSc4d1
pro	pro	k7c4
mladší	mladý	k2eAgMnPc4d2
čtenáře	čtenář	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Hunting	Hunting	k1gInSc1
of	of	k?
the	the	k?
Snark	Snark	k1gInSc1
(	(	kIx(
<g/>
Lovení	lovení	k1gNnSc1
Snárka	Snárk	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
Dodgson	Dodgson	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
poslední	poslední	k2eAgNnSc4d1
velké	velký	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
knihu	kniha	k1gFnSc4
The	The	k1gFnSc2
Hunting	Hunting	k1gInSc1
of	of	k?
the	the	k?
Snark	Snark	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
své	svůj	k3xOyFgFnPc4
dětské	dětský	k2eAgFnPc4d1
přítelkyni	přítelkyně	k1gFnSc4
Gertrude	Gertrud	k1gInSc5
Chatawayové	Chatawayové	k2eAgInSc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
fantaskní	fantaskní	k2eAgFnSc1d1
"	"	kIx"
<g/>
nesmyslná	smyslný	k2eNgFnSc1d1
<g/>
"	"	kIx"
báseň	báseň	k1gFnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
dobrodružstvích	dobrodružství	k1gNnPc6
bizarní	bizarní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
různorodých	různorodý	k2eAgMnPc2d1
neschopných	schopný	k2eNgMnPc2d1
tvorů	tvor	k1gMnPc2
a	a	k8xC
jednoho	jeden	k4xCgMnSc2
bobra	bobr	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
hledat	hledat	k5eAaImF
zmíněného	zmíněný	k2eAgInSc2d1
Snárka	Snárk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malíř	malíř	k1gMnSc1
Dante	Dante	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Rossetti	Rossetti	k1gNnPc2
byl	být	k5eAaImAgMnS
údajně	údajně	k6eAd1
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
báseň	báseň	k1gFnSc1
je	být	k5eAaImIp3nS
o	o	k7c6
něm.	něm.	k?
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotografie	fotografie	k1gFnSc1
1856-1880	1856-1880	k4
</s>
<s>
Autoportrét	autoportrét	k1gInSc1
Lewise	Lewise	k1gFnSc2
Carrolla	Carroll	k1gMnSc2
<g/>
,	,	kIx,
cca	cca	kA
1856	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
se	se	k3xPyFc4
Dodgson	Dodgson	k1gMnSc1
začal	začít	k5eAaPmAgMnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
nové	nový	k2eAgFnSc3d1
formě	forma	k1gFnSc3
umění	umění	k1gNnSc2
–	–	k?
fotografování	fotografování	k1gNnSc2
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
pod	pod	k7c7
vlivem	vliv	k1gInSc7
svého	svůj	k3xOyFgMnSc2
strýce	strýc	k1gMnSc2
Roberta	Robert	k1gMnSc2
Wilfreda	Wilfred	k1gMnSc2
Skeffingtona	Skeffington	k1gMnSc2
Lutwidga	Lutwidg	k1gMnSc2
a	a	k8xC
později	pozdě	k6eAd2
svého	svůj	k3xOyFgMnSc4
oxfordského	oxfordský	k2eAgMnSc4d1
přítele	přítel	k1gMnSc4
Reginalda	Reginald	k1gMnSc4
Southeyho	Southey	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Brzy	brzy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
tomto	tento	k3xDgNnSc6
umění	umění	k1gNnSc6
velmi	velmi	k6eAd1
dobrý	dobrý	k2eAgMnSc1d1
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
známým	známý	k2eAgMnSc7d1
fotografem	fotograf	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgInPc6
letech	let	k1gInPc6
si	se	k3xPyFc3
zřejmě	zřejmě	k6eAd1
i	i	k9
pohrával	pohrávat	k5eAaImAgMnS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
fotografováním	fotografování	k1gNnSc7
bude	být	k5eAaImBp3nS
živit	živit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studie	studie	k1gFnSc1
Rogera	Roger	k1gMnSc2
Taylora	Taylor	k1gMnSc2
a	a	k8xC
Edwarda	Edward	k1gMnSc2
Wakelinga	Wakeling	k1gMnSc2
podává	podávat	k5eAaImIp3nS
vyčerpávající	vyčerpávající	k2eAgInSc4d1
výčet	výčet	k1gInSc4
všech	všecek	k3xTgFnPc2
dochovaných	dochovaný	k2eAgFnPc2d1
fotografií	fotografia	k1gFnPc2
a	a	k8xC
Taylor	Taylor	k1gMnSc1
odhadl	odhadnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
něco	něco	k3yInSc4
přes	přes	k7c4
polovinu	polovina	k1gFnSc4
jeho	jeho	k3xOp3gNnPc2
dochovaných	dochovaný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
zachycuje	zachycovat	k5eAaImIp3nS
mladé	mladý	k2eAgFnPc4d1
dívky	dívka	k1gFnPc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
asi	asi	k9
60	#num#	k4
%	%	kIx~
původního	původní	k2eAgNnSc2d1
fotografického	fotografický	k2eAgNnSc2d1
portfolia	portfolio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
mnoho	mnoho	k4c1
svých	svůj	k3xOyFgFnPc2
fotografií	fotografia	k1gFnPc2
dětí	dítě	k1gFnPc2
použil	použít	k5eAaPmAgInS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
svými	svůj	k3xOyFgNnPc7
literárními	literární	k2eAgNnPc7d1
díly	dílo	k1gNnPc7
jako	jako	k9
ilustrace	ilustrace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandra	Alexandra	k1gFnSc1
Kitchinová	Kitchinová	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
Xie	Xie	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vyslovováno	vyslovován	k2eAgNnSc4d1
"	"	kIx"
<g/>
Iksi	Iksi	k1gNnSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
oblíbeným	oblíbený	k2eAgInSc7d1
objektem	objekt	k1gInSc7
pro	pro	k7c4
fotografování	fotografování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1869	#num#	k4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
fotografování	fotografování	k1gNnSc4
nechal	nechat	k5eAaPmAgMnS
<g/>
,	,	kIx,
ji	on	k3xPp3gFnSc4
Dodgson	Dodgson	k1gInSc1
vyfotografoval	vyfotografovat	k5eAaPmAgInS
alespoň	alespoň	k9
padesátkrát	padesátkrát	k6eAd1
<g/>
,	,	kIx,
naposled	naposled	k6eAd1
těsně	těsně	k6eAd1
před	před	k7c7
jejími	její	k3xOp3gFnPc7
šestnáctými	šestnáctý	k4xOgFnPc7
narozeninami	narozeniny	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
původního	původní	k2eAgNnSc2d1
portfolia	portfolio	k1gNnSc2
ale	ale	k8xC
zbyla	zbýt	k5eAaPmAgFnS
méně	málo	k6eAd2
než	než	k8xS
třetina	třetina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgson	Dodgson	k1gMnSc1
také	také	k9
zachytil	zachytit	k5eAaPmAgMnS
mnoho	mnoho	k4c4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
chlapců	chlapec	k1gMnPc2
a	a	k8xC
krajin	krajina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
objekty	objekt	k1gInPc7
byly	být	k5eAaImAgFnP
také	také	k9
kostry	kostra	k1gFnPc1
<g/>
,	,	kIx,
loutky	loutka	k1gFnPc1
<g/>
,	,	kIx,
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
sochy	socha	k1gFnPc1
<g/>
,	,	kIx,
obrazy	obraz	k1gInPc1
a	a	k8xC
stromy	strom	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc3
skici	skica	k1gFnSc3
nahých	nahý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
byly	být	k5eAaImAgInP
dlouho	dlouho	k6eAd1
považovány	považován	k2eAgInPc1d1
za	za	k7c4
ztracené	ztracený	k2eAgNnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
šest	šest	k4xCc1
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
po	po	k7c6
čase	čas	k1gInSc6
našlo	najít	k5eAaPmAgNnS
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
čtyři	čtyři	k4xCgFnPc1
byly	být	k5eAaImAgFnP
uveřejněny	uveřejnit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Xie	Xie	k?
Kitchinová	Kitchinová	k1gFnSc1
jako	jako	k8xC,k8xS
obchodnice	obchodnice	k1gFnSc1
s	s	k7c7
čajem	čaj	k1gInSc7
<g/>
,	,	kIx,
1873	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
provozoval	provozovat	k5eAaImAgMnS
Carroll	Carroll	k1gMnSc1
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
fotografické	fotografický	k2eAgNnSc4d1
studio	studio	k1gNnSc4
v	v	k7c6
podkrovní	podkrovní	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
<g/>
,	,	kIx,
vybavené	vybavený	k2eAgFnPc4d1
a	a	k8xC
navržené	navržený	k2eAgFnPc4d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
fotografovat	fotografovat	k5eAaImF
i	i	k9
za	za	k7c2
nepříznivého	příznivý	k2eNgNnSc2d1
počasí	počasí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
mnoha	mnoho	k4c2
hračkám	hračka	k1gFnPc3
a	a	k8xC
kostýmům	kostým	k1gInPc3
pronajatým	pronajatý	k2eAgMnPc3d1
od	od	k7c2
divadla	divadlo	k1gNnSc2
Drury	Drura	k1gFnSc2
Lane	Lanus	k1gMnSc5
Theatre	Theatr	k1gMnSc5
(	(	kIx(
<g/>
i	i	k9
když	když	k8xS
některé	některý	k3yIgInPc4
kostýmy	kostým	k1gInPc4
navrhl	navrhnout	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
některé	některý	k3yIgFnPc1
si	se	k3xPyFc3
půjčoval	půjčovat	k5eAaImAgMnS
z	z	k7c2
Ashmoleova	Ashmoleův	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
skleněný	skleněný	k2eAgInSc1d1
pavilon	pavilon	k1gInSc1
rájem	ráj	k1gInSc7
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgMnPc4
malé	malý	k2eAgMnPc4d1
kamarády	kamarád	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
jako	jako	k8xS,k8xC
fotomodely	fotomodel	k1gMnPc4
<g/>
,	,	kIx,
Carroll	Carroll	k1gMnSc1
nazýval	nazývat	k5eAaImAgMnS
„	„	k?
<g/>
dětský	dětský	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
vědci	vědec	k1gMnPc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
fráze	fráze	k1gFnSc1
spíše	spíše	k9
než	než	k8xS
věk	věk	k1gInSc4
naznačovala	naznačovat	k5eAaImAgFnS
typ	typ	k1gInSc4
vztahu	vztah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
viktoriánské	viktoriánský	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
výraz	výraz	k1gInSc1
velmi	velmi	k6eAd1
rozšířený	rozšířený	k2eAgInSc1d1
a	a	k8xC
někdy	někdy	k6eAd1
odrážel	odrážet	k5eAaImAgMnS
povahu	povaha	k1gFnSc4
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
různého	různý	k2eAgNnSc2d1
sociálního	sociální	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
jiné	jiný	k2eAgFnPc1d1
interpretace	interpretace	k1gFnPc1
tohoto	tento	k3xDgInSc2
pojmu	pojem	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgInPc1
portréty	portrét	k1gInPc1
dětí	dítě	k1gFnPc2
vytvořené	vytvořený	k2eAgFnPc1d1
Carrollem	Carrollo	k1gNnSc7
byly	být	k5eAaImAgFnP
podle	podle	k7c2
jeho	jeho	k3xOp3gFnSc2
vůle	vůle	k1gFnSc2
distribuovány	distribuován	k2eAgInPc1d1
rodinám	rodina	k1gFnPc3
modelů	model	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carrollovy	Carrollův	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
byly	být	k5eAaImAgFnP
rozptýleny	rozptýlit	k5eAaPmNgFnP
a	a	k8xC
zapomenuty	zapomenut	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zajímat	zajímat	k5eAaImF
o	o	k7c4
viktoriánskou	viktoriánský	k2eAgFnSc4d1
fotografii	fotografia	k1gFnSc4
sběratel	sběratel	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
Helmut	Helmut	k1gMnSc1
Gernsheim	Gernsheim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
o	o	k7c6
tom	ten	k3xDgNnSc6
nebyla	být	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
speciální	speciální	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýnský	londýnský	k2eAgMnSc1d1
antikvář	antikvář	k1gMnSc1
a	a	k8xC
knihkupec	knihkupec	k1gMnSc1
nabídl	nabídnout	k5eAaPmAgMnS
Gernsheimovi	Gernsheim	k1gMnSc3
album	album	k1gNnSc4
dětských	dětský	k2eAgFnPc2d1
fotografií	fotografia	k1gFnPc2
(	(	kIx(
<g/>
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
fotografie	fotografia	k1gFnPc1
byly	být	k5eAaImAgFnP
„	„	k?
<g/>
extrahovány	extrahovat	k5eAaBmNgInP
z	z	k7c2
hromady	hromada	k1gFnSc2
knih	kniha	k1gFnPc2
ve	v	k7c6
sklepě	sklep	k1gInSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
údajně	údajně	k6eAd1
Lewis	Lewis	k1gFnSc4
Carroll	Carrolla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyly	být	k5eNaImAgInP
o	o	k7c6
tom	ten	k3xDgNnSc6
žádné	žádný	k3yNgInPc4
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
Gernsheim	Gernsheim	k1gInSc1
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
porovnal	porovnat	k5eAaPmAgInS
podpisy	podpis	k1gInPc4
fotografií	fotografia	k1gFnPc2
se	s	k7c7
slavnými	slavný	k2eAgInPc7d1
Carrollovými	Carrollův	k2eAgInPc7d1
autogramy	autogram	k1gInPc7
a	a	k8xC
ujistil	ujistit	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
skutečné	skutečný	k2eAgInPc1d1
patří	patřit	k5eAaImIp3nP
spisovateli	spisovatel	k1gMnSc6
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fotografování	fotografování	k1gNnSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
také	také	k6eAd1
bylo	být	k5eAaImAgNnS
vstupní	vstupní	k2eAgFnSc7d1
branou	brána	k1gFnSc7
do	do	k7c2
vyšší	vysoký	k2eAgFnSc2d2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
svého	svůj	k3xOyFgNnSc2
nejplodnějšího	plodný	k2eAgNnSc2d3
období	období	k1gNnSc2
pořídil	pořídit	k5eAaPmAgInS
portréty	portrét	k1gInPc4
významných	významný	k2eAgMnPc2d1
osobnosti	osobnost	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Johna	John	k1gMnSc4
Everetta	Everetto	k1gNnSc2
Millaise	Millaise	k1gFnSc2
<g/>
,	,	kIx,
Ellen	Ellen	k1gInSc4
Terryové	Terryová	k1gFnSc2
<g/>
,	,	kIx,
Dante	Dante	k1gMnSc1
Gabriela	Gabriela	k1gFnSc1
Rossettiho	Rossetti	k1gMnSc2
<g/>
,	,	kIx,
fotografky	fotografka	k1gFnSc2
Julie	Julie	k1gFnSc2
Margarety	Margareta	k1gFnSc2
Cameronové	Cameronový	k2eAgFnSc2d1
<g/>
,	,	kIx,
Michaela	Michael	k1gMnSc2
Faradaye	Faraday	k1gMnSc2
a	a	k8xC
Lorda	lord	k1gMnSc2
Alfreda	Alfred	k1gMnSc2
Tennysona	Tennyson	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dodgson	Dodgson	k1gInSc1
s	s	k7c7
fotografováním	fotografování	k1gNnSc7
náhle	náhle	k6eAd1
přestal	přestat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
24	#num#	k4
let	léto	k1gNnPc2
toto	tento	k3xDgNnSc4
médium	médium	k1gNnSc4
mistrovsky	mistrovsky	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgInS
v	v	k7c4
Christ	Christ	k1gInSc4
Church	Church	k1gMnSc1
vlastní	vlastnit	k5eAaImIp3nS
studio	studio	k1gNnSc4
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgInS
asi	asi	k9
tři	tři	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čas	čas	k1gInSc4
a	a	k8xC
záměrné	záměrný	k2eAgNnSc4d1
ničení	ničení	k1gNnSc4
přečkalo	přečkat	k5eAaPmAgNnS
méně	málo	k6eAd2
než	než	k8xS
tisíc	tisíc	k4xCgInSc4
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
<g/>
,	,	kIx,
proč	proč	k6eAd1
nakonec	nakonec	k6eAd1
s	s	k7c7
fotografováním	fotografování	k1gNnSc7
přestal	přestat	k5eAaPmAgMnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
časová	časový	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
udržování	udržování	k1gNnSc2
studia	studio	k1gNnSc2
v	v	k7c6
chodu	chod	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gInPc1
snímky	snímek	k1gInPc1
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgInP
ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
Art	Art	k1gFnSc2
Institute	institut	k1gInSc5
of	of	k?
Chicago	Chicago	k1gNnSc1
v	v	k7c6
Illinois	Illinois	k1gFnPc6
<g/>
,	,	kIx,
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
prominentů	prominent	k1gMnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
</s>
<s>
Rodina	rodina	k1gFnSc1
Rossettiových	Rossettiový	k2eAgMnPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1865	#num#	k4
<g/>
;	;	kIx,
zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Dante	Dante	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Rossetti	Rossetti	k1gNnPc2
<g/>
,	,	kIx,
Christina	Christin	k1gMnSc4
<g/>
,	,	kIx,
Mrs	Mrs	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rossetti	Rossetti	k1gNnPc1
a	a	k8xC
William	William	k1gInSc1
Michael	Michael	k1gMnSc1
</s>
<s>
Samuel	Samuel	k1gMnSc1
Wilberforce	Wilberforka	k1gFnSc3
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
</s>
<s>
Alice	Alice	k1gFnSc1
Liddellová	Liddellová	k1gFnSc1
<g/>
,	,	kIx,
1860	#num#	k4
</s>
<s>
Beatrice	Beatrice	k1gFnSc1
Hatch	Hatcha	k1gFnPc2
<g/>
,	,	kIx,
kolorováno	kolorován	k2eAgNnSc1d1
Annou	Anna	k1gFnSc7
Lydií	Lydie	k1gFnSc7
Bondovou	Bondová	k1gFnSc7
<g/>
,	,	kIx,
1873	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
:	:	kIx,
portrét	portrét	k1gInSc1
Alice	Alice	k1gFnSc2
Liddellové	Liddellová	k1gFnSc2
<g/>
,	,	kIx,
1858	#num#	k4
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Jiří	Jiří	k1gMnSc1
a	a	k8xC
drak	drak	k1gMnSc1
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1875	#num#	k4
</s>
<s>
Fotografie	fotografia	k1gFnPc1
Červené	Červené	k2eAgFnSc2d1
Karkulky	Karkulka	k1gFnSc2
<g/>
,	,	kIx,
1857	#num#	k4
</s>
<s>
Reginald	Reginald	k1gInSc1
Southey	Southea	k1gFnSc2
a	a	k8xC
kostry	kostra	k1gFnSc2
<g/>
,	,	kIx,
1857	#num#	k4
</s>
<s>
Povaha	povaha	k1gFnSc1
a	a	k8xC
vzhled	vzhled	k1gInSc1
</s>
<s>
Fyzický	fyzický	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
</s>
<s>
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
,	,	kIx,
autoportrét	autoportrét	k1gInSc1
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Charles	Charles	k1gMnSc1
Dodgson	Dodgson	k1gMnSc1
byl	být	k5eAaImAgMnS
asi	asi	k9
180	#num#	k4
cm	cm	kA
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
štíhlý	štíhlý	k2eAgMnSc1d1
a	a	k8xC
hezký	hezký	k2eAgMnSc1d1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
vlnité	vlnitý	k2eAgInPc4d1
hnědé	hnědý	k2eAgInPc4d1
vlasy	vlas	k1gInPc4
a	a	k8xC
modré	modrý	k2eAgFnPc4d1
oči	oko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
popisován	popisovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
trochu	trochu	k6eAd1
asymetrický	asymetrický	k2eAgMnSc1d1
a	a	k8xC
říkalo	říkat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
toporně	toporně	k6eAd1
a	a	k8xC
těžkopádně	těžkopádně	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
toto	tento	k3xDgNnSc1
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
zapříčiněno	zapříčiněn	k2eAgNnSc1d1
nějakým	nějaký	k3yIgInSc7
úrazem	úraz	k1gInSc7
kolena	koleno	k1gNnSc2
utrpěným	utrpěný	k2eAgInSc7d1
ve	v	k7c6
středním	střední	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
ještě	ještě	k6eAd1
velmi	velmi	k6eAd1
malé	malý	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
prodělal	prodělat	k5eAaPmAgInS
horečku	horečka	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
následkem	následek	k1gInSc7
ohluchl	ohluchnout	k5eAaPmAgMnS
na	na	k7c4
jedno	jeden	k4xCgNnSc4
ucho	ucho	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sedmnácti	sedmnáct	k4xCc2
letech	let	k1gInPc6
měl	mít	k5eAaImAgInS
několik	několik	k4yIc4
záchvatů	záchvat	k1gInPc2
černého	černý	k2eAgInSc2d1
kašle	kašel	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
nejspíše	nejspíše	k9
způsobilo	způsobit	k5eAaPmAgNnS
chronickou	chronický	k2eAgFnSc4d1
slabost	slabost	k1gFnSc4
hrudníku	hrudník	k1gInSc2
v	v	k7c6
pozdějších	pozdní	k2eAgInPc6d2
letech	let	k1gInPc6
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
zjevná	zjevný	k2eAgFnSc1d1
vada	vada	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
přetrvala	přetrvat	k5eAaPmAgFnS
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
čemu	co	k3yQnSc3,k3yInSc3,k3yRnSc3
říkal	říkat	k5eAaImAgMnS
svoje	svůj	k3xOyFgInPc4
„	„	k?
<g/>
rozpaky	rozpak	k1gInPc4
<g/>
“	“	k?
-	-	kIx~
koktavost	koktavost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
projevovat	projevovat	k5eAaImF
v	v	k7c6
raném	raný	k2eAgNnSc6d1
dětství	dětství	k1gNnSc6
a	a	k8xC
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ho	on	k3xPp3gMnSc4
sužovala	sužovat	k5eAaImAgFnS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koktavost	koktavost	k1gFnSc1
</s>
<s>
Koktavost	koktavost	k1gFnSc1
vždy	vždy	k6eAd1
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
lidem	člověk	k1gMnPc3
při	při	k7c6
vyslovení	vyslovení	k1gNnSc6
jména	jméno	k1gNnSc2
"	"	kIx"
<g/>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
<g/>
"	"	kIx"
vybavilo	vybavit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panuje	panovat	k5eAaImIp3nS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
koktal	koktat	k5eAaImAgMnS
pouze	pouze	k6eAd1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
dospělých	dospělí	k1gMnPc2
a	a	k8xC
s	s	k7c7
dětmi	dítě	k1gFnPc7
mluvil	mluvit	k5eAaImAgMnS
plynně	plynně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
podporu	podpora	k1gFnSc4
této	tento	k3xDgFnSc2
myšlenky	myšlenka	k1gFnSc2
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
důkazy	důkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodně	hodně	k6eAd1
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
ho	on	k3xPp3gMnSc4
znalo	znát	k5eAaImAgNnS
<g/>
,	,	kIx,
si	se	k3xPyFc3
jeho	jeho	k3xOp3gFnSc4
koktavost	koktavost	k1gFnSc4
pamatovalo	pamatovat	k5eAaImAgNnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
hodně	hodně	k6eAd1
dospělých	dospělí	k1gMnPc2
si	se	k3xPyFc3
jí	on	k3xPp3gFnSc3
nevšimlo	všimnout	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nějakého	nějaký	k3yIgInSc2
důvodu	důvod	k1gInSc2
mizela	mizet	k5eAaImAgFnS
a	a	k8xC
zase	zase	k9
se	se	k3xPyFc4
objevovala	objevovat	k5eAaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgInS
to	ten	k3xDgNnSc1
projev	projev	k1gInSc1
strachu	strach	k1gInSc2
ze	z	k7c2
světa	svět	k1gInSc2
dospělých	dospělí	k1gMnPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
často	často	k6eAd1
říká	říkat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgson	Dodgson	k1gMnSc1
sám	sám	k3xTgMnSc1
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
asi	asi	k9
uvědomoval	uvědomovat	k5eAaImAgMnS
mnohem	mnohem	k6eAd1
intenzivněji	intenzivně	k6eAd2
než	než	k8xS
většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
obtíží	obtíž	k1gFnPc2
vyslovit	vyslovit	k5eAaPmF
své	své	k1gNnSc4
příjmení	příjmení	k1gNnSc2
karikoval	karikovat	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
jako	jako	k9
Doda	Doda	k1gFnSc1
v	v	k7c6
Alenčiných	Alenčin	k2eAgNnPc6d1
dobrodružstvích	dobrodružství	k1gNnPc6
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
těch	ten	k3xDgInPc2
"	"	kIx"
<g/>
faktů	fakt	k1gInPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
opakují	opakovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
přesvědčivé	přesvědčivý	k2eAgInPc4d1
podklady	podklad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opravdu	opravdu	k6eAd1
o	o	k7c4
sobě	se	k3xPyFc3
mluvil	mluvit	k5eAaImAgMnS
jako	jako	k9
o	o	k7c6
dodovi	doda	k1gMnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
to	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
souvislost	souvislost	k1gFnSc4
s	s	k7c7
koktavostí	koktavost	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jen	jen	k9
spekulace	spekulace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobnost	osobnost	k1gFnSc1
</s>
<s>
I	i	k9
když	když	k8xS
Dodgsona	Dodgsona	k1gFnSc1
koktavost	koktavost	k1gFnSc1
trápila	trápit	k5eAaImAgFnS
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgFnS
tak	tak	k6eAd1
rušivá	rušivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
nemohl	moct	k5eNaImAgMnS
využít	využít	k5eAaPmF
své	svůj	k3xOyFgFnPc4
jiné	jiný	k2eAgFnPc4d1
přednosti	přednost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
lidé	člověk	k1gMnPc1
běžně	běžně	k6eAd1
zařizovali	zařizovat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
zábavu	zábava	k1gFnSc4
a	a	k8xC
zpěv	zpěv	k1gInSc4
a	a	k8xC
recitace	recitace	k1gFnSc1
byly	být	k5eAaImAgFnP
požadovanými	požadovaný	k2eAgFnPc7d1
společenskými	společenský	k2eAgFnPc7d1
dovednostmi	dovednost	k1gFnPc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Dodgson	Dodgson	k1gMnSc1
dobrým	dobrý	k2eAgMnSc7d1
bavičem	bavič	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpíval	zpívat	k5eAaImAgMnS
docela	docela	k6eAd1
přijatelně	přijatelně	k6eAd1
a	a	k8xC
před	před	k7c7
publikem	publikum	k1gNnSc7
se	se	k3xPyFc4
nebál	bát	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
mistr	mistr	k1gMnSc1
v	v	k7c4
imitování	imitování	k1gNnSc4
a	a	k8xC
vyprávění	vyprávění	k1gNnSc4
a	a	k8xC
údajně	údajně	k6eAd1
i	i	k9
docela	docela	k6eAd1
dobrý	dobrý	k2eAgInSc1d1
v	v	k7c6
šarádách	šaráda	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dodgson	Dodgson	k1gMnSc1
měl	mít	k5eAaImAgMnS
také	také	k9
jisté	jistý	k2eAgFnPc4d1
společenské	společenský	k2eAgFnPc4d1
ambice	ambice	k1gFnPc4
a	a	k8xC
horlivě	horlivě	k6eAd1
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c6
světě	svět	k1gInSc6
zanechal	zanechat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
otisk	otisk	k1gInSc4
jako	jako	k9
spisovatel	spisovatel	k1gMnSc1
nebo	nebo	k8xC
umělec	umělec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
mezi	mezi	k7c7
časně	časně	k6eAd1
vydanými	vydaný	k2eAgFnPc7d1
pracemi	práce	k1gFnPc7
a	a	k8xC
úspěchem	úspěch	k1gInSc7
knih	kniha	k1gFnPc2
o	o	k7c6
Alence	Alenka	k1gFnSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
pohybovat	pohybovat	k5eAaImF
v	v	k7c6
prerafaelitských	prerafaelitský	k2eAgInPc6d1
společenských	společenský	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
akademická	akademický	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
zamýšlena	zamýšlet	k5eAaImNgFnS
jen	jen	k9
jako	jako	k9
prozatímní	prozatímní	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
mnohem	mnohem	k6eAd1
zajímavějším	zajímavý	k2eAgInPc3d2
úspěchům	úspěch	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johna	John	k1gMnSc2
Ruskina	Ruskin	k1gMnSc2
poprvé	poprvé	k6eAd1
potkal	potkat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
a	a	k8xC
spřátelil	spřátelit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgInS
blízký	blízký	k2eAgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
Dante	Dante	k1gMnSc1
Gabrielem	Gabriel	k1gMnSc7
Rossettim	Rossetti	k1gNnSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
rodinou	rodina	k1gFnSc7
a	a	k8xC
z	z	k7c2
ostatních	ostatní	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
znal	znát	k5eAaImAgMnS
také	také	k9
Williama	William	k1gMnSc4
Holmana	Holman	k1gMnSc4
<g/>
,	,	kIx,
Johna	John	k1gMnSc4
Everetta	Everett	k1gInSc2
Millaise	Millaise	k1gFnSc1
a	a	k8xC
Arthura	Arthura	k1gFnSc1
Hughese	Hughese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobře	dobře	k6eAd1
také	také	k9
znal	znát	k5eAaImAgMnS
autora	autor	k1gMnSc4
pohádek	pohádka	k1gFnPc2
George	Georg	k1gMnSc4
MacDonalda	Macdonald	k1gMnSc4
-	-	kIx~
právě	právě	k9
nadšená	nadšený	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
malých	malý	k2eAgFnPc2d1
MacDonaldových	Macdonaldových	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
na	na	k7c4
Alenku	Alenka	k1gFnSc4
přiměla	přimět	k5eAaPmAgFnS
Dodgsona	Dodgsona	k1gFnSc1
dílo	dílo	k1gNnSc4
vydat	vydat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Polemiky	polemika	k1gFnPc1
a	a	k8xC
záhady	záhada	k1gFnPc1
</s>
<s>
Kněžství	kněžství	k1gNnSc1
</s>
<s>
Dodgson	Dodgson	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
raného	raný	k2eAgInSc2d1
věku	věk	k1gInSc2
vyučován	vyučovat	k5eAaImNgInS
pro	pro	k7c4
službu	služba	k1gFnSc4
v	v	k7c6
anglikánské	anglikánský	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
a	a	k8xC
očekávalo	očekávat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
Christ	Christ	k1gMnSc1
Church	Church	k1gMnSc1
bude	být	k5eAaImBp3nS
vysvěcen	vysvěcen	k2eAgMnSc1d1
na	na	k7c4
kněze	kněz	k1gMnPc4
do	do	k7c2
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
po	po	k7c6
získání	získání	k1gNnSc6
univerzitního	univerzitní	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodgson	Dodgson	k1gMnSc1
se	se	k3xPyFc4
ale	ale	k8xC
očividně	očividně	k6eAd1
začal	začít	k5eAaPmAgMnS
zdráhat	zdráhat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
to	ten	k3xDgNnSc4
odkládal	odkládat	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1861	#num#	k4
vysvěcen	vysvětit	k5eAaPmNgMnS
na	na	k7c4
jáhna	jáhen	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ale	ale	k9
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
měl	mít	k5eAaImAgInS
postoupit	postoupit	k5eAaPmF
k	k	k7c3
vysvěcení	vysvěcení	k1gNnSc3
na	na	k7c4
kněze	kněz	k1gMnPc4
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgMnS
děkana	děkan	k1gMnSc4
o	o	k7c4
povolení	povolení	k1gNnSc4
v	v	k7c6
kněžské	kněžský	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
nepokračovat	pokračovat	k5eNaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
proti	proti	k7c3
pravidlům	pravidlo	k1gNnPc3
koleje	kolej	k1gFnSc2
a	a	k8xC
děkan	děkan	k1gMnSc1
Liddell	Liddell	k1gMnSc1
mu	on	k3xPp3gMnSc3
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
odmítne	odmítnout	k5eAaPmIp3nS
být	být	k5eAaImF
vysvěcen	vysvětit	k5eAaPmNgMnS
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
velmi	velmi	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
místa	místo	k1gNnSc2
odejít	odejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekl	říct	k5eAaPmAgMnS
Dodgsonovi	Dodgson	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
poradit	poradit	k5eAaPmF
s	s	k7c7
kolejní	kolejní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
téměř	téměř	k6eAd1
nepochybně	pochybně	k6eNd1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vyloučení	vyloučení	k1gNnSc3
z	z	k7c2
koleje	kolej	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkan	děkan	k1gMnSc1
Liddell	Liddell	k1gMnSc1
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
ale	ale	k9
nakonec	nakonec	k6eAd1
rozmyslel	rozmyslet	k5eAaPmAgInS
a	a	k8xC
dovolil	dovolit	k5eAaPmAgInS
Dodgsonovi	Dodgson	k1gMnSc3
zůstat	zůstat	k5eAaPmF
i	i	k9
přes	přes	k7c4
porušení	porušení	k1gNnSc4
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Dodgson	Dodgson	k1gMnSc1
se	s	k7c7
knězem	kněz	k1gMnSc7
nikdy	nikdy	k6eAd1
nestal	stát	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
přesvědčivé	přesvědčivý	k2eAgInPc4d1
podklady	podklad	k1gInPc4
pro	pro	k7c4
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
Dodgson	Dodgson	k1gMnSc1
kněžství	kněžství	k1gNnSc2
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
předpokládají	předpokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gFnSc3
koktavosti	koktavost	k1gFnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
kázání	kázání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Robin	robin	k2eAgInSc1d1
Wilson	Wilson	k1gInSc1
však	však	k9
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
"	"	kIx"
<g/>
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
in	in	k?
Numberland	Numberland	k1gInSc1
<g/>
:	:	kIx,
His	his	k1gNnSc1
Fantastical	Fantastical	k1gFnSc2
Mathematical	Mathematical	k1gMnSc2
Logical	Logical	k1gMnSc2
Life	Lif	k1gMnSc2
<g/>
"	"	kIx"
cituje	citovat	k5eAaBmIp3nS
Dodgsonovy	Dodgsonův	k2eAgInPc4d1
dopisy	dopis	k1gInPc4
popisující	popisující	k2eAgFnSc2d1
obtíže	obtíž	k1gFnSc2
se	s	k7c7
čtením	čtení	k1gNnSc7
z	z	k7c2
Bible	bible	k1gFnSc2
a	a	k8xC
modlitbami	modlitba	k1gFnPc7
spíše	spíše	k9
než	než	k8xS
s	s	k7c7
kázáním	kázání	k1gNnSc7
vlastními	vlastní	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Možnou	možný	k2eAgFnSc7d1
příčinnou	příčinný	k2eAgFnSc7d1
mohly	moct	k5eAaImAgInP
také	také	k9
být	být	k5eAaImF
jeho	jeho	k3xOp3gFnPc4
výhrady	výhrada	k1gFnPc4
k	k	k7c3
anglikánské	anglikánský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgInS
o	o	k7c4
křesťanské	křesťanský	k2eAgFnPc4d1
minority	minorita	k1gFnPc4
(	(	kIx(
<g/>
obdivoval	obdivovat	k5eAaImAgMnS
teologa	teolog	k1gMnSc2
F.	F.	kA
D.	D.	kA
Maurice	Maurika	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
alternativní	alternativní	k2eAgNnSc1d1
<g/>
“	“	k?
náboženství	náboženství	k1gNnSc1
(	(	kIx(
<g/>
teosofii	teosofie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Dodgsona	Dodgsona	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
(	(	kIx(
<g/>
začátek	začátek	k1gInSc1
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
)	)	kIx)
velmi	velmi	k6eAd1
trápil	trápit	k5eAaImAgMnS
nevyjasněný	vyjasněný	k2eNgInSc4d1
pocit	pocit	k1gInSc4
hříchu	hřích	k1gInSc2
a	a	k8xC
viny	vina	k1gFnSc2
a	a	k8xC
často	často	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgInPc2
deníků	deník	k1gInPc2
psal	psát	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
špatný	špatný	k2eAgInSc1d1
a	a	k8xC
bezcenný	bezcenný	k2eAgMnSc1d1
<g/>
“	“	k?
hříšník	hříšník	k1gMnSc1
a	a	k8xC
nezaslouží	zasloužit	k5eNaPmIp3nS
si	se	k3xPyFc3
být	být	k5eAaImF
knězem	kněz	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chybějící	chybějící	k2eAgInPc1d1
deníky	deník	k1gInPc1
</s>
<s>
Z	z	k7c2
třinácti	třináct	k4xCc2
Dodgsonových	Dodgsonův	k2eAgInPc2d1
deníků	deník	k1gInPc2
alespoň	alespoň	k9
čtyři	čtyři	k4xCgFnPc4
celé	celý	k2eAgFnPc4d1
knihy	kniha	k1gFnPc4
a	a	k8xC
asi	asi	k9
sedm	sedm	k4xCc1
stránek	stránka	k1gFnPc2
textu	text	k1gInSc2
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
knihy	kniha	k1gFnPc1
ztratily	ztratit	k5eAaPmAgFnP
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stránky	stránka	k1gFnPc1
někdo	někdo	k3yInSc1
záměrně	záměrně	k6eAd1
odstranil	odstranit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
odborníků	odborník	k1gMnPc2
zastává	zastávat	k5eAaImIp3nS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
deníky	deník	k1gInPc4
odstranili	odstranit	k5eAaPmAgMnP
rodinní	rodinní	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nepoškodily	poškodit	k5eNaPmAgInP
pověst	pověst	k1gFnSc4
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
dokázáno	dokázán	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc4
chybějící	chybějící	k2eAgInPc4d1
materiály	materiál	k1gInPc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
jediné	jediný	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
období	období	k1gNnSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
1853	#num#	k4
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
Dodgsonovi	Dodgsonův	k2eAgMnPc1d1
21	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
1863	#num#	k4
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
32	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
vysvětlení	vysvětlení	k1gNnSc4
ztráty	ztráta	k1gFnSc2
těchto	tento	k3xDgInPc2
materiálů	materiál	k1gInPc2
existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbené	oblíbený	k2eAgFnSc2d1
vysvětlení	vysvětlení	k1gNnSc4
pro	pro	k7c4
jednu	jeden	k4xCgFnSc4
konkrétní	konkrétní	k2eAgFnSc4d1
chybějící	chybějící	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1863	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
takový	takový	k3xDgInSc4
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
vytržena	vytržen	k2eAgFnSc1d1
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Dodgson	Dodgson	k1gMnSc1
ten	ten	k3xDgInSc4
den	den	k1gInSc4
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
ruku	ruka	k1gFnSc4
jedenáctileté	jedenáctiletý	k2eAgFnSc2d1
Alice	Alice	k1gFnSc2
Liddellové	Liddellová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
pro	pro	k7c4
to	ten	k3xDgNnSc4
ale	ale	k8xC
nebyly	být	k5eNaImAgInP
žádné	žádný	k3yNgInPc1
podklady	podklad	k1gInPc1
a	a	k8xC
list	list	k1gInSc1
nalezený	nalezený	k2eAgInSc1d1
v	v	k7c6
archivu	archiv	k1gInSc6
Dodgsonovy	Dodgsonův	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
naznačuje	naznačovat	k5eAaImIp3nS
opak	opak	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Migréna	migréna	k1gFnSc1
a	a	k8xC
epilepsie	epilepsie	k1gFnSc1
</s>
<s>
V	v	k7c6
deníku	deník	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
Dodgson	Dodgson	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
záchvat	záchvat	k1gInSc4
migrény	migréna	k1gFnSc2
s	s	k7c7
aurou	aura	k1gFnSc7
a	a	k8xC
velmi	velmi	k6eAd1
přesně	přesně	k6eAd1
popsal	popsat	k5eAaPmAgMnS
průběh	průběh	k1gInSc4
„	„	k?
<g/>
hýbajících	hýbající	k2eAgInPc2d1
se	se	k3xPyFc4
hradů	hrad	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
příznak	příznak	k1gInSc1
fáze	fáze	k1gFnSc2
aury	aura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Právě	právě	k9
po	po	k7c6
Dodgsonově	Dodgsonův	k2eAgFnSc6d1
Alence	Alenka	k1gFnSc6
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
forma	forma	k1gFnSc1
aury	aura	k1gFnSc2
migrény	migréna	k1gFnSc2
zvaná	zvaný	k2eAgFnSc1d1
syndrom	syndrom	k1gInSc4
Alenky	Alenka	k1gFnPc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
projevy	projev	k1gInPc1
tohoto	tento	k3xDgNnSc2
onemocnění	onemocnění	k1gNnSc2
mohou	moct	k5eAaImIp3nP
připomínat	připomínat	k5eAaImF
náhlé	náhlý	k2eAgFnPc4d1
změny	změna	k1gFnPc4
velikosti	velikost	k1gFnSc2
popsané	popsaný	k2eAgFnSc2d1
v	v	k7c6
knize	kniha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
s	s	k7c7
tímto	tento	k3xDgNnSc7
onemocněním	onemocnění	k1gNnSc7
se	se	k3xPyFc4
tedy	tedy	k9
například	například	k6eAd1
může	moct	k5eAaImIp3nS
dívat	dívat	k5eAaImF
na	na	k7c4
větší	veliký	k2eAgInSc4d2
předmět	předmět	k1gInSc4
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
basketbalový	basketbalový	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
vidět	vidět	k5eAaImF
ho	on	k3xPp3gMnSc4
ve	v	k7c6
velikosti	velikost	k1gFnSc6
golfového	golfový	k2eAgInSc2d1
míčku	míček	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
domněnky	domněnka	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
Dodgson	Dodgson	k1gMnSc1
trpěl	trpět	k5eAaImAgMnS
právě	právě	k9
tímto	tento	k3xDgInSc7
typem	typ	k1gInSc7
aury	aura	k1gFnSc2
a	a	k8xC
mohl	moct	k5eAaImAgInS
ji	on	k3xPp3gFnSc4
tedy	tedy	k9
využít	využít	k5eAaPmF
jako	jako	k8xC,k8xS
inspiraci	inspirace	k1gFnSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
díle	dílo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgFnPc4
hypotézy	hypotéza	k1gFnPc4
však	však	k9
neexistují	existovat	k5eNaImIp3nP
přesvědčivé	přesvědčivý	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dodgson	Dodgson	k1gMnSc1
také	také	k6eAd1
zažil	zažít	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
záchvaty	záchvat	k1gInPc4
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yRgFnPc6,k3yQgFnPc6,k3yIgFnPc6
ztratil	ztratit	k5eAaPmAgMnS
vědomí	vědomí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
diagnózu	diagnóza	k1gFnSc4
určovali	určovat	k5eAaImAgMnP
dva	dva	k4xCgMnPc1
lékaři	lékař	k1gMnPc1
<g/>
:	:	kIx,
dr	dr	kA
<g/>
.	.	kIx.
Morshead	Morshead	k1gInSc1
považoval	považovat	k5eAaImAgInS
záchvat	záchvat	k1gInSc4
za	za	k7c4
„	„	k?
<g/>
něco	něco	k3yInSc4
připomínajícího	připomínající	k2eAgNnSc2d1
epilepsii	epilepsie	k1gFnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
z	z	k7c2
toho	ten	k3xDgInSc2
vyvodili	vyvodit	k5eAaPmAgMnP,k5eAaBmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
epilepsií	epilepsie	k1gFnSc7
trpěl	trpět	k5eAaImAgMnS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
denících	deník	k1gInPc6
ani	ani	k8xC
dopisech	dopis	k1gInPc6
pro	pro	k7c4
to	ten	k3xDgNnSc4
není	být	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
důkaz	důkaz	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
podle	podle	k7c2
Sadiho	Sadi	k1gMnSc2
Ranson-Polizzottiho	Ranson-Polizzotti	k1gMnSc2
mohl	moct	k5eAaImAgMnS
Carroll	Carroll	k1gMnSc1
trpět	trpět	k5eAaImF
epilepsií	epilepsie	k1gFnSc7
spánkového	spánkový	k2eAgInSc2d1
laloku	lalok	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
ne	ne	k9
vždy	vždy	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
úplné	úplný	k2eAgFnSc3d1
ztrátě	ztráta	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
změně	změna	k1gFnSc3
vědomí	vědomí	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pacient	pacient	k1gMnSc1
prožívá	prožívat	k5eAaImIp3nS
mnoho	mnoho	k4c1
obdobných	obdobný	k2eAgInPc2d1
zážitků	zážitek	k1gInPc2
jako	jako	k8xS,k8xC
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
vydání	vydání	k1gNnSc4
děl	dělo	k1gNnPc2
Lewise	Lewise	k1gFnSc2
Carolla	Carolla	k1gFnSc1
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
Česky	česky	k6eAd1
vyšla	vyjít	k5eAaPmAgFnS
nejen	nejen	k6eAd1
díla	dílo	k1gNnSc2
Lewise	Lewise	k1gFnSc2
Carolla	Carollo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jejich	jejich	k3xOp3gFnPc4
různé	různý	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
jsou	být	k5eAaImIp3nP
vydávány	vydáván	k2eAgInPc1d1
překlady	překlad	k1gInPc1
Aloyse	Aloyse	k1gFnSc2
a	a	k8xC
Hany	Hana	k1gFnSc2
Skoumalových	Skoumalových	k2eAgFnSc2d1
z	z	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
a	a	k8xC
Jaroslava	Jaroslav	k1gMnSc4
Císaře	Císař	k1gMnSc4
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
modernější	moderní	k2eAgFnSc4d2
českou	český	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
Vladimír	Vladimír	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
např.	např.	kA
Alenku	Alenka	k1gFnSc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k8xS,k8xC
Aličku	Alička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlady	překlad	k1gInPc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
především	především	k6eAd1
interpretací	interpretace	k1gFnSc7
jmen	jméno	k1gNnPc2
a	a	k8xC
slovních	slovní	k2eAgFnPc2d1
hříček	hříčka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Překlad	překlad	k1gInSc1
Skoumalových	Skoumalová	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
základem	základ	k1gInSc7
pro	pro	k7c4
české	český	k2eAgFnPc4d1
verze	verze	k1gFnPc4
filmových	filmový	k2eAgNnPc2d1
zpracování	zpracování	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
Elišky	Eliška	k1gFnPc1
země	zem	k1gFnSc2
divů	div	k1gInPc2
a	a	k8xC
příhod	příhoda	k1gFnPc2
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Jan	Jan	k1gMnSc1
Váňa	Váňa	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
Hynek	Hynek	k1gMnSc1
1903	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kouzelný	kouzelný	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Dle	dle	k7c2
báchorky	báchorka	k1gFnSc2
Lewise	Lewise	k1gFnSc2
Carrolla	Carroll	k1gMnSc2
čes.	čes.	k?
mládeži	mládež	k1gFnSc6
vypravuje	vypravovat	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Houdek	Houdek	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Josef	Josef	k1gMnSc1
Wenig	Wenig	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Jos	Jos	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
podzemní	podzemní	k2eAgFnSc6d1
říši	říš	k1gFnSc6
(	(	kIx(
<g/>
ilustroval	ilustrovat	k5eAaBmAgMnS
John	John	k1gMnSc1
Tenniel	Tenniel	k1gMnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Elzevir	Elzevir	k1gMnSc1
(	(	kIx(
<g/>
B.	B.	kA
Moser	Moser	k1gMnSc1
<g/>
)	)	kIx)
1931	#num#	k4
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
a	a	k8xC
co	co	k9
tam	tam	k6eAd1
Alenka	Alenka	k1gFnSc1
našla	najít	k5eAaPmAgFnS
(	(	kIx(
<g/>
překlad	překlad	k1gInSc4
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
John	John	k1gMnSc1
Tenniel	Tenniel	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
a	a	k8xC
doslov	doslov	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Dagmar	Dagmar	k1gFnSc1
Berková	Berková	k1gFnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalův	k2eAgMnPc1d1
;	;	kIx,
doslov	doslovit	k5eAaPmRp2nS
Adolf	Adolf	k1gMnSc1
Hoffmeister	Hoffmeister	k1gMnSc1
<g/>
;	;	kIx,
ilustrovala	ilustrovat	k5eAaBmAgFnS
Dagmar	Dagmar	k1gFnSc1
Berková	Berková	k1gFnSc1
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
dětské	dětský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalův	k2eAgMnPc1d1
<g/>
,	,	kIx,
ilustroval	ilustrovat	k5eAaBmAgMnS
John	John	k1gMnSc1
Tenniel	Tenniel	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Logika	logika	k1gFnSc1
hrou	hra	k1gFnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
P.	P.	kA
Lánský	lánský	k2eAgInSc1d1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Pressfoto	Pressfota	k1gFnSc5
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
pro	pro	k7c4
čtenáře	čtenář	k1gMnPc4
od	od	k7c2
6	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumal	k1gMnSc3
<g/>
,	,	kIx,
doslov	doslov	k1gInSc1
Aloys	Aloysa	k1gFnPc2
Skoumalilustrace	Skoumalilustrace	k1gFnSc2
Markéta	Markéta	k1gFnSc1
Prachatická	prachatický	k2eAgFnSc1d1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Albatros	albatros	k1gMnSc1
1983	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
(	(	kIx(
<g/>
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pohádka	pohádka	k1gFnSc1
pro	pro	k7c4
chytré	chytrý	k2eAgFnPc4d1
i	i	k8xC
nezbedné	nezbedný	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
s	s	k7c7
použitím	použití	k1gNnSc7
překladu	překlad	k1gInSc2
Aloyse	Aloys	k1gMnSc2
Skoumala	Skoumal	k1gMnSc2
zdramatizoval	zdramatizovat	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
Texel	Texel	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Dilia	Dilia	k1gFnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
(	(	kIx(
<g/>
obrázkový	obrázkový	k2eAgInSc1d1
příběh	příběh	k1gInSc1
podle	podle	k7c2
Lewise	Lewise	k1gFnSc2
Carrolla	Carrollo	k1gNnSc2
<g/>
,	,	kIx,
kresby	kresba	k1gFnSc2
Chiqui	Chiqu	k1gFnSc2
de	de	k?
la	la	k0
Fuente	Fuent	k1gMnSc5
<g/>
,	,	kIx,
ze	z	k7c2
španělštiny	španělština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Kamil	Kamil	k1gMnSc1
Pecka	Pecka	k1gMnSc1
;	;	kIx,
scénář	scénář	k1gInSc1
Joseluis	Joseluis	k1gInSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Kentaur	kentaur	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sylvie	Sylvie	k1gFnSc1
a	a	k8xC
Bruno	Bruno	k1gMnSc1
(	(	kIx(
<g/>
ilustroval	ilustrovat	k5eAaBmAgMnS
Harry	Harr	k1gInPc4
Furniss	Furnissa	k1gFnPc2
<g/>
,	,	kIx,
přeložila	přeložit	k5eAaPmAgFnS
Markéta	Markéta	k1gFnSc1
Cukrová	cukrový	k2eAgFnSc1d1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Trigon	trigon	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zamotaný	zamotaný	k2eAgInSc1d1
příběh	příběh	k1gInSc1
(	(	kIx(
<g/>
přeložil	přeložit	k5eAaPmAgMnS
Luboš	Luboš	k1gMnSc1
Pick	Pick	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Arthur	Arthur	k1gMnSc1
B.	B.	kA
Frost	Frost	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Volvox	Volvox	k1gInSc1
Globator	Globator	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
(	(	kIx(
<g/>
podle	podle	k7c2
Lewise	Lewise	k1gFnSc2
Carrolla	Carrollo	k1gNnSc2
napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
Lucy	Luc	k1gMnPc4
Kincaidová	Kincaidový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ilustroval	ilustrovat	k5eAaBmAgMnS
Gill	Gill	k1gMnSc1
Guile	Guile	k1gFnSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc4
z	z	k7c2
angličtiny	angličtina	k1gFnSc2
Jarmila	Jarmila	k1gFnSc1
Jurečková	Jurečková	k1gFnSc1
<g/>
;	;	kIx,
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
(	(	kIx(
<g/>
ilustrace	ilustrace	k1gFnSc1
Johna	John	k1gMnSc2
Tenniela	Tenniel	k1gMnSc2
kolorovali	kolorovat	k5eAaBmAgMnP
Harry	Harr	k1gInPc1
Theaker	Theaker	k1gInSc1
a	a	k8xC
Diz	Diz	k1gFnSc1
Wallis	Wallis	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Aurora	Aurora	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tlachapoud	Tlachapoud	k1gInSc1
=	=	kIx~
Jabberwocky	Jabberwocka	k1gFnPc1
;	;	kIx,
Žvahlav	Žvahlav	k1gFnPc1
=	=	kIx~
Jabberwocky	Jabberwocka	k1gFnPc1
;	;	kIx,
Mrož	mrož	k1gMnSc1
a	a	k8xC
tesař	tesař	k1gMnSc1
=	=	kIx~
The	The	k1gMnSc1
Walrus	Walrus	k1gMnSc1
and	and	k?
the	the	k?
Carpenter	Carpenter	k1gInSc1
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
a	a	k8xC
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalovi	k1gRnPc1
<g/>
,	,	kIx,
šest	šest	k4xCc4
leptů	lept	k1gInPc2
Markéta	Markéta	k1gFnSc1
Prachatická	prachatický	k2eAgFnSc1d1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Aulos	Aulos	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zamotaný	zamotaný	k2eAgInSc1d1
příběh	příběh	k1gInSc1
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Luboš	Luboš	k1gMnSc1
Pick	Pick	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Levné	levný	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
KMa	KMa	k1gFnSc1
1996	#num#	k4
<g/>
,	,	kIx,
Volvox	Volvox	k1gInSc1
Globator	Globator	k1gInSc1
2001	#num#	k4
<g/>
,	,	kIx,
Dokořán	dokořán	k6eAd1
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Through	Through	k1gInSc1
the	the	k?
looking-glass	looking-glass	k1gInSc1
=	=	kIx~
Za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
a	a	k8xC
s	s	k7c7
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
se	se	k3xPyFc4
tam	tam	k6eAd1
Alenka	Alenka	k1gFnSc1
setkala	setkat	k5eAaPmAgFnS
(	(	kIx(
<g/>
překlad	překlad	k1gInSc4
Aloys	Aloysa	k1gFnPc2
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalovi	k1gRnPc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Garamond	garamond	k1gInSc1
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
ilustroval	ilustrovat	k5eAaBmAgMnS
Dušan	Dušan	k1gMnSc1
Kállay	Kállaa	k1gFnSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc4
Aloys	Aloysa	k1gFnPc2
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumal	k1gMnSc6
;	;	kIx,
básně	báseň	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Hanzlík	Hanzlík	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
2005	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
ilustrace	ilustrace	k1gFnSc1
Markéta	Markéta	k1gFnSc1
Prachatická	prachatický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalovi	k1gRnPc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Levné	levný	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lovení	lovení	k1gNnSc1
Snárka	Snárk	k1gInSc2
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Václav	Václav	k1gMnSc1
Pinkava	Pinkava	k?
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Adolf	Adolf	k1gMnSc1
Born	Born	k1gInSc1
<g/>
;	;	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Host	host	k1gMnSc1
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalův	k2eAgMnPc1d1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
John	John	k1gMnSc1
Tenniel	Tenniel	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Aloys	Aloys	k1gInSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Skoumalovi	Skoumalovi	k1gRnPc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
George	Georg	k1gMnSc2
Lukas	Lukas	k1gInSc4
<g/>
,	,	kIx,
text	text	k1gInSc4
upravila	upravit	k5eAaPmAgFnS
a	a	k8xC
didakticky	didakticky	k6eAd1
připravila	připravit	k5eAaPmAgFnS
Petra	Petra	k1gFnSc1
Sůvová	Sůvová	k1gFnSc1
<g/>
;	;	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Sova	sova	k1gFnSc1
Libris	Libris	k1gFnSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alice	Alice	k1gFnSc1
in	in	k?
Wonderland	Wonderland	k1gInSc1
=	=	kIx~
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
(	(	kIx(
<g/>
převyprávění	převyprávění	k1gNnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
<g/>
,	,	kIx,
jazykové	jazykový	k2eAgFnPc1d1
poznámky	poznámka	k1gFnPc1
<g/>
)	)	kIx)
Michelle	Michelle	k1gFnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Lucie	Lucie	k1gFnSc1
Poslušná	poslušný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Špirko	Špirko	k1gNnSc1
<g/>
;	;	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Computer	computer	k1gInSc1
Press	Press	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Jitka	Jitka	k1gFnSc1
Boková	bokový	k2eAgFnSc1d1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Argo	Argo	k6eAd1
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Aliččina	Aliččin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
Divukraji	Divukraj	k1gFnSc6
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Vladimír	Vladimír	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Tamara	Tamara	k1gFnSc1
Pauknerová	Pauknerová	k1gFnSc1
<g/>
;	;	kIx,
Neratovice	Neratovice	k1gFnPc1
<g/>
,	,	kIx,
Mojeknihy	Mojeknih	k1gInPc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Helena	Helena	k1gFnSc1
Čížková	Čížková	k1gFnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
XYZ	XYZ	kA
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
kraji	kraj	k1gInSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Žák	Žák	k1gMnSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
Ladislav	Ladislava	k1gFnPc2
Vlna	vlna	k1gFnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
XYZ	XYZ	kA
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenčina	Alenčin	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
a	a	k8xC
za	za	k7c7
zrcadlem	zrcadlo	k1gNnSc7
(	(	kIx(
<g/>
ilustrace	ilustrace	k1gFnSc2
Jan	Jan	k1gMnSc1
Švankmajer	Švankmajer	k1gMnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Dybbuk	Dybbuk	k1gInSc1
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alenka	Alenka	k1gFnSc1
pro	pro	k7c4
nejmenší	malý	k2eAgInSc4d3
(	(	kIx(
<g/>
text	text	k1gInSc4
pro	pro	k7c4
nejmenší	malý	k2eAgMnPc4d3
čtenáře	čtenář	k1gMnPc4
upravil	upravit	k5eAaPmAgInS
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnSc1
John	John	k1gMnSc1
Tenniela	Tenniela	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Rambousek	Rambousek	k1gMnSc1
<g/>
;	;	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Books	Books	k1gInSc1
&	&	k?
Pipes	Pipes	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Například	například	k6eAd1
Jaroslav	Jaroslav	k1gMnSc1
Císař	Císař	k1gMnSc1
přeložil	přeložit	k5eAaPmAgMnS
anglické	anglický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Jabberwocky	Jabberwocka	k1gFnSc2
jako	jako	k8xC,k8xS
Žvahlav	Žvahlav	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Z.	Z.	kA
J.	J.	kA
Pinkava	Pinkava	k?
jej	on	k3xPp3gMnSc4
přeložil	přeložit	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
Hromoplkie	Hromoplkie	k1gFnSc1
a	a	k8xC
Skoumalovi	Skoumalův	k2eAgMnPc1d1
počeštili	počeštit	k5eAaPmAgMnP
na	na	k7c4
Tlachapoud	Tlachapoud	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Jako	jako	k8xC,k8xS
rok	rok	k1gInSc4
vzniku	vznik	k1gInSc2
překladu	překlad	k1gInSc2
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
rok	rok	k1gInSc1
1896	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
knižního	knižní	k2eAgNnSc2d1
vydání	vydání	k1gNnSc2
též	též	k9
rok	rok	k1gInSc4
1902	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
volnou	volnou	k6eAd1
adaptací	adaptace	k1gFnSc7
originálu	originál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
Kindred	Kindred	k1gMnSc1
Britain	Britain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CLARK	CLARK	kA
<g/>
,	,	kIx,
Ann	Ann	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
J.	J.	kA
M.	M.	kA
Dent	Dent	k1gMnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
286	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
460	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4302	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Clark	Clark	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
COLLINGWOOD	COLLINGWOOD	kA
<g/>
,	,	kIx,
Stuart	Stuart	k1gInSc4
Dodgson	Dodgsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Life	Life	k1gFnSc1
and	and	k?
Letters	Letters	k1gInSc1
of	of	k?
Lewis	Lewis	k1gFnSc2
Carroll	Carrolla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
T.	T.	kA
Fisher	Fishra	k1gFnPc2
Unwin	Unwina	k1gFnPc2
<g/>
,	,	kIx,
1898	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Collingwood	Collingwood	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Collingwood	Collingwood	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
COHEN	COHEN	kA
<g/>
,	,	kIx,
Morton	Morton	k1gInSc1
N.	N.	kA
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Vintage	Vintage	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
608	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
679	#num#	k4
<g/>
-	-	kIx~
<g/>
74562	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Cohen	Cohen	k2eAgMnSc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
O	O	kA
<g/>
'	'	kIx"
<g/>
CONNOR	CONNOR	kA
<g/>
,	,	kIx,
J.	J.	kA
J.	J.	kA
<g/>
;	;	kIx,
ROBERTSON	ROBERTSON	kA
<g/>
,	,	kIx,
E.	E.	kA
F.	F.	kA
Charles	Charles	k1gMnSc1
Lutwidge	Lutwidg	k1gFnSc2
Dodgson	Dodgson	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
School	School	k1gInSc1
of	of	k?
Mathematics	Mathematics	k1gInSc1
and	and	k?
Statistics	Statisticsa	k1gFnPc2
<g/>
,	,	kIx,
University	universita	k1gFnSc2
of	of	k?
St	St	kA
Andrews	Andrews	k1gInSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2002-11	2002-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
202	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WINCHESTER	Winchester	k1gMnSc1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Alice	Alice	k1gFnSc2
Behind	Behinda	k1gFnPc2
Wonderland	Wonderlanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
110	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
539619	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
641525313	#num#	k4
S.	S.	kA
11	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Winchester	Winchester	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
RUIZ	RUIZ	kA
PRADOS	PRADOS	kA
<g/>
,	,	kIx,
Lara	Larum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alice	Alice	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Adventures	Adventures	k1gInSc1
in	in	k?
Wonderland	Wonderland	k1gInSc1
and	and	k?
Literary	Literara	k1gFnSc2
Nonsense	nonsens	k1gInSc5
<g/>
:	:	kIx,
A	a	k9
Deconstructive	Deconstructiv	k1gInSc5
Analysis	Analysis	k1gFnPc6
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Novel	novela	k1gFnPc2
<g/>
.	.	kIx.
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Iceland	Iceland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc2
Anna	Anna	k1gFnSc1
Heið	Heið	k1gMnSc1
Pálsdóttir	Pálsdóttir	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Winchester	Winchester	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SEDLACEK	SEDLACEK	kA
<g/>
,	,	kIx,
Cameron	Cameron	k1gMnSc1
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satire	Satir	k1gInSc5
and	and	k?
Synthesis	Synthesis	k1gFnPc7
<g/>
:	:	kIx,
Parody	parodos	k1gInPc1
and	and	k?
Satire	Satir	k1gInSc5
of	of	k?
Victorian	Victorian	k1gInSc1
Education	Education	k1gInSc1
in	in	k?
the	the	k?
Works	Works	kA
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
.	.	kIx.
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masters	Masters	k1gInSc1
Thesis	Thesis	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Central	Central	k1gFnSc1
Washington	Washington	k1gInSc1
University	universita	k1gFnSc2
<g/>
.	.	kIx.
.	.	kIx.
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Collingwood	Collingwood	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Collingwood	Collingwood	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Clark	Clark	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
–	–	k?
<g/>
65.1	65.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
LEACH	LEACH	kA
<g/>
,	,	kIx,
Karoline	Karolin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
the	the	k?
Shadow	Shadow	k1gMnSc1
of	of	k?
the	the	k?
Dreamchild	Dreamchild	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
New	New	k1gFnPc3
Understanding	Understanding	k1gInSc1
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Owen	Owen	k1gMnSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7206	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TVERITINA	TVERITINA	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
in	in	k?
a	a	k8xC
Russian	Russian	k1gMnSc1
wonderland	wonderlanda	k1gFnPc2
of	of	k?
surprises	surprisesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-06	2013-09-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CARPENTER	CARPENTER	kA
<g/>
,	,	kIx,
Angelica	Angelicum	k1gNnSc2
Shirley	Shirlea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
<g/>
:	:	kIx,
Through	Through	k1gInSc1
the	the	k?
Looking	Looking	k1gInSc1
Glass	Glass	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Lerner	Lerner	k1gMnSc1
Pub	Pub	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
128	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
822500736	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
98	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Carpenter	Carpenter	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Cohen	Cohna	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
100	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CARROLL	CARROLL	kA
<g/>
,	,	kIx,
Lewis	Lewis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Letters	Lettersa	k1gFnPc2
of	of	k?
Lewis	Lewis	k1gFnPc2
Carroll	Carroll	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Morton	Morton	k1gInSc1
N.	N.	kA
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
Roger	Roger	k1gInSc1
Lancelyn	Lancelyna	k1gFnPc2
Green	Grena	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volume	volum	k1gInSc5
1	#num#	k4
<g/>
:	:	kIx,
ca	ca	kA
<g/>
.	.	kIx.
1837	#num#	k4
<g/>
-	-	kIx~
<g/>
1885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Macmillan	Macmillan	k1gMnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
svazky	svazek	k1gInPc4
(	(	kIx(
<g/>
XXXVIII	XXXVIII	kA
<g/>
,	,	kIx,
614	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
333089790	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Carroll	Carroll	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
LEACH	LEACH	kA
<g/>
,	,	kIx,
Karoline	Karolin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
the	the	k?
Shadow	Shadow	k1gMnSc1
of	of	k?
the	the	k?
Dreamchild	Dreamchild	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
New	New	k1gFnPc3
Understanding	Understanding	k1gInSc1
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Owen	Owen	k1gMnSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7206	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
5	#num#	k4
"	"	kIx"
<g/>
The	The	k1gFnSc1
Unreal	Unreal	k1gInSc1
Alice	Alice	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Winchester	Winchester	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
56.1	56.1	k4
2	#num#	k4
LEACH	LEACH	kA
<g/>
,	,	kIx,
Karoline	Karolin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
the	the	k?
Shadow	Shadow	k1gMnSc1
of	of	k?
the	the	k?
Dreamchild	Dreamchild	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
New	New	k1gFnPc3
Understanding	Understanding	k1gInSc1
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Owen	Owen	k1gMnSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7206	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GARDNER	GARDNER	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Annotated	Annotated	k1gMnSc1
Alice	Alice	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Definitive	Definitiv	k1gInSc5
Edition	Edition	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
W.	W.	kA
W.	W.	kA
Norton	Norton	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XXVIII	XXVIII	kA
<g/>
,	,	kIx,
312	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
393048470	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HEATH	HEATH	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philosopher	Philosophra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Alice	Alice	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
249	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
856701262	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ELSTER	ELSTER	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Harrington	Harrington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
big	big	k?
book	book	k1gMnSc1
of	of	k?
beastly	beastnout	k5eAaPmAgInP
mispronunciations	mispronunciations	k6eAd1
<g/>
:	:	kIx,
the	the	k?
complete	complést	k5eAaPmIp3nS
opinionated	opinionated	k1gInSc4
guide	guide	k6eAd1
for	forum	k1gNnPc2
the	the	k?
careful	careful	k1gInSc1
speaker	speaker	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Houghton	Houghton	k1gInSc1
Mifflin	Mifflina	k1gFnPc2
Harcourt	Harcourta	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XIX	XIX	kA
<g/>
,	,	kIx,
522	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
61842315	#num#	k4
<g/>
X.	X.	kA
S.	S.	kA
158	#num#	k4
<g/>
–	–	k?
<g/>
159	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JOHANNESSEN	JOHANNESSEN	kA
<g/>
,	,	kIx,
Finn-Henning	Finn-Henning	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alice	Alice	k1gFnSc1
in	in	k?
Wonderland	Wonderland	k1gInSc1
<g/>
:	:	kIx,
Development	Development	k1gInSc1
of	of	k?
Alice	Alice	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Identity	identita	k1gFnSc2
within	within	k2eAgInSc1d1
Adaptations	Adaptations	k1gInSc1
<g/>
.	.	kIx.
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
91	#num#	k4
s.	s.	k?
Master	master	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Thesis	Thesis	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faculty	Facult	k1gInPc7
of	of	k?
Humanities	Humanities	k1gMnSc1
<g/>
,	,	kIx,
Social	Social	k1gMnSc1
Sciences	Sciences	k1gMnSc1
and	and	k?
Education	Education	k1gInSc1
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Tromsø	Tromsø	k1gFnSc1
<g/>
.	.	kIx.
.	.	kIx.
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Clark	Clark	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
93	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Roger	Roger	k1gMnSc1
<g/>
;	;	kIx,
WAKELING	WAKELING	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
<g/>
,	,	kIx,
Photographer	Photographra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
Jersey	Jersea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XV	XV	kA
<g/>
,	,	kIx,
287	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
691074437	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
COHEN	COHEN	kA
<g/>
,	,	kIx,
Morton	Morton	k1gInSc1
N.	N.	kA
Reflections	Reflections	k1gInSc1
in	in	k?
a	a	k8xC
looking	looking	k1gInSc1
glass	glassa	k1gFnPc2
<g/>
:	:	kIx,
a	a	k8xC
centennial	centenniat	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
celebration	celebration	k1gInSc1
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carrolla	k1gFnPc2
<g/>
,	,	kIx,
photographer	photographra	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Aperture	Apertur	k1gMnSc5
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
143	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
893817961	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Tolkačova	Tolkačův	k2eAgFnSc1d1
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
THOMAS	Thomas	k1gMnSc1
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
Serrell	Serrell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Barnes	Barnes	k1gInSc1
&	&	k?
Noble	Noble	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7607	#num#	k4
<g/>
-	-	kIx~
<g/>
1232	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
116	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Thomas	Thomas	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
265	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Featured	Featured	k1gInSc1
Works	Works	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
<g/>
:	:	kIx,
Art	Art	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dodgson	Dodgson	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
MS	MS	kA
diaries	diaries	k1gInSc1
<g/>
,	,	kIx,
volume	volum	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
October	October	k1gInSc1
1862	#num#	k4
<g/>
↑	↑	k?
Cohen	Cohen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
263	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WILSON	WILSON	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
in	in	k?
Numberland	Numberland	k1gInSc1
<g/>
:	:	kIx,
His	his	k1gNnSc1
Fantastical	Fantastical	k1gFnSc2
Mathematical	Mathematical	k1gMnSc2
Logical	Logical	k1gMnSc2
Life	Lif	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Allen	Allen	k1gMnSc1
Lane	Lan	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7139	#num#	k4
<g/>
-	-	kIx~
<g/>
9757	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
103	#num#	k4
<g/>
–	–	k?
<g/>
104	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEACH	LEACH	kA
<g/>
,	,	kIx,
Karoline	Karolin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
the	the	k?
Shadow	Shadow	k1gMnSc1
of	of	k?
the	the	k?
Dreamchild	Dreamchild	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
New	New	k1gFnPc3
Understanding	Understanding	k1gInSc1
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Owen	Owen	k1gMnSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7206	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
134	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Leach	Leach	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Dodgson	Dodgson	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
MS	MS	kA
diaries	diaries	k1gInSc1
<g/>
,	,	kIx,
volume	volum	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
see	see	k?
prayers	prayers	k1gInSc1
scattered	scattered	k1gMnSc1
throughout	throughout	k1gMnSc1
the	the	k?
text	text	k1gInSc1
<g/>
↑	↑	k?
Collingwood	Collingwood	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
48	#num#	k4
<g/>
,	,	kIx,
51	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Collingwood	Collingwood	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dodgson	Dodgson	k1gMnSc1
Family	Famila	k1gFnSc2
Collection	Collection	k1gInSc1
<g/>
,	,	kIx,
Cat	Cat	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
No	no	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
F	F	kA
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
„	„	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.lewiscarroll.cc	www.lewiscarroll.cc	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
“	“	k?
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
K	k	k7c3
objevu	objev	k1gInSc3
vizte	vidět	k5eAaImRp2nP
The	The	k1gMnSc6
Times	Times	k1gMnSc1
Literary	Literara	k1gFnSc2
Supplement	Supplement	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
CARROLL	CARROLL	kA
<g/>
,	,	kIx,
Lewis	Lewis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
diaries	diariesa	k1gFnPc2
<g/>
:	:	kIx,
the	the	k?
private	privat	k1gInSc5
journals	journals	k6eAd1
of	of	k?
Charles	Charles	k1gMnSc1
Lutwidge	Lutwidg	k1gFnSc2
Dodgson	Dodgson	k1gMnSc1
(	(	kIx(
<g/>
Lewis	Lewis	k1gFnSc1
Carroll	Carroll	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
the	the	k?
first	first	k1gInSc1
complete	complést	k5eAaPmIp3nS
version	version	k1gInSc4
of	of	k?
the	the	k?
nine	ninat	k5eAaPmIp3nS
surviving	surviving	k1gInSc1
volumes	volumesa	k1gFnPc2
with	witha	k1gFnPc2
notes	notes	k1gInSc1
and	and	k?
annotations	annotations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Edward	Edward	k1gMnSc1
Wakeling	Wakeling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clifford	Clifford	k1gInSc4
(	(	kIx(
<g/>
Herefordshire	Herefordshir	k1gMnSc5
<g/>
)	)	kIx)
<g/>
:	:	kIx,
The	The	k1gMnSc3
Lewis	Lewis	k1gFnSc2
Carroll	Carroll	k1gInSc4
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
svazků	svazek	k1gInPc2
(	(	kIx(
<g/>
408	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
904117	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MAUDIE	MAUDIE	kA
<g/>
,	,	kIx,
F.	F.	kA
W.	W.	kA
Migraine	Migrain	k1gInSc5
and	and	k?
Lewis	Lewis	k1gFnSc3
Carroll	Carroll	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc5
Migraine	Migrain	k1gMnSc5
Periodical	Periodical	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PODOLL	PODOLL	kA
<g/>
,	,	kIx,
Klaus	Klaus	k1gMnSc1
<g/>
;	;	kIx,
ROBINSON	Robinson	k1gMnSc1
<g/>
,	,	kIx,
Derek	Derek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
migraine	migrainout	k5eAaPmIp3nS
experiences	experiencesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Lancet	lanceta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elsevier	Elsevier	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
353	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9161	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1366	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
140	#num#	k4
<g/>
-	-	kIx~
<g/>
6736	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
140	#num#	k4
<g/>
-	-	kIx~
<g/>
6736	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
74368	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10218566	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sadi	Sadi	k1gNnSc2
Ranson	Ransona	k1gFnPc2
Polizetti	Polizetť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Photography	Photographa	k1gFnSc2
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
blogcritics	blogcritics	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
NÝVLTOVÁ	Nývltová	k1gFnSc1
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analýza	analýza	k1gFnSc1
jazykových	jazykový	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
výstavby	výstavba	k1gFnSc2
tří	tři	k4xCgInPc2
českých	český	k2eAgInPc2d1
překladů	překlad	k1gInPc2
básně	báseň	k1gFnSc2
Jabberwocky	Jabberwocka	k1gFnSc2
(	(	kIx(
<g/>
Žvahlav	Žvahlav	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Tlachapoud	Tlachapoud	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Hromoplkie	Hromoplkie	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
knihy	kniha	k1gFnSc2
L.	L.	kA
Carrolla	Carrolla	k1gMnSc1
Through	Through	k1gMnSc1
the	the	k?
Looking-Glass	Looking-Glass	k1gInSc1
and	and	k?
What	What	k1gInSc1
Alice	Alice	k1gFnSc1
Found	Found	k1gMnSc1
There	Ther	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
50	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc1
Ivana	Ivana	k1gFnSc1
Bozděchová	Bozděchová	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOŘÍNKOVÁ	Kořínková	k1gFnSc1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrané	vybraný	k2eAgInPc1d1
problémy	problém	k1gInPc1
překladu	překlad	k1gInSc2
knihy	kniha	k1gFnSc2
Alenka	Alenka	k1gFnSc1
v	v	k7c6
říši	říš	k1gFnSc6
divů	div	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Milan	Milan	k1gMnSc1
Hrdlička	Hrdlička	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDLÁKOVÁ	Sedláková	k1gFnSc1
<g/>
,	,	kIx,
Michaela	Michaela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadčasová	nadčasový	k2eAgFnSc1d1
Alenčina	Alenčin	k2eAgFnSc1d1
Říše	říše	k1gFnSc1
Divů	div	k1gMnPc2
ve	v	k7c6
třech	tři	k4xCgFnPc6
filmových	filmový	k2eAgFnPc6d1
adaptacích	adaptace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
47	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
Kobíková	Kobíková	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PRETSCHNER	PRETSCHNER	kA
<g/>
,	,	kIx,
Adéla	Adéla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knižní	knižní	k2eAgInPc1d1
překlady	překlad	k1gInPc1
soudobé	soudobý	k2eAgFnSc2d1
britské	britský	k2eAgFnSc2d1
prózy	próza	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1862	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
60	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Dalibor	Dalibor	k1gMnSc1
Tureček	Tureček	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
REED	REED	kA
<g/>
,	,	kIx,
Langford	Langford	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Life	Life	k1gFnSc1
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
W	W	kA
&	&	k?
G	G	kA
Foyle	Foyle	k1gFnSc2
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
Lucas	Lucasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
White	Whit	k1gInSc5
Knight	Knight	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edinburgh	Edinburgh	k1gMnSc1
<g/>
:	:	kIx,
Oliver	Oliver	k1gMnSc1
&	&	k?
Boyd	Boyd	k1gMnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIII	VIII	kA
<g/>
,	,	kIx,
209	#num#	k4
s.	s.	k?
OCLC	OCLC	kA
186449183	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Roger	Roger	k1gMnSc1
<g/>
;	;	kIx,
WAKELING	WAKELING	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
<g/>
,	,	kIx,
Photographer	Photographra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
Jersey	Jersea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
691074437	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BOWMAN	BOWMAN	kA
<g/>
,	,	kIx,
Isa	Isa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
Lewis	Lewis	k1gFnSc2
Carroll	Carrolla	k1gFnPc2
<g/>
:	:	kIx,
Told	Tolda	k1gFnPc2
for	forum	k1gNnPc2
Young	Young	k1gMnSc1
People	People	k1gMnSc1
by	by	k9
the	the	k?
Real	Real	k1gInSc1
Alice	Alice	k1gFnSc1
in	in	k?
Wonderland	Wonderland	k1gInSc1
<g/>
,	,	kIx,
Miss	miss	k1gFnSc1
Isa	Isa	k1gMnSc1
Bowman	Bowman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
J.	J.	kA
<g/>
M.	M.	kA
Dent	Dent	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WAGGONER	WAGGONER	kA
<g/>
,	,	kIx,
Diane	Dian	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Photography	Photograph	k1gInPc7
and	and	k?
Modern	Modern	k1gInSc4
Childhood	Childhooda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
Jersey	Jersea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
691	#num#	k4
<g/>
-	-	kIx~
<g/>
19318	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WAKELING	WAKELING	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Photographs	Photographsa	k1gFnPc2
of	of	k?
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Catalogue	Catalogue	k1gNnSc1
Raisonné	Raisonný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austin	Austin	k1gInSc1
(	(	kIx(
<g/>
Texas	Texas	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Texas	Texas	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
292	#num#	k4
<g/>
-	-	kIx~
<g/>
76743	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tolkačova	Tolkačův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Irina	Irina	k1gFnSc1
<g/>
.	.	kIx.
Ф	Ф	k?
Л	Л	k?
К	К	k?
<g/>
,	,	kIx,
Ф	Ф	k?
<g/>
:	:	kIx,
časopis	časopis	k1gInSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2010	#num#	k4
(	(	kIx(
<g/>
č.	č.	k?
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
–	–	k?
<g/>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
7082	#num#	k4
<g/>
-	-	kIx~
<g/>
3907	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
a	a	k8xC
šachy	šach	k1gInPc4
</s>
<s>
Literatura	literatura	k1gFnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
</s>
<s>
Seznam	seznam	k1gInSc1
anglických	anglický	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lewis	Lewis	k1gFnSc2
Carroll	Carroll	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Lewis	Lewis	k1gFnSc1
Carroll	Carroll	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Lewis	Lewis	k1gFnSc2
Carroll	Carrolla	k1gFnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Lewis	Lewis	k1gFnSc2
Carroll	Carrolla	k1gFnPc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Lewis	Lewis	k1gFnSc2
Carroll	Carrolla	k1gFnPc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
</s>
<s>
Plné	plný	k2eAgInPc1d1
texty	text	k1gInPc1
děl	dělo	k1gNnPc2
autora	autor	k1gMnSc2
Lewis	Lewis	k1gFnPc2
Carroll	Carroll	k1gMnSc1
na	na	k7c6
projektu	projekt	k1gInSc6
Gutenberg	Gutenberg	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Fotografie	fotografia	k1gFnPc1
Lewise	Lewise	k1gFnSc2
Carrolla	Carrolla	k1gFnSc1
Fotografie	fotografie	k1gFnSc1
</s>
<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
a	a	k8xC
umělecká	umělecký	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
</s>
<s>
Reginald	Reginald	k1gInSc1
Southey	Southea	k1gFnSc2
a	a	k8xC
kostry	kostra	k1gFnSc2
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Karkulka	Karkulka	k1gFnSc1
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alice	Alice	k1gFnSc1
Liddellová	Liddellová	k1gFnSc1
jako	jako	k8xC,k8xS
žebračka	žebračka	k1gFnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šachy	šach	k1gInPc1
na	na	k7c6
fotografiích	fotografia	k1gFnPc6
Lewise	Lewise	k1gFnSc2
Carrolla	Carrollo	k1gNnSc2
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
-	-	kIx~
<g/>
1866	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alice	Alice	k1gFnSc1
Liddellová	Liddellová	k1gFnSc1
a	a	k8xC
kapradina	kapradina	k1gFnSc1
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Májová	májový	k2eAgFnSc1d1
královna	královna	k1gFnSc1
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Královna	královna	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
a	a	k8xC
překrásná	překrásný	k2eAgFnSc1d1
Rosamunda	Rosamunda	k1gFnSc1
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Penelope	Penelop	k1gMnSc5
Boothby	Boothba	k1gMnSc2
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
—	—	k?
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Jiří	Jiří	k1gMnSc1
a	a	k8xC
drak	drak	k1gMnSc1
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
Fotomodely	Fotomodela	k1gFnSc2
</s>
<s>
Agnes	Agnes	k1gMnSc1
Grace	Grace	k1gMnSc1
Weld	Weld	k1gMnSc1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Angelina	Angelin	k2eAgFnSc1d1
Acland	Acland	k1gInSc4
</s>
<s>
Isa	Isa	k?
Bowman	Bowman	k1gMnSc1
</s>
<s>
Connie	Connie	k1gFnSc1
Gilchrist	Gilchrist	k1gInSc1
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Orkney	Orknea	k1gFnSc2
</s>
<s>
Charles	Charles	k1gMnSc1
Dodgson	Dodgson	k1gMnSc1
(	(	kIx(
<g/>
kněz	kněz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Kitchin	Kitchina	k1gFnPc2
</s>
<s>
Alice	Alice	k1gFnSc1
Liddellová	Liddellová	k1gFnSc1
</s>
<s>
Annie	Annie	k1gFnSc1
Rogers	Rogersa	k1gFnPc2
</s>
<s>
Dante	Dante	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Rossetti	Rossetť	k1gFnSc2
</s>
<s>
Ethel	Ethel	k1gMnSc1
Hatch	Hatch	k1gMnSc1
</s>
<s>
Evelyn	Evelyn	k1gMnSc1
Hatch	Hatch	k1gMnSc1
</s>
<s>
Beatrice	Beatrice	k1gFnSc2
Hatch	Hatcha	k1gFnPc2
Carrollovi	Carrollův	k2eAgMnPc1d1
mentoři	mentor	k1gMnPc1
ve	v	k7c6
fotografii	fotografia	k1gFnSc6
</s>
<s>
Clementina	Clementina	k1gFnSc1
Hawarden	Hawardna	k1gFnPc2
</s>
<s>
Oscar	Oscar	k1gInSc1
Gustave	Gustav	k1gMnSc5
Rejlander	Rejlander	k1gMnSc1
</s>
<s>
Reginald	Reginald	k1gInSc1
Southey	Southea	k1gFnSc2
Následovníci	následovník	k1gMnPc1
a	a	k8xC
napodobitelé	napodobitel	k1gMnPc1
</s>
<s>
Polixeni	Polixen	k2eAgMnPc1d1
Papapetrou	Papapetra	k1gFnSc7
</s>
<s>
Bill	Bill	k1gMnSc1
Gekas	Gekas	k1gMnSc1
</s>
<s>
Hadžime	Hadžimat	k5eAaPmIp3nS
Sawatari	Sawatare	k1gFnSc4
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1
fotografové	fotograf	k1gMnPc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
William	William	k1gInSc1
de	de	k?
Wiveleslie	Wiveleslie	k1gFnSc2
Abney	Abnea	k1gFnSc2
</s>
<s>
Sarah	Sarah	k1gFnSc1
Angelina	Angelin	k2eAgFnSc1d1
Acland	Acland	k1gInSc4
</s>
<s>
James	James	k1gMnSc1
Craig	Craig	k1gMnSc1
Annan	Annan	k1gMnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Atkinsová	Atkinsová	k1gFnSc1
</s>
<s>
William	William	k1gInSc1
Bambridge	Bambridg	k1gFnSc2
</s>
<s>
Henry	Henry	k1gMnSc1
Walter	Walter	k1gMnSc1
Barnett	Barnett	k1gMnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Bassano	Bassana	k1gFnSc5
</s>
<s>
Felice	Felika	k1gFnSc3
Beato	Beato	k1gNnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Beard	Beard	k1gMnSc1
</s>
<s>
Francis	Francis	k1gFnSc1
Bedford	Bedforda	k1gFnPc2
</s>
<s>
Joshua	Joshua	k1gMnSc1
Benoliel	Benoliel	k1gMnSc1
</s>
<s>
George	Georg	k1gMnSc2
Wilson	Wilson	k1gMnSc1
Bridges	Bridges	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
Bingham	Bingham	k1gInSc4
</s>
<s>
Graystone	Grayston	k1gInSc5
Bird	Bird	k1gInSc4
</s>
<s>
Samuel	Samuel	k1gMnSc1
Bourne	Bourn	k1gInSc5
</s>
<s>
Sarah	Sarah	k1gFnSc1
Anne	Ann	k1gFnSc2
Bright	Brighta	k1gFnPc2
</s>
<s>
Samuel	Samuel	k1gMnSc1
Buckle	Buckl	k1gInSc5
</s>
<s>
Julia	Julius	k1gMnSc4
Margaret	Margareta	k1gFnPc2
Cameronová	Cameronový	k2eAgNnPc4d1
</s>
<s>
Lewis	Lewis	k1gFnSc1
Carroll	Carrolla	k1gFnPc2
</s>
<s>
William	William	k1gInSc1
Crooke	Crook	k1gFnSc2
</s>
<s>
Hugh	Hugh	k1gMnSc1
Welch	Welch	k1gMnSc1
Diamond	Diamond	k1gMnSc1
</s>
<s>
Philip	Philip	k1gMnSc1
Henry	Henry	k1gMnSc1
Delamotte	Delamott	k1gMnSc5
</s>
<s>
W.	W.	kA
&	&	k?
D.	D.	kA
Downey	Downea	k1gFnSc2
</s>
<s>
Elliott	Elliott	k1gMnSc1
&	&	k?
Fry	Fry	k1gMnSc1
</s>
<s>
William	William	k6eAd1
England	England	k1gInSc1
</s>
<s>
Roger	Roger	k1gInSc1
Fenton	Fenton	k1gInSc1
</s>
<s>
Francis	Francis	k1gFnSc1
Frith	Fritha	k1gFnPc2
</s>
<s>
Peter	Peter	k1gMnSc1
Wickens	Wickensa	k1gFnPc2
Fry	Fry	k1gMnSc1
</s>
<s>
Clementina	Clementina	k1gFnSc1
Hawarden	Hawardna	k1gFnPc2
</s>
<s>
William	William	k6eAd1
Hayes	Hayes	k1gInSc1
</s>
<s>
Norman	Norman	k1gMnSc1
Heathcote	Heathcot	k1gInSc5
</s>
<s>
John	John	k1gMnSc1
Herschel	Herschel	k1gMnSc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
Horsley	Horslea	k1gFnSc2
Hinton	Hinton	k1gInSc1
</s>
<s>
Frederick	Frederick	k1gMnSc1
Hollyer	Hollyer	k1gMnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
Hughes	Hughesa	k1gFnPc2
</s>
<s>
Richard	Richard	k1gMnSc1
Keene	Keen	k1gMnSc5
</s>
<s>
William	William	k6eAd1
Edward	Edward	k1gMnSc1
Kilburn	Kilburn	k1gMnSc1
</s>
<s>
James	James	k1gMnSc1
Lafayette	Lafayett	k1gMnSc5
</s>
<s>
Martin	Martin	k1gMnSc1
Laroche	Laroch	k1gFnSc2
</s>
<s>
John	John	k1gMnSc1
Dillwyn	Dillwyn	k1gMnSc1
Llewelyn	Llewelyn	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Cockle	Cockl	k1gInSc5
Lucas	Lucas	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Wilfred	Wilfred	k1gMnSc1
Skeffington	Skeffington	k1gInSc4
Lutwidge	Lutwidge	k1gNnSc2
</s>
<s>
John	John	k1gMnSc1
Jabez	Jabez	k1gMnSc1
Edwin	Edwin	k1gMnSc1
Mayall	Mayall	k1gMnSc1
</s>
<s>
Farnham	Farnham	k6eAd1
Maxwell-Lyte	Maxwell-Lyt	k1gInSc5
</s>
<s>
Eveleen	Eveleen	k2eAgInSc1d1
Myers	Myers	k1gInSc1
</s>
<s>
William	William	k6eAd1
Eastman	Eastman	k1gMnSc1
Palmer	Palmer	k1gMnSc1
&	&	k?
Sons	Sons	k1gInSc1
</s>
<s>
William	William	k1gInSc1
Pumphrey	Pumphrea	k1gFnSc2
</s>
<s>
James	James	k1gMnSc1
Robertson	Robertson	k1gMnSc1
</s>
<s>
Henry	Henry	k1gMnSc1
Peach	Peach	k1gMnSc1
Robinson	Robinson	k1gMnSc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
Seaman	Seaman	k1gMnSc1
</s>
<s>
Charles	Charles	k1gMnSc1
Shepherd	Shepherd	k1gMnSc1
</s>
<s>
Reginald	Reginald	k1gInSc1
Southey	Southea	k1gFnSc2
</s>
<s>
Jane	Jan	k1gMnSc5
Martha	Martha	k1gMnSc1
St.	st.	kA
John	John	k1gMnSc1
</s>
<s>
Francis	Francis	k1gFnSc1
Meadow	Meadow	k1gFnSc2
Sutcliffe	Sutcliff	k1gInSc5
</s>
<s>
Constance	Constance	k1gFnSc1
Fox	fox	k1gInSc1
Talbot	Talbot	k1gInSc1
</s>
<s>
William	William	k6eAd1
Fox	fox	k1gInSc1
Talbot	Talbota	k1gFnPc2
</s>
<s>
John	John	k1gMnSc1
Thomson	Thomson	k1gMnSc1
</s>
<s>
James	James	k1gMnSc1
Valentine	Valentin	k1gMnSc5
</s>
<s>
Carl	Carl	k1gMnSc1
Vandyk	Vandyk	k1gMnSc1
</s>
<s>
Henry	Henry	k1gMnSc1
Van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Weyde	Weyd	k1gMnSc5
</s>
<s>
George	George	k1gNnSc1
Washington	Washington	k1gInSc1
Wilson	Wilson	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19981000442	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
11851928X	11851928X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2137	#num#	k4
136X	136X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79056546	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500027372	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
66462036	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79056546	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
