<p>
<s>
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnSc1	Zombie
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
protestní	protestní	k2eAgFnSc4d1	protestní
píseň	píseň	k1gFnSc4	píseň
irské	irský	k2eAgFnSc2d1	irská
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Cranberries	Cranberries	k1gMnSc1	Cranberries
z	z	k7c2	z
alba	album	k1gNnSc2	album
No	no	k9	no
Need	Need	k1gInSc4	Need
to	ten	k3xDgNnSc1	ten
Argue	Argue	k1gNnSc1	Argue
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
násilí	násilí	k1gNnSc4	násilí
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
konfliktem	konflikt	k1gInSc7	konflikt
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
naráží	narážet	k5eAaImIp3nS	narážet
zejména	zejména	k9	zejména
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
při	při	k7c6	při
bombových	bombový	k2eAgInPc6d1	bombový
útocích	útok	k1gInPc6	útok
ve	v	k7c6	v
Warringtonu	Warrington	k1gInSc6	Warrington
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc4	text
napsala	napsat	k5eAaPmAgFnS	napsat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
skupiny	skupina	k1gFnSc2	skupina
Dolores	Doloresa	k1gFnPc2	Doloresa
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Riordanová	Riordanová	k1gFnSc1	Riordanová
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
těžký	těžký	k2eAgInSc4d1	těžký
kytarový	kytarový	k2eAgInSc4d1	kytarový
riff	riff	k1gInSc4	riff
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
blízký	blízký	k2eAgInSc1d1	blízký
zvuku	zvuk	k1gInSc3	zvuk
post-grunge	postrunge	k1gNnPc2	post-grunge
<g/>
/	/	kIx~	/
<g/>
alternativního	alternativní	k2eAgInSc2d1	alternativní
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
CD	CD	kA	CD
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnSc1	Zombie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Full	Full	k1gInSc1	Full
length	length	k1gInSc1	length
album	album	k1gNnSc1	album
version	version	k1gInSc1	version
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Away	Awa	k2eAgMnPc4d1	Awa
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
Do	do	k7c2	do
not	nota	k1gFnPc2	nota
Need	Needa	k1gFnPc2	Needa
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
31	[number]	k4	31
<g/>
Obě	dva	k4xCgFnPc1	dva
B-strany	Btrana	k1gFnPc1	B-strana
nebyly	být	k5eNaImAgFnP	být
dříve	dříve	k6eAd2	dříve
vydané	vydaný	k2eAgFnPc1d1	vydaná
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Away	Awaa	k1gFnPc1	Awaa
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
použit	použít	k5eAaPmNgInS	použít
do	do	k7c2	do
soundtracku	soundtrack	k1gInSc2	soundtrack
filmu	film	k1gInSc2	film
Clueless	Cluelessa	k1gFnPc2	Cluelessa
<g/>
.	.	kIx.	.
<g/>
Limited	limited	k2eAgInSc1d1	limited
Edition	Edition	k1gInSc1	Edition
CD	CD	kA	CD
singl	singl	k1gInSc1	singl
<g/>
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnSc1	Zombie
<g/>
"	"	kIx"	"
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Waltzing	Waltzing	k1gInSc1	Waltzing
Back	Back	k1gInSc1	Back
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Live	Live	k1gInSc1	Live
at	at	k?	at
the	the	k?	the
Fleadh	Fleadh	k1gInSc1	Fleadh
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Linger	Linger	k1gInSc4	Linger
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Live	Live	k1gInSc1	Live
at	at	k?	at
the	the	k?	the
Fleadh	Fleadh	k1gInSc1	Fleadh
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
<g/>
CD	CD	kA	CD
Promo	Proma	k1gFnSc5	Proma
<g/>
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnPc4	Zombie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Edit	Edita	k1gFnPc2	Edita
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnPc4	Zombie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Album	album	k1gNnSc1	album
version	version	k1gInSc1	version
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
67	[number]	k4	67
"	"	kIx"	"
<g/>
singl	singl	k1gInSc1	singl
<g/>
"	"	kIx"	"
<g/>
Zombie	Zombie	k1gFnSc1	Zombie
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Away	Awa	k2eAgMnPc4d1	Awa
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Pozice	pozice	k1gFnPc1	pozice
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zombie	Zombie	k1gFnSc2	Zombie
(	(	kIx(	(
<g/>
singel	singel	k1gMnSc1	singel
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
