<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
byla	být	k5eAaImAgFnS	být
kodifikována	kodifikován	k2eAgFnSc1d1	kodifikována
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zjednoznačnění	zjednoznačnění	k1gNnSc3	zjednoznačnění
přiřazení	přiřazení	k1gNnSc2	přiřazení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
symbolů	symbol	k1gInPc2	symbol
ke	k	k7c3	k
zvukům	zvuk	k1gInPc3	zvuk
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
symboly	symbol	k1gInPc4	symbol
(	(	kIx(	(
<g/>
wi	wi	k?	wi
<g/>
,	,	kIx,	,
we	we	k?	we
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
