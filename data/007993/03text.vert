<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
neboli	neboli	k8xC	neboli
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
či	či	k8xC	či
také	také	k9	také
absolutní	absolutní	k2eAgInSc1d1	absolutní
trest	trest	k1gInSc1	trest
je	být	k5eAaImIp3nS	být
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
usmrcení	usmrcení	k1gNnSc4	usmrcení
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
popravu	poprava	k1gFnSc4	poprava
<g/>
)	)	kIx)	)
odsouzeného	odsouzený	k2eAgMnSc2d1	odsouzený
člověka	člověk	k1gMnSc2	člověk
za	za	k7c4	za
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
platného	platný	k2eAgNnSc2d1	platné
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
možné	možný	k2eAgNnSc1d1	možné
tento	tento	k3xDgInSc4	tento
trest	trest	k1gInSc4	trest
uložit	uložit	k5eAaPmF	uložit
(	(	kIx(	(
<g/>
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
byly	být	k5eAaImAgFnP	být
takovéto	takovýto	k3xDgFnPc4	takovýto
trestné	trestný	k2eAgFnPc4d1	trestná
činy	čina	k1gFnPc4	čina
nazývány	nazýván	k2eAgFnPc4d1	nazývána
hrdelními	hrdelní	k2eAgInPc7d1	hrdelní
zločiny	zločin	k1gInPc7	zločin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
definitivnosti	definitivnost	k1gFnSc3	definitivnost
(	(	kIx(	(
<g/>
nemožnosti	nemožnost	k1gFnPc1	nemožnost
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
nápravy	náprava	k1gFnSc2	náprava
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vykonání	vykonání	k1gNnSc6	vykonání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
trest	trest	k1gInSc4	trest
velmi	velmi	k6eAd1	velmi
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
zastánci	zastánce	k1gMnPc1	zastánce
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
spravedlivý	spravedlivý	k2eAgInSc4d1	spravedlivý
trest	trest	k1gInSc4	trest
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
závažné	závažný	k2eAgInPc4d1	závažný
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
recidivu	recidiva	k1gFnSc4	recidiva
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
významné	významný	k2eAgInPc4d1	významný
odstrašující	odstrašující	k2eAgInPc4d1	odstrašující
(	(	kIx(	(
<g/>
preventivní	preventivní	k2eAgInPc4d1	preventivní
<g/>
)	)	kIx)	)
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
kromě	kromě	k7c2	kromě
morálních	morální	k2eAgInPc2d1	morální
argumentů	argument	k1gInPc2	argument
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
každého	každý	k3xTgMnSc4	každý
na	na	k7c4	na
život	život	k1gInSc4	život
především	především	k9	především
na	na	k7c4	na
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
justičního	justiční	k2eAgInSc2d1	justiční
omylu	omyl	k1gInSc2	omyl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nenapravitelný	napravitelný	k2eNgInSc1d1	nenapravitelný
<g/>
,	,	kIx,	,
zneužití	zneužití	k1gNnSc1	zneužití
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
k	k	k7c3	k
justiční	justiční	k2eAgFnSc3d1	justiční
vraždě	vražda	k1gFnSc3	vražda
<g/>
,	,	kIx,	,
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
odstrašující	odstrašující	k2eAgInSc4d1	odstrašující
účinek	účinek	k1gInSc4	účinek
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
navíc	navíc	k6eAd1	navíc
poprava	poprava	k1gFnSc1	poprava
bývá	bývat	k5eAaImIp3nS	bývat
podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
dražší	drahý	k2eAgMnSc1d2	dražší
než	než	k8xS	než
doživotní	doživotní	k2eAgNnSc1d1	doživotní
vězení	vězení	k1gNnSc1	vězení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
postupně	postupně	k6eAd1	postupně
rušen	rušit	k5eAaImNgInS	rušit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgInPc2d1	evropský
a	a	k8xC	a
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
nadále	nadále	k6eAd1	nadále
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mj.	mj.	kA	mj.
některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
států	stát	k1gInPc2	stát
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
pouze	pouze	k6eAd1	pouze
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
trestem	trest	k1gInSc7	trest
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byla	být	k5eAaImAgFnS	být
osobní	osobní	k2eAgFnSc1d1	osobní
msta	msta	k1gFnSc1	msta
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
bylo	být	k5eAaImAgNnS	být
zabití	zabití	k1gNnSc1	zabití
pachatele	pachatel	k1gMnSc2	pachatel
činem	čin	k1gInSc7	čin
odplaty	odplata	k1gFnSc2	odplata
a	a	k8xC	a
zárukou	záruka	k1gFnSc7	záruka
udržení	udržení	k1gNnSc2	udržení
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
upevňováním	upevňování	k1gNnSc7	upevňování
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
byla	být	k5eAaImAgFnS	být
osobní	osobní	k2eAgFnSc1d1	osobní
msta	msta	k1gFnSc1	msta
postupně	postupně	k6eAd1	postupně
omezována	omezován	k2eAgFnSc1d1	omezována
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc4	trest
začala	začít	k5eAaPmAgFnS	začít
vykonávat	vykonávat	k5eAaImF	vykonávat
státní	státní	k2eAgFnSc1d1	státní
autorita	autorita	k1gFnSc1	autorita
jako	jako	k8xC	jako
následek	následek	k1gInSc1	následek
porušení	porušení	k1gNnSc2	porušení
zákona	zákon	k1gInSc2	zákon
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
exemplárně	exemplárně	k6eAd1	exemplárně
potrestat	potrestat	k5eAaPmF	potrestat
pachatele	pachatel	k1gMnSc4	pachatel
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
ho	on	k3xPp3gInSc4	on
vyřadit	vyřadit	k5eAaPmF	vyřadit
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
krátkodobě	krátkodobě	k6eAd1	krátkodobě
zrušen	zrušit	k5eAaPmNgInS	zrušit
už	už	k6eAd1	už
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
myšlenka	myšlenka	k1gFnSc1	myšlenka
zakázat	zakázat	k5eAaPmF	zakázat
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
až	až	k9	až
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
filozof	filozof	k1gMnSc1	filozof
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
právní	právní	k2eAgMnSc1d1	právní
reformátor	reformátor	k1gMnSc1	reformátor
Jeremy	Jerema	k1gFnSc2	Jerema
Bentham	Bentham	k1gInSc1	Bentham
a	a	k8xC	a
zejména	zejména	k9	zejména
právník	právník	k1gMnSc1	právník
Cesare	Cesar	k1gMnSc5	Cesar
Beccaria	Beccarium	k1gNnSc2	Beccarium
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
publikoval	publikovat	k5eAaBmAgMnS	publikovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
převratné	převratný	k2eAgNnSc1d1	převratné
dílo	dílo	k1gNnSc1	dílo
Dei	Dei	k1gFnSc2	Dei
Delitti	Delitť	k1gFnSc2	Delitť
e	e	k0	e
Delle	Delle	k1gInSc4	Delle
Pene	Pene	k1gNnPc7	Pene
(	(	kIx(	(
<g/>
O	o	k7c6	o
zločinech	zločin	k1gInPc6	zločin
a	a	k8xC	a
trestech	trest	k1gInPc6	trest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zrušil	zrušit	k5eAaPmAgInS	zrušit
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
habsburského	habsburský	k2eAgMnSc2d1	habsburský
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
krátkodobě	krátkodobě	k6eAd1	krátkodobě
de	de	k?	de
facto	facto	k1gNnSc4	facto
zrušen	zrušit	k5eAaPmNgMnS	zrušit
za	za	k7c2	za
panování	panování	k1gNnSc2	panování
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
ohlasy	ohlas	k1gInPc1	ohlas
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
až	až	k9	až
o	o	k7c4	o
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
zrušil	zrušit	k5eAaPmAgInS	zrušit
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
americký	americký	k2eAgInSc1d1	americký
stát	stát	k1gInSc1	stát
Michigan	Michigan	k1gInSc4	Michigan
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
převzato	převzít	k5eAaPmNgNnS	převzít
trestní	trestní	k2eAgNnSc1d1	trestní
právo	právo	k1gNnSc1	právo
z	z	k7c2	z
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
zachován	zachován	k2eAgInSc4d1	zachován
a	a	k8xC	a
uplatňován	uplatňován	k2eAgInSc4d1	uplatňován
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
Zatímní	zatímní	k2eAgInSc1d1	zatímní
návrh	návrh	k1gInSc1	návrh
obecné	obecný	k2eAgFnSc2d1	obecná
části	část	k1gFnSc2	část
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
možné	možný	k2eAgNnSc1d1	možné
uložit	uložit	k5eAaPmF	uložit
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
stanném	stanný	k2eAgNnSc6d1	stanné
právu	právo	k1gNnSc6	právo
nebo	nebo	k8xC	nebo
u	u	k7c2	u
doživotně	doživotně	k6eAd1	doživotně
odsouzeného	odsouzený	k2eAgInSc2d1	odsouzený
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
opětovně	opětovně	k6eAd1	opětovně
spáchal	spáchat	k5eAaPmAgInS	spáchat
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
byl	být	k5eAaImAgInS	být
odsouzen	odsouzen	k2eAgInSc1d1	odsouzen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1933	[number]	k4	1933
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
devět	devět	k4xCc1	devět
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
udělil	udělit	k5eAaPmAgMnS	udělit
naprosté	naprostý	k2eAgFnSc3d1	naprostá
většině	většina	k1gFnSc3	většina
odsouzenců	odsouzenec	k1gMnPc2	odsouzenec
milost	milost	k1gFnSc1	milost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
(	(	kIx(	(
<g/>
vydání	vydání	k1gNnSc6	vydání
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
1934	[number]	k4	1934
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
,	,	kIx,	,
využívaly	využívat	k5eAaImAgFnP	využívat
se	se	k3xPyFc4	se
ale	ale	k9	ale
spíše	spíše	k9	spíše
jiné	jiný	k2eAgInPc4d1	jiný
tresty	trest	k1gInPc4	trest
<g/>
:	:	kIx,	:
těžší	těžký	k2eAgInPc4d2	těžší
tresty	trest	k1gInPc4	trest
žaláře	žalář	k1gInSc2	žalář
a	a	k8xC	a
doživotí	doživotí	k1gNnSc2	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1939	[number]	k4	1939
až	až	k8xS	až
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
často	často	k6eAd1	často
využíván	využívat	k5eAaPmNgInS	využívat
(	(	kIx(	(
<g/>
jen	jen	k9	jen
na	na	k7c6	na
pankrácké	pankrácký	k2eAgFnSc6d1	Pankrácká
gilotině	gilotina	k1gFnSc6	gilotina
přes	přes	k7c4	přes
1000	[number]	k4	1000
popravených	popravený	k2eAgFnPc2d1	popravená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1946	[number]	k4	1946
až	až	k8xS	až
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
udělován	udělovat	k5eAaImNgInS	udělovat
hojně	hojně	k6eAd1	hojně
<g/>
,	,	kIx,	,
především	především	k9	především
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
retribučních	retribuční	k2eAgInPc2d1	retribuční
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zneužila	zneužít	k5eAaPmAgFnS	zneužít
komunistická	komunistický	k2eAgFnSc1d1	komunistická
diktatura	diktatura	k1gFnSc1	diktatura
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
justičních	justiční	k2eAgFnPc2d1	justiční
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
případy	případ	k1gInPc1	případ
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
a	a	k8xC	a
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Slánského	Slánský	k1gMnSc2	Slánský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
bylo	být	k5eAaImAgNnS	být
provedení	provedení	k1gNnSc1	provedení
popravy	poprava	k1gFnSc2	poprava
omezeno	omezen	k2eAgNnSc1d1	omezeno
jen	jen	k9	jen
na	na	k7c6	na
oběšení	oběšení	k1gNnSc6	oběšení
(	(	kIx(	(
<g/>
při	při	k7c6	při
výjimečném	výjimečný	k2eAgInSc6d1	výjimečný
stavu	stav	k1gInSc6	stav
zákon	zákon	k1gInSc1	zákon
připouštěl	připouštět	k5eAaImAgInS	připouštět
zastřelení	zastřelení	k1gNnSc4	zastřelení
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
ale	ale	k9	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
použita	použít	k5eAaPmNgFnS	použít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
historika	historik	k1gMnSc2	historik
Petra	Petr	k1gMnSc2	Petr
Blažka	Blažek	k1gMnSc2	Blažek
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
několika	několik	k4yIc3	několik
popravám	poprava	k1gFnPc3	poprava
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
použit	použít	k5eAaPmNgInS	použít
také	také	k9	také
tzv.	tzv.	kA	tzv.
trhací	trhací	k2eAgInSc4d1	trhací
stůl	stůl	k1gInSc4	stůl
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
odsouzence	odsouzenec	k1gMnSc2	odsouzenec
byla	být	k5eAaImAgFnS	být
připoutána	připoutat	k5eAaPmNgFnS	připoutat
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
části	část	k1gFnSc3	část
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc4	tělo
k	k	k7c3	k
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
prudkým	prudký	k2eAgNnSc7d1	prudké
odtržením	odtržení	k1gNnSc7	odtržení
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
zlomen	zlomen	k2eAgInSc1d1	zlomen
vaz	vaz	k1gInSc1	vaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
oficiální	oficiální	k2eAgInPc1d1	oficiální
prameny	pramen	k1gInPc1	pramen
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
popravy	poprava	k1gFnSc2	poprava
ale	ale	k8xC	ale
neexistují	existovat	k5eNaImIp3nP	existovat
a	a	k8xC	a
i	i	k9	i
ohledací	ohledací	k2eAgInPc1d1	ohledací
listy	list	k1gInPc1	list
údajných	údajný	k2eAgFnPc2d1	údajná
obětí	oběť	k1gFnPc2	oběť
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
popravy	poprava	k1gFnSc2	poprava
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc3	smrt
strangulaci	strangulace	k1gFnSc3	strangulace
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
poprava	poprava	k1gFnSc1	poprava
oběšením	oběšení	k1gNnPc3	oběšení
vykonávána	vykonáván	k2eAgFnSc1d1	vykonávána
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
popravčím	popravčí	k2eAgNnSc6d1	popravčí
prkně	prkno	k1gNnSc6	prkno
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
bolestivou	bolestivý	k2eAgFnSc7d1	bolestivá
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
zděděnou	zděděný	k2eAgFnSc4d1	zděděná
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
byl	být	k5eAaImAgMnS	být
přiveden	přivést	k5eAaPmNgMnS	přivést
k	k	k7c3	k
vysokému	vysoký	k2eAgNnSc3d1	vysoké
prknu	prkno	k1gNnSc3	prkno
<g/>
,	,	kIx,	,
na	na	k7c4	na
krk	krk	k1gInSc4	krk
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nasazena	nasazen	k2eAgFnSc1d1	nasazena
oprátka	oprátka	k1gFnSc1	oprátka
a	a	k8xC	a
v	v	k7c6	v
podpaží	podpaží	k1gNnSc6	podpaží
byl	být	k5eAaImAgInS	být
připoután	připoutat	k5eAaPmNgInS	připoutat
řemenem	řemen	k1gInSc7	řemen
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yIgNnSc2	který
ho	on	k3xPp3gInSc4	on
kat	kat	k1gMnSc1	kat
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
řemen	řemen	k1gInSc4	řemen
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
a	a	k8xC	a
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
zůstal	zůstat	k5eAaPmAgMnS	zůstat
viset	viset	k5eAaImF	viset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
oprátce	oprátka	k1gFnSc6	oprátka
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
uškrtil	uškrtit	k5eAaPmAgMnS	uškrtit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
pomocníci	pomocník	k1gMnPc1	pomocník
kata	kata	k9	kata
pomocí	pomocí	k7c2	pomocí
druhého	druhý	k4xOgInSc2	druhý
provazu	provaz	k1gInSc2	provaz
tahali	tahat	k5eAaImAgMnP	tahat
za	za	k7c4	za
odsouzencovy	odsouzencův	k2eAgFnPc4d1	odsouzencova
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
urychlili	urychlit	k5eAaPmAgMnP	urychlit
strangulaci	strangulace	k1gFnSc4	strangulace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
používána	používán	k2eAgFnSc1d1	používána
o	o	k7c4	o
něco	něco	k3yInSc4	něco
humánnější	humánní	k2eAgFnSc1d2	humánnější
tzv.	tzv.	kA	tzv.
metoda	metoda	k1gFnSc1	metoda
short	short	k1gInSc1	short
drop	drop	k1gMnSc1	drop
(	(	kIx(	(
<g/>
krátkého	krátký	k2eAgInSc2d1	krátký
pádu	pád	k1gInSc2	pád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
byla	být	k5eAaImAgFnS	být
oprátka	oprátka	k1gFnSc1	oprátka
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
hák	hák	k1gInSc4	hák
visící	visící	k2eAgInSc4d1	visící
ve	v	k7c6	v
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgNnSc7	jenž
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
propadlo	propadlo	k1gNnSc1	propadlo
<g/>
.	.	kIx.	.
</s>
<s>
Katovi	katův	k2eAgMnPc1d1	katův
pomocníci	pomocník	k1gMnPc1	pomocník
vložili	vložit	k5eAaPmAgMnP	vložit
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
oprátku	oprátka	k1gFnSc4	oprátka
na	na	k7c4	na
krk	krk	k1gInSc4	krk
a	a	k8xC	a
kat	kat	k1gMnSc1	kat
propadlo	propadlo	k1gNnSc4	propadlo
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Propadliště	propadliště	k1gNnSc1	propadliště
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
osmdesát	osmdesát	k4xCc1	osmdesát
centimetrů	centimetr	k1gInPc2	centimetr
hluboké	hluboký	k2eAgInPc1d1	hluboký
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
metodě	metoda	k1gFnSc6	metoda
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
smrti	smrt	k1gFnSc3	smrt
zlomením	zlomení	k1gNnSc7	zlomení
vazu	vaz	k1gInSc2	vaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
několikaminutovému	několikaminutový	k2eAgNnSc3d1	několikaminutové
škrcení	škrcení	k1gNnSc3	škrcení
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
long	long	k1gMnSc1	long
drop	drop	k1gMnSc1	drop
<g/>
,	,	kIx,	,
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
smrt	smrt	k1gFnSc4	smrt
(	(	kIx(	(
<g/>
propadliště	propadliště	k1gNnSc2	propadliště
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
metodě	metoda	k1gFnSc6	metoda
2-3	[number]	k4	2-3
metry	metr	k1gInPc1	metr
hluboké	hluboký	k2eAgInPc1d1	hluboký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
nikdy	nikdy	k6eAd1	nikdy
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Popravčí	popravčí	k1gMnPc1	popravčí
byli	být	k5eAaImAgMnP	být
vybírání	vybírání	k1gNnSc4	vybírání
z	z	k7c2	z
vězeňských	vězeňský	k2eAgMnPc2d1	vězeňský
dozorců	dozorce	k1gMnPc2	dozorce
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
tajná	tajný	k2eAgNnPc1d1	tajné
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
uložen	uložen	k2eAgInSc1d1	uložen
těhotné	těhotný	k2eAgFnSc3d1	těhotná
ženě	žena	k1gFnSc3	žena
a	a	k8xC	a
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
spáchání	spáchání	k1gNnSc2	spáchání
zločinu	zločin	k1gInSc2	zločin
nebylo	být	k5eNaImAgNnS	být
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
ukládán	ukládat	k5eAaImNgInS	ukládat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
uvěznění	uvěznění	k1gNnSc4	uvěznění
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
přineslo	přinést	k5eAaPmAgNnS	přinést
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
také	také	k9	také
ukládán	ukládat	k5eAaImNgInS	ukládat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
ochránit	ochránit	k5eAaPmF	ochránit
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
jen	jen	k9	jen
po	po	k7c6	po
prošetření	prošetření	k1gNnSc6	prošetření
případu	případ	k1gInSc2	případ
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
schválení	schválení	k1gNnSc6	schválení
rozsudku	rozsudek	k1gInSc2	rozsudek
a	a	k8xC	a
po	po	k7c6	po
zamítnutí	zamítnutí	k1gNnSc6	zamítnutí
všech	všecek	k3xTgFnPc2	všecek
žádostí	žádost	k1gFnPc2	žádost
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
popravy	poprava	k1gFnSc2	poprava
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
předseda	předseda	k1gMnSc1	předseda
trestního	trestní	k2eAgInSc2d1	trestní
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
prokurátor	prokurátor	k1gMnSc1	prokurátor
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
věznice	věznice	k1gFnSc2	věznice
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
350	[number]	k4	350
lidí	člověk	k1gMnPc2	člověk
petici	petice	k1gFnSc4	petice
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
)	)	kIx)	)
žádající	žádající	k2eAgNnSc1d1	žádající
zrušení	zrušení	k1gNnSc1	zrušení
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
s	s	k7c7	s
názvem	název	k1gInSc7	název
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejtemnějších	temný	k2eAgFnPc2d3	nejtemnější
skvrn	skvrna	k1gFnPc2	skvrna
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
našeho	náš	k3xOp1gInSc2	náš
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Petice	petice	k1gFnSc1	petice
byla	být	k5eAaImAgFnS	být
zaslána	zaslat	k5eAaPmNgFnS	zaslat
Federálnímu	federální	k2eAgNnSc3d1	federální
shromáždění	shromáždění	k1gNnSc3	shromáždění
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
mnohými	mnohý	k2eAgFnPc7d1	mnohá
známými	známý	k2eAgFnPc7d1	známá
osobnostmi	osobnost	k1gFnPc7	osobnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kyncl	Kyncl	k1gMnSc1	Kyncl
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
také	také	k9	také
výslovně	výslovně	k6eAd1	výslovně
zakázán	zakázat	k5eAaPmNgInS	zakázat
ústavně	ústavně	k6eAd1	ústavně
<g/>
,	,	kIx,	,
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
6	[number]	k4	6
Listiny	listina	k1gFnSc2	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
poprava	poprava	k1gFnSc1	poprava
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
byla	být	k5eAaImAgFnS	být
vykonána	vykonán	k2eAgFnSc1d1	vykonána
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1989	[number]	k4	1989
a	a	k8xC	a
na	na	k7c4	na
území	území	k1gNnSc4	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
období	období	k1gNnSc2	období
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
1217	[number]	k4	1217
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
61	[number]	k4	61
%	%	kIx~	%
za	za	k7c4	za
retribuční	retribuční	k2eAgInPc4d1	retribuční
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
21	[number]	k4	21
%	%	kIx~	%
za	za	k7c4	za
politické	politický	k2eAgInPc4d1	politický
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
a	a	k8xC	a
18	[number]	k4	18
%	%	kIx~	%
za	za	k7c4	za
kriminální	kriminální	k2eAgInPc4d1	kriminální
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ústavněprávního	ústavněprávní	k2eAgInSc2d1	ústavněprávní
zákazu	zákaz	k1gInSc2	zákaz
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
zavázala	zavázat	k5eAaPmAgFnS	zavázat
k	k	k7c3	k
nepoužívání	nepoužívání	k1gNnSc3	nepoužívání
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
ratifikací	ratifikace	k1gFnPc2	ratifikace
Protokolu	protokol	k1gInSc2	protokol
č.	č.	k?	č.
6	[number]	k4	6
k	k	k7c3	k
Úmluvě	úmluva	k1gFnSc3	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
sociologové	sociolog	k1gMnPc1	sociolog
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
pojetím	pojetí	k1gNnSc7	pojetí
obecného	obecný	k2eAgInSc2d1	obecný
významu	význam	k1gInSc2	význam
trestu	trest	k1gInSc2	trest
a	a	k8xC	a
aplikací	aplikace	k1gFnPc2	aplikace
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
evropská	evropský	k2eAgFnSc1d1	Evropská
tradice	tradice	k1gFnSc1	tradice
vnímá	vnímat	k5eAaImIp3nS	vnímat
trest	trest	k1gInSc4	trest
jako	jako	k8xC	jako
výchovný	výchovný	k2eAgInSc4d1	výchovný
prostředek	prostředek	k1gInSc4	prostředek
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
napravení	napravení	k1gNnSc3	napravení
odsouzeného	odsouzený	k1gMnSc2	odsouzený
<g/>
,	,	kIx,	,
americké	americký	k2eAgNnSc1d1	americké
pojetí	pojetí	k1gNnSc1	pojetí
práva	právo	k1gNnSc2	právo
dává	dávat	k5eAaImIp3nS	dávat
trestu	trest	k1gInSc3	trest
význam	význam	k1gInSc1	význam
odplaty	odplata	k1gFnSc2	odplata
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
velmi	velmi	k6eAd1	velmi
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
zrušení	zrušení	k1gNnSc1	zrušení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
88	[number]	k4	88
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
trestních	trestní	k2eAgInPc6d1	trestní
řádech	řád	k1gInPc6	řád
a	a	k8xC	a
popravy	poprava	k1gFnPc1	poprava
nevykonávají	vykonávat	k5eNaImIp3nP	vykonávat
<g/>
,	,	kIx,	,
11	[number]	k4	11
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
jen	jen	k9	jen
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
stavech	stav	k1gInPc6	stav
<g/>
,	,	kIx,	,
29	[number]	k4	29
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
trestním	trestní	k2eAgInSc6d1	trestní
řádu	řád	k1gInSc6	řád
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
10	[number]	k4	10
let	léto	k1gNnPc2	léto
neprovedly	provést	k5eNaPmAgFnP	provést
žádnou	žádný	k3yNgFnSc4	žádný
popravu	poprava	k1gFnSc4	poprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
69	[number]	k4	69
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
využívají	využívat	k5eAaImIp3nP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
trestní	trestní	k2eAgInSc1d1	trestní
řád	řád	k1gInSc1	řád
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
tento	tento	k3xDgInSc4	tento
trest	trest	k1gInSc4	trest
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
AI	AI	kA	AI
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
ve	v	k7c6	v
22	[number]	k4	22
zemích	zem	k1gFnPc6	zem
popraveno	popraven	k2eAgNnSc1d1	popraveno
2148	[number]	k4	2148
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
provedli	provést	k5eAaPmAgMnP	provést
popravčí	popravčí	k1gMnPc1	popravčí
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pětinový	pětinový	k2eAgInSc4d1	pětinový
pokles	pokles	k1gInSc4	pokles
počtu	počet	k1gInSc2	počet
poprav	poprava	k1gFnPc2	poprava
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
statistiky	statistika	k1gFnPc1	statistika
států	stát	k1gInPc2	stát
jsou	být	k5eAaImIp3nP	být
kompletní	kompletní	k2eAgInPc1d1	kompletní
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
popravováno	popravovat	k5eAaImNgNnS	popravovat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
zemích	zem	k1gFnPc6	zem
bývá	bývat	k5eAaImIp3nS	bývat
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
zneužíván	zneužíván	k2eAgInSc1d1	zneužíván
totalitní	totalitní	k2eAgInSc1d1	totalitní
mocí	moc	k1gFnSc7	moc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
vyškrtly	vyškrtnout	k5eAaPmAgFnP	vyškrtnout
dvě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
Libérie	Libérie	k1gFnSc1	Libérie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
různé	různý	k2eAgInPc1d1	různý
stát	stát	k5eAaImF	stát
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
devatenácti	devatenáct	k4xCc2	devatenáct
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
federálně	federálně	k6eAd1	federálně
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972-1976	[number]	k4	1972-1976
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozsudku	rozsudek	k1gInSc2	rozsudek
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Furman	furman	k1gMnSc1	furman
v.	v.	k?	v.
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
obnoven	obnovit	k5eAaPmNgInS	obnovit
rozsudkem	rozsudek	k1gInSc7	rozsudek
Gregg	Gregg	k1gInSc4	Gregg
v.	v.	k?	v.
Georgia	Georgius	k1gMnSc2	Georgius
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgFnP	být
zpřísněny	zpřísněn	k2eAgFnPc1d1	zpřísněna
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
vykonávání	vykonávání	k1gNnSc4	vykonávání
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
nejvíce	hodně	k6eAd3	hodně
trestů	trest	k1gInPc2	trest
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
vykonáváno	vykonávat	k5eAaImNgNnS	vykonávat
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgInS	popravit
400	[number]	k4	400
<g/>
.	.	kIx.	.
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
popraveno	popravit	k5eAaPmNgNnS	popravit
15	[number]	k4	15
269	[number]	k4	269
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
probíhají	probíhat	k5eAaImIp3nP	probíhat
popravy	poprava	k1gFnPc1	poprava
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
nejčastěji	často	k6eAd3	často
stětím	stětit	k5eAaImIp1nS	stětit
hlavy	hlava	k1gFnPc4	hlava
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
v	v	k7c6	v
Islámském	islámský	k2eAgInSc6d1	islámský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
popraven	popravit	k5eAaPmNgInS	popravit
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arabii	Arabie	k1gFnSc6	Arabie
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arabii	Arabie	k1gFnSc4	Arabie
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
množstvím	množství	k1gNnSc7	množství
poprav	poprava	k1gFnPc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Ukamenování	ukamenování	k1gNnSc1	ukamenování
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
islámském	islámský	k2eAgNnSc6d1	islámské
právu	právo	k1gNnSc6	právo
šaría	šaríum	k1gNnSc2	šaríum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
CVVM	CVVM	kA	CVVM
z	z	k7c2	z
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
se	s	k7c7	s
62	[number]	k4	62
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
ČR	ČR	kA	ČR
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
je	být	k5eAaImIp3nS	být
32	[number]	k4	32
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
průzkumech	průzkum	k1gInPc6	průzkum
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
zastánců	zastánce	k1gMnPc2	zastánce
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
kolem	kolem	k7c2	kolem
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
již	již	k6eAd1	již
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
humanismu	humanismus	k1gInSc2	humanismus
polemiky	polemika	k1gFnPc1	polemika
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
argumenty	argument	k1gInPc1	argument
zastánců	zastánce	k1gMnPc2	zastánce
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
odstrašení	odstrašení	k1gNnSc6	odstrašení
ostatních	ostatní	k2eAgMnPc2d1	ostatní
případných	případný	k2eAgMnPc2d1	případný
pachatelů	pachatel	k1gMnPc2	pachatel
<g/>
,	,	kIx,	,
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
recidivy	recidiva	k1gFnSc2	recidiva
(	(	kIx(	(
<g/>
popravený	popravený	k2eAgMnSc1d1	popravený
zločinec	zločinec	k1gMnSc1	zločinec
byl	být	k5eAaImAgMnS	být
zaručeně	zaručeně	k6eAd1	zaručeně
zneškodněn	zneškodněn	k2eAgMnSc1d1	zneškodněn
<g/>
)	)	kIx)	)
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jistá	jistý	k2eAgFnSc1d1	jistá
pojistka	pojistka	k1gFnSc1	pojistka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
recidiva	recidiva	k1gFnSc1	recidiva
vůbec	vůbec	k9	vůbec
nenastala	nastat	k5eNaPmAgFnS	nastat
<g/>
,	,	kIx,	,
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
může	moct	k5eAaImIp3nS	moct
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
eugeniků	eugenik	k1gMnPc2	eugenik
snížit	snížit	k5eAaPmF	snížit
kriminalitu	kriminalita	k1gFnSc4	kriminalita
díky	díky	k7c3	díky
redukci	redukce	k1gFnSc3	redukce
genů	gen	k1gInPc2	gen
vedoucích	vedoucí	k1gMnPc2	vedoucí
ke	k	k7c3	k
kriminálnímu	kriminální	k2eAgNnSc3d1	kriminální
chování	chování	k1gNnSc3	chování
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ekvivalentní	ekvivalentní	k2eAgFnSc4d1	ekvivalentní
odplatu	odplata	k1gFnSc4	odplata
(	(	kIx(	(
<g/>
smrt	smrt	k1gFnSc4	smrt
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jediný	jediný	k2eAgInSc1d1	jediný
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
trest	trest	k1gInSc1	trest
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nějakého	nějaký	k3yIgInSc2	nějaký
velice	velice	k6eAd1	velice
brutálního	brutální	k2eAgInSc2d1	brutální
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ignorovat	ignorovat	k5eAaImF	ignorovat
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
povětšinou	povětšinou	k6eAd1	povětšinou
přikloněno	přikloněn	k2eAgNnSc1d1	přikloněno
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
věznění	věznění	k1gNnSc1	věznění
zločinců	zločinec	k1gMnPc2	zločinec
představuje	představovat	k5eAaImIp3nS	představovat
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
finanční	finanční	k2eAgFnPc4d1	finanční
potřeby	potřeba	k1gFnPc4	potřeba
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
než	než	k8xS	než
poprava	poprava	k1gFnSc1	poprava
zmenší	zmenšit	k5eAaPmIp3nS	zmenšit
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
vězňů	vězeň	k1gMnPc2	vězeň
-	-	kIx~	-
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
světové	světový	k2eAgFnPc4d1	světová
věznice	věznice	k1gFnPc4	věznice
přeplněné	přeplněný	k2eAgFnPc4d1	přeplněná
praktičnost	praktičnost	k1gFnSc4	praktičnost
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
;	;	kIx,	;
popravený	popravený	k2eAgMnSc1d1	popravený
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
vrah	vrah	k1gMnSc1	vrah
už	už	k6eAd1	už
<g />
.	.	kIx.	.
</s>
<s>
nepředstavuje	představovat	k5eNaImIp3nS	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
vrah	vrah	k1gMnSc1	vrah
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
může	moct	k5eAaImIp3nS	moct
jistou	jistý	k2eAgFnSc4d1	jistá
hrozbu	hrozba	k1gFnSc4	hrozba
stále	stále	k6eAd1	stále
představovat	představovat	k5eAaImF	představovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
může	moct	k5eAaImIp3nS	moct
po	po	k7c6	po
případném	případný	k2eAgNnSc6d1	případné
propuštění	propuštění	k1gNnSc6	propuštění
svůj	svůj	k3xOyFgInSc1	svůj
čin	čin	k1gInSc1	čin
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
ostrahu	ostraha	k1gFnSc4	ostraha
věznice	věznice	k1gFnSc2	věznice
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hrozbou	hrozba	k1gFnSc7	hrozba
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
budou	být	k5eAaImBp3nP	být
chtít	chtít	k5eAaImF	chtít
osvobodit	osvobodit	k5eAaPmF	osvobodit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
napadení	napadení	k1gNnSc2	napadení
věznice	věznice	k1gFnSc2	věznice
nebo	nebo	k8xC	nebo
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
rukojmí	rukojmí	k1gNnPc4	rukojmí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
zajmou	zajmout	k5eAaPmIp3nP	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
argumenty	argument	k1gInPc1	argument
odpůrců	odpůrce	k1gMnPc2	odpůrce
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
:	:	kIx,	:
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
nemorální	morální	k2eNgMnSc1d1	nemorální
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
stejného	stejný	k2eAgInSc2d1	stejný
deliktu	delikt	k1gInSc2	delikt
jako	jako	k8xS	jako
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrozba	hrozba	k1gFnSc1	hrozba
justičního	justiční	k2eAgInSc2d1	justiční
omylu	omyl	k1gInSc2	omyl
(	(	kIx(	(
<g/>
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
nevratný	vratný	k2eNgInSc1d1	nevratný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c4	o
pouhý	pouhý	k2eAgInSc4d1	pouhý
akt	akt	k1gInSc4	akt
msty	msta	k1gFnSc2	msta
(	(	kIx(	(
<g/>
zločincův	zločincův	k2eAgInSc1d1	zločincův
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
hraje	hrát	k5eAaImIp3nS	hrát
jen	jen	k9	jen
určitou	určitý	k2eAgFnSc4d1	určitá
společenskou	společenský	k2eAgFnSc4d1	společenská
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dle	dle	k7c2	dle
statistik	statistika	k1gFnPc2	statistika
je	být	k5eAaImIp3nS	být
popravena	popravit	k5eAaPmNgFnS	popravit
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
vrahů	vrah	k1gMnPc2	vrah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
prokázala	prokázat	k5eAaPmAgFnS	prokázat
odstrašující	odstrašující	k2eAgInSc4d1	odstrašující
účinek	účinek	k1gInSc4	účinek
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
vražd	vražda	k1gFnPc2	vražda
se	se	k3xPyFc4	se
neplánuje	plánovat	k5eNaImIp3nS	plánovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g />
.	.	kIx.	.
</s>
<s>
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
oponují	oponovat	k5eAaImIp3nP	oponovat
i	i	k9	i
vysokými	vysoký	k2eAgInPc7d1	vysoký
finančními	finanční	k2eAgInPc7d1	finanční
náklady	náklad	k1gInPc7	náklad
institutu	institut	k1gInSc2	institut
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
demokratických	demokratický	k2eAgInPc6d1	demokratický
státech	stát	k1gInPc6	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možná	možný	k2eAgFnSc1d1	možná
diskriminace	diskriminace	k1gFnSc1	diskriminace
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
statistik	statistika	k1gFnPc2	statistika
bývá	bývat	k5eAaImIp3nS	bývat
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
uplatňován	uplatňovat	k5eAaImNgInS	uplatňovat
častěji	často	k6eAd2	často
vůči	vůči	k7c3	vůči
náboženským	náboženský	k2eAgFnPc3d1	náboženská
a	a	k8xC	a
etnickým	etnický	k2eAgFnPc3d1	etnická
minoritám	minorita	k1gFnPc3	minorita
a	a	k8xC	a
vůči	vůči	k7c3	vůči
chudým	chudý	k1gMnPc3	chudý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
dobré	dobrý	k2eAgMnPc4d1	dobrý
právníky	právník	k1gMnPc4	právník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Argument	argument	k1gInSc1	argument
justičního	justiční	k2eAgInSc2d1	justiční
omylu	omyl	k1gInSc2	omyl
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nabírá	nabírat	k5eAaImIp3nS	nabírat
výrazně	výrazně	k6eAd1	výrazně
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
revize	revize	k1gFnSc1	revize
rozsudků	rozsudek	k1gInPc2	rozsudek
v	v	k7c6	v
USA	USA	kA	USA
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
nových	nový	k2eAgFnPc2d1	nová
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
metod	metoda	k1gFnPc2	metoda
(	(	kIx(	(
<g/>
analýza	analýza	k1gFnSc1	analýza
DNA	dno	k1gNnSc2	dno
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
prokázaly	prokázat	k5eAaPmAgInP	prokázat
značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
justičních	justiční	k2eAgInPc2d1	justiční
omylů	omyl	k1gInPc2	omyl
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
jediné	jediný	k2eAgInPc4d1	jediný
existující	existující	k2eAgInPc4d1	existující
důkazy	důkaz	k1gInPc4	důkaz
pro	pro	k7c4	pro
vinu	vina	k1gFnSc4	vina
odsouzených	odsouzený	k1gMnPc2	odsouzený
ve	v	k7c6	v
značném	značný	k2eAgNnSc6d1	značné
množství	množství	k1gNnSc6	množství
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
takových	takový	k3xDgMnPc2	takový
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
obžalovaní	obžalovaný	k1gMnPc1	obžalovaný
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
obavy	obava	k1gFnPc1	obava
ze	z	k7c2	z
zneužití	zneužití	k1gNnSc2	zneužití
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
justiční	justiční	k2eAgFnSc6d1	justiční
vraždě	vražda	k1gFnSc6	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
obecně	obecně	k6eAd1	obecně
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
prohřešků	prohřešek	k1gInPc2	prohřešek
<g/>
,	,	kIx,	,
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
morálka	morálka	k1gFnSc1	morálka
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
odpuštění	odpuštění	k1gNnSc1	odpuštění
a	a	k8xC	a
milosrdenství	milosrdenství	k1gNnSc1	milosrdenství
vůči	vůči	k7c3	vůči
provinilcům	provinilec	k1gMnPc3	provinilec
a	a	k8xC	a
nedotknutelnost	nedotknutelnost	k1gFnSc1	nedotknutelnost
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
teologické	teologický	k2eAgInPc1d1	teologický
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
současné	současný	k2eAgFnSc2d1	současná
pozice	pozice	k1gFnSc2	pozice
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
připouští	připouštět	k5eAaImIp3nS	připouštět
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
krajní	krajní	k2eAgFnSc4d1	krajní
metodu	metoda	k1gFnSc4	metoda
prevence	prevence	k1gFnSc2	prevence
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
popravit	popravit	k5eAaPmF	popravit
nenapravitelného	napravitelný	k2eNgMnSc4d1	nenapravitelný
zločince	zločinec	k1gMnSc4	zločinec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
zločinech	zločin	k1gInPc6	zločin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
encyklice	encyklika	k1gFnSc6	encyklika
Evangelium	evangelium	k1gNnSc1	evangelium
vitae	vitae	k6eAd1	vitae
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
přece	přece	k9	přece
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
účinně	účinně	k6eAd1	účinně
kriminalitě	kriminalita	k1gFnSc3	kriminalita
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
provinilce	provinilec	k1gMnSc4	provinilec
zneškodní	zneškodnit	k5eAaPmIp3nS	zneškodnit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
zcela	zcela	k6eAd1	zcela
uzavírala	uzavírat	k5eAaPmAgFnS	uzavírat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
<g/>
...	...	k?	...
Musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pečlivě	pečlivě	k6eAd1	pečlivě
zvažován	zvažován	k2eAgInSc4d1	zvažován
a	a	k8xC	a
vybírán	vybírán	k2eAgInSc4d1	vybírán
druh	druh	k1gInSc4	druh
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
trestu	trest	k1gInSc2	trest
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odsuzovat	odsuzovat	k5eAaImF	odsuzovat
provinilce	provinilec	k1gMnPc4	provinilec
k	k	k7c3	k
nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
trestu	trest	k1gInSc3	trest
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
případ	případ	k1gInSc4	případ
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nezbytnosti	nezbytnost	k1gFnSc2	nezbytnost
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
o	o	k7c4	o
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
nemůže	moct	k5eNaImIp3nS	moct
bránit	bránit	k5eAaImF	bránit
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vhodnějšímu	vhodný	k2eAgNnSc3d2	vhodnější
uspořádání	uspořádání	k1gNnSc3	uspořádání
trestního	trestní	k2eAgInSc2d1	trestní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
případy	případ	k1gInPc1	případ
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
staví	stavit	k5eAaBmIp3nS	stavit
také	také	k9	také
představitelé	představitel	k1gMnPc1	představitel
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
<g/>
,	,	kIx,	,
metodistické	metodistický	k2eAgFnSc2d1	metodistická
a	a	k8xC	a
luteránské	luteránský	k2eAgFnSc2d1	luteránská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
američtí	americký	k2eAgMnPc1d1	americký
baptisté	baptista	k1gMnPc1	baptista
<g/>
,	,	kIx,	,
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
naopak	naopak	k6eAd1	naopak
schvalují	schvalovat	k5eAaImIp3nP	schvalovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
vyznání	vyznání	k1gNnSc2	vyznání
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
XVI	XVI	kA	XVI
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
ukládat	ukládat	k5eAaImF	ukládat
zločincům	zločinec	k1gMnPc3	zločinec
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
ani	ani	k8xC	ani
neodsuzuje	odsuzovat	k5eNaImIp3nS	odsuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1	islámské
právo	právo	k1gNnSc1	právo
šaría	šaría	k6eAd1	šaría
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
a	a	k8xC	a
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koránu	korán	k1gInSc6	korán
např.	např.	kA	např.
stojí	stát	k5eAaImIp3nP	stát
:	:	kIx,	:
Judaismus	judaismus	k1gInSc1	judaismus
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
principu	princip	k1gInSc6	princip
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vynesení	vynesení	k1gNnSc1	vynesení
spravedlivé	spravedlivý	k2eAgNnSc1d1	spravedlivé
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Talmud	talmud	k1gInSc1	talmud
klade	klást	k5eAaImIp3nS	klást
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
ale	ale	k9	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
činí	činit	k5eAaImIp3nS	činit
téměř	téměř	k6eAd1	téměř
nemožným	možný	k2eNgInSc7d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
citován	citován	k2eAgInSc1d1	citován
výrok	výrok	k1gInSc1	výrok
židovského	židovský	k2eAgMnSc2d1	židovský
filosofa	filosof	k1gMnSc2	filosof
Maimonida	Maimonid	k1gMnSc2	Maimonid
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
propustit	propustit	k5eAaPmF	propustit
tisíc	tisíc	k4xCgInSc1	tisíc
zločinců	zločinec	k1gMnPc2	zločinec
než	než	k8xS	než
odsoudit	odsoudit	k5eAaPmF	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
jednoho	jeden	k4xCgInSc2	jeden
nevinného	vinný	k2eNgInSc2d1	nevinný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
vykonán	vykonat	k5eAaPmNgInS	vykonat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
nacistickém	nacistický	k2eAgMnSc6d1	nacistický
válečném	válečný	k2eAgMnSc6d1	válečný
zločinci	zločinec	k1gMnSc6	zločinec
Adolfu	Adolf	k1gMnSc6	Adolf
Eichmannovi	Eichmann	k1gMnSc6	Eichmann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hinduistických	hinduistický	k2eAgNnPc6d1	hinduistické
písmech	písmo	k1gNnPc6	písmo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Manu-smriti	Manumrit	k1gMnPc1	Manu-smrit
(	(	kIx(	(
<g/>
zákoník	zákoník	k1gInSc1	zákoník
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vraha	vrah	k1gMnSc4	vrah
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
projevuje	projevovat	k5eAaImIp3nS	projevovat
vrahovi	vrah	k1gMnSc3	vrah
milost	milost	k1gFnSc4	milost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
zabit	zabit	k2eAgInSc1d1	zabit
v	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
a	a	k8xC	a
podroben	podrobit	k5eAaPmNgInS	podrobit
mnohem	mnohem	k6eAd1	mnohem
většímu	veliký	k2eAgNnSc3d2	veliký
utrpení	utrpení	k1gNnSc3	utrpení
v	v	k7c6	v
budoucích	budoucí	k2eAgInPc6d1	budoucí
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Stanovisko	stanovisko	k1gNnSc1	stanovisko
buddhismu	buddhismus	k1gInSc2	buddhismus
není	být	k5eNaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
hinduistické	hinduistický	k2eAgInPc4d1	hinduistický
směry	směr	k1gInPc4	směr
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Poprava	poprava	k1gFnSc1	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Garota	Garota	k1gFnSc1	Garota
Smrtící	smrtící	k2eAgFnSc2d1	smrtící
injekce	injekce	k1gFnSc2	injekce
Plynová	plynový	k2eAgFnSc1d1	plynová
komora	komora	k1gFnSc1	komora
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
křeslo	křeslo	k1gNnSc1	křeslo
Zastřelení	zastřelení	k1gNnSc2	zastřelení
popravčí	popravčí	k2eAgFnSc7d1	popravčí
četou	četa	k1gFnSc7	četa
Oběšení	oběšení	k1gNnSc2	oběšení
Setnutí	setnutí	k1gNnSc2	setnutí
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
gilotina	gilotina	k1gFnSc1	gilotina
<g/>
)	)	kIx)	)
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
způsobů	způsob	k1gInPc2	způsob
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
historické	historický	k2eAgInPc4d1	historický
způsoby	způsob	k1gInPc4	způsob
<g/>
:	:	kIx,	:
ukřižování	ukřižování	k1gNnSc1	ukřižování
<g/>
,	,	kIx,	,
ukamenování	ukamenování	k1gNnSc1	ukamenování
<g/>
,	,	kIx,	,
estrapáda	estrapáda	k1gFnSc1	estrapáda
<g/>
,	,	kIx,	,
rozdupání	rozdupání	k1gNnSc1	rozdupání
slonem	slon	k1gMnSc7	slon
<g/>
,	,	kIx,	,
upálení	upálení	k1gNnSc1	upálení
<g/>
,	,	kIx,	,
stětí	stětit	k5eAaImIp3nS	stětit
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
lámání	lámání	k1gNnPc4	lámání
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
stažení	stažení	k1gNnSc1	stažení
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
historických	historický	k2eAgInPc2d1	historický
způsobů	způsob	k1gInPc2	způsob
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
uplatňovány	uplatňován	k2eAgInPc4d1	uplatňován
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ukamenování	ukamenování	k1gNnSc4	ukamenování
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
stětí	stětit	k5eAaImIp3nS	stětit
mečem	meč	k1gInSc7	meč
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Florian	Florian	k1gMnSc1	Florian
Sivák	Sivák	k1gMnSc1	Sivák
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
do	do	k7c2	do
r.	r.	kA	r.
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Cornelie	Cornelie	k1gFnSc1	Cornelie
C.	C.	kA	C.
Bestová	Bestová	k1gFnSc1	Bestová
<g/>
:	:	kIx,	:
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
německo-českém	německo-český	k2eAgNnSc6d1	německo-české
porovnání	porovnání	k1gNnSc6	porovnání
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Karel	Karel	k1gMnSc1	Karel
Malý	Malý	k1gMnSc1	Malý
<g/>
:	:	kIx,	:
České	český	k2eAgNnSc1d1	české
právo	právo	k1gNnSc1	právo
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Martin	Martin	k1gMnSc1	Martin
Monestier	Monestier	k1gMnSc1	Monestier
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Jaromír	Jaromíra	k1gFnPc2	Jaromíra
Tauchen	Tauchna	k1gFnPc2	Tauchna
<g/>
:	:	kIx,	:
Vývoj	vývoj	k1gInSc1	vývoj
trestního	trestní	k2eAgNnSc2d1	trestní
soudnictví	soudnictví	k1gNnSc2	soudnictví
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
1933	[number]	k4	1933
-	-	kIx~	-
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
The	The	k1gMnSc1	The
European	European	k1gMnSc1	European
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
History	Histor	k1gInPc7	Histor
of	of	k?	of
Law	Law	k1gMnPc7	Law
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
182	[number]	k4	182
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-904522-2-0	[number]	k4	978-80-904522-2-0
Amnesty	Amnest	k1gInPc7	Amnest
International	International	k1gMnSc1	International
<g/>
:	:	kIx,	:
When	When	k1gMnSc1	When
The	The	k1gMnSc1	The
State	status	k1gInSc5	status
Kills	Kills	k1gInSc1	Kills
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Death	Death	k1gInSc1	Death
Penalty	penalty	k1gNnSc2	penalty
Information	Information	k1gInSc1	Information
Center	centrum	k1gNnPc2	centrum
Britannica	Britannic	k1gInSc2	Britannic
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
Úřad	úřad	k1gInSc1	úřad
dokumentace	dokumentace	k1gFnSc2	dokumentace
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
zločinů	zločin	k1gInPc2	zločin
komunismu	komunismus	k1gInSc2	komunismus
Doživotí	doživotí	k1gNnSc2	doživotí
Poprava	poprava	k1gFnSc1	poprava
Výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
trest	trest	k1gInSc4	trest
<g />
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc1	Wikidat
<g/>
:	:	kIx,	:
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Death	Death	k1gInSc1	Death
penalty	penalta	k1gFnSc2	penalta
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Executions	Executions	k1gInSc1	Executions
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaBmAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Trest	trest	k1gInSc1	trest
<g />
.	.	kIx.	.
</s>
<s>
smrti	smrt	k1gFnSc3	smrt
www.prodeathpenalty.com	www.prodeathpenalty.com	k1gInSc1	www.prodeathpenalty.com
-	-	kIx~	-
Stránka	stránka	k1gFnSc1	stránka
amerických	americký	k2eAgMnPc2d1	americký
příznivců	příznivec	k1gMnPc2	příznivec
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
Web	web	k1gInSc1	web
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnPc1	International
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgInPc4d1	aktuální
přehledy	přehled	k1gInPc4	přehled
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
www.worldcoalition.org	www.worldcoalition.org	k1gInSc1	www.worldcoalition.org
-	-	kIx~	-
Světová	světový	k2eAgFnSc1d1	světová
koalice	koalice	k1gFnSc1	koalice
proti	proti	k7c3	proti
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
www.deathpenaltyinfo.org	www.deathpenaltyinfo.org	k1gInSc1	www.deathpenaltyinfo.org
-	-	kIx~	-
Americký	americký	k2eAgInSc1d1	americký
informační	informační	k2eAgInSc1d1	informační
server	server	k1gInSc1	server
o	o	k7c6	o
trestu	trest	k1gInSc6	trest
smrti	smrt	k1gFnSc2	smrt
O.	O.	kA	O.
Liška	liška	k1gFnSc1	liška
<g/>
:	:	kIx,	:
Vykonané	vykonaný	k2eAgInPc1d1	vykonaný
tresty	trest	k1gInPc1	trest
smrti	smrt	k1gFnSc2	smrt
Československo	Československo	k1gNnSc1	Československo
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
doc	doc	kA	doc
<g/>
,	,	kIx,	,
3	[number]	k4	3
MB	MB	kA	MB
<g/>
)	)	kIx)	)
BlogTS	BlogTS	k1gMnSc1	BlogTS
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
blog	blog	k1gInSc1	blog
věnující	věnující	k2eAgInSc1d1	věnující
se	s	k7c7	s
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
Poprava	poprava	k1gFnSc1	poprava
úkladného	úkladný	k2eAgMnSc2d1	úkladný
vraha	vrah	k1gMnSc2	vrah
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
vykonán	vykonán	k2eAgInSc4d1	vykonán
–	–	k?	–
dobové	dobový	k2eAgFnPc4d1	dobová
reportáže	reportáž	k1gFnPc4	reportáž
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgFnPc4d1	popisující
okolnosti	okolnost	k1gFnPc4	okolnost
a	a	k8xC	a
průběh	průběh	k1gInSc4	průběh
popravy	poprava	k1gFnSc2	poprava
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1895	[number]	k4	1895
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
