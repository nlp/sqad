<s>
Megatsunami	Megatsuna	k1gFnPc7	Megatsuna
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
zničující	zničující	k2eAgFnSc1d1	zničující
oceánská	oceánský	k2eAgFnSc1d1	oceánská
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgFnSc2d1	klasická
<g/>
"	"	kIx"	"
tsunami	tsunami	k1gNnSc7	tsunami
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
zasaženým	zasažený	k2eAgNnSc7d1	zasažené
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Megatsunami	Megatsuna	k1gFnPc7	Megatsuna
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
charakter	charakter	k1gInSc1	charakter
velké	velký	k2eAgFnSc2d1	velká
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgNnSc1d1	normální
tsunami	tsunami	k1gNnSc1	tsunami
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
téměř	téměř	k6eAd1	téměř
nerozpoznatelné	rozpoznatelný	k2eNgInPc1d1	nerozpoznatelný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
megatsunami	megatsuna	k1gFnPc7	megatsuna
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
síle	síla	k1gFnSc3	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
viditelná	viditelný	k2eAgFnSc1d1	viditelná
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
výšky	výška	k1gFnSc2	výška
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přiblížení	přiblížení	k1gNnSc6	přiblížení
k	k	k7c3	k
pevnině	pevnina	k1gFnSc3	pevnina
narůstá	narůstat	k5eAaImIp3nS	narůstat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
megatsunami	megatsuna	k1gFnPc7	megatsuna
jsou	být	k5eAaImIp3nP	být
pády	pád	k1gInPc1	pád
meteoritů	meteorit	k1gInPc2	meteorit
nebo	nebo	k8xC	nebo
masivní	masivní	k2eAgInPc1d1	masivní
sesuvy	sesuv	k1gInPc1	sesuv
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlna	vlna	k1gFnSc1	vlna
nemůže	moct	k5eNaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Megatsunami	Megatsuna	k1gFnPc7	Megatsuna
mají	mít	k5eAaImIp3nP	mít
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
oproti	oproti	k7c3	oproti
tsunami	tsunami	k1gNnSc3	tsunami
<g/>
,	,	kIx,	,
způsobeným	způsobený	k2eAgMnSc7d1	způsobený
obvykle	obvykle	k6eAd1	obvykle
seismickou	seismický	k2eAgFnSc7d1	seismická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
,	,	kIx,	,
lokálnější	lokální	k2eAgInPc4d2	lokální
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
ani	ani	k8xC	ani
vznik	vznik	k1gInSc1	vznik
megatsunami	megatsuna	k1gFnPc7	megatsuna
razantním	razantní	k2eAgInSc7d1	razantní
výbuchem	výbuch	k1gInSc7	výbuch
podmořské	podmořský	k2eAgFnSc2d1	podmořská
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
megatsunami	megatsuna	k1gFnPc7	megatsuna
geologové	geolog	k1gMnPc1	geolog
poprvé	poprvé	k6eAd1	poprvé
spekulovali	spekulovat	k5eAaImAgMnP	spekulovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
objevili	objevit	k5eAaPmAgMnP	objevit
známky	známka	k1gFnSc2	známka
neobvykle	obvykle	k6eNd1	obvykle
velkých	velký	k2eAgFnPc2d1	velká
vln	vlna	k1gFnPc2	vlna
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Lituya	Lituy	k1gInSc2	Lituy
<g/>
.	.	kIx.	.
</s>
<s>
Devátého	devátý	k4xOgInSc2	devátý
července	červenec	k1gInSc2	červenec
1958	[number]	k4	1958
tam	tam	k6eAd1	tam
pak	pak	k6eAd1	pak
sesuv	sesuv	k1gInSc1	sesuv
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
horniny	hornina	k1gFnSc2	hornina
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
během	během	k7c2	během
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
7,7	[number]	k4	7,7
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
škály	škála	k1gFnSc2	škála
způsobil	způsobit	k5eAaPmAgMnS	způsobit
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
530	[number]	k4	530
metrů	metr	k1gInPc2	metr
nad	nad	k7c4	nad
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
při	při	k7c6	při
naplňování	naplňování	k1gNnSc6	naplňování
přehrady	přehrada	k1gFnSc2	přehrada
Vajont	Vajonta	k1gFnPc2	Vajonta
sesulo	sesout	k5eAaPmAgNnS	sesout
270	[number]	k4	270
milionů	milion	k4xCgInPc2	milion
kubických	kubický	k2eAgInPc2d1	kubický
metrů	metr	k1gInPc2	metr
půdy	půda	k1gFnSc2	půda
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
hory	hora	k1gFnSc2	hora
Monte	Mont	k1gMnSc5	Mont
Toc	Toc	k1gMnSc5	Toc
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
vlna	vlna	k1gFnSc1	vlna
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
100	[number]	k4	100
m	m	kA	m
zničila	zničit	k5eAaPmAgFnS	zničit
několik	několik	k4yIc4	několik
blízkých	blízký	k2eAgFnPc2d1	blízká
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
způsobil	způsobit	k5eAaPmAgInS	způsobit
sesuv	sesuv	k1gInSc1	sesuv
půdy	půda	k1gFnSc2	půda
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
do	do	k7c2	do
západogrónského	západogrónský	k2eAgInSc2d1	západogrónský
fjordu	fjord	k1gInSc2	fjord
Karrat	Karrat	k1gMnSc1	Karrat
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výšky	výška	k1gFnPc4	výška
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
m	m	kA	m
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
protějším	protější	k2eAgNnSc6d1	protější
pobřeží	pobřeží	k1gNnSc6	pobřeží
vynesla	vynést	k5eAaPmAgFnS	vynést
vodu	voda	k1gFnSc4	voda
až	až	k6eAd1	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
50	[number]	k4	50
m	m	kA	m
<g/>
,	,	kIx,	,
poničila	poničit	k5eAaPmAgFnS	poničit
vesnici	vesnice	k1gFnSc4	vesnice
Nuugaatsiaq	Nuugaatsiaq	k1gFnPc2	Nuugaatsiaq
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Megatsunami	Megatsuna	k1gFnPc7	Megatsuna
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
zničila	zničit	k5eAaPmAgFnS	zničit
jedenáct	jedenáct	k4xCc4	jedenáct
domů	dům	k1gInPc2	dům
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zabila	zabít	k5eAaPmAgFnS	zabít
čtyři	čtyři	k4xCgMnPc4	čtyři
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k9	i
záběry	záběr	k1gInPc1	záběr
této	tento	k3xDgFnSc2	tento
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
..	..	k?	..
Z	z	k7c2	z
geologických	geologický	k2eAgFnPc2d1	geologická
stop	stopa	k1gFnPc2	stopa
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
výskyt	výskyt	k1gInSc1	výskyt
megatsunami	megatsuna	k1gFnPc7	megatsuna
je	být	k5eAaImIp3nS	být
řídký	řídký	k2eAgMnSc1d1	řídký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
síle	síla	k1gFnSc3	síla
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
devastující	devastující	k2eAgFnPc1d1	devastující
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zátoky	zátoka	k1gFnSc2	zátoka
Lituya	Lituy	k1gInSc2	Lituy
ovšem	ovšem	k9	ovšem
zpravidla	zpravidla	k6eAd1	zpravidla
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
jen	jen	k9	jen
omezené	omezený	k2eAgNnSc4d1	omezené
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
známá	známý	k2eAgFnSc1d1	známá
megatsunami	megatsuna	k1gFnPc7	megatsuna
velkého	velký	k2eAgInSc2d1	velký
dosahu	dosah	k1gInSc2	dosah
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
před	před	k7c7	před
asi	asi	k9	asi
čtyřmi	čtyři	k4xCgInPc7	čtyři
tisíci	tisíc	k4xCgInPc7	tisíc
lety	let	k1gInPc7	let
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Réunion	Réunion	k1gInSc1	Réunion
východně	východně	k6eAd1	východně
od	od	k7c2	od
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Sopečný	sopečný	k2eAgInSc1d1	sopečný
ostrov	ostrov	k1gInSc1	ostrov
La	la	k1gNnSc2	la
Palma	palma	k1gFnSc1	palma
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Atlantickém	atlantický	k2eAgInSc6d1	atlantický
oceánu	oceán	k1gInSc6	oceán
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
sopkou	sopka	k1gFnSc7	sopka
Cumbre	Cumbr	k1gInSc5	Cumbr
Vieja	Viejum	k1gNnSc2	Viejum
nejpravděpodobnější	pravděpodobný	k2eAgMnSc1d3	nejpravděpodobnější
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
megatsunami	megatsuna	k1gFnPc7	megatsuna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sopečný	sopečný	k2eAgInSc1d1	sopečný
ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
jeho	jeho	k3xOp3gInSc1	jeho
vnitřek	vnitřek	k1gInSc1	vnitřek
nasycen	nasytit	k5eAaPmNgInS	nasytit
podzemní	podzemní	k2eAgFnSc7d1	podzemní
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
sopka	sopka	k1gFnSc1	sopka
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
<g/>
,	,	kIx,	,
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
páru	pár	k1gInSc6	pár
a	a	k8xC	a
vnikne	vniknout	k5eAaPmIp3nS	vniknout
ohromný	ohromný	k2eAgInSc4d1	ohromný
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
sklouznutí	sklouznutí	k1gNnSc4	sklouznutí
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
části	část	k1gFnSc2	část
ostrova	ostrov	k1gInSc2	ostrov
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
po	po	k7c6	po
erupci	erupce	k1gFnSc6	erupce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
rozpůlen	rozpůlit	k5eAaPmNgInS	rozpůlit
puklinou	puklina	k1gFnSc7	puklina
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
severu	sever	k1gInSc2	sever
po	po	k7c4	po
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Ohromný	ohromný	k2eAgInSc1d1	ohromný
sesuv	sesuv	k1gInSc1	sesuv
by	by	kYmCp3nS	by
vyslal	vyslat	k5eAaPmAgInS	vyslat
vlnu	vlna	k1gFnSc4	vlna
vysokou	vysoký	k2eAgFnSc4d1	vysoká
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
100-600	[number]	k4	100-600
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
100	[number]	k4	100
metrová	metrový	k2eAgFnSc1d1	metrová
stěna	stěna	k1gFnSc1	stěna
se	se	k3xPyFc4	se
přelije	přelít	k5eAaPmIp3nS	přelít
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnSc1d2	menší
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dorazí	dorazit	k5eAaPmIp3nS	dorazit
vlna	vlna	k1gFnSc1	vlna
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
m	m	kA	m
na	na	k7c4	na
Východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
ohrožená	ohrožený	k2eAgNnPc1d1	ohrožené
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
New	New	k1gMnPc4	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vlna	vlna	k1gFnSc1	vlna
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
zbytek	zbytek	k1gInSc4	zbytek
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
megatsunami	megatsuna	k1gFnPc7	megatsuna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Mega-tsunami	Megasuna	k1gFnPc7	Mega-tsuna
<g/>
:	:	kIx,	:
Wave	Wave	k1gInSc1	Wave
of	of	k?	of
Destruction	Destruction	k1gInSc1	Destruction
-	-	kIx~	-
BBC	BBC	kA	BBC
Science	Science	k1gFnSc1	Science
&	&	k?	&
Nature	Natur	k1gInSc5	Natur
</s>
