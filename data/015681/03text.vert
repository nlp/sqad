<s>
Severská	severský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
</s>
<s>
Runový	runový	k2eAgInSc1d1
kámen	kámen	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
ze	z	k7c2
švédského	švédský	k2eAgInSc2d1
Röku	Rökus	k1gInSc2
v	v	k7c6
Östergötlandu	Östergötland	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
původ	původ	k1gInSc1
je	být	k5eAaImIp3nS
datován	datovat	k5eAaImNgInS
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
odkazuje	odkazovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
na	na	k7c4
ztracené	ztracený	k2eAgFnPc4d1
germánské	germánský	k2eAgFnPc4d1
legendy	legenda	k1gFnPc4
z	z	k7c2
období	období	k1gNnSc2
stěhování	stěhování	k1gNnSc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c4
příběhy	příběh	k1gInPc4
ze	z	k7c2
severské	severský	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Germánská	germánský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Severská	severský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
je	být	k5eAaImIp3nS
souhrn	souhrn	k1gInSc4
pověstí	pověst	k1gFnPc2
<g/>
,	,	kIx,
legend	legenda	k1gFnPc2
a	a	k8xC
představ	představa	k1gFnPc2
o	o	k7c6
hrdinech	hrdina	k1gMnPc6
<g/>
,	,	kIx,
bozích	bůh	k1gMnPc6
<g/>
,	,	kIx,
vzniku	vznik	k1gInSc3
<g/>
,	,	kIx,
účelu	účel	k1gInSc3
a	a	k8xC
konci	konec	k1gInSc3
světa	svět	k1gInSc2
pocházející	pocházející	k2eAgFnSc2d1
od	od	k7c2
germánských	germánský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
obývajících	obývající	k2eAgInPc2d1
Skandinávský	skandinávský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
o	o	k7c4
Nory	Nor	k1gMnPc4
<g/>
,	,	kIx,
Švédy	Švéd	k1gMnPc4
nebo	nebo	k8xC
Dány	Dán	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
spojuje	spojovat	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
vikinská	vikinský	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
širším	široký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
staré	starý	k2eAgFnPc4d1
náboženské	náboženský	k2eAgFnPc4d1
představy	představa	k1gFnPc4
společné	společný	k2eAgFnPc4d1
všem	všecek	k3xTgInPc3
germánským	germánský	k2eAgInPc3d1
kmenům	kmen	k1gInPc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
např.	např.	kA
i	i	k8xC
Anglům	Angl	k1gMnPc3
nebo	nebo	k8xC
Sasům	Sas	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
však	však	k9
přijali	přijmout	k5eAaPmAgMnP
křesťanství	křesťanství	k1gNnSc4
mnohem	mnohem	k6eAd1
dříve	dříve	k6eAd2
než	než	k8xS
seveřané	seveřan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
severské	severský	k2eAgFnSc3d1
mytologii	mytologie	k1gFnSc3
se	se	k3xPyFc4
někdy	někdy	k6eAd1
také	také	k9
řadí	řadit	k5eAaImIp3nS
mytologie	mytologie	k1gFnSc1
finská	finský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Severský	severský	k2eAgInSc1d1
polyteismus	polyteismus	k1gInSc1
</s>
<s>
Severský	severský	k2eAgInSc4d1
polyteismus	polyteismus	k1gInSc4
tvoří	tvořit	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
skupiny	skupina	k1gFnPc1
bohů	bůh	k1gMnPc2
<g/>
:	:	kIx,
válečničtí	válečnický	k2eAgMnPc1d1
Ásové	Ásový	k2eAgFnSc2d1
a	a	k8xC
Vanové	vanový	k2eAgFnSc2d1
spojovaní	spojovaný	k2eAgMnPc1d1
s	s	k7c7
plodností	plodnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
nejvyššího	vysoký	k2eAgMnSc2d3
boha	bůh	k1gMnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
považovat	považovat	k5eAaImF
Ódin	Ódin	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
tří	tři	k4xCgMnPc2
prvotních	prvotní	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
a	a	k8xC
patron	patrona	k1gFnPc2
magie	magie	k1gFnSc2
a	a	k8xC
run	run	k1gInSc1
<g/>
,	,	kIx,
Thór	Thór	k1gMnSc1
<g/>
,	,	kIx,
bůh	bůh	k1gMnSc1
hromu	hrom	k1gInSc2
<g/>
,	,	kIx,
deště	dešť	k1gInSc2
<g/>
,	,	kIx,
nebe	nebe	k1gNnSc2
a	a	k8xC
plodnosti	plodnost	k1gFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
<g/>
Tyr	Tyr	k1gMnSc1
bůh	bůh	k1gMnSc1
války	válka	k1gFnSc2
a	a	k8xC
Frey	Frea	k1gFnSc2
<g/>
,	,	kIx,
bůh	bůh	k1gMnSc1
plodnosti	plodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
bohů	bůh	k1gMnPc2
existují	existovat	k5eAaImIp3nP
také	také	k9
zlí	zlý	k2eAgMnPc1d1
obři	obr	k1gMnPc1
<g/>
,	,	kIx,
světlí	světlet	k5eAaImIp3nP
álfové	álfové	k2eAgMnPc1d1
a	a	k8xC
také	také	k9
černí	černý	k2eAgMnPc1d1
álfové	álfus	k1gMnPc1
(	(	kIx(
<g/>
trpaslíci	trpaslík	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severské	severský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
také	také	k9
existuje	existovat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nejasný	jasný	k2eNgInSc1d1
systém	systém	k1gInSc1
devíti	devět	k4xCc2
světů	svět	k1gInPc2
spojených	spojený	k2eAgInPc2d1
světovým	světový	k2eAgInSc7d1
jasanem	jasan	k1gInSc7
Yggdrasilem	Yggdrasil	k1gMnSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
sídlí	sídlet	k5eAaImIp3nS
zmiňovaná	zmiňovaný	k2eAgNnPc4d1
stvoření	stvoření	k1gNnPc4
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
a	a	k8xC
duše	duše	k1gFnPc1
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ódin	Ódin	k1gInSc1
–	–	k?
vzácná	vzácný	k2eAgFnSc1d1
středověká	středověký	k2eAgFnSc1d1
soška	soška	k1gFnSc1
postavy	postava	k1gFnSc2
sedící	sedící	k2eAgFnSc1d1
na	na	k7c6
trůnu	trůn	k1gInSc6
s	s	k7c7
dvěma	dva	k4xCgInPc7
havrany	havran	k1gMnPc7
<g/>
,	,	kIx,
Roskildské	Roskildský	k2eAgNnSc1d1
museum	museum	k1gNnSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Thór	Thór	k1gInSc1
–	–	k?
originální	originální	k2eAgInSc1d1
nález	nález	k1gInSc1
bronzové	bronzový	k2eAgFnSc2d1
sošky	soška	k1gFnSc2
o	o	k7c6
velikosti	velikost	k1gFnSc6
6.7	6.7	k4
cm	cm	kA
<g/>
,	,	kIx,
Islandské	islandský	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Frey	Frea	k1gFnPc1
–	–	k?
figurka	figurka	k1gFnSc1
se	s	k7c7
ztopořeným	ztopořený	k2eAgInSc7d1
falem	falos	k1gInSc7
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
představuje	představovat	k5eAaImIp3nS
boha	bůh	k1gMnSc4
Freye	Frey	k1gMnSc4
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc4
Rällinge	Rälling	k1gInSc2
<g/>
,	,	kIx,
vikinské	vikinský	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Freya	Freya	k1gFnSc1
–	–	k?
originální	originální	k2eAgInSc4d1
středověký	středověký	k2eAgInSc4d1
amulet	amulet	k1gInSc4
ze	z	k7c2
stříbra	stříbro	k1gNnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
bohyně	bohyně	k1gFnSc2
Freyi	Frey	k1gFnSc2
a	a	k8xC
Brísingamenu	Brísingamen	k1gInSc3
nalezený	nalezený	k2eAgMnSc1d1
v	v	k7c6
Östergötlandu	Östergötland	k1gInSc6
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Týr	Týr	k?
–	–	k?
nákres	nákres	k1gInSc1
Týra	Týrus	k1gMnSc2
a	a	k8xC
Fenrira	Fenrir	k1gMnSc2
z	z	k7c2
nálezu	nález	k1gInSc2
germánského	germánský	k2eAgInSc2d1
brakteátu	brakteát	k1gInSc2
z	z	k7c2
období	období	k1gNnSc2
Stěhování	stěhování	k1gNnSc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Trollhättanu	Trollhättan	k1gInSc2
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Loki	Loki	k6eAd1
–	–	k?
výjev	výjev	k1gInSc4
z	z	k7c2
tzv.	tzv.	kA
gosfortského	gosfortský	k2eAgInSc2d1
kříže	kříž	k1gInSc2
zobrazuje	zobrazovat	k5eAaImIp3nS
svázaného	svázaný	k2eAgMnSc4d1
Lokiho	Loki	k1gMnSc4
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
Sigyn	Sigyna	k1gFnPc2
</s>
<s>
Stvoření	stvoření	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
Thór	Thór	k1gInSc1
v	v	k7c6
rybářské	rybářský	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
loví	lovit	k5eAaImIp3nS
Jörmungandra	Jörmungandra	k1gFnSc1
<g/>
,	,	kIx,
kamenná	kamenný	k2eAgFnSc1d1
stéla	stéla	k1gFnSc1
<g/>
,	,	kIx,
švédská	švédský	k2eAgFnSc1d1
Altuna	Altuna	k1gFnSc1
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
světa	svět	k1gInSc2
existovala	existovat	k5eAaImAgFnS
jen	jen	k9
zející	zející	k2eAgFnSc1d1
propast	propast	k1gFnSc1
Ginnungagap	Ginnungagap	k1gInSc1
mezi	mezi	k7c7
Niflheimem	Niflheim	k1gInSc7
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
říší	říš	k1gFnSc7
chladu	chlad	k1gInSc2
a	a	k8xC
mlhy	mlha	k1gFnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
říše	říše	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
Múspellheimem	Múspellheim	k1gInSc7
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
říší	říš	k1gFnSc7
ohně	oheň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Niflheimu	Niflheimo	k1gNnSc6
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
všepohlcující	všepohlcující	k2eAgFnSc1d1
studna	studna	k1gFnSc1
Hwergelmir	Hwergelmira	k1gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
vytékalo	vytékat	k5eAaImAgNnS
dvanáct	dvanáct	k4xCc4
řek	řeka	k1gFnPc2
jejichž	jejichž	k3xOyRp3gFnPc1
voda	voda	k1gFnSc1
stále	stále	k6eAd1
zamrzala	zamrzat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tuto	tento	k3xDgFnSc4
masu	masa	k1gFnSc4
ledu	led	k1gInSc2
začalo	začít	k5eAaPmAgNnS
působit	působit	k5eAaImF
teplo	teplo	k1gNnSc4
z	z	k7c2
Múspellheimu	Múspellheim	k1gInSc2
a	a	k8xC
z	z	k7c2
kapek	kapka	k1gFnPc2
vzniklých	vzniklý	k2eAgFnPc2d1
z	z	k7c2
roztávajícího	roztávající	k2eAgInSc2d1
ledu	led	k1gInSc2
povstal	povstat	k5eAaPmAgMnS
mrazivý	mrazivý	k2eAgMnSc1d1
obr	obr	k1gMnSc1
Ymir	Ymir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
vznikla	vzniknout	k5eAaPmAgFnS
mlékem	mléko	k1gNnSc7
bohatá	bohatý	k2eAgFnSc1d1
kráva	kráva	k1gFnSc1
Audhumla	Audhumlo	k1gNnSc2
jejímž	jejíž	k3xOyRp3gNnSc7
mlékem	mléko	k1gNnSc7
se	se	k3xPyFc4
živil	živit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
však	však	k9
začala	začít	k5eAaPmAgFnS
olizovat	olizovat	k5eAaImF
slaný	slaný	k2eAgInSc4d1
kámen	kámen	k1gInSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
slanou	slaný	k2eAgFnSc4d1
ledovou	ledový	k2eAgFnSc4d1
kru	kra	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgInSc4
den	den	k1gInSc4
z	z	k7c2
ledu	led	k1gInSc2
vystoupily	vystoupit	k5eAaPmAgInP
vlasy	vlas	k1gInPc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
hlava	hlava	k1gFnSc1
a	a	k8xC
třetí	třetí	k4xOgInSc4
den	den	k1gInSc4
celý	celý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
praotec	praotec	k1gMnSc1
Ásů	Ásů	k1gFnSc2
Búri	Búr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Ymirovi	Ymir	k1gMnSc3
v	v	k7c6
levém	levý	k2eAgNnSc6d1
podpaží	podpaží	k1gNnSc6
vznikl	vzniknout	k5eAaPmAgInS
obr	obr	k1gMnSc1
a	a	k8xC
obryně	obryně	k1gFnSc1
<g/>
,	,	kIx,
třením	tření	k1gNnSc7
nohou	noha	k1gFnPc2
o	o	k7c4
sebe	sebe	k3xPyFc4
vznikl	vzniknout	k5eAaPmAgMnS
šestihlavý	šestihlavý	k2eAgMnSc1d1
obr	obr	k1gMnSc1
Thrudgelm	Thrudgelm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Búri	Búri	k1gNnSc1
neznámo	neznámo	k6eAd1
jak	jak	k6eAd1
zplodil	zplodit	k5eAaPmAgMnS
syna	syn	k1gMnSc4
Bora	Borus	k1gMnSc4
jež	jenž	k3xRgNnSc4
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
za	za	k7c4
manželku	manželka	k1gFnSc4
obryni	obryně	k1gFnSc4
Bestlu	Bestla	k1gFnSc4
<g/>
,	,	kIx,
dceru	dcera	k1gFnSc4
obra	obr	k1gMnSc2
Bolthorna	Bolthorno	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
spolu	spolu	k6eAd1
zplodili	zplodit	k5eAaPmAgMnP
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
jménem	jméno	k1gNnSc7
Ódin	Ódina	k1gFnPc2
<g/>
,	,	kIx,
Vili	vít	k5eAaImAgMnP
a	a	k8xC
Vé	Vé	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
viděli	vidět	k5eAaImAgMnP
jak	jak	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
androgynního	androgynní	k2eAgInSc2d1
Ymira	Ymir	k1gInSc2
rodí	rodit	k5eAaImIp3nP
stále	stále	k6eAd1
noví	nový	k2eAgMnPc1d1
obři	obr	k1gMnPc1
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
zabít	zabít	k5eAaPmF
jeho	jeho	k3xOp3gNnSc4
i	i	k8xC
jeho	jeho	k3xOp3gMnPc4
potomky	potomek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krvi	krev	k1gFnSc6
zabitého	zabitý	k2eAgMnSc2d1
praotce	praotec	k1gMnSc2
obrů	obr	k1gMnPc2
se	se	k3xPyFc4
utopili	utopit	k5eAaPmAgMnP
všichni	všechen	k3xTgMnPc1
obři	obr	k1gMnPc1
až	až	k9
na	na	k7c4
Thrudgelmirova	Thrudgelmirův	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Bergelmiho	Bergelmi	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
ženu	žena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mrtvé	mrtvý	k2eAgFnPc4d1
tělo	tělo	k1gNnSc4
odtáhli	odtáhnout	k5eAaPmAgMnP
Borovi	Borův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
doprostřed	doprostřed	k7c2
Ginnungagapu	Ginnungagap	k1gInSc2
a	a	k8xC
začali	začít	k5eAaPmAgMnP
z	z	k7c2
něj	on	k3xPp3gNnSc2
tvořit	tvořit	k5eAaImF
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
masa	maso	k1gNnSc2
vytvořili	vytvořit	k5eAaPmAgMnP
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
kostí	kost	k1gFnPc2
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
krve	krev	k1gFnSc2
moře	moře	k1gNnSc2
a	a	k8xC
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
vlasů	vlas	k1gInPc2
stromy	strom	k1gInPc1
a	a	k8xC
z	z	k7c2
mozku	mozek	k1gInSc2
oblaka	oblaka	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
světových	světový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
postavili	postavit	k5eAaPmAgMnP
skřítky	skřítek	k1gMnPc7
nazývané	nazývaný	k2eAgFnSc2d1
Ausbui	Ausbu	k1gFnSc2
<g/>
,	,	kIx,
Westri	Westr	k1gMnPc1
<g/>
,	,	kIx,
Sudri	Sudr	k1gMnPc1
a	a	k8xC
Nordi	Nord	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
Ymirova	Ymirův	k2eAgNnSc2d1
obočí	obočí	k1gNnSc2
vytvořili	vytvořit	k5eAaPmAgMnP
zeď	zeď	k1gFnSc4
chránící	chránící	k2eAgInSc1d1
svět	svět	k1gInSc1
lidí	člověk	k1gMnPc2
před	před	k7c7
obry	obr	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
vyzvedli	vyzvednout	k5eAaPmAgMnP
na	na	k7c4
nebe	nebe	k1gNnSc4
jeho	jeho	k3xOp3gNnSc1
lebku	lebka	k1gFnSc4
a	a	k8xC
do	do	k7c2
ní	on	k3xPp3gFnSc2
uschovali	uschovat	k5eAaPmAgMnP
ohnivou	ohnivý	k2eAgFnSc4d1
jiskru	jiskra	k1gFnSc4
z	z	k7c2
Múspellheimu	Múspellheim	k1gInSc2
čímž	což	k3yQnSc7,k3yRnSc7
stvořili	stvořit	k5eAaPmAgMnP
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Lidé	člověk	k1gMnPc1
vznikli	vzniknout	k5eAaPmAgMnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
se	se	k3xPyFc4
Ódin	Ódin	k1gInSc1
<g/>
,	,	kIx,
Vili	vít	k5eAaImAgMnP
a	a	k8xC
Vé	Vé	k1gMnPc1
procházeli	procházet	k5eAaImAgMnP
po	po	k7c6
mořském	mořský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
narazili	narazit	k5eAaPmAgMnP
na	na	k7c4
vyplavené	vyplavený	k2eAgInPc4d1
stromy	strom	k1gInPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgMnPc2,k3yIgMnPc2,k3yRgMnPc2
stvořili	stvořit	k5eAaPmAgMnP
první	první	k4xOgMnPc4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
muž	muž	k1gMnSc1
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Ask	Ask	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
jasan	jasan	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
první	první	k4xOgFnSc1
žena	žena	k1gFnSc1
Embla	Embla	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
jíva	jíva	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
bohů	bůh	k1gMnPc2
jim	on	k3xPp3gInPc3
vdechl	vdechnout	k5eAaPmAgInS
život	život	k1gInSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
jim	on	k3xPp3gMnPc3
dal	dát	k5eAaPmAgInS
rozum	rozum	k1gInSc4
a	a	k8xC
pohyb	pohyb	k1gInSc4
a	a	k8xC
třetí	třetí	k4xOgFnSc4
podobu	podoba	k1gFnSc4
a	a	k8xC
smysly	smysl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
bohové	bůh	k1gMnPc1
odebrali	odebrat	k5eAaPmAgMnP
do	do	k7c2
Ásgardu	Ásgard	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
spojili	spojit	k5eAaPmAgMnP
s	s	k7c7
Midgardem	Midgard	k1gInSc7
<g/>
,	,	kIx,
světem	svět	k1gInSc7
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
duhovým	duhový	k2eAgInSc7d1
mostem	most	k1gInSc7
zvaným	zvaný	k2eAgInSc7d1
Bifröst	Bifröst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Posmrtný	posmrtný	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
zabitých	zabitý	k1gMnPc2
na	na	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
a	a	k8xC
utopených	utopený	k2eAgInPc2d1
odchází	odcházet	k5eAaImIp3nS
duše	duše	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
do	do	k7c2
říše	říš	k1gFnSc2
Helheimu	Helheim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgFnPc2
představ	představa	k1gFnPc2
mrtví	mrtvý	k1gMnPc1
věčně	věčně	k6eAd1
chodí	chodit	k5eAaImIp3nP
po	po	k7c6
vřesovišti	vřesoviště	k1gNnSc6
porostlém	porostlý	k2eAgNnSc6d1
trnitými	trnitý	k2eAgFnPc7d1
rostlinami	rostlina	k1gFnPc7
a	a	k8xC
musí	muset	k5eAaImIp3nP
se	se	k3xPyFc4
přebrodit	přebrodit	k5eAaPmF
přes	přes	k7c4
řeku	řeka	k1gFnSc4
plnou	plný	k2eAgFnSc4d1
ostrých	ostrý	k2eAgInPc2d1
úlomků	úlomek	k1gInPc2
ledu	led	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Duše	duše	k1gFnPc1
utonulých	utonulý	k1gMnPc2
odcházejí	odcházet	k5eAaImIp3nP
k	k	k7c3
bohyni	bohyně	k1gFnSc3
Rán	ráno	k1gNnPc2
<g/>
,	,	kIx,
manželce	manželka	k1gFnSc3
Aegiho	Aegi	k1gMnSc2
boha	bůh	k1gMnSc2
moře	moře	k1gNnSc1
a	a	k8xC
zemřelí	zemřelý	k1gMnPc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
odchází	odcházet	k5eAaImIp3nS
do	do	k7c2
Valhally	Valhalla	k1gFnSc2
kde	kde	k6eAd1
se	se	k3xPyFc4
cvičí	cvičit	k5eAaImIp3nP
v	v	k7c6
boji	boj	k1gInSc6
na	na	k7c4
Ragnarök	Ragnarök	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Edda	Edda	k1gFnSc1
</s>
<s>
Snorri	Snorri	k6eAd1
Sturluson	Sturluson	k1gInSc1
</s>
<s>
Germánská	germánský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
severská	severský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Severská	severský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
Bohové	bůh	k1gMnPc1
severské	severský	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
</s>
<s>
Ásové	Ásová	k1gFnSc3
•	•	k?
Vanové	vanový	k2eAgFnSc2d1
•	•	k?
Dísy	Dísa	k1gFnSc2
•	•	k?
Obři	obr	k1gMnPc1
•	•	k?
Elfové	elf	k1gMnPc1
•	•	k?
Álfové	Álfová	k1gFnSc6
•	•	k?
Valkýry	valkýra	k1gFnSc2
•	•	k?
Einherjové	Einherjový	k2eAgFnSc2d1
•	•	k?
Norny	norna	k1gFnSc2
•	•	k?
Ódin	Ódin	k1gMnSc1
•	•	k?
Thór	Thór	k1gMnSc1
•	•	k?
Frey	Frea	k1gFnSc2
•	•	k?
Freya	Freya	k1gMnSc1
•	•	k?
Loki	Lok	k1gFnSc2
•	•	k?
Baldr	Baldr	k1gInSc1
•	•	k?
Týr	Týr	k1gMnSc1
•	•	k?
Njörd	Njörd	k1gMnSc1
•	•	k?
Heimdall	Heimdall	k1gMnSc1
•	•	k?
Bragi	Brag	k1gFnSc2
•	•	k?
Idunn	Idunn	k1gMnSc1
•	•	k?
Frigg	Frigg	k1gMnSc1
•	•	k?
Höd	Höd	k1gMnSc1
•	•	k?
Gefjun	Gefjun	k1gMnSc1
•	•	k?
Fulla	Fulla	k1gMnSc1
•	•	k?
Beyla	Beyla	k1gMnSc1
•	•	k?
Dellingr	Dellingr	k1gMnSc1
•	•	k?
Nótt	Nótt	k1gMnSc1
•	•	k?
Dagr	Dagr	k1gMnSc1
•	•	k?
Fjörgyn	Fjörgyn	k1gMnSc1
•	•	k?
Jörð	Jörð	k1gMnSc1
•	•	k?
Forseti	Forse	k1gNnSc6
Světy	svět	k1gInPc1
</s>
<s>
Ljósálfheim	Ljósálfheim	k1gMnSc1
•	•	k?
Ásgard	Ásgard	k1gMnSc1
•	•	k?
Midgard	Midgard	k1gMnSc1
•	•	k?
Vanaheim	Vanaheim	k1gMnSc1
•	•	k?
Jötunheim	Jötunheim	k1gMnSc1
•	•	k?
Múspellheim	Múspellheim	k1gMnSc1
•	•	k?
Niflheim	Niflheim	k1gMnSc1
•	•	k?
Svartálfheim	Svartálfheim	k1gMnSc1
•	•	k?
Helheim	Helheim	k1gMnSc1
Zdroje	zdroj	k1gInSc2
</s>
<s>
Poetická	poetický	k2eAgFnSc1d1
Edda	Edda	k1gFnSc1
•	•	k?
Prozaická	prozaický	k2eAgFnSc1d1
Edda	Edda	k1gFnSc1
•	•	k?
Ságy	sága	k1gFnSc2
•	•	k?
Snorri	Snorr	k1gFnSc2
Sturluson	Sturluson	k1gMnSc1
•	•	k?
Runy	Runa	k1gFnSc2
•	•	k?
Runové	Runové	k2eAgInPc4d1
kameny	kámen	k1gInPc4
•	•	k?
Staroseverština	Staroseverština	k1gFnSc1
Společnost	společnost	k1gFnSc1
</s>
<s>
Skaldi	skald	k1gMnPc1
•	•	k?
Kenning	Kenning	k1gInSc1
•	•	k?
Čísla	číslo	k1gNnPc1
Původ	původ	k1gInSc1
</s>
<s>
Germánská	germánský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
•	•	k?
Praindoevropské	Praindoevropský	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
Ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
Ásgard	Ásgard	k1gInSc1
•	•	k?
Devět	devět	k4xCc4
světů	svět	k1gInPc2
severské	severský	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
•	•	k?
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
místa	místo	k1gNnPc1
a	a	k8xC
věci	věc	k1gFnPc1
•	•	k?
Yggdrasil	Yggdrasil	k1gMnSc1
•	•	k?
Ginnungagap	Ginnungagap	k1gMnSc1
•	•	k?
Ragnarök	Ragnarök	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7503879-1	7503879-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85089413	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85089413	#num#	k4
</s>
