<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
severské	severský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
světový	světový	k2eAgInSc1d1
jasan	jasan	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
spojuje	spojovat	k5eAaImIp3nS
systém	systém	k1gInSc4
devíti	devět	k4xCc2
světů	svět	k1gInPc2
<g/>
?	?	kIx.
</s>