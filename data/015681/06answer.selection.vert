<s desamb="1">
V	v	k7c6
severské	severský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
také	také	k9
existuje	existovat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nejasný	jasný	k2eNgInSc1d1
systém	systém	k1gInSc1
devíti	devět	k4xCc2
světů	svět	k1gInPc2
spojených	spojený	k2eAgInPc2d1
světovým	světový	k2eAgInSc7d1
jasanem	jasan	k1gInSc7
Yggdrasilem	Yggdrasil	k1gMnSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
sídlí	sídlet	k5eAaImIp3nS
zmiňovaná	zmiňovaný	k2eAgNnPc4d1
stvoření	stvoření	k1gNnPc4
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
a	a	k8xC
duše	duše	k1gFnPc1
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>