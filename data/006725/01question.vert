<s>
Kdo	kdo	k3yQnSc1	kdo
definoval	definovat	k5eAaBmAgInS	definovat
romantismus	romantismus	k1gInSc1	romantismus
výrokem	výrok	k1gInSc7	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
praví	pravit	k5eAaImIp3nS	pravit
romantismus	romantismus	k1gInSc4	romantismus
<g/>
,	,	kIx,	,
praví	pravit	k5eAaBmIp3nS	pravit
umění	umění	k1gNnSc1	umění
moderní	moderní	k2eAgNnSc1d1	moderní
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
intimita	intimita	k1gFnSc1	intimita
<g/>
,	,	kIx,	,
duchovost	duchovost	k1gFnSc1	duchovost
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
o	o	k7c6	o
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
,	,	kIx,	,
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
mění	měnit	k5eAaImIp3nP	měnit
vládne	vládnout	k5eAaImIp3nS	vládnout
<g/>
"	"	kIx"	"
</s>
