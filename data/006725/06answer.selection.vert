<s>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
intimita	intimita	k1gFnSc1	intimita
<g/>
,	,	kIx,	,
duchovost	duchovost	k1gFnSc1	duchovost
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
,	,	kIx,	,
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
umění	umění	k1gNnPc1	umění
vládne	vládnout	k5eAaImIp3nS	vládnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
<g/>
)	)	kIx)	)
Rozvoji	rozvoj	k1gInSc3	rozvoj
romantismu	romantismus	k1gInSc2	romantismus
napomohl	napomoct	k5eAaPmAgInS	napomoct
mj.	mj.	kA	mj.
anglický	anglický	k2eAgInSc1d1	anglický
gotický	gotický	k2eAgInSc1d1	gotický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
vášeň	vášeň	k1gFnSc4	vášeň
pro	pro	k7c4	pro
tajemno	tajemno	k1gNnSc4	tajemno
a	a	k8xC	a
středověk	středověk	k1gInSc4	středověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
německé	německý	k2eAgNnSc1d1	německé
preromantické	preromantický	k2eAgNnSc1d1	preromantický
literární	literární	k2eAgNnSc1d1	literární
hnutí	hnutí	k1gNnSc1	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
(	(	kIx(	(
<g/>
Bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
vzdor	vzdor	k1gInSc1	vzdor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
několik	několik	k4yIc1	několik
generací	generace	k1gFnPc2	generace
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
