<s>
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
výškové	výškový	k2eAgFnSc2d1	výšková
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
souvisle	souvisle	k6eAd1	souvisle
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
výška	výška	k1gFnSc1	výška
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
150	[number]	k4	150
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
