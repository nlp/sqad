<s>
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
výškové	výškový	k2eAgFnSc2d1	výšková
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
souvisle	souvisle	k6eAd1	souvisle
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
výška	výška	k1gFnSc1	výška
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
150	[number]	k4	150
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
se	se	k3xPyFc4	se
nepovažuje	považovat	k5eNaImIp3nS	považovat
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
či	či	k8xC	či
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
podoba	podoba	k1gFnSc1	podoba
výrazu	výraz	k1gInSc2	výraz
(	(	kIx(	(
<g/>
skyscraper	skyscraper	k1gInSc1	skyscraper
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
první	první	k4xOgInPc1	první
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
postaveny	postaven	k2eAgInPc1d1	postaven
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
do	do	k7c2	do
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
současným	současný	k2eAgInSc7d1	současný
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
Burdž	Burdž	k1gFnSc1	Burdž
Chalífa	chalífa	k1gMnSc1	chalífa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
828	[number]	k4	828
m	m	kA	m
a	a	k8xC	a
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
výška	výška	k1gFnSc1	výška
motivována	motivovat	k5eAaBmNgFnS	motivovat
praktickými	praktický	k2eAgFnPc7d1	praktická
(	(	kIx(	(
<g/>
např.	např.	kA	např.
majáky	maják	k1gInPc1	maják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ideovými	ideový	k2eAgInPc7d1	ideový
(	(	kIx(	(
<g/>
kostelní	kostelní	k2eAgFnSc1d1	kostelní
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
vedly	vést	k5eAaImAgInP	vést
důvody	důvod	k1gInPc1	důvod
komerční	komerční	k2eAgInPc1d1	komerční
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
růstem	růst	k1gInSc7	růst
městské	městský	k2eAgFnSc2d1	městská
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zvyšovaly	zvyšovat	k5eAaImAgInP	zvyšovat
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zvýšení	zvýšení	k1gNnSc3	zvýšení
cen	cena	k1gFnPc2	cena
stavebních	stavební	k2eAgInPc2d1	stavební
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
přímým	přímý	k2eAgInSc7d1	přímý
impulzem	impulz	k1gInSc7	impulz
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
sehrál	sehrát	k5eAaPmAgInS	sehrát
také	také	k9	také
začátek	začátek	k1gInSc1	začátek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
výtahů	výtah	k1gInPc2	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Výtahy	výtah	k1gInPc1	výtah
najednou	najednou	k6eAd1	najednou
zatraktivnily	zatraktivnit	k5eAaPmAgInP	zatraktivnit
horní	horní	k2eAgNnSc4d1	horní
patra	patro	k1gNnPc4	patro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyl	být	k5eNaImAgInS	být
hluk	hluk	k1gInSc1	hluk
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
výhled	výhled	k1gInSc1	výhled
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
často	často	k6eAd1	často
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
největší	veliký	k2eAgInSc1d3	veliký
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
evakuace	evakuace	k1gFnSc2	evakuace
a	a	k8xC	a
záchranné	záchranný	k2eAgFnSc2d1	záchranná
práce	práce	k1gFnSc2	práce
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgInPc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
nežádoucími	žádoucí	k2eNgInPc7d1	nežádoucí
jevy	jev	k1gInPc7	jev
bylo	být	k5eAaImAgNnS	být
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
a	a	k8xC	a
extrémní	extrémní	k2eAgNnSc1d1	extrémní
meteorologické	meteorologický	k2eAgNnSc1d1	meteorologické
zatížení	zatížení	k1gNnSc1	zatížení
(	(	kIx(	(
<g/>
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
prudký	prudký	k2eAgInSc1d1	prudký
a	a	k8xC	a
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnPc1d1	vysoká
nebo	nebo	k8xC	nebo
nízké	nízký	k2eAgFnPc1d1	nízká
venkovní	venkovní	k2eAgFnPc1d1	venkovní
teploty	teplota	k1gFnPc1	teplota
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byla	být	k5eAaImAgFnS	být
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
pyramida	pyramida	k1gFnSc1	pyramida
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
146	[number]	k4	146
m	m	kA	m
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
překonávala	překonávat	k5eAaImAgFnS	překonávat
160	[number]	k4	160
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
Lincolnská	Lincolnský	k2eAgFnSc1d1	Lincolnský
katedrála	katedrála	k1gFnSc1	katedrála
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1311	[number]	k4	1311
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1549	[number]	k4	1549
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc1d1	centrální
věž	věž	k1gFnSc1	věž
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Překonána	překonán	k2eAgFnSc1d1	překonána
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
Washingtonovo	Washingtonův	k2eAgNnSc1d1	Washingtonovo
monumentem	monument	k1gInSc7	monument
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
169	[number]	k4	169
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nebyly	být	k5eNaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
stavby	stavba	k1gFnPc1	stavba
trvale	trvale	k6eAd1	trvale
obyvatelné	obyvatelný	k2eAgFnPc1d1	obyvatelná
<g/>
,	,	kIx,	,
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
moderní	moderní	k2eAgFnSc4d1	moderní
definici	definice	k1gFnSc4	definice
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
výškové	výškový	k2eAgFnPc1d1	výšková
budovy	budova	k1gFnPc1	budova
vyrůstaly	vyrůstat	k5eAaImAgFnP	vyrůstat
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
starověké	starověký	k2eAgFnPc4d1	starověká
římské	římský	k2eAgFnPc4d1	římská
budovy	budova	k1gFnPc4	budova
v	v	k7c6	v
císařských	císařský	k2eAgNnPc6d1	císařské
městech	město	k1gNnPc6	město
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
deseti	deset	k4xCc7	deset
a	a	k8xC	a
více	hodně	k6eAd2	hodně
pater	patro	k1gNnPc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
aplikoval	aplikovat	k5eAaBmAgInS	aplikovat
na	na	k7c4	na
ocelové	ocelový	k2eAgFnPc4d1	ocelová
stavby	stavba	k1gFnPc4	stavba
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
deseti	deset	k4xCc7	deset
podlažími	podlaží	k1gNnPc7	podlaží
v	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
litina	litina	k1gFnSc1	litina
a	a	k8xC	a
ocel	ocel	k1gFnSc1	ocel
nahradila	nahradit	k5eAaPmAgFnS	nahradit
cihly	cihla	k1gFnPc4	cihla
jako	jako	k8xS	jako
základní	základní	k2eAgInSc4d1	základní
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
budovy	budova	k1gFnPc4	budova
byly	být	k5eAaImAgFnP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
výtahy	výtah	k1gInPc1	výtah
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc1	rozvoj
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
urychlil	urychlit	k5eAaPmAgInS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
během	během	k7c2	během
programu	program	k1gInSc2	program
jeho	jeho	k3xOp3gFnSc2	jeho
zásadní	zásadní	k2eAgFnSc2d1	zásadní
přestavby	přestavba	k1gFnSc2	přestavba
po	po	k7c6	po
katastrofálním	katastrofální	k2eAgInSc6d1	katastrofální
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
Detroit	Detroit	k1gInSc1	Detroit
a	a	k8xC	a
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
brzdily	brzdit	k5eAaImAgFnP	brzdit
rozvoj	rozvoj	k1gInSc4	rozvoj
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
obavy	obava	k1gFnSc2	obava
z	z	k7c2	z
estetiky	estetika	k1gFnSc2	estetika
a	a	k8xC	a
požární	požární	k2eAgFnSc2d1	požární
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
významnější	významný	k2eAgFnPc1d2	významnější
výjimky	výjimka	k1gFnPc1	výjimka
byly	být	k5eAaImAgFnP	být
např.	např.	kA	např.
90	[number]	k4	90
m	m	kA	m
vysoký	vysoký	k2eAgMnSc1d1	vysoký
Royal	Royal	k1gMnSc1	Royal
Liver	livra	k1gFnPc2	livra
Building	Building	k1gInSc4	Building
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
57	[number]	k4	57
m	m	kA	m
vysoký	vysoký	k2eAgMnSc1d1	vysoký
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Marx	Marx	k1gMnSc1	Marx
House	house	k1gNnSc1	house
v	v	k7c6	v
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
;	;	kIx,	;
61	[number]	k4	61
m	m	kA	m
vysoké	vysoký	k2eAgInPc4d1	vysoký
Kungstornen	Kungstornen	k1gInSc4	Kungstornen
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
;	;	kIx,	;
89	[number]	k4	89
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
budova	budova	k1gFnSc1	budova
Edificio	Edificio	k1gMnSc1	Edificio
Telefónica	Telefónica	k1gMnSc1	Telefónica
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
;	;	kIx,	;
87,5	[number]	k4	87,5
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
Boerentoren	Boerentorna	k1gFnPc2	Boerentorna
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
;	;	kIx,	;
66	[number]	k4	66
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
Prudential	Prudential	k1gInSc4	Prudential
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
a	a	k8xC	a
108	[number]	k4	108
<g/>
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
Torre	torr	k1gInSc5	torr
Piacentini	Piacentin	k2eAgMnPc1d1	Piacentin
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
státní	státní	k2eAgFnSc2d1	státní
univerzity	univerzita	k1gFnSc2	univerzita
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
spolu	spolu	k6eAd1	spolu
soutěžily	soutěžit	k5eAaImAgInP	soutěžit
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
budovu	budova	k1gFnSc4	budova
světa	svět	k1gInSc2	svět
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
vedení	vedení	k1gNnSc4	vedení
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
dokončením	dokončení	k1gNnSc7	dokončení
103	[number]	k4	103
metrů	metr	k1gInPc2	metr
vysoké	vysoká	k1gFnSc2	vysoká
American	Americana	k1gFnPc2	Americana
Surety	Sureta	k1gFnSc2	Sureta
Building	Building	k1gInSc1	Building
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
dále	daleko	k6eAd2	daleko
držel	držet	k5eAaImAgInS	držet
toto	tento	k3xDgNnSc4	tento
prvenství	prvenství	k1gNnSc4	prvenství
se	s	k7c7	s
282	[number]	k4	282
vysokou	vysoký	k2eAgFnSc7d1	vysoká
Chrysler	Chrysler	k1gMnSc1	Chrysler
Building	Building	k1gInSc1	Building
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
a	a	k8xC	a
381	[number]	k4	381
m	m	kA	m
vysokou	vysoký	k2eAgFnSc7d1	vysoká
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Světové	světový	k2eAgNnSc1d1	světové
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
417	[number]	k4	417
m	m	kA	m
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
poté	poté	k6eAd1	poté
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
pouhých	pouhý	k2eAgNnPc6d1	pouhé
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
překonala	překonat	k5eAaPmAgFnS	překonat
Sears	Sears	k1gInSc4	Sears
Tower	Towra	k1gFnPc2	Towra
(	(	kIx(	(
<g/>
442	[number]	k4	442
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
24	[number]	k4	24
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
překonaná	překonaný	k2eAgFnSc1d1	překonaná
452	[number]	k4	452
m	m	kA	m
vysokými	vysoký	k2eAgInPc7d1	vysoký
Petronas	Petronas	k1gInSc4	Petronas
Twin	Twin	k1gInSc4	Twin
Towers	Towersa	k1gFnPc2	Towersa
v	v	k7c4	v
Kuala	Kual	k1gMnSc4	Kual
Lumpuru	Lumpur	k1gInSc2	Lumpur
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
držely	držet	k5eAaImAgFnP	držet
titul	titul	k1gInSc4	titul
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Burdž	Burdž	k1gFnSc1	Burdž
Chalífa	chalífa	k1gMnSc1	chalífa
(	(	kIx(	(
<g/>
830	[number]	k4	830
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Věž	věž	k1gFnSc1	věž
Federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
374	[number]	k4	374
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
One	One	k1gMnSc1	One
World	World	k1gMnSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
417	[number]	k4	417
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
postavena	postaven	k2eAgFnSc1d1	postavena
osmipodlažní	osmipodlažní	k2eAgFnSc1d1	osmipodlažní
činžovní	činžovní	k2eAgFnSc1d1	činžovní
budova	budova	k1gFnSc1	budova
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
ČSR	ČSR	kA	ČSR
nová	nový	k2eAgFnSc1d1	nová
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
Spolku	spolek	k1gInSc2	spolek
pro	pro	k7c4	pro
chemickou	chemický	k2eAgFnSc4d1	chemická
a	a	k8xC	a
hutní	hutní	k2eAgFnSc4d1	hutní
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
od	od	k7c2	od
drážďanské	drážďanský	k2eAgFnSc2d1	Drážďanská
architektonické	architektonický	k2eAgFnSc2d1	architektonická
kanceláře	kancelář	k1gFnSc2	kancelář
Lossow	Lossow	k1gFnSc2	Lossow
a	a	k8xC	a
Kühne	Kühn	k1gInSc5	Kühn
(	(	kIx(	(
<g/>
10	[number]	k4	10
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
43	[number]	k4	43
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
pražským	pražský	k2eAgInSc7d1	pražský
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
byl	být	k5eAaImAgInS	být
52	[number]	k4	52
m	m	kA	m
vysoký	vysoký	k2eAgInSc1d1	vysoký
funkcionalistický	funkcionalistický	k2eAgInSc1d1	funkcionalistický
palác	palác	k1gInSc1	palác
bývalého	bývalý	k2eAgInSc2d1	bývalý
Všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
penzijního	penzijní	k2eAgInSc2d1	penzijní
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Dům	dům	k1gInSc1	dům
odborových	odborový	k2eAgInPc2d1	odborový
svazů	svaz	k1gInPc2	svaz
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Havlíček	Havlíček	k1gMnSc1	Havlíček
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Honzík	Honzík	k1gMnSc1	Honzík
<g/>
)	)	kIx)	)
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
tzv.	tzv.	kA	tzv.
Baťův	Baťův	k2eAgInSc1d1	Baťův
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
77,5	[number]	k4	77,5
m	m	kA	m
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
za	za	k7c7	za
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
Boerentoren	Boerentorna	k1gFnPc2	Boerentorna
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
výškových	výškový	k2eAgInPc2d1	výškový
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
AZ	AZ	kA	AZ
Tower	Tower	k1gMnSc1	Tower
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
brněnský	brněnský	k2eAgInSc1d1	brněnský
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
AZ	AZ	kA	AZ
Tower	Tower	k1gInSc1	Tower
v	v	k7c6	v
Jižním	jižní	k2eAgNnSc6d1	jižní
centru	centrum	k1gNnSc6	centrum
se	s	k7c7	s
111	[number]	k4	111
metry	metro	k1gNnPc7	metro
<g/>
.	.	kIx.	.
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
AZ	AZ	kA	AZ
Tower	Tower	k1gInSc1	Tower
překonal	překonat	k5eAaPmAgInS	překonat
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
–	–	k?	–
budova	budova	k1gFnSc1	budova
FSI	FSI	kA	FSI
VUT	VUT	kA	VUT
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
AZ	AZ	kA	AZ
Toweru	Tower	k1gInSc2	Tower
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
Tower	Towra	k1gFnPc2	Towra
Druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
109	[number]	k4	109
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
City	city	k1gNnSc1	city
Tower	Towra	k1gFnPc2	Towra
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
sídlo	sídlo	k1gNnSc1	sídlo
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
kanceláře	kancelář	k1gFnPc1	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
Empiria	Empirium	k1gNnSc2	Empirium
Třetí	třetí	k4xOgInSc1	třetí
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
City	city	k1gNnSc1	city
Empiria	Empirium	k1gNnSc2	Empirium
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc1	sídlo
firmy	firma	k1gFnSc2	firma
PZO	PZO	kA	PZO
Motokov	Motokov	k1gInSc1	Motokov
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
využívána	využíván	k2eAgFnSc1d1	využívána
především	především	k9	především
jako	jako	k8xC	jako
administrativní	administrativní	k2eAgFnSc1d1	administrativní
budova	budova	k1gFnSc1	budova
<g/>
.	.	kIx.	.
</s>
