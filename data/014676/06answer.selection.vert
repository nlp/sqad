<s>
Roku	rok	k1gInSc2
1783	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
Štěkni	štěknout	k5eAaPmRp2nS,k5eAaImRp2nS
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Annou	Anna	k1gFnSc7
Herzovou	Herzová	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mu	on	k3xPp3gMnSc3
přinesla	přinést	k5eAaPmAgFnS
věnem	věno	k1gNnSc7
2	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
a	a	k8xC
s	s	k7c7
níž	jenž	k3xRgFnSc7
měl	mít	k5eAaImAgMnS
celkem	celkem	k6eAd1
šest	šest	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1784	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tereza	Tereza	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1786	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
Josefa	Josefa	k1gFnSc1
<g/>
,	,	kIx,
Johanka	Johanka	k1gFnSc1
a	a	k8xC
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>