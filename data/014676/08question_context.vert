<s>
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
nebo	nebo	k8xC
také	také	k9
Franz	Franz	k1gInSc1
Andreas	Andreasa	k1gFnPc2
Paupie	Paupie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1753	#num#	k4
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
Šternberk	Šternberk	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
významné	významný	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
českého	český	k2eAgNnSc2d1
pivovarnictví	pivovarnictví	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přímo	přímo	k6eAd1
zakladatelem	zakladatel	k1gMnSc7
českého	český	k2eAgNnSc2d1
odborného	odborný	k2eAgNnSc2d1
pivovarnictví	pivovarnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>