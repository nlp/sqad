<s>
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1753	#num#	k4
<g/>
Český	český	k2eAgInSc1d1
Šternberk	Šternberk	k1gInSc1
<g/>
,	,	kIx,
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Moravské	moravský	k2eAgNnSc1d1
markrabství	markrabství	k1gNnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Herzova	Herzův	k2eAgFnSc1d1
(	(	kIx(
<g/>
1783	#num#	k4
<g/>
–	–	k?
<g/>
1805	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Anna	Anna	k1gFnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
Josefa	Josefa	k1gFnSc1
<g/>
,	,	kIx,
Johanka	Johanka	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
nebo	nebo	k8xC
také	také	k9
Franz	Franz	k1gInSc1
Andreas	Andreasa	k1gFnPc2
Paupie	Paupie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1753	#num#	k4
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
Šternberk	Šternberk	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
významné	významný	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
českého	český	k2eAgNnSc2d1
pivovarnictví	pivovarnictví	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přímo	přímo	k6eAd1
zakladatelem	zakladatel	k1gMnSc7
českého	český	k2eAgNnSc2d1
odborného	odborný	k2eAgNnSc2d1
pivovarnictví	pivovarnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
ho	on	k3xPp3gNnSc2
nazvaly	nazvat	k5eAaPmAgFnP,k5eAaBmAgFnP
„	„	k?
<g/>
geniálním	geniální	k2eAgMnSc7d1
sládkem	sládek	k1gMnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
zajímal	zajímat	k5eAaImAgMnS
se	se	k3xPyFc4
o	o	k7c4
kvasnou	kvasný	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
<g/>
,	,	kIx,
proslul	proslout	k5eAaPmAgMnS
také	také	k9
jako	jako	k9
konstruktér	konstruktér	k1gMnSc1
pivní	pivní	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Byl	být	k5eAaImAgInS
synem	syn	k1gMnSc7
kováře	kovář	k1gMnSc2
Františka	František	k1gMnSc2
a	a	k8xC
matky	matka	k1gFnSc2
Anny	Anna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc4d1
vzdělání	vzdělání	k1gNnSc1
nabyl	nabýt	k5eAaPmAgInS
v	v	k7c6
Českém	český	k2eAgInSc6d1
Šternberku	Šternberk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
se	se	k3xPyFc4
vyučil	vyučit	k5eAaPmAgInS
u	u	k7c2
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
sládkem	sládek	k1gMnSc7
ve	v	k7c6
Velké	velká	k1gFnSc6
Bíteši	Bítech	k1gMnPc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
František	František	k1gMnSc1
působil	působit	k5eAaImAgMnS
i	i	k9
v	v	k7c6
pivovaru	pivovar	k1gInSc6
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkušenosti	zkušenost	k1gFnSc2
o	o	k7c6
výrobě	výroba	k1gFnSc6
piva	pivo	k1gNnSc2
získával	získávat	k5eAaImAgMnS
i	i	k9
v	v	k7c6
cizině	cizina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1780	#num#	k4
byl	být	k5eAaImAgInS
sládkem	sládek	k1gMnSc7
v	v	k7c6
pivovaru	pivovar	k1gInSc6
knížete	kníže	k1gNnSc2wR
Windischgrätze	Windischgrätze	k1gFnSc2
ve	v	k7c6
Štěkni	štěknout	k5eAaImRp2nS,k5eAaPmRp2nS
na	na	k7c6
Strakonicku	Strakonicko	k1gNnSc3
<g/>
,	,	kIx,
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1787	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
sládkem	sládek	k1gMnSc7
v	v	k7c6
pivovaře	pivovar	k1gInSc6
v	v	k7c6
Tachově	Tachov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1783	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
Štěkni	štěknout	k5eAaPmRp2nS,k5eAaImRp2nS
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Annou	Anna	k1gFnSc7
Herzovou	Herzová	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mu	on	k3xPp3gMnSc3
přinesla	přinést	k5eAaPmAgFnS
věnem	věno	k1gNnSc7
2	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
a	a	k8xC
s	s	k7c7
níž	jenž	k3xRgFnSc7
měl	mít	k5eAaImAgMnS
celkem	celkem	k6eAd1
šest	šest	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1784	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tereza	Tereza	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1786	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
Josefa	Josefa	k1gFnSc1
<g/>
,	,	kIx,
Johanka	Johanka	k1gFnSc1
a	a	k8xC
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
s	s	k7c7
rodinou	rodina	k1gFnSc7
přestěhoval	přestěhovat	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
pro	pro	k7c4
pivovar	pivovar	k1gInSc4
v	v	k7c6
Jinonicích	Jinonice	k1gFnPc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozšiřoval	rozšiřovat	k5eAaImAgMnS
si	se	k3xPyFc3
své	své	k1gNnSc4
teoretické	teoretický	k2eAgFnSc2d1
znalosti	znalost	k1gFnSc2
a	a	k8xC
zaváděl	zavádět	k5eAaImAgMnS
je	být	k5eAaImIp3nS
do	do	k7c2
praxe	praxe	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
zlepšovaly	zlepšovat	k5eAaImAgFnP
metody	metoda	k1gFnPc1
výroby	výroba	k1gFnSc2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
dvoudílný	dvoudílný	k2eAgInSc4d1
spis	spis	k1gInSc4
–	–	k?
Umění	umění	k1gNnSc2
vařit	vařit	k5eAaImF
pivo	pivo	k1gNnSc4
fyzicko-chemicko-hospodářsky	fyzicko-chemicko-hospodářsky	k6eAd1
popsané	popsaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
díle	dílo	k1gNnSc6
je	být	k5eAaImIp3nS
popsaný	popsaný	k2eAgInSc1d1
postup	postup	k1gInSc1
při	při	k7c6
výrobě	výroba	k1gFnSc6
piva	pivo	k1gNnSc2
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	s	k7c7
první	první	k4xOgFnSc7
českou	český	k2eAgFnSc7d1
vědeckou	vědecký	k2eAgFnSc7d1
publikací	publikace	k1gFnSc7
o	o	k7c6
výrobě	výroba	k1gFnSc6
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Poupě	Poupě	k1gMnSc1
onemocněl	onemocnět	k5eAaPmAgMnS
patrně	patrně	k6eAd1
plicní	plicní	k2eAgFnSc7d1
chorobou	choroba	k1gFnSc7
a	a	k8xC
byl	být	k5eAaImAgInS
také	také	k9
velice	velice	k6eAd1
zklamán	zklamat	k5eAaPmNgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
nenacházelo	nacházet	k5eNaImAgNnS
takový	takový	k3xDgInSc4
ohlas	ohlas	k1gInSc4
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
si	se	k3xPyFc3
představoval	představovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těžké	těžký	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
mu	on	k3xPp3gNnSc3
pomohla	pomoct	k5eAaPmAgFnS
hraběnka	hraběnka	k1gFnSc1
Marianna	Marianna	k1gFnSc1
Clam-Martinicová	Clam-Martinicový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
mu	on	k3xPp3gMnSc3
nabídla	nabídnout	k5eAaPmAgFnS
místo	místo	k7c2
sládka	sládek	k1gMnSc2
ve	v	k7c6
Slaném	Slaný	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
zde	zde	k6eAd1
Poupě	Poupě	k1gMnSc1
začal	začít	k5eAaPmAgMnS
používat	používat	k5eAaImF
teploměr	teploměr	k1gInSc4
a	a	k8xC
takzvanou	takzvaný	k2eAgFnSc4d1
pivní	pivní	k2eAgFnSc4d1
váhu	váha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1801	#num#	k4
vydal	vydat	k5eAaPmAgMnS
takovou	takový	k3xDgFnSc4
první	první	k4xOgFnSc4
českou	český	k2eAgFnSc4d1
učebnici	učebnice	k1gFnSc4
o	o	k7c6
pivovarnictví	pivovarnictví	k1gNnSc6
–	–	k?
Počátkové	počátkový	k2eAgFnPc1d1
základního	základní	k2eAgNnSc2d1
naučení	naučení	k1gNnSc3
o	o	k7c6
vaření	vaření	k1gNnSc6
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1798	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
konkurs	konkurs	k1gInSc1
na	na	k7c4
sládka	sládek	k1gMnSc4
v	v	k7c6
městském	městský	k2eAgInSc6d1
pivovaru	pivovar	k1gInSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
Poupě	Poupě	k1gMnSc1
prožil	prožít	k5eAaPmAgMnS
svá	svůj	k3xOyFgNnPc4
nejlepší	dobrý	k2eAgNnPc4d3
léta	léto	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
němu	on	k3xPp3gInSc3
se	se	k3xPyFc4
zvyšovala	zvyšovat	k5eAaImAgFnS
kvalita	kvalita	k1gFnSc1
piva	pivo	k1gNnSc2
i	i	k8xC
příjmy	příjem	k1gInPc4
pivovaru	pivovar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
působení	působení	k1gNnSc2
vzrostl	vzrůst	k5eAaPmAgInS
výstav	výstav	k1gInSc1
z	z	k7c2
16	#num#	k4
tisíc	tisíc	k4xCgInPc2
na	na	k7c4
28	#num#	k4
tisíc	tisíc	k4xCgInPc2
hektolitrů	hektolitr	k1gInPc2
a	a	k8xC
Poupě	Poupě	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
žádat	žádat	k5eAaImF
o	o	k7c6
navýšení	navýšení	k1gNnSc6
maximální	maximální	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
zde	zde	k6eAd1
také	také	k9
první	první	k4xOgFnSc4
pivovarskou	pivovarský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobré	dobrý	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
o	o	k7c6
pivovaru	pivovar	k1gInSc6
se	se	k3xPyFc4
šířily	šířit	k5eAaImAgFnP
do	do	k7c2
celé	celý	k2eAgFnSc2d1
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1805	#num#	k4
na	na	k7c4
obnovenou	obnovený	k2eAgFnSc4d1
plicní	plicní	k2eAgFnSc4d1
chorobu	choroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Umění	umění	k1gNnSc1
vařit	vařit	k5eAaImF
pivo	pivo	k1gNnSc4
fyzicko-chemicko-hospodářsky	fyzicko-chemicko-hospodářsky	k6eAd1
popsané	popsaný	k2eAgNnSc4d1
–	–	k?
německy	německy	k6eAd1
</s>
<s>
Titul	titul	k1gInSc1
<g/>
:	:	kIx,
Die	Die	k1gFnSc1
Kunst	Kunst	k1gInSc1
des	des	k1gNnSc1
Bierbrauens	Bierbrauens	k1gInSc1
physisch	physisch	k1gMnSc1
-	-	kIx~
chemisch	chemisch	k1gMnSc1
-	-	kIx~
oekonomisch	oekonomisch	k1gInSc1
beschrieben	beschriebna	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1794	#num#	k4
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
3	#num#	k4
díly	díl	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
Praze	Praha	k1gFnSc6
1794	#num#	k4
Karel	Karel	k1gMnSc1
Barth	Barth	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
1820	#num#	k4
Donat	Donat	k1gInSc4
Hartmann	Hartmann	k1gMnSc1
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
Praze	Praha	k1gFnSc6
1820	#num#	k4
Donat	Donat	k1gInSc4
Hartmann	Hartmann	k1gMnSc1
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
Praze	Praha	k1gFnSc6
1821	#num#	k4
Donat	Donat	k1gInSc4
Hartmann	Hartmann	k1gMnSc1
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkové	počátkový	k2eAgNnSc1d1
základního	základní	k2eAgNnSc2d1
naučení	naučení	k1gNnSc3
o	o	k7c6
vaření	vaření	k1gNnSc6
piva	pivo	k1gNnSc2
–	–	k?
vydáno	vydán	k2eAgNnSc4d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
1801	#num#	k4
<g/>
,	,	kIx,
Antonjn	Antonjn	k1gMnSc1
Alexius	Alexius	k1gMnSc1
Sskarnycl	Sskarnycl	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
česká	český	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
učebnice	učebnice	k1gFnSc1
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Basařová	Basařová	k1gFnSc1
<g/>
,	,	kIx,
Gabriela	Gabriela	k1gFnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
-	-	kIx~
<g/>
)	)	kIx)
Hlaváček	Hlaváček	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
Nuga	Nuga	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85903-08-3	80-85903-08-3	k4
</s>
<s>
Nohejl	Nohejl	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Neslýchaná	slýchaný	k2eNgFnSc1d1
věc	věc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgFnPc1
povídky	povídka	k1gFnPc1
o	o	k7c6
"	"	kIx"
<g/>
učeném	učený	k2eAgInSc6d1
<g/>
"	"	kIx"
sládku	sládek	k1gMnSc3
F.	F.	kA
O.	O.	kA
Poupěti	poupě	k1gNnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1957	#num#	k4
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
dětské	dětský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
(	(	kIx(
<g/>
SNDK	SNDK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
edice	edice	k1gFnSc1
Živé	živá	k1gFnSc2
prameny	pramen	k1gInPc1
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
č.	č.	k?
35	#num#	k4
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bělohoubek	Bělohoubek	k1gInSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
:	:	kIx,
Život	život	k1gInSc1
a	a	k8xC
působení	působení	k1gNnSc1
Františka	František	k1gMnSc2
Ondřeje	Ondřej	k1gMnSc2
Poupěte	Poupě	k1gMnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1878	#num#	k4
<g/>
,	,	kIx,
Vydáno	vydán	k2eAgNnSc1d1
komisí	komise	k1gFnSc7
knihkupectví	knihkupectví	k1gNnPc2
Fr.	Fr.	k1gMnSc2
Řivnáče	řivnáč	k1gMnSc2
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
Encyklopedie	encyklopedie	k1gFnSc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc1d1
záznam	záznam	k1gInSc1
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
<g/>
↑	↑	k?
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
<g/>
,	,	kIx,
geniální	geniální	k2eAgMnSc1d1
sládek	sládek	k1gMnSc1
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
reformátor	reformátor	k1gMnSc1
českého	český	k2eAgNnSc2d1
pivovarství	pivovarství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1929	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Matrika	matrika	k1gFnSc1
narozených	narozený	k2eAgInPc2d1
<g/>
,	,	kIx,
Štěkeň	Štěkeň	k1gFnSc4
<g/>
,	,	kIx,
1771	#num#	k4
<g/>
-	-	kIx~
<g/>
1790	#num#	k4
<g/>
,	,	kIx,
snímek	snímek	k1gInSc1
69	#num#	k4
<g/>
,	,	kIx,
Záznam	záznam	k1gInSc1
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
<g/>
↑	↑	k?
Die	Die	k1gFnSc2
Kunst	Kunst	k1gMnSc1
des	des	k1gNnSc2
Bierbrauens	Bierbrauens	k1gInSc1
by	by	kYmCp3nS
Franz	Franz	k1gInSc4
Andreas	Andreasa	k1gFnPc2
Paupie	Paupie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
LibraryThing	LibraryThing	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Die	Die	k1gMnSc1
Kunst	Kunst	k1gFnSc4
des	des	k1gNnSc2
Bierbrauens	Bierbrauensa	k1gFnPc2
<g/>
,	,	kIx,
physisch-chemisch-ökonomisch	physisch-chemisch-ökonomischa	k1gFnPc2
beschrieben	beschriebna	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hartmann	Hartmann	k1gMnSc1
284	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
lidSAAAAcAAJ	lidSAAAAcAAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
NTK	NTK	kA
Historický	historický	k2eAgInSc1d1
fond	fond	k1gInSc1
C	C	kA
177	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Sysno	Sysno	k6eAd1
<g/>
:	:	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
612966	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Die	Die	k1gMnSc1
Kunst	Kunst	k1gFnSc4
des	des	k1gNnSc2
Bierbrauens	Bierbrauensa	k1gFnPc2
<g/>
,	,	kIx,
physisch-chemisch-ökonomisch	physisch-chemisch-ökonomischa	k1gFnPc2
beschrieben	beschriebna	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hartmann	Hartmann	k1gMnSc1
306	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
sCdSAAAAcAAJ	sCdSAAAAcAAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
NTK	NTK	kA
Historický	historický	k2eAgInSc1d1
fond	fond	k1gInSc1
C	C	kA
177	#num#	k4
[	[	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Sysno	Sysno	k6eAd1
<g/>
:	:	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
622520	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Die	Die	k1gMnSc1
Kunst	Kunst	k1gFnSc4
des	des	k1gNnSc2
Bierbrauens	Bierbrauensa	k1gFnPc2
<g/>
,	,	kIx,
physisch-chemisch-ökonomisch	physisch-chemisch-ökonomischa	k1gFnPc2
beschrieben	beschriebna	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hartmann	Hartmann	k1gMnSc1
222	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
yidSAAAAcAAJ	yidSAAAAcAAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
NTK	NTK	kA
Historický	historický	k2eAgInSc1d1
fond	fond	k1gInSc1
C	C	kA
177	#num#	k4
[	[	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Sysno	Sysno	k6eAd1
<g/>
:	:	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
622521	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
WWW.QBIZM-SERVICES.CZ	WWW.QBIZM-SERVICES.CZ	k1gMnSc1
<g/>
,	,	kIx,
QBIZM	QBIZM	kA
<g/>
,	,	kIx,
www.qbizm.cz	www.qbizm.cz	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kramerius	Kramerius	k1gMnSc1
-	-	kIx~
Monografie	monografie	k1gFnSc1
<g/>
.	.	kIx.
kramerius	kramerius	k1gInSc1
<g/>
.	.	kIx.
<g/>
nkp	nkp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WWW.QBIZM-SERVICES.CZ	WWW.QBIZM-SERVICES.CZ	k1gMnSc1
<g/>
,	,	kIx,
QBIZM	QBIZM	kA
<g/>
,	,	kIx,
www.qbizm.cz	www.qbizm.cz	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kramerius	Kramerius	k1gMnSc1
-	-	kIx~
Monografie	monografie	k1gFnSc1
<g/>
.	.	kIx.
kramerius	kramerius	k1gInSc1
<g/>
.	.	kIx.
<g/>
nkp	nkp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NOHEJL	NOHEJL	kA
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neslýchaná	slýchaný	k2eNgFnSc1d1
věc	věc	k1gFnSc1
<g/>
:	:	kIx,
tři	tři	k4xCgFnPc1
povídky	povídka	k1gFnPc1
o	o	k7c6
"	"	kIx"
<g/>
učeném	učený	k2eAgMnSc6d1
sládku	sládek	k1gMnSc6
Františku	František	k1gMnSc6
Ondřeji	Ondřej	k1gMnSc6
Poupětovi	Poupě	k1gMnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Státní	státní	k2eAgInSc1d1
nakl	nakl	k1gInSc1
<g/>
.	.	kIx.
dětské	dětský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
92	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
QnmHAAACAAJ	QnmHAAACAAJ	k1gFnPc2
<g/>
.	.	kIx.
↑	↑	k?
WWW.QBIZM-SERVICES.CZ	WWW.QBIZM-SERVICES.CZ	k1gMnSc1
<g/>
,	,	kIx,
QBIZM	QBIZM	kA
<g/>
,	,	kIx,
www.qbizm.cz	www.qbizm.cz	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kramerius	Kramerius	k1gMnSc1
-	-	kIx~
Monografie	monografie	k1gFnSc1
<g/>
.	.	kIx.
kramerius	kramerius	k1gInSc1
<g/>
.	.	kIx.
<g/>
nkp	nkp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	poupě	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
dějinách	dějiny	k1gFnPc6
</s>
<s>
Sládek	Sládek	k1gMnSc1
Josef	Josef	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Suk	Suk	k1gMnSc1
<g/>
:	:	kIx,
Nový	nový	k2eAgMnSc1d1
Poupě	Poupě	k1gMnSc1
:	:	kIx,
katechismus	katechismus	k1gInSc1
pro	pro	k7c4
sladovnické	sladovnický	k2eAgMnPc4d1
učenníky	učenník	k1gMnPc4
a	a	k8xC
tovaryše	tovaryš	k1gMnPc4
<g/>
,	,	kIx,
1880	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1100828	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
128692510	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5544	#num#	k4
2122	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2014153762	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
42896335	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2014153762	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Pivo	pivo	k1gNnSc1
</s>
