<p>
<s>
V	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
se	se	k3xPyFc4	se
jako	jako	k9	jako
jed	jed	k1gInSc1	jed
označuje	označovat	k5eAaImIp3nS	označovat
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
poruchy	porucha	k1gFnPc4	porucha
funkce	funkce	k1gFnSc2	funkce
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
chemickou	chemický	k2eAgFnSc7d1	chemická
reakcí	reakce	k1gFnSc7	reakce
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
aktivitou	aktivita	k1gFnSc7	aktivita
na	na	k7c6	na
molekulární	molekulární	k2eAgFnSc6d1	molekulární
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
a	a	k8xC	a
při	při	k7c6	při
označování	označování	k1gNnSc6	označování
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jedy	jed	k1gInPc4	jed
<g/>
"	"	kIx"	"
označují	označovat	k5eAaImIp3nP	označovat
vysoce	vysoce	k6eAd1	vysoce
toxické	toxický	k2eAgFnPc1d1	toxická
nebo	nebo	k8xC	nebo
toxické	toxický	k2eAgFnPc1d1	toxická
látky	látka	k1gFnPc1	látka
<g/>
;	;	kIx,	;
méně	málo	k6eAd2	málo
toxické	toxický	k2eAgFnPc1d1	toxická
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zdraví	zdraví	k1gNnSc1	zdraví
škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dráždivé	dráždivý	k2eAgNnSc4d1	dráždivé
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
označení	označení	k1gNnSc2	označení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
části	část	k1gFnSc6	část
Jedy	jed	k1gInPc1	jed
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
veterinární	veterinární	k2eAgMnPc4d1	veterinární
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
zoologii	zoologie	k1gFnSc6	zoologie
se	se	k3xPyFc4	se
jedy	jed	k1gInPc7	jed
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
od	od	k7c2	od
toxinů	toxin	k1gInPc2	toxin
a	a	k8xC	a
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
specifičtěji	specificky	k6eAd2	specificky
<g/>
)	)	kIx)	)
hmyzích	hmyzí	k2eAgNnPc2d1	hmyzí
<g/>
/	/	kIx~	/
<g/>
hadích	hadí	k2eAgNnPc2d1	hadí
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
jedů	jed	k1gInPc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Toxiny	toxin	k1gInPc1	toxin
jsou	být	k5eAaImIp3nP	být
jedy	jed	k1gInPc1	jed
produkované	produkovaný	k2eAgInPc1d1	produkovaný
určitou	určitý	k2eAgFnSc7d1	určitá
biologickou	biologický	k2eAgFnSc7d1	biologická
funkcí	funkce	k1gFnSc7	funkce
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Hmyzí	hmyzí	k2eAgInPc1d1	hmyzí
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
podobné	podobný	k2eAgInPc1d1	podobný
jedy	jed	k1gInPc1	jed
jsou	být	k5eAaImIp3nP	být
toxiny	toxin	k1gInPc1	toxin
vstřikované	vstřikovaný	k2eAgInPc1d1	vstřikovaný
žihadlem	žihadlo	k1gNnSc7	žihadlo
<g/>
,	,	kIx,	,
jedovými	jedový	k2eAgInPc7d1	jedový
zuby	zub	k1gInPc7	zub
apod.	apod.	kA	apod.
do	do	k7c2	do
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ostatní	ostatní	k2eAgInPc1d1	ostatní
jedy	jed	k1gInPc1	jed
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
obecně	obecně	k6eAd1	obecně
definovány	definovat	k5eAaBmNgFnP	definovat
jako	jako	k8xS	jako
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pohlcovány	pohlcován	k2eAgFnPc1d1	pohlcována
kůží	kůže	k1gFnSc7	kůže
nebo	nebo	k8xC	nebo
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
jedy	jed	k1gInPc1	jed
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
také	také	k9	také
jako	jako	k9	jako
toxiny	toxin	k1gInPc1	toxin
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
látky	látka	k1gFnPc4	látka
produkované	produkovaný	k2eAgFnPc4d1	produkovaná
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bakteriální	bakteriální	k2eAgFnPc1d1	bakteriální
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tetanus	tetanus	k1gInSc4	tetanus
nebo	nebo	k8xC	nebo
botulismus	botulismus	k1gInSc4	botulismus
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišování	rozlišování	k1gNnPc1	rozlišování
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
termíny	termín	k1gInPc7	termín
není	být	k5eNaImIp3nS	být
však	však	k9	však
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k9	ani
mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojmy	pojem	k1gInPc1	pojem
"	"	kIx"	"
<g/>
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
synonymní	synonymní	k2eAgFnPc1d1	synonymní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
fyzice	fyzika	k1gFnSc6	fyzika
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jed	jed	k1gInSc1	jed
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
blokuje	blokovat	k5eAaImIp3nS	blokovat
nebo	nebo	k8xC	nebo
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
svou	svůj	k3xOyFgFnSc7	svůj
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
neutronovou	neutronový	k2eAgFnSc4d1	neutronová
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paracelsus	Paracelsus	k1gMnSc1	Paracelsus
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
toxikologie	toxikologie	k1gFnSc2	toxikologie
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
je	být	k5eAaImIp3nS	být
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
dávce	dávka	k1gFnSc6	dávka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
jed	jed	k1gInSc1	jed
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
často	často	k6eAd1	často
kolokviálně	kolokviálně	k6eAd1	kolokviálně
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
škodlivé	škodlivý	k2eAgFnSc2d1	škodlivá
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
žíravin	žíravina	k1gFnPc2	žíravina
<g/>
,	,	kIx,	,
karcinogenů	karcinogen	k1gInPc2	karcinogen
<g/>
,	,	kIx,	,
mutagenů	mutagen	k1gInPc2	mutagen
<g/>
,	,	kIx,	,
teratogenů	teratogen	k1gInPc2	teratogen
a	a	k8xC	a
škodlivých	škodlivý	k2eAgInPc2d1	škodlivý
polutantů	polutant	k1gInPc2	polutant
<g/>
,	,	kIx,	,
a	a	k8xC	a
ke	k	k7c3	k
zveličování	zveličování	k1gNnSc3	zveličování
nebezpečnosti	nebezpečnost	k1gFnSc2	nebezpečnost
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnSc1d1	právní
definice	definice	k1gFnSc1	definice
"	"	kIx"	"
<g/>
jedu	jed	k1gInSc2	jed
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
však	však	k9	však
přísnější	přísný	k2eAgMnSc1d2	přísnější
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gMnSc1	otrava
z	z	k7c2	z
lékařského	lékařský	k2eAgInSc2d1	lékařský
pohledu	pohled	k1gInSc2	pohled
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsoben	k2eAgFnSc1d1	způsobena
i	i	k9	i
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
z	z	k7c2	z
právního	právní	k2eAgInSc2d1	právní
pohledu	pohled	k1gInSc2	pohled
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c7	mezi
jedy	jed	k1gInPc7	jed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
jedy	jed	k1gInPc1	jed
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
4500	[number]	k4	4500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jako	jako	k9	jako
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jako	jako	k9	jako
léky	lék	k1gInPc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Jedy	jed	k1gInPc1	jed
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
primitivními	primitivní	k2eAgInPc7d1	primitivní
kmeny	kmen	k1gInPc7	kmen
a	a	k8xC	a
civilizacemi	civilizace	k1gFnPc7	civilizace
na	na	k7c4	na
lov	lov	k1gInSc4	lov
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
smrti	smrt	k1gFnSc2	smrt
kořisti	kořist	k1gFnSc2	kořist
nebo	nebo	k8xC	nebo
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
využití	využití	k1gNnSc1	využití
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgMnSc1d2	dokonalejší
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
starověkých	starověký	k2eAgInPc2d1	starověký
národů	národ	k1gInPc2	národ
začalo	začít	k5eAaPmAgNnS	začít
kovat	kovat	k5eAaImF	kovat
na	na	k7c4	na
jedy	jed	k1gInPc4	jed
speciální	speciální	k2eAgFnSc2d1	speciální
zbraně	zbraň	k1gFnSc2	zbraň
a	a	k8xC	a
příslušenství	příslušenství	k1gNnSc2	příslušenství
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
patřil	patřit	k5eAaImAgInS	patřit
jed	jed	k1gInSc1	jed
k	k	k7c3	k
běžnějším	běžný	k2eAgFnPc3d2	běžnější
zbraním	zbraň	k1gFnPc3	zbraň
pro	pro	k7c4	pro
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
331	[number]	k4	331
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
otrávená	otrávený	k2eAgNnPc4d1	otrávené
jídla	jídlo	k1gNnPc4	jídlo
nebo	nebo	k8xC	nebo
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
jedů	jed	k1gInPc2	jed
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
společenské	společenský	k2eAgFnSc6d1	společenská
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
šlechta	šlechta	k1gFnSc1	šlechta
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používala	používat	k5eAaImAgFnS	používat
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
různých	různý	k2eAgMnPc2d1	různý
oponentů	oponent	k1gMnPc2	oponent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdokonalilo	zdokonalit	k5eAaPmAgNnS	zdokonalit
zjišťování	zjišťování	k1gNnSc4	zjišťování
jedů	jed	k1gInPc2	jed
<g/>
,	,	kIx,	,
a	a	k8xC	a
přebíjení	přebíjení	k1gNnSc1	přebíjení
jejich	jejich	k3xOp3gInPc2	jejich
účinků	účinek	k1gInPc2	účinek
protijedy	protijed	k1gInPc4	protijed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vznikaly	vznikat	k5eAaImAgFnP	vznikat
stále	stále	k6eAd1	stále
nové	nový	k2eAgFnPc1d1	nová
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
jedu	jed	k1gInSc2	jed
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vraždy	vražda	k1gFnSc2	vražda
stále	stále	k6eAd1	stále
nižší	nízký	k2eAgMnSc1d2	nižší
<g/>
,	,	kIx,	,
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
se	se	k3xPyFc4	se
však	však	k9	však
riziko	riziko	k1gNnSc1	riziko
náhodné	náhodný	k2eAgFnPc1d1	náhodná
otravy	otrava	k1gFnPc1	otrava
<g/>
,	,	kIx,	,
jedy	jed	k1gInPc1	jed
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
např.	např.	kA	např.
v	v	k7c6	v
pesticidech	pesticid	k1gInPc6	pesticid
<g/>
,	,	kIx,	,
dezinfekčních	dezinfekční	k2eAgInPc6d1	dezinfekční
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
roztocích	roztok	k1gInPc6	roztok
na	na	k7c4	na
čištění	čištění	k1gNnSc4	čištění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
v	v	k7c6	v
odlehlých	odlehlý	k2eAgInPc6d1	odlehlý
a	a	k8xC	a
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
např.	např.	kA	např.
odranec	odranec	k1gMnSc1	odranec
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
štírů	štír	k1gMnPc2	štír
a	a	k8xC	a
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
existuje	existovat	k5eAaImIp3nS	existovat
žába	žába	k1gFnSc1	žába
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kuňkovitých	kuňkovitý	k2eAgInPc2d1	kuňkovitý
s	s	k7c7	s
tak	tak	k6eAd1	tak
prudkým	prudký	k2eAgInSc7d1	prudký
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
některé	některý	k3yIgInPc1	některý
domorodé	domorodý	k2eAgInPc1d1	domorodý
kmeny	kmen	k1gInPc1	kmen
namáčejí	namáčet	k5eAaImIp3nP	namáčet
hroty	hrot	k1gInPc4	hrot
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejznámější	známý	k2eAgInPc1d3	nejznámější
jedy	jed	k1gInPc1	jed
==	==	k?	==
</s>
</p>
<p>
<s>
Častými	častý	k2eAgInPc7d1	častý
jedy	jed	k1gInPc7	jed
jsou	být	k5eAaImIp3nP	být
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc1	chrom
<g/>
,	,	kIx,	,
kadmium	kadmium	k1gNnSc1	kadmium
<g/>
,	,	kIx,	,
antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
baryum	baryum	k1gNnSc1	baryum
<g/>
,	,	kIx,	,
thallium	thallium	k1gNnSc1	thallium
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jedy	jed	k1gInPc1	jed
vznikající	vznikající	k2eAgInPc1d1	vznikající
jako	jako	k8xS	jako
produkt	produkt	k1gInSc1	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
botulotoxin	botulotoxin	k1gInSc1	botulotoxin
<g/>
)	)	kIx)	)
a	a	k8xC	a
plísní	plísnit	k5eAaImIp3nS	plísnit
aflatoxin	aflatoxin	k1gInSc4	aflatoxin
či	či	k8xC	či
vytvářené	vytvářený	k2eAgNnSc4d1	vytvářené
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
rostlinami	rostlina	k1gFnPc7	rostlina
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
či	či	k8xC	či
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
(	(	kIx(	(
<g/>
jed	jed	k1gInSc1	jed
kobry	kobra	k1gFnSc2	kobra
indické	indický	k2eAgFnSc2d1	indická
<g/>
,	,	kIx,	,
kurare	kurare	k1gNnPc1	kurare
<g/>
,	,	kIx,	,
amanitiny	amanitin	k1gInPc1	amanitin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
některé	některý	k3yIgInPc1	některý
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
polonium	polonium	k1gNnSc1	polonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
jedy	jed	k1gInPc4	jed
patří	patřit	k5eAaImIp3nS	patřit
===	===	k?	===
</s>
</p>
<p>
<s>
kyanidy	kyanid	k1gInPc1	kyanid
</s>
</p>
<p>
<s>
kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
cyankáli	cyankáli	k1gNnSc4	cyankáli
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
arsenu	arsen	k1gInSc2	arsen
</s>
</p>
<p>
<s>
oxid	oxid	k1gInSc1	oxid
arsenitý	arsenitý	k2eAgInSc1d1	arsenitý
neboli	neboli	k8xC	neboli
arsenik	arsenik	k1gInSc1	arsenik
</s>
</p>
<p>
<s>
rtuť	rtuť	k1gFnSc1	rtuť
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
sloučeniny	sloučenina	k1gFnPc1	sloučenina
</s>
</p>
<p>
<s>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
beryllia	beryllium	k1gNnSc2	beryllium
</s>
</p>
<p>
<s>
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
</s>
</p>
<p>
<s>
methanol	methanol	k1gInSc1	methanol
</s>
</p>
<p>
<s>
dioxiny	dioxin	k1gInPc1	dioxin
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejsilnějším	silný	k2eAgInPc3d3	nejsilnější
známým	známý	k2eAgInPc3d1	známý
jedům	jed	k1gInPc3	jed
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
strychnin	strychnin	k1gInSc1	strychnin
</s>
</p>
<p>
<s>
thallium	thallium	k1gNnSc1	thallium
</s>
</p>
<p>
<s>
kurare	kurare	k1gNnSc1	kurare
</s>
</p>
<p>
<s>
amanitiny	amanitina	k1gFnPc1	amanitina
</s>
</p>
<p>
<s>
muskarin	muskarin	k1gInSc1	muskarin
</s>
</p>
<p>
<s>
nikotin	nikotin	k1gInSc1	nikotin
</s>
</p>
<p>
<s>
polonium	polonium	k1gNnSc1	polonium
(	(	kIx(	(
<g/>
netradiční	tradiční	k2eNgInSc1d1	netradiční
jed	jed	k1gInSc1	jed
použitý	použitý	k2eAgInSc1d1	použitý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
k	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
Alexandera	Alexandera	k1gFnSc1	Alexandera
Litvinenka	Litvinenka	k1gFnSc1	Litvinenka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
botulotoxin	botulotoxin	k1gInSc1	botulotoxin
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klobásový	klobásový	k2eAgInSc1d1	klobásový
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
nejsilnější	silný	k2eAgInSc1d3	nejsilnější
známý	známý	k2eAgInSc1d1	známý
přírodní	přírodní	k2eAgInSc1d1	přírodní
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
rozdávkovaný	rozdávkovaný	k2eAgInSc1d1	rozdávkovaný
1	[number]	k4	1
kg	kg	kA	kg
by	by	kYmCp3nS	by
stačil	stačit	k5eAaBmAgInS	stačit
k	k	k7c3	k
vyhubení	vyhubení	k1gNnSc3	vyhubení
celého	celý	k2eAgNnSc2d1	celé
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ricin	ricin	k1gInSc1	ricin
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
vniknutí	vniknutí	k1gNnSc2	vniknutí
jedu	jed	k1gInSc2	jed
do	do	k7c2	do
organizmu	organizmus	k1gInSc2	organizmus
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
požitím	požití	k1gNnSc7	požití
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
vdechnutím	vdechnutí	k1gNnSc7	vdechnutí
nebo	nebo	k8xC	nebo
přímým	přímý	k2eAgNnSc7d1	přímé
vniknutím	vniknutí	k1gNnSc7	vniknutí
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
</s>
</p>
<p>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
škumpa	škumpa	k1gFnSc1	škumpa
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
je	být	k5eAaImIp3nS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
už	už	k9	už
jen	jen	k9	jen
dotykem	dotyk	k1gInSc7	dotyk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
produkuje	produkovat	k5eAaImIp3nS	produkovat
<g/>
,	,	kIx,	,
proniknou	proniknout	k5eAaPmIp3nP	proniknout
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
jedy	jed	k1gInPc1	jed
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
důmyslné	důmyslný	k2eAgFnPc1d1	důmyslná
a	a	k8xC	a
dokážou	dokázat	k5eAaPmIp3nP	dokázat
zvýšit	zvýšit	k5eAaPmF	zvýšit
citlivost	citlivost	k1gFnSc4	citlivost
kůže	kůže	k1gFnSc2	kůže
na	na	k7c4	na
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
potom	potom	k6eAd1	potom
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
vážné	vážný	k2eAgFnPc4d1	vážná
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
použijí	použít	k5eAaPmIp3nP	použít
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
solí	sůl	k1gFnPc2	sůl
než	než	k8xS	než
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
kovová	kovový	k2eAgFnSc1d1	kovová
rtuť	rtuť	k1gFnSc1	rtuť
prochází	procházet	k5eAaImIp3nS	procházet
lidským	lidský	k2eAgNnSc7d1	lidské
trávícím	trávící	k2eAgNnSc7d1	trávící
ústrojím	ústrojí	k1gNnSc7	ústrojí
bez	bez	k7c2	bez
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
plombách	plomba	k1gFnPc6	plomba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
soli	sůl	k1gFnPc1	sůl
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
inhalovaná	inhalovaný	k2eAgFnSc1d1	Inhalovaná
rtuťová	rtuťový	k2eAgFnSc1d1	rtuťová
pára	pára	k1gFnSc1	pára
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rychlost	rychlost	k1gFnSc1	rychlost
účinku	účinek	k1gInSc2	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
účinku	účinek	k1gInSc2	účinek
jedu	jed	k1gInSc2	jed
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
fyziologii	fyziologie	k1gFnSc6	fyziologie
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jedy	jed	k1gInPc1	jed
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
symptomy	symptom	k1gInPc1	symptom
takřka	takřka	k6eAd1	takřka
okamžitě	okamžitě	k6eAd1	okamžitě
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
smrtelně	smrtelně	k6eAd1	smrtelně
jedovatých	jedovatý	k2eAgNnPc2d1	jedovaté
muchomůrek	muchomůrek	k?	muchomůrek
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
až	až	k9	až
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
oranžovočerveného	oranžovočervený	k2eAgInSc2d1	oranžovočervený
pavučince	pavučinec	k1gInSc2	pavučinec
(	(	kIx(	(
<g/>
Cortinarius	Cortinarius	k1gInSc1	Cortinarius
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
projevit	projevit	k5eAaPmF	projevit
až	až	k9	až
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
možnost	možnost	k1gFnSc4	možnost
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
odhalení	odhalení	k1gNnSc2	odhalení
zdroje	zdroj	k1gInSc2	zdroj
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
houba	houba	k1gFnSc1	houba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
orellanin	orellanin	k2eAgInSc4d1	orellanin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
se	se	k3xPyFc4	se
nejrychleji	rychle	k6eAd3	rychle
projevují	projevovat	k5eAaImIp3nP	projevovat
účinky	účinek	k1gInPc1	účinek
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
arsenovodík	arsenovodík	k1gInSc1	arsenovodík
<g/>
,	,	kIx,	,
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
<g/>
,	,	kIx,	,
sulfan	sulfan	k1gInSc1	sulfan
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejpomaleji	pomale	k6eAd3	pomale
zpravidla	zpravidla	k6eAd1	zpravidla
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
jedy	jed	k1gInPc1	jed
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
(	(	kIx(	(
<g/>
ricin	ricin	k1gInSc1	ricin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
vniknutí	vniknutí	k1gNnSc2	vniknutí
jedu	jed	k1gInSc2	jed
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jedy	jed	k1gInPc7	jed
dle	dle	k7c2	dle
účinku	účinek	k1gInSc2	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
jedy	jed	k1gInPc1	jed
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
slabost	slabost	k1gFnSc4	slabost
až	až	k9	až
ochrnutí	ochrnutí	k1gNnPc1	ochrnutí
<g/>
,	,	kIx,	,
jiné	jiná	k1gFnPc1	jiná
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
oběhový	oběhový	k2eAgInSc4d1	oběhový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc4d1	jiné
napadající	napadající	k2eAgFnPc4d1	napadající
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
naruší	narušit	k5eAaPmIp3nS	narušit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
krve	krev	k1gFnSc2	krev
nebo	nebo	k8xC	nebo
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
vážné	vážný	k2eAgNnSc4d1	vážné
poškození	poškození	k1gNnSc4	poškození
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
mitotické	mitotický	k2eAgInPc4d1	mitotický
jedy	jed	k1gInPc4	jed
(	(	kIx(	(
<g/>
jedy	jed	k1gInPc4	jed
ovlivňující	ovlivňující	k2eAgFnSc2d1	ovlivňující
buněčné	buněčný	k2eAgFnSc2d1	buněčná
dělení	dělení	k1gNnSc1	dělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgInPc1d1	krevní
jedy	jed	k1gInPc1	jed
(	(	kIx(	(
<g/>
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
krev	krev	k1gFnSc1	krev
<g/>
)	)	kIx)	)
a	a	k8xC	a
nervové	nervový	k2eAgInPc4d1	nervový
jedy	jed	k1gInPc4	jed
(	(	kIx(	(
<g/>
jedy	jed	k1gInPc4	jed
ovlivňující	ovlivňující	k2eAgInPc4d1	ovlivňující
nervové	nervový	k2eAgInPc4d1	nervový
impulsy	impuls	k1gInPc4	impuls
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neurotoxiny	Neurotoxin	k1gInPc1	Neurotoxin
působící	působící	k2eAgInPc1d1	působící
na	na	k7c4	na
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
blokují	blokovat	k5eAaImIp3nP	blokovat
např.	např.	kA	např.
svalovou	svalový	k2eAgFnSc4d1	svalová
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
otrávený	otrávený	k2eAgMnSc1d1	otrávený
živočich	živočich	k1gMnSc1	živočich
dusí	dusit	k5eAaImIp3nS	dusit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
nefungují	fungovat	k5eNaImIp3nP	fungovat
dýchací	dýchací	k2eAgInPc4d1	dýchací
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jed	jed	k1gInSc1	jed
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
sval	sval	k1gInSc4	sval
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
zástavě	zástava	k1gFnSc3	zástava
srdce	srdce	k1gNnSc2	srdce
apod.	apod.	kA	apod.
Jedy	jed	k1gInPc1	jed
působící	působící	k2eAgInPc1d1	působící
na	na	k7c4	na
krev	krev	k1gFnSc4	krev
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
např.	např.	kA	např.
nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
srážení	srážení	k1gNnSc1	srážení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otravu	otrava	k1gFnSc4	otrava
může	moct	k5eAaImIp3nS	moct
způsoby	způsoba	k1gFnSc2	způsoba
také	také	k9	také
alkohol	alkohol	k1gInSc1	alkohol
(	(	kIx(	(
<g/>
etylalkohol	etylalkohol	k1gInSc1	etylalkohol
<g/>
,	,	kIx,	,
metylalkohol	metylalkohol	k1gInSc1	metylalkohol
<g/>
)	)	kIx)	)
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
o	o	k7c4	o
otravu	otrava	k1gFnSc4	otrava
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
kvašením	kvašení	k1gNnSc7	kvašení
z	z	k7c2	z
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
etylalkoholu	etylalkohol	k1gInSc2	etylalkohol
se	se	k3xPyFc4	se
u	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
250	[number]	k4	250
–	–	k?	–
750	[number]	k4	750
g	g	kA	g
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
asi	asi	k9	asi
600	[number]	k4	600
–	–	k?	–
1800	[number]	k4	1800
ml	ml	kA	ml
40	[number]	k4	40
<g/>
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
vypije	vypít	k5eAaPmIp3nS	vypít
během	během	k7c2	během
kratší	krátký	k2eAgFnSc2d2	kratší
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hladině	hladina	k1gFnSc3	hladina
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
3,5	[number]	k4	3,5
–	–	k?	–
5	[number]	k4	5
promile	promile	k1gNnPc2	promile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
jedy	jed	k1gInPc1	jed
dráždí	dráždit	k5eAaImIp3nP	dráždit
(	(	kIx(	(
<g/>
atropin	atropin	k1gInSc1	atropin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
naopak	naopak	k6eAd1	naopak
uklidňují	uklidňovat	k5eAaImIp3nP	uklidňovat
(	(	kIx(	(
<g/>
morfium	morfium	k1gNnSc1	morfium
–	–	k?	–
mj.	mj.	kA	mj.
z	z	k7c2	z
máku	mák	k1gInSc2	mák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
některé	některý	k3yIgInPc4	některý
jedy	jed	k1gInPc4	jed
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
neškodné	škodný	k2eNgFnSc2d1	neškodná
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jiné	jiný	k2eAgInPc1d1	jiný
organizmy	organizmus	k1gInPc1	organizmus
mohou	moct	k5eAaImIp3nP	moct
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
)	)	kIx)	)
rozličně	rozličně	k6eAd1	rozličně
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
jed	jed	k1gInSc1	jed
kantharidin	kantharidin	k1gInSc1	kantharidin
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Puchýřníku	puchýřník	k1gInSc6	puchýřník
lékařském	lékařský	k2eAgInSc6d1	lékařský
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
hmyzožravé	hmyzožravý	k2eAgMnPc4d1	hmyzožravý
živočichy	živočich	k1gMnPc4	živočich
neškodný	škodný	k2eNgMnSc1d1	neškodný
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pak	pak	k9	pak
dokonce	dokonce	k9	dokonce
tento	tento	k3xDgInSc4	tento
jed	jed	k1gInSc4	jed
kumulují	kumulovat	k5eAaImIp3nP	kumulovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgNnPc4d1	ostatní
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
zdroje	zdroj	k1gInPc1	zdroj
jedu	jed	k1gInSc2	jed
==	==	k?	==
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
přírodních	přírodní	k2eAgInPc6d1	přírodní
jedech	jed	k1gInPc6	jed
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Toxin	toxin	k1gInSc1	toxin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
rostliny	rostlina	k1gFnPc1	rostlina
===	===	k?	===
</s>
</p>
<p>
<s>
Jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
téměř	téměř	k6eAd1	téměř
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
část	část	k1gFnSc1	část
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
kořen	kořen	k1gInSc1	kořen
<g/>
,	,	kIx,	,
plod	plod	k1gInSc1	plod
<g/>
,	,	kIx,	,
květ	květ	k1gInSc1	květ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
i	i	k9	i
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
(	(	kIx(	(
<g/>
škumpa	škumpa	k1gFnSc1	škumpa
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
,	,	kIx,	,
škumpa	škumpa	k1gFnSc1	škumpa
koželužná	koželužný	k2eAgFnSc1d1	koželužná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
rostliny	rostlina	k1gFnPc4	rostlina
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
bolehlav	bolehlav	k1gInSc1	bolehlav
<g/>
,	,	kIx,	,
narcis	narcis	k1gInSc1	narcis
(	(	kIx(	(
<g/>
cibule	cibule	k1gFnSc1	cibule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sněženka	sněženka	k1gFnSc1	sněženka
(	(	kIx(	(
<g/>
cibule	cibule	k1gFnSc1	cibule
<g/>
)	)	kIx)	)
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
houby	houba	k1gFnPc4	houba
===	===	k?	===
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
zelená	zelená	k1gFnSc1	zelená
(	(	kIx(	(
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
phalloides	phalloides	k1gMnSc1	phalloides
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejčastějším	častý	k2eAgInSc7d3	nejčastější
zdrojem	zdroj	k1gInSc7	zdroj
otravy	otrava	k1gFnSc2	otrava
z	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
hub	houba	k1gFnPc2	houba
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
plodnice	plodnice	k1gFnSc1	plodnice
postačí	postačit	k5eAaPmIp3nS	postačit
k	k	k7c3	k
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
otravě	otrava	k1gFnSc3	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
houby	houba	k1gFnPc1	houba
však	však	k9	však
vyvolají	vyvolat	k5eAaPmIp3nP	vyvolat
otravu	otrava	k1gFnSc4	otrava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
skončit	skončit	k5eAaPmF	skončit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
také	také	k9	také
muchomůrka	muchomůrka	k?	muchomůrka
červená	červené	k1gNnPc1	červené
(	(	kIx(	(
<g/>
Amanita	Amanit	k2eAgNnPc1d1	Amanit
muscaria	muscarium	k1gNnPc1	muscarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třepenitka	třepenitka	k1gFnSc1	třepenitka
svazčitá	svazčitý	k2eAgFnSc1d1	svazčitá
(	(	kIx(	(
<g/>
Hypholoma	Hypholoma	k1gFnSc1	Hypholoma
fasciculare	fascicular	k1gMnSc5	fascicular
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgFnPc4d1	další
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
houby	houba	k1gFnPc4	houba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
živočichové	živočich	k1gMnPc1	živočich
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
živočichy	živočich	k1gMnPc4	živočich
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
obecně	obecně	k6eAd1	obecně
známých	známý	k2eAgNnPc2d1	známé
i	i	k8xC	i
méně	málo	k6eAd2	málo
známých	známý	k2eAgFnPc2d1	známá
skupin	skupina	k1gFnPc2	skupina
schopných	schopný	k2eAgMnPc2d1	schopný
produkce	produkce	k1gFnSc2	produkce
jedů	jed	k1gInPc2	jed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
hadi	had	k1gMnPc1	had
vstřikují	vstřikovat	k5eAaImIp3nP	vstřikovat
jed	jed	k1gInSc4	jed
prodlouženými	prodloužená	k1gFnPc7	prodloužená
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c6	v
žláze	žláza	k1gFnSc6	žláza
nacházející	nacházející	k2eAgFnSc6d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
had	had	k1gMnSc1	had
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgInSc1d3	nejsilnější
jed	jed	k1gInSc1	jed
z	z	k7c2	z
hadů	had	k1gMnPc2	had
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
taipan	taipan	k1gInSc4	taipan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jedovaté	jedovatý	k2eAgMnPc4d1	jedovatý
hady	had	k1gMnPc4	had
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
kobra	kobra	k1gFnSc1	kobra
indická	indický	k2eAgFnSc1d1	indická
<g/>
,	,	kIx,	,
mamba	mamba	k1gFnSc1	mamba
černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
Dendroaspis	Dendroaspis	k1gFnSc1	Dendroaspis
polylepis	polylepis	k1gFnSc2	polylepis
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bojga	bojga	k1gFnSc1	bojga
hnědá	hnědý	k2eAgFnSc1d1	hnědá
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mořských	mořský	k2eAgMnPc2d1	mořský
hadů	had	k1gMnPc2	had
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
pět	pět	k4xCc4	pět
set	set	k1gInSc4	set
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
jedovatých	jedovatý	k2eAgMnPc2d1	jedovatý
ještěrů	ještěr	k1gMnPc2	ještěr
(	(	kIx(	(
<g/>
korovec	korovec	k1gMnSc1	korovec
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
a	a	k8xC	a
korovec	korovec	k1gMnSc1	korovec
mexický	mexický	k2eAgMnSc1d1	mexický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
nezaútočí	zaútočit	k5eNaPmIp3nS	zaútočit
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
vyprovokováni	vyprovokován	k2eAgMnPc1d1	vyprovokován
<g/>
.	.	kIx.	.
</s>
<s>
Varani	varan	k1gMnPc1	varan
komodští	komodský	k2eAgMnPc1d1	komodský
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
slinných	slinný	k2eAgFnPc6d1	slinná
žlázách	žláza	k1gFnPc6	žláza
jed	jed	k1gInSc4	jed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
srážlivost	srážlivost	k1gFnSc4	srážlivost
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
pavouci	pavouk	k1gMnPc1	pavouk
vstřikují	vstřikovat	k5eAaImIp3nP	vstřikovat
jed	jed	k1gInSc4	jed
do	do	k7c2	do
oběti	oběť	k1gFnSc2	oběť
svými	svůj	k3xOyFgFnPc7	svůj
chelicerami	chelicera	k1gFnPc7	chelicera
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pavouci	pavouk	k1gMnPc1	pavouk
dokážou	dokázat	k5eAaPmIp3nP	dokázat
prokousnost	prokousnost	k1gFnSc4	prokousnost
i	i	k8xC	i
lidskou	lidský	k2eAgFnSc4d1	lidská
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
asi	asi	k9	asi
kolem	kolem	k7c2	kolem
30	[number]	k4	30
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
člověku	člověk	k1gMnSc3	člověk
způsobit	způsobit	k5eAaPmF	způsobit
otravu	otrava	k1gFnSc4	otrava
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zápřednice	Zápřednice	k1gFnSc1	Zápřednice
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
jedovatých	jedovatý	k2eAgMnPc2d1	jedovatý
pavouků	pavouk	k1gMnPc2	pavouk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
kousnutí	kousnutí	k1gNnPc1	kousnutí
působí	působit	k5eAaImIp3nP	působit
velkou	velký	k2eAgFnSc4d1	velká
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc4	pocit
úzkosti	úzkost	k1gFnSc2	úzkost
a	a	k8xC	a
chvilkovým	chvilkový	k2eAgNnSc7d1	chvilkové
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
okolo	okolo	k7c2	okolo
místa	místo	k1gNnSc2	místo
pokousání	pokousání	k1gNnSc2	pokousání
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
kousnutí	kousnutí	k1gNnPc2	kousnutí
však	však	k9	však
většinou	většina	k1gFnSc7	většina
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
samy	sám	k3xTgFnPc1	sám
odezní	odeznět	k5eAaPmIp3nS	odeznět
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgInSc1d3	nejsilnější
jed	jed	k1gInSc1	jed
z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pavouk	pavouk	k1gMnSc1	pavouk
vodouch	vodouch	k1gMnSc1	vodouch
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
dochází	docházet	k5eAaImIp3nS	docházet
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejjedovatější	jedovatý	k2eAgMnPc4d3	nejjedovatější
pavouky	pavouk	k1gMnPc4	pavouk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
patří	patřit	k5eAaImIp3nS	patřit
sklípkan	sklípkan	k1gMnSc1	sklípkan
Atrax	Atrax	k1gInSc4	Atrax
robustus	robustus	k1gMnSc1	robustus
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
též	též	k9	též
snovačky	snovačka	k1gFnPc4	snovačka
rodu	rod	k1gInSc2	rod
Latrodectus	Latrodectus	k1gInSc1	Latrodectus
či	či	k8xC	či
pavouci	pavouk	k1gMnPc1	pavouk
rodu	rod	k1gInSc2	rod
Loxosceles	Loxosceles	k1gInSc1	Loxosceles
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
mají	mít	k5eAaImIp3nP	mít
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
ostrý	ostrý	k2eAgInSc4d1	ostrý
zahnutý	zahnutý	k2eAgInSc4d1	zahnutý
bodec	bodec	k1gInSc4	bodec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
štírů	štír	k1gMnPc2	štír
mají	mít	k5eAaImIp3nP	mít
jedovatější	jedovatý	k2eAgInSc1d2	jedovatější
jed	jed	k1gInSc1	jed
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
2000	[number]	k4	2000
druhů	druh	k1gInPc2	druh
štírů	štír	k1gMnPc2	štír
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
100	[number]	k4	100
silný	silný	k2eAgInSc4d1	silný
jed	jed	k1gInSc4	jed
<g/>
,	,	kIx,	,
25	[number]	k4	25
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
někdy	někdy	k6eAd1	někdy
smrt	smrt	k1gFnSc1	smrt
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
má	mít	k5eAaImIp3nS	mít
bodnutí	bodnutí	k1gNnSc1	bodnutí
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
účinky	účinek	k1gInPc4	účinek
komára	komár	k1gMnSc4	komár
nebo	nebo	k8xC	nebo
vosy	vosa	k1gFnPc4	vosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
jedovatou	jedovatý	k2eAgFnSc4d1	jedovatá
rybu	ryba	k1gFnSc4	ryba
odrance	odranec	k1gMnSc2	odranec
šlápneme	šlápnout	k5eAaPmIp1nP	šlápnout
v	v	k7c6	v
tropických	tropický	k2eAgNnPc6d1	tropické
mořích	moře	k1gNnPc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
také	také	k9	také
produkují	produkovat	k5eAaImIp3nP	produkovat
medúzy	medúza	k1gFnPc1	medúza
nebo	nebo	k8xC	nebo
někteří	některý	k3yIgMnPc1	některý
mořští	mořský	k2eAgMnPc1d1	mořský
ježci	ježek	k1gMnPc1	ježek
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
jed	jed	k1gInSc1	jed
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
žab	žába	k1gFnPc2	žába
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Dendrobatidae	Dendrobatida	k1gInSc2	Dendrobatida
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jedním	jeden	k4xCgNnSc7	jeden
stříknutím	stříknutí	k1gNnSc7	stříknutí
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
usmrtit	usmrtit	k5eAaPmF	usmrtit
až	až	k9	až
1500	[number]	k4	1500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
šípové	šípový	k2eAgFnPc1d1	šípová
žáby	žába	k1gFnPc1	žába
si	se	k3xPyFc3	se
vysloužily	vysloužit	k5eAaPmAgFnP	vysloužit
pralesničky	pralesnička	k1gFnPc1	pralesnička
batikové	batikový	k2eAgFnPc1d1	batiková
(	(	kIx(	(
<g/>
Dendrobates	Dendrobates	k1gMnSc1	Dendrobates
auratus	auratus	k1gMnSc1	auratus
<g/>
)	)	kIx)	)
díky	díky	k7c3	díky
látce	látka	k1gFnSc3	látka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
produkuje	produkovat	k5eAaImIp3nS	produkovat
jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
a	a	k8xC	a
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
si	se	k3xPyFc3	se
domorodí	domorodý	k2eAgMnPc1d1	domorodý
indiáni	indián	k1gMnPc1	indián
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
jed	jed	k1gInSc4	jed
na	na	k7c4	na
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
které	který	k3yRgMnPc4	který
jsou	být	k5eAaImIp3nP	být
chované	chovaný	k2eAgFnPc1d1	chovaná
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
však	však	k9	však
jed	jed	k1gInSc1	jed
nemají	mít	k5eNaImIp3nP	mít
prakticky	prakticky	k6eAd1	prakticky
žádný	žádný	k3yNgInSc4	žádný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přijímají	přijímat	k5eAaImIp3nP	přijímat
chybějí	chybět	k5eAaImIp3nP	chybět
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
zástupců	zástupce	k1gMnPc2	zástupce
schopných	schopný	k2eAgMnPc2d1	schopný
tvorby	tvorba	k1gFnSc2	tvorba
jedů	jed	k1gInPc2	jed
(	(	kIx(	(
<g/>
exotoxinů	exotoxin	k1gInPc2	exotoxin
nebo	nebo	k8xC	nebo
endotoxinů	endotoxin	k1gInPc2	endotoxin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
sinice	sinice	k1gFnPc4	sinice
(	(	kIx(	(
<g/>
produkující	produkující	k2eAgFnPc4d1	produkující
cyanotoxiny	cyanotoxina	k1gFnPc4	cyanotoxina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bakterie	bakterie	k1gFnSc1	bakterie
rodu	rod	k1gInSc2	rod
Clostridium	Clostridium	k1gNnSc1	Clostridium
<g/>
;	;	kIx,	;
právě	právě	k9	právě
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
produkují	produkovat	k5eAaImIp3nP	produkovat
extrémně	extrémně	k6eAd1	extrémně
silné	silný	k2eAgInPc1d1	silný
jedy	jed	k1gInPc1	jed
-	-	kIx~	-
například	například	k6eAd1	například
botulotoxiny	botulotoxin	k1gInPc1	botulotoxin
bakterií	bakterie	k1gFnSc7	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc1	botulinum
nebo	nebo	k8xC	nebo
tetanospasmin	tetanospasmin	k1gInSc1	tetanospasmin
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
bakteriemi	bakterie	k1gFnPc7	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
tetani	tetan	k1gMnPc1	tetan
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejprudší	prudký	k2eAgInPc4d3	nejprudší
jedy	jed	k1gInPc4	jed
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jedy	jed	k1gInPc7	jed
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
práva	právo	k1gNnSc2	právo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
jed	jed	k1gInSc1	jed
<g/>
"	"	kIx"	"
používá	používat	k5eAaImIp3nS	používat
trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vymezení	vymezení	k1gNnSc2	vymezení
skutkových	skutkový	k2eAgFnPc2d1	skutková
podstat	podstata	k1gFnPc2	podstata
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
Nedovolená	dovolený	k2eNgFnSc1d1	nedovolená
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
jiné	jiný	k2eAgNnSc1d1	jiné
nakládání	nakládání	k1gNnSc1	nakládání
s	s	k7c7	s
omamnými	omamný	k2eAgFnPc7d1	omamná
a	a	k8xC	a
psychotropními	psychotropní	k2eAgFnPc7d1	psychotropní
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
s	s	k7c7	s
jedy	jed	k1gInPc7	jed
(	(	kIx(	(
<g/>
§	§	k?	§
283	[number]	k4	283
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přechovávání	přechovávání	k1gNnSc1	přechovávání
omamné	omamný	k2eAgFnSc2d1	omamná
a	a	k8xC	a
psychotropní	psychotropní	k2eAgFnSc2d1	psychotropní
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
jedu	jed	k1gInSc2	jed
(	(	kIx(	(
<g/>
§	§	k?	§
284	[number]	k4	284
<g/>
)	)	kIx)	)
a	a	k8xC	a
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
držení	držení	k1gNnSc1	držení
předmětu	předmět	k1gInSc2	předmět
k	k	k7c3	k
nedovolené	dovolený	k2eNgFnSc3d1	nedovolená
výrobě	výroba	k1gFnSc3	výroba
omamné	omamný	k2eAgFnSc3d1	omamná
a	a	k8xC	a
psychotropní	psychotropní	k2eAgFnSc2d1	psychotropní
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
jedu	jed	k1gInSc2	jed
(	(	kIx(	(
<g/>
§	§	k?	§
286	[number]	k4	286
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
§	§	k?	§
289	[number]	k4	289
<g/>
,	,	kIx,	,
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
2	[number]	k4	2
zákoník	zákoník	k1gInSc1	zákoník
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
vládní	vládní	k2eAgNnSc4d1	vládní
nařízení	nařízení	k1gNnSc4	nařízení
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
zákoníku	zákoník	k1gInSc2	zákoník
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
jedy	jed	k1gInPc4	jed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
účinné	účinný	k2eAgNnSc1d1	účinné
nařízení	nařízení	k1gNnSc1	nařízení
vlády	vláda	k1gFnSc2	vláda
č.	č.	k?	č.
467	[number]	k4	467
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
za	za	k7c4	za
jedy	jed	k1gInPc4	jed
považuje	považovat	k5eAaImIp3nS	považovat
látky	látka	k1gFnPc4	látka
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
všechny	všechen	k3xTgFnPc4	všechen
směsi	směs	k1gFnPc4	směs
obsahující	obsahující	k2eAgFnPc4d1	obsahující
nejméně	málo	k6eAd3	málo
7	[number]	k4	7
%	%	kIx~	%
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
příloze	příloha	k1gFnSc6	příloha
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
například	například	k6eAd1	například
kyanidy	kyanid	k1gInPc1	kyanid
<g/>
,	,	kIx,	,
strychnin	strychnin	k1gInSc1	strychnin
<g/>
,	,	kIx,	,
brom	brom	k1gInSc1	brom
nebo	nebo	k8xC	nebo
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybit	k5eAaPmIp3nS	chybit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
naopak	naopak	k6eAd1	naopak
některé	některý	k3yIgFnSc2	některý
látky	látka	k1gFnSc2	látka
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
jedy	jed	k1gInPc1	jed
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
methanol	methanol	k1gInSc1	methanol
nebo	nebo	k8xC	nebo
mnohé	mnohý	k2eAgInPc1d1	mnohý
bakteriální	bakteriální	k2eAgInPc1d1	bakteriální
a	a	k8xC	a
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
jedy	jed	k1gInPc1	jed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
se	se	k3xPyFc4	se
jedy	jed	k1gInPc7	jed
zabývá	zabývat	k5eAaImIp3nS	zabývat
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
chemických	chemický	k2eAgFnPc6d1	chemická
látkách	látka	k1gFnPc6	látka
a	a	k8xC	a
chemických	chemický	k2eAgInPc6d1	chemický
přípravcích	přípravek	k1gInPc6	přípravek
(	(	kIx(	(
<g/>
356	[number]	k4	356
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
implementuje	implementovat	k5eAaImIp3nS	implementovat
i	i	k9	i
předpisy	předpis	k1gInPc4	předpis
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
jed	jed	k1gInSc1	jed
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
však	však	k9	však
nikde	nikde	k6eAd1	nikde
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obdobném	obdobný	k2eAgInSc6d1	obdobný
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
klasifikací	klasifikace	k1gFnSc7	klasifikace
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
látek	látka	k1gFnPc2	látka
jako	jako	k8xC	jako
látky	látka	k1gFnPc1	látka
vysoce	vysoce	k6eAd1	vysoce
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
zdraví	zdravit	k5eAaImIp3nP	zdravit
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
<g/>
,	,	kIx,	,
karcinogenní	karcinogenní	k2eAgFnPc4d1	karcinogenní
<g/>
,	,	kIx,	,
mutagenní	mutagenní	k2eAgFnPc4d1	mutagenní
a	a	k8xC	a
toxické	toxický	k2eAgFnPc4d1	toxická
pro	pro	k7c4	pro
reprodukci	reprodukce	k1gFnSc4	reprodukce
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
dalších	další	k2eAgFnPc2d1	další
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
s	s	k7c7	s
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
látkami	látka	k1gFnPc7	látka
spjaty	spjat	k2eAgFnPc1d1	spjata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
nakládání	nakládání	k1gNnSc4	nakládání
s	s	k7c7	s
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
s	s	k7c7	s
přípravky	přípravek	k1gInPc7	přípravek
obsahující	obsahující	k2eAgFnPc1d1	obsahující
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
označené	označený	k2eAgFnSc2d1	označená
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vysoce	vysoce	k6eAd1	vysoce
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
oxid	oxid	k1gInSc1	oxid
kademnatý	kademnatý	k2eAgInSc1d1	kademnatý
<g/>
,	,	kIx,	,
dichroman	dichroman	k1gMnSc1	dichroman
draselný	draselný	k2eAgMnSc1d1	draselný
nebo	nebo	k8xC	nebo
chlorid	chlorid	k1gInSc1	chlorid
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
<g/>
)	)	kIx)	)
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
poskytnuty	poskytnout	k5eAaPmNgInP	poskytnout
nikomu	nikdo	k3yNnSc3	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
nemá	mít	k5eNaImIp3nS	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nakládat	nakládat	k5eAaImF	nakládat
(	(	kIx(	(
<g/>
podmínky	podmínka	k1gFnPc4	podmínka
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
označené	označený	k2eAgFnSc2d1	označená
jaké	jaký	k3yRgFnPc4	jaký
"	"	kIx"	"
<g/>
toxické	toxický	k2eAgFnPc4d1	toxická
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jodid	jodid	k1gInSc4	jodid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
glutaraldehyd	glutaraldehyd	k1gInSc1	glutaraldehyd
nebo	nebo	k8xC	nebo
deltamethrin	deltamethrin	k1gInSc1	deltamethrin
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
žíravé	žíravý	k2eAgNnSc1d1	žíravé
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hydroxid	hydroxid	k1gInSc4	hydroxid
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
<g/>
,	,	kIx,	,
benzoylchlorid	benzoylchlorid	k1gInSc1	benzoylchlorid
a	a	k8xC	a
mnohé	mnohé	k1gNnSc1	mnohé
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
poskytnuty	poskytnout	k5eAaPmNgInP	poskytnout
osobám	osoba	k1gFnPc3	osoba
mladším	mladý	k2eAgFnPc3d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
osobám	osoba	k1gFnPc3	osoba
zbavených	zbavený	k2eAgFnPc2d1	zbavená
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
i	i	k9	i
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
)	)	kIx)	)
způsobilosti	způsobilost	k1gFnSc2	způsobilost
k	k	k7c3	k
právním	právní	k2eAgInPc3d1	právní
úkonům	úkon	k1gInPc3	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
nakládání	nakládání	k1gNnSc4	nakládání
s	s	k7c7	s
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
označování	označování	k1gNnSc1	označování
obalů	obal	k1gInPc2	obal
nebo	nebo	k8xC	nebo
vytváření	vytváření	k1gNnSc4	vytváření
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
pracoviště	pracoviště	k1gNnSc4	pracoviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označování	označování	k1gNnSc1	označování
jedů	jed	k1gInPc2	jed
==	==	k?	==
</s>
</p>
<p>
<s>
Jedy	jed	k1gInPc1	jed
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
označují	označovat	k5eAaImIp3nP	označovat
symbolem	symbol	k1gInSc7	symbol
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
symbol	symbol	k1gInSc1	symbol
pomalu	pomalu	k6eAd1	pomalu
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
symbolem	symbol	k1gInSc7	symbol
zvaným	zvaný	k2eAgInSc7d1	zvaný
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnPc6	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Yuk	Yuk	k?	Yuk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
symbol	symbol	k1gInSc1	symbol
lebky	lebka	k1gFnSc2	lebka
zejména	zejména	k9	zejména
děti	dítě	k1gFnPc1	dítě
intuitivně	intuitivně	k6eAd1	intuitivně
spojovaly	spojovat	k5eAaImAgFnP	spojovat
s	s	k7c7	s
piráty	pirát	k1gMnPc7	pirát
a	a	k8xC	a
chybně	chybně	k6eAd1	chybně
interpretovaly	interpretovat	k5eAaBmAgFnP	interpretovat
výstražný	výstražný	k2eAgInSc4d1	výstražný
význam	význam	k1gInSc4	význam
tohoto	tento	k3xDgInSc2	tento
symbolu	symbol	k1gInSc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Yuk	Yuk	k?	Yuk
je	být	k5eAaImIp3nS	být
autorsky	autorsky	k6eAd1	autorsky
chráněn	chránit	k5eAaImNgInS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
lebky	lebka	k1gFnSc2	lebka
se	s	k7c7	s
zkříženými	zkřížený	k2eAgInPc7d1	zkřížený
hnáty	hnát	k1gInPc7	hnát
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jolly	Joll	k1gInPc1	Joll
Roger	Rogra	k1gFnPc2	Rogra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
užitelný	užitelný	k2eAgInSc1d1	užitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odstraňování	odstraňování	k1gNnSc1	odstraňování
jedů	jed	k1gInPc2	jed
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Poison	Poisona	k1gFnPc2	Poisona
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
otrava	otrava	k1gFnSc1	otrava
</s>
</p>
<p>
<s>
protijed	protijed	k1gInSc1	protijed
</s>
</p>
<p>
<s>
toxicita	toxicita	k1gFnSc1	toxicita
</s>
</p>
<p>
<s>
smrtná	smrtný	k2eAgFnSc1d1	Smrtná
dávka	dávka	k1gFnSc1	dávka
</s>
</p>
<p>
<s>
chemická	chemický	k2eAgFnSc1d1	chemická
zbraň	zbraň	k1gFnSc1	zbraň
</s>
</p>
<p>
<s>
toxin	toxin	k1gInSc1	toxin
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
látek	látka	k1gFnPc2	látka
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c4	za
jedy	jed	k1gInPc4	jed
(	(	kIx(	(
<g/>
české	český	k2eAgNnSc4d1	české
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jed	jed	k1gInSc1	jed
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jed	jed	k1gInSc1	jed
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Jed	jed	k1gInSc1	jed
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Jedy	jed	k1gInPc7	jed
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
<g/>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
chemických	chemický	k2eAgFnPc6d1	chemická
látkách	látka	k1gFnPc6	látka
a	a	k8xC	a
chemických	chemický	k2eAgInPc6d1	chemický
přípravcích	přípravek	k1gInPc6	přípravek
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
zákonů	zákon	k1gInPc2	zákon
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
GHS	GHS	kA	GHS
-	-	kIx~	-
nová	nový	k2eAgFnSc1d1	nová
chemická	chemický	k2eAgFnSc1d1	chemická
legislativa	legislativa	k1gFnSc1	legislativa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
REACH	REACH	kA	REACH
-	-	kIx~	-
nová	nový	k2eAgFnSc1d1	nová
chemická	chemický	k2eAgFnSc1d1	chemická
legislativa	legislativa	k1gFnSc1	legislativa
</s>
</p>
