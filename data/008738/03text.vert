<p>
<s>
Riversleigh	Riversleigh	k1gInSc1	Riversleigh
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
archeologické	archeologický	k2eAgFnSc2d1	archeologická
lokality	lokalita	k1gFnSc2	lokalita
na	na	k7c6	na
severu	sever	k1gInSc6	sever
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
mnoho	mnoho	k4c1	mnoho
fosilií	fosilie	k1gFnPc2	fosilie
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
plazů	plaz	k1gMnPc2	plaz
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
období	období	k1gNnSc6	období
oligocénu	oligocén	k1gInSc2	oligocén
a	a	k8xC	a
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
100	[number]	k4	100
km2	km2	k4	km2
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
Naracoorte	Naracoort	k1gInSc5	Naracoort
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
fosilie	fosilie	k1gFnSc1	fosilie
jsou	být	k5eAaImIp3nP	být
unikátní	unikátní	k2eAgMnPc1d1	unikátní
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
měkkém	měkký	k2eAgInSc6d1	měkký
vápenci	vápenec	k1gInSc6	vápenec
historických	historický	k2eAgNnPc2d1	historické
sladkovodních	sladkovodní	k2eAgNnPc2d1	sladkovodní
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neprošel	projít	k5eNaPmAgInS	projít
výraznějším	výrazný	k2eAgInSc7d2	výraznější
procesem	proces	k1gInSc7	proces
konsolidace	konsolidace	k1gFnSc1	konsolidace
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
zdejší	zdejší	k2eAgFnPc1d1	zdejší
fosilie	fosilie	k1gFnPc1	fosilie
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
v	v	k7c6	v
trojrozměrné	trojrozměrný	k2eAgFnSc6d1	trojrozměrná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Zkameněliny	zkamenělina	k1gFnPc1	zkamenělina
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
a	a	k8xC	a
prostorovém	prostorový	k2eAgNnSc6d1	prostorové
rozmístění	rozmístění	k1gNnSc6	rozmístění
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
Gondwaně	Gondwaň	k1gFnPc4	Gondwaň
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
severní	severní	k2eAgFnSc2d1	severní
Austrálie	Austrálie	k1gFnSc2	Austrálie
postupně	postupně	k6eAd1	postupně
měnila	měnit	k5eAaImAgFnS	měnit
z	z	k7c2	z
deštného	deštný	k2eAgInSc2d1	deštný
lesa	les	k1gInSc2	les
na	na	k7c4	na
polopouštní	polopouštní	k2eAgFnSc4d1	polopouštní
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
nalezeny	naleznout	k5eAaPmNgInP	naleznout
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
předchůdců	předchůdce	k1gMnPc2	předchůdce
mnoha	mnoho	k4c2	mnoho
dnešních	dnešní	k2eAgMnPc2d1	dnešní
vačnatců	vačnatec	k1gMnPc2	vačnatec
(	(	kIx(	(
<g/>
Metatheria	Metatherium	k1gNnPc4	Metatherium
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
klokanů	klokan	k1gMnPc2	klokan
(	(	kIx(	(
<g/>
Macropodidae	Macropodidae	k1gNnSc1	Macropodidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vakovlků	vakovlek	k1gInPc2	vakovlek
(	(	kIx(	(
<g/>
Thylacinidae	Thylacinidae	k1gInSc1	Thylacinidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vombatů	vombat	k1gMnPc2	vombat
(	(	kIx(	(
<g/>
Vombatidae	Vombatidae	k1gNnSc1	Vombatidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koal	koala	k1gFnPc2	koala
(	(	kIx(	(
<g/>
Phascolarctidae	Phascolarctidae	k1gFnSc1	Phascolarctidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
possumů	possum	k1gInPc2	possum
(	(	kIx(	(
<g/>
Pseudocheiridae	Pseudocheiridae	k1gInSc1	Pseudocheiridae
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
netopýrů	netopýr	k1gMnPc2	netopýr
(	(	kIx(	(
<g/>
Vespertilioniformes	Vespertilioniformes	k1gMnSc1	Vespertilioniformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
živočichů	živočich	k1gMnPc2	živočich
australské	australský	k2eAgFnSc2d1	australská
megafauny	megafauna	k1gFnSc2	megafauna
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obdurodona	obdurodona	k1gFnSc1	obdurodona
a	a	k8xC	a
nimbadona	nimbadona	k1gFnSc1	nimbadona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgFnSc1d1	významná
světová	světový	k2eAgFnSc1d1	světová
hodnota	hodnota	k1gFnSc1	hodnota
podle	podle	k7c2	podle
UNESCO	UNESCO	kA	UNESCO
==	==	k?	==
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejvíce	nejvíce	k6eAd1	nejvíce
biologicky	biologicky	k6eAd1	biologicky
odlišný	odlišný	k2eAgInSc1d1	odlišný
kontinent	kontinent	k1gInSc1	kontinent
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
téměř	téměř	k6eAd1	téměř
úplné	úplný	k2eAgFnSc2d1	úplná
izolace	izolace	k1gFnSc2	izolace
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
od	od	k7c2	od
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
řádů	řád	k1gInPc2	řád
jedinečně	jedinečně	k6eAd1	jedinečně
odlišných	odlišný	k2eAgMnPc2d1	odlišný
savců	savec	k1gMnPc2	savec
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
někde	někde	k6eAd1	někde
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
fosilních	fosilní	k2eAgNnPc2d1	fosilní
nalezišť	naleziště	k1gNnPc2	naleziště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Riversleigh	Riversleigh	k1gMnSc1	Riversleigh
a	a	k8xC	a
Naracoorte	Naracoort	k1gInSc5	Naracoort
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
významné	významný	k2eAgInPc1d1	významný
fosilní	fosilní	k2eAgInPc1d1	fosilní
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
této	tento	k3xDgFnSc2	tento
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
fauny	fauna	k1gFnSc2	fauna
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sériovost	sériovost	k1gFnSc1	sériovost
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
příklady	příklad	k1gInPc1	příklad
skupin	skupina	k1gFnPc2	skupina
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgInPc2d1	poslední
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starší	starý	k2eAgFnPc1d2	starší
fosilie	fosilie	k1gFnPc1	fosilie
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
u	u	k7c2	u
Riversleigh	Riversleigha	k1gFnPc2	Riversleigha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
sbírkou	sbírka	k1gFnSc7	sbírka
od	od	k7c2	od
oligocénu	oligocén	k1gInSc2	oligocén
k	k	k7c3	k
miocénu	miocén	k1gInSc3	miocén
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
před	před	k7c7	před
10-30	[number]	k4	10-30
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInSc1d2	novější
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
Naracoorte	Naracoort	k1gInSc5	Naracoort
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgNnPc2d3	nejbohatší
ložisek	ložisko	k1gNnPc2	ložisko
fosílií	fosílie	k1gFnPc2	fosílie
obratlovců	obratlovec	k1gMnPc2	obratlovec
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
pleistocénu	pleistocén	k1gInSc2	pleistocén
až	až	k9	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
(	(	kIx(	(
<g/>
před	před	k7c7	před
530	[number]	k4	530
000	[number]	k4	000
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
celosvětově	celosvětově	k6eAd1	celosvětově
významné	významný	k2eAgNnSc1d1	významné
kosterní	kosterní	k2eAgNnSc1d1	kosterní
naleziště	naleziště	k1gNnSc1	naleziště
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
obraz	obraz	k1gInSc4	obraz
klíčových	klíčový	k2eAgFnPc2d1	klíčová
etap	etapa	k1gFnPc2	etapa
vývoje	vývoj	k1gInSc2	vývoj
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
ilustrující	ilustrující	k2eAgFnSc1d1	ilustrující
jejich	jejich	k3xOp3gFnSc4	jejich
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
na	na	k7c4	na
lidské	lidský	k2eAgInPc4d1	lidský
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Riversleigh	Riversleigha	k1gFnPc2	Riversleigha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
AUSTRALIAN	AUSTRALIAN	kA	AUSTRALIAN
FOSSIL	FOSSIL	kA	FOSSIL
MAMMALS	MAMMALS	kA	MAMMALS
SITES	SITES	kA	SITES
<g/>
,	,	kIx,	,
RIVERSLEIGH	RIVERSLEIGH	kA	RIVERSLEIGH
/	/	kIx~	/
NARACOORTE	NARACOORTE	kA	NARACOORTE
<g/>
,	,	kIx,	,
QUEENSLAND	QUEENSLAND	kA	QUEENSLAND
&	&	k?	&
SOUTH	SOUTH	kA	SOUTH
AUSTRALIA	AUSTRALIA	kA	AUSTRALIA
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
World	World	k1gMnSc1	World
Heritage	Heritag	k1gFnSc2	Heritag
Places	Places	k1gMnSc1	Places
-	-	kIx~	-
Australian	Australian	k1gMnSc1	Australian
Fossil	Fossil	k1gMnSc1	Fossil
Mammal	Mammal	k1gMnSc1	Mammal
Sites	Sites	k1gMnSc1	Sites
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Australian	Australian	k1gMnSc1	Australian
Government	Government	k1gMnSc1	Government
-	-	kIx~	-
Department	department	k1gInSc1	department
of	of	k?	of
the	the	k?	the
Environment	Environment	k1gInSc4	Environment
and	and	k?	and
Energy	Energ	k1gInPc4	Energ
[	[	kIx(	[
<g/>
cit	cit	k1gInSc4	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
