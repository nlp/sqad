<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
hladiny	hladina	k1gFnSc2	hladina
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
příliv	příliv	k1gInSc1	příliv
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
jako	jako	k8xS	jako
odliv	odliv	k1gInSc1	odliv
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
dmutí	dmutí	k1gNnSc6	dmutí
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
