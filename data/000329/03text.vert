<s>
Slapové	slapový	k2eAgInPc1d1	slapový
jevy	jev	k1gInPc1	jev
představují	představovat	k5eAaImIp3nP	představovat
zvyšování	zvyšování	k1gNnSc4	zvyšování
a	a	k8xC	a
snižování	snižování	k1gNnSc4	snižování
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
působení	působení	k1gNnSc2	působení
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
hladiny	hladina	k1gFnSc2	hladina
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
příliv	příliv	k1gInSc1	příliv
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
jako	jako	k8xS	jako
odliv	odliv	k1gInSc1	odliv
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
dmutí	dmutí	k1gNnSc6	dmutí
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgInPc1d1	slapový
jevy	jev	k1gInPc1	jev
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
deformace	deformace	k1gFnSc2	deformace
povrchu	povrch	k1gInSc2	povrch
oceánu	oceán	k1gInSc2	oceán
vlivem	vlivem	k7c2	vlivem
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
na	na	k7c4	na
vodní	vodní	k2eAgFnSc4d1	vodní
masu	masa	k1gFnSc4	masa
působí	působit	k5eAaImIp3nP	působit
nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
především	především	k9	především
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
mohou	moct	k5eAaImIp3nP	moct
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
dokonce	dokonce	k9	dokonce
způsobit	způsobit	k5eAaPmF	způsobit
rozpad	rozpad	k1gInSc4	rozpad
obíhajícího	obíhající	k2eAgNnSc2d1	obíhající
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
oběžná	oběžný	k2eAgFnSc1d1	oběžná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
tzv.	tzv.	kA	tzv.
Rocheova	Rocheův	k2eAgFnSc1d1	Rocheova
mez	mez	k1gFnSc1	mez
<g/>
.	.	kIx.	.
</s>
<s>
Pravou	pravý	k2eAgFnSc7d1	pravá
silou	síla	k1gFnSc7	síla
působící	působící	k2eAgNnSc1d1	působící
dmutí	dmutí	k1gNnSc1	dmutí
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
nehomogenita	nehomogenita	k1gFnSc1	nehomogenita
jejího	její	k3xOp3gNnSc2	její
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
velikost	velikost	k1gFnSc1	velikost
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc3	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
silněji	silně	k6eAd2	silně
tělesa	těleso	k1gNnPc4	těleso
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
slaběji	slabo	k6eAd2	slabo
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
uvážit	uvážit	k5eAaPmF	uvážit
neinerciálnost	neinerciálnost	k1gFnSc4	neinerciálnost
soustavy	soustava	k1gFnSc2	soustava
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
tedy	tedy	k9	tedy
působení	působení	k1gNnSc3	působení
zdánlivých	zdánlivý	k2eAgFnPc2d1	zdánlivá
setrvačných	setrvačný	k2eAgFnPc2d1	setrvačná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
přílivového	přílivový	k2eAgNnSc2d1	přílivové
dmutí	dmutí	k1gNnSc2	dmutí
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nahlížet	nahlížet	k5eAaImF	nahlížet
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
pohyb	pohyb	k1gInSc4	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
ne	ne	k9	ne
jako	jako	k8xC	jako
pouhý	pouhý	k2eAgInSc4d1	pouhý
oběh	oběh	k1gInSc4	oběh
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
nehybné	hybný	k2eNgFnSc2d1	nehybná
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
rotaci	rotace	k1gFnSc6	rotace
obou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tedy	tedy	k9	tedy
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
setrvačná	setrvačný	k2eAgFnSc1d1	setrvačná
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naopak	naopak	k6eAd1	naopak
roste	růst	k5eAaImIp3nS	růst
přímo	přímo	k6eAd1	přímo
úměrně	úměrně	k6eAd1	úměrně
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
společné	společný	k2eAgNnSc1d1	společné
těžiště	těžiště	k1gNnSc1	těžiště
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odstředivý	odstředivý	k2eAgInSc4d1	odstředivý
příspěvek	příspěvek	k1gInSc4	příspěvek
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
přispívá	přispívat	k5eAaImIp3nS	přispívat
i	i	k9	i
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
smyslu	smysl	k1gInSc6	smysl
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
převládá	převládat	k5eAaImIp3nS	převládat
gravitační	gravitační	k2eAgFnSc1d1	gravitační
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
a	a	k8xC	a
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
obě	dva	k4xCgFnPc1	dva
síly	síla	k1gFnPc1	síla
působí	působit	k5eAaImIp3nP	působit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
odečítají	odečítat	k5eAaImIp3nP	odečítat
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
sil	síla	k1gFnPc2	síla
proto	proto	k8xC	proto
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
oceánech	oceán	k1gInPc6	oceán
dvě	dva	k4xCgFnPc4	dva
přílivové	přílivový	k2eAgFnPc4d1	přílivová
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
Měsíci	měsíc	k1gInSc3	měsíc
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
protilehlé	protilehlý	k2eAgFnPc1d1	protilehlá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
dmutí	dmutí	k1gNnSc3	dmutí
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
tuhosti	tuhost	k1gFnSc3	tuhost
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
tak	tak	k6eAd1	tak
znatelnému	znatelný	k2eAgMnSc3d1	znatelný
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
přílivové	přílivový	k2eAgFnPc1d1	přílivová
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přílivu	příliv	k1gInSc3	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc3	odliv
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
s	s	k7c7	s
dvojnásobkem	dvojnásobek	k1gInSc7	dvojnásobek
frekvence	frekvence	k1gFnSc1	frekvence
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
průchodu	průchod	k1gInSc6	průchod
Měsíce	měsíc	k1gInSc2	měsíc
nad	nad	k7c7	nad
příslušným	příslušný	k2eAgInSc7d1	příslušný
poledníkem	poledník	k1gInSc7	poledník
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
každých	každý	k3xTgFnPc2	každý
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
25	[number]	k4	25
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
14	[number]	k4	14
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
půldenním	půldenní	k2eAgNnSc6d1	půldenní
dmutí	dmutí	k1gNnSc6	dmutí
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
mezi	mezi	k7c7	mezi
přílivem	příliv	k1gInSc7	příliv
a	a	k8xC	a
odlivem	odliv	k1gInSc7	odliv
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
12	[number]	k4	12
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
37	[number]	k4	37
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
deklinace	deklinace	k1gFnSc2	deklinace
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
každé	každý	k3xTgNnSc4	každý
druhé	druhý	k4xOgNnSc4	druhý
dmutí	dmutí	k1gNnSc4	dmutí
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
jednodenním	jednodenní	k2eAgNnSc6d1	jednodenní
dmutí	dmutí	k1gNnSc6	dmutí
<g/>
.	.	kIx.	.
</s>
<s>
Půldenní	půldenní	k2eAgNnSc1d1	půldenní
dmutí	dmutí	k1gNnSc1	dmutí
probíhá	probíhat	k5eAaImIp3nS	probíhat
především	především	k9	především
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
a	a	k8xC	a
Severním	severní	k2eAgInSc6d1	severní
ledovém	ledový	k2eAgInSc6d1	ledový
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
jednodenní	jednodenní	k2eAgInPc4d1	jednodenní
zejména	zejména	k9	zejména
v	v	k7c6	v
Jávském	jávský	k2eAgNnSc6d1	jávské
a	a	k8xC	a
Ochotském	ochotský	k2eAgNnSc6d1	Ochotské
moři	moře	k1gNnSc6	moře
<g/>
;	;	kIx,	;
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
dmutím	dmutí	k1gNnSc7	dmutí
lze	lze	k6eAd1	lze
přílivové	přílivový	k2eAgFnSc2d1	přílivová
vody	voda	k1gFnSc2	voda
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
Slunce	slunce	k1gNnSc2	slunce
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
měsíčním	měsíční	k2eAgFnPc3d1	měsíční
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
slabší	slabý	k2eAgMnSc1d2	slabší
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
slapové	slapový	k2eAgFnPc4d1	slapová
síly	síla	k1gFnPc4	síla
obou	dva	k4xCgInPc2	dva
těles	těleso	k1gNnPc2	těleso
se	se	k3xPyFc4	se
sečtou	sečíst	k5eAaPmIp3nP	sečíst
a	a	k8xC	a
dmutí	dmutí	k1gNnSc1	dmutí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgNnSc1d1	výrazné
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
skočné	skočný	k2eAgNnSc1d1	skočné
dmutí	dmutí	k1gNnSc1	dmutí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
naopak	naopak	k6eAd1	naopak
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Měsíc	měsíc	k1gInSc4	měsíc
svírají	svírat	k5eAaImIp3nP	svírat
pravý	pravý	k2eAgInSc4d1	pravý
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
vyruší	vyrušit	k5eAaPmIp3nS	vyrušit
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
hluché	hluchý	k2eAgNnSc4d1	hluché
dmutí	dmutí	k1gNnSc4	dmutí
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
skočnému	skočný	k2eAgNnSc3d1	skočné
dmutí	dmutí	k1gNnSc3	dmutí
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
novu	nov	k1gInSc2	nov
nebo	nebo	k8xC	nebo
úplňku	úplněk	k1gInSc2	úplněk
<g/>
,	,	kIx,	,
k	k	k7c3	k
hluchému	hluchý	k2eAgNnSc3d1	hluché
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
fáze	fáze	k1gFnSc2	fáze
dorůstání	dorůstání	k1gNnSc2	dorůstání
nebo	nebo	k8xC	nebo
ubývání	ubývání	k1gNnSc2	ubývání
<g/>
;	;	kIx,	;
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
polohy	poloha	k1gFnSc2	poloha
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
polohy	poloha	k1gFnSc2	poloha
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výšku	výška	k1gFnSc4	výška
dmutí	dmutí	k1gNnSc2	dmutí
také	také	k9	také
tvar	tvar	k1gInSc4	tvar
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
úhel	úhel	k1gInSc1	úhel
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
volném	volný	k2eAgNnSc6d1	volné
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
hladiny	hladina	k1gFnSc2	hladina
mění	měnit	k5eAaImIp3nS	měnit
asi	asi	k9	asi
o	o	k7c6	o
0,8	[number]	k4	0,8
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hranice	hranice	k1gFnSc1	hranice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
příliv	příliv	k1gInSc1	příliv
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Fundy	fund	k1gInPc4	fund
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hladina	hladina	k1gFnSc1	hladina
stoupá	stoupat	k5eAaImIp3nS	stoupat
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
rozpětí	rozpětí	k1gNnSc4	rozpětí
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
poblíž	poblíž	k7c2	poblíž
pobřeží	pobřeží	k1gNnSc2	pobřeží
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Mont-Saint-Michel	Mont-Saint-Michela	k1gFnPc2	Mont-Saint-Michela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
přílivu	příliv	k1gInSc2	příliv
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Čáry	čára	k1gFnPc4	čára
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnPc4d1	spojující
místa	místo	k1gNnPc4	místo
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
dobou	doba	k1gFnSc7	doba
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
přílivu	příliv	k1gInSc2	příliv
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
izohachie	izohachie	k1gFnPc1	izohachie
<g/>
.	.	kIx.	.
</s>
<s>
Vzdutí	vzdutí	k1gNnSc1	vzdutí
hladiny	hladina	k1gFnSc2	hladina
u	u	k7c2	u
ústí	ústí	k1gNnPc2	ústí
řek	řeka	k1gFnPc2	řeka
s	s	k7c7	s
povlovným	povlovný	k2eAgInSc7d1	povlovný
dnem	den	k1gInSc7	den
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příliv	příliv	k1gInSc1	příliv
šíří	šířit	k5eAaImIp3nS	šířit
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
vodní	vodní	k2eAgFnSc1d1	vodní
vlna	vlna	k1gFnSc1	vlna
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
přílivový	přílivový	k2eAgInSc4d1	přílivový
příboj	příboj	k1gInSc4	příboj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
Amazonky	Amazonka	k1gFnSc2	Amazonka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přílivový	přílivový	k2eAgInSc1d1	přílivový
příboj	příboj	k1gInSc1	příboj
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
ještě	ještě	k9	ještě
850	[number]	k4	850
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
dostává	dostávat	k5eAaImIp3nS	dostávat
přílivový	přílivový	k2eAgInSc4d1	přílivový
příboj	příboj	k1gInSc4	příboj
dokonce	dokonce	k9	dokonce
své	svůj	k3xOyFgNnSc4	svůj
místní	místní	k2eAgNnSc4d1	místní
jméno	jméno	k1gNnSc4	jméno
-	-	kIx~	-
například	například	k6eAd1	například
právě	právě	k9	právě
na	na	k7c6	na
Amazonce	Amazonka	k1gFnSc6	Amazonka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
indiánským	indiánský	k2eAgNnSc7d1	indiánské
slovem	slovo	k1gNnSc7	slovo
pororoca	pororocum	k1gNnSc2	pororocum
(	(	kIx(	(
<g/>
hřmící	hřmící	k2eAgFnSc2d1	hřmící
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
přílivový	přílivový	k2eAgInSc1d1	přílivový
příboj	příboj	k1gInSc1	příboj
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
150	[number]	k4	150
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
jezer	jezero	k1gNnPc2	jezero
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
slapové	slapový	k2eAgInPc1d1	slapový
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
hladiny	hladina	k1gFnSc2	hladina
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
příliv	příliv	k1gInSc1	příliv
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
Michiganském	michiganský	k2eAgNnSc6d1	Michiganské
jezeře	jezero	k1gNnSc6	jezero
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
výše	výše	k1gFnSc1	výše
7	[number]	k4	7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
při	při	k7c6	při
dmutí	dmutí	k1gNnSc6	dmutí
šíří	šíř	k1gFnPc2	šíř
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
specifickým	specifický	k2eAgInSc7d1	specifický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
slapové	slapový	k2eAgInPc4d1	slapový
proudy	proud	k1gInPc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Silnější	silný	k2eAgFnPc1d2	silnější
bývají	bývat	k5eAaImIp3nP	bývat
při	při	k7c6	při
odlivu	odliv	k1gInSc6	odliv
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
22	[number]	k4	22
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
a	a	k8xC	a
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
