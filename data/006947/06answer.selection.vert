<s>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
pseudonymem	pseudonym	k1gInSc7	pseudonym
Havel	Havla	k1gFnPc2	Havla
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1821	[number]	k4	1821
Borová	borový	k2eAgFnSc1d1	Borová
u	u	k7c2	u
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
