<s>
Prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
Květoslav	Květoslava	k1gFnPc2	Květoslava
Šipr	Šipra	k1gFnPc2	Šipra
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
gerontologii	gerontologie	k1gFnSc4	gerontologie
a	a	k8xC	a
bioetiku	bioetika	k1gFnSc4	bioetika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
Kolegia	kolegium	k1gNnSc2	kolegium
katolických	katolický	k2eAgMnPc2d1	katolický
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
tajemníkem	tajemník	k1gMnSc7	tajemník
Rady	Rada	k1gMnSc2	Rada
České	český	k2eAgFnSc2d1	Česká
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
pro	pro	k7c4	pro
bioetiku	bioetika	k1gFnSc4	bioetika
a	a	k8xC	a
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
geriatrické	geriatrický	k2eAgFnSc2d1	geriatrická
a	a	k8xC	a
gerontologické	gerontologický	k2eAgFnSc2d1	gerontologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
