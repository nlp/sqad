<s>
Jaroslav	Jaroslav	k1gMnSc1
Šulc	Šulc	k1gMnSc1
(	(	kIx(
<g/>
spisovatel	spisovatel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Šulc	Šulc	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1903	#num#	k4
Mělník	Mělník	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Poruba	Poruba	k1gFnSc1
Pseudonym	pseudonym	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Klen	klen	k2eAgMnSc1d1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Šulc	Šulc	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1903	#num#	k4
Mělník	Mělník	k1gInSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1977	#num#	k4
Ostrava-Poruba	Ostrava-Poruba	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
Raisův	Raisův	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
učitelský	učitelský	k2eAgInSc4d1
ústav	ústav	k1gInSc4
v	v	k7c6
Jičíně	Jičín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
odborný	odborný	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Beskyd	Beskydy	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
Opavsku	Opavsko	k1gNnSc6
(	(	kIx(
<g/>
Jaktař	Jaktař	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Ostravsku	Ostravsko	k1gNnSc6
(	(	kIx(
<g/>
Přívoz	přívoz	k1gInSc1
<g/>
,	,	kIx,
Poruba	Poruba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
ostravským	ostravský	k2eAgInSc7d1
rozhlasem	rozhlas	k1gInSc7
<g/>
,	,	kIx,
během	během	k7c2
okupace	okupace	k1gFnSc2
působil	působit	k5eAaImAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
prózy	próza	k1gFnSc2
i	i	k8xC
literatury	literatura	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
má	mít	k5eAaImIp3nS
vztah	vztah	k1gInSc4
ke	k	k7c3
kraji	kraj	k1gInSc3
a	a	k8xC
regionálním	regionální	k2eAgMnPc3d1
umělcům	umělec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užíval	užívat	k5eAaImAgMnS
i	i	k9
pseudonymy	pseudonym	k1gInPc7
Jindřich	Jindřich	k1gMnSc1
Hahn	Hahn	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Klen	klen	k1gInSc1
a	a	k8xC
J.	J.	kA
Šulcová	Šulcová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
přišel	přijít	k5eAaPmAgInS
roku	rok	k1gInSc2
1948	#num#	k4
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
realizovat	realizovat	k5eAaBmF
festival	festival	k1gInSc4
Bezručova	Bezručův	k2eAgFnSc1d1
Opava	Opava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Jako	jako	k8xS,k8xC
básník	básník	k1gMnSc1
debutoval	debutovat	k5eAaBmAgMnS
sbírkou	sbírka	k1gFnSc7
Dva	dva	k4xCgInPc4
hlasy	hlas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
sbírky	sbírka	k1gFnSc2
básní	básnit	k5eAaImIp3nS
pro	pro	k7c4
mládež	mládež	k1gFnSc4
Od	od	k7c2
Pradědu	praděd	k1gMnSc6
k	k	k7c3
Lysé	Lysé	k2eAgFnSc3d1
hoře	hora	k1gFnSc3
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
pět	pět	k4xCc4
básní	báseň	k1gFnPc2
zhudebnil	zhudebnit	k5eAaPmAgMnS
skladatel	skladatel	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Rychlý	Rychlý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Báseň	báseň	k1gFnSc1
„	„	k?
<g/>
Dívka	dívka	k1gFnSc1
v	v	k7c6
lese	les	k1gInSc6
<g/>
“	“	k?
ze	z	k7c2
sbírky	sbírka	k1gFnSc2
Cesta	cesta	k1gFnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
antologie	antologie	k1gFnSc2
Zapadlo	zapadnout	k5eAaPmAgNnS
slunce	slunce	k1gNnSc1
za	za	k7c7
dnem	den	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nebyl	být	k5eNaImAgInS
<g/>
,	,	kIx,
uspořádané	uspořádaný	k2eAgNnSc1d1
Ivanem	Ivan	k1gMnSc7
Wernischem	Wernisch	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Román	román	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
Příběh	příběh	k1gInSc1
poštmistra	poštmistr	k1gMnSc2
Dluhoše	Dluhoš	k1gMnSc2
o	o	k7c6
konfliktním	konfliktní	k2eAgNnSc6d1
nerovném	rovný	k2eNgNnSc6d1
manželství	manželství	k1gNnSc6
Čecha	Čech	k1gMnSc2
a	a	k8xC
Němky	Němka	k1gFnSc2
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
národnostním	národnostní	k2eAgNnSc6d1
pomezí	pomezí	k1gNnSc6
v	v	k7c6
okolí	okolí	k1gNnSc6
Krnova	Krnov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1940	#num#	k4
vydal	vydat	k5eAaPmAgInS
román	román	k1gInSc4
Chata	chata	k1gFnSc1
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
roku	rok	k1gInSc6
1943	#num#	k4
životopisný	životopisný	k2eAgInSc4d1
román	román	k1gInSc4
Marnotratný	marnotratný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
o	o	k7c6
básníku	básník	k1gMnSc6
Václavu	Václav	k1gMnSc6
Šolcovi	Šolc	k1gMnSc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1945	#num#	k4
životopisnou	životopisný	k2eAgFnSc4d1
prózu	próza	k1gFnSc4
určenou	určený	k2eAgFnSc4d1
dětem	dítě	k1gFnPc3
Dům	dům	k1gInSc1
pod	pod	k7c7
horami	hora	k1gFnPc7
<g/>
:	:	kIx,
vypravování	vypravování	k1gNnPc4
o	o	k7c6
šťastném	šťastný	k2eAgNnSc6d1
dětství	dětství	k1gNnSc6
o	o	k7c6
básníku	básník	k1gMnSc6
Josefu	Josef	k1gMnSc6
Kalusovi	Kalus	k1gMnSc6
<g/>
,	,	kIx,
situovanou	situovaný	k2eAgFnSc7d1
do	do	k7c2
údolí	údolí	k1gNnSc2
u	u	k7c2
Čeladné	Čeladný	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
roku	rok	k1gInSc2
1948	#num#	k4
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
Přátelství	přátelství	k1gNnSc2
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gInSc2
s	s	k7c7
Valašským	valašský	k2eAgInSc7d1
slavíkem	slavík	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
literární	literární	k2eAgInSc1d1
fond	fond	k1gInSc1
je	být	k5eAaImIp3nS
uchováván	uchovávat	k5eAaImNgInS
v	v	k7c6
Památníku	památník	k1gInSc6
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
v	v	k7c6
Opavě	Opava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Šulc	Šulc	k1gMnSc1
</s>
<s>
Vrak	vrak	k1gInSc1
:	:	kIx,
prosa	prosa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrabyně	Hrabyně	k1gFnSc1
:	:	kIx,
Iskra	Iskra	k1gMnSc1
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hnědka	hnědka	k1gFnSc1
:	:	kIx,
příběh	příběh	k1gInSc1
koně	kůň	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
:	:	kIx,
Edice	edice	k1gFnSc1
Pásmo	pásmo	k1gNnSc1
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dva	dva	k4xCgInPc4
hlasy	hlas	k1gInPc4
:	:	kIx,
[	[	kIx(
<g/>
verše	verš	k1gInPc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petřvald	Petřvald	k1gInSc1
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
:	:	kIx,
Jan	Jan	k1gMnSc1
Vicher	Vicher	k?
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Návrat	návrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
:	:	kIx,
Edice	edice	k1gFnSc1
Pásmo	pásmo	k1gNnSc1
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
Pradědu	praděd	k1gMnSc6
k	k	k7c3
Lysé	Lysé	k2eAgFnSc3d1
hoře	hora	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
:	:	kIx,
Učitelská	učitelský	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
státních	státní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
národních	národní	k2eAgFnPc2d1
na	na	k7c6
Opavsku	Opavsko	k1gNnSc6
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
19	#num#	k4
básní	báseň	k1gFnPc2
pro	pro	k7c4
mládež	mládež	k1gFnSc4
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
V.	V.	kA
Rais	Rais	k1gMnSc1
v	v	k7c6
Sobotce	Sobotka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sobotka	Sobotka	k1gFnSc1
:	:	kIx,
Okresní	okresní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
osvětový	osvětový	k2eAgInSc1d1
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
drobný	drobný	k2eAgInSc1d1
spisek	spisek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Příběh	příběh	k1gInSc1
poštmistra	poštmistr	k1gMnSc2
Dluhoše	Dluhoš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
:	:	kIx,
Edice	edice	k1gFnSc1
Pásmo	pásmo	k1gNnSc1
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chata	chata	k1gFnSc1
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
:	:	kIx,
Iskra	Iskra	k1gMnSc1
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
:	:	kIx,
básně	báseň	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sobotka	Sobotka	k1gMnSc1
:	:	kIx,
Leopold	Leopold	k1gMnSc1
Lažan	Lažan	k1gMnSc1
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Marnotratný	marnotratný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Pražská	pražský	k2eAgFnSc1d1
akciová	akciový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
životopisný	životopisný	k2eAgInSc1d1
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
jara	jaro	k1gNnSc2
do	do	k7c2
jara	jaro	k1gNnSc2
:	:	kIx,
verše	verš	k1gInSc2
pro	pro	k7c4
malé	malý	k2eAgMnPc4d1
čtenáře	čtenář	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Vilém	Vilém	k1gMnSc1
Šmidt	Šmidt	k1gMnSc1
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dům	dům	k1gInSc1
pod	pod	k7c7
horami	hora	k1gFnPc7
:	:	kIx,
vypravování	vypravování	k1gNnPc4
o	o	k7c6
šťastném	šťastný	k2eAgNnSc6d1
dětství	dětství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stateční	statečný	k2eAgMnPc1d1
chlapci	chlapec	k1gMnPc1
z	z	k7c2
Přímoří	Přímoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přátelství	přátelství	k1gNnSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
s	s	k7c7
Valašským	valašský	k2eAgInSc7d1
slavíkem	slavík	k1gInSc7
:	:	kIx,
Vzpomínka	vzpomínka	k1gFnSc1
k	k	k7c3
80	#num#	k4
<g/>
.	.	kIx.
narozeninám	narozeniny	k1gFnPc3
autora	autor	k1gMnSc2
Slezských	slezský	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
:	:	kIx,
B.	B.	kA
a	a	k8xC
Zb	Zb	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezecných	Bezecný	k2eAgMnPc2d1
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Záznam	záznam	k1gInSc1
v	v	k7c6
katalogu	katalog	k1gInSc6
Vědecké	vědecký	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
↑	↑	k?
Šulc	Šulc	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezručova	Bezručův	k2eAgFnSc1d1
Opava	Opava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
osvětě	osvěta	k1gFnSc6
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
17	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
–	–	k?
<g/>
258	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
cit	cit	k1gInSc4
<g/>
.	.	kIx.
dle	dle	k7c2
http://is.muni.cz/th/180607/ff_m/Diplomova_prace-Bezrucova_Opava.doc	http://is.muni.cz/th/180607/ff_m/Diplomova_prace-Bezrucova_Opava.doc	k1gFnSc1
[	[	kIx(
<g/>
DOC	doc	kA
<g/>
,	,	kIx,
1,83	1,83	k4
MB	MB	kA
<g/>
]	]	kIx)
<g/>
,	,	kIx,
s.	s.	k?
<g/>
5	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
↑	↑	k?
MARTÍNEK	Martínek	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Šulc	Šulc	k1gMnSc1
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
inspirovat	inspirovat	k5eAaBmF
Krnovskem	Krnovsko	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2008-08-02	2008-08-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Valašské	valašský	k2eAgFnSc2d1
Athény	Athéna	k1gFnSc2
(	(	kIx(
<g/>
sdružení	sdružení	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
osobností	osobnost	k1gFnPc2
východní	východní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Josef	Josef	k1gMnSc1
Fabián	Fabián	k1gMnSc1
<g/>
.	.	kIx.
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
ŠULC	Šulc	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
-	-	kIx~
spisovatel	spisovatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovník	slovník	k1gInSc4
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
hesla	heslo	k1gNnSc2
Pavel	Pavel	k1gMnSc1
Šopák	Šopák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
pro	pro	k7c4
českou	český	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
AV	AV	kA
ĆR	ĆR	kA
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-07-31	2011-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Památník	památník	k1gInSc1
Petra	Petr	k1gMnSc4
Bezruče	Bezruč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1131660	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
40593186	#num#	k4
</s>
