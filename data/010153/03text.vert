<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Vaňková	Vaňková	k1gFnSc1	Vaňková
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
na	na	k7c6	na
Letní	letní	k2eAgFnSc6d1	letní
univerziádě	univerziáda	k1gFnSc6	univerziáda
v	v	k7c6	v
Kazani	Kazaň	k1gFnSc6	Kazaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Finálové	finálový	k2eAgFnSc3d1	finálová
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
okruhu	okruh	k1gInSc2	okruh
ITF	ITF	kA	ITF
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vítězka	vítězka	k1gFnSc1	vítězka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finalistka	finalistka	k1gFnSc1	finalistka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
6	[number]	k4	6
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vítězka	vítězka	k1gFnSc1	vítězka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Finalistka	finalistka	k1gFnSc1	finalistka
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
