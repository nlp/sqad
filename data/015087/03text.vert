<s>
Lečchumi	Lečchu	k1gFnPc7
</s>
<s>
Lečchumi	Lečchu	k1gFnPc7
Geografie	geografie	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
42	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
722	#num#	k4
km²	km²	k?
Obyvatelstvo	obyvatelstvo	k1gNnSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lečchumi	Lečchumi	k1gFnPc1
(	(	kIx(
<g/>
gruzínsky	gruzínsky	k6eAd1
ლ	ლ	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
historický	historický	k2eAgInSc4d1
region	region	k1gInSc4
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Gruzie	Gruzie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
722	#num#	k4
km²	km²	k?
a	a	k8xC
17	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
povodí	povodí	k1gNnSc6
řek	řeka	k1gFnPc2
Cchenisckali	Cchenisckali	k1gFnSc4
a	a	k8xC
Rioni	Rioeň	k1gFnSc3
a	a	k8xC
jeho	jeho	k3xOp3gNnSc7
střediskem	středisko	k1gNnSc7
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
Cageri	Cager	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
je	být	k5eAaImIp3nS
administrativní	administrativní	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
kraje	kraj	k1gInSc2
Rača-Lečchumi	Rača-Lečchu	k1gFnPc7
a	a	k8xC
Dolní	dolní	k2eAgFnPc4d1
Svanetie	Svanetie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
železné	železný	k2eAgFnSc2d1
zde	zde	k6eAd1
vzkvétala	vzkvétat	k5eAaImAgFnS
kolchidská	kolchidský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
byla	být	k5eAaImAgFnS
oblast	oblast	k1gFnSc1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Takveri	Takveri	k1gNnSc1
a	a	k8xC
Prokopios	Prokopios	k1gInSc1
z	z	k7c2
Kaisareie	Kaisareie	k1gFnSc2
ji	on	k3xPp3gFnSc4
uvádí	uvádět	k5eAaImIp3nS
pod	pod	k7c7
názvem	název	k1gInSc7
Scymnia	Scymnium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1455	#num#	k4
byla	být	k5eAaImAgFnS
oblast	oblast	k1gFnSc4
součástí	součást	k1gFnPc2
Imeretského	Imeretský	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1714	#num#	k4
patřila	patřit	k5eAaImAgFnS
pod	pod	k7c4
Samegrelo	Samegrela	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pod	pod	k7c7
ruskou	ruský	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
zde	zde	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
Lečchumský	Lečchumský	k2eAgInSc1d1
ujezd	ujezd	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hornatá	hornatý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
„	„	k?
<g/>
gruzínské	gruzínský	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
“	“	k?
a	a	k8xC
dominuje	dominovat	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
hora	hora	k1gFnSc1
Chvamli	Chvamle	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
podle	podle	k7c2
pověsti	pověst	k1gFnSc2
upoután	upoután	k2eAgMnSc1d1
Prométheus	Prométheus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
četné	četný	k2eAgFnPc1d1
louky	louka	k1gFnPc1
a	a	k8xC
kaštanovníkové	kaštanovníkový	k2eAgInPc1d1
a	a	k8xC
habrové	habrový	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
výroba	výroba	k1gFnSc1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
především	především	k9
odrůdy	odrůda	k1gFnSc2
usacheluri	usachelur	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Lečchumi	Lečchu	k1gFnPc7
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
historických	historický	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
jako	jako	k9
např.	např.	kA
Dechviri	Dechviri	k1gNnSc4
a	a	k8xC
Murisciche	Murisciche	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://georgiantravelguide.com/en/lechkhumi	https://georgiantravelguide.com/en/lechkhu	k1gFnPc7
<g/>
↑	↑	k?
http://www.georgiawinecountry.com/racha-lechkhumi-and-kvemo-svaneti/	http://www.georgiawinecountry.com/racha-lechkhumi-and-kvemo-svaneti/	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Gruzie	Gruzie	k1gFnSc1
</s>
