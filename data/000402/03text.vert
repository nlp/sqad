<s>
Děkanát	děkanát	k1gInSc1	děkanát
a	a	k8xC	a
vikariát	vikariát	k1gInSc1	vikariát
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
územní	územní	k2eAgFnSc2d1	územní
jednotky	jednotka	k1gFnSc2	jednotka
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
farnost	farnost	k1gFnSc1	farnost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
několika	několik	k4yIc2	několik
farností	farnost	k1gFnPc2	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
děkan	děkan	k1gMnSc1	děkan
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
decanus	decanus	k1gMnSc1	decanus
(	(	kIx(	(
<g/>
zástupce	zástupce	k1gMnSc1	zástupce
deseti	deset	k4xCc7	deset
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
vikář	vikář	k1gMnSc1	vikář
<g/>
"	"	kIx"	"
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
vicarius	vicarius	k1gMnSc1	vicarius
(	(	kIx(	(
<g/>
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
děkanát	děkanát	k1gInSc1	děkanát
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
či	či	k8xC	či
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
děkanátu	děkanát	k1gInSc2	děkanát
je	být	k5eAaImIp3nS	být
děkanství	děkanství	k1gNnSc4	děkanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
děkanátu	děkanát	k1gInSc2	děkanát
je	být	k5eAaImIp3nS	být
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pomocníkem	pomocník	k1gMnSc7	pomocník
biskupa	biskup	k1gMnSc2	biskup
v	v	k7c4	v
řízení	řízení	k1gNnSc4	řízení
diecéze	diecéze	k1gFnSc2	diecéze
na	na	k7c4	na
území	území	k1gNnSc4	území
svého	svůj	k3xOyFgInSc2	svůj
děkanátu	děkanát	k1gInSc2	děkanát
<g/>
,	,	kIx,	,
svolává	svolávat	k5eAaImIp3nS	svolávat
kněze	kněz	k1gMnPc4	kněz
na	na	k7c4	na
pravidelná	pravidelný	k2eAgNnPc4d1	pravidelné
setkání	setkání	k1gNnPc4	setkání
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
vizitace	vizitace	k1gFnSc2	vizitace
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc1	povinnost
vůči	vůči	k7c3	vůči
kněžím	kněz	k1gMnPc3	kněz
a	a	k8xC	a
farnostem	farnost	k1gFnPc3	farnost
svého	své	k1gNnSc2	své
děkanátu	děkanát	k1gInSc2	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
Děkanát	děkanát	k1gInSc1	děkanát
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
tvořen	tvořit	k5eAaImNgInS	tvořit
několika	několik	k4yIc7	několik
farnostmi	farnost	k1gFnPc7	farnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
farář	farář	k1gMnSc1	farář
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
tyto	tento	k3xDgFnPc4	tento
farnosti	farnost	k1gFnPc4	farnost
vizituje	vizitovat	k5eAaImIp3nS	vizitovat
-	-	kIx~	-
to	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
jménem	jméno	k1gNnSc7	jméno
místního	místní	k2eAgInSc2d1	místní
biskupa	biskup	k1gInSc2	biskup
stav	stav	k1gInSc4	stav
pastorační	pastorační	k2eAgInSc4d1	pastorační
<g/>
,	,	kIx,	,
administrativní	administrativní	k2eAgInSc4d1	administrativní
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
farností	farnost	k1gFnPc2	farnost
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
dle	dle	k7c2	dle
směrnic	směrnice	k1gFnPc2	směrnice
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
vikariát	vikariát	k1gInSc1	vikariát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
okrskový	okrskový	k2eAgMnSc1d1	okrskový
vikář	vikář	k1gMnSc1	vikář
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
děkanáty	děkanát	k1gInPc4	děkanát
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Arnoštem	Arnošt	k1gMnSc7	Arnošt
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
z	z	k7c2	z
Harrachu	Harrach	k1gInSc2	Harrach
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
dělením	dělení	k1gNnSc7	dělení
na	na	k7c4	na
vikariáty	vikariát	k1gInPc4	vikariát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
krajového	krajový	k2eAgNnSc2d1	krajové
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
Vikariát	vikariát	k1gInSc1	vikariát
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
obvykle	obvykle	k6eAd1	obvykle
několik	několik	k4yIc4	několik
původních	původní	k2eAgInPc2d1	původní
děkanátů	děkanát	k1gInPc2	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k9	až
1999	[number]	k4	1999
existovaly	existovat	k5eAaImAgInP	existovat
vikariáty	vikariát	k1gInPc1	vikariát
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
diecézi	diecéze	k1gFnSc6	diecéze
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
děkanáty	děkanát	k1gInPc7	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
brněnská	brněnský	k2eAgFnSc1d1	brněnská
diecéze	diecéze	k1gFnSc1	diecéze
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
biskupa	biskup	k1gInSc2	biskup
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Cikrleho	Cikrle	k1gMnSc2	Cikrle
nově	nově	k6eAd1	nově
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
podle	podle	k7c2	podle
děkanátů	děkanát	k1gInPc2	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
web	web	k1gInSc1	web
brněnského	brněnský	k2eAgNnSc2d1	brněnské
biskupství	biskupství	k1gNnSc2	biskupství
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
děkanáty	děkanát	k1gInPc4	děkanát
<g/>
.	.	kIx.	.
</s>
