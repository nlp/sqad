<s>
Metropolitní	metropolitní	k2eAgNnSc1d1	metropolitní
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Art	Art	k1gFnSc1	Art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
MET	met	k1gInSc1	met
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgNnSc4d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
muzeum	muzeum	k1gNnSc4	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
