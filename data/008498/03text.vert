<p>
<s>
Metropolitní	metropolitní	k2eAgNnSc1d1	metropolitní
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Art	Art	k1gFnSc1	Art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
MET	met	k1gInSc1	met
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgNnSc4d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
muzeum	muzeum	k1gNnSc4	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Páté	pátý	k4xOgFnSc6	pátý
avenue	avenue	k1gFnSc6	avenue
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Central	Central	k1gFnSc2	Central
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
"	"	kIx"	"
<g/>
86	[number]	k4	86
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
"	"	kIx"	"
<g/>
..	..	k?	..
Založeno	založen	k2eAgNnSc1d1	založeno
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
ze	z	k7c2	z
soukromých	soukromý	k2eAgInPc2d1	soukromý
darů	dar	k1gInPc2	dar
(	(	kIx(	(
<g/>
nadační	nadační	k2eAgNnSc1d1	nadační
jmění	jmění	k1gNnSc1	jmění
přes	přes	k7c4	přes
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provizorně	provizorně	k6eAd1	provizorně
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
1872	[number]	k4	1872
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
několikrát	několikrát	k6eAd1	několikrát
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
190	[number]	k4	190
tisíc	tisíc	k4xCgInPc2	tisíc
m	m	kA	m
<g/>
2	[number]	k4	2
podlahové	podlahový	k2eAgFnSc2d1	podlahová
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
sbírce	sbírka	k1gFnSc6	sbírka
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
je	on	k3xPp3gMnPc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
přes	přes	k7c4	přes
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
volný	volný	k2eAgInSc1d1	volný
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
silně	silně	k6eAd1	silně
omezila	omezit	k5eAaPmAgFnS	omezit
výnos	výnos	k1gInSc4	výnos
nadačního	nadační	k2eAgNnSc2d1	nadační
jmění	jmění	k1gNnSc2	jmění
<g/>
,	,	kIx,	,
zavedlo	zavést	k5eAaPmAgNnS	zavést
muzeum	muzeum	k1gNnSc1	muzeum
vstupné	vstupný	k2eAgFnSc2d1	vstupná
25	[number]	k4	25
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
pojato	pojmout	k5eAaPmNgNnS	pojmout
jako	jako	k8xC	jako
univerzální	univerzální	k2eAgNnSc1d1	univerzální
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
představovat	představovat	k5eAaImF	představovat
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
včetně	včetně	k7c2	včetně
užitého	užitý	k2eAgNnSc2d1	užité
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
19	[number]	k4	19
sekcí	sekce	k1gFnPc2	sekce
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
podle	podle	k7c2	podle
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
stáří	stáří	k1gNnSc4	stáří
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
jako	jako	k8xC	jako
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
sbírky	sbírka	k1gFnPc4	sbírka
například	například	k6eAd1	například
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
oblékání	oblékání	k1gNnSc2	oblékání
a	a	k8xC	a
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
šperků	šperk	k1gInPc2	šperk
nebo	nebo	k8xC	nebo
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
grafiky	grafika	k1gFnSc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
restaurátorské	restaurátorský	k2eAgFnPc4d1	restaurátorská
dílny	dílna	k1gFnPc4	dílna
a	a	k8xC	a
badatelská	badatelský	k2eAgNnPc4d1	badatelské
oddělení	oddělení	k1gNnPc4	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc4	dva
pobočky	pobočka	k1gFnPc4	pobočka
<g/>
,	,	kIx,	,
sbírku	sbírka	k1gFnSc4	sbírka
evropského	evropský	k2eAgNnSc2d1	Evropské
středověkého	středověký	k2eAgNnSc2d1	středověké
umění	umění	k1gNnSc2	umění
The	The	k1gFnSc2	The
Cloisters	Cloistersa	k1gFnPc2	Cloistersa
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
Manhattanu	Manhattan	k1gInSc6	Manhattan
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
budovu	budova	k1gFnSc4	budova
Met	Mety	k1gFnPc2	Mety
Breuer	Breura	k1gFnPc2	Breura
na	na	k7c4	na
Madison	Madison	k1gNnSc4	Madison
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
evropského	evropský	k2eAgNnSc2d1	Evropské
a	a	k8xC	a
amerického	americký	k2eAgNnSc2d1	americké
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
muzeum	muzeum	k1gNnSc1	muzeum
projekt	projekt	k1gInSc1	projekt
"	"	kIx"	"
<g/>
digitálních	digitální	k2eAgFnPc2d1	digitální
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
postupně	postupně	k6eAd1	postupně
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
muzejních	muzejní	k2eAgFnPc2d1	muzejní
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
katalogy	katalog	k1gInPc1	katalog
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
publikace	publikace	k1gFnSc2	publikace
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírky	sbírka	k1gFnSc2	sbírka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
a	a	k8xC	a
historické	historický	k2eAgInPc4d1	historický
celky	celek	k1gInPc4	celek
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Blízký	blízký	k2eAgInSc1d1	blízký
východ	východ	k1gInSc1	východ
–	–	k?	–
Sumer	Sumer	k1gInSc1	Sumer
<g/>
,	,	kIx,	,
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
Elam	Elam	k1gInSc1	Elam
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
předmětů	předmět	k1gInPc2	předmět
</s>
</p>
<p>
<s>
Asijské	asijský	k2eAgNnSc1d1	asijské
umění	umění	k1gNnSc1	umění
–	–	k?	–
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
zahradní	zahradní	k2eAgInSc1d1	zahradní
dvorek	dvorek	k1gInSc1	dvorek
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Minga	k1gFnPc2	Minga
</s>
</p>
<p>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
umění	umění	k1gNnSc1	umění
–	–	k?	–
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
archeologické	archeologický	k2eAgFnSc2d1	archeologická
činnosti	činnost	k1gFnSc2	činnost
<g/>
;	;	kIx,	;
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
sbírka	sbírka	k1gFnSc1	sbírka
modelů	model	k1gInPc2	model
z	z	k7c2	z
faraonských	faraonský	k2eAgInPc2d1	faraonský
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
hroch	hroch	k1gMnSc1	hroch
William	William	k1gInSc4	William
je	být	k5eAaImIp3nS	být
maskotem	maskot	k1gInSc7	maskot
muzea	muzeum	k1gNnSc2	muzeum
</s>
</p>
<p>
<s>
Řecký	řecký	k2eAgInSc1d1	řecký
a	a	k8xC	a
římský	římský	k2eAgInSc1d1	římský
starověk	starověk	k1gInSc1	starověk
–	–	k?	–
17	[number]	k4	17
tisíc	tisíc	k4xCgInPc2	tisíc
předmětů	předmět	k1gInPc2	předmět
od	od	k7c2	od
kykladských	kykladský	k2eAgFnPc2d1	kykladská
soch	socha	k1gFnPc2	socha
po	po	k7c4	po
pozdní	pozdní	k2eAgInSc4d1	pozdní
Řím	Řím	k1gInSc4	Řím
</s>
</p>
<p>
<s>
Islámské	islámský	k2eAgNnSc1d1	islámské
umění	umění	k1gNnSc1	umění
–	–	k?	–
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
rukopisy	rukopis	k1gInPc1	rukopis
a	a	k8xC	a
miniatury	miniatura	k1gFnPc1	miniatura
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
a	a	k8xC	a
textil	textil	k1gInSc1	textil
</s>
</p>
<p>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
malířství	malířství	k1gNnSc1	malířství
–	–	k?	–
asi	asi	k9	asi
1700	[number]	k4	1700
předmětů	předmět	k1gInPc2	předmět
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
období	období	k1gNnPc2	období
</s>
</p>
<p>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
sochařství	sochařství	k1gNnSc1	sochařství
a	a	k8xC	a
užité	užitý	k2eAgNnSc1d1	užité
umění	umění	k1gNnSc1	umění
–	–	k?	–
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
předmětů	předmět	k1gInPc2	předmět
od	od	k7c2	od
soch	socha	k1gFnPc2	socha
až	až	k9	až
po	po	k7c4	po
nábytek	nábytek	k1gInSc4	nábytek
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Americké	americký	k2eAgNnSc1d1	americké
křídlo	křídlo	k1gNnSc1	křídlo
–	–	k?	–
americké	americký	k2eAgNnSc4d1	americké
umění	umění	k1gNnSc4	umění
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Oceánie	Oceánie	k1gFnSc1	Oceánie
a	a	k8xC	a
obojí	obojí	k4xRgFnSc1	obojí
Amerika	Amerika	k1gFnSc1	Amerika
–	–	k?	–
Rockefellerovo	Rockefellerův	k2eAgNnSc4d1	Rockefellerovo
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
objektů	objekt	k1gInPc2	objekt
od	od	k7c2	od
australských	australský	k2eAgFnPc2d1	australská
skalních	skalní	k2eAgFnPc2d1	skalní
kreseb	kresba	k1gFnPc2	kresba
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
</s>
</p>
<p>
<s>
===	===	k?	===
Tematické	tematický	k2eAgInPc1d1	tematický
celky	celek	k1gInPc1	celek
===	===	k?	===
</s>
</p>
<p>
<s>
Zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
zbroj	zbroj	k1gFnSc1	zbroj
–	–	k?	–
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
předmětů	předmět	k1gInPc2	předmět
od	od	k7c2	od
japonských	japonský	k2eAgInPc2d1	japonský
mečů	meč	k1gInPc2	meč
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
americké	americký	k2eAgFnPc4d1	americká
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Oblečení	oblečení	k1gNnSc1	oblečení
a	a	k8xC	a
móda	móda	k1gFnSc1	móda
–	–	k?	–
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
obleků	oblek	k1gInPc2	oblek
a	a	k8xC	a
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc1	výstava
slavných	slavný	k2eAgMnPc2d1	slavný
módních	módní	k2eAgMnPc2d1	módní
návrhářů	návrhář	k1gMnPc2	návrhář
</s>
</p>
<p>
<s>
Kresby	kresba	k1gFnPc1	kresba
a	a	k8xC	a
grafika	grafika	k1gFnSc1	grafika
–	–	k?	–
17	[number]	k4	17
tisíc	tisíc	k4xCgInPc2	tisíc
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
grafik	grafika	k1gFnPc2	grafika
a	a	k8xC	a
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
knih	kniha	k1gFnPc2	kniha
včetně	včetně	k7c2	včetně
slavných	slavný	k2eAgNnPc2d1	slavné
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
Michelangelo	Michelangela	k1gFnSc5	Michelangela
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Rembrandt	Rembrandt	k1gMnSc1	Rembrandt
<g/>
,	,	kIx,	,
Degas	Degas	k1gMnSc1	Degas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
Roberta	Robert	k1gMnSc2	Robert
Lehmana	Lehman	k1gMnSc2	Lehman
–	–	k?	–
soukromá	soukromý	k2eAgFnSc1d1	soukromá
sbírka	sbírka	k1gFnSc1	sbírka
evropského	evropský	k2eAgNnSc2d1	Evropské
malířství	malířství	k1gNnSc2	malířství
(	(	kIx(	(
<g/>
Boticelli	Boticell	k1gMnPc1	Boticell
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Greco	Greco	k6eAd1	Greco
<g/>
,	,	kIx,	,
Rembrandt	Rembrandt	k1gMnSc1	Rembrandt
<g/>
,	,	kIx,	,
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
–	–	k?	–
asi	asi	k9	asi
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
exponátů	exponát	k1gInPc2	exponát
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
</s>
</p>
<p>
<s>
==	==	k?	==
The	The	k1gFnPc2	The
Cloisters	Cloisters	k1gInSc1	Cloisters
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Cloisters	Cloisters	k1gInSc1	Cloisters
je	být	k5eAaImIp3nS	být
oddělení	oddělení	k1gNnSc4	oddělení
Metropolitního	metropolitní	k2eAgNnSc2d1	metropolitní
muzea	muzeum	k1gNnSc2	muzeum
umění	umění	k1gNnSc4	umění
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
středověké	středověký	k2eAgNnSc4d1	středověké
evropské	evropský	k2eAgNnSc4d1	Evropské
Umění	umění	k1gNnSc4	umění
a	a	k8xC	a
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
Fort	Fort	k?	Fort
Tryon	Tryon	k1gNnSc1	Tryon
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
na	na	k7c6	na
návrší	návrší	k1gNnPc4	návrší
při	při	k7c6	při
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
řeku	řeka	k1gFnSc4	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
sbírka	sbírka	k1gFnSc1	sbírka
čítá	čítat	k5eAaImIp3nS	čítat
asi	asi	k9	asi
pět	pět	k4xCc1	pět
tisíc	tisíc	k4xCgInSc1	tisíc
středověkých	středověký	k2eAgNnPc2d1	středověké
evropských	evropský	k2eAgNnPc2d1	Evropské
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
období	období	k1gNnSc2	období
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
stolet	stolet	k1gInSc1	stolet
<g/>
,	,	kIx,	,
od	od	k7c2	od
drobných	drobný	k2eAgInPc2d1	drobný
předmětů	předmět	k1gInPc2	předmět
až	až	k9	až
po	po	k7c4	po
architektonické	architektonický	k2eAgInPc4d1	architektonický
celky	celek	k1gInPc4	celek
(	(	kIx(	(
<g/>
kvadratury	kvadratura	k1gFnPc4	kvadratura
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přivezené	přivezený	k2eAgFnPc1d1	přivezená
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnPc4	fotografia
==	==	k?	==
</s>
</p>
<p>
<s>
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
kolekce	kolekce	k1gFnSc1	kolekce
fotografií	fotografia	k1gFnPc2	fotografia
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20.000	[number]	k4	20.000
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
pět	pět	k4xCc4	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
sbírek	sbírka	k1gFnPc2	sbírka
plus	plus	k6eAd1	plus
ještě	ještě	k6eAd1	ještě
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
akvizice	akvizice	k1gFnPc4	akvizice
<g/>
.	.	kIx.	.
</s>
<s>
Proslulý	proslulý	k2eAgMnSc1d1	proslulý
fotograf	fotograf	k1gMnSc1	fotograf
Alfred	Alfred	k1gMnSc1	Alfred
Stieglitz	Stieglitz	k1gMnSc1	Stieglitz
muzeu	muzeum	k1gNnSc6	muzeum
daroval	darovat	k5eAaPmAgMnS	darovat
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
sbírku	sbírka	k1gFnSc4	sbírka
svých	svůj	k3xOyFgFnPc2	svůj
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgInSc4d1	komplexní
přehled	přehled	k1gInSc4	přehled
piktorialistických	piktorialistický	k2eAgMnPc2d1	piktorialistický
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc4d1	bohatý
soubor	soubor	k1gInSc4	soubor
grafických	grafický	k2eAgInPc2d1	grafický
listů	list	k1gInPc2	list
mistra	mistr	k1gMnSc2	mistr
Edwarda	Edward	k1gMnSc2	Edward
Steichena	Steichen	k1gMnSc2	Steichen
a	a	k8xC	a
kolekce	kolekce	k1gFnSc2	kolekce
Stieglitzových	Stieglitzových	k2eAgFnSc7d1	Stieglitzových
fotografií	fotografia	k1gFnSc7	fotografia
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
vlastního	vlastní	k2eAgNnSc2d1	vlastní
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Stieglitzovu	Stieglitzův	k2eAgFnSc4d1	Stieglitzův
sbírku	sbírka	k1gFnSc4	sbírka
doplnilo	doplnit	k5eAaPmAgNnS	doplnit
8500	[number]	k4	8500
kusy	kus	k1gInPc4	kus
kolekce	kolekce	k1gFnPc1	kolekce
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
Gilman	Gilman	k1gMnSc1	Gilman
Paper	Paper	k1gInSc4	Paper
Company	Compana	k1gFnSc2	Compana
Collection	Collection	k1gInSc1	Collection
<g/>
,	,	kIx,	,
Rubel	Rubel	k1gInSc1	Rubel
Collection	Collection	k1gInSc1	Collection
a	a	k8xC	a
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
Collection	Collection	k1gInSc1	Collection
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
díla	dílo	k1gNnPc4	dílo
rané	raný	k2eAgFnSc2d1	raná
francouzské	francouzský	k2eAgFnSc2d1	francouzská
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
rané	raný	k2eAgFnSc2d1	raná
britské	britský	k2eAgFnSc2d1	britská
fotografie	fotografia	k1gFnSc2	fotografia
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
i	i	k8xC	i
evropské	evropský	k2eAgFnSc2d1	Evropská
fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
také	také	k9	také
získalo	získat	k5eAaPmAgNnS	získat
osobní	osobní	k2eAgFnSc4d1	osobní
sbírku	sbírka	k1gFnSc4	sbírka
fotografií	fotografia	k1gFnPc2	fotografia
Walkera	Walkera	k1gFnSc1	Walkera
Evanse	Evanse	k1gFnSc1	Evanse
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
oddělení	oddělení	k1gNnSc1	oddělení
získalo	získat	k5eAaPmAgNnS	získat
stálou	stálý	k2eAgFnSc4d1	stálá
expozici	expozice	k1gFnSc4	expozice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
vystavena	vystavit	k5eAaPmNgFnS	vystavit
všechna	všechen	k3xTgNnPc4	všechen
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
citlivým	citlivý	k2eAgInPc3d1	citlivý
materiálům	materiál	k1gInPc3	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
fotografické	fotografický	k2eAgNnSc4d1	fotografické
oddělení	oddělení	k1gNnSc4	oddělení
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
zorganizovalo	zorganizovat	k5eAaPmAgNnS	zorganizovat
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
výstav	výstava	k1gFnPc2	výstava
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
výstavy	výstava	k1gFnSc2	výstava
Diane	Dian	k1gInSc5	Dian
Arbus	Arbus	k1gInSc4	Arbus
a	a	k8xC	a
také	také	k9	také
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
retrospektivy	retrospektiva	k1gFnSc2	retrospektiva
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
tématu	téma	k1gNnSc3	téma
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
díla	dílo	k1gNnSc2	dílo
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Gustave	Gustav	k1gMnSc5	Gustav
Le	Le	k1gMnSc2	Le
Gray	Gra	k2eAgFnPc1d1	Gra
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
1820	[number]	k4	1820
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Édouard	Édouard	k1gMnSc1	Édouard
Baldus	Baldus	k1gMnSc1	Baldus
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
,	,	kIx,	,
1813	[number]	k4	1813
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nadar	Nadar	k1gMnSc1	Nadar
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1820	[number]	k4	1820
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eugè	Eugè	k1gMnSc1	Eugè
Cuvelier	Cuvelier	k1gMnSc1	Cuvelier
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Diane	Dian	k1gInSc5	Dian
Arbus	Arbus	k1gInSc1	Arbus
(	(	kIx(	(
<g/>
Američanka	Američanka	k1gFnSc1	Američanka
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Bernd	Bernd	k1gMnSc1	Bernd
a	a	k8xC	a
Hilla	Hilla	k1gMnSc1	Hilla
Becherovi	Becherův	k2eAgMnPc1d1	Becherův
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
;	;	kIx,	;
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rineke	Rineke	k1gFnSc1	Rineke
Dijkstra	Dijkstra	k1gFnSc1	Dijkstra
(	(	kIx(	(
<g/>
Holanďanka	Holanďanka	k1gFnSc1	Holanďanka
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Ruff	Ruff	k1gMnSc1	Ruff
(	(	kIx(	(
<g/>
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Yousuf	Yousuf	k1gMnSc1	Yousuf
Karsh	Karsh	k1gMnSc1	Karsh
(	(	kIx(	(
<g/>
Kanaďan	Kanaďan	k1gMnSc1	Kanaďan
arménského	arménský	k2eAgInSc2d1	arménský
původu	původ	k1gInSc2	původ
1908	[number]	k4	1908
-	-	kIx~	-
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
od	od	k7c2	od
André	André	k1gMnPc2	André
Kertésze	Kertésze	k1gFnSc2	Kertésze
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
100	[number]	k4	100
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgInSc1d3	veliký
muzejní	muzejní	k2eAgInSc1d1	muzejní
přírůstek	přírůstek	k1gInSc1	přírůstek
od	od	k7c2	od
žijícího	žijící	k2eAgMnSc2d1	žijící
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
obrazů	obraz	k1gInPc2	obraz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
</s>
</p>
<p>
<s>
Guggenheimovo	Guggenheimův	k2eAgNnSc1d1	Guggenheimovo
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Art	Art	k1gFnPc2	Art
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc2	Art
<g/>
:	:	kIx,	:
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
The	The	k?	The
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
presents	presents	k6eAd1	presents
a	a	k8xC	a
Timeline	Timelin	k1gInSc5	Timelin
of	of	k?	of
Art	Art	k1gFnSc1	Art
History	Histor	k1gInPc4	Histor
</s>
</p>
<p>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
Topic	Topic	k1gMnSc1	Topic
Page	Page	k1gInSc4	Page
</s>
</p>
<p>
<s>
Watsonline	Watsonlin	k1gInSc5	Watsonlin
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Catalog	Catalog	k1gMnSc1	Catalog
of	of	k?	of
the	the	k?	the
Libraries	Librariesa	k1gFnPc2	Librariesa
of	of	k?	of
The	The	k1gMnPc2	The
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Art	Art	k1gFnPc2	Art
</s>
</p>
<p>
<s>
Artwork	Artwork	k1gInSc1	Artwork
owned	owned	k1gInSc1	owned
by	by	kYmCp3nS	by
The	The	k1gMnSc3	The
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
</s>
</p>
