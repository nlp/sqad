<p>
<s>
Banánovník	banánovník	k1gInSc1	banánovník
textilní	textilní	k2eAgInSc1d1	textilní
(	(	kIx(	(
<g/>
Musa	Musa	k1gFnSc1	Musa
textilis	textilis	k1gFnSc2	textilis
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
též	též	k9	též
manilské	manilský	k2eAgNnSc1d1	manilské
konopí	konopí	k1gNnSc1	konopí
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
banánovníkovitých	banánovníkovitý	k2eAgFnPc2d1	banánovníkovitý
proslulá	proslulý	k2eAgFnSc1d1	proslulá
svými	svůj	k3xOyFgNnPc7	svůj
textilními	textilní	k2eAgNnPc7d1	textilní
vlákny	vlákno	k1gNnPc7	vlákno
z	z	k7c2	z
listů	list	k1gInPc2	list
a	a	k8xC	a
řapíků	řapík	k1gInPc2	řapík
zvanými	zvaný	k2eAgFnPc7d1	zvaná
abaka	abaka	k1gFnSc1	abaka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pružná	pružný	k2eAgFnSc1d1	pružná
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
trvanlivá	trvanlivý	k2eAgNnPc1d1	trvanlivé
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
provazů	provaz	k1gInPc2	provaz
a	a	k8xC	a
motouzů	motouz	k1gInPc2	motouz
<g/>
.	.	kIx.	.
</s>
<s>
Banánovník	banánovník	k1gInSc1	banánovník
textilní	textilní	k2eAgFnSc2d1	textilní
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
pěstoval	pěstovat	k5eAaImAgInS	pěstovat
jen	jen	k9	jen
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
také	také	k9	také
na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
a	a	k8xC	a
Sumatře	Sumatra	k1gFnSc6	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
průměrně	průměrně	k6eAd1	průměrně
výšky	výška	k1gFnPc1	výška
6	[number]	k4	6
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Banánovník	banánovník	k1gInSc1	banánovník
textilní	textilní	k2eAgInSc1d1	textilní
pochází	pocházet	k5eAaImIp3nS	pocházet
původně	původně	k6eAd1	původně
z	z	k7c2	z
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
vláken	vlákno	k1gNnPc2	vlákno
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vyráběna	vyráběn	k2eAgNnPc4d1	vyráběno
lodní	lodní	k2eAgNnPc4d1	lodní
lana	lano	k1gNnPc4	lano
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Holanďané	Holanďan	k1gMnPc1	Holanďan
pěstování	pěstování	k1gNnPc2	pěstování
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
<g/>
,	,	kIx,	,
americké	americký	k2eAgNnSc1d1	americké
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
financovalo	financovat	k5eAaBmAgNnS	financovat
jeho	jeho	k3xOp3gFnSc4	jeho
pěstování	pěstování	k1gNnSc1	pěstování
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
AMerice	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
začala	začít	k5eAaPmAgFnS	začít
malá	malý	k2eAgFnSc1d1	malá
soukromá	soukromý	k2eAgFnSc1d1	soukromá
firma	firma	k1gFnSc1	firma
produkci	produkce	k1gFnSc4	produkce
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
severním	severní	k2eAgNnSc6d1	severní
Borneu	Borneo	k1gNnSc6	Borneo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Spojenci	spojenec	k1gMnPc1	spojenec
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
k	k	k7c3	k
manilskému	manilský	k2eAgNnSc3d1	manilské
konopí	konopí	k1gNnSc3	konopí
z	z	k7c2	z
Filipín	Filipíny	k1gFnPc2	Filipíny
ztratili	ztratit	k5eAaPmAgMnP	ztratit
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
jeho	jeho	k3xOp3gNnSc1	jeho
pěstování	pěstování	k1gNnSc1	pěstování
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
prudce	prudko	k6eAd1	prudko
zvýšit	zvýšit	k5eAaPmF	zvýšit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
banánovník	banánovník	k1gInSc1	banánovník
pěstován	pěstován	k2eAgInSc1d1	pěstován
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
částech	část	k1gFnPc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
tropická	tropický	k2eAgFnSc1d1	tropická
rostlina	rostlina	k1gFnSc1	rostlina
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
dešťové	dešťový	k2eAgFnPc4d1	dešťová
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Hektarový	hektarový	k2eAgInSc1d1	hektarový
výnos	výnos	k1gInSc1	výnos
u	u	k7c2	u
dobrých	dobrý	k2eAgFnPc2d1	dobrá
výsadeb	výsadba	k1gFnPc2	výsadba
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
čtyř	čtyři	k4xCgFnPc2	čtyři
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
<g/>
Významnými	významný	k2eAgMnPc7d1	významný
producenty	producent	k1gMnPc7	producent
jsou	být	k5eAaImIp3nP	být
Filipíny	Filipíny	k1gFnPc1	Filipíny
a	a	k8xC	a
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Indonésie	Indonésie	k1gFnSc1	Indonésie
a	a	k8xC	a
Panama	Panama	k1gFnSc1	Panama
produkují	produkovat	k5eAaImIp3nP	produkovat
kolem	kolem	k7c2	kolem
100	[number]	k4	100
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
při	při	k7c6	při
výnosu	výnos	k1gInSc6	výnos
0,1	[number]	k4	0,1
až	až	k9	až
1,5	[number]	k4	1,5
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
produkce	produkce	k1gFnSc2	produkce
je	být	k5eAaImIp3nS	být
exportována	exportován	k2eAgFnSc1d1	exportována
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
celkové	celkový	k2eAgFnSc2d1	celková
roční	roční	k2eAgFnSc2d1	roční
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
vláken	vlákna	k1gFnPc2	vlákna
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgNnPc1	všechen
jsou	být	k5eAaImIp3nP	být
exportována	exportován	k2eAgNnPc1d1	exportováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
