<s>
Niels	Niels	k6eAd1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
David	David	k1gMnSc1	David
Bohr	Bohr	k1gMnSc1	Bohr
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1885	[number]	k4	1885
Kodaň	Kodaň	k1gFnSc1	Kodaň
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
filantrop	filantrop	k1gMnSc1	filantrop
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
působící	působící	k2eAgMnSc1d1	působící
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
atomové	atomový	k2eAgFnSc2d1	atomová
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
