<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
územní	územní	k2eAgFnSc1d1	územní
mocenská	mocenský	k2eAgFnSc1d1	mocenská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnSc1	instituce
(	(	kIx(	(
<g/>
či	či	k8xC	či
organizace	organizace	k1gFnSc2	organizace
<g/>
)	)	kIx)	)
disponující	disponující	k2eAgFnSc7d1	disponující
mocí	moc	k1gFnSc7	moc
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
soudit	soudit	k5eAaImF	soudit
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
zákony	zákon	k1gInPc4	zákon
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
státní	státní	k2eAgFnSc7d1	státní
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
státním	státní	k2eAgInSc7d1	státní
lidem	lid	k1gInSc7	lid
(	(	kIx(	(
<g/>
státním	státní	k2eAgNnSc7d1	státní
občanstvím	občanství	k1gNnSc7	občanství
<g/>
)	)	kIx)	)
a	a	k8xC	a
státním	státní	k2eAgNnSc7d1	státní
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Svrchovaný	svrchovaný	k2eAgInSc4d1	svrchovaný
stát	stát	k1gInSc4	stát
není	být	k5eNaImIp3nS	být
podřízen	podřídit	k5eAaPmNgMnS	podřídit
žádné	žádný	k3yNgFnSc3	žádný
jiné	jiný	k2eAgFnSc3d1	jiná
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnSc3d1	státní
<g/>
)	)	kIx)	)
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
vnější	vnější	k2eAgNnSc1d1	vnější
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc4d1	státní
aparát	aparát	k1gInSc4	aparát
(	(	kIx(	(
<g/>
byrokracii	byrokracie	k1gFnSc4	byrokracie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
sbory	sbor	k1gInPc1	sbor
(	(	kIx(	(
<g/>
policii	policie	k1gFnSc3	policie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Niccolò	Niccolò	k?	Niccolò
Machiavelli	Machiavelli	k1gMnSc1	Machiavelli
poprvé	poprvé	k6eAd1	poprvé
definoval	definovat	k5eAaBmAgMnS	definovat
"	"	kIx"	"
<g/>
stát	stát	k5eAaPmF	stát
<g/>
"	"	kIx"	"
jako	jako	k9	jako
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
politický	politický	k2eAgInSc1d1	politický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
sledoval	sledovat	k5eAaImAgInS	sledovat
typ	typ	k1gInSc1	typ
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgMnSc1d1	neutrální
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
dobra	dobro	k1gNnSc2	dobro
ani	ani	k8xC	ani
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
zájmem	zájem	k1gInSc7	zájem
je	být	k5eAaImIp3nS	být
rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
udržení	udržení	k1gNnSc1	udržení
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
či	či	k8xC	či
jeho	jeho	k3xOp3gFnSc2	jeho
stability	stabilita	k1gFnSc2	stabilita
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
neudržuje	udržovat	k5eNaImIp3nS	udržovat
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vojenská	vojenský	k2eAgFnSc1d1	vojenská
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
definuje	definovat	k5eAaBmIp3nS	definovat
"	"	kIx"	"
<g/>
stát	stát	k5eAaImF	stát
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
lidské	lidský	k2eAgNnSc4d1	lidské
společenství	společenství	k1gNnSc4	společenství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
monopol	monopol	k1gInSc4	monopol
legitimního	legitimní	k2eAgNnSc2d1	legitimní
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
neexistovalo	existovat	k5eNaImAgNnS	existovat
násilí	násilí	k1gNnSc4	násilí
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
vynucování	vynucování	k1gNnSc2	vynucování
státního	státní	k2eAgInSc2d1	státní
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
by	by	kYmCp3nS	by
stát	stát	k1gInSc1	stát
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gNnSc2	on
by	by	kYmCp3nS	by
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
anarchie	anarchie	k1gFnSc1	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
definována	definován	k2eAgFnSc1d1	definována
v	v	k7c6	v
Konvenci	konvence	k1gFnSc6	konvence
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
povinnostech	povinnost	k1gFnPc6	povinnost
států	stát	k1gInPc2	stát
z	z	k7c2	z
Montevidea	Montevideo	k1gNnSc2	Montevideo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konvence	konvence	k1gFnSc2	konvence
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
subjektem	subjekt	k1gInSc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
splňující	splňující	k2eAgFnSc2d1	splňující
tato	tento	k3xDgNnPc4	tento
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
:	:	kIx,	:
stálé	stálý	k2eAgNnSc4d1	stálé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
vymezené	vymezený	k2eAgNnSc4d1	vymezené
státní	státní	k2eAgNnSc4d1	státní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc4d1	státní
moc	moc	k1gFnSc4	moc
-	-	kIx~	-
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
státní	státní	k2eAgInSc1d1	státní
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mezinárodněprávní	mezinárodněprávní	k2eAgFnSc1d1	mezinárodněprávní
subjektivita	subjektivita	k1gFnSc1	subjektivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Montevideo	Montevideo	k1gNnSc1	Montevideo
je	být	k5eAaImIp3nS	být
oblastní	oblastní	k2eAgFnSc1d1	oblastní
Americká	americký	k2eAgFnSc1d1	americká
konvence	konvence	k1gFnSc1	konvence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
principy	princip	k1gInPc1	princip
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
obecně	obecně	k6eAd1	obecně
uznány	uznat	k5eAaPmNgFnP	uznat
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
jsou	být	k5eAaImIp3nP	být
dostatečná	dostatečný	k2eAgNnPc1d1	dostatečné
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
z	z	k7c2	z
uvedených	uvedený	k2eAgNnPc2d1	uvedené
kritérií	kritérion	k1gNnPc2	kritérion
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
uznávána	uznáván	k2eAgNnPc1d1	uznáváno
<g/>
,	,	kIx,	,
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
de	de	k?	de
facto	facto	k1gNnSc1	facto
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
automaticky	automaticky	k6eAd1	automaticky
při	při	k7c6	při
splnění	splnění	k1gNnSc6	splnění
prvních	první	k4xOgFnPc2	první
třech	tři	k4xCgMnPc6	tři
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
v	v	k7c4	v
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
právu	právo	k1gNnSc3	právo
ani	ani	k9	ani
neudává	udávat	k5eNaImIp3nS	udávat
<g/>
.	.	kIx.	.
</s>
<s>
Praktickou	praktický	k2eAgFnSc7d1	praktická
podmínkou	podmínka	k1gFnSc7	podmínka
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
hlavní	hlavní	k2eAgNnSc1d1	hlavní
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
jeho	on	k3xPp3gNnSc2	on
uznání	uznání	k1gNnSc2	uznání
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
definuje	definovat	k5eAaBmIp3nS	definovat
stát	stát	k1gInSc4	stát
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
společenskou	společenský	k2eAgFnSc4d1	společenská
smlouvu	smlouva	k1gFnSc4	smlouva
mezi	mezi	k7c7	mezi
jednotlivcem	jednotlivec	k1gMnSc7	jednotlivec
a	a	k8xC	a
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
král	král	k1gMnSc1	král
přejímá	přejímat	k5eAaImIp3nS	přejímat
veškerou	veškerý	k3xTgFnSc4	veškerý
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yIgFnSc3	který
není	být	k5eNaImIp3nS	být
žádné	žádný	k3yNgNnSc4	žádný
odvolání	odvolání	k1gNnSc4	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
G.	G.	kA	G.
W.	W.	kA	W.
F.	F.	kA	F.
Hegel	Hegel	k1gMnSc1	Hegel
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
politická	politický	k2eAgFnSc1d1	politická
jednotka	jednotka	k1gFnSc1	jednotka
určená	určený	k2eAgFnSc1d1	určená
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
a	a	k8xC	a
autonomní	autonomní	k2eAgFnSc7d1	autonomní
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
státu	stát	k1gInSc3	stát
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
občanstvím	občanství	k1gNnSc7	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
forem	forma	k1gFnPc2	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
státu	stát	k1gInSc2	stát
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
například	například	k6eAd1	například
podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
suverenity	suverenita	k1gFnSc2	suverenita
svrchované	svrchovaný	k2eAgInPc4d1	svrchovaný
státy	stát	k1gInPc4	stát
nesvrchované	svrchovaný	k2eNgInPc4d1	svrchovaný
(	(	kIx(	(
<g/>
závislé	závislý	k2eAgInPc4d1	závislý
<g/>
)	)	kIx)	)
státy	stát	k1gInPc4	stát
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
zdroje	zdroj	k1gInPc1	zdroj
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
demokratické	demokratický	k2eAgInPc1d1	demokratický
státy	stát	k1gInPc1	stát
-	-	kIx~	-
na	na	k7c6	na
principech	princip	k1gInPc6	princip
(	(	kIx(	(
<g/>
nepřímé	přímý	k2eNgFnPc4d1	nepřímá
<g/>
)	)	kIx)	)
zastupitelské	zastupitelský	k2eAgFnPc4d1	zastupitelská
nebo	nebo	k8xC	nebo
přímé	přímý	k2eAgFnPc4d1	přímá
demokracie	demokracie	k1gFnPc4	demokracie
nedemokratické	demokratický	k2eNgFnPc4d1	nedemokratická
podle	podle	k7c2	podle
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
státní	státní	k2eAgFnPc1d1	státní
formy	forma	k1gFnPc1	forma
<g/>
)	)	kIx)	)
teokracie	teokracie	k1gFnSc1	teokracie
monarchie	monarchie	k1gFnSc1	monarchie
republika	republika	k1gFnSc1	republika
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
výkonu	výkon	k1gInSc2	výkon
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
centralizované	centralizovaný	k2eAgInPc1d1	centralizovaný
státy	stát	k1gInPc1	stát
decentralizované	decentralizovaný	k2eAgInPc1d1	decentralizovaný
státy	stát	k1gInPc4	stát
podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
státu	stát	k1gInSc2	stát
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
federace	federace	k1gFnSc2	federace
konfederace	konfederace	k1gFnSc2	konfederace
podle	podle	k7c2	podle
idejí	idea	k1gFnPc2	idea
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
–	–	k?	–
národ	národ	k1gInSc1	národ
má	mít	k5eAaImIp3nS	mít
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
(	(	kIx(	(
<g/>
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
občanský	občanský	k2eAgInSc1d1	občanský
stát	stát	k1gInSc1	stát
–	–	k?	–
všichni	všechen	k3xTgMnPc1	všechen
občané	občan	k1gMnPc1	občan
státu	stát	k1gInSc2	stát
mají	mít	k5eAaImIp3nP	mít
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
národu	národ	k1gInSc3	národ
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
národní	národní	k2eAgInSc1d1	národní
stát	stát	k1gInSc1	stát
–	–	k?	–
většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
menšiny	menšina	k1gFnPc1	menšina
téměř	téměř	k6eAd1	téměř
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
státy	stát	k1gInPc1	stát
s	s	k7c7	s
národnostními	národnostní	k2eAgFnPc7d1	národnostní
menšinami	menšina	k1gFnPc7	menšina
–	–	k?	–
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinový	většinový	k2eAgInSc4d1	většinový
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ale	ale	k8xC	ale
i	i	k9	i
významné	významný	k2eAgFnPc1d1	významná
menšiny	menšina	k1gFnPc1	menšina
(	(	kIx(	(
<g/>
Albánie	Albánie	k1gFnPc1	Albánie
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
Státy	stát	k1gInPc1	stát
více	hodně	k6eAd2	hodně
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
federativní	federativní	k2eAgInSc1d1	federativní
–	–	k?	–
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
žijí	žít	k5eAaImIp3nP	žít
dva	dva	k4xCgMnPc1	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
rovnocenných	rovnocenný	k2eAgInPc2d1	rovnocenný
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
státních	státní	k2eAgInPc2d1	státní
celků	celek	k1gInPc2	celek
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
společný	společný	k2eAgMnSc1d1	společný
panovník	panovník	k1gMnSc1	panovník
více	hodně	k6eAd2	hodně
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
reálné	reálný	k2eAgFnSc2d1	reálná
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
vlastně	vlastně	k9	vlastně
mezistupeň	mezistupeň	k1gInSc1	mezistupeň
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
kon-federace	konederace	k1gFnSc2	kon-federace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
způsoby	způsob	k1gInPc4	způsob
vzniku	vznik	k1gInSc2	vznik
států	stát	k1gInPc2	stát
působením	působení	k1gNnSc7	působení
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
i	i	k8xC	i
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
základních	základní	k2eAgFnPc2d1	základní
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
stát	stát	k1gInSc1	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
:	:	kIx,	:
nábožensko-teleologická	náboženskoeleologický	k2eAgFnSc1d1	nábožensko-teleologický
-	-	kIx~	-
stát	stát	k1gInSc1	stát
existuje	existovat	k5eAaImIp3nS	existovat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
boží	boží	k2eAgFnSc2d1	boží
vůle	vůle	k1gFnSc2	vůle
a	a	k8xC	a
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
přikázání	přikázání	k1gNnPc2	přikázání
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgMnSc1	každý
povinen	povinen	k2eAgMnSc1d1	povinen
ho	on	k3xPp3gMnSc4	on
uznávat	uznávat	k5eAaImF	uznávat
a	a	k8xC	a
podřizovat	podřizovat	k5eAaImF	podřizovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
učení	učení	k1gNnSc1	učení
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
<g/>
.	.	kIx.	.
teorie	teorie	k1gFnSc1	teorie
moci	moc	k1gFnSc2	moc
-	-	kIx~	-
představuje	představovat	k5eAaImIp3nS	představovat
si	se	k3xPyFc3	se
stát	stát	k5eAaImF	stát
jako	jako	k8xC	jako
vládu	vláda	k1gFnSc4	vláda
silného	silný	k2eAgNnSc2d1	silné
nad	nad	k7c7	nad
slabým	slabý	k2eAgNnSc7d1	slabé
a	a	k8xC	a
současně	současně	k6eAd1	současně
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
za	za	k7c4	za
přirozený	přirozený	k2eAgInSc4d1	přirozený
teorie	teorie	k1gFnSc2	teorie
právní	právní	k2eAgInPc1d1	právní
-	-	kIx~	-
vidí	vidět	k5eAaImIp3nP	vidět
stát	stát	k5eAaImF	stát
jako	jako	k8xS	jako
právní	právní	k2eAgInSc1d1	právní
výtvor	výtvor	k1gInSc1	výtvor
patriarchální	patriarchální	k2eAgFnSc2d1	patriarchální
teorie	teorie	k1gFnSc2	teorie
-	-	kIx~	-
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
vznik	vznik	k1gInSc4	vznik
státu	stát	k1gInSc2	stát
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
jako	jako	k8xS	jako
přirozeně	přirozeně	k6eAd1	přirozeně
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
forem	forma	k1gFnPc2	forma
společnosti	společnost	k1gFnSc2	společnost
patrimoniální	patrimoniální	k2eAgFnSc2d1	patrimoniální
teorie	teorie	k1gFnSc2	teorie
-	-	kIx~	-
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g />
.	.	kIx.	.
</s>
<s>
vládní	vládní	k2eAgFnSc4d1	vládní
moc	moc	k1gFnSc4	moc
jako	jako	k8xC	jako
moc	moc	k1gFnSc4	moc
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
pozemkovém	pozemkový	k2eAgNnSc6d1	pozemkové
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
smluvní	smluvní	k2eAgFnSc2d1	smluvní
teorie	teorie	k1gFnSc2	teorie
-	-	kIx~	-
dle	dle	k7c2	dle
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
smlouva	smlouva	k1gFnSc1	smlouva
právním	právní	k2eAgInSc7d1	právní
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
státu	stát	k1gInSc2	stát
teorie	teorie	k1gFnSc2	teorie
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
-	-	kIx~	-
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
dualismu	dualismus	k1gInSc2	dualismus
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gNnSc4	jejich
ztotožnění	ztotožnění	k1gNnSc4	ztotožnění
teorie	teorie	k1gFnSc1	teorie
etická	etický	k2eAgFnSc1d1	etická
-	-	kIx~	-
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
mravně	mravně	k6eAd1	mravně
nutný	nutný	k2eAgInSc1d1	nutný
teorie	teorie	k1gFnSc1	teorie
psychologická	psychologický	k2eAgFnSc1d1	psychologická
-	-	kIx~	-
lze	lze	k6eAd1	lze
sem	sem	k6eAd1	sem
zařadit	zařadit	k5eAaPmF	zařadit
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
stát	stát	k5eAaImF	stát
za	za	k7c4	za
produkt	produkt	k1gInSc4	produkt
ducha	duch	k1gMnSc2	duch
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
výtvor	výtvor	k1gInSc1	výtvor
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
dějinný	dějinný	k2eAgInSc1d1	dějinný
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgFnPc1d1	psychická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
či	či	k8xC	či
biopsychické	biopsychický	k2eAgInPc1d1	biopsychický
instinkty	instinkt	k1gInPc1	instinkt
a	a	k8xC	a
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
ho	on	k3xPp3gInSc4	on
zdůvodňují	zdůvodňovat	k5eAaImIp3nP	zdůvodňovat
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
trvalý	trvalý	k2eAgInSc4d1	trvalý
organický	organický	k2eAgInSc4d1	organický
svazek	svazek	k1gInSc4	svazek
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
jisté	jistý	k2eAgNnSc1d1	jisté
teritorium	teritorium	k1gNnSc1	teritorium
obývajícího	obývající	k2eAgMnSc4d1	obývající
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
vůlí	vůle	k1gFnSc7	vůle
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
mocí	moc	k1gFnSc7	moc
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
jest	být	k5eAaImIp3nS	být
napomáhati	napomáhat	k5eAaBmF	napomáhat
dosažení	dosažení	k1gNnSc4	dosažení
všech	všecek	k3xTgInPc2	všecek
životních	životní	k2eAgInPc2d1	životní
úkolů	úkol	k1gInPc2	úkol
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
spojeného	spojený	k2eAgNnSc2d1	spojené
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
státu	stát	k1gInSc3	stát
jako	jako	k8xC	jako
takovému	takový	k3xDgNnSc3	takový
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
anarchisté	anarchista	k1gMnPc1	anarchista
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nemorální	morální	k2eNgInSc4d1	nemorální
<g/>
,	,	kIx,	,
nepotřebný	potřebný	k2eNgInSc4d1	nepotřebný
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
škodlivý	škodlivý	k2eAgInSc1d1	škodlivý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
nahrazení	nahrazení	k1gNnSc4	nahrazení
státu	stát	k1gInSc2	stát
jiným	jiný	k2eAgNnPc3d1	jiné
společenským	společenský	k2eAgNnPc3d1	společenské
uspořádáním	uspořádání	k1gNnPc3	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Anarchismus	anarchismus	k1gInSc1	anarchismus
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
vícero	vícero	k1gNnSc4	vícero
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusejí	muset	k5eNaImIp3nP	muset
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
navzájem	navzájem	k6eAd1	navzájem
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgMnPc1d1	sociální
anarchisté	anarchista	k1gMnPc1	anarchista
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
stát	stát	k5eAaPmF	stát
nahradit	nahradit	k5eAaPmF	nahradit
autonomními	autonomní	k2eAgFnPc7d1	autonomní
komunitami	komunita	k1gFnPc7	komunita
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc7	jejich
obdobou	obdoba	k1gFnSc7	obdoba
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
individualističtí	individualistický	k2eAgMnPc1d1	individualistický
anarchisté	anarchista	k1gMnPc1	anarchista
dávají	dávat	k5eAaImIp3nP	dávat
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgMnSc7	jaký
se	se	k3xPyFc4	se
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
bude	být	k5eAaImBp3nS	být
sdružovat	sdružovat	k5eAaImF	sdružovat
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
nechávají	nechávat	k5eAaImIp3nP	nechávat
na	na	k7c4	na
něm.	něm.	k?	něm.
Ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
anarchokapitalista	anarchokapitalista	k1gMnSc1	anarchokapitalista
Murray	Murraa	k1gFnSc2	Murraa
Rothbard	Rothbard	k1gMnSc1	Rothbard
považoval	považovat	k5eAaImAgMnS	považovat
stát	stát	k5eAaPmF	stát
za	za	k7c4	za
nemorální	morální	k2eNgNnSc4d1	nemorální
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prostředky	prostředek	k1gInPc1	prostředek
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
činnosti	činnost	k1gFnSc3	činnost
<g />
.	.	kIx.	.
</s>
<s>
získává	získávat	k5eAaImIp3nS	získávat
stát	stát	k5eAaImF	stát
daněmi	daň	k1gFnPc7	daň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lidé	člověk	k1gMnPc1	člověk
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
platí	platit	k5eAaImIp3nS	platit
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
přisvojuje	přisvojovat	k5eAaImIp3nS	přisvojovat
pravomoci	pravomoc	k1gFnPc4	pravomoc
vyšší	vysoký	k2eAgFnPc4d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
stát	stát	k1gInSc1	stát
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemohli	moct	k5eNaImAgMnP	moct
tedy	tedy	k9	tedy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
tato	tento	k3xDgFnSc1	tento
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
delegovat	delegovat	k5eAaBmF	delegovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
právo	práv	k2eAgNnSc1d1	právo
vymáhat	vymáhat	k5eAaImF	vymáhat
daně	daň	k1gFnPc4	daň
po	po	k7c6	po
jiných	jiný	k2eAgFnPc6d1	jiná
osobách	osoba	k1gFnPc6	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
Apatrida	Apatrid	k1gMnSc2	Apatrid
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
bez	bez	k7c2	bez
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
Ministát	Ministát	k1gInSc4	Ministát
Ohrožení	ohrožení	k1gNnSc2	ohrožení
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
<g/>
,	,	kIx,	,
Revoluce	revoluce	k1gFnSc1	revoluce
Ideologie	ideologie	k1gFnPc1	ideologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
nebo	nebo	k8xC	nebo
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
existenci	existence	k1gFnSc4	existence
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
Anarchismus	anarchismus	k1gInSc1	anarchismus
Sociální	sociální	k2eAgInSc1d1	sociální
anarchismus	anarchismus	k1gInSc1	anarchismus
Anarchoindividualismus	Anarchoindividualismus	k1gInSc4	Anarchoindividualismus
SCHELLE	SCHELLE	kA	SCHELLE
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
TAUCHEN	TAUCHEN	kA	TAUCHEN
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
ADAMOVÁ	Adamová	k1gFnSc1	Adamová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
LOJEK	Lojek	k1gMnSc1	Lojek
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Tematická	tematický	k2eAgFnSc1d1	tematická
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
652	[number]	k4	652
s.	s.	k?	s.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
652	[number]	k4	652
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
SCHELLE	SCHELLE	kA	SCHELLE
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
TAUCHEN	TAUCHEN	kA	TAUCHEN
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Vývoj	vývoj	k1gInSc1	vývoj
konstitucionalismu	konstitucionalismus	k1gInSc2	konstitucionalismus
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Linde	Lind	k1gMnSc5	Lind
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
1300	[number]	k4	1300
s.	s.	k?	s.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7201	[number]	k4	7201
<g/>
-	-	kIx~	-
<g/>
922	[number]	k4	922
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
SCHELLE	SCHELLE	kA	SCHELLE
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
TAUCHEN	TAUCHEN	kA	TAUCHEN
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Vývoj	vývoj	k1gInSc1	vývoj
konstitucionalismu	konstitucionalismus	k1gInSc2	konstitucionalismus
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Linde	Lind	k1gMnSc5	Lind
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
1476	[number]	k4	1476
s.	s.	k?	s.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7201	[number]	k4	7201
<g/>
-	-	kIx~	-
<g/>
926	[number]	k4	926
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stát	stát	k5eAaPmF	stát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Státy	stát	k1gInPc1	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
představitelé	představitel	k1gMnPc1	představitel
–	–	k?	–
encyklopedická	encyklopedický	k2eAgFnSc1d1	encyklopedická
databáze	databáze	k1gFnSc1	databáze
na	na	k7c6	na
serveru	server	k1gInSc6	server
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Libri	Libr	k1gFnSc2	Libr
</s>
