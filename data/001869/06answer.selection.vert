<s>
Americká	americký	k2eAgFnSc1d1	americká
Dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
eugeniku	eugenika	k1gFnSc4	eugenika
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
laboratoř	laboratoř	k1gFnSc4	laboratoř
Cold	Colda	k1gFnPc2	Colda
Spring	Spring	k1gInSc1	Spring
Harbor	Harbor	k1gMnSc1	Harbor
a	a	k8xC	a
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
Dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
eugeniku	eugenika	k1gFnSc4	eugenika
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Galtonovu	Galtonův	k2eAgFnSc4d1	Galtonova
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
