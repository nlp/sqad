<p>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Václava	Václava	k1gFnSc1	Václava
Křístka	Křístka	k1gFnSc1	Křístka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
natočená	natočený	k2eAgFnSc1d1	natočená
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
pověstí	pověst	k1gFnPc2	pověst
o	o	k7c6	o
císaři	císař	k1gMnSc3	císař
Josefovi	Josefův	k2eAgMnPc1d1	Josefův
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c4	za
tovaryše	tovaryš	k1gMnSc4	tovaryš
potuloval	potulovat	k5eAaImAgMnS	potulovat
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
poddanými	poddaný	k1gMnPc7	poddaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Tvůrci	tvůrce	k1gMnPc1	tvůrce
==	==	k?	==
</s>
</p>
<p>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Křístek	Křístka	k1gFnPc2	Křístka
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
pověstí	pověst	k1gFnPc2	pověst
o	o	k7c6	o
císaři	císař	k1gMnSc6	císař
Josefu	Josefa	k1gFnSc4	Josefa
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Křístek	Křístka	k1gFnPc2	Křístka
</s>
</p>
<p>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hrubý	Hrubý	k1gMnSc1	Hrubý
</s>
</p>
<p>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Polášek	Polášek	k1gMnSc1	Polášek
</s>
</p>
<p>
<s>
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Duba	Duba	k1gMnSc1	Duba
</s>
</p>
<p>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Křístek	Křístka	k1gFnPc2	Křístka
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
,	,	kIx,	,
76	[number]	k4	76
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
pohádka	pohádka	k1gFnSc1	pohádka
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaPmIp3nS	vydávat
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
mezi	mezi	k7c4	mezi
poddaný	poddaný	k2eAgInSc4d1	poddaný
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
četných	četný	k2eAgFnPc2d1	četná
vycházek	vycházka	k1gFnPc2	vycházka
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
císařským	císařský	k2eAgInSc7d1	císařský
tamborem	tambor	k1gInSc7	tambor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
zažívají	zažívat	k5eAaImIp3nP	zažívat
všelijaká	všelijaký	k3yIgNnPc1	všelijaký
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Potkají	potkat	k5eAaPmIp3nP	potkat
lupiče	lupič	k1gMnSc4	lupič
<g/>
,	,	kIx,	,
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
hloupost	hloupost	k1gFnSc4	hloupost
a	a	k8xC	a
setkají	setkat	k5eAaPmIp3nP	setkat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
věrolomností	věrolomnost	k1gFnSc7	věrolomnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	on	k3xPp3gFnPc4	on
téměř	téměř	k6eAd1	téměř
přivede	přivést	k5eAaPmIp3nS	přivést
až	až	k9	až
před	před	k7c4	před
kata	kat	k1gMnSc4	kat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepříliš	příliš	k6eNd1	příliš
růžové	růžový	k2eAgFnPc1d1	růžová
vyhlídky	vyhlídka	k1gFnPc1	vyhlídka
na	na	k7c4	na
budoucnost	budoucnost	k1gFnSc4	budoucnost
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
obrátí	obrátit	k5eAaPmIp3nS	obrátit
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
na	na	k7c6	na
FDb	FDb	k1gFnSc6	FDb
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
na	na	k7c4	na
ČT	ČT	kA	ČT
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
