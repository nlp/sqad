<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
tambor	tambor	k1gMnSc1	tambor
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Václava	Václava	k1gFnSc1	Václava
Křístka	Křístka	k1gFnSc1	Křístka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
natočená	natočený	k2eAgFnSc1d1	natočená
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
pověstí	pověst	k1gFnPc2	pověst
o	o	k7c6	o
císaři	císař	k1gMnSc3	císař
Josefovi	Josefův	k2eAgMnPc1d1	Josefův
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c4	za
tovaryše	tovaryš	k1gMnSc4	tovaryš
potuloval	potulovat	k5eAaImAgMnS	potulovat
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
poddanými	poddaný	k1gMnPc7	poddaný
<g/>
.	.	kIx.	.
</s>
