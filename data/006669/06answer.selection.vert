<s>
Zámek	zámek	k1gInSc1	zámek
Hluboká	Hluboká	k1gFnSc1	Hluboká
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Frauenberg	Frauenberg	k1gInSc1	Frauenberg
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
asi	asi	k9	asi
15	[number]	k4	15
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
