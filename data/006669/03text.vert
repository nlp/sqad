<s>
Zámek	zámek	k1gInSc1	zámek
Hluboká	Hluboká	k1gFnSc1	Hluboká
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Frauenberg	Frauenberg	k1gInSc1	Frauenberg
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
asi	asi	k9	asi
15	[number]	k4	15
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
veřejnosti	veřejnost	k1gFnPc4	veřejnost
přístupný	přístupný	k2eAgMnSc1d1	přístupný
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
turisticky	turisticky	k6eAd1	turisticky
nejatraktivnějším	atraktivní	k2eAgFnPc3d3	nejatraktivnější
památkám	památka	k1gFnPc3	památka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spravován	spravovat	k5eAaImNgInS	spravovat
Národním	národní	k2eAgInSc7d1	národní
památkovým	památkový	k2eAgInSc7d1	památkový
ústavem	ústav	k1gInSc7	ústav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
zámecké	zámecký	k2eAgFnSc6d1	zámecká
jízdárně	jízdárna	k1gFnSc6	jízdárna
sídlí	sídlet	k5eAaImIp3nS	sídlet
Alšova	Alšův	k2eAgFnSc1d1	Alšova
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
sbírku	sbírka	k1gFnSc4	sbírka
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
holandských	holandský	k2eAgMnPc2d1	holandský
a	a	k8xC	a
vlámských	vlámský	k2eAgMnPc2d1	vlámský
malířů	malíř	k1gMnPc2	malíř
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
raně	raně	k6eAd1	raně
gotické	gotický	k2eAgFnSc2d1	gotická
tvrze	tvrz	k1gFnSc2	tvrz
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
gotický	gotický	k2eAgInSc1d1	gotický
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
koncem	konec	k1gInSc7	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Vilém	Viléma	k1gFnPc2	Viléma
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
prodal	prodat	k5eAaPmAgMnS	prodat
roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
přešla	přejít	k5eAaPmAgFnS	přejít
Hluboká	Hluboká	k1gFnSc1	Hluboká
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
přestavili	přestavit	k5eAaPmAgMnP	přestavit
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
panství	panství	k1gNnSc4	panství
Hluboké	Hluboká	k1gFnSc2	Hluboká
Jan	Jan	k1gMnSc1	Jan
Adolf	Adolf	k1gMnSc1	Adolf
I.	I.	kA	I.
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Schwarzenberků	Schwarzenberka	k1gMnPc2	Schwarzenberka
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
sídlila	sídlit	k5eAaImAgFnS	sídlit
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgNnPc4	tři
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
knížete	kníže	k1gMnSc2	kníže
Adama	Adam	k1gMnSc2	Adam
Františka	František	k1gMnSc2	František
byl	být	k5eAaImAgMnS	být
zámek	zámek	k1gInSc4	zámek
počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přestavěn	přestavět	k5eAaPmNgInS	přestavět
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
stavitele	stavitel	k1gMnSc2	stavitel
Pavla	Pavel	k1gMnSc2	Pavel
Ignáce	Ignác	k1gMnSc2	Ignác
Bayera	Bayer	k1gMnSc2	Bayer
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
následovníka	následovník	k1gMnSc2	následovník
Antonína	Antonín	k1gMnSc2	Antonín
Erharda	Erhard	k1gMnSc2	Erhard
Martinelliho	Martinelli	k1gMnSc2	Martinelli
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
kníže	kníže	k1gMnSc1	kníže
Jan	Jan	k1gMnSc1	Jan
Adolf	Adolf	k1gMnSc1	Adolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Hlubokou	Hluboká	k1gFnSc4	Hluboká
od	od	k7c2	od
základu	základ	k1gInSc2	základ
strhnout	strhnout	k5eAaPmF	strhnout
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
nový	nový	k2eAgInSc4d1	nový
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
architekt	architekt	k1gMnSc1	architekt
Franz	Franz	k1gMnSc1	Franz
Beer	Beer	k1gMnSc1	Beer
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
také	také	k6eAd1	také
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
po	po	k7c4	po
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
sám	sám	k3xTgMnSc1	sám
vedl	vést	k5eAaImAgInS	vést
stavební	stavební	k2eAgFnPc4d1	stavební
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
tudorovské	tudorovský	k2eAgFnSc2d1	Tudorovská
gotiky	gotika	k1gFnSc2	gotika
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
anglického	anglický	k2eAgInSc2d1	anglický
královského	královský	k2eAgInSc2d1	královský
zámku	zámek	k1gInSc2	zámek
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
</s>
<s>
Náročné	náročný	k2eAgInPc1d1	náročný
exteriéry	exteriér	k1gInPc1	exteriér
a	a	k8xC	a
interiéry	interiér	k1gInPc1	interiér
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Damasia	Damasius	k1gMnSc2	Damasius
Deworezkého	Deworezký	k1gMnSc2	Deworezký
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
anglickým	anglický	k2eAgInSc7d1	anglický
parkem	park	k1gInSc7	park
se	s	k7c7	s
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
a	a	k8xC	a
stropy	strop	k1gInPc1	strop
interiérů	interiér	k1gInPc2	interiér
jsou	být	k5eAaImIp3nP	být
bohatě	bohatě	k6eAd1	bohatě
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
dřevořezbami	dřevořezba	k1gFnPc7	dřevořezba
a	a	k8xC	a
obloženy	obložit	k5eAaPmNgFnP	obložit
ušlechtilým	ušlechtilý	k2eAgNnSc7d1	ušlechtilé
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgInSc1d3	nejcennější
nábytek	nábytek	k1gInSc1	nábytek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Ranním	ranní	k2eAgInSc6d1	ranní
salónu	salón	k1gInSc6	salón
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
místnosti	místnost	k1gFnPc1	místnost
jsou	být	k5eAaImIp3nP	být
zdobeny	zdoben	k2eAgInPc4d1	zdoben
obrazy	obraz	k1gInPc4	obraz
evropských	evropský	k2eAgMnPc2d1	evropský
mistrů	mistr	k1gMnPc2	mistr
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
lustry	lustr	k1gInPc7	lustr
<g/>
,	,	kIx,	,
vitrážemi	vitráž	k1gFnPc7	vitráž
a	a	k8xC	a
keramikou	keramika	k1gFnSc7	keramika
z	z	k7c2	z
Delft	Delft	k1gInSc4	Delft
–	–	k?	–
především	především	k9	především
ložnice	ložnice	k1gFnSc1	ložnice
a	a	k8xC	a
šatna	šatna	k1gFnSc1	šatna
kněžny	kněžna	k1gFnSc2	kněžna
Eleonory	Eleonora	k1gFnSc2	Eleonora
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Hamiltonův	Hamiltonův	k2eAgInSc1d1	Hamiltonův
Kabinet	kabinet	k1gInSc1	kabinet
a	a	k8xC	a
čítárna	čítárna	k1gFnSc1	čítárna
<g/>
.	.	kIx.	.
</s>
<s>
Portréty	portrét	k1gInPc1	portrét
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
členy	člen	k1gMnPc4	člen
rodu	rod	k1gInSc2	rod
Schwarzenberků	Schwarzenberka	k1gMnPc2	Schwarzenberka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
místností	místnost	k1gFnSc7	místnost
je	být	k5eAaImIp3nS	být
sál	sál	k1gInSc1	sál
knihovny	knihovna	k1gFnSc2	knihovna
s	s	k7c7	s
kazetovým	kazetový	k2eAgInSc7d1	kazetový
stropem	strop	k1gInSc7	strop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
přenesen	přenést	k5eAaPmNgInS	přenést
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
Schwarzenberg	Schwarzenberg	k1gInSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníky	návštěvník	k1gMnPc4	návštěvník
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
i	i	k9	i
zbrojnice	zbrojnice	k1gFnPc4	zbrojnice
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novogotické	novogotický	k2eAgFnSc6d1	novogotická
kapli	kaple	k1gFnSc6	kaple
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
velkou	velká	k1gFnSc7	velká
pozdně	pozdně	k6eAd1	pozdně
gotickou	gotický	k2eAgFnSc7d1	gotická
archou	archa	k1gFnSc7	archa
<g/>
.	.	kIx.	.
</s>
