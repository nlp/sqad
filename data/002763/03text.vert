<s>
K2	K2	k4	K2
(	(	kIx(	(
<g/>
baltsky	baltsky	k6eAd1	baltsky
Čhogori	Čhogori	k1gNnSc1	Čhogori
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Čchokori	Čchokor	k1gMnPc1	Čchokor
<g/>
,	,	kIx,	,
urdsky	urdsky	k6eAd1	urdsky
ک	ک	k?	ک
ٹ	ٹ	k?	ٹ
<g/>
,	,	kIx,	,
transliterováno	transliterován	k2eAgNnSc4d1	transliterován
Ke	k	k7c3	k
tū	tū	k?	tū
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Lambá	Lambý	k2eAgFnSc1d1	Lambý
Pahár	Pahár	k1gInSc1	Pahár
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
乔	乔	k?	乔
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc4d1	oficiální
přepis	přepis	k1gInSc4	přepis
Qogir	Qogir	k1gMnSc1	Qogir
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gMnSc1	pinyin
Qiáogē	Qiáogē	k1gMnSc1	Qiáogē
Fē	Fē	k1gMnSc1	Fē
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Čchiao-ke	Čchiao	k1gFnSc2	Čchiao-k
<g/>
-li	i	k?	-li
feng	feng	k1gInSc1	feng
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
pod	pod	k7c7	pod
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
Mount	Mount	k1gInSc1	Mount
Godwin-Austen	Godwin-Austen	k2eAgInSc1d1	Godwin-Austen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Karákóram	Karákóram	k1gInSc1	Karákóram
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
pákistánskou	pákistánský	k2eAgFnSc7d1	pákistánská
částí	část	k1gFnSc7	část
Kašmíru	Kašmír	k1gInSc2	Kašmír
a	a	k8xC	a
čínskou	čínský	k2eAgFnSc7d1	čínská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
Sin-ťiang	Sin-ťianga	k1gFnPc2	Sin-ťianga
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
8611	[number]	k4	8611
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nezdá	zdát	k5eNaImIp3nS	zdát
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
Čhogori	Čhogor	k1gFnSc2	Čhogor
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
čínské	čínský	k2eAgFnPc1d1	čínská
Qogir	Qogir	k1gInSc4	Qogir
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
odvozené	odvozený	k2eAgInPc1d1	odvozený
tvary	tvar	k1gInPc1	tvar
<g/>
)	)	kIx)	)
dali	dát	k5eAaPmAgMnP	dát
hoře	hoře	k6eAd1	hoře
západní	západní	k2eAgMnPc1d1	západní
cestovatelé	cestovatel	k1gMnPc1	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ho	on	k3xPp3gInSc4	on
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
slov	slovo	k1gNnPc2	slovo
místního	místní	k2eAgInSc2d1	místní
jazyka	jazyk	k1gInSc2	jazyk
balti	balti	k1gNnSc1	balti
<g/>
,	,	kIx,	,
čhogo	čhogo	k1gNnSc1	čhogo
=	=	kIx~	=
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
ri	ri	k?	ri
=	=	kIx~	=
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
více	hodně	k6eAd2	hodně
vžil	vžít	k5eAaPmAgInS	vžít
ještě	ještě	k9	ještě
umělejší	umělý	k2eAgInSc1d2	umělejší
název	název	k1gInSc1	název
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
přidělila	přidělit	k5eAaPmAgFnS	přidělit
této	tento	k3xDgFnSc3	tento
hoře	hora	k1gFnSc3	hora
i	i	k8xC	i
sousedním	sousední	k2eAgInPc3d1	sousední
vrcholkům	vrcholek	k1gInPc3	vrcholek
označení	označení	k1gNnSc2	označení
K1	K1	k1gFnSc2	K1
až	až	k8xS	až
K5	K5	k1gFnSc2	K5
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
2	[number]	k4	2
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
byly	být	k5eAaImAgFnP	být
přejmenovány	přejmenován	k2eAgFnPc1d1	přejmenována
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Expedici	expedice	k1gFnSc4	expedice
vedl	vést	k5eAaImAgMnS	vést
Henry	Henry	k1gMnSc1	Henry
Haversham	Haversham	k1gInSc4	Haversham
Godwin-Austen	Godwin-Austen	k2eAgInSc4d1	Godwin-Austen
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
K2	K2	k1gMnSc2	K2
dal	dát	k5eAaPmAgMnS	dát
hoře	hoře	k1gNnSc4	hoře
T.	T.	kA	T.
G.	G.	kA	G.
Montgomerie	Montgomerie	k1gFnSc1	Montgomerie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
veliteli	velitel	k1gMnSc6	velitel
expedice	expedice	k1gFnSc2	expedice
má	mít	k5eAaImIp3nS	mít
K2	K2	k1gFnSc1	K2
své	svůj	k3xOyFgNnSc4	svůj
další	další	k2eAgNnSc4d1	další
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
Mount	Mount	k1gInSc4	Mount
Godwin-Austen	Godwin-Austen	k2eAgInSc4d1	Godwin-Austen
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nevžilo	vžít	k5eNaPmAgNnS	vžít
<g/>
.	.	kIx.	.
</s>
<s>
Urdské	urdský	k2eAgNnSc1d1	urdský
Lambá	Lambý	k2eAgFnSc1d1	Lambý
Pahár	Pahár	k1gInSc4	Pahár
znamená	znamenat	k5eAaImIp3nS	znamenat
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
urdštině	urdština	k1gFnSc6	urdština
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
pojmenování	pojmenování	k1gNnSc1	pojmenování
ک	ک	k?	ک
ٹ	ٹ	k?	ٹ
(	(	kIx(	(
<g/>
Ke	k	k7c3	k
tū	tū	k?	tū
je	být	k5eAaImIp3nS	být
urdský	urdský	k2eAgInSc1d1	urdský
fonetický	fonetický	k2eAgInSc1d1	fonetický
přepis	přepis	k1gInSc1	přepis
anglického	anglický	k2eAgMnSc2d1	anglický
K	k	k7c3	k
two	two	k?	two
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
česky	česky	k6eAd1	česky
Ká	ká	k0	ká
dvě	dva	k4xCgFnPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K2	K2	k4	K2
drží	držet	k5eAaImIp3nS	držet
jeden	jeden	k4xCgInSc1	jeden
statistický	statistický	k2eAgInSc1d1	statistický
primát	primát	k1gInSc1	primát
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
14	[number]	k4	14
osmitisícovkami	osmitisícovka	k1gFnPc7	osmitisícovka
-	-	kIx~	-
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvíce	hodně	k6eAd3	hodně
nezdařených	zdařený	k2eNgInPc2d1	nezdařený
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
K2	K2	k1gFnSc4	K2
o	o	k7c4	o
239	[number]	k4	239
metrů	metr	k1gInPc2	metr
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
Mount	Mount	k1gMnSc1	Mount
Everest	Everest	k1gInSc4	Everest
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
relativní	relativní	k2eAgNnSc4d1	relativní
převýšení	převýšení	k1gNnSc4	převýšení
od	od	k7c2	od
úpatí	úpatí	k1gNnSc2	úpatí
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc1	její
svahy	svah	k1gInPc1	svah
mnohem	mnohem	k6eAd1	mnohem
strmější	strmý	k2eAgMnSc1d2	strmější
a	a	k8xC	a
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
K2	K2	k1gFnSc4	K2
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
italské	italský	k2eAgFnSc2d1	italská
expedice	expedice	k1gFnSc2	expedice
<g/>
,	,	kIx,	,
Achille	Achilles	k1gMnSc5	Achilles
Compagnoni	Compagnon	k1gMnPc1	Compagnon
a	a	k8xC	a
Lino	Lina	k1gFnSc5	Lina
Lacedelli	Lacedelle	k1gFnSc3	Lacedelle
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgInSc4d1	další
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
pokus	pokus	k1gInSc4	pokus
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
plných	plný	k2eAgNnPc2d1	plné
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
pokořilo	pokořit	k5eAaPmAgNnS	pokořit
vrchol	vrchol	k1gInSc4	vrchol
sedm	sedm	k4xCc1	sedm
členů	člen	k1gMnPc2	člen
japonské	japonský	k2eAgFnSc2d1	japonská
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
K2	K2	k1gFnSc6	K2
byla	být	k5eAaImAgFnS	být
polská	polský	k2eAgFnSc1d1	polská
horolezkyně	horolezkyně	k1gFnSc1	horolezkyně
Wanda	Wanda	k1gFnSc1	Wanda
Rutkiewiczová	Rutkiewiczová	k1gFnSc1	Rutkiewiczová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
kyslíkového	kyslíkový	k2eAgInSc2d1	kyslíkový
přístroje	přístroj	k1gInSc2	přístroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
zdolalo	zdolat	k5eAaPmAgNnS	zdolat
vrchol	vrchol	k1gInSc4	vrchol
K2	K2	k1gMnPc2	K2
305	[number]	k4	305
horolezců	horolezec	k1gMnPc2	horolezec
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
11	[number]	k4	11
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
76	[number]	k4	76
lezců	lezec	k1gMnPc2	lezec
zde	zde	k6eAd1	zde
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
(	(	kIx(	(
<g/>
35	[number]	k4	35
na	na	k7c6	na
sestupu	sestup	k1gInSc6	sestup
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
K2	K2	k1gFnPc4	K2
si	se	k3xPyFc3	se
hora	hora	k1gFnSc1	hora
vybrala	vybrat	k5eAaPmAgFnS	vybrat
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
během	během	k7c2	během
série	série	k1gFnSc2	série
nehod	nehoda	k1gFnPc2	nehoda
při	při	k7c6	při
jediném	jediný	k2eAgInSc6d1	jediný
výstupu	výstup	k1gInSc6	výstup
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
11	[number]	k4	11
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zdolal	zdolat	k5eAaPmAgMnS	zdolat
K2	K2	k1gFnSc4	K2
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
horolezec	horolezec	k1gMnSc1	horolezec
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
a	a	k8xC	a
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
doposud	doposud	k6eAd1	doposud
podařilo	podařit	k5eAaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
španělskému	španělský	k2eAgMnSc3d1	španělský
horolezci	horolezec	k1gMnSc3	horolezec
Juanitovi	Juanita	k1gMnSc3	Juanita
Oiarzabalovi	Oiarzabal	k1gMnSc3	Oiarzabal
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šerpovi	Šerp	k1gMnSc6	Šerp
Jangbu	Jangb	k1gMnSc6	Jangb
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
a	a	k8xC	a
2001	[number]	k4	2001
s	s	k7c7	s
kyslíkovým	kyslíkový	k2eAgInSc7d1	kyslíkový
přístrojem	přístroj	k1gInSc7	přístroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
se	s	k7c7	s
spolulezcem	spolulezec	k1gMnSc7	spolulezec
Agostinem	Agostin	k1gMnSc7	Agostin
da	da	k?	da
Polenzou	Polenza	k1gFnSc7	Polenza
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
přežili	přežít	k5eAaPmAgMnP	přežít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bivak	bivak	k1gInSc4	bivak
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnSc2	hora
museli	muset	k5eAaImAgMnP	muset
přenocovat	přenocovat	k5eAaPmF	přenocovat
jen	jen	k9	jen
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgInPc4d1	jediný
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
neúspěšné	úspěšný	k2eNgInPc4d1	neúspěšný
<g/>
)	)	kIx)	)
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
K2	K2	k1gFnSc4	K2
provedli	provést	k5eAaPmAgMnP	provést
Poláci	Polák	k1gMnPc1	Polák
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
<g/>
/	/	kIx~	/
<g/>
1988	[number]	k4	1988
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
K2	K2	k1gFnSc1	K2
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnohem	mnohem	k6eAd1	mnohem
blíže	blízce	k6eAd2	blízce
centrální	centrální	k2eAgFnSc4d1	centrální
Asii	Asie	k1gFnSc4	Asie
než	než	k8xS	než
Everest	Everest	k1gInSc4	Everest
<g/>
,	,	kIx,	,
zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
daleko	daleko	k6eAd1	daleko
studenější	studený	k2eAgMnSc1d2	studenější
a	a	k8xC	a
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
K2	K2	k1gFnSc6	K2
dnes	dnes	k6eAd1	dnes
vede	vést	k5eAaImIp3nS	vést
10	[number]	k4	10
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
nebyla	být	k5eNaImAgFnS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
obtížnost	obtížnost	k1gFnSc1	obtížnost
opakována	opakován	k2eAgFnSc1d1	opakována
<g/>
.	.	kIx.	.
1954	[number]	k4	1954
-	-	kIx~	-
Abruzziho	Abruzzi	k1gMnSc4	Abruzzi
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
a	a	k8xC	a
nejfrekventovanější	frekventovaný	k2eAgInSc1d3	nejfrekventovanější
výstup	výstup	k1gInSc1	výstup
na	na	k7c6	na
K2	K2	k1gFnSc6	K2
vede	vést	k5eAaImIp3nS	vést
JV	JV	kA	JV
pilířem	pilíř	k1gInSc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
Luigi	Luig	k1gMnSc6	Luig
Amadeovi	Amadeus	k1gMnSc6	Amadeus
vévodovi	vévoda	k1gMnSc6	vévoda
Abruzzském	Abruzzský	k2eAgMnSc6d1	Abruzzský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
(	(	kIx(	(
<g/>
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
Brochelerovými	Brochelerův	k2eAgMnPc7d1	Brochelerův
<g/>
)	)	kIx)	)
pokusil	pokusit	k5eAaPmAgMnS	pokusit
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
prostoupit	prostoupit	k5eAaPmF	prostoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tehdy	tehdy	k6eAd1	tehdy
asi	asi	k9	asi
6200	[number]	k4	6200
<g/>
m.	m.	k?	m.
Následovaly	následovat	k5eAaImAgFnP	následovat
tři	tři	k4xCgFnPc1	tři
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
americké	americký	k2eAgFnPc1d1	americká
výpravy	výprava	k1gFnPc1	výprava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
a	a	k8xC	a
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Reálnou	reálný	k2eAgFnSc4d1	reálná
šanci	šance	k1gFnSc4	šance
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
měl	mít	k5eAaImAgMnS	mít
Fritz	Fritz	k1gInSc4	Fritz
Wiessner	Wiessner	k1gMnSc1	Wiessner
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
čase	čas	k1gInSc6	čas
a	a	k8xC	a
za	za	k7c2	za
pěkného	pěkný	k2eAgNnSc2d1	pěkné
počasí	počasí	k1gNnSc2	počasí
8400	[number]	k4	8400
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
Šerpy	šerpa	k1gFnSc2	šerpa
Pasanga	Pasang	k1gMnSc2	Pasang
Dava-lamy	Davaam	k1gInPc4	Dava-lam
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výstup	výstup	k1gInSc1	výstup
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
Italům	Ital	k1gMnPc3	Ital
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
strmý	strmý	k2eAgInSc1d1	strmý
výstup	výstup	k1gInSc1	výstup
překonává	překonávat	k5eAaImIp3nS	překonávat
v	v	k7c4	v
7700	[number]	k4	7700
<g/>
m	m	kA	m
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
skalní	skalní	k2eAgFnSc4d1	skalní
stěnu	stěna	k1gFnSc4	stěna
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Černá	černat	k5eAaImIp3nS	černat
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
,	,	kIx,	,
výše	výše	k1gFnSc1	výše
v	v	k7c6	v
8200	[number]	k4	8200
<g/>
m	m	kA	m
navazuje	navazovat	k5eAaImIp3nS	navazovat
nejnebezpečnější	bezpečný	k2eNgFnSc4d3	nejnebezpečnější
část	část	k1gFnSc4	část
-	-	kIx~	-
sněhový	sněhový	k2eAgInSc4d1	sněhový
kuloár	kuloár	k1gInSc4	kuloár
Butylka	butylka	k1gFnSc1	butylka
pod	pod	k7c7	pod
převislou	převislý	k2eAgFnSc7d1	převislá
ledovou	ledový	k2eAgFnSc7d1	ledová
stěnou	stěna	k1gFnSc7	stěna
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
neštěstí	neštěstí	k1gNnSc3	neštěstí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
-	-	kIx~	-
SV	sv	kA	sv
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
hřebenem	hřeben	k1gInSc7	hřeben
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
vystoupit	vystoupit	k5eAaPmF	vystoupit
už	už	k6eAd1	už
úplně	úplně	k6eAd1	úplně
první	první	k4xOgFnSc1	první
expedice	expedice	k1gFnSc1	expedice
na	na	k7c6	na
K2	K2	k1gFnSc6	K2
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
tehdy	tehdy	k6eAd1	tehdy
6600	[number]	k4	6600
<g/>
m.	m.	k?	m.
Téměř	téměř	k6eAd1	téměř
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
polská	polský	k2eAgFnSc1d1	polská
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pronikla	proniknout	k5eAaPmAgFnS	proniknout
až	až	k9	až
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
pod	pod	k7c4	pod
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
jejich	jejich	k3xOp3gFnSc4	jejich
linii	linie	k1gFnSc4	linie
použili	použít	k5eAaPmAgMnP	použít
Američané	Američan	k1gMnPc1	Američan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
však	však	k9	však
v	v	k7c6	v
8000	[number]	k4	8000
<g/>
m	m	kA	m
přetraverzovali	přetraverzovat	k5eAaImAgMnP	přetraverzovat
V	v	k7c6	v
stěnou	stěna	k1gFnSc7	stěna
do	do	k7c2	do
klasické	klasický	k2eAgFnSc2d1	klasická
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Lou	Lou	k1gMnSc1	Lou
Reichardt	Reichardt	k1gMnSc1	Reichardt
a	a	k8xC	a
James	James	k1gMnSc1	James
Wickwire	Wickwir	k1gMnSc5	Wickwir
<g/>
,	,	kIx,	,
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
je	on	k3xPp3gFnPc4	on
následovali	následovat	k5eAaImAgMnP	následovat
John	John	k1gMnSc1	John
Roskelley	Roskellea	k1gFnSc2	Roskellea
a	a	k8xC	a
Rick	Rick	k1gMnSc1	Rick
Ridgeway	Ridgewaa	k1gFnSc2	Ridgewaa
(	(	kIx(	(
<g/>
Reichardt	Reichardt	k1gInSc1	Reichardt
a	a	k8xC	a
Roskelley	Roskelley	k1gInPc1	Roskelley
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
výstup	výstup	k1gInSc4	výstup
plný	plný	k2eAgInSc4d1	plný
sněhových	sněhový	k2eAgInPc2d1	sněhový
převisů	převis	k1gInPc2	převis
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
opakován	opakovat	k5eAaImNgInS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejsnazší	snadný	k2eAgFnSc4d3	nejsnadnější
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
-	-	kIx~	-
Z	Z	kA	Z
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Trasu	trasa	k1gFnSc4	trasa
Z	z	k7c2	z
hřebenem	hřeben	k1gInSc7	hřeben
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
zlézt	zlézt	k5eAaPmF	zlézt
Američané	Američan	k1gMnPc1	Američan
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Obtížný	obtížný	k2eAgInSc4d1	obtížný
skalní	skalní	k2eAgInSc4d1	skalní
hřeben	hřeben	k1gInSc4	hřeben
nakonec	nakonec	k6eAd1	nakonec
zlezla	zlézt	k5eAaPmAgFnS	zlézt
japonsko-pákistánská	japonskoákistánský	k2eAgFnSc1d1	japonsko-pákistánský
expedice	expedice	k1gFnSc1	expedice
(	(	kIx(	(
<g/>
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Japonec	Japonec	k1gMnSc1	Japonec
Eiho	Eiho	k1gMnSc1	Eiho
Ohtani	Ohtan	k1gMnPc1	Ohtan
a	a	k8xC	a
Pákistánec	Pákistánec	k1gMnSc1	Pákistánec
Nasir	Nasir	k1gMnSc1	Nasir
Sabir	Sabir	k1gMnSc1	Sabir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc4	výstup
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
malá	malý	k2eAgFnSc1d1	malá
americká	americký	k2eAgFnSc1d1	americká
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
Dan	Dan	k1gMnSc1	Dan
Mazur	Mazur	k1gMnSc1	Mazur
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Pratt	Pratt	k1gMnSc1	Pratt
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
znovu	znovu	k6eAd1	znovu
Japonci	Japonec	k1gMnPc1	Japonec
expedičním	expediční	k2eAgInSc7d1	expediční
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
-	-	kIx~	-
S	s	k7c7	s
pilíř	pilíř	k1gInSc1	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
získala	získat	k5eAaPmAgFnS	získat
povolení	povolení	k1gNnSc4	povolení
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
K2	K2	k1gFnSc6	K2
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
otevřela	otevřít	k5eAaPmAgFnS	otevřít
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
impozantním	impozantní	k2eAgMnSc6d1	impozantní
S	s	k7c7	s
pilířem	pilíř	k1gInSc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc1	sedm
horolezců	horolezec	k1gMnPc2	horolezec
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
byl	být	k5eAaImAgInS	být
zopakován	zopakován	k2eAgInSc4d1	zopakován
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
italskou	italský	k2eAgFnSc7d1	italská
expedicí	expedice	k1gFnSc7	expedice
(	(	kIx(	(
<g/>
a	a	k8xC	a
J.	J.	kA	J.
<g/>
Rakoncajem	Rakoncaj	k1gMnSc7	Rakoncaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ještě	ještě	k6eAd1	ještě
Američany	Američan	k1gMnPc7	Američan
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španěly	Španěly	k1gInPc1	Španěly
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poláky	Polák	k1gMnPc7	Polák
a	a	k8xC	a
Rusy	Rus	k1gMnPc7	Rus
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kazachy	Kazach	k1gMnPc7	Kazach
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
výjimku	výjimka	k1gFnSc4	výjimka
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
výstupy	výstup	k1gInPc4	výstup
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
-	-	kIx~	-
J	J	kA	J
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
Piotrowski	Piotrowsk	k1gFnSc2	Piotrowsk
a	a	k8xC	a
Jerzy	Jerza	k1gFnSc2	Jerza
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
zlezli	zlézt	k5eAaPmAgMnP	zlézt
velmi	velmi	k6eAd1	velmi
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
jižní	jižní	k2eAgFnSc4d1	jižní
stěnu	stěna	k1gFnSc4	stěna
K2	K2	k1gFnSc2	K2
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Piotrowski	Piotrowski	k6eAd1	Piotrowski
se	se	k3xPyFc4	se
zabil	zabít	k5eAaPmAgMnS	zabít
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
zopakován	zopakovat	k5eAaPmNgInS	zopakovat
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
-	-	kIx~	-
Magic	Magice	k1gFnPc2	Magice
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Magickou	magický	k2eAgFnSc7d1	magická
linií	linie	k1gFnSc7	linie
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
jihozápadní	jihozápadní	k2eAgInSc1d1	jihozápadní
hřeben	hřeben	k1gInSc1	hřeben
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zdařilý	zdařilý	k2eAgInSc1d1	zdařilý
výstup	výstup	k1gInSc1	výstup
provedli	provést	k5eAaPmAgMnP	provést
opět	opět	k6eAd1	opět
Poláci	Polák	k1gMnPc1	Polák
Przemyslaw	Przemyslaw	k1gFnSc2	Przemyslaw
Piasecki	Piasecki	k1gNnSc1	Piasecki
<g/>
,	,	kIx,	,
Wojciech	Wojciech	k1gInSc1	Wojciech
Wroz	Wroz	k1gMnSc1	Wroz
a	a	k8xC	a
Slovák	Slovák	k1gMnSc1	Slovák
Peter	Peter	k1gMnSc1	Peter
Božík	Božík	k1gMnSc1	Božík
<g/>
.	.	kIx.	.
</s>
<s>
Wroz	Wroz	k1gMnSc1	Wroz
se	se	k3xPyFc4	se
zabil	zabít	k5eAaPmAgMnS	zabít
pádem	pád	k1gInSc7	pád
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
výstup	výstup	k1gInSc4	výstup
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Španěl	Španěl	k1gMnSc1	Španěl
Jordi	Jord	k1gMnPc1	Jord
Corominas	Corominas	k1gMnSc1	Corominas
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
výstupy	výstup	k1gInPc1	výstup
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
-	-	kIx~	-
Japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
otevřela	otevřít	k5eAaPmAgFnS	otevřít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
trasu	trasa	k1gFnSc4	trasa
napříč	napříč	k7c7	napříč
SZ	SZ	kA	SZ
stěnou	stěna	k1gFnSc7	stěna
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
horolezci	horolezec	k1gMnPc1	horolezec
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
se	se	k3xPyFc4	se
během	během	k7c2	během
výstupu	výstup	k1gInSc2	výstup
nečekaně	nečekaně	k6eAd1	nečekaně
"	"	kIx"	"
<g/>
srazili	srazit	k5eAaPmAgMnP	srazit
<g/>
"	"	kIx"	"
s	s	k7c7	s
Poláky	polák	k1gInPc7	polák
lezoucími	lezoucí	k2eAgInPc7d1	lezoucí
ilegálně	ilegálně	k6eAd1	ilegálně
do	do	k7c2	do
SZ	SZ	kA	SZ
stěny	stěna	k1gFnSc2	stěna
přes	přes	k7c4	přes
sedlo	sedlo	k1gNnSc4	sedlo
Savoia	Savoius	k1gMnSc2	Savoius
z	z	k7c2	z
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
poté	poté	k6eAd1	poté
výstup	výstup	k1gInSc4	výstup
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
-	-	kIx~	-
Francouzi	Francouz	k1gMnSc5	Francouz
Pierre	Pierr	k1gMnSc5	Pierr
Beghin	Beghina	k1gFnPc2	Beghina
a	a	k8xC	a
Christophe	Christophe	k1gNnSc7	Christophe
Profit	profit	k1gInSc1	profit
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
horolezeckou	horolezecký	k2eAgFnSc4d1	horolezecká
elitu	elita	k1gFnSc4	elita
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
přes	přes	k7c4	přes
sedlo	sedlo	k1gNnSc4	sedlo
Savoia	Savoium	k1gNnSc2	Savoium
do	do	k7c2	do
japonské	japonský	k2eAgFnSc2d1	japonská
trasy	trasa	k1gFnSc2	trasa
SZ	SZ	kA	SZ
stěnou	stěna	k1gFnSc7	stěna
a	a	k8xC	a
zlezli	zlézt	k5eAaPmAgMnP	zlézt
K2	K2	k1gFnSc4	K2
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byli	být	k5eAaImAgMnP	být
navíc	navíc	k6eAd1	navíc
jediní	jediný	k2eAgMnPc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
uspěl	uspět	k5eAaPmAgMnS	uspět
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
-	-	kIx~	-
Česenův	Česenův	k2eAgInSc4d1	Česenův
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pilíř	pilíř	k1gInSc4	pilíř
protínající	protínající	k2eAgFnSc4d1	protínající
jižní	jižní	k2eAgFnSc4d1	jižní
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
Angličan	Angličan	k1gMnSc1	Angličan
Doug	Doug	k1gMnSc1	Doug
Scott	Scott	k1gMnSc1	Scott
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
a	a	k8xC	a
Slovinec	Slovinec	k1gMnSc1	Slovinec
Tomo	Tomo	k6eAd1	Tomo
Česen	česno	k1gNnPc2	česno
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Česen	česno	k1gNnPc2	česno
prostoupil	prostoupit	k5eAaPmAgInS	prostoupit
celý	celý	k2eAgInSc1d1	celý
pilíř	pilíř	k1gInSc1	pilíř
do	do	k7c2	do
8100	[number]	k4	8100
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
výstup	výstup	k1gInSc4	výstup
dokončili	dokončit	k5eAaPmAgMnP	dokončit
až	až	k9	až
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
hory	hora	k1gFnSc2	hora
Španělé	Španěl	k1gMnPc1	Španěl
(	(	kIx(	(
<g/>
Baskové	Bask	k1gMnPc1	Bask
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tuto	tento	k3xDgFnSc4	tento
trasu	trasa	k1gFnSc4	trasa
zopakovalo	zopakovat	k5eAaPmAgNnS	zopakovat
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejbezpečnější	bezpečný	k2eAgFnSc4d3	nejbezpečnější
trasu	trasa	k1gFnSc4	trasa
na	na	k7c4	na
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
-	-	kIx~	-
Z	Z	kA	Z
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
ruských	ruský	k2eAgMnPc2d1	ruský
horolezců	horolezec	k1gMnPc2	horolezec
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
několikrát	několikrát	k6eAd1	několikrát
zkoušenou	zkoušený	k2eAgFnSc4d1	zkoušená
cestu	cesta	k1gFnSc4	cesta
západní	západní	k2eAgFnSc7d1	západní
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Expedičním	expediční	k2eAgInSc7d1	expediční
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
technicky	technicky	k6eAd1	technicky
nejobtížnější	obtížný	k2eAgFnSc4d3	nejobtížnější
trasu	trasa	k1gFnSc4	trasa
na	na	k7c4	na
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gFnSc4	Rakoncaj
-	-	kIx~	-
výstup	výstup	k1gInSc4	výstup
severním	severní	k2eAgInSc7d1	severní
pilířem	pilíř	k1gInSc7	pilíř
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1986	[number]	k4	1986
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
-	-	kIx~	-
Abruzziho	Abruzzi	k1gMnSc4	Abruzzi
hřeben	hřeben	k1gInSc4	hřeben
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
-	-	kIx~	-
Libor	Libor	k1gMnSc1	Libor
Uher	Uher	k1gMnSc1	Uher
-	-	kIx~	-
Česenův	Česenův	k2eAgInSc1d1	Česenův
pilíř	pilíř	k1gInSc1	pilíř
(	(	kIx(	(
<g/>
Baskická	baskický	k2eAgFnSc1d1	baskická
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
JJV	JJV	kA	JJV
pilíř	pilíř	k1gInSc1	pilíř
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
-	-	kIx~	-
Pavel	Pavel	k1gMnSc1	Pavel
Bém	Bém	k1gMnSc1	Bém
-	-	kIx~	-
Abruzziho	Abruzzi	k1gMnSc4	Abruzzi
hřeben	hřeben	k1gInSc4	hřeben
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
-	-	kIx~	-
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Trávníček	Trávníček	k1gMnSc1	Trávníček
-	-	kIx~	-
Abruzziho	Abruzzi	k1gMnSc2	Abruzzi
hřeben	hřeben	k1gInSc1	hřeben
</s>
