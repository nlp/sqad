<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
šelfu	šelf	k1gInSc6	šelf
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
však	však	k9	však
částí	část	k1gFnSc7	část
ponořeného	ponořený	k2eAgInSc2d1	ponořený
kontinentu	kontinent	k1gInSc2	kontinent
Zélandie	Zélandie	k1gFnSc2	Zélandie
<g/>
.	.	kIx.	.
</s>
