<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	Brdy	k1gInPc1
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2016	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
zrušeného	zrušený	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
a	a	k8xC
několika	několik	k4yIc2
stávajících	stávající	k2eAgInPc2d1
brdských	brdský	k2eAgInPc2d1
přírodních	přírodní	k2eAgInPc2d1
parků	park	k1gInPc2
<g/>
.	.	kIx.
</s>