<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
připomínek	připomínka	k1gFnPc2
některých	některý	k3yIgFnPc2
obcí	obec	k1gFnPc2
byla	být	k5eAaImAgFnS
rozloha	rozloha	k1gFnSc1
zredukována	zredukovat	k5eAaPmNgFnS
na	na	k7c4
345	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
260	#num#	k4
km²	km²	k?
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
převážně	převážně	k6eAd1
Jižní	jižní	k2eAgInPc4d1
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
CHKO	CHKO	kA
nahradila	nahradit	k5eAaPmAgFnS
dosavadní	dosavadní	k2eAgInSc4d1
dva	dva	k4xCgInPc4
přírodní	přírodní	k2eAgInPc4d1
parky	park	k1gInPc4
<g/>
:	:	kIx,
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Brdy	Brdy	k1gInPc7
na	na	k7c6
území	území	k1gNnSc6
Plzeňského	plzeňský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
částečně	částečně	k6eAd1
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Třemšín	Třemšín	k1gInSc4
na	na	k7c6
území	území	k1gNnSc6
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>