<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	Brdy	k1gInPc4
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuCHKO	infoboxuCHKO	k?
Brdy	Brdy	k1gInPc1
Dolejší	Dolejší	k2eAgInSc1d1
Padrťský	Padrťský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
v	v	k7c6
Brdech	Brdy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vpravo	vpravo	k6eAd1
v	v	k7c6
pozadí	pozadí	k1gNnSc6
vrch	vrch	k1gInSc1
Kočka	kočka	k1gFnSc1
(	(	kIx(
<g/>
789	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
345	#num#	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInPc5
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
a	a	k8xC
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
26,79	26,79	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
15,43	15,43	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
6018	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	Brdy	k1gInPc1
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2016	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
zrušeného	zrušený	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
a	a	k8xC
několika	několik	k4yIc2
stávajících	stávající	k2eAgInPc2d1
brdských	brdský	k2eAgInPc2d1
přírodních	přírodní	k2eAgInPc2d1
parků	park	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
přírodní	přírodní	k2eAgNnPc4d1
bohatství	bohatství	k1gNnPc4
s	s	k7c7
cennými	cenný	k2eAgInPc7d1
lesními	lesní	k2eAgInPc7d1
porosty	porost	k1gInPc7
<g/>
,	,	kIx,
loukami	louka	k1gFnPc7
<g/>
,	,	kIx,
mokřady	mokřad	k1gInPc7
<g/>
,	,	kIx,
vřesovišti	vřesoviště	k1gNnPc7
a	a	k8xC
desítkami	desítka	k1gFnPc7
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
návrhu	návrh	k1gInSc2
Agentury	agentura	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
podporovaného	podporovaný	k2eAgInSc2d1
Středočeským	středočeský	k2eAgInSc7d1
i	i	k9
Plzeňským	plzeňský	k2eAgInSc7d1
krajem	kraj	k1gInSc7
měla	mít	k5eAaImAgFnS
mít	mít	k5eAaImF
rozlohu	rozloha	k1gFnSc4
330	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Původní	původní	k2eAgInSc1d1
návrh	návrh	k1gInSc1
na	na	k7c6
vyhlášení	vyhlášení	k1gNnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
zahrnoval	zahrnovat	k5eAaImAgMnS
území	území	k1gNnSc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
380	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
připomínek	připomínka	k1gFnPc2
některých	některý	k3yIgFnPc2
obcí	obec	k1gFnPc2
byla	být	k5eAaImAgFnS
rozloha	rozloha	k1gFnSc1
zredukována	zredukovat	k5eAaPmNgFnS
na	na	k7c4
345	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
260	#num#	k4
km²	km²	k?
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
převážně	převážně	k6eAd1
Jižní	jižní	k2eAgInPc4d1
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
CHKO	CHKO	kA
nahradila	nahradit	k5eAaPmAgFnS
dosavadní	dosavadní	k2eAgInSc4d1
dva	dva	k4xCgInPc4
přírodní	přírodní	k2eAgInPc4d1
parky	park	k1gInPc4
<g/>
:	:	kIx,
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Brdy	Brdy	k1gInPc7
na	na	k7c6
území	území	k1gNnSc6
Plzeňského	plzeňský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
částečně	částečně	k6eAd1
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Třemšín	Třemšín	k1gInSc4
na	na	k7c6
území	území	k1gNnSc6
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
byla	být	k5eAaImAgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2016	#num#	k4
zřízena	zřízen	k2eAgFnSc1d1
nařízením	nařízení	k1gNnSc7
vlády	vláda	k1gFnSc2
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
záměru	záměr	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
Brd	Brdy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
přišla	přijít	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
poslanců	poslanec	k1gMnPc2
Sněmovny	sněmovna	k1gFnSc2
s	s	k7c7
návrhem	návrh	k1gInSc7
vyhlásit	vyhlásit	k5eAaPmF
v	v	k7c6
Brdech	Brdy	k1gInPc6
národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
nebyl	být	k5eNaImAgInS
přijat	přijmout	k5eAaPmNgInS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
vláda	vláda	k1gFnSc1
ČSR	ČSR	kA
naopak	naopak	k6eAd1
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c6
zřízení	zřízení	k1gNnSc6
dělostřelecké	dělostřelecký	k2eAgFnSc2d1
střelnice	střelnice	k1gFnSc2
v	v	k7c6
Brdech	Brdy	k1gInPc6
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgInS
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Brd	Brdy	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nebyla	být	k5eNaImAgFnS
součástí	součást	k1gFnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
,	,	kIx,
vyhlášeny	vyhlášen	k2eAgInPc1d1
přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
<g/>
:	:	kIx,
na	na	k7c4
části	část	k1gFnPc4
ležící	ležící	k2eAgFnPc4d1
v	v	k7c6
Plzeňském	plzeňský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
Přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Brdy	Brdy	k1gInPc7
a	a	k8xC
na	na	k7c4
části	část	k1gFnPc4
ležící	ležící	k2eAgFnPc4d1
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc1
Třemšín	Třemšína	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Obklopovaly	obklopovat	k5eAaImAgFnP
několik	několik	k4yIc4
maloplošných	maloplošný	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
např.	např.	kA
přírodní	přírodní	k2eAgFnSc4d1
rezervaci	rezervace	k1gFnSc4
Chynínské	Chynínský	k2eAgInPc4d1
buky	buk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
záměru	záměr	k1gInSc6
zrušit	zrušit	k5eAaPmF
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
a	a	k8xC
o	o	k7c6
případné	případný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
ochrany	ochrana	k1gFnSc2
přírodního	přírodní	k2eAgNnSc2d1
území	území	k1gNnSc2
se	se	k3xPyFc4
znovu	znovu	k6eAd1
začalo	začít	k5eAaPmAgNnS
diskutovat	diskutovat	k5eAaImF
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zrušením	zrušení	k1gNnSc7
vojenských	vojenský	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
Ralsko	Ralsko	k1gNnSc1
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
a	a	k8xC
Dobrá	dobrý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
začátkem	začátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresní	okresní	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
navrhl	navrhnout	k5eAaPmAgMnS
vyhlášení	vyhlášení	k1gNnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
odborná	odborný	k2eAgFnSc1d1
publikace	publikace	k1gFnSc1
Střední	střední	k2eAgFnSc1d1
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
doporučila	doporučit	k5eAaPmAgFnS
speciální	speciální	k2eAgFnSc4d1
formu	forma	k1gFnSc4
ochrany	ochrana	k1gFnSc2
brdské	brdský	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byly	být	k5eAaImAgFnP
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
plánem	plán	k1gInSc7
výstavby	výstavba	k1gFnSc2
radaru	radar	k1gInSc2
americké	americký	k2eAgFnSc2d1
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
zpřístupněny	zpřístupněn	k2eAgFnPc4d1
okrajové	okrajový	k2eAgFnPc4d1
části	část	k1gFnPc4
újezdu	újezd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
ministerstvo	ministerstvo	k1gNnSc4
obrany	obrana	k1gFnSc2
zpracovalo	zpracovat	k5eAaPmAgNnS
Bílou	bílý	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
o	o	k7c6
obraně	obrana	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
byla	být	k5eAaImAgFnS
navrženo	navržen	k2eAgNnSc4d1
úplné	úplný	k2eAgNnSc4d1
zrušení	zrušení	k1gNnSc4
jednoho	jeden	k4xCgInSc2
z	z	k7c2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ministerstvo	ministerstvo	k1gNnSc1
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
oznámilo	oznámit	k5eAaPmAgNnS
záměr	záměr	k1gInSc4
zrušit	zrušit	k5eAaPmF
některý	některý	k3yIgInSc4
z	z	k7c2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
,	,	kIx,
nejpravděpodobněji	pravděpodobně	k6eAd3
brdský	brdský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
a	a	k8xC
2012	#num#	k4
bylo	být	k5eAaImAgNnS
zvažováno	zvažován	k2eAgNnSc1d1
vyhlášení	vyhlášení	k1gNnSc1
přírodního	přírodní	k2eAgInSc2d1
parku	park	k1gInSc2
nebo	nebo	k8xC
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
převážila	převážit	k5eAaPmAgFnS
myšlenka	myšlenka	k1gFnSc1
CHKO	CHKO	kA
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
podporovalo	podporovat	k5eAaImAgNnS
i	i	k9
ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
mj.	mj.	kA
i	i	k8xC
na	na	k7c6
základě	základ	k1gInSc6
pozitivních	pozitivní	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
ze	z	k7c2
zrušeného	zrušený	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Dobrá	dobrý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
zadalo	zadat	k5eAaPmAgNnS
Agentuře	agentura	k1gFnSc3
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
vypracovat	vypracovat	k5eAaPmF
analýzu	analýza	k1gFnSc4
vhodného	vhodný	k2eAgInSc2d1
režimu	režim	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agentura	agentura	k1gFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
buď	buď	k8xC
zřídit	zřídit	k5eAaPmF
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
doplněný	doplněný	k2eAgInSc4d1
sítí	síť	k1gFnSc7
maloplošných	maloplošný	k2eAgNnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
vyhlásit	vyhlásit	k5eAaPmF
chráněnou	chráněný	k2eAgFnSc4d1
krajinnou	krajinný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
kromě	kromě	k7c2
újezdu	újezd	k1gInSc2
zahrnovala	zahrnovat	k5eAaImAgFnS
i	i	k9
území	území	k1gNnSc4
sousedních	sousední	k2eAgInPc2d1
přírodních	přírodní	k2eAgInPc2d1
parků	park	k1gInPc2
včetně	včetně	k7c2
PřP	PřP	k1gFnSc2
Radeč	Radeč	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Varianta	varianta	k1gFnSc1
CHKO	CHKO	kA
byla	být	k5eAaImAgFnS
preferovaná	preferovaný	k2eAgFnSc1d1
agenturou	agentura	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
hejtmany	hejtman	k1gMnPc7
Středočeského	středočeský	k2eAgNnSc2d1
a	a	k8xC
Plzeňského	plzeňské	k1gNnSc2
kraje	kraj	k1gInSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
občanským	občanský	k2eAgNnPc3d1
sdružením	sdružení	k1gNnPc3
BRDY	Brdy	k1gInPc4
–	–	k?
Res	Res	k1gFnSc2
publica	publicum	k1gNnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
2013	#num#	k4
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
proces	proces	k1gInSc1
vyhlašování	vyhlašování	k1gNnSc2
CHKO	CHKO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yRgNnSc7,k3yQgNnSc7
byl	být	k5eAaImAgInS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
zrušen	zrušen	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Brdy	Brdy	k1gInPc7
<g/>
,	,	kIx,
stanoveny	stanovit	k5eAaPmNgFnP
hranice	hranice	k1gFnPc1
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
a	a	k8xC
změněny	změněn	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
schválila	schválit	k5eAaPmAgFnS
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2014	#num#	k4
<g/>
,	,	kIx,
Senátem	senát	k1gInSc7
byl	být	k5eAaImAgInS
schválen	schválit	k5eAaPmNgInS
v	v	k7c6
lednu	leden	k1gInSc6
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ve	v	k7c6
Sbírce	sbírka	k1gFnSc6
zákonů	zákon	k1gInPc2
vyšel	vyjít	k5eAaPmAgInS
pod	pod	k7c7
č.	č.	k?
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
vláda	vláda	k1gFnSc1
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
schválila	schválit	k5eAaPmAgFnS
nařízení	nařízení	k1gNnSc4
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yQgMnPc3,k3yRgMnPc3
na	na	k7c6
území	území	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
vyhlásila	vyhlásit	k5eAaPmAgFnS
chráněnou	chráněný	k2eAgFnSc4d1
krajinnou	krajinný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pyrotechnická	pyrotechnický	k2eAgFnSc1d1
očista	očista	k1gFnSc1
území	území	k1gNnSc2
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
2012	#num#	k4
začala	začít	k5eAaPmAgFnS
pyrotechnická	pyrotechnický	k2eAgFnSc1d1
očista	očista	k1gFnSc1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
listopadu	listopad	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
vyčištěna	vyčištěn	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
5710	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
asi	asi	k9
sedm	sedm	k4xCc1
tisíc	tisíc	k4xCgInPc2
kusů	kus	k1gInPc2
nevybuchlé	vybuchlý	k2eNgFnSc2d1
munice	munice	k1gFnSc2
či	či	k8xC
jejích	její	k3xOp3gInPc2
zbytků	zbytek	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
zbývalo	zbývat	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
6900	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
hotova	hotov	k2eAgFnSc1d1
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
rozlohy	rozloha	k1gFnSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asanace	asanace	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pročištěné	pročištěný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
jsou	být	k5eAaImIp3nP
zpřístupněny	zpřístupněn	k2eAgFnPc1d1
veřejnosti	veřejnost	k1gFnPc1
nově	nově	k6eAd1
vyznačenými	vyznačený	k2eAgMnPc7d1
pěšími	pěší	k1gMnPc7
i	i	k8xC
cyklo	cyknout	k5eAaPmAgNnS,k5eAaImAgNnS
trasami	trasa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
některé	některý	k3yIgFnPc4
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
dopadové	dopadový	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
(	(	kIx(
<g/>
vysoké	vysoký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
nevybuchlé	vybuchlý	k2eNgFnSc2d1
munice	munice	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
těžko	těžko	k6eAd1
přístupná	přístupný	k2eAgNnPc1d1
místa	místo	k1gNnPc1
nešla	jít	k5eNaImAgNnP
vyčistit	vyčistit	k5eAaPmF
a	a	k8xC
je	být	k5eAaImIp3nS
do	do	k7c2
nich	on	k3xPp3gMnPc2
trvalý	trvalý	k2eAgInSc1d1
zákaz	zákaz	k1gInSc1
vstupu	vstup	k1gInSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvale	trvale	k6eAd1
nepřístupné	přístupný	k2eNgNnSc1d1
nadále	nadále	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
posádkové	posádkový	k2eAgNnSc1d1
cvičiště	cvičiště	k1gNnSc1
o	o	k7c6
rozloze	rozloha	k1gFnSc6
přibližně	přibližně	k6eAd1
50	#num#	k4
km²	km²	k?
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
v	v	k7c6
trojúhelníku	trojúhelník	k1gInSc6
obcí	obec	k1gFnSc7
Jince	Jince	k1gInPc4
<g/>
,	,	kIx,
Zaječov	Zaječov	k1gInSc1
a	a	k8xC
Obecnice	obecnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zonace	zonace	k1gFnSc1
CHKO	CHKO	kA
a	a	k8xC
maloplošná	maloplošný	k2eAgFnSc1d1
chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnSc1
</s>
<s>
Území	území	k1gNnSc1
CHKO	CHKO	kA
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
do	do	k7c2
4	#num#	k4
zón	zóna	k1gFnPc2
podle	podle	k7c2
stupně	stupeň	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výrazná	výrazný	k2eAgFnSc1d1
souvislá	souvislý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
území	území	k1gNnSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
III	III	kA
<g/>
.	.	kIx.
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
zóny	zóna	k1gFnSc2
patří	patřit	k5eAaImIp3nS
pouze	pouze	k6eAd1
několik	několik	k4yIc1
malých	malý	k2eAgInPc2d1
ostrůvků	ostrůvek	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Jižních	jižní	k2eAgInPc2d1
Brd	Brdy	k1gInPc2
(	(	kIx(
<g/>
zástavba	zástavba	k1gFnSc1
vsi	ves	k1gFnSc2
Míšov	Míšovo	k1gNnPc2
<g/>
,	,	kIx,
osady	osada	k1gFnSc2
Teslíny	Teslína	k1gFnSc2
a	a	k8xC
Koukalka	Koukalka	k1gFnSc1
<g/>
,	,	kIx,
ves	ves	k1gFnSc1
Planiny	planina	k1gFnPc1
<g/>
,	,	kIx,
Chynín	Chynín	k1gInSc1
<g/>
,	,	kIx,
severovýchodní	severovýchodní	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
Železného	Železný	k1gMnSc2
Újezda	Újezd	k1gMnSc2
kolem	kolem	k7c2
silnice	silnice	k1gFnSc2
do	do	k7c2
Nových	Nových	k2eAgFnPc2d1
Mitrovic	Mitrovice	k1gFnPc2
<g/>
,	,	kIx,
chatová	chatový	k2eAgFnSc1d1
osada	osada	k1gFnSc1
u	u	k7c2
rybníka	rybník	k1gInSc2
Drahota	drahota	k1gFnSc1
u	u	k7c2
Nových	Nových	k2eAgFnPc2d1
Mitrovic	Mitrovice	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
část	část	k1gFnSc1
osady	osada	k1gFnSc2
Mítov	Mítov	k1gInSc1
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
silnice	silnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
II	II	kA
<g/>
.	.	kIx.
zóny	zóna	k1gFnSc2
patří	patřit	k5eAaImIp3nS
převážně	převážně	k6eAd1
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
maloplošnými	maloplošný	k2eAgFnPc7d1
zvláště	zvláště	k6eAd1
chráněnými	chráněný	k2eAgFnPc7d1
územími	území	k1gNnPc7
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
na	na	k7c4
ně	on	k3xPp3gNnPc4
navržena	navržen	k2eAgNnPc4d1
<g/>
,	,	kIx,
nejcennější	cenný	k2eAgFnPc4d3
části	část	k1gFnPc4
některých	některý	k3yIgFnPc2
těchto	tento	k3xDgFnPc2
lokalit	lokalita	k1gFnPc2
jsou	být	k5eAaImIp3nP
zařazeny	zařadit	k5eAaPmNgFnP
do	do	k7c2
I.	I.	kA
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nařízení	nařízení	k1gNnSc1
vlády	vláda	k1gFnSc2
292	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
vyhlašuje	vyhlašovat	k5eAaImIp3nS
<g/>
,	,	kIx,
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
území	území	k1gNnSc1
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
do	do	k7c2
4	#num#	k4
zón	zóna	k1gFnPc2
odstupňované	odstupňovaný	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
bližší	blízký	k2eAgFnPc4d2
ochranné	ochranný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
stanoví	stanovit	k5eAaPmIp3nS
toto	tento	k3xDgNnSc1
nařízení	nařízení	k1gNnSc1
pro	pro	k7c4
celou	celá	k1gFnSc4
CHKO	CHKO	kA
shodně	shodně	k6eAd1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
zóny	zóna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
pro	pro	k7c4
III	III	kA
<g/>
.	.	kIx.
zónu	zóna	k1gFnSc4
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tam	tam	k6eAd1
lze	lze	k6eAd1
pouze	pouze	k6eAd1
se	s	k7c7
souhlasem	souhlas	k1gInSc7
příslušného	příslušný	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
provádět	provádět	k5eAaImF
činnosti	činnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
snížit	snížit	k5eAaPmF
hladinu	hladina	k1gFnSc4
povrchové	povrchový	k2eAgFnSc2d1
či	či	k8xC
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
na	na	k7c6
rašeliništích	rašeliniště	k1gNnPc6
<g/>
,	,	kIx,
prameništích	prameniště	k1gNnPc6
a	a	k8xC
v	v	k7c6
olšinách	olšina	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gNnSc2
vytvoření	vytvoření	k1gNnSc2
nacházelo	nacházet	k5eAaImAgNnS
5	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
a	a	k8xC
18	#num#	k4
evropsky	evropsky	k6eAd1
významných	významný	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Všech	všecek	k3xTgFnPc2
pět	pět	k4xCc1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nebyla	být	k5eNaImAgFnS
součástí	součást	k1gFnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
Jižních	jižní	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
tato	tento	k3xDgNnPc4
místa	místo	k1gNnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Fajmanovy	Fajmanův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
a	a	k8xC
Klenky	klenek	k1gInPc4
</s>
<s>
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Getsemanka	Getsemanka	k1gFnSc1
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Chynínské	Chynínský	k2eAgFnPc1d1
buky	buk	k1gInPc4
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Kokšín	Kokšína	k1gFnPc2
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Na	na	k7c6
skalách	skála	k1gFnPc6
</s>
<s>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Hřebenec	Hřebenec	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Míšovské	Míšovská	k1gFnPc1
buky	buk	k1gInPc1
</s>
<s>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Třemešný	Třemešný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
v	v	k7c6
Jižních	jižní	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bradava	Bradava	k1gFnSc1
</s>
<s>
V	v	k7c6
Úličkách	Úlička	k1gFnPc6
</s>
<s>
Niva	niva	k1gFnSc1
Kotelského	Kotelský	k2eAgInSc2d1
potoka	potok	k1gInSc2
</s>
<s>
Třemšín	Třemšín	k1gInSc1
a	a	k8xC
Hřebence	Hřebenka	k1gFnSc3
</s>
<s>
Závišínský	Závišínský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
ve	v	k7c6
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Felbabka	Felbabka	k1gFnSc1
</s>
<s>
Hrachoviště	hrachoviště	k1gNnSc1
</s>
<s>
Ohrazenický	Ohrazenický	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Brda	brdo	k1gNnPc1
</s>
<s>
Octárna	octárna	k1gFnSc1
</s>
<s>
Tok	tok	k1gInSc1
</s>
<s>
Ledný	Ledný	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Trokavecké	Trokavecký	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Mešenský	Mešenský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Padrťsko	Padrťsko	k6eAd1
</s>
<s>
Teslíny	Teslína	k1gFnPc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
ve	v	k7c6
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
bylo	být	k5eAaImAgNnS
navrhováno	navrhovat	k5eAaImNgNnS
50	#num#	k4
maloplošných	maloplošný	k2eAgFnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Zřízení	zřízení	k1gNnSc1
nových	nový	k2eAgNnPc2d1
maloplošných	maloplošný	k2eAgNnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
navrhováno	navrhovat	k5eAaImNgNnS
pro	pro	k7c4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
celá	celý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Středních	střední	k2eAgInPc2d1
Brd	Brdy	k1gInPc2
nebyla	být	k5eNaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
přírodním	přírodní	k2eAgInSc7d1
parkem	park	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
realizované	realizovaný	k2eAgFnSc6d1
variantě	varianta	k1gFnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
těchto	tento	k3xDgNnPc2
míst	místo	k1gNnPc2
vyhlášena	vyhlášen	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
oblasti	oblast	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
nebo	nebo	k8xC
I.	I.	kA
zóny	zóna	k1gFnSc2
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Láz	Láz	k?
–	–	k?
Bílá	bílý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Pilská	Pilský	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
</s>
<s>
Pilský	Pilský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Klobouček	klobouček	k1gInSc1
</s>
<s>
Tok	tok	k1gInSc1
</s>
<s>
Prameniště	prameniště	k1gNnSc1
Rezervy	rezerva	k1gFnSc2
</s>
<s>
Bor	bor	k1gInSc1
</s>
<s>
Houpák	Houpák	k1gMnSc1
</s>
<s>
Brda	brdo	k1gNnPc1
</s>
<s>
Octárna	octárna	k1gFnSc1
</s>
<s>
Trokavecké	Trokavecký	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Kolvín	Kolvín	k1gMnSc1
</s>
<s>
Bahna	bahno	k1gNnPc1
</s>
<s>
Teslínské	Teslínský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Teslínské	Teslínský	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
</s>
<s>
Hrachoviště	hrachoviště	k1gNnSc1
</s>
<s>
Albrechtický	albrechtický	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Červený	červený	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Jahodová	jahodový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
Míšov	Míšov	k1gInSc1
</s>
<s>
Bojovka	Bojovka	k1gFnSc1
</s>
<s>
Tisý	Tisý	k?
</s>
<s>
Jindřichova	Jindřichův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Slonovec	Slonovec	k1gMnSc1
</s>
<s>
Vlč	vlčit	k5eAaImRp2nS
</s>
<s>
Převážení	převážení	k1gNnSc1
</s>
<s>
Lipovsko	Lipovsko	k6eAd1
</s>
<s>
Trokavecká	Trokavecký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Prančík	Prančík	k1gMnSc1
</s>
<s>
Na	na	k7c6
Planinách	planina	k1gFnPc6
</s>
<s>
Ledný	Ledný	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Florian	Florian	k1gMnSc1
</s>
<s>
Údolí	údolí	k1gNnSc1
Skořického	Skořický	k2eAgInSc2d1
potoka	potok	k1gInSc2
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
</s>
<s>
Brauchitschova	Brauchitschův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Valdek	Valdek	k1gMnSc1
</s>
<s>
Padrťsko	Padrťsko	k6eAd1
</s>
<s>
Údolí	údolí	k1gNnSc1
Klabavy	Klabava	k1gFnSc2
</s>
<s>
Skládaná	skládaný	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Kočka	kočka	k1gFnSc1
</s>
<s>
Třemošná	Třemošný	k2eAgFnSc1d1
</s>
<s>
Žernovák	Žernovák	k1gMnSc1
</s>
<s>
Skelná	skelný	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
</s>
<s>
Vystrkov	Vystrkov	k1gInSc1
</s>
<s>
Plošina	plošina	k1gFnSc1
mezi	mezi	k7c4
vrchy	vrch	k1gInPc4
Praha	Praha	k1gFnSc1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Tok	tok	k1gInSc1
</s>
<s>
Koníček	Koníček	k1gMnSc1
</s>
<s>
Felbabka	Felbabka	k1gFnSc1
</s>
<s>
Pod	pod	k7c7
Palcířem	Palcíř	k1gInSc7
</s>
<s>
V	v	k7c6
Jižních	jižní	k2eAgNnPc6d1
Brdech	brdo	k1gNnPc6
CHKO	CHKO	kA
Brdy	brdo	k1gNnPc7
nahradila	nahradit	k5eAaPmAgFnS
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Brdy	Brdy	k1gInPc7
na	na	k7c6
území	území	k1gNnSc6
Plzeňského	plzeňský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Třemšín	Třemšína	k1gFnPc2
na	na	k7c4
území	území	k1gNnSc4
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severozápadě	severozápad	k1gInSc6
CHKO	CHKO	kA
zčásti	zčásti	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Hřebeny	hřeben	k1gInPc7
a	a	k8xC
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
na	na	k7c4
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
Trhoň	Trhoň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
</s>
<s>
S	s	k7c7
počátkem	počátek	k1gInSc7
roku	rok	k1gInSc2
2015	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zásadní	zásadní	k2eAgFnSc3d1
reorganizaci	reorganizace	k1gFnSc3
orgánů	orgán	k1gInPc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
se	se	k3xPyFc4
jednotlivé	jednotlivý	k2eAgFnPc1d1
správy	správa	k1gFnPc1
CHKO	CHKO	kA
staly	stát	k5eAaPmAgFnP
součástí	součást	k1gFnSc7
regionálních	regionální	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
AOPK	AOPK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
je	být	k5eAaImIp3nS
tedy	tedy	k9
podobně	podobně	k6eAd1
jako	jako	k9
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
,	,	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
a	a	k8xC
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Blaník	Blaník	k1gInSc1
v	v	k7c6
postavení	postavení	k1gNnSc6
oddělení	oddělení	k1gNnSc2
Regionálního	regionální	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
Střední	střední	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc4
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
je	být	k5eAaImIp3nS
v	v	k7c6
Jincích	Jince	k1gInPc6
na	na	k7c6
adrese	adresa	k1gFnSc6
Jince	Jince	k1gInPc1
č.	č.	k?
<g/>
p.	p.	k?
461	#num#	k4
v	v	k7c6
bývalém	bývalý	k2eAgNnSc6d1
sídle	sídlo	k1gNnSc6
zrušeného	zrušený	k2eAgInSc2d1
Újezdního	újezdní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
Brdy	Brdy	k1gInPc4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
každou	každý	k3xTgFnSc4
první	první	k4xOgFnSc4
středu	středa	k1gFnSc4
v	v	k7c6
měsíci	měsíc	k1gInSc6
úřední	úřední	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
ve	v	k7c6
Spáleném	spálený	k2eAgNnSc6d1
Poříčí	Poříčí	k1gNnSc6
na	na	k7c6
Náměstí	náměstí	k1gNnSc6
Svobody	svoboda	k1gFnSc2
132	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
symbolickému	symbolický	k2eAgNnSc3d1
předání	předání	k1gNnSc3
území	území	k1gNnSc2
do	do	k7c2
péče	péče	k1gFnSc2
AOPK	AOPK	kA
ČR	ČR	kA
došlo	dojít	k5eAaPmAgNnS
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgInSc1d1
předal	předat	k5eAaPmAgInS
bývalý	bývalý	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
ministru	ministr	k1gMnSc3
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Brabcovi	Brabcův	k2eAgMnPc1d1
a	a	k8xC
Správě	správa	k1gFnSc6
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
na	na	k7c6
zámečku	zámeček	k1gInSc6
Tři	tři	k4xCgFnPc1
Trubky	trubka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelé	ředitel	k1gMnPc1
Agentury	agentura	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
a	a	k8xC
Vojenských	vojenský	k2eAgInPc2d1
lesů	les	k1gInPc2
a	a	k8xC
statků	statek	k1gInPc2
současně	současně	k6eAd1
podepsali	podepsat	k5eAaPmAgMnP
dohodu	dohoda	k1gFnSc4
o	o	k7c6
spolupráci	spolupráce	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
o	o	k7c6
vzniku	vznik	k1gInSc6
a	a	k8xC
poslání	poslání	k1gNnSc6
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
o	o	k7c6
zajímavostech	zajímavost	k1gFnPc6
a	a	k8xC
aktualitách	aktualita	k1gFnPc6
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgFnPc1d1
na	na	k7c6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
AOPK	AOPK	kA
ČR	ČR	kA
brdy	brdo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
ochranaprirody	ochranaprirod	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s>
Pěší	pěší	k2eAgFnSc1d1
turistika	turistika	k1gFnSc1
</s>
<s>
Stav	stav	k1gInSc1
před	před	k7c7
zrušením	zrušení	k1gNnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
</s>
<s>
Před	před	k7c7
zřízením	zřízení	k1gNnSc7
CHKO	CHKO	kA
vedlo	vést	k5eAaImAgNnS
jejím	její	k3xOp3gNnSc7
územím	území	k1gNnSc7
několik	několik	k4yIc1
značených	značený	k2eAgFnPc2d1
pěších	pěší	k2eAgFnPc2d1
turistických	turistický	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
KČT	KČT	kA
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
(	(	kIx(
<g/>
cca	cca	kA
25	#num#	k4
km	km	kA
<g/>
,	,	kIx,
původně	původně	k6eAd1
přístupné	přístupný	k2eAgInPc1d1
jen	jen	k9
o	o	k7c6
sobotách	sobota	k1gFnPc6
<g/>
,	,	kIx,
nedělích	neděle	k1gFnPc6
a	a	k8xC
svátcích	svátek	k1gInPc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
č.	č.	k?
6157	#num#	k4
Podluhy	Podluha	k1gFnSc2
–	–	k?
Neřežín	Neřežín	k1gMnSc1
(	(	kIx(
<g/>
9,9	9,9	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
č.	č.	k?
6614	#num#	k4
a	a	k8xC
6726	#num#	k4
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
–	–	k?
Těně	Těň	k1gMnSc2
–	–	k?
Strašice	Strašice	k1gFnPc1
(	(	kIx(
<g/>
7,4	7,4	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
zelená	zelená	k1gFnSc1
č.	č.	k?
3097	#num#	k4
Orlov	Orlov	k1gInSc1
–	–	k?
Bohutín	Bohutín	k1gInSc1
(	(	kIx(
<g/>
6,5	6,5	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
místní	místní	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
č.	č.	k?
1191	#num#	k4
Orlov	Orlov	k1gInSc1
–	–	k?
Třemošná	Třemošný	k2eAgFnSc1d1
–	–	k?
Orlov	Orlov	k1gInSc1
(	(	kIx(
<g/>
2,9	2,9	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Mimo	mimo	k7c4
území	území	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
(	(	kIx(
<g/>
cca	cca	kA
70	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
Hořesedly	Hořesedlo	k1gNnPc7
–	–	k?
Míšov	Míšov	k1gInSc1
(	(	kIx(
<g/>
7,4	7,4	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
Míšov	Míšov	k1gInSc1
–	–	k?
Nad	nad	k7c7
Maráskem	Marásek	k1gInSc7
(	(	kIx(
<g/>
3,9	3,9	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
zelená	zelená	k1gFnSc1
Teslíny	Teslína	k1gFnSc2
–	–	k?
Spálená	spálený	k2eAgFnSc1d1
Bouda	bouda	k1gFnSc1
(	(	kIx(
<g/>
1,9	1,9	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
zelená	zelená	k1gFnSc1
Hutě	huť	k1gFnSc2
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
–	–	k?
Uhelnice	Uhelnice	k1gFnSc1
(	(	kIx(
<g/>
4,7	4,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
Třemšín	Třemšín	k1gInSc1
–	–	k?
Na	na	k7c6
Dědku	Dědek	k1gMnSc6
(	(	kIx(
<g/>
2,7	2,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
Míšov	Míšov	k1gInSc1
–	–	k?
Pod	pod	k7c7
Maráskem	Marásek	k1gInSc7
(	(	kIx(
<g/>
2,7	2,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
modrá	modrat	k5eAaImIp3nS
Starý	starý	k2eAgMnSc1d1
Smolivec	Smolivec	k1gMnSc1
–	–	k?
Třemšín	Třemšín	k1gMnSc1
–	–	k?
silnice	silnice	k1gFnSc1
Rožmitál	Rožmitál	k1gMnSc1
(	(	kIx(
<g/>
10,2	10,2	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
Borovno	Borovna	k1gFnSc5
–	–	k?
Nad	nad	k7c7
Maráskem	Marásek	k1gInSc7
–	–	k?
Třemšín	Třemšín	k1gInSc1
–	–	k?
Brdy	Brdy	k1gInPc4
(	(	kIx(
<g/>
bus	bus	k1gInSc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
22,1	22,1	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
červená	červený	k2eAgFnSc1d1
Nové	Nové	k2eAgFnSc1d1
Mitrovice	Mitrovice	k1gFnSc1
–	–	k?
Pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
(	(	kIx(
<g/>
rozc	rozc	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
Mýta	mýto	k1gNnSc2
(	(	kIx(
<g/>
12,2	12,2	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
červená	červená	k1gFnSc1
Hutě	huť	k1gFnSc2
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
–	–	k?
Třemšín	Třemšín	k1gInSc1
(	(	kIx(
<g/>
3,9	3,9	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
značení	značení	k1gNnSc1
KČT	KČT	kA
</s>
<s>
Klub	klub	k1gInSc1
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
měl	mít	k5eAaImAgInS
v	v	k7c6
plánu	plán	k1gInSc6
páteřní	páteřní	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
újezdu	újezd	k1gInSc6
vyznačit	vyznačit	k5eAaPmF
ještě	ještě	k6eAd1
před	před	k7c7
vznikem	vznik	k1gInSc7
CHKO	CHKO	kA
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
údajně	údajně	k6eAd1
s	s	k7c7
armádou	armáda	k1gFnSc7
předběžně	předběžně	k6eAd1
dohodl	dohodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Předseda	předseda	k1gMnSc1
komise	komise	k1gFnSc2
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
pro	pro	k7c4
optimalizaci	optimalizace	k1gFnSc4
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
Vladimír	Vladimír	k1gMnSc1
Kazatel	kazatel	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
červnu	červen	k1gInSc6
2015	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
vydá	vydat	k5eAaPmIp3nS
ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
souhlas	souhlas	k1gInSc1
se	s	k7c7
značením	značení	k1gNnSc7
před	před	k7c7
lednem	leden	k1gInSc7
2016	#num#	k4
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
ministerstvo	ministerstvo	k1gNnSc4
obrany	obrana	k1gFnSc2
problém	problém	k1gInSc4
a	a	k8xC
bude	být	k5eAaImBp3nS
souhlasit	souhlasit	k5eAaImF
<g/>
,	,	kIx,
samozřejmě	samozřejmě	k6eAd1
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
vyčištěná	vyčištěný	k2eAgFnSc1d1
od	od	k7c2
munice	munice	k1gFnSc2
a	a	k8xC
zpřístupněná	zpřístupněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Nakonec	nakonec	k6eAd1
značení	značení	k1gNnSc1
před	před	k7c7
zrušením	zrušení	k1gNnSc7
újezdu	újezd	k1gInSc2
nebylo	být	k5eNaImAgNnS
KČT	KČT	kA
umožněno	umožnit	k5eAaPmNgNnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mluvčí	mluvčit	k5eAaImIp3nS
ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
zdůvodnil	zdůvodnit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
rozvoj	rozvoj	k1gInSc4
turismu	turismus	k1gInSc2
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
i	i	k9
stezek	stezka	k1gFnPc2
bude	být	k5eAaImBp3nS
ponechán	ponechat	k5eAaPmNgInS
v	v	k7c6
gesci	gesce	k1gFnSc6
budoucí	budoucí	k2eAgFnSc2d1
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
začne	začít	k5eAaPmIp3nS
území	území	k1gNnSc4
značit	značit	k5eAaImF
po	po	k7c6
jejím	její	k3xOp3gInSc6
vzniku	vznik	k1gInSc6
<g/>
,	,	kIx,
tedy	tedy	k9
po	po	k7c4
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
rady	rada	k1gMnSc2
značení	značení	k1gNnSc4
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
Karel	Karel	k1gMnSc1
Markvart	Markvart	k1gInSc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
připravenost	připravenost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vedoucí	vedoucí	k1gMnSc1
oddělení	oddělení	k1gNnSc2
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc7
Bohumil	Bohumil	k1gMnSc1
Fišer	Fišer	k1gMnSc1
(	(	kIx(
<g/>
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
samozřejmě	samozřejmě	k6eAd1
existovalo	existovat	k5eAaImAgNnS
již	již	k6eAd1
před	před	k7c7
vznikem	vznik	k1gInSc7
CHKO	CHKO	kA
<g/>
)	)	kIx)
označil	označit	k5eAaPmAgMnS
dřívější	dřívější	k2eAgInSc4d1
termín	termín	k1gInSc4
za	za	k7c4
nereálný	reálný	k2eNgInSc4d1
<g/>
,	,	kIx,
protože	protože	k8xS
zatím	zatím	k6eAd1
nebylo	být	k5eNaImAgNnS
vymezeno	vymezit	k5eAaPmNgNnS
posádkové	posádkový	k2eAgNnSc1d1
cvičiště	cvičiště	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
nepřístupné	přístupný	k2eNgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
nebylo	být	k5eNaImAgNnS
ani	ani	k9
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
a	a	k8xC
v	v	k7c6
jakém	jaký	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
rozsahu	rozsah	k1gInSc6
se	se	k3xPyFc4
zpřístupní	zpřístupnit	k5eAaPmIp3nS
turisticky	turisticky	k6eAd1
atraktivní	atraktivní	k2eAgFnPc4d1
odlesněné	odlesněný	k2eAgFnPc4d1
dopadové	dopadový	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2015	#num#	k4
již	již	k6eAd1
měl	mít	k5eAaImAgInS
KČT	KČT	kA
hrubý	hrubý	k2eAgInSc4d1
plán	plán	k1gInSc4
nových	nový	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
chtěl	chtít	k5eAaImAgMnS
ještě	ještě	k6eAd1
v	v	k7c6
terénu	terén	k1gInSc6
prozkoumat	prozkoumat	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
schůdnost	schůdnost	k1gFnSc4
a	a	k8xC
vytipovat	vytipovat	k5eAaPmF
objekty	objekt	k1gInPc4
a	a	k8xC
křižovatky	křižovatka	k1gFnPc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
lze	lze	k6eAd1
umístit	umístit	k5eAaPmF
cedule	cedule	k1gFnPc1
se	s	k7c7
směrovkami	směrovka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
prvními	první	k4xOgInPc7
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
předseda	předseda	k1gMnSc1
rady	rada	k1gMnSc2
značení	značení	k1gNnSc2
zpřístupnění	zpřístupnění	k1gNnSc2
vrcholu	vrchol	k1gInSc2
Tok	toka	k1gFnPc2
a	a	k8xC
přeložku	přeložka	k1gFnSc4
trasy	trasa	k1gFnSc2
do	do	k7c2
Třemošné	Třemošný	k2eAgFnSc2d1
tak	tak	k8xC,k8xS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vedla	vést	k5eAaImAgFnS
krásnými	krásný	k2eAgFnPc7d1
skalami	skála	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
Padrťským	Padrťský	k2eAgInPc3d1
rybníkům	rybník	k1gInPc3
<g/>
,	,	kIx,
kam	kam	k6eAd1
dosud	dosud	k6eAd1
vedle	vedle	k6eAd1
jediná	jediný	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
<g/>
,	,	kIx,
přístupná	přístupný	k2eAgFnSc1d1
jen	jen	k9
o	o	k7c6
víkendech	víkend	k1gInPc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
variabilnější	variabilní	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
červená	červený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
by	by	kYmCp3nS
podle	podle	k7c2
plánu	plán	k1gInSc2
mohla	moct	k5eAaImAgFnS
vést	vést	k5eAaImF
hřebenem	hřeben	k1gInSc7
a	a	k8xC
na	na	k7c4
ni	on	k3xPp3gFnSc4
by	by	kYmCp3nP
navazovaly	navazovat	k5eAaImAgFnP
další	další	k2eAgNnSc4d1
napříč	napříč	k7c7
bývalým	bývalý	k2eAgInSc7d1
újezdem	újezd	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vyznačení	vyznačení	k1gNnSc6
200	#num#	k4
km	km	kA
nových	nový	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
tehdy	tehdy	k6eAd1
KČT	KČT	kA
odhadoval	odhadovat	k5eAaImAgInS
potřebu	potřeba	k1gFnSc4
asi	asi	k9
40	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tvorbě	tvorba	k1gFnSc6
projektu	projekt	k1gInSc2
KČT	KČT	kA
vycházel	vycházet	k5eAaImAgInS
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kudy	kudy	k6eAd1
trasy	trasa	k1gFnPc1
vedly	vést	k5eAaImAgFnP
za	za	k7c2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
zdrojem	zdroj	k1gInSc7
informací	informace	k1gFnPc2
byli	být	k5eAaImAgMnP
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
to	ten	k3xDgNnSc4
tam	tam	k6eAd1
znali	znát	k5eAaImAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
tam	tam	k6eAd1
chodili	chodit	k5eAaImAgMnP
„	„	k?
<g/>
načerno	načerno	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
rady	rada	k1gMnSc2
značení	značení	k1gNnSc2
KČT	KČT	kA
vyjádřil	vyjádřit	k5eAaPmAgMnS
naději	naděje	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
vojáci	voják	k1gMnPc1
znalí	znalý	k2eAgMnPc1d1
místa	místo	k1gNnSc2
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
pomoci	pomoct	k5eAaPmF
domyslet	domyslet	k5eAaPmF
propojení	propojení	k1gNnSc4
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
zpráv	zpráva	k1gFnPc2
z	z	k7c2
října	říjen	k1gInSc2
2015	#num#	k4
měl	mít	k5eAaImAgMnS
po	po	k7c6
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2016	#num#	k4
začít	začít	k5eAaPmF
na	na	k7c6
daném	daný	k2eAgNnSc6d1
území	území	k1gNnSc6
Klub	klub	k1gInSc1
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
provádět	provádět	k5eAaImF
turistické	turistický	k2eAgNnSc4d1
značení	značení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
jara	jaro	k1gNnSc2
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
chtěl	chtít	k5eAaImAgMnS
vyznačit	vyznačit	k5eAaPmF
zhruba	zhruba	k6eAd1
450	#num#	k4
kilometrů	kilometr	k1gInPc2
značených	značený	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
a	a	k8xC
nejméně	málo	k6eAd3
tři	tři	k4xCgFnPc1
naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
dopadových	dopadový	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
Tok	tok	k1gInSc1
a	a	k8xC
Jordán	Jordán	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
Padrtě	Padrta	k1gFnSc6
<g/>
,	,	kIx,
Tří	tři	k4xCgInPc2
Trubek	trubka	k1gFnPc2
<g/>
,	,	kIx,
Strašic	Strašice	k1gFnPc2
a	a	k8xC
v	v	k7c6
jednání	jednání	k1gNnSc6
byla	být	k5eAaImAgFnS
také	také	k9
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
na	na	k7c4
Třemšín	Třemšín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
naučnou	naučný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
Klouček	Klouček	k1gMnSc1
<g/>
,	,	kIx,
připravovaly	připravovat	k5eAaImAgInP
Vojenské	vojenský	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
statky	statek	k1gInPc1
a	a	k8xC
jednání	jednání	k1gNnPc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgNnP
vést	vést	k5eAaImF
i	i	k9
o	o	k7c6
Příbramí	Příbram	k1gFnSc7
navrhované	navrhovaný	k2eAgFnSc3d1
stezce	stezka	k1gFnSc3
na	na	k7c4
Třemošnou	Třemošný	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
prosince	prosinec	k1gInSc2
2015	#num#	k4
dostali	dostat	k5eAaPmAgMnP
značkaři	značkař	k1gMnPc1
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
povolení	povolení	k1gNnSc4
ke	k	k7c3
vstupu	vstup	k1gInSc3
do	do	k7c2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
území	území	k1gNnSc4
mohli	moct	k5eAaImAgMnP
projít	projít	k5eAaPmF
a	a	k8xC
připravit	připravit	k5eAaPmF
si	se	k3xPyFc3
místa	místo	k1gNnPc4
pro	pro	k7c4
rozcestníky	rozcestník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
jaře	jaro	k1gNnSc6
2016	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
chtělo	chtít	k5eAaImAgNnS
KČT	KČT	kA
vyznačit	vyznačit	k5eAaPmF
40	#num#	k4
až	až	k9
50	#num#	k4
kilometrů	kilometr	k1gInPc2
tras	trasa	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
páteřní	páteřní	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
od	od	k7c2
Jinců	Jince	k1gInPc2
přes	přes	k7c4
Padrťské	Padrťský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
na	na	k7c4
jih	jih	k1gInSc4
směrem	směr	k1gInSc7
na	na	k7c4
Míšov	Míšov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hraničních	hraniční	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
z	z	k7c2
Dobřívi	Dobříev	k1gFnSc3
<g/>
,	,	kIx,
Skořic	skořice	k1gFnPc2
či	či	k8xC
Míšova	Míšův	k2eAgFnSc1d1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
navázány	navázán	k2eAgFnPc1d1
další	další	k2eAgFnPc1d1
větve	větev	k1gFnPc1
<g/>
;	;	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
kopec	kopec	k1gInSc1
Tok	toka	k1gFnPc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zpřístupněn	zpřístupnit	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
června	červen	k1gInSc2
2018	#num#	k4
Klub	klub	k1gInSc4
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
ohlásil	ohlásit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
nové	nový	k2eAgNnSc1d1
pěší	pěší	k2eAgNnSc1d1
značení	značení	k1gNnSc1
v	v	k7c6
rozsahu	rozsah	k1gInSc6
250	#num#	k4
kilometrů	kilometr	k1gInPc2
bude	být	k5eAaImBp3nS
dokončeno	dokončit	k5eAaPmNgNnS
před	před	k7c7
začátkem	začátek	k1gInSc7
letních	letní	k2eAgFnPc2d1
prázdnin	prázdniny	k1gFnPc2
2018	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
cyklotrasy	cyklotrasa	k1gFnPc1
dosud	dosud	k6eAd1
vyznačeny	vyznačit	k5eAaPmNgFnP
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Páteřní	páteřní	k2eAgFnSc2d1
červeně	červeň	k1gFnSc2
značená	značený	k2eAgFnSc1d1
trase	trasa	k1gFnSc6
vede	vést	k5eAaImIp3nS
na	na	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Tok	toka	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
dopadovou	dopadový	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
Jordán	Jordán	k1gMnSc1
i	i	k9
k	k	k7c3
loveckému	lovecký	k2eAgNnSc3d1
zámečku	zámeček	k1gInSc6
Tři	tři	k4xCgFnPc1
Trubky	trubka	k1gFnPc1
a	a	k8xC
na	na	k7c4
Padrťské	Padrťský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Značkaři	Značkař	k1gMnPc1
museli	muset	k5eAaImAgMnP
do	do	k7c2
oblasti	oblast	k1gFnSc2
vyrazit	vyrazit	k5eAaPmF
zhruba	zhruba	k6eAd1
padesátkrát	padesátkrát	k6eAd1
a	a	k8xC
náklady	náklad	k1gInPc1
na	na	k7c4
vyznačení	vyznačení	k1gNnSc4
tras	trasa	k1gFnPc2
v	v	k7c6
nově	nově	k6eAd1
vyhlášené	vyhlášený	k2eAgFnSc6d1
CHKO	CHKO	kA
dobrovolnými	dobrovolný	k2eAgMnPc7d1
značkaři	značkař	k1gMnPc7
odhadl	odhadnout	k5eAaPmAgMnS
KČT	KČT	kA
na	na	k7c4
25	#num#	k4
až	až	k9
30	#num#	k4
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
byly	být	k5eAaImAgInP
čerpány	čerpán	k2eAgInPc1d1
z	z	k7c2
peněz	peníze	k1gInPc2
na	na	k7c4
pravidelnou	pravidelný	k2eAgFnSc4d1
údržbu	údržba	k1gFnSc4
značených	značený	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Hlavní	hlavní	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
se	se	k3xPyFc4
již	již	k6eAd1
rozšiřovat	rozšiřovat	k5eAaImF
nemají	mít	k5eNaImIp3nP
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
však	však	k9
ještě	ještě	k6eAd1
vznikat	vznikat	k5eAaImF
odbočky	odbočka	k1gFnPc4
na	na	k7c4
zajímavá	zajímavý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
u	u	k7c2
obcí	obec	k1gFnPc2
Obecnice	obecnice	k1gFnSc2
či	či	k8xC
Chaloupky	chaloupka	k1gFnSc2
zamýšlejí	zamýšlet	k5eAaImIp3nP
obce	obec	k1gFnPc1
zřídit	zřídit	k5eAaPmF
naučné	naučný	k2eAgFnPc4d1
stezky	stezka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
22	#num#	k4
<g/>
]	]	kIx)
Obec	obec	k1gFnSc1
Chaloupky	chaloupka	k1gFnSc2
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
získání	získání	k1gNnSc4
hradu	hrad	k1gInSc2
Valdek	Valdek	k1gInSc4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Mikroregion	mikroregion	k1gInSc1
Hořovicko	Hořovicko	k1gNnSc1
má	mít	k5eAaImIp3nS
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
spolku	spolek	k1gInSc2
Turistická	turistický	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	Brdy	k1gInPc4
a	a	k8xC
Podbrdsko	Podbrdsko	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
rozvojové	rozvojový	k2eAgInPc4d1
projekty	projekt	k1gInPc4
podporovat	podporovat	k5eAaImF
a	a	k8xC
má	mít	k5eAaImIp3nS
mít	mít	k5eAaImF
na	na	k7c4
starosti	starost	k1gFnPc4
zřizování	zřizování	k1gNnSc2
a	a	k8xC
správu	správa	k1gFnSc4
cyklotras	cyklotrasa	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Projektovou	projektový	k2eAgFnSc4d1
dokumentaci	dokumentace	k1gFnSc4
pro	pro	k7c4
vyznačení	vyznačení	k1gNnSc4
cyklotras	cyklotrasa	k1gFnPc2
připravuje	připravovat	k5eAaImIp3nS
město	město	k1gNnSc1
Příbram	Příbram	k1gFnSc4
s	s	k7c7
podporou	podpora	k1gFnSc7
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
vybudování	vybudování	k1gNnSc1
cyklostezek	cyklostezka	k1gFnPc2
má	mít	k5eAaImIp3nS
podle	podle	k7c2
vedoucí	vedoucí	k1gFnSc2
odboru	odbor	k1gInSc2
kanceláře	kancelář	k1gFnSc2
města	město	k1gNnSc2
Příbram	Příbram	k1gFnSc4
vyjít	vyjít	k5eAaPmF
přibližně	přibližně	k6eAd1
na	na	k7c4
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
KČT	KČT	kA
připravuje	připravovat	k5eAaImIp3nS
vydání	vydání	k1gNnSc1
nové	nový	k2eAgFnSc2d1
turistické	turistický	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
č.	č.	k?
99	#num#	k4
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
měl	mít	k5eAaImAgInS
území	území	k1gNnSc4
Středních	střední	k2eAgNnPc2d1
Brd	brdo	k1gNnPc2
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
mapy	mapa	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrh	návrh	k1gInSc1
AOPK	AOPK	kA
</s>
<s>
AOPK	AOPK	kA
na	na	k7c6
svém	svůj	k3xOyFgInSc6
webu	web	k1gInSc6
uvedlo	uvést	k5eAaPmAgNnS
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
jako	jako	k8xS,k8xC
aktualitu	aktualita	k1gFnSc4
článek	článek	k1gInSc4
„	„	k?
<g/>
Naučné	naučný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
a	a	k8xC
cyklostezky	cyklostezka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
úvodem	úvod	k1gInSc7
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
a	a	k8xC
Újezdní	újezdní	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Brdy	brdo	k1gNnPc7
připravily	připravit	k5eAaPmAgFnP
návrh	návrh	k1gInSc4
koncepce	koncepce	k1gFnSc2
cyklostezek	cyklostezka	k1gFnPc2
a	a	k8xC
turistických	turistický	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
pro	pro	k7c4
bývalý	bývalý	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
navržených	navržený	k2eAgFnPc2d1
pěších	pěší	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
uváděna	uváděn	k2eAgFnSc1d1
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
autoři	autor	k1gMnPc1
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
navržené	navržený	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
změřili	změřit	k5eAaPmAgMnP
na	na	k7c4
92,2	92,2	k4
km	km	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
představuje	představovat	k5eAaImIp3nS
hustotu	hustota	k1gFnSc4
pěších	pěší	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
v	v	k7c6
rušeném	rušený	k2eAgInSc6d1
újezdu	újezd	k1gInSc6
(	(	kIx(
<g/>
s	s	k7c7
vyloučením	vyloučení	k1gNnSc7
trvale	trvale	k6eAd1
zcela	zcela	k6eAd1
nepřístupných	přístupný	k2eNgNnPc2d1
území	území	k1gNnPc2
<g/>
)	)	kIx)
416	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
,	,	kIx,
podle	podle	k7c2
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
zcela	zcela	k6eAd1
nedostatečnou	dostatečný	k2eNgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
navržených	navržený	k2eAgFnPc2d1
pěších	pěší	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
některé	některý	k3yIgFnPc1
kolidují	kolidovat	k5eAaImIp3nP
s	s	k7c7
cyklostezkami	cyklostezka	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
několika	několik	k4yIc6
případech	případ	k1gInPc6
vedou	vést	k5eAaImIp3nP
po	po	k7c6
veřejných	veřejný	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
Strašicích	Strašice	k1gFnPc6
<g/>
)	)	kIx)
a	a	k8xC
32,5	32,5	k4
km	km	kA
vede	vést	k5eAaImIp3nS
po	po	k7c6
asfaltových	asfaltový	k2eAgFnPc6d1
silnicích	silnice	k1gFnPc6
(	(	kIx(
<g/>
míněno	mínit	k5eAaImNgNnS
zřejmě	zřejmě	k6eAd1
asfaltových	asfaltový	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrh	návrh	k1gInSc1
AOPK	AOPK	kA
ČR	ČR	kA
zpracovaný	zpracovaný	k2eAgInSc4d1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
újezdním	újezdní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
obsahuje	obsahovat	k5eAaImIp3nS
pouhých	pouhý	k2eAgInPc2d1
8	#num#	k4
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
sítě	síť	k1gFnSc2
pěších	pěší	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Hřebenová	hřebenový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
od	od	k7c2
Jinců	Jince	k1gInPc2
až	až	k9
po	po	k7c4
Padrťsko	Padrťsko	k1gNnSc4
<g/>
“	“	k?
Autoři	autor	k1gMnPc1
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
byli	být	k5eAaImAgMnP
návrhem	návrh	k1gInSc7
hluboce	hluboko	k6eAd1
zklamáni	zklamat	k5eAaPmNgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
po	po	k7c6
hlavním	hlavní	k2eAgInSc6d1
hřebeni	hřeben	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
dlouhý	dlouhý	k2eAgInSc1d1
28	#num#	k4
km	km	kA
(	(	kIx(
<g/>
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
6	#num#	k4
km	km	kA
je	být	k5eAaImIp3nS
v	v	k7c6
trvale	trvale	k6eAd1
nepřístupném	přístupný	k2eNgNnSc6d1
území	území	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
celý	celý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
průchodný	průchodný	k2eAgInSc1d1
po	po	k7c6
zpravidla	zpravidla	k6eAd1
neasfaltovaných	asfaltovaný	k2eNgFnPc6d1
cestách	cesta	k1gFnPc6
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
jen	jen	k9
celkem	celkem	k6eAd1
8	#num#	k4
km	km	kA
navržených	navržený	k2eAgFnPc2d1
značených	značený	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
ve	v	k7c6
třech	tři	k4xCgInPc6
vzájemně	vzájemně	k6eAd1
nesouvisejících	související	k2eNgInPc6d1
úsecích	úsek	k1gInPc6
<g/>
:	:	kIx,
od	od	k7c2
Čenkova	Čenkov	k1gInSc2
po	po	k7c6
hřebeni	hřeben	k1gInSc6
Slonovce	Slonovec	k1gMnSc2
na	na	k7c6
Klouček	Klouček	k1gMnSc1
(	(	kIx(
<g/>
3,6	3,6	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
hřebeni	hřeben	k1gInSc6
Toku	tok	k1gInSc2
od	od	k7c2
Houpáku	Houpák	k1gInSc2
po	po	k7c4
vrchol	vrchol	k1gInSc4
Toku	tok	k1gInSc2
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
součást	součást	k1gFnSc1
NS	NS	kA
<g/>
,	,	kIx,
2,7	2,7	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
Sv.	sv.	kA
Jana	Jana	k1gFnSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1,7	1,7	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
po	po	k7c4
14	#num#	k4
km	km	kA
cest	cesta	k1gFnPc2
po	po	k7c6
turisticky	turisticky	k6eAd1
atraktivním	atraktivní	k2eAgInSc6d1
hlavním	hlavní	k2eAgInSc6d1
hřebeni	hřeben	k1gInSc6
není	být	k5eNaImIp3nS
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
navržena	navržen	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
původně	původně	k6eAd1
proklamovanou	proklamovaný	k2eAgFnSc7d1
„	„	k?
<g/>
páteřní	páteřní	k2eAgInPc1d1
hřebenovou	hřebenový	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autoři	autor	k1gMnPc1
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
považují	považovat	k5eAaImIp3nP
tento	tento	k3xDgInSc4
návrh	návrh	k1gInSc4
za	za	k7c4
naprosto	naprosto	k6eAd1
nevyhovující	vyhovující	k2eNgMnSc1d1
a	a	k8xC
nedostatečný	dostatečný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Po	po	k7c6
stopách	stopa	k1gFnPc6
československého	československý	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
je	být	k5eAaImIp3nS
trasována	trasován	k2eAgFnSc1d1
po	po	k7c6
okrajích	okraj	k1gInPc6
dopadových	dopadový	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
Tok	tok	k1gInSc1
a	a	k8xC
Jordán	Jordán	k1gInSc1
a	a	k8xC
spojuje	spojovat	k5eAaImIp3nS
je	on	k3xPp3gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
autorů	autor	k1gMnPc2
návrhu	návrh	k1gInSc2
AOPK	AOPK	kA
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
asi	asi	k9
10	#num#	k4
km	km	kA
<g/>
,	,	kIx,
autoři	autor	k1gMnPc1
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
navrženou	navržený	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
pečlivě	pečlivě	k6eAd1
přeměřili	přeměřit	k5eAaPmAgMnP
a	a	k8xC
došli	dojít	k5eAaPmAgMnP
k	k	k7c3
délce	délka	k1gFnSc3
základní	základní	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
15,5	15,5	k4
km	km	kA
<g/>
,	,	kIx,
s	s	k7c7
dvěma	dva	k4xCgFnPc7
odbočkami	odbočka	k1gFnPc7
18	#num#	k4
km	km	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
návrh	návrh	k1gInSc4
považují	považovat	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
možností	možnost	k1gFnPc2
za	za	k7c4
dobrý	dobrý	k2eAgInSc4d1
a	a	k8xC
oceňují	oceňovat	k5eAaImIp3nP
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jen	jen	k9
v	v	k7c6
minimálním	minimální	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
vede	vést	k5eAaImIp3nS
po	po	k7c6
silnicích	silnice	k1gFnPc6
s	s	k7c7
asfaltovým	asfaltový	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Po	po	k7c6
stopách	stopa	k1gFnPc6
vysídlených	vysídlený	k2eAgFnPc2d1
brdských	brdský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
je	být	k5eAaImIp3nS
trasována	trasovat	k5eAaImNgFnS
oblastí	oblast	k1gFnSc7
Padrtě	Padrta	k1gFnSc3
<g/>
,	,	kIx,
přes	přes	k7c4
vrch	vrch	k1gInSc4
Kočka	kočka	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
zámečku	zámeček	k1gInSc6
Tři	tři	k4xCgFnPc1
Trubky	trubka	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c4
vrch	vrch	k1gInSc4
Lipovsko	Lipovsko	k1gNnSc1
a	a	k8xC
do	do	k7c2
obce	obec	k1gFnSc2
Strašice	Strašice	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
těžištěm	těžiště	k1gNnSc7
má	mít	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
Muzeum	muzeum	k1gNnSc1
Středních	střední	k2eAgInPc2d1
Brd	Brdy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
v	v	k7c6
návrhu	návrh	k1gInSc6
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
délka	délka	k1gFnSc1
naučné	naučný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
je	být	k5eAaImIp3nS
do	do	k7c2
25	#num#	k4
km	km	kA
<g/>
,	,	kIx,
autoři	autor	k1gMnPc1
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
však	však	k9
naměřili	naměřit	k5eAaBmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
navržená	navržený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
délky	délka	k1gFnSc2
35,7	35,7	k4
km	km	kA
<g/>
,	,	kIx,
a	a	k8xC
konstatovali	konstatovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nejméně	málo	k6eAd3
50	#num#	k4
%	%	kIx~
trasy	trasa	k1gFnSc2
vede	vést	k5eAaImIp3nS
po	po	k7c6
asfaltu	asfalt	k1gInSc6
nebo	nebo	k8xC
dlažbě	dlažba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
AOPK	AOPK	kA
navrhla	navrhnout	k5eAaPmAgFnS
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
trasu	trasa	k1gFnSc4
z	z	k7c2
Oseče	Oseč	k1gInSc2
okolo	okolo	k7c2
Třemošné	Třemošný	k2eAgFnSc2d1
přes	přes	k7c4
Zavírku	zavírka	k1gFnSc4
<g/>
,	,	kIx,
Skelnou	skelný	k2eAgFnSc4d1
Huť	huť	k1gFnSc4
a	a	k8xC
vrch	vrch	k1gInSc4
Prahu	Praha	k1gFnSc4
do	do	k7c2
lokality	lokalita	k1gFnSc2
Studánka	studánka	k1gFnSc1
na	na	k7c6
Padrtích	padrť	k1gFnPc6
(	(	kIx(
<g/>
18,3	18,3	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
nástupní	nástupní	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
z	z	k7c2
východisek	východisko	k1gNnPc2
Obecnice	obecnice	k1gFnSc2
<g/>
,	,	kIx,
Příbram	Příbram	k1gFnSc1
<g/>
,	,	kIx,
Bohutín	Bohutín	k1gInSc1
a	a	k8xC
Zaječov	Zaječov	k1gInSc1
na	na	k7c4
Naučnou	naučný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
Po	po	k7c6
stopách	stopa	k1gFnPc6
československého	československý	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
spojku	spojka	k1gFnSc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
navrženými	navržený	k2eAgFnPc7d1
naučnými	naučný	k2eAgFnPc7d1
stezkami	stezka	k1gFnPc7
(	(	kIx(
<g/>
od	od	k7c2
Dlouhého	Dlouhého	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
na	na	k7c4
Tři	tři	k4xCgFnPc4
Trubky	trubka	k1gFnPc4
<g/>
,	,	kIx,
6,6	6,6	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
koncepce	koncepce	k1gFnSc1
byla	být	k5eAaImAgFnS
předložena	předložit	k5eAaPmNgFnS
vedení	vedení	k1gNnSc1
KČT	KČT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
ji	on	k3xPp3gFnSc4
akceptovalo	akceptovat	k5eAaBmAgNnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
značkařské	značkařský	k2eAgInPc1d1
týmy	tým	k1gInPc1
z	z	k7c2
Příbramska	Příbramsko	k1gNnSc2
<g/>
,	,	kIx,
Rokycanska	Rokycansko	k1gNnSc2
a	a	k8xC
Berounska	Berounsko	k1gNnSc2
provedou	provést	k5eAaPmIp3nP
rekognoskaci	rekognoskace	k1gFnSc4
terénu	terén	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
upřesní	upřesnit	k5eAaPmIp3nS
vedení	vedení	k1gNnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
příbramští	příbramský	k2eAgMnPc1d1
značkaři	značkař	k1gMnPc1
navrženou	navržený	k2eAgFnSc4d1
koncepci	koncepce	k1gFnSc4
rozšířili	rozšířit	k5eAaPmAgMnP
o	o	k7c4
spojovací	spojovací	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
do	do	k7c2
přilehlých	přilehlý	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
AOPK	AOPK	kA
v	v	k7c6
návrhu	návrh	k1gInSc6
z	z	k7c2
prosince	prosinec	k1gInSc2
2015	#num#	k4
navrhlo	navrhnout	k5eAaPmAgNnS
k	k	k7c3
20	#num#	k4
km	km	kA
už	už	k6eAd1
existujících	existující	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
dalších	další	k2eAgFnPc2d1
92,2	92,2	k4
km	km	kA
(	(	kIx(
<g/>
včetně	včetně	k7c2
zmíněných	zmíněný	k2eAgFnPc2d1
dvou	dva	k4xCgFnPc2
naučných	naučný	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
KČT	KČT	kA
z	z	k7c2
návrhu	návrh	k1gInSc2
AOPK	AOPK	kA
nevyužil	využít	k5eNaPmAgInS
17	#num#	k4
km	km	kA
<g/>
,	,	kIx,
ale	ale	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
(	(	kIx(
<g/>
v	v	k7c6
části	část	k1gFnSc6
v	v	k7c6
kompetenci	kompetence	k1gFnSc6
příbramských	příbramský	k2eAgMnPc2d1
značkařů	značkař	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
území	území	k1gNnSc2
bývalého	bývalý	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
)	)	kIx)
jiných	jiný	k2eAgFnPc2d1
41	#num#	k4
km	km	kA
turistických	turistický	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
toho	ten	k3xDgInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
rušeném	rušený	k2eAgInSc6d1
újezdu	újezd	k1gInSc6
mělo	mít	k5eAaImAgNnS
značit	značit	k5eAaImF
nových	nový	k2eAgInPc2d1
116	#num#	k4
km	km	kA
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Web	web	k1gInSc1
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
k	k	k7c3
návrhu	návrh	k1gInSc3
KČT	KČT	kA
připomínkuje	připomínkovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
nově	nově	k6eAd1
navržených	navržený	k2eAgFnPc2d1
pěších	pěší	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
pro	pro	k7c4
pěší	pěší	k1gMnPc4
vede	vést	k5eAaImIp3nS
po	po	k7c6
asfaltkách	asfaltka	k1gFnPc6
a	a	k8xC
betonových	betonový	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
jedna	jeden	k4xCgFnSc1
dokonce	dokonce	k9
po	po	k7c6
poměrně	poměrně	k6eAd1
frekventované	frekventovaný	k2eAgFnSc6d1
veřejné	veřejný	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
jednom	jeden	k4xCgInSc6
případě	případ	k1gInSc6
prakticky	prakticky	k6eAd1
neprůchodným	průchodný	k2eNgInSc7d1
bažinovitým	bažinovitý	k2eAgInSc7d1
terénem	terén	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
je	být	k5eAaImIp3nS
prý	prý	k9
poznamenán	poznamenán	k2eAgInSc1d1
malou	malý	k2eAgFnSc7d1
znalostí	znalost	k1gFnSc7
terénu	terén	k1gInSc2
<g/>
,	,	kIx,
nedostatečnou	dostatečný	k2eNgFnSc7d1
vzájemnou	vzájemný	k2eAgFnSc7d1
komunikací	komunikace	k1gFnSc7
a	a	k8xC
nerespektováním	nerespektování	k1gNnSc7
mínění	mínění	k1gNnSc2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
situaci	situace	k1gFnSc4
v	v	k7c6
místě	místo	k1gNnSc6
dobře	dobře	k6eAd1
znají	znát	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
KČT	KČT	kA
nejenže	nejenže	k6eAd1
se	se	k3xPyFc4
nepřiklonil	přiklonit	k5eNaPmAgInS
k	k	k7c3
původní	původní	k2eAgFnSc3d1
vizi	vize	k1gFnSc3
hlavní	hlavní	k2eAgFnSc2d1
hřebenové	hřebenový	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
dokonce	dokonce	k9
rezignoval	rezignovat	k5eAaBmAgMnS
i	i	k9
na	na	k7c4
návrh	návrh	k1gInSc4
AOPK	AOPK	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
alespoň	alespoň	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
vrchu	vrch	k1gInSc2
Praha	Praha	k1gFnSc1
vede	vést	k5eAaImIp3nS
pěší	pěší	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
po	po	k7c6
hlavním	hlavní	k2eAgInSc6d1
hřebeni	hřeben	k1gInSc6
<g/>
,	,	kIx,
takže	takže	k8xS
KČT	KČT	kA
na	na	k7c4
druhou	druhý	k4xOgFnSc4
nejvyšší	vysoký	k2eAgFnSc4d3
horu	hora	k1gFnSc4
Brd	Brdy	k1gInPc2
<g/>
,	,	kIx,
Prahu	Praha	k1gFnSc4
<g/>
,	,	kIx,
nevede	vést	k5eNaImIp3nS
vůbec	vůbec	k9
žádnou	žádný	k3yNgFnSc4
trasu	trasa	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
se	se	k3xPyFc4
jednou	jeden	k4xCgFnSc7
trasou	trasa	k1gFnSc7
vedenou	vedený	k2eAgFnSc7d1
po	po	k7c6
silnici	silnice	k1gFnSc6
vrcholu	vrchol	k1gInSc2
přibližuje	přibližovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cykloturistika	cykloturistika	k1gFnSc1
</s>
<s>
V	v	k7c6
okrajových	okrajový	k2eAgFnPc6d1
částech	část	k1gFnPc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc7
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
zrušením	zrušení	k1gNnSc7
vyznačeno	vyznačit	k5eAaPmNgNnS
asi	asi	k9
55	#num#	k4
km	km	kA
cyklotras	cyklotrasa	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2251	#num#	k4
Dobřív	Dobřív	k1gMnSc1
–	–	k?
Skořice	skořice	k1gFnSc1
(	(	kIx(
<g/>
3,8	3,8	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
2251A	2251A	k4
kóta	kóta	k1gFnSc1
526	#num#	k4
–	–	k?
Mirošov	Mirošov	k1gInSc1
(	(	kIx(
<g/>
3,0	3,0	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
2252	#num#	k4
Zaječov	Zaječov	k1gInSc1
–	–	k?
Strašice	Strašice	k1gFnPc1
(	(	kIx(
<g/>
8,9	8,9	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
2275	#num#	k4
Teslíny	Teslína	k1gFnSc2
–	–	k?
Václavka	václavka	k1gFnSc1
(	(	kIx(
<g/>
2,8	2,8	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
2274	#num#	k4
Červený	červený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
–	–	k?
Věšín	Věšín	k1gInSc1
(	(	kIx(
<g/>
4,2	4,2	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
2273	#num#	k4
Trokavec	Trokavec	k1gMnSc1
–	–	k?
Vranovice	Vranovice	k1gFnSc1
(	(	kIx(
<g/>
20,2	20,2	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
8190	#num#	k4
Rožmitál	Rožmitál	k1gMnSc1
–	–	k?
Bohutín	Bohutín	k1gMnSc1
(	(	kIx(
<g/>
5,3	5,3	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
8198	#num#	k4
Láz	Láz	k?
–	–	k?
Obecnice	obecnice	k1gFnSc2
(	(	kIx(
<g/>
7,4	7,4	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
byly	být	k5eAaImAgFnP
vedeny	vést	k5eAaImNgFnP
dvě	dva	k4xCgFnPc1
cyklotrasy	cyklotrasa	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
2147	#num#	k4
Borovno	Borovna	k1gFnSc5
–	–	k?
Nové	Nové	k2eAgFnPc1d1
Mitrovice	Mitrovice	k1gFnPc1
(	(	kIx(
<g/>
4,6	4,6	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
2039	#num#	k4
Hořehledy	Hořehled	k1gInPc7
–	–	k?
Nové	Nové	k2eAgFnSc2d1
Mitrovice	Mitrovice	k1gFnSc2
–	–	k?
Železný	železný	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
(	(	kIx(
<g/>
6,0	6,0	k4
km	km	kA
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
po	po	k7c6
hranici	hranice	k1gFnSc6
CHKO	CHKO	kA
<g/>
)	)	kIx)
</s>
<s>
Návrh	návrh	k1gInSc1
koncepce	koncepce	k1gFnSc2
cyklostezek	cyklostezka	k1gFnPc2
a	a	k8xC
turistických	turistický	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
pro	pro	k7c4
bývalý	bývalý	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Brdy	Brdy	k1gInPc7
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
zpracovala	zpracovat	k5eAaPmAgFnS
AOPK	AOPK	kA
ČR	ČR	kA
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
újezdním	újezdní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
a	a	k8xC
v	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
zveřejnila	zveřejnit	k5eAaPmAgFnS
na	na	k7c6
svém	svůj	k3xOyFgInSc6
webu	web	k1gInSc6
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
návrh	návrh	k1gInSc1
cyklotras	cyklotrasa	k1gFnPc2
(	(	kIx(
<g/>
nazývaných	nazývaný	k2eAgFnPc2d1
cyklostezky	cyklostezka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
délku	délka	k1gFnSc4
autoři	autor	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
jako	jako	k9
cca	cca	kA
450	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
však	však	k9
zveřejněné	zveřejněný	k2eAgInPc4d1
trasy	tras	k1gInPc4
přeměřil	přeměřit	k5eAaPmAgInS
a	a	k8xC
došel	dojít	k5eAaPmAgInS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
navržené	navržený	k2eAgFnPc4d1
cyklotrasy	cyklotrasa	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
již	již	k6eAd1
existujících	existující	k2eAgFnPc2d1
a	a	k8xC
včetně	včetně	k7c2
spojek	spojka	k1gFnPc2
k	k	k7c3
obcím	obec	k1gFnPc3
mimo	mimo	k6eAd1
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
<g/>
,	,	kIx,
měří	měřit	k5eAaImIp3nS
dohromady	dohromady	k6eAd1
pouze	pouze	k6eAd1
173,6	173,6	k4
km	km	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představuje	představovat	k5eAaImIp3nS
hustotu	hustota	k1gFnSc4
cyklostezek	cyklostezka	k1gFnPc2
cca	cca	kA
785	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
km²	km²	k?
(	(	kIx(
<g/>
do	do	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
nejsou	být	k5eNaImIp3nP
započtena	započíst	k5eAaPmNgNnP
trvale	trvale	k6eAd1
nepřístupná	přístupný	k2eNgNnPc1d1
výcviková	výcvikový	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrženou	navržený	k2eAgFnSc4d1
síť	síť	k1gFnSc4
však	však	k9
považují	považovat	k5eAaImIp3nP
autoři	autor	k1gMnPc1
webu	web	k1gInSc2
turistika-brdy	turistika-brda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
v	v	k7c6
podstatě	podstata	k1gFnSc6
za	za	k7c4
optimální	optimální	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyznačení	vyznačení	k1gNnSc1
cyklotras	cyklotrasa	k1gFnPc2
ovšem	ovšem	k9
proběhlo	proběhnout	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
;	;	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
12	#num#	k4
cyklotras	cyklotrasa	k1gFnPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
270	#num#	k4
km	km	kA
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
realizaci	realizace	k1gFnSc4
provedl	provést	k5eAaPmAgMnS
Turistický	turistický	k2eAgInSc4d1
spolek	spolek	k1gInSc4
Brdy	Brdy	k1gInPc7
a	a	k8xC
Podbrdsko	Podbrdsko	k1gNnSc1
za	za	k7c2
spolupráce	spolupráce	k1gFnSc2
KČT	KČT	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1
formy	forma	k1gFnPc1
turistiky	turistika	k1gFnSc2
</s>
<s>
V	v	k7c6
rozborech	rozbor	k1gInPc6
stavu	stav	k1gInSc2
území	území	k1gNnSc4
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
zřízením	zřízení	k1gNnSc7
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
zmiňována	zmiňován	k2eAgFnSc1d1
i	i	k8xC
módní	módní	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
čtyřkolek	čtyřkolka	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
negativní	negativní	k2eAgInSc4d1
i	i	k8xC
pozitivní	pozitivní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
přírodu	příroda	k1gFnSc4
–	–	k?
hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
zplodiny	zplodina	k1gFnPc4
<g/>
,	,	kIx,
úniky	únik	k1gInPc4
provozních	provozní	k2eAgFnPc2d1
kapalin	kapalina	k1gFnPc2
a	a	k8xC
lokální	lokální	k2eAgFnSc1d1
eroze	eroze	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
pozitivní	pozitivní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
rozrušování	rozrušování	k1gNnSc2
povrchu	povrch	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
může	moct	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
mikrolokality	mikrolokalita	k1gFnPc4
pro	pro	k7c4
obojživelníky	obojživelník	k1gMnPc4
nebo	nebo	k8xC
některé	některý	k3yIgFnPc4
konkurenčně	konkurenčně	k6eAd1
méně	málo	k6eAd2
zdatné	zdatný	k2eAgInPc1d1
druhy	druh	k1gInPc1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
obecně	obecně	k6eAd1
mít	mít	k5eAaImF
pozitivní	pozitivní	k2eAgInSc4d1
účinek	účinek	k1gInSc4
při	při	k7c6
potlačování	potlačování	k1gNnSc6
sukcesních	sukcesní	k2eAgInPc2d1
pochodů	pochod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přímo	přímo	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
nejsou	být	k5eNaImIp3nP
farmy	farma	k1gFnPc1
zabývající	zabývající	k2eAgFnPc1d1
se	s	k7c7
chovem	chov	k1gInSc7
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
jich	on	k3xPp3gNnPc2
několik	několik	k4yIc4
a	a	k8xC
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
je	být	k5eAaImIp3nS
území	území	k1gNnSc4
CHKO	CHKO	kA
atraktivní	atraktivní	k2eAgFnSc1d1
pro	pro	k7c4
vyjížďky	vyjížďka	k1gFnPc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
do	do	k7c2
budoucna	budoucno	k1gNnSc2
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
vytvořením	vytvoření	k1gNnSc7
hipostezek	hipostezka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Horolezecky	horolezecky	k6eAd1
nejatraktivnější	atraktivní	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
Jindřichova	Jindřichův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
jižně	jižně	k6eAd1
od	od	k7c2
Chaloupek	chaloupka	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
lezeckých	lezecký	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
různé	různý	k2eAgFnSc2d1
obtížnosti	obtížnost	k1gFnSc2
s	s	k7c7
trvalými	trvalý	k2eAgInPc7d1
jisticími	jisticí	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
zrušením	zrušení	k1gNnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
byla	být	k5eAaImAgNnP
využívána	využívat	k5eAaPmNgNnP,k5eAaImNgNnP
omezeně	omezeně	k6eAd1
na	na	k7c6
povolení	povolení	k1gNnSc6
újezdního	újezdní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
místními	místní	k2eAgMnPc7d1
horolezci	horolezec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občasné	občasný	k2eAgNnSc1d1
horolezecké	horolezecký	k2eAgNnSc1d1
využívání	využívání	k1gNnSc1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
i	i	k9
na	na	k7c6
dalších	další	k2eAgFnPc6d1
skalách	skála	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
Jezevčí	jezevčí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
u	u	k7c2
Dobříva	Dobřívo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
hlavní	hlavní	k2eAgNnSc4d1
negativum	negativum	k1gNnSc4
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
sešlap	sešlapat	k5eAaPmRp2nS
společenství	společenství	k1gNnSc2
lišejníků	lišejník	k1gInPc2
na	na	k7c6
hranách	hrana	k1gFnPc6
skal	skála	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Infrastruktura	infrastruktura	k1gFnSc1
</s>
<s>
Na	na	k7c6
okrajích	okraj	k1gInPc6
CHKO	CHKO	kA
má	mít	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
nejméně	málo	k6eAd3
pět	pět	k4xCc4
záchytných	záchytný	k2eAgNnPc2d1
parkovišť	parkoviště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
velkokapacitní	velkokapacitní	k2eAgMnPc4d1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
na	na	k7c6
okraji	okraj	k1gInSc6
vesnice	vesnice	k1gFnSc2
Obecnice	obecnice	k1gFnSc2
nedaleko	nedaleko	k7c2
nádrže	nádrž	k1gFnSc2
Octárna	octárna	k1gFnSc1
<g/>
,	,	kIx,
další	další	k2eAgNnPc1d1
nástupní	nástupní	k2eAgNnPc1d1
místa	místo	k1gNnPc1
jsou	být	k5eAaImIp3nP
plánována	plánován	k2eAgNnPc1d1
v	v	k7c6
Kozičíně	Kozičín	k1gInSc6
a	a	k8xC
v	v	k7c6
Orlově	orlově	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
výstavbou	výstavba	k1gFnSc7
občerstvovacích	občerstvovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
rozhleden	rozhledna	k1gFnPc2
<g/>
,	,	kIx,
parkovišť	parkoviště	k1gNnPc2
<g/>
,	,	kIx,
silničních	silniční	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
ani	ani	k8xC
lyžařských	lyžařský	k2eAgFnPc2d1
sjezdovek	sjezdovka	k1gFnPc2
uvnitř	uvnitř	k7c2
CHKO	CHKO	kA
se	se	k3xPyFc4
nepočítá	počítat	k5eNaImIp3nS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
však	však	k9
být	být	k5eAaImF
vybudováno	vybudovat	k5eAaPmNgNnS
několik	několik	k4yIc1
vyhlídkových	vyhlídkový	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
upraveny	upraven	k2eAgFnPc4d1
přístupové	přístupový	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
k	k	k7c3
nim	on	k3xPp3gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
byly	být	k5eAaImAgFnP
ve	v	k7c6
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
v	v	k7c6
rámci	rámec	k1gInSc6
přípravy	příprava	k1gFnSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
na	na	k7c6
zřízení	zřízení	k1gNnSc6
a	a	k8xC
zpřístupnění	zpřístupnění	k1gNnSc6
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Brdy	Brdy	k1gInPc1
vyznačeny	vyznačen	k2eAgInPc1d1
traumatologické	traumatologický	k2eAgInPc1d1
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
ochotná	ochotný	k2eAgFnSc1d1
jednat	jednat	k5eAaImF
o	o	k7c6
vzniku	vznik	k1gInSc6
povolených	povolený	k2eAgNnPc2d1
nocovišť	nocoviště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sama	sám	k3xTgFnSc1
však	však	k9
nevlastní	vlastnit	k5eNaImIp3nS
v	v	k7c6
CHKO	CHKO	kA
žádné	žádný	k3yNgInPc4
pozemky	pozemek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majoritními	majoritní	k2eAgMnPc7d1
vlastníky	vlastník	k1gMnPc7
jsou	být	k5eAaImIp3nP
Vojenské	vojenský	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
statky	statek	k1gInPc1
a	a	k8xC
Arcibiskupství	arcibiskupství	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
nepřiklánějí	přiklánět	k5eNaImIp3nP
<g/>
,	,	kIx,
protože	protože	k8xS
to	ten	k3xDgNnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
odpadků	odpadek	k1gInPc2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
připravit	připravit	k5eAaPmF
místo	místo	k1gNnSc4
pro	pro	k7c4
rozdělávání	rozdělávání	k1gNnSc4
ohně	oheň	k1gInSc2
<g/>
,	,	kIx,
majitel	majitel	k1gMnSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
o	o	k7c4
nocoviště	nocoviště	k1gNnSc4
starat	starat	k5eAaImF
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
tam	tam	k6eAd1
vzniknout	vzniknout	k5eAaPmF
hygienické	hygienický	k2eAgNnSc4d1
a	a	k8xC
sociální	sociální	k2eAgNnSc4d1
zázemí	zázemí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Publikace	publikace	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
vyšel	vyjít	k5eAaPmAgMnS
knižní	knižní	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
po	po	k7c6
Brdech	brdo	k1gNnPc6
s	s	k7c7
názvem	název	k1gInSc7
Brdy	Brdy	k1gInPc7
opět	opět	k6eAd1
otevřené	otevřený	k2eAgNnSc1d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Hajšman	Hajšman	k1gMnSc1
(	(	kIx(
<g/>
již	již	k6eAd1
dříve	dříve	k6eAd2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
knihu	kniha	k1gFnSc4
Tajemství	tajemství	k1gNnSc2
brdských	brdský	k2eAgInPc2d1
vrcholů	vrchol	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydalo	vydat	k5eAaPmAgNnS
jej	on	k3xPp3gMnSc4
nakladatelství	nakladatelství	k1gNnSc2
Starý	starý	k2eAgInSc4d1
most	most	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
uspořádán	uspořádán	k2eAgInSc1d1
jako	jako	k8xS,k8xC
abecední	abecední	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
s	s	k7c7
350	#num#	k4
místopisnými	místopisný	k2eAgNnPc7d1
hesly	heslo	k1gNnPc7
a	a	k8xC
300	#num#	k4
ilustračními	ilustrační	k2eAgFnPc7d1
fotografiemi	fotografia	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
obce	obec	k1gFnPc4
<g/>
,	,	kIx,
vrcholy	vrchol	k1gInPc4
a	a	k8xC
skály	skála	k1gFnPc4
<g/>
,	,	kIx,
historické	historický	k2eAgFnPc4d1
a	a	k8xC
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgInPc4d1
objekty	objekt	k1gInPc4
<g/>
,	,	kIx,
zaniklé	zaniklý	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
vodní	vodní	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
<g/>
,	,	kIx,
hájovny	hájovna	k1gFnPc1
a	a	k8xC
boudy	bouda	k1gFnPc1
včetně	včetně	k7c2
zaniklých	zaniklý	k2eAgInPc2d1
a	a	k8xC
kříže	kříž	k1gInPc4
<g/>
,	,	kIx,
pomníky	pomník	k1gInPc4
a	a	k8xC
mezníky	mezník	k1gInPc4
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
kromě	kromě	k7c2
území	území	k1gNnSc2
bývalého	bývalý	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
i	i	k8xC
část	část	k1gFnSc4
jižních	jižní	k2eAgInPc2d1
Brd	Brdy	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turistické	turistický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
prvních	první	k4xOgFnPc6
hodinách	hodina	k1gFnPc6
roku	rok	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgInS
o	o	k7c4
návštěvu	návštěva	k1gFnSc4
dosud	dosud	k6eAd1
nepřístupných	přístupný	k2eNgNnPc2d1
míst	místo	k1gNnPc2
v	v	k7c6
nové	nový	k2eAgFnSc6d1
chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
značný	značný	k2eAgInSc4d1
zájem	zájem	k1gInSc4
ze	z	k7c2
strany	strana	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Od	od	k7c2
Nového	Nového	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
Brdy	Brdy	k1gInPc1
otevřou	otevřít	k5eAaPmIp3nP
turistům	turist	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cest	cesta	k1gFnPc2
je	být	k5eAaImIp3nS
mnoho	mnoho	k6eAd1
<g/>
,	,	kIx,
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
ale	ale	k9
značení	značení	k1gNnSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Šrámková	Šrámková	k1gFnSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
KLAPALOVÁ	KLAPALOVÁ	kA
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
Brdech	brdo	k1gNnPc6
bude	být	k5eAaImBp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
vojenská	vojenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
obce	obec	k1gFnPc1
čekají	čekat	k5eAaImIp3nP
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-19	2011-09-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
HODRMENT	HODRMENT	kA
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
ČR	ČR	kA
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c6
vyhlášení	vyhlášení	k1gNnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
www.turistika-brdy.cz	www.turistika-brdy.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rozbory	rozbor	k1gInPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Brdy	Brdy	k1gInPc1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
41	#num#	k4
2	#num#	k4
3	#num#	k4
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
TOLAR	tolar	k1gInSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
;	;	kIx,
ŠRÁMKOVÁ	Šrámková	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
zrušila	zrušit	k5eAaPmAgFnS
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Brdy	Brdy	k1gInPc7
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
cvičiště	cvičiště	k1gNnPc1
se	se	k3xPyFc4
zmenší	zmenšit	k5eAaPmIp3nP
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BERNÝ	berný	k2eAgInSc1d1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
;	;	kIx,
ROKOSOVÁ	Rokosová	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdělení	rozdělení	k1gNnSc1
Brd	Brdy	k1gInPc2
mezi	mezi	k7c7
kraje	kraj	k1gInSc2
nebude	být	k5eNaImBp3nS
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
,	,	kIx,
shodli	shodnout	k5eAaBmAgMnP,k5eAaPmAgMnP
se	se	k3xPyFc4
hejtmané	hejtmaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-03	2011-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hlasování	hlasování	k1gNnSc1
Senátu	senát	k1gInSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
<g/>
,	,	kIx,
37	#num#	k4
<g/>
.	.	kIx.
hlasování	hlasování	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2015-01-14	2015-01-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Očista	očista	k1gFnSc1
Brd	Brdy	k1gInPc2
skončila	skončit	k5eAaPmAgFnS
<g/>
,	,	kIx,
pyrotechnici	pyrotechnik	k1gMnPc1
našli	najít	k5eAaPmAgMnP
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
kusů	kus	k1gInPc2
nevybuchlé	vybuchlý	k2eNgFnSc2d1
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbramský	příbramský	k2eAgInSc1d1
deník	deník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VLTAVA	Vltava	k1gFnSc1
LABE	Labe	k1gNnSc2
MEDIA	medium	k1gNnSc2
a.s.	a.s.	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
↑	↑	k?
Mapu	mapa	k1gFnSc4
hranic	hranice	k1gFnPc2
a	a	k8xC
vymezení	vymezení	k1gNnSc2
zón	zóna	k1gFnPc2
ochrany	ochrana	k1gFnSc2
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
↑	↑	k?
Nařízení	nařízení	k1gNnSc2
vlády	vláda	k1gFnSc2
292	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
webu	web	k1gInSc2
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
(	(	kIx(
<g/>
bez	bez	k7c2
příloh	příloha	k1gFnPc2
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	brdo	k1gNnPc7
(	(	kIx(
<g/>
mapa	mapa	k1gFnSc1
maloplošných	maloplošný	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
<g/>
↑	↑	k?
Mapa	mapa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
(	(	kIx(
<g/>
existující	existující	k2eAgNnPc1d1
a	a	k8xC
navrhovaná	navrhovaný	k2eAgNnPc1d1
maloplošná	maloplošný	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
,	,	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
↑	↑	k?
Vojáci	voják	k1gMnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
předali	předat	k5eAaPmAgMnP
Brdy	Brdy	k1gInPc4
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
turistické	turistický	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
značit	značit	k5eAaImF
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-15	2016-01-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Plán	plán	k1gInSc1
péče	péče	k1gFnSc2
o	o	k7c6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
–	–	k?
Rozbory	rozbor	k1gInPc7
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013.1	2013.1	k4
2	#num#	k4
Dohody	dohoda	k1gFnPc1
s	s	k7c7
armádou	armáda	k1gFnSc7
selhaly	selhat	k5eAaPmAgFnP
<g/>
,	,	kIx,
turistické	turistický	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
v	v	k7c6
Brdech	Brdy	k1gInPc6
se	se	k3xPyFc4
nestihnou	stihnout	k5eNaPmIp3nP
vyznačit	vyznačit	k5eAaPmF
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-08-17	2015-08-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Martina	Martina	k1gFnSc1
Vyroubalová	Vyroubalová	k1gFnSc1
<g/>
:	:	kIx,
Na	na	k7c4
Tok	tok	k1gInSc4
i	i	k9
po	po	k7c6
hřebeni	hřeben	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turisté	turist	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
vyznačit	vyznačit	k5eAaPmF
cesty	cesta	k1gFnPc4
v	v	k7c6
Brdech	Brdy	k1gInPc6
ještě	ještě	k9
letos	letos	k6eAd1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
20151	#num#	k4
2	#num#	k4
3	#num#	k4
Už	už	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
budou	být	k5eAaImBp3nP
v	v	k7c6
Brdech	brdo	k1gNnPc6
značené	značený	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
vrch	vrch	k1gInSc4
Tok	tok	k1gInSc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
nesmí	smět	k5eNaImIp3nS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-11-04	2015-11-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
návrh	návrh	k1gInSc4
nařízení	nařízení	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
se	se	k3xPyFc4
vyhlašuje	vyhlašovat	k5eAaImIp3nS
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	brdo	k1gNnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
vláda	vláda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikne	vzniknout	k5eAaPmIp3nS
nová	nový	k2eAgFnSc1d1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Nová	Nová	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
nabídne	nabídnout	k5eAaPmIp3nS
450	#num#	k4
kilometrů	kilometr	k1gInPc2
značených	značený	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
a	a	k8xC
tři	tři	k4xCgFnPc1
naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-11-16	2015-11-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Marek	Marek	k1gMnSc1
Kočovský	kočovský	k2eAgMnSc1d1
<g/>
:	:	kIx,
Brdy	Brdy	k1gInPc1
hlásí	hlásit	k5eAaImIp3nP
doznačeno	doznačit	k5eAaPmNgNnS
<g/>
,	,	kIx,
pro	pro	k7c4
pěší	pěší	k1gMnPc4
je	být	k5eAaImIp3nS
připraveno	připravit	k5eAaPmNgNnS
250	#num#	k4
kilometrů	kilometr	k1gInPc2
tras	trasa	k1gFnPc2
<g/>
,	,	kIx,
iDnes	iDnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
20181	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
JUDr.	JUDr.	kA
Pavel	Pavel	k1gMnSc1
Čámský	Čámský	k1gMnSc1
<g/>
:	:	kIx,
Značené	značený	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
v	v	k7c6
Brdech	Brdy	k1gInPc6
<g/>
,	,	kIx,
turistika-brdy	turistika-brdy	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
↑	↑	k?
Lesníci	lesník	k1gMnPc1
a	a	k8xC
turisté	turist	k1gMnPc1
spojili	spojit	k5eAaPmAgMnP
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
chtějí	chtít	k5eAaImIp3nP
chránit	chránit	k5eAaImF
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniknou	vzniknout	k5eAaPmIp3nP
cyklotrasy	cyklotrasa	k1gFnPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nové	Nové	k2eAgFnSc2d1
cyklotrasy	cyklotrasa	k1gFnSc2
v	v	k7c6
Brdech	Brdy	k1gInPc6
si	se	k3xPyFc3
užijí	užít	k5eAaPmIp3nP
cyklisté	cyklista	k1gMnPc1
od	od	k7c2
půlky	půlka	k1gFnSc2
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávy	zpráva	k1gFnSc2
Příbram	Příbram	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martin	Martin	k1gMnSc1
Poulíček	Poulíček	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Jinečtí	jinecký	k2eAgMnPc1d1
hasiči	hasič	k1gMnPc1
se	se	k3xPyFc4
nenudí	nudit	k5eNaImIp3nP
<g/>
“	“	k?
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Ze	z	k7c2
života	život	k1gInSc2
Jinec	Jince	k1gInPc2
PN	PN	kA
21	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
Podbrdské	podbrdský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
.	.	kIx.
-ung-	-ung-	k?
<g/>
↑	↑	k?
Jitka	Jitka	k1gFnSc1
Šrámková	Šrámková	k1gFnSc1
<g/>
:	:	kIx,
Nocoviště	nocoviště	k1gNnSc1
v	v	k7c6
Brdech	Brdy	k1gInPc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jsme	být	k5eAaImIp1nP
ochotní	ochotný	k2eAgMnPc1d1
o	o	k7c6
nich	on	k3xPp3gInPc6
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
správce	správce	k1gMnSc1
CHKO	CHKO	kA
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
↑	↑	k?
Václav	Václav	k1gMnSc1
Prokš	Prokš	k1gMnSc1
<g/>
:	:	kIx,
Vyšel	vyjít	k5eAaPmAgMnS
první	první	k4xOgMnSc1
turistický	turistický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
po	po	k7c6
Brdech	Brdy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gInSc1
představí	představit	k5eAaPmIp3nS
i	i	k9
území	území	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
zůstane	zůstat	k5eAaPmIp3nS
nepřístupné	přístupný	k2eNgNnSc1d1
Archivováno	archivován	k2eAgNnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Ekolist	Ekolist	k1gMnSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
↑	↑	k?
KOŘÍNEK	Kořínek	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čerstvě	čerstvě	k6eAd1
zpřístupněné	zpřístupněný	k2eAgInPc1d1
Brdy	Brdy	k1gInPc1
<g/>
:	:	kIx,
Ticho	ticho	k1gNnSc1
<g/>
,	,	kIx,
jinovatka	jinovatka	k1gFnSc1
a	a	k8xC
varovné	varovný	k2eAgFnPc1d1
cedule	cedule	k1gFnPc1
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-04	2016-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Brdy	Brdy	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
významných	významný	k2eAgInPc2d1
druhů	druh	k1gInPc2
v	v	k7c6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc7
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Brdy	Brdy	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Turistické	turistický	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
o	o	k7c6
Brdech	brdo	k1gNnPc6
POZNEJBRDY	POZNEJBRDY	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
zrušení	zrušení	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
o	o	k7c6
stanovení	stanovení	k1gNnSc6
hranic	hranice	k1gFnPc2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
změně	změna	k1gFnSc6
hranic	hranice	k1gFnPc2
krajů	kraj	k1gInPc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
souvisejících	související	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hranicích	hranice	k1gFnPc6
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Nařízení	nařízení	k1gNnPc1
vlády	vláda	k1gFnSc2
č.	č.	k?
292	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Brdy	Brdy	k1gInPc1
</s>
<s>
Záměr	záměr	k1gInSc1
na	na	k7c6
vyhlášení	vyhlášení	k1gNnSc6
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
Podrobná	podrobný	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
a	a	k8xC
charakteristika	charakteristika	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc1
<g/>
,	,	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
</s>
<s>
Karel	Karel	k1gMnSc1
Hutr	Hutr	k1gMnSc1
<g/>
:	:	kIx,
CHKO	CHKO	kA
Brdy	Brdy	k1gInPc4
má	mít	k5eAaImIp3nS
mít	mít	k5eAaImF
330	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
,	,	kIx,
pohltí	pohltit	k5eAaPmIp3nS
i	i	k9
Třemšínsko	Třemšínsko	k1gNnSc1
<g/>
,	,	kIx,
Příbramský	příbramský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
(	(	kIx(
<g/>
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
mapku	mapka	k1gFnSc4
navrženého	navržený	k2eAgNnSc2d1
CHKO	CHKO	kA
<g/>
)	)	kIx)
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1
CHKO	CHKO	kA
v	v	k7c6
Brdech	Brdy	k1gInPc6
po	po	k7c6
odchodu	odchod	k1gInSc6
armády	armáda	k1gFnSc2
je	být	k5eAaImIp3nS
nejlepším	dobrý	k2eAgNnSc7d3
řešením	řešení	k1gNnSc7
<g/>
,	,	kIx,
říkají	říkat	k5eAaImIp3nP
starostové	starosta	k1gMnPc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
Zprávy	zpráva	k1gFnPc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kološ	Kološ	k1gMnSc1
(	(	kIx(
<g/>
pek	pek	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Kroc	Kroc	k1gFnSc1
<g/>
,	,	kIx,
bre	bre	k?
</s>
<s>
10	#num#	k4
turistických	turistický	k2eAgInPc2d1
cílů	cíl	k1gInPc2
v	v	k7c6
Brdech	Brdy	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Beroun	Beroun	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hřebeny	hřeben	k1gInPc1
•	•	k?
Povodí	povodí	k1gNnSc1
Kačáku	Kačák	k1gInSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1
•	•	k?
Koda	Kod	k1gInSc2
•	•	k?
Týřov	Týřov	k1gInSc1
•	•	k?
Vůznice	Vůznice	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Klonk	Klonk	k1gMnSc1
•	•	k?
Kotýz	Kotýz	k1gMnSc1
•	•	k?
Zlatý	zlatý	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Jouglovka	Jouglovka	k1gFnSc1
•	•	k?
Karlické	Karlický	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Kobyla	kobyla	k1gFnSc1
•	•	k?
Na	na	k7c6
Voskopě	Voskopa	k1gFnSc6
•	•	k?
Tetínské	Tetínský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Voškov	Voškov	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
památky	památka	k1gFnSc2
</s>
<s>
Branžovy	Branžův	k2eAgInPc1d1
•	•	k?
Housina	Housin	k2eAgFnSc1d1
•	•	k?
Jindřichova	Jindřichův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Lom	lom	k1gInSc1
Kozolupy	Kozolupa	k1gFnSc2
•	•	k?
Lounín	Lounín	k1gMnSc1
•	•	k?
Otmíčská	Otmíčský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Stará	starý	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Stroupínský	Stroupínský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Studánky	studánka	k1gFnSc2
u	u	k7c2
Cerhovic	Cerhovice	k1gFnPc2
•	•	k?
Syslí	syslí	k2eAgFnSc2d1
louky	louka	k1gFnSc2
u	u	k7c2
Loděnice	loděnice	k1gFnSc2
•	•	k?
Špičatý	špičatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
–	–	k?
Barrandovy	Barrandov	k1gInPc4
jámy	jáma	k1gFnSc2
•	•	k?
Trubínský	Trubínský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Vraní	vraní	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Zahořanský	Zahořanský	k2eAgMnSc1d1
stratotyp	stratotyp	k1gMnSc1
•	•	k?
Zdická	zdický	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
u	u	k7c2
Kublova	Kublův	k2eAgInSc2d1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Plzeň-jih	Plzeň-jiha	k1gFnPc2
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Plánický	Plánický	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
–	–	k?
Kákov	Kákov	k1gInSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Chejlava	Chejlava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Fajmanovy	Fajmanův	k2eAgFnPc1d1
skály	skála	k1gFnPc1
a	a	k8xC
Klenky	klenek	k1gInPc1
•	•	k?
Chynínské	Chynínský	k2eAgInPc1d1
buky	buk	k1gInPc1
•	•	k?
Kokšín	Kokšín	k1gInSc1
•	•	k?
Lopata	lopata	k1gFnSc1
•	•	k?
Polánecký	Polánecký	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
památky	památka	k1gFnSc2
</s>
<s>
Bouřidla	Bouřidlo	k1gNnSc2
•	•	k?
Hádky	hádka	k1gFnSc2
•	•	k?
Hořehledy	Hořehled	k1gInPc1
•	•	k?
Loupensko	Loupensko	k1gNnSc4
•	•	k?
Lužany	Lužana	k1gFnSc2
•	•	k?
Míšovské	Míšovský	k2eAgInPc1d1
buky	buk	k1gInPc1
•	•	k?
Novoveská	Novoveský	k2eAgFnSc1d1
draha	draha	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Smutným	smutný	k2eAgInSc7d1
koutem	kout	k1gInSc7
•	•	k?
Šlovický	Šlovický	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
V	v	k7c6
Houlištích	Houliště	k1gNnPc6
•	•	k?
Vojovická	Vojovický	k2eAgFnSc1d1
draha	draha	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Příbram	Příbram	k1gFnSc4
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hřebeny	hřeben	k1gInPc1
•	•	k?
Petrovicko	Petrovicko	k1gNnSc4
•	•	k?
Třemšín	Třemšín	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Drbákov	Drbákov	k1gInSc4
–	–	k?
Albertovy	Albertův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
•	•	k?
Getsemanka	Getsemanka	k1gFnSc1
•	•	k?
Hradec	Hradec	k1gInSc1
•	•	k?
Jezero	jezero	k1gNnSc1
•	•	k?
Kuchyňka	Kuchyňka	k1gMnSc1
•	•	k?
Na	na	k7c6
skalách	skála	k1gFnPc6
•	•	k?
Vymyšlenská	Vymyšlenský	k2eAgFnSc1d1
pěšina	pěšina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
•	•	k?
Bezděkovský	Bezděkovský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Bohostice	Bohostika	k1gFnSc6
•	•	k?
Březnice	Březnice	k1gFnSc2
–	–	k?
Oblouček	oblouček	k1gInSc1
•	•	k?
Dobříšský	dobříšský	k2eAgInSc1d1
park	park	k1gInSc1
•	•	k?
Dobříšský	dobříšský	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
a	a	k8xC
Dolní	dolní	k2eAgInSc1d1
obděnický	obděnický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
solopyský	solopyský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Hřebenec	Hřebenec	k1gInSc1
•	•	k?
Husova	Husův	k2eAgFnSc1d1
kazatelna	kazatelna	k1gFnSc1
•	•	k?
Jablonná	Jablonná	k1gFnSc1
–	–	k?
mokřad	mokřad	k1gInSc1
•	•	k?
Jezera	jezero	k1gNnSc2
•	•	k?
Jezírko	jezírko	k1gNnSc1
u	u	k7c2
Dobříše	Dobříš	k1gFnSc2
•	•	k?
Kosova	Kosův	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Drahlína	Drahlín	k1gInSc2
•	•	k?
Na	na	k7c6
horách	hora	k1gFnPc6
•	•	k?
Pařezitý	Pařezitý	k2eAgInSc1d1
•	•	k?
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gMnSc7
•	•	k?
Starý	starý	k2eAgMnSc1d1
u	u	k7c2
Líchov	Líchovo	k1gNnPc2
•	•	k?
Rybník	rybník	k1gInSc1
Vočert	Vočert	k1gMnSc1
a	a	k8xC
Lazy	Lazy	k?
•	•	k?
Štola	štola	k1gFnSc1
Jarnice	Jarnice	k1gFnSc2
•	•	k?
Třemešný	Třemešný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Vápenické	vápenický	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Raputovský	Raputovský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vinice	vinice	k1gFnSc2
•	•	k?
Vrškámen	Vrškámen	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Rokycany	Rokycany	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Berounka	Berounka	k1gFnSc1
•	•	k?
Trhoň	Trhoň	k1gFnSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Chlumská	chlumský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kohoutov	Kohoutov	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Vosek	Vosek	k6eAd1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Skryjská	Skryjský	k2eAgNnPc1d1
jezírka	jezírko	k1gNnPc1
•	•	k?
Lípa	lípa	k1gFnSc1
•	•	k?
Louky	louka	k1gFnSc2
pod	pod	k7c7
Palcířem	Palcíř	k1gInSc7
•	•	k?
Třímanské	třímanský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
V	v	k7c6
Horách	hora	k1gFnPc6
•	•	k?
Zvoníčkovna	Zvoníčkovna	k1gFnSc1
•	•	k?
Žďár	Žďár	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bašta	Bašta	k1gMnSc1
•	•	k?
Biskoupky	Biskoupka	k1gFnSc2
•	•	k?
Ejpovické	ejpovický	k2eAgInPc4d1
útesy	útes	k1gInPc4
•	•	k?
Hrádecká	hrádecký	k2eAgFnSc1d1
bahna	bahno	k1gNnSc2
•	•	k?
Jalovce	jalovec	k1gInPc1
na	na	k7c6
Světovině	Světovina	k1gFnSc6
•	•	k?
Kakejcov	Kakejcov	k1gInSc1
•	•	k?
Kamenec	Kamenec	k1gInSc4
•	•	k?
Kařezské	Kařezský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Kašparův	Kašparův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Kateřina	Kateřina	k1gFnSc1
•	•	k?
Medový	medový	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Niva	niva	k1gFnSc1
u	u	k7c2
Volduch	Volducha	k1gFnPc2
•	•	k?
Pod	pod	k7c7
Starým	starý	k2eAgInSc7d1
hradem	hrad	k1gInSc7
•	•	k?
Rokycanská	rokycanský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Rumpál	rumpál	k1gInSc1
•	•	k?
Štěpánský	štěpánský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
U	u	k7c2
hřbitova	hřbitov	k1gInSc2
•	•	k?
Zavírka	zavírka	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
904277	#num#	k4
</s>
