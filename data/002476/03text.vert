<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1923	[number]	k4	1923
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1994	[number]	k4	1994
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
především	především	k9	především
psychologické	psychologický	k2eAgFnSc2d1	psychologická
prózy	próza	k1gFnSc2	próza
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
úzkosti	úzkost	k1gFnSc2	úzkost
člověka	člověk	k1gMnSc2	člověk
ohrožovaného	ohrožovaný	k2eAgMnSc2d1	ohrožovaný
nesvobodou	nesvoboda	k1gFnSc7	nesvoboda
a	a	k8xC	a
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
tohoto	tento	k3xDgNnSc2	tento
tématu	téma	k1gNnSc2	téma
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
zvolil	zvolit	k5eAaPmAgMnS	zvolit
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
holokaust	holokaust	k1gInSc4	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
skrytě	skrytě	k6eAd1	skrytě
-	-	kIx~	-
téměř	téměř	k6eAd1	téměř
všemi	všecek	k3xTgFnPc7	všecek
jeho	jeho	k3xOp3gFnPc7	jeho
knihami	kniha	k1gFnPc7	kniha
prochází	procházet	k5eAaImIp3nS	procházet
figura	figura	k1gFnSc1	figura
senzitivního	senzitivní	k2eAgMnSc2d1	senzitivní
<g/>
,	,	kIx,	,
slabého	slabý	k2eAgMnSc2d1	slabý
hocha	hoch	k1gMnSc2	hoch
<g/>
,	,	kIx,	,
žijícího	žijící	k2eAgInSc2d1	žijící
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
světě	svět	k1gInSc6	svět
a	a	k8xC	a
toužícího	toužící	k2eAgInSc2d1	toužící
po	po	k7c6	po
citovém	citový	k2eAgNnSc6d1	citové
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
vracející	vracející	k2eAgFnSc1d1	vracející
postava	postava	k1gFnSc1	postava
trpícího	trpící	k2eAgNnSc2d1	trpící
a	a	k8xC	a
mučeného	mučený	k2eAgMnSc2d1	mučený
chlapce	chlapec	k1gMnSc2	chlapec
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
míru	míra	k1gFnSc4	míra
autobiografičnosti	autobiografičnost	k1gFnSc2	autobiografičnost
<g/>
.	.	kIx.	.
</s>
<s>
Fuksovo	Fuksův	k2eAgNnSc1d1	Fuksovo
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
také	také	k9	také
autobiografickou	autobiografický	k2eAgFnSc7d1	autobiografická
travestií	travestie	k1gFnSc7	travestie
-	-	kIx~	-
např.	např.	kA	např.
Vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
a	a	k8xC	a
kuchařka	kuchařka	k1gFnSc1	kuchařka
<g/>
.	.	kIx.	.
</s>
<s>
Fuks	Fuks	k1gMnSc1	Fuks
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
též	též	k9	též
mistrem	mistr	k1gMnSc7	mistr
masky	maska	k1gFnSc2	maska
<g/>
,	,	kIx,	,
jinotajů	jinotaj	k1gInPc2	jinotaj
a	a	k8xC	a
náznaků	náznak	k1gInPc2	náznak
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
byl	být	k5eAaImAgMnS	být
jako	jako	k8xS	jako
homosexuál	homosexuál	k1gMnSc1	homosexuál
přirozeně	přirozeně	k6eAd1	přirozeně
donucen	donutit	k5eAaPmNgMnS	donutit
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgInS	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
odhalitelných	odhalitelný	k2eAgInPc2d1	odhalitelný
žertů	žert	k1gInPc2	žert
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
čtenáři	čtenář	k1gMnPc7	čtenář
nejednou	jednou	k6eNd1	jednou
hraje	hrát	k5eAaImIp3nS	hrát
rafinovanou	rafinovaný	k2eAgFnSc4d1	rafinovaná
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mnohým	mnohé	k1gNnSc7	mnohé
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
neodhalena	odhalen	k2eNgFnSc1d1	neodhalena
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	on	k3xPp3gNnSc2	on
dětství	dětství	k1gNnSc2	dětství
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgMnSc1d1	policejní
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
chladný	chladný	k2eAgInSc1d1	chladný
a	a	k8xC	a
dominantní	dominantní	k2eAgMnSc1d1	dominantní
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
správnou	správný	k2eAgFnSc4d1	správná
společenskou	společenský	k2eAgFnSc4d1	společenská
výchovu	výchova	k1gFnSc4	výchova
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
smutného	smutný	k2eAgNnSc2d1	smutné
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
vzdor	vzdor	k1gInSc4	vzdor
proti	proti	k7c3	proti
otci	otec	k1gMnSc3	otec
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
jeho	jeho	k3xOp3gFnPc6	jeho
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
životní	životní	k2eAgFnSc7d1	životní
krizí	krize	k1gFnSc7	krize
ale	ale	k8xC	ale
prošel	projít	k5eAaPmAgMnS	projít
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
počátku	počátek	k1gInSc2	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
svou	svůj	k3xOyFgFnSc4	svůj
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Krutý	krutý	k2eAgInSc1d1	krutý
osud	osud	k1gInSc1	osud
svých	svůj	k3xOyFgMnPc2	svůj
židovských	židovský	k2eAgMnPc2d1	židovský
spolužáků	spolužák	k1gMnPc2	spolužák
z	z	k7c2	z
gymnázia	gymnázium	k1gNnSc2	gymnázium
prožíval	prožívat	k5eAaImAgMnS	prožívat
citelněji	citelně	k6eAd2	citelně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
sám	sám	k3xTgInSc1	sám
trpěl	trpět	k5eAaImAgInS	trpět
silným	silný	k2eAgInSc7d1	silný
pocitem	pocit	k1gInSc7	pocit
ohrožení	ohrožení	k1gNnSc2	ohrožení
-	-	kIx~	-
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
tehdy	tehdy	k6eAd1	tehdy
mizí	mizet	k5eAaImIp3nP	mizet
nejen	nejen	k6eAd1	nejen
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
homosexuálové	homosexuál	k1gMnPc1	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
literární	literární	k2eAgMnPc1d1	literární
kritici	kritik	k1gMnPc1	kritik
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
často	často	k6eAd1	často
kladli	klást	k5eAaImAgMnP	klást
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nežidovský	židovský	k2eNgMnSc1d1	nežidovský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
tak	tak	k6eAd1	tak
vžít	vžít	k5eAaPmF	vžít
do	do	k7c2	do
pocitů	pocit	k1gInPc2	pocit
židovské	židovský	k2eAgFnSc2d1	židovská
menšiny	menšina	k1gFnSc2	menšina
a	a	k8xC	a
věnovat	věnovat	k5eAaImF	věnovat
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
jeho	on	k3xPp3gInSc4	on
pocit	pocit	k1gInSc4	pocit
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
s	s	k7c7	s
tragédií	tragédie	k1gFnSc7	tragédie
tohoto	tento	k3xDgInSc2	tento
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
úzkost	úzkost	k1gFnSc4	úzkost
z	z	k7c2	z
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
jeho	jeho	k3xOp3gNnSc7	jeho
silným	silný	k2eAgNnSc7d1	silné
duchovním	duchovní	k2eAgNnSc7d1	duchovní
poutem	pouto	k1gNnSc7	pouto
s	s	k7c7	s
pronásledovanou	pronásledovaný	k2eAgFnSc7d1	pronásledovaná
židovskou	židovský	k2eAgFnSc7d1	židovská
menšinou	menšina	k1gFnSc7	menšina
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
také	také	k9	také
název	název	k1gInSc1	název
jedné	jeden	k4xCgFnSc2	jeden
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
-	-	kIx~	-
Mí	můj	k3xOp1gMnPc1	můj
černovlasí	černovlasý	k2eAgMnPc1d1	černovlasý
bratři	bratr	k1gMnPc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
gymnázia	gymnázium	k1gNnSc2	gymnázium
pracoval	pracovat	k5eAaImAgMnS	pracovat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
za	za	k7c2	za
války	válka	k1gFnSc2	válka
na	na	k7c6	na
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
správě	správa	k1gFnSc6	správa
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
psychologii	psychologie	k1gFnSc3	psychologie
a	a	k8xC	a
dějinám	dějiny	k1gFnPc3	dějiny
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
zdárně	zdárně	k6eAd1	zdárně
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgInS	pracovat
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
úředník	úředník	k1gMnSc1	úředník
v	v	k7c6	v
papírnách	papírna	k1gFnPc6	papírna
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
Státní	státní	k2eAgFnSc2d1	státní
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
kuriozit	kuriozita	k1gFnPc2	kuriozita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
zámku	zámek	k1gInSc6	zámek
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c6	po
knížeti	kníže	k1gMnSc6	kníže
Metternichovi	Metternich	k1gMnSc6	Metternich
<g/>
,	,	kIx,	,
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
natolik	natolik	k6eAd1	natolik
nadchla	nadchnout	k5eAaPmAgFnS	nadchnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
něco	něco	k3yInSc1	něco
obdobného	obdobný	k2eAgNnSc2d1	obdobné
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
bytu	byt	k1gInSc2	byt
kde	kde	k6eAd1	kde
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Bubenči	Bubeneč	k1gInSc6	Bubeneč
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
jeho	on	k3xPp3gInSc4	on
nesourodý	sourodý	k2eNgInSc4d1	nesourodý
soubor	soubor	k1gInSc4	soubor
morbidních	morbidní	k2eAgFnPc2d1	morbidní
bizarností	bizarnost	k1gFnPc2	bizarnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
už	už	k6eAd1	už
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
legendou	legenda	k1gFnSc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
až	až	k9	až
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
konečně	konečně	k6eAd1	konečně
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
-	-	kIx~	-
Pan	Pan	k1gMnSc1	Pan
Theodor	Theodor	k1gMnSc1	Theodor
Mundstock	Mundstock	k1gMnSc1	Mundstock
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jeho	jeho	k3xOp3gInSc1	jeho
vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
literárního	literární	k2eAgNnSc2d1	literární
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
doslova	doslova	k6eAd1	doslova
bleskem	blesk	k1gInSc7	blesk
z	z	k7c2	z
čistého	čistý	k2eAgNnSc2d1	čisté
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
z	z	k7c2	z
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
během	během	k7c2	během
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
nejen	nejen	k6eAd1	nejen
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
ale	ale	k8xC	ale
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
politickému	politický	k2eAgInSc3d1	politický
tlaku	tlak	k1gInSc3	tlak
a	a	k8xC	a
stále	stále	k6eAd1	stále
zřetelněji	zřetelně	k6eAd2	zřetelně
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Poskytl	poskytnout	k5eAaPmAgInS	poskytnout
spoustu	spousta	k1gFnSc4	spousta
rozhovorů	rozhovor	k1gInPc2	rozhovor
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
chváli	chvánout	k5eAaImAgMnP	chvánout
socialistické	socialistický	k2eAgNnSc4d1	socialistické
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
normalizačního	normalizační	k2eAgInSc2d1	normalizační
Svazu	svaz	k1gInSc2	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
servilita	servilita	k1gFnSc1	servilita
vůči	vůči	k7c3	vůči
mocným	mocný	k2eAgMnPc3d1	mocný
komunistickým	komunistický	k2eAgMnPc3d1	komunistický
funkcionářům	funkcionář	k1gMnPc3	funkcionář
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
literárními	literární	k2eAgMnPc7d1	literární
kolegy	kolega	k1gMnPc7	kolega
pověstná	pověstný	k2eAgFnSc1d1	pověstná
<g/>
.	.	kIx.	.
</s>
<s>
Dopouštěl	dopouštět	k5eAaImAgInS	dopouštět
se	se	k3xPyFc4	se
také	také	k9	také
literárních	literární	k2eAgInPc2d1	literární
poklesků	poklesek	k1gInPc2	poklesek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
asi	asi	k9	asi
tím	ten	k3xDgNnSc7	ten
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
je	být	k5eAaImIp3nS	být
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
žitného	žitný	k2eAgNnSc2d1	žitné
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
možnost	možnost	k1gFnSc4	možnost
žít	žít	k5eAaImF	žít
si	se	k3xPyFc3	se
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
viděl	vidět	k5eAaImAgMnS	vidět
tehdy	tehdy	k6eAd1	tehdy
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
právě	právě	k9	právě
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
žitného	žitný	k2eAgNnSc2d1	žitné
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
autor	autor	k1gMnSc1	autor
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
Fuks	Fuks	k1gMnSc1	Fuks
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
nový	nový	k2eAgInSc4d1	nový
chomout	chomout	k1gInSc4	chomout
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
románů	román	k1gInPc2	román
(	(	kIx(	(
<g/>
Myši	myš	k1gFnSc2	myš
Natalie	Natalie	k1gFnSc2	Natalie
Mooshabrové	Mooshabrová	k1gFnSc2	Mooshabrová
<g/>
,	,	kIx,	,
Oslovení	oslovení	k1gNnSc2	oslovení
z	z	k7c2	z
tmy	tma	k1gFnSc2	tma
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
science	science	k1gFnPc1	science
fiction	fiction	k1gInSc4	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
prožil	prožít	k5eAaPmAgMnS	prožít
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
v	v	k7c6	v
osamění	osamění	k1gNnSc6	osamění
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
mladíků	mladík	k1gMnPc2	mladík
s	s	k7c7	s
často	často	k6eAd1	často
kriminální	kriminální	k2eAgFnSc7d1	kriminální
minulostí	minulost	k1gFnSc7	minulost
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
živili	živit	k5eAaImAgMnP	živit
prostitucí	prostituce	k1gFnSc7	prostituce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
vymanit	vymanit	k5eAaPmF	vymanit
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
překvapivě	překvapivě	k6eAd1	překvapivě
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Italka	Italka	k1gFnSc1	Italka
Giuliana	Giuliana	k1gFnSc1	Giuliana
Limiti	Limit	k2eAgMnPc1d1	Limit
a	a	k8xC	a
svatba	svatba	k1gFnSc1	svatba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
Maggiore	Maggior	k1gInSc5	Maggior
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Blahopřání	blahopřání	k1gNnSc4	blahopřání
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
tehdy	tehdy	k6eAd1	tehdy
zaslal	zaslat	k5eAaPmAgMnS	zaslat
jak	jak	k8xS	jak
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
předseda	předseda	k1gMnSc1	předseda
Italské	italský	k2eAgFnSc2d1	italská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Palmiro	Palmiro	k1gNnSc4	Palmiro
Togliatti	Togliatti	k1gNnSc2	Togliatti
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
však	však	k9	však
na	na	k7c6	na
svatební	svatební	k2eAgFnSc6d1	svatební
hostině	hostina	k1gFnSc6	hostina
svedl	svést	k5eAaPmAgInS	svést
rumunského	rumunský	k2eAgMnSc4d1	rumunský
číšníka	číšník	k1gMnSc4	číšník
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
ze	z	k7c2	z
svatby	svatba	k1gFnSc2	svatba
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
i	i	k9	i
strávil	strávit	k5eAaPmAgMnS	strávit
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
hospitalizovat	hospitalizovat	k5eAaBmF	hospitalizovat
na	na	k7c6	na
psychiatrii	psychiatrie	k1gFnSc6	psychiatrie
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgInS	uniknout
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
rozezlenou	rozezlený	k2eAgFnSc7d1	rozezlená
novomanželkou	novomanželka	k1gFnSc7	novomanželka
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
lásky	láska	k1gFnSc2	láska
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
Fuks	Fuks	k1gMnSc1	Fuks
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
puzen	puzen	k2eAgInSc4d1	puzen
také	také	k9	také
náhlým	náhlý	k2eAgInSc7d1	náhlý
nápadem	nápad	k1gInSc7	nápad
emigrovat	emigrovat	k5eAaBmF	emigrovat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
vydával	vydávat	k5eAaImAgInS	vydávat
další	další	k2eAgFnPc4d1	další
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
stále	stále	k6eAd1	stále
ho	on	k3xPp3gInSc4	on
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
tajná	tajný	k2eAgFnSc1d1	tajná
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Fuks	Fuks	k1gMnSc1	Fuks
si	se	k3xPyFc3	se
vedl	vést	k5eAaImAgMnS	vést
deníkové	deníkový	k2eAgInPc4d1	deníkový
záznamy	záznam	k1gInPc4	záznam
svých	svůj	k3xOyFgInPc2	svůj
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
StB	StB	k1gFnPc2	StB
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ale	ale	k8xC	ale
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
zmizely	zmizet	k5eAaPmAgInP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
dílem	díl	k1gInSc7	díl
byl	být	k5eAaImAgInS	být
obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
román	román	k1gInSc1	román
Vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
a	a	k8xC	a
kuchařka	kuchařka	k1gFnSc1	kuchařka
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
zemřel	zemřít	k5eAaPmAgMnS	zemřít
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1994	[number]	k4	1994
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pražském	pražský	k2eAgInSc6d1	pražský
bytě	byt	k1gInSc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
až	až	k9	až
dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
osobité	osobitý	k2eAgNnSc1d1	osobité
a	a	k8xC	a
svébytné	svébytný	k2eAgNnSc1d1	svébytné
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
složitou	složitý	k2eAgFnSc7d1	složitá
metaforikou	metaforika	k1gFnSc7	metaforika
a	a	k8xC	a
stavěním	stavění	k1gNnSc7	stavění
příběhu	příběh	k1gInSc2	příběh
na	na	k7c6	na
detailech	detail	k1gInPc6	detail
a	a	k8xC	a
opakujících	opakující	k2eAgInPc6d1	opakující
se	se	k3xPyFc4	se
motivech	motiv	k1gInPc6	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
období	období	k1gNnPc2	období
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Zámek	zámek	k1gInSc1	zámek
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
-	-	kIx~	-
odborná	odborný	k2eAgFnSc1d1	odborná
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Pan	Pan	k1gMnSc1	Pan
Theodor	Theodor	k1gMnSc1	Theodor
Mundstock	Mundstock	k1gMnSc1	Mundstock
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
-	-	kIx~	-
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
pražského	pražský	k2eAgMnSc2d1	pražský
Žida	Žid	k1gMnSc2	Žid
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
sužován	sužovat	k5eAaImNgMnS	sužovat
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
transportován	transportován	k2eAgMnSc1d1	transportován
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
naprosté	naprostý	k2eAgFnSc2d1	naprostá
apatie	apatie	k1gFnSc2	apatie
a	a	k8xC	a
melancholie	melancholie	k1gFnSc2	melancholie
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
postupovat	postupovat	k5eAaImF	postupovat
prakticky	prakticky	k6eAd1	prakticky
a	a	k8xC	a
metodicky	metodicky	k6eAd1	metodicky
-	-	kIx~	-
připravovat	připravovat	k5eAaImF	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
budoucí	budoucí	k2eAgFnSc4d1	budoucí
situaci	situace	k1gFnSc4	situace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
spí	spát	k5eAaImIp3nS	spát
na	na	k7c6	na
pryčně	pryčna	k1gFnSc6	pryčna
<g/>
,	,	kIx,	,
moří	mořit	k5eAaImIp3nS	mořit
se	s	k7c7	s
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
tahá	tahat	k5eAaImIp3nS	tahat
těžké	těžký	k2eAgFnPc4d1	těžká
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
trénuje	trénovat	k5eAaImIp3nS	trénovat
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
prožívá	prožívat	k5eAaImIp3nS	prožívat
řadu	řada	k1gFnSc4	řada
halucinací	halucinace	k1gFnPc2	halucinace
a	a	k8xC	a
dialogů	dialog	k1gInPc2	dialog
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
stínem	stín	k1gInSc7	stín
Monem	Monem	k1gInSc1	Monem
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
jeho	jeho	k3xOp3gFnSc7	jeho
psychickou	psychický	k2eAgFnSc7d1	psychická
nemocí	nemoc	k1gFnSc7	nemoc
-	-	kIx~	-
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
<g/>
;	;	kIx,	;
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
halucinací	halucinace	k1gFnPc2	halucinace
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
"	"	kIx"	"
<g/>
slípka	slípka	k1gFnSc1	slípka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
žije	žít	k5eAaImIp3nS	žít
doma	doma	k6eAd1	doma
a	a	k8xC	a
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
stará	stará	k1gFnSc1	stará
<g/>
;	;	kIx,	;
transportu	transport	k1gInSc2	transport
se	se	k3xPyFc4	se
nedožije	dožít	k5eNaPmIp3nS	dožít
<g/>
,	,	kIx,	,
před	před	k7c7	před
nástupištěm	nástupiště	k1gNnSc7	nástupiště
ho	on	k3xPp3gInSc2	on
přejede	přejet	k5eAaPmIp3nS	přejet
auto	auto	k1gNnSc4	auto
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
člověka	člověk	k1gMnSc2	člověk
ponižuje	ponižovat	k5eAaImIp3nS	ponižovat
a	a	k8xC	a
pokřivuje	pokřivovat	k5eAaImIp3nS	pokřivovat
diskriminace	diskriminace	k1gFnSc1	diskriminace
a	a	k8xC	a
strach	strach	k1gInSc1	strach
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
zcela	zcela	k6eAd1	zcela
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nám	my	k3xPp1nPc3	my
osud	osud	k1gInSc1	osud
přichystá	přichystat	k5eAaPmIp3nS	přichystat
<g/>
.	.	kIx.	.
</s>
<s>
Mí	můj	k3xOp1gMnPc1	můj
černovlasí	černovlasý	k2eAgMnPc1d1	černovlasý
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
šesti	šest	k4xCc2	šest
povídek	povídka	k1gFnPc2	povídka
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
pěti	pět	k4xCc2	pět
gymnazistů	gymnazista	k1gMnPc2	gymnazista
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
mimo	mimo	k7c4	mimo
společnost	společnost	k1gFnSc4	společnost
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
spojuje	spojovat	k5eAaImIp3nS	spojovat
je	on	k3xPp3gNnPc4	on
postava	postava	k1gFnSc1	postava
rasistického	rasistický	k2eAgMnSc2d1	rasistický
učitele	učitel	k1gMnSc2	učitel
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
chlapec	chlapec	k1gMnSc1	chlapec
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
alter	alter	k1gMnSc1	alter
ego	ego	k1gNnSc2	ego
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
nucen	nutit	k5eAaImNgMnS	nutit
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
svých	svůj	k3xOyFgMnPc2	svůj
kamarádů	kamarád	k1gMnPc2	kamarád
-	-	kIx~	-
"	"	kIx"	"
<g/>
bratrů	bratr	k1gMnPc2	bratr
<g/>
"	"	kIx"	"
tváří	tvář	k1gFnPc2	tvář
v	v	k7c6	v
tvář	tvář	k1gFnSc1	tvář
<g/>
;	;	kIx,	;
židovští	židovský	k2eAgMnPc1d1	židovský
chlapci	chlapec	k1gMnPc1	chlapec
totiž	totiž	k9	totiž
jeden	jeden	k4xCgMnSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgMnSc6	druhý
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
,	,	kIx,	,
<g/>
ať	ať	k8xS	ať
už	už	k9	už
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
či	či	k8xC	či
tragicky	tragicky	k6eAd1	tragicky
<g/>
(	(	kIx(	(
<g/>
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
knihu	kniha	k1gFnSc4	kniha
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
leitmotiv	leitmotiv	k1gInSc1	leitmotiv
"	"	kIx"	"
<g/>
Smutek	smutek	k1gInSc1	smutek
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
a	a	k8xC	a
šesticípý	šesticípý	k2eAgInSc1d1	šesticípý
jako	jako	k8xS	jako
Davidova	Davidův	k2eAgFnSc1d1	Davidova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
Michaelovy	Michaelův	k2eAgFnPc4d1	Michaelova
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
náhle	náhle	k6eAd1	náhle
postaveno	postavit	k5eAaPmNgNnS	postavit
před	před	k7c4	před
strašlivou	strašlivý	k2eAgFnSc4d1	strašlivá
pravdu	pravda	k1gFnSc4	pravda
války	válka	k1gFnSc2	válka
a	a	k8xC	a
umírání	umírání	k1gNnSc2	umírání
<g/>
.	.	kIx.	.
</s>
<s>
Variace	variace	k1gFnPc1	variace
pro	pro	k7c4	pro
temnou	temný	k2eAgFnSc4d1	temná
strunu	struna	k1gFnSc4	struna
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
-	-	kIx~	-
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
román	román	k1gInSc1	román
o	o	k7c6	o
dospívajícím	dospívající	k2eAgMnSc6d1	dospívající
chlapci	chlapec	k1gMnSc6	chlapec
Michalovi	Michal	k1gMnSc6	Michal
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
patricijské	patricijský	k2eAgFnSc2d1	patricijská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
citu	cit	k1gInSc2	cit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vede	vést	k5eAaImIp3nS	vést
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
schizofrenní	schizofrenní	k2eAgFnPc1d1	schizofrenní
<g/>
,	,	kIx,	,
rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
porculánovou	porculánový	k2eAgFnSc7d1	porculánová
míšeňskou	míšeňský	k2eAgFnSc7d1	Míšeňská
tanečnicí	tanečnice	k1gFnSc7	tanečnice
a	a	k8xC	a
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
též	též	k9	též
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
temnějící	temnějící	k2eAgNnSc4d1	temnějící
klima	klima	k1gNnSc4	klima
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
-	-	kIx~	-
psychologický	psychologický	k2eAgInSc1d1	psychologický
hororový	hororový	k2eAgInSc1d1	hororový
román	román	k1gInSc1	román
o	o	k7c4	o
pracovníku	pracovník	k1gMnSc3	pracovník
krematoria	krematorium	k1gNnSc2	krematorium
Karlu	Karel	k1gMnSc3	Karel
Kopfrkinglovi	Kopfrkingl	k1gMnSc3	Kopfrkingl
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
vlivem	vliv	k1gInSc7	vliv
nacistické	nacistický	k2eAgFnSc2d1	nacistická
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
orientální	orientální	k2eAgFnSc7d1	orientální
filosofií	filosofie	k1gFnSc7	filosofie
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
vrah	vrah	k1gMnSc1	vrah
<g/>
;	;	kIx,	;
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
Jurajem	Juraj	k1gMnSc7	Juraj
Herzem	Herz	k1gMnSc7	Herz
Myši	myš	k1gFnSc2	myš
Natálie	Natálie	k1gFnSc2	Natálie
Mooshabrové	Mooshabrová	k1gFnSc2	Mooshabrová
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1970	[number]	k4	1970
-	-	kIx~	-
román	román	k1gInSc1	román
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
hororového	hororový	k2eAgInSc2d1	hororový
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
grotesknosti	grotesknost	k1gFnSc2	grotesknost
i	i	k8xC	i
fantastické	fantastický	k2eAgFnPc4d1	fantastická
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
;	;	kIx,	;
příběh	příběh	k1gInSc1	příběh
chudé	chudý	k2eAgFnSc2d1	chudá
vdovy	vdova	k1gFnSc2	vdova
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
kriminálník	kriminálník	k1gMnSc1	kriminálník
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
prostitutka	prostitutka	k1gFnSc1	prostitutka
Příběh	příběh	k1gInSc1	příběh
kriminálního	kriminální	k2eAgMnSc2d1	kriminální
rady	rada	k1gMnSc2	rada
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
-	-	kIx~	-
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c7	mezi
despotickým	despotický	k2eAgMnSc7d1	despotický
kriminálním	kriminální	k2eAgMnSc7d1	kriminální
radou	rada	k1gMnSc7	rada
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
plachým	plachý	k2eAgMnSc7d1	plachý
dospívajícím	dospívající	k2eAgMnSc7d1	dospívající
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
nejmenovaného	jmenovaný	k2eNgInSc2d1	nejmenovaný
západoevropského	západoevropský	k2eAgInSc2d1	západoevropský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
jako	jako	k8xC	jako
detektivní	detektivní	k2eAgInSc1d1	detektivní
horor	horor	k1gInSc1	horor
<g/>
.	.	kIx.	.
</s>
<s>
Oslovení	oslovení	k1gNnSc1	oslovení
z	z	k7c2	z
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
-	-	kIx~	-
novela	novela	k1gFnSc1	novela
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
záhady	záhada	k1gFnSc2	záhada
a	a	k8xC	a
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
apokalyptická	apokalyptický	k2eAgFnSc1d1	apokalyptická
vize	vize	k1gFnSc1	vize
zániku	zánik	k1gInSc2	zánik
světa	svět	k1gInSc2	svět
po	po	k7c6	po
jaderné	jaderný	k2eAgFnSc6d1	jaderná
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
chlapcem	chlapec	k1gMnSc7	chlapec
<g/>
,	,	kIx,	,
umírajícím	umírající	k2eAgMnSc7d1	umírající
v	v	k7c6	v
rozvalinách	rozvalina	k1gFnPc6	rozvalina
rozbombardovaného	rozbombardovaný	k2eAgNnSc2d1	rozbombardované
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
velmi	velmi	k6eAd1	velmi
obtížnou	obtížný	k2eAgFnSc7d1	obtížná
<g/>
,	,	kIx,	,
náročnou	náročný	k2eAgFnSc7d1	náročná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Nebožtíci	nebožtík	k1gMnPc1	nebožtík
na	na	k7c6	na
bále	bál	k1gInSc6	bál
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
-	-	kIx~	-
život	život	k1gInSc1	život
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc4d1	nový
prvky	prvek	k1gInPc4	prvek
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
(	(	kIx(	(
<g/>
groteskní	groteskní	k2eAgInSc4d1	groteskní
humor	humor	k1gInSc4	humor
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
příběhu	příběh	k1gInSc2	příběh
záměny	záměna	k1gFnSc2	záměna
dvou	dva	k4xCgInPc2	dva
těl	tělo	k1gNnPc2	tělo
nebožtíků	nebožtík	k1gMnPc2	nebožtík
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poklidnost	poklidnost	k1gFnSc1	poklidnost
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
života	život	k1gInSc2	život
byla	být	k5eAaImAgNnP	být
pouze	pouze	k6eAd1	pouze
iluzí	iluze	k1gFnSc7	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vážných	vážný	k2eAgNnPc2d1	vážné
autorových	autorův	k2eAgNnPc2d1	autorovo
děl	dělo	k1gNnPc2	dělo
z	z	k7c2	z
počátků	počátek	k1gInPc2	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
jeví	jevit	k5eAaImIp3nS	jevit
spíše	spíše	k9	spíše
jako	jako	k9	jako
maloměstská	maloměstský	k2eAgFnSc1d1	maloměstská
fraška	fraška	k1gFnSc1	fraška
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc4d1	možné
se	se	k3xPyFc4	se
skvěle	skvěle	k6eAd1	skvěle
pobavit	pobavit	k5eAaPmF	pobavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
hodnotnějším	hodnotný	k2eAgNnPc3d2	hodnotnější
Fuksovým	Fuksův	k2eAgNnPc3d1	Fuksovo
dílům	dílo	k1gNnPc3	dílo
ji	on	k3xPp3gFnSc4	on
řadit	řadit	k5eAaImF	řadit
rozhodně	rozhodně	k6eAd1	rozhodně
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Fuks	Fuks	k1gMnSc1	Fuks
začal	začít	k5eAaPmAgMnS	začít
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c6	na
válečné	válečný	k2eAgFnSc6d1	válečná
a	a	k8xC	a
poválečné	poválečný	k2eAgFnSc6d1	poválečná
události	událost	k1gFnSc6	událost
v	v	k7c6	v
širších	široký	k2eAgFnPc6d2	širší
souvislostech	souvislost	k1gFnPc6	souvislost
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
žitného	žitný	k2eAgNnSc2d1	žitné
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
-	-	kIx~	-
dílo	dílo	k1gNnSc4	dílo
o	o	k7c6	o
poúnorové	poúnorový	k2eAgFnSc6d1	poúnorová
emigraci	emigrace	k1gFnSc6	emigrace
(	(	kIx(	(
<g/>
antiemigrační	antiemigrační	k2eAgFnSc2d1	antiemigrační
<g/>
,	,	kIx,	,
poplatné	poplatný	k2eAgFnSc6d1	poplatná
době	doba	k1gFnSc6	doba
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Mrtvý	mrtvý	k1gMnSc1	mrtvý
v	v	k7c6	v
podchodu	podchod	k1gInSc6	podchod
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
-	-	kIx~	-
detektivní	detektivní	k2eAgInSc1d1	detektivní
román	román	k1gInSc1	román
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pašování	pašování	k1gNnSc1	pašování
heroinu	heroin	k1gInSc2	heroin
přes	přes	k7c4	přes
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
na	na	k7c4	na
západ	západ	k1gInSc4	západ
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc4	důkaz
mravní	mravní	k2eAgFnSc2d1	mravní
zkaženosti	zkaženost	k1gFnSc2	zkaženost
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Spoluautor	spoluautor	k1gMnSc1	spoluautor
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kosek	Kosek	k1gMnSc1	Kosek
<g/>
.	.	kIx.	.
</s>
<s>
Pasáček	pasáček	k1gMnSc1	pasáček
z	z	k7c2	z
doliny	dolina	k1gFnSc2	dolina
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
-	-	kIx~	-
baladicky	baladicky	k6eAd1	baladicky
laděné	laděný	k2eAgNnSc4d1	laděné
dílo	dílo	k1gNnSc4	dílo
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k6eAd1	právě
probíhá	probíhat	k5eAaImIp3nS	probíhat
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
<g/>
;	;	kIx,	;
opět	opět	k6eAd1	opět
dobově	dobově	k6eAd1	dobově
poplatné	poplatný	k2eAgNnSc4d1	poplatné
Křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
pantoflíček	pantoflíček	k1gInSc4	pantoflíček
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
-	-	kIx~	-
idylické	idylický	k2eAgNnSc1d1	idylické
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
dětství	dětství	k1gNnSc6	dětství
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
Obraz	obraz	k1gInSc1	obraz
Martina	Martin	k1gMnSc4	Martin
Blaskowitze	Blaskowitze	k1gFnSc2	Blaskowitze
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
-	-	kIx~	-
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
spisovatelské	spisovatelský	k2eAgFnSc3d1	spisovatelská
úrovni	úroveň	k1gFnSc3	úroveň
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc1d1	předchozí
dobově	dobově	k6eAd1	dobově
poplatné	poplatný	k2eAgFnPc1d1	poplatná
knihy	kniha	k1gFnPc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
o	o	k7c6	o
problému	problém	k1gInSc6	problém
mravnosti	mravnost	k1gFnSc2	mravnost
-	-	kIx~	-
konfrontace	konfrontace	k1gFnSc1	konfrontace
bezúhoného	bezúhoný	k1gMnSc2	bezúhoný
vypravěče	vypravěč	k1gMnSc2	vypravěč
s	s	k7c7	s
kariéristou	kariérista	k1gMnSc7	kariérista
a	a	k8xC	a
kolaborantem	kolaborant	k1gMnSc7	kolaborant
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
msta	msta	k1gFnSc1	msta
je	být	k5eAaImIp3nS	být
odpuštění	odpuštění	k1gNnSc4	odpuštění
a	a	k8xC	a
žádný	žádný	k3yNgMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
soudcem	soudce	k1gMnSc7	soudce
svědomí	svědomí	k1gNnSc2	svědomí
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
a	a	k8xC	a
kuchařka	kuchařka	k1gFnSc1	kuchařka
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
-	-	kIx~	-
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1897	[number]	k4	1897
a	a	k8xC	a
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgFnSc6d1	zachycující
předzvěsti	předzvěst	k1gFnSc6	předzvěst
konce	konec	k1gInSc2	konec
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
travestií	travestie	k1gFnSc7	travestie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
Vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
samotného	samotný	k2eAgMnSc4d1	samotný
Fukse	Fuks	k1gMnSc4	Fuks
a	a	k8xC	a
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
jsou	být	k5eAaImIp3nP	být
zašifrována	zašifrován	k2eAgNnPc1d1	zašifrováno
i	i	k8xC	i
jména	jméno	k1gNnPc1	jméno
Fuksových	Fuksových	k2eAgMnPc2d1	Fuksových
reálných	reálný	k2eAgMnPc2d1	reálný
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
milenců	milenec	k1gMnPc2	milenec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
propracovaná	propracovaný	k2eAgFnSc1d1	propracovaná
psychologie	psychologie	k1gFnSc1	psychologie
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
detailní	detailní	k2eAgInSc1d1	detailní
popis	popis	k1gInSc1	popis
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
situací	situace	k1gFnPc2	situace
a	a	k8xC	a
prvky	prvek	k1gInPc4	prvek
záhady	záhada	k1gFnSc2	záhada
a	a	k8xC	a
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Fuksovým	Fuksův	k2eAgInSc7d1	Fuksův
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
románového	románový	k2eAgInSc2d1	románový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
pojednával	pojednávat	k5eAaImAgInS	pojednávat
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
milované	milovaný	k2eAgFnSc6d1	milovaná
době	doba	k1gFnSc6	doba
sklonku	sklonek	k1gInSc2	sklonek
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
napsání	napsání	k1gNnSc6	napsání
románu	román	k1gInSc2	román
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
selhává	selhávat	k5eAaImIp3nS	selhávat
paměť	paměť	k1gFnSc4	paměť
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedaří	dařit	k5eNaImIp3nS	dařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
zůstala	zůstat	k5eAaPmAgFnS	zůstat
osamocena	osamotit	k5eAaPmNgFnS	osamotit
<g/>
.	.	kIx.	.
</s>
<s>
Moje	můj	k3xOp1gNnSc1	můj
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
-	-	kIx~	-
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
vyšly	vyjít	k5eAaPmAgInP	vyjít
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jiří	Jiří	k1gMnSc1	Jiří
Tušl	Tušl	k1gMnSc1	Tušl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
ironie	ironie	k1gFnSc2	ironie
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
díl	díl	k1gInSc4	díl
Vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
a	a	k8xC	a
kuchařky	kuchařka	k1gFnSc2	kuchařka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
příznačnou	příznačný	k2eAgFnSc7d1	příznačná
ukázkou	ukázka	k1gFnSc7	ukázka
Fuksova	Fuksův	k2eAgInSc2d1	Fuksův
vyprávěcího	vyprávěcí	k2eAgInSc2d1	vyprávěcí
postupu	postup	k1gInSc2	postup
a	a	k8xC	a
nevšedního	všední	k2eNgInSc2d1	nevšední
smyslu	smysl	k1gInSc2	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
-	-	kIx~	-
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nepřebernou	přeberný	k2eNgFnSc4d1	nepřeberná
řadu	řada	k1gFnSc4	řada
detailů	detail	k1gInPc2	detail
z	z	k7c2	z
Fuksova	Fuksův	k2eAgInSc2d1	Fuksův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
odhalila	odhalit	k5eAaPmAgFnS	odhalit
cokoli	cokoli	k3yInSc4	cokoli
podstatného	podstatný	k2eAgMnSc4d1	podstatný
<g/>
.	.	kIx.	.
</s>
<s>
Nebýt	být	k5eNaImF	být
Tušlových	Tušlův	k2eAgFnPc2d1	Tušlův
intermezz	intermezzo	k1gNnPc2	intermezzo
<g/>
,	,	kIx,	,
nedozvíme	dozvědět	k5eNaPmIp1nP	dozvědět
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
o	o	k7c6	o
Fuksově	Fuksův	k2eAgFnSc6d1	Fuksova
osobnosti	osobnost	k1gFnSc6	osobnost
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgNnPc2d1	uvedené
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
také	také	k6eAd1	také
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
kratších	krátký	k2eAgFnPc2d2	kratší
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
morčete	morče	k1gNnSc2	morče
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
1969	[number]	k4	1969
-	-	kIx~	-
soubor	soubor	k1gInSc1	soubor
deseti	deset	k4xCc2	deset
baladicky	baladicky	k6eAd1	baladicky
laděných	laděný	k2eAgFnPc2d1	laděná
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
období	období	k1gNnPc2	období
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
častý	častý	k2eAgInSc1d1	častý
motiv	motiv	k1gInSc1	motiv
vykořenění	vykořenění	k1gNnSc2	vykořenění
způsobeného	způsobený	k2eAgInSc2d1	způsobený
holocaustem	holocaust	k1gInSc7	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
jeho	jeho	k3xOp3gFnPc2	jeho
kratších	krátký	k2eAgFnPc2d2	kratší
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
napsaných	napsaný	k2eAgFnPc2d1	napsaná
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
činnosti	činnost	k1gFnSc2	činnost
až	až	k9	až
po	po	k7c4	po
dílo	dílo	k1gNnSc4	dílo
napsané	napsaný	k2eAgNnSc4d1	napsané
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bližší	blízký	k2eAgNnSc4d2	bližší
poznání	poznání	k1gNnSc4	poznání
Fuksovy	Fuksův	k2eAgFnSc2d1	Fuksova
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
poslední	poslední	k2eAgFnSc1d1	poslední
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
kapitolou	kapitola	k1gFnSc7	kapitola
jeho	on	k3xPp3gInSc2	on
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
románu	román	k1gInSc2	román
Podivné	podivný	k2eAgNnSc1d1	podivné
manželství	manželství	k1gNnSc1	manželství
paní	paní	k1gFnSc2	paní
Lucy	Luca	k1gFnSc2	Luca
Fehrové	Fehrová	k1gFnSc2	Fehrová
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Horizont	horizont	k1gInSc1	horizont
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
se	se	k3xPyFc4	se
vesměs	vesměs	k6eAd1	vesměs
věnují	věnovat	k5eAaImIp3nP	věnovat
podobným	podobný	k2eAgNnPc3d1	podobné
tématům	téma	k1gNnPc3	téma
jako	jako	k8xS	jako
Fuksova	Fuksův	k2eAgFnSc1d1	Fuksova
románová	románový	k2eAgFnSc1d1	románová
tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
Povídky	povídka	k1gFnPc4	povídka
s	s	k7c7	s
válečnou	válečný	k2eAgFnSc7d1	válečná
a	a	k8xC	a
židovskou	židovský	k2eAgFnSc7d1	židovská
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
mravnosti	mravnost	k1gFnSc6	mravnost
a	a	k8xC	a
hodnotě	hodnota	k1gFnSc6	hodnota
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Humoresky	humoreska	k1gFnPc1	humoreska
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Smrt	smrt	k1gFnSc1	smrt
morčete	morče	k1gNnSc2	morče
<g/>
,	,	kIx,	,
mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
datováno	datovat	k5eAaImNgNnS	datovat
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
-	-	kIx~	-
strhující	strhující	k2eAgInSc4d1	strhující
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
patnácti	patnáct	k4xCc6	patnáct
bohatých	bohatý	k2eAgInPc6d1	bohatý
vídeňských	vídeňský	k2eAgMnPc6d1	vídeňský
Židech	Žid	k1gMnPc6	Žid
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
anšlusu	anšlus	k1gInSc6	anšlus
"	"	kIx"	"
<g/>
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
"	"	kIx"	"
emigrovat	emigrovat	k5eAaBmF	emigrovat
po	po	k7c6	po
Dunaji	Dunaj	k1gInSc6	Dunaj
na	na	k7c6	na
vratké	vratký	k2eAgFnSc6d1	vratká
kocábce	kocábka	k1gFnSc6	kocábka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nemáte	mít	k5eNaImIp2nP	mít
průjezdní	průjezdní	k2eAgNnPc1d1	průjezdní
víza	vízo	k1gNnPc1	vízo
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
nesmí	smět	k5eNaImIp3nS	smět
nikde	nikde	k6eAd1	nikde
přistát	přistát	k5eAaImF	přistát
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Nejprve	nejprve	k6eAd1	nejprve
si	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
užívají	užívat	k5eAaImIp3nP	užívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
s	s	k7c7	s
hákovým	hákový	k2eAgInSc7d1	hákový
křížem	kříž	k1gInSc7	kříž
přistát	přistát	k5eAaPmF	přistát
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
jsou	být	k5eAaImIp3nP	být
zahnáni	zahnat	k5eAaPmNgMnP	zahnat
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
řeku	řek	k1gMnSc6	řek
<g/>
.	.	kIx.	.
</s>
<s>
Přistanou	přistat	k5eAaPmIp3nP	přistat
na	na	k7c6	na
liduprázdném	liduprázdný	k2eAgInSc6d1	liduprázdný
slovenském	slovenský	k2eAgInSc6d1	slovenský
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
opraví	opravit	k5eAaPmIp3nP	opravit
závadu	závada	k1gFnSc4	závada
ale	ale	k8xC	ale
mezitím	mezitím	k6eAd1	mezitím
klesne	klesnout	k5eAaPmIp3nS	klesnout
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
suchu	sucho	k1gNnSc6	sucho
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
sílu	síla	k1gFnSc4	síla
ji	on	k3xPp3gFnSc4	on
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
hledají	hledat	k5eAaImIp3nP	hledat
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
veškerou	veškerý	k3xTgFnSc4	veškerý
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
trosečníků	trosečník	k1gMnPc2	trosečník
nalezne	nalézt	k5eAaBmIp3nS	nalézt
slovenský	slovenský	k2eAgMnSc1d1	slovenský
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
je	být	k5eAaImIp3nS	být
....	....	k?	....
<g/>
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
<g/>
,	,	kIx,	,
nevzdali	vzdát	k5eNaPmAgMnP	vzdát
<g/>
,	,	kIx,	,
zachránili	zachránit	k5eAaPmAgMnP	zachránit
....	....	k?	....
Rabbi	Rabbe	k1gFnSc4	Rabbe
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
Pořád	pořád	k6eAd1	pořád
někdo	někdo	k3yInSc1	někdo
někoho	někdo	k3yInSc4	někdo
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
a	a	k8xC	a
bere	brát	k5eAaImIp3nS	brát
volnost	volnost	k1gFnSc1	volnost
<g/>
,	,	kIx,	,
pořád	pořád	k6eAd1	pořád
někdo	někdo	k3yInSc1	někdo
někoho	někdo	k3yInSc4	někdo
viní	vinit	k5eAaImIp3nS	vinit
<g/>
,	,	kIx,	,
štve	štvát	k5eAaImIp3nS	štvát
a	a	k8xC	a
bere	brát	k5eAaImIp3nS	brát
mu	on	k3xPp3gMnSc3	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pořád	pořád	k6eAd1	pořád
někdo	někdo	k3yInSc1	někdo
utlačuje	utlačovat	k5eAaImIp3nS	utlačovat
jiné	jiný	k2eAgNnSc1d1	jiné
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
pravdy	pravda	k1gFnSc2	pravda
<g/>
....	....	k?	....
<g/>
pořád	pořád	k6eAd1	pořád
někdo	někdo	k3yInSc1	někdo
někomu	někdo	k3yInSc3	někdo
brání	bránit	k5eAaImIp3nS	bránit
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
spát	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
bydlet	bydlet	k5eAaImF	bydlet
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Židé	Žid	k1gMnPc1	Žid
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
dopláceli	doplácet	k5eAaImAgMnP	doplácet
nejvíc	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
volají	volat	k5eAaImIp3nP	volat
šalom	šalom	k0	šalom
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
vzdáleným	vzdálený	k2eAgInSc7d1	vzdálený
i	i	k8xC	i
blízkým	blízký	k2eAgInSc7d1	blízký
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
vede	vést	k5eAaImIp3nS	vést
už	už	k6eAd1	už
jen	jen	k9	jen
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
dobrovolně	dobrovolně	k6eAd1	dobrovolně
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
vrchol	vrchol	k1gInSc4	vrchol
Fuksovy	Fuksův	k2eAgFnSc2d1	Fuksova
povídkové	povídkový	k2eAgFnSc2d1	povídková
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Motivem	motiv	k1gInSc7	motiv
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ze	z	k7c2	z
ctihodných	ctihodný	k2eAgMnPc2d1	ctihodný
<g/>
,	,	kIx,	,
slušných	slušný	k2eAgMnPc2d1	slušný
lidí	člověk	k1gMnPc2	člověk
během	během	k7c2	během
krizové	krizový	k2eAgFnSc2d1	krizová
situace	situace	k1gFnSc2	situace
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
stanou	stanout	k5eAaPmIp3nP	stanout
špinavé	špinavý	k2eAgFnPc4d1	špinavá
a	a	k8xC	a
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
akce	akce	k1gFnPc4	akce
neschopné	schopný	k2eNgFnSc2d1	neschopná
trosky	troska	k1gFnSc2	troska
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
poněkud	poněkud	k6eAd1	poněkud
připomínat	připomínat	k5eAaImF	připomínat
Goldingova	Goldingův	k2eAgMnSc4d1	Goldingův
Pána	pán	k1gMnSc4	pán
much	moucha	k1gFnPc2	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Podivuhodné	podivuhodný	k2eAgNnSc1d1	podivuhodné
setkání	setkání	k1gNnSc1	setkání
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
....	....	k?	....
<g/>
ono	onen	k3xDgNnSc1	onen
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
žádný	žádný	k3yNgInSc1	žádný
děj	děj	k1gInSc1	děj
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
pořádný	pořádný	k2eAgInSc4d1	pořádný
začátek	začátek	k1gInSc4	začátek
a	a	k8xC	a
konec	konec	k1gInSc4	konec
<g/>
....	....	k?	....
Povídka	povídka	k1gFnSc1	povídka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
škoda	škoda	k1gFnSc1	škoda
každého	každý	k3xTgInSc2	každý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nacistickou	nacistický	k2eAgFnSc7d1	nacistická
zvůlí	zvůle	k1gFnSc7	zvůle
promarněn	promarnit	k5eAaPmNgMnS	promarnit
<g/>
.	.	kIx.	.
</s>
<s>
Mí	můj	k3xOp1gMnPc1	můj
černovlasí	černovlasý	k2eAgMnPc1d1	černovlasý
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
-	-	kIx~	-
Sbírka	sbírka	k1gFnSc1	sbírka
šesti	šest	k4xCc2	šest
povídek	povídka	k1gFnPc2	povídka
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
pěti	pět	k4xCc2	pět
gymnazistů	gymnazista	k1gMnPc2	gymnazista
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
mimo	mimo	k7c4	mimo
společnost	společnost	k1gFnSc4	společnost
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
spojuje	spojovat	k5eAaImIp3nS	spojovat
je	on	k3xPp3gNnPc4	on
postava	postava	k1gFnSc1	postava
rasistického	rasistický	k2eAgMnSc2d1	rasistický
učitele	učitel	k1gMnSc2	učitel
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
chlapec	chlapec	k1gMnSc1	chlapec
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
alter	alter	k1gMnSc1	alter
ego	ego	k1gNnSc2	ego
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
nucen	nutit	k5eAaImNgMnS	nutit
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
svých	svůj	k3xOyFgMnPc2	svůj
kamarádů	kamarád	k1gMnPc2	kamarád
-	-	kIx~	-
"	"	kIx"	"
<g/>
bratrů	bratr	k1gMnPc2	bratr
<g/>
"	"	kIx"	"
tváří	tvář	k1gFnPc2	tvář
v	v	k7c6	v
tvář	tvář	k1gFnSc1	tvář
<g/>
;	;	kIx,	;
židovští	židovský	k2eAgMnPc1d1	židovský
chlapci	chlapec	k1gMnPc1	chlapec
totiž	totiž	k9	totiž
jeden	jeden	k4xCgMnSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgMnSc6	druhý
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
,	,	kIx,	,
<g/>
ať	ať	k8xS	ať
už	už	k9	už
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
či	či	k8xC	či
tragicky	tragicky	k6eAd1	tragicky
<g/>
(	(	kIx(	(
<g/>
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
knihu	kniha	k1gFnSc4	kniha
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
leitmotiv	leitmotiv	k1gInSc1	leitmotiv
"	"	kIx"	"
<g/>
Smutek	smutek	k1gInSc1	smutek
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
a	a	k8xC	a
šesticípý	šesticípý	k2eAgInSc1d1	šesticípý
jako	jako	k8xC	jako
Davidova	Davidův	k2eAgFnSc1d1	Davidova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
Michaelovy	Michaelův	k2eAgFnPc4d1	Michaelova
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
dítětě	dítěť	k1gFnSc2	dítěť
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
náhle	náhle	k6eAd1	náhle
postaveno	postavit	k5eAaPmNgNnS	postavit
před	před	k7c4	před
strašlivou	strašlivý	k2eAgFnSc4d1	strašlivá
pravdu	pravda	k1gFnSc4	pravda
války	válka	k1gFnSc2	válka
a	a	k8xC	a
umírání	umírání	k1gNnSc2	umírání
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
svatba	svatba	k1gFnSc1	svatba
1962	[number]	k4	1962
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
starých	starý	k2eAgFnPc6d1	stará
židovských	židovská	k1gFnPc6	židovská
manželích	manžel	k1gMnPc6	manžel
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sami	sám	k3xTgMnPc1	sám
sobě	se	k3xPyFc3	se
nalhávají	nalhávat	k5eAaImIp3nP	nalhávat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
příjemný	příjemný	k2eAgInSc1d1	příjemný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
skutečný	skutečný	k2eAgInSc1d1	skutečný
stav	stav	k1gInSc1	stav
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgInS	moct
dohnat	dohnat	k5eAaPmF	dohnat
k	k	k7c3	k
šílenství	šílenství	k1gNnSc3	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Věneček	věneček	k1gInSc1	věneček
z	z	k7c2	z
vavřínu	vavřín	k1gInSc2	vavřín
1953	[number]	k4	1953
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
bezmocný	bezmocný	k1gMnSc1	bezmocný
proti	proti	k7c3	proti
zlu	zlo	k1gNnSc3	zlo
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
nacismus	nacismus	k1gInSc1	nacismus
a	a	k8xC	a
jak	jak	k6eAd1	jak
slušné	slušný	k2eAgMnPc4d1	slušný
lidi	člověk	k1gMnPc4	člověk
ponižuje	ponižovat	k5eAaImIp3nS	ponižovat
neurvalé	neurvalé	k?	neurvalé
jednání	jednání	k1gNnSc4	jednání
jeho	jeho	k3xOp3gMnPc2	jeho
vykonavatelů	vykonavatel	k1gMnPc2	vykonavatel
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
poklidu	poklid	k1gInSc6	poklid
cukrárny	cukrárna	k1gFnSc2	cukrárna
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
vtrhne	vtrhnout	k5eAaPmIp3nS	vtrhnout
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
a	a	k8xC	a
tygr	tygr	k1gMnSc1	tygr
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
interiér	interiér	k1gInSc1	interiér
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Anebo	anebo	k8xC	anebo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
alegorie	alegorie	k1gFnSc1	alegorie
na	na	k7c4	na
život	život	k1gInSc4	život
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
....	....	k?	....
Mezi	mezi	k7c7	mezi
dvojím	dvojí	k4xRgInSc7	dvojí
zahýkání	zahýkání	k1gNnPc2	zahýkání
oslíka	oslík	k1gMnSc2	oslík
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
napsaná	napsaný	k2eAgFnSc1d1	napsaná
sci-fi	scii	k1gFnSc1	sci-fi
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
v	v	k7c4	v
níž	nízce	k6eAd2	nízce
starého	starý	k2eAgMnSc4d1	starý
rybáře	rybář	k1gMnSc4	rybář
Alda	Aldus	k1gMnSc4	Aldus
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
Alda	Aldus	k1gMnSc4	Aldus
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc4	jejich
osla	osel	k1gMnSc4	osel
Alda	Aldus	k1gMnSc4	Aldus
přijde	přijít	k5eAaPmIp3nS	přijít
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
navštívit	navštívit	k5eAaPmF	navštívit
tajemný	tajemný	k2eAgMnSc1d1	tajemný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	on	k3xPp3gNnSc4	on
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
žili	žít	k5eAaImAgMnP	žít
navzájem	navzájem	k6eAd1	navzájem
v	v	k7c6	v
míru	mír	k1gInSc6	mír
a	a	k8xC	a
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
ostrov	ostrov	k1gInSc1	ostrov
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyQgNnSc7	čí
vším	všecek	k3xTgNnSc7	všecek
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
souží	soužit	k5eAaImIp3nP	soužit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
jen	jen	k9	jen
přelud	přelud	k1gInSc4	přelud
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
děsí	děsit	k5eAaImIp3nS	děsit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
marné	marný	k2eAgNnSc1d1	marné
a	a	k8xC	a
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
údolí	údolí	k1gNnSc6	údolí
slz	slza	k1gFnPc2	slza
zneklidňovat	zneklidňovat	k5eAaImF	zneklidňovat
<g/>
..	..	k?	..
Manželé	manžel	k1gMnPc1	manžel
Novákovi	Novákův	k2eAgMnPc1d1	Novákův
si	se	k3xPyFc3	se
hodlají	hodlat	k5eAaImIp3nP	hodlat
jako	jako	k9	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
práci	práce	k1gFnSc4	práce
dopřát	dopřát	k5eAaPmF	dopřát
dovolenou	dovolená	k1gFnSc4	dovolená
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
těší	těšit	k5eAaImIp3nS	těšit
a	a	k8xC	a
nadšeně	nadšeně	k6eAd1	nadšeně
si	se	k3xPyFc3	se
čtou	číst	k5eAaImIp3nP	číst
v	v	k7c6	v
Průvodci	průvodce	k1gMnSc6	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
připraveni	připraven	k2eAgMnPc1d1	připraven
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
vědí	vědět	k5eAaImIp3nP	vědět
všechno	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
je	on	k3xPp3gInPc4	on
tedy	tedy	k9	tedy
ničím	ničit	k5eAaImIp1nS	ničit
překvapit	překvapit	k5eAaPmF	překvapit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
smutkem	smutek	k1gInSc7	smutek
tedy	tedy	k8xC	tedy
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
morčete	morče	k1gNnSc2	morče
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
Neurvalá	Neurvalá	k?	Neurvalá
paní	paní	k1gFnSc1	paní
Kadloubková	Kadloubkový	k2eAgFnSc1d1	Kadloubkový
odpraví	odpravit	k5eAaPmIp3nS	odpravit
pojištěné	pojištěný	k2eAgNnSc4d1	pojištěné
morče	morče	k1gNnSc4	morče
<g/>
....	....	k?	....
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
jen	jen	k9	jen
zlatku	zlatka	k1gFnSc4	zlatka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
co	co	k9	co
teprve	teprve	k6eAd1	teprve
za	za	k7c4	za
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
byste	by	kYmCp2nP	by
za	za	k7c4	za
ty	ten	k3xDgInPc4	ten
peníze	peníz	k1gInPc4	peníz
mohla	moct	k5eAaImAgFnS	moct
bydlit	bydlit	k5eAaImF	bydlit
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
....	....	k?	....
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Barbaře	Barbara	k1gFnSc6	Barbara
z	z	k7c2	z
Mníšku	Mníšek	k1gInSc2	Mníšek
<g/>
.	.	kIx.	.
</s>
<s>
Pověsti	pověst	k1gFnSc3	pověst
podobná	podobný	k2eAgFnSc1d1	podobná
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
zaživa	zaživa	k6eAd1	zaživa
pohřbené	pohřbený	k2eAgFnSc6d1	pohřbená
přítelkyni	přítelkyně	k1gFnSc6	přítelkyně
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
duch	duch	k1gMnSc1	duch
dodnes	dodnes	k6eAd1	dodnes
bloudí	bloudit	k5eAaImIp3nS	bloudit
po	po	k7c6	po
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
vyptává	vyptávat	k5eAaImIp3nS	vyptávat
se	se	k3xPyFc4	se
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
novinky	novinka	k1gFnPc4	novinka
a	a	k8xC	a
prorokuje	prorokovat	k5eAaImIp3nS	prorokovat
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Povídkový	povídkový	k2eAgInSc1d1	povídkový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
:	:	kIx,	:
Jedna	jeden	k4xCgFnSc1	jeden
malá	malý	k2eAgFnSc1d1	malá
hezká	hezký	k2eAgFnSc1d1	hezká
idylka	idylka	k1gFnSc1	idylka
<g/>
,	,	kIx,	,
povídková	povídkový	k2eAgFnSc1d1	povídková
črta	črta	k1gFnSc1	črta
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
Štedrý	Štedrý	k2eAgInSc1d1	Štedrý
den	den	k1gInSc1	den
u	u	k7c2	u
paní	paní	k1gFnSc2	paní
Allerheiligové	Allerheiligový	k2eAgFnSc2d1	Allerheiligový
<g/>
,	,	kIx,	,
vdovy	vdova	k1gFnSc2	vdova
z	z	k7c2	z
Celetné	Celetný	k2eAgFnSc2d1	Celetná
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
Paní	paní	k1gFnSc1	paní
Allerheiligová	Allerheiligový	k2eAgFnSc1d1	Allerheiligový
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
Povídky	povídka	k1gFnPc1	povídka
jsou	být	k5eAaImIp3nP	být
laděny	ladit	k5eAaImNgFnP	ladit
jako	jako	k8xS	jako
humoreska	humoreska	k1gFnSc1	humoreska
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
tak	tak	k6eAd1	tak
útlý	útlý	k2eAgInSc4d1	útlý
Fuksův	Fuksův	k2eAgInSc4d1	Fuksův
román	román	k1gInSc4	román
Nebožtíci	nebožtík	k1gMnPc1	nebožtík
na	na	k7c6	na
bále	bál	k1gInSc6	bál
<g/>
.	.	kIx.	.
</s>
<s>
Růžové	růžový	k2eAgInPc1d1	růžový
logaritmy	logaritmus	k1gInPc1	logaritmus
1967	[number]	k4	1967
částečně	částečně	k6eAd1	částečně
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
Fuksových	Fuksových	k2eAgInPc2d1	Fuksových
školních	školní	k2eAgInPc2d1	školní
zážitků	zážitek	k1gInPc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Humorný	humorný	k2eAgInSc1d1	humorný
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
nadaném	nadaný	k2eAgMnSc6d1	nadaný
gymnazistovi	gymnazista	k1gMnSc6	gymnazista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neumí	umět	k5eNaImIp3nS	umět
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Vavřínek	Vavřínek	k1gMnSc1	Vavřínek
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinou	Hrdina	k1gMnSc7	Hrdina
je	být	k5eAaImIp3nS	být
polosirotek	polosirotek	k1gMnSc1	polosirotek
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
hrobníka	hrobník	k1gMnSc2	hrobník
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
grotesknost	grotesknost	k1gFnSc1	grotesknost
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
vrcholných	vrcholný	k2eAgInPc6d1	vrcholný
dílech	díl	k1gInPc6	díl
<g/>
,	,	kIx,	,
provázela	provázet	k5eAaImAgFnS	provázet
Fukse	Fuks	k1gMnPc4	Fuks
i	i	k9	i
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
měl	mít	k5eAaImAgMnS	mít
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
netradičním	tradiční	k2eNgInPc3d1	netradiční
nápadům	nápad	k1gInPc3	nápad
a	a	k8xC	a
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
co	co	k9	co
výmysl	výmysl	k1gInSc4	výmysl
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
byl	být	k5eAaImAgMnS	být
silný	silný	k2eAgInSc4d1	silný
kuřák	kuřák	k1gInSc4	kuřák
a	a	k8xC	a
rád	rád	k6eAd1	rád
pil	pít	k5eAaImAgMnS	pít
červené	červený	k2eAgNnSc4d1	červené
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Fuks	Fuks	k1gMnSc1	Fuks
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
italské	italský	k2eAgFnSc2d1	italská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
