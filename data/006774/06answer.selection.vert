<s>
Výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
přísluší	příslušet	k5eAaImIp3nP	příslušet
základní	základní	k2eAgFnSc4d1	základní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
udržení	udržení	k1gNnSc4	udržení
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
míru	mír	k1gInSc2	mír
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
rezoluce	rezoluce	k1gFnSc2	rezoluce
jsou	být	k5eAaImIp3nP	být
právně	právně	k6eAd1	právně
závazné	závazný	k2eAgFnPc1d1	závazná
<g/>
.	.	kIx.	.
</s>
