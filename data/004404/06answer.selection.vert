<s>
Členy	člen	k1gMnPc7	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
let	léto	k1gNnPc2	léto
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
kontrasignace	kontrasignace	k1gFnSc2	kontrasignace
nebo	nebo	k8xC	nebo
souhlasu	souhlas	k1gInSc2	souhlas
jiného	jiný	k2eAgInSc2d1	jiný
ústavního	ústavní	k2eAgInSc2d1	ústavní
orgánu	orgán	k1gInSc2	orgán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
