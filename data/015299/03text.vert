<s>
Efekt	efekt	k1gInSc1
hrdla	hrdlo	k1gNnSc2
láhve	láhev	k1gFnSc2
(	(	kIx(
<g/>
software	software	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
softwarovém	softwarový	k2eAgNnSc6d1
inženýrství	inženýrství	k1gNnSc6
k	k	k7c3
efektu	efekt	k1gInSc3
hrdla	hrdlo	k1gNnSc2
láhve	láhev	k1gFnSc2
(	(	kIx(
<g/>
bottleneck	bottleneck	k1gInSc1
<g/>
)	)	kIx)
dochází	docházet	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
schopnost	schopnost	k1gFnSc1
aplikace	aplikace	k1gFnSc2
nebo	nebo	k8xC
počítačového	počítačový	k2eAgInSc2d1
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
omezena	omezit	k5eAaPmNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jednotlivé	jednotlivý	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
zpomalují	zpomalovat	k5eAaImIp3nP
celkový	celkový	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
hrdlo	hrdlo	k1gNnSc1
láhve	láhev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúžení	zúžení	k1gNnSc2
má	mít	k5eAaImIp3nS
nejnižší	nízký	k2eAgFnSc1d3
propustnost	propustnost	k1gFnSc1
ze	z	k7c2
všech	všecek	k3xTgMnPc2
častí	častit	k5eAaImIp3nS
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Systémoví	systémový	k2eAgMnPc1d1
návrháři	návrhář	k1gMnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
vyhnout	vyhnout	k5eAaPmF
těmto	tento	k3xDgInPc3
efektům	efekt	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
usilují	usilovat	k5eAaImIp3nP
o	o	k7c6
hledání	hledání	k1gNnSc6
efektivnějších	efektivní	k2eAgNnPc2d2
a	a	k8xC
účinnějších	účinný	k2eAgNnPc2d2
řešení	řešení	k1gNnPc2
pro	pro	k7c4
stávající	stávající	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
příklady	příklad	k1gInPc4
možných	možný	k2eAgFnPc2d1
překážek	překážka	k1gFnPc2
v	v	k7c6
počítačovém	počítačový	k2eAgInSc6d1
systému	systém	k1gInSc6
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
procesor	procesor	k1gInSc1
<g/>
,	,	kIx,
komunikační	komunikační	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
<g/>
,	,	kIx,
disk	disk	k1gInSc1
<g/>
,	,	kIx,
vstup	vstup	k1gInSc1
<g/>
/	/	kIx~
<g/>
výstup	výstup	k1gInSc1
<g/>
,	,	kIx,
atd.	atd.	kA
V	v	k7c6
jakémkoli	jakýkoli	k3yIgInSc6
systému	systém	k1gInSc6
nebo	nebo	k8xC
aplikaci	aplikace	k1gFnSc6
bude	být	k5eAaImBp3nS
existovat	existovat	k5eAaImF
překážka	překážka	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
hrdlo	hrdlo	k1gNnSc1
láhve	láhev	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
práce	práce	k1gFnSc1
přichází	přicházet	k5eAaImIp3nS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
rychlém	rychlý	k2eAgNnSc6d1
tempu	tempo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sledování	sledování	k1gNnSc1
překážek	překážka	k1gFnPc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
známých	známý	k2eAgMnPc2d1
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
horké	horký	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
<g/>
"	"	kIx"
-	-	kIx~
úseky	úsek	k1gInPc7
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
vykonávány	vykonávat	k5eAaImNgInP
nejčastěji	často	k6eAd3
-	-	kIx~
tj.	tj.	kA
mají	mít	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgInSc1d3
počet	počet	k1gInSc1
exekucí	exekuce	k1gFnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
analýza	analýza	k1gFnSc1
výkonnosti	výkonnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snížení	snížení	k1gNnSc1
překážek	překážka	k1gFnPc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
dosaženo	dosáhnout	k5eAaPmNgNnS
s	s	k7c7
pomocí	pomoc	k1gFnSc7
specializovaných	specializovaný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
známých	známá	k1gFnPc2
jako	jako	k8xC,k8xS
výkonové	výkonový	k2eAgMnPc4d1
analyzátory	analyzátor	k1gMnPc4
nebo	nebo	k8xC
analytiky	analytik	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
celkové	celkový	k2eAgFnSc2d1
efektivity	efektivita	k1gFnSc2
algoritmů	algoritmus	k1gInPc2
tyto	tento	k3xDgFnPc1
konkrétní	konkrétní	k2eAgFnPc1d1
části	část	k1gFnPc1
kódu	kód	k1gInSc2
provedly	provést	k5eAaPmAgFnP
co	co	k9
nejrychleji	rychle	k6eAd3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bottleneck	Bottleneck	k1gInSc1
(	(	kIx(
<g/>
software	software	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WESCOTT	WESCOTT	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Every	Evera	k1gFnSc2
Computer	computer	k1gInSc1
Performance	performance	k1gFnSc2
Book	Booka	k1gFnPc2
<g/>
,	,	kIx,
Chapter	Chapter	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
Useful	Useful	k1gInSc1
laws	laws	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
CreateSpace	CreateSpace	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1482657759	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Profilování	profilování	k1gNnSc1
(	(	kIx(
<g/>
programování	programování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Program	program	k1gInSc1
optimalizace	optimalizace	k1gFnSc2
</s>
