<s>
V	v	k7c6
softwarovém	softwarový	k2eAgNnSc6d1
inženýrství	inženýrství	k1gNnSc6
k	k	k7c3
efektu	efekt	k1gInSc3
hrdla	hrdlo	k1gNnSc2
láhve	láhev	k1gFnSc2
(	(	kIx(
<g/>
bottleneck	bottleneck	k1gInSc1
<g/>
)	)	kIx)
dochází	docházet	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
schopnost	schopnost	k1gFnSc1
aplikace	aplikace	k1gFnSc2
nebo	nebo	k8xC
počítačového	počítačový	k2eAgInSc2d1
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
omezena	omezit	k5eAaPmNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jednotlivé	jednotlivý	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
zpomalují	zpomalovat	k5eAaImIp3nP
celkový	celkový	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
hrdlo	hrdlo	k1gNnSc1
láhve	láhev	k1gFnSc2
<g/>
.	.	kIx.
</s>