<s>
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Helmut	Helmut	k2eAgMnSc1d1
Herzfeld	Herzfeld	k1gMnSc1
Narození	narození	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1891	#num#	k4
Schmargendorf	Schmargendorf	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1968	#num#	k4
východní	východní	k2eAgInSc1d1
Berlín	Berlín	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Dorotheenstädtischer	Dorotheenstädtischra	k1gFnPc2
Friedhof	Friedhof	k1gMnSc1
Povolání	povolání	k1gNnSc3
</s>
<s>
fotograf	fotograf	k1gMnSc1
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
plakátů	plakát	k1gInPc2
<g/>
,	,	kIx,
designér	designér	k1gMnSc1
<g/>
,	,	kIx,
kolážista	kolážista	k1gMnSc1
a	a	k8xC
vydavatel	vydavatel	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Franz	Franz	k1gMnSc1
Herzfeld	Herzfeld	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Wieland	Wieland	k1gInSc1
Herzfelde	Herzfeld	k1gInSc5
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
Karla	Karel	k1gMnSc2
MarxeZlatý	MarxeZlatý	k2eAgInSc1d1
vlastenecký	vlastenecký	k2eAgInSc1d1
záslužný	záslužný	k2eAgInSc1d1
řádNárodní	řádNárodní	k2eAgInSc1d1
cena	cena	k1gFnSc1
Německé	německý	k2eAgFnSc2d1
demokratické	demokratický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Heartfiled	Heartfiled	k1gMnSc1
<g/>
:	:	kIx,
Ruka	ruka	k1gFnSc1
má	mít	k5eAaImIp3nS
5	#num#	k4
prstů	prst	k1gInPc2
<g/>
,	,	kIx,
plakát	plakát	k1gInSc1
k	k	k7c3
příležitosti	příležitost	k1gFnSc3
voleb	volba	k1gFnPc2
do	do	k7c2
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
(	(	kIx(
<g/>
pro	pro	k7c4
KPD	KPD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
květen	květen	k1gInSc4
1928	#num#	k4
</s>
<s>
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
<g/>
,	,	kIx,
narozen	narozen	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Helmut	Helmut	k1gMnSc1
Herzfeld	Herzfeld	k1gMnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1891	#num#	k4
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
fotomontáží	fotomontáž	k1gFnSc7
a	a	k8xC
politickou	politický	k2eAgFnSc7d1
satirou	satira	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
aktivním	aktivní	k2eAgMnSc7d1
členem	člen	k1gMnSc7
hnutí	hnutí	k1gNnSc2
Dada	dada	k1gNnSc2
<g/>
,	,	kIx,
spolku	spolek	k1gInSc2
výtvarných	výtvarný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
Mánes	Mánes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlásil	hlásit	k5eAaImAgMnS
se	se	k3xPyFc4
k	k	k7c3
antifašismu	antifašismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žil	žít	k5eAaImAgMnS
v	v	k7c6
exilu	exil	k1gInSc6
v	v	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1891	#num#	k4
jako	jako	k8xC,k8xS
Helmut	Helmut	k1gMnSc1
Herzfeld	Herzfeld	k1gMnSc1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
Franzi	Franze	k1gFnSc4
a	a	k8xC
Alici	Alice	k1gFnSc4
Herzfeldovým	Herzfeldová	k1gFnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
aktivními	aktivní	k2eAgMnPc7d1
účastníky	účastník	k1gMnPc7
společenského	společenský	k2eAgNnSc2d1
dění	dění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
spisovatelem	spisovatel	k1gMnSc7
klonícím	klonící	k2eAgMnSc7d1
se	se	k3xPyFc4
k	k	k7c3
socialismu	socialismus	k1gInSc3
a	a	k8xC
matka	matka	k1gFnSc1
politickou	politický	k2eAgFnSc7d1
aktivistkou	aktivistka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1889	#num#	k4
byli	být	k5eAaImAgMnP
Helmutovi	Helmutův	k2eAgMnPc1d1
rodiče	rodič	k1gMnPc1
nařčeni	nařknout	k5eAaPmNgMnP
z	z	k7c2
rouhačství	rouhačství	k1gNnSc2
a	a	k8xC
byli	být	k5eAaImAgMnP
tedy	tedy	k8xC
nuceni	nutit	k5eAaImNgMnP
opustit	opustit	k5eAaPmF
Německo	Německo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanechali	zanechat	k5eAaPmAgMnP
tak	tak	k6eAd1
své	svůj	k3xOyFgFnPc4
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
v	v	k7c6
péči	péče	k1gFnSc6
strýce	strýc	k1gMnSc4
a	a	k8xC
následně	následně	k6eAd1
i	i	k9
dalších	další	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Helmut	Helmut	k1gMnSc1
brzy	brzy	k6eAd1
projevil	projevit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
talent	talent	k1gInSc4
pro	pro	k7c4
malbu	malba	k1gFnSc4
a	a	k8xC
začal	začít	k5eAaPmAgInS
tedy	tedy	k9
roku	rok	k1gInSc2
1908	#num#	k4
studovat	studovat	k5eAaImF
Könglische-Bayerische	Könglische-Bayerische	k1gFnSc4
Kunstgewerbeschule	Kunstgewerbeschule	k1gFnSc2
(	(	kIx(
<g/>
Královskou	královský	k2eAgFnSc4d1
Bavorskou	bavorský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
umění	umění	k1gNnPc2
a	a	k8xC
řemesel	řemeslo	k1gNnPc2
<g/>
)	)	kIx)
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
reklamními	reklamní	k2eAgMnPc7d1
designéry	designér	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počínající	počínající	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
-	-	kIx~
Albertem	Albert	k1gMnSc7
Weisgerberem	Weisgerber	k1gMnSc7
a	a	k8xC
Ludwigem	Ludwig	k1gMnSc7
Hohlweinem	Hohlwein	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
sám	sám	k3xTgMnSc1
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Berlíně	Berlín	k1gInSc6
Herzfeld	Herzfelda	k1gFnPc2
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiích	studio	k1gNnPc6
na	na	k7c4
Kunst	Kunst	k1gFnSc4
und	und	k?
Handwerkerschule	Handwerkerschule	k1gFnSc1
(	(	kIx(
<g/>
umělecko-průmyslové	umělecko-průmyslový	k2eAgFnSc3d1
škole	škola	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Ernsta	Ernst	k1gMnSc2
Neumana	Neuman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pětadvaceti	pětadvacet	k4xCc6
letech	léto	k1gNnPc6
byl	být	k5eAaImAgInS
Herzfeld	Herzfeld	k1gInSc1
odveden	odvést	k5eAaPmNgInS
do	do	k7c2
německé	německý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
jeho	jeho	k3xOp3gFnSc1
aktivní	aktivní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
netrvala	trvat	k5eNaImAgFnS
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
předstíral	předstírat	k5eAaImAgMnS
nervové	nervový	k2eAgNnSc4d1
zhroucení	zhroucení	k1gNnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
z	z	k7c2
armády	armáda	k1gFnSc2
propuštěn	propuštěn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
období	období	k1gNnSc1
zuřivého	zuřivý	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
a	a	k8xC
protibritského	protibritský	k2eAgNnSc2d1
smýšlení	smýšlení	k1gNnSc2
ve	v	k7c6
znamení	znamení	k1gNnSc6
hesla	heslo	k1gNnSc2
„	„	k?
<g/>
Bůh	bůh	k1gMnSc1
trestej	trestat	k5eAaImRp2nS
Anglii	Anglie	k1gFnSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
se	se	k3xPyFc4
u	u	k7c2
Herzfelda	Herzfeld	k1gMnSc2
projevilo	projevit	k5eAaPmAgNnS
prvními	první	k4xOgInPc7
výraznějšími	výrazný	k2eAgInPc7d2
protiválečnými	protiválečný	k2eAgInPc7d1
protesty	protest	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
změnou	změna	k1gFnSc7
jména	jméno	k1gNnSc2
na	na	k7c4
John	John	k1gMnSc1
Heartfield	Heartfieldo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
do	do	k7c2
roku	rok	k1gInSc2
1916	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
též	též	k9
založení	založení	k1gNnSc4
vydavatelství	vydavatelství	k1gNnSc2
Neue	Neue	k1gNnSc2
Jugend	Jugend	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
Heartfield	Heartfield	k1gMnSc1
podílel	podílet	k5eAaImAgMnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Wielandem	Wieland	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
přechodnou	přechodný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byl	být	k5eAaImAgInS
Heartfield	Heartfield	k1gInSc1
též	též	k9
fotosazečem	fotosazeč	k1gMnSc7
a	a	k8xC
vedoucím	vedoucí	k1gMnSc7
UFA	UFA	kA
<g/>
,	,	kIx,
německé	německý	k2eAgFnSc2d1
vzdělávací	vzdělávací	k2eAgFnSc2d1
filmové	filmový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
byl	být	k5eAaImAgMnS
ovšem	ovšem	k9
vyloučen	vyloučen	k2eAgMnSc1d1
za	za	k7c4
podporu	podpora	k1gFnSc4
stávky	stávka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
následovala	následovat	k5eAaImAgFnS
po	po	k7c6
vraždě	vražda	k1gFnSc6
Karla	Karel	k1gMnSc2
Liebknechta	Liebknecht	k1gMnSc2
a	a	k8xC
Rosy	Rosa	k1gFnSc2
Luxemburg	Luxemburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
1918	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
Hertfieldovu	Hertfieldův	k2eAgFnSc4d1
další	další	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
určující	určující	k2eAgFnSc4d1
díky	díky	k7c3
členství	členství	k1gNnSc3
v	v	k7c6
berlínském	berlínský	k2eAgNnSc6d1
seskupení	seskupení	k1gNnSc6
Dada	dada	k1gNnSc2
a	a	k8xC
také	také	k9
v	v	k7c6
německé	německý	k2eAgFnSc6d1
komunistické	komunistický	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1919	#num#	k4
až	až	k9
1933	#num#	k4
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
umělecké	umělecký	k2eAgFnSc6d1
a	a	k8xC
novinářské	novinářský	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Georgem	Georg	k1gMnSc7
Grozsem	Grozs	k1gMnSc7
založil	založit	k5eAaPmAgInS
satirický	satirický	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Die	Die	k1gMnSc5
Pleite	Pleit	k1gMnSc5
(	(	kIx(
<g/>
Bankrot	bankrot	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spolupodílel	spolupodílet	k5eAaImAgMnS
se	se	k3xPyFc4
na	na	k7c4
pořádání	pořádání	k1gNnSc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
dadaistické	dadaistický	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
nebo	nebo	k8xC
vydávání	vydávání	k1gNnSc2
periodika	periodikum	k1gNnSc2
Die	Die	k1gMnSc1
Knüpel	Knüpel	k1gMnSc1
<g/>
,	,	kIx,
komunistického	komunistický	k2eAgInSc2d1
Die	Die	k1gMnSc5
Rote	Rotus	k1gMnSc5
Fahne	Fahn	k1gMnSc5
(	(	kIx(
<g/>
Červená	červenat	k5eAaImIp3nS
vlajka	vlajka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Arbeiter-Illustrierte	Arbeiter-Illustriert	k1gInSc5
Zeitung	Zeitunga	k1gFnPc2
(	(	kIx(
<g/>
Ilustrované	ilustrovaný	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
pracujících	pracující	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
zásadní	zásadní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
další	další	k2eAgNnSc4d1
Heartfieldovo	Heartfieldův	k2eAgNnSc4d1
směřování	směřování	k1gNnSc4
byl	být	k5eAaImAgMnS
bezesporu	bezesporu	k9
Bertold	Bertold	k1gMnSc1
Brecht	Brecht	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
Heartfield	Heartfield	k1gMnSc1
potkal	potkat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
10	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
svět	svět	k1gInSc4
jeho	jeho	k3xOp3gFnSc4
první	první	k4xOgFnSc4
fotomontáž	fotomontáž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
Německu	Německo	k1gNnSc6
dostal	dostat	k5eAaPmAgMnS
k	k	k7c3
moci	moc	k1gFnSc3
Hitlerův	Hitlerův	k2eAgInSc4d1
fašistický	fašistický	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
Heartfield	Heartfield	k1gMnSc1
do	do	k7c2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
práce	práce	k1gFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
součástí	součást	k1gFnSc7
výstav	výstava	k1gFnPc2
pražského	pražský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Mánes	Mánes	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
příčinou	příčina	k1gFnSc7
diplomatických	diplomatický	k2eAgInPc2d1
sporů	spor	k1gInPc2
mezi	mezi	k7c7
českou	český	k2eAgFnSc7d1
a	a	k8xC
německou	německý	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostoucí	rostoucí	k2eAgInSc4d1
nepokoj	nepokoj	k1gInSc4
týkající	týkající	k2eAgInSc4d1
se	se	k3xPyFc4
důsledků	důsledek	k1gInPc2
Heartfieldovy	Heartfieldův	k2eAgFnSc2d1
přítomnosti	přítomnost	k1gFnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
spolu	spolu	k6eAd1
s	s	k7c7
okupací	okupace	k1gFnSc7
donutily	donutit	k5eAaPmAgFnP
umělce	umělec	k1gMnPc4
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
přesídlení	přesídlení	k1gNnSc3
–	–	k?
tentokrát	tentokrát	k6eAd1
do	do	k7c2
Anglie	Anglie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
pak	pak	k6eAd1
poskytl	poskytnout	k5eAaPmAgInS
prostor	prostor	k1gInSc1
pro	pro	k7c4
významnou	významný	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
„	„	k?
<g/>
Válka	válka	k1gFnSc1
jednoho	jeden	k4xCgMnSc2
muže	muž	k1gMnSc2
proti	proti	k7c3
Hitlerovi	Hitler	k1gMnSc3
<g/>
“	“	k?
v	v	k7c6
Arcade	Arcad	k1gInSc5
Gallery	Galler	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
ani	ani	k9
zde	zde	k6eAd1
Heartfield	Heartfield	k1gMnSc1
nenalezl	naleznout	k5eNaPmAgMnS,k5eNaBmAgMnS
vhodné	vhodný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
byl	být	k5eAaImAgMnS
zadržen	zadržet	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
nepřítel	nepřítel	k1gMnSc1
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgInSc3
německému	německý	k2eAgInSc3d1
původu	původ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
ale	ale	k8xC
League	League	k1gFnSc1
of	of	k?
Culture	Cultur	k1gMnSc5
poctila	poctít	k5eAaPmAgFnS
Heartfielda	Heartfielda	k1gFnSc1
výstavou	výstava	k1gFnSc7
jeho	jeho	k3xOp3gFnPc2
fotomontáží	fotomontáž	k1gFnPc2
a	a	k8xC
knižních	knižní	k2eAgFnPc2d1
obálek	obálka	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dostal	dostat	k5eAaPmAgMnS
též	též	k9
příležistost	příležistost	k1gFnSc4
přednášet	přednášet	k5eAaImF
o	o	k7c6
metodách	metoda	k1gFnPc6
fotomontáže	fotomontáž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
pak	pak	k6eAd1
následovalo	následovat	k5eAaImAgNnS
vydání	vydání	k1gNnSc4
knihy	kniha	k1gFnSc2
Konrada	Konrada	k1gFnSc1
Farnera	Farner	k1gMnSc2
–	–	k?
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Photomontages	Photomontages	k1gMnSc1
and	and	k?
Contemporary	Contemporara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
Heartfieldovi	Heartfield	k1gMnSc3
dostalo	dostat	k5eAaPmAgNnS
jistého	jistý	k2eAgNnSc2d1
zadostiučinění	zadostiučinění	k1gNnSc2
i	i	k9
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
domoviny	domovina	k1gFnSc2
–	–	k?
obdržel	obdržet	k5eAaPmAgMnS
nabídku	nabídka	k1gFnSc4
z	z	k7c2
Humboldtovy	Humboldtův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
stát	stát	k5eAaImF,k5eAaPmF
se	s	k7c7
profesorem	profesor	k1gMnSc7
grafiky	grafika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
nabídky	nabídka	k1gFnSc2
se	se	k3xPyFc4
Heartfield	Heartfield	k1gMnSc1
mohl	moct	k5eAaImAgMnS
vrátit	vrátit	k5eAaPmF
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
opět	opět	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
založil	založit	k5eAaPmAgMnS
vlastní	vlastní	k2eAgNnSc1d1
vydavatelství	vydavatelství	k1gNnSc1
Werkstatt	Werkstatta	k1gFnPc2
H	H	kA
<g/>
&	&	k?
<g/>
H.	H.	kA
</s>
<s>
Heartfield	Heartfield	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
i	i	k9
po	po	k7c6
návratu	návrat	k1gInSc6
domů	dům	k1gInPc2
znovu	znovu	k6eAd1
pronásledován	pronásledován	k2eAgMnSc1d1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgInPc3
zahraničním	zahraniční	k2eAgInPc3d1
kontaktům	kontakt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
neustálá	neustálý	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
nepohodlnost	nepohodlnost	k1gFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
vězněním	věznění	k1gNnSc7
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
prošel	projít	k5eAaPmAgMnS
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
podepsala	podepsat	k5eAaPmAgFnS
na	na	k7c6
Heartfieldově	Heartfieldův	k2eAgNnSc6d1
zdraví	zdraví	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
od	od	k7c2
těch	ten	k3xDgFnPc2
dob	doba	k1gFnPc2
zůstalo	zůstat	k5eAaPmAgNnS
značně	značně	k6eAd1
podlomené	podlomený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Padesátá	padesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
pak	pak	k6eAd1
pro	pro	k7c4
umělce	umělec	k1gMnSc4
znamenala	znamenat	k5eAaImAgFnS
definitivní	definitivní	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
přínosu	přínos	k1gInSc2
pro	pro	k7c4
společensko-politické	společensko-politický	k2eAgNnSc4d1
a	a	k8xC
sociální	sociální	k2eAgNnSc4d1
dění	dění	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
vydobytím	vydobytí	k1gNnSc7
postavení	postavení	k1gNnSc2
též	též	k6eAd1
na	na	k7c6
poli	pole	k1gNnSc6
uměleckém	umělecký	k2eAgNnSc6d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
politickým	politický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zasahovaly	zasahovat	k5eAaImAgFnP
celou	celý	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heartfield	Heartfield	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Svazu	svaz	k1gInSc2
výtvarných	výtvarný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
Německa	Německo	k1gNnSc2
i	i	k8xC
členem	člen	k1gInSc7
Svazu	svaz	k1gInSc2
Československých	československý	k2eAgMnPc2d1
výtvarných	výtvarný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
vystavoval	vystavovat	k5eAaImAgMnS
a	a	k8xC
přednášel	přednášet	k5eAaImAgMnS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
též	též	k9
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
výstavy	výstava	k1gFnSc2
o	o	k7c6
příčinách	příčina	k1gFnPc6
a	a	k8xC
vzniku	vznik	k1gInSc6
nacismu	nacismus	k1gInSc2
ve	v	k7c6
špilberské	špilberský	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
umělcova	umělcův	k2eAgFnSc1d1
stálá	stálý	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1968	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Tvorba	tvorba	k1gFnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
Nagel	Nagel	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
a	a	k8xC
Wieland	Wieland	k1gInSc1
Herzfelde	Herzfeld	k1gInSc5
na	na	k7c6
výstavě	výstava	k1gFnSc6
Akademie	akademie	k1gFnSc2
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1960	#num#	k4
</s>
<s>
Předešlé	předešlý	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
z	z	k7c2
vydavatelské	vydavatelský	k2eAgFnSc2d1
a	a	k8xC
umělecké	umělecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
fotosazby	fotosazba	k1gFnSc2
i	i	k8xC
z	z	k7c2
dadaistického	dadaistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
vedly	vést	k5eAaImAgFnP
Heartfilda	Heartfilda	k1gFnSc1
k	k	k7c3
fotomontáži	fotomontáž	k1gFnSc3
jako	jako	k8xC,k8xS
nástroji	nástroj	k1gInSc3
upozornění	upozornění	k1gNnSc2
na	na	k7c4
aktuální	aktuální	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
dění	dění	k1gNnSc4
a	a	k8xC
protestu	protest	k1gInSc2
proti	proti	k7c3
nekalým	kalý	k2eNgFnPc3d1
politickým	politický	k2eAgFnPc3d1
a	a	k8xC
mocenským	mocenský	k2eAgFnPc3d1
praktikám	praktika	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Razil	razit	k5eAaImAgMnS
heslo	heslo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
zaznělo	zaznět	k5eAaImAgNnS,k5eAaPmAgNnS
v	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
časopisů	časopis	k1gInPc2
-	-	kIx~
„	„	k?
<g/>
Použijte	použít	k5eAaPmRp2nP
fotografii	fotografia	k1gFnSc4
jako	jako	k8xS,k8xC
zbraň	zbraň	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvinul	vyvinout	k5eAaPmAgMnS
unikátní	unikátní	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
„	„	k?
<g/>
znovuvyužití	znovuvyužití	k1gNnSc2
<g/>
“	“	k?
fotografií	fotografia	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
značný	značný	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
efekt	efekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
časech	čas	k1gInPc6
veliké	veliký	k2eAgFnPc1d1
nejistoty	nejistota	k1gFnPc1
Heartfieldovy	Heartfieldův	k2eAgInPc4d1
zneklidňující	zneklidňující	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
předpovídaly	předpovídat	k5eAaImAgFnP
a	a	k8xC
odrážely	odrážet	k5eAaImAgFnP
chaos	chaos	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
zakusilo	zakusit	k5eAaPmAgNnS
Německo	Německo	k1gNnSc1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sklouzávalo	sklouzávat	k5eAaImAgNnS
k	k	k7c3
sociální	sociální	k2eAgFnSc3d1
a	a	k8xC
politické	politický	k2eAgFnSc3d1
katastrofě	katastrofa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
atmosféře	atmosféra	k1gFnSc6
se	se	k3xPyFc4
komunisté	komunista	k1gMnPc1
<g/>
,	,	kIx,
nacisté	nacista	k1gMnPc1
a	a	k8xC
další	další	k2eAgMnPc1d1
přívrženci	přívrženec	k1gMnPc1
střetávali	střetávat	k5eAaImAgMnP
u	u	k7c2
volebních	volební	k2eAgFnPc2d1
uren	urna	k1gFnPc2
a	a	k8xC
na	na	k7c6
ulicích	ulice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinek	účinek	k1gInSc1
Heartfieldovy	Heartfieldův	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
silný	silný	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pomohl	pomoct	k5eAaPmAgMnS
transformovat	transformovat	k5eAaBmF
fotomontáž	fotomontáž	k1gFnSc4
až	až	k9
k	k	k7c3
účinné	účinný	k2eAgFnSc3d1
formě	forma	k1gFnSc3
masové	masový	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heartfield	Heartfield	k1gMnSc1
například	například	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
pro	pro	k7c4
komunistickou	komunistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Německa	Německo	k1gNnSc2
symboly	symbol	k1gInPc7
na	na	k7c6
základě	základ	k1gInSc6
fotografií	fotografia	k1gFnPc2
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgNnSc1d1
organizaci	organizace	k1gFnSc4
soutěžit	soutěžit	k5eAaImF
s	s	k7c7
nacistickou	nacistický	k2eAgFnSc7d1
svastikou	svastika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
jeho	jeho	k3xOp3gInPc4
obrazy	obraz	k1gInPc4
zaťatých	zaťatý	k2eAgFnPc2d1
pěstí	pěst	k1gFnPc2
<g/>
,	,	kIx,
otevřených	otevřený	k2eAgFnPc2d1
dlaní	dlaň	k1gFnPc2
a	a	k8xC
zvednutých	zvednutý	k2eAgFnPc2d1
paží	paže	k1gFnPc2
vyjadřovaly	vyjadřovat	k5eAaImAgFnP
výraznou	výrazný	k2eAgFnSc4d1
akci	akce	k1gFnSc4
a	a	k8xC
odhodlanost	odhodlanost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotomontáž	fotomontáž	k1gFnSc1
dovolila	dovolit	k5eAaPmAgFnS
Heartfieldovi	Heartfield	k1gMnSc3
tvořit	tvořit	k5eAaImF
obsahově	obsahově	k6eAd1
nabité	nabitý	k2eAgFnPc1d1
a	a	k8xC
politicky	politicky	k6eAd1
ambivalentní	ambivalentní	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tvorbě	tvorba	k1gFnSc3
takových	takový	k3xDgFnPc2
prací	práce	k1gFnPc2
si	se	k3xPyFc3
vybíral	vybírat	k5eAaImAgMnS
snadno	snadno	k6eAd1
identifikovatelné	identifikovatelný	k2eAgFnPc4d1
fotografie	fotografia	k1gFnPc4
politiků	politik	k1gMnPc2
nebo	nebo	k8xC
událostí	událost	k1gFnPc2
z	z	k7c2
masově	masově	k6eAd1
čtených	čtený	k2eAgFnPc2d1
novin	novina	k1gFnPc2
a	a	k8xC
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
vybral	vybrat	k5eAaPmAgMnS
části	část	k1gFnPc4
fotografií	fotografia	k1gFnPc2
a	a	k8xC
znovu	znovu	k6eAd1
je	být	k5eAaImIp3nS
uspořádal	uspořádat	k5eAaPmAgMnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
radikálně	radikálně	k6eAd1
změnily	změnit	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Heartfieldovou	Heartfieldová	k1gFnSc7
nejsilnějsí	jsilnějsit	k5eNaPmIp3nS
zbraní	zbraň	k1gFnSc7
tvorby	tvorba	k1gFnSc2
bylo	být	k5eAaImAgNnS
střídání	střídání	k1gNnSc1
měřítka	měřítko	k1gNnSc2
a	a	k8xC
„	„	k?
<g/>
strnulosti	strnulost	k1gFnSc2
<g/>
“	“	k?
skládaných	skládaný	k2eAgFnPc2d1
částí	část	k1gFnPc2
fotografií	fotografia	k1gFnPc2
směrem	směr	k1gInSc7
k	k	k7c3
aktivování	aktivování	k1gNnSc3
již	již	k6eAd1
tak	tak	k6eAd1
hrozivých	hrozivý	k2eAgInPc2d1
fotofragmentů	fotofragment	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
tak	tak	k6eAd1
mohl	moct	k5eAaImAgInS
mít	mít	k5eAaImF
děsivý	děsivý	k2eAgInSc4d1
vizuální	vizuální	k2eAgInSc4d1
dopad	dopad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heartfield	Heartfield	k1gInSc1
tak	tak	k6eAd1
například	například	k6eAd1
vytvořil	vytvořit	k5eAaPmAgInS
koláž	koláž	k1gFnSc4
nazvanou	nazvaný	k2eAgFnSc4d1
„	„	k?
<g/>
Adolf	Adolf	k1gMnSc1
<g/>
,	,	kIx,
nadčlověk	nadčlověk	k1gMnSc1
polykající	polykající	k2eAgNnSc1d1
zlato	zlato	k1gNnSc1
a	a	k8xC
chrlící	chrlící	k2eAgInSc1d1
cín	cín	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
překrytím	překrytí	k1gNnSc7
známé	známá	k1gFnSc2
Hitlerovy	Hitlerův	k2eAgFnSc2d1
fotografie	fotografia	k1gFnSc2
obrazem	obraz	k1gInSc7
rentgenované	rentgenovaný	k2eAgFnPc4d1
hrudi	hruď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
Heartfield	Heartfield	k1gMnSc1
odkazuje	odkazovat	k5eAaImIp3nS
k	k	k7c3
obrovským	obrovský	k2eAgInPc3d1
příspěvkům	příspěvek	k1gInPc3
<g/>
,	,	kIx,
kterými	který	k3yIgMnPc7,k3yQgMnPc7,k3yRgMnPc7
bohatí	bohatit	k5eAaImIp3nP
průmyslníci	průmyslník	k1gMnPc1
přispívali	přispívat	k5eAaImAgMnP
nacistické	nacistický	k2eAgNnSc4d1
straně	strana	k1gFnSc6
navzdory	navzdory	k7c3
její	její	k3xOp3gFnSc3
údajné	údajný	k2eAgFnSc3d1
základně	základna	k1gFnSc3
v	v	k7c6
socialismu	socialismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Heartfield	Heartfield	k1gMnSc1
také	také	k9
rozpoutal	rozpoutat	k5eAaPmAgMnS
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
nejostřejších	ostrý	k2eAgFnPc2d3
satir	satira	k1gFnPc2
Hitlerova	Hitlerův	k2eAgInSc2d1
kultu	kult	k1gInSc2
vůdce	vůdce	k1gMnSc2
<g/>
,	,	kIx,
základu	základ	k1gInSc2
německého	německý	k2eAgInSc2d1
fašismu	fašismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc4
fotomontáže	fotomontáž	k1gFnPc1
parodovaly	parodovat	k5eAaImAgFnP
Hitlerovy	Hitlerův	k2eAgFnPc4d1
pózy	póza	k1gFnPc4
<g/>
,	,	kIx,
gesta	gesto	k1gNnPc4
a	a	k8xC
symboly	symbol	k1gInPc4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
dojmu	dojem	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
stačí	stačit	k5eAaBmIp3nS
pouze	pouze	k6eAd1
seškrábnout	seškrábnout	k5eAaPmF
tenký	tenký	k2eAgInSc4d1
povrch	povrch	k1gInSc4
fašistické	fašistický	k2eAgFnSc2d1
propagandy	propaganda	k1gFnSc2
k	k	k7c3
odkrytí	odkrytí	k1gNnSc3
její	její	k3xOp3gFnSc2
absurdní	absurdní	k2eAgFnSc2d1
reality	realita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Herzfelde	Herzfelde	k6eAd1
<g/>
,	,	kIx,
Wieland	Wieland	k1gInSc1
<g/>
:	:	kIx,
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
<g/>
:	:	kIx,
Leben	Leben	k1gInSc1
und	und	k?
Werk	Werk	k1gInSc1
<g/>
/	/	kIx~
<g/>
Dargestellt	Dargestellt	k2eAgInSc1d1
von	von	k1gInSc1
Wieland	Wielanda	k1gFnPc2
Herzfelde	Herzfeld	k1gInSc5
<g/>
,	,	kIx,
Wieland	Wieland	k1gInSc1
Dresden	Dresdna	k1gFnPc2
<g/>
:	:	kIx,
Verl	Verla	k1gFnPc2
<g/>
.	.	kIx.
der	drát	k5eAaImRp2nS
Kunst	Kunst	k1gInSc1
<g/>
,	,	kIx,
1986	#num#	k4
</s>
<s>
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
<g/>
:	:	kIx,
Exil	exil	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
ČSR	ČSR	kA
<g/>
:	:	kIx,
Fotomontage	Fotomontage	k1gInSc1
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
:	:	kIx,
Katalog	katalog	k1gInSc1
<g/>
:	:	kIx,
Ausstellung	Ausstellung	k1gMnSc1
im	im	k?
Otto	Otto	k1gMnSc1
Nagel-Haus	Nagel-Haus	k1gMnSc1
vom	vom	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Januar	Januar	k1gInSc1
bis	bis	k?
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
April	April	k1gInSc1
1986	#num#	k4
<g/>
,	,	kIx,
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Staatliche	Staatliche	k1gInSc1
Museen	Musena	k1gFnPc2
<g/>
,	,	kIx,
1986	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
John	John	k1gMnSc1
Heartfield	Heartfielda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
</s>
<s>
Výstava	výstava	k1gFnSc1
Agitated	Agitated	k1gMnSc1
Images	Images	k1gMnSc1
<g/>
:	:	kIx,
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
&	&	k?
German	German	k1gMnSc1
Photomontage	Photomontage	k1gNnSc1
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
v	v	k7c6
J.	J.	kA
Paul	Paul	k1gMnSc1
Getty	Getta	k1gMnSc2
Museum	museum	k1gNnSc4
</s>
<s>
John	John	k1gMnSc1
Heartfield	Heartfield	k1gMnSc1
–	–	k?
fotomontáže	fotomontáž	k1gFnSc2
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700683	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118547437	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8189	#num#	k4
4288	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79139067	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500018521	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
120725202	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79139067	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
