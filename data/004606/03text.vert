<s>
Cizrna	cizrna	k1gFnSc1	cizrna
(	(	kIx(	(
<g/>
Cicer	cicero	k1gNnPc2	cicero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
rod	rod	k1gInSc4	rod
bobovitých	bobovitý	k2eAgFnPc2d1	bobovitá
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
rod	rod	k1gInSc1	rod
tribu	tribat	k5eAaPmIp1nS	tribat
Cicereae	Cicereae	k1gInSc4	Cicereae
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přibližně	přibližně	k6eAd1	přibližně
43	[number]	k4	43
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
cizrna	cizrna	k1gFnSc1	cizrna
beraní	beranit	k5eAaImIp3nS	beranit
(	(	kIx(	(
<g/>
Cicer	cicero	k1gNnPc2	cicero
arietinum	arietinum	k1gInSc1	arietinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
římský	římský	k2eAgInSc1d1	římský
hrách	hrách	k1gInSc1	hrách
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
domestikován	domestikovat	k5eAaBmNgInS	domestikovat
již	již	k6eAd1	již
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pěstován	pěstován	k2eAgMnSc1d1	pěstován
jako	jako	k8xS	jako
luštěnina	luštěnina	k1gFnSc1	luštěnina
<g/>
.	.	kIx.	.
</s>
<s>
Cizrny	cizrna	k1gFnPc1	cizrna
jsou	být	k5eAaImIp3nP	být
jednoleté	jednoletý	k2eAgFnPc1d1	jednoletá
nebo	nebo	k8xC	nebo
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
byliny	bylina	k1gFnPc1	bylina
se	s	k7c7	s
složenými	složený	k2eAgInPc7d1	složený
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
pokryty	pokryt	k2eAgInPc4d1	pokryt
žláznatými	žláznatý	k2eAgInPc7d1	žláznatý
chlupy	chlup	k1gInPc7	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
lichozpeřené	lichozpeřený	k2eAgNnSc4d1	lichozpeřené
nebo	nebo	k8xC	nebo
sudozpeřené	sudozpeřený	k2eAgNnSc4d1	sudozpeřený
s	s	k7c7	s
vřetenem	vřeten	k1gInSc7	vřeten
listu	list	k1gInSc2	list
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přecházejícím	přecházející	k2eAgInSc6d1	přecházející
v	v	k7c6	v
úponku	úponek	k1gInSc6	úponek
<g/>
.	.	kIx.	.
</s>
<s>
Lístků	lístek	k1gInPc2	lístek
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
složených	složený	k2eAgInPc6d1	složený
listech	list	k1gInPc6	list
3	[number]	k4	3
až	až	k9	až
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zubaté	zubatá	k1gFnSc2	zubatá
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zubaté	zubatá	k1gFnSc2	zubatá
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
nebo	nebo	k8xC	nebo
v	v	k7c6	v
chudých	chudý	k2eAgInPc6d1	chudý
úžlabních	úžlabní	k2eAgInPc6d1	úžlabní
hroznech	hrozen	k1gInPc6	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
5	[number]	k4	5
stejnými	stejné	k1gNnPc7	stejné
nebo	nebo	k8xC	nebo
nestejnými	stejný	k2eNgInPc7d1	nestejný
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
typickou	typický	k2eAgFnSc4d1	typická
skladbu	skladba	k1gFnSc4	skladba
květů	květ	k1gInPc2	květ
bobovitých	bobovitý	k2eAgInPc2d1	bobovitý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
fialová	fialový	k2eAgFnSc1d1	fialová
nebo	nebo	k8xC	nebo
růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
deset	deset	k4xCc1	deset
<g/>
,	,	kIx,	,
devět	devět	k4xCc1	devět
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
srostlých	srostlý	k2eAgFnPc2d1	srostlá
nitkami	nitka	k1gFnPc7	nitka
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
horní	horní	k2eAgFnSc1d1	horní
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
chlupatý	chlupatý	k2eAgMnSc1d1	chlupatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
lysou	lysý	k2eAgFnSc7d1	Lysá
čnělkou	čnělka	k1gFnSc7	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
plochý	plochý	k2eAgInSc1d1	plochý
nezaškrcovaný	zaškrcovaný	k2eNgInSc1d1	zaškrcovaný
lusk	lusk	k1gInSc1	lusk
pukající	pukající	k2eAgFnSc2d1	pukající
oběma	dva	k4xCgInPc7	dva
švy	šev	k1gInPc7	šev
a	a	k8xC	a
obsahující	obsahující	k2eAgFnPc1d1	obsahující
jedno	jeden	k4xCgNnSc1	jeden
až	až	k9	až
deset	deset	k4xCc4	deset
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
kulovitá	kulovitý	k2eAgNnPc1d1	kulovité
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
cizrna	cizrna	k1gFnSc1	cizrna
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
43	[number]	k4	43
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
od	od	k7c2	od
východního	východní	k2eAgNnSc2d1	východní
Středomoří	středomoří	k1gNnSc2	středomoří
po	po	k7c6	po
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Izolované	izolovaný	k2eAgFnPc1d1	izolovaná
arely	arela	k1gFnPc1	arela
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
Etiopii	Etiopie	k1gFnSc6	Etiopie
a	a	k8xC	a
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
planě	planě	k6eAd1	planě
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
cizrny	cizrna	k1gFnPc1	cizrna
objevují	objevovat	k5eAaImIp3nP	objevovat
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
až	až	k9	až
po	po	k7c4	po
pětitisícová	pětitisícový	k2eAgNnPc4d1	pětitisícové
pohoří	pohoří	k1gNnPc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
zřídka	zřídka	k6eAd1	zřídka
pěstována	pěstován	k2eAgFnSc1d1	pěstována
cizrna	cizrna	k1gFnSc1	cizrna
beraní	beranit	k5eAaImIp3nS	beranit
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
zplaňuje	zplaňovat	k5eAaImIp3nS	zplaňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
květeně	květena	k1gFnSc6	květena
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
rod	rod	k1gInSc4	rod
zastoupen	zastoupen	k2eAgInSc4d1	zastoupen
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
a	a	k8xC	a
zplaňující	zplaňující	k2eAgFnPc4d1	zplaňující
cizrny	cizrna	k1gFnSc2	cizrna
beraní	beraní	k2eAgFnSc1d1	beraní
se	se	k3xPyFc4	se
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
druhy	druh	k1gInPc1	druh
Cicer	cicero	k1gNnPc2	cicero
incisum	incisum	k1gInSc4	incisum
a	a	k8xC	a
C.	C.	kA	C.
montbretii	montbretie	k1gFnSc4	montbretie
a	a	k8xC	a
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
endemický	endemický	k2eAgInSc1d1	endemický
druh	druh	k1gInSc1	druh
C.	C.	kA	C.
graecum	graecum	k1gInSc1	graecum
<g/>
.	.	kIx.	.
</s>
<s>
Cizrna	cizrna	k1gFnSc1	cizrna
beraní	beraní	k2eAgFnSc1d1	beraní
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
třetí	třetí	k4xOgFnSc1	třetí
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
luštěnina	luštěnina	k1gFnSc1	luštěnina
po	po	k7c6	po
hrachu	hrách	k1gInSc6	hrách
setém	setý	k2eAgInSc6d1	setý
a	a	k8xC	a
fazolu	fazol	k1gInSc6	fazol
obecném	obecný	k2eAgInSc6d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
před	před	k7c7	před
7000	[number]	k4	7000
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jihovýchodního	jihovýchodní	k2eAgNnSc2d1	jihovýchodní
Turecka	Turecko	k1gNnSc2	Turecko
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
divokého	divoký	k2eAgInSc2d1	divoký
druhu	druh	k1gInSc2	druh
Cicer	cicero	k1gNnPc2	cicero
reticulatum	reticulatum	k1gNnSc1	reticulatum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
cizrny	cizrna	k1gFnSc2	cizrna
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
klimaticky	klimaticky	k6eAd1	klimaticky
příhodných	příhodný	k2eAgFnPc6d1	příhodná
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
potraviny	potravina	k1gFnPc4	potravina
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
ke	k	k7c3	k
krmným	krmný	k2eAgInPc3d1	krmný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozřazují	rozřazovat	k5eAaImIp3nP	rozřazovat
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
podrodů	podrod	k1gInPc2	podrod
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
poněkud	poněkud	k6eAd1	poněkud
jiný	jiný	k2eAgInSc1d1	jiný
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
2	[number]	k4	2
podrody	podrod	k1gInPc1	podrod
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nich	on	k3xPp3gMnPc2	on
ještě	ještě	k9	ještě
sekce	sekce	k1gFnSc1	sekce
<g/>
)	)	kIx)	)
Monocicer	Monocicer	k1gInSc1	Monocicer
–	–	k?	–
např.	např.	kA	např.
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
cizrna	cizrna	k1gFnSc1	cizrna
beraní	beraní	k2eAgFnSc1d1	beraní
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
jednoletky	jednoletka	k1gFnPc4	jednoletka
Chamaecicer	Chamaecicra	k1gFnPc2	Chamaecicra
–	–	k?	–
cizrny	cizrna	k1gFnSc2	cizrna
horských	horský	k2eAgFnPc2d1	horská
asijských	asijský	k2eAgFnPc2d1	asijská
oblastí	oblast	k1gFnPc2	oblast
Polycicer	Polycicra	k1gFnPc2	Polycicra
–	–	k?	–
25	[number]	k4	25
vytrvalých	vytrvalý	k2eAgInPc2d1	vytrvalý
druhů	druh	k1gInPc2	druh
cizrn	cizrna	k1gFnPc2	cizrna
Acanthocicer	Acanthocicero	k1gNnPc2	Acanthocicero
–	–	k?	–
vytrvalé	vytrvalý	k2eAgFnSc2d1	vytrvalá
rostliny	rostlina	k1gFnSc2	rostlina
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cizrna	cizrna	k1gFnSc1	cizrna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cizrna	cizrna	k1gFnSc1	cizrna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Cicer	cicero	k1gNnPc2	cicero
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
