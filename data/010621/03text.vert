<p>
<s>
Stracené	Stracený	k2eAgInPc1d1	Stracený
Ráj	ráj	k1gInSc1	ráj
je	být	k5eAaImIp3nS	být
moravská	moravský	k2eAgFnSc1d1	Moravská
kapela	kapela	k1gFnSc1	kapela
pocházející	pocházející	k2eAgFnSc1d1	pocházející
ze	z	k7c2	z
vsi	ves	k1gFnSc2	ves
Mezice	Mezice	k1gFnSc2	Mezice
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
prvkem	prvek	k1gInSc7	prvek
jejich	jejich	k3xOp3gFnSc2	jejich
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaPmNgFnS	napsat
v	v	k7c6	v
hanáckém	hanácký	k2eAgNnSc6d1	Hanácké
nářečí	nářečí	k1gNnSc6	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
žánrů	žánr	k1gInPc2	žánr
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
sama	sám	k3xTgFnSc1	sám
svůj	svůj	k3xOyFgInSc4	svůj
styl	styl	k1gInSc1	styl
nazývá	nazývat	k5eAaImIp3nS	nazývat
jako	jako	k9	jako
hanácké	hanácký	k2eAgNnSc1d1	Hanácké
bigbit	bigbit	k5eAaImF	bigbit
<g/>
.	.	kIx.	.
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Ošťádal	ošťádat	k5eAaImAgMnS	ošťádat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
starostou	starosta	k1gMnSc7	starosta
obce	obec	k1gFnSc2	obec
Náklo	Náklo	k1gNnSc1	Náklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Demo	demo	k2eAgMnSc1d1	demo
CD	CD	kA	CD
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
mozeke	mozeke	k1gFnSc6	mozeke
</s>
</p>
<p>
<s>
Víla	víla	k1gFnSc1	víla
</s>
</p>
<p>
<s>
Vrána	vrána	k1gFnSc1	vrána
</s>
</p>
<p>
<s>
Lojzek	Lojzek	k1gMnSc1	Lojzek
z	z	k7c2	z
Hané	Haná	k1gFnSc2	Haná
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
křováku	křovák	k1gMnSc3	křovák
</s>
</p>
<p>
<s>
Gumové	gumový	k2eAgInPc1d1	gumový
zvon	zvon	k1gInSc1	zvon
</s>
</p>
<p>
<s>
Životu	život	k1gInSc3	život
naslouchej	naslouchat	k5eAaImRp2nS	naslouchat
</s>
</p>
<p>
<s>
Žebrák	žebrák	k1gMnSc1	žebrák
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
je	být	k5eAaImIp3nS	být
smutná	smutný	k2eAgFnSc1d1	smutná
</s>
</p>
<p>
<s>
Letni	letnit	k5eAaImRp2nS	letnit
fotka	fotka	k1gFnSc1	fotka
</s>
</p>
<p>
<s>
Žiznivé	Žiznivý	k2eAgInPc1d1	Žiznivý
lev	lev	k1gInSc1	lev
</s>
</p>
<p>
<s>
===	===	k?	===
Demo	demo	k2eAgMnSc1d1	demo
CD	CD	kA	CD
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
</s>
</p>
<p>
<s>
Poboske	Poboske	k6eAd1	Poboske
</s>
</p>
<p>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
Zkošebna	Zkošebna	k1gFnSc1	Zkošebna
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
doba	doba	k1gFnSc1	doba
</s>
</p>
<p>
<s>
Brouk	brouk	k1gMnSc1	brouk
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
nějak	nějak	k6eAd1	nějak
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnSc2	tau
</s>
</p>
<p>
<s>
Kočka	kočka	k1gFnSc1	kočka
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
</s>
</p>
<p>
<s>
===	===	k?	===
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Trávnikář	Trávnikář	k1gMnSc1	Trávnikář
</s>
</p>
<p>
<s>
Nečoché	Nečochý	k2eAgNnSc1d1	Nečochý
</s>
</p>
<p>
<s>
190	[number]	k4	190
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
16	[number]	k4	16
minot	minota	k1gFnPc2	minota
</s>
</p>
<p>
<s>
Šlemák	Šlemák	k1gMnSc1	Šlemák
</s>
</p>
<p>
<s>
Blbá	blbý	k2eAgFnSc1d1	blbá
holka	holka	k1gFnSc1	holka
</s>
</p>
<p>
<s>
Pomalo	Pomat	k5eAaImAgNnS	Pomat
</s>
</p>
<p>
<s>
Velká	velká	k1gFnSc1	velká
slepiči	slepič	k1gInSc6	slepič
lópež	lópež	k6eAd1	lópež
</s>
</p>
<p>
<s>
Meslevec	Meslevec	k1gMnSc1	Meslevec
</s>
</p>
<p>
<s>
Vašek	Vašek	k1gMnSc1	Vašek
</s>
</p>
<p>
<s>
Větkong	Větkong	k1gMnSc1	Větkong
</s>
</p>
<p>
<s>
Smraďoch	smraďoch	k1gMnSc1	smraďoch
</s>
</p>
<p>
<s>
Holka	holka	k1gFnSc1	holka
holka	holka	k1gFnSc1	holka
holka	holka	k1gFnSc1	holka
</s>
</p>
<p>
<s>
Tráva	tráva	k1gFnSc1	tráva
</s>
</p>
<p>
<s>
===	===	k?	===
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Hanák	Hanák	k1gMnSc1	Hanák
</s>
</p>
<p>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
</s>
</p>
<p>
<s>
Piskáče	Piskáč	k1gInPc1	Piskáč
</s>
</p>
<p>
<s>
Nerve	rvát	k5eNaImIp3nS	rvát
</s>
</p>
<p>
<s>
Maróděni	Maróděn	k2eAgMnPc1d1	Maróděn
</s>
</p>
<p>
<s>
Omiráni	Omirán	k2eAgMnPc1d1	Omirán
</s>
</p>
<p>
<s>
Mázl	máznout	k5eAaPmAgMnS	máznout
</s>
</p>
<p>
<s>
Jorafest	Jorafest	k1gFnSc1	Jorafest
</s>
</p>
<p>
<s>
Kubrt	Kubrt	k1gInSc1	Kubrt
</s>
</p>
<p>
<s>
Kapřék	Kapřék	k6eAd1	Kapřék
</s>
</p>
<p>
<s>
Nevěsta	nevěsta	k1gFnSc1	nevěsta
</s>
</p>
<p>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
</s>
</p>
<p>
<s>
Tepláková	teplákový	k2eAgFnSc1d1	tepláková
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
Los	los	k1gMnSc1	los
putikos	putikos	k1gMnSc1	putikos
</s>
</p>
<p>
<s>
MoravanSkupina	MoravanSkupina	k1gFnSc1	MoravanSkupina
natočila	natočit	k5eAaBmAgFnS	natočit
videoklipy	videoklip	k1gInPc4	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Tráva	tráva	k1gFnSc1	tráva
<g/>
,	,	kIx,	,
Nečoché	Nečochý	k2eAgInPc1d1	Nečochý
a	a	k8xC	a
Piskáče	Piskáč	k1gInPc1	Piskáč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.straceneraj.cz/	[url]	k?	http://www.straceneraj.cz/
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
http://bandzone.cz/straceneraj	[url]	k1gFnSc1	http://bandzone.cz/straceneraj
–	–	k?	–
Profil	profil	k1gInSc4	profil
na	na	k7c4	na
Bandzone	Bandzon	k1gMnSc5	Bandzon
</s>
</p>
<p>
<s>
http://www.straceneraj.estranky.cz/	[url]	k?	http://www.straceneraj.estranky.cz/
–	–	k?	–
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
