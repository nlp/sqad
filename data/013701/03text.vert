<s>
Rehmanie	Rehmanie	k1gFnSc1
</s>
<s>
Rehmanie	Rehmanie	k1gFnSc1
Rehmannia	Rehmannium	k1gNnSc2
piasezkii	piasezkie	k1gFnSc6
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
hluchavkotvaré	hluchavkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Lamiales	Lamiales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
zárazovité	zárazovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Orobanchaceae	Orobanchacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
rehmanie	rehmanie	k1gFnPc1
(	(	kIx(
<g/>
Rehmannia	Rehmannium	k1gNnPc1
<g/>
)	)	kIx)
<g/>
Libosch	Libosch	k1gInSc1
<g/>
.	.	kIx.
ex	ex	k6eAd1
Fisch	Fisch	k1gInSc1
<g/>
.	.	kIx.
&	&	k?
C.A.	C.A.	k1gMnSc1
<g/>
Mey	Mey	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1835	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
Rehmannia	Rehmannium	k1gNnSc2
henryi	henry	k1gFnSc2
</s>
<s>
Rehmanie	Rehmanie	k1gFnSc2
(	(	kIx(
<g/>
Rehmannia	Rehmannium	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rod	rod	k1gInSc4
rostlin	rostlina	k1gFnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
zárazovité	zárazovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
vytrvalé	vytrvalý	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
s	s	k7c7
přízemní	přízemní	k2eAgFnSc7d1
růžicí	růžice	k1gFnSc7
listů	list	k1gInPc2
a	a	k8xC
olistěnou	olistěný	k2eAgFnSc7d1
lodyhou	lodyha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
nápadné	nápadný	k2eAgFnPc1d1
<g/>
,	,	kIx,
purpurové	purpurový	k2eAgFnPc1d1
nebo	nebo	k8xC
žluté	žlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
trubkovité	trubkovitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
tobolka	tobolka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
6	#num#	k4
druhů	druh	k1gInPc2
a	a	k8xC
pochází	pocházet	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgInPc2d1
rodů	rod	k1gInPc2
čeledi	čeleď	k1gFnSc2
zárazovité	zárazovitý	k2eAgFnPc1d1
nejsou	být	k5eNaImIp3nP
rostliny	rostlina	k1gFnPc1
parazitické	parazitický	k2eAgFnPc1d1
ani	ani	k8xC
poloparazitické	poloparazitický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
rehmanie	rehmanie	k1gFnSc1
lepkavá	lepkavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
léčivá	léčivý	k2eAgFnSc1d1
rostlina	rostlina	k1gFnSc1
tradičně	tradičně	k6eAd1
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
v	v	k7c6
čínské	čínský	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgFnPc7d1
účinnými	účinný	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
jsou	být	k5eAaImIp3nP
iridoidní	iridoidní	k2eAgFnSc1d1
glukosidy	glukosida	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
jsou	být	k5eAaImIp3nP
rehmanie	rehmanie	k1gFnPc4
pěstovány	pěstován	k2eAgFnPc4d1
jako	jako	k8xS,k8xC
zahradní	zahradní	k2eAgFnPc4d1
trvalky	trvalka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Rehmanie	Rehmanie	k1gFnPc1
jsou	být	k5eAaImIp3nP
vytrvalé	vytrvalý	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
s	s	k7c7
podzemními	podzemní	k2eAgInPc7d1
oddenky	oddenek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodyha	lodyha	k1gFnSc1
je	být	k5eAaImIp3nS
vzpřímená	vzpřímený	k2eAgFnSc1d1
<g/>
,	,	kIx,
jednoduchá	jednoduchý	k2eAgFnSc1d1
nebo	nebo	k8xC
od	od	k7c2
báze	báze	k1gFnSc2
větvená	větvený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Listy	list	k1gInPc1
jsou	být	k5eAaImIp3nP
uspořádané	uspořádaný	k2eAgInPc1d1
v	v	k7c6
přízemní	přízemní	k2eAgFnSc6d1
růžici	růžice	k1gFnSc6
<g/>
,	,	kIx,
lodyha	lodyha	k1gFnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
olistěná	olistěný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodyžní	lodyžní	k2eAgInPc1d1
listy	list	k1gInPc1
jsou	být	k5eAaImIp3nP
střídavé	střídavý	k2eAgInPc1d1
<g/>
,	,	kIx,
řapíkaté	řapíkatý	k2eAgInPc1d1
<g/>
,	,	kIx,
na	na	k7c6
okraji	okraj	k1gInSc6
zubaté	zubatý	k2eAgFnSc2d1
nebo	nebo	k8xC
laločnaté	laločnatý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Květy	květ	k1gInPc1
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
<g/>
,	,	kIx,
úžlabní	úžlabní	k2eAgInPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
uspořádané	uspořádaný	k2eAgFnPc4d1
ve	v	k7c6
vrcholových	vrcholový	k2eAgInPc6d1
hroznech	hrozen	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Kalich	kalich	k1gInSc1
je	být	k5eAaImIp3nS
srostlý	srostlý	k2eAgMnSc1d1
<g/>
,	,	kIx,
zakončený	zakončený	k2eAgMnSc1d1
5	#num#	k4
(	(	kIx(
<g/>
až	až	k9
7	#num#	k4
<g/>
)	)	kIx)
laloky	lalok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
purpurově	purpurově	k6eAd1
červená	červený	k2eAgFnSc1d1
nebo	nebo	k8xC
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
trubkovitá	trubkovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
dvoupyská	dvoupyský	k2eAgFnSc1d1
<g/>
,	,	kIx,
zakončená	zakončený	k2eAgFnSc1d1
5	#num#	k4
laloky	lalok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1
jsou	být	k5eAaImIp3nP
4	#num#	k4
nebo	nebo	k8xC
výjimečně	výjimečně	k6eAd1
5	#num#	k4
<g/>
,	,	kIx,
nevyčnívající	vyčnívající	k2eNgNnSc1d1
z	z	k7c2
květů	květ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeník	semeník	k1gInSc1
většinou	většinou	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
2	#num#	k4
komůrky	komůrka	k1gFnSc2
(	(	kIx(
<g/>
výjimečně	výjimečně	k6eAd1
jen	jen	k9
1	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
mnoha	mnoho	k4c7
vajíčky	vajíčko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
pouzdrosečná	pouzdrosečný	k2eAgFnSc1d1
tobolka	tobolka	k1gFnSc1
s	s	k7c7
vytrvalým	vytrvalý	k2eAgInSc7d1
kalichem	kalich	k1gInSc7
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgFnSc7d1
mnoho	mnoho	k6eAd1
drobných	drobný	k2eAgNnPc2d1
semen	semeno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Do	do	k7c2
rodu	rod	k1gInSc2
rehmanie	rehmanie	k1gFnSc2
je	být	k5eAaImIp3nS
řazeno	řazen	k2eAgNnSc1d1
6	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgInPc4
endemity	endemit	k1gInPc4
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
areál	areál	k1gInSc1
má	mít	k5eAaImIp3nS
druh	druh	k1gInSc1
Rehmannia	Rehmannium	k1gNnSc2
glutinosa	glutinosa	k1gFnSc1
<g/>
,	,	kIx,
rozšířený	rozšířený	k2eAgInSc1d1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc6d1
a	a	k8xC
severovýchodní	severovýchodní	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdomácněle	zdomácněle	k6eAd1
roste	růst	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Koreji	Korea	k1gFnSc6
a	a	k8xC
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obsahové	obsahový	k2eAgFnPc1d1
látky	látka	k1gFnPc1
</s>
<s>
Jediným	jediný	k2eAgInSc7d1
druhem	druh	k1gInSc7
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
fytochemické	fytochemický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
podrobněji	podrobně	k6eAd2
zkoumán	zkoumat	k5eAaImNgInS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rehmanie	rehmanie	k1gFnSc1
lepkavá	lepkavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
asi	asi	k9
70	#num#	k4
zjištěnými	zjištěný	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
figurují	figurovat	k5eAaImIp3nP
zejména	zejména	k9
sacharidy	sacharid	k1gInPc1
a	a	k8xC
iridoidní	iridoidní	k2eAgInPc1d1
glukosidy	glukosid	k1gInPc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
obsaženy	obsáhnout	k5eAaPmNgInP
monoterpeny	monoterpen	k2eAgInPc1d1
<g/>
,	,	kIx,
steroidy	steroid	k1gInPc1
<g/>
,	,	kIx,
flavonoidy	flavonoid	k1gInPc1
<g/>
,	,	kIx,
fenolový	fenolový	k2eAgInSc1d1
glukosid	glukosid	k1gInSc1
ionon	ionon	k1gInSc4
<g/>
,	,	kIx,
aminokyseliny	aminokyselina	k1gFnPc4
aj.	aj.	kA
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
asi	asi	k9
33	#num#	k4
rozličných	rozličný	k2eAgInPc2d1
iridoidních	iridoidní	k2eAgInPc2d1
glukosidů	glukosid	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgFnPc7
jsou	být	k5eAaImIp3nP
hlavními	hlavní	k2eAgFnPc7d1
biologicky	biologicky	k6eAd1
aktivními	aktivní	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
katalpol	katalpola	k1gFnPc2
a	a	k8xC
rehmanniosidy	rehmanniosida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
katalpolu	katalpol	k1gInSc2
je	být	k5eAaImIp3nS
u	u	k7c2
různých	různý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
daného	daný	k2eAgInSc2d1
druhu	druh	k1gInSc2
různý	různý	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
pohybovat	pohybovat	k5eAaImF
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
0,014	0,014	k4
až	až	k9
0,6	0,6	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
Rehmannia	Rehmannium	k1gNnSc2
je	být	k5eAaImIp3nS
řazen	řadit	k5eAaImNgInS
spolu	spolu	k6eAd1
s	s	k7c7
rovněž	rovněž	k6eAd1
čínským	čínský	k2eAgInSc7d1
rodem	rod	k1gInSc7
Triaenophora	Triaenophor	k1gMnSc2
do	do	k7c2
tribu	trib	k1gInSc2
Rehmannieae	Rehmanniea	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgMnS
součástí	součást	k1gFnPc2
čeledi	čeleď	k1gFnSc2
Scrophulariaceae	Scrophulariacea	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
byla	být	k5eAaImAgFnS
řada	řada	k1gFnSc1
převážně	převážně	k6eAd1
poloparazitických	poloparazitický	k2eAgInPc2d1
rodů	rod	k1gInPc2
přeřazena	přeřazen	k2eAgFnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
výsledků	výsledek	k1gInPc2
fylogenetických	fylogenetický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
do	do	k7c2
čeledi	čeleď	k1gFnSc2
Orobanchaceae	Orobanchacea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
taxonomové	taxonom	k1gMnPc1
jej	on	k3xPp3gMnSc4
řadili	řadit	k5eAaImAgMnP
do	do	k7c2
čeledi	čeleď	k1gFnSc2
Gesneriaceae	Gesneriacea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
molekulárních	molekulární	k2eAgFnPc2d1
studií	studie	k1gFnPc2
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
tribus	tribus	k1gInSc1
Rehmannieae	Rehmannieae	k1gNnSc2
představuje	představovat	k5eAaImIp3nS
bazální	bazální	k2eAgFnSc1d1
větev	větev	k1gFnSc1
celé	celý	k2eAgFnSc2d1
čeledi	čeleď	k1gFnSc2
Orobanchaceae	Orobanchacea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
čeledi	čeleď	k1gFnSc2
nejsou	být	k5eNaImIp3nP
tyto	tento	k3xDgInPc4
2	#num#	k4
rody	rod	k1gInPc1
poloparazitické	poloparazitický	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
rehmanie	rehmanie	k1gFnSc1
lepkavá	lepkavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rehmannia	Rehmannium	k1gNnSc2
glutinosa	glutinosa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Květenství	květenství	k1gNnSc1
Rehmannia	Rehmannium	k1gNnSc2
glutinosa	glutinosa	k1gFnSc1
</s>
<s>
Rehmanie	Rehmanie	k1gFnSc1
lepkavá	lepkavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
v	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
čínské	čínský	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
zejména	zejména	k9
při	při	k7c6
léčení	léčení	k1gNnSc6
horeček	horečka	k1gFnPc2
a	a	k8xC
krvácení	krvácení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lékařství	lékařství	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
používá	používat	k5eAaImIp3nS
při	při	k7c6
hormonálních	hormonální	k2eAgFnPc6d1
poruchách	porucha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používanou	používaný	k2eAgFnSc7d1
částí	část	k1gFnSc7
je	být	k5eAaImIp3nS
oddenek	oddenek	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Rostlina	rostlina	k1gFnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
léčivka	léčivka	k1gFnSc1
pěstována	pěstován	k2eAgFnSc1d1
nejen	nejen	k6eAd1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
např.	např.	kA
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
<g/>
,	,	kIx,
Japonsku	Japonsko	k1gNnSc6
a	a	k8xC
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
pěstován	pěstovat	k5eAaImNgInS
jako	jako	k8xS,k8xC
okrasná	okrasný	k2eAgFnSc1d1
zahradní	zahradní	k2eAgFnSc1d1
rostlina	rostlina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řidčeji	řídce	k6eAd2
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
druh	druh	k1gInSc1
Rehmannia	Rehmannium	k1gNnSc2
piasezkii	piasezkie	k1gFnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
pod	pod	k7c7
synonymním	synonymní	k2eAgInSc7d1
názvem	název	k1gInSc7
R.	R.	kA
elata	elato	k1gNnSc2
nebo	nebo	k8xC
R.	R.	kA
angulata	angule	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rehmanie	Rehmanie	k1gFnPc1
vyžadují	vyžadovat	k5eAaImIp3nP
plné	plný	k2eAgNnSc4d1
slunce	slunce	k1gNnSc4
a	a	k8xC
dobře	dobře	k6eAd1
propustnou	propustný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množí	množit	k5eAaImIp3nS
se	se	k3xPyFc4
výsevem	výsev	k1gInSc7
semen	semeno	k1gNnPc2
nebo	nebo	k8xC
zimními	zimní	k2eAgInPc7d1
kořenovými	kořenový	k2eAgInPc7d1
řízky	řízek	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
HONG	HONG	kA
<g/>
,	,	kIx,
Deyuan	Deyuan	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flora	Flora	k1gFnSc1
of	of	k?
China	China	k1gFnSc1
<g/>
:	:	kIx,
Rehmannia	Rehmannium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Plants	Plants	k1gInSc1
of	of	k?
the	the	k?
world	world	k6eAd1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Botanic	Botanice	k1gFnPc2
Gardens	Gardensa	k1gFnPc2
<g/>
,	,	kIx,
Kew	Kew	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BIREN	BIREN	kA
,,	,,	k?
N.	N.	kA
Shah	Shah	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rehmannia	Rehmannium	k1gNnSc2
glutinosa	glutinosa	k1gFnSc1
–	–	k?
A	a	k9
phytopharmacological	phytopharmacologicat	k5eAaPmAgMnS
review	review	k?
<g/>
..	..	k?
Pharmacologyonline	Pharmacologyonlin	k1gInSc5
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VALÍČEK	VALÍČEK	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
KOKOŠKA	Kokoška	k1gMnSc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
;	;	kIx,
HOLUBOVÁ	Holubová	k1gFnSc1
<g/>
,	,	kIx,
K.	K.	kA
Léčivé	léčivý	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
třetího	třetí	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Start	start	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86231	#num#	k4
<g/>
-	-	kIx~
<g/>
57	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
XIA	XIA	kA
<g/>
,	,	kIx,
Zhi	Zhi	k1gFnSc1
<g/>
;	;	kIx,
WANG	WANG	kA
<g/>
,	,	kIx,
Yin-Zheng	Yin-Zheng	k1gMnSc1
<g/>
;	;	kIx,
SMITH	SMITH	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
F.	F.	kA
Familial	Familial	k1gMnSc1
placement	placement	k1gMnSc1
and	and	k?
relations	relations	k1gInSc1
of	of	k?
Rehmannia	Rehmannium	k1gNnSc2
and	and	k?
Triaenophora	Triaenophora	k1gFnSc1
(	(	kIx(
<g/>
Scrophulariaceae	Scrophulariaceae	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
inferred	inferred	k1gInSc1
from	from	k6eAd1
five	fivat	k5eAaPmIp3nS
gene	gen	k1gInSc5
regions	regions	k1gInSc1
<g/>
..	..	k?
American	American	k1gInSc1
Journal	Journal	k1gFnSc2
of	of	k?
Botany	Botana	k1gFnSc2
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
96	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
KUBITZKI	KUBITZKI	kA
<g/>
,	,	kIx,
K.	K.	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
;	;	kIx,
BAYER	Bayer	k1gMnSc1
<g/>
,	,	kIx,
C.	C.	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
families	families	k1gMnSc1
and	and	k?
genera	genera	k1gFnSc1
of	of	k?
vascular	vascular	k1gInSc1
plants	plants	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Springer	Springer	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
62200	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ALBACH	ALBACH	kA
<g/>
,	,	kIx,
Dirk	Dirk	k1gMnSc1
C.	C.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
systematics	systematics	k1gInSc4
and	and	k?
phytochemistry	phytochemistr	k1gMnPc4
of	of	k?
Rehmannia	Rehmannium	k1gNnPc1
(	(	kIx(
<g/>
Scrophulariaceae	Scrophulariaceae	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biochemical	Biochemical	k1gFnSc1
Systematics	Systematicsa	k1gFnPc2
&	&	k?
Ecology	Ecologa	k1gFnSc2
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BRICKELL	BRICKELL	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
plants	plants	k1gInSc1
&	&	k?
flowers	flowers	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
American	American	k1gInSc1
Horticultural	Horticultural	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7566	#num#	k4
<g/>
-	-	kIx~
<g/>
6857	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rehmanie	rehmanie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Rehmannia	Rehmannium	k1gNnSc2
ve	v	k7c6
Wikidruzích	Wikidruh	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
