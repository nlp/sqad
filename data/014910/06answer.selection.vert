<s>
Do	do	k7c2
koncernu	koncern	k1gInSc2
vchází	vcházet	k5eAaImIp3nP
podniky	podnik	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgInP
na	na	k7c4
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
opravy	oprava	k1gFnPc4
<g/>
,	,	kIx,
modernizaci	modernizace	k1gFnSc4
a	a	k8xC
likvidaci	likvidace	k1gFnSc4
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgFnSc2d1
a	a	k8xC
speciální	speciální	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
či	či	k8xC
munice	munice	k1gFnSc2
a	a	k8xC
také	také	k9
spolupracují	spolupracovat	k5eAaImIp3nP
ve	v	k7c6
vojensko-technické	vojensko-technický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>