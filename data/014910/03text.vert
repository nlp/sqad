<s>
Ukroboronprom	Ukroboronprom	k1gInSc1
</s>
<s>
Ukroboronprom	Ukroboronprom	k1gInSc1
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Státní	státní	k2eAgInSc4d1
podnik	podnik	k1gInSc4
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2010	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
Pavlo	Pavla	k1gFnSc5
Bukin	Bukin	k2eAgMnSc1d1
(	(	kIx(
<g/>
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
)	)	kIx)
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Zbrojní	zbrojní	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc1
</s>
<s>
▲	▲	k?
28,3	28,3	k4
miliardy	miliarda	k4xCgFnSc2
Hřiven	hřivna	k1gFnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
▲	▲	k?
80	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Ivčenko-ProgressAntonovSpetstechnoexport	Ivčenko-ProgressAntonovSpetstechnoexport	k1gInSc4
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInPc4d1
web	web	k1gInSc4
</s>
<s>
ukroboronprom	ukroboronprom	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
ua	ua	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgInSc1d1
koncern	koncern	k1gInSc1
Ukroboronprom	Ukroboronprom	k1gInSc1
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
Д	Д	k?
к	к	k?
У	У	k?
<g/>
,	,	kIx,
Deržavnyj	Deržavnyj	k1gInSc1
koncern	koncern	k1gInSc1
Ukroboronprom	Ukroboronprom	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sdružení	sdružení	k1gNnSc1
víceoborových	víceoborův	k2eAgInPc2d1
podniků	podnik	k1gInPc2
v	v	k7c6
různých	různý	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
obranného	obranný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
koncernu	koncern	k1gInSc2
vchází	vcházet	k5eAaImIp3nP
podniky	podnik	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgInP
na	na	k7c4
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
opravy	oprava	k1gFnPc4
<g/>
,	,	kIx,
modernizaci	modernizace	k1gFnSc4
a	a	k8xC
likvidaci	likvidace	k1gFnSc4
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgFnSc2d1
a	a	k8xC
speciální	speciální	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
či	či	k8xC
munice	munice	k1gFnSc2
a	a	k8xC
také	také	k9
spolupracují	spolupracovat	k5eAaImIp3nP
ve	v	k7c6
vojensko-technické	vojensko-technický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc4
koncernu	koncern	k1gInSc2
vytvořil	vytvořit	k5eAaPmAgMnS
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
organizaci	organizace	k1gFnSc4
ve	v	k7c6
zbrojním	zbrojní	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
Ukrajiny	Ukrajina	k1gFnSc2
cyklu	cyklus	k1gInSc2
<g/>
:	:	kIx,
výzkum	výzkum	k1gInSc1
-	-	kIx~
vývoj	vývoj	k1gInSc1
-	-	kIx~
sériová	sériový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
-	-	kIx~
prodej	prodej	k1gInSc1
-	-	kIx~
servisní	servisní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
-	-	kIx~
likvidace	likvidace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
koncern	koncern	k1gInSc1
Ukroboronprom	Ukroboronprom	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
dekretu	dekret	k1gInSc2
prezidenta	prezident	k1gMnSc2
Ukrajiny	Ukrajina	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2013	#num#	k4
bylo	být	k5eAaImAgNnS
stvořeno	stvořit	k5eAaPmNgNnS
pět	pět	k4xCc1
divizí	divize	k1gFnPc2
<g/>
:	:	kIx,
výroba	výroba	k1gFnSc1
a	a	k8xC
servis	servis	k1gInSc1
letecké	letecký	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
;	;	kIx,
výroba	výroba	k1gFnSc1
přesně	přesně	k6eAd1
naváděných	naváděný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
a	a	k8xC
munice	munice	k1gFnSc2
<g/>
;	;	kIx,
výroba	výroba	k1gFnSc1
obrněných	obrněný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
automobilů	automobil	k1gInPc2
a	a	k8xC
speciální	speciální	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
;	;	kIx,
výroba	výroba	k1gFnSc1
lodí	loď	k1gFnPc2
a	a	k8xC
lodního	lodní	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
;	;	kIx,
výroba	výroba	k1gFnSc1
rádio-telekomunikační	rádio-telekomunikační	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
a	a	k8xC
systému	systém	k1gInSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
koncernu	koncern	k1gInSc2
jsou	být	k5eAaImIp3nP
převedeny	převést	k5eAaPmNgInP
podniky	podnik	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
oprávněny	oprávněn	k2eAgInPc1d1
provádět	provádět	k5eAaImF
zahraniční	zahraniční	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
ve	v	k7c6
sféře	sféra	k1gFnSc6
exportu	export	k1gInSc2
a	a	k8xC
importu	import	k1gInSc2
zboží	zboží	k1gNnSc2
a	a	k8xC
materiálů	materiál	k1gInPc2
vojenského	vojenský	k2eAgNnSc2d1
určení	určení	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
pod	pod	k7c4
státní	státní	k2eAgNnSc4d1
tajemství	tajemství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
podniky	podnik	k1gInPc1
prodávají	prodávat	k5eAaImIp3nP
na	na	k7c6
zahraničních	zahraniční	k2eAgInPc6d1
trzích	trh	k1gInPc6
produkci	produkce	k1gFnSc4
zbrojního	zbrojní	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
Ukrajiny	Ukrajina	k1gFnSc2
a	a	k8xC
zajišťují	zajišťovat	k5eAaImIp3nP
její	její	k3xOp3gInSc4
poprodejní	poprodejní	k2eAgInSc4d1
servis	servis	k1gInSc4
<g/>
,	,	kIx,
opravy	oprava	k1gFnPc4
<g/>
,	,	kIx,
modernizaci	modernizace	k1gFnSc4
výzbroje	výzbroj	k1gFnSc2
a	a	k8xC
vojenské	vojenský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
byl	být	k5eAaImAgMnS
Roman	Roman	k1gMnSc1
Romanov	Romanov	k1gInSc4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
Ukroboronpromu	Ukroboronprom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
půlroční	půlroční	k2eAgFnSc2d1
práce	práce	k1gFnSc2
se	se	k3xPyFc4
Ukroboronprom	Ukroboronprom	k1gInSc1
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2014	#num#	k4
po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
období	období	k1gNnSc6
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
Ukroboronprom	Ukroboronprom	k1gInSc1
vykonal	vykonat	k5eAaPmAgInS
100	#num#	k4
<g/>
%	%	kIx~
státních	státní	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhého	druhý	k4xOgNnSc2
pololetí	pololetí	k1gNnSc2
roku	rok	k1gInSc2
2014	#num#	k4
vytvořeno	vytvořit	k5eAaPmNgNnS
2000	#num#	k4
nových	nový	k2eAgNnPc2d1
pracovních	pracovní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
13	#num#	k4
podniků	podnik	k1gInPc2
z	z	k7c2
koncernu	koncern	k1gInSc2
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Řízení	řízení	k1gNnSc1
</s>
<s>
Pro	pro	k7c4
zajištění	zajištění	k1gNnSc4
co	co	k9
nejlepších	dobrý	k2eAgNnPc2d3
rozhodnutí	rozhodnutí	k1gNnPc2
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
zájmů	zájem	k1gInPc2
koncernu	koncern	k1gInSc2
je	být	k5eAaImIp3nS
realizovaná	realizovaný	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
divizního	divizní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
přijata	přijmout	k5eAaPmNgFnS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
moderními	moderní	k2eAgInPc7d1
trendy	trend	k1gInPc7
v	v	k7c6
managementu	management	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
podstatou	podstata	k1gFnSc7
je	být	k5eAaImIp3nS
oddělit	oddělit	k5eAaPmF
strategické	strategický	k2eAgInPc4d1
a	a	k8xC
provozní	provozní	k2eAgInPc4d1
cíle	cíl	k1gInPc4
od	od	k7c2
řešení	řešení	k1gNnSc2
sektorových	sektorový	k2eAgInPc2d1
operačních	operační	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divizní	divizní	k2eAgInSc1d1
princip	princip	k1gInSc1
má	mít	k5eAaImIp3nS
zabezpečit	zabezpečit	k5eAaPmF
provozní	provozní	k2eAgFnSc4d1
a	a	k8xC
obchodní	obchodní	k2eAgFnSc4d1
„	„	k?
<g/>
autonomii	autonomie	k1gFnSc4
<g/>
“	“	k?
odvětví	odvětví	k1gNnSc2
<g/>
,	,	kIx,
převedení	převedení	k1gNnSc1
části	část	k1gFnSc2
správních	správní	k2eAgFnPc2d1
pravomocí	pravomoc	k1gFnPc2
manažerům	manažer	k1gMnPc3
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Koncern	koncern	k1gInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
starosti	starost	k1gFnSc6
funkce	funkce	k1gFnSc2
strategického	strategický	k2eAgNnSc2d1
plánování	plánování	k1gNnSc2
<g/>
,	,	kIx,
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
orgány	orgán	k1gInPc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
koordinaci	koordinace	k1gFnSc3
činnosti	činnost	k1gFnSc2
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přístup	přístup	k1gInSc4
umožňuje	umožňovat	k5eAaImIp3nS
vrchnímu	vrchní	k2eAgNnSc3d1
vedení	vedení	k1gNnSc3
koncernu	koncern	k1gInSc2
soustředit	soustředit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
na	na	k7c4
strategická	strategický	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
,	,	kIx,
dlouhodobém	dlouhodobý	k2eAgInSc6d1
programu	program	k1gInSc6
rozvoje	rozvoj	k1gInSc2
obranného	obranný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Produkce	produkce	k1gFnSc1
</s>
<s>
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
znázorněny	znázorněn	k2eAgInPc4d1
některé	některý	k3yIgInPc4
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
produkuje	produkovat	k5eAaImIp3nS
Ukroboronprom	Ukroboronprom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Obrněný	obrněný	k2eAgInSc1d1
transportér	transportér	k1gInSc1
Dozor-B	Dozor-B	k1gFnSc2
</s>
<s>
Obrněný	obrněný	k2eAgInSc1d1
transportér	transportér	k1gInSc1
BTR-3E1	BTR-3E1	k1gFnSc2
</s>
<s>
Obrněný	obrněný	k2eAgInSc1d1
transportér	transportér	k1gInSc1
BTR-4	BTR-4	k1gFnSc2
</s>
<s>
Tank	tank	k1gInSc1
T-64BM	T-64BM	k1gFnSc2
Bulat	bulat	k5eAaImF
</s>
<s>
Tank	tank	k1gInSc1
T-84BM	T-84BM	k1gFnSc2
Oplot	oplota	k1gFnPc2
</s>
<s>
Antonov	Antonov	k1gInSc1
An-	An-	k1gFnSc2
<g/>
178	#num#	k4
</s>
<s>
Obrněný	obrněný	k2eAgInSc1d1
transportér	transportér	k1gInSc1
Bohdan	Bohdana	k1gFnPc2
Bars-	Bars-	k1gMnPc2
<g/>
8	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
České	český	k2eAgFnPc1d1
zbrojovky	zbrojovka	k1gFnPc1
s	s	k7c7
námi	my	k3xPp1nPc7
chtějí	chtít	k5eAaImIp3nP
obchodovat	obchodovat	k5eAaImF
<g/>
,	,	kIx,
někdo	někdo	k3yInSc1
tomu	ten	k3xDgMnSc3
ale	ale	k9
brání	bránit	k5eAaImIp3nS
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
zbrojař	zbrojař	k1gMnSc1
z	z	k7c2
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
;	;	kIx,
aktualne	aktualnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
09-11-2015	09-11-2015	k4
</s>
<s>
Lehká	lehký	k2eAgNnPc4d1
obrněná	obrněný	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
pro	pro	k7c4
Ukrajinu	Ukrajina	k1gFnSc4
<g/>
;	;	kIx,
armadninoviny	armadninovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
04-02-2016	04-02-2016	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
spustí	spustit	k5eAaPmIp3nS
výrobu	výroba	k1gFnSc4
legendarních	legendarní	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
M-	M-	k1gFnSc4
<g/>
16	#num#	k4
<g/>
;	;	kIx,
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
03-01-2017	03-01-2017	k4
</s>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1
zbrojní	zbrojní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
jede	jet	k5eAaImIp3nS
naplno	naplno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ve	v	k7c6
světové	světový	k2eAgFnSc6d1
desítce	desítka	k1gFnSc6
<g/>
;	;	kIx,
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
08-01-2017	08-01-2017	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ukrajina	Ukrajina	k1gFnSc1
</s>
