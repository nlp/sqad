<s>
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc4d1	americký
řetězec	řetězec	k1gInSc4	řetězec
provozoven	provozovna	k1gFnPc2	provozovna
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Miami-Dade	Miami-Dad	k1gInSc5	Miami-Dad
County	Count	k1gMnPc4	Count
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
zahrnující	zahrnující	k2eAgMnSc1d1	zahrnující
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
restaurací	restaurace	k1gFnPc2	restaurace
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
65	[number]	k4	65
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
provozována	provozován	k2eAgFnSc1d1	provozována
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Hungry	Hungra	k1gMnSc2	Hungra
Jack	Jacka	k1gFnPc2	Jacka
<g/>
'	'	kIx"	'
<g/>
s	s	kA	s
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
restaurace	restaurace	k1gFnPc4	restaurace
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1954	[number]	k4	1954
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
síť	síť	k1gFnSc1	síť
restaurací	restaurace	k1gFnPc2	restaurace
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
nabízí	nabízet	k5eAaImIp3nS	nabízet
především	především	k6eAd1	především
hamburgery	hamburger	k1gInPc4	hamburger
<g/>
,	,	kIx,	,
hranolky	hranolek	k1gInPc4	hranolek
a	a	k8xC	a
chlazené	chlazený	k2eAgInPc4d1	chlazený
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
hamburger	hamburger	k1gInSc1	hamburger
Whopper	Whopper	k1gInSc1	Whopper
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
prodáván	prodávat	k5eAaImNgInS	prodávat
také	také	k9	také
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
variantách	varianta	k1gFnPc6	varianta
od	od	k7c2	od
nejmenšího	malý	k2eAgNnSc2d3	nejmenší
Whopper	Whopper	k1gInSc4	Whopper
Junior	junior	k1gMnSc1	junior
až	až	k9	až
po	po	k7c6	po
Triple	tripl	k1gInSc5	tripl
Whopper	Whopper	k1gMnSc1	Whopper
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
grilovaného	grilovaný	k2eAgNnSc2d1	grilované
mletého	mletý	k2eAgNnSc2d1	mleté
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
v	v	k7c6	v
housce	houska	k1gFnSc6	houska
posypané	posypaný	k2eAgFnSc2d1	posypaná
sezamovým	sezamový	k2eAgNnSc7d1	sezamové
semínkem	semínko	k1gNnSc7	semínko
s	s	k7c7	s
majonézou	majonéza	k1gFnSc7	majonéza
<g/>
,	,	kIx,	,
zeleninou	zelenina	k1gFnSc7	zelenina
a	a	k8xC	a
případně	případně	k6eAd1	případně
dalšími	další	k2eAgFnPc7d1	další
přísadami	přísada	k1gFnPc7	přísada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
restaurace	restaurace	k1gFnPc4	restaurace
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
v	v	k7c6	v
nákupním	nákupní	k2eAgNnSc6d1	nákupní
centru	centrum	k1gNnSc6	centrum
Metropole	metropol	k1gFnSc2	metropol
Zličín	Zličína	k1gFnPc2	Zličína
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
získala	získat	k5eAaPmAgFnS	získat
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
franchisingovou	franchisingový	k2eAgFnSc4d1	franchisingová
licenci	licence	k1gFnSc4	licence
polská	polský	k2eAgFnSc1d1	polská
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
firma	firma	k1gFnSc1	firma
AmRest	AmRest	k1gFnSc1	AmRest
<g/>
,	,	kIx,	,
provozující	provozující	k2eAgFnSc1d1	provozující
též	též	k9	též
restaurace	restaurace	k1gFnSc1	restaurace
KFC	KFC	kA	KFC
<g/>
,	,	kIx,	,
Pizza	pizza	k1gFnSc1	pizza
Hut	Hut	k1gFnSc2	Hut
nebo	nebo	k8xC	nebo
kavárny	kavárna	k1gFnSc2	kavárna
Starbucks	Starbucksa	k1gFnPc2	Starbucksa
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
nově	nově	k6eAd1	nově
také	také	k9	také
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
licencovaných	licencovaný	k2eAgFnPc2d1	licencovaná
značek	značka	k1gFnPc2	značka
provozuje	provozovat	k5eAaImIp3nS	provozovat
restaurace	restaurace	k1gFnPc4	restaurace
pod	pod	k7c7	pod
vlastními	vlastní	k2eAgFnPc7d1	vlastní
značkami	značka	k1gFnPc7	značka
freshpoint	freshpointa	k1gFnPc2	freshpointa
a	a	k8xC	a
Rodeo	rodeo	k1gNnSc4	rodeo
Drive	drive	k1gInSc1	drive
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
akcie	akcie	k1gFnPc1	akcie
jsou	být	k5eAaImIp3nP	být
kotovány	kotovat	k5eAaImNgFnP	kotovat
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zastoupení	zastoupení	k1gNnSc1	zastoupení
pro	pro	k7c4	pro
značku	značka	k1gFnSc4	značka
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
a	a	k8xC	a
licenci	licence	k1gFnSc4	licence
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
splní	splnit	k5eAaPmIp3nS	splnit
podmínky	podmínka	k1gFnPc4	podmínka
centrály	centrála	k1gFnSc2	centrála
Burger	Burgra	k1gFnPc2	Burgra
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zatím	zatím	k6eAd1	zatím
drží	držet	k5eAaImIp3nP	držet
další	další	k2eAgFnPc1d1	další
licence	licence	k1gFnPc1	licence
také	také	k9	také
firma	firma	k1gFnSc1	firma
JLV	JLV	kA	JLV
(	(	kIx(	(
<g/>
Jídelní	jídelní	k2eAgInPc4d1	jídelní
a	a	k8xC	a
lůžkové	lůžkový	k2eAgInPc4d1	lůžkový
vozy	vůz	k1gInPc4	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
její	její	k3xOp3gFnSc7	její
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
firma	firma	k1gFnSc1	firma
BK	BK	kA	BK
Team	team	k1gInSc1	team
(	(	kIx(	(
<g/>
restaurace	restaurace	k1gFnSc1	restaurace
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
,	,	kIx,	,
Václavské	václavský	k2eAgNnSc1d1	Václavské
nám.	nám.	k?	nám.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
-	-	kIx~	-
Chodovská	chodovský	k2eAgFnSc1d1	Chodovská
ul	ul	kA	ul
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
italský	italský	k2eAgMnSc1d1	italský
stravovací	stravovací	k2eAgMnSc1d1	stravovací
gigant	gigant	k1gMnSc1	gigant
Autogrill	Autogrill	k1gMnSc1	Autogrill
(	(	kIx(	(
<g/>
restaurace	restaurace	k1gFnSc1	restaurace
Praha	Praha	k1gFnSc1	Praha
hl.	hl.	k?	hl.
<g/>
n.	n.	k?	n.
a	a	k8xC	a
Šlovice	Šlovice	k1gFnSc2	Šlovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
tyto	tento	k3xDgFnPc1	tento
provozovny	provozovna	k1gFnPc1	provozovna
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
ulice	ulice	k1gFnSc2	ulice
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
OC	OC	kA	OC
Palladium	palladium	k1gNnSc1	palladium
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
-	-	kIx~	-
Chodovská	chodovský	k2eAgFnSc1d1	Chodovská
ulice	ulice	k1gFnSc1	ulice
-	-	kIx~	-
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
-	-	kIx~	-
Metropole	metropole	k1gFnSc1	metropole
Zličín	Zličín	k1gInSc1	Zličín
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
6	[number]	k4	6
-	-	kIx~	-
Letiště	letiště	k1gNnSc1	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havla	k1gFnSc1	Havla
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
-	-	kIx~	-
ÚAN	ÚAN	kA	ÚAN
Florenc	Florenc	k1gFnSc1	Florenc
Praha	Praha	k1gFnSc1	Praha
9	[number]	k4	9
-	-	kIx~	-
D	D	kA	D
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
km	km	kA	km
-	-	kIx~	-
s	s	k7c7	s
"	"	kIx"	"
<g/>
Drive	drive	k1gInSc1	drive
Thru	Thrus	k1gInSc2	Thrus
<g/>
"	"	kIx"	"
Praha	Praha	k1gFnSc1	Praha
9	[number]	k4	9
-	-	kIx~	-
Galerie	galerie	k1gFnSc1	galerie
Harfa	harfa	k1gFnSc1	harfa
Praha	Praha	k1gFnSc1	Praha
9	[number]	k4	9
-	-	kIx~	-
OC	OC	kA	OC
Letňany	Letňan	k1gMnPc4	Letňan
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
OC	OC	kA	OC
Olympia	Olympia	k1gFnSc1	Olympia
Ostrava	Ostrava	k1gFnSc1	Ostrava
-	-	kIx~	-
Forum	forum	k1gNnSc1	forum
Nová	Nová	k1gFnSc1	Nová
Karolina	Karolinum	k1gNnSc2	Karolinum
Kladno	Kladno	k1gNnSc1	Kladno
-	-	kIx~	-
OC	OC	kA	OC
Central	Central	k1gFnSc1	Central
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
OC	OC	kA	OC
Plaza	plaz	k1gMnSc2	plaz
ZAVŘENO	zavřít	k5eAaPmNgNnS	zavřít
<g/>
:	:	kIx,	:
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
OC	OC	kA	OC
Fórum	fórum	k1gNnSc1	fórum
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
U	u	k7c2	u
Nádraží	nádraží	k1gNnSc2	nádraží
1135	[number]	k4	1135
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
-	-	kIx~	-
Centrum	centrum	k1gNnSc1	centrum
Chodov	Chodov	k1gInSc1	Chodov
-	-	kIx~	-
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
Šlovice	Šlovice	k1gFnSc1	Šlovice
-	-	kIx~	-
D5	D5	k1gFnSc1	D5
</s>
