<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
povídky	povídka	k1gFnPc4	povídka
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
Geraltovi	Geralt	k1gMnSc6	Geralt
<g/>
?	?	kIx.	?
</s>
