<s>
Andrzej	Andrzat	k5eAaPmRp2nS	Andrzat
Sapkowski	Sapkowski	k1gNnSc4	Sapkowski
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Lodž	Lodž	k1gFnSc1	Lodž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
autorů	autor	k1gMnPc2	autor
slovanské	slovanský	k2eAgInPc4d1	slovanský
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
populární	populární	k2eAgInSc1d1	populární
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
osmidílná	osmidílný	k2eAgFnSc1d1	osmidílná
sága	sága	k1gFnSc1	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
Geraltovi	Geralt	k1gMnSc6	Geralt
z	z	k7c2	z
Rivie	Rivie	k1gFnSc2	Rivie
a	a	k8xC	a
princezně	princezna	k1gFnSc3	princezna
Ciri	Cir	k1gFnSc3	Cir
<g/>
.	.	kIx.	.
</s>

