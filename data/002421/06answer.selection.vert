<s>
Grafomanie	grafomanie	k1gFnSc1	grafomanie
či	či	k8xC	či
hypergrafie	hypergrafie	k1gFnSc1	hypergrafie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
grafein	grafein	k1gInSc1	grafein
-	-	kIx~	-
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
mania	mania	k1gFnSc1	mania
-	-	kIx~	-
touha	touha	k1gFnSc1	touha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chorobná	chorobný	k2eAgFnSc1d1	chorobná
touha	touha	k1gFnSc1	touha
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
