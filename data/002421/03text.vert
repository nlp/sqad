<s>
Grafomanie	grafomanie	k1gFnSc1	grafomanie
či	či	k8xC	či
hypergrafie	hypergrafie	k1gFnSc1	hypergrafie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
grafein	grafein	k1gInSc1	grafein
-	-	kIx~	-
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
mania	mania	k1gFnSc1	mania
-	-	kIx~	-
touha	touha	k1gFnSc1	touha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chorobná	chorobný	k2eAgFnSc1d1	chorobná
touha	touha	k1gFnSc1	touha
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lékařském	lékařský	k2eAgInSc6d1	lékařský
smyslu	smysl	k1gInSc6	smysl
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
chorobný	chorobný	k2eAgInSc4d1	chorobný
stav	stav	k1gInSc4	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc4d1	typické
psaní	psaní	k1gNnSc4	psaní
nesmyslných	smyslný	k2eNgInPc2d1	nesmyslný
<g/>
,	,	kIx,	,
nesouvislých	souvislý	k2eNgInPc2d1	nesouvislý
shluků	shluk	k1gInPc2	shluk
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
obecnějším	obecní	k2eAgInSc6d2	obecní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
termín	termín	k1gInSc1	termín
označuje	označovat	k5eAaImIp3nS	označovat
chorobné	chorobný	k2eAgNnSc4d1	chorobné
nutkání	nutkání	k1gNnSc4	nutkání
psát	psát	k5eAaImF	psát
texty	text	k1gInPc4	text
literárního	literární	k2eAgInSc2d1	literární
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xC	jako
pejorativní	pejorativní	k2eAgNnSc1d1	pejorativní
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
málo	málo	k4c1	málo
kvalitních	kvalitní	k2eAgNnPc2d1	kvalitní
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
obecnému	obecný	k2eAgNnSc3d1	obecné
chápání	chápání	k1gNnSc3	chápání
je	být	k5eAaImIp3nS	být
bližší	blízký	k2eAgInSc4d2	bližší
odborný	odborný	k2eAgInSc4d1	odborný
termín	termín	k1gInSc4	termín
typomanie	typomanie	k1gFnSc2	typomanie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
chorobnou	chorobný	k2eAgFnSc4d1	chorobná
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
publikaci	publikace	k1gFnSc6	publikace
vlastního	vlastní	k2eAgNnSc2d1	vlastní
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
píší	psát	k5eAaImIp3nP	psát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
píší	psát	k5eAaImIp3nP	psát
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
pouze	pouze	k6eAd1	pouze
shluk	shluk	k1gInSc1	shluk
obecně	obecně	k6eAd1	obecně
uznávaných	uznávaný	k2eAgFnPc2d1	uznávaná
nepravd	nepravda	k1gFnPc2	nepravda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
většinou	většina	k1gFnSc7	většina
ví	vědět	k5eAaImIp3nS	vědět
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ví	vědět	k5eAaImIp3nS	vědět
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
čtenář	čtenář	k1gMnSc1	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
grafomani	grafoman	k1gMnPc1	grafoman
mají	mít	k5eAaImIp3nP	mít
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
ponětí	ponětí	k1gNnSc6	ponětí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nedovedou	dovést	k5eNaPmIp3nP	dovést
řádně	řádně	k6eAd1	řádně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižený	k2eAgMnPc1d1	postižený
používají	používat	k5eAaImIp3nP	používat
slova	slovo	k1gNnPc4	slovo
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
se	se	k3xPyFc4	se
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
a	a	k8xC	a
obrazy	obraz	k1gInPc7	obraz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	on	k3xPp3gMnPc4	on
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nadaní	nadaný	k2eAgMnPc1d1	nadaný
a	a	k8xC	a
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
psát	psát	k5eAaImF	psát
i	i	k9	i
autobiografie	autobiografie	k1gFnSc1	autobiografie
a	a	k8xC	a
lyrická	lyrický	k2eAgFnSc1d1	lyrická
či	či	k8xC	či
epická	epický	k2eAgNnPc1d1	epické
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižený	k2eAgMnPc1d1	postižený
píší	psát	k5eAaImIp3nP	psát
kdekoliv	kdekoliv	k6eAd1	kdekoliv
<g/>
,	,	kIx,	,
po	po	k7c6	po
papírech	papír	k1gInPc6	papír
<g/>
,	,	kIx,	,
po	po	k7c6	po
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
uchýlí	uchýlit	k5eAaPmIp3nP	uchýlit
se	se	k3xPyFc4	se
i	i	k9	i
ke	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
po	po	k7c6	po
pažích	paže	k1gFnPc6	paže
či	či	k8xC	či
po	po	k7c6	po
nohách	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
má	mít	k5eAaImIp3nS	mít
chorobnou	chorobný	k2eAgFnSc4d1	chorobná
touhu	touha	k1gFnSc4	touha
po	po	k7c4	po
vyjádření	vyjádření	k1gNnSc4	vyjádření
svých	svůj	k3xOyFgFnPc2	svůj
fantazií	fantazie	k1gFnPc2	fantazie
pomocí	pomocí	k7c2	pomocí
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
touha	touha	k1gFnSc1	touha
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
touha	touha	k1gFnSc1	touha
po	po	k7c4	po
vyjádření	vyjádření	k1gNnSc4	vyjádření
vlastní	vlastní	k2eAgFnSc2d1	vlastní
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgNnSc1	svůj
dílo	dílo	k1gNnSc1	dílo
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
velmi	velmi	k6eAd1	velmi
považují	považovat	k5eAaImIp3nP	považovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvalita	kvalita	k1gFnSc1	kvalita
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
pochybná	pochybný	k2eAgFnSc1d1	pochybná
<g/>
.	.	kIx.	.
</s>
