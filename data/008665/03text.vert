<p>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
Doktor	doktor	k1gMnSc1	doktor
je	být	k5eAaImIp3nS	být
inkarnací	inkarnace	k1gFnSc7	inkarnace
Doctora	Doctor	k1gMnSc2	Doctor
<g/>
,	,	kIx,	,
protagonisty	protagonista	k1gMnSc2	protagonista
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
serálu	serál	k1gInSc2	serál
od	od	k7c2	od
BBC	BBC	kA	BBC
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvárněn	zvárnit	k5eAaPmNgInS	zvárnit
hercem	herec	k1gMnSc7	herec
Patrickem	Patricko	k1gNnSc7	Patricko
Troughtonem	Troughton	k1gInSc7	Troughton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
je	být	k5eAaImIp3nS	být
Doktor	doktor	k1gMnSc1	doktor
vylíčen	vylíčen	k2eAgMnSc1d1	vylíčen
jako	jako	k8xS	jako
mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
humanoidního	humanoidní	k2eAgInSc2d1	humanoidní
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
cestuje	cestovat	k5eAaImIp3nS	cestovat
prostorem	prostor	k1gInSc7	prostor
a	a	k8xC	a
časem	čas	k1gInSc7	čas
ve	v	k7c4	v
své	své	k1gNnSc4	své
TARDIS	TARDIS	kA	TARDIS
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
anglické	anglický	k2eAgInPc4d1	anglický
"	"	kIx"	"
<g/>
Time	Tim	k1gInPc4	Tim
and	and	k?	and
Relative	Relativ	k1gInSc5	Relativ
Dimensions	Dimensions	k1gInSc4	Dimensions
in	in	k?	in
Space	Spaec	k1gInSc2	Spaec
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
Relativní	relativní	k2eAgFnSc1d1	relativní
dimenze	dimenze	k1gFnSc1	dimenze
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Doktor	doktor	k1gMnSc1	doktor
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
regenerovat	regenerovat	k5eAaBmF	regenerovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
změnit	změnit	k5eAaPmF	změnit
úplně	úplně	k6eAd1	úplně
svojí	svojit	k5eAaImIp3nP	svojit
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Regenerace	regenerace	k1gFnSc1	regenerace
===	===	k?	===
</s>
</p>
<p>
<s>
Regenerace	regenerace	k1gFnPc1	regenerace
do	do	k7c2	do
druhého	druhý	k4xOgMnSc2	druhý
doktora	doktor	k1gMnSc2	doktor
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
jen	jen	k9	jen
jako	jako	k9	jako
"	"	kIx"	"
<g/>
obnova	obnova	k1gFnSc1	obnova
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
seriálu	seriál	k1gInSc2	seriál
velikým	veliký	k2eAgNnSc7d1	veliké
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
něco	něco	k3yInSc1	něco
takového	takový	k3xDgMnSc4	takový
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnPc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
Williamem	William	k1gInSc7	William
Hartnellem	Hartnell	k1gInSc7	Hartnell
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
slábnul	slábnout	k5eAaPmAgMnS	slábnout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
bojoval	bojovat	k5eAaImAgInS	bojovat
s	s	k7c7	s
Kyberlidmi	Kyberlid	k1gFnPc7	Kyberlid
během	během	k7c2	během
epizody	epizoda	k1gFnSc2	epizoda
Desátá	desátá	k1gFnSc1	desátá
planeta	planeta	k1gFnSc1	planeta
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
kvůli	kvůli	k7c3	kvůli
stáří	stáří	k1gNnSc3	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
samo	sám	k3xTgNnSc1	sám
zregenerovalo	zregenerovat	k5eAaPmAgNnS	zregenerovat
do	do	k7c2	do
druhého	druhý	k4xOgMnSc2	druhý
Doktora	doktor	k1gMnSc2	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
druhým	druhý	k4xOgMnSc7	druhý
Doktorem	doktor	k1gMnSc7	doktor
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
nejasný	jasný	k2eNgMnSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
Doktor	doktor	k1gMnSc1	doktor
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc1	předchůdce
v	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgMnSc1d1	jiný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
společníci	společník	k1gMnPc1	společník
Ben	Ben	k1gInSc4	Ben
a	a	k8xC	a
Polly	Polla	k1gFnPc4	Polla
si	se	k3xPyFc3	se
nebyli	být	k5eNaImAgMnP	být
nejprve	nejprve	k6eAd1	nejprve
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
opravdu	opravdu	k6eAd1	opravdu
Doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
a	a	k8xC	a
uznali	uznat	k5eAaPmAgMnP	uznat
to	ten	k3xDgNnSc4	ten
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ho	on	k3xPp3gInSc4	on
Dalek	dalek	k2eAgMnSc1d1	dalek
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Doktorem	doktor	k1gMnSc7	doktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Společníci	společník	k1gMnPc5	společník
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
Highlanders	Highlanders	k1gInSc1	Highlanders
,	,	kIx,	,
se	se	k3xPyFc4	se
Jamie	Jamie	k1gFnPc1	Jamie
McCrimmon	McCrimmona	k1gFnPc2	McCrimmona
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
posádce	posádka	k1gFnSc3	posádka
Tardis	Tardis	k1gFnSc2	Tardis
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
s	s	k7c7	s
Druhým	druhý	k4xOgMnSc7	druhý
Doktorem	doktor	k1gMnSc7	doktor
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
,	,	kIx,	,
<g/>
Ben	Ben	k1gInSc4	Ben
a	a	k8xC	a
Polly	Polla	k1gFnPc4	Polla
opustili	opustit	k5eAaPmAgMnP	opustit
Tardis	Tardis	k1gFnSc7	Tardis
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
když	když	k8xS	když
TARDIS	TARDIS	kA	TARDIS
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c4	na
Gatwick	Gatwick	k1gInSc4	Gatwick
Airport	Airport	k1gInSc4	Airport
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
původně	původně	k6eAd1	původně
odešli	odejít	k5eAaPmAgMnP	odejít
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
Doktorem	doktor	k1gMnSc7	doktor
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zastavili	zastavit	k5eAaPmAgMnP	zastavit
masovoý	masovoý	k?	masovoý
únos	únos	k1gInSc1	únos
turistů	turist	k1gMnPc2	turist
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
měnícími	měnící	k2eAgInPc7d1	měnící
mimozemštany	mimozemštan	k1gInPc7	mimozemštan
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
a	a	k8xC	a
Jamie	Jamie	k1gFnSc1	Jamie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stali	stát	k5eAaPmAgMnP	stát
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
Daleků	Dalek	k1gInPc2	Dalek
získat	získat	k5eAaPmF	získat
oba	dva	k4xCgInPc4	dva
"	"	kIx"	"
<g/>
Lidské	lidský	k2eAgInPc4d1	lidský
a	a	k8xC	a
Dalekovské	Dalekovský	k2eAgInPc4d1	Dalekovský
faktory	faktor	k1gInPc4	faktor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
Doctor	Doctor	k1gInSc1	Doctor
setkává	setkávat	k5eAaImIp3nS	setkávat
ještě	ještě	k9	ještě
s	s	k7c7	s
Vicky	Vick	k1gInPc7	Vick
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
,	,	kIx,	,
a	a	k8xC	a
Zoe	Zoe	k1gFnSc1	Zoe
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Jamiem	Jamius	k1gMnSc7	Jamius
zůstane	zůstat	k5eAaPmIp3nS	zůstat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
regenerace	regenerace	k1gFnSc2	regenerace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
inkarnace	inkarnace	k1gFnSc2	inkarnace
se	se	k3xPyFc4	se
také	také	k9	také
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Brigadýrem	brigadýr	k1gMnSc7	brigadýr
Alistairem	Alistair	k1gMnSc7	Alistair
Gordonem	Gordon	k1gMnSc7	Gordon
Lethbridgem-Stewartem	Lethbridgem-Stewart	k1gMnSc7	Lethbridgem-Stewart
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
jednotku	jednotka	k1gFnSc4	jednotka
Unit	Unita	k1gFnPc2	Unita
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
hledáním	hledání	k1gNnSc7	hledání
a	a	k8xC	a
potlačováním	potlačování	k1gNnSc7	potlačování
mimozemských	mimozemský	k2eAgInPc2d1	mimozemský
jevů	jev	k1gInPc2	jev
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
i	i	k9	i
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Brigadýr	brigadýr	k1gMnSc1	brigadýr
Lethbridge-Stewart	Lethbridge-Stewart	k1gInSc1	Lethbridge-Stewart
Doktora	doktor	k1gMnSc2	doktor
provází	provázet	k5eAaImIp3nS	provázet
během	během	k7c2	během
klasických	klasický	k2eAgFnPc2d1	klasická
sérií	série	k1gFnPc2	série
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
Planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepřátelé	nepřítel	k1gMnPc5	nepřítel
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
inkarnace	inkarnace	k1gFnSc2	inkarnace
se	se	k3xPyFc4	se
Doktor	doktor	k1gMnSc1	doktor
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
známých	známý	k2eAgMnPc2d1	známý
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Dalekové	Dalekový	k2eAgInPc1d1	Dalekový
a	a	k8xC	a
Kyberlidé	Kyberlidý	k2eAgInPc1d1	Kyberlidý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
potkává	potkávat	k5eAaImIp3nS	potkávat
nové	nový	k2eAgMnPc4d1	nový
nepřátele	nepřítel	k1gMnPc4	nepřítel
jako	jako	k8xS	jako
například	například	k6eAd1	například
Velkou	velký	k2eAgFnSc4d1	velká
Inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
Ledové	ledový	k2eAgMnPc4d1	ledový
Válečníky	válečník	k1gMnPc4	válečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
Doktorova	doktorův	k2eAgInSc2d1	doktorův
konce	konec	k1gInSc2	konec
nadešel	nadejít	k5eAaPmAgMnS	nadejít
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
TARDIS	TARDIS	kA	TARDIS
přistála	přistát	k5eAaImAgFnS	přistát
uprostřed	uprostřed	k7c2	uprostřed
válečné	válečný	k2eAgFnSc2d1	válečná
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
rasou	rasa	k1gFnSc7	rasa
mimozemských	mimozemský	k2eAgMnPc2d1	mimozemský
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dalšího	další	k2eAgMnSc2d1	další
odpadlíka	odpadlík	k1gMnSc2	odpadlík
Pánů	pan	k1gMnPc2	pan
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
Náčelníka	náčelník	k1gMnSc4	náčelník
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
unášeli	unášet	k5eAaImAgMnP	unášet
a	a	k8xC	a
vymývali	vymývat	k5eAaImAgMnP	vymývat
mozky	mozek	k1gInPc4	mozek
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vojáky	voják	k1gMnPc4	voják
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobijí	dobít	k5eAaPmIp3nP	dobít
galaxii	galaxie	k1gFnSc4	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Doktor	doktor	k1gMnSc1	doktor
byl	být	k5eAaImAgMnS	být
schopný	schopný	k2eAgMnSc1d1	schopný
porazit	porazit	k5eAaPmF	porazit
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vrátit	vrátit	k5eAaPmF	vrátit
lidi	člověk	k1gMnPc4	člověk
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
různých	různý	k2eAgInPc2d1	různý
původních	původní	k2eAgInPc2d1	původní
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
Pány	pan	k1gMnPc4	pan
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
obětoval	obětovat	k5eAaBmAgMnS	obětovat
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
domovskou	domovský	k2eAgFnSc4d1	domovská
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
postaven	postavit	k5eAaPmNgInS	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
zákonů	zákon	k1gInPc2	zákon
nevměšování	nevměšování	k1gNnPc2	nevměšování
se	se	k3xPyFc4	se
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tvrzení	tvrzení	k1gNnSc3	tvrzení
Doctora	Doctor	k1gMnSc2	Doctor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Páni	pan	k1gMnPc1	pan
času	čas	k1gInSc2	čas
měli	mít	k5eAaImAgMnP	mít
použít	použít	k5eAaPmF	použít
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
k	k	k7c3	k
pomáhání	pomáhání	k1gNnSc3	pomáhání
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k6eAd1	ještě
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Pánů	pan	k1gMnPc2	pan
času	čas	k1gInSc2	čas
vynucenou	vynucený	k2eAgFnSc4d1	vynucená
regeneraci	regenerace	k1gFnSc4	regenerace
do	do	k7c2	do
Třetího	třetí	k4xOgMnSc2	třetí
Doctora	Doctor	k1gMnSc2	Doctor
<g/>
.	.	kIx.	.
</s>
<s>
Jamie	Jamie	k1gFnSc1	Jamie
a	a	k8xC	a
Zoe	Zoe	k1gFnSc2	Zoe
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
s	s	k7c7	s
vymazanými	vymazaný	k2eAgFnPc7d1	vymazaná
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
na	na	k7c4	na
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jejich	jejich	k3xOp3gNnSc2	jejich
prvního	první	k4xOgNnSc2	první
setkání	setkání	k1gNnPc4	setkání
s	s	k7c7	s
Doctorem	Doctor	k1gInSc7	Doctor
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
TARDIS	TARDIS	kA	TARDIS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Second	Second	k1gMnSc1	Second
Doctor	Doctor	k1gMnSc1	Doctor
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
