<s>
Giganotosaurus	Giganotosaurus	k1gInSc1	Giganotosaurus
carolinii	carolinie	k1gFnSc3	carolinie
(	(	kIx(	(
<g/>
Z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
obří	obří	k2eAgMnSc1d1	obří
jižní	jižní	k2eAgMnSc1d1	jižní
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
masožravých	masožravý	k2eAgMnPc2d1	masožravý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
teropodů	teropod	k1gMnPc2	teropod
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
carcharodontosauridae	carcharodontosauridae	k1gNnSc2	carcharodontosauridae
<g/>
,	,	kIx,	,
známým	známý	k2eAgInSc7d1	známý
podle	podle	k7c2	podle
dobře	dobře	k6eAd1	dobře
zachované	zachovaný	k2eAgFnSc2d1	zachovaná
kostry	kostra	k1gFnSc2	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
obří	obří	k2eAgMnSc1d1	obří
dravec	dravec	k1gMnSc1	dravec
žil	žít	k5eAaImAgMnS	žít
asi	asi	k9	asi
před	před	k7c7	před
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
automechanikem	automechanik	k1gMnSc7	automechanik
a	a	k8xC	a
sběratelem	sběratel	k1gMnSc7	sběratel
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
Rubenem	Ruben	k1gMnSc7	Ruben
Carolinim	Carolinima	k1gFnPc2	Carolinima
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
popsán	popsat	k5eAaPmNgMnS	popsat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
podle	podle	k7c2	podle
kostry	kostra	k1gFnSc2	kostra
kompletní	kompletní	k2eAgInSc1d1	kompletní
asi	asi	k9	asi
ze	z	k7c2	z
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
