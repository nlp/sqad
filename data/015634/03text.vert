<s>
FEMEN	FEMEN	kA
</s>
<s>
FEMEN	FEMEN	kA
logo	logo	k1gNnSc1
Femen	Femen	k1gInSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Hutsol	Hutsol	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
jul	jul	k?
<g/>
.	.	kIx.
/	/	kIx~
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
40	#num#	k4
Lídr	lídr	k1gMnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Hutsol	Hutsola	k1gFnPc2
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
femen	femen	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
FEMEN	FEMEN	kA
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
Ф	Ф	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ukrajinská	ukrajinský	k2eAgFnSc1d1
radikálně	radikálně	k6eAd1
feministická	feministický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
založená	založený	k2eAgFnSc1d1
v	v	k7c6
Kyjevě	Kyjev	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
je	být	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
veřejných	veřejný	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
zpravidla	zpravidla	k6eAd1
účinkuje	účinkovat	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc1
polonahých	polonahý	k2eAgFnPc2d1
nebo	nebo	k8xC
úplně	úplně	k6eAd1
nahých	nahý	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
za	za	k7c7
účelem	účel	k1gInSc7
upoutání	upoutání	k1gNnSc1
pozornosti	pozornost	k1gFnSc2
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
převážně	převážně	k6eAd1
ženské	ženský	k2eAgNnSc4d1
sociální	sociální	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
témata	téma	k1gNnPc4
jako	jako	k8xS,k8xC
sexuální	sexuální	k2eAgFnSc1d1
turistika	turistika	k1gFnSc1
<g/>
,	,	kIx,
sexismus	sexismus	k1gInSc1
<g/>
,	,	kIx,
LGBT	LGBT	kA
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
porušování	porušování	k1gNnSc1
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
další	další	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Hnutím	hnutí	k1gNnSc7
se	se	k3xPyFc4
inspirovaly	inspirovat	k5eAaBmAgFnP
také	také	k9
ženské	ženský	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
je	být	k5eAaImIp3nS
sdružení	sdružení	k1gNnSc4
FEMEN	FEMEN	kA
registrované	registrovaný	k2eAgMnPc4d1
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německá	německý	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
nejčastěji	často	k6eAd3
protestuje	protestovat	k5eAaBmIp3nS
proti	proti	k7c3
sílícímu	sílící	k2eAgInSc3d1
neonacismu	neonacismus	k1gInSc3
<g/>
,	,	kIx,
islámskému	islámský	k2eAgInSc3d1
fundamentalismu	fundamentalismus	k1gInSc3
a	a	k8xC
katolické	katolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgNnSc4d1
zaměření	zaměření	k1gNnSc4
má	mít	k5eAaImIp3nS
i	i	k8xC
FEMEN	FEMEN	kA
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Aktivistky	aktivistka	k1gFnPc1
Femenu	Femen	k1gInSc2
vítají	vítat	k5eAaImIp3nP
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
v	v	k7c6
červenci	červenec	k1gInSc6
2010	#num#	k4
americkou	americký	k2eAgFnSc4d1
ministryni	ministryně	k1gFnSc4
zahraničí	zahraničí	k1gNnSc2
Hillary	Hillara	k1gFnSc2
Clintonovou	Clintonův	k2eAgFnSc7d1
</s>
<s>
Aktivistky	aktivistka	k1gFnPc1
Femenu	Femen	k1gInSc2
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
upozornit	upozornit	k5eAaPmF
na	na	k7c4
sexuální	sexuální	k2eAgFnSc4d1
turistiku	turistika	k1gFnSc4
cizinců	cizinec	k1gMnPc2
po	po	k7c6
zavedení	zavedení	k1gNnSc6
bezvízového	bezvízový	k2eAgInSc2d1
vstupu	vstup	k1gInSc2
na	na	k7c4
Ukrajinu	Ukrajina	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
</s>
<s>
FEMEN	FEMEN	kA
založil	založit	k5eAaPmAgMnS
a	a	k8xC
dlouho	dlouho	k6eAd1
ovládal	ovládat	k5eAaImAgMnS
ukrajinský	ukrajinský	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
Viktor	Viktor	k1gMnSc1
Svjatskij	Svjatskij	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
podle	podle	k7c2
australské	australský	k2eAgFnSc2d1
režisérky	režisérka	k1gFnSc2
Kitty	Kitta	k1gFnSc2
Greenové	Greenové	k2eAgMnPc2d1
choval	chovat	k5eAaImAgMnS
jako	jako	k9
sexistický	sexistický	k2eAgMnSc1d1
despota	despota	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
organizace	organizace	k1gFnSc2
vybíral	vybírat	k5eAaImAgMnS
nejhezčí	hezký	k2eAgFnPc4d3
dívky	dívka	k1gFnPc4
a	a	k8xC
hnutí	hnutí	k1gNnPc4
založil	založit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
mít	mít	k5eAaImF
s	s	k7c7
dívkami	dívka	k1gFnPc7
sex	sex	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aktivity	aktivita	k1gFnPc1
</s>
<s>
Odpor	odpor	k1gInSc1
proti	proti	k7c3
Rusku	Rusko	k1gNnSc3
</s>
<s>
Hnutí	hnutí	k1gNnSc1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
svým	svůj	k3xOyFgInSc7
negativním	negativní	k2eAgInSc7d1
postojem	postoj	k1gInSc7
k	k	k7c3
Rusku	Rusko	k1gNnSc3
a	a	k8xC
také	také	k9
prosazuje	prosazovat	k5eAaImIp3nS
nezávislost	nezávislost	k1gFnSc4
Ukrajinské	ukrajinský	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
na	na	k7c6
Moskevském	moskevský	k2eAgInSc6d1
patriarchátu	patriarchát	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
FEMEN	FEMEN	kA
protestoval	protestovat	k5eAaBmAgInS
proti	proti	k7c3
návštěvě	návštěva	k1gFnSc3
ruského	ruský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Dimitrije	Dimitrije	k1gMnSc2
Medveděva	Medveděv	k1gMnSc2
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
nejvyšší	vysoký	k2eAgMnSc1d3
představitel	představitel	k1gMnSc1
Ruské	ruský	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
patriarcha	patriarcha	k1gMnSc1
Kirill	Kirill	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
ukrajinský	ukrajinský	k2eAgInSc1d1
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
aktivistka	aktivistka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
měla	mít	k5eAaImAgFnS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
těle	tělo	k1gNnSc6
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
„	„	k?
<g/>
Zabijte	zabít	k5eAaPmRp2nP
Kirilla	Kirill	k1gMnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
pařížském	pařížský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
Grévin	Grévina	k1gFnPc2
protestovala	protestovat	k5eAaBmAgFnS
ukrajinská	ukrajinský	k2eAgFnSc1d1
aktivistka	aktivistka	k1gFnSc1
Femenu	Femen	k1gInSc2
s	s	k7c7
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
Zabijte	zabít	k5eAaPmRp2nP
Putina	putin	k2eAgNnPc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aktivity	aktivita	k1gFnPc1
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
aktivistka	aktivistka	k1gFnSc1
Femenu	Femen	k1gInSc2
Alexandra	Alexandra	k1gFnSc1
Ševčenková	Ševčenkový	k2eAgFnSc1d1
vysvlékla	vysvléknout	k5eAaPmAgFnS
a	a	k8xC
demonstrovala	demonstrovat	k5eAaBmAgFnS
na	na	k7c6
Svatopetrském	svatopetrský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
při	při	k7c6
nedělním	nedělní	k2eAgNnSc6d1
kázání	kázání	k1gNnSc6
papeže	papež	k1gMnSc2
Benedikta	Benedikt	k1gMnSc2
XVI	XVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ševčenková	Ševčenkový	k2eAgFnSc1d1
a	a	k8xC
její	její	k3xOp3gInSc1
doprovod	doprovod	k1gInSc1
byli	být	k5eAaImAgMnP
okamžitě	okamžitě	k6eAd1
zadrženi	zadržet	k5eAaPmNgMnP
italskou	italský	k2eAgFnSc7d1
policií	policie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znovu	znovu	k6eAd1
před	před	k7c7
papežem	papež	k1gMnSc7
na	na	k7c6
Svatopetrském	svatopetrský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
ve	v	k7c6
Vatikáně	Vatikán	k1gInSc6
demonstrovaly	demonstrovat	k5eAaBmAgInP
čtyři	čtyři	k4xCgFnPc4
ukrajinské	ukrajinský	k2eAgFnPc4d1
aktivistky	aktivistka	k1gFnPc4
z	z	k7c2
Femenu	Femen	k1gInSc2
v	v	k7c6
lednu	leden	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
těle	tělo	k1gNnSc6
měly	mít	k5eAaImAgFnP
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
„	„	k?
<g/>
In	In	k1gMnSc4
Gay	gay	k1gMnSc1
We	We	k1gFnSc2
Trust	trust	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
protest	protest	k1gInSc1
začal	začít	k5eAaPmAgInS
během	během	k7c2
modlitby	modlitba	k1gFnSc2
papeže	papež	k1gMnSc2
Benedikta	Benedikt	k1gMnSc2
XVI	XVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demonstrantky	demonstrantka	k1gFnSc2
byly	být	k5eAaImAgInP
okamžitě	okamžitě	k6eAd1
zadrženy	zadržet	k5eAaPmNgInP
policií	policie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Polonahé	polonahý	k2eAgFnPc1d1
aktivistky	aktivistka	k1gFnPc1
z	z	k7c2
Femenu	Femen	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
letech	léto	k1gNnPc6
2014	#num#	k4
a	a	k8xC
2017	#num#	k4
opakovaně	opakovaně	k6eAd1
pokoušely	pokoušet	k5eAaImAgFnP
ukrást	ukrást	k5eAaPmF
sošku	soška	k1gFnSc4
Ježíška	ježíšek	k1gInSc2
z	z	k7c2
jesliček	jesličky	k1gFnPc2
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
<g/>
,	,	kIx,
aktivistka	aktivistka	k1gFnSc1
křičela	křičet	k5eAaImAgFnS
„	„	k?
<g/>
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
žena	žena	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc1
FEMEN	FEMEN	kA
apelovalo	apelovat	k5eAaImAgNnS
na	na	k7c4
širší	široký	k2eAgFnSc4d2
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kradla	krást	k5eAaImAgFnS
sošky	soška	k1gFnSc2
Ježíšků	Ježíšek	k1gMnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protest	protest	k1gInSc1
proti	proti	k7c3
prezidentu	prezident	k1gMnSc3
Zemanovi	Zeman	k1gMnSc3
</s>
<s>
V	v	k7c4
pátek	pátek	k1gInSc4
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
během	během	k7c2
volby	volba	k1gFnSc2
prezidenta	prezident	k1gMnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2018	#num#	k4
konfrontovala	konfrontovat	k5eAaBmAgFnS
ve	v	k7c6
volební	volební	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
ukrajinská	ukrajinský	k2eAgFnSc1d1
aktivistka	aktivistka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
prezidenta	prezident	k1gMnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Miloše	Miloš	k1gMnSc2
Zemana	Zeman	k1gMnSc2
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
vykřikovala	vykřikovat	k5eAaImAgFnS
„	„	k?
<g/>
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Putin	putin	k2eAgMnSc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
slut	slut	k2eAgMnSc1d1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Putinova	Putinův	k2eAgFnSc1d1
děvka	děvka	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Ukrajinka	Ukrajinka	k1gFnSc1
s	s	k7c7
angolskými	angolský	k2eAgInPc7d1
kořeny	kořen	k1gInPc7
Angelina	Angelin	k2eAgFnSc1d1
Diashová	Diashová	k1gFnSc1
se	se	k3xPyFc4
skrývala	skrývat	k5eAaImAgFnS
mezi	mezi	k7c7
novináři	novinář	k1gMnPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
minulosti	minulost	k1gFnSc6
například	například	k6eAd1
narušila	narušit	k5eAaPmAgFnS
mezinárodní	mezinárodní	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
v	v	k7c6
Charkově	Charkov	k1gInSc6
<g/>
,	,	kIx,
zúčastnila	zúčastnit	k5eAaPmAgFnS
se	se	k3xPyFc4
protestů	protest	k1gInPc2
Euromajdan	Euromajdan	k1gInSc4
proti	proti	k7c3
Viktoru	Viktor	k1gMnSc3
Janukovyčovi	Janukovyč	k1gMnSc3
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
ji	on	k3xPp3gFnSc4
zadržela	zadržet	k5eAaPmAgFnS
ukrajinská	ukrajinský	k2eAgFnSc1d1
policie	policie	k1gFnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
polonahá	polonahý	k2eAgFnSc1d1
protestovala	protestovat	k5eAaBmAgFnS
proti	proti	k7c3
návštěvě	návštěva	k1gFnSc3
běloruského	běloruský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
Lukašenka	Lukašenka	k1gFnSc1
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
a	a	k8xC
narušila	narušit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnSc4
setkání	setkání	k1gNnSc4
s	s	k7c7
ukrajinským	ukrajinský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Petrem	Petr	k1gMnSc7
Porošenkem	Porošenek	k1gMnSc7
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
jí	on	k3xPp3gFnSc3
hrozí	hrozit	k5eAaImIp3nP
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
za	za	k7c4
chuligánství	chuligánství	k1gNnSc4
dva	dva	k4xCgInPc4
až	až	k9
pět	pět	k4xCc4
let	léto	k1gNnPc2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemanův	Zemanův	k2eAgMnSc1d1
protikandidát	protikandidát	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Drahoš	Drahoš	k1gMnSc1
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
incident	incident	k1gInSc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
obavu	obava	k1gFnSc4
o	o	k7c4
život	život	k1gInSc4
ukrajinské	ukrajinský	k2eAgFnSc2d1
aktivistky	aktivistka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
podle	podle	k7c2
Drahoše	Drahoš	k1gMnSc2
provedla	provést	k5eAaPmAgFnS
„	„	k?
<g/>
velkou	velký	k2eAgFnSc4d1
nerozvážnost	nerozvážnost	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
prezident	prezident	k1gMnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
je	být	k5eAaImIp3nS
chráněnou	chráněný	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
pod	pod	k7c7
dohledem	dohled	k1gInSc7
prezidentské	prezidentský	k2eAgFnSc2d1
ochranky	ochranka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Financování	financování	k1gNnSc1
</s>
<s>
Hnutí	hnutí	k1gNnSc1
FEMEN	FEMEN	kA
financují	financovat	k5eAaBmIp3nP
nebo	nebo	k8xC
financovali	financovat	k5eAaBmAgMnP
například	například	k6eAd1
americký	americký	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
Jed	jed	k1gInSc1
Sunden	Sundno	k1gNnPc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
vlastník	vlastník	k1gMnSc1
deníku	deník	k1gInSc2
Kyiv	Kyiva	k1gFnPc2
Post	post	k1gInSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
ukrajinské	ukrajinský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
KP	KP	kA
Media	medium	k1gNnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
německá	německý	k2eAgFnSc1d1
podnikatelka	podnikatelka	k1gFnSc1
Beate	beat	k1gInSc5
Schober	Schober	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
žije	žít	k5eAaImIp3nS
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
příslušníci	příslušník	k1gMnPc1
početné	početný	k2eAgFnSc2d1
ukrajinské	ukrajinský	k2eAgFnSc2d1
diaspory	diaspora	k1gFnSc2
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
italského	italský	k2eAgInSc2d1
deníku	deník	k1gInSc2
Libero	Libero	k1gNnSc1
pracovnice	pracovnice	k1gFnSc2
ukrajinského	ukrajinský	k2eAgInSc2d1
televizního	televizní	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
1	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pronikla	proniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
do	do	k7c2
organizace	organizace	k1gFnSc2
FEMEN	FEMEN	kA
<g/>
,	,	kIx,
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
ukrajinské	ukrajinský	k2eAgFnPc1d1
členky	členka	k1gFnPc1
Femenu	Femen	k1gInSc2
dostávají	dostávat	k5eAaImIp3nP
od	od	k7c2
organizace	organizace	k1gFnSc2
1000	#num#	k4
eur	euro	k1gNnPc2
měsíčně	měsíčně	k6eAd1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
dívky	dívka	k1gFnPc1
pracující	pracující	k2eAgFnPc4d1
v	v	k7c6
kyjevském	kyjevský	k2eAgNnSc6d1
ústředí	ústředí	k1gNnSc6
Femenu	Femen	k1gInSc2
dostávají	dostávat	k5eAaImIp3nP
2500	#num#	k4
eur	euro	k1gNnPc2
za	za	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
měsíc	měsíc	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
průměrná	průměrný	k2eAgFnSc1d1
měsíční	měsíční	k2eAgFnSc1d1
mzda	mzda	k1gFnSc1
okolo	okolo	k7c2
500	#num#	k4
eur	euro	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Členky	členka	k1gFnPc1
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
ve	v	k7c6
Francii	Francie	k1gFnSc6
dostávají	dostávat	k5eAaImIp3nP
podle	podle	k7c2
deníku	deník	k1gInSc2
Libero	Libero	k1gNnSc1
při	při	k7c6
účasti	účast	k1gFnSc6
na	na	k7c6
protestech	protest	k1gInPc6
dokonce	dokonce	k9
1000	#num#	k4
eur	euro	k1gNnPc2
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1
tuniská	tuniský	k2eAgFnSc1d1
členka	členka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
Amina	Amin	k1gInSc2
Sboui	Sbou	k1gFnSc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Amina	Amina	k1gFnSc1
Tyler	Tyler	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
jako	jako	k9
členka	členka	k1gFnSc1
feministické	feministický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
zatčena	zatčen	k2eAgFnSc1d1
v	v	k7c6
Tunisku	Tunisko	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
protestovala	protestovat	k5eAaBmAgFnS
proti	proti	k7c3
sjezdu	sjezd	k1gInSc2
radikálních	radikální	k2eAgMnPc2d1
islamistických	islamistický	k2eAgMnPc2d1
salafistů	salafista	k1gMnPc2
v	v	k7c6
tuniském	tuniský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Kajruván	Kajruván	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
opustila	opustit	k5eAaPmAgFnS
FEMEN	FEMEN	kA
v	v	k7c6
srpnu	srpen	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc1
z	z	k7c2
důvodů	důvod	k1gInPc2
svého	svůj	k3xOyFgInSc2
odchodu	odchod	k1gInSc2
uvedla	uvést	k5eAaPmAgFnS
vedle	vedle	k7c2
islamofobie	islamofobie	k1gFnSc2
Femenu	Femen	k1gInSc2
také	také	k9
nedostatek	nedostatek	k1gInSc4
transparentnosti	transparentnost	k1gFnSc2
a	a	k8xC
nejasný	jasný	k2eNgInSc1d1
původ	původ	k1gInSc1
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
se	se	k3xPyFc4
FEMEN	FEMEN	kA
financuje	financovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amina	Amina	k1gFnSc1
Sboui	Sbou	k1gFnSc2
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
ptala	ptat	k5eAaImAgFnS
jedné	jeden	k4xCgFnSc2
z	z	k7c2
vůdkyň	vůdkyně	k1gFnPc2
organizace	organizace	k1gFnSc2
FEMEN	FEMEN	kA
Inny	Inna	k1gFnSc2
Ševčenkové	Ševčenkový	k2eAgFnSc2d1
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
peníze	peníz	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
nikdy	nikdy	k6eAd1
nedostala	dostat	k5eNaPmAgFnS
jasnou	jasný	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aktivistky	aktivistka	k1gFnPc1
z	z	k7c2
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
protestující	protestující	k2eAgInPc4d1
před	před	k7c7
ruskou	ruský	k2eAgFnSc7d1
ambasádou	ambasáda	k1gFnSc7
v	v	k7c6
Kyjevě	Kyjev	k1gInSc6
v	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
</s>
<s>
Žena	žena	k1gFnSc1
nahoře	nahoře	k6eAd1
bez	bez	k1gInSc4
z	z	k7c2
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
</s>
<s>
Protest	protest	k1gInSc1
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
ve	v	k7c6
Francii	Francie	k1gFnSc6
za	za	k7c4
sňatky	sňatek	k1gInPc4
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
</s>
<s>
Aktivistky	aktivistka	k1gFnPc1
FEMEN	FEMEN	kA
protestují	protestovat	k5eAaBmIp3nP
v	v	k7c6
únoru	únor	k1gInSc6
2010	#num#	k4
proti	proti	k7c3
zvolení	zvolení	k1gNnSc3
Viktora	Viktor	k1gMnSc2
Janukovyče	Janukovyč	k1gFnSc2
prezidentem	prezident	k1gMnSc7
Ukrajiny	Ukrajina	k1gFnSc2
s	s	k7c7
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
dnes	dnes	k6eAd1
začíná	začínat	k5eAaImIp3nS
válka	válka	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Femen	Femen	k1gInSc4
protestující	protestující	k2eAgInSc4d1
proti	proti	k7c3
EURU	euro	k1gNnSc3
2012	#num#	k4
</s>
<s>
Protest	protest	k1gInSc1
hnutí	hnutí	k1gNnSc2
FEMEN	FEMEN	kA
v	v	k7c6
Paříži	Paříž	k1gFnSc6
ve	v	k7c6
Francii	Francie	k1gFnSc6
</s>
<s>
Aktivistky	aktivistka	k1gFnPc1
Femenu	Femen	k1gInSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
obklopeny	obklopit	k5eAaPmNgFnP
novináři	novinář	k1gMnPc7
a	a	k8xC
místními	místní	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
FEMEN	FEMEN	kA
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Nahotiny	Nahotiny	k?
z	z	k7c2
Femenu	Femen	k1gInSc2
řídil	řídit	k5eAaImAgMnS
despotický	despotický	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Topless	Topless	k1gInSc1
protester	protester	k1gMnSc1
pursues	pursues	k1gMnSc1
Russia	Russium	k1gNnSc2
church	church	k1gMnSc1
leader	leader	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Daily	Daila	k1gFnSc2
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012.1	2012.1	k4
2	#num#	k4
Who	Who	k1gFnPc2
stands	standsa	k1gFnPc2
behind	behind	k1gInSc1
ridiculous	ridiculous	k1gMnSc1
FEMEN	FEMEN	kA
movement	movement	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Hnutí	hnutí	k1gNnSc1
Femen	Femno	k1gNnPc2
se	se	k3xPyFc4
rozpadlo	rozpadnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
aktivistky	aktivistka	k1gFnPc1
ukazují	ukazovat	k5eAaImIp3nP
prsa	prsa	k1gNnPc1
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
pěst	pěst	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Femen	Femen	k1gInSc1
Takes	Takes	k1gMnSc1
Topless	Toplessa	k1gFnPc2
Act	Act	k1gMnSc1
To	to	k9
The	The	k1gMnSc1
Vatican	Vatican	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
Free	Freus	k1gMnSc5
Europe	Europ	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Topless	Topless	k1gInSc1
women	women	k2eAgInSc1d1
protest	protest	k1gInSc1
Vatican	Vaticana	k1gFnPc2
as	as	k1gInSc1
gay	gay	k1gMnSc1
adoption	adoption	k1gInSc1
criticized	criticized	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NY	NY	kA
Daily	Daila	k1gFnSc2
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Polonahá	polonahý	k2eAgFnSc1d1
aktivistka	aktivistka	k1gFnSc1
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
zkusila	zkusit	k5eAaPmAgFnS
sebrat	sebrat	k5eAaPmF
Ježíška	Ježíšek	k1gMnSc4
z	z	k7c2
jesliček	jesličky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
křičela	křičet	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Obnažená	obnažený	k2eAgFnSc1d1
členka	členka	k1gFnSc1
Femen	Femna	k1gFnPc2
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
znesvětila	znesvětit	k5eAaPmAgFnS
sošku	soška	k1gFnSc4
Ježíška	ježíšek	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
:	:	kIx,
Na	na	k7c4
Zemana	Zeman	k1gMnSc4
skočila	skočit	k5eAaPmAgFnS
polonahá	polonahý	k2eAgFnSc1d1
žena	žena	k1gFnSc1
ve	v	k7c6
volební	volební	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
s	s	k7c7
výkřikem	výkřik	k1gInSc7
„	„	k?
<g/>
Zeman	Zeman	k1gMnSc1
je	být	k5eAaImIp3nS
Putinova	Putinův	k2eAgMnSc4d1
děv	děva	k1gFnPc2
<g/>
..	..	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Češi	Čech	k1gMnPc1
vybírají	vybírat	k5eAaImIp3nP
prezidenta	prezident	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
den	dna	k1gFnPc2
volilo	volit	k5eAaImAgNnS
podle	podle	k7c2
odhadů	odhad	k1gInPc2
asi	asi	k9
40	#num#	k4
procent	procento	k1gNnPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
polonahá	polonahý	k2eAgFnSc1d1
aktivistka	aktivistka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zaskočila	zaskočit	k5eAaPmAgFnS
Zemanovu	Zemanův	k2eAgFnSc4d1
ochranku	ochranka	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
nerozvážnost	nerozvážnost	k1gFnSc1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
o	o	k7c6
aktivistce	aktivistka	k1gFnSc6
Drahoš	Drahoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Hilšera	Hilšero	k1gNnSc2
měla	mít	k5eAaImAgFnS
protestovat	protestovat	k5eAaBmF
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Femen	Femen	k2eAgInSc4d1
wants	wants	k1gInSc4
to	ten	k3xDgNnSc1
move	movat	k5eAaPmIp3nS
from	from	k1gInSc1
public	publicum	k1gNnPc2
exposure	exposur	k1gMnSc5
to	ten	k3xDgNnSc1
political	politicat	k5eAaPmAgMnS
power	power	k1gMnSc1
<g/>
,	,	kIx,
Kyiv	Kyiv	k1gMnSc1
Post	posta	k1gFnPc2
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kyiv	Kyiv	k1gInSc1
Post	posta	k1gFnPc2
founder	foundra	k1gFnPc2
reflects	reflects	k6eAd1
on	on	k3xPp3gMnSc1
14	#num#	k4
years	years	k1gInSc4
as	as	k9
newspaper	newspaper	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
owner	owner	k1gInSc4
<g/>
,	,	kIx,
reasons	reasons	k1gInSc4
for	forum	k1gNnPc2
sale	sale	k6eAd1
<g/>
,	,	kIx,
Kyiv	Kyiv	k1gInSc1
Post	posta	k1gFnPc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Die	Die	k1gMnSc5
Ukraine	Ukrain	k1gMnSc5
ist	ist	k?
kein	kein	k1gMnSc1
Bordell	Bordell	k1gMnSc1
<g/>
"	"	kIx"
"	"	kIx"
<g/>
Ukraine	Ukrain	k1gMnSc5
is	is	k?
not	nota	k1gFnPc2
a	a	k8xC
brothel	brothela	k1gFnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Berliner	Berliner	k1gMnSc1
Zeitung	Zeitung	k1gMnSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zychowicz	Zychowicz	k1gMnSc1
<g/>
,	,	kIx,
Jessica	Jessica	k1gMnSc1
(	(	kIx(
<g/>
Fall	Fall	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two	Two	k1gFnSc1
Bad	Bad	k1gFnSc2
Words	Wordsa	k1gFnPc2
<g/>
:	:	kIx,
FEMEN	FEMEN	kA
&	&	k?
Feminism	Feminism	k1gMnSc1
in	in	k?
Independent	independent	k1gMnSc1
Ukraine	Ukrain	k1gInSc5
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Michigan	Michigan	k1gInSc1
<g/>
.1	.1	k4
2	#num#	k4
Mille	Milla	k1gFnSc6
euro	euro	k1gNnSc1
al	ala	k1gFnPc2
giorno	giorno	k1gNnSc1
per	pero	k1gNnPc2
mostrare	mostrar	k1gMnSc5
le	le	k?
tette	tette	k5eAaPmIp2nP
Tutti	tutti	k2eAgMnPc1d1
i	i	k9
segreti	segret	k1gMnPc1
delle	delle	k1gNnSc2
Femen	Femna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libero	Libero	k1gNnSc1
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tunisia	Tunisia	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Amina	Amien	k2eAgFnSc1d1
quits	quits	k1gInSc4
‘	‘	k?
<g/>
Islamophobic	Islamophobice	k1gInPc2
<g/>
’	’	k?
Femen	Femna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
France	Franc	k1gMnSc2
<g/>
24	#num#	k4
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
FEMEN	FEMEN	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
FEMEN	FEMEN	kA
<g/>
.	.	kIx.
<g/>
tv	tv	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1033481572	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2013071506	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
298186288	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2013071506	#num#	k4
</s>
