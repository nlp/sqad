<s>
Petr	Petr	k1gMnSc1	Petr
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
také	také	k9	také
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
jako	jako	k9	jako
obránce	obránce	k1gMnSc1	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Lichtnégl	Lichtnégl	k1gMnSc1	Lichtnégl
<g/>
.	.	kIx.	.
</s>
<s>
Odchovanec	odchovanec	k1gMnSc1	odchovanec
klubu	klub	k1gInSc2	klub
TJ	tj	kA	tj
OP	op	k1gMnSc1	op
Prostějov	Prostějov	k1gInSc4	Prostějov
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
za	za	k7c4	za
Spartak	Spartak	k1gInSc4	Spartak
ZJŠ	ZJŠ	kA	ZJŠ
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
TŽ	TŽ	kA	TŽ
Třinec	Třinec	k1gInSc1	Třinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
ke	k	k7c3	k
131	[number]	k4	131
utkáním	utkání	k1gNnPc3	utkání
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
15	[number]	k4	15
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
za	za	k7c4	za
LIAZ	liaz	k1gInSc4	liaz
Jablonec	Jablonec	k1gInSc1	Jablonec
<g/>
.	.	kIx.	.
</s>
