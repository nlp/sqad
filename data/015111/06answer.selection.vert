<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
délka	délka	k1gFnSc1	délka
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
36	[number]	k4	36
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
severní	severní	k2eAgFnSc2d1	severní
věže	věž	k1gFnSc2	věž
59	[number]	k4	59
m	m	kA	m
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc1d1	hlavní
lodě	loď	k1gFnPc1	loď
24	[number]	k4	24
m	m	kA	m
a	a	k8xC	a
bočných	bočný	k2eAgFnPc2d1	bočná
lodí	loď	k1gFnPc2	loď
12	[number]	k4	12
m.	m.	k?	m.
Stavba	stavba	k1gFnSc1	stavba
měla	mít	k5eAaImAgFnS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
stavitelskou	stavitelský	k2eAgFnSc4d1	stavitelská
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
okolních	okolní	k2eAgNnPc6d1	okolní
městech	město	k1gNnPc6	město
Prešov	Prešov	k1gInSc4	Prešov
<g/>
,	,	kIx,	,
Bardejov	Bardejov	k1gInSc1	Bardejov
<g/>
,	,	kIx,	,
Sabinov	Sabinov	k1gInSc1	Sabinov
<g/>
,	,	kIx,	,
Rožňava	Rožňava	k1gFnSc1	Rožňava
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
výstavbu	výstavba	k1gFnSc4	výstavba
dalších	další	k2eAgInPc2d1	další
chrámů	chrám	k1gInPc2	chrám
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
(	(	kIx(	(
<g/>
Sibiu	Sibium	k1gNnSc6	Sibium
<g/>
,	,	kIx,	,
Brašov	Brašov	k1gInSc4	Brašov
a	a	k8xC	a
Kluž	Kluž	k1gFnSc4	Kluž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
