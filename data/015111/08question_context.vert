<s>
Katedrála	katedrála	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Alžběty	Alžběta	k1gFnSc2
Durynské	durynský	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgInSc4d3
kostel	kostel	k1gInSc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
1200	#num#	k4
m²	m²	k?
a	a	k8xC
kapacitou	kapacita	k1gFnSc7
více	hodně	k6eAd2
než	než	k8xS
5000	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
gotická	gotický	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
<g/>
.	.	kIx.
</s>