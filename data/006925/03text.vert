<s>
Keg	Keg	k?	Keg
je	být	k5eAaImIp3nS	být
vratný	vratný	k2eAgInSc1d1	vratný
sud	sud	k1gInSc1	sud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
speciálně	speciálně	k6eAd1	speciálně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
plnění	plnění	k1gNnSc4	plnění
a	a	k8xC	a
sterilní	sterilní	k2eAgNnSc4d1	sterilní
skladování	skladování	k1gNnSc4	skladování
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Sudy	sud	k1gInPc1	sud
Keg	Keg	k1gMnPc2	Keg
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
široce	široko	k6eAd1	široko
prosadily	prosadit	k5eAaPmAgFnP	prosadit
v	v	k7c6	v
gastronomii	gastronomie	k1gFnSc6	gastronomie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pro	pro	k7c4	pro
domácí	domácí	k2eAgNnSc4d1	domácí
použití	použití	k1gNnSc4	použití
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
výčepní	výčepní	k2eAgInPc1d1	výčepní
přístroje	přístroj	k1gInPc1	přístroj
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
sudy	sud	k1gInPc4	sud
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgFnSc3d1	obvyklá
velikosti	velikost	k1gFnSc3	velikost
Keg	Keg	k1gFnPc2	Keg
sudů	sud	k1gInPc2	sud
jsou	být	k5eAaImIp3nP	být
15	[number]	k4	15
<g/>
,	,	kIx,	,
30	[number]	k4	30
a	a	k8xC	a
50	[number]	k4	50
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Keg	Keg	k?	Keg
sud	sud	k1gInSc1	sud
je	být	k5eAaImIp3nS	být
válcová	válcový	k2eAgFnSc1d1	válcová
nádoba	nádoba	k1gFnSc1	nádoba
z	z	k7c2	z
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
i	i	k8xC	i
dolní	dolní	k2eAgNnSc1d1	dolní
dno	dno	k1gNnSc1	dno
je	být	k5eAaImIp3nS	být
klenuté	klenutý	k2eAgNnSc4d1	klenuté
a	a	k8xC	a
sud	sud	k1gInSc4	sud
je	být	k5eAaImIp3nS	být
dole	dole	k6eAd1	dole
i	i	k9	i
nahoře	nahoře	k6eAd1	nahoře
opatřen	opatřit	k5eAaPmNgInS	opatřit
lemem	lem	k1gInSc7	lem
s	s	k7c7	s
prolisy	prolis	k1gInPc7	prolis
umožňujícím	umožňující	k2eAgMnSc7d1	umožňující
stohování	stohování	k1gNnPc2	stohování
sudů	sud	k1gInPc2	sud
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
sudy	sud	k1gInPc1	sud
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
opláštěné	opláštěný	k2eAgMnPc4d1	opláštěný
izolačním	izolační	k2eAgInSc7d1	izolační
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
delší	dlouhý	k2eAgNnSc4d2	delší
zachování	zachování	k1gNnSc4	zachování
vychlazeného	vychlazený	k2eAgInSc2d1	vychlazený
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
i	i	k9	i
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
k	k	k7c3	k
vychlazení	vychlazení	k1gNnSc3	vychlazení
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
horního	horní	k2eAgNnSc2d1	horní
dna	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
ventil	ventil	k1gInSc1	ventil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
se	se	k3xPyFc4	se
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
výčepní	výčepní	k2eAgFnSc1d1	výčepní
armatura	armatura	k1gFnSc1	armatura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
hnací	hnací	k2eAgInSc4d1	hnací
plyn	plyn	k1gInSc4	plyn
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
nebo	nebo	k8xC	nebo
dusík	dusík	k1gInSc1	dusík
či	či	k8xC	či
směs	směs	k1gFnSc1	směs
obou	dva	k4xCgInPc2	dva
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
z	z	k7c2	z
externího	externí	k2eAgInSc2d1	externí
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgInSc1d1	hnací
plyn	plyn	k1gInSc1	plyn
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
obsah	obsah	k1gInSc4	obsah
sudu	sud	k1gInSc2	sud
do	do	k7c2	do
výčepního	výčepní	k2eAgNnSc2d1	výčepní
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Výčepní	výčepní	k2eAgFnSc1d1	výčepní
armatura	armatura	k1gFnSc1	armatura
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Keg	Keg	k1gFnSc4	Keg
sud	suda	k1gFnPc2	suda
natolik	natolik	k6eAd1	natolik
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
správném	správný	k2eAgInSc6d1	správný
stavu	stav	k1gInSc6	stav
hadic	hadice	k1gFnPc2	hadice
a	a	k8xC	a
výčepní	výčepní	k2eAgFnSc2d1	výčepní
stolice	stolice	k1gFnSc2	stolice
(	(	kIx(	(
<g/>
pípy	pípa	k1gFnSc2	pípa
<g/>
)	)	kIx)	)
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nápoj	nápoj	k1gInSc4	nápoj
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
až	až	k9	až
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
sterilní	sterilní	k2eAgFnSc2d1	sterilní
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgInSc1d1	hnací
plyn	plyn	k1gInSc1	plyn
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
Keg	Keg	k1gFnSc6	Keg
sudu	sud	k1gInSc2	sud
přetlak	přetlak	k1gInSc4	přetlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
při	při	k7c6	při
otevření	otevření	k1gNnSc6	otevření
výčepního	výčepní	k2eAgInSc2d1	výčepní
kohoutu	kohout	k1gInSc2	kohout
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
ze	z	k7c2	z
sudu	sud	k1gInSc2	sud
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
výčepní	výčepní	k2eAgFnSc2d1	výčepní
armatury	armatura	k1gFnSc2	armatura
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
ventil	ventil	k1gInSc1	ventil
sud	suda	k1gFnPc2	suda
vzduchotěsně	vzduchotěsně	k6eAd1	vzduchotěsně
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
skladování	skladování	k1gNnSc1	skladování
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
jeho	jeho	k3xOp3gNnSc4	jeho
zvětrávání	zvětrávání	k1gNnSc4	zvětrávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
používají	používat	k5eAaImIp3nP	používat
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
druhy	druh	k1gInPc1	druh
výčepních	výčepní	k2eAgFnPc2d1	výčepní
armatur	armatura	k1gFnPc2	armatura
<g/>
:	:	kIx,	:
Armatura	armatura	k1gFnSc1	armatura
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
čepovacím	čepovací	k2eAgInSc7d1	čepovací
fitinkem	fitink	k1gInSc7	fitink
se	se	k3xPyFc4	se
na	na	k7c4	na
sud	sud	k1gInSc4	sud
pouze	pouze	k6eAd1	pouze
nasune	nasunout	k5eAaPmIp3nS	nasunout
<g/>
.	.	kIx.	.
</s>
<s>
Armatura	armatura	k1gFnSc1	armatura
s	s	k7c7	s
košíčkovým	košíčkový	k2eAgInSc7d1	košíčkový
fitinkem	fitink	k1gInSc7	fitink
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
při	při	k7c6	při
nasazování	nasazování	k1gNnSc6	nasazování
na	na	k7c4	na
sud	sud	k1gInSc4	sud
otáčet	otáčet	k5eAaImF	otáčet
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
armatur	armatura	k1gFnPc2	armatura
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgNnPc4	dva
trubková	trubkový	k2eAgNnPc4d1	trubkové
vedení	vedení	k1gNnPc4	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
se	se	k3xPyFc4	se
odvádí	odvádět	k5eAaImIp3nS	odvádět
nápoj	nápoj	k1gInSc1	nápoj
při	při	k7c6	při
čepování	čepování	k1gNnSc6	čepování
a	a	k8xC	a
čistící	čistící	k2eAgInSc1d1	čistící
roztok	roztok	k1gInSc1	roztok
při	při	k7c6	při
sanitaci	sanitace	k1gFnSc6	sanitace
u	u	k7c2	u
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
při	při	k7c6	při
čepování	čepování	k1gNnSc6	čepování
přivádí	přivádět	k5eAaImIp3nS	přivádět
hnací	hnací	k2eAgInSc1d1	hnací
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
při	při	k7c6	při
sanitaci	sanitace	k1gFnSc6	sanitace
čistící	čistící	k2eAgInSc4d1	čistící
roztok	roztok	k1gInSc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Mytí	mytí	k1gNnSc1	mytí
a	a	k8xC	a
plnění	plnění	k1gNnSc1	plnění
sudů	sud	k1gInPc2	sud
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
výrobce	výrobce	k1gMnSc2	výrobce
nápoje	nápoj	k1gInPc4	nápoj
automatizovaně	automatizovaně	k6eAd1	automatizovaně
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
přetlakem	přetlak	k1gInSc7	přetlak
vzduchu	vzduch	k1gInSc2	vzduch
odstraní	odstranit	k5eAaPmIp3nP	odstranit
případné	případný	k2eAgInPc1d1	případný
zbytky	zbytek	k1gInPc1	zbytek
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
omytí	omytí	k1gNnSc1	omytí
vnějšího	vnější	k2eAgInSc2d1	vnější
povrchu	povrch	k1gInSc2	povrch
sudu	sud	k1gInSc2	sud
a	a	k8xC	a
napuštění	napuštění	k1gNnSc1	napuštění
horkého	horký	k2eAgInSc2d1	horký
louhu	louh	k1gInSc2	louh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
časové	časový	k2eAgFnSc6d1	časová
prodlevě	prodleva	k1gFnSc6	prodleva
nutné	nutný	k2eAgNnSc1d1	nutné
k	k	k7c3	k
působení	působení	k1gNnSc3	působení
louhu	louh	k1gInSc2	louh
se	se	k3xPyFc4	se
louh	louh	k1gInSc1	louh
vypustí	vypustit	k5eAaPmIp3nS	vypustit
a	a	k8xC	a
sud	sud	k1gInSc1	sud
nasadí	nasadit	k5eAaPmIp3nS	nasadit
ventilem	ventil	k1gInSc7	ventil
dolů	dol	k1gInPc2	dol
na	na	k7c4	na
mycí	mycí	k2eAgFnSc4d1	mycí
hlavici	hlavice	k1gFnSc4	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vystřikuje	vystřikovat	k5eAaImIp3nS	vystřikovat
vnitřek	vnitřek	k1gInSc1	vnitřek
sudu	sud	k1gInSc2	sud
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
mycím	mycí	k2eAgInSc7d1	mycí
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
desinfekcí	desinfekce	k1gFnSc7	desinfekce
<g/>
.	.	kIx.	.
</s>
<s>
Mytí	mytí	k1gNnSc1	mytí
je	být	k5eAaImIp3nS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
propláchnutím	propláchnutí	k1gNnSc7	propláchnutí
čistou	čistá	k1gFnSc4	čistá
pitnou	pitný	k2eAgFnSc4d1	pitná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
vyprázdněním	vyprázdnění	k1gNnSc7	vyprázdnění
přetlakem	přetlak	k1gInSc7	přetlak
sterilního	sterilní	k2eAgInSc2d1	sterilní
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
sud	sud	k1gInSc1	sud
přemístěn	přemístěn	k2eAgInSc1d1	přemístěn
na	na	k7c4	na
plnící	plnící	k2eAgInSc4d1	plnící
ventil	ventil	k1gInSc4	ventil
<g/>
,	,	kIx,	,
dotlakován	dotlakován	k2eAgInSc4d1	dotlakován
na	na	k7c4	na
potřebný	potřebný	k2eAgInSc4d1	potřebný
přetlak	přetlak	k1gInSc4	přetlak
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
naplněn	naplnit	k5eAaPmNgInS	naplnit
nápojem	nápoj	k1gInSc7	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Nápoje	nápoj	k1gInPc1	nápoj
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
do	do	k7c2	do
natlakovaného	natlakovaný	k2eAgInSc2d1	natlakovaný
sudu	sud	k1gInSc2	sud
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
napěnění	napěnění	k1gNnSc1	napěnění
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
naplnění	naplnění	k1gNnSc6	naplnění
se	se	k3xPyFc4	se
na	na	k7c4	na
ventil	ventil	k1gInSc4	ventil
nasadí	nasadit	k5eAaPmIp3nS	nasadit
plastová	plastový	k2eAgFnSc1d1	plastová
krytka	krytka	k1gFnSc1	krytka
s	s	k7c7	s
údaji	údaj	k1gInPc7	údaj
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
naplněnými	naplněný	k2eAgInPc7d1	naplněný
sudy	sud	k1gInPc7	sud
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
manipuluje	manipulovat	k5eAaImIp3nS	manipulovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
paletách	paleta	k1gFnPc6	paleta
pomocí	pomocí	k7c2	pomocí
vysokozdvižných	vysokozdvižný	k2eAgInPc2d1	vysokozdvižný
vozíků	vozík	k1gInPc2	vozík
<g/>
.	.	kIx.	.
</s>
