<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vratný	vratný	k2eAgInSc1d1	vratný
sud	sud	k1gInSc1	sud
<g/>
,	,	kIx,	,
<g/>
speciálně	speciálně	k6eAd1	speciálně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
plnění	plnění	k1gNnSc4	plnění
a	a	k8xC	a
sterilní	sterilní	k2eAgNnSc4d1	sterilní
skladování	skladování	k1gNnSc4	skladování
nápojů	nápoj	k1gInPc2	nápoj
<g/>
?	?	kIx.	?
</s>
