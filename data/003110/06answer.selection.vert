<s>
Nevratné	vratný	k2eNgNnSc1d1	nevratné
vypadávání	vypadávání	k1gNnSc1	vypadávání
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
činnosti	činnost	k1gFnSc2	činnost
mužských	mužský	k2eAgInPc2d1	mužský
hormonů	hormon	k1gInPc2	hormon
testosteronu	testosteron	k1gInSc2	testosteron
a	a	k8xC	a
dihydrotestosteronu	dihydrotestosteron	k1gInSc2	dihydrotestosteron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
důsledkem	důsledek	k1gInSc7	důsledek
léčby	léčba	k1gFnSc2	léčba
nádorového	nádorový	k2eAgNnSc2d1	nádorové
onemocnění	onemocnění	k1gNnSc2	onemocnění
(	(	kIx(	(
<g/>
radioterapie	radioterapie	k1gFnSc1	radioterapie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
