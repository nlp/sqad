<s>
Volejbal	volejbal	k1gInSc1	volejbal
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
volleyball	volleyball	k1gInSc1	volleyball
<g/>
:	:	kIx,	:
volley	vollea	k1gFnPc1	vollea
=	=	kIx~	=
volej	volej	k1gInSc1	volej
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgNnSc1d1	přímé
odehrání	odehrání	k1gNnSc1	odehrání
míče	míč	k1gInSc2	míč
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dotkl	dotknout	k5eAaPmAgInS	dotknout
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ball	ball	k1gInSc4	ball
=	=	kIx~	=
míč	míč	k1gInSc4	míč
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
též	též	k9	též
odbíjená	odbíjený	k2eAgFnSc1d1	odbíjená
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
týmový	týmový	k2eAgInSc1d1	týmový
míčový	míčový	k2eAgInSc1d1	míčový
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
dvě	dva	k4xCgNnPc1	dva
družstva	družstvo	k1gNnPc1	družstvo
(	(	kIx(	(
<g/>
standardně	standardně	k6eAd1	standardně
po	po	k7c6	po
6	[number]	k4	6
hráčích	hráč	k1gMnPc6	hráč
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
také	také	k9	také
přídomek	přídomek	k1gInSc1	přídomek
šestkový	šestkový	k2eAgInSc4d1	šestkový
volejbal	volejbal	k1gInSc4	volejbal
<g/>
)	)	kIx)	)
na	na	k7c6	na
obdélníkovém	obdélníkový	k2eAgNnSc6d1	obdélníkové
hřišti	hřiště	k1gNnSc6	hřiště
rozpůleném	rozpůlený	k2eAgNnSc6d1	rozpůlené
sítí	síť	k1gFnSc7	síť
snaží	snažit	k5eAaImIp3nP	snažit
odehrát	odehrát	k5eAaPmF	odehrát
míč	míč	k1gInSc4	míč
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
polovinu	polovina	k1gFnSc4	polovina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gNnSc4	on
soupeř	soupeř	k1gMnSc1	soupeř
nezpracoval	zpracovat	k5eNaPmAgMnS	zpracovat
a	a	k8xC	a
míč	míč	k1gInSc4	míč
se	se	k3xPyFc4	se
dotkl	dotknout	k5eAaPmAgMnS	dotknout
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
ho	on	k3xPp3gNnSc4	on
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1895	[number]	k4	1895
William	William	k1gInSc1	William
G.	G.	kA	G.
Morgan	morgan	k1gInSc1	morgan
z	z	k7c2	z
YMCA	YMCA	kA	YMCA
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Morganovým	morganův	k2eAgInSc7d1	morganův
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
bezkontaktní	bezkontaktní	k2eAgInSc4d1	bezkontaktní
halový	halový	k2eAgInSc4d1	halový
sport	sport	k1gInSc4	sport
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
rizikem	riziko	k1gNnSc7	riziko
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
ho	on	k3xPp3gMnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Mintonette	Mintonett	k1gInSc5	Mintonett
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
odehrávání	odehrávání	k1gNnSc2	odehrávání
míčů	míč	k1gInPc2	míč
se	s	k7c7	s
sportu	sport	k1gInSc3	sport
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
volejbal	volejbal	k1gInSc4	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
volejbalová	volejbalový	k2eAgFnSc1d1	volejbalová
federace	federace	k1gFnSc1	federace
FIVB	FIVB	kA	FIVB
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
pořádalo	pořádat	k5eAaImAgNnS	pořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
je	být	k5eAaImIp3nS	být
volejbal	volejbal	k1gInSc4	volejbal
součástí	součást	k1gFnPc2	součást
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
přijala	přijmout	k5eAaPmAgFnS	přijmout
FIVB	FIVB	kA	FIVB
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc4	jeho
variantu	varianta	k1gFnSc4	varianta
–	–	k?	–
plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
obdélníkovém	obdélníkový	k2eAgNnSc6d1	obdélníkové
hřišti	hřiště	k1gNnSc6	hřiště
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
18	[number]	k4	18
<g/>
×	×	k?	×
<g/>
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
čtvercové	čtvercový	k2eAgFnPc4d1	čtvercová
poloviny	polovina	k1gFnSc2	polovina
střední	střední	k2eAgFnSc1d1	střední
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yIgFnSc7	který
visí	vise	k1gFnSc7	vise
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
napnuta	napnout	k5eAaPmNgFnS	napnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
horní	horní	k2eAgFnSc1d1	horní
hrana	hrana	k1gFnSc1	hrana
vede	vést	k5eAaImIp3nS	vést
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2,43	[number]	k4	2,43
m	m	kA	m
při	při	k7c6	při
zápasech	zápas	k1gInPc6	zápas
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
2,24	[number]	k4	2,24
m	m	kA	m
při	při	k7c6	při
zápasech	zápas	k1gInPc6	zápas
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
hřiště	hřiště	k1gNnSc2	hřiště
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
3	[number]	k4	3
m	m	kA	m
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
hřiště	hřiště	k1gNnSc2	hřiště
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
7	[number]	k4	7
m	m	kA	m
volného	volný	k2eAgInSc2d1	volný
hracího	hrací	k2eAgInSc2d1	hrací
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
polovině	polovina	k1gFnSc6	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
m	m	kA	m
od	od	k7c2	od
střední	střední	k2eAgFnSc2d1	střední
čáry	čára	k1gFnSc2	čára
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
útočná	útočný	k2eAgFnSc1d1	útočná
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
polovině	polovina	k1gFnSc6	polovina
tzv.	tzv.	kA	tzv.
přední	přední	k2eAgFnSc4d1	přední
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
postranními	postranní	k2eAgFnPc7d1	postranní
čárami	čára	k1gFnPc7	čára
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
svisle	svisle	k6eAd1	svisle
připevněny	připevněn	k2eAgFnPc1d1	připevněna
anténky	anténka	k1gFnPc1	anténka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
povolený	povolený	k2eAgInSc4d1	povolený
prostor	prostor	k1gInSc4	prostor
přeletu	přelet	k1gInSc2	přelet
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
kulatým	kulatý	k2eAgInSc7d1	kulatý
míčem	míč	k1gInSc7	míč
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
či	či	k8xC	či
syntetické	syntetický	k2eAgFnSc2d1	syntetická
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obvod	obvod	k1gInSc1	obvod
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
67	[number]	k4	67
cm	cm	kA	cm
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
průměr	průměr	k1gInSc1	průměr
cca	cca	kA	cca
21	[number]	k4	21
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
260	[number]	k4	260
<g/>
–	–	k?	–
<g/>
280	[number]	k4	280
g.	g.	k?	g.
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
sítě	síť	k1gFnSc2	síť
hraje	hrát	k5eAaImIp3nS	hrát
jedno	jeden	k4xCgNnSc1	jeden
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
šesti	šest	k4xCc2	šest
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozestavení	rozestavení	k1gNnSc6	rozestavení
tři	tři	k4xCgFnPc1	tři
vpředu	vpředu	k6eAd1	vpředu
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
si	se	k3xPyFc3	se
kapitáni	kapitán	k1gMnPc1	kapitán
obou	dva	k4xCgNnPc2	dva
družstev	družstvo	k1gNnPc2	družstvo
vylosují	vylosovat	k5eAaPmIp3nP	vylosovat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
první	první	k4xOgNnSc4	první
podání	podání	k1gNnSc4	podání
či	či	k8xC	či
volbu	volba	k1gFnSc4	volba
strany	strana	k1gFnSc2	strana
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
družstev	družstvo	k1gNnPc2	družstvo
zahájí	zahájit	k5eAaPmIp3nP	zahájit
hru	hra	k1gFnSc4	hra
podáním	podání	k1gNnSc7	podání
(	(	kIx(	(
<g/>
servisem	servis	k1gInSc7	servis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
podávajícího	podávající	k2eAgNnSc2d1	podávající
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
stojí	stát	k5eAaImIp3nS	stát
vpravo	vpravo	k6eAd1	vpravo
vzadu	vzadu	k6eAd1	vzadu
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
za	za	k7c4	za
koncovou	koncový	k2eAgFnSc4d1	koncová
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
nadhodí	nadhodit	k5eAaPmIp3nS	nadhodit
si	se	k3xPyFc3	se
míč	míč	k1gInSc4	míč
a	a	k8xC	a
úderem	úder	k1gInSc7	úder
ruky	ruka	k1gFnSc2	ruka
či	či	k8xC	či
paže	paže	k1gFnSc1	paže
ho	on	k3xPp3gMnSc4	on
pošle	poslat	k5eAaPmIp3nS	poslat
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
stranu	strana	k1gFnSc4	strana
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Protější	protější	k2eAgNnSc1d1	protější
družstvo	družstvo	k1gNnSc1	družstvo
musí	muset	k5eAaImIp3nS	muset
míči	míč	k1gInPc7	míč
zabránit	zabránit	k5eAaPmF	zabránit
v	v	k7c6	v
dotyku	dotyk	k1gInSc6	dotyk
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
odehrát	odehrát	k5eAaPmF	odehrát
zpět	zpět	k6eAd1	zpět
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
smí	smět	k5eAaImIp3nS	smět
míče	míč	k1gInPc4	míč
dotknout	dotknout	k5eAaPmF	dotknout
maximálně	maximálně	k6eAd1	maximálně
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
žádný	žádný	k3yNgMnSc1	žádný
hráč	hráč	k1gMnSc1	hráč
nesmí	smět	k5eNaImIp3nS	smět
míče	míč	k1gInPc4	míč
dotknout	dotknout	k5eAaPmF	dotknout
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
družstvu	družstvo	k1gNnSc6	družstvo
podaří	podařit	k5eAaPmIp3nS	podařit
bez	bez	k7c2	bez
chyby	chyba	k1gFnSc2	chyba
odehrát	odehrát	k5eAaPmF	odehrát
míč	míč	k1gInSc4	míč
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
o	o	k7c4	o
totéž	týž	k3xTgNnSc4	týž
snažit	snažit	k5eAaImF	snažit
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jedna	jeden	k4xCgFnSc1	jeden
rozehra	rozehra	k1gFnSc1	rozehra
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
míč	míč	k1gInSc1	míč
spadne	spadnout	k5eAaPmIp3nS	spadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
nebo	nebo	k8xC	nebo
některé	některý	k3yIgNnSc1	některý
družstvo	družstvo	k1gNnSc1	družstvo
neučiní	učinit	k5eNaPmIp3nS	učinit
jinou	jiný	k2eAgFnSc4d1	jiná
chybu	chyba	k1gFnSc4	chyba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
rozehře	rozehra	k1gFnSc6	rozehra
nechybovalo	chybovat	k5eNaImAgNnS	chybovat
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
skončení	skončení	k1gNnSc6	skončení
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgNnSc7	tento
družstvem	družstvo	k1gNnSc7	družstvo
to	ten	k3xDgNnSc4	ten
družstvo	družstvo	k1gNnSc4	družstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
rozehru	rozehra	k1gFnSc4	rozehra
zahajovalo	zahajovat	k5eAaImAgNnS	zahajovat
podáním	podání	k1gNnSc7	podání
<g/>
,	,	kIx,	,
podává	podávat	k5eAaImIp3nS	podávat
opět	opět	k6eAd1	opět
stejný	stejný	k2eAgMnSc1d1	stejný
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
rozehru	rozehra	k1gFnSc4	rozehra
podávající	podávající	k2eAgNnSc1d1	podávající
družstvo	družstvo	k1gNnSc1	družstvo
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
podávat	podávat	k5eAaImF	podávat
soupeřící	soupeřící	k2eAgNnSc1d1	soupeřící
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
hry	hra	k1gFnSc2	hra
toto	tento	k3xDgNnSc4	tento
družstvo	družstvo	k1gNnSc4	družstvo
postoupí	postoupit	k5eAaPmIp3nP	postoupit
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
:	:	kIx,	:
původně	původně	k6eAd1	původně
podávající	podávající	k2eAgMnSc1d1	podávající
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
zadní	zadní	k2eAgFnSc2d1	zadní
pozice	pozice	k1gFnSc2	pozice
posune	posunout	k5eAaPmIp3nS	posunout
na	na	k7c4	na
zadní	zadní	k2eAgFnPc4d1	zadní
střední	střední	k2eAgFnPc4d1	střední
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
střední	střední	k2eAgFnSc2d1	střední
se	se	k3xPyFc4	se
posune	posunout	k5eAaPmIp3nS	posunout
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
levou	levá	k1gFnSc4	levá
atd.	atd.	kA	atd.
Podávat	podávat	k5eAaImF	podávat
tedy	tedy	k9	tedy
bude	být	k5eAaImBp3nS	být
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
předešlé	předešlý	k2eAgFnSc6d1	předešlá
rozehře	rozehra	k1gFnSc6	rozehra
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
přední	přední	k2eAgMnSc1d1	přední
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
jedno	jeden	k4xCgNnSc1	jeden
družstvo	družstvo	k1gNnSc1	družstvo
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
součtu	součet	k1gInSc2	součet
25	[number]	k4	25
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhé	druhý	k4xOgNnSc1	druhý
družstvo	družstvo	k1gNnSc1	družstvo
má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
nižší	nízký	k2eAgNnSc4d2	nižší
skóre	skóre	k1gNnSc4	skóre
<g/>
,	,	kIx,	,
družstvo	družstvo	k1gNnSc1	družstvo
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
jeden	jeden	k4xCgInSc4	jeden
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pokud	pokud	k8xS	pokud
obě	dva	k4xCgNnPc4	dva
družstva	družstvo	k1gNnPc4	družstvo
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
24	[number]	k4	24
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
družstev	družstvo	k1gNnPc2	družstvo
nezíská	získat	k5eNaPmIp3nS	získat
dvoubodový	dvoubodový	k2eAgInSc4d1	dvoubodový
náskok	náskok	k1gInSc4	náskok
<g/>
,	,	kIx,	,
např.	např.	kA	např.
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
setu	set	k1gInSc6	set
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
podáním	podání	k1gNnSc7	podání
družstvo	družstvo	k1gNnSc4	družstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
setu	set	k1gInSc6	set
nepodávalo	podávat	k5eNaImAgNnS	podávat
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
3	[number]	k4	3
vítězné	vítězný	k2eAgInPc4d1	vítězný
sety	set	k1gInPc4	set
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
setu	set	k1gInSc6	set
stále	stále	k6eAd1	stále
nerozhodnuto	rozhodnut	k2eNgNnSc1d1	nerozhodnuto
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
sety	set	k1gInPc4	set
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
pátý	pátý	k4xOgInSc4	pátý
set	set	k1gInSc4	set
hrán	hrát	k5eAaImNgInS	hrát
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
15	[number]	k4	15
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
minimálně	minimálně	k6eAd1	minimálně
dvoubodový	dvoubodový	k2eAgInSc1d1	dvoubodový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozehra	Rozehra	k1gFnSc1	Rozehra
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mužstev	mužstvo	k1gNnPc2	mužstvo
udělá	udělat	k5eAaPmIp3nS	udělat
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
nepodaří	podařit	k5eNaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tedy	tedy	k9	tedy
míč	míč	k1gInSc1	míč
správně	správně	k6eAd1	správně
předat	předat	k5eAaPmF	předat
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
chybou	chyba	k1gFnSc7	chyba
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zejména	zejména	k9	zejména
následující	následující	k2eAgFnSc1d1	následující
situace	situace	k1gFnSc1	situace
<g/>
:	:	kIx,	:
Míč	míč	k1gInSc1	míč
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
nějakého	nějaký	k3yIgInSc2	nějaký
předmětu	předmět	k1gInSc2	předmět
mimo	mimo	k7c4	mimo
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
osoby	osoba	k1gFnSc2	osoba
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
dotyk	dotyk	k1gInSc1	dotyk
vnější	vnější	k2eAgFnSc2d1	vnější
části	část	k1gFnSc2	část
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
antének	anténka	k1gFnPc2	anténka
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
sloupků	sloupek	k1gInPc2	sloupek
a	a	k8xC	a
provazu	provaz	k1gInSc2	provaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
síť	síť	k1gFnSc1	síť
drží	držet	k5eAaImIp3nS	držet
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Rozehru	Rozehra	k1gFnSc4	Rozehra
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
mužstvo	mužstvo	k1gNnSc1	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
naposledy	naposledy	k6eAd1	naposledy
dotklo	dotknout	k5eAaPmAgNnS	dotknout
míče	míč	k1gInPc4	míč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Míč	míč	k1gInSc1	míč
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Rozehru	Rozehra	k1gFnSc4	Rozehra
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
mužstvo	mužstvo	k1gNnSc1	mužstvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
poloviny	polovina	k1gFnSc2	polovina
se	se	k3xPyFc4	se
dotklo	dotknout	k5eAaPmAgNnS	dotknout
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Míč	míč	k1gInSc1	míč
zcela	zcela	k6eAd1	zcela
podletí	podletět	k5eAaPmIp3nS	podletět
pod	pod	k7c7	pod
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
přeletí	přeletět	k5eAaPmIp3nS	přeletět
síť	síť	k1gFnSc4	síť
mimo	mimo	k7c4	mimo
prostor	prostor	k1gInSc4	prostor
přeletu	přelet	k1gInSc2	přelet
vymezený	vymezený	k2eAgInSc4d1	vymezený
anténkami	anténka	k1gFnPc7	anténka
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
pomyslným	pomyslný	k2eAgNnSc7d1	pomyslné
prodloužením	prodloužení	k1gNnSc7	prodloužení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
chyba	chyba	k1gFnSc1	chyba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
některému	některý	k3yIgMnSc3	některý
hráči	hráč	k1gMnSc3	hráč
podaří	podařit	k5eAaPmIp3nS	podařit
míč	míč	k1gInSc4	míč
správně	správně	k6eAd1	správně
odehrát	odehrát	k5eAaPmF	odehrát
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
mimo	mimo	k7c4	mimo
prostor	prostor	k1gInSc4	prostor
přeletu	přelet	k1gInSc2	přelet
<g/>
)	)	kIx)	)
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
ho	on	k3xPp3gNnSc4	on
správně	správně	k6eAd1	správně
odehrají	odehrát	k5eAaPmIp3nP	odehrát
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
platným	platný	k2eAgInSc7d1	platný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
soupeři	soupeř	k1gMnPc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
mužstva	mužstvo	k1gNnSc2	mužstvo
se	se	k3xPyFc4	se
před	před	k7c7	před
odehráním	odehrání	k1gNnSc7	odehrání
soupeře	soupeř	k1gMnSc2	soupeř
počtvrté	počtvrté	k4xO	počtvrté
dotknou	dotknout	k5eAaPmIp3nP	dotknout
míče	míč	k1gInPc1	míč
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
Jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
míče	míč	k1gInSc2	míč
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
míč	míč	k1gInSc4	míč
dotkne	dotknout	k5eAaPmIp3nS	dotknout
postupně	postupně	k6eAd1	postupně
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
Hráč	hráč	k1gMnSc1	hráč
míč	míč	k1gInSc4	míč
chytí	chytit	k5eAaPmIp3nS	chytit
nebo	nebo	k8xC	nebo
hodí	hodit	k5eAaImIp3nS	hodit
(	(	kIx(	(
<g/>
drží	držet	k5eAaImIp3nS	držet
<g/>
/	/	kIx~	/
<g/>
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
dotkne	dotknout	k5eAaPmIp3nS	dotknout
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
pod	pod	k7c7	pod
sítí	síť	k1gFnSc7	síť
do	do	k7c2	do
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
poloviny	polovina	k1gFnSc2	polovina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
překáží	překážet	k5eAaImIp3nS	překážet
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
soupeři	soupeř	k1gMnPc1	soupeř
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nohou	noha	k1gFnSc7	noha
přešlápne	přešlápnout	k5eAaPmIp3nS	přešlápnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k9	ani
její	její	k3xOp3gFnSc7	její
částí	část	k1gFnSc7	část
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
středové	středový	k2eAgInPc4d1	středový
čáry	čár	k1gInPc4	čár
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
míče	míč	k1gInPc4	míč
nad	nad	k7c7	nad
soupeřovou	soupeřův	k2eAgFnSc7d1	soupeřova
polovinou	polovina	k1gFnSc7	polovina
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
Hráč	hráč	k1gMnSc1	hráč
zadní	zadní	k2eAgFnSc2d1	zadní
řady	řada	k1gFnSc2	řada
zasmečuje	zasmečovat	k5eAaImIp3nS	zasmečovat
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
odehraje	odehrát	k5eAaPmIp3nS	odehrát
míč	míč	k1gInSc4	míč
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
polovinu	polovina	k1gFnSc4	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
dotyku	dotyk	k1gInSc2	dotyk
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
byl	být	k5eAaImAgInS	být
míč	míč	k1gInSc4	míč
nad	nad	k7c7	nad
rovinou	rovina	k1gFnSc7	rovina
horního	horní	k2eAgInSc2d1	horní
okraje	okraj	k1gInSc2	okraj
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
zóny	zóna	k1gFnSc2	zóna
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
povoleno	povolit	k5eAaPmNgNnS	povolit
smečovat	smečovat	k5eAaBmF	smečovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
odrazit	odrazit	k5eAaPmF	odrazit
zpoza	zpoza	k7c2	zpoza
útočné	útočný	k2eAgFnSc2d1	útočná
čáry	čára	k1gFnSc2	čára
<g/>
;	;	kIx,	;
dopadnout	dopadnout	k5eAaPmF	dopadnout
už	už	k6eAd1	už
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
přední	přední	k2eAgFnSc2d1	přední
zóny	zóna	k1gFnSc2	zóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
zadní	zadní	k2eAgFnSc2d1	zadní
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
blok	blok	k1gInSc4	blok
(	(	kIx(	(
<g/>
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
zabránění	zabránění	k1gNnSc4	zabránění
soupeřovi	soupeř	k1gMnSc3	soupeř
dostat	dostat	k5eAaPmF	dostat
míč	míč	k1gInSc4	míč
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraje	hrát	k5eAaImIp3nS	hrát
míč	míč	k1gInSc4	míč
nad	nad	k7c7	nad
rovinou	rovina	k1gFnSc7	rovina
horního	horní	k2eAgInSc2d1	horní
okraje	okraj	k1gInSc2	okraj
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Libero	Libero	k1gNnSc1	Libero
(	(	kIx(	(
<g/>
speciálně	speciálně	k6eAd1	speciálně
určený	určený	k2eAgMnSc1d1	určený
bránící	bránící	k2eAgMnSc1d1	bránící
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
neúčastní	účastnit	k5eNaImIp3nS	účastnit
se	se	k3xPyFc4	se
podání	podání	k1gNnSc1	podání
<g/>
)	)	kIx)	)
zasmečuje	zasmečovat	k5eAaImIp3nS	zasmečovat
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
zasmečuje	zasmečovat	k5eAaImIp3nS	zasmečovat
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
prsty	prst	k1gInPc7	prst
přihrál	přihrát	k5eAaPmAgMnS	přihrát
libero	libero	k1gNnSc1	libero
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Případy	případ	k1gInPc1	případ
označené	označený	k2eAgInPc1d1	označený
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
netýkají	týkat	k5eNaImIp3nP	týkat
bloků	blok	k1gInPc2	blok
<g/>
:	:	kIx,	:
Dotyky	dotyk	k1gInPc7	dotyk
míče	míč	k1gInSc2	míč
při	při	k7c6	při
blokování	blokování	k1gNnSc6	blokování
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
povolených	povolený	k2eAgInPc2d1	povolený
úderů	úder	k1gInPc2	úder
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
dotknout	dotknout	k5eAaPmF	dotknout
míče	míč	k1gInPc4	míč
víckrát	víckrát	k6eAd1	víckrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
pokud	pokud	k6eAd1	pokud
byly	být	k5eAaImAgInP	být
první	první	k4xOgInPc1	první
dotyky	dotyk	k1gInPc1	dotyk
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
blokování	blokování	k1gNnSc2	blokování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
blokování	blokování	k1gNnSc6	blokování
je	být	k5eAaImIp3nS	být
též	též	k9	též
povoleno	povolen	k2eAgNnSc1d1	povoleno
dotknout	dotknout	k5eAaPmF	dotknout
se	se	k3xPyFc4	se
míče	míč	k1gInSc2	míč
nad	nad	k7c7	nad
soupeřovou	soupeřův	k2eAgFnSc7d1	soupeřova
polovinou	polovina	k1gFnSc7	polovina
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
až	až	k9	až
po	po	k7c6	po
soupeřově	soupeřův	k2eAgInSc6d1	soupeřův
útočném	útočný	k2eAgInSc6d1	útočný
úderu	úder	k1gInSc6	úder
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
blokován	blokován	k2eAgInSc1d1	blokován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
míč	míč	k1gInSc1	míč
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
do	do	k7c2	do
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
,	,	kIx,	,
či	či	k8xC	či
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
část	část	k1gFnSc1	část
míče	míč	k1gInSc2	míč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dotkla	dotknout	k5eAaPmAgFnS	dotknout
země	země	k1gFnSc1	země
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
míč	míč	k1gInSc4	míč
alespoň	alespoň	k9	alespoň
svou	svůj	k3xOyFgFnSc7	svůj
částí	část	k1gFnSc7	část
dotkl	dotknout	k5eAaPmAgInS	dotknout
alespoň	alespoň	k9	alespoň
čáry	čára	k1gFnPc1	čára
vymezující	vymezující	k2eAgFnSc4d1	vymezující
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
<g/>
,	,	kIx,	,
že	že	k8xS	že
spadl	spadnout	k5eAaPmAgInS	spadnout
do	do	k7c2	do
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
útok	útok	k1gInSc4	útok
(	(	kIx(	(
<g/>
smečování	smečování	k1gNnSc2	smečování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bloky	blok	k1gInPc1	blok
<g/>
,	,	kIx,	,
na	na	k7c4	na
podání	podání	k1gNnSc4	podání
a	a	k8xC	a
na	na	k7c4	na
obrannou	obranný	k2eAgFnSc4d1	obranná
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
podání	podání	k1gNnSc2	podání
však	však	k9	však
hráči	hráč	k1gMnPc7	hráč
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
rozestavěni	rozestavět	k5eAaPmNgMnP	rozestavět
podle	podle	k7c2	podle
daného	daný	k2eAgNnSc2d1	dané
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
povolují	povolovat	k5eAaImIp3nP	povolovat
využít	využít	k5eAaPmF	využít
jednoho	jeden	k4xCgMnSc4	jeden
specializovaného	specializovaný	k2eAgMnSc4d1	specializovaný
hráče	hráč	k1gMnSc4	hráč
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
libero	libero	k1gNnSc1	libero
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
kterémkoli	kterýkoli	k3yIgNnSc6	kterýkoli
přerušení	přerušení	k1gNnSc6	přerušení
hry	hra	k1gFnSc2	hra
může	moct	k5eAaImIp3nS	moct
vystřídat	vystřídat	k5eAaPmF	vystřídat
libovolného	libovolný	k2eAgMnSc4d1	libovolný
hráče	hráč	k1gMnSc4	hráč
zadní	zadní	k2eAgFnSc2d1	zadní
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
výměna	výměna	k1gFnSc1	výměna
se	se	k3xPyFc4	se
nepočítá	počítat	k5eNaImIp3nS	počítat
do	do	k7c2	do
limitu	limit	k1gInSc2	limit
počtu	počet	k1gInSc2	počet
střídání	střídání	k1gNnSc1	střídání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterého	který	k3yIgNnSc2	který
pravidla	pravidlo	k1gNnSc2	pravidlo
omezují	omezovat	k5eAaImIp3nP	omezovat
na	na	k7c4	na
čistě	čistě	k6eAd1	čistě
obranné	obranný	k2eAgFnPc4d1	obranná
činnosti	činnost	k1gFnPc4	činnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
označený	označený	k2eAgMnSc1d1	označený
jako	jako	k8xS	jako
libero	libero	k1gNnSc1	libero
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
barevně	barevně	k6eAd1	barevně
odlišený	odlišený	k2eAgInSc4d1	odlišený
dres	dres	k1gInSc4	dres
<g/>
.	.	kIx.	.
</s>
<s>
Libero	Libero	k1gNnSc1	Libero
hraje	hrát	k5eAaImIp3nS	hrát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nepodává	podávat	k5eNaImIp3nS	podávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
vrcholovém	vrcholový	k2eAgInSc6d1	vrcholový
volejbale	volejbal	k1gInSc6	volejbal
sestává	sestávat	k5eAaImIp3nS	sestávat
hrající	hrající	k2eAgFnSc1d1	hrající
šestice	šestice	k1gFnSc1	šestice
hráčů	hráč	k1gMnPc2	hráč
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nahrávače	nahrávač	k1gMnSc2	nahrávač
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
diagonálního	diagonální	k2eAgMnSc4d1	diagonální
hráče	hráč	k1gMnSc4	hráč
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
univerzála	univerzála	k1gFnSc1	univerzála
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvou	dva	k4xCgMnPc2	dva
smečařů	smečař	k1gMnPc2	smečař
a	a	k8xC	a
dvou	dva	k4xCgMnPc2	dva
blokařů	blokař	k1gMnPc2	blokař
(	(	kIx(	(
<g/>
plus	plus	k1gInSc1	plus
libero	libero	k1gNnSc1	libero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
volejbalu	volejbal	k1gInSc6	volejbal
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
a	a	k8xC	a
2000	[number]	k4	2000
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
výrazným	výrazný	k2eAgFnPc3d1	výrazná
změnám	změna	k1gFnPc3	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
:	:	kIx,	:
Byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc4d1	zaveden
libero	libero	k1gNnSc4	libero
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
tzv.	tzv.	kA	tzv.
na	na	k7c4	na
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
podávající	podávající	k2eAgNnSc1d1	podávající
mužstvo	mužstvo	k1gNnSc1	mužstvo
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
rozehru	rozehra	k1gFnSc4	rozehra
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
podání	podání	k1gNnSc2	podání
<g/>
,	,	kIx,	,
soupeři	soupeř	k1gMnPc1	soupeř
se	se	k3xPyFc4	se
nepřipočítal	připočítat	k5eNaPmAgInS	připočítat
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
získal	získat	k5eAaPmAgMnS	získat
právo	právo	k1gNnSc4	právo
podávat	podávat	k5eAaImF	podávat
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
15	[number]	k4	15
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
predikovatelnosti	predikovatelnost	k1gFnSc3	predikovatelnost
doby	doba	k1gFnSc2	doba
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
televizní	televizní	k2eAgInPc4d1	televizní
přenosy	přenos	k1gInPc4	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
dotkne	dotknout	k5eAaPmIp3nS	dotknout
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
aktuálních	aktuální	k2eAgNnPc2d1	aktuální
pravidel	pravidlo	k1gNnPc2	pravidlo
platné	platný	k2eAgNnSc1d1	platné
podání	podání	k1gNnSc1	podání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
pravidel	pravidlo	k1gNnPc2	pravidlo
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
chybu	chyba	k1gFnSc4	chyba
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
chybou	chyba	k1gFnSc7	chyba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
dotkl	dotknout	k5eAaPmAgMnS	dotknout
hráče	hráč	k1gMnSc4	hráč
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
pasu	pas	k1gInSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
pravidla	pravidlo	k1gNnPc1	pravidlo
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
hráčům	hráč	k1gMnPc3	hráč
hrát	hrát	k5eAaImF	hrát
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
i	i	k8xC	i
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěžních	soutěžní	k2eAgInPc6d1	soutěžní
volejbalových	volejbalový	k2eAgInPc6d1	volejbalový
zápasech	zápas	k1gInPc6	zápas
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
hráči	hráč	k1gMnPc1	hráč
střídáni	střídat	k5eAaImNgMnP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
setu	set	k1gInSc2	set
smí	smět	k5eAaImIp3nS	smět
jedno	jeden	k4xCgNnSc1	jeden
mužstvo	mužstvo	k1gNnSc1	mužstvo
střídat	střídat	k5eAaImF	střídat
maximálně	maximálně	k6eAd1	maximálně
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tímto	tento	k3xDgNnSc7	tento
střídáním	střídání	k1gNnSc7	střídání
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
měnit	měnit	k5eAaImF	měnit
pořadí	pořadí	k1gNnSc4	pořadí
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
ani	ani	k8xC	ani
pořadí	pořadí	k1gNnSc2	pořadí
na	na	k7c4	na
podání	podání	k1gNnSc4	podání
<g/>
)	)	kIx)	)
–	–	k?	–
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
smí	smět	k5eAaImIp3nS	smět
vrátit	vrátit	k5eAaPmF	vrátit
pouze	pouze	k6eAd1	pouze
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gNnSc4	on
původně	původně	k6eAd1	původně
střídal	střídat	k5eAaImAgMnS	střídat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
Libero	Libero	k1gNnSc1	Libero
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
v	v	k7c6	v
libovolném	libovolný	k2eAgNnSc6d1	libovolné
přerušení	přerušení	k1gNnSc6	přerušení
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
střídání	střídání	k1gNnSc1	střídání
se	se	k3xPyFc4	se
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
střídání	střídání	k1gNnSc2	střídání
nezapočítává	započítávat	k5eNaImIp3nS	započítávat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
rekreačním	rekreační	k2eAgInSc6d1	rekreační
volejbale	volejbal	k1gInSc6	volejbal
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hry	hra	k1gFnPc1	hra
účastní	účastnit	k5eAaImIp3nP	účastnit
vyšší	vysoký	k2eAgInSc4d2	vyšší
počet	počet	k1gInSc4	počet
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
jednodušší	jednoduchý	k2eAgNnPc1d2	jednodušší
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
nejčastějším	častý	k2eAgInSc7d3	nejčastější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
točení	točení	k1gNnSc1	točení
přes	přes	k7c4	přes
střídačku	střídačka	k1gFnSc4	střídačka
<g/>
"	"	kIx"	"
–	–	k?	–
při	při	k7c6	při
získání	získání	k1gNnSc6	získání
podání	podání	k1gNnSc2	podání
nejde	jít	k5eNaImIp3nS	jít
pravý	pravý	k2eAgMnSc1d1	pravý
přední	přední	k2eAgMnSc1d1	přední
hráč	hráč	k1gMnSc1	hráč
podávat	podávat	k5eAaImF	podávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesune	přesunout	k5eAaPmIp3nS	přesunout
se	se	k3xPyFc4	se
na	na	k7c4	na
střídačku	střídačka	k1gFnSc4	střídačka
<g/>
,	,	kIx,	,
a	a	k8xC	a
podávat	podávat	k5eAaImF	podávat
jde	jít	k5eAaImIp3nS	jít
hráč	hráč	k1gMnSc1	hráč
ze	z	k7c2	z
střídačky	střídačka	k1gFnSc2	střídačka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dotyk	dotyk	k1gInSc1	dotyk
je	být	k5eAaImIp3nS	být
obranný	obranný	k2eAgInSc1d1	obranný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
způsobem	způsob	k1gInSc7	způsob
zachytit	zachytit	k5eAaPmF	zachytit
míč	míč	k1gInSc1	míč
odehraný	odehraný	k2eAgInSc1d1	odehraný
soupeřem	soupeř	k1gMnSc7	soupeř
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
úder	úder	k1gInSc4	úder
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
bagr	bagr	k1gInSc1	bagr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
míč	míč	k1gInSc1	míč
odehraje	odehrát	k5eAaPmIp3nS	odehrát
spodem	spodem	k6eAd1	spodem
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
obě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
spojené	spojený	k2eAgFnSc2d1	spojená
a	a	k8xC	a
míče	míč	k1gInSc2	míč
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
plochou	plocha	k1gFnSc7	plocha
předloktí	předloktí	k1gNnSc2	předloktí
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
dotyk	dotyk	k1gInSc1	dotyk
nejčastěji	často	k6eAd3	často
hraje	hrát	k5eAaImIp3nS	hrát
tzv.	tzv.	kA	tzv.
nahrávač	nahrávač	k1gMnSc1	nahrávač
<g/>
,	,	kIx,	,
v	v	k7c6	v
rekreačním	rekreační	k2eAgInSc6d1	rekreační
volejbale	volejbal	k1gInSc6	volejbal
často	často	k6eAd1	často
přední	přední	k2eAgMnSc1d1	přední
střední	střední	k2eAgMnSc1d1	střední
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hráč	hráč	k1gMnSc1	hráč
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
herní	herní	k2eAgFnSc4d1	herní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
úderu	úder	k1gInSc2	úder
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
útočný	útočný	k2eAgInSc4d1	útočný
úder	úder	k1gInSc4	úder
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hraje	hrát	k5eAaImIp3nS	hrát
vrchním	vrchní	k2eAgNnSc7d1	vrchní
odbitím	odbití	k1gNnSc7	odbití
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
míče	míč	k1gInSc2	míč
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
dotknou	dotknout	k5eAaPmIp3nP	dotknout
současně	současně	k6eAd1	současně
obě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
mírně	mírně	k6eAd1	mírně
pokrčenými	pokrčený	k2eAgInPc7d1	pokrčený
prsty	prst	k1gInPc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
úder	úder	k1gInSc1	úder
je	být	k5eAaImIp3nS	být
útočný	útočný	k2eAgInSc1d1	útočný
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejčastěji	často	k6eAd3	často
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
hráčů	hráč	k1gMnPc2	hráč
využije	využít	k5eAaPmIp3nS	využít
přesnou	přesný	k2eAgFnSc4d1	přesná
nahrávku	nahrávka	k1gFnSc4	nahrávka
ke	k	k7c3	k
smeči	smeč	k1gInSc3	smeč
<g/>
,	,	kIx,	,
silnému	silný	k2eAgInSc3d1	silný
úderu	úder	k1gInSc3	úder
paží	paží	k1gNnSc2	paží
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
provedenému	provedený	k2eAgInSc3d1	provedený
ve	v	k7c6	v
výskoku	výskok	k1gInSc6	výskok
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Bránící	bránící	k2eAgNnSc1d1	bránící
se	se	k3xPyFc4	se
mužstvo	mužstvo	k1gNnSc1	mužstvo
obvykle	obvykle	k6eAd1	obvykle
využívá	využívat	k5eAaImIp3nS	využívat
bloky	blok	k1gInPc4	blok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
buď	buď	k8xC	buď
dva	dva	k4xCgInPc1	dva
nebo	nebo	k8xC	nebo
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
přední	přední	k2eAgMnPc1d1	přední
hráči	hráč	k1gMnPc1	hráč
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
současně	současně	k6eAd1	současně
vyskočí	vyskočit	k5eAaPmIp3nP	vyskočit
s	s	k7c7	s
nataženýma	natažený	k2eAgFnPc7d1	natažená
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
soupeři	soupeř	k1gMnPc7	soupeř
snaží	snažit	k5eAaImIp3nP	snažit
zabránit	zabránit	k5eAaPmF	zabránit
v	v	k7c6	v
přenesení	přenesení	k1gNnSc6	přenesení
míče	míč	k1gInSc2	míč
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
rozehra	rozehra	k1gFnSc1	rozehra
vypadá	vypadat	k5eAaPmIp3nS	vypadat
přesně	přesně	k6eAd1	přesně
takto	takto	k6eAd1	takto
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
taktiky	taktika	k1gFnSc2	taktika
ve	v	k7c6	v
volejbalu	volejbal	k1gInSc6	volejbal
je	být	k5eAaImIp3nS	být
moment	moment	k1gInSc1	moment
překvapení	překvapení	k1gNnPc2	překvapení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
někdy	někdy	k6eAd1	někdy
nahrávač	nahrávač	k1gMnSc1	nahrávač
pouze	pouze	k6eAd1	pouze
předstírá	předstírat	k5eAaImIp3nS	předstírat
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odpoutá	odpoutat	k5eAaPmIp3nS	odpoutat
blokující	blokující	k2eAgMnPc4d1	blokující
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
překvapivým	překvapivý	k2eAgInSc7d1	překvapivý
úderem	úder	k1gInSc7	úder
(	(	kIx(	(
<g/>
ulitím	ulití	k1gNnSc7	ulití
<g/>
)	)	kIx)	)
pošle	poslat	k5eAaPmIp3nS	poslat
míč	míč	k1gInSc4	míč
těsně	těsně	k6eAd1	těsně
za	za	k7c4	za
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
volejbalu	volejbal	k1gInSc2	volejbal
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
volejbalem	volejbal	k1gInSc7	volejbal
mnohé	mnohý	k2eAgInPc4d1	mnohý
podobné	podobný	k2eAgInPc4d1	podobný
rysy	rys	k1gInPc4	rys
<g/>
:	:	kIx,	:
Volejbalové	volejbalový	k2eAgInPc1d1	volejbalový
debly	debl	k1gInPc1	debl
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
na	na	k7c6	na
polovině	polovina	k1gFnSc6	polovina
běžného	běžný	k2eAgNnSc2d1	běžné
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
se	se	k3xPyFc4	se
kategorie	kategorie	k1gFnSc2	kategorie
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
mixy	mix	k1gInPc1	mix
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
totožná	totožný	k2eAgNnPc1d1	totožné
s	s	k7c7	s
běžným	běžný	k2eAgInSc7d1	běžný
volejbalem	volejbal	k1gInSc7	volejbal
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
následujícím	následující	k2eAgMnSc6d1	následující
<g/>
:	:	kIx,	:
velikost	velikost	k1gFnSc1	velikost
hřiště	hřiště	k1gNnSc1	hřiště
a	a	k8xC	a
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
2	[number]	k4	2
proti	proti	k7c3	proti
2	[number]	k4	2
<g/>
)	)	kIx)	)
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
antének	anténka	k1gFnPc2	anténka
bloky	blok	k1gInPc1	blok
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
jako	jako	k8xS	jako
dotyk	dotyk	k1gInSc1	dotyk
míče	míč	k1gInSc2	míč
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
tahání	tahání	k1gNnSc1	tahání
míče	míč	k1gInSc2	míč
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Plážový	plážový	k2eAgInSc4d1	plážový
volejbal	volejbal	k1gInSc4	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
(	(	kIx(	(
<g/>
beach	beach	k1gMnSc1	beach
volleyball	volleyball	k1gMnSc1	volleyball
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
varianta	varianta	k1gFnSc1	varianta
volejbalu	volejbal	k1gInSc2	volejbal
hraná	hraný	k2eAgFnSc1d1	hraná
na	na	k7c6	na
menším	malý	k2eAgNnSc6d2	menší
pískovém	pískový	k2eAgNnSc6d1	pískové
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zde	zde	k6eAd1	zde
mužstva	mužstvo	k1gNnSc2	mužstvo
sestávají	sestávat	k5eAaImIp3nP	sestávat
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jemný	jemný	k2eAgInSc1d1	jemný
písek	písek	k1gInSc1	písek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
obranné	obranný	k2eAgInPc4d1	obranný
zákroky	zákrok	k1gInPc4	zákrok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
byly	být	k5eAaImAgInP	být
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
divácky	divácky	k6eAd1	divácky
velice	velice	k6eAd1	velice
atraktivní	atraktivní	k2eAgFnSc6d1	atraktivní
(	(	kIx(	(
<g/>
nemluvě	nemluva	k1gFnSc6	nemluva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráči	hráč	k1gMnPc1	hráč
a	a	k8xC	a
hráčky	hráčka	k1gFnPc1	hráčka
obvykle	obvykle	k6eAd1	obvykle
nehrají	hrát	k5eNaImIp3nP	hrát
v	v	k7c6	v
dresech	dres	k1gInPc6	dres
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
plavkách	plavka	k1gFnPc6	plavka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
profesionály	profesionál	k1gMnPc4	profesionál
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
CEV	CEV	kA	CEV
a	a	k8xC	a
FIVB	FIVB	kA	FIVB
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
plážovým	plážový	k2eAgMnPc3d1	plážový
volejbalistům	volejbalista	k1gMnPc3	volejbalista
turnaje	turnaj	k1gInPc4	turnaj
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
rozšířenější	rozšířený	k2eAgFnSc1d2	rozšířenější
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
za	za	k7c2	za
teplých	teplý	k2eAgInPc2d1	teplý
letních	letní	k2eAgInPc2d1	letní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nově	nově	k6eAd1	nově
i	i	k9	i
v	v	k7c6	v
krytých	krytý	k2eAgFnPc6d1	krytá
halách	hala	k1gFnPc6	hala
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
totožná	totožný	k2eAgNnPc1d1	totožné
s	s	k7c7	s
běžným	běžný	k2eAgInSc7d1	běžný
volejbalem	volejbal	k1gInSc7	volejbal
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
následujícím	následující	k2eAgMnSc6d1	následující
<g/>
:	:	kIx,	:
velikost	velikost	k1gFnSc1	velikost
hřiště	hřiště	k1gNnSc1	hřiště
a	a	k8xC	a
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
bloky	blok	k1gInPc1	blok
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
jako	jako	k9	jako
dotyk	dotyk	k1gInSc4	dotyk
míče	míč	k1gInSc2	míč
zákaz	zákaz	k1gInSc4	zákaz
ulití	ulití	k1gNnSc2	ulití
míče	míč	k1gInSc2	míč
Volejbalový	volejbalový	k2eAgInSc4d1	volejbalový
zápas	zápas	k1gInSc4	zápas
obvykle	obvykle	k6eAd1	obvykle
hrají	hrát	k5eAaImIp3nP	hrát
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
speciálně	speciálně	k6eAd1	speciálně
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
smíšená	smíšený	k2eAgNnPc4d1	smíšené
družstva	družstvo	k1gNnPc4	družstvo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgInPc2	který
<g/>
:	:	kIx,	:
Musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
alespoň	alespoň	k9	alespoň
dvě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
Ženy	žena	k1gFnPc1	žena
hrají	hrát	k5eAaImIp3nP	hrát
většinou	většina	k1gFnSc7	většina
nahrávačky	nahrávačka	k1gFnSc2	nahrávačka
Tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
4	[number]	k4	4
muži	muž	k1gMnPc1	muž
+	+	kIx~	+
2	[number]	k4	2
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
CÍSAŘ	Císař	k1gMnSc1	Císař
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
<g/>
:	:	kIx,	:
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
taktika	taktika	k1gFnSc1	taktika
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
průpravná	průpravný	k2eAgNnPc1d1	průpravné
cvičení	cvičení	k1gNnPc1	cvičení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
165	[number]	k4	165
s.	s.	k?	s.
Sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
Grada	Grada	k1gFnSc1	Grada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
502	[number]	k4	502
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
KAPLAN	Kaplan	k1gMnSc1	Kaplan
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
DŽAVORONOK	DŽAVORONOK	kA	DŽAVORONOK
<g/>
.	.	kIx.	.
</s>
<s>
Plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
<g/>
:	:	kIx,	:
průpravná	průpravný	k2eAgNnPc1d1	průpravné
cvičení	cvičení	k1gNnPc1	cvičení
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc1	pravidlo
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
herní	herní	k2eAgFnPc4d1	herní
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
,	,	kIx,	,
rekreační	rekreační	k2eAgFnPc4d1	rekreační
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
103	[number]	k4	103
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
VLACH	Vlach	k1gMnSc1	Vlach
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
HANÍK	HANÍK	kA	HANÍK
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
PINZÍK	PINZÍK	kA	PINZÍK
<g/>
.	.	kIx.	.
</s>
<s>
Plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
<g/>
:	:	kIx,	:
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
:	:	kIx,	:
Ústecký	ústecký	k2eAgInSc4d1	ústecký
volejbal	volejbal	k1gInSc4	volejbal
o.s.	o.s.	k?	o.s.
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
92	[number]	k4	92
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87504	[number]	k4	87504
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
HANÍK	HANÍK	kA	HANÍK
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
<g/>
:	:	kIx,	:
učebnice	učebnice	k1gFnPc1	učebnice
pro	pro	k7c4	pro
trenéry	trenér	k1gMnPc4	trenér
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
503	[number]	k4	503
s.	s.	k?	s.
Edice	edice	k1gFnPc4	edice
Českého	český	k2eAgInSc2d1	český
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
3380	[number]	k4	3380
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
volejbal	volejbal	k1gInSc4	volejbal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Český	český	k2eAgInSc1d1	český
volejbalový	volejbalový	k2eAgInSc1d1	volejbalový
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
ČVS	ČVS	kA	ČVS
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
volejbalová	volejbalový	k2eAgFnSc1d1	volejbalová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIVB	FIVB	kA	FIVB
<g/>
)	)	kIx)	)
Web	web	k1gInSc1	web
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
volejbalem	volejbal	k1gInSc7	volejbal
myslí	myslet	k5eAaImIp3nS	myslet
vážně	vážně	k6eAd1	vážně
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
jen	jen	k9	jen
zabavit	zabavit	k5eAaPmF	zabavit
(	(	kIx(	(
<g/>
HANIKVOLLEYBALL	HANIKVOLLEYBALL	kA	HANIKVOLLEYBALL
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
Server	server	k1gInSc1	server
o	o	k7c6	o
českém	český	k2eAgInSc6d1	český
volejbalu	volejbal	k1gInSc6	volejbal
(	(	kIx(	(
<g/>
VOLEJBAL	volejbal	k1gInSc1	volejbal
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
Server	server	k1gInSc1	server
pro	pro	k7c4	pro
volejbalisty	volejbalista	k1gMnPc4	volejbalista
(	(	kIx(	(
<g/>
CESKY-VOLEJBAL	CESKY-VOLEJBAL	k1gMnSc1	CESKY-VOLEJBAL
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
Volejbal	volejbal	k1gInSc1	volejbal
na	na	k7c4	na
Volley	Vollea	k1gFnPc4	Vollea
Country	country	k2eAgMnPc2d1	country
–	–	k?	–
nej	nej	k?	nej
volejbal	volejbal	k1gInSc4	volejbal
na	na	k7c4	na
netu	neta	k1gFnSc4	neta
Extraliga	extraliga	k1gFnSc1	extraliga
muži	muž	k1gMnSc6	muž
–	–	k?	–
historický	historický	k2eAgInSc4d1	historický
přehled	přehled	k1gInSc4	přehled
vítězů	vítěz	k1gMnPc2	vítěz
Extraliga	extraliga	k1gFnSc1	extraliga
ženy	žena	k1gFnSc2	žena
–	–	k?	–
historický	historický	k2eAgInSc4d1	historický
přehled	přehled	k1gInSc4	přehled
vítězek	vítězka	k1gFnPc2	vítězka
</s>
