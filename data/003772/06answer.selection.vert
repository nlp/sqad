<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Robertem	Robert	k1gMnSc7	Robert
Noycem	Noyce	k1gMnSc7	Noyce
<g/>
,	,	kIx,	,
Gordonem	Gordon	k1gMnSc7	Gordon
Moorem	Moor	k1gMnSc7	Moor
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
Moorově	Moorův	k2eAgInSc6d1	Moorův
zákonu	zákon	k1gInSc6	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arthurem	Arthur	k1gInSc7	Arthur
Rockem	rock	k1gInSc7	rock
a	a	k8xC	a
Maxem	Max	k1gMnSc7	Max
Palevskym	Palevskymum	k1gNnPc2	Palevskymum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Integrated	Integrated	k1gInSc1	Integrated
Electronics	Electronicsa	k1gFnPc2	Electronicsa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
