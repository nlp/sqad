<s>
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1892	[number]	k4	1892
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
Gliwice	Gliwice	k1gFnSc2	Gliwice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
humorista	humorista	k1gMnSc1	humorista
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
s	s	k7c7	s
židovskými	židovská	k1gFnPc7	židovská
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
Jindřich	Jindřich	k1gMnSc1	Jindřich
Poláček	Poláček	k1gMnSc1	Poláček
byl	být	k5eAaImAgMnS	být
židovský	židovský	k2eAgMnSc1d1	židovský
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Žofie	Žofie	k1gFnSc1	Žofie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
rozená	rozený	k2eAgFnSc1d1	rozená
Kohnová	Kohnová	k1gFnSc1	Kohnová
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
bratry	bratr	k1gMnPc4	bratr
-	-	kIx~	-
Arnošta	Arnošt	k1gMnSc4	Arnošt
<g/>
,	,	kIx,	,
Kamila	Kamil	k1gMnSc4	Kamil
<g/>
,	,	kIx,	,
Ludvíka	Ludvík	k1gMnSc4	Ludvík
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
sourozence	sourozenec	k1gMnSc4	sourozenec
Bertu	Berta	k1gFnSc4	Berta
a	a	k8xC	a
Milana	Milan	k1gMnSc4	Milan
z	z	k7c2	z
otcova	otcův	k2eAgInSc2d1	otcův
druhého	druhý	k4xOgInSc2	druhý
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Emílií	Emílie	k1gFnSc7	Emílie
Posilesovou	Posilesový	k2eAgFnSc7d1	Posilesový
<g/>
.	.	kIx.	.
</s>
<s>
Poláček	Poláček	k1gMnSc1	Poláček
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
vyšší	vysoký	k2eAgNnSc4d2	vyšší
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
vyhozen	vyhozen	k2eAgInSc1d1	vyhozen
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
vzdorovité	vzdorovitý	k2eAgNnSc4d1	vzdorovité
chování	chování	k1gNnSc4	chování
<g/>
"	"	kIx"	"
a	a	k8xC	a
špatný	špatný	k2eAgInSc4d1	špatný
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
gymnázium	gymnázium	k1gNnSc4	gymnázium
dokončil	dokončit	k5eAaPmAgMnS	dokončit
maturitní	maturitní	k2eAgFnSc7d1	maturitní
zkouškou	zkouška	k1gFnSc7	zkouška
v	v	k7c6	v
Truhlářské	truhlářský	k2eAgFnSc6d1	truhlářská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
organizoval	organizovat	k5eAaBmAgMnS	organizovat
studentské	studentský	k2eAgNnSc4d1	studentské
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
překládal	překládat	k5eAaImAgMnS	překládat
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
také	také	k6eAd1	také
je	být	k5eAaImIp3nS	být
řídil	řídit	k5eAaImAgMnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
Poláček	Poláček	k1gMnSc1	Poláček
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
též	též	k9	též
o	o	k7c4	o
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
však	však	k9	však
"	"	kIx"	"
<g/>
neměl	mít	k5eNaImAgMnS	mít
štěstí	štěstit	k5eAaImIp3nS	štěstit
na	na	k7c4	na
trvalá	trvalý	k2eAgNnPc4d1	trvalé
místa	místo	k1gNnPc4	místo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Poláček	Poláček	k1gMnSc1	Poláček
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
na	na	k7c4	na
hodnost	hodnost	k1gFnSc4	hodnost
četaře	četař	k1gMnSc2	četař
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
zkušenostech	zkušenost	k1gFnPc6	zkušenost
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c2	za
čtyřletého	čtyřletý	k2eAgNnSc2d1	čtyřleté
vojákování	vojákování	k1gNnSc2	vojákování
jsem	být	k5eAaImIp1nS	být
'	'	kIx"	'
<g/>
Švejka	Švejk	k1gMnSc4	Švejk
<g/>
'	'	kIx"	'
vůbec	vůbec	k9	vůbec
nepotkal	potkat	k5eNaPmAgMnS	potkat
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
neměli	mít	k5eNaImAgMnP	mít
rádi	rád	k2eAgMnPc1d1	rád
Rakouska	Rakousko	k1gNnSc2	Rakousko
ani	ani	k8xC	ani
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dřeli	dřít	k5eAaImAgMnP	dřít
do	do	k7c2	do
úpadu	úpad	k1gInSc2	úpad
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prošel	projít	k5eAaPmAgInS	projít
ruskou	ruský	k2eAgFnSc4d1	ruská
a	a	k8xC	a
italskou	italský	k2eAgFnSc4d1	italská
frontou	fronta	k1gFnSc7	fronta
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
války	válka	k1gFnSc2	válka
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
srbského	srbský	k2eAgNnSc2d1	srbské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
ČSR	ČSR	kA	ČSR
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
ve	v	k7c4	v
vývozní	vývozní	k2eAgFnSc4d1	vývozní
a	a	k8xC	a
dovozní	dovozní	k2eAgFnSc4d1	dovozní
komisi	komise	k1gFnSc4	komise
(	(	kIx(	(
<g/>
pozdějším	pozdní	k2eAgInSc6d2	pozdější
Úřadě	úřad	k1gInSc6	úřad
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Zkušeností	zkušenost	k1gFnSc7	zkušenost
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
zužitkoval	zužitkovat	k5eAaPmAgInS	zužitkovat
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Kolotoč	kolotoč	k1gInSc1	kolotoč
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
začátkem	začátek	k1gInSc7	začátek
jeho	jeho	k3xOp3gFnSc2	jeho
spisovatelské	spisovatelský	k2eAgFnSc2d1	spisovatelská
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
do	do	k7c2	do
satirických	satirický	k2eAgInPc2d1	satirický
časopisů	časopis	k1gInPc2	časopis
Štika	Štika	k1gMnSc1	Štika
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
Nebojsa	nebojsa	k1gMnSc1	nebojsa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
Karlem	Karel	k1gMnSc7	Karel
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Čapkovými	Čapkův	k2eAgNnPc7d1	Čapkovo
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
členem	člen	k1gInSc7	člen
kruhu	kruh	k1gInSc2	kruh
pátečníků	pátečník	k1gMnPc2	pátečník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
soustavně	soustavně	k6eAd1	soustavně
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
Lidovými	lidový	k2eAgFnPc7d1	lidová
novinami	novina	k1gFnPc7	novina
jako	jako	k9	jako
sloupkař	sloupkař	k1gMnSc1	sloupkař
a	a	k8xC	a
fejetonista	fejetonista	k1gMnSc1	fejetonista
a	a	k8xC	a
také	také	k9	také
soudní	soudní	k2eAgInSc1d1	soudní
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
Tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
listech	list	k1gInPc6	list
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Melantrich	Melantricha	k1gFnPc2	Melantricha
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
slově	slovo	k1gNnSc6	slovo
jako	jako	k9	jako
sloupkař	sloupkař	k1gMnSc1	sloupkař
a	a	k8xC	a
soudničkář	soudničkář	k1gMnSc1	soudničkář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
redigoval	redigovat	k5eAaImAgInS	redigovat
humoristický	humoristický	k2eAgInSc1d1	humoristický
časopis	časopis	k1gInSc1	časopis
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
znovu	znovu	k6eAd1	znovu
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
zavádění	zavádění	k1gNnSc3	zavádění
rasových	rasový	k2eAgInPc2d1	rasový
zákonů	zákon	k1gInPc2	zákon
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
psal	psát	k5eAaImAgInS	psát
zejména	zejména	k9	zejména
sloupky	sloupek	k1gInPc4	sloupek
a	a	k8xC	a
soudničky	soudnička	k1gFnPc4	soudnička
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
se	se	k3xPyFc4	se
Poláček	Poláček	k1gMnSc1	Poláček
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Adélou	Adéla	k1gFnSc7	Adéla
Herrmannovou	Herrmannová	k1gFnSc7	Herrmannová
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
dcery	dcera	k1gFnSc2	dcera
Jiřiny	Jiřina	k1gFnSc2	Jiřina
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
manželství	manželství	k1gNnSc1	manželství
nebylo	být	k5eNaImAgNnS	být
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
Poláčkovo	Poláčkův	k2eAgNnSc1d1	Poláčkovo
a	a	k8xC	a
Herrmannové	Herrmannový	k2eAgNnSc1d1	Herrmannový
manželství	manželství	k1gNnSc1	manželství
rozvedeno	rozveden	k2eAgNnSc1d1	rozvedeno
"	"	kIx"	"
<g/>
od	od	k7c2	od
stolu	stol	k1gInSc2	stol
a	a	k8xC	a
lože	lože	k1gNnSc2	lože
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
a	a	k8xC	a
rasová	rasový	k2eAgFnSc1d1	rasová
perzekuce	perzekuce	k1gFnSc1	perzekuce
Poláčkovi	Poláček	k1gMnSc3	Poláček
znemožnily	znemožnit	k5eAaPmAgInP	znemožnit
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
<g/>
;	;	kIx,	;
živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
jako	jako	k9	jako
knihovník	knihovník	k1gMnSc1	knihovník
v	v	k7c6	v
Židovské	židovský	k2eAgFnSc6d1	židovská
náboženské	náboženský	k2eAgFnSc6d1	náboženská
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Dceru	dcera	k1gFnSc4	dcera
Jiřinu	Jiřina	k1gFnSc4	Jiřina
stačil	stačit	k5eAaBmAgMnS	stačit
(	(	kIx(	(
<g/>
především	především	k9	především
díky	díky	k7c3	díky
aktivitě	aktivita	k1gFnSc3	aktivita
své	svůj	k3xOyFgFnSc2	svůj
družky	družka	k1gFnSc2	družka
<g/>
,	,	kIx,	,
právničky	právnička	k1gFnSc2	právnička
Dory	Dora	k1gFnSc2	Dora
Vaňákové	Vaňáková	k1gFnSc2	Vaňáková
<g/>
)	)	kIx)	)
ještě	ještě	k6eAd1	ještě
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
byli	být	k5eAaImAgMnP	být
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
i	i	k8xC	i
Dora	Dora	k1gFnSc1	Dora
Vaňáková	Vaňáková	k1gFnSc1	Vaňáková
deportováni	deportovat	k5eAaBmNgMnP	deportovat
do	do	k7c2	do
terezínského	terezínský	k2eAgNnSc2d1	Terezínské
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
pak	pak	k6eAd1	pak
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
do	do	k7c2	do
transportu	transport	k1gInSc2	transport
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Osvětim	Osvětim	k1gFnSc4	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poláček	Poláček	k1gMnSc1	Poláček
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
v	v	k7c6	v
plynové	plynový	k2eAgFnSc6d1	plynová
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
najít	najít	k5eAaPmF	najít
důležitého	důležitý	k2eAgMnSc4d1	důležitý
svědka	svědek	k1gMnSc4	svědek
<g/>
,	,	kIx,	,
účastnici	účastnice	k1gFnSc4	účastnice
pochodu	pochod	k1gInSc2	pochod
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
Kláru	Klára	k1gFnSc4	Klára
Baumöhlovou	Baumöhlový	k2eAgFnSc4d1	Baumöhlový
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poláček	Poláček	k1gMnSc1	Poláček
přežil	přežít	k5eAaPmAgMnS	přežít
transport	transport	k1gInSc4	transport
z	z	k7c2	z
Osvětimi	Osvětim	k1gFnSc2	Osvětim
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
Hindenburg	Hindenburg	k1gInSc4	Hindenburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgMnS	napsat
krátkou	krátký	k2eAgFnSc4d1	krátká
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
svědkyně	svědkyně	k1gFnSc1	svědkyně
také	také	k6eAd1	také
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
účastnil	účastnit	k5eAaImAgMnS	účastnit
pochodu	pochod	k1gInSc2	pochod
z	z	k7c2	z
Hindenburgu	Hindenburg	k1gInSc2	Hindenburg
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
Gleiwitz	Gleiwitza	k1gFnPc2	Gleiwitza
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
jej	on	k3xPp3gMnSc4	on
paní	paní	k1gFnSc3	paní
Baumöhlová	Baumöhlová	k1gFnSc1	Baumöhlová
viděla	vidět	k5eAaImAgFnS	vidět
naposled	naposled	k6eAd1	naposled
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Poláček	Poláček	k1gMnSc1	Poláček
zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c6	na
pochodu	pochod	k1gInSc6	pochod
<g/>
,	,	kIx,	,
při	při	k7c6	při
vstupní	vstupní	k2eAgFnSc6d1	vstupní
selekci	selekce	k1gFnSc6	selekce
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Údaj	údaj	k1gInSc1	údaj
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
datu	datum	k1gNnSc3	datum
selekce	selekce	k1gFnSc2	selekce
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Gleiwitz	Gleiwitz	k1gInSc1	Gleiwitz
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
ho	on	k3xPp3gNnSc4	on
též	též	k9	též
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
A	a	k9	a
přiveď	přivést	k5eAaPmRp2nS	přivést
zpět	zpět	k6eAd1	zpět
naše	náš	k3xOp1gFnPc1	náš
roztroušené	roztroušený	k2eAgFnPc1d1	roztroušená
<g/>
"	"	kIx"	"
z	z	k7c2	z
r.	r.	kA	r.
1946	[number]	k4	1946
jeho	jeho	k3xOp3gNnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
předválečný	předválečný	k2eAgMnSc1d1	předválečný
kolega	kolega	k1gMnSc1	kolega
a	a	k8xC	a
spoluvězeň	spoluvězeň	k1gMnSc1	spoluvězeň
František	František	k1gMnSc1	František
R.	R.	kA	R.
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgMnPc2d1	poslední
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
Poláčka	Poláček	k1gMnSc4	Poláček
viděl	vidět	k5eAaImAgMnS	vidět
naživu	naživu	k6eAd1	naživu
v	v	k7c6	v
KZ	KZ	kA	KZ
Gleiwitz	Gleiwitz	k1gInSc4	Gleiwitz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
Poláčkovi	Poláček	k1gMnSc3	Poláček
in	in	k?	in
memoriam	memoriam	k1gInSc4	memoriam
propůjčen	propůjčit	k5eAaPmNgInS	propůjčit
Řád	řád	k1gInSc4	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garrigu	k1gInSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Poláček	Poláček	k1gMnSc1	Poláček
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
česky	česky	k6eAd1	česky
píšícím	píšící	k2eAgMnPc3d1	píšící
humoristům	humorist	k1gMnPc3	humorist
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
románech	román	k1gInPc6	román
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
především	především	k9	především
směšnost	směšnost	k1gFnSc4	směšnost
i	i	k8xC	i
tragiku	tragika	k1gFnSc4	tragika
života	život	k1gInSc2	život
"	"	kIx"	"
<g/>
malého	malý	k2eAgMnSc2d1	malý
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
Mariáš	mariáš	k1gInSc1	mariáš
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
živnosti	živnost	k1gFnPc1	živnost
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hedvika	Hedvika	k1gFnSc1	Hedvika
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
Hráči	hráč	k1gMnPc1	hráč
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k8xS	jako
specifický	specifický	k2eAgInSc1d1	specifický
"	"	kIx"	"
<g/>
román	román	k1gInSc1	román
živnosti	živnost	k1gFnSc2	živnost
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
všechny	všechen	k3xTgFnPc4	všechen
vystupující	vystupující	k2eAgFnPc4d1	vystupující
postavy	postava	k1gFnPc4	postava
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
utvářeny	utvářen	k2eAgFnPc1d1	utvářena
svým	svůj	k3xOyFgNnSc7	svůj
povoláním	povolání	k1gNnSc7	povolání
nebo	nebo	k8xC	nebo
zálibou	záliba	k1gFnSc7	záliba
<g/>
.	.	kIx.	.
</s>
<s>
Poláčkův	Poláčkův	k2eAgInSc1d1	Poláčkův
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
lakonický	lakonický	k2eAgInSc4d1	lakonický
<g/>
,	,	kIx,	,
bezozdobný	bezozdobný	k2eAgInSc4d1	bezozdobný
<g/>
,	,	kIx,	,
antiromantický	antiromantický	k2eAgInSc4d1	antiromantický
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
najde	najít	k5eAaPmIp3nS	najít
přesně	přesně	k6eAd1	přesně
odměřený	odměřený	k2eAgInSc1d1	odměřený
patos	patos	k1gInSc1	patos
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vyprodáno	vyprodán	k2eAgNnSc1d1	vyprodáno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
s	s	k7c7	s
komickým	komický	k2eAgInSc7d1	komický
efektem	efekt	k1gInSc7	efekt
přívlastek	přívlastek	k1gInSc1	přívlastek
stálý	stálý	k2eAgInSc1d1	stálý
(	(	kIx(	(
<g/>
epiteton	epiteton	k1gNnSc1	epiteton
constans	constansa	k1gFnPc2	constansa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
důsledný	důsledný	k2eAgInSc4d1	důsledný
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
žurnalistické	žurnalistický	k2eAgFnSc3d1	žurnalistická
a	a	k8xC	a
politické	politický	k2eAgFnSc3d1	politická
frázi	fráze	k1gFnSc3	fráze
<g/>
,	,	kIx,	,
posmutnělý	posmutnělý	k2eAgInSc4d1	posmutnělý
humor	humor	k1gInSc4	humor
a	a	k8xC	a
niterný	niterný	k2eAgInSc4d1	niterný
realismus	realismus	k1gInSc4	realismus
byl	být	k5eAaImAgInS	být
srovnáván	srovnáván	k2eAgInSc1d1	srovnáván
s	s	k7c7	s
A.	A.	kA	A.
P.	P.	kA	P.
Čechovem	Čechov	k1gMnSc7	Čechov
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
dobovou	dobový	k2eAgFnSc7d1	dobová
kritikou	kritika	k1gFnSc7	kritika
bylo	být	k5eAaImAgNnS	být
smíšené	smíšený	k2eAgNnSc1d1	smíšené
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
F.	F.	kA	F.
X.	X.	kA	X.
Šalda	Šalda	k1gMnSc1	Šalda
neuznal	uznat	k5eNaPmAgMnS	uznat
literární	literární	k2eAgFnPc4d1	literární
kvality	kvalita	k1gFnPc4	kvalita
Poláčkovy	Poláčkův	k2eAgFnSc2d1	Poláčkova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jazykové	jazykový	k2eAgInPc1d1	jazykový
prostředky	prostředek	k1gInPc1	prostředek
(	(	kIx(	(
<g/>
opakování	opakování	k1gNnSc1	opakování
a	a	k8xC	a
sdílení	sdílení	k1gNnSc1	sdílení
frází	fráze	k1gFnPc2	fráze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Poláček	Poláček	k1gMnSc1	Poláček
užívá	užívat	k5eAaImIp3nS	užívat
k	k	k7c3	k
vystižení	vystižení	k1gNnSc3	vystižení
omezeného	omezený	k2eAgInSc2d1	omezený
životního	životní	k2eAgInSc2d1	životní
a	a	k8xC	a
duševního	duševní	k2eAgInSc2d1	duševní
obzoru	obzor	k1gInSc2	obzor
svých	svůj	k3xOyFgMnPc2	svůj
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
Šalda	Šalda	k1gMnSc1	Šalda
jako	jako	k9	jako
chudobu	chudoba	k1gFnSc4	chudoba
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
přelíčení	přelíčení	k1gNnPc1	přelíčení
byla	být	k5eAaImAgNnP	být
podle	podle	k7c2	podle
Šaldy	Šalda	k1gMnSc2	Šalda
práce	práce	k1gFnSc2	práce
"	"	kIx"	"
<g/>
věcná	věcný	k2eAgFnSc1d1	věcná
<g/>
,	,	kIx,	,
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
střízlivá	střízlivý	k2eAgFnSc1d1	střízlivá
až	až	k9	až
do	do	k7c2	do
nudy	nuda	k1gFnSc2	nuda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
i	i	k9	i
Okresní	okresní	k2eAgNnSc4d1	okresní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Karla	Karel	k1gMnSc2	Karel
Poláčka	Poláček	k1gMnSc2	Poláček
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Poláčkova	Poláčkův	k2eAgFnSc1d1	Poláčkova
sbírka	sbírka	k1gFnSc1	sbírka
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Povídky	povídka	k1gFnSc2	povídka
pana	pan	k1gMnSc2	pan
Kočkodana	kočkodan	k1gMnSc2	kočkodan
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Mariáš	mariáš	k1gInSc4	mariáš
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
živnosti	živnost	k1gFnPc4	živnost
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
podává	podávat	k5eAaImIp3nS	podávat
Poláček	Poláček	k1gMnSc1	Poláček
"	"	kIx"	"
<g/>
komiku	komika	k1gFnSc4	komika
živností	živnost	k1gFnPc2	živnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
neznáma	neznámo	k1gNnSc2	neznámo
a	a	k8xC	a
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
šunka	šunka	k1gFnSc1	šunka
(	(	kIx(	(
<g/>
F.	F.	kA	F.
Borový	borový	k2eAgInSc1d1	borový
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
satirické	satirický	k2eAgFnSc2d1	satirická
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
sbírkám	sbírka	k1gFnPc3	sbírka
krátkých	krátká	k1gFnPc2	krátká
próz	próza	k1gFnPc2	próza
patří	patřit	k5eAaImIp3nP	patřit
Povídky	povídka	k1gFnPc1	povídka
izraelského	izraelský	k2eAgNnSc2d1	izraelské
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
ve	v	k7c6	v
filmu	film	k1gInSc6	film
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	o	k7c6	o
humoru	humor	k1gInSc6	humor
v	v	k7c6	v
životě	život	k1gInSc6	život
a	a	k8xC	a
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Metempsychóza	metempsychóza	k1gFnSc1	metempsychóza
čili	čili	k8xC	čili
stěhování	stěhování	k1gNnSc1	stěhování
duší	duše	k1gFnPc2	duše
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Doktor	doktor	k1gMnSc1	doktor
Munory	Munora	k1gFnSc2	Munora
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poláček	Poláček	k1gMnSc1	Poláček
také	také	k9	také
sestavil	sestavit	k5eAaPmAgMnS	sestavit
sbírku	sbírka	k1gFnSc4	sbírka
židovského	židovský	k2eAgInSc2d1	židovský
humoru	humor	k1gInSc2	humor
Židovské	židovský	k2eAgFnSc2d1	židovská
anekdoty	anekdota	k1gFnSc2	anekdota
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žurnalistický	žurnalistický	k2eAgInSc1d1	žurnalistický
slovník	slovník	k1gInSc1	slovník
s	s	k7c7	s
předmluvou	předmluva	k1gFnSc7	předmluva
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
parodii	parodie	k1gFnSc6	parodie
na	na	k7c4	na
výkladový	výkladový	k2eAgInSc4d1	výkladový
slovník	slovník	k1gInSc4	slovník
Poláček	Poláček	k1gMnSc1	Poláček
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
soudobou	soudobý	k2eAgFnSc4d1	soudobá
novinářskou	novinářský	k2eAgFnSc4d1	novinářská
frázi	fráze	k1gFnSc4	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Lehká	lehký	k2eAgFnSc1d1	lehká
dívka	dívka	k1gFnSc1	dívka
a	a	k8xC	a
reportér	reportér	k1gMnSc1	reportér
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
větším	veliký	k2eAgInSc7d2	veliký
dílem	díl	k1gInSc7	díl
Karla	Karel	k1gMnSc2	Karel
Poláčka	Poláček	k1gMnSc2	Poláček
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
novinářské	novinářský	k2eAgNnSc1d1	novinářské
prostředí	prostředí	k1gNnSc1	prostředí
s	s	k7c7	s
postavou	postava	k1gFnSc7	postava
redaktora	redaktor	k1gMnSc2	redaktor
Hirsche	Hirsch	k1gMnSc2	Hirsch
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
offsidu	offsid	k1gInSc6	offsid
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
Poláčkův	Poláčkův	k2eAgInSc4d1	Poláčkův
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
výrazný	výrazný	k2eAgInSc1d1	výrazný
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
fanoušků	fanoušek	k1gMnPc2	fanoušek
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
FK	FK	kA	FK
Viktoria	Viktoria	k1gFnSc1	Viktoria
Žižkov	Žižkov	k1gInSc1	Žižkov
a	a	k8xC	a
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knížce	knížka	k1gFnSc6	knížka
Hedvika	Hedvika	k1gFnSc1	Hedvika
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
humor	humor	k1gInSc1	humor
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
převrácení	převrácení	k1gNnSc2	převrácení
stereotypu	stereotyp	k1gInSc2	stereotyp
pečujících	pečující	k2eAgMnPc2d1	pečující
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
neodpovědných	odpovědný	k2eNgFnPc2d1	neodpovědná
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
i	i	k9	i
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Hráči	hráč	k1gMnPc1	hráč
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
a	a	k8xC	a
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
hráče	hráč	k1gMnPc4	hráč
mariáše	mariáš	k1gInSc2	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
Edudant	Edudant	k1gMnSc1	Edudant
a	a	k8xC	a
Francimor	Francimor	k1gMnSc1	Francimor
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
povídka	povídka	k1gFnSc1	povídka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
někdy	někdy	k6eAd1	někdy
absurdním	absurdní	k2eAgInSc7d1	absurdní
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Hostinec	hostinec	k1gInSc1	hostinec
U	u	k7c2	u
Kamenného	kamenný	k2eAgInSc2d1	kamenný
stolu	stol	k1gInSc2	stol
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
humoristický	humoristický	k2eAgInSc1d1	humoristický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
pokryto	pokryt	k2eAgNnSc4d1	pokryto
<g/>
"	"	kIx"	"
jménem	jméno	k1gNnSc7	jméno
malíře	malíř	k1gMnSc2	malíř
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
policejní	policejní	k2eAgMnSc1d1	policejní
strážník	strážník	k1gMnSc1	strážník
zakoupí	zakoupit	k5eAaPmIp3nS	zakoupit
dům	dům	k1gInSc4	dům
a	a	k8xC	a
terorizuje	terorizovat	k5eAaImIp3nS	terorizovat
své	svůj	k3xOyFgMnPc4	svůj
nájemníky	nájemník	k1gMnPc4	nájemník
<g/>
.	.	kIx.	.
</s>
<s>
Michelup	Michelup	k1gInSc1	Michelup
a	a	k8xC	a
motocykl	motocykl	k1gInSc1	motocykl
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
satirou	satira	k1gFnSc7	satira
na	na	k7c4	na
vlastnický	vlastnický	k2eAgInSc4d1	vlastnický
pud	pud	k1gInSc4	pud
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
romány	román	k1gInPc1	román
končí	končit	k5eAaImIp3nP	končit
konvenčním	konvenčnět	k5eAaImIp1nS	konvenčnět
happy	happ	k1gMnPc4	happ
endem	endem	k6eAd1	endem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
přelíčení	přelíčení	k1gNnSc1	přelíčení
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
vážný	vážný	k2eAgInSc1d1	vážný
Poláčkův	Poláčkův	k2eAgInSc1d1	Poláčkův
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
případem	případ	k1gInSc7	případ
loupežné	loupežný	k2eAgFnSc2d1	loupežná
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
udála	udát	k5eAaPmAgFnS	udát
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1928	[number]	k4	1928
v	v	k7c6	v
Hostivaři	Hostivař	k1gFnSc6	Hostivař
a	a	k8xC	a
o	o	k7c6	o
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
soudním	soudní	k2eAgNnSc6d1	soudní
projednávání	projednávání	k1gNnSc6	projednávání
Poláček	Poláček	k1gMnSc1	Poláček
referoval	referovat	k5eAaBmAgMnS	referovat
<g/>
.	.	kIx.	.
</s>
<s>
Tragikomický	tragikomický	k2eAgInSc1d1	tragikomický
příběh	příběh	k1gInSc1	příběh
malého	malý	k2eAgMnSc2d1	malý
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
slabocha	slaboch	k1gMnSc2	slaboch
a	a	k8xC	a
naivního	naivní	k2eAgMnSc2d1	naivní
snílka	snílek	k1gMnSc2	snílek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
majetku	majetek	k1gInSc6	majetek
dopustí	dopustit	k5eAaPmIp3nS	dopustit
nejprve	nejprve	k6eAd1	nejprve
podvodu	podvod	k1gInSc2	podvod
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
předstupeň	předstupeň	k1gInSc4	předstupeň
Okresního	okresní	k2eAgNnSc2d1	okresní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Poláček	Poláček	k1gMnSc1	Poláček
chtěl	chtít	k5eAaImAgMnS	chtít
napsat	napsat	k5eAaBmF	napsat
román	román	k1gInSc4	román
ze	z	k7c2	z
života	život	k1gInSc2	život
českého	český	k2eAgNnSc2d1	české
maloměsta	maloměsto	k1gNnSc2	maloměsto
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
český	český	k2eAgInSc1d1	český
politický	politický	k2eAgInSc1d1	politický
život	život	k1gInSc1	život
tkví	tkvět	k5eAaImIp3nS	tkvět
svými	svůj	k3xOyFgInPc7	svůj
kořeny	kořen	k1gInPc7	kořen
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
minulosti	minulost	k1gFnSc6	minulost
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydány	vydán	k2eAgInPc1d1	vydán
byly	být	k5eAaImAgInP	být
čtyři	čtyři	k4xCgInPc1	čtyři
díly	díl	k1gInPc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
českém	český	k2eAgNnSc6d1	české
městě	město	k1gNnSc6	město
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Hrdinové	Hrdinová	k1gFnSc2	Hrdinová
táhnou	táhnout	k5eAaImIp3nP	táhnout
do	do	k7c2	do
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
propuká	propukat	k5eAaImIp3nS	propukat
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
"	"	kIx"	"
<g/>
s	s	k7c7	s
konzervami	konzerva	k1gFnPc7	konzerva
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
výzbrojí	výzbroj	k1gFnSc7	výzbroj
vyfasovali	vyfasovat	k5eAaPmAgMnP	vyfasovat
i	i	k9	i
vůli	vůle	k1gFnSc4	vůle
erární	erární	k2eAgInPc1d1	erární
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgNnSc1d1	podzemní
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
celé	celá	k1gFnSc2	celá
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jakési	jakýsi	k3yIgNnSc4	jakýsi
surogátní	surogátní	k2eAgNnSc4d1	surogátní
podzemní	podzemní	k2eAgNnSc4d1	podzemní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Vyprodáno	vyprodat	k5eAaPmNgNnS	vyprodat
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
vyhladovělé	vyhladovělý	k2eAgNnSc1d1	vyhladovělé
zázemí	zázemí	k1gNnSc1	zázemí
a	a	k8xC	a
události	událost	k1gFnPc1	událost
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
společenské	společenský	k2eAgFnPc4d1	společenská
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
rituály	rituál	k1gInPc4	rituál
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgNnSc4d1	dané
společenské	společenský	k2eAgNnSc4d1	společenské
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
ustáleným	ustálený	k2eAgInSc7d1	ustálený
životem	život	k1gInSc7	život
neotřese	otřást	k5eNaPmIp3nS	otřást
zcela	zcela	k6eAd1	zcela
ani	ani	k8xC	ani
taková	takový	k3xDgFnSc1	takový
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
je	být	k5eAaImIp3nS	být
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
válka	válka	k1gFnSc1	válka
...	...	k?	...
není	být	k5eNaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
natrvalo	natrvalo	k6eAd1	natrvalo
změnila	změnit	k5eAaPmAgFnS	změnit
lidské	lidský	k2eAgNnSc4d1	lidské
ustrojení	ustrojení	k1gNnSc4	ustrojení
a	a	k8xC	a
lidské	lidský	k2eAgNnSc4d1	lidské
stavovství	stavovství	k1gNnSc4	stavovství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tvůrčí	tvůrčí	k2eAgFnSc3d1	tvůrčí
metodě	metoda	k1gFnSc3	metoda
spisovatel	spisovatel	k1gMnSc1	spisovatel
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
mne	já	k3xPp1nSc4	já
individuální	individuální	k2eAgFnSc1d1	individuální
psychologie	psychologie	k1gFnSc1	psychologie
neláká	lákat	k5eNaImIp3nS	lákat
tak	tak	k6eAd1	tak
jako	jako	k9	jako
psychologie	psychologie	k1gFnSc1	psychologie
davu	dav	k1gInSc2	dav
a	a	k8xC	a
stavu	stav	k1gInSc2	stav
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
aktualita	aktualita	k1gFnSc1	aktualita
mne	já	k3xPp1nSc4	já
neláká	lákat	k5eNaImIp3nS	lákat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nestane	stanout	k5eNaPmIp3nS	stanout
perpetualitou	perpetualita	k1gFnSc7	perpetualita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dle	dle	k7c2	dle
Aleny	Alena	k1gFnSc2	Alena
Hájkové	Hájková	k1gFnSc2	Hájková
se	se	k3xPyFc4	se
spisovatel	spisovatel	k1gMnSc1	spisovatel
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
maloměstské	maloměstský	k2eAgFnSc6d1	maloměstská
kronice	kronika	k1gFnSc6	kronika
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obejít	obejít	k5eAaPmF	obejít
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
kazatelského	kazatelský	k2eAgInSc2d1	kazatelský
tónu	tón	k1gInSc2	tón
i	i	k8xC	i
přičinlivého	přičinlivý	k2eAgInSc2d1	přičinlivý
komentáře	komentář	k1gInSc2	komentář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc4	román
Bylo	být	k5eAaImAgNnS	být
nás	my	k3xPp1nPc4	my
pět	pět	k4xCc1	pět
napsal	napsat	k5eAaBmAgMnS	napsat
Poláček	Poláček	k1gMnSc1	Poláček
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
maloměstě	maloměsto	k1gNnSc6	maloměsto
tentokrát	tentokrát	k6eAd1	tentokrát
očima	oko	k1gNnPc7	oko
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
jazykové	jazykový	k2eAgFnSc2d1	jazyková
komiky	komika	k1gFnSc2	komika
<g/>
.	.	kIx.	.
</s>
<s>
Příhody	příhoda	k1gFnSc2	příhoda
pěti	pět	k4xCc2	pět
chlapců	chlapec	k1gMnPc2	chlapec
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgInPc1d1	psán
groteskní	groteskní	k2eAgFnSc7d1	groteskní
kombinací	kombinace	k1gFnSc7	kombinace
jazyka	jazyk	k1gInSc2	jazyk
hovorového	hovorový	k2eAgInSc2d1	hovorový
<g/>
,	,	kIx,	,
obratů	obrat	k1gInPc2	obrat
z	z	k7c2	z
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
četby	četba	k1gFnSc2	četba
a	a	k8xC	a
slohu	sloh	k1gInSc2	sloh
povinných	povinný	k2eAgFnPc2d1	povinná
školních	školní	k2eAgFnPc2d1	školní
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
napsal	napsat	k5eAaPmAgMnS	napsat
Radko	Radka	k1gFnSc5	Radka
Pytlík	pytlík	k1gInSc4	pytlík
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nesourodými	sourodý	k2eNgFnPc7d1	nesourodá
jazykovými	jazykový	k2eAgFnPc7d1	jazyková
rovinami	rovina	k1gFnPc7	rovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
parodii	parodie	k1gFnSc6	parodie
školního	školní	k2eAgInSc2d1	školní
<g/>
,	,	kIx,	,
vědomě	vědomě	k6eAd1	vědomě
strojeného	strojený	k2eAgNnSc2d1	strojené
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgInS	ukázat
kouzelný	kouzelný	k2eAgInSc4d1	kouzelný
dětský	dětský	k2eAgInSc4d1	dětský
svět	svět	k1gInSc4	svět
bez	bez	k7c2	bez
chtěné	chtěný	k2eAgFnSc2d1	chtěná
naivity	naivita	k1gFnSc2	naivita
a	a	k8xC	a
poučování	poučování	k1gNnSc2	poučování
<g/>
,	,	kIx,	,
v	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
humoru	humor	k1gInSc6	humor
klukovských	klukovský	k2eAgFnPc2d1	klukovská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
řečí	řeč	k1gFnPc2	řeč
a	a	k8xC	a
anekdot	anekdota	k1gFnPc2	anekdota
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
si	se	k3xPyFc3	se
Poláček	Poláček	k1gMnSc1	Poláček
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
offsidu	offsid	k1gInSc6	offsid
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
Obrácení	obrácení	k1gNnSc4	obrácení
Ferdyše	Ferdyš	k1gMnSc2	Ferdyš
Pištory	Pištor	k1gInPc1	Pištor
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Fr.	Fr.	k1gMnSc2	Fr.
Langera	Langer	k1gMnSc2	Langer
Načeradec	Načeradec	k1gMnSc1	Načeradec
král	král	k1gMnSc1	král
kibiců	kibic	k1gMnPc2	kibic
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
v	v	k7c6	v
Kocourkově	Kocourkov	k1gInSc6	Kocourkov
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Včera	včera	k6eAd1	včera
neděle	neděle	k1gFnSc1	neděle
byla	být	k5eAaImAgFnS	být
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Hostinec	hostinec	k1gInSc1	hostinec
"	"	kIx"	"
<g/>
U	u	k7c2	u
Kamenného	kamenný	k2eAgInSc2d1	kamenný
stolu	stol	k1gInSc2	stol
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
přelíčení	přelíčení	k1gNnSc1	přelíčení
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Dohazovač	dohazovač	k1gMnSc1	dohazovač
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Michelup	Michelup	k1gInSc1	Michelup
a	a	k8xC	a
motocykl	motocykl	k1gInSc1	motocykl
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Uctivá	uctivý	k2eAgFnSc1d1	uctivá
poklona	poklona	k1gFnSc1	poklona
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Kohn	Kohn	k1gMnSc1	Kohn
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Z	z	k7c2	z
deníku	deník	k1gInSc2	deník
žáka	žák	k1gMnSc2	žák
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
B	B	kA	B
aneb	aneb	k?	aneb
Edudant	Edudant	k1gMnSc1	Edudant
a	a	k8xC	a
Francimor	Francimor	k1gMnSc1	Francimor
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Michelup	Michelup	k1gInSc1	Michelup
a	a	k8xC	a
motocykl	motocykl	k1gInSc1	motocykl
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Bylo	být	k5eAaImAgNnS	být
nás	my	k3xPp1nPc2	my
pět	pět	k4xCc1	pět
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Doktor	doktor	k1gMnSc1	doktor
Munory	Munora	k1gFnSc2	Munora
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Vše	všechen	k3xTgNnSc1	všechen
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
