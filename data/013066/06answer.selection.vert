<s>
Měď	měď	k1gFnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Cu	Cu	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Cuprum	Cuprum	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ušlechtilý	ušlechtilý	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
načervenalé	načervenalý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc4d1
člověkem	člověk	k1gMnSc7
již	již	k6eAd1
od	od	k7c2
starověku	starověk	k1gInSc2
<g/>
.	.	kIx.
</s>