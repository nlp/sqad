<s>
Triglavský	Triglavský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
Triglavski	Triglavski	k1gNnSc1
narodni	narodnit	k5eAaPmRp2nS
park	park	k1gInSc1
<g/>
;	;	kIx,
TNP	TNP	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
,	,	kIx,
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
severozápadě	severozápad	k1gInSc6
republiky	republika	k1gFnSc2
v	v	k7c6
obvodu	obvod	k1gInSc6
osmi	osm	k4xCc2
občin	občina	k1gFnPc2
<g/>
.	.	kIx.
</s>