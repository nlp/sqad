<s>
Triglavský	Triglavský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuTriglavský	infoboxuTriglavský	k2eAgInSc4d1
národní	národní	k2eAgFnSc4d1
parkTriglavski	parkTriglavske	k1gFnSc4
narodni	narodnit	k5eAaPmRp2nS
parkIUCN	parkIUCN	k?
kategorie	kategorie	k1gFnPc4
II	II	kA
(	(	kIx(
<g/>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
TriglavZákladní	TriglavZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1961	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
839,81	839,81	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Správa	správa	k1gFnSc1
</s>
<s>
Javni	Javn	k1gMnPc1
zavod	zavoda	k1gFnPc2
Triglavski	Triglavski	k1gNnSc2
narodni	narodnit	k5eAaPmRp2nS
park	park	k1gInSc1
<g/>
,	,	kIx,
Bled	Bled	k1gInSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
Slovinsko	Slovinsko	k1gNnSc1
Občiny	občina	k1gFnSc2
</s>
<s>
Bled	Bled	k1gMnSc1
<g/>
,	,	kIx,
Bohinj	Bohinj	k1gMnSc1
<g/>
,	,	kIx,
Bovec	Bovec	k1gMnSc1
<g/>
,	,	kIx,
Gorje	Gorje	k1gFnSc1
<g/>
,	,	kIx,
Jesenice	Jesenice	k1gFnSc1
<g/>
,	,	kIx,
Kobarid	Kobarid	k1gInSc1
<g/>
,	,	kIx,
Kranjska	Kranjska	k1gFnSc1
Gora	Gora	k1gFnSc1
<g/>
,	,	kIx,
Tolmin	Tolmin	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
TNP	TNP	kA
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
www.tnp.si	www.tnp.se	k1gFnSc4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Triglavský	Triglavský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
Triglavski	Triglavski	k1gNnSc1
narodni	narodnit	k5eAaPmRp2nS
park	park	k1gInSc1
<g/>
;	;	kIx,
TNP	TNP	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
,	,	kIx,
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
severozápadě	severozápad	k1gInSc6
republiky	republika	k1gFnSc2
v	v	k7c6
obvodu	obvod	k1gInSc6
osmi	osm	k4xCc2
občin	občina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
počátky	počátek	k1gInPc1
sahají	sahat	k5eAaImIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
takový	takový	k3xDgInSc1
byl	být	k5eAaImAgInS
ustaven	ustavit	k5eAaPmNgInS
v	v	k7c6
létě	léto	k1gNnSc6
1961	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
současnost	současnost	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
z	z	k7c2
období	období	k1gNnSc2
let	let	k1gInSc4
1906	#num#	k4
až	až	k9
1908	#num#	k4
pochází	pocházet	k5eAaImIp3nS
první	první	k4xOgInSc1
návrh	návrh	k1gInSc1
prof.	prof.	kA
Albina	Albin	k1gMnSc2
Belara	Belar	k1gMnSc2
na	na	k7c4
park	park	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
tento	tento	k3xDgInSc1
nebyl	být	k5eNaImAgInS
realizován	realizovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
údolí	údolí	k1gNnSc6
triglavských	triglavský	k2eAgNnPc2d1
jezer	jezero	k1gNnPc2
Alpský	alpský	k2eAgInSc1d1
ochranný	ochranný	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
Alpski	Alpsk	k1gMnPc1
varstveni	varstven	k2eAgMnPc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1961	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Lidová	lidový	k2eAgFnSc1d1
skupščina	skupščin	k2eAgFnSc1d1
Lidové	lidový	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
Slovinsko	Slovinsko	k1gNnSc1
(	(	kIx(
<g/>
LS	LS	kA
LRS	LRS	kA
<g/>
)	)	kIx)
usnesení	usnesení	k1gNnSc1
o	o	k7c4
vyhlášení	vyhlášení	k1gNnSc4
Doliny	dolina	k1gFnSc2
sedmi	sedm	k4xCc2
jezer	jezero	k1gNnPc2
za	za	k7c4
národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Triglavský	Triglavský	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usnesení	usnesení	k1gNnSc1
bylo	být	k5eAaImAgNnS
publikováno	publikovat	k5eAaBmNgNnS
ve	v	k7c6
slovinském	slovinský	k2eAgInSc6d1
promulgačním	promulgační	k2eAgInSc6d1
listu	list	k1gInSc6
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1961	#num#	k4
a	a	k8xC
účinnosti	účinnost	k1gFnSc2
nabylo	nabýt	k5eAaPmAgNnS
osmého	osmý	k4xOgInSc2
dne	den	k1gInSc2
od	od	k7c2
vyhlášení	vyhlášení	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1961	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnPc2
usnesení	usnesení	k1gNnSc2
LS	LS	kA
LRS	LRS	kA
bylo	být	k5eAaImAgNnS
i	i	k8xC
vymezení	vymezení	k1gNnSc1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Současnou	současný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
parku	park	k1gInSc2
zahrnující	zahrnující	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
východních	východní	k2eAgFnPc2d1
Julských	Julský	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
vymezil	vymezit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
všechny	všechen	k3xTgInPc4
dílčí	dílčí	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
TNP	TNP	kA
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zrušeny	zrušit	k5eAaPmNgFnP
přijetím	přijetí	k1gNnSc7
nového	nový	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
Triglavském	Triglavský	k2eAgInSc6d1
národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
jsou	být	k5eAaImIp3nP
Julské	Julský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
a	a	k8xC
TNP	TNP	kA
součástí	součást	k1gFnSc7
světové	světový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
biosférických	biosférický	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příroda	příroda	k1gFnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
TNP	TNP	kA
převládá	převládat	k5eAaImIp3nS
vysokohorský	vysokohorský	k2eAgInSc1d1
kras	kras	k1gInSc1
<g/>
,	,	kIx,
flora	flora	k6eAd1
je	být	k5eAaImIp3nS
alpská	alpský	k2eAgNnPc4d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
parku	park	k1gInSc2
zjevný	zjevný	k2eAgInSc4d1
i	i	k9
vliv	vliv	k1gInSc4
jadranského	jadranský	k2eAgNnSc2d1
a	a	k8xC
středomořského	středomořský	k2eAgNnSc2d1
ovzduší	ovzduší	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc1
Triglav	Triglav	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
864	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
jenž	jenž	k3xRgMnSc1
dal	dát	k5eAaPmAgInS
parku	park	k1gInSc6
jméno	jméno	k1gNnSc4
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
pomyslném	pomyslný	k2eAgInSc6d1
středu	střed	k1gInSc6
<g/>
,	,	kIx,
nejnižším	nízký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
Tolminka	Tolminka	k1gFnSc1
(	(	kIx(
<g/>
180	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
parku	park	k1gInSc6
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
vyskytují	vyskytovat	k5eAaImIp3nP
jeleni	jelen	k1gMnPc1
<g/>
,	,	kIx,
srnci	srnec	k1gMnPc1
<g/>
,	,	kIx,
kamzíci	kamzík	k1gMnPc1
<g/>
,	,	kIx,
kozorozi	kozoroh	k1gMnPc1
<g/>
,	,	kIx,
medvědi	medvěd	k1gMnPc1
a	a	k8xC
vlci	vlk	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Správa	správa	k1gFnSc1
parku	park	k1gInSc2
</s>
<s>
TNP	TNP	kA
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
pod	pod	k7c4
slovinské	slovinský	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
zemědělství	zemědělství	k1gNnSc2
a	a	k8xC
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Bledu	Bled	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
zaměstnanců	zaměstnanec	k1gMnPc2
TNP	TNP	kA
je	být	k5eAaImIp3nS
64	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
se	se	k3xPyFc4
o	o	k7c4
dohled	dohled	k1gInSc4
nad	nad	k7c7
územím	území	k1gNnSc7
stará	starat	k5eAaImIp3nS
dvacet	dvacet	k4xCc1
strážců	strážce	k1gMnPc2
a	a	k8xC
na	na	k7c4
zvěř	zvěř	k1gFnSc4
a	a	k8xC
regulaci	regulace	k1gFnSc4
lovu	lov	k1gInSc2
dohlíží	dohlížet	k5eAaImIp3nS
čtrnáct	čtrnáct	k4xCc1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
TNP	TNP	kA
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
tří	tři	k4xCgFnPc2
oblastí	oblast	k1gFnPc2
(	(	kIx(
<g/>
I.	I.	kA
ochranná	ochranný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
:	:	kIx,
314,87	314,87	k4
km²	km²	k?
<g/>
;	;	kIx,
II	II	kA
<g/>
.	.	kIx.
ochranná	ochranný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
:	:	kIx,
324,12	324,12	k4
km²	km²	k?
<g/>
;	;	kIx,
III	III	kA
<g/>
.	.	kIx.
ochranná	ochranný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
:	:	kIx,
200,82	200,82	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jen	jen	k9
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
a	a	k8xC
třetí	třetí	k4xOgFnSc2
ochranné	ochranný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
povolena	povolit	k5eAaPmNgFnS
těžba	těžba	k1gFnSc1
dřeva	dřevo	k1gNnSc2
a	a	k8xC
lov	lov	k1gInSc1
zvěře	zvěř	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
první	první	k4xOgFnSc6
ochranné	ochranný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
lov	lov	k1gInSc1
možný	možný	k2eAgInSc1d1
pouze	pouze	k6eAd1
ve	v	k7c6
zvláštních	zvláštní	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
PISKERNIK	PISKERNIK	kA
<g/>
,	,	kIx,
Angela	Angela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zgodovina	Zgodovina	k1gFnSc1
prizadevanj	prizadevanj	k1gFnSc1
za	za	k7c4
ustanovitev	ustanovitev	k1gFnSc4
Triglavskega	Triglavskeg	k1gMnSc2
narodnega	narodneg	k1gMnSc2
parka	parek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varstvo	Varstvo	k1gNnSc1
narave	naravat	k5eAaPmIp3nS
<g/>
:	:	kIx,
revija	revija	k6eAd1
za	za	k7c4
teorijo	teorijo	k1gNnSc4
in	in	k?
prakso	praksa	k1gFnSc5
varstva	varstvo	k1gNnSc2
naravne	naravnout	k5eAaPmIp3nS
dediščine	dediščinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1962	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgFnSc1wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
<g/>
,	,	kIx,
s.	s.	k?
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Triglavski	Triglavski	k1gNnPc2
narodni	narodnit	k5eAaPmRp2nS
park	park	k1gInSc1
(	(	kIx(
<g/>
TNP	TNP	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
C2012	C2012	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
NĚMCOVÁ	Němcová	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkání	setkání	k1gNnSc1
se	s	k7c7
Slovinskou	slovinský	k2eAgFnSc7d1
inspekcí	inspekce	k1gFnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
:	:	kIx,
Zpráva	zpráva	k1gFnSc1
ze	z	k7c2
zahraniční	zahraniční	k2eAgFnSc2d1
služební	služební	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-10-19	2011-10-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zakon	Zakon	k1gInSc1
o	o	k7c4
Triglavskem	Triglavsko	k1gNnSc7
narodnem	narodn	k1gInSc7
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
Uradni	Uradni	k1gFnSc1
list	list	k1gInSc1
RS	RS	kA
<g/>
,	,	kIx,
št	št	k?
<g/>
.	.	kIx.
52	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
z	z	k7c2
dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UNESCO	Unesco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biosphere	Biospher	k1gInSc5
Reserve	Reserev	k1gFnPc4
Information	Information	k1gInSc1
<g/>
:	:	kIx,
Slovenia	Slovenium	k1gNnSc2
<g/>
:	:	kIx,
Julian	Julian	k1gMnSc1
Alps	Alpsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-05-02	2005-05-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Triglav	Triglav	k1gMnSc1
</s>
<s>
Bohinjské	Bohinjský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Triglavský	Triglavský	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Triglavski	Triglavski	k1gNnSc7
narodni	narodnit	k5eAaPmRp2nS
park	park	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Příroda	příroda	k1gFnSc1
|	|	kIx~
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
1083499	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4948843-0	4948843-0	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315526935	#num#	k4
</s>
