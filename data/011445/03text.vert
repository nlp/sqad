<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Hamilton	Hamilton	k1gInSc1	Hamilton
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1755	[number]	k4	1755
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Otců	otec	k1gMnPc2	otec
zakladatelů	zakladatel	k1gMnPc2	zakladatel
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
==	==	k?	==
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gInSc2	Georg
Washington	Washington	k1gInSc1	Washington
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
Hamiltona	Hamilton	k1gMnSc4	Hamilton
prvním	první	k4xOgMnSc6	první
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
USA	USA	kA	USA
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
zůstal	zůstat	k5eAaPmAgInS	zůstat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
ledna	leden	k1gInSc2	leden
1795	[number]	k4	1795
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
vládních	vládní	k2eAgFnPc2d1	vládní
struktur	struktura	k1gFnPc2	struktura
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
během	během	k7c2	během
právě	právě	k6eAd1	právě
těchto	tento	k3xDgInPc2	tento
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
skladbou	skladba	k1gFnSc7	skladba
a	a	k8xC	a
funkcí	funkce	k1gFnSc7	funkce
samotného	samotný	k2eAgInSc2d1	samotný
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Forrest	Forrest	k1gMnSc1	Forrest
McDonald	McDonald	k1gMnSc1	McDonald
Hamiltonona	Hamiltonon	k1gMnSc4	Hamiltonon
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
přitom	přitom	k6eAd1	přitom
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
britského	britský	k2eAgMnSc2d1	britský
strážce	strážce	k1gMnSc2	strážce
státního	státní	k2eAgInSc2d1	státní
pokladu	poklad	k1gInSc2	poklad
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
britský	britský	k2eAgInSc1d1	britský
úřad	úřad	k1gInSc1	úřad
premiéra	premiér	k1gMnSc2	premiér
<g/>
;	;	kIx,	;
Hamilton	Hamilton	k1gInSc1	Hamilton
měl	mít	k5eAaImAgInS	mít
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
George	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Washington	Washington	k1gInSc4	Washington
si	se	k3xPyFc3	se
cenil	cenit	k5eAaImAgMnS	cenit
Hamiltonových	Hamiltonový	k2eAgFnPc2d1	Hamiltonová
rad	rada	k1gFnPc2	rada
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgInS	podporovat
jeho	jeho	k3xOp3gNnSc4	jeho
zasahování	zasahování	k1gNnSc4	zasahování
i	i	k9	i
mimo	mimo	k7c4	mimo
samotný	samotný	k2eAgInSc4d1	samotný
rezort	rezort	k1gInSc4	rezort
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hamilton	Hamilton	k1gInSc1	Hamilton
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
podal	podat	k5eAaPmAgMnS	podat
Kongresu	kongres	k1gInSc2	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
pět	pět	k4xCc4	pět
významných	významný	k2eAgFnPc2d1	významná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
veřejném	veřejný	k2eAgInSc6d1	veřejný
úvěru	úvěr	k1gInSc6	úvěr
(	(	kIx(	(
<g/>
First	First	k1gFnSc1	First
Report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Public	publicum	k1gNnPc2	publicum
Credit	Credit	k1gFnSc1	Credit
<g/>
)	)	kIx)	)
podaná	podaný	k2eAgFnSc1d1	podaná
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
ponechání	ponechání	k1gNnSc4	ponechání
daní	daň	k1gFnPc2	daň
z	z	k7c2	z
importu	import	k1gInSc2	import
(	(	kIx(	(
<g/>
Operations	Operations	k1gInSc1	Operations
of	of	k?	of
the	the	k?	the
Act	Act	k1gFnSc1	Act
Laying	Laying	k1gInSc1	Laying
Duties	Duties	k1gMnSc1	Duties
on	on	k3xPp3gMnSc1	on
Imports	Importsa	k1gFnPc2	Importsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednesená	přednesený	k2eAgFnSc1d1	přednesená
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
veřejném	veřejný	k2eAgInSc6d1	veřejný
úvěru	úvěr	k1gInSc6	úvěr
<g/>
:	:	kIx,	:
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Národní	národní	k2eAgFnSc6d1	národní
bance	banka	k1gFnSc6	banka
(	(	kIx(	(
<g/>
Second	Second	k1gInSc1	Second
Report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
Public	publicum	k1gNnPc2	publicum
Credit	Credit	k1gFnSc1	Credit
<g/>
:	:	kIx,	:
Report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
National	National	k1gFnPc6	National
Bank	bank	k1gInSc1	bank
<g/>
)	)	kIx)	)
podaná	podaný	k2eAgFnSc1d1	podaná
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
ražby	ražba	k1gFnSc2	ražba
mincí	mince	k1gFnPc2	mince
(	(	kIx(	(
<g/>
Report	report	k1gInSc1	report
on	on	k3xPp3gInSc1	on
the	the	k?	the
Establishment	establishment	k1gInSc1	establishment
of	of	k?	of
a	a	k8xC	a
Mint	Minta	k1gFnPc2	Minta
<g/>
)	)	kIx)	)
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zprava	zprava	k6eAd1	zprava
o	o	k7c6	o
výrobcích	výrobek	k1gInPc6	výrobek
(	(	kIx(	(
<g/>
Report	report	k1gInSc4	report
on	on	k3xPp3gMnSc1	on
Manufactures	Manufactures	k1gMnSc1	Manufactures
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podaná	podaný	k2eAgFnSc1d1	podaná
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
postoje	postoj	k1gInPc1	postoj
a	a	k8xC	a
politický	politický	k2eAgInSc1d1	politický
odkaz	odkaz	k1gInSc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
Federálních	federální	k2eAgInPc2d1	federální
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Madisonem	Madison	k1gMnSc7	Madison
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Jayem	Jay	k1gMnSc7	Jay
<g/>
)	)	kIx)	)
sloužících	sloužící	k2eAgNnPc2d1	sloužící
k	k	k7c3	k
volnější	volný	k2eAgFnSc3d2	volnější
interpretaci	interpretace	k1gFnSc3	interpretace
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
budoucích	budoucí	k2eAgInPc2d1	budoucí
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
razil	razit	k5eAaImAgMnS	razit
koncepci	koncepce	k1gFnSc4	koncepce
převzetí	převzetí	k1gNnSc2	převzetí
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
USA	USA	kA	USA
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc4	vytvoření
společného	společný	k2eAgInSc2d1	společný
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
George	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
také	také	k6eAd1	také
ustavení	ustavení	k1gNnSc4	ustavení
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
USA	USA	kA	USA
(	(	kIx(	(
<g/>
Bank	bank	k1gInSc1	bank
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
kroky	krok	k1gInPc4	krok
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
řadami	řada	k1gFnPc7	řada
formující	formující	k2eAgMnSc1d1	formující
se	se	k3xPyFc4	se
opozice	opozice	k1gFnSc2	opozice
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
Jeffersonem	Jefferson	k1gMnSc7	Jefferson
a	a	k8xC	a
Jamesem	James	k1gMnSc7	James
Madisonem	Madison	k1gMnSc7	Madison
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
tuto	tento	k3xDgFnSc4	tento
snahu	snaha	k1gFnSc4	snaha
sjednocení	sjednocení	k1gNnSc2	sjednocení
a	a	k8xC	a
kontroly	kontrola	k1gFnSc2	kontrola
finanční	finanční	k2eAgFnSc2d1	finanční
politiky	politika	k1gFnSc2	politika
ztotožňovali	ztotožňovat	k5eAaImAgMnP	ztotožňovat
s	s	k7c7	s
předešlou	předešlý	k2eAgFnSc7d1	předešlá
koloniální	koloniální	k2eAgFnSc7d1	koloniální
britskou	britský	k2eAgFnSc7d1	britská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Hamilton	Hamilton	k1gInSc1	Hamilton
se	se	k3xPyFc4	se
znelíbil	znelíbit	k5eAaPmAgInS	znelíbit
též	též	k6eAd1	též
politickou	politický	k2eAgFnSc7d1	politická
odměřeností	odměřenost	k1gFnSc7	odměřenost
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
choval	chovat	k5eAaImAgMnS	chovat
k	k	k7c3	k
událostem	událost	k1gFnPc3	událost
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
sympatizovala	sympatizovat	k5eAaImAgFnS	sympatizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
založil	založit	k5eAaPmAgMnS	založit
Federalistickou	federalistický	k2eAgFnSc4d1	federalistická
stranu	strana	k1gFnSc4	strana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
předchůdkyni	předchůdkyně	k1gFnSc4	předchůdkyně
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
ve	v	k7c6	v
střeleckém	střelecký	k2eAgInSc6d1	střelecký
duelu	duel	k1gInSc6	duel
s	s	k7c7	s
Aaronem	Aaron	k1gMnSc7	Aaron
Burrem	Burr	k1gMnSc7	Burr
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
politickým	politický	k2eAgMnSc7d1	politický
rivalem	rival	k1gMnSc7	rival
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Weehawkenu	Weehawkeno	k1gNnSc6	Weehawkeno
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
J.	J.	kA	J.
Ellis	Ellis	k1gFnSc1	Ellis
-	-	kIx~	-
Jeho	jeho	k3xOp3gFnSc1	jeho
Excelence	Excelence	k1gFnSc1	Excelence
George	George	k1gInSc4	George
Washington	Washington	k1gInSc1	Washington
ISBN	ISBN	kA	ISBN
80-7341-741-3	[number]	k4	80-7341-741-3
</s>
</p>
