<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Kobylka	Kobylka	k1gMnSc1	Kobylka
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1909	[number]	k4	1909
Lišov	Lišov	k1gInSc1	Lišov
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
národně	národně	k6eAd1	národně
socialistické	socialistický	k2eAgNnSc1d1	socialistické
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
oficiálně	oficiálně	k6eAd1	oficiálně
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
socialistická	socialistický	k2eAgFnSc1d1	socialistická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
poslancem	poslanec	k1gMnSc7	poslanec
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
a	a	k8xC	a
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
frakci	frakce	k1gFnSc3	frakce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
převzala	převzít	k5eAaPmAgFnS	převzít
moc	moc	k1gFnSc1	moc
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
podřídila	podřídit	k5eAaPmAgFnS	podřídit
se	se	k3xPyFc4	se
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
lišovského	lišovského	k2eAgFnSc6d1	lišovského
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
rudolfovského	rudolfovský	k1gMnSc2	rudolfovský
<g/>
,	,	kIx,	,
truhláře	truhlář	k1gMnSc2	truhlář
Štěpána	Štěpán	k1gMnSc2	Štěpán
Kobylky	Kobylka	k1gMnSc2	Kobylka
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
Kobilková	Kobilková	k1gFnSc1	Kobilková
z	z	k7c2	z
Lišova	Lišov	k1gInSc2	Lišov
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Petra	Petr	k1gMnSc2	Petr
Kobilky	Kobilka	k1gFnSc2	Kobilka
a	a	k8xC	a
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Krůčkové	Krůček	k1gMnPc1	Krůček
<g/>
)	)	kIx)	)
a	a	k8xC	a
Františky	Františka	k1gFnPc1	Františka
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Soudkové	soudkový	k2eAgNnSc1d1	soudkové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
si	se	k3xPyFc3	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
změnila	změnit	k5eAaPmAgFnS	změnit
příjmení	příjmení	k1gNnSc4	příjmení
z	z	k7c2	z
Kobilka	Kobilka	k1gFnSc1	Kobilka
na	na	k7c6	na
Kobylka	Kobylka	k1gMnSc1	Kobylka
(	(	kIx(	(
<g/>
povoleno	povolit	k5eAaPmNgNnS	povolit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
zemského	zemský	k2eAgMnSc2d1	zemský
prezidenta	prezident	k1gMnSc2	prezident
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
Kobylka	Kobylka	k1gMnSc1	Kobylka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1910	[number]	k4	1910
Lišov	Lišovo	k1gNnPc2	Lišovo
+	+	kIx~	+
1967	[number]	k4	1967
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
hostinský	hostinský	k1gMnSc1	hostinský
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Americe	Amerika	k1gFnSc6	Amerika
<g/>
"	"	kIx"	"
v	v	k7c6	v
Rudolfově	Rudolfův	k2eAgNnSc6d1	Rudolfovo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
politicky	politicky	k6eAd1	politicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
národně	národně	k6eAd1	národně
socialistické	socialistický	k2eAgFnSc6d1	socialistická
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Kobylka	Kobylka	k1gMnSc1	Kobylka
vychodil	vychodit	k5eAaImAgMnS	vychodit
čtyřletou	čtyřletý	k2eAgFnSc4d1	čtyřletá
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pojišťovacím	pojišťovací	k2eAgMnSc7d1	pojišťovací
agentem	agent	k1gMnSc7	agent
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
píli	píle	k1gFnSc4	píle
a	a	k8xC	a
ctižádost	ctižádost	k1gFnSc4	ctižádost
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
nejmladšího	mladý	k2eAgMnSc2d3	nejmladší
ředitele	ředitel	k1gMnSc2	ředitel
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
mládežnické	mládežnický	k2eAgFnSc6d1	mládežnická
organizaci	organizace	k1gFnSc6	organizace
národních	národní	k2eAgInPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
vize	vize	k1gFnPc4	vize
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
pojišťováka	pojišťovák	k1gMnSc2	pojišťovák
shrnul	shrnout	k5eAaPmAgMnS	shrnout
do	do	k7c2	do
autorské	autorský	k2eAgFnSc2d1	autorská
knížky	knížka	k1gFnSc2	knížka
Apoštol	apoštol	k1gMnSc1	apoštol
či	či	k8xC	či
agent	agent	k1gMnSc1	agent
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
krajského	krajský	k2eAgInSc2d1	krajský
výboru	výbor	k1gInSc2	výbor
mladých	mladý	k1gMnPc2	mladý
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934-1938	[number]	k4	1934-1938
místopředsedou	místopředseda	k1gMnSc7	místopředseda
celostátní	celostátní	k2eAgFnSc2d1	celostátní
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
organizace	organizace	k1gFnSc2	organizace
národních	národní	k2eAgInPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
Komárně	Komárno	k1gNnSc6	Komárno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkal	potkat	k5eAaPmAgInS	potkat
jako	jako	k8xS	jako
vězně	vězně	k6eAd1	vězně
i	i	k9	i
budoucího	budoucí	k2eAgMnSc2d1	budoucí
prezidenta	prezident	k1gMnSc2	prezident
G.	G.	kA	G.
Husáka	Husák	k1gMnSc2	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
politicky	politicky	k6eAd1	politicky
angažoval	angažovat	k5eAaBmAgInS	angažovat
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
krajským	krajský	k2eAgMnSc7d1	krajský
tajemníkem	tajemník	k1gMnSc7	tajemník
národních	národní	k2eAgMnPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
a	a	k8xC	a
náměstkem	náměstek	k1gMnSc7	náměstek
předsedy	předseda	k1gMnSc2	předseda
Místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945-1946	[number]	k4	1945-1946
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
národní	národní	k2eAgMnPc4d1	národní
socialisty	socialist	k1gMnPc4	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poslancem	poslanec	k1gMnSc7	poslanec
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
únorového	únorový	k2eAgInSc2d1	únorový
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
prokomunistické	prokomunistický	k2eAgFnSc2d1	prokomunistická
skupiny	skupina	k1gFnSc2	skupina
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
akčního	akční	k2eAgInSc2d1	akční
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přebíral	přebírat	k5eAaImAgMnS	přebírat
moc	moc	k6eAd1	moc
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
při	při	k7c6	při
přerodu	přerod	k1gInSc6	přerod
národně	národně	k6eAd1	národně
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
v	v	k7c4	v
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
prokomunistickým	prokomunistický	k2eAgInSc7d1	prokomunistický
postojem	postoj	k1gInSc7	postoj
během	během	k7c2	během
únorového	únorový	k2eAgInSc2d1	únorový
převratu	převrat	k1gInSc2	převrat
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovské	domovský	k2eAgFnSc6d1	domovská
olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
organizaci	organizace	k1gFnSc6	organizace
strany	strana	k1gFnSc2	strana
zdiskreditoval	zdiskreditovat	k5eAaPmAgMnS	zdiskreditovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
takticky	takticky	k6eAd1	takticky
nasazen	nasadit	k5eAaPmNgInS	nasadit
na	na	k7c4	na
kandidátní	kandidátní	k2eAgFnSc4d1	kandidátní
listinu	listina	k1gFnSc4	listina
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zde	zde	k6eAd1	zde
skutečně	skutečně	k6eAd1	skutečně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
držel	držet	k5eAaImAgInS	držet
do	do	k7c2	do
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
náhradnice	náhradnice	k1gFnSc1	náhradnice
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
Růžena	Růžena	k1gFnSc1	Růžena
Skřivanová	Skřivanová	k1gFnSc1	Skřivanová
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
mandátu	mandát	k1gInSc3	mandát
poslance	poslanec	k1gMnSc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Branná	branný	k2eAgFnSc1d1	Branná
a	a	k8xC	a
Žárová	žárový	k2eAgFnSc1d1	Žárová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
mládežník	mládežník	k1gMnSc1	mládežník
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
Sokolu	Sokol	k1gInSc6	Sokol
<g/>
,	,	kIx,	,
v	v	k7c6	v
divadelních	divadelní	k2eAgInPc6d1	divadelní
ochotnických	ochotnický	k2eAgInPc6d1	ochotnický
spolcích	spolek	k1gInPc6	spolek
a	a	k8xC	a
v	v	k7c6	v
Československé	československý	k2eAgFnSc3d1	Československá
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
Rady	rada	k1gFnSc2	rada
starších	starý	k2eAgMnPc2d2	starší
v	v	k7c6	v
náboženské	náboženský	k2eAgFnSc6d1	náboženská
obci	obec	k1gFnSc6	obec
v	v	k7c6	v
Rudolfově	Rudolfův	k2eAgFnSc6d1	Rudolfova
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
výstavbu	výstavba	k1gFnSc4	výstavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
místního	místní	k2eAgInSc2d1	místní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Kružíkovou	Kružíková	k1gFnSc4	Kružíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1909	[number]	k4	1909
Okříšky	Okříšek	k1gInPc4	Okříšek
+	+	kIx~	+
1975	[number]	k4	1975
Velké	velký	k2eAgFnSc2d1	velká
Losiny	losina	k1gFnSc2	losina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedli	rozvést	k5eAaPmAgMnP	rozvést
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Sytařovou	Sytařův	k2eAgFnSc7d1	Sytařův
(	(	kIx(	(
<g/>
*	*	kIx~	*
1909	[number]	k4	1909
+	+	kIx~	+
1985	[number]	k4	1985
Rudolfov	Rudolfovo	k1gNnPc2	Rudolfovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
měl	mít	k5eAaImAgMnS	mít
dceru	dcera	k1gFnSc4	dcera
Naděždu	Naděžda	k1gFnSc4	Naděžda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
+	+	kIx~	+
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
synové	syn	k1gMnPc1	syn
Dušan	Dušan	k1gMnSc1	Dušan
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Drahoše	Drahoš	k1gMnPc4	Drahoš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1935	[number]	k4	1935
Olomouc	Olomouc	k1gFnSc1	Olomouc
+	+	kIx~	+
10	[number]	k4	10
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Drahoš	Drahoš	k1gMnSc1	Drahoš
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Hana	Hana	k1gFnSc1	Hana
<g/>
)	)	kIx)	)
a	a	k8xC	a
Petra	Petra	k1gFnSc1	Petra
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1945	[number]	k4	1945
+	+	kIx~	+
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dožil	dožít	k5eAaPmAgInS	dožít
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
obou	dva	k4xCgFnPc6	dva
svých	svůj	k3xOyFgMnPc2	svůj
synů	syn	k1gMnPc2	syn
i	i	k8xC	i
úmrtí	úmrtí	k1gNnSc2	úmrtí
snachy	snacha	k1gFnSc2	snacha
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
Dolná	Dolná	k1gFnSc1	Dolná
Streda	Streda	k1gMnSc1	Streda
+	+	kIx~	+
1963	[number]	k4	1963
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
rozené	rozený	k2eAgFnSc2d1	rozená
Dubovské	Dubovská	k1gFnSc2	Dubovská
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnučky	vnučka	k1gFnSc2	vnučka
Hany	Hana	k1gFnSc2	Hana
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
Šternberk	Šternberk	k1gInSc1	Šternberk
+	+	kIx~	+
1963	[number]	k4	1963
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rodinná	rodinný	k2eAgFnSc1d1	rodinná
tragédie	tragédie	k1gFnSc1	tragédie
se	se	k3xPyFc4	se
podepsala	podepsat	k5eAaPmAgFnS	podepsat
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
zhoršení	zhoršení	k1gNnSc2	zhoršení
jeho	jeho	k3xOp3gInSc2	jeho
celkového	celkový	k2eAgInSc2d1	celkový
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
pak	pak	k6eAd1	pak
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
v	v	k7c6	v
Pohřební	pohřební	k2eAgFnSc6d1	pohřební
službě	služba	k1gFnSc6	služba
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Lišově	Lišov	k1gInSc6	Lišov
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
roky	rok	k1gInPc4	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jako	jako	k9	jako
důchodce	důchodce	k1gMnSc1	důchodce
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
vrátný	vrátný	k1gMnSc1	vrátný
v	v	k7c6	v
Jihočeských	jihočeský	k2eAgFnPc6d1	Jihočeská
drůbežárnách	drůbežárna	k1gFnPc6	drůbežárna
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
osobnost	osobnost	k1gFnSc4	osobnost
byl	být	k5eAaImAgMnS	být
Kobylka	Kobylka	k1gMnSc1	Kobylka
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
výrazný	výrazný	k2eAgMnSc1d1	výrazný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Uměl	umět	k5eAaImAgMnS	umět
upoutat	upoutat	k5eAaPmF	upoutat
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
jistou	jistý	k2eAgFnSc4d1	jistá
dávku	dávka	k1gFnSc4	dávka
empatie	empatie	k1gFnSc2	empatie
a	a	k8xC	a
ovládal	ovládat	k5eAaImAgInS	ovládat
řečnické	řečnický	k2eAgNnSc4d1	řečnické
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vypjatých	vypjatý	k2eAgFnPc6d1	vypjatá
situacích	situace	k1gFnPc6	situace
byl	být	k5eAaImAgInS	být
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
"	"	kIx"	"
<g/>
pruďas	pruďas	k1gMnSc1	pruďas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mírnilo	mírnit	k5eAaImAgNnS	mírnit
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
si	se	k3xPyFc3	se
zakládal	zakládat	k5eAaImAgInS	zakládat
na	na	k7c6	na
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
na	na	k7c6	na
budování	budování	k1gNnSc6	budování
rodinných	rodinný	k2eAgInPc2d1	rodinný
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Pěstoval	pěstovat	k5eAaImAgMnS	pěstovat
kladné	kladný	k2eAgInPc4d1	kladný
vztahy	vztah	k1gInPc4	vztah
i	i	k9	i
s	s	k7c7	s
příbuznými	příbuzný	k1gMnPc7	příbuzný
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
hledat	hledat	k5eAaImF	hledat
kompromisy	kompromis	k1gInPc4	kompromis
jak	jak	k8xC	jak
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
typ	typ	k1gInSc1	typ
lidového	lidový	k2eAgMnSc2d1	lidový
"	"	kIx"	"
<g/>
písmáka	písmák	k1gMnSc2	písmák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sečtělého	sečtělý	k2eAgMnSc4d1	sečtělý
<g/>
,	,	kIx,	,
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
jeho	jeho	k3xOp3gNnSc4	jeho
sobotní	sobotní	k2eAgNnSc4d1	sobotní
dopoledne	dopoledne	k1gNnSc4	dopoledne
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zavíral	zavírat	k5eAaImAgMnS	zavírat
do	do	k7c2	do
pracovny	pracovna	k1gFnSc2	pracovna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nikým	nikdo	k3yNnSc7	nikdo
nerušen	rušen	k2eNgMnSc1d1	nerušen
mohl	moct	k5eAaImAgMnS	moct
korespondovat	korespondovat	k5eAaImF	korespondovat
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
známými	známý	k1gMnPc7	známý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
mottem	motto	k1gNnSc7	motto
bylo	být	k5eAaImAgNnS	být
masarykovské	masarykovský	k2eAgNnSc1d1	masarykovské
Nebát	bát	k5eNaImF	bát
se	se	k3xPyFc4	se
a	a	k8xC	a
nekrást	krást	k5eNaImF	krást
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
pedagogické	pedagogický	k2eAgFnPc1d1	pedagogická
Pravdomluvnost	pravdomluvnost	k1gFnSc1	pravdomluvnost
Kázeň	kázeň	k1gFnSc1	kázeň
Pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Kobylka	Kobylka	k1gMnSc1	Kobylka
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
