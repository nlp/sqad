<s>
Hrách	hrách	k1gInSc1	hrách
setý	setý	k2eAgInSc1d1	setý
(	(	kIx(	(
<g/>
Pisum	Pisum	k1gInSc1	Pisum
sativum	sativum	k1gInSc1	sativum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významná	významný	k2eAgFnSc1d1	významná
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovitých	bobovitý	k2eAgFnPc2d1	bobovitá
(	(	kIx(	(
<g/>
Fabaceae	Fabaceae	k1gFnPc2	Fabaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrách	hrách	k1gInSc1	hrách
je	být	k5eAaImIp3nS	být
jednoletý	jednoletý	k2eAgMnSc1d1	jednoletý
<g/>
,	,	kIx,	,
popínavý	popínavý	k2eAgMnSc1d1	popínavý
<g/>
,	,	kIx,	,
se	s	k7c7	s
sbíhavými	sbíhavý	k2eAgInPc7d1	sbíhavý
a	a	k8xC	a
prorostlými	prorostlý	k2eAgInPc7d1	prorostlý
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
lusky	lusk	k1gInPc4	lusk
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dužnatá	dužnatý	k2eAgNnPc1d1	dužnaté
semena	semeno	k1gNnPc1	semeno
zvaná	zvaný	k2eAgNnPc1d1	zvané
hrášky	hrášek	k1gInPc7	hrášek
<g/>
.	.	kIx.	.
</s>
<s>
Nezralé	zralý	k2eNgInPc1d1	nezralý
hrášky	hrášek	k1gInPc1	hrášek
či	či	k8xC	či
celé	celý	k2eAgInPc1d1	celý
lusky	lusk	k1gInPc1	lusk
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xS	jako
zelenina	zelenina	k1gFnSc1	zelenina
(	(	kIx(	(
<g/>
hrášek	hrášek	k1gInSc1	hrášek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zralá	zralý	k2eAgNnPc1d1	zralé
semena	semeno	k1gNnPc1	semeno
(	(	kIx(	(
<g/>
hrách	hra	k1gFnPc6	hra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xC	jako
luštěnina	luštěnina	k1gFnSc1	luštěnina
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
občas	občas	k6eAd1	občas
zplaňuje	zplaňovat	k5eAaImIp3nS	zplaňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Hrách	hrách	k1gInSc1	hrách
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
luštěnina	luštěnina	k1gFnSc1	luštěnina
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
chutným	chutný	k2eAgInPc3d1	chutný
plodům	plod	k1gInPc3	plod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vitamíny	vitamín	k1gInPc4	vitamín
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
skupiny	skupina	k1gFnPc4	skupina
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
minerální	minerální	k2eAgFnPc4d1	minerální
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
fosfor	fosfor	k1gInSc4	fosfor
a	a	k8xC	a
draslík	draslík	k1gInSc4	draslík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vápník	vápník	k1gInSc4	vápník
a	a	k8xC	a
hořčík	hořčík	k1gInSc4	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Hrách	hrách	k1gInSc1	hrách
má	mít	k5eAaImIp3nS	mít
hypogeické	hypogeický	k2eAgNnSc4d1	hypogeický
klíčení	klíčení	k1gNnSc4	klíčení
(	(	kIx(	(
<g/>
dělohy	děloha	k1gFnSc2	děloha
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
hlubší	hluboký	k2eAgInSc4d2	hlubší
výsev	výsev	k1gInSc4	výsev
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
lodyha	lodyha	k1gFnSc1	lodyha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poléhavý	poléhavý	k2eAgInSc4d1	poléhavý
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
až	až	k6eAd1	až
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dělen	dělit	k5eAaImNgMnS	dělit
uzlinami	uzlina	k1gFnPc7	uzlina
(	(	kIx(	(
<g/>
nody	noda	k1gFnPc1	noda
<g/>
)	)	kIx)	)
na	na	k7c4	na
články	článek	k1gInPc4	článek
(	(	kIx(	(
<g/>
internodium	internodium	k1gNnSc1	internodium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kořen	kořen	k1gInSc1	kořen
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
kůlový	kůlový	k2eAgInSc1d1	kůlový
a	a	k8xC	a
větvený	větvený	k2eAgInSc1d1	větvený
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
až	až	k9	až
1	[number]	k4	1
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
sudozpeřený	sudozpeřený	k2eAgInSc1d1	sudozpeřený
s	s	k7c7	s
úponky	úponek	k1gInPc7	úponek
a	a	k8xC	a
palisty	palist	k1gInPc7	palist
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
řapíků	řapík	k1gInPc2	řapík
(	(	kIx(	(
<g/>
odrůdy	odrůda	k1gFnPc1	odrůda
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
lichozpeřené	lichozpeřený	k2eAgFnPc1d1	lichozpeřený
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
lístkové	lístkový	k2eAgFnPc1d1	lístková
a	a	k8xC	a
často	často	k6eAd1	často
bezlístkové	bezlístkový	k2eAgInPc4d1	bezlístkový
=	=	kIx~	=
lístky	lístek	k1gInPc4	lístek
přeměněné	přeměněný	k2eAgInPc4d1	přeměněný
v	v	k7c4	v
úponky	úponek	k1gInPc4	úponek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
hrozen	hrozen	k1gInSc4	hrozen
v	v	k7c6	v
úžlabí	úžlabí	k1gNnSc6	úžlabí
řapíků	řapík	k1gInPc2	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
je	být	k5eAaImIp3nS	být
pětičetný	pětičetný	k2eAgInSc1d1	pětičetný
-	-	kIx~	-
srostlý	srostlý	k2eAgInSc1d1	srostlý
zelený	zelený	k2eAgInSc1d1	zelený
kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
koruna	koruna	k1gFnSc1	koruna
=	=	kIx~	=
pavéza	pavéza	k1gFnSc1	pavéza
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
člunek	člunek	k1gInSc1	člunek
(	(	kIx(	(
<g/>
srostlý	srostlý	k2eAgMnSc1d1	srostlý
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
lístků	lístek	k1gInPc2	lístek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deset	deset	k4xCc4	deset
tyčinek	tyčinka	k1gFnPc2	tyčinka
=	=	kIx~	=
dvoubratré	dvoubratrý	k2eAgFnSc2d1	dvoubratrý
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
semeník	semeník	k1gInSc1	semeník
svrchní	svrchní	k2eAgInSc1d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
postupné	postupný	k2eAgNnSc1d1	postupné
kvetení	kvetení	k1gNnSc1	kvetení
=	=	kIx~	=
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
dozrávání	dozrávání	k1gNnSc1	dozrávání
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
lusk	lusk	k1gInSc1	lusk
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
chlopně	chlopeň	k1gFnPc1	chlopeň
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
přirůstají	přirůstat	k5eAaImIp3nP	přirůstat
k	k	k7c3	k
hřbetní	hřbetní	k2eAgFnSc3d1	hřbetní
straně	strana	k1gFnSc3	strana
lusku	lusk	k1gInSc2	lusk
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
poutkem	poutko	k1gNnSc7	poutko
(	(	kIx(	(
<g/>
pupek	pupek	k1gInSc1	pupek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semeno	semeno	k1gNnSc1	semeno
má	mít	k5eAaImIp3nS	mít
průsvitné	průsvitný	k2eAgNnSc1d1	průsvitné
osemení	osemení	k1gNnSc1	osemení
-	-	kIx~	-
dělohy	děloha	k1gFnPc1	děloha
se	s	k7c7	s
zásobními	zásobní	k2eAgFnPc7d1	zásobní
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
22	[number]	k4	22
%	%	kIx~	%
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
škrobu	škrob	k1gInSc2	škrob
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
děloha	děloha	k1gFnSc1	děloha
má	mít	k5eAaImIp3nS	mít
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Hrách	hrách	k1gInSc1	hrách
setý	setý	k2eAgInSc1d1	setý
pravý	pravý	k2eAgMnSc1d1	pravý
zralé	zralý	k2eAgNnSc4d1	zralé
semeno	semeno	k1gNnSc4	semeno
=	=	kIx~	=
polní	polní	k2eAgInPc4d1	polní
hrachy	hrách	k1gInPc4	hrách
zelené	zelený	k2eAgNnSc4d1	zelené
semeno	semeno	k1gNnSc4	semeno
=	=	kIx~	=
zahradní	zahradní	k2eAgInPc4d1	zahradní
hrachy	hrách	k1gInPc4	hrách
hrách	hrách	k1gInSc1	hrách
dřeňový	dřeňový	k2eAgInSc1d1	dřeňový
-	-	kIx~	-
semena	semeno	k1gNnPc1	semeno
hrách	hrách	k1gInSc4	hrách
cukrový	cukrový	k2eAgInSc4d1	cukrový
-	-	kIx~	-
lusky	luska	k1gFnSc2	luska
Hrách	hrách	k1gInSc1	hrách
setý	setý	k2eAgInSc1d1	setý
rolní	rolní	k2eAgInSc1d1	rolní
=	=	kIx~	=
peluška	peluška	k1gFnSc1	peluška
Dělají	dělat	k5eAaImIp3nP	dělat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
směsky	směska	k1gFnPc4	směska
a	a	k8xC	a
senáž	senáž	k1gFnSc4	senáž
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
má	mít	k5eAaImIp3nS	mít
červenofialový	červenofialový	k2eAgInSc1d1	červenofialový
<g/>
,	,	kIx,	,
na	na	k7c6	na
palistech	palist	k1gInPc6	palist
má	mít	k5eAaImIp3nS	mít
antokyanovou	antokyanový	k2eAgFnSc4d1	antokyanový
skvrnu	skvrna	k1gFnSc4	skvrna
(	(	kIx(	(
<g/>
červenofialové	červenofialový	k2eAgNnSc1d1	červenofialové
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
listů	list	k1gInPc2	list
-	-	kIx~	-
vegetativní	vegetativní	k2eAgMnSc1d1	vegetativní
a	a	k8xC	a
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
květu	květ	k1gInSc2	květ
-	-	kIx~	-
generativní	generativní	k2eAgFnSc1d1	generativní
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
,	,	kIx,	,
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
do	do	k7c2	do
tmavě	tmavě	k6eAd1	tmavě
hnědofialové	hnědofialový	k2eAgFnSc2d1	hnědofialová
<g/>
.	.	kIx.	.
</s>
<s>
Neutrální	neutrální	k2eAgFnSc1d1	neutrální
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
pH	ph	kA	ph
6,5	[number]	k4	6,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
lehčí	lehký	k2eAgFnSc2d2	lehčí
půdy	půda	k1gFnSc2	půda
nezaplevelené	zaplevelený	k2eNgFnSc2d1	zaplevelený
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
sít	sít	k5eAaImF	sít
3	[number]	k4	3
km	km	kA	km
od	od	k7c2	od
pozemku	pozemek	k1gInSc2	pozemek
s	s	k7c7	s
hrachem	hrách	k1gInSc7	hrách
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
chorobám	choroba	k1gFnPc3	choroba
a	a	k8xC	a
škůdcům	škůdce	k1gMnPc3	škůdce
-	-	kIx~	-
obaleč	obaleč	k1gMnSc1	obaleč
hrachový	hrachový	k2eAgMnSc1d1	hrachový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
řádky	řádek	k1gInPc1	řádek
-	-	kIx~	-
12,5	[number]	k4	12,5
až	až	k9	až
15	[number]	k4	15
cm	cm	kA	cm
hloubka	hloubka	k1gFnSc1	hloubka
-	-	kIx~	-
5	[number]	k4	5
až	až	k9	až
8	[number]	k4	8
cm	cm	kA	cm
termín	termín	k1gInSc1	termín
-	-	kIx~	-
časný	časný	k2eAgMnSc1d1	časný
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
dekáda	dekáda	k1gFnSc1	dekáda
jarních	jarní	k2eAgFnPc2d1	jarní
prací	práce	k1gFnPc2	práce
<g/>
)	)	kIx)	)
výsevek	výsevek	k1gInSc1	výsevek
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
0,9	[number]	k4	0,9
až	až	k6eAd1	až
1,1	[number]	k4	1,1
MKS	MKS	kA	MKS
(	(	kIx(	(
<g/>
milion	milion	k4xCgInSc1	milion
klíčivých	klíčivý	k2eAgNnPc2d1	klíčivé
semen	semeno	k1gNnPc2	semeno
<g/>
)	)	kIx)	)
výnos	výnos	k1gInSc1	výnos
-	-	kIx~	-
průměrně	průměrně	k6eAd1	průměrně
2,5	[number]	k4	2,5
až	až	k9	až
3	[number]	k4	3
tuny	tuna	k1gFnPc4	tuna
na	na	k7c4	na
hektar	hektar	k1gInSc4	hektar
Ärtsoppa	Ärtsoppa	k1gFnSc1	Ärtsoppa
(	(	kIx(	(
<g/>
hrachová	hrachový	k2eAgFnSc1d1	hrachová
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
kuchyně	kuchyně	k1gFnSc1	kuchyně
Hrachová	hrachový	k2eAgFnSc1d1	hrachová
polévka	polévka	k1gFnSc1	polévka
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
)	)	kIx)	)
Hrachová	hrachový	k2eAgFnSc1d1	hrachová
kaše	kaše	k1gFnSc1	kaše
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
)	)	kIx)	)
Hrachové	hrachový	k2eAgFnPc1d1	hrachová
placky	placka	k1gFnPc1	placka
(	(	kIx(	(
<g/>
staročeská	staročeský	k2eAgFnSc1d1	staročeská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
)	)	kIx)	)
Hrachový	hrachový	k2eAgInSc1d1	hrachový
šoulet	šoulet	k1gInSc1	šoulet
(	(	kIx(	(
<g/>
židovská	židovský	k2eAgFnSc1d1	židovská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
)	)	kIx)	)
Mater	mater	k1gFnSc1	mater
paneer	paneer	k1gMnSc1	paneer
(	(	kIx(	(
<g/>
hrách	hrách	k1gInSc1	hrách
se	s	k7c7	s
sýrem	sýr	k1gInSc7	sýr
<g/>
)	)	kIx)	)
indická	indický	k2eAgFnSc1d1	indická
kuchyně	kuchyně	k1gFnSc1	kuchyně
házet	házet	k5eAaImF	házet
hrách	hrách	k1gInSc4	hrách
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
<g/>
,	,	kIx,	,
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
<g/>
:	:	kIx,	:
marně	marně	k6eAd1	marně
někoho	někdo	k3yInSc4	někdo
napomínat	napomínat	k5eAaImF	napomínat
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hrách	hra	k1gFnPc6	hra
setý	setý	k2eAgMnSc1d1	setý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
