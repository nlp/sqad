<p>
<s>
Moulin	moulin	k2eAgMnSc1d1	moulin
Rouge	rouge	k1gFnPc4	rouge
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Červený	červený	k2eAgInSc1d1	červený
mlýn	mlýn	k1gInSc1	mlýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
kabaret	kabaret	k1gInSc1	kabaret
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pigalle	Pigalle	k1gFnSc2	Pigalle
<g/>
,	,	kIx,	,
na	na	k7c6	na
Boulevardu	Boulevard	k1gInSc6	Boulevard
de	de	k?	de
Clichy	Clicha	k1gFnSc2	Clicha
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Montmartru	Montmartr	k1gInSc2	Montmartr
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Kabaret	kabaret	k1gInSc1	kabaret
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
zde	zde	k6eAd1	zde
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
La	la	k1gNnSc1	la
Goulue	Goulue	k1gInSc1	Goulue
<g/>
,	,	kIx,	,
Josephine	Josephin	k1gInSc5	Josephin
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatrum	k1gNnSc2	Sinatrum
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Gabin	Gabin	k1gMnSc1	Gabin
<g/>
,	,	kIx,	,
Yvette	Yvett	k1gInSc5	Yvett
Guilbert	Guilbert	k1gMnSc1	Guilbert
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Avril	Avril	k1gMnSc1	Avril
<g/>
,	,	kIx,	,
Mistinguett	Mistinguett	k1gMnSc1	Mistinguett
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Pujol	Pujol	k1gInSc1	Pujol
<g/>
,	,	kIx,	,
Édith	Édith	k1gInSc1	Édith
Piaf	Piaf	k1gFnSc1	Piaf
<g/>
,	,	kIx,	,
Ella	Ella	k1gFnSc1	Ella
Fitzgerald	Fitzgerald	k1gInSc1	Fitzgerald
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Toya	Toya	k1gMnSc1	Toya
Jackson	Jackson	k1gMnSc1	Jackson
aj.	aj.	kA	aj.
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
kabaretu	kabaret	k1gInSc6	kabaret
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
natočeno	natočit	k5eAaBmNgNnS	natočit
několik	několik	k4yIc1	několik
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgInPc2d1	filmový
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
literárních	literární	k2eAgNnPc6d1	literární
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
kabaretem	kabaret	k1gInSc7	kabaret
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
tanec	tanec	k1gInSc1	tanec
kankán	kankán	k1gInSc1	kankán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tanečnice	tanečnice	k1gFnSc1	tanečnice
zachytil	zachytit	k5eAaPmAgMnS	zachytit
častý	častý	k2eAgMnSc1d1	častý
návštěvník	návštěvník	k1gMnSc1	návštěvník
podniku	podnik	k1gInSc2	podnik
malíř	malíř	k1gMnSc1	malíř
Henri	Henr	k1gFnSc2	Henr
de	de	k?	de
Toulouse-Lautrec	Toulouse-Lautrec	k1gMnSc1	Toulouse-Lautrec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Moulin	moulin	k2eAgMnSc1d1	moulin
Rouge	rouge	k1gFnSc2	rouge
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
Joseph	Joseph	k1gMnSc1	Joseph
Oller	Oller	k1gMnSc1	Oller
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
podnik	podnik	k1gInSc1	podnik
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Zidler	Zidler	k1gMnSc1	Zidler
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Moulin	moulin	k2eAgMnSc1d1	moulin
Rouge	rouge	k1gFnSc3	rouge
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Harold	Harold	k1gMnSc1	Harold
Zidler	Zidler	k1gMnSc1	Zidler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1889	[number]	k4	1889
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
červený	červený	k2eAgInSc4d1	červený
mlýn	mlýn	k1gInSc4	mlýn
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
symbolika	symbolika	k1gFnSc1	symbolika
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
Montmartru	Montmartr	k1gInSc2	Montmartr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nacházelo	nacházet	k5eAaImAgNnS	nacházet
množství	množství	k1gNnSc1	množství
větrných	větrný	k2eAgInPc2d1	větrný
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
Moulin	moulin	k2eAgInSc1d1	moulin
Rouge	rouge	k1gFnPc2	rouge
využíván	využívat	k5eAaPmNgMnS	využívat
jako	jako	k9	jako
tančírna	tančírna	k1gFnSc1	tančírna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tanečnice	tanečnice	k1gFnPc1	tanečnice
předváděly	předvádět	k5eAaImAgFnP	předvádět
především	především	k6eAd1	především
cancan	cancan	k1gInSc4	cancan
a	a	k8xC	a
chahut	chahut	k1gInSc4	chahut
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
zde	zde	k6eAd1	zde
významné	významný	k2eAgFnPc1d1	významná
umělkyně	umělkyně	k1gFnPc1	umělkyně
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
jako	jako	k8xC	jako
byly	být	k5eAaImAgFnP	být
La	la	k1gNnSc4	la
Goulue	Goulu	k1gFnSc2	Goulu
<g/>
,	,	kIx,	,
Yvette	Yvett	k1gInSc5	Yvett
Guilbert	Guilbert	k1gMnSc1	Guilbert
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Avril	Avrila	k1gFnPc2	Avrila
nebo	nebo	k8xC	nebo
Mistinguett	Mistinguetta	k1gFnPc2	Mistinguetta
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
reklamní	reklamní	k2eAgInPc1d1	reklamní
plakáty	plakát	k1gInPc1	plakát
pro	pro	k7c4	pro
podnik	podnik	k1gInSc4	podnik
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Toulouse-Lautrec	Toulouse-Lautrec	k1gMnSc1	Toulouse-Lautrec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c4	v
Moulin	moulin	k2eAgInSc4d1	moulin
Rouge	rouge	k1gFnPc6	rouge
uváděly	uvádět	k5eAaImAgFnP	uvádět
operety	opereta	k1gFnPc1	opereta
a	a	k8xC	a
revue	revue	k1gFnPc1	revue
<g/>
,	,	kIx,	,
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
využíváno	využívat	k5eAaPmNgNnS	využívat
jako	jako	k9	jako
kino	kino	k1gNnSc1	kino
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
uváděny	uvádět	k5eAaImNgFnP	uvádět
tzv.	tzv.	kA	tzv.
dinner-spectacles	dinnerpectacles	k1gInSc1	dinner-spectacles
(	(	kIx(	(
<g/>
představení	představení	k1gNnSc1	představení
s	s	k7c7	s
večeří	večeře	k1gFnSc7	večeře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
také	také	k9	také
známí	známý	k2eAgMnPc1d1	známý
zpěváci	zpěvák	k1gMnPc1	zpěvák
šansonů	šanson	k1gInPc2	šanson
jako	jako	k8xS	jako
Charles	Charles	k1gMnSc1	Charles
Trenet	Trenet	k1gMnSc1	Trenet
nebo	nebo	k8xC	nebo
Charles	Charles	k1gMnSc1	Charles
Aznavour	Aznavour	k1gMnSc1	Aznavour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
jako	jako	k8xS	jako
atrakce	atrakce	k1gFnSc1	atrakce
instalováno	instalován	k2eAgNnSc4d1	instalováno
akvárium	akvárium	k1gNnSc4	akvárium
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
nahé	nahý	k2eAgFnPc1d1	nahá
tanečnice	tanečnice	k1gFnPc1	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
finanční	finanční	k2eAgFnSc6d1	finanční
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
podnik	podnik	k1gInSc1	podnik
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
oblibu	obliba	k1gFnSc4	obliba
opět	opět	k6eAd1	opět
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
i	i	k9	i
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
Moulin	moulin	k2eAgInSc4d1	moulin
Rouge	rouge	k1gFnSc7	rouge
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
má	mít	k5eAaImIp3nS	mít
850	[number]	k4	850
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
jej	on	k3xPp3gMnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
zhruba	zhruba	k6eAd1	zhruba
420	[number]	k4	420
000	[number]	k4	000
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
i	i	k8xC	i
styl	styl	k1gInSc1	styl
pařížského	pařížský	k2eAgInSc2d1	pařížský
originálu	originál	k1gInSc2	originál
byly	být	k5eAaImAgFnP	být
napodobeny	napodoben	k2eAgFnPc1d1	napodobena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInPc4d3	nejznámější
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
podnik	podnik	k1gInSc1	podnik
Moulin	moulin	k2eAgInSc1d1	moulin
Rouge	rouge	k1gFnSc4	rouge
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
o	o	k7c6	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moulin	moulin	k2eAgInSc4d1	moulin
Rouge	rouge	k1gFnSc4	rouge
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
kabaretu	kabaret	k1gInSc2	kabaret
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
kabaretu	kabaret	k1gInSc2	kabaret
</s>
</p>
<p>
<s>
Reportáž	reportáž	k1gFnSc1	reportáž
na	na	k7c4	na
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
