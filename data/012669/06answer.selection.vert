<s>
Moulin	moulin	k2eAgMnSc1d1	moulin
Rouge	rouge	k1gFnPc4	rouge
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Červený	červený	k2eAgInSc1d1	červený
mlýn	mlýn	k1gInSc1	mlýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
kabaret	kabaret	k1gInSc1	kabaret
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pigalle	Pigalle	k1gFnSc2	Pigalle
<g/>
,	,	kIx,	,
na	na	k7c6	na
Boulevardu	Boulevard	k1gInSc6	Boulevard
de	de	k?	de
Clichy	Clicha	k1gFnSc2	Clicha
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Montmartru	Montmartr	k1gInSc2	Montmartr
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
