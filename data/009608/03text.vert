<p>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
bublák	bublák	k1gMnSc1	bublák
je	být	k5eAaImIp3nS	být
plemeno	plemeno	k1gNnSc4	plemeno
holuba	holub	k1gMnSc2	holub
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
barevní	barevný	k2eAgMnPc1d1	barevný
holubi	holub	k1gMnPc1	holub
<g/>
,	,	kIx,	,
z	z	k7c2	z
podskupiny	podskupina	k1gFnSc2	podskupina
bubláci	bublák	k1gMnPc1	bublák
<g/>
.	.	kIx.	.
</s>
<s>
Bubláci	bublák	k1gMnPc1	bublák
jsou	být	k5eAaImIp3nP	být
charakterističtí	charakteristický	k2eAgMnPc1d1	charakteristický
hlavně	hlavně	k9	hlavně
svým	svůj	k3xOyFgInSc7	svůj
hlasem	hlas	k1gInSc7	hlas
<g/>
;	;	kIx,	;
bubláním	bublání	k1gNnSc7	bublání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
modifikací	modifikace	k1gFnSc7	modifikace
vrkání	vrkání	k1gNnSc2	vrkání
holubů	holub	k1gMnPc2	holub
v	v	k7c4	v
dlouhotrvající	dlouhotrvající	k2eAgInSc4d1	dlouhotrvající
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
zurčení	zurčení	k1gNnSc4	zurčení
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
zvonění	zvonění	k1gNnSc1	zvonění
zvonů	zvon	k1gInPc2	zvon
nebo	nebo	k8xC	nebo
bubnování	bubnování	k1gNnSc2	bubnování
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
anglický	anglický	k2eAgMnSc1d1	anglický
bublák	bublák	k1gMnSc1	bublák
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
z	z	k7c2	z
podskupiny	podskupina	k1gFnSc2	podskupina
bubláků	bublák	k1gMnPc2	bublák
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
bublácích	bublák	k1gMnPc6	bublák
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Ruska	Rusko	k1gNnSc2	Rusko
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
bubláci	bublák	k1gMnPc1	bublák
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
doložil	doložit	k5eAaPmAgMnS	doložit
chovatel	chovatel	k1gMnSc1	chovatel
Aldrovandi	Aldrovand	k1gMnPc1	Aldrovand
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
bublácích	bublák	k1gMnPc6	bublák
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ale	ale	k8xC	ale
až	až	k9	až
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnSc2	The
Columbarium	Columbarium	k1gNnSc1	Columbarium
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Moorea	Mooreus	k1gMnSc2	Mooreus
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oné	onen	k3xDgFnSc6	onen
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
perlové	perlová	k1gFnSc6	perlová
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
černé	černý	k2eAgNnSc1d1	černé
strakaté	strakatý	k2eAgNnSc1d1	strakaté
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
chomáč	chomáč	k1gInSc1	chomáč
peří	peřit	k5eAaImIp3nS	peřit
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
původ	původ	k1gInSc4	původ
jejich	jejich	k3xOp3gNnSc2	jejich
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
napodobování	napodobování	k1gNnSc2	napodobování
zvuku	zvuk	k1gInSc2	zvuk
polnice	polnice	k1gFnSc2	polnice
,	,	kIx,	,
mimo	mimo	k7c4	mimo
tuto	tento	k3xDgFnSc4	tento
stručnou	stručný	k2eAgFnSc4d1	stručná
zmínku	zmínka	k1gFnSc4	zmínka
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
popisováni	popisovat	k5eAaImNgMnP	popisovat
jen	jen	k6eAd1	jen
málo	málo	k6eAd1	málo
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
prostšeji	prostšej	k1gMnPc7	prostšej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
dostali	dostat	k5eAaPmAgMnP	dostat
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
než	než	k8xS	než
o	o	k7c4	o
dnešní	dnešní	k2eAgMnPc4d1	dnešní
bubláky	bublák	k1gMnPc4	bublák
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jejich	jejich	k3xOp3gMnPc4	jejich
předky	předek	k1gMnPc4	předek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
chovali	chovat	k5eAaImAgMnP	chovat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1700	[number]	k4	1700
až	až	k9	až
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc1	Británie
bucharští	bucharský	k2eAgMnPc1d1	bucharský
bubláci	bublák	k1gMnPc1	bublák
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
holubi	holub	k1gMnPc1	holub
zásadně	zásadně	k6eAd1	zásadně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
další	další	k2eAgNnSc4d1	další
šlechtění	šlechtění	k1gNnSc4	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnPc1d1	moderní
bubláci	bublák	k1gMnPc1	bublák
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
dostali	dostat	k5eAaPmAgMnP	dostat
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
a	a	k8xC	a
původní	původní	k2eAgFnSc1d1	původní
varianta	varianta	k1gFnSc1	varianta
byla	být	k5eAaImAgFnS	být
zavržena	zavrhnout	k5eAaPmNgFnS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
propagátorem	propagátor	k1gMnSc7	propagátor
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
Frank	Frank	k1gMnSc1	Frank
Rommel	Rommel	k1gMnSc1	Rommel
<g/>
,	,	kIx,	,
chovatel	chovatel	k1gMnSc1	chovatel
bucharských	bucharský	k2eAgMnPc2d1	bucharský
bubláků	bublák	k1gMnPc2	bublák
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
bubláků	bublák	k1gMnPc2	bublák
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
chov	chov	k1gInSc1	chov
holubů	holub	k1gMnPc2	holub
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
začal	začít	k5eAaPmAgInS	začít
upadat	upadat	k5eAaPmF	upadat
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
angličtí	anglický	k2eAgMnPc1d1	anglický
bubláci	bublák	k1gMnPc1	bublák
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
chovali	chovat	k5eAaImAgMnP	chovat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
oblibě	obliba	k1gFnSc3	obliba
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
značně	značně	k6eAd1	značně
těšili	těšit	k5eAaImAgMnP	těšit
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
bylo	být	k5eAaImAgNnS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
výstavě	výstava	k1gFnSc6	výstava
celkem	celkem	k6eAd1	celkem
55	[number]	k4	55
jedinců	jedinec	k1gMnPc2	jedinec
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
standard	standard	k1gInSc1	standard
sepsal	sepsat	k5eAaPmAgMnS	sepsat
A.	A.	kA	A.
B.	B.	kA	B.
Warder	Wardero	k1gNnPc2	Wardero
z	z	k7c2	z
kanadského	kanadský	k2eAgNnSc2d1	kanadské
Ontaria	Ontario	k1gNnSc2	Ontario
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
chovatel	chovatel	k1gMnSc1	chovatel
Paul	Paul	k1gMnSc1	Paul
Stefanson	Stefanson	k1gMnSc1	Stefanson
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
křížit	křížit	k5eAaImF	křížit
staré	starý	k2eAgFnSc2d1	stará
linie	linie	k1gFnSc2	linie
bubláků	bublák	k1gMnPc2	bublák
s	s	k7c7	s
čejkami	čejka	k1gFnPc7	čejka
<g/>
,	,	kIx,	,
s	s	k7c7	s
bubláky	bublák	k1gMnPc7	bublák
bernburskými	bernburský	k2eAgInPc7d1	bernburský
<g/>
,	,	kIx,	,
drážďanskými	drážďanský	k2eAgInPc7d1	drážďanský
<g/>
,	,	kIx,	,
bucharskými	bucharský	k2eAgFnPc7d1	Bucharská
a	a	k8xC	a
německými	německý	k2eAgFnPc7d1	německá
dvojvrkočatými	dvojvrkočatá	k1gFnPc7	dvojvrkočatá
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
plemeno	plemeno	k1gNnSc1	plemeno
získalo	získat	k5eAaPmAgNnS	získat
název	název	k1gInSc4	název
anglický	anglický	k2eAgMnSc1d1	anglický
bublák	bublák	k1gMnSc1	bublák
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
chovatelský	chovatelský	k2eAgInSc1d1	chovatelský
klub	klub	k1gInSc1	klub
a	a	k8xC	a
sepsán	sepsat	k5eAaPmNgInS	sepsat
první	první	k4xOgInSc1	první
platný	platný	k2eAgInSc1d1	platný
standard	standard	k1gInSc1	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
prvkem	prvek	k1gInSc7	prvek
při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
opeření	opeření	k1gNnSc1	opeření
a	a	k8xC	a
pernaté	pernatý	k2eAgFnPc1d1	pernatá
ozdoby	ozdoba	k1gFnPc1	ozdoba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
hlasový	hlasový	k2eAgInSc1d1	hlasový
projev	projev	k1gInSc1	projev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
opeření	opeření	k1gNnSc2	opeření
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
kresba	kresba	k1gFnSc1	kresba
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
jsou	být	k5eAaImIp3nP	být
obočnice	obočnice	k1gFnPc4	obočnice
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
znaky	znak	k1gInPc4	znak
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
bubláci	bublák	k1gMnPc1	bublák
jsou	být	k5eAaImIp3nP	být
holubi	holub	k1gMnPc1	holub
středního	střední	k2eAgInSc2d1	střední
rámce	rámec	k1gInSc2	rámec
se	s	k7c7	s
vzpřímenou	vzpřímený	k2eAgFnSc7d1	vzpřímená
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
statné	statný	k2eAgNnSc1d1	statné
a	a	k8xC	a
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
zezadu	zezadu	k6eAd1	zezadu
klínovité	klínovitý	k2eAgFnPc1d1	klínovitá
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
harmonii	harmonie	k1gFnSc6	harmonie
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
těla	tělo	k1gNnPc1	tělo
<g/>
,	,	kIx,	,
široká	široký	k2eAgNnPc1d1	široké
a	a	k8xC	a
klínovitého	klínovitý	k2eAgInSc2d1	klínovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pernaté	pernatý	k2eAgFnPc1d1	pernatá
ozdoby	ozdoba	k1gFnPc1	ozdoba
dělají	dělat	k5eAaImIp3nP	dělat
hlavu	hlava	k1gFnSc4	hlava
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
doopravdy	doopravdy	k6eAd1	doopravdy
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
oranžové	oranžový	k2eAgInPc1d1	oranžový
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
rázy	ráz	k1gInPc1	ráz
mají	mít	k5eAaImIp3nP	mít
okolí	okolí	k1gNnSc4	okolí
očí	oko	k1gNnPc2	oko
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Obočnice	Obočnice	k1gFnPc1	Obočnice
jsou	být	k5eAaImIp3nP	být
jemné	jemný	k2eAgFnPc1d1	jemná
a	a	k8xC	a
peří	peřit	k5eAaImIp3nP	peřit
je	on	k3xPp3gFnPc4	on
těsně	těsně	k6eAd1	těsně
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
nesen	nesen	k2eAgInSc1d1	nesen
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
sklonu	sklon	k1gInSc6	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
postoj	postoj	k1gInSc4	postoj
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
správná	správný	k2eAgFnSc1d1	správná
délka	délka	k1gFnSc1	délka
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
podklesnutí	podklesnutí	k1gNnSc2	podklesnutí
v	v	k7c6	v
patních	patní	k2eAgInPc6d1	patní
kloubech	kloub	k1gInPc6	kloub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
==	==	k?	==
</s>
</p>
<p>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
bubláci	bublák	k1gMnPc1	bublák
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
bubláků	bublák	k1gMnPc2	bublák
<g/>
,	,	kIx,	,
prostorný	prostorný	k2eAgInSc4d1	prostorný
a	a	k8xC	a
otevřený	otevřený	k2eAgInSc4d1	otevřený
holubník	holubník	k1gInSc4	holubník
s	s	k7c7	s
vrstvou	vrstva	k1gFnSc7	vrstva
hoblin	hoblina	k1gFnPc2	hoblina
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
speciální	speciální	k2eAgFnPc4d1	speciální
krmné	krmný	k2eAgFnPc4d1	krmná
směsi	směs	k1gFnPc4	směs
<g/>
.	.	kIx.	.
</s>
<s>
Rousy	rous	k1gInPc4	rous
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zkracovat	zkracovat	k5eAaImF	zkracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
chovatelé	chovatel	k1gMnPc1	chovatel
to	ten	k3xDgNnSc4	ten
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
praktických	praktický	k2eAgInPc2d1	praktický
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
odchovech	odchov	k1gInPc6	odchov
nutné	nutný	k2eAgInPc1d1	nutný
shánět	shánět	k5eAaImF	shánět
náhradní	náhradní	k2eAgFnSc4d1	náhradní
chůvu	chůva	k1gFnSc4	chůva
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
bublák	bublák	k1gMnSc1	bublák
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgFnPc4d1	dobrá
rodičovské	rodičovský	k2eAgFnPc4d1	rodičovská
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
o	o	k7c4	o
holoubata	holoubě	k1gNnPc4	holoubě
stará	starý	k2eAgNnPc4d1	staré
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
hluční	hlučný	k2eAgMnPc1d1	hlučný
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
obvykle	obvykle	k6eAd1	obvykle
bublají	bublat	k5eAaImIp3nP	bublat
méně	málo	k6eAd2	málo
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
bublají	bublat	k5eAaImIp3nP	bublat
jen	jen	k9	jen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
klidný	klidný	k2eAgInSc4d1	klidný
a	a	k8xC	a
nebojácný	bojácný	k2eNgInSc4d1	nebojácný
temperament	temperament	k1gInSc4	temperament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
