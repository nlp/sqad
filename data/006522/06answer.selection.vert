<s>
Máj	máj	k1gInSc1	máj
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
Hynku	Hynek	k1gMnSc3	Hynek
Kommovi	Komm	k1gMnSc3	Komm
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pražskému	pražský	k2eAgMnSc3d1	pražský
měšťanovi	měšťan	k1gMnSc3	měšťan
<g/>
,	,	kIx,	,
pekařskému	pekařský	k2eAgMnSc3d1	pekařský
mistru	mistr	k1gMnSc3	mistr
<g/>
,	,	kIx,	,
pozdějšímu	pozdní	k2eAgMnSc3d2	pozdější
radnímu	radní	k1gMnSc3	radní
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Máchův	Máchův	k2eAgMnSc1d1	Máchův
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
