<s>
Lucien	Lucien	k2eAgInSc1d1	Lucien
Clergue	Clergue	k1gInSc1	Clergue
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
Arles	Arlesa	k1gFnPc2	Arlesa
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
−	−	k?	−
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Akademie	akademie	k1gFnSc2	akademie
umění	umění	k1gNnSc2	umění
Académie	Académie	k1gFnSc2	Académie
des	des	k1gNnSc7	des
Beaux-Arts	Beaux-Artsa	k1gFnPc2	Beaux-Artsa
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Lucien	Lucien	k2eAgInSc1d1	Lucien
Clergue	Clergue	k1gInSc1	Clergue
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
7	[number]	k4	7
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgInS	učit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gNnSc4	jeho
učitel	učitel	k1gMnSc1	učitel
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
ho	on	k3xPp3gMnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
víc	hodně	k6eAd2	hodně
naučit	naučit	k5eAaPmF	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
dalšímu	další	k2eAgNnSc3d1	další
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
základům	základ	k1gInPc3	základ
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
koridě	korida	k1gFnSc6	korida
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
ukázal	ukázat	k5eAaPmAgMnS	ukázat
své	svůj	k3xOyFgFnPc4	svůj
fotografie	fotografia	k1gFnPc4	fotografia
Pablu	Pabl	k1gMnSc3	Pabl
Picassovi	Picass	k1gMnSc3	Picass
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
půl	půl	k1xP	půl
mladý	mladý	k2eAgMnSc1d1	mladý
Clergue	Clergu	k1gFnPc4	Clergu
pracoval	pracovat	k5eAaImAgMnS	pracovat
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
snímky	snímka	k1gFnSc2	snímka
posílal	posílat	k5eAaImAgInS	posílat
Picassovi	Picass	k1gMnSc3	Picass
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
sérii	série	k1gFnSc4	série
fotografií	fotografia	k1gFnPc2	fotografia
kočovných	kočovný	k2eAgMnPc2d1	kočovný
bavičů	bavič	k1gMnPc2	bavič
<g/>
,	,	kIx,	,
akrobatů	akrobat	k1gMnPc2	akrobat
a	a	k8xC	a
harlekýnů	harlekýn	k1gMnPc2	harlekýn
Saltimbanques	Saltimbanquesa	k1gFnPc2	Saltimbanquesa
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
sérii	série	k1gFnSc4	série
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hlavním	hlavní	k2eAgInSc7d1	hlavní
prvkem	prvek	k1gInSc7	prvek
byla	být	k5eAaImAgFnS	být
mršina	mršina	k1gFnSc1	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
i	i	k8xC	i
knihu	kniha	k1gFnSc4	kniha
od	od	k7c2	od
spisovatele	spisovatel	k1gMnSc2	spisovatel
Yvese	Yves	k1gMnSc2	Yves
Navarreho	Navarre	k1gMnSc2	Navarre
<g/>
.	.	kIx.	.
</s>
<s>
Pořídil	pořídit	k5eAaPmAgInS	pořídit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
snímků	snímek	k1gInPc2	snímek
cikánů	cikán	k1gMnPc2	cikán
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
slávě	sláva	k1gFnSc6	sláva
kytaristy	kytarista	k1gMnSc2	kytarista
Manitase	Manitasa	k1gFnSc6	Manitasa
de	de	k?	de
Platy	plato	k1gNnPc7	plato
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
snímky	snímek	k1gInPc1	snímek
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
sbírek	sbírka	k1gFnPc2	sbírka
známých	známý	k2eAgNnPc2d1	známé
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc2d1	soukromá
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovce	stovka	k1gFnSc6	stovka
samostatných	samostatný	k2eAgFnPc6d1	samostatná
výstavách	výstava	k1gFnPc6	výstava
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
takovými	takový	k3xDgFnPc7	takový
významnými	významný	k2eAgFnPc7d1	významná
výstavami	výstava	k1gFnPc7	výstava
jakoje	jakoj	k1gInSc2	jakoj
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
v	v	k7c4	v
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Modern	Modern	k1gMnSc1	Modern
Art	Art	k1gFnSc2	Art
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
výstava	výstava	k1gFnSc1	výstava
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
Edward	Edward	k1gMnSc1	Edward
Steichen	Steichen	k1gInSc4	Steichen
s	s	k7c7	s
Lucienem	Lucien	k1gMnSc7	Lucien
Clerguem	Clergu	k1gMnSc7	Clergu
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Brandt	Brandt	k1gMnSc1	Brandt
a	a	k8xC	a
Yasuhiro	Yasuhiro	k1gNnSc1	Yasuhiro
Ishimoto	Ishimota	k1gFnSc5	Ishimota
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
muzeím	muzeum	k1gNnPc3	muzeum
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
kolekcí	kolekce	k1gFnSc7	kolekce
fotografií	fotografia	k1gFnPc2	fotografia
Luciena	Lucien	k1gMnSc2	Lucien
Clergua	Clerguus	k1gMnSc2	Clerguus
patří	patřit	k5eAaImIp3nS	patřit
The	The	k1gFnSc4	The
Fogg	Fogga	k1gFnPc2	Fogga
Museum	museum	k1gNnSc4	museum
při	při	k7c6	při
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnPc1	universita
a	a	k8xC	a
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc4	Arts
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
snímky	snímek	k1gInPc1	snímek
Jeana	Jean	k1gMnSc2	Jean
Cocteau	Cocteaa	k1gMnSc4	Cocteaa
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
stálé	stálý	k2eAgFnSc2d1	stálá
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Jean	Jean	k1gMnSc1	Jean
Cocteau	Coctea	k1gMnSc3	Coctea
Museum	museum	k1gNnSc4	museum
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Mentonu	menton	k1gInSc6	menton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
výstava	výstava	k1gFnSc1	výstava
fotografií	fotografia	k1gFnPc2	fotografia
Jeana	Jean	k1gMnSc2	Jean
Cocteau	Cocteaa	k1gFnSc4	Cocteaa
ve	v	k7c6	v
Westwood	Westwooda	k1gFnPc2	Westwooda
Gallery	Galler	k1gInPc1	Galler
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmdesáti	osmdesát	k4xCc2	osmdesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
Lucien	Lucien	k1gInSc4	Lucien
Clergue	Clergu	k1gFnSc2	Clergu
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Picassa	Picass	k1gMnSc4	Picass
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
přátelství	přátelství	k1gNnSc1	přátelství
trvalo	trvat	k5eAaImAgNnS	trvat
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
mistrovy	mistrův	k2eAgFnSc2d1	Mistrova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Můj	můj	k1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
Picasso	Picassa	k1gFnSc5	Picassa
my	my	k3xPp1nPc1	my
friend	friend	k1gInSc1	friend
<g/>
)	)	kIx)	)
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
důležité	důležitý	k2eAgInPc4d1	důležitý
okamžiky	okamžik	k1gInPc4	okamžik
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
přáteli	přítel	k1gMnPc7	přítel
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Michelem	Michel	k1gMnSc7	Michel
Tournierem	Tournier	k1gMnSc7	Tournier
a	a	k8xC	a
historikem	historik	k1gMnSc7	historik
Jean-Mauricem	Jean-Mauric	k1gMnSc7	Jean-Mauric
Rouquettem	Rouquett	k1gMnSc7	Rouquett
založili	založit	k5eAaPmAgMnP	založit
fotografický	fotografický	k2eAgInSc4d1	fotografický
festival	festival	k1gInSc4	festival
Rencontres	Rencontres	k1gMnSc1	Rencontres
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arles	Arles	k1gMnSc1	Arles
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
koná	konat	k5eAaImIp3nS	konat
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
prezentována	prezentovat	k5eAaBmNgNnP	prezentovat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
letech	let	k1gInPc6	let
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
město	město	k1gNnSc1	město
Arles	Arlesa	k1gFnPc2	Arlesa
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Luciena	Lucieno	k1gNnSc2	Lucieno
Clergua	Clergua	k1gFnSc1	Clergua
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
retrospektivní	retrospektivní	k2eAgFnSc4d1	retrospektivní
výstavu	výstava	k1gFnSc4	výstava
360	[number]	k4	360
jeho	jeho	k3xOp3gFnPc2	jeho
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
období	období	k1gNnPc2	období
1953	[number]	k4	1953
-	-	kIx~	-
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Lucie	Lucie	k1gFnSc2	Lucie
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
rytířem	rytíř	k1gMnSc7	rytíř
Légion	Légion	k1gInSc1	Légion
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
honneur	honneur	k1gMnSc1	honneur
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
členem	člen	k1gMnSc7	člen
Académie	Académie	k1gFnSc2	Académie
des	des	k1gNnSc1	des
Beaux-Arts	Beaux-Arts	k1gInSc4	Beaux-Arts
při	pře	k1gFnSc3	pře
Institut	institut	k1gInSc1	institut
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
při	při	k7c6	při
otevírání	otevírání	k1gNnSc6	otevírání
oddělení	oddělení	k1gNnSc2	oddělení
věnované	věnovaný	k2eAgFnSc6d1	věnovaná
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Clergue	Clergue	k6eAd1	Clergue
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Corps	corps	k1gInSc1	corps
mémorable	mémorable	k6eAd1	mémorable
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
Seghers	Seghersa	k1gFnPc2	Seghersa
editions	editions	k1gInSc1	editions
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
<g/>
:	:	kIx,	:
Paul	Paul	k1gMnSc1	Paul
Éluard	Éluard	k1gMnSc1	Éluard
<g/>
,	,	kIx,	,
obálka	obálka	k1gFnSc1	obálka
<g/>
:	:	kIx,	:
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc1d1	úvodní
báseň	báseň	k1gFnSc1	báseň
<g/>
:	:	kIx,	:
Jean	Jean	k1gMnSc1	Jean
Cocteau	Cocteaus	k1gInSc2	Cocteaus
<g/>
.	.	kIx.	.
</s>
<s>
Reedice	reedice	k1gFnSc1	reedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bez	bez	k7c2	bez
básně	báseň	k1gFnSc2	báseň
J.	J.	kA	J.
Cocteau	Cocteaus	k1gInSc2	Cocteaus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
verzi	verze	k1gFnSc6	verze
kde	kde	k6eAd1	kde
cenzoři	cenzor	k1gMnPc1	cenzor
nařídili	nařídit	k5eAaPmAgMnP	nařídit
změnu	změna	k1gFnSc4	změna
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
tuctu	tucet	k1gInSc2	tucet
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
s	s	k7c7	s
veškerým	veškerý	k3xTgInSc7	veškerý
textem	text	k1gInSc7	text
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
remake	remake	k1gFnSc1	remake
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
s	s	k7c7	s
přidanými	přidaný	k2eAgFnPc7d1	přidaná
fotografiemi	fotografia	k1gFnPc7	fotografia
a	a	k8xC	a
novou	nový	k2eAgFnSc7d1	nová
intarzií	intarzie	k1gFnSc7	intarzie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgMnSc2	stý
výročí	výročí	k1gNnSc3	výročí
básníka	básník	k1gMnSc2	básník
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc4d1	další
vydání	vydání	k1gNnSc4	vydání
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-2-221-08423-6	[number]	k4	978-2-221-08423-6
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
poslední	poslední	k2eAgInSc1d1	poslední
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnPc4	organizace
<g/>
:	:	kIx,	:
Carré	Carrý	k2eAgFnPc4d1	Carrá
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Nîmes	Nîmes	k1gMnSc1	Nîmes
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
oslava	oslava	k1gFnSc1	oslava
50	[number]	k4	50
let	léto	k1gNnPc2	léto
legendárního	legendární	k2eAgNnSc2d1	legendární
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Listopad	listopad	k1gInSc1	listopad
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
in	in	k?	in
Turck	Turck	k1gInSc1	Turck
<g/>
,	,	kIx,	,
Eva-Monika	Eva-Monika	k1gFnSc1	Eva-Monika
<g/>
:	:	kIx,	:
Poésie	Poésie	k1gFnSc1	Poésie
Photographique	Photographique	k1gFnSc1	Photographique
<g/>
(	(	kIx(	(
<g/>
Hardcover	Hardcover	k1gInSc1	Hardcover
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Photographic	Photographic	k1gMnSc1	Photographic
Poetry	Poetr	k1gMnPc7	Poetr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Clergue	Clergue	k6eAd1	Clergue
<g/>
,	,	kIx,	,
Lucien	Lucien	k1gInSc4	Lucien
(	(	kIx(	(
<g/>
Photographer	Photographra	k1gFnPc2	Photographra
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Heiting	Heiting	k1gInSc1	Heiting
<g/>
,	,	kIx,	,
Manfred	Manfred	k1gMnSc1	Manfred
(	(	kIx(	(
<g/>
Foreword	Foreword	k1gMnSc1	Foreword
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kranzfelder	Kranzfelder	k1gMnSc1	Kranzfelder
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
(	(	kIx(	(
<g/>
Contributor	Contributor	k1gMnSc1	Contributor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
English	English	k1gMnSc1	English
<g/>
,	,	kIx,	,
French	French	k1gMnSc1	French
and	and	k?	and
German	German	k1gMnSc1	German
<g/>
,	,	kIx,	,
Prestel	Prestel	k1gMnSc1	Prestel
Publishing	Publishing	k1gInSc1	Publishing
<g/>
.	.	kIx.	.
</s>
<s>
Ověřeno	ověřit	k5eAaPmNgNnS	ověřit
k	k	k7c3	k
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-7913-2850-6	[number]	k4	3-7913-2850-6
Langage	Langage	k1gNnSc2	Langage
des	des	k1gNnSc2	des
Sables	Sables	k1gMnSc1	Sables
<g/>
,	,	kIx,	,
Agep	Agep	k1gMnSc1	Agep
<g/>
,	,	kIx,	,
Marseilles	Marseilles	k1gMnSc1	Marseilles
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-902634-08-0	[number]	k4	2-902634-08-0
Portraits	Portraitsa	k1gFnPc2	Portraitsa
<g/>
,	,	kIx,	,
Actes	Actesa	k1gFnPc2	Actesa
Sud	suda	k1gFnPc2	suda
<g/>
,	,	kIx,	,
Arles	Arlesa	k1gFnPc2	Arlesa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-7427-5423-7	[number]	k4	2-7427-5423-7
</s>
