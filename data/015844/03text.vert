<s>
Hrabství	hrabství	k1gNnSc1
Cork	Corka	k1gFnPc2
</s>
<s>
Hrabství	hrabství	k1gNnSc1
Cork	Corka	k1gFnPc2
Mapa	mapa	k1gFnSc1
hrabství	hrabství	k1gNnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Cork	Cork	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
7	#num#	k4
500	#num#	k4
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
542	#num#	k4
196	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
72,3	72,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
C	C	kA
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.corkcoco.ie	www.corkcoco.ie	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
County	county	k1nS
Cork	cork	k1gInSc1
(	(	kIx(
<g/>
irsky	irsky	k6eAd1
Contae	Contae	k1gFnSc1
Chorcaí	Chorcaí	k1gFnSc2
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
County	Count	k1gInPc1
Cork	Corka	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
irské	irský	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
jihu	jih	k1gInSc6
země	zem	k1gFnSc2
v	v	k7c6
bývalé	bývalý	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Munster	Munstra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
hrabstvím	hrabství	k1gNnSc7
Limerick	Limericka	k1gFnPc2
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
hrabstvím	hrabství	k1gNnSc7
Kerry	Kerra	k1gFnSc2
na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
hrabstvími	hrabství	k1gNnPc7
Tipperary	Tipperara	k1gFnSc2
a	a	k8xC
Waterford	Waterforda	k1gFnPc2
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
členité	členitý	k2eAgNnSc4d1
jižní	jižní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
omývá	omývat	k5eAaImIp3nS
Keltské	keltský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
hrabství	hrabství	k1gNnSc2
je	být	k5eAaImIp3nS
Cork	Cork	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
také	také	k9
samostatnou	samostatný	k2eAgFnSc7d1
samosprávní	samosprávní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrabství	hrabství	k1gNnPc4
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
7499	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ho	on	k3xPp3gMnSc4
činí	činit	k5eAaImIp3nS
největším	veliký	k2eAgNnSc7d3
hrabstvím	hrabství	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
žije	žít	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gInSc6
518	#num#	k4
218	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
zajímavá	zajímavý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
hrabství	hrabství	k1gNnSc2
patří	patřit	k5eAaImIp3nS
Cork	Cork	k1gInSc1
<g/>
,	,	kIx,
Cobh	Cobh	k1gInSc1
a	a	k8xC
mys	mys	k1gInSc1
Mizen	mizen	k2eAgInSc4d1
Head	Head	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
hrabství	hrabství	k1gNnPc1
<g/>
,	,	kIx,
používaná	používaný	k2eAgNnPc1d1
zejména	zejména	k9
na	na	k7c6
SPZ	SPZ	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
C.	C.	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hrabství	hrabství	k1gNnSc2
Cork	Corka	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Irsko	Irsko	k1gNnSc1
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
–	–	k?
Irské	irský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
•	•	k?
Irská	irský	k2eAgNnPc1d1
hrabství	hrabství	k1gNnPc1
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
)	)	kIx)
Connacht	Connacht	k1gInSc1
</s>
<s>
Galway	Galwa	k2eAgInPc4d1
(	(	kIx(
<g/>
Galway	Galway	k1gInPc4
<g/>
)	)	kIx)
•	•	k?
Leitrim	Leitrim	k1gMnSc1
•	•	k?
Mayo	Mayo	k1gMnSc1
•	•	k?
Roscommon	Roscommon	k1gMnSc1
•	•	k?
Sligo	Sligo	k1gMnSc1
Munster	Munster	k1gMnSc1
</s>
<s>
Clare	Clar	k1gMnSc5
•	•	k?
Cork	Cork	k1gMnSc1
(	(	kIx(
<g/>
Cork	Cork	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Kerry	Kerra	k1gFnSc2
•	•	k?
Limerick	Limerick	k1gMnSc1
(	(	kIx(
<g/>
Limerick	Limerick	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Tipperary	Tipperara	k1gFnSc2
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc2d1
Tipperary	Tipperara	k1gFnSc2
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc2d1
Tipperary	Tipperara	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Waterford	Waterford	k1gMnSc1
(	(	kIx(
<g/>
Waterford	Waterford	k1gMnSc1
<g/>
)	)	kIx)
Leinster	Leinster	k1gMnSc1
</s>
<s>
Carlow	Carlow	k?
•	•	k?
Dublin	Dublin	k1gInSc1
(	(	kIx(
<g/>
Dublin	Dublin	k1gInSc1
<g/>
,	,	kIx,
Dún	Dún	k1gMnSc5
Laoghaire	Laoghair	k1gMnSc5
–	–	k?
Rathdown	Rathdown	k1gInSc1
<g/>
,	,	kIx,
Fingal	Fingal	k1gInSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgInSc1d1
Dublin	Dublin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kildare	Kildar	k1gMnSc5
•	•	k?
Kilkenny	Kilkenn	k1gInPc1
•	•	k?
Laois	Laois	k1gInSc1
(	(	kIx(
<g/>
Queen	Queen	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
)	)	kIx)
•	•	k?
Longford	Longford	k1gMnSc1
•	•	k?
Louth	Louth	k1gMnSc1
•	•	k?
Meath	Meath	k1gMnSc1
•	•	k?
Offaly	Offala	k1gFnSc2
(	(	kIx(
<g/>
King	King	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
)	)	kIx)
•	•	k?
Westmeath	Westmeath	k1gMnSc1
•	•	k?
Wexford	Wexford	k1gMnSc1
•	•	k?
Wicklow	Wicklow	k1gMnSc4
Ulster	Ulster	k1gInSc1
</s>
<s>
Antrim	Antrim	k1gInSc1
*	*	kIx~
•	•	k?
Armagh	Armagh	k1gInSc1
*	*	kIx~
•	•	k?
Cavan	Cavan	k1gInSc1
•	•	k?
Fermanagh	Fermanagh	k1gInSc1
*	*	kIx~
•	•	k?
Londonderry	Londonderra	k1gMnSc2
(	(	kIx(
<g/>
Derry	Derra	k1gMnSc2
<g/>
)	)	kIx)
*	*	kIx~
•	•	k?
Donegal	Donegal	k1gInSc1
•	•	k?
Down	Down	k1gInSc1
*	*	kIx~
•	•	k?
Monaghan	Monaghan	k1gInSc1
•	•	k?
Tyrone	Tyron	k1gInSc5
*	*	kIx~
*	*	kIx~
značí	značit	k5eAaImIp3nS
hrabství	hrabství	k1gNnSc4
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
Irsku	Irsko	k1gNnSc6
</s>
<s>
Hrabství	hrabství	k1gNnSc1
Cork	Corka	k1gFnPc2
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
–	–	k?
Cork	Cork	k1gMnSc1
města	město	k1gNnSc2
</s>
<s>
Bandon	Bandon	k1gMnSc1
•	•	k?
Bantry	Bantr	k1gInPc1
•	•	k?
Clonakilty	Clonakilt	k1gInPc1
•	•	k?
Cobh	Cobh	k1gInSc1
•	•	k?
Cork	Cork	k1gInSc1
•	•	k?
Fermoy	Fermoa	k1gFnSc2
•	•	k?
Kinsale	Kinsala	k1gFnSc3
•	•	k?
Macroom	Macroom	k1gInSc1
•	•	k?
Mallow	Mallow	k1gFnSc2
•	•	k?
Midleton	Midleton	k1gInSc1
•	•	k?
Passage	Passage	k1gInSc1
West	West	k1gInSc1
•	•	k?
Skibbereen	Skibbereen	k1gInSc1
•	•	k?
Youghal	Youghal	k1gMnSc1
vesnice	vesnice	k1gFnSc2
</s>
<s>
Adrigole	Adrigole	k1gFnSc1
•	•	k?
Aghabullogue	Aghabullogue	k1gInSc1
•	•	k?
Aghada	Aghada	k1gFnSc1
•	•	k?
Ahakista	Ahakista	k1gMnSc1
•	•	k?
Aherla	Aherla	k1gMnSc1
•	•	k?
Ahiohill	Ahiohill	k1gMnSc1
•	•	k?
Allihies	Allihies	k1gMnSc1
•	•	k?
Annabella	Annabella	k1gMnSc1
•	•	k?
Ardfield	Ardfield	k1gMnSc1
•	•	k?
Ardgroom	Ardgroom	k1gInSc1
•	•	k?
Ballinacurra	Ballinacurr	k1gMnSc2
•	•	k?
Ballinadee	Ballinade	k1gMnSc2
•	•	k?
Ballinagree	Ballinagre	k1gMnSc2
•	•	k?
Ballinascarty	Ballinascarta	k1gFnSc2
•	•	k?
Ballincollig	Ballincollig	k1gInSc1
•	•	k?
Ballineen	Ballineen	k1gInSc1
and	and	k?
Enniskean	Enniskean	k1gInSc1
•	•	k?
Ballingeary	Ballingeara	k1gFnSc2
•	•	k?
Ballinhassig	Ballinhassig	k1gMnSc1
•	•	k?
Ballinora	Ballinora	k1gFnSc1
•	•	k?
Ballinspittle	Ballinspittle	k1gFnSc2
•	•	k?
Ballintemple	Ballintemple	k1gFnSc2
•	•	k?
Ballycotton	Ballycotton	k1gInSc1
•	•	k?
Ballydehob	Ballydehoba	k1gFnPc2
•	•	k?
Ballydesmond	Ballydesmond	k1gMnSc1
•	•	k?
Ballygarvan	Ballygarvan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Ballylickey	Ballylickea	k1gMnSc2
•	•	k?
Ballymakeera	Ballymakeer	k1gMnSc2
•	•	k?
Ballymore	Ballymor	k1gInSc5
•	•	k?
Ballyvourney	Ballyvourney	k1gInPc1
•	•	k?
Baltimore	Baltimore	k1gInSc1
•	•	k?
Banteer	Banteer	k1gInSc1
•	•	k?
Béal	Béal	k1gInSc1
na	na	k7c4
Bláth	Bláth	k1gInSc4
•	•	k?
Belgooly	Belgoola	k1gFnSc2
•	•	k?
Belvelly	Belvella	k1gFnSc2
•	•	k?
Blarney	Blarnea	k1gFnSc2
•	•	k?
Boherbue	Boherbue	k1gInSc1
•	•	k?
Buttevant	Buttevant	k1gInSc1
•	•	k?
Canovee	Canovee	k1gInSc1
•	•	k?
Carrigadrohid	Carrigadrohid	k1gInSc1
•	•	k?
Carrigaline	Carrigalin	k1gInSc5
•	•	k?
Carriganimmy	Carriganimm	k1gInPc1
•	•	k?
Carrigtwohill	Carrigtwohill	k1gInSc1
•	•	k?
Castlehaven	Castlehaven	k2eAgInSc1d1
•	•	k?
Castlelyons	Castlelyons	k1gInSc1
•	•	k?
Castlemartyr	Castlemartyr	k1gInSc1
•	•	k?
Castletown-Kinneigh	Castletown-Kinneigh	k1gInSc1
•	•	k?
Castletownbere	Castletownber	k1gInSc5
•	•	k?
Castletownroche	Castletownroch	k1gFnSc2
•	•	k?
Castletownshend	Castletownshend	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Charleville	Charleville	k1gInSc1
•	•	k?
Churchtown	Churchtown	k1gInSc1
•	•	k?
Cloghroe	Cloghroe	k1gInSc1
•	•	k?
Clondulane	Clondulan	k1gMnSc5
•	•	k?
Cloughduv	Cloughduv	k1gInSc1
•	•	k?
Cloyne	Cloyn	k1gInSc5
•	•	k?
Coachford	Coachfordo	k1gNnPc2
•	•	k?
Conna	Conn	k1gMnSc2
•	•	k?
Coolea	Cooleus	k1gMnSc2
•	•	k?
Courtmacsherry	Courtmacsherra	k1gMnSc2
•	•	k?
Crookhaven	Crookhaven	k2eAgInSc4d1
•	•	k?
Crookstown	Crookstown	k1gInSc4
•	•	k?
Crossbarry	Crossbarra	k1gFnSc2
•	•	k?
Crosshaven	Crosshaven	k2eAgInSc1d1
•	•	k?
Cullen	Cullen	k1gInSc1
•	•	k?
Doneraile	Doneraila	k1gFnSc3
•	•	k?
Douglas	Douglas	k1gInSc1
•	•	k?
Drimoleague	Drimoleague	k1gInSc1
•	•	k?
Dripsey	Dripsea	k1gFnSc2
•	•	k?
Dromahane	Dromahan	k1gMnSc5
•	•	k?
Dungourney	Dungournea	k1gMnSc2
•	•	k?
Dunmanway	Dunmanwaa	k1gMnSc2
•	•	k?
Durrus	Durrus	k1gMnSc1
•	•	k?
Eyeries	Eyeries	k1gMnSc1
•	•	k?
Farran	Farran	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Fermoy	Fermoa	k1gFnSc2
•	•	k?
Fountainstown	Fountainstown	k1gMnSc1
•	•	k?
Glandore	Glandor	k1gInSc5
•	•	k?
Glanmire	Glanmir	k1gInSc5
•	•	k?
Glanworth	Glanworth	k1gInSc1
•	•	k?
Glasheen	Glasheen	k1gInSc1
•	•	k?
Glenbrook	Glenbrook	k1gInSc1
•	•	k?
Glengarriff	Glengarriff	k1gInSc1
•	•	k?
Glounthaune	Glounthaun	k1gInSc5
•	•	k?
Goleen	Goleen	k1gInSc1
•	•	k?
Gougane	Gougan	k1gMnSc5
Barra	Barro	k1gNnPc1
•	•	k?
Halfway	Halfwaa	k1gMnSc2
•	•	k?
Innishannon	Innishannon	k1gInSc1
•	•	k?
Kanturk	Kanturk	k1gInSc1
•	•	k?
Kilbrittain	Kilbrittain	k1gMnSc1
•	•	k?
Kilmichael	Kilmichael	k1gMnSc1
•	•	k?
Kilnamartyra	Kilnamartyra	k1gMnSc1
•	•	k?
Killumney	Killumnea	k1gFnSc2
•	•	k?
Kilworth	Kilworth	k1gMnSc1
•	•	k?
Knockavilla	Knockavilla	k1gMnSc1
•	•	k?
Knocknagree	Knocknagre	k1gFnSc2
•	•	k?
Knockraha	Knockraha	k1gMnSc1
•	•	k?
Leap	Leap	k1gMnSc1
•	•	k?
Liscarroll	Liscarroll	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lisgoold	Lisgoold	k1gMnSc1
•	•	k?
Lismire	Lismir	k1gInSc5
•	•	k?
Lombardstown	Lombardstown	k1gInSc1
•	•	k?
Lyre	Lyre	k1gInSc1
•	•	k?
Mayfield	Mayfield	k1gInSc1
•	•	k?
Meelin	Meelin	k1gInSc1
•	•	k?
Milford	Milford	k1gInSc1
•	•	k?
Millstreet	Millstreet	k1gInSc1
•	•	k?
Minane	Minan	k1gMnSc5
Bridge	Bridgus	k1gMnSc5
•	•	k?
Mitchelstown	Mitchelstown	k1gMnSc1
•	•	k?
Monard	Monard	k1gMnSc1
•	•	k?
Monkstown	Monkstown	k1gMnSc1
•	•	k?
Montenotte	Montenott	k1gInSc5
•	•	k?
Mourne	Mourn	k1gInSc5
Abbey	Abbeum	k1gNnPc7
•	•	k?
Nad	nad	k7c7
•	•	k?
Newcestown	Newcestown	k1gMnSc1
•	•	k?
Newmarket	Newmarket	k1gMnSc1
•	•	k?
Nohoval	Nohoval	k1gMnSc1
•	•	k?
Ovens	Ovens	k1gInSc1
•	•	k?
Rathcormac	Rathcormac	k1gInSc1
•	•	k?
Riverstick	Riverstick	k1gInSc1
•	•	k?
Roberts	Roberts	k1gInSc1
Cove	Cove	k1gInSc1
•	•	k?
Rockchapel	Rockchapel	k1gInSc1
•	•	k?
Rosscarbery	Rosscarbera	k1gFnSc2
•	•	k?
Rylane	Rylan	k1gMnSc5
•	•	k?
Sallybrook	Sallybrook	k1gInSc1
•	•	k?
Schull	Schull	k1gInSc1
•	•	k?
Shanagarry	Shanagarra	k1gFnSc2
•	•	k?
Shanbally	Shanballa	k1gFnSc2
•	•	k?
Shanballymore	Shanballymor	k1gInSc5
•	•	k?
Timoleague	Timoleague	k1gFnSc6
•	•	k?
Togher	Toghra	k1gFnPc2
•	•	k?
Tower	Tower	k1gMnSc1
•	•	k?
Union	union	k1gInSc1
Hall	Hall	k1gMnSc1
•	•	k?
Upton	Upton	k1gMnSc1
•	•	k?
Waterfall	Waterfall	k1gMnSc1
•	•	k?
Watergrasshill	Watergrasshill	k1gMnSc1
•	•	k?
Whiddy	Whidd	k1gInPc1
Island	Island	k1gInSc1
•	•	k?
Whitegate	Whitegat	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Irsko	Irsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4085331-7	4085331-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79129259	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
159438370	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79129259	#num#	k4
</s>
