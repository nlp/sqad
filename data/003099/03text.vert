<s>
Trikolóra	trikolóra	k1gFnSc1	trikolóra
je	být	k5eAaImIp3nS	být
stuha	stuha	k1gFnSc1	stuha
nebo	nebo	k8xC	nebo
vlajka	vlajka	k1gFnSc1	vlajka
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
do	do	k7c2	do
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgInPc2d1	široký
pruhů	pruh	k1gInPc2	pruh
vodorovně	vodorovně	k6eAd1	vodorovně
nebo	nebo	k8xC	nebo
svisle	svisle	k6eAd1	svisle
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
trikolóry	trikolóra	k1gFnPc4	trikolóra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používají	používat	k5eAaImIp3nP	používat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
svislé	svislý	k2eAgFnPc4d1	svislá
trikolóry	trikolóra	k1gFnPc4	trikolóra
patří	patřit	k5eAaImIp3nS	patřit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
trikolóra	trikolóra	k1gFnSc1	trikolóra
má	mít	k5eAaImIp3nS	mít
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
.	.	kIx.	.
</s>
