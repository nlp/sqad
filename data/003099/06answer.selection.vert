<s>
Trikolóra	trikolóra	k1gFnSc1	trikolóra
je	být	k5eAaImIp3nS	být
stuha	stuha	k1gFnSc1	stuha
nebo	nebo	k8xC	nebo
vlajka	vlajka	k1gFnSc1	vlajka
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
do	do	k7c2	do
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgInPc2d1	široký
pruhů	pruh	k1gInPc2	pruh
vodorovně	vodorovně	k6eAd1	vodorovně
nebo	nebo	k8xC	nebo
svisle	svisle	k6eAd1	svisle
<g/>
.	.	kIx.	.
</s>
