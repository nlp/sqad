<s>
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
How	How	k1gFnSc2
I	i	k9
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
zkracováno	zkracován	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
HIMYM	HIMYM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
sitcom	sitcom	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc7
autory	autor	k1gMnPc7
jsou	být	k5eAaImIp3nP
Carter	Carter	k1gMnSc1
Bays	Bays	k1gInSc1
a	a	k8xC
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>