<s>
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
</s>
<s>
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
Logo	logo	k1gNnSc4
seriáluZákladní	seriáluZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
How	How	k?
I	i	k9
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
sitcomromantická	sitcomromantický	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
Formát	formát	k1gInSc1
</s>
<s>
seriál	seriál	k1gInSc1
Námět	námět	k1gInSc1
</s>
<s>
Carter	Carter	k1gMnSc1
BaysCraig	BaysCraig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Josh	Josh	k1gMnSc1
RadnorJason	RadnorJason	k1gMnSc1
SegelCobie	SegelCobie	k1gFnSc2
SmuldersNeil	SmuldersNeil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
HarrisAlyson	HarrisAlyson	k1gMnSc1
HanniganCristin	HanniganCristin	k1gMnSc1
Milioti	Miliot	k1gMnPc1
Skladatel	skladatel	k1gMnSc1
znělky	znělka	k1gFnPc1
</s>
<s>
The	The	k?
Solids	Solids	k1gInSc1
Úvodní	úvodní	k2eAgInSc1d1
znělka	znělka	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Hey	Hey	k1gFnSc1
Beautiful	Beautiful	k1gInSc1
<g/>
“	“	k?
Země	země	k1gFnSc1
původu	původ	k1gInSc2
</s>
<s>
USA	USA	kA
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
9	#num#	k4
Počet	počet	k1gInSc4
dílů	díl	k1gInPc2
</s>
<s>
208	#num#	k4
(	(	kIx(
<g/>
seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
22	#num#	k4
minut	minuta	k1gFnPc2
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Výkonnýproducent	Výkonnýproducent	k1gInSc1
</s>
<s>
Carter	Carter	k1gInSc1
BaysCraig	BaysCraiga	k1gFnPc2
ThomasRob	ThomasRoba	k1gFnPc2
GreenbergPamela	GreenbergPamel	k1gMnSc2
FrymanEileen	FrymanEileen	k2eAgInSc4d1
HeisleeAnn	HeisleeAnn	k1gInSc4
HelineGreg	HelineGreg	k1gMnSc1
MalinsChris	MalinsChris	k1gFnSc2
HarrisStephen	HarrisStephen	k1gInSc1
LloydKourtney	LloydKourtnea	k1gFnSc2
KangJamie	KangJamie	k1gFnSc2
RhonheimerChuck	RhonheimerChuck	k1gInSc1
Tatham	Tatham	k1gInSc1
Produkčníspolečnost	Produkčníspolečnost	k1gFnSc1
</s>
<s>
Bays	Bays	k6eAd1
&	&	k?
Thomas	Thomas	k1gMnSc1
Productions	Productions	k1gInSc4
<g/>
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
Fox	fox	k1gInSc1
Television	Television	k1gInSc1
Distributor	distributor	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
th	th	k?
TelevisionNetflixHuluDisney	TelevisionNetflixHuluDisnea	k1gFnSc2
<g/>
+	+	kIx~
Premiérové	premiérový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
Stanice	stanice	k1gFnSc1
</s>
<s>
CBS	CBS	kA
Formát	formát	k1gInSc1
obrazu	obraz	k1gInSc2
</s>
<s>
multi-camera	multi-camera	k1gFnSc1
<g/>
480	#num#	k4
<g/>
i	i	k8xC
(	(	kIx(
<g/>
SDTV	SDTV	kA
<g/>
)	)	kIx)
<g/>
1080	#num#	k4
<g/>
i	i	k8xC
(	(	kIx(
<g/>
HDTV	HDTV	kA
<g/>
)	)	kIx)
Vysíláno	vysílán	k2eAgNnSc1d1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
Kinoboxu	Kinobox	k1gInSc2
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
,	,	kIx,
IMDb	IMDb	k1gMnSc1
<g/>
,	,	kIx,
TV	TV	kA
<g/>
.	.	kIx.
<g/>
comNěkterá	comNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
How	How	k1gFnSc2
I	i	k9
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
zkracováno	zkracován	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
HIMYM	HIMYM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
sitcom	sitcom	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc7
autory	autor	k1gMnPc7
jsou	být	k5eAaImIp3nP
Carter	Carter	k1gMnSc1
Bays	Bays	k1gInSc1
a	a	k8xC
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiérově	premiérově	k6eAd1
byl	být	k5eAaImAgInS
vysílán	vysílat	k5eAaImNgInS
na	na	k7c6
stanici	stanice	k1gFnSc6
CBS	CBS	kA
v	v	k7c6
letech	let	k1gInPc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
208	#num#	k4
dílů	díl	k1gInPc2
v	v	k7c6
devíti	devět	k4xCc6
řadách	řada	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
osou	osa	k1gFnSc7
příběhu	příběh	k1gInSc2
jsou	být	k5eAaImIp3nP
životní	životní	k2eAgFnPc4d1
etudy	etuda	k1gFnPc4
Teda	Ted	k1gMnSc2
Mosbyho	Mosby	k1gMnSc2
(	(	kIx(
<g/>
Josh	Josh	k1gMnSc1
Radnor	Radnor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2030	#num#	k4
vypráví	vyprávět	k5eAaImIp3nS
svým	svůj	k3xOyFgFnPc3
dětem	dítě	k1gFnPc3
příběh	příběh	k1gInSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
poznal	poznat	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnSc4
matku	matka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1
seriálu	seriál	k1gInSc2
Craig	Craig	k1gInSc1
Thomas	Thomas	k1gMnSc1
a	a	k8xC
Carter	Carter	k1gMnSc1
Bays	Baysa	k1gFnPc2
působili	působit	k5eAaImAgMnP
také	také	k9
jako	jako	k9
výkonní	výkonný	k2eAgMnPc1d1
producenti	producent	k1gMnPc1
a	a	k8xC
podíleli	podílet	k5eAaImAgMnP
se	se	k3xPyFc4
i	i	k9
na	na	k7c6
některých	některý	k3yIgInPc6
scénářích	scénář	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
pořad	pořad	k1gInSc1
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
jejich	jejich	k3xOp3gInSc7
životem	život	k1gInSc7
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
12	#num#	k4
dílů	díl	k1gInPc2
režírovala	režírovat	k5eAaImAgFnS
všechny	všechen	k3xTgFnPc4
epizody	epizoda	k1gFnPc4
Pamela	Pamela	k1gFnSc1
Fryman	Fryman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Seriál	seriál	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
úspěch	úspěch	k1gInSc4
u	u	k7c2
kritiků	kritik	k1gMnPc2
a	a	k8xC
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
vysílání	vysílání	k1gNnSc4
si	se	k3xPyFc3
držel	držet	k5eAaImAgMnS
vysokou	vysoký	k2eAgFnSc4d1
sledovanost	sledovanost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nominován	nominovat	k5eAaBmNgMnS
na	na	k7c4
28	#num#	k4
cen	cena	k1gFnPc2
Emmy	Emma	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
devět	devět	k4xCc4
získal	získat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herci	herc	k1gInPc7
Alyson	Alyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
a	a	k8xC
Neil	Neil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc2
získali	získat	k5eAaPmAgMnP
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
i	i	k8xC
ceny	cena	k1gFnPc1
People	People	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gFnPc4
Awards	Awards	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
premiérově	premiérově	k6eAd1
vysílán	vysílat	k5eAaImNgInS
v	v	k7c6
letech	let	k1gInPc6
2009	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
Prima	prima	k2eAgFnPc2d1
Cool	Coola	k1gFnPc2
a	a	k8xC
následně	následně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
Prima	prima	k6eAd1
Love	lov	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
První	první	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2030	#num#	k4
vypráví	vyprávět	k5eAaImIp3nS
Ted	Ted	k1gMnSc1
Mosby	Mosba	k1gFnSc2
(	(	kIx(
<g/>
hlasem	hlas	k1gInSc7
Boba	Bob	k1gMnSc2
Sageta	Saget	k1gMnSc2
<g/>
)	)	kIx)
své	svůj	k3xOyFgFnSc3
dceři	dcera	k1gFnSc3
a	a	k8xC
synovi	syn	k1gMnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
poznal	poznat	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnSc4
matku	matka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
:	:	kIx,
Ted	Ted	k1gMnSc1
(	(	kIx(
<g/>
Josh	Josh	k1gMnSc1
Radnor	Radnor	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
svobodný	svobodný	k2eAgMnSc1d1
sedmadvacetiletý	sedmadvacetiletý	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
dvěma	dva	k4xCgMnPc7
nejlepšími	dobrý	k2eAgMnPc7d3
kamarády	kamarád	k1gMnPc7
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
–	–	k?
s	s	k7c7
právníkem	právník	k1gMnSc7
Marshallem	Marshall	k1gMnSc7
Eriksenem	Eriksen	k1gMnSc7
(	(	kIx(
<g/>
Jason	Jason	k1gMnSc1
Segel	Segel	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
učitelkou	učitelka	k1gFnSc7
v	v	k7c6
mateřské	mateřský	k2eAgFnSc6d1
školce	školka	k1gFnSc6
Lily	lít	k5eAaImAgInP
Aldrinovou	Aldrinový	k2eAgFnSc4d1
(	(	kIx(
<g/>
Alyson	Alyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
po	po	k7c6
9	#num#	k4
letech	let	k1gInPc6
vztahu	vztah	k1gInSc2
zasnoubí	zasnoubit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
vnímá	vnímat	k5eAaImIp3nS
jako	jako	k9
příkaz	příkaz	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
také	také	k9
našel	najít	k5eAaPmAgMnS
celoživotní	celoživotní	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
mu	on	k3xPp3gMnSc3
chce	chtít	k5eAaImIp3nS
„	„	k?
<g/>
pomoci	pomoc	k1gFnSc2
<g/>
“	“	k?
další	další	k2eAgNnSc1d1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
přátel	přítel	k1gMnPc2
<g/>
,	,	kIx,
Barney	Barnea	k1gFnPc1
Stinson	Stinson	k1gInSc1
(	(	kIx(
<g/>
Neil	Neil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
sukničkář	sukničkář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ted	Ted	k1gMnSc1
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
hledání	hledání	k1gNnSc6
pozná	poznat	k5eAaPmIp3nS
kanadskou	kanadský	k2eAgFnSc4d1
televizní	televizní	k2eAgFnSc4d1
reportérku	reportérka	k1gFnSc4
Robin	Robina	k1gFnPc2
Scherbatskou	Scherbatský	k2eAgFnSc4d1
(	(	kIx(
<g/>
Cobie	Cobie	k1gFnSc1
Smulders	Smuldersa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
zamiluje	zamilovat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
sice	sice	k8xC
žádný	žádný	k3yNgInSc1
vážný	vážný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
nehledá	hledat	k5eNaImIp3nS
<g/>
,	,	kIx,
nabídne	nabídnout	k5eAaPmIp3nS
však	však	k9
Tedovi	Tedův	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přátelé	přítel	k1gMnPc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Budoucí	budoucí	k2eAgMnSc1d1
Ted	Ted	k1gMnSc1
okamžitě	okamžitě	k6eAd1
řekne	říct	k5eAaPmIp3nS
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
že	že	k8xS
Robin	robin	k2eAgMnSc1d1
není	být	k5eNaImIp3nS
jejich	jejich	k3xOp3gNnSc1
matkou	matka	k1gFnSc7
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgInPc6d1
dílech	díl	k1gInPc6
ji	on	k3xPp3gFnSc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
tetu	teta	k1gFnSc4
Robin	Robina	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Později	pozdě	k6eAd2
začne	začít	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
s	s	k7c7
cukrářkou	cukrářka	k1gFnSc7
Victorií	Victorie	k1gFnPc2
(	(	kIx(
<g/>
Ashley	Ashley	k1gInPc1
Williams	Williamsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
pozná	poznat	k5eAaPmIp3nS
na	na	k7c6
svatbě	svatba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
ale	ale	k9
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Robin	Robina	k1gFnPc2
k	k	k7c3
němu	on	k3xPp3gInSc3
nějaké	nějaký	k3yIgInPc4
city	cit	k1gInPc4
chová	chovat	k5eAaImIp3nS
a	a	k8xC
žárlí	žárlit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Victoria	Victorium	k1gNnSc2
odjede	odjet	k5eAaPmIp3nS
na	na	k7c4
kulinářský	kulinářský	k2eAgInSc4d1
institut	institut	k1gInSc4
do	do	k7c2
Německa	Německo	k1gNnSc2
a	a	k8xC
s	s	k7c7
Tedem	Ted	k1gMnSc7
zkouší	zkoušet	k5eAaImIp3nS
vztah	vztah	k1gInSc4
na	na	k7c4
dálku	dálka	k1gFnSc4
<g/>
,	,	kIx,
řekne	říct	k5eAaPmIp3nS
Ted	Ted	k1gMnSc1
Robin	robin	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
Victorií	Victorie	k1gFnSc7
rozešli	rozejít	k5eAaPmAgMnP
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ještě	ještě	k6eAd1
není	být	k5eNaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
Victoria	Victorium	k1gNnSc2
zavolá	zavolat	k5eAaPmIp3nS
Tedovi	Ted	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
omylem	omylem	k6eAd1
telefon	telefon	k1gInSc4
zvedne	zvednout	k5eAaPmIp3nS
<g/>
,	,	kIx,
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
Ted	Ted	k1gMnSc1
lhal	lhát	k5eAaImAgMnS
a	a	k8xC
ten	ten	k3xDgMnSc1
tak	tak	k6eAd1
přijde	přijít	k5eAaPmIp3nS
o	o	k7c4
obě	dva	k4xCgFnPc4
dívky	dívka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Robin	robin	k2eAgInSc1d1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
ale	ale	k9
přece	přece	k9
jenom	jenom	k9
usmíří	usmířit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Lily	lít	k5eAaImAgFnP
mezitím	mezitím	k6eAd1
začne	začít	k5eAaPmIp3nS
pochybovat	pochybovat	k5eAaImF
<g/>
,	,	kIx,
jestli	jestli	k8xS
do	do	k7c2
svatby	svatba	k1gFnSc2
s	s	k7c7
Marshallem	Marshall	k1gInSc7
stihne	stihnout	k5eAaPmIp3nS
udělat	udělat	k5eAaPmF
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
chtěla	chtít	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touží	toužit	k5eAaImIp3nS
odjet	odjet	k5eAaPmF
na	na	k7c4
kurz	kurz	k1gInSc4
umění	umění	k1gNnSc2
do	do	k7c2
San	San	k1gFnSc2
Francisca	Francisc	k1gInSc2
a	a	k8xC
s	s	k7c7
Marshallem	Marshallo	k1gNnSc7
se	se	k3xPyFc4
proto	proto	k8xC
rozejde	rozejít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Ted	Ted	k1gMnSc1
po	po	k7c6
první	první	k4xOgFnSc6
noci	noc	k1gFnSc6
s	s	k7c7
Robin	Robina	k1gFnPc2
vrací	vracet	k5eAaImIp3nS
domů	domů	k6eAd1
a	a	k8xC
tam	tam	k6eAd1
sedí	sedit	k5eAaImIp3nS
v	v	k7c6
dešti	dešť	k1gInSc6
Marshall	Marshalla	k1gFnPc2
s	s	k7c7
Lilyiným	Lilyin	k2eAgInSc7d1
zásnubním	zásnubní	k2eAgInSc7d1
prstenem	prsten	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Ted	Ted	k1gMnSc1
a	a	k8xC
Robin	robin	k2eAgMnSc1d1
jsou	být	k5eAaImIp3nP
na	na	k7c6
začátku	začátek	k1gInSc6
druhé	druhý	k4xOgFnSc2
sezóny	sezóna	k1gFnSc2
partnery	partner	k1gMnPc7
a	a	k8xC
Marshall	Marshalla	k1gFnPc2
se	s	k7c7
zlomeným	zlomený	k2eAgNnSc7d1
srdcem	srdce	k1gNnSc7
zkouší	zkoušet	k5eAaImIp3nS
z	z	k7c2
počátku	počátek	k1gInSc2
žít	žít	k5eAaImF
bez	bez	k1gInSc4
Lily	lít	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
na	na	k7c6
kurzu	kurz	k1gInSc6
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
umělkyní	umělkyně	k1gFnSc7
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
setká	setkat	k5eAaPmIp3nS
s	s	k7c7
Marshallem	Marshall	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dají	dát	k5eAaPmIp3nP
se	se	k3xPyFc4
opět	opět	k6eAd1
dohromady	dohromady	k6eAd1
a	a	k8xC
řada	řada	k1gFnSc1
nakonec	nakonec	k6eAd1
vyvrcholí	vyvrcholit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gFnSc7
svatbou	svatba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
se	se	k3xPyFc4
dozví	dozvědět	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Robin	robin	k2eAgMnSc1d1
nesnáší	snášet	k5eNaImIp3nP
obchoďáky	obchoďák	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
neví	vědět	k5eNaImIp3nS
proč	proč	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
na	na	k7c4
to	ten	k3xDgNnSc4
snaží	snažit	k5eAaImIp3nP
přijít	přijít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekne	říct	k5eAaPmIp3nS
to	ten	k3xDgNnSc4
tedy	tedy	k8xC
Barneymu	Barneym	k1gInSc2
a	a	k8xC
Marshallovi	Marshall	k1gMnSc3
a	a	k8xC
tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
vsadí	vsadit	k5eAaPmIp3nS
o	o	k7c4
facku	facka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Robin	Robina	k1gFnPc2
točila	točit	k5eAaImAgFnS
porno	porno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nS
to	ten	k3xDgNnSc1
nebyla	být	k5eNaImAgFnS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
by	by	kYmCp3nS
Barney	Barney	k1gInPc4
musel	muset	k5eAaImAgMnS
dostat	dostat	k5eAaPmF
facku	facka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
to	ten	k3xDgNnSc1
nebylo	být	k5eNaImAgNnS
porno	porno	k1gNnSc4
a	a	k8xC
všichni	všechen	k3xTgMnPc1
zjistí	zjistit	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Robin	Robina	k1gFnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
teenagerovskou	teenagerovský	k2eAgFnSc7d1
popovou	popový	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natočila	natočit	k5eAaBmAgFnS
píseň	píseň	k1gFnSc1
„	„	k?
<g/>
Let	léto	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Go	Go	k1gFnSc7
to	ten	k3xDgNnSc1
the	the	k?
Mall	Mall	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
měla	mít	k5eAaImAgFnS
turné	turné	k1gNnSc4
po	po	k7c6
nákupních	nákupní	k2eAgNnPc6d1
centrech	centrum	k1gNnPc6
a	a	k8xC
proto	proto	k8xC
je	on	k3xPp3gInPc4
nesnáší	snášet	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
tohoto	tento	k3xDgInSc2
objevu	objev	k1gInSc2
Barney	Barnea	k1gFnSc2
prohraje	prohrát	k5eAaPmIp3nS
sázku	sázka	k1gFnSc4
a	a	k8xC
Marshall	Marshall	k1gInSc4
získá	získat	k5eAaPmIp3nS
povolení	povolení	k1gNnSc4
beztrestně	beztrestně	k6eAd1
dát	dát	k5eAaPmF
Barneymu	Barneym	k1gInSc3
kdykoliv	kdykoliv	k6eAd1
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
pět	pět	k4xCc4
facek	facka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
řadě	řada	k1gFnSc6
využije	využít	k5eAaPmIp3nS
tohoto	tento	k3xDgNnSc2
privilegia	privilegium	k1gNnSc2
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
také	také	k9
poprvé	poprvé	k6eAd1
objeví	objevit	k5eAaPmIp3nS
Barneyho	Barney	k1gMnSc4
homosexuální	homosexuální	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
Afroameričan	Afroameričan	k1gMnSc1
James	James	k1gMnSc1
(	(	kIx(
<g/>
Wayne	Wayn	k1gInSc5
Brady	Brada	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInSc1d1
se	se	k3xPyFc4
rovněž	rovněž	k9
dozvědí	dozvědět	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Barney	Barnea	k1gFnPc1
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
je	být	k5eAaImIp3nS
televizní	televizní	k2eAgMnSc1d1
moderátor	moderátor	k1gMnSc1
Bob	Bob	k1gMnSc1
Barker	Barker	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
ho	on	k3xPp3gInSc4
přesvědčovala	přesvědčovat	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
vždy	vždy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
spolu	spolu	k6eAd1
sledovali	sledovat	k5eAaImAgMnP
televizní	televizní	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
The	The	k1gMnSc1
Price	Priec	k1gInSc2
Is	Is	k1gMnSc1
Right	Right	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barney	Barneum	k1gNnPc7
se	se	k3xPyFc4
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
zúčastní	zúčastnit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
série	série	k1gFnSc2
sdělí	sdělit	k5eAaPmIp3nS
Ted	Ted	k1gMnSc1
Barneymu	Barneym	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
Robin	Robina	k1gFnPc2
rozchází	rozcházet	k5eAaImIp3nP
–	–	k?
důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
mají	mít	k5eAaImIp3nP
jiný	jiný	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
svatbu	svatba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neřekli	říct	k5eNaPmAgMnP
to	ten	k3xDgNnSc1
nikomu	nikdo	k3yNnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
nechtěli	chtít	k5eNaImAgMnP
kazit	kazit	k5eAaImF
svatbu	svatba	k1gFnSc4
Lily	lít	k5eAaImAgInP
a	a	k8xC
Marshalla	Marshalla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
skončí	skončit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
Barney	Barnea	k1gFnSc2
poví	povědět	k5eAaPmIp3nS
Tedovi	Ted	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
opět	opět	k6eAd1
svobodní	svobodný	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Robin	robin	k2eAgMnSc1d1
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
z	z	k7c2
cesty	cesta	k1gFnSc2
po	po	k7c6
Argentině	Argentina	k1gFnSc6
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
ní	on	k3xPp3gFnSc7
přijede	přijet	k5eAaPmIp3nS
i	i	k9
její	její	k3xOp3gMnSc1
nový	nový	k2eAgMnSc1d1
partner	partner	k1gMnSc1
Gael	Gael	k1gMnSc1
(	(	kIx(
<g/>
Enrique	Enrique	k1gFnSc1
Iglesias	Iglesias	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
si	se	k3xPyFc3
postupně	postupně	k6eAd1
zvyká	zvykat	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
Robin	Robina	k1gFnPc2
jsou	být	k5eAaImIp3nP
již	již	k9
pouze	pouze	k6eAd1
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gInSc4
a	a	k8xC
Lily	lít	k5eAaImAgFnP
se	se	k3xPyFc4
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přestěhují	přestěhovat	k5eAaPmIp3nP
do	do	k7c2
vlastního	vlastní	k2eAgInSc2d1
bytu	byt	k1gInSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
zalíbí	zalíbit	k5eAaPmIp3nS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
takový	takový	k3xDgInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
si	se	k3xPyFc3
nemůžou	nemůžou	k?
dovolit	dovolit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gInSc1
tak	tak	k6eAd1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Lily	lít	k5eAaImAgInP
je	on	k3xPp3gFnPc4
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
nákupech	nákup	k1gInPc6
a	a	k8xC
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c1
kreditních	kreditní	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ale	ale	k8xC
naleznou	nalézt	k5eAaBmIp3nP,k5eAaPmIp3nP
byt	byt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
jim	on	k3xPp3gMnPc3
vyhovuje	vyhovovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
však	však	k9
ve	v	k7c6
špatné	špatný	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
a	a	k8xC
také	také	k9
špatně	špatně	k6eAd1
konstrukčně	konstrukčně	k6eAd1
vyřešený	vyřešený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barney	Barne	k1gMnPc7
v	v	k7c6
této	tento	k3xDgFnSc6
řadě	řada	k1gFnSc6
opět	opět	k6eAd1
dostane	dostat	k5eAaPmIp3nS
facku	facka	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
na	na	k7c4
den	den	k1gInSc4
díkůvzdání	díkůvzdání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Budoucí	budoucí	k2eAgMnSc1d1
Ted	Ted	k1gMnSc1
svým	svůj	k3xOyFgFnPc3
dětem	dítě	k1gFnPc3
v	v	k7c6
roce	rok	k1gInSc6
2030	#num#	k4
poví	povědět	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc4
matku	matka	k1gFnSc4
potkal	potkat	k5eAaPmAgMnS
během	během	k7c2
příhody	příhoda	k1gFnSc2
se	s	k7c7
žlutým	žlutý	k2eAgInSc7d1
deštníkem	deštník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
tento	tento	k3xDgMnSc1
deštník	deštník	k1gInSc4
našel	najít	k5eAaPmAgMnS
Ted	Ted	k1gMnSc1
v	v	k7c6
tanečním	taneční	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
večírku	večírek	k1gInSc6
ke	k	k7c3
dni	den	k1gInSc3
svatého	svatý	k2eAgMnSc2d1
Patrika	Patrik	k1gMnSc2
si	se	k3xPyFc3
jej	on	k3xPp3gMnSc4
vezme	vzít	k5eAaPmIp3nS
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
party	party	k1gFnSc6
byla	být	k5eAaImAgFnS
i	i	k9
jeho	jeho	k3xOp3gFnSc1
budoucí	budoucí	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
Ted	Ted	k1gMnSc1
ale	ale	k9
nepotkal	potkat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
začne	začít	k5eAaPmIp3nS
usilovat	usilovat	k5eAaImF
o	o	k7c4
dermatoložku	dermatoložka	k1gFnSc4
Stellu	Stella	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nS
při	při	k7c6
odstraňování	odstraňování	k1gNnSc6
svého	svůj	k3xOyFgNnSc2
tetování	tetování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
snaha	snaha	k1gFnSc1
vyvrcholí	vyvrcholit	k5eAaPmIp3nS
při	při	k7c6
„	„	k?
<g/>
dvouminutovém	dvouminutový	k2eAgNnSc6d1
rande	rande	k1gNnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
flirtování	flirtování	k1gNnSc4
<g/>
,	,	kIx,
večeři	večeře	k1gFnSc4
<g/>
,	,	kIx,
film	film	k1gInSc4
<g/>
,	,	kIx,
kávu	káva	k1gFnSc4
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
jízdy	jízda	k1gFnPc4
taxíkem	taxík	k1gInSc7
a	a	k8xC
polibek	polibek	k1gInSc4
na	na	k7c4
dobrou	dobrý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
–	–	k?
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
ve	v	k7c6
dvou	dva	k4xCgFnPc6
minutách	minuta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
vyspí	vyspat	k5eAaPmIp3nS
s	s	k7c7
Barneym	Barneym	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ji	on	k3xPp3gFnSc4
utěšuje	utěšovat	k5eAaImIp3nS
po	po	k7c6
rozchodu	rozchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
Teda	Ted	k1gMnSc2
rozzuří	rozzuřit	k5eAaPmIp3nS
a	a	k8xC
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
Barneym	Barneymum	k1gNnPc2
přestane	přestat	k5eAaPmIp3nS
přátelit	přátelit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
neznámá	známý	k2eNgFnSc1d1
žena	žena	k1gFnSc1
snaží	snažit	k5eAaImIp3nS
sabotovat	sabotovat	k5eAaImF
Barneyho	Barney	k1gMnSc4
pokusy	pokus	k1gInPc1
o	o	k7c4
flirtování	flirtování	k1gNnSc4
s	s	k7c7
ženami	žena	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sabotérem	sabotér	k1gMnSc7
byla	být	k5eAaImAgFnS
Abby	Abba	k1gFnPc4
(	(	kIx(
<g/>
Britney	Britney	k1gInPc4
Spears	Spears	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
recepční	recepční	k1gMnSc1
ze	z	k7c2
Stellyiny	Stellyin	k2eAgFnSc2d1
dermatologické	dermatologický	k2eAgFnSc2d1
ordinace	ordinace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
série	série	k1gFnSc2
se	se	k3xPyFc4
Ted	Ted	k1gMnSc1
a	a	k8xC
Barney	Barnea	k1gFnPc1
zúčastní	zúčastnit	k5eAaPmIp3nP
automobilových	automobilový	k2eAgFnPc2d1
nehod	nehoda	k1gFnPc2
a	a	k8xC
oba	dva	k4xCgMnPc1
skončí	skončit	k5eAaPmIp3nP
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
spřátelí	spřátelit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaleny	odhalen	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
Barneyho	Barney	k1gMnSc2
city	city	k1gNnSc2
k	k	k7c3
Robin	robin	k2eAgMnSc1d1
a	a	k8xC
Ted	Ted	k1gMnSc1
ke	k	k7c3
konci	konec	k1gInSc3
řady	řada	k1gFnSc2
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
ruku	ruka	k1gFnSc4
Stellu	Stella	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
Zredukovat	zredukovat	k5eAaPmF
na	na	k7c4
max	max	kA
<g/>
.	.	kIx.
3	#num#	k4
odstavce	odstavec	k1gInSc2
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
čtvrté	čtvrtá	k1gFnSc2
série	série	k1gFnSc1
Stella	Stella	k1gFnSc1
řekne	říct	k5eAaPmIp3nS
ano	ano	k9
na	na	k7c4
Tedovu	Tedův	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
o	o	k7c4
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
přijme	přijmout	k5eAaPmIp3nS
nové	nový	k2eAgNnSc4d1
zaměstnání	zaměstnání	k1gNnSc4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
rychle	rychle	k6eAd1
ale	ale	k8xC
z	z	k7c2
této	tento	k3xDgFnSc2
práce	práce	k1gFnSc2
odejde	odejít	k5eAaPmIp3nS
a	a	k8xC
vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
zúčastnit	zúčastnit	k5eAaPmF
Tedovy	Tedův	k2eAgFnPc4d1
svatby	svatba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ní	on	k3xPp3gFnSc6
Stella	Stella	k1gFnSc1
u	u	k7c2
oltáře	oltář	k1gInSc2
opustí	opustit	k5eAaPmIp3nP
Teda	Ted	k1gMnSc4
a	a	k8xC
vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
zpět	zpět	k6eAd1
k	k	k7c3
Tonymu	Tony	k1gMnSc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
otcem	otec	k1gMnSc7
její	její	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Barney	Barnea	k1gFnSc2
se	se	k3xPyFc4
potýká	potýkat	k5eAaImIp3nS
s	s	k7c7
city	city	k1gNnSc7
k	k	k7c3
Robin	robin	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
firma	firma	k1gFnSc1
mezitím	mezitím	k6eAd1
provede	provést	k5eAaPmIp3nS
novou	nový	k2eAgFnSc4d1
akvizici	akvizice	k1gFnSc4
<g/>
,	,	kIx,
Gigantickou	gigantický	k2eAgFnSc4d1
národní	národní	k2eAgFnSc4d1
banku	banka	k1gFnSc4
(	(	kIx(
<g/>
GNB	GNB	kA
<g/>
,	,	kIx,
v	v	k7c6
originále	originál	k1gInSc6
Goliath	Goliath	k1gMnSc1
National	National	k1gFnSc2
Bank	bank	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gInSc1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
začne	začít	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
také	také	k9
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
s	s	k7c7
Lily	lít	k5eAaImAgFnP
potřebují	potřebovat	k5eAaImIp3nP
peníze	peníz	k1gInPc4
na	na	k7c4
byt	byt	k1gInSc4
a	a	k8xC
také	také	k9
na	na	k7c4
zaplacení	zaplacení	k1gNnSc4
dluhů	dluh	k1gInPc2
z	z	k7c2
Lilyiných	Lilyin	k2eAgInPc2d1
nákupů	nákup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
pro	pro	k7c4
GNB	GNB	kA
začne	začít	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
i	i	k9
Ted	Ted	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
naskytne	naskytnout	k5eAaPmIp3nS
příležitost	příležitost	k1gFnSc4
být	být	k5eAaImF
architektem	architekt	k1gMnSc7
pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
novou	nový	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
několik	několik	k4yIc4
týdnech	týden	k1gInPc6
práce	práce	k1gFnPc1
ale	ale	k8xC
tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
ruší	rušit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
pak	pak	k6eAd1
Ted	Ted	k1gMnSc1
pokusí	pokusit	k5eAaPmIp3nS
založit	založit	k5eAaPmF
vlastní	vlastní	k2eAgFnSc4d1
firmu	firma	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nedaří	dařit	k5eNaImIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
získávat	získávat	k5eAaImF
klienty	klient	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Marshall	Marshall	k1gMnSc1
a	a	k8xC
Lily	lít	k5eAaImAgFnP
se	se	k3xPyFc4
přestěhují	přestěhovat	k5eAaPmIp3nP
do	do	k7c2
nového	nový	k2eAgInSc2d1
bytu	byt	k1gInSc2
a	a	k8xC
diskutují	diskutovat	k5eAaImIp3nP
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
či	či	k8xC
nejsou	být	k5eNaImIp3nP
připraveni	připraven	k2eAgMnPc1d1
si	se	k3xPyFc3
pořídit	pořídit	k5eAaPmF
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
Tedovou	Tedův	k2eAgFnSc7d1
spolubydlící	spolubydlící	k1gFnSc7
a	a	k8xC
přijme	přijmout	k5eAaPmIp3nS
pozici	pozice	k1gFnSc4
moderátorky	moderátorka	k1gFnSc2
zpráv	zpráva	k1gFnPc2
ve	v	k7c4
4	#num#	k4
hodiny	hodina	k1gFnSc2
ráno	ráno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
této	tento	k3xDgFnSc3
práci	práce	k1gFnSc3
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Barney	Barney	k1gInPc1
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
poslal	poslat	k5eAaPmAgMnS
její	její	k3xOp3gInSc4
videoživotopis	videoživotopis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
a	a	k8xC
Robin	robin	k2eAgMnSc1d1
se	se	k3xPyFc4
rozhodnou	rozhodnout	k5eAaPmIp3nP
spát	spát	k5eAaImF
spolu	spolu	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
překonali	překonat	k5eAaPmAgMnP
hádky	hádek	k1gInPc4
o	o	k7c6
svých	svůj	k3xOyFgInPc6
vzájemných	vzájemný	k2eAgInPc6d1
zlozvycích	zlozvyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barney	Barneum	k1gNnPc7
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
tyto	tento	k3xDgFnPc4
hádky	hádka	k1gFnPc4
zastavit	zastavit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
spolu	spolu	k6eAd1
nespali	spát	k5eNaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Ted	Ted	k1gMnSc1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Barney	Barney	k1gInPc1
je	být	k5eAaImIp3nS
do	do	k7c2
Robin	Robina	k1gFnPc2
zamilovaný	zamilovaný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ted	Ted	k1gMnSc1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Lily	lít	k5eAaImAgFnP
sabotovala	sabotovat	k5eAaImAgFnS
všechny	všechen	k3xTgInPc4
jeho	jeho	k3xOp3gInPc4
vztahy	vztah	k1gInPc4
s	s	k7c7
kýmkoliv	kdokoliv	k3yInSc7
<g/>
,	,	kIx,
koho	kdo	k3yQnSc4,k3yRnSc4,k3yInSc4
neschvalovala	schvalovat	k5eNaImAgFnS
<g/>
,	,	kIx,
nebo	nebo	k8xC
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
nepřímo	přímo	k6eNd1
mohl	moct	k5eAaImAgMnS
zapříčinit	zapříčinit	k5eAaPmF
rozchod	rozchod	k1gInSc4
Robin	robin	k2eAgInSc4d1
a	a	k8xC
Teda	Ted	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
a	a	k8xC
Ted	Ted	k1gMnSc1
diskutují	diskutovat	k5eAaImIp3nP
o	o	k7c6
těchto	tento	k3xDgInPc6
problémech	problém	k1gInPc6
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
tohoto	tento	k3xDgMnSc2
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc1
přátelství	přátelství	k1gNnSc1
posune	posunout	k5eAaPmIp3nS
do	do	k7c2
lepšího	dobrý	k2eAgInSc2d2
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Barney	Barnea	k1gFnPc1
konečně	konečně	k6eAd1
vyspí	vyspat	k5eAaPmIp3nP
s	s	k7c7
200	#num#	k4
<g/>
.	.	kIx.
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
začne	začít	k5eAaPmIp3nS
přemýšlet	přemýšlet	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
bude	být	k5eAaImBp3nS
dělat	dělat	k5eAaImF
zbytek	zbytek	k1gInSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ho	on	k3xPp3gNnSc4
ještě	ještě	k9
více	hodně	k6eAd2
ujistí	ujistit	k5eAaPmIp3nP
o	o	k7c6
jeho	jeho	k3xOp3gInPc6
citech	cit	k1gInPc6
k	k	k7c3
Robin	robin	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
Ted	Ted	k1gMnSc1
nese	nést	k5eAaImIp3nS
žlutý	žlutý	k2eAgInSc4d1
deštník	deštník	k1gInSc4
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
patřit	patřit	k5eAaImF
jeho	jeho	k3xOp3gFnSc3
budoucí	budoucí	k2eAgFnSc3d1
manželce	manželka	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
potká	potkat	k5eAaPmIp3nS
svoji	svůj	k3xOyFgFnSc4
bývalou	bývalý	k2eAgFnSc4d1
přítelkyni	přítelkyně	k1gFnSc4
Stellu	Stella	k1gFnSc4
a	a	k8xC
jejího	její	k3xOp3gMnSc4
nynějšího	nynější	k2eAgMnSc4d1
přítele	přítel	k1gMnSc4
Tonyho	Tony	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tony	Tony	k1gMnSc1
ho	on	k3xPp3gMnSc4
později	pozdě	k6eAd2
navštíví	navštívit	k5eAaPmIp3nS
a	a	k8xC
poděkuje	poděkovat	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
umožnil	umožnit	k5eAaPmAgMnS
být	být	k5eAaImF
znovu	znovu	k6eAd1
se	s	k7c7
Stellou	Stella	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídne	nabídnout	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
zaměstnání	zaměstnání	k1gNnSc1
profesora	profesor	k1gMnSc2
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
Ted	Ted	k1gMnSc1
ale	ale	k9
tuto	tento	k3xDgFnSc4
pozici	pozice	k1gFnSc4
odmítne	odmítnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
řady	řada	k1gFnSc2
Robin	Robina	k1gFnPc2
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
do	do	k7c2
ní	on	k3xPp3gFnSc2
Barney	Barnea	k1gFnSc2
zamilován	zamilován	k2eAgMnSc1d1
a	a	k8xC
souhlasí	souhlasit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
ním	on	k3xPp3gNnSc7
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
vztah	vztah	k1gInSc4
založený	založený	k2eAgInSc4d1
na	na	k7c6
sexu	sex	k1gInSc6
<g/>
,	,	kIx,
protože	protože	k8xS
by	by	kYmCp3nS
tak	tak	k6eAd1
spolu	spolu	k6eAd1
stejně	stejně	k6eAd1
skončili	skončit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
vzít	vzít	k5eAaPmF
práci	práce	k1gFnSc4
profesora	profesor	k1gMnSc2
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
usoudí	usoudit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
architektem	architekt	k1gMnSc7
teď	teď	k6eAd1
být	být	k5eAaImF
nemůže	moct	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
série	série	k1gFnSc2
se	se	k3xPyFc4
Ted	Ted	k1gMnSc1
připravuje	připravovat	k5eAaImIp3nS
na	na	k7c4
první	první	k4xOgFnSc4
hodinu	hodina	k1gFnSc4
a	a	k8xC
budoucí	budoucí	k2eAgMnSc1d1
Ted	Ted	k1gMnSc1
prozradí	prozradit	k5eAaPmIp3nS
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
že	že	k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
dívek	dívka	k1gFnPc2
v	v	k7c6
učebně	učebna	k1gFnSc6
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc1
matka	matka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pátá	pátý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Ted	Ted	k1gMnSc1
začíná	začínat	k5eAaImIp3nS
pracovat	pracovat	k5eAaImF
jako	jako	k9
profesor	profesor	k1gMnSc1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplně	úplně	k6eAd1
první	první	k4xOgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začíná	začínat	k5eAaImIp3nS
učit	učit	k5eAaImF
<g/>
,	,	kIx,
přijde	přijít	k5eAaPmIp3nS
omylem	omyl	k1gInSc7
do	do	k7c2
učebny	učebna	k1gFnSc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
právě	právě	k9
jeho	jeho	k3xOp3gFnSc1
budoucí	budoucí	k2eAgFnSc1d1
žena	žena	k1gFnSc1
a	a	k8xC
matka	matka	k1gFnSc1
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
celý	celý	k2eAgInSc4d1
příběh	příběh	k1gInSc4
vypráví	vyprávět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barney	Barnea	k1gFnSc2
a	a	k8xC
Robin	Robina	k1gFnPc2
už	už	k6eAd1
mají	mít	k5eAaImIp3nP
vztah	vztah	k1gInSc4
po	po	k7c4
celé	celý	k2eAgNnSc4d1
léto	léto	k1gNnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
je	on	k3xPp3gInPc4
Lily	lít	k5eAaImAgFnP
zamkne	zamknout	k5eAaPmIp3nS
v	v	k7c6
místnosti	místnost	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	on	k3xPp3gMnPc4
donutí	donutit	k5eAaPmIp3nS
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
vztahem	vztah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
rozejdou	rozejít	k5eAaPmIp3nP
a	a	k8xC
Barney	Barney	k1gInPc1
se	se	k3xPyFc4
zase	zase	k9
vrátí	vrátit	k5eAaPmIp3nS
ke	k	k7c3
svým	svůj	k3xOyFgInPc3
starým	starý	k2eAgInPc3d1
způsobům	způsob	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
se	se	k3xPyFc4
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
jedné	jeden	k4xCgFnSc2
dívky	dívka	k1gFnSc2
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
třídy	třída	k1gFnSc2
jménem	jméno	k1gNnSc7
Cindy	Cinda	k1gFnSc2
(	(	kIx(
<g/>
Rachel	Rachel	k1gMnSc1
Bilson	Bilson	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
spolubydlící	spolubydlící	k1gMnPc4
Tedovy	Tedův	k2eAgFnSc2d1
budoucí	budoucí	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
se	se	k3xPyFc4
setká	setkat	k5eAaPmIp3nS
s	s	k7c7
novým	nový	k2eAgMnSc7d1
kolegou	kolega	k1gMnSc7
Donem	Don	k1gMnSc7
a	a	k8xC
ačkoliv	ačkoliv	k8xS
ho	on	k3xPp3gMnSc4
Robin	Robina	k1gFnPc2
nejprve	nejprve	k6eAd1
nesnáší	snášet	k5eNaImIp3nS
<g/>
,	,	kIx,
později	pozdě	k6eAd2
s	s	k7c7
ním	on	k3xPp3gMnSc7
začne	začít	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
řady	řada	k1gFnSc2
se	se	k3xPyFc4
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
,	,	kIx,
protože	protože	k8xS
Don	Don	k1gMnSc1
vzal	vzít	k5eAaPmAgMnS
práci	práce	k1gFnSc4
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
předtím	předtím	k6eAd1
Robin	robin	k2eAgInSc4d1
právě	právě	k6eAd1
kvůli	kvůli	k7c3
Donovi	Don	k1gMnSc3
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gInSc1
použije	použít	k5eAaPmIp3nS
během	během	k7c2
série	série	k1gFnSc2
svou	svůj	k3xOyFgFnSc4
čtvrtou	čtvrtý	k4xOgFnSc4
facku	facka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ted	Ted	k1gMnSc1
si	se	k3xPyFc3
koupí	koupit	k5eAaPmIp3nS
dům	dům	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
nakonec	nakonec	k6eAd1
bude	být	k5eAaImBp3nS
žít	žít	k5eAaImF
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
budoucí	budoucí	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lily	lít	k5eAaImAgInP
a	a	k8xC
Marshall	Marshall	k1gInSc1
si	se	k3xPyFc3
pořád	pořád	k6eAd1
nejsou	být	k5eNaImIp3nP
jisti	jist	k2eAgMnPc1d1
<g/>
,	,	kIx,
jestli	jestli	k8xS
je	být	k5eAaImIp3nS
dobrý	dobrý	k2eAgInSc1d1
nápad	nápad	k1gInSc1
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
až	až	k9
uvidí	uvidět	k5eAaPmIp3nS
Barneyho	Barney	k1gMnSc4
dvojníka	dvojník	k1gMnSc4
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
si	se	k3xPyFc3
děti	dítě	k1gFnPc1
pořídí	pořídit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Šestá	šestý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
šesté	šestý	k4xOgFnSc2
řady	řada	k1gFnSc2
se	se	k3xPyFc4
GNB	GNB	kA
nakonec	nakonec	k6eAd1
rozhodne	rozhodnout	k5eAaPmIp3nS
postavit	postavit	k5eAaPmF
budovu	budova	k1gFnSc4
a	a	k8xC
najmout	najmout	k5eAaPmF
Teda	Ted	k1gMnSc4
jako	jako	k8xC,k8xS
architekta	architekt	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
novou	nový	k2eAgFnSc7d1
přítelkyní	přítelkyně	k1gFnSc7
Zoey	Zoea	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
novou	nový	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
bojkotuje	bojkotovat	k5eAaImIp3nS
kvůli	kvůli	k7c3
hotelu	hotel	k1gInSc3
Arcadian	Arcadiana	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
místě	místo	k1gNnSc6
má	mít	k5eAaImIp3nS
vyrůst	vyrůst	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Lily	lít	k5eAaImAgFnP
a	a	k8xC
Marshall	Marshall	k1gMnSc1
se	se	k3xPyFc4
pokoušejí	pokoušet	k5eAaImIp3nP
o	o	k7c4
dítě	dítě	k1gNnSc4
a	a	k8xC
Marshallovi	Marshall	k1gMnSc3
zemře	zemřít	k5eAaPmIp3nS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
evidentně	evidentně	k6eAd1
stále	stále	k6eAd1
něco	něco	k3yInSc4
cítí	cítit	k5eAaImIp3nS
k	k	k7c3
Barneymu	Barneym	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
v	v	k7c6
nedaleké	daleký	k2eNgFnSc6d1
budoucnosti	budoucnost	k1gFnSc6
ženit	ženit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
této	tento	k3xDgFnSc2
série	série	k1gFnSc2
Lily	lít	k5eAaImAgFnP
otěhotní	otěhotnět	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
v	v	k7c6
několika	několik	k4yIc6
dílech	dílo	k1gNnPc6
sezóny	sezóna	k1gFnSc2
si	se	k3xPyFc3
zahrály	zahrát	k5eAaPmAgFnP
známé	známý	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
třeba	třeba	k6eAd1
zpěvačka	zpěvačka	k1gFnSc1
Katy	Kata	k1gFnSc2
Perry	Perra	k1gFnSc2
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
Jorge	Jorge	k1gNnSc2
Garcia	Garcium	k1gNnSc2
nebo	nebo	k8xC
také	také	k9
Jennifer	Jennifer	k1gMnSc1
Morrison	Morrison	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ztvárnila	ztvárnit	k5eAaPmAgFnS
Zoey	Zoea	k1gFnPc4
<g/>
,	,	kIx,
Tedovu	Tedův	k2eAgFnSc4d1
dívku	dívka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
řada	řada	k1gFnSc1
seriálu	seriál	k1gInSc2
začíná	začínat	k5eAaImIp3nS
náhledem	náhled	k1gInSc7
do	do	k7c2
budoucnosti	budoucnost	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
Ted	Ted	k1gMnSc1
pomáhá	pomáhat	k5eAaImIp3nS
Barneymu	Barneymum	k1gNnSc3
při	při	k7c6
přípravě	příprava	k1gFnSc6
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
svatbu	svatba	k1gFnSc4
se	s	k7c7
stále	stále	k6eAd1
neznámou	známý	k2eNgFnSc7d1
nevěstou	nevěsta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
získá	získat	k5eAaPmIp3nS
Marshall	Marshall	k1gMnSc1
práci	práce	k1gFnSc4
jako	jako	k9
právníka	právník	k1gMnSc4
specializovaného	specializovaný	k2eAgMnSc4d1
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
Lily	lít	k5eAaImAgFnP
je	on	k3xPp3gFnPc4
stále	stále	k6eAd1
těhotná	těhotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barney	Barney	k1gInPc4
slíbí	slíbit	k5eAaPmIp3nS
Noře	Nora	k1gFnSc3
(	(	kIx(
<g/>
Nazanin	Nazanina	k1gFnPc2
Boniadi	Boniad	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
tím	ten	k3xDgMnSc7
správným	správný	k2eAgMnSc7d1
přítelem	přítel	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
přesto	přesto	k8xC
Robin	robin	k2eAgMnSc1d1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stále	stále	k6eAd1
něco	něco	k6eAd1
k	k	k7c3
Barneymu	Barneym	k1gInSc3
cítí	cítit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
tedy	tedy	k9
přistoupí	přistoupit	k5eAaPmIp3nP
na	na	k7c6
psychoterapii	psychoterapie	k1gFnSc6
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
pozná	poznat	k5eAaPmIp3nS
terapeuta	terapeut	k1gMnSc4
Kevina	Kevin	k1gMnSc4
(	(	kIx(
<g/>
Kal	kal	k1gInSc1
Penn	Penn	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
později	pozdě	k6eAd2
začne	začít	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
hurikánu	hurikán	k1gInSc2
Irena	Irena	k1gFnSc1
skupina	skupina	k1gFnSc1
přátel	přítel	k1gMnPc2
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Marshall	Marshall	k1gInSc4
a	a	k8xC
Lily	lít	k5eAaImAgFnP
počali	počnout	k5eAaPmAgMnP
svoje	svůj	k3xOyFgNnSc4
dítě	dítě	k1gNnSc4
v	v	k7c6
Barneyho	Barney	k1gMnSc2
bytě	byt	k1gInSc6
a	a	k8xC
že	že	k8xS
Barney	Barnea	k1gFnSc2
a	a	k8xC
Robin	Robina	k1gFnPc2
spolu	spolu	k6eAd1
přestali	přestat	k5eAaPmAgMnP
spát	spát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
začnou	začít	k5eAaPmIp3nP
přemýšlet	přemýšlet	k5eAaImF
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
budou	být	k5eAaImBp3nP
dělat	dělat	k5eAaImF
se	s	k7c7
svým	svůj	k3xOyFgInSc7
neurčitým	určitý	k2eNgInSc7d1
vztahem	vztah	k1gInSc7
–	–	k?
rozhodnou	rozhodnout	k5eAaPmIp3nP
se	se	k3xPyFc4
rozejít	rozejít	k5eAaPmF
se	se	k3xPyFc4
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
partnery	partner	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
rozejde	rozejít	k5eAaPmIp3nS
jenom	jenom	k9
Barney	Barnea	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Robin	robin	k2eAgMnSc1d1
nadále	nadále	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
Kevinem	Kevin	k1gMnSc7
<g/>
,	,	kIx,
což	což	k9
Barneyho	Barney	k1gMnSc4
ničí	ničit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c4
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
také	také	k9
Marshall	Marshall	k1gMnSc1
a	a	k8xC
Lily	lít	k5eAaImAgFnP
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přestěhují	přestěhovat	k5eAaPmIp3nP
do	do	k7c2
domu	dům	k1gInSc2
na	na	k7c4
Long	Long	k1gInSc4
Islandu	Island	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
patří	patřit	k5eAaImIp3nS
Lilyiným	Lilyin	k2eAgMnPc3d1
prarodičům	prarodič	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Robin	robin	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
řadě	řada	k1gFnSc6
obává	obávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
otěhotněla	otěhotnět	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
sdělí	sdělit	k5eAaPmIp3nS
Barneymu	Barneym	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
dítě	dítě	k1gNnSc1
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
jeho	jeho	k3xOp3gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
s	s	k7c7
Kevinem	Kevin	k1gInSc7
prozatím	prozatím	k6eAd1
nespala	spát	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
planý	planý	k2eAgInSc1d1
poplach	poplach	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
od	od	k7c2
gynekoložky	gynekoložka	k1gFnSc2
dozví	dozvědět	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemůže	moct	k5eNaImIp3nS
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jí	on	k3xPp3gFnSc3
ublíží	ublížit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kevin	Kevin	k1gMnSc1
slíbí	slíbit	k5eAaPmIp3nS
Robin	robin	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
miluje	milovat	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
vztahu	vztah	k1gInSc6
připraven	připraven	k2eAgMnSc1d1
postoupit	postoupit	k5eAaPmF
dále	daleko	k6eAd2
<g/>
,	,	kIx,
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
mu	on	k3xPp3gMnSc3
však	však	k9
sdělí	sdělit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemůže	moct	k5eNaImIp3nS
mít	mít	k5eAaImF
a	a	k8xC
ani	ani	k8xC
nechce	chtít	k5eNaImIp3nS
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzo	brzo	k6eAd1
poté	poté	k6eAd1
sdělí	sdělit	k5eAaPmIp3nP
Tedovi	Tedův	k2eAgMnPc1d1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
všechno	všechen	k3xTgNnSc1
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
Ted	Ted	k1gMnSc1
usoudí	usoudit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
má	mít	k5eAaImIp3nS
stále	stále	k6eAd1
rád	rád	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robin	Robina	k1gFnPc2
mu	on	k3xPp3gMnSc3
však	však	k9
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
nemiluje	milovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Barney	Barnea	k1gFnPc4
začne	začít	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
s	s	k7c7
Quinn	Quinna	k1gFnPc2
(	(	kIx(
<g/>
Becki	Beck	k1gFnSc2
Newton	Newton	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
poznal	poznat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
striptérku	striptérka	k1gFnSc4
Karmu	karma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
ji	on	k3xPp3gFnSc4
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Osmá	osmý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Marshall	Marshall	k1gMnSc1
a	a	k8xC
Lily	lít	k5eAaImAgFnP
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
najít	najít	k5eAaPmF
chůvu	chůva	k1gFnSc4
pro	pro	k7c4
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
padne	padnout	k5eAaImIp3nS,k5eAaPmIp3nS
volba	volba	k1gFnSc1
na	na	k7c4
Teda	Ted	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
souhlasí	souhlasit	k5eAaImIp3nS
a	a	k8xC
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
na	na	k7c6
hlídaní	hlídaný	k2eAgMnPc1d1
těší	těšit	k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
tak	tak	k6eAd1
podvědomě	podvědomě	k6eAd1
nahrazuje	nahrazovat	k5eAaImIp3nS
dítě	dítě	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
ale	ale	k9
Lily	lít	k5eAaImAgFnP
s	s	k7c7
Marshallem	Marshallo	k1gNnSc7
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
už	už	k6eAd1
mu	on	k3xPp3gMnSc3
dítě	dítě	k1gNnSc1
svěřovat	svěřovat	k5eAaImF
nebudou	být	k5eNaImBp3nP
<g/>
,	,	kIx,
protože	protože	k8xS
Ted	Ted	k1gMnSc1
mu	on	k3xPp3gMnSc3
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
až	až	k9
nezdravě	zdravě	k6eNd1
obětavou	obětavý	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lily	lít	k5eAaImAgInP
začne	začít	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
pro	pro	k7c4
kapitána	kapitán	k1gMnSc4
<g/>
,	,	kIx,
bývalého	bývalý	k2eAgMnSc4d1
manžela	manžel	k1gMnSc4
Zoey	Zoea	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
poradkyně	poradkyně	k1gFnSc1
pro	pro	k7c4
umění	umění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostane	dostat	k5eAaPmIp3nS
od	od	k7c2
něj	on	k3xPp3gInSc2
nabídku	nabídka	k1gFnSc4
jet	jet	k5eAaImF
na	na	k7c4
rok	rok	k1gInSc4
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsně	těsně	k6eAd1
před	před	k7c7
odjezdem	odjezd	k1gInSc7
dostane	dostat	k5eAaPmIp3nS
Marshall	Marshall	k1gMnSc1
nabídku	nabídka	k1gFnSc4
začít	začít	k5eAaPmF
jako	jako	k9
soudce	soudce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Barney	Barnea	k1gFnPc1
a	a	k8xC
Quinn	Quinn	k1gNnSc1
se	se	k3xPyFc4
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
je	být	k5eAaImIp3nS
Barney	Barne	k2eAgInPc1d1
zničený	zničený	k2eAgMnSc1d1
a	a	k8xC
podvědomě	podvědomě	k6eAd1
si	se	k3xPyFc3
hledá	hledat	k5eAaImIp3nS
až	až	k9
příliš	příliš	k6eAd1
bizarní	bizarní	k2eAgMnPc4d1
parťáky	parťák	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzo	brzo	k6eAd1
si	se	k3xPyFc3
ale	ale	k9
uvědomí	uvědomit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
miluje	milovat	k5eAaImIp3nS
Robin	robin	k2eAgMnSc1d1
a	a	k8xC
pomocí	pomocí	k7c2
rafinovaného	rafinovaný	k2eAgInSc2d1
plánu	plán	k1gInSc2
ji	on	k3xPp3gFnSc4
získá	získat	k5eAaPmIp3nS
<g/>
;	;	kIx,
zasnoubí	zasnoubit	k5eAaPmIp3nS
se	se	k3xPyFc4
spolu	spolu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
Tedův	Tedův	k2eAgInSc4d1
večírek	večírek	k1gInSc4
se	s	k7c7
slavnostním	slavnostní	k2eAgNnSc7d1
představením	představení	k1gNnSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
nového	nový	k2eAgInSc2d1
mrakodrapu	mrakodrap	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barney	Barnea	k1gFnSc2
a	a	k8xC
Robin	Robina	k1gFnPc2
poté	poté	k6eAd1
připravují	připravovat	k5eAaImIp3nP
svatbu	svatba	k1gFnSc4
a	a	k8xC
Ted	Ted	k1gMnSc1
jim	on	k3xPp3gMnPc3
s	s	k7c7
tím	ten	k3xDgNnSc7
pomáhá	pomáhat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohadují	dohadovat	k5eAaImIp3nP
se	se	k3xPyFc4
jestli	jestli	k9
DJ	DJ	kA
nebo	nebo	k8xC
kapela	kapela	k1gFnSc1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
jim	on	k3xPp3gMnPc3
ale	ale	k9
pár	pár	k4xCyI
dnů	den	k1gInPc2
před	před	k7c7
svatbou	svatba	k1gFnSc7
to	ten	k3xDgNnSc1
odřekne	odřeknout	k5eAaPmIp3nS
a	a	k8xC
Ted	Ted	k1gMnSc1
narychlo	narychlo	k6eAd1
někoho	někdo	k3yInSc4
shání	shánět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najme	najmout	k5eAaPmIp3nS
proto	proto	k8xC
kapelu	kapela	k1gFnSc4
svojí	svůj	k3xOyFgFnSc2
budoucí	budoucí	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
končí	končit	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
odjíždí	odjíždět	k5eAaImIp3nP
na	na	k7c4
svatbu	svatba	k1gFnSc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
jede	jet	k5eAaImIp3nS
i	i	k9
Tedova	Tedův	k2eAgFnSc1d1
budoucí	budoucí	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nám	my	k3xPp1nPc3
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
ukázána	ukázán	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cristin	Cristina	k1gFnPc2
Milioti	Miliot	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Devátá	devátý	k4xOgFnSc1
řada	řada	k1gFnSc1
</s>
<s>
Celý	celý	k2eAgInSc4d1
děj	děj	k1gInSc4
deváté	devátý	k4xOgFnSc2
řady	řada	k1gFnSc2
<g/>
,	,	kIx,
vyjma	vyjma	k7c2
posledního	poslední	k2eAgInSc2d1
dílu	díl	k1gInSc2
<g/>
,	,	kIx,
probíhá	probíhat	k5eAaImIp3nS
během	během	k7c2
posledních	poslední	k2eAgFnPc2d1
56	#num#	k4
hodin	hodina	k1gFnPc2
před	před	k7c7
svatbou	svatba	k1gFnSc7
Barneyho	Barney	k1gMnSc2
a	a	k8xC
Robin	Robina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
ve	v	k7c6
fiktivní	fiktivní	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
Farhampton	Farhampton	k1gInSc1
v	v	k7c6
the	the	k?
Hamptons	Hamptons	k1gInSc1
na	na	k7c4
Long	Long	k1gInSc4
Islandu	Island	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lily	lít	k5eAaImAgInP
s	s	k7c7
Marshallem	Marshall	k1gInSc7
se	se	k3xPyFc4
chystají	chystat	k5eAaImIp3nP
na	na	k7c4
rok	rok	k1gInSc4
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bude	být	k5eAaImBp3nS
Lily	lít	k5eAaImAgFnP
pracovat	pracovat	k5eAaImF
jako	jako	k9
výtvarná	výtvarný	k2eAgFnSc1d1
poradkyně	poradkyně	k1gFnSc1
kapitána	kapitán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
svatbou	svatba	k1gFnSc7
odjede	odjet	k5eAaPmIp3nS
Marshall	Marshall	k1gInSc1
s	s	k7c7
Marvinem	Marvin	k1gInSc7
do	do	k7c2
Minnesoty	Minnesota	k1gFnSc2
za	za	k7c7
Marshallovou	Marshallův	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshalla	k1gFnPc2
ale	ale	k8xC
dostane	dostat	k5eAaPmIp3nS
nabídku	nabídka	k1gFnSc4
práce	práce	k1gFnSc2
soudce	soudce	k1gMnSc2
a	a	k8xC
tak	tak	k6eAd1
ho	on	k3xPp3gMnSc4
čeká	čekat	k5eAaImIp3nS
velmi	velmi	k6eAd1
peprný	peprný	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
s	s	k7c7
Lily	lít	k5eAaImAgInP
<g/>
,	,	kIx,
jestli	jestli	k8xS
zamíří	zamířit	k5eAaPmIp3nS
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gMnSc1
nestihne	stihnout	k5eNaPmIp3nS
letadlo	letadlo	k1gNnSc4
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
musí	muset	k5eAaImIp3nS
dopravit	dopravit	k5eAaPmF
autem	aut	k1gInSc7
s	s	k7c7
nesympatickou	sympatický	k2eNgFnSc7d1
spolujezdkyní	spolujezdkyně	k1gFnSc7
Dafné	Dafná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
dostane	dostat	k5eAaPmIp3nS
včas	včas	k6eAd1
a	a	k8xC
svatbu	svatba	k1gFnSc4
stihne	stihnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devátá	devátý	k4xOgFnSc1
řada	řada	k1gFnSc1
vrcholí	vrcholit	k5eAaImIp3nS
svatbou	svatba	k1gFnSc7
Barneyho	Barney	k1gMnSc2
a	a	k8xC
Robin	Robina	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
přinese	přinést	k5eAaPmIp3nS
snubní	snubní	k2eAgInPc4d1
prstýnky	prstýnek	k1gInPc4
medvěd	medvěd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kapele	kapela	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
na	na	k7c4
svatbu	svatba	k1gFnSc4
sehnal	sehnat	k5eAaPmAgMnS
Ted	Ted	k1gMnSc1
<g/>
,	,	kIx,
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
baskytaru	baskytara	k1gFnSc4
„	„	k?
<g/>
Matka	matka	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
právě	právě	k9
tam	tam	k6eAd1
ji	on	k3xPp3gFnSc4
Ted	Ted	k1gMnSc1
poprvé	poprvé	k6eAd1
uvidí	uvidět	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechce	chtít	k5eNaImIp3nS
si	se	k3xPyFc3
s	s	k7c7
ní	on	k3xPp3gFnSc7
ale	ale	k8xC
nic	nic	k3yNnSc1
začínat	začínat	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
po	po	k7c6
svatbě	svatba	k1gFnSc6
má	mít	k5eAaImIp3nS
v	v	k7c6
plánu	plán	k1gInSc6
se	se	k3xPyFc4
odstěhovat	odstěhovat	k5eAaPmF
do	do	k7c2
Chicaga	Chicago	k1gNnSc2
a	a	k8xC
začít	začít	k5eAaPmF
tam	tam	k6eAd1
nový	nový	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
čekání	čekání	k1gNnSc6
na	na	k7c4
vlak	vlak	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
zpoždění	zpoždění	k1gNnSc4
45	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
však	však	k9
poznává	poznávat	k5eAaImIp3nS
matku	matka	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
žlutý	žlutý	k2eAgInSc4d1
deštník	deštník	k1gInSc4
a	a	k8xC
navzájem	navzájem	k6eAd1
zjišťují	zjišťovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
znají	znát	k5eAaImIp3nP
víc	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
mysleli	myslet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Herci	herec	k1gMnPc1
na	na	k7c6
oslavě	oslava	k1gFnSc6
100	#num#	k4
<g/>
.	.	kIx.
dílu	dílo	k1gNnSc3
seriálu	seriál	k1gInSc2
<g/>
,	,	kIx,
zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Neil	Neil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc1
<g/>
,	,	kIx,
Cobie	Cobie	k1gFnSc1
Smulders	Smulders	k1gInSc1
<g/>
,	,	kIx,
Jason	Jason	k1gMnSc1
Segel	Segel	k1gMnSc1
<g/>
,	,	kIx,
Alyson	Alyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
a	a	k8xC
Josh	Josh	k1gMnSc1
Radnor	Radnor	k1gMnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Josh	Josh	k1gMnSc1
Radnor	Radnor	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Písařík	písařík	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Ted	Ted	k1gMnSc1
Mosby	Mosba	k1gFnSc2
</s>
<s>
Jason	Jason	k1gMnSc1
Segel	Segel	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Michal	Michal	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Marshall	Marshall	k1gInSc1
Eriksen	Eriksna	k1gFnPc2
</s>
<s>
Cobie	Cobie	k1gFnSc1
Smulders	Smuldersa	k1gFnPc2
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Tereza	Tereza	k1gFnSc1
Chudobová	Chudobová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Robin	Robina	k1gFnPc2
Scherbatská	Scherbatský	k2eAgFnSc1d1
</s>
<s>
Neil	Neil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
David	David	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Barney	Barnea	k1gFnPc1
Stinson	Stinsona	k1gFnPc2
</s>
<s>
Alyson	Alyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Andrea	Andrea	k1gFnSc1
Elsnerová	Elsnerová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Lily	lít	k5eAaImAgFnP
Aldrinová	Aldrinová	k1gFnSc1
</s>
<s>
Cristin	Cristin	k1gInSc4
Milioti	Miliot	k1gMnPc1
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
:	:	kIx,
Pavlína	Pavlína	k1gFnSc1
Dytrtová	Dytrtová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k9
Tracy	Traca	k1gFnSc2
McConnellová	McConnellový	k2eAgFnSc1d1
/	/	kIx~
Matka	matka	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
„	„	k?
<g/>
The	The	k1gFnSc2
Mother	Mothra	k1gFnPc2
<g/>
“	“	k?
<g/>
;	;	kIx,
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
řadě	řada	k1gFnSc6
jako	jako	k8xS,k8xC
host	host	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vedlejší	vedlejší	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Bob	Bob	k1gMnSc1
Saget	Saget	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Otakar	Otakar	k1gMnSc1
Brousek	brousek	k1gInSc4
ml.	ml.	kA
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
a	a	k8xC
Tomáš	Tomáš	k1gMnSc1
Juřička	Juřička	k1gMnSc1
[	[	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
vypravěč	vypravěč	k1gMnSc1
/	/	kIx~
starší	starý	k2eAgMnSc1d2
Ted	Ted	k1gMnSc1
Mosby	Mosba	k1gFnSc2
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
Henrie	Henrie	k1gFnSc2
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
:	:	kIx,
Radek	Radek	k1gMnSc1
Škvor	Škvor	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
a	a	k8xC
Jan	Jan	k1gMnSc1
Škvor	Škvor	k1gMnSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Luke	Luke	k1gFnSc1
Mosby	Mosba	k1gFnSc2
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
staršího	starý	k2eAgMnSc2d2
Teda	Ted	k1gMnSc2
Mosbyho	Mosby	k1gMnSc2
</s>
<s>
Lyndsy	Lynds	k1gInPc1
Fonseca	Fonsecum	k1gNnSc2
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Kateřina	Kateřina	k1gFnSc1
Velebová	Velebová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Penny	penny	k1gFnSc1
Mosbyová	Mosbyová	k1gFnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
staršího	starý	k2eAgMnSc2d2
Teda	Ted	k1gMnSc2
Mosbyho	Mosby	k1gMnSc2
</s>
<s>
Marshall	Marshall	k1gMnSc1
Manesh	Manesh	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Prager	Prager	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
a	a	k8xC
Jan	Jan	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
[	[	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Ranjit	Ranjit	k2eAgInSc1d1
Singh	Singh	k1gInSc1
</s>
<s>
Cristine	Cristin	k1gMnSc5
Rose	Rose	k1gMnSc5
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Obermaierová	Obermaierová	k1gFnSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
a	a	k8xC
Radana	Radana	k1gFnSc1
Herrmannová	Herrmannová	k1gFnSc1
[	[	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Virginia	Virginium	k1gNnSc2
Mosbyová	Mosbyová	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ashley	Ashley	k1gInPc1
Williams	Williamsa	k1gFnPc2
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Nikola	Nikola	k1gFnSc1
Votočková	Votočková	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k9
Victoria	Victorium	k1gNnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Wayne	Waynout	k5eAaPmIp3nS,k5eAaImIp3nS
Brady	brada	k1gFnPc4
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
James	James	k1gMnSc1
Stinson	Stinson	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sarah	Sarah	k1gFnSc1
Chalke	Chalke	k1gFnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Lucie	Lucie	k1gFnSc1
Benešová	Benešová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Stella	Stella	k1gFnSc1
Zinmanová	Zinmanová	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Koldyke	Koldyke	k1gNnSc2
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Hruška	Hruška	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Don	Don	k1gMnSc1
Frank	Frank	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Joe	Joe	k?
Nieves	Nieves	k1gInSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Radek	Radek	k1gMnSc1
Hoppe	Hopp	k1gInSc5
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Carl	Carl	k1gMnSc1
MacLaren	MacLarna	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Jennifer	Jennifer	k1gMnSc1
Morrison	Morrison	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Anna	Anna	k1gFnSc1
Remková	Remková	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Zoey	Zoea	k1gFnSc2
Piersonová	Piersonová	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nazanin	Nazanin	k2eAgMnSc1d1
Boniadi	Boniad	k1gMnPc1
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
:	:	kIx,
Laďka	Laďka	k1gFnSc1
Něrgešová	Něrgešová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Nora	Nora	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kal	kal	k1gInSc1
Penn	Penn	k1gInSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Rašilov	Rašilov	k1gInSc1
[	[	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
a	a	k8xC
Filip	Filip	k1gMnSc1
Jančík	Jančík	k1gMnSc1
[	[	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Kevin	Kevin	k1gMnSc1
Venkataraghavan	Venkataraghavan	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Produkce	produkce	k1gFnSc1
</s>
<s>
Carter	Carter	k1gInSc1
Bays	Bays	k1gInSc1
a	a	k8xC
Craig	Craig	k1gInSc1
Thomas	Thomas	k1gMnSc1
měli	mít	k5eAaImAgMnP
při	při	k7c6
vymýšlení	vymýšlení	k1gNnSc6
seriálu	seriál	k1gInSc2
Jak	jak	k8xS,k8xC
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
základní	základní	k2eAgFnSc4d1
ideu	idea	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Napíšeme	napsat	k5eAaPmIp1nP,k5eAaBmIp1nP
o	o	k7c6
nás	my	k3xPp1nPc6
<g/>
,	,	kIx,
našich	náš	k3xOp1gMnPc6
přátelích	přítel	k1gMnPc6
a	a	k8xC
hloupých	hloupý	k2eAgFnPc6d1
činnostech	činnost	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
jsme	být	k5eAaImIp1nP
dělali	dělat	k5eAaImAgMnP
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
tvorbě	tvorba	k1gFnSc6
základních	základní	k2eAgNnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
postav	postava	k1gFnPc2
vycházela	vycházet	k5eAaImAgFnS
dvojice	dvojice	k1gFnSc1
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
–	–	k?
postava	postava	k1gFnSc1
Teda	Ted	k1gMnSc2
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
Baysovi	Bays	k1gMnSc6
<g/>
,	,	kIx,
postavy	postava	k1gFnPc4
Marshalla	Marshallo	k1gNnSc2
a	a	k8xC
Lily	lít	k5eAaImAgFnP
na	na	k7c6
Thomasovi	Thomas	k1gMnSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
manželce	manželka	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Thomasova	Thomasův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
zprvu	zprvu	k6eAd1
nesouhlasila	souhlasit	k5eNaImAgFnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
seriálu	seriál	k1gInSc6
<g/>
,	,	kIx,
byť	byť	k8xS
nepřímo	přímo	k6eNd1
<g/>
,	,	kIx,
objevila	objevit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
změnila	změnit	k5eAaPmAgFnS
názor	názor	k1gInSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc4
postavu	postava	k1gFnSc4
může	moct	k5eAaImIp3nS
hrát	hrát	k5eAaImF
Alyson	Alyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
hledala	hledat	k5eAaImAgFnS
roli	role	k1gFnSc4
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1
také	také	k9
chtěli	chtít	k5eAaImAgMnP
do	do	k7c2
seriálu	seriál	k1gInSc2
umístit	umístit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
oblíbený	oblíbený	k2eAgInSc4d1
podnik	podnik	k1gInSc4
McGee	McGe	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
v	v	k7c6
New	New	k1gFnPc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
část	část	k1gFnSc1
seriálu	seriál	k1gInSc2
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
MacLarenově	MacLarenův	k2eAgInSc6d1
baru	bar	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
MacLaren	MacLarna	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Jméno	jméno	k1gNnSc1
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
majitele	majitel	k1gMnSc2
a	a	k8xC
barmana	barman	k1gMnSc2
Carla	Carl	k1gMnSc2
McLarena	McLareno	k1gNnSc2
si	se	k3xPyFc3
vypůjčili	vypůjčit	k5eAaPmAgMnP
od	od	k7c2
stejnojmenného	stejnojmenný	k2eAgMnSc2d1
Baysova	Baysův	k2eAgMnSc2d1
asistenta	asistent	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Thomas	Thomas	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Ted	Ted	k1gMnSc1
je	být	k5eAaImIp3nS
nedůvěryhodným	důvěryhodný	k2eNgMnSc7d1
vypravěčem	vypravěč	k1gMnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
vypráví	vyprávět	k5eAaImIp3nS
o	o	k7c6
věcech	věc	k1gFnPc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
před	před	k7c4
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
dvaceti	dvacet	k4xCc7
lety	léto	k1gNnPc7
a	a	k8xC
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
tendenci	tendence	k1gFnSc4
některé	některý	k3yIgFnSc2
události	událost	k1gFnSc2
vyprávět	vyprávět	k5eAaImF
zkresleně	zkresleně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
zkreslení	zkreslení	k1gNnSc1
bylo	být	k5eAaImAgNnS
součástí	součást	k1gFnSc7
některých	některý	k3yIgFnPc2
epizod	epizoda	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
„	„	k?
<g/>
Jak	jak	k8xS,k8xC
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgMnS
všechny	všechen	k3xTgInPc4
ostatní	ostatní	k2eAgInPc4d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Koza	koza	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Ale	ale	k9
<g/>
,	,	kIx,
zlato	zlato	k1gNnSc1
<g/>
…	…	k?
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Teorie	teorie	k1gFnSc1
o	o	k7c6
mořské	mořský	k2eAgFnSc6d1
panně	panna	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thomas	Thomas	k1gMnSc1
ovšem	ovšem	k9
také	také	k9
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
v	v	k7c6
seriálu	seriál	k1gInSc6
udržet	udržet	k5eAaPmF
koherentní	koherentní	k2eAgInSc4d1
a	a	k8xC
konzistentní	konzistentní	k2eAgInSc4d1
děj	děj	k1gInSc4
a	a	k8xC
snaží	snažit	k5eAaImIp3nP
se	se	k3xPyFc4
vyhnout	vyhnout	k5eAaPmF
chybám	chyba	k1gFnPc3
v	v	k7c6
kontinuitě	kontinuita	k1gFnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
seriálech	seriál	k1gInPc6
<g/>
,	,	kIx,
jichž	jenž	k3xRgMnPc2
je	být	k5eAaImIp3nS
fanouškem	fanoušek	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
je	být	k5eAaImIp3nS
předělávkou	předělávka	k1gFnSc7
skladby	skladba	k1gFnSc2
„	„	k?
<g/>
Hey	Hey	k1gFnSc2
Beatiful	Beatifula	k1gFnPc2
<g/>
“	“	k?
od	od	k7c2
skupiny	skupina	k1gFnSc2
The	The	k1gFnPc2
Solids	Solids	k1gInSc1
<g/>
,	,	kIx,
jejímiž	jejíž	k3xOyRp3gMnPc7
členy	člen	k1gMnPc7
oba	dva	k4xCgMnPc1
tvůrci	tvůrce	k1gMnPc1
seriálu	seriál	k1gInSc2
jsou	být	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epizody	epizoda	k1gFnPc1
první	první	k4xOgFnSc2
řady	řada	k1gFnSc2
obvykle	obvykle	k6eAd1
začínají	začínat	k5eAaImIp3nP
úvodními	úvodní	k2eAgInPc7d1
titulky	titulek	k1gInPc7
<g/>
,	,	kIx,
od	od	k7c2
druhé	druhý	k4xOgFnSc2
řady	řada	k1gFnSc2
byla	být	k5eAaImAgFnS
zařazována	zařazovat	k5eAaImNgFnS
scéna	scéna	k1gFnSc1
před	před	k7c7
titulky	titulek	k1gInPc7
<g/>
:	:	kIx,
buď	buď	k8xC
šlo	jít	k5eAaImAgNnS
o	o	k7c4
děj	děj	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2030	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Tedovy	Tedův	k2eAgFnPc1d1
děti	dítě	k1gFnPc1
na	na	k7c6
pohovce	pohovka	k1gFnSc6
naslouchají	naslouchat	k5eAaImIp3nP
vyprávění	vyprávění	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
poznal	poznat	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnSc4
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
anebo	anebo	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
připomínací	připomínací	k2eAgFnPc4d1
scény	scéna	k1gFnPc4
z	z	k7c2
předchozí	předchozí	k2eAgFnSc2d1
epizody	epizoda	k1gFnSc2
či	či	k8xC
pohledy	pohled	k1gInPc4
na	na	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Ted	Ted	k1gMnSc1
vypráví	vyprávět	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
příběh	příběh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Natáčení	natáčení	k1gNnSc3
jednoho	jeden	k4xCgInSc2
dílu	díl	k1gInSc2
v	v	k7c6
ateliéru	ateliér	k1gInSc6
Soundstage	Soundstage	k1gFnPc2
Studio	studio	k1gNnSc1
22	#num#	k4
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
zpravidla	zpravidla	k6eAd1
trvalo	trvat	k5eAaImAgNnS
déle	dlouho	k6eAd2
než	než	k8xS
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
situačních	situační	k2eAgFnPc2d1
komedií	komedie	k1gFnPc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
natočena	natočit	k5eAaBmNgFnS
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
dni	den	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Jedna	jeden	k4xCgFnSc1
epizoda	epizoda	k1gFnSc1
obvykle	obvykle	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
scén	scéna	k1gFnPc2
s	s	k7c7
rychlými	rychlý	k2eAgInPc7d1
přechody	přechod	k1gInPc7
a	a	k8xC
flashbacky	flashback	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvuková	zvukový	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
se	s	k7c7
smíchem	smích	k1gInSc7
byla	být	k5eAaImAgFnS
nahrávána	nahrávat	k5eAaImNgFnS
později	pozdě	k6eAd2
při	při	k7c6
promítání	promítání	k1gNnSc6
finálně	finálně	k6eAd1
sestříhané	sestříhaný	k2eAgFnSc2d1
epizody	epizoda	k1gFnSc2
před	před	k7c7
publikem	publikum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
natáčení	natáčení	k1gNnSc1
před	před	k7c7
živým	živý	k2eAgNnSc7d1
publikem	publikum	k1gNnSc7
by	by	k9
kvůli	kvůli	k7c3
mnoha	mnoho	k4c3
flashbackům	flashback	k1gInPc3
a	a	k8xC
flashforwardům	flashforward	k1gMnPc3
bylo	být	k5eAaImAgNnS
nemožné	možný	k2eNgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
ale	ale	k8xC
některé	některý	k3yIgFnPc1
epizody	epizoda	k1gFnPc1
byly	být	k5eAaImAgFnP
<g/>
,	,	kIx,
pokud	pokud	k8xS
to	ten	k3xDgNnSc1
scénář	scénář	k1gInSc4
umožňoval	umožňovat	k5eAaImAgInS
<g/>
,	,	kIx,
natočeny	natočen	k2eAgInPc1d1
před	před	k7c7
živým	živý	k2eAgNnSc7d1
publikem	publikum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Scéna	scéna	k1gFnSc1
přímo	přímo	k6eAd1
vedoucí	vedoucí	k2eAgFnSc1d1
k	k	k7c3
odhalení	odhalení	k1gNnSc3
identity	identita	k1gFnSc2
Matky	matka	k1gFnSc2
byla	být	k5eAaImAgFnS
s	s	k7c7
Tedovými	Tedův	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
natočena	natočit	k5eAaBmNgFnS
v	v	k7c6
době	doba	k1gFnSc6
začátku	začátek	k1gInSc2
vzniku	vznik	k1gInSc2
druhé	druhý	k4xOgFnSc2
řady	řada	k1gFnSc2
jako	jako	k8xC,k8xS
možný	možný	k2eAgInSc1d1
konec	konec	k1gInSc1
seriálu	seriál	k1gInSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
hlavně	hlavně	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
představitelé	představitel	k1gMnPc1
Tedových	Tedův	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
by	by	kYmCp3nS
v	v	k7c6
době	doba	k1gFnSc6
ukončení	ukončení	k1gNnSc2
natáčení	natáčení	k1gNnSc2
seriálu	seriál	k1gInSc2
za	za	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
již	již	k6eAd1
byli	být	k5eAaImAgMnP
pravděpodobně	pravděpodobně	k6eAd1
dospělí	dospělí	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autoři	autor	k1gMnPc1
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
relativně	relativně	k6eAd1
často	často	k6eAd1
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
herce	herec	k1gMnPc4
ze	z	k7c2
seriálů	seriál	k1gInPc2
Josse	Joss	k1gMnSc2
Whedona	Whedon	k1gMnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
je	být	k5eAaImIp3nS
spjatá	spjatý	k2eAgFnSc1d1
především	především	k9
představitelka	představitelka	k1gFnSc1
Lily	lít	k5eAaImAgFnP
<g/>
,	,	kIx,
Alyson	Alyson	k1gInSc4
Hannigan	Hannigana	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
sedm	sedm	k4xCc4
let	let	k1gInSc4
působila	působit	k5eAaImAgFnS
ve	v	k7c6
Whedonově	Whedonův	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Buffy	buffa	k1gFnSc2
<g/>
,	,	kIx,
přemožitelka	přemožitelka	k1gFnSc1
upírů	upír	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carter	Carter	k1gInSc1
Bays	Bays	k1gInSc4
uvedl	uvést	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
velkým	velký	k2eAgMnSc7d1
fanouškem	fanoušek	k1gMnSc7
těchto	tento	k3xDgInPc2
herců	herc	k1gInPc2
a	a	k8xC
že	že	k8xS
je	on	k3xPp3gInPc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
populární	populární	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
stávky	stávka	k1gFnSc2
hollywoodských	hollywoodský	k2eAgMnPc2d1
scenáristů	scenárista	k1gMnPc2
v	v	k7c6
letech	let	k1gInPc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
bylo	být	k5eAaImAgNnS
natáčení	natáčení	k1gNnSc1
seriálu	seriál	k1gInSc2
Jak	jak	k8xS,k8xC
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
pozastaveno	pozastaven	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
ukončení	ukončení	k1gNnSc6
stávky	stávka	k1gFnSc2
se	se	k3xPyFc4
seriál	seriál	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vysílání	vysílání	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
Jak	jak	k8xS,k8xC
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
ŘadaDílyPremiéra	ŘadaDílyPremiéra	k1gFnSc1
v	v	k7c4
USAPremiéra	USAPremiér	k1gMnSc4
v	v	k7c6
ČR	ČR	kA
</s>
<s>
První	první	k4xOgMnPc1
dílPoslední	dílPosledný	k2eAgMnPc1d1
dílPrvní	dílPrvnit	k5eAaPmIp3nP
dílPoslední	dílPosledný	k2eAgMnPc1d1
díl	díl	k1gInSc4
</s>
<s>
1	#num#	k4
</s>
<s>
2220050919	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200520060515	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200620091126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
200920100104	#num#	k4
<g/>
a	a	k8xC
<g/>
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2220060918	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200620070514	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200720100105	#num#	k4
<g/>
a	a	k8xC
<g/>
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
201020100210	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2020070924	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200720080519	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200820100916	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201020101020	#num#	k4
<g/>
a	a	k8xC
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2420080922	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200820090518	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200920101021	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
201020101201	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2010	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2420090921	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200920100524	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201020101202	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
201020110112	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2011	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2420100920	#num#	k4
<g/>
a	a	k8xC
<g/>
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201020110516	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201120120112	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
201220120222	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
2420110919	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201120120514	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201220120802	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
201220120924	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
2420120924	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201220130513	#num#	k4
<g/>
a	a	k8xC
<g/>
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201320131128	#num#	k4
<g/>
a	a	k8xC
<g/>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
201320131221	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
2420130923	#num#	k4
<g/>
a	a	k8xC
<g/>
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201320140331	#num#	k4
<g/>
a	a	k8xC
<g/>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
201420140614	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
201420140626	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
</s>
<s>
První	první	k4xOgInSc1
díl	díl	k1gInSc1
seriálu	seriál	k1gInSc2
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
byl	být	k5eAaImAgMnS
na	na	k7c4
stanici	stanice	k1gFnSc4
CBS	CBS	kA
odvysílán	odvysílán	k2eAgMnSc1d1
19	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
třetí	třetí	k4xOgFnSc2
řady	řada	k1gFnSc2
seriálu	seriál	k1gInSc2
proběhla	proběhnout	k5eAaPmAgFnS
stávka	stávka	k1gFnSc1
hollywoodských	hollywoodský	k2eAgMnPc2d1
scenáristů	scenárista	k1gMnPc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
po	po	k7c6
stávce	stávka	k1gFnSc6
byla	být	k5eAaImAgFnS
uvedena	uveden	k2eAgFnSc1d1
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
si	se	k3xPyFc3
seriál	seriál	k1gInSc1
prohodil	prohodit	k5eAaPmAgInS
časový	časový	k2eAgInSc4d1
slot	slot	k1gInSc4
s	s	k7c7
jiným	jiný	k2eAgInSc7d1
sitcomem	sitcom	k1gInSc7
Teorie	teorie	k1gFnSc1
velkého	velký	k2eAgInSc2d1
třesku	třesk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
oznámena	oznámen	k2eAgFnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
seriálu	seriál	k1gInSc2
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
její	její	k3xOp3gFnSc1
první	první	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
byla	být	k5eAaImAgFnS
odvysílána	odvysílán	k2eAgFnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
byla	být	k5eAaImAgFnS
oznámena	oznámen	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
seriálu	seriál	k1gInSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
společnost	společnost	k1gFnSc1
CBS	CBS	kA
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
seriál	seriál	k1gInSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
původnímu	původní	k2eAgInSc3d1
vysílacímu	vysílací	k2eAgInSc3d1
času	čas	k1gInSc3
ve	v	k7c4
20	#num#	k4
hodin	hodina	k1gFnPc2
východoamerického	východoamerický	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
byla	být	k5eAaImAgFnS
odvysílána	odvysílán	k2eAgFnSc1d1
100	#num#	k4
<g/>
.	.	kIx.
epizoda	epizoda	k1gFnSc1
seriálu	seriál	k1gInSc2
a	a	k8xC
přitom	přitom	k6eAd1
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
seriál	seriál	k1gInSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
i	i	k9
6	#num#	k4
<g/>
.	.	kIx.
řadu	řad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Herečtí	herecký	k2eAgMnPc1d1
protagonisté	protagonista	k1gMnPc1
nicméně	nicméně	k8xC
prohlásili	prohlásit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
neradi	nerad	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
v	v	k7c6
seriálu	seriál	k1gInSc6
déle	dlouho	k6eAd2
než	než	k8xS
osm	osm	k4xCc4
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
televizní	televizní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
CBS	CBS	kA
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
natočena	natočen	k2eAgFnSc1d1
také	také	k9
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
seriálu	seriál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
díl	díl	k1gInSc1
sedmé	sedmý	k4xOgFnSc2
řady	řada	k1gFnSc2
byl	být	k5eAaImAgInS
odvysílán	odvysílat	k5eAaPmNgInS
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1
devátá	devátý	k4xOgFnSc1
řada	řada	k1gFnSc1
seriálu	seriál	k1gInSc2
byla	být	k5eAaImAgFnS
odvysílána	odvysílat	k5eAaPmNgFnS
od	od	k7c2
září	září	k1gNnSc2
2013	#num#	k4
do	do	k7c2
března	březen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
měl	mít	k5eAaImAgInS
úvodní	úvodní	k2eAgInSc4d1
díl	díl	k1gInSc4
premiéru	premiéra	k1gFnSc4
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
Prima	prima	k2eAgFnPc2d1
Cool	Coola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
byly	být	k5eAaImAgFnP
odvysílány	odvysílán	k2eAgFnPc1d1
další	další	k2eAgFnPc1d1
řady	řada	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
série	série	k1gFnPc1
se	se	k3xPyFc4
dočkaly	dočkat	k5eAaPmAgFnP
české	český	k2eAgFnPc1d1
premiéry	premiéra	k1gFnPc1
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
Prima	prima	k6eAd1
Love	lov	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
je	být	k5eAaImIp3nS
dílem	dílo	k1gNnSc7
Petra	Petr	k1gMnSc2
Anderleho	Anderle	k1gMnSc2
<g/>
,	,	kIx,
Mikuláše	Mikuláš	k1gMnSc2
Bryana	Bryan	k1gMnSc2
a	a	k8xC
Silvie	Silvia	k1gFnSc2
Šustrové	Šustrová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
seriály	seriál	k1gInPc1
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
ze	z	k7c2
strany	strana	k1gFnSc2
CBS	CBS	kA
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
probíhají	probíhat	k5eAaImIp3nP
jednání	jednání	k1gNnSc4
o	o	k7c6
spin-offu	spin-off	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
umožnit	umožnit	k5eAaPmF
pohled	pohled	k1gInSc4
z	z	k7c2
druhé	druhý	k4xOgFnSc2
strany	strana	k1gFnSc2
a	a	k8xC
měl	mít	k5eAaImAgMnS
by	by	kYmCp3nS
se	se	k3xPyFc4
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
How	How	k1gFnSc4
I	i	k9
Met	met	k1gInSc4
Your	Youra	k1gFnPc2
Father	Fathra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
zástupci	zástupce	k1gMnPc1
CBS	CBS	kA
oznámili	oznámit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
objednali	objednat	k5eAaPmAgMnP
pilotní	pilotní	k2eAgInSc4d1
díl	díl	k1gInSc4
seriálu	seriál	k1gInSc2
(	(	kIx(
<g/>
s	s	k7c7
finálním	finální	k2eAgInSc7d1
názvem	název	k1gInSc7
How	How	k1gFnPc2
I	i	k8xC
Met	Mety	k1gFnPc2
Your	Your	k1gMnSc1
Dad	Dad	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
měli	mít	k5eAaImAgMnP
mít	mít	k5eAaImF
na	na	k7c4
starosti	starost	k1gFnPc4
Carter	Cartrum	k1gNnPc2
Bays	Baysa	k1gFnPc2
a	a	k8xC
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
společně	společně	k6eAd1
s	s	k7c7
Emily	Emil	k1gMnPc7
Spivey	Spivea	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
ledna	leden	k1gInSc2
2014	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
byly	být	k5eAaImAgFnP
plánovány	plánovat	k5eAaImNgFnP
pro	pro	k7c4
pilotní	pilotní	k2eAgFnSc4d1
epizodu	epizoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roli	role	k1gFnSc6
vypravěče	vypravěč	k1gMnSc2
děje	děj	k1gInSc2
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
představit	představit	k5eAaPmF
slečna	slečna	k1gFnSc1
jménem	jméno	k1gNnSc7
Sally	Salla	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
děj	děj	k1gInSc4
retrospektivně	retrospektivně	k6eAd1
vyprávět	vyprávět	k5eAaImF
svým	svůj	k3xOyFgFnPc3
dětem	dítě	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
stanice	stanice	k1gFnSc2
CBS	CBS	kA
od	od	k7c2
projektu	projekt	k1gInSc2
How	How	k1gFnPc2
I	i	k8xC
Met	Mety	k1gFnPc2
Your	Youra	k1gFnPc2
Dad	Dad	k1gFnSc2
odstoupila	odstoupit	k5eAaPmAgFnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
a	a	k8xC
Carter	Carter	k1gMnSc1
Bays	Baysa	k1gFnPc2
odmítli	odmítnout	k5eAaPmAgMnP
přetočit	přetočit	k5eAaPmF
pilotní	pilotní	k2eAgInSc4d1
díl	díl	k1gInSc4
bez	bez	k7c2
záruky	záruka	k1gFnSc2
objednání	objednání	k1gNnSc2
celého	celý	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Merchandising	Merchandising	k1gInSc1
</s>
<s>
Postava	postava	k1gFnSc1
Barneyho	Barney	k1gMnSc2
Stinsona	Stinson	k1gMnSc2
mluví	mluvit	k5eAaImIp3nS
v	v	k7c6
několika	několik	k4yIc6
epizodách	epizoda	k1gFnPc6
o	o	k7c6
svých	svůj	k3xOyFgFnPc6
knihách	kniha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
skutečně	skutečně	k6eAd1
vyšly	vyjít	k5eAaPmAgFnP
pod	pod	k7c4
Barneyho	Barney	k1gMnSc4
jménem	jméno	k1gNnSc7
čtyři	čtyři	k4xCgFnPc4
publikace	publikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc7
skutečným	skutečný	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Matt	Matt	k1gMnSc1
Kuhn	Kuhn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihy	kniha	k1gFnPc4
byly	být	k5eAaImAgFnP
v	v	k7c6
překladu	překlad	k1gInSc6
Víta	Vít	k1gMnSc2
Penkaly	Penkal	k1gMnPc4
vydány	vydat	k5eAaPmNgInP
i	i	k9
v	v	k7c6
češtině	čeština	k1gFnSc6
v	v	k7c6
nakladatelství	nakladatelství	k1gNnSc6
Argo	Argo	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Borcův	borcův	k2eAgInSc1d1
Kodex	kodex	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Bro	Bro	k1gMnSc2
Code	Cod	k1gMnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Borec	borec	k1gMnSc1
v	v	k7c6
akci	akce	k1gFnSc6
(	(	kIx(
<g/>
Bro	Bro	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Go	Go	k1gMnPc7
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Barneyho	Barneyze	k6eAd1
Příručka	příručka	k1gFnSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Playbook	Playbook	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Borcův	borcův	k2eAgInSc1d1
kodex	kodex	k1gInSc1
pro	pro	k7c4
rodiče	rodič	k1gMnPc4
(	(	kIx(
<g/>
Bro	Bro	k1gFnSc1
Code	Cod	k1gInSc2
for	forum	k1gNnPc2
Parents	Parents	k1gInSc1
<g/>
:	:	kIx,
What	What	k1gInSc1
to	ten	k3xDgNnSc1
Expect	Expect	k2eAgMnSc1d1
When	When	k1gMnSc1
You	You	k1gMnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
Awesome	Awesom	k1gInSc5
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Název	název	k1gInSc1
ocenění	ocenění	k1gNnSc2
</s>
<s>
Oceněné	oceněný	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
</s>
<s>
2006	#num#	k4
<g/>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluCeny	seriáluCen	k1gMnPc4
Emmyštáb	Emmyštáb	k1gInSc4
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
komediální	komediální	k2eAgInSc1d1
seriálPeople	seriálPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choika	k1gFnSc3
Awards	Awards	k1gInSc1
</s>
<s>
2007	#num#	k4
<g/>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluCeny	seriáluCen	k1gInPc5
Emmyštáb	Emmyštáb	k1gInSc4
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
střih	střih	k1gInSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
výkon	výkon	k1gInSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluNeil	seriáluNeil	k1gInSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
TV	TV	kA
<g/>
:	:	kIx,
KomedieTeen	KomedieTeen	k1gInSc1
Choice	Choice	k1gFnSc2
AwardsNeil	AwardsNeila	k1gFnPc2
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc2
</s>
<s>
2008	#num#	k4
<g/>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluCeny	seriáluCeno	k1gNnPc7
Emmyštáb	Emmyštáb	k1gMnSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
výkon	výkon	k1gInSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluNeil	seriáluNeil	k1gInSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
beroucí	beroucí	k2eAgFnSc1d1
scénuPeople	scénuPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choic	k1gMnSc2
AwardsNeil	AwardsNeila	k1gFnPc2
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc2
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
seriál	seriál	k1gInSc1
v	v	k7c6
TV	TV	kA
<g/>
:	:	kIx,
KomedieTeen	KomedieTeen	k1gInSc1
Choice	Choice	k1gFnSc2
Awardsherci	Awardsherek	k1gMnPc1
a	a	k8xC
štáb	štáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
TV	TV	kA
<g/>
:	:	kIx,
KomedieNeil	KomedieNeil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc4
</s>
<s>
2009	#num#	k4
<g/>
NominaceNejlepší	NominaceNejlepší	k1gNnSc1
komediální	komediální	k2eAgFnSc2d1
seriálCeny	seriálCena	k1gFnSc2
Emmyštáb	Emmyštáb	k1gMnSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
výkon	výkon	k1gInSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluNeil	seriáluNeil	k1gInSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc1
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
střih	střih	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
výkon	výkon	k1gInSc1
herce	herec	k1gMnSc2
ve	v	k7c4
vedlejší	vedlejší	k2eAgInSc4d1
roliZlatý	roliZlatý	k2eAgInSc4d1
glóbusNeil	glóbusNeil	k1gInSc4
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc2
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
beroucí	beroucí	k2eAgNnSc4d1
scénuPeople	scénuPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choika	k1gFnSc6
AwardsBritney	AwardsBritnea	k1gFnPc1
Spears	Spearsa	k1gFnPc2
</s>
<s>
NominaceNejvětší	NominaceNejvětší	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluTCA	seriáluTCA	k?
Awards	Awards	k1gInSc4
</s>
<s>
NominaceOsobní	NominaceOsobní	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluNeil	seriáluNeil	k1gInSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gInSc1
</s>
<s>
2010	#num#	k4
<g/>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
výkon	výkon	k1gInSc1
herce	herec	k1gMnSc2
ve	v	k7c4
vedlejší	vedlejší	k2eAgInSc4d1
roliZlatý	roliZlatý	k2eAgInSc4d1
glóbusNeil	glóbusNeil	k1gInSc4
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc2
</s>
<s>
OceněníOblíbená	OceněníOblíbený	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
komediálním	komediální	k2eAgMnSc6d1
seriáluPeople	seriáluPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gFnSc2
AwardsAlyson	AwardsAlyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgMnSc1d1
herec	herec	k1gMnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluCeny	seriáluCena	k1gFnSc2
EmmyNeil	EmmyNeil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gInSc4
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
původní	původní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
a	a	k8xC
textštáb	textštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
vlasový	vlasový	k2eAgInSc1d1
styling	styling	k1gInSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
nebo	nebo	k8xC
speciálním	speciální	k2eAgInSc7d1
pořaduštáb	pořaduštáb	k1gInSc4
</s>
<s>
2011	#num#	k4
<g/>
NominaceOblíbená	NominaceOblíbený	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
komediálním	komediální	k2eAgMnSc6d1
seriáluPeople	seriáluPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gFnSc2
AwardsAlyson	AwardsAlyson	k1gMnSc1
Hannigan	Hannigan	k1gMnSc1
</s>
<s>
OceněníOblíbený	OceněníOblíbený	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
televizním	televizní	k2eAgNnSc6d1
komediálním	komediální	k2eAgNnSc6d1
seriáluNeil	seriáluNeit	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnPc2
</s>
<s>
NominaceOblíbený	NominaceOblíbený	k2eAgMnSc1d1
televizní	televizní	k2eAgMnSc1d1
komediální	komediální	k2eAgMnSc1d1
seriálherci	seriálherek	k1gMnPc1
a	a	k8xC
štáb	štáb	k1gInSc1
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgMnSc1d1
herec	herec	k1gMnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
komediálním	komediální	k2eAgMnSc6d1
seriáluCritics	seriáluCritics	k6eAd1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Television	Television	k1gInSc1
AwardsNeil	AwardsNeil	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc4
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgInSc1d1
střih	střih	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgMnSc6d1
seriáluCeny	seriáluCen	k2eAgFnPc4d1
Emmyštáb	Emmyštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
režie	režie	k1gFnSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
makeup	makeup	k1gInSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
nebo	nebo	k8xC
speciálním	speciální	k2eAgInSc7d1
pořaduštáb	pořaduštába	k1gFnPc2
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
2012	#num#	k4
<g/>
OceněníOblíbený	OceněníOblíbený	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
komediální	komediální	k2eAgInSc1d1
seriálPeople	seriálPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gInPc4
Awardsherci	Awardsherek	k1gMnPc1
a	a	k8xC
štáb	štáb	k1gInSc1
</s>
<s>
OceněníOblíbený	OceněníOblíbený	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
televizním	televizní	k2eAgNnSc6d1
komediálním	komediální	k2eAgNnSc6d1
seriáluNeil	seriáluNeit	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnPc2
</s>
<s>
OceněníOblíbená	OceněníOblíbený	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
ve	v	k7c4
vedlejší	vedlejší	k2eAgInPc4d1
roliKaty	roliKat	k2eAgInPc4d1
Perry	Perr	k1gInPc4
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
TV	TV	kA
<g/>
:	:	kIx,
KomedieTeen	KomedieTeen	k1gInSc1
Choice	Choice	k1gFnSc2
AwardsNeil	AwardsNeila	k1gFnPc2
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc2
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgInSc1d1
střih	střih	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgMnSc6d1
seriáluCeny	seriáluCen	k2eAgInPc4d1
Emmyštáb	Emmyštáb	k1gInSc4
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
makeup	makeup	k1gInSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
nebo	nebo	k8xC
speciálním	speciální	k2eAgInSc7d1
pořaduštáb	pořaduštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
NominaceOblíbený	NominaceOblíbený	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
komediální	komediální	k2eAgInSc1d1
seriálPeople	seriálPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gInPc4
Awardsherci	Awardsherek	k1gMnPc1
a	a	k8xC
štáb	štáb	k1gInSc1
</s>
<s>
NominaceOblíbený	NominaceOblíbený	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
televizním	televizní	k2eAgNnSc6d1
komediálním	komediální	k2eAgNnSc6d1
seriáluNeil	seriáluNeit	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnPc2
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgInSc1d1
střih	střih	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgMnSc6d1
seriáluCeny	seriáluCen	k2eAgInPc4d1
Emmyštáb	Emmyštába	k1gFnPc2
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
makeup	makeup	k1gInSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
nebo	nebo	k8xC
speciálním	speciální	k2eAgInSc7d1
pořaduštáb	pořaduštáb	k1gMnSc1
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
2014	#num#	k4
<g/>
NominaceOblíbený	NominaceOblíbený	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
komediální	komediální	k2eAgInSc1d1
seriálPeople	seriálPeople	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gInPc4
Awardsherci	Awardsherek	k1gMnPc1
a	a	k8xC
štáb	štáb	k1gInSc1
</s>
<s>
NominaceOblíbený	NominaceOblíbený	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
televizním	televizní	k2eAgNnSc6d1
komediálním	komediální	k2eAgNnSc6d1
seriáluNeil	seriáluNeit	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnPc2
</s>
<s>
NominaceOblíbená	NominaceOblíbený	k2eAgFnSc1d1
bromanceNeil	bromanceNeil	k1gInSc1
Patrick	Patrick	k1gMnSc1
Harris	Harris	k1gFnSc1
<g/>
,	,	kIx,
Jason	Jason	k1gMnSc1
Segel	Segel	k1gMnSc1
a	a	k8xC
Josh	Josh	k1gMnSc1
Radnor	Radnor	k1gMnSc1
</s>
<s>
NominaceOblíbené	NominaceOblíbený	k2eAgFnPc1d1
kámoškyCobie	kámoškyCobie	k1gFnPc1
Smulders	Smuldersa	k1gFnPc2
a	a	k8xC
Alyson	Alysona	k1gFnPc2
Hannigan	Hannigana	k1gFnPc2
</s>
<s>
NominaceNejlepší	NominaceNejlepší	k2eAgInSc1d1
střih	střih	k1gInSc1
v	v	k7c6
komediálním	komediální	k2eAgMnSc6d1
seriáluCeny	seriáluCen	k2eAgInPc4d1
Emmyštáb	Emmyštába	k1gFnPc2
</s>
<s>
OceněníNejlepší	OceněníNejlepší	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriáluštáb	seriáluštáb	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
How	How	k1gFnSc2
I	i	k9
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Print	Print	k1gInSc1
–	–	k?
TV	TV	kA
Summer	Summer	k1gInSc1
School	School	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
to	ten	k3xDgNnSc4
Create	Creat	k1gInSc5
and	and	k?
Run	run	k1gInSc1
a	a	k8xC
Successful	Successful	k1gInSc1
Sitcom	Sitcom	k1gInSc1
<g/>
.	.	kIx.
www.eonline.com	www.eonline.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TV	TV	kA
Summer	Summer	k1gInSc1
School	School	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
to	ten	k3xDgNnSc4
Create	Creat	k1gInSc5
and	and	k?
Run	run	k1gInSc1
a	a	k8xC
Successful	Successful	k1gInSc1
Sitcom	Sitcom	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Online	Onlin	k1gInSc5
<g/>
,	,	kIx,
August	August	k1gMnSc1
6	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Alyson	Alysona	k1gFnPc2
Hannigan	Hannigana	k1gFnPc2
–	–	k?
"	"	kIx"
<g/>
How	How	k1gMnSc1
I	i	k9
Met	met	k1gInSc4
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
"	"	kIx"
Sitcom	Sitcom	k1gInSc1
–	–	k?
William	William	k1gInSc1
S.	S.	kA
Paley	palea	k1gFnSc2
TV	TV	kA
Fest	fest	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Whedon	Whedon	k1gNnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
<g/>
,	,	kIx,
2006-03-28	2006-03-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Maureen	Maureen	k2eAgInSc4d1
Ryan	Ryan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Sitcoms	Sitcoms	k1gInSc1
used	used	k6eAd1
to	ten	k3xDgNnSc1
be	be	k?
about	about	k1gInSc1
something	something	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
Tribune	tribun	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
April	April	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
TV	TV	kA
Guide	Guid	k1gInSc5
Editor	editor	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Blog	Bloga	k1gFnPc2
<g/>
:	:	kIx,
Video	video	k1gNnSc1
Q	Q	kA
<g/>
&	&	k?
<g/>
As	as	k1gInSc1
<g/>
:	:	kIx,
I	i	k9
Hit	hit	k1gInSc1
the	the	k?
How	How	k1gFnPc2
I	i	k8xC
Met	Mety	k1gFnPc2
Your	Youra	k1gFnPc2
Motherlode	Motherlod	k1gInSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tvguide	tvguid	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
How	How	k1gFnSc4
I	i	k8xC
Met	met	k1gInSc4
Your	Your	k1gInSc4
Mother	Mothra	k1gFnPc2
1	#num#	k4
<g/>
st	st	kA
Season	Seasona	k1gFnPc2
DVD	DVD	kA
Commentary	Commentara	k1gFnPc1
<g/>
↑	↑	k?
KELLER	Keller	k1gMnSc1
<g/>
,	,	kIx,
Joel	Joel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carter	Carter	k1gInSc1
Bays	Bays	k1gInSc4
of	of	k?
How	How	k1gFnSc2
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc2
TV	TV	kA
Squad	Squad	k1gInSc1
Interview	interview	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-05-11	2007-05-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
'	'	kIx"
<g/>
How	How	k1gMnSc1
I	i	k9
Met	met	k1gInSc4
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
'	'	kIx"
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
on	on	k3xPp3gMnSc1
Ted	Ted	k1gMnSc1
&	&	k?
Barney	Barnea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Breakup	Breakup	k1gInSc1
<g/>
,	,	kIx,
Eriksen	Eriksen	k2eAgInSc1d1
Babies	Babies	k1gInSc1
and	and	k?
The	The	k1gMnSc5
Future	Futur	k1gMnSc5
of	of	k?
Robarn	Robarn	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zap	Zap	k1gFnSc1
<g/>
2	#num#	k4
<g/>
it	it	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Suit	suita	k1gFnPc2
Up	Up	k1gFnSc1
for	forum	k1gNnPc2
How	How	k1gFnSc2
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
Scoops	Scoops	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Without	Without	k1gMnSc1
Pity	pit	k2eAgInPc1d1
<g/>
,	,	kIx,
2008-10-03	2008-10-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
We	We	k1gMnSc1
Call	Call	k1gMnSc1
this	this	k6eAd1
the	the	k?
Cave	Cave	k1gInSc1
of	of	k?
Magic	Magice	k1gInPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
...	...	k?
<g/>
Or	Or	k1gFnPc2
Soundstage	Soundstage	k1gNnPc2
22	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
<g/>
,	,	kIx,
2011-08-13	2011-08-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnSc2
Watcher	Watchra	k1gFnPc2
<g/>
:	:	kIx,
How	How	k1gFnSc1
'	'	kIx"
<g/>
How	How	k1gFnSc1
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
is	is	k?
not	nota	k1gFnPc2
a	a	k8xC
traditional	traditionat	k5eAaImAgMnS,k5eAaPmAgMnS
sitcom	sitcom	k1gInSc4
<g/>
.	.	kIx.
featuresblogs	featuresblogs	k1gInSc4
<g/>
.	.	kIx.
<g/>
chicagotribune	chicagotribun	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
Tribune	tribun	k1gMnSc5
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Barney	Barney	k1gInPc1
writes	writes	k1gInSc1
a	a	k8xC
book	book	k1gInSc1
<g/>
,	,	kIx,
Barney	Barney	k1gInPc1
+	+	kIx~
Robin	robin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ted	Ted	k1gMnSc1
+	+	kIx~
Stella	Stella	k1gFnSc1
and	and	k?
other	other	k1gInSc1
'	'	kIx"
<g/>
How	How	k1gFnSc1
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
news	news	k1gInSc4
<g/>
.	.	kIx.
featuresblogs	featuresblogs	k1gInSc4
<g/>
.	.	kIx.
<g/>
chicagotribune	chicagotribun	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
Tribune	tribun	k1gMnSc5
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
'	'	kIx"
<g/>
How	How	k1gMnSc1
I	i	k9
Met	met	k1gInSc4
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
'	'	kIx"
Craig	Craig	k1gMnSc1
Thomas	Thomas	k1gMnSc1
on	on	k3xPp3gMnSc1
Ted	Ted	k1gMnSc1
&	&	k?
Barney	Barnea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Breakup	Breakup	k1gInSc1
<g/>
,	,	kIx,
Eriksen	Eriksen	k2eAgInSc1d1
Babies	Babies	k1gInSc1
and	and	k?
The	The	k1gMnSc5
Future	Futur	k1gMnSc5
of	of	k?
Robarn	Robarn	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zap	Zap	k1gFnSc1
<g/>
2	#num#	k4
<g/>
it	it	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GELMAN	GELMAN	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Vlada	Vlada	k1gFnSc1
<g/>
.	.	kIx.
'	'	kIx"
<g/>
How	How	k1gFnSc1
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Buffy	buffa	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
'	'	kIx"
Danny	Danna	k1gMnSc2
Strong	Strong	k1gInSc4
<g/>
,	,	kIx,
Alexis	Alexis	k1gFnSc4
Denisof	Denisof	k1gInSc4
to	ten	k3xDgNnSc4
guest	guest	k5eAaPmF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-12-16	2010-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
CBS	CBS	kA
Sets	Setsa	k1gFnPc2
Series	Series	k1gMnSc1
Return	Return	k1gMnSc1
Dates	Dates	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Breaking	Breaking	k1gInSc1
News	News	k1gInSc1
–	–	k?
"	"	kIx"
<g/>
The	The	k1gMnSc1
Big	Big	k1gMnSc1
Bang	Bang	k1gMnSc1
Theory	Theora	k1gFnSc2
<g/>
"	"	kIx"
And	Anda	k1gFnPc2
"	"	kIx"
<g/>
How	How	k1gFnSc1
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
"	"	kIx"
To	to	k9
Swap	Swap	k1gInSc1
Time	Time	k1gNnSc1
Periods	Periods	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TheFutonCritic	TheFutonCritice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Upfront	Upfront	k1gMnSc1
<g/>
:	:	kIx,
CBS	CBS	kA
Releases	Releases	k1gMnSc1
Full	Full	k1gMnSc1
Schedule	Schedule	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CBS	CBS	kA
Announces	Announces	k1gInSc4
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
Premiere	Premier	k1gInSc5
Dates	Dates	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Exclusive	Exclusiev	k1gFnSc2
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Without	Without	k1gMnSc1
a	a	k8xC
Trace	Trace	k1gMnSc1
<g/>
,	,	kIx,
<g/>
'	'	kIx"
'	'	kIx"
<g/>
Privileged	Privileged	k1gInSc1
<g/>
,	,	kIx,
<g/>
'	'	kIx"
canceled	canceled	k1gInSc1
<g/>
,	,	kIx,
'	'	kIx"
<g/>
Gossip	Gossip	k1gInSc1
<g/>
'	'	kIx"
spin-off	spin-off	k1gInSc1
DOA	DOA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
May	May	k1gMnSc1
19	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
'	'	kIx"
<g/>
How	How	k1gMnSc1
I	i	k9
Met	met	k1gInSc4
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
meets	meetsit	k5eAaPmRp2nS
Season	Season	k1gInSc1
6	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
January	Januara	k1gFnSc2
25	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
How	How	k1gFnSc1
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
to	ten	k3xDgNnSc1
end	end	k?
after	after	k1gInSc1
8	#num#	k4
seasons	seasonsa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TV	TV	kA
Series	Series	k1gInSc1
Finale	Finala	k1gFnSc3
<g/>
,	,	kIx,
2010-07-07	2010-07-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
'	'	kIx"
<g/>
How	How	k1gMnSc1
I	i	k9
met	met	k1gInSc4
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
<g/>
'	'	kIx"
renewed	renewed	k1gMnSc1
for	forum	k1gNnPc2
two	two	k?
more	mor	k1gInSc5
seasons	seasonsit	k5eAaPmRp2nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
CBS	CBS	kA
announces	announces	k1gInSc4
2011-2012	2011-2012	k4
premiere	premirat	k5eAaPmIp3nS
dates	dates	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
Express	express	k1gInSc1
<g/>
,	,	kIx,
2011-06-29	2011-06-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CÍFKA	CÍFKA	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kultovní	kultovní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009-11-26	2009-11-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gFnSc1
I	i	k8xC
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Father	Fathra	k1gFnPc2
<g/>
?!	?!	k?
|	|	kIx~
SerialZone	SerialZon	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
How	How	k1gFnPc2
I	i	k8xC
Met	Mety	k1gFnPc2
Your	Your	k1gMnSc1
Dad	Dad	k1gMnSc1
<g/>
:	:	kIx,
CBS	CBS	kA
objednala	objednat	k5eAaPmAgFnS
pilot	pilot	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
|	|	kIx~
SerialZone	SerialZon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
http://www.serialzone.cz/clanek/3129-himyd-kdo-bude-v-nove-parte/	http://www.serialzone.cz/clanek/3129-himyd-kdo-bude-v-nove-parte/	k4
<g/>
↑	↑	k?
Jak	jak	k6eAd1
přežít	přežít	k5eAaPmF
Valentýna	Valentýna	k1gFnSc1
vám	vy	k3xPp2nPc3
poradí	poradit	k5eAaPmIp3nS
Barney	Barney	k1gInPc4
Stinson	Stinsona	k1gFnPc2
a	a	k8xC
jím	jíst	k5eAaImIp1nS
sepsaný	sepsaný	k2eAgInSc1d1
Borcův	borcův	k2eAgInSc1d1
kodex	kodex	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
–	–	k?
TOPZINE	TOPZINE	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
How	How	k1gFnSc2
I	i	k9
Met	met	k1gInSc1
Your	Youra	k1gFnPc2
Mother	Mothra	k1gFnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
How	How	k?
I	i	k9
Met	met	k1gInSc4
Your	Your	k1gInSc1
Mother	Mothra	k1gFnPc2
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
na	na	k7c4
SerialZone	SerialZon	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7746815-6	7746815-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2010159293	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
110147662995160551208	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2010159293	#num#	k4
</s>
