<s>
Triptych	triptych	k1gInSc1	triptych
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
τ	τ	k?	τ
<g/>
,	,	kIx,	,
trojitý	trojitý	k2eAgInSc1d1	trojitý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
rozdělené	rozdělená	k1gFnSc2	rozdělená
na	na	k7c4	na
3	[number]	k4	3
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typ	typ	k1gInSc1	typ
polyptychu	polyptycha	k1gFnSc4	polyptycha
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
díla	dílo	k1gNnPc4	dílo
rozdělená	rozdělený	k2eAgNnPc4d1	rozdělené
na	na	k7c4	na
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
triptych	triptych	k1gInSc4	triptych
byly	být	k5eAaImAgInP	být
zhotovovány	zhotovován	k2eAgInPc1d1	zhotovován
často	často	k6eAd1	často
oltářní	oltářní	k2eAgInPc1d1	oltářní
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
dominantní	dominantní	k2eAgFnSc1d1	dominantní
a	a	k8xC	a
instalována	instalovat	k5eAaBmNgFnS	instalovat
napevno	napevno	k6eAd1	napevno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
krajní	krajní	k2eAgFnSc3d1	krajní
části	část	k1gFnSc3	část
byly	být	k5eAaImAgFnP	být
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
a	a	k8xC	a
případně	případně	k6eAd1	případně
mohly	moct	k5eAaImAgFnP	moct
střední	střední	k2eAgFnSc4d1	střední
část	část	k1gFnSc4	část
zakrýt	zakrýt	k5eAaPmF	zakrýt
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Triptych	triptychon	k1gNnPc2	triptychon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
