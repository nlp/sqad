<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
ingredience	ingredience	k1gFnPc4	ingredience
patří	patřit	k5eAaImIp3nS	patřit
tvarohový	tvarohový	k2eAgInSc1d1	tvarohový
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
sýr	sýr	k1gInSc1	sýr
(	(	kIx(	(
<g/>
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
cheesecaku	cheesecak	k1gInSc2	cheesecak
spojen	spojit	k5eAaPmNgInS	spojit
sýr	sýr	k1gInSc1	sýr
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc1	sušenka
<g/>
,	,	kIx,	,
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
a	a	k8xC	a
zakysaná	zakysaný	k2eAgFnSc1d1	zakysaná
smetana	smetana	k1gFnSc1	smetana
<g/>
.	.	kIx.	.
</s>
