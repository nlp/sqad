<s>
Cheesecake	Cheesecake	k6eAd1	Cheesecake
je	být	k5eAaImIp3nS	být
dezert	dezert	k1gInSc1	dezert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgFnPc1	dva
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
:	:	kIx,	:
krusta	krusta	k1gFnSc1	krusta
z	z	k7c2	z
rozdrobených	rozdrobený	k2eAgFnPc2d1	rozdrobená
sušenek	sušenka	k1gFnPc2	sušenka
a	a	k8xC	a
sýrová	sýrový	k2eAgFnSc1d1	sýrová
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
tvarohová	tvarohový	k2eAgFnSc1d1	tvarohová
<g/>
)	)	kIx)	)
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
byl	být	k5eAaImAgInS	být
cheesecake	cheesecake	k6eAd1	cheesecake
pouze	pouze	k6eAd1	pouze
pečeným	pečený	k2eAgInSc7d1	pečený
dezertem	dezert	k1gInSc7	dezert
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
nepečená	pečený	k2eNgFnSc1d1	nepečená
varianta	varianta	k1gFnSc1	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Cheesecake	Cheesecake	k6eAd1	Cheesecake
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
podle	podle	k7c2	podle
původní	původní	k2eAgFnSc2d1	původní
receptury	receptura	k1gFnSc2	receptura
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čistě	čistě	k6eAd1	čistě
sýrový	sýrový	k2eAgMnSc1d1	sýrový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
příchutích	příchuť	k1gFnPc6	příchuť
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
bývají	bývat	k5eAaImIp3nP	bývat
obohaceny	obohatit	k5eAaPmNgFnP	obohatit
i	i	k9	i
o	o	k7c4	o
třetí	třetí	k4xOgFnSc4	třetí
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
:	:	kIx,	:
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
čokoládu	čokoláda	k1gFnSc4	čokoláda
atp.	atp.	kA	atp.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
ingredience	ingredience	k1gFnPc4	ingredience
patří	patřit	k5eAaImIp3nS	patřit
tvarohový	tvarohový	k2eAgInSc1d1	tvarohový
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
sýr	sýr	k1gInSc1	sýr
(	(	kIx(	(
<g/>
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
cheesecaku	cheesecak	k1gInSc2	cheesecak
spojen	spojit	k5eAaPmNgInS	spojit
sýr	sýr	k1gInSc1	sýr
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc1	sušenka
<g/>
,	,	kIx,	,
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
a	a	k8xC	a
zakysaná	zakysaný	k2eAgFnSc1d1	zakysaná
smetana	smetana	k1gFnSc1	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cheesecake	cheesecak	k1gFnSc2	cheesecak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Popis	popis	k1gInSc4	popis
přípravy	příprava	k1gFnSc2	příprava
cheesecake	cheesecak	k1gInSc2	cheesecak
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgMnSc1d1	oficiální
dovozce	dovozce	k1gMnSc1	dovozce
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
