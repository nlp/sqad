<p>
<s>
Tulák	tulák	k1gMnSc1	tulák
Rover	rover	k1gMnSc1	rover
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Roverandom	Roverandom	k1gInSc1	Roverandom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
próza	próza	k1gFnSc1	próza
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
dětským	dětský	k2eAgMnPc3d1	dětský
čtenářům	čtenář	k1gMnPc3	čtenář
a	a	k8xC	a
v	v	k7c6	v
pěti	pět	k4xCc6	pět
kapitolách	kapitola	k1gFnPc6	kapitola
popisuje	popisovat	k5eAaImIp3nS	popisovat
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
štěněte	štěně	k1gNnSc2	štěně
Rovera	rover	k1gMnSc2	rover
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
Tulák	tulák	k1gMnSc1	tulák
Rover	rover	k1gMnSc1	rover
<g/>
,	,	kIx,	,
proměněného	proměněný	k2eAgInSc2d1	proměněný
čarodějem	čaroděj	k1gMnSc7	čaroděj
v	v	k7c4	v
hračku	hračka	k1gFnSc4	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svých	svůj	k3xOyFgNnPc2	svůj
dobrodružství	dobrodružství	k1gNnPc2	dobrodružství
Rover	rover	k1gMnSc1	rover
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
podmořskou	podmořský	k2eAgFnSc4d1	podmořská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
původně	původně	k6eAd1	původně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
vyprávění	vyprávění	k1gNnSc4	vyprávění
pro	pro	k7c4	pro
Tolkienovy	Tolkienův	k2eAgMnPc4d1	Tolkienův
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
autor	autor	k1gMnSc1	autor
převedl	převést	k5eAaPmAgMnS	převést
do	do	k7c2	do
písemné	písemný	k2eAgFnSc2d1	písemná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Tiskem	tisk	k1gInSc7	tisk
však	však	k9	však
vyšel	vyjít	k5eAaPmAgInS	vyjít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Tolkienův	Tolkienův	k2eAgMnSc1d1	Tolkienův
slavnější	slavný	k2eAgMnSc1d2	slavnější
Hobit	hobit	k1gMnSc1	hobit
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Tulák	tulák	k1gMnSc1	tulák
Rover	rover	k1gMnSc1	rover
původně	původně	k6eAd1	původně
jako	jako	k9	jako
vyprávění	vyprávění	k1gNnSc4	vyprávění
pro	pro	k7c4	pro
autorovy	autorův	k2eAgMnPc4d1	autorův
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
příběhu	příběh	k1gInSc2	příběh
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
do	do	k7c2	do
září	září	k1gNnSc2	září
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Tolkienova	Tolkienův	k2eAgFnSc1d1	Tolkienova
rodina	rodina	k1gFnSc1	rodina
trávila	trávit	k5eAaImAgFnS	trávit
dovolenou	dovolená	k1gFnSc4	dovolená
ve	v	k7c4	v
Filey	Filea	k1gFnPc4	Filea
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Yorkshiru	Yorkshiro	k1gNnSc6	Yorkshiro
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hrách	hra	k1gFnPc6	hra
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
ztratil	ztratit	k5eAaPmAgMnS	ztratit
tehdy	tehdy	k6eAd1	tehdy
pětiletý	pětiletý	k2eAgMnSc1d1	pětiletý
Michael	Michael	k1gMnSc1	Michael
Tolkien	Tolkina	k1gFnPc2	Tolkina
svou	svůj	k3xOyFgFnSc4	svůj
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
hračku	hračka	k1gFnSc4	hračka
psa	pes	k1gMnSc2	pes
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
popisoval	popisovat	k5eAaImAgMnS	popisovat
Roverova	roverův	k2eAgNnPc4d1	roverův
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
utěšil	utěšit	k5eAaPmAgInS	utěšit
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
příběhy	příběh	k1gInPc4	příběh
keltské	keltský	k2eAgFnSc2d1	keltská
a	a	k8xC	a
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ranými	raný	k2eAgInPc7d1	raný
náčrty	náčrt	k1gInPc7	náčrt
svých	svůj	k3xOyFgInPc2	svůj
vlastních	vlastní	k2eAgInPc2d1	vlastní
mytologických	mytologický	k2eAgInPc2d1	mytologický
textů	text	k1gInPc2	text
a	a	k8xC	a
staršími	starý	k2eAgInPc7d2	starší
díly	díl	k1gInPc7	díl
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
skřítek	skřítek	k1gMnSc1	skřítek
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Edith	Edith	k1gMnSc1	Edith
Nesbitové	Nesbitový	k2eAgFnSc2d1	Nesbitová
převzal	převzít	k5eAaPmAgMnS	převzít
například	například	k6eAd1	například
postavu	postava	k1gFnSc4	postava
písečného	písečný	k2eAgMnSc2d1	písečný
čaroděje	čaroděj	k1gMnSc2	čaroděj
Psamata	Psama	k1gNnPc4	Psama
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
jazykové	jazykový	k2eAgFnSc2d1	jazyková
hry	hra	k1gFnSc2	hra
mohou	moct	k5eAaImIp3nP	moct
připomínat	připomínat	k5eAaImF	připomínat
také	také	k9	také
Milneho	Milne	k1gMnSc4	Milne
Medvídka	medvídek	k1gMnSc4	medvídek
Pú	Pú	k1gMnSc4	Pú
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
knihy	kniha	k1gFnSc2	kniha
Hobit	hobit	k1gMnSc1	hobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
Tolkien	Tolkien	k1gInSc1	Tolkien
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
Tuláka	tulák	k1gMnSc2	tulák
Rovera	rover	k1gMnSc2	rover
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
texty	text	k1gInPc7	text
svému	svůj	k3xOyFgMnSc3	svůj
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
nakladateli	nakladatel	k1gMnSc3	nakladatel
<g/>
,	,	kIx,	,
firmě	firma	k1gFnSc3	firma
Allen	Allen	k1gMnSc1	Allen
&	&	k?	&
Unwin	Unwin	k1gMnSc1	Unwin
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
společnost	společnost	k1gFnSc1	společnost
doufala	doufat	k5eAaImAgFnS	doufat
spíše	spíše	k9	spíše
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
čtenářsky	čtenářsky	k6eAd1	čtenářsky
přitažlivých	přitažlivý	k2eAgInPc2d1	přitažlivý
příběhů	příběh	k1gInPc2	příběh
o	o	k7c6	o
hobitech	hobit	k1gMnPc6	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
publikovalo	publikovat	k5eAaBmAgNnS	publikovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
HarperCollins	HarperCollinsa	k1gFnPc2	HarperCollinsa
<g/>
,	,	kIx,	,
s	s	k7c7	s
poznámkovým	poznámkový	k2eAgInSc7d1	poznámkový
aparátem	aparát	k1gInSc7	aparát
a	a	k8xC	a
v	v	k7c6	v
redakční	redakční	k2eAgFnSc6d1	redakční
úpravě	úprava	k1gFnSc6	úprava
Christiny	Christin	k2eAgFnSc2d1	Christina
Scullové	Scullová	k1gFnSc2	Scullová
a	a	k8xC	a
Wayna	Wayn	k1gMnSc2	Wayn
D.	D.	kA	D.
Hammonda	Hammond	k1gMnSc2	Hammond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Zlomyslný	zlomyslný	k2eAgMnSc1d1	zlomyslný
čaroděj	čaroděj	k1gMnSc1	čaroděj
Artaxerses	Artaxerses	k1gMnSc1	Artaxerses
promění	proměnit	k5eAaPmIp3nS	proměnit
štěně	štěně	k1gNnSc4	štěně
jménem	jméno	k1gNnSc7	jméno
Rover	rover	k1gMnSc1	rover
v	v	k7c4	v
malou	malý	k2eAgFnSc4d1	malá
hračku	hračka	k1gFnSc4	hračka
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odnesen	odnést	k5eAaPmNgInS	odnést
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
koupí	koupit	k5eAaPmIp3nS	koupit
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
paní	paní	k1gFnSc1	paní
Tolkienová	Tolkienová	k1gFnSc1	Tolkienová
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Rover	rover	k1gMnSc1	rover
se	se	k3xPyFc4	se
však	však	k9	však
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
podobu	podoba	k1gFnSc4	podoba
hračky	hračka	k1gFnSc2	hračka
<g/>
,	,	kIx,	,
a	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vypadnout	vypadnout	k5eAaPmF	vypadnout
z	z	k7c2	z
kapsy	kapsa	k1gFnSc2	kapsa
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
písečným	písečný	k2eAgMnSc7d1	písečný
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
Psamatem	Psamat	k1gInSc7	Psamat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
promění	proměnit	k5eAaPmIp3nS	proměnit
zpátky	zpátky	k6eAd1	zpátky
ve	v	k7c4	v
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
malého	malý	k2eAgInSc2d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
racka	racek	k1gMnSc2	racek
Chechtala	chechtal	k1gMnSc2	chechtal
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
hrdina	hrdina	k1gMnSc1	hrdina
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
získá	získat	k5eAaPmIp3nS	získat
dočasně	dočasně	k6eAd1	dočasně
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
Mužem	muž	k1gMnSc7	muž
z	z	k7c2	z
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
vyrábějícím	vyrábějící	k2eAgNnSc7d1	vyrábějící
sny	sen	k1gInPc7	sen
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
psem	pes	k1gMnSc7	pes
Roverem	rover	k1gMnSc7	rover
–	–	k?	–
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rozlišili	rozlišit	k5eAaPmAgMnP	rozlišit
<g/>
,	,	kIx,	,
říkají	říkat	k5eAaImIp3nP	říkat
hrdinovi	hrdinův	k2eAgMnPc1d1	hrdinův
Tulák	tulák	k1gMnSc1	tulák
Rover	rover	k1gMnSc1	rover
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
zažije	zažít	k5eAaPmIp3nS	zažít
Tulák	tulák	k1gMnSc1	tulák
Rover	rover	k1gMnSc1	rover
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
drakem	drak	k1gInSc7	drak
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
také	také	k9	také
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
snů	sen	k1gInPc2	sen
na	na	k7c6	na
temné	temný	k2eAgFnSc6d1	temná
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
chlapcem	chlapec	k1gMnSc7	chlapec
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
jako	jako	k9	jako
hračka	hračka	k1gMnSc1	hračka
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
hoch	hoch	k1gMnSc1	hoch
neprobudí	probudit	k5eNaPmIp3nS	probudit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Psamates	Psamates	k1gInSc1	Psamates
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
zlomit	zlomit	k5eAaPmF	zlomit
Artaxerxovo	Artaxerxův	k2eAgNnSc4d1	Artaxerxův
zakletí	zakletí	k1gNnSc4	zakletí
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
vydává	vydávat	k5eAaImIp3nS	vydávat
v	v	k7c6	v
tlamě	tlama	k1gFnSc6	tlama
velryby	velryba	k1gFnSc2	velryba
Uin	Uin	k1gFnSc2	Uin
do	do	k7c2	do
podmořské	podmořský	k2eAgFnSc2d1	podmořská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Artaxerxes	Artaxerxes	k1gMnSc1	Artaxerxes
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
mořskou	mořský	k2eAgFnSc7d1	mořská
pannou	panna	k1gFnSc7	panna
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
Pacifický	pacifický	k2eAgMnSc1d1	pacifický
a	a	k8xC	a
atlantický	atlantický	k2eAgMnSc1d1	atlantický
mág	mág	k1gMnSc1	mág
<g/>
.	.	kIx.	.
</s>
<s>
Čaroděj	čaroděj	k1gMnSc1	čaroděj
hrdinovi	hrdina	k1gMnSc3	hrdina
původní	původní	k2eAgFnSc2d1	původní
podobu	podoba	k1gFnSc4	podoba
sice	sice	k8xC	sice
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
promění	proměnit	k5eAaPmIp3nS	proměnit
ho	on	k3xPp3gInSc4	on
však	však	k9	však
ve	v	k7c4	v
vodního	vodní	k2eAgMnSc4d1	vodní
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
Rover	rover	k1gMnSc1	rover
společně	společně	k6eAd1	společně
s	s	k7c7	s
podmořským	podmořský	k2eAgMnSc7d1	podmořský
Roverem	rover	k1gMnSc7	rover
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
pokusí	pokusit	k5eAaPmIp3nS	pokusit
čaroděje	čaroděj	k1gMnPc4	čaroděj
znovu	znovu	k6eAd1	znovu
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
a	a	k8xC	a
Artaxerxes	Artaxerxes	k1gInSc1	Artaxerxes
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pomstil	pomstít	k5eAaPmAgMnS	pomstít
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
Rover	rover	k1gMnSc1	rover
čarodějovi	čaroděj	k1gMnSc3	čaroděj
naschvály	naschvál	k1gInPc1	naschvál
–	–	k?	–
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
probudí	probudit	k5eAaPmIp3nS	probudit
obrovský	obrovský	k2eAgMnSc1d1	obrovský
podmořský	podmořský	k2eAgMnSc1d1	podmořský
had	had	k1gMnSc1	had
sahající	sahající	k2eAgMnSc1d1	sahající
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
konce	konec	k1gInSc2	konec
světa	svět	k1gInSc2	svět
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
zuřivostí	zuřivost	k1gFnSc7	zuřivost
způsobí	způsobit	k5eAaPmIp3nS	způsobit
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
chaos	chaos	k1gInSc1	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
incidentu	incident	k1gInSc6	incident
je	být	k5eAaImIp3nS	být
Artaxerxes	Artaxerxes	k1gInSc1	Artaxerxes
i	i	k8xC	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
vykázán	vykázat	k5eAaPmNgMnS	vykázat
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jej	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
Rover	rover	k1gMnSc1	rover
znovu	znovu	k6eAd1	znovu
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c6	o
odkouzlení	odkouzlení	k1gNnSc6	odkouzlení
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
jej	on	k3xPp3gMnSc4	on
promění	proměnit	k5eAaPmIp3nS	proměnit
nazpátek	nazpátek	k6eAd1	nazpátek
ve	v	k7c4	v
velkého	velký	k2eAgMnSc4d1	velký
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
hrdina	hrdina	k1gMnSc1	hrdina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vydat	vydat	k5eAaPmF	vydat
nazpátek	nazpátek	k6eAd1	nazpátek
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
majitelka	majitelka	k1gFnSc1	majitelka
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
babičkou	babička	k1gFnSc7	babička
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
patřil	patřit	k5eAaImAgMnS	patřit
jako	jako	k8xC	jako
hračka	hračka	k1gFnSc1	hračka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
