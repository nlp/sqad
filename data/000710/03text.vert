<s>
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gNnSc7	Lake
City	City	k1gFnSc1	City
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Utah	Utah	k1gInSc1	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
přibližně	přibližně	k6eAd1	přibližně
178	[number]	k4	178
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
aglomerace	aglomerace	k1gFnSc2	aglomerace
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Křižovatka	křižovatka	k1gFnSc1	křižovatka
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
ústředí	ústředí	k1gNnSc2	ústředí
Církve	církev	k1gFnSc2	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgFnP	konat
zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
kočující	kočující	k2eAgInPc1d1	kočující
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
Šošónů	Šošón	k1gMnPc2	Šošón
<g/>
,	,	kIx,	,
Utů	Utů	k1gMnPc2	Utů
a	a	k8xC	a
Paiutů	Paiut	k1gMnPc2	Paiut
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1847	[number]	k4	1847
sem	sem	k6eAd1	sem
mormonský	mormonský	k2eAgMnSc1d1	mormonský
prorok	prorok	k1gMnSc1	prorok
Brigham	Brigham	k1gInSc4	Brigham
Young	Young	k1gMnSc1	Young
přivedl	přivést	k5eAaPmAgMnS	přivést
své	svůj	k3xOyFgInPc4	svůj
souvěrníky	souvěrník	k1gInPc4	souvěrník
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
zde	zde	k6eAd1	zde
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
také	také	k9	také
založil	založit	k5eAaPmAgMnS	založit
duchovní	duchovní	k2eAgNnSc4d1	duchovní
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
-	-	kIx~	-
náměstí	náměstí	k1gNnSc1	náměstí
Temple	templ	k1gInSc5	templ
Square	square	k1gInSc4	square
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
stojící	stojící	k2eAgInSc1d1	stojící
chrám	chrám	k1gInSc4	chrám
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gMnSc2	Lak
Temple	templ	k1gInSc5	templ
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
některá	některý	k3yIgNnPc1	některý
další	další	k2eAgNnPc1d1	další
střediska	středisko	k1gNnPc1	středisko
mormonské	mormonský	k2eAgFnSc2d1	mormonská
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Mormoni	mormon	k1gMnPc1	mormon
usazení	usazený	k2eAgMnPc1d1	usazený
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gInSc2	Lak
City	City	k1gFnSc2	City
chtěli	chtít	k5eAaImAgMnP	chtít
založit	založit	k5eAaPmF	založit
vlastní	vlastní	k2eAgInSc4d1	vlastní
stát	stát	k1gInSc4	stát
s	s	k7c7	s
názvem	název	k1gInSc7	název
Deseret	Deseret	k1gMnSc1	Deseret
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
také	také	k9	také
území	území	k1gNnSc4	území
Nevady	Nevada	k1gFnSc2	Nevada
a	a	k8xC	a
části	část	k1gFnSc2	část
jižní	jižní	k2eAgFnSc2d1	jižní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
ale	ale	k8xC	ale
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
státu	stát	k1gInSc2	stát
Utah	Utah	k1gInSc4	Utah
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
mělo	mít	k5eAaImAgNnS	mít
později	pozdě	k6eAd2	pozdě
stát	stát	k5eAaImF	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
hlavně	hlavně	k9	hlavně
kolem	kolem	k7c2	kolem
polygamie	polygamie	k1gFnSc2	polygamie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
mormony	mormon	k1gMnPc7	mormon
běžnou	běžný	k2eAgFnSc4d1	běžná
praxí	praxe	k1gFnSc7	praxe
<g/>
,	,	kIx,	,
přerostly	přerůst	k5eAaPmAgInP	přerůst
v	v	k7c4	v
otevřený	otevřený	k2eAgInSc4d1	otevřený
konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
Washingtonem	Washington	k1gInSc7	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
složit	složit	k5eAaPmF	složit
funkci	funkce	k1gFnSc4	funkce
guvernéra	guvernér	k1gMnSc2	guvernér
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
James	James	k1gMnSc1	James
Buchanan	Buchanan	k1gMnSc1	Buchanan
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
oblast	oblast	k1gFnSc4	oblast
za	za	k7c7	za
vzbouřenou	vzbouřený	k2eAgFnSc7d1	vzbouřená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
k	k	k7c3	k
utažské	utažský	k2eAgFnSc3d1	utažská
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mormonská	mormonský	k2eAgFnSc1d1	mormonská
církev	církev	k1gFnSc1	církev
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
polygamii	polygamie	k1gFnSc4	polygamie
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
správy	správa	k1gFnSc2	správa
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
stalo	stát	k5eAaPmAgNnS	stát
oficiálním	oficiální	k2eAgMnPc3d1	oficiální
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Rozmachu	rozmach	k1gInSc2	rozmach
města	město	k1gNnSc2	město
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
vybudování	vybudování	k1gNnSc4	vybudování
první	první	k4xOgFnSc2	první
transkontinentální	transkontinentální	k2eAgFnSc2d1	transkontinentální
železnice	železnice	k1gFnSc2	železnice
vedoucí	vedoucí	k1gFnSc2	vedoucí
městem	město	k1gNnSc7	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
město	město	k1gNnSc1	město
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
velikost	velikost	k1gFnSc1	velikost
celé	celý	k2eAgFnSc2d1	celá
aglomerace	aglomerace	k1gFnSc2	aglomerace
se	se	k3xPyFc4	se
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
demokratičtí	demokratický	k2eAgMnPc1d1	demokratický
starostové	starosta	k1gMnPc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
propojení	propojení	k1gNnSc1	propojení
správy	správa	k1gFnSc2	správa
města	město	k1gNnSc2	město
a	a	k8xC	a
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
s	s	k7c7	s
Církví	církev	k1gFnSc7	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
285,9	[number]	k4	285,9
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
cípu	cíp	k1gInSc6	cíp
údolí	údolí	k1gNnSc2	údolí
Velkého	velký	k2eAgNnSc2d1	velké
Solného	solný	k2eAgNnSc2d1	solné
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
samotného	samotný	k2eAgNnSc2d1	samotné
jezera	jezero	k1gNnSc2	jezero
ho	on	k3xPp3gMnSc4	on
dělí	dělit	k5eAaImIp3nS	dělit
faunou	fauna	k1gFnSc7	fauna
i	i	k8xC	i
flórou	flóra	k1gFnSc7	flóra
bohaté	bohatý	k2eAgFnSc2d1	bohatá
mokřiny	mokřina	k1gFnSc2	mokřina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
ročně	ročně	k6eAd1	ročně
dostává	dostávat	k5eAaImIp3nS	dostávat
"	"	kIx"	"
<g/>
jezerní	jezerní	k2eAgInSc1d1	jezerní
smrad	smrad	k1gInSc1	smrad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
zápach	zápach	k1gInSc1	zápach
zkažených	zkažený	k2eAgNnPc2d1	zkažené
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
hory	hora	k1gFnPc1	hora
Wasatch	Wasatcha	k1gFnPc2	Wasatcha
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
blízkým	blízký	k2eAgInSc7d1	blízký
vrcholem	vrchol	k1gInSc7	vrchol
Twin	Twin	k1gNnSc4	Twin
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
vyvýšený	vyvýšený	k2eAgInSc1d1	vyvýšený
Capitol	Capitol	k1gInSc1	Capitol
Hill	Hilla	k1gFnPc2	Hilla
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
pravoúhlý	pravoúhlý	k2eAgInSc4d1	pravoúhlý
půdorys	půdorys	k1gInSc4	půdorys
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
tříd	třída	k1gFnPc2	třída
číslovaných	číslovaný	k2eAgFnPc2d1	číslovaná
od	od	k7c2	od
křižovatky	křižovatka	k1gFnSc2	křižovatka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Temple	templ	k1gInSc5	templ
Square	square	k1gInSc5	square
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
186	[number]	k4	186
440	[number]	k4	440
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
aglomerace	aglomerace	k1gFnSc1	aglomerace
má	mít	k5eAaImIp3nS	mít
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Wasatch	Wasatch	k1gInSc1	Wasatch
Front	front	k1gInSc1	front
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
i	i	k8xC	i
město	město	k1gNnSc1	město
Ogden	Ogdna	k1gFnPc2	Ogdna
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c4	na
2,15	[number]	k4	2,15
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
anglo-amerického	anglomerický	k2eAgInSc2d1	anglo-americký
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
ale	ale	k9	ale
podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
tichomořských	tichomořský	k2eAgInPc2d1	tichomořský
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
Samoy	Samoa	k1gFnSc2	Samoa
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
75,1	[number]	k4	75,1
<g/>
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
2,7	[number]	k4	2,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,2	[number]	k4	1,2
<g/>
%	%	kIx~	%
američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
4,4	[number]	k4	4,4
<g/>
%	%	kIx~	%
asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
2,0	[number]	k4	2,0
<g/>
%	%	kIx~	%
pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
10,7	[number]	k4	10,7
<g/>
%	%	kIx~	%
jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,7	[number]	k4	3,7
<g/>
%	%	kIx~	%
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
22,3	[number]	k4	22,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
hospodářství	hospodářství	k1gNnSc4	hospodářství
města	město	k1gNnSc2	město
spojené	spojený	k2eAgNnSc4d1	spojené
hlavně	hlavně	k6eAd1	hlavně
s	s	k7c7	s
dolováním	dolování	k1gNnSc7	dolování
a	a	k8xC	a
těžbou	těžba	k1gFnSc7	těžba
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
činnost	činnost	k1gFnSc4	činnost
státní	státní	k2eAgFnSc2d1	státní
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
turistiky	turistika	k1gFnSc2	turistika
výrazně	výrazně	k6eAd1	výrazně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
po	po	k7c6	po
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Veletržní	veletržní	k2eAgFnPc1d1	veletržní
a	a	k8xC	a
kongresové	kongresový	k2eAgFnPc1d1	kongresová
služby	služba	k1gFnPc1	služba
vzkvétají	vzkvétat	k5eAaImIp3nP	vzkvétat
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
velkých	velký	k2eAgFnPc2d1	velká
veletržních	veletržní	k2eAgFnPc2d1	veletržní
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gNnSc7	Lake
Palace	Palace	k1gFnSc1	Palace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
dopravními	dopravní	k2eAgInPc7d1	dopravní
spojeními	spojení	k1gNnPc7	spojení
jsou	být	k5eAaImIp3nP	být
mezistátní	mezistátní	k2eAgFnSc4d1	mezistátní
dálnice	dálnice	k1gFnPc4	dálnice
Insterstate	Insterstat	k1gInSc5	Insterstat
15	[number]	k4	15
<g/>
,	,	kIx,	,
transkontinentální	transkontinentální	k2eAgFnSc1d1	transkontinentální
železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
do	do	k7c2	do
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gInSc2	Lak
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Utažský	Utažský	k2eAgInSc1d1	Utažský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Utah	Utah	k1gInSc1	Utah
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
)	)	kIx)	)
pořádá	pořádat	k5eAaImIp3nS	pořádat
své	svůj	k3xOyFgInPc4	svůj
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
koncertní	koncertní	k2eAgFnSc6d1	koncertní
síni	síň	k1gFnSc6	síň
Maurice	Maurika	k1gFnSc6	Maurika
Abravanela	Abravanela	k1gFnSc1	Abravanela
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Abravanel	Abravanel	k1gMnSc1	Abravanel
Hall	Hall	k1gMnSc1	Hall
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Unikátním	unikátní	k2eAgNnSc7d1	unikátní
hudebním	hudební	k2eAgNnSc7d1	hudební
tělesem	těleso	k1gNnSc7	těleso
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
Mormon	mormon	k1gMnSc1	mormon
Tabernacle	Tabernacle	k1gFnSc2	Tabernacle
Choir	Choir	k1gMnSc1	Choir
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
řada	řada	k1gFnSc1	řada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
státní	státní	k2eAgFnSc2d1	státní
University	universita	k1gFnSc2	universita
of	of	k?	of
Utah	Utah	k1gInSc1	Utah
a	a	k8xC	a
soukromá	soukromý	k2eAgFnSc1d1	soukromá
Brigham	Brigham	k1gInSc1	Brigham
Young	Young	k1gInSc4	Young
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gNnSc7	Lake
City	City	k1gFnSc1	City
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
světovým	světový	k2eAgInSc7d1	světový
centrem	centr	k1gInSc7	centr
Církve	církev	k1gFnSc2	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
hostilo	hostit	k5eAaImAgNnS	hostit
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnSc1	Lake
City	City	k1gFnSc1	City
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
byly	být	k5eAaImAgInP	být
provázené	provázený	k2eAgInPc1d1	provázený
skandály	skandál	k1gInPc1	skandál
(	(	kIx(	(
<g/>
úplatkářskou	úplatkářský	k2eAgFnSc7d1	úplatkářská
aférou	aféra	k1gFnSc7	aféra
při	při	k7c6	při
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
pořádání	pořádání	k1gNnSc4	pořádání
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
dopingovými	dopingový	k2eAgInPc7d1	dopingový
případy	případ	k1gInPc7	případ
a	a	k8xC	a
skandálem	skandál	k1gInSc7	skandál
s	s	k7c7	s
neobjektivitou	neobjektivita	k1gFnSc7	neobjektivita
rozhodčích	rozhodčí	k2eAgNnPc2d1	rozhodčí
krasobruslení	krasobruslení	k1gNnPc2	krasobruslení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přinesly	přinést	k5eAaPmAgFnP	přinést
městu	město	k1gNnSc3	město
velký	velký	k2eAgInSc4d1	velký
finanční	finanční	k2eAgInSc4d1	finanční
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
rozmach	rozmach	k1gInSc4	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
uspořádání	uspořádání	k1gNnSc1	uspořádání
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
dopravní	dopravní	k2eAgFnSc2d1	dopravní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
stavbě	stavba	k1gFnSc3	stavba
nových	nový	k2eAgInPc2d1	nový
i	i	k8xC	i
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
starých	starý	k2eAgNnPc2d1	staré
sportovišť	sportoviště	k1gNnPc2	sportoviště
i	i	k8xC	i
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
využívaných	využívaný	k2eAgMnPc2d1	využívaný
k	k	k7c3	k
pořádání	pořádání	k1gNnSc3	pořádání
významných	významný	k2eAgFnPc2d1	významná
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
her	hra	k1gFnPc2	hra
posílilo	posílit	k5eAaPmAgNnS	posílit
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
jediný	jediný	k2eAgInSc1d1	jediný
velký	velký	k2eAgInSc1d1	velký
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
tým	tým	k1gInSc1	tým
Utah	Utah	k1gInSc1	Utah
Jazz	jazz	k1gInSc4	jazz
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
svátkem	svátek	k1gInSc7	svátek
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc1d1	státní
svátek	svátek	k1gInSc1	svátek
Utahu	Utah	k1gInSc2	Utah
Pioneer	Pioneer	kA	Pioneer
Day	Day	k1gMnSc2	Day
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Den	den	k1gInSc1	den
osadníků	osadník	k1gMnPc2	osadník
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
velká	velký	k2eAgFnSc1d1	velká
přehlídka	přehlídka	k1gFnSc1	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
přehlídkou	přehlídka	k1gFnSc7	přehlídka
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgMnSc1d1	místní
Gay	gay	k1gMnSc1	gay
Pride	Prid	k1gInSc5	Prid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Temple	templ	k1gInSc5	templ
Square	square	k1gInSc4	square
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
budovy	budova	k1gFnPc1	budova
Církev	církev	k1gFnSc1	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
<g/>
:	:	kIx,	:
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gNnSc2	Lake
Temple	templ	k1gInSc5	templ
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnSc1	Lake
Tabernacle	Tabernacle	k1gFnSc1	Tabernacle
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
Přes	přes	k7c4	přes
ulici	ulice	k1gFnSc4	ulice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obří	obří	k2eAgInSc1d1	obří
komplex	komplex	k1gInSc1	komplex
LDS	LDS	kA	LDS
Conference	Conferenec	k1gInSc2	Conferenec
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Konferenční	konferenční	k2eAgNnSc1d1	konferenční
centrum	centrum	k1gNnSc1	centrum
Svatých	svatý	k2eAgInPc2d1	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
prostorami	prostora	k1gFnPc7	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Utah	Utah	k1gInSc1	Utah
State	status	k1gInSc5	status
Capitol	Capitol	k1gInSc1	Capitol
-	-	kIx~	-
sídlo	sídlo	k1gNnSc1	sídlo
utažského	utažský	k2eAgInSc2d1	utažský
kongresu	kongres	k1gInSc2	kongres
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Capitol	Capitol	k1gInSc4	Capitol
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
památníkem	památník	k1gInSc7	památník
na	na	k7c4	na
první	první	k4xOgInSc4	první
osidlovatele	osidlovatel	k1gMnSc2	osidlovatel
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
This	This	k1gInSc1	This
Is	Is	k1gMnSc2	Is
The	The	k1gMnSc2	The
Place	plac	k1gInSc6	plac
Heritage	Heritage	k1gFnSc1	Heritage
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Park	park	k1gInSc1	park
dědictví	dědictví	k1gNnSc2	dědictví
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
místo	místo	k1gNnSc4	místo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnPc1d1	připomínající
prvotní	prvotní	k2eAgFnPc4d1	prvotní
dějiny	dějiny	k1gFnPc4	dějiny
mormonského	mormonský	k2eAgNnSc2d1	mormonské
osídlení	osídlení	k1gNnSc2	osídlení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
římsko-katolické	římskoatolický	k2eAgFnSc2d1	římsko-katolická
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
horská	horský	k2eAgNnPc4d1	horské
střediska	středisko	k1gNnPc4	středisko
Park	park	k1gInSc1	park
City	city	k1gNnSc1	city
nebo	nebo	k8xC	nebo
Deer	Deer	k1gInSc1	Deer
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hostila	hostit	k5eAaImAgFnS	hostit
soutěže	soutěž	k1gFnPc4	soutěž
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2002	[number]	k4	2002
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vyhledávána	vyhledáván	k2eAgNnPc4d1	vyhledáváno
turisty	turist	k1gMnPc4	turist
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Černovice	Černovice	k1gFnSc1	Černovice
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Keelung	Keelung	k1gInSc1	Keelung
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
Macumoto	Macumota	k1gFnSc5	Macumota
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Oruro	Oruro	k1gNnSc1	Oruro
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
Quezon	Quezona	k1gFnPc2	Quezona
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
Thurles	Thurlesa	k1gFnPc2	Thurlesa
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Církev	církev	k1gFnSc1	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gInSc2	Lak
Temple	templ	k1gInSc5	templ
Utah	Utah	k1gInSc1	Utah
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2002	[number]	k4	2002
Velké	velký	k2eAgNnSc1d1	velké
Solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
Brigham	Brigham	k1gInSc1	Brigham
Young	Young	k1gMnSc1	Young
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	City	k1gFnSc2	City
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc4	dílo
V	v	k7c6	v
městě	město	k1gNnSc6	město
"	"	kIx"	"
<g/>
Svatých	svatý	k1gMnPc2	svatý
soudného	soudný	k2eAgInSc2d1	soudný
dne	den	k1gInSc2	den
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Stránka	stránka	k1gFnSc1	stránka
městské	městský	k2eAgFnSc2d1	městská
správy	správa	k1gFnSc2	správa
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc3	Lake
Convention	Convention	k1gInSc1	Convention
&	&	k?	&
Visitors	Visitors	k1gInSc4	Visitors
Bureau	Bureaa	k1gMnSc4	Bureaa
-	-	kIx~	-
turistické	turistický	k2eAgFnSc2d1	turistická
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
informace	informace	k1gFnSc2	informace
SLCTravel	SLCTravela	k1gFnPc2	SLCTravela
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
turistické	turistický	k2eAgFnSc2d1	turistická
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
informace	informace	k1gFnSc2	informace
Olympijský	olympijský	k2eAgInSc1d1	olympijský
park	park	k1gInSc1	park
-	-	kIx~	-
bývalá	bývalý	k2eAgFnSc1d1	bývalá
stránka	stránka	k1gFnSc1	stránka
ZOH	ZOH	kA	ZOH
2002	[number]	k4	2002
</s>
