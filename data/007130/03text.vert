<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
nebo	nebo	k8xC	nebo
též	též	k9	též
helikoptéra	helikoptéra	k1gFnSc1	helikoptéra
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
rotorového	rotorový	k2eAgNnSc2d1	rotorové
letadla	letadlo	k1gNnSc2	letadlo
těžšího	těžký	k2eAgNnSc2d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
s	s	k7c7	s
poháněnými	poháněný	k2eAgFnPc7d1	poháněná
horizontálně	horizontálně	k6eAd1	horizontálně
rotujícími	rotující	k2eAgFnPc7d1	rotující
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
helikoptéra	helikoptéra	k1gFnSc1	helikoptéra
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
helix	helix	k1gInSc1	helix
(	(	kIx(	(
<g/>
spirála	spirála	k1gFnSc1	spirála
<g/>
,	,	kIx,	,
šroub	šroub	k1gInSc1	šroub
<g/>
)	)	kIx)	)
a	a	k8xC	a
pteron	pteron	k1gInSc1	pteron
(	(	kIx(	(
<g/>
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hračka	hračka	k1gFnSc1	hračka
s	s	k7c7	s
rotujícími	rotující	k2eAgFnPc7d1	rotující
plochami	plocha	k1gFnPc7	plocha
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
už	už	k6eAd1	už
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
popsán	popsat	k5eAaPmNgInS	popsat
i	i	k9	i
obecný	obecný	k2eAgInSc1d1	obecný
princip	princip	k1gInSc1	princip
vrtulníku	vrtulník	k1gInSc2	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
nákresech	nákres	k1gInPc6	nákres
popsal	popsat	k5eAaPmAgMnS	popsat
také	také	k9	také
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc4	stroj
podobné	podobný	k2eAgInPc4d1	podobný
vrtulníkům	vrtulník	k1gInPc3	vrtulník
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
i	i	k8xC	i
románech	román	k1gInPc6	román
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
(	(	kIx(	(
<g/>
Robur	Robur	k1gMnSc1	Robur
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
<g/>
,	,	kIx,	,
Podivuhodná	podivuhodný	k2eAgNnPc1d1	podivuhodné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
výpravy	výprava	k1gFnSc2	výprava
Barsacovy	Barsacův	k2eAgFnSc2d1	Barsacův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
návrh	návrh	k1gInSc4	návrh
vrtulníku	vrtulník	k1gInSc2	vrtulník
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
lidské	lidský	k2eAgFnSc2d1	lidská
síly	síla	k1gFnSc2	síla
slovenský	slovenský	k2eAgMnSc1d1	slovenský
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
konstruktér	konstruktér	k1gMnSc1	konstruktér
Ján	Ján	k1gMnSc1	Ján
Bahýľ	Bahýľ	k1gMnSc1	Bahýľ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1906	[number]	k4	1906
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
tzv.	tzv.	kA	tzv.
popis	popis	k1gInSc4	popis
patentu	patent	k1gInSc2	patent
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
další	další	k2eAgInSc4d1	další
osud	osud	k1gInSc4	osud
jeho	jeho	k3xOp3gInSc2	jeho
vrtulníku	vrtulník	k1gInSc2	vrtulník
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
administrativním	administrativní	k2eAgInPc3d1	administrativní
problémům	problém	k1gInPc3	problém
není	být	k5eNaImIp3nS	být
prvenství	prvenství	k1gNnSc1	prvenství
Jána	Ján	k1gMnSc2	Ján
Bahýľa	Bahýľus	k1gMnSc2	Bahýľus
všeobecně	všeobecně	k6eAd1	všeobecně
uznávané	uznávaný	k2eAgInPc1d1	uznávaný
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zdokumentovaný	zdokumentovaný	k2eAgInSc4d1	zdokumentovaný
vrtulník	vrtulník	k1gInSc4	vrtulník
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
jízdními	jízdní	k2eAgNnPc7d1	jízdní
koly	kolo	k1gNnPc7	kolo
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Cornu	Corn	k1gInSc2	Corn
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
strojem	stroj	k1gInSc7	stroj
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
šestimetrovými	šestimetrový	k2eAgFnPc7d1	šestimetrová
čtyřlistými	čtyřlistý	k2eAgFnPc7d1	čtyřlistá
vrtulemi	vrtule	k1gFnPc7	vrtule
a	a	k8xC	a
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
24	[number]	k4	24
koňských	koňský	k2eAgFnPc2d1	koňská
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
vznesl	vznést	k5eAaPmAgMnS	vznést
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
nad	nad	k7c4	nad
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
pokus	pokus	k1gInSc4	pokus
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
vznesl	vznést	k5eAaPmAgMnS	vznést
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
1,5	[number]	k4	1,5
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
stroj	stroj	k1gInSc4	stroj
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
nijak	nijak	k6eAd1	nijak
stranově	stranově	k6eAd1	stranově
ovládat	ovládat	k5eAaImF	ovládat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
helikoptéru	helikoptéra	k1gFnSc4	helikoptéra
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
o	o	k7c4	o
stroj	stroj	k1gInSc4	stroj
zajímal	zajímat	k5eAaImAgMnS	zajímat
například	například	k6eAd1	například
Igor	Igor	k1gMnSc1	Igor
Sikorski	Sikorsk	k1gFnSc2	Sikorsk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgInSc1d1	funkční
vrtulník	vrtulník	k1gInSc1	vrtulník
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
rotující	rotující	k2eAgFnSc7d1	rotující
nosnou	nosný	k2eAgFnSc7d1	nosná
plochou	plocha	k1gFnSc7	plocha
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Německo	Německo	k1gNnSc1	Německo
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
firmy	firma	k1gFnPc1	firma
Flettner	Flettnero	k1gNnPc2	Flettnero
a	a	k8xC	a
Focke	Focke	k1gFnPc2	Focke
Achgelis	Achgelis	k1gFnSc2	Achgelis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
skončení	skončení	k1gNnSc6	skončení
převzaly	převzít	k5eAaPmAgInP	převzít
iniciativu	iniciativa	k1gFnSc4	iniciativa
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
hlavně	hlavně	k9	hlavně
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nosném	nosný	k2eAgInSc6d1	nosný
rotoru	rotor	k1gInSc6	rotor
vrtulníku	vrtulník	k1gInSc2	vrtulník
vzniká	vznikat	k5eAaImIp3nS	vznikat
aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
vztlak	vztlak	k1gInSc4	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
horizontálnímu	horizontální	k2eAgInSc3d1	horizontální
letu	let	k1gInSc3	let
vrtulníku	vrtulník	k1gInSc2	vrtulník
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
vyvinout	vyvinout	k5eAaPmF	vyvinout
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
vlivem	vliv	k1gInSc7	vliv
aerodynamických	aerodynamický	k2eAgFnPc2d1	aerodynamická
sil	síla	k1gFnPc2	síla
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
na	na	k7c6	na
nosném	nosný	k2eAgInSc6d1	nosný
rotoru	rotor	k1gInSc6	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Naklápění	naklápění	k1gNnSc1	naklápění
kolem	kolem	k7c2	kolem
horizontálních	horizontální	k2eAgFnPc2d1	horizontální
os	osa	k1gFnPc2	osa
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
změnou	změna	k1gFnSc7	změna
úhlu	úhel	k1gInSc2	úhel
náběhu	náběh	k1gInSc3	náběh
listů	list	k1gInPc2	list
rotoru	rotor	k1gInSc2	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Kroutící	kroutící	k2eAgInSc1d1	kroutící
moment	moment	k1gInSc1	moment
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
pohonem	pohon	k1gInSc7	pohon
hlavního	hlavní	k2eAgInSc2d1	hlavní
rotoru	rotor	k1gInSc2	rotor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kompenzován	kompenzovat	k5eAaBmNgInS	kompenzovat
pomocným	pomocný	k2eAgInSc7d1	pomocný
rotorem	rotor	k1gInSc7	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
řídí	řídit	k5eAaImIp3nS	řídit
otáčení	otáčení	k1gNnSc1	otáčení
kolem	kolem	k7c2	kolem
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Pomocný	pomocný	k2eAgInSc1d1	pomocný
rotor	rotor	k1gInSc1	rotor
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
realizován	realizovat	k5eAaBmNgInS	realizovat
jako	jako	k8xC	jako
vertikální	vertikální	k2eAgFnSc1d1	vertikální
vrtule	vrtule	k1gFnSc1	vrtule
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
stroje	stroj	k1gInSc2	stroj
(	(	kIx(	(
<g/>
ocasní	ocasní	k2eAgInSc1d1	ocasní
rotor	rotor	k1gInSc1	rotor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
výrobců	výrobce	k1gMnPc2	výrobce
bývá	bývat	k5eAaImIp3nS	bývat
ocasní	ocasní	k2eAgFnSc1d1	ocasní
vrtule	vrtule	k1gFnSc1	vrtule
nahrazována	nahrazován	k2eAgFnSc1d1	nahrazována
dmychadlem	dmychadlo	k1gNnSc7	dmychadlo
s	s	k7c7	s
vodicím	vodicí	k2eAgInSc7d1	vodicí
prstencem	prstenec	k1gInSc7	prstenec
(	(	kIx(	(
<g/>
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
ocasním	ocasní	k2eAgInSc7d1	ocasní
rotorem	rotor	k1gInSc7	rotor
<g/>
,	,	kIx,	,
u	u	k7c2	u
vrtulníků	vrtulník	k1gInPc2	vrtulník
skupiny	skupina	k1gFnSc2	skupina
Eurocopter	Eurocoptra	k1gFnPc2	Eurocoptra
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
Fenestron	Fenestron	k1gInSc1	Fenestron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulníky	vrtulník	k1gInPc1	vrtulník
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
výrobců	výrobce	k1gMnPc2	výrobce
užívají	užívat	k5eAaImIp3nP	užívat
technologie	technologie	k1gFnPc1	technologie
NOTAR	NOTAR	kA	NOTAR
(	(	kIx(	(
<g/>
No	no	k9	no
Tail	Tail	k1gInSc1	Tail
Rotor	rotor	k1gInSc1	rotor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Točivý	točivý	k2eAgInSc1d1	točivý
moment	moment	k1gInSc1	moment
hlavního	hlavní	k2eAgInSc2d1	hlavní
rotoru	rotor	k1gInSc2	rotor
je	být	k5eAaImIp3nS	být
kompenzován	kompenzovat	k5eAaBmNgInS	kompenzovat
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
usměrňovaným	usměrňovaný	k2eAgInSc7d1	usměrňovaný
vertikálními	vertikální	k2eAgFnPc7d1	vertikální
řídicími	řídicí	k2eAgFnPc7d1	řídicí
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Kroutící	kroutící	k2eAgInSc1d1	kroutící
moment	moment	k1gInSc1	moment
lze	lze	k6eAd1	lze
také	také	k9	také
eliminovat	eliminovat	k5eAaBmF	eliminovat
použitím	použití	k1gNnSc7	použití
druhého	druhý	k4xOgInSc2	druhý
horizontálního	horizontální	k2eAgInSc2d1	horizontální
rotoru	rotor	k1gInSc2	rotor
otáčejícího	otáčející	k2eAgInSc2d1	otáčející
se	se	k3xPyFc4	se
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
než	než	k8xS	než
první	první	k4xOgInSc4	první
horizontální	horizontální	k2eAgInSc4d1	horizontální
rotor	rotor	k1gInSc4	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rotor	rotor	k1gInSc1	rotor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěn	umístit	k5eAaPmNgInS	umístit
jak	jak	k8xC	jak
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
prvního	první	k4xOgInSc2	první
rotoru	rotor	k1gInSc2	rotor
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
potřeba	potřeba	k6eAd1	potřeba
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
listy	list	k1gInPc1	list
obou	dva	k4xCgInPc2	dva
rotorů	rotor	k1gInPc2	rotor
nestřetnou	střetnout	k5eNaPmIp3nP	střetnout
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulníky	vrtulník	k1gInPc4	vrtulník
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
počet	počet	k1gInSc1	počet
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
rotorů	rotor	k1gInPc2	rotor
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
přistávací	přistávací	k2eAgFnSc1d1	přistávací
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
účel	účel	k1gInSc4	účel
vrtulníku	vrtulník	k1gInSc2	vrtulník
atd.	atd.	kA	atd.
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
parametrů	parametr	k1gInPc2	parametr
vrtulníků	vrtulník	k1gInPc2	vrtulník
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
lze	lze	k6eAd1	lze
vrtulníky	vrtulník	k1gInPc4	vrtulník
dělit	dělit	k5eAaImF	dělit
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
ultralehké	ultralehký	k2eAgInPc1d1	ultralehký
vrtulníky	vrtulník	k1gInPc1	vrtulník
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
vzletovou	vzletový	k2eAgFnSc7d1	vzletová
hmotností	hmotnost	k1gFnSc7	hmotnost
do	do	k7c2	do
600	[number]	k4	600
kg	kg	kA	kg
lehké	lehký	k2eAgInPc4d1	lehký
vrtulníky	vrtulník	k1gInPc4	vrtulník
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
vzletovou	vzletový	k2eAgFnSc7d1	vzletová
hmotností	hmotnost	k1gFnSc7	hmotnost
do	do	k7c2	do
2500	[number]	k4	2500
kg	kg	kA	kg
střední	střední	k2eAgInPc4d1	střední
vrtulníky	vrtulník	k1gInPc4	vrtulník
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
vzletovou	vzletový	k2eAgFnSc7d1	vzletová
hmotností	hmotnost	k1gFnSc7	hmotnost
do	do	k7c2	do
8000	[number]	k4	8000
kg	kg	kA	kg
těžké	těžký	k2eAgInPc1d1	těžký
vrtulníky	vrtulník	k1gInPc1	vrtulník
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
vzletovou	vzletový	k2eAgFnSc7d1	vzletová
hmotností	hmotnost	k1gFnSc7	hmotnost
nad	nad	k7c4	nad
8000	[number]	k4	8000
kg	kg	kA	kg
supertěžké	supertěžký	k2eAgInPc4d1	supertěžký
vrtulníky	vrtulník	k1gInPc4	vrtulník
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
vzletovou	vzletový	k2eAgFnSc7d1	vzletová
hmotností	hmotnost	k1gFnSc7	hmotnost
nad	nad	k7c7	nad
25	[number]	k4	25
000	[number]	k4	000
kg	kg	kA	kg
Vrtulník	vrtulník	k1gInSc1	vrtulník
se	se	k3xPyFc4	se
pilotuje	pilotovat	k5eAaImIp3nS	pilotovat
z	z	k7c2	z
levého	levý	k2eAgInSc2d1	levý
i	i	k8xC	i
pravého	pravý	k2eAgInSc2d1	pravý
postu	post	k1gInSc2	post
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
smyslu	smysl	k1gInSc6	smysl
otáčení	otáčení	k1gNnSc2	otáčení
hlavního	hlavní	k2eAgInSc2d1	hlavní
rotoru	rotor	k1gInSc2	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Pravotočivé	pravotočivý	k2eAgInPc1d1	pravotočivý
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
pilotují	pilotovat	k5eAaImIp3nP	pilotovat
zleva	zleva	k6eAd1	zleva
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
technikou	technika	k1gFnSc7	technika
startu	start	k1gInSc2	start
–	–	k?	–
takzvaným	takzvaný	k2eAgNnPc3d1	takzvané
bočením	bočení	k1gNnPc3	bočení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
rozběhnutí	rozběhnutí	k1gNnSc3	rozběhnutí
vrtulníku	vrtulník	k1gInSc2	vrtulník
užívá	užívat	k5eAaImIp3nS	užívat
tahu	tah	k1gInSc3	tah
vyrovnávacího	vyrovnávací	k2eAgInSc2d1	vyrovnávací
rotoru	rotor	k1gInSc2	rotor
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
musí	muset	k5eAaImIp3nS	muset
vidět	vidět	k5eAaImF	vidět
do	do	k7c2	do
osy	osa	k1gFnSc2	osa
vzletu	vzlet	k1gInSc2	vzlet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
vrtulníků	vrtulník	k1gInPc2	vrtulník
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
několika	několik	k4yIc2	několik
ovládacích	ovládací	k2eAgFnPc2d1	ovládací
pák	páka	k1gFnPc2	páka
<g/>
:	:	kIx,	:
Páka	páka	k1gFnSc1	páka
cyklického	cyklický	k2eAgNnSc2d1	cyklické
řízení	řízení	k1gNnSc2	řízení
ovládá	ovládat	k5eAaImIp3nS	ovládat
klopení	klopení	k1gNnSc4	klopení
a	a	k8xC	a
klonění	klonění	k1gNnSc4	klonění
<g/>
.	.	kIx.	.
</s>
<s>
Páka	páka	k1gFnSc1	páka
sdruženého	sdružený	k2eAgNnSc2d1	sdružené
ovládání	ovládání	k1gNnSc2	ovládání
listů	list	k1gInPc2	list
hlavního	hlavní	k2eAgInSc2d1	hlavní
rotoru	rotor	k1gInSc2	rotor
–	–	k?	–
kolektivu	kolektiv	k1gInSc2	kolektiv
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
stoupání	stoupání	k1gNnSc1	stoupání
a	a	k8xC	a
klesání	klesání	k1gNnSc1	klesání
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řídicí	řídicí	k2eAgInPc4d1	řídicí
prvky	prvek	k1gInPc4	prvek
pohonné	pohonný	k2eAgFnSc2d1	pohonná
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
plynová	plynový	k2eAgFnSc1d1	plynová
přípusť	přípustit	k5eAaPmRp2nS	přípustit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pedály	pedál	k1gInPc1	pedál
ovládají	ovládat	k5eAaImIp3nP	ovládat
úhel	úhel	k1gInSc4	úhel
nastavení	nastavení	k1gNnSc2	nastavení
listů	list	k1gInPc2	list
vyrovnávacího	vyrovnávací	k2eAgInSc2d1	vyrovnávací
rotoru	rotor	k1gInSc2	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
pracuje	pracovat	k5eAaImIp3nS	pracovat
též	též	k9	též
vírník	vírník	k1gInSc1	vírník
(	(	kIx(	(
<g/>
autogyra	autogyra	k1gFnSc1	autogyra
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
nepoháněná	poháněný	k2eNgFnSc1d1	nepoháněná
rotující	rotující	k2eAgFnSc1d1	rotující
nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
a	a	k8xC	a
tlačná	tlačný	k2eAgFnSc1d1	tlačná
vrtule	vrtule	k1gFnSc1	vrtule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
letovými	letový	k2eAgFnPc7d1	letová
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
vrtulníku	vrtulník	k1gInSc2	vrtulník
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
autorotace	autorotace	k1gFnSc1	autorotace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysazení	vysazení	k1gNnSc2	vysazení
nebo	nebo	k8xC	nebo
vypnutí	vypnutí	k1gNnSc2	vypnutí
pohonné	pohonný	k2eAgFnSc2d1	pohonná
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
motoru	motor	k1gInSc2	motor
<g/>
)	)	kIx)	)
začne	začít	k5eAaPmIp3nS	začít
vrtulník	vrtulník	k1gInSc4	vrtulník
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
protékajícího	protékající	k2eAgInSc2d1	protékající
rotorem	rotor	k1gInSc7	rotor
na	na	k7c4	na
listy	list	k1gInPc4	list
nosného	nosný	k2eAgInSc2d1	nosný
rotoru	rotor	k1gInSc2	rotor
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
roztáčí	roztáčet	k5eAaImIp3nS	roztáčet
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
s	s	k7c7	s
vrtulníkem	vrtulník	k1gInSc7	vrtulník
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
manévrovat	manévrovat	k5eAaImF	manévrovat
a	a	k8xC	a
poté	poté	k6eAd1	poté
úspěšně	úspěšně	k6eAd1	úspěšně
přistát	přistát	k5eAaImF	přistát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
základní	základní	k2eAgInSc1d1	základní
princip	princip	k1gInSc1	princip
fungování	fungování	k1gNnSc2	fungování
vírníku	vírník	k1gInSc2	vírník
a	a	k8xC	a
autogyry	autogyra	k1gFnSc2	autogyra
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
letovou	letový	k2eAgFnSc4d1	letová
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
Svazu	svaz	k1gInSc6	svaz
se	se	k3xPyFc4	se
konstrukcí	konstrukce	k1gFnSc7	konstrukce
vrtulníků	vrtulník	k1gInPc2	vrtulník
zabývalo	zabývat	k5eAaImAgNnS	zabývat
několik	několik	k4yIc1	několik
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
patřily	patřit	k5eAaImAgFnP	patřit
OKB	OKB	kA	OKB
(	(	kIx(	(
<g/>
Opytno	Opytno	k6eAd1	Opytno
konstrukčnoje	konstrukčnoj	k1gInPc1	konstrukčnoj
bjuro	bjuro	k1gNnSc1	bjuro
<g/>
)	)	kIx)	)
Mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
Kamov	Kamovo	k1gNnPc2	Kamovo
a	a	k8xC	a
Jakovlev	Jakovlev	k1gFnPc2	Jakovlev
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
s	s	k7c7	s
pokusy	pokus	k1gInPc7	pokus
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Mil	míle	k1gFnPc2	míle
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
vrtulníků	vrtulník	k1gInPc2	vrtulník
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgFnPc4d1	pozemní
armádní	armádní	k2eAgFnPc4d1	armádní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
Kamov	Kamov	k1gInSc4	Kamov
pro	pro	k7c4	pro
námořní	námořní	k2eAgNnSc4d1	námořní
letectvo	letectvo	k1gNnSc4	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
konstrukční	konstrukční	k2eAgFnPc1d1	konstrukční
kanceláře	kancelář	k1gFnPc1	kancelář
vyprodukovaly	vyprodukovat	k5eAaPmAgFnP	vyprodukovat
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Mil	míle	k1gFnPc2	míle
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
klasickou	klasický	k2eAgFnSc4d1	klasická
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaImAgInS	využívat
osvědčené	osvědčený	k2eAgFnPc4d1	osvědčená
kombinace	kombinace	k1gFnPc4	kombinace
nosného	nosný	k2eAgMnSc2d1	nosný
a	a	k8xC	a
vyrovnávacího	vyrovnávací	k2eAgInSc2d1	vyrovnávací
rotoru	rotor	k1gInSc2	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Kamov	Kamov	k1gInSc1	Kamov
začal	začít	k5eAaPmAgInS	začít
využívat	využívat	k5eAaPmF	využívat
dva	dva	k4xCgInPc1	dva
souosé	souosý	k2eAgInPc1d1	souosý
protiběžné	protiběžný	k2eAgInPc1d1	protiběžný
rotory	rotor	k1gInPc1	rotor
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
Milovy	Milův	k2eAgInPc4d1	Milův
nebitevní	bitevní	k2eNgInPc4d1	bitevní
výrobky	výrobek	k1gInPc4	výrobek
patří	patřit	k5eAaImIp3nS	patřit
lehké	lehký	k2eAgNnSc1d1	lehké
spojovací	spojovací	k2eAgFnSc7d1	spojovací
Mi-	Mi-	k1gFnSc7	Mi-
<g/>
1	[number]	k4	1
a	a	k8xC	a
Mi-	Mi-	k1gMnSc1	Mi-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
Mi-	Mi-	k1gFnSc6	Mi-
<g/>
4	[number]	k4	4
a	a	k8xC	a
těžké	těžký	k2eAgFnPc1d1	těžká
Mi-	Mi-	k1gFnPc1	Mi-
<g/>
8	[number]	k4	8
a	a	k8xC	a
modernizovaný	modernizovaný	k2eAgMnSc1d1	modernizovaný
Mi-	Mi-	k1gMnSc1	Mi-
<g/>
17	[number]	k4	17
(	(	kIx(	(
<g/>
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
značen	značen	k2eAgMnSc1d1	značen
Mi-	Mi-	k1gMnSc1	Mi-
<g/>
8	[number]	k4	8
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
armády	armáda	k1gFnSc2	armáda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
bitevních	bitevní	k2eAgFnPc2d1	bitevní
modifikací	modifikace	k1gFnPc2	modifikace
transportního	transportní	k2eAgInSc2d1	transportní
vrtulníku	vrtulník	k1gInSc2	vrtulník
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
8	[number]	k4	8
<g/>
TB	TB	kA	TB
<g/>
,	,	kIx,	,
MTB	MTB	kA	MTB
<g/>
,	,	kIx,	,
exportní	exportní	k2eAgFnSc1d1	exportní
TBK	TBK	kA	TBK
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
sovětského	sovětský	k2eAgNnSc2d1	sovětské
letectva	letectvo	k1gNnSc2	letectvo
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
transportní	transportní	k2eAgInSc1d1	transportní
vrtulník	vrtulník	k1gInSc1	vrtulník
není	být	k5eNaImIp3nS	být
ideálním	ideální	k2eAgNnSc7d1	ideální
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Zadalo	zadat	k5eAaPmAgNnS	zadat
tedy	tedy	k9	tedy
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
nového	nový	k2eAgInSc2d1	nový
bitevního	bitevní	k2eAgInSc2d1	bitevní
vrtulníku	vrtulník	k1gInSc2	vrtulník
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vynikající	vynikající	k2eAgInSc1d1	vynikající
bitevní	bitevní	k2eAgInSc1d1	bitevní
a	a	k8xC	a
víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
vrtulník	vrtulník	k1gInSc1	vrtulník
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
v	v	k7c6	v
exportních	exportní	k2eAgFnPc6d1	exportní
variantách	varianta	k1gFnPc6	varianta
značen	značen	k2eAgInSc4d1	značen
Mi-	Mi-	k1gFnSc7	Mi-
<g/>
25	[number]	k4	25
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
Mil	míle	k1gFnPc2	míle
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
Mi-	Mi-	k1gMnSc7	Mi-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
bitevní	bitevní	k2eAgMnSc1d1	bitevní
bez	bez	k7c2	bez
prostor	prostora	k1gFnPc2	prostora
k	k	k7c3	k
transportu	transport	k1gInSc3	transport
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pozvolně	pozvolně	k6eAd1	pozvolně
zaváděn	zavádět	k5eAaImNgMnS	zavádět
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgInPc4d1	známý
Milovy	Milův	k2eAgInPc4d1	Milův
projekty	projekt	k1gInPc4	projekt
patří	patřit	k5eAaImIp3nP	patřit
obří	obří	k2eAgInPc1d1	obří
vrtulníky	vrtulník	k1gInPc1	vrtulník
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
pokusný	pokusný	k2eAgMnSc1d1	pokusný
Mi-	Mi-	k1gMnSc1	Mi-
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
vrtulník	vrtulník	k1gInSc1	vrtulník
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mi-	Mi-	k1gMnSc1	Mi-
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Kamov	Kamov	k1gInSc1	Kamov
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
lehké	lehký	k2eAgInPc4d1	lehký
typy	typ	k1gInPc4	typ
Ka-	Ka-	k1gFnSc2	Ka-
<g/>
15	[number]	k4	15
a	a	k8xC	a
Ka-	Ka-	k1gMnSc1	Ka-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
střední	střední	k2eAgInPc1d1	střední
vrtulníky	vrtulník	k1gInPc1	vrtulník
Ka-	Ka-	k1gFnSc2	Ka-
<g/>
25	[number]	k4	25
a	a	k8xC	a
Ka-	Ka-	k1gMnSc1	Ka-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
výrobce	výrobce	k1gMnSc1	výrobce
dodával	dodávat	k5eAaImAgMnS	dodávat
i	i	k9	i
v	v	k7c6	v
protiponorkové	protiponorkový	k2eAgFnSc6d1	protiponorková
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ka-	Ka-	k1gFnSc2	Ka-
<g/>
27	[number]	k4	27
později	pozdě	k6eAd2	pozdě
konstruktéři	konstruktér	k1gMnPc1	konstruktér
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
civilní	civilní	k2eAgFnSc4d1	civilní
Ka-	Ka-	k1gFnSc4	Ka-
<g/>
32	[number]	k4	32
a	a	k8xC	a
bitevní	bitevní	k2eAgFnPc1d1	bitevní
<g/>
/	/	kIx~	/
<g/>
transportní	transportní	k2eAgFnPc1d1	transportní
Ka-	Ka-	k1gFnPc1	Ka-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Kamov	Kamov	k1gInSc1	Kamov
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
Ka-	Ka-	k1gFnSc1	Ka-
<g/>
50	[number]	k4	50
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
poněkud	poněkud	k6eAd1	poněkud
netradičně	tradičně	k6eNd1	tradičně
tendru	tendr	k1gInSc2	tendr
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
nového	nový	k2eAgInSc2d1	nový
bitevního	bitevní	k2eAgInSc2d1	bitevní
vrtulníku	vrtulník	k1gInSc2	vrtulník
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgFnPc4d1	pozemní
složky	složka	k1gFnPc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Soupeřil	soupeřit	k5eAaImAgMnS	soupeřit
zde	zde	k6eAd1	zde
s	s	k7c7	s
Milovým	Milův	k2eAgInSc7d1	Milův
konceptem	koncept	k1gInSc7	koncept
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
několikrát	několikrát	k6eAd1	několikrát
změnila	změnit	k5eAaPmAgFnS	změnit
stanovisko	stanovisko	k1gNnSc4	stanovisko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
přibírány	přibírat	k5eAaImNgFnP	přibírat
i	i	k8xC	i
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
Ka-	Ka-	k1gFnSc2	Ka-
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Ka-	Ka-	k?	Ka-
<g/>
50	[number]	k4	50
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
ojedinělým	ojedinělý	k2eAgInPc3d1	ojedinělý
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvoří	tvořit	k5eAaImIp3nS	tvořit
díky	díky	k7c3	díky
pokročilé	pokročilý	k2eAgFnSc3d1	pokročilá
avionice	avionika	k1gFnSc3	avionika
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
své	svůj	k3xOyFgFnSc2	svůj
mateřské	mateřský	k2eAgFnSc2d1	mateřská
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
souosých	souosý	k2eAgInPc2d1	souosý
rotorů	rotor	k1gInPc2	rotor
eliminovalo	eliminovat	k5eAaBmAgNnS	eliminovat
potřebu	potřeba	k1gFnSc4	potřeba
instalovat	instalovat	k5eAaBmF	instalovat
ocasní	ocasní	k2eAgInSc4d1	ocasní
rotor	rotor	k1gInSc4	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Kamov	Kamov	k1gInSc1	Kamov
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
slabinu	slabina	k1gFnSc4	slabina
všech	všecek	k3xTgInPc2	všecek
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ztrát	ztráta	k1gFnPc2	ztráta
v	v	k7c6	v
boji	boj	k1gInSc6	boj
totiž	totiž	k9	totiž
připadá	připadat	k5eAaImIp3nS	připadat
právě	právě	k9	právě
na	na	k7c4	na
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
ocasního	ocasní	k2eAgInSc2d1	ocasní
rotoru	rotor	k1gInSc2	rotor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
snahy	snaha	k1gFnPc1	snaha
obou	dva	k4xCgInPc2	dva
výrobců	výrobce	k1gMnPc2	výrobce
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
modernizační	modernizační	k2eAgFnPc4d1	modernizační
úpravy	úprava	k1gFnPc4	úprava
stávajících	stávající	k2eAgInPc2d1	stávající
zavedených	zavedený	k2eAgInPc2d1	zavedený
typů	typ	k1gInPc2	typ
používaných	používaný	k2eAgInPc2d1	používaný
jako	jako	k8xC	jako
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
i	i	k9	i
jako	jako	k8xS	jako
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
