<s>
Praia	Praia	k1gFnSc1	Praia
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Pláž	pláž	k1gFnSc1	pláž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
přístavní	přístavní	k2eAgNnSc1d1	přístavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
afrického	africký	k2eAgInSc2d1	africký
ostrovního	ostrovní	k2eAgInSc2d1	ostrovní
státu	stát	k1gInSc2	stát
Kapverdy	Kapverda	k1gFnSc2	Kapverda
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
127	[number]	k4	127
832	[number]	k4	832
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
61	[number]	k4	61
644	[number]	k4	644
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
a	a	k8xC	a
tropické	tropický	k2eAgNnSc1d1	tropické
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Praia	Praium	k1gNnSc2	Praium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
