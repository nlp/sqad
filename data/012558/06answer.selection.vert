<s>
Mont	Mont	k2eAgMnSc1d1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nejednotné	jednotný	k2eNgFnSc2d1	nejednotná
metodiky	metodika	k1gFnSc2	metodika
pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
určení	určení	k1gNnSc4	určení
evropsko-asijské	evropskosijský	k2eAgFnSc2d1	evropsko-asijský
hranice	hranice	k1gFnSc2	hranice
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
připisován	připisovat	k5eAaImNgInS	připisovat
i	i	k8xC	i
kavkazské	kavkazský	k2eAgNnSc1d1	kavkazské
hoře	hoře	k1gNnSc1	hoře
Elbrus	Elbrus	k1gInSc1	Elbrus
(	(	kIx(	(
<g/>
5642	[number]	k4	5642
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
