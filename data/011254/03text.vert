<p>
<s>
Horst	Horst	k1gMnSc1	Horst
Fuchs	Fuchs	k1gMnSc1	Fuchs
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
teleshoppingový	teleshoppingový	k2eAgMnSc1d1	teleshoppingový
prodavač	prodavač	k1gMnSc1	prodavač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
a	a	k8xC	a
původně	původně	k6eAd1	původně
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
v	v	k7c6	v
automobilce	automobilka	k1gFnSc6	automobilka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jeho	jeho	k3xOp3gFnSc7	jeho
obchodnický	obchodnický	k2eAgInSc1d1	obchodnický
talent	talent	k1gInSc1	talent
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
auta	auto	k1gNnPc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
je	být	k5eAaImIp3nS	být
prodával	prodávat	k5eAaImAgMnS	prodávat
na	na	k7c6	na
pouličních	pouliční	k2eAgInPc6d1	pouliční
trzích	trh	k1gInPc6	trh
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
nabízením	nabízení	k1gNnSc7	nabízení
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Teleshoppingové	Teleshoppingový	k2eAgInPc4d1	Teleshoppingový
pořady	pořad	k1gInPc4	pořad
od	od	k7c2	od
WS	WS	kA	WS
International	International	k1gFnSc1	International
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
vysílaly	vysílat	k5eAaImAgFnP	vysílat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
obchodně	obchodně	k6eAd1	obchodně
zabýval	zabývat	k5eAaImAgInS	zabývat
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
2000	[number]	k4	2000
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
televizi	televize	k1gFnSc6	televize
QVC	QVC	kA	QVC
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
pořady	pořad	k1gInPc1	pořad
vysílány	vysílat	k5eAaImNgInP	vysílat
také	také	k9	také
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horst	Horst	k1gMnSc1	Horst
Fuchs	Fuchs	k1gMnSc1	Fuchs
je	být	k5eAaImIp3nS	být
kreativním	kreativní	k2eAgMnSc7d1	kreativní
ředitelem	ředitel	k1gMnSc7	ředitel
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
firmě	firma	k1gFnSc6	firma
WS	WS	kA	WS
Invention	Invention	k1gInSc1	Invention
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
teleshoppingové	teleshoppingový	k2eAgInPc4d1	teleshoppingový
spoty	spot	k1gInPc4	spot
připravuje	připravovat	k5eAaImIp3nS	připravovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teleshopping	Teleshopping	k1gInSc4	Teleshopping
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Horsta	Horst	k1gMnSc4	Horst
Fuchse	Fuchs	k1gMnSc4	Fuchs
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
okázalé	okázalý	k2eAgNnSc1d1	okázalé
vystupování	vystupování	k1gNnSc1	vystupování
<g/>
,	,	kIx,	,
výrazné	výrazný	k2eAgNnSc1d1	výrazné
oblečení	oblečení	k1gNnSc1	oblečení
většinou	většinou	k6eAd1	většinou
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
náušnice	náušnice	k1gFnSc2	náušnice
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
brýle	brýle	k1gFnPc4	brýle
s	s	k7c7	s
tónovanými	tónovaný	k2eAgNnPc7d1	tónované
skly	sklo	k1gNnPc7	sklo
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
lehce	lehko	k6eAd1	lehko
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
prodávaném	prodávaný	k2eAgInSc6d1	prodávaný
předmětu	předmět	k1gInSc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
reklamy	reklama	k1gFnPc1	reklama
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
značným	značný	k2eAgInSc7d1	značný
množstvím	množství	k1gNnSc7	množství
jakýchsi	jakýsi	k3yIgFnPc2	jakýsi
praktických	praktický	k2eAgFnPc2d1	praktická
zkoušek	zkouška	k1gFnPc2	zkouška
kvalit	kvalita	k1gFnPc2	kvalita
prodávaného	prodávaný	k2eAgInSc2d1	prodávaný
výrobku	výrobek	k1gInSc2	výrobek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
smažení	smažení	k1gNnSc1	smažení
vajíček	vajíčko	k1gNnPc2	vajíčko
na	na	k7c4	na
karosérii	karosérie	k1gFnSc4	karosérie
auta	auto	k1gNnSc2	auto
ošetřené	ošetřený	k2eAgMnPc4d1	ošetřený
speciálním	speciální	k2eAgInSc7d1	speciální
přípravkem	přípravek	k1gInSc7	přípravek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
produktům	produkt	k1gInPc3	produkt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Fuchs	Fuchs	k1gMnSc1	Fuchs
v	v	k7c4	v
televizi	televize	k1gFnSc4	televize
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
kráječ	kráječ	k1gInSc1	kráječ
na	na	k7c4	na
zeleninu	zelenina	k1gFnSc4	zelenina
nebo	nebo	k8xC	nebo
přípravek	přípravek	k1gInSc4	přípravek
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
karosérií	karosérie	k1gFnPc2	karosérie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jeho	jeho	k3xOp3gInPc4	jeho
teleshoppingové	teleshoppingový	k2eAgInPc4d1	teleshoppingový
pořady	pořad	k1gInPc4	pořad
dabuje	dabovat	k5eAaBmIp3nS	dabovat
herec	herec	k1gMnSc1	herec
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Junák	junák	k1gMnSc1	junák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
První	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
fanklub	fanklub	k1gInSc1	fanklub
Horsta	Horst	k1gMnSc2	Horst
Fuchse	Fuchs	k1gMnSc2	Fuchs
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
stránka	stránka	k1gFnSc1	stránka
WS	WS	kA	WS
International	International	k1gFnPc1	International
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gFnPc2	jeho
reklam	reklama	k1gFnPc2	reklama
jako	jako	k9	jako
streamovaná	streamovaný	k2eAgNnPc4d1	streamované
videa	video	k1gNnPc4	video
</s>
</p>
