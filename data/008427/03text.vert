<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Tartu	Tarto	k1gNnSc6	Tarto
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Tartu	Tarta	k1gFnSc4	Tarta
Ülikool	Ülikoola	k1gFnPc2	Ülikoola
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gMnSc1	Universitas
Tartuensis	Tartuensis	k1gFnSc2	Tartuensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
estonskou	estonský	k2eAgFnSc4d1	Estonská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Academia	academia	k1gFnSc1	academia
Gustaviana	Gustaviana	k1gFnSc1	Gustaviana
roku	rok	k1gInSc2	rok
1632	[number]	k4	1632
švédským	švédský	k2eAgMnSc7d1	švédský
králem	král	k1gMnSc7	král
Gustavem	Gustav	k1gMnSc7	Gustav
Adolfem	Adolf	k1gMnSc7	Adolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
po	po	k7c6	po
Vilenské	Vilenský	k2eAgFnSc6d1	Vilenský
univerzitě	univerzita	k1gFnSc6	univerzita
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
původně	původně	k6eAd1	původně
švédsko-latinská	švédskoatinský	k2eAgFnSc1d1	švédsko-latinský
univerzita	univerzita	k1gFnSc1	univerzita
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
roku	rok	k1gInSc2	rok
1710	[number]	k4	1710
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Švédské	švédský	k2eAgNnSc1d1	švédské
Estonsko	Estonsko	k1gNnSc1	Estonsko
obsazeno	obsazen	k2eAgNnSc1d1	obsazeno
Ruským	ruský	k2eAgNnSc7d1	ruské
carstvím	carství	k1gNnSc7	carství
<g/>
.	.	kIx.	.
</s>
<s>
Baltští	baltský	k2eAgMnPc1d1	baltský
Němci	Němec	k1gMnPc1	Němec
využili	využít	k5eAaPmAgMnP	využít
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
nabídky	nabídka	k1gFnPc1	nabídka
carů	car	k1gMnPc2	car
Pavla	Pavel	k1gMnSc2	Pavel
I.	I.	kA	I.
i	i	k8xC	i
Alexandra	Alexandra	k1gFnSc1	Alexandra
I.	I.	kA	I.
a	a	k8xC	a
znovuobnovili	znovuobnovit	k5eAaImAgMnP	znovuobnovit
univerzitu	univerzita	k1gFnSc4	univerzita
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k6eAd1	už
jako	jako	k8xC	jako
čistě	čistě	k6eAd1	čistě
německojazyčný	německojazyčný	k2eAgInSc1d1	německojazyčný
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Imperátorská	imperátorský	k2eAgFnSc1d1	imperátorská
dorpatská	dorpatský	k2eAgFnSc1d1	dorpatský
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kaiserliche	Kaiserliche	k1gNnSc1	Kaiserliche
Universität	Universitäta	k1gFnPc2	Universitäta
zu	zu	k?	zu
Dorpat	Dorpat	k1gMnSc1	Dorpat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
zdejší	zdejší	k2eAgFnSc1d1	zdejší
výuka	výuka	k1gFnSc1	výuka
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
evropsky	evropsky	k6eAd1	evropsky
srovnatelné	srovnatelný	k2eAgFnSc2d1	srovnatelná
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
vlna	vlna	k1gFnSc1	vlna
rusifikace	rusifikace	k1gFnSc1	rusifikace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jak	jak	k6eAd1	jak
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
škola	škola	k1gFnSc1	škola
přejmenovány	přejmenován	k2eAgMnPc4d1	přejmenován
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	let	k1gInPc6	let
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
se	se	k3xPyFc4	se
škola	škola	k1gFnSc1	škola
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Imperátorská	imperátorský	k2eAgFnSc1d1	imperátorská
jurjevská	jurjevský	k2eAgFnSc1d1	jurjevský
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
Ю	Ю	k?	Ю
у	у	k?	у
<g/>
,	,	kIx,	,
Imperatorskij	Imperatorskij	k1gMnSc1	Imperatorskij
Jurjevskij	Jurjevskij	k1gMnSc1	Jurjevskij
universitět	universitět	k5eAaImF	universitět
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
vyučovací	vyučovací	k2eAgInSc1d1	vyučovací
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zavedla	zavést	k5eAaPmAgFnS	zavést
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
německých	německý	k2eAgMnPc2d1	německý
profesorů	profesor	k1gMnPc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Estonska	Estonsko	k1gNnSc2	Estonsko
byla	být	k5eAaImAgFnS	být
univerzita	univerzita	k1gFnSc1	univerzita
nacionalizována	nacionalizován	k2eAgFnSc1d1	nacionalizován
a	a	k8xC	a
obnovena	obnoven	k2eAgFnSc1d1	obnovena
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vymanění	vymanění	k1gNnSc6	vymanění
se	se	k3xPyFc4	se
Estonska	Estonsko	k1gNnSc2	Estonsko
zpod	zpod	k7c2	zpod
SSSR	SSSR	kA	SSSR
byla	být	k5eAaImAgFnS	být
univerzita	univerzita	k1gFnSc1	univerzita
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
opět	opět	k6eAd1	opět
ustavena	ustavit	k5eAaPmNgFnS	ustavit
jako	jako	k8xC	jako
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
instituce	instituce	k1gFnSc1	instituce
s	s	k7c7	s
akademickými	akademický	k2eAgFnPc7d1	akademická
svobodami	svoboda	k1gFnPc7	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
studia	studio	k1gNnSc2	studio
hledala	hledat	k5eAaImAgFnS	hledat
inspirace	inspirace	k1gFnSc1	inspirace
ve	v	k7c6	v
studijním	studijní	k2eAgInSc6d1	studijní
systému	systém	k1gInSc6	systém
USA	USA	kA	USA
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
však	však	k9	však
všechny	všechen	k3xTgFnPc1	všechen
estonské	estonský	k2eAgFnPc1d1	Estonská
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
zapojily	zapojit	k5eAaPmAgFnP	zapojit
do	do	k7c2	do
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Tartuská	tartuský	k2eAgFnSc1d1	Tartuská
univerzita	univerzita	k1gFnSc1	univerzita
je	být	k5eAaImIp3nS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
sdružení	sdružení	k1gNnSc2	sdružení
evropských	evropský	k2eAgFnPc2d1	Evropská
prestižních	prestižní	k2eAgFnPc2d1	prestižní
univerzit	univerzita	k1gFnPc2	univerzita
Coimbra	Coimbra	k1gMnSc1	Coimbra
Group	Group	k1gMnSc1	Group
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
školami	škola	k1gFnPc7	škola
i	i	k8xC	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Utrechtské	Utrechtský	k2eAgFnSc2d1	Utrechtská
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Krčálová	Krčálová	k1gFnSc1	Krčálová
<g/>
.	.	kIx.	.
</s>
<s>
Tartu	Tart	k1gMnSc3	Tart
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
univerzita	univerzita	k1gFnSc1	univerzita
//	//	k?	//
časopis	časopis	k1gInSc1	časopis
Navýchod	Navýchod	k1gInSc1	Navýchod
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tartuská	tartuský	k2eAgFnSc1d1	Tartuská
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
