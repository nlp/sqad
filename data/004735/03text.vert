<s>
Kuzmányho	Kuzmányze	k6eAd1	Kuzmányze
kruh	kruh	k1gInSc1	kruh
evanjelických	evanjelický	k2eAgFnPc2d1	evanjelická
akademikov	akademikov	k1gInSc4	akademikov
byl	být	k5eAaImAgInS	být
slovenský	slovenský	k2eAgInSc1d1	slovenský
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
profesora	profesor	k1gMnSc2	profesor
Samuele	Samuela	k1gFnSc6	Samuela
Štefana	Štefan	k1gMnSc4	Štefan
Osuského	Osuský	k1gMnSc4	Osuský
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Branislav	Branislav	k1gMnSc1	Branislav
Varsik	Varsik	k1gMnSc1	Varsik
<g/>
.	.	kIx.	.
</s>
<s>
Kruh	kruh	k1gInSc1	kruh
obhajoval	obhajovat	k5eAaImAgInS	obhajovat
zájmy	zájem	k1gInPc4	zájem
evangelických	evangelický	k2eAgMnPc2d1	evangelický
univerzitních	univerzitní	k2eAgMnPc2d1	univerzitní
studentů	student	k1gMnPc2	student
a	a	k8xC	a
přispíval	přispívat	k5eAaImAgInS	přispívat
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
mravně-náboženskému	mravněáboženský	k2eAgInSc3d1	mravně-náboženský
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
názvu	název	k1gInSc6	název
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
ke	k	k7c3	k
slovenskému	slovenský	k2eAgMnSc3d1	slovenský
spisovateli	spisovatel	k1gMnSc3	spisovatel
a	a	k8xC	a
teologovi	teolog	k1gMnSc3	teolog
Karolu	Karola	k1gFnSc4	Karola
Kuzmánymu	Kuzmánym	k1gInSc2	Kuzmánym
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
byl	být	k5eAaImAgInS	být
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
