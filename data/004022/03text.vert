<s>
Ethen	Ethen	k1gInSc1	Ethen
(	(	kIx(	(
<g/>
starší	starý	k2eAgInPc1d2	starší
názvy	název	k1gInPc1	název
<g/>
:	:	kIx,	:
ethylen	ethylen	k1gInSc1	ethylen
<g/>
,	,	kIx,	,
etylén	etylén	k1gInSc1	etylén
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjednodušším	jednoduchý	k2eAgMnSc7d3	nejjednodušší
zástupcem	zástupce	k1gMnSc7	zástupce
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
alkenů	alken	k1gInPc2	alken
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
plyn	plyn	k1gInSc1	plyn
nasládlé	nasládlý	k2eAgFnSc2d1	nasládlá
vůně	vůně	k1gFnSc2	vůně
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
tání	tání	k1gNnSc1	tání
-169,1	-169,1	k4	-169,1
°	°	k?	°
<g/>
C.	C.	kA	C.
Se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
tvoří	tvořit	k5eAaImIp3nP	tvořit
výbušnou	výbušný	k2eAgFnSc4d1	výbušná
směs	směs	k1gFnSc4	směs
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
zemním	zemní	k2eAgInSc6d1	zemní
a	a	k8xC	a
koksárenském	koksárenský	k2eAgInSc6d1	koksárenský
plynu	plyn	k1gInSc6	plyn
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
krakováním	krakování	k1gNnSc7	krakování
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
suroviny	surovina	k1gFnPc4	surovina
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
ethylenoxidu	ethylenoxid	k1gInSc2	ethylenoxid
<g/>
,	,	kIx,	,
polyethylenu	polyethylen	k1gInSc2	polyethylen
<g/>
,	,	kIx,	,
styrenu	styren	k1gInSc2	styren
aj.	aj.	kA	aj.
Odštěpením	odštěpení	k1gNnSc7	odštěpení
jednoho	jeden	k4xCgInSc2	jeden
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
vzniká	vznikat	k5eAaImIp3nS	vznikat
funkční	funkční	k2eAgFnSc1d1	funkční
skupina	skupina	k1gFnSc1	skupina
ethenyl	ethenyl	k1gInSc1	ethenyl
(	(	kIx(	(
<g/>
triviálním	triviální	k2eAgInSc7d1	triviální
názvem	název	k1gInSc7	název
vinyl	vinyl	k1gInSc1	vinyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dále	daleko	k6eAd2	daleko
vázat	vázat	k5eAaImF	vázat
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
kyslíku	kyslík	k1gInSc2	kyslík
hoří	hořet	k5eAaImIp3nS	hořet
ethen	ethit	k5eAaBmNgInS	ethit
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
+	+	kIx~	+
3	[number]	k4	3
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
CO2	CO2	k1gFnPc2	CO2
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
V	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
stříbra	stříbro	k1gNnSc2	stříbro
jako	jako	k8xS	jako
katalyzátoru	katalyzátor	k1gInSc2	katalyzátor
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
za	za	k7c2	za
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
220-280	[number]	k4	220-280
°	°	k?	°
<g/>
C	C	kA	C
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
ethylenoxidu	ethylenoxid	k1gInSc2	ethylenoxid
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
reaguje	reagovat	k5eAaBmIp3nS	reagovat
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ethylenglykolu	ethylenglykol	k1gInSc2	ethylenglykol
<g/>
.	.	kIx.	.
2	[number]	k4	2
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2	[number]	k4	2
C2H4O	C2H4O	k1gFnSc1	C2H4O
C2H4O	C2H4O	k1gFnSc1	C2H4O
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
HOCH2CH2OH	HOCH2CH2OH	k1gFnSc2	HOCH2CH2OH
Ethen	Ethna	k1gFnPc2	Ethna
polymeruje	polymerovat	k5eAaBmIp3nS	polymerovat
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
nebo	nebo	k8xC	nebo
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
Ziegler-Nattova	Ziegler-Nattův	k2eAgInSc2d1	Ziegler-Nattův
katalyzátoru	katalyzátor	k1gInSc2	katalyzátor
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
polyethylenu	polyethylen	k1gInSc2	polyethylen
<g/>
.	.	kIx.	.
n	n	k0	n
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
→	→	k?	→
[	[	kIx(	[
<g/>
-CH	-CH	k?	-CH
<g/>
2	[number]	k4	2
<g/>
-CH	-CH	k?	-CH
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
]	]	kIx)	]
<g/>
n	n	k0	n
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
metabolit	metabolit	k1gInSc1	metabolit
rostlin	rostlina	k1gFnPc2	rostlina
mající	mající	k2eAgInSc4d1	mající
charakter	charakter	k1gInSc4	charakter
fytohormonu	fytohormon	k1gInSc2	fytohormon
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňován	k2eAgFnSc1d1	ovlivňována
auxiny	auxina	k1gFnPc1	auxina
<g/>
..	..	k?	..
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
lokální	lokální	k2eAgFnSc1d1	lokální
hladina	hladina	k1gFnSc1	hladina
auxinu	auxin	k1gInSc2	auxin
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
lokální	lokální	k2eAgFnSc3d1	lokální
biosyntéze	biosyntéza	k1gFnSc3	biosyntéza
etylenu	etylen	k1gInSc2	etylen
<g/>
.	.	kIx.	.
</s>
<s>
Etylen	etylen	k1gInSc1	etylen
pak	pak	k6eAd1	pak
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
transport	transport	k1gInSc4	transport
auxinu	auxin	k1gInSc2	auxin
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
biosyntéza	biosyntéza	k1gFnSc1	biosyntéza
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
ATP	atp	kA	atp
a	a	k8xC	a
methioninu	methionin	k1gInSc2	methionin
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgInPc2	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
činností	činnost	k1gFnPc2	činnost
ACC	ACC	kA	ACC
syntázy	syntáza	k1gFnSc2	syntáza
ACC	ACC	kA	ACC
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-aminocyklopropan-	minocyklopropan-	k?	-aminocyklopropan-
<g/>
1	[number]	k4	1
<g/>
-karboxylová	arboxylový	k2eAgFnSc1d1	-karboxylový
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
ACC	ACC	kA	ACC
oxidázou	oxidáza	k1gFnSc7	oxidáza
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
oxidována	oxidován	k2eAgFnSc1d1	oxidována
na	na	k7c4	na
kyanid	kyanid	k1gInSc4	kyanid
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
ethen	ethen	k2eAgInSc4d1	ethen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klíčních	klíční	k2eAgFnPc6d1	klíční
rostlinách	rostlina	k1gFnPc6	rostlina
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
tzv.	tzv.	kA	tzv.
trojí	trojit	k5eAaImIp3nS	trojit
efekt	efekt	k1gInSc4	efekt
–	–	k?	–
etiolizované	etiolizovaný	k2eAgFnSc2d1	etiolizovaný
klíční	klíční	k2eAgFnSc2d1	klíční
rostliny	rostlina	k1gFnSc2	rostlina
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgFnPc1d2	kratší
<g/>
,	,	kIx,	,
silnější	silný	k2eAgFnPc1d2	silnější
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
porušenou	porušený	k2eAgFnSc4d1	porušená
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Urychluje	urychlovat	k5eAaImIp3nS	urychlovat
senescenci	senescence	k1gFnSc4	senescence
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
zrání	zrání	k1gNnSc4	zrání
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
opadávání	opadávání	k1gNnSc2	opadávání
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
tvorbu	tvorba	k1gFnSc4	tvorba
kořenových	kořenový	k2eAgInPc2d1	kořenový
vlásků	vlásek	k1gInPc2	vlásek
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
epinastii	epinastie	k1gFnSc4	epinastie
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Receptor	receptor	k1gInSc1	receptor
pro	pro	k7c4	pro
ethen	ethen	k1gInSc4	ethen
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vazbu	vazba	k1gFnSc4	vazba
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
měď	měď	k1gFnSc1	měď
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
ethen	ethen	k1gInSc1	ethen
přítomen	přítomen	k2eAgInSc1d1	přítomen
<g/>
,	,	kIx,	,
receptory	receptor	k1gInPc4	receptor
tvoří	tvořit	k5eAaImIp3nP	tvořit
dimery	dimera	k1gFnPc1	dimera
<g/>
,	,	kIx,	,
trans-autofosforylují	transutofosforylovat	k5eAaBmIp3nP	trans-autofosforylovat
se	se	k3xPyFc4	se
na	na	k7c6	na
histidinu	histidin	k1gInSc6	histidin
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
fosfát	fosfát	k1gInSc1	fosfát
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
aspartát	aspartát	k1gInSc4	aspartát
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
kinázu	kináza	k1gFnSc4	kináza
CTR	CTR	kA	CTR
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgInSc7	ten
aktivována	aktivovat	k5eAaBmNgFnS	aktivovat
a	a	k8xC	a
blokuje	blokovat	k5eAaImIp3nS	blokovat
regulační	regulační	k2eAgInSc1d1	regulační
protein	protein	k1gInSc1	protein
EIN2	EIN2	k1gFnSc2	EIN2
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
s	s	k7c7	s
cytokininem	cytokinin	k1gInSc7	cytokinin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ethen	ethen	k1gInSc1	ethen
přítomen	přítomen	k2eAgInSc1d1	přítomen
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
receptorový	receptorový	k2eAgInSc1d1	receptorový
dimer	dimer	k1gInSc1	dimer
kinázovou	kinázový	k2eAgFnSc4d1	kinázový
aktivitu	aktivita	k1gFnSc4	aktivita
a	a	k8xC	a
neaktivuje	aktivovat	k5eNaBmIp3nS	aktivovat
CTR	CTR	kA	CTR
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tedy	tedy	k9	tedy
neblokuje	blokovat	k5eNaImIp3nS	blokovat
EIN	EIN	kA	EIN
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1	funkční
EIN2	EIN2	k1gFnSc1	EIN2
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
transkripční	transkripční	k2eAgInSc4d1	transkripční
faktor	faktor	k1gInSc4	faktor
EIN	EIN	kA	EIN
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dimerizuje	dimerizovat	k5eAaImIp3nS	dimerizovat
s	s	k7c7	s
transkripčním	transkripční	k2eAgInSc7d1	transkripční
faktorem	faktor	k1gInSc7	faktor
ERF1	ERF1	k1gFnSc2	ERF1
a	a	k8xC	a
společně	společně	k6eAd1	společně
regulují	regulovat	k5eAaImIp3nP	regulovat
geny	gen	k1gInPc1	gen
řízené	řízený	k2eAgInPc1d1	řízený
ethenem	ethen	k1gInSc7	ethen
<g/>
.	.	kIx.	.
</s>
