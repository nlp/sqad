<s>
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
</s>
<s>
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
Účel	účel	k1gInSc1
stavby	stavba	k1gFnSc2
</s>
<s>
kanceláře	kancelář	k1gFnPc1
<g/>
,	,	kIx,
hotel	hotel	k1gInSc1
<g/>
,	,	kIx,
observatoř	observatoř	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
P	P	kA
<g/>
&	&	k?
<g/>
T	T	kA
Group	Group	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Materiály	materiál	k1gInPc4
</s>
<s>
ocel	ocel	k1gFnSc4
<g/>
,	,	kIx,
beton	beton	k1gInSc4
<g/>
,	,	kIx,
sklo	sklo	k1gNnSc4
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
Goldin	Goldin	k2eAgInSc1d1
Properties	Properties	k1gInSc1
Holding	holding	k1gInSc1
Technické	technický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
Výška	výška	k1gFnSc1
střechy	střecha	k1gFnPc4
</s>
<s>
596,6	596,6	k4
m	m	kA
Architektonická	architektonický	k2eAgFnSc1d1
</s>
<s>
596,6	596,6	k4
m	m	kA
Počet	počet	k1gInSc1
podlaží	podlaží	k1gNnSc2
</s>
<s>
128	#num#	k4
+	+	kIx~
4	#num#	k4
podzemní	podzemní	k2eAgFnSc1d1
Podlahová	podlahový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
847	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
(	(	kIx(
<g/>
celkem	celek	k1gInSc7
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
výtahů	výtah	k1gInPc2
</s>
<s>
89	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Tchien-ťin	Tchien-ťin	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
Ulice	ulice	k1gFnSc1
</s>
<s>
čtvrť	čtvrť	k1gFnSc1
Xiqing	Xiqing	k1gInSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
21,19	21,19	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
117	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
50,99	50,99	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
<g/>
:	:	kIx,
中	中	k?
<g/>
117	#num#	k4
<g/>
大	大	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rozestavěný	rozestavěný	k2eAgInSc1d1
mrakodrap	mrakodrap	k1gInSc1
v	v	k7c6
čínském	čínský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Tchien-ťin	Tchien-ťina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nové	nový	k2eAgFnSc6d1
centrální	centrální	k2eAgFnSc6d1
obchodní	obchodní	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Xiqing	Xiqing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
vzdálený	vzdálený	k2eAgInSc1d1
asi	asi	k9
8	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mrakodrap	mrakodrap	k1gInSc1
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
a	a	k8xC
extrémně	extrémně	k6eAd1
úzký	úzký	k2eAgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
poměrem	poměr	k1gInSc7
šířky	šířka	k1gFnSc2
k	k	k7c3
výšce	výška	k1gFnSc3
1	#num#	k4
<g/>
:	:	kIx,
<g/>
9,5	9,5	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Směrem	směr	k1gInSc7
k	k	k7c3
vrcholu	vrchol	k1gInSc3
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zužuje	zužovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přízemí	přízemí	k1gNnSc6
má	mít	k5eAaImIp3nS
šířku	šířka	k1gFnSc4
65	#num#	k4
x	x	k?
65	#num#	k4
m	m	kA
(	(	kIx(
<g/>
4	#num#	k4
200	#num#	k4
m	m	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
nejvyšším	vysoký	k2eAgNnSc6d3
patře	patro	k1gNnSc6
46	#num#	k4
x	x	k?
46	#num#	k4
m	m	kA
(	(	kIx(
<g/>
2	#num#	k4
100	#num#	k4
m	m	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
pří	přít	k5eAaImIp3nS
výšce	výška	k1gFnSc6
596,6	596,6	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vizuálně	vizuálně	k6eAd1
nejnápadnějším	nápadní	k2eAgInSc7d3
konstrukčním	konstrukční	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
jsou	být	k5eAaImIp3nP
masivní	masivní	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
stoupající	stoupající	k2eAgInPc1d1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
budovy	budova	k1gFnSc2
na	na	k7c6
každém	každý	k3xTgInSc6
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
rohů	roh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
sloupy	sloup	k1gInPc1
spojují	spojovat	k5eAaImIp3nP
nosníky	nosník	k1gInPc1
<g/>
,	,	kIx,
přenosové	přenosový	k2eAgInPc1d1
vazníky	vazník	k1gInPc1
a	a	k8xC
vzpěry	vzpěra	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
elegantně	elegantně	k6eAd1
skryty	skryt	k1gInPc1
za	za	k7c7
hliníkovou	hliníkový	k2eAgFnSc7d1
a	a	k8xC
stříbrnou	stříbrný	k2eAgFnSc7d1
reflexní	reflexní	k2eAgFnSc7d1
obvodovou	obvodový	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Design	design	k1gInSc1
budovy	budova	k1gFnSc2
připomíná	připomínat	k5eAaImIp3nS
vycházkovou	vycházkový	k2eAgFnSc4d1
hůl	hůl	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dlouhou	dlouhý	k2eAgFnSc4d1
štíhlou	štíhlý	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
bude	být	k5eAaImBp3nS
<g/>
)	)	kIx)
na	na	k7c6
vrcholu	vrchol	k1gInSc6
zakončená	zakončený	k2eAgFnSc1d1
atriem	atrium	k1gNnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nepřehlédnutelnou	přehlédnutelný	k2eNgFnSc7d1
dominantou	dominanta	k1gFnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Po	po	k7c4
střechu	střecha	k1gFnSc4
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
vysoká	vysoká	k1gFnSc1
596,6	596,6	k4
m	m	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgNnP
být	být	k5eAaImF
zároveň	zároveň	k6eAd1
architektonická	architektonický	k2eAgFnSc1d1
výška	výška	k1gFnSc1
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
únoru	únor	k1gInSc3
2021	#num#	k4
je	být	k5eAaImIp3nS
5	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc7d3
budovou	budova	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc7d3
budovou	budova	k1gFnSc7
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
by	by	kYmCp3nS
mrakodrap	mrakodrap	k1gInSc1
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
128	#num#	k4
nadzemních	nadzemní	k2eAgNnPc2d1
podlaží	podlaží	k1gNnPc2
(	(	kIx(
<g/>
11	#num#	k4
technických	technický	k2eAgMnPc2d1
<g/>
,	,	kIx,
117	#num#	k4
obyvatelných	obyvatelný	k2eAgFnPc2d1
<g/>
)	)	kIx)
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
4	#num#	k4
podzemní	podzemní	k2eAgFnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
nabídne	nabídnout	k5eAaPmIp3nS
prostor	prostor	k1gInSc4
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
výměře	výměra	k1gFnSc6
847	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
(	(	kIx(
<g/>
342	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
podlahové	podlahový	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
v	v	k7c6
suterénu	suterén	k1gInSc6
<g/>
,	,	kIx,
497	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
podlahové	podlahový	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
nad	nad	k7c7
zemí	zem	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
samém	samý	k3xTgInSc6
vrcholu	vrchol	k1gInSc6
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
zakončený	zakončený	k2eAgInSc1d1
prosklenou	prosklený	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
diamantu	diamant	k1gInSc2
či	či	k8xC
křišťálu	křišťál	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
nacházet	nacházet	k5eAaImF
veřejnosti	veřejnost	k1gFnSc3
přístupná	přístupný	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
úrovni	úroveň	k1gFnSc6
116	#num#	k4
<g/>
.	.	kIx.
podlaží	podlaží	k1gNnSc6
ve	v	k7c6
výšce	výška	k1gFnSc6
asi	asi	k9
579	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
zemí	zem	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
bude	být	k5eAaImBp3nS
otevřena	otevřít	k5eAaPmNgFnS
<g/>
,	,	kIx,
stane	stanout	k5eAaPmIp3nS
se	se	k3xPyFc4
nejvýše	vysoce	k6eAd3,k6eAd1
položenou	položený	k2eAgFnSc7d1
observatoří	observatoř	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doposud	doposud	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
tohoto	tento	k3xDgNnSc2
prvenství	prvenství	k1gNnSc2
mrakodrap	mrakodrap	k1gInSc1
Ping	pingo	k1gNnPc2
An	An	k1gFnSc2
International	International	k1gFnSc1
Finance	finance	k1gFnSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
disponuje	disponovat	k5eAaBmIp3nS
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1
terasu	terasa	k1gFnSc4
ve	v	k7c6
výšce	výška	k1gFnSc6
562,2	562,2	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
115	#num#	k4
<g/>
.	.	kIx.
podlaží	podlaží	k1gNnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
vyskytovat	vyskytovat	k5eAaImF
krytý	krytý	k2eAgInSc1d1
bazén	bazén	k1gInSc1
ve	v	k7c6
výšce	výška	k1gFnSc6
564	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
nejvýše	nejvýše	k6eAd1,k6eAd3
umístěným	umístěný	k2eAgInSc7d1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc4
navrhla	navrhnout	k5eAaPmAgFnS
hongkongská	hongkongský	k2eAgFnSc1d1
architektonická	architektonický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
P	P	kA
<g/>
&	&	k?
<g/>
T	T	kA
Group	Group	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zhostila	zhostit	k5eAaPmAgFnS
několika	několik	k4yIc2
dalších	další	k2eAgFnPc2d1
známých	známý	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
<g/>
,	,	kIx,
Šanghaji	Šanghaj	k1gFnSc6
a	a	k8xC
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastníkem	vlastník	k1gMnSc7
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
developerská	developerský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Goldin	Goldina	k1gFnPc2
Properties	Properties	k1gInSc4
Holding	holding	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
názvu	název	k1gInSc2
mrakodrapu	mrakodrap	k1gInSc2
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
nabízet	nabízet	k5eAaImF
zejména	zejména	k9
kancelářské	kancelářský	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
(	(	kIx(
<g/>
ty	ten	k3xDgFnPc1
budou	být	k5eAaImBp3nP
zřejmě	zřejmě	k6eAd1
zabírat	zabírat	k5eAaImF
7	#num#	k4
<g/>
.	.	kIx.
až	až	k9
92	#num#	k4
<g/>
.	.	kIx.
podlaží	podlaží	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Sky	Sky	k1gFnSc1
lobby	lobby	k1gFnPc7
(	(	kIx(
<g/>
přechodové	přechodový	k2eAgNnSc1d1
mezipodlaží	mezipodlaží	k1gNnSc1
<g/>
,	,	kIx,
skrze	skrze	k?
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
lidé	člověk	k1gMnPc1
mohou	moct	k5eAaImIp3nP
přecházet	přecházet	k5eAaImF
z	z	k7c2
expresního	expresní	k2eAgInSc2d1
výtahu	výtah	k1gInSc2
na	na	k7c4
místní	místní	k2eAgInSc4d1
výtah	výtah	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zastavuje	zastavovat	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgNnSc6
patře	patro	k1gNnSc6
budovy	budova	k1gFnSc2
od	od	k7c2
tohoto	tento	k3xDgNnSc2
podlaží	podlaží	k1gNnSc2
výše	vysoce	k6eAd2
<g/>
)	)	kIx)
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
nacházet	nacházet	k5eAaImF
na	na	k7c6
úrovni	úroveň	k1gFnSc6
32	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
63	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
94	#num#	k4
podlaží	podlaží	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
kanceláří	kancelář	k1gFnPc2
a	a	k8xC
observatoře	observatoř	k1gFnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
v	v	k7c6
budově	budova	k1gFnSc6
měl	mít	k5eAaImAgInS
nacházet	nacházet	k5eAaImF
také	také	k9
hotel	hotel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
bude	být	k5eAaImBp3nS
pravděpodobně	pravděpodobně	k6eAd1
zabírat	zabírat	k5eAaImF
94	#num#	k4
<g/>
.	.	kIx.
až	až	k9
115	#num#	k4
<g/>
.	.	kIx.
podlaží	podlaží	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
a	a	k8xC
hotová	hotová	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
toho	ten	k3xDgInSc2
byla	být	k5eAaImAgFnS
dvakrát	dvakrát	k6eAd1
pozastavena	pozastavit	k5eAaPmNgFnS
a	a	k8xC
od	od	k7c2
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
2019	#num#	k4
stále	stále	k6eAd1
není	být	k5eNaImIp3nS
mrakodrap	mrakodrap	k1gInSc1
dokončený	dokončený	k2eAgInSc1d1
a	a	k8xC
ani	ani	k8xC
nikým	nikdo	k3yNnSc7
obsazený	obsazený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgMnSc1d1
dodavatel	dodavatel	k1gMnSc1
projektu	projekt	k1gInSc2
společnost	společnost	k1gFnSc1
China	China	k1gFnSc1
State	status	k1gInSc5
Construction	Construction	k1gInSc4
Engineering	Engineering	k1gInSc1
(	(	kIx(
<g/>
CSCEC	CSCEC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podílející	podílející	k2eAgFnSc2d1
se	se	k3xPyFc4
např.	např.	kA
na	na	k7c6
mrakodrapech	mrakodrap	k1gInPc6
Shanghai	Shanghai	k1gNnSc2
Tower	Towra	k1gFnPc2
<g/>
,	,	kIx,
CITIC	CITIC	kA
Tower	Tower	k1gMnSc1
nebo	nebo	k8xC
Ping	pingo	k1gNnPc2
An	An	k1gFnPc2
International	International	k1gFnSc1
Finance	finance	k1gFnSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
stavbu	stavba	k1gFnSc4
přerušila	přerušit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátky	počátek	k1gInPc1
a	a	k8xC
výstavba	výstavba	k1gFnSc1
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2007	#num#	k4
byl	být	k5eAaImAgInS
zakoupen	zakoupen	k2eAgInSc1d1
pozemek	pozemek	k1gInSc1
společností	společnost	k1gFnPc2
Goldin	Goldin	k2eAgMnSc1d1
Properties	Properties	k1gMnSc1
Holdings	Holdingsa	k1gFnPc2
Ltd	ltd	kA
(	(	kIx(
<g/>
údajně	údajně	k6eAd1
za	za	k7c4
velmi	velmi	k6eAd1
nízkou	nízký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
slavnostní	slavnostní	k2eAgInSc1d1
ceremoniál	ceremoniál	k1gInSc1
projektu	projekt	k1gInSc2
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
stavba	stavba	k1gFnSc1
mrakodrapu	mrakodrap	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
suterén	suterén	k1gInSc1
budovy	budova	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
říjnu	říjen	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
dodavatelem	dodavatel	k1gMnSc7
společnost	společnost	k1gFnSc1
China	China	k1gFnSc1
State	status	k1gInSc5
Construction	Construction	k1gInSc4
Engineering	Engineering	k1gInSc1
(	(	kIx(
<g/>
CSCEC	CSCEC	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
konstrukce	konstrukce	k1gFnSc1
mrakodrapu	mrakodrap	k1gInSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
výšky	výška	k1gFnSc2
100	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2013	#num#	k4
vyrostla	vyrůst	k5eAaPmAgFnS
o	o	k7c4
dalších	další	k2eAgInPc2d1
100	#num#	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
výšky	výška	k1gFnSc2
337	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
budova	budova	k1gFnSc1
překonala	překonat	k5eAaPmAgFnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nejvyšší	vysoký	k2eAgInSc4d3
mrakodrap	mrakodrap	k1gInSc4
v	v	k7c6
Tchien-ťinu	Tchien-ťin	k1gInSc6
Tianjin	Tianjin	k2eAgMnSc1d1
World	World	k1gMnSc1
Financial	Financial	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
září	září	k1gNnSc6
2014	#num#	k4
svou	svůj	k3xOyFgFnSc7
výškou	výška	k1gFnSc7
417	#num#	k4
metrů	metr	k1gInPc2
překonala	překonat	k5eAaPmAgFnS
Tiencinskou	Tiencinský	k2eAgFnSc4d1
televizní	televizní	k2eAgFnSc4d1
věž	věž	k1gFnSc4
(	(	kIx(
<g/>
415	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nejvyšší	vysoký	k2eAgFnSc1d3
stavba	stavba	k1gFnSc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
bylo	být	k5eAaImAgNnS
zhotoveno	zhotoven	k2eAgNnSc1d1
betonové	betonový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
budovy	budova	k1gFnSc2
do	do	k7c2
úrovně	úroveň	k1gFnSc2
101	#num#	k4
<g/>
.	.	kIx.
podlaží	podlaží	k1gNnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dosáhla	dosáhnout	k5eAaPmAgFnS
konstrukční	konstrukční	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
503	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
třetí	třetí	k4xOgFnSc7
budovou	budova	k1gFnSc7
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
tyčí	tyčit	k5eAaImIp3nS
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
přes	přes	k7c4
500	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
od	od	k7c2
září	září	k1gNnSc2
2015	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
3	#num#	k4
roky	rok	k1gInPc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
pozastavena	pozastaven	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
pravděpodobně	pravděpodobně	k6eAd1
konečné	konečný	k2eAgFnSc2d1
konstrukční	konstrukční	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
596,6	596,6	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
k	k	k7c3
únoru	únor	k1gInSc3
2021	#num#	k4
5	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
je	být	k5eAaImIp3nS
stavba	stavba	k1gFnSc1
opět	opět	k6eAd1
pozastavena	pozastaven	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
-	-	kIx~
The	The	k1gFnSc1
Skyscraper	Skyscraper	k1gInSc1
Center	centrum	k1gNnPc2
<g/>
.	.	kIx.
www.skyscrapercenter.com	www.skyscrapercenter.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc2
117	#num#	k4
facts	facts	k1gInSc1
and	and	k?
information	information	k1gInSc1
–	–	k?
The	The	k1gFnSc2
Tower	Tower	k1gMnSc1
Info	Info	k1gMnSc1
<g/>
.	.	kIx.
thetowerinfo	thetowerinfo	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Highest	Highest	k1gFnSc1
Observation	Observation	k1gInSc4
Decks	Decks	k1gInSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-10-28	2018-10-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
světa	svět	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc2
117	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pár	pár	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vylezl	vylézt	k5eAaPmAgInS
až	až	k6eAd1
na	na	k7c4
vrchol	vrchol	k1gInSc4
rozestavěného	rozestavěný	k2eAgInSc2d1
mrakodrapu	mrakodrap	k1gInSc2
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
(	(	kIx(
<g/>
YouTube	YouTub	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Čína	Čína	k1gFnSc1
</s>
