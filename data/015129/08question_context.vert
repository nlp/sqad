<s>
Vizuálně	vizuálně	k6eAd1
nejnápadnějším	nápadní	k2eAgInSc7d3
konstrukčním	konstrukční	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
jsou	být	k5eAaImIp3nP
masivní	masivní	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
stoupající	stoupající	k2eAgInPc1d1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
budovy	budova	k1gFnSc2
na	na	k7c6
každém	každý	k3xTgInSc6
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
rohů	roh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Goldin	Goldina	k1gFnPc2
Finance	finance	k1gFnSc1
117	#num#	k4
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
<g/>
:	:	kIx,
中	中	k?
<g/>
117	#num#	k4
<g/>
大	大	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rozestavěný	rozestavěný	k2eAgInSc1d1
mrakodrap	mrakodrap	k1gInSc1
v	v	k7c6
čínském	čínský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Tchien-ťin	Tchien-ťina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nové	nový	k2eAgFnSc6d1
centrální	centrální	k2eAgFnSc6d1
obchodní	obchodní	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Xiqing	Xiqing	k1gInSc4
<g/>
.	.	kIx.
</s>