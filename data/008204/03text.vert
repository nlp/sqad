<p>
<s>
Ašdod	Ašdod	k1gInSc1	Ašdod
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
א	א	k?	א
<g/>
,	,	kIx,	,
Ašdod	Ašdod	k1gInSc1	Ašdod
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
إ	إ	k?	إ
Isdúd	Isdúd	k1gInSc1	Isdúd
<g/>
,	,	kIx,	,
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
přepisu	přepis	k1gInSc6	přepis
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
Ashdod	Ashdod	k1gInSc1	Ashdod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
distriktu	distrikt	k1gInSc6	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
je	být	k5eAaImIp3nS	být
Jechi	Jechi	k1gNnSc1	Jechi
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
Lasri	Lasri	k1gNnPc2	Lasri
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
24	[number]	k4	24
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
izraelské	izraelský	k2eAgFnSc6d1	izraelská
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
planině	planina	k1gFnSc6	planina
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
32	[number]	k4	32
kilometrů	kilometr	k1gInPc2	kilometr
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
a	a	k8xC	a
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
Aškelonu	Aškelon	k1gInSc2	Aškelon
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
je	být	k5eAaImIp3nS	být
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využíváno	využívat	k5eAaImNgNnS	využívat
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
pás	pás	k1gInSc1	pás
lemují	lemovat	k5eAaImIp3nP	lemovat
písečné	písečný	k2eAgFnPc1d1	písečná
duny	duna	k1gFnPc1	duna
<g/>
,	,	kIx,	,
skrz	skrz	k6eAd1	skrz
které	který	k3yRgNnSc1	který
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
prochází	procházet	k5eAaImIp3nS	procházet
potok	potok	k1gInSc1	potok
Lachiš	Lachiš	k1gInSc1	Lachiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
osídlené	osídlený	k2eAgFnSc6d1	osídlená
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
etnicky	etnicky	k6eAd1	etnicky
zcela	zcela	k6eAd1	zcela
židovská	židovský	k2eAgFnSc1d1	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
napojeno	napojen	k2eAgNnSc1d1	napojeno
pomocí	pomocí	k7c2	pomocí
severojižní	severojižní	k2eAgFnSc2d1	severojižní
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
tu	tu	k6eAd1	tu
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnPc2	vnitrozemí
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
stojí	stát	k5eAaImIp3nS	stát
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Ašdod	Ašdoda	k1gFnPc2	Ašdoda
Ad	ad	k7c4	ad
Halom	Halom	k1gInSc4	Halom
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
tudy	tudy	k6eAd1	tudy
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Tel	tel	kA	tel
Aviv-Aškelon	Aviv-Aškelon	k1gInSc1	Aviv-Aškelon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Ašdod	Ašdod	k1gInSc1	Ašdod
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Sídelní	sídelní	k2eAgFnSc1d1	sídelní
tradice	tradice	k1gFnSc1	tradice
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
3	[number]	k4	3
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
před	před	k7c7	před
našim	náš	k3xOp1gFnPc3	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Biblické	biblický	k2eAgFnSc2d1	biblická
zmínky	zmínka	k1gFnSc2	zmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Ašdodu	Ašdod	k1gInSc2	Ašdod
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
biblické	biblický	k2eAgNnSc4d1	biblické
město	město	k1gNnSc4	město
Ašdód	Ašdóda	k1gFnPc2	Ašdóda
citované	citovaný	k2eAgFnSc2d1	citovaná
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
Jozue	Jozue	k1gMnSc1	Jozue
15,46	[number]	k4	15,46
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
je	být	k5eAaImIp3nS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
celkem	celkem	k6eAd1	celkem
třináctkrát	třináctkrát	k6eAd1	třináctkrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jozue	Jozue	k1gMnSc1	Jozue
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
proklamovaná	proklamovaný	k2eAgFnSc1d1	proklamovaná
součást	součást	k1gFnSc1	součást
Judského	judský	k2eAgNnSc2d1	Judské
království	království	k1gNnSc2	království
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Joz	Joza	k1gFnPc2	Joza
15,46	[number]	k4	15,46
<g/>
-	-	kIx~	-
<g/>
15,47	[number]	k4	15,47
<g/>
:	:	kIx,	:
od	od	k7c2	od
Ekrónu	Ekrón	k1gInSc2	Ekrón
až	až	k9	až
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
leží	ležet	k5eAaImIp3nS	ležet
stranou	strana	k1gFnSc7	strana
Ašdódu	Ašdód	k1gInSc2	Ašdód
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
dvorce	dvorec	k1gInPc4	dvorec
<g/>
,	,	kIx,	,
<g/>
Ašdód	Ašdód	k1gInSc4	Ašdód
s	s	k7c7	s
vesnicemi	vesnice	k1gFnPc7	vesnice
a	a	k8xC	a
dvorci	dvorec	k1gInPc7	dvorec
<g/>
,	,	kIx,	,
Gáza	gáza	k1gFnSc1	gáza
s	s	k7c7	s
vesnicemi	vesnice	k1gFnPc7	vesnice
a	a	k8xC	a
dvorci	dvorec	k1gInPc7	dvorec
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
Potoku	potok	k1gInSc3	potok
egyptskému	egyptský	k2eAgNnSc3d1	egyptské
a	a	k8xC	a
Velkému	velký	k2eAgNnSc3d1	velké
moři	moře	k1gNnSc3	moře
s	s	k7c7	s
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
knize	kniha	k1gFnSc6	kniha
Samuelově	Samuelův	k2eAgFnSc6d1	Samuelova
je	být	k5eAaImIp3nS	být
Ašdod	Ašdod	k1gInSc1	Ašdod
popisován	popisován	k2eAgInSc1d1	popisován
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
filištínských	filištínský	k2eAgNnPc2d1	filištínský
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1S	[number]	k4	1S
6,17	[number]	k4	6,17
<g/>
:	:	kIx,	:
Toto	tento	k3xDgNnSc1	tento
jsou	být	k5eAaImIp3nP	být
zlaté	zlatý	k2eAgFnPc1d1	zlatá
hlízy	hlíza	k1gFnPc1	hlíza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
odvedli	odvést	k5eAaPmAgMnP	odvést
Pelištejci	Pelištejec	k1gMnPc1	Pelištejec
Hospodinu	Hospodin	k1gMnSc3	Hospodin
v	v	k7c4	v
oběť	oběť	k1gFnSc4	oběť
za	za	k7c2	za
provinění	provinění	k1gNnSc2	provinění
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
za	za	k7c4	za
Ašdód	Ašdód	k1gInSc4	Ašdód
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
za	za	k7c2	za
Gázu	gáz	k1gInSc2	gáz
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
za	za	k7c4	za
Aškalón	Aškalón	k1gInSc4	Aškalón
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
za	za	k7c2	za
Gat	Gat	k1gFnPc2	Gat
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
za	za	k7c4	za
Ekrón	Ekrón	k1gInSc4	Ekrón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
je	být	k5eAaImIp3nS	být
též	též	k9	též
popisováno	popisován	k2eAgNnSc4d1	popisováno
dobytí	dobytí	k1gNnSc4	dobytí
Ašdodu	Ašdod	k1gInSc2	Ašdod
judským	judský	k2eAgMnSc7d1	judský
králem	král	k1gMnSc7	král
Uzijášem	Uzijáš	k1gMnSc7	Uzijáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
Pa	Pa	kA	Pa
26,6	[number]	k4	26,6
<g/>
:	:	kIx,	:
Vytáhl	vytáhnout	k5eAaPmAgInS	vytáhnout
a	a	k8xC	a
válčil	válčit	k5eAaImAgInS	válčit
s	s	k7c7	s
Pelištejci	Pelištejec	k1gMnPc7	Pelištejec
a	a	k8xC	a
strhl	strhnout	k5eAaPmAgMnS	strhnout
hradby	hradba	k1gFnPc4	hradba
města	město	k1gNnSc2	město
Gatu	Gatus	k1gInSc2	Gatus
<g/>
,	,	kIx,	,
hradby	hradba	k1gFnPc1	hradba
Jabne	Jabn	k1gInSc5	Jabn
a	a	k8xC	a
hradby	hradba	k1gFnPc1	hradba
Ašdódu	Ašdód	k1gInSc2	Ašdód
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěl	vystavět	k5eAaPmAgMnS	vystavět
města	město	k1gNnPc4	město
kolem	kolo	k1gNnSc7	kolo
Ašdódu	Ašdóda	k1gFnSc4	Ašdóda
na	na	k7c6	na
území	území	k1gNnSc6	území
Pelištejců	Pelištejec	k1gMnPc2	Pelištejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starověké	starověký	k2eAgNnSc4d1	starověké
a	a	k8xC	a
středověké	středověký	k2eAgNnSc4d1	středověké
osídlení	osídlení	k1gNnSc4	osídlení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
budoucího	budoucí	k2eAgInSc2d1	budoucí
Ašdodu	Ašdod	k1gInSc2	Ašdod
žili	žít	k5eAaImAgMnP	žít
lidé	člověk	k1gMnPc1	člověk
již	již	k6eAd1	již
v	v	k7c6	v
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
;	;	kIx,	;
historie	historie	k1gFnSc2	historie
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
zmínka	zmínka	k1gFnSc1	zmínka
dokládá	dokládat	k5eAaImIp3nS	dokládat
osídlení	osídlení	k1gNnSc4	osídlení
Ašdodu	Ašdod	k1gInSc2	Ašdod
v	v	k7c6	v
období	období	k1gNnSc6	období
kanaánské	kanaánský	k2eAgFnSc2d1	kanaánská
kultury	kultura	k1gFnSc2	kultura
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,,	,,	k?	,,
<g/>
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnSc4d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
mezníkem	mezník	k1gInSc7	mezník
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dobytí	dobytí	k1gNnSc1	dobytí
Filištíny	Filištín	k1gMnPc7	Filištín
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
z	z	k7c2	z
Ašdodu	Ašdod	k1gInSc2	Ašdod
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
stalo	stát	k5eAaPmAgNnS	stát
prosperující	prosperující	k2eAgNnSc1d1	prosperující
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
několikrát	několikrát	k6eAd1	několikrát
dobyt	dobýt	k5eAaPmNgInS	dobýt
a	a	k8xC	a
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
950	[number]	k4	950
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
egyptským	egyptský	k2eAgMnSc7d1	egyptský
faraonem	faraon	k1gMnSc7	faraon
Siamunem	Siamun	k1gMnSc7	Siamun
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
kompletně	kompletně	k6eAd1	kompletně
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
815	[number]	k4	815
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
dobyto	dobýt	k5eAaPmNgNnS	dobýt
a	a	k8xC	a
zničeno	zničit	k5eAaPmNgNnS	zničit
asyrským	asyrský	k2eAgMnSc7d1	asyrský
králem	král	k1gMnSc7	král
Sargonem	Sargon	k1gMnSc7	Sargon
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
605	[number]	k4	605
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
město	město	k1gNnSc1	město
dobyl	dobýt	k5eAaPmAgMnS	dobýt
babylónský	babylónský	k2eAgMnSc1d1	babylónský
král	král	k1gMnSc1	král
Nabukadnezar	Nabukadnezar	k1gMnSc1	Nabukadnezar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
perskou	perský	k2eAgFnSc4d1	perská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ukončil	ukončit	k5eAaPmAgMnS	ukončit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
–	–	k?	–
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
ho	on	k3xPp3gNnSc2	on
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Izotus	Izotus	k1gInSc4	Izotus
<g/>
.	.	kIx.	.
</s>
<s>
Izotus	Izotus	k1gInSc1	Izotus
pod	pod	k7c7	pod
helénskou	helénský	k2eAgFnSc7d1	helénská
správou	správa	k1gFnSc7	správa
prosperoval	prosperovat	k5eAaImAgInS	prosperovat
až	až	k9	až
do	do	k7c2	do
Makabejského	makabejský	k2eAgNnSc2d1	Makabejské
povstání	povstání	k1gNnSc2	povstání
<g/>
;	;	kIx,	;
Judovi	Juda	k1gMnSc3	Juda
Makabejskému	makabejský	k2eAgMnSc3d1	makabejský
se	se	k3xPyFc4	se
město	město	k1gNnSc4	město
dobýt	dobýt	k5eAaPmF	dobýt
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgMnS	obsadit
ho	on	k3xPp3gInSc4	on
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
147	[number]	k4	147
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jeho	jeho	k3xOp3gFnSc7	jeho
bratr	bratr	k1gMnSc1	bratr
Jonatán	Jonatán	k1gMnSc1	Jonatán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
osídleno	osídlen	k2eAgNnSc1d1	osídleno
Pelištejci	Pelištejec	k1gMnPc7	Pelištejec
<g/>
,	,	kIx,	,
Izraelity	izraelita	k1gMnPc7	izraelita
<g/>
,	,	kIx,	,
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
<g/>
,	,	kIx,	,
křižáky	křižák	k1gMnPc7	křižák
a	a	k8xC	a
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Byzantincích	Byzantinec	k1gMnPc6	Byzantinec
městu	město	k1gNnSc3	město
vládli	vládnout	k5eAaImAgMnP	vládnout
Fátimovci	Fátimovec	k1gMnPc1	Fátimovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
pobřežní	pobřežní	k2eAgFnSc4d1	pobřežní
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
strategickou	strategický	k2eAgFnSc4d1	strategická
polohu	poloha	k1gFnSc4	poloha
význam	význam	k1gInSc4	význam
Ašdodu	Ašdod	k1gInSc2	Ašdod
postupně	postupně	k6eAd1	postupně
upadal	upadat	k5eAaPmAgInS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
za	za	k7c2	za
nadvlády	nadvláda	k1gFnSc2	nadvláda
Osmanů	Osman	k1gMnPc2	Osman
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc4	město
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
Isdud	Isdud	k1gInSc4	Isdud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgInSc1d1	moderní
Ašdod	Ašdod	k1gInSc1	Ašdod
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
izraelské	izraelský	k2eAgFnSc2d1	izraelská
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Isdud	Isduda	k1gFnPc2	Isduda
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
jejich	jejich	k3xOp3gInSc1	jejich
postup	postup	k1gInSc1	postup
na	na	k7c4	na
sever	sever	k1gInSc4	sever
zastavil	zastavit	k5eAaPmAgMnS	zastavit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc2	operace
Plešet	Plešet	k1gFnSc2	Plešet
na	na	k7c4	na
Egypťany	Egypťan	k1gMnPc4	Egypťan
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
Izraelci	Izraelec	k1gMnPc1	Izraelec
včetně	včetně	k7c2	včetně
nově	nově	k6eAd1	nově
nasazeného	nasazený	k2eAgNnSc2d1	nasazené
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
u	u	k7c2	u
mostu	most	k1gInSc2	most
Gešer	Gešra	k1gFnPc2	Gešra
ad	ad	k7c4	ad
Halom	Halom	k1gInSc4	Halom
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1948	[number]	k4	1948
se	se	k3xPyFc4	se
egyptská	egyptský	k2eAgNnPc1d1	egyptské
vojska	vojsko	k1gNnPc1	vojsko
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
obklíčení	obklíčení	k1gNnSc2	obklíčení
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
většina	většina	k1gFnSc1	většina
arabských	arabský	k2eAgMnPc2d1	arabský
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
Ašdod	Ašdod	k1gInSc1	Ašdod
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
na	na	k7c6	na
písečných	písečný	k2eAgFnPc6d1	písečná
dunách	duna	k1gFnPc6	duna
poblíž	poblíž	k7c2	poblíž
místa	místo	k1gNnSc2	místo
starověkého	starověký	k2eAgNnSc2d1	starověké
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgMnS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
plánovitě	plánovitě	k6eAd1	plánovitě
postavené	postavený	k2eAgNnSc4d1	postavené
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rozvoj	rozvoj	k1gInSc1	rozvoj
sledoval	sledovat	k5eAaImAgInS	sledovat
hlavní	hlavní	k2eAgInSc4d1	hlavní
rozvojový	rozvojový	k2eAgInSc4d1	rozvojový
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
dopravním	dopravní	k2eAgInPc3d1	dopravní
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
znečištění	znečištění	k1gNnSc3	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
v	v	k7c6	v
rezidenčních	rezidenční	k2eAgFnPc6d1	rezidenční
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
nový	nový	k2eAgInSc4d1	nový
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Ašdod	Ašdod	k1gInSc1	Ašdod
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
regionálním	regionální	k2eAgNnSc7d1	regionální
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
Ašdodský	Ašdodský	k2eAgInSc1d1	Ašdodský
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
izraelským	izraelský	k2eAgInSc7d1	izraelský
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
prochází	procházet	k5eAaImIp3nS	procházet
60	[number]	k4	60
%	%	kIx~	%
veškerého	veškerý	k3xTgNnSc2	veškerý
importovaného	importovaný	k2eAgNnSc2d1	importované
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
důležitá	důležitý	k2eAgFnSc1d1	důležitá
elektrárna	elektrárna	k1gFnSc1	elektrárna
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
izraelských	izraelský	k2eAgFnPc2d1	izraelská
ropných	ropný	k2eAgFnPc2d1	ropná
rafinerií	rafinerie	k1gFnPc2	rafinerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
tvořili	tvořit	k5eAaImAgMnP	tvořit
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Židé	Žid	k1gMnPc1	Žid
-	-	kIx~	-
cca	cca	kA	cca
188	[number]	k4	188
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
statistické	statistický	k2eAgFnSc2d1	statistická
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nearabské	arabský	k2eNgMnPc4d1	nearabský
obyvatele	obyvatel	k1gMnPc4	obyvatel
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
ale	ale	k8xC	ale
bez	bez	k7c2	bez
formální	formální	k2eAgFnSc2d1	formální
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
židovskému	židovský	k2eAgNnSc3d1	Židovské
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
cca	cca	kA	cca
206	[number]	k4	206
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
obec	obec	k1gFnSc4	obec
velkoměstského	velkoměstský	k2eAgInSc2d1	velkoměstský
typu	typ	k1gInSc2	typ
s	s	k7c7	s
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
222	[number]	k4	222
900	[number]	k4	900
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
město	město	k1gNnSc1	město
zažívá	zažívat	k5eAaImIp3nS	zažívat
nebývalý	nebývalý	k2eAgInSc4d1	nebývalý
příliv	příliv	k1gInSc4	příliv
obyvatel	obyvatel	k1gMnPc2	obyvatel
díky	díky	k7c3	díky
židovským	židovský	k2eAgMnPc3d1	židovský
imigrantům	imigrant	k1gMnPc3	imigrant
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Centrálního	centrální	k2eAgInSc2d1	centrální
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
žilo	žít	k5eAaImAgNnS	žít
209	[number]	k4	209
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
šesté	šestý	k4xOgNnSc4	šestý
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
izraelské	izraelský	k2eAgNnSc4d1	izraelské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
*	*	kIx~	*
údaje	údaj	k1gInPc1	údaj
za	za	k7c4	za
roky	rok	k1gInPc4	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
zaokrouhleny	zaokrouhlen	k2eAgInPc4d1	zaokrouhlen
na	na	k7c4	na
stovky	stovka	k1gFnPc4	stovka
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Bahía	Bahíus	k1gMnSc4	Bahíus
Blanca	Blanc	k1gMnSc4	Blanc
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
</s>
</p>
<p>
<s>
Spandau	Spandau	k6eAd1	Spandau
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Tampa	Tampa	k1gFnSc1	Tampa
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Záporoží	Záporoží	k1gNnSc1	Záporoží
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ashdod	Ashdoda	k1gFnPc2	Ashdoda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ašdod	Ašdoda	k1gFnPc2	Ašdoda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
