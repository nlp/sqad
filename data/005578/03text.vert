<s>
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolbna	k1gFnPc2	Kolbna
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
Strančice	Strančice	k1gFnSc1	Strančice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Terezín	Terezín	k1gInSc1	Terezín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
českých	český	k2eAgMnPc2d1	český
elektrotechniků	elektrotechnik	k1gMnPc2	elektrotechnik
a	a	k8xC	a
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
továrny	továrna	k1gFnSc2	továrna
Kolben	Kolben	k2eAgMnSc1d1	Kolben
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
akcionář	akcionář	k1gMnSc1	akcionář
Českomoravské-Kolben-Daněk	Českomoravské-Kolben-Daňka	k1gFnPc2	Českomoravské-Kolben-Daňka
(	(	kIx(	(
<g/>
ČKD	ČKD	kA	ČKD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Terezín	Terezín	k1gInSc1	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
Kolbenů	Kolben	k1gMnPc2	Kolben
ve	v	k7c6	v
Strančicích	Strančice	k1gFnPc6	Strančice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podle	podle	k7c2	podle
zápisu	zápis	k1gInSc2	zápis
ve	v	k7c6	v
familiantské	familiantský	k2eAgFnSc6d1	familiantský
knize	kniha	k1gFnSc6	kniha
dostal	dostat	k5eAaPmAgMnS	dostat
sklenář	sklenář	k1gMnSc1	sklenář
David	David	k1gMnSc1	David
Kolben	Kolben	k2eAgMnSc1d1	Kolben
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Joachim	Joachim	k1gMnSc1	Joachim
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
domkář	domkář	k1gMnSc1	domkář
a	a	k8xC	a
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Františkou	Františka	k1gFnSc7	Františka
Freundovou	Freundový	k2eAgFnSc7d1	Freundová
z	z	k7c2	z
Radějovic	Radějovice	k1gFnPc2	Radějovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
rodině	rodina	k1gFnSc6	rodina
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
manželství	manželství	k1gNnSc2	manželství
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
devět	devět	k4xCc1	devět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstarší	starý	k2eAgFnSc2d3	nejstarší
byl	být	k5eAaImAgMnS	být
Emil	Emil	k1gMnSc1	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Alfréd	Alfréd	k1gMnSc1	Alfréd
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
také	také	k9	také
inženýrem	inženýr	k1gMnSc7	inženýr
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
s	s	k7c7	s
Emilem	Emil	k1gMnSc7	Emil
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
elektrických	elektrický	k2eAgInPc2d1	elektrický
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolbna	k1gFnPc2	Kolbna
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
vyšší	vysoký	k2eAgMnSc1d2	vyšší
reálné	reálný	k2eAgNnSc4d1	reálné
gymnázium	gymnázium	k1gNnSc4	gymnázium
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
a	a	k8xC	a
strojnictví	strojnictví	k1gNnSc4	strojnictví
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
dokončil	dokončit	k5eAaPmAgInS	dokončit
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
a	a	k8xC	a
po	po	k7c6	po
jednoroční	jednoroční	k2eAgFnSc6d1	jednoroční
elektrotechnické	elektrotechnický	k2eAgFnSc6d1	elektrotechnická
praxi	praxe	k1gFnSc6	praxe
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
Zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
Gerstnerovo	Gerstnerův	k2eAgNnSc1d1	Gerstnerův
cestovní	cestovní	k2eAgNnSc1d1	cestovní
stipendium	stipendium	k1gNnSc1	stipendium
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1	[number]	k4	1
200	[number]	k4	200
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
studijní	studijní	k2eAgInSc4d1	studijní
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
podniky	podnik	k1gInPc7	podnik
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
odplul	odplout	k5eAaPmAgMnS	odplout
i	i	k9	i
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Malvínou	Malvína	k1gFnSc7	Malvína
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
podnikal	podnikat	k5eAaImAgMnS	podnikat
studijní	studijní	k2eAgFnPc4d1	studijní
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
získal	získat	k5eAaPmAgInS	získat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Thomase	Thomas	k1gMnSc2	Thomas
Alva	Alvus	k1gMnSc2	Alvus
Edisona	Edison	k1gMnSc2	Edison
Edison	Edison	k1gMnSc1	Edison
Machine	Machin	k1gInSc5	Machin
Company	Compan	k1gMnPc4	Compan
v	v	k7c4	v
Schenectady	Schenectada	k1gFnPc4	Schenectada
<g/>
,	,	kIx,	,
NY	NY	kA	NY
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešní	dnešní	k2eAgFnSc2d1	dnešní
General	General	k1gFnSc2	General
Electric	Electrice	k1gFnPc2	Electrice
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Edisonovým	Edisonův	k2eAgMnSc7d1	Edisonův
přímým	přímý	k2eAgMnSc7d1	přímý
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
ve	v	k7c6	v
vývojových	vývojový	k2eAgFnPc6d1	vývojová
laboratořích	laboratoř	k1gFnPc6	laboratoř
firmy	firma	k1gFnSc2	firma
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Orange	Orang	k1gFnSc2	Orang
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
jej	on	k3xPp3gInSc4	on
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
přizval	přizvat	k5eAaPmAgMnS	přizvat
ke	k	k7c3	k
zkouškám	zkouška	k1gFnPc3	zkouška
vícefázových	vícefázový	k2eAgMnPc2d1	vícefázový
elektromotorů	elektromotor	k1gInPc2	elektromotor
do	do	k7c2	do
laboratoří	laboratoř	k1gFnPc2	laboratoř
své	svůj	k3xOyFgFnSc2	svůj
firmy	firma	k1gFnSc2	firma
Tesla	Tesla	k1gMnSc1	Tesla
Electric	Electric	k1gMnSc1	Electric
Company	Compana	k1gFnSc2	Compana
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zkušenost	zkušenost	k1gFnSc1	zkušenost
velmi	velmi	k6eAd1	velmi
přispěla	přispět	k5eAaPmAgFnS	přispět
ke	k	k7c3	k
Kolbenově	Kolbenův	k2eAgFnSc3d1	Kolbenova
orientaci	orientace	k1gFnSc3	orientace
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
celoživotnímu	celoživotní	k2eAgInSc3d1	celoživotní
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Kolbena	Kolben	k2eAgFnSc1d1	Kolbena
s	s	k7c7	s
Teslou	Tesla	k1gFnSc7	Tesla
sbližoval	sbližovat	k5eAaImAgInS	sbližovat
i	i	k9	i
podobný	podobný	k2eAgInSc1d1	podobný
styl	styl	k1gInSc1	styl
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
oba	dva	k4xCgMnPc1	dva
pracovali	pracovat	k5eAaImAgMnP	pracovat
systematicky	systematicky	k6eAd1	systematicky
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samouk	samouk	k1gMnSc1	samouk
Edison	Edison	k1gMnSc1	Edison
vynalézal	vynalézat	k5eAaImAgMnS	vynalézat
spíše	spíše	k9	spíše
intuitivně	intuitivně	k6eAd1	intuitivně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Tesly	Tesla	k1gFnSc2	Tesla
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Kolben	Kolben	k2eAgMnSc1d1	Kolben
nakonec	nakonec	k6eAd1	nakonec
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
Edisonem	Edison	k1gInSc7	Edison
v	v	k7c6	v
dobrém	dobré	k1gNnSc6	dobré
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
přátelství	přátelství	k1gNnSc4	přátelství
později	pozdě	k6eAd2	pozdě
Kolbenovi	Kolben	k1gMnSc3	Kolben
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
budovat	budovat	k5eAaImF	budovat
obchodní	obchodní	k2eAgInPc4d1	obchodní
kontakty	kontakt	k1gInPc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
způsobila	způsobit	k5eAaPmAgFnS	způsobit
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
nikdy	nikdy	k6eAd1	nikdy
nezdomácněla	zdomácnět	k5eNaPmAgFnS	zdomácnět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
firmy	firma	k1gFnSc2	firma
Maschinenfabrik	Maschinenfabrik	k1gMnSc1	Maschinenfabrik
Oerlikon	Oerlikon	k1gMnSc1	Oerlikon
(	(	kIx(	(
<g/>
Strojírna	strojírna	k1gFnSc1	strojírna
Oerlikon	Oerlikon	k1gMnSc1	Oerlikon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
generátory	generátor	k1gInPc4	generátor
a	a	k8xC	a
motory	motor	k1gInPc4	motor
na	na	k7c4	na
střídavý	střídavý	k2eAgInSc4d1	střídavý
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
oborem	obor	k1gInSc7	obor
byl	být	k5eAaImAgInS	být
přenos	přenos	k1gInSc1	přenos
energie	energie	k1gFnSc2	energie
soustavou	soustava	k1gFnSc7	soustava
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
125	[number]	k4	125
km	km	kA	km
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
vedení	vedení	k1gNnSc6	vedení
z	z	k7c2	z
Laufenu	Laufen	k1gInSc2	Laufen
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
se	se	k3xPyFc4	se
prokázala	prokázat	k5eAaPmAgFnS	prokázat
správnost	správnost	k1gFnSc1	správnost
Teslových	Teslových	k2eAgFnPc2d1	Teslových
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oerlikonu	Oerlikon	k1gInSc6	Oerlikon
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Behn-Eschenburgem	Behn-Eschenburg	k1gMnSc7	Behn-Eschenburg
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vynálezcem	vynálezce	k1gMnSc7	vynálezce
jednofázového	jednofázový	k2eAgInSc2d1	jednofázový
komutátorového	komutátorový	k2eAgInSc2d1	komutátorový
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
vynikajícími	vynikající	k2eAgMnPc7d1	vynikající
elektrotechniky	elektrotechnik	k1gMnPc7	elektrotechnik
(	(	kIx(	(
<g/>
Dobrowolski	Dobrowolske	k1gFnSc4	Dobrowolske
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
Fischer-Hinnen	Fischer-Hinnen	k2eAgMnSc1d1	Fischer-Hinnen
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
zde	zde	k6eAd1	zde
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
základy	základ	k1gInPc4	základ
teorie	teorie	k1gFnSc2	teorie
i	i	k9	i
konstrukce	konstrukce	k1gFnPc1	konstrukce
asynchronních	asynchronní	k2eAgInPc2d1	asynchronní
i	i	k8xC	i
synchronních	synchronní	k2eAgInPc2d1	synchronní
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
transformátorů	transformátor	k1gInPc2	transformátor
a	a	k8xC	a
přenosové	přenosový	k2eAgFnSc2d1	přenosová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dílo	dílo	k1gNnSc1	dílo
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
se	se	k3xPyFc4	se
Kolbenovým	Kolbenův	k2eAgFnPc3d1	Kolbenova
také	také	k9	také
narodil	narodit	k5eAaPmAgInS	narodit
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
jejich	jejich	k3xOp3gMnSc1	jejich
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
-	-	kIx~	-
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
Kolben	Kolben	k2eAgMnSc1d1	Kolben
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
zabydleli	zabydlet	k5eAaPmAgMnP	zabydlet
se	se	k3xPyFc4	se
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Fleissnerka	Fleissnerka	k1gFnSc1	Fleissnerka
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
založil	založit	k5eAaPmAgMnS	založit
s	s	k7c7	s
několika	několik	k4yIc7	několik
tichými	tichý	k2eAgMnPc7d1	tichý
společníky	společník	k1gMnPc7	společník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Bondy	bond	k1gInPc4	bond
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
továrnu	továrna	k1gFnSc4	továrna
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kolben	Kolbna	k1gFnPc2	Kolbna
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
továrna	továrna	k1gFnSc1	továrna
v	v	k7c6	v
Praze-Vysočanech	Praze-Vysočan	k1gMnPc6	Praze-Vysočan
<g/>
.	.	kIx.	.
</s>
<s>
Dispozici	dispozice	k1gFnSc4	dispozice
továrny	továrna	k1gFnSc2	továrna
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
sám	sám	k3xTgMnSc1	sám
Kolben	Kolben	k2eAgMnSc1d1	Kolben
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
vzniknul	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
racionálně	racionálně	k6eAd1	racionálně
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
závod	závod	k1gInSc1	závod
s	s	k7c7	s
moderním	moderní	k2eAgNnSc7d1	moderní
vybavením	vybavení	k1gNnSc7	vybavení
umožňujícím	umožňující	k2eAgNnSc7d1	umožňující
hospodárnou	hospodárný	k2eAgFnSc4d1	hospodárná
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
periferii	periferie	k1gFnSc6	periferie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
jeho	jeho	k3xOp3gNnSc4	jeho
budoucí	budoucí	k2eAgNnSc1d1	budoucí
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Kolbenova	Kolbenův	k2eAgInSc2d1	Kolbenův
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
chystala	chystat	k5eAaImAgFnS	chystat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
velká	velký	k2eAgFnSc1d1	velká
investice	investice	k1gFnSc1	investice
–	–	k?	–
stavby	stavba	k1gFnSc2	stavba
městské	městský	k2eAgFnSc2d1	městská
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
i	i	k9	i
zde	zde	k6eAd1	zde
vzplály	vzplát	k5eAaPmAgFnP	vzplát
velké	velký	k2eAgFnPc1d1	velká
diskuze	diskuze	k1gFnPc1	diskuze
mezi	mezi	k7c7	mezi
zastánci	zastánce	k1gMnPc7	zastánce
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
a	a	k8xC	a
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
český	český	k2eAgInSc1d1	český
Edison	Edison	k1gInSc1	Edison
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
stejnosměrnou	stejnosměrný	k2eAgFnSc4d1	stejnosměrná
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
profesoři	profesor	k1gMnPc1	profesor
Domalíp	Domalíp	k1gMnSc1	Domalíp
a	a	k8xC	a
Puluj	Puluj	k1gMnPc1	Puluj
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
třífázovou	třífázový	k2eAgFnSc4d1	třífázová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
Kolben	Kolbna	k1gFnPc2	Kolbna
se	se	k3xPyFc4	se
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
diskuze	diskuze	k1gFnSc2	diskuze
zapojil	zapojit	k5eAaPmAgMnS	zapojit
a	a	k8xC	a
k	k	k7c3	k
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
překvapení	překvapení	k1gNnSc3	překvapení
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
vyhlášené	vyhlášený	k2eAgFnSc6d1	vyhlášená
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
svoji	svůj	k3xOyFgFnSc4	svůj
firmou	firma	k1gFnSc7	firma
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
Kolbenům	Kolben	k1gMnPc3	Kolben
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Gerta	Gert	k1gMnSc2	Gert
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
dcera	dcera	k1gFnSc1	dcera
Lilly	Lilla	k1gFnSc2	Lilla
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Červené	Červené	k2eAgFnSc2d1	Červené
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Hradešínské	Hradešínský	k2eAgFnSc6d1	Hradešínská
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
firma	firma	k1gFnSc1	firma
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
stoupl	stoupnout	k5eAaPmAgMnS	stoupnout
počet	počet	k1gInSc4	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
25	[number]	k4	25
na	na	k7c4	na
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
dodávala	dodávat	k5eAaImAgFnS	dodávat
především	především	k6eAd1	především
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
generátory	generátor	k1gInPc4	generátor
a	a	k8xC	a
jeřábové	jeřábový	k2eAgInPc4d1	jeřábový
pohony	pohon	k1gInPc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
banky	banka	k1gFnSc2	banka
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Kolben	Kolben	k2eAgMnSc1d1	Kolben
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Přibyla	přibýt	k5eAaPmAgFnS	přibýt
modelárna	modelárna	k1gFnSc1	modelárna
<g/>
,	,	kIx,	,
slévárna	slévárna	k1gFnSc1	slévárna
<g/>
,	,	kIx,	,
kovárna	kovárna	k1gFnSc1	kovárna
<g/>
,	,	kIx,	,
a	a	k8xC	a
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
firma	firma	k1gFnSc1	firma
první	první	k4xOgFnSc4	první
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
,	,	kIx,	,
zhotovila	zhotovit	k5eAaPmAgNnP	zhotovit
kompletní	kompletní	k2eAgNnPc1d1	kompletní
zařízení	zařízení	k1gNnPc1	zařízení
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
a	a	k8xC	a
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
tisící	tisící	k4xOgInSc4	tisící
alternátor	alternátor	k1gInSc4	alternátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
produkce	produkce	k1gFnSc1	produkce
parních	parní	k2eAgInPc2d1	parní
válců	válec	k1gInPc2	válec
a	a	k8xC	a
turbín	turbína	k1gFnPc2	turbína
a	a	k8xC	a
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
dva	dva	k4xCgInPc1	dva
alternátory	alternátor	k1gInPc1	alternátor
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
pro	pro	k7c4	pro
londýnskou	londýnský	k2eAgFnSc4d1	londýnská
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
EAS	EAS	kA	EAS
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Ringhofferovou	Ringhofferův	k2eAgFnSc7d1	Ringhofferova
strojírnou	strojírna	k1gFnSc7	strojírna
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
automobilky	automobilka	k1gFnSc2	automobilka
Praga	Praga	k1gFnSc1	Praga
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
dodala	dodat	k5eAaPmAgFnS	dodat
EAS	EAS	kA	EAS
kolem	kolem	k6eAd1	kolem
10	[number]	k4	10
000	[number]	k4	000
elektrických	elektrický	k2eAgInPc2d1	elektrický
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
70	[number]	k4	70
velkých	velký	k2eAgFnPc2d1	velká
elektráren	elektrárna	k1gFnPc2	elektrárna
včetně	včetně	k7c2	včetně
rozvoden	rozvodna	k1gFnPc2	rozvodna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
až	až	k9	až
v	v	k7c6	v
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
vybavila	vybavit	k5eAaPmAgFnS	vybavit
EAS	EAS	kA	EAS
vodní	vodní	k2eAgFnSc4d1	vodní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Štvanici	Štvanice	k1gFnSc6	Štvanice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
licenčních	licenční	k2eAgNnPc2d1	licenční
ujednání	ujednání	k1gNnPc2	ujednání
i	i	k8xC	i
osobních	osobní	k2eAgInPc2d1	osobní
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Oerlikon	Oerlikona	k1gFnPc2	Oerlikona
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
Rakousko-Uherským	rakouskoherský	k2eAgFnPc3d1	rakousko-uherská
státním	státní	k2eAgFnPc3d1	státní
drahám	draha	k1gFnPc3	draha
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
pražského	pražský	k2eAgInSc2d1	pražský
uzlu	uzel	k1gInSc2	uzel
systémem	systém	k1gInSc7	systém
10	[number]	k4	10
kV	kV	k?	kV
16	[number]	k4	16
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
tehdy	tehdy	k6eAd1	tehdy
užívaného	užívaný	k2eAgInSc2d1	užívaný
kmitočtu	kmitočet	k1gInSc2	kmitočet
48	[number]	k4	48
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nabídka	nabídka	k1gFnSc1	nabídka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
bez	bez	k7c2	bez
odezvy	odezva	k1gFnSc2	odezva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
zakládá	zakládat	k5eAaImIp3nS	zakládat
firma	firma	k1gFnSc1	firma
Schwechater	Schwechatra	k1gFnPc2	Schwechatra
Kabelwerke	Kabelwerk	k1gMnSc2	Kabelwerk
GmbH	GmbH	k1gMnSc2	GmbH
pobočku	pobočka	k1gFnSc4	pobočka
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
podílníkem	podílník	k1gMnSc7	podílník
je	být	k5eAaImIp3nS	být
i	i	k9	i
Kolben	Kolben	k2eAgMnSc1d1	Kolben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
Hostivaře	Hostivař	k1gFnSc2	Hostivař
a	a	k8xC	a
Kolben	Kolbna	k1gFnPc2	Kolbna
přebírá	přebírat	k5eAaImIp3nS	přebírat
většinový	většinový	k2eAgInSc4d1	většinový
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
Pražská	pražský	k2eAgFnSc1d1	Pražská
kabelovna	kabelovna	k1gFnSc1	kabelovna
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
s	s	k7c7	s
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
PRAKAB	PRAKAB	kA	PRAKAB
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgNnSc1d2	pozdější
Kablo	Kablo	k1gNnSc1	Kablo
Hostivař	Hostivař	k1gFnSc1	Hostivař
<g/>
.	.	kIx.	.
</s>
<s>
Kolben	Kolben	k2eAgMnSc1d1	Kolben
nebyl	být	k5eNaImAgMnS	být
jen	jen	k6eAd1	jen
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
podnikatelem	podnikatel	k1gMnSc7	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
majitelem	majitel	k1gMnSc7	majitel
velkého	velký	k2eAgNnSc2d1	velké
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
-	-	kIx~	-
licence	licence	k1gFnSc1	licence
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
kupovaly	kupovat	k5eAaImAgFnP	kupovat
významné	významný	k2eAgFnPc1d1	významná
elektrotechnické	elektrotechnický	k2eAgFnPc1d1	elektrotechnická
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
sepsal	sepsat	k5eAaPmAgMnS	sepsat
mnoho	mnoho	k4c4	mnoho
prací	práce	k1gFnPc2	práce
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
silnoproudé	silnoproudý	k2eAgFnSc3d1	silnoproudá
elektrotechnice	elektrotechnika	k1gFnSc3	elektrotechnika
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
zavádění	zavádění	k1gNnSc1	zavádění
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
vyšel	vyjít	k5eAaPmAgInS	vyjít
jeho	on	k3xPp3gInSc4	on
článek	článek	k1gInSc4	článek
Obrat	obrat	k5eAaPmF	obrat
ve	v	k7c6	v
strojním	strojní	k2eAgInSc6d1	strojní
průmyslu	průmysl	k1gInSc6	průmysl
vývojem	vývoj	k1gInSc7	vývoj
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
pojednání	pojednání	k1gNnPc4	pojednání
Vliv	vliv	k1gInSc1	vliv
křemíku	křemík	k1gInSc2	křemík
na	na	k7c4	na
elektrické	elektrický	k2eAgFnPc4d1	elektrická
a	a	k8xC	a
magnetické	magnetický	k2eAgFnPc4d1	magnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
železa	železo	k1gNnSc2	železo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
aktuální	aktuální	k2eAgFnSc1d1	aktuální
<g/>
.	.	kIx.	.
</s>
<s>
Kolbenovi	Kolben	k1gMnSc3	Kolben
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
společenského	společenský	k2eAgNnSc2d1	společenské
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
mu	on	k3xPp3gMnSc3	on
německá	německý	k2eAgFnSc1d1	německá
technika	technika	k1gFnSc1	technika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
udělila	udělit	k5eAaPmAgFnS	udělit
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1908	[number]	k4	1908
a	a	k8xC	a
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
a	a	k8xC	a
udělil	udělit	k5eAaPmAgInS	udělit
mu	on	k3xPp3gMnSc3	on
Řád	řád	k1gInSc1	řád
železné	železný	k2eAgFnSc2d1	železná
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Emila	Emil	k1gMnSc2	Emil
Kolbena	Kolben	k2eAgFnSc1d1	Kolbena
jeho	jeho	k3xOp3gMnSc1	jeho
dřívější	dřívější	k2eAgMnSc1d1	dřívější
šéf	šéf	k1gMnSc1	šéf
T.	T.	kA	T.
A.	A.	kA	A.
Edison	Edison	k1gMnSc1	Edison
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Zemského	zemský	k2eAgInSc2d1	zemský
svazu	svaz	k1gInSc2	svaz
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
království	království	k1gNnSc2	království
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
členem	člen	k1gMnSc7	člen
Československé	československý	k2eAgFnSc2d1	Československá
státní	státní	k2eAgFnSc2d1	státní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
1921	[number]	k4	1921
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
firmy	firma	k1gFnSc2	firma
s	s	k7c7	s
První	první	k4xOgFnSc7	první
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
továrnou	továrna	k1gFnSc7	továrna
na	na	k7c4	na
stroje	stroj	k1gInPc4	stroj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Českomoravská-Kolben	Českomoravská-Kolben	k2eAgInSc4d1	Českomoravská-Kolben
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolbna	k1gFnPc2	Kolbna
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
odstraněn	odstranit	k5eAaPmNgInS	odstranit
z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
EAS	EAS	kA	EAS
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ředitele	ředitel	k1gMnSc2	ředitel
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
firma	firma	k1gFnSc1	firma
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
technické	technický	k2eAgFnSc6d1	technická
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
firmou	firma	k1gFnSc7	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
firma	firma	k1gFnSc1	firma
projekt	projekt	k1gInSc1	projekt
tepelné	tepelný	k2eAgFnSc2d1	tepelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Ervěnicích	Ervěnice	k1gFnPc6	Ervěnice
a	a	k8xC	a
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
začíná	začínat	k5eAaImIp3nS	začínat
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
elektrizace	elektrizace	k1gFnSc2	elektrizace
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
dodává	dodávat	k5eAaImIp3nS	dodávat
první	první	k4xOgFnPc4	první
elektrické	elektrický	k2eAgFnPc4d1	elektrická
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
fúzi	fúze	k1gFnSc3	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Přičleněním	přičlenění	k1gNnSc7	přičlenění
Akciové	akciový	k2eAgFnSc2d1	akciová
společností	společnost	k1gFnSc7	společnost
Strojírny	strojírna	k1gFnPc1	strojírna
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Breitfeld	Breitfeld	k1gMnSc1	Breitfeld
<g/>
,	,	kIx,	,
Daněk	Daněk	k1gMnSc1	Daněk
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Českomoravská-Kolben-Daněk	Českomoravská-Kolben-Daněk	k1gInSc4	Českomoravská-Kolben-Daněk
(	(	kIx(	(
<g/>
ČKD	ČKD	kA	ČKD
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolben	k2eAgMnSc1d1	Kolben
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
měla	mít	k5eAaImAgFnS	mít
12	[number]	k4	12
tisíc	tisíc	k4xCgInSc1	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
široký	široký	k2eAgInSc4d1	široký
sortiment	sortiment	k1gInSc4	sortiment
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
dobovým	dobový	k2eAgNnSc7d1	dobové
heslem	heslo	k1gNnSc7	heslo
Vyrábíme	vyrábět	k5eAaImIp1nP	vyrábět
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
od	od	k7c2	od
špendlíku	špendlík	k1gInSc2	špendlík
po	po	k7c4	po
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
zahájila	zahájit	k5eAaPmAgFnS	zahájit
firma	firma	k1gFnSc1	firma
dokonce	dokonce	k9	dokonce
výrobu	výroba	k1gFnSc4	výroba
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
měla	mít	k5eAaImAgFnS	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
dodávala	dodávat	k5eAaImAgFnS	dodávat
firma	firma	k1gFnSc1	firma
tanky	tank	k1gInPc1	tank
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
vyjel	vyjet	k5eAaPmAgInS	vyjet
první	první	k4xOgInSc1	první
trolejbus	trolejbus	k1gInSc1	trolejbus
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
událostmi	událost	k1gFnPc7	událost
v	v	k7c6	v
Kolbenově	Kolbenův	k2eAgInSc6d1	Kolbenův
životě	život	k1gInSc6	život
byly	být	k5eAaImAgFnP	být
oslavy	oslava	k1gFnPc1	oslava
jeho	jeho	k3xOp3gInSc7	jeho
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
75	[number]	k4	75
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
jmenované	jmenovaná	k1gFnSc6	jmenovaná
příležitosti	příležitost	k1gFnSc2	příležitost
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
Vyzbrojen	vyzbrojit	k5eAaPmNgInS	vyzbrojit
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
vědomostmi	vědomost	k1gFnPc7	vědomost
a	a	k8xC	a
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
odvětvích	odvětví	k1gNnPc6	odvětví
silnoproudé	silnoproudý	k2eAgFnSc2d1	silnoproudá
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
drže	držet	k5eAaImSgInS	držet
krok	krok	k1gInSc4	krok
s	s	k7c7	s
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
nejmodernějším	moderní	k2eAgInSc7d3	nejmodernější
pokrokem	pokrok	k1gInSc7	pokrok
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
elektrických	elektrický	k2eAgInPc2d1	elektrický
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
pomyslel	pomyslet	k5eAaPmAgMnS	pomyslet
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1896	[number]	k4	1896
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dám	dát	k5eAaPmIp1nS	dát
své	svůj	k3xOyFgFnPc4	svůj
bohaté	bohatý	k2eAgFnPc4d1	bohatá
znalosti	znalost	k1gFnPc4	znalost
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
své	svůj	k3xOyFgFnSc2	svůj
české	český	k2eAgFnSc2d1	Česká
domoviny	domovina	k1gFnSc2	domovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Rakousku	Rakousko	k1gNnSc6	Rakousko
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
málo	málo	k4c1	málo
menších	malý	k2eAgFnPc2d2	menší
elektrotechnických	elektrotechnický	k2eAgFnPc2d1	elektrotechnická
výroben	výrobna	k1gFnPc2	výrobna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
počátečním	počáteční	k2eAgNnSc6d1	počáteční
stadiu	stadion	k1gNnSc6	stadion
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
nejlepším	dobrý	k2eAgMnSc6d3	nejlepší
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
sám	sám	k3xTgMnSc1	sám
zřídil	zřídit	k5eAaPmAgMnS	zřídit
moderní	moderní	k2eAgFnSc4d1	moderní
elektrotechnickou	elektrotechnický	k2eAgFnSc4d1	elektrotechnická
továrnu	továrna	k1gFnSc4	továrna
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
velké	velký	k2eAgFnSc2d1	velká
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
tradičním	tradiční	k2eAgNnSc6d1	tradiční
sídle	sídlo	k1gNnSc6	sídlo
vysoce	vysoce	k6eAd1	vysoce
rozvinutého	rozvinutý	k2eAgNnSc2d1	rozvinuté
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
:	:	kIx,	:
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
...	...	k?	...
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
dnešního	dnešní	k2eAgInSc2d1	dnešní
největšího	veliký	k2eAgInSc2d3	veliký
elektrotechnického	elektrotechnický	k2eAgInSc2d1	elektrotechnický
podniku	podnik	k1gInSc2	podnik
československého	československý	k2eAgInSc2d1	československý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1938	[number]	k4	1938
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
doporučovalo	doporučovat	k5eAaImAgNnS	doporučovat
Kolbenovi	Kolbenův	k2eAgMnPc1d1	Kolbenův
emigraci	emigrace	k1gFnSc4	emigrace
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
však	však	k9	však
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Hanuš	Hanuš	k1gMnSc1	Hanuš
a	a	k8xC	a
mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
Lilly	Lilla	k1gFnSc2	Lilla
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
nechtěli	chtít	k5eNaImAgMnP	chtít
otce	otec	k1gMnSc4	otec
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
jen	jen	k9	jen
starší	starý	k2eAgFnSc1d2	starší
Greta	Greta	k1gFnSc1	Greta
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1939	[number]	k4	1939
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
musel	muset	k5eAaImAgMnS	muset
Kolben	Kolben	k2eAgMnSc1d1	Kolben
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
židovský	židovský	k2eAgInSc4d1	židovský
původ	původ	k1gInSc4	původ
nejprve	nejprve	k6eAd1	nejprve
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
ČKD	ČKD	kA	ČKD
a	a	k8xC	a
prodat	prodat	k5eAaPmF	prodat
své	svůj	k3xOyFgInPc4	svůj
podíly	podíl	k1gInPc4	podíl
v	v	k7c6	v
Káblovce	Káblovka	k1gFnSc6	Káblovka
a	a	k8xC	a
v	v	k7c6	v
Elektroizolační	elektroizolační	k2eAgFnSc6d1	elektroizolační
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1940	[number]	k4	1940
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Malvína	Malvína	k1gFnSc1	Malvína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
deportován	deportovat	k5eAaBmNgMnS	deportovat
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
Vilém	Vilém	k1gMnSc1	Vilém
Lieder-Kolben	Lieder-Kolben	k2eAgMnSc1d1	Lieder-Kolben
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
vnuci	vnuk	k1gMnPc1	vnuk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1943	[number]	k4	1943
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
80	[number]	k4	80
let	léto	k1gNnPc2	léto
deportován	deportován	k2eAgMnSc1d1	deportován
také	také	k9	také
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolben	k2eAgMnSc1d1	Kolben
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Hanušem	Hanuš	k1gMnSc7	Hanuš
a	a	k8xC	a
vnukem	vnuk	k1gMnSc7	vnuk
Jindřichem	Jindřich	k1gMnSc7	Jindřich
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Terezín	Terezín	k1gInSc1	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
protektorátní	protektorátní	k2eAgFnSc1d1	protektorátní
vláda	vláda	k1gFnSc1	vláda
žádala	žádat	k5eAaImAgFnS	žádat
o	o	k7c4	o
výjimku	výjimka	k1gFnSc4	výjimka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
nebyla	být	k5eNaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolbna	k1gFnPc2	Kolbna
zemřel	zemřít	k5eAaPmAgMnS	zemřít
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
80	[number]	k4	80
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Podmokelských	podmokelský	k2eAgFnPc6d1	Podmokelská
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
vnuka	vnuk	k1gMnSc2	vnuk
Jindřicha	Jindřich	k1gMnSc2	Jindřich
měl	mít	k5eAaImAgMnS	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
kromě	kromě	k7c2	kromě
pár	pár	k4xCyI	pár
osobních	osobní	k2eAgFnPc2d1	osobní
věcí	věc	k1gFnPc2	věc
kufřík	kufřík	k1gInSc4	kufřík
se	s	k7c7	s
180	[number]	k4	180
akciemi	akcie	k1gFnPc7	akcie
ČKD	ČKD	kA	ČKD
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Strančicích	Strančice	k1gFnPc6	Strančice
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
Kolbenův	Kolbenův	k2eAgInSc1d1	Kolbenův
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Emila	Emil	k1gMnSc2	Emil
Kolbena	Kolben	k2eAgFnSc1d1	Kolbena
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
stanice	stanice	k1gFnPc4	stanice
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
linky	linka	k1gFnSc2	linka
B	B	kA	B
<g/>
)	)	kIx)	)
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
busta	busta	k1gFnSc1	busta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
neznámí	známý	k2eNgMnPc1d1	neznámý
pachatelé	pachatel	k1gMnPc1	pachatel
odcizili	odcizit	k5eAaPmAgMnP	odcizit
koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
podél	podél	k7c2	podél
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
stávala	stávat	k5eAaImAgFnS	stávat
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Kolbenovu	Kolbenův	k2eAgFnSc4d1	Kolbenova
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
na	na	k7c6	na
webu	web	k1gInSc6	web
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Praha	Praha	k1gFnSc1	Praha
9	[number]	k4	9
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
vybudování	vybudování	k1gNnSc1	vybudování
pomníku	pomník	k1gInSc2	pomník
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
Emila	Emil	k1gMnSc2	Emil
Kolbena	Kolben	k2eAgFnSc1d1	Kolbena
<g/>
.	.	kIx.	.
</s>
