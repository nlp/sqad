<s>
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Columbine	Columbin	k1gInSc5	Columbin
High	High	k1gInSc4	High
School	School	k1gInSc1	School
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
v	v	k7c4	v
Jefferson	Jefferson	k1gNnSc4	Jefferson
County	Counta	k1gFnSc2	Counta
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
poblíž	poblíž	k7c2	poblíž
Littletonu	Littleton	k1gInSc2	Littleton
a	a	k8xC	a
Denveru	Denver	k1gInSc2	Denver
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
náctiletí	náctiletý	k2eAgMnPc1d1	náctiletý
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
Eric	Eric	k1gInSc1	Eric
Harris	Harris	k1gInSc1	Harris
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dylan	Dylan	k1gMnSc1	Dylan
Klebold	Klebold	k1gMnSc1	Klebold
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
dvanáct	dvanáct	k4xCc4	dvanáct
studentů	student	k1gMnPc2	student
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
zranili	zranit	k5eAaPmAgMnP	zranit
dalších	další	k2eAgInPc2d1	další
dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgNnPc4	čtyři
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útoku	útok	k1gInSc6	útok
oba	dva	k4xCgMnPc1	dva
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Columbine	Columbin	k1gInSc5	Columbin
High	High	k1gInSc4	High
School	School	k1gInSc1	School
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
největším	veliký	k2eAgInSc7d3	veliký
masakrem	masakr	k1gInSc7	masakr
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k1gInSc1	Eric
Harris	Harris	k1gInSc1	Harris
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Wayne	Wayn	k1gInSc5	Wayn
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
pilot	pilot	k1gMnSc1	pilot
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
práci	práce	k1gFnSc3	práce
musel	muset	k5eAaImAgInS	muset
často	často	k6eAd1	často
stěhovat	stěhovat	k5eAaImF	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
však	však	k9	však
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Littleton	Littleton	k1gInSc1	Littleton
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eric	Eric	k1gInSc1	Eric
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Dylanem	Dylan	k1gMnSc7	Dylan
Kleboldem	Klebold	k1gMnSc7	Klebold
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
koupil	koupit	k5eAaPmAgMnS	koupit
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
dům	dům	k1gInSc1	dům
na	na	k7c6	na
Pierce	Pierka	k1gFnSc6	Pierka
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Columbine	Columbin	k1gMnSc5	Columbin
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
s	s	k7c7	s
Dylanem	Dylan	k1gInSc7	Dylan
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
také	také	k9	také
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
populární	populární	k2eAgFnSc2d1	populární
hry	hra	k1gFnSc2	hra
Doom	Doom	k1gMnSc1	Doom
II	II	kA	II
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
mise	mise	k1gFnPc4	mise
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
však	však	k9	však
přišel	přijít	k5eAaPmAgInS	přijít
zlom	zlom	k1gInSc1	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
šikany	šikana	k1gFnPc4	šikana
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
blog	blog	k1gInSc4	blog
zaplnily	zaplnit	k5eAaPmAgFnP	zaplnit
nenávistné	návistný	k2eNgInPc4d1	nenávistný
komentáře	komentář	k1gInPc4	komentář
mířené	mířený	k2eAgInPc4d1	mířený
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
škole	škola	k1gFnSc3	škola
<g/>
,	,	kIx,	,
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
udání	udání	k1gNnSc2	udání
vyšetřován	vyšetřován	k2eAgInSc4d1	vyšetřován
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
nejdříve	dříve	k6eAd3	dříve
chtěla	chtít	k5eAaImAgFnS	chtít
provést	provést	k5eAaPmF	provést
domovní	domovní	k2eAgFnSc4d1	domovní
prohlídku	prohlídka	k1gFnSc4	prohlídka
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
upustila	upustit	k5eAaPmAgFnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Klebold	Klebold	k6eAd1	Klebold
s	s	k7c7	s
Harrisem	Harris	k1gInSc7	Harris
se	se	k3xPyFc4	se
také	také	k9	také
pokusili	pokusit	k5eAaPmAgMnP	pokusit
vykrást	vykrást	k5eAaPmF	vykrást
dodávku	dodávka	k1gFnSc4	dodávka
a	a	k8xC	a
soud	soud	k1gInSc4	soud
jim	on	k3xPp3gMnPc3	on
nařídil	nařídit	k5eAaPmAgMnS	nařídit
podstoupit	podstoupit	k5eAaPmF	podstoupit
několik	několik	k4yIc1	několik
psychologických	psychologický	k2eAgNnPc2d1	psychologické
sezení	sezení	k1gNnPc2	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k1gInSc1	Eric
dokonce	dokonce	k9	dokonce
napsal	napsat	k5eAaPmAgInS	napsat
majiteli	majitel	k1gMnSc3	majitel
dodávky	dodávka	k1gFnSc2	dodávka
omluvný	omluvný	k2eAgInSc4d1	omluvný
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
jednání	jednání	k1gNnSc6	jednání
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
náhlý	náhlý	k2eAgInSc4d1	náhlý
zkrat	zkrat	k1gInSc4	zkrat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
velice	velice	k6eAd1	velice
mrzí	mrzet	k5eAaImIp3nS	mrzet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
povinného	povinný	k2eAgNnSc2d1	povinné
sezení	sezení	k1gNnSc2	sezení
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byli	být	k5eAaImAgMnP	být
psychologem	psycholog	k1gMnSc7	psycholog
hodnoceni	hodnotit	k5eAaImNgMnP	hodnotit
velice	velice	k6eAd1	velice
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
)	)	kIx)	)
raději	rád	k6eAd2	rád
smazal	smazat	k5eAaPmAgInS	smazat
veškeré	veškerý	k3xTgInPc4	veškerý
nenávistné	návistný	k2eNgInPc4d1	nenávistný
komentáře	komentář	k1gInPc4	komentář
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
blogu	blog	k1gInSc2	blog
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
opět	opět	k6eAd1	opět
jen	jen	k6eAd1	jen
hře	hra	k1gFnSc3	hra
Doom	Dooma	k1gFnPc2	Dooma
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
však	však	k9	však
psát	psát	k5eAaImF	psát
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
nenávistných	návistný	k2eNgFnPc2d1	nenávistná
poznámek	poznámka	k1gFnPc2	poznámka
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
(	(	kIx(	(
<g/>
urážel	urážet	k5eAaImAgInS	urážet
zde	zde	k6eAd1	zde
i	i	k8xC	i
majitele	majitel	k1gMnSc2	majitel
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
kvůli	kvůli	k7c3	kvůli
léku	lék	k1gInSc3	lék
Luvox	Luvox	k1gInSc1	Luvox
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
předepsali	předepsat	k5eAaPmAgMnP	předepsat
psychiatři	psychiatr	k1gMnPc1	psychiatr
<g/>
.	.	kIx.	.
</s>
<s>
Dylan	Dylan	k1gMnSc1	Dylan
Klebold	Klebold	k1gMnSc1	Klebold
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Littletonu	Littleton	k1gInSc6	Littleton
bydlel	bydlet	k5eAaImAgMnS	bydlet
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
zde	zde	k6eAd1	zde
veškeré	veškerý	k3xTgInPc4	veškerý
typy	typ	k1gInPc4	typ
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
programu	program	k1gInSc2	program
CHIPS	chips	k1gInSc1	chips
pro	pro	k7c4	pro
chytré	chytrý	k2eAgFnPc4d1	chytrá
a	a	k8xC	a
nadané	nadaný	k2eAgFnPc4d1	nadaná
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
vesměs	vesměs	k6eAd1	vesměs
normálně	normálně	k6eAd1	normálně
až	až	k9	až
do	do	k7c2	do
osmé	osmý	k4xOgFnSc2	osmý
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Ericem	Eriec	k1gInSc7	Eriec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stali	stát	k5eAaPmAgMnP	stát
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
spolu	spolu	k6eAd1	spolu
natáčet	natáčet	k5eAaImF	natáčet
amatérské	amatérský	k2eAgInPc4d1	amatérský
filmy	film	k1gInPc4	film
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Dylanovy	Dylanův	k2eAgFnSc2d1	Dylanova
kamarádky	kamarádka	k1gFnSc2	kamarádka
si	se	k3xPyFc3	se
opatřili	opatřit	k5eAaPmAgMnP	opatřit
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Dylanově	Dylanův	k2eAgInSc6d1	Dylanův
laptopu	laptop	k1gInSc6	laptop
se	se	k3xPyFc4	se
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
nalezly	nalézt	k5eAaBmAgInP	nalézt
zápisky	zápisek	k1gInPc1	zápisek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Eric	Eric	k1gInSc1	Eric
projevoval	projevovat	k5eAaImAgInS	projevovat
znechucení	znechucení	k1gNnSc1	znechucení
současným	současný	k2eAgInSc7d1	současný
stavem	stav	k1gInSc7	stav
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
si	se	k3xPyFc3	se
spolu	spolu	k6eAd1	spolu
chodili	chodit	k5eAaImAgMnP	chodit
zastřílet	zastřílet	k5eAaPmF	zastřílet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
natáčeli	natáčet	k5eAaImAgMnP	natáčet
na	na	k7c4	na
zapůjčenou	zapůjčený	k2eAgFnSc4d1	zapůjčená
školní	školní	k2eAgFnSc4d1	školní
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Dylan	Dylan	k1gInSc1	Dylan
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Erica	Eric	k1gInSc2	Eric
rád	rád	k6eAd1	rád
sportoval	sportovat	k5eAaImAgMnS	sportovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
sportovních	sportovní	k2eAgFnPc2d1	sportovní
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
o	o	k7c6	o
Ericovi	Eriec	k1gMnSc6	Eriec
rozhodně	rozhodně	k6eAd1	rozhodně
říct	říct	k5eAaPmF	říct
nedalo	dát	k5eNaPmAgNnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k1gInSc1	Eric
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
obecně	obecně	k6eAd1	obecně
mnohem	mnohem	k6eAd1	mnohem
radikálněji	radikálně	k6eAd2	radikálně
a	a	k8xC	a
agresivněji	agresivně	k6eAd2	agresivně
a	a	k8xC	a
několik	několik	k4yIc1	několik
odborníků	odborník	k1gMnPc2	odborník
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotný	samotný	k2eAgInSc1d1	samotný
Klebold	Klebold	k1gInSc1	Klebold
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
nikdy	nikdy	k6eAd1	nikdy
neodhodlal	odhodlat	k5eNaPmAgInS	odhodlat
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
studenti	student	k1gMnPc1	student
přijeli	přijet	k5eAaPmAgMnP	přijet
ke	k	k7c3	k
škole	škola	k1gFnSc3	škola
v	v	k7c6	v
černých	černý	k2eAgInPc6d1	černý
kabátech	kabát	k1gInPc6	kabát
a	a	k8xC	a
se	s	k7c7	s
slunečními	sluneční	k2eAgFnPc7d1	sluneční
brýlemi	brýle	k1gFnPc7	brýle
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
střelců	střelec	k1gMnPc2	střelec
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
vybuchnout	vybuchnout	k5eAaPmF	vybuchnout
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
jídelně	jídelna	k1gFnSc6	jídelna
dvě	dva	k4xCgFnPc4	dva
propanbutanové	propanbutanový	k2eAgFnPc4d1	propanbutanová
bomby	bomba	k1gFnPc4	bomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bomby	bomba	k1gFnPc1	bomba
ale	ale	k9	ale
selhaly	selhat	k5eAaPmAgFnP	selhat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
útočníci	útočník	k1gMnPc1	útočník
začnou	začít	k5eAaPmIp3nP	začít
své	svůj	k3xOyFgMnPc4	svůj
spolužáky	spolužák	k1gMnPc4	spolužák
likvidovat	likvidovat	k5eAaBmF	likvidovat
"	"	kIx"	"
<g/>
ručně	ručně	k6eAd1	ručně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
byla	být	k5eAaImAgFnS	být
Rachel	Rachel	k1gInSc4	Rachel
Scottová	Scottová	k1gFnSc1	Scottová
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
sedící	sedící	k2eAgFnSc1d1	sedící
u	u	k7c2	u
východního	východní	k2eAgInSc2d1	východní
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
kulky	kulka	k1gFnPc1	kulka
ji	on	k3xPp3gFnSc4	on
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
do	do	k7c2	do
hrudníku	hrudník	k1gInSc2	hrudník
a	a	k8xC	a
poté	poté	k6eAd1	poté
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
těsné	těsný	k2eAgFnSc2d1	těsná
blízkosti	blízkost	k1gFnSc2	blízkost
zasadil	zasadit	k5eAaPmAgInS	zasadit
smrtící	smrtící	k2eAgFnSc4d1	smrtící
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
ránu	rána	k1gFnSc4	rána
Eric	Eric	k1gFnSc4	Eric
Harris	Harris	k1gFnSc2	Harris
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
zabijáků	zabiják	k1gMnPc2	zabiják
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
nekontrolovatelnou	kontrolovatelný	k2eNgFnSc7d1	nekontrolovatelná
střelbou	střelba	k1gFnSc7	střelba
už	už	k9	už
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
na	na	k7c6	na
chodbách	chodba	k1gFnPc6	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Vyděšení	vyděšený	k2eAgMnPc1d1	vyděšený
studenti	student	k1gMnPc1	student
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
ukrývali	ukrývat	k5eAaImAgMnP	ukrývat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
utekli	utéct	k5eAaPmAgMnP	utéct
okny	okno	k1gNnPc7	okno
v	v	k7c6	v
učebnách	učebna	k1gFnPc6	učebna
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgFnSc1d3	nejhorší
část	část	k1gFnSc1	část
masakru	masakr	k1gInSc2	masakr
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
střelci	střelec	k1gMnPc7	střelec
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
pod	pod	k7c7	pod
malými	malý	k2eAgInPc7d1	malý
stolky	stolek	k1gInPc7	stolek
a	a	k8xC	a
regály	regál	k1gInPc1	regál
s	s	k7c7	s
knihami	kniha	k1gFnPc7	kniha
třáslo	třást	k5eAaImAgNnS	třást
52	[number]	k4	52
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
knihovnice	knihovnice	k1gFnPc1	knihovnice
a	a	k8xC	a
dvojice	dvojice	k1gFnSc1	dvojice
vyučujících	vyučující	k2eAgNnPc2d1	vyučující
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
učitelka	učitelka	k1gFnSc1	učitelka
Patti	Patť	k1gFnSc2	Patť
Nielson	Nielsona	k1gFnPc2	Nielsona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
zrovna	zrovna	k6eAd1	zrovna
držela	držet	k5eAaImAgFnS	držet
u	u	k7c2	u
ucha	ucho	k1gNnSc2	ucho
telefon	telefon	k1gInSc1	telefon
a	a	k8xC	a
hovořila	hovořit	k5eAaImAgFnS	hovořit
s	s	k7c7	s
dispečerkou	dispečerka	k1gFnSc7	dispečerka
tísňové	tísňový	k2eAgFnSc2d1	tísňová
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
popisovala	popisovat	k5eAaImAgFnS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgMnPc1	dva
studenti	student	k1gMnPc1	student
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tu	tu	k6eAd1	tu
střílí	střílet	k5eAaImIp3nP	střílet
po	po	k7c6	po
ostatních	ostatní	k2eAgMnPc6d1	ostatní
lidech	člověk	k1gMnPc6	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hovor	hovor	k1gInSc1	hovor
mezi	mezi	k7c7	mezi
Patti	Patti	k1gNnSc7	Patti
a	a	k8xC	a
dispečerkou	dispečerka	k1gFnSc7	dispečerka
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
masakru	masakr	k1gInSc2	masakr
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
uniklých	uniklý	k2eAgInPc6d1	uniklý
útržcích	útržek	k1gInPc6	útržek
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
slyšet	slyšet	k5eAaImF	slyšet
oba	dva	k4xCgMnPc4	dva
střelce	střelec	k1gMnPc4	střelec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
střelby	střelba	k1gFnSc2	střelba
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
šestnáctiletý	šestnáctiletý	k2eAgInSc4d1	šestnáctiletý
Kyle	Kyle	k1gInSc4	Kyle
Velasquez	Velasquez	k1gMnSc1	Velasquez
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
Dylan	Dylan	k1gMnSc1	Dylan
nemilosrdně	milosrdně	k6eNd1	milosrdně
popravil	popravit	k5eAaPmAgMnS	popravit
brokovnicí	brokovnice	k1gFnSc7	brokovnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
sedmi	sedm	k4xCc6	sedm
minutách	minuta	k1gFnPc6	minuta
pak	pak	k9	pak
oba	dva	k4xCgMnPc1	dva
střelci	střelec	k1gMnPc1	střelec
pochodovali	pochodovat	k5eAaImAgMnP	pochodovat
po	po	k7c6	po
knihovně	knihovna	k1gFnSc6	knihovna
a	a	k8xC	a
stříleli	střílet	k5eAaImAgMnP	střílet
na	na	k7c4	na
schovávající	schovávající	k2eAgInSc4d1	schovávající
se	s	k7c7	s
studenty	student	k1gMnPc7	student
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
situaci	situace	k1gFnSc4	situace
krutě	krutě	k6eAd1	krutě
komentovali	komentovat	k5eAaBmAgMnP	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
masakru	masakr	k1gInSc6	masakr
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
střelci	střelec	k1gMnPc1	střelec
odebrali	odebrat	k5eAaPmAgMnP	odebrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
oněch	onen	k3xDgFnPc2	onen
osudných	osudný	k2eAgFnPc2d1	osudná
sedmi	sedm	k4xCc2	sedm
minut	minuta	k1gFnPc2	minuta
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
deset	deset	k4xCc1	deset
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
jídelny	jídelna	k1gFnSc2	jídelna
házeli	házet	k5eAaImAgMnP	házet
Eric	Eric	k1gFnSc4	Eric
a	a	k8xC	a
Dylan	Dylan	k1gInSc4	Dylan
do	do	k7c2	do
prázdných	prázdný	k2eAgFnPc2d1	prázdná
učeben	učebna	k1gFnPc2	učebna
podomácku	podomácku	k6eAd1	podomácku
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
bomby	bomba	k1gFnPc4	bomba
a	a	k8xC	a
náhodně	náhodně	k6eAd1	náhodně
stříleli	střílet	k5eAaImAgMnP	střílet
do	do	k7c2	do
věcí	věc	k1gFnPc2	věc
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
nepokoušeli	pokoušet	k5eNaImAgMnP	pokoušet
se	se	k3xPyFc4	se
však	však	k9	však
nikomu	nikdo	k3yNnSc3	nikdo
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
dorazili	dorazit	k5eAaPmAgMnP	dorazit
oba	dva	k4xCgMnPc1	dva
útočníci	útočník	k1gMnPc1	útočník
do	do	k7c2	do
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečností	bezpečnost	k1gFnSc7	bezpečnost
kamery	kamera	k1gFnSc2	kamera
v	v	k7c6	v
jídelně	jídelna	k1gFnSc6	jídelna
zachytily	zachytit	k5eAaPmAgFnP	zachytit
jejich	jejich	k3xOp3gInSc4	jejich
příchod	příchod	k1gInSc4	příchod
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejich	jejich	k3xOp3gInPc1	jejich
pohyby	pohyb	k1gInPc1	pohyb
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
jídelny	jídelna	k1gFnSc2	jídelna
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
Eric	Eric	k1gInSc4	Eric
několik	několik	k4yIc1	několik
ran	rána	k1gFnPc2	rána
do	do	k7c2	do
nevybuchlé	vybuchlý	k2eNgFnSc2d1	nevybuchlá
propanové	propanový	k2eAgFnSc2d1	propanová
bomby	bomba	k1gFnSc2	bomba
při	při	k7c6	při
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
ji	on	k3xPp3gFnSc4	on
odpálit	odpálit	k5eAaPmF	odpálit
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
Dylan	Dylan	k1gMnSc1	Dylan
hodil	hodit	k5eAaImAgMnS	hodit
na	na	k7c4	na
propanovou	propanový	k2eAgFnSc4d1	propanová
bombu	bomba	k1gFnSc4	bomba
Molotov	Molotovo	k1gNnPc2	Molotovo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
způsobil	způsobit	k5eAaPmAgMnS	způsobit
její	její	k3xOp3gFnSc4	její
detonaci	detonace	k1gFnSc4	detonace
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
oba	dva	k4xCgMnPc1	dva
opustili	opustit	k5eAaPmAgMnP	opustit
jídelnu	jídelna	k1gFnSc4	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
následujících	následující	k2eAgNnPc2d1	následující
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
se	se	k3xPyFc4	se
toulali	toulat	k5eAaImAgMnP	toulat
po	po	k7c6	po
chodbách	chodba	k1gFnPc6	chodba
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
jídelny	jídelna	k1gFnSc2	jídelna
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
později	pozdě	k6eAd2	pozdě
odešli	odejít	k5eAaPmAgMnP	odejít
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
knihovny	knihovna	k1gFnSc2	knihovna
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
si	se	k3xPyFc3	se
několik	několik	k4yIc1	několik
výstřelů	výstřel	k1gInPc2	výstřel
s	s	k7c7	s
policisty	policista	k1gMnPc7	policista
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k1gFnSc1	Eric
Harris	Harris	k1gFnPc2	Harris
si	se	k3xPyFc3	se
strčil	strčit	k5eAaPmAgMnS	strčit
hlaveň	hlaveň	k1gFnSc4	hlaveň
své	svůj	k3xOyFgFnSc2	svůj
upilované	upilovaný	k2eAgFnSc2d1	upilovaná
brokovnice	brokovnice	k1gFnSc2	brokovnice
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgInS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
Dylan	Dylan	k1gMnSc1	Dylan
si	se	k3xPyFc3	se
sundal	sundat	k5eAaPmAgMnS	sundat
svou	svůj	k3xOyFgFnSc4	svůj
náušnici	náušnice	k1gFnSc4	náušnice
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
střelil	střelit	k5eAaPmAgMnS	střelit
svou	svůj	k3xOyFgFnSc4	svůj
Tec-	Tec-	k1gFnSc4	Tec-
<g/>
9	[number]	k4	9
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c4	o
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
výročím	výročí	k1gNnSc7	výročí
narozenin	narozeniny	k1gFnPc2	narozeniny
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
a	a	k8xC	a
rasovém	rasový	k2eAgInSc6d1	rasový
motivu	motiv	k1gInSc6	motiv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
mediální	mediální	k2eAgFnPc1d1	mediální
spekulace	spekulace	k1gFnPc1	spekulace
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
jestli	jestli	k8xS	jestli
tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
tzv.	tzv.	kA	tzv.
Trenchcoat	Trenchcoat	k1gInSc4	Trenchcoat
Mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgInPc4	dva
patřili	patřit	k5eAaImAgMnP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Znakem	znak	k1gInSc7	znak
této	tento	k3xDgFnSc2	tento
školní	školní	k2eAgFnSc2d1	školní
party	parta	k1gFnSc2	parta
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
tmavé	tmavý	k2eAgNnSc4d1	tmavé
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
kabáty	kabát	k1gInPc4	kabát
a	a	k8xC	a
sluneční	sluneční	k2eAgFnPc4d1	sluneční
brýle	brýle	k1gFnPc4	brýle
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
rozproudil	rozproudit	k5eAaPmAgInS	rozproudit
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
kontrole	kontrola	k1gFnSc6	kontrola
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
dostupnosti	dostupnost	k1gFnPc1	dostupnost
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
o	o	k7c6	o
násilí	násilí	k1gNnSc6	násilí
mezi	mezi	k7c7	mezi
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
masakru	masakr	k1gInSc2	masakr
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
šikana	šikana	k1gFnSc1	šikana
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
násilných	násilný	k2eAgFnPc2d1	násilná
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
kritizováni	kritizovat	k5eAaImNgMnP	kritizovat
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
Marilyn	Marilyn	k1gFnSc4	Marilyn
Manson	Mansona	k1gFnPc2	Mansona
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
užívání	užívání	k1gNnSc1	užívání
antidepresiv	antidepresivum	k1gNnPc2	antidepresivum
náctiletými	náctiletý	k2eAgInPc7d1	náctiletý
(	(	kIx(	(
<g/>
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
pitva	pitva	k1gFnSc1	pitva
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
efektivní	efektivní	k2eAgFnSc4d1	efektivní
dávku	dávka	k1gFnSc4	dávka
léčiva	léčivo	k1gNnSc2	léčivo
Luvox	Luvox	k1gInSc1	Luvox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oběti	oběť	k1gFnPc1	oběť
<g/>
,	,	kIx,	,
zavražděné	zavražděný	k2eAgFnPc1d1	zavražděná
údajně	údajně	k6eAd1	údajně
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gNnSc3	jejich
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
úpadek	úpadek	k1gInSc4	úpadek
role	role	k1gFnSc2	role
náboženství	náboženství	k1gNnSc2	náboženství
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
svalovali	svalovat	k5eAaImAgMnP	svalovat
vinu	vina	k1gFnSc4	vina
za	za	k7c4	za
tragédii	tragédie	k1gFnSc4	tragédie
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nedostatečně	dostatečně	k6eNd1	dostatečně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
náboženství	náboženství	k1gNnSc1	náboženství
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
také	také	k9	také
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c6	v
zvýšení	zvýšení	k1gNnSc6	zvýšení
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
morální	morální	k2eAgFnSc2d1	morální
paniky	panika	k1gFnSc2	panika
namířené	namířený	k2eAgFnSc2d1	namířená
proti	proti	k7c3	proti
gotické	gotický	k2eAgFnSc3d1	gotická
subkultuře	subkultura	k1gFnSc3	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tragédii	tragédie	k1gFnSc4	tragédie
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
masakru	masakr	k1gInSc6	masakr
a	a	k8xC	a
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
držení	držení	k1gNnSc2	držení
zbraní	zbraň	k1gFnPc2	zbraň
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
natočil	natočit	k5eAaBmAgMnS	natočit
americký	americký	k2eAgMnSc1d1	americký
filmař	filmař	k1gMnSc1	filmař
Michael	Michael	k1gMnSc1	Michael
Moore	Moor	k1gInSc5	Moor
film	film	k1gInSc4	film
Bowling	bowling	k1gInSc4	bowling
for	forum	k1gNnPc2	forum
Columbine	Columbin	k1gMnSc5	Columbin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
tento	tento	k3xDgMnSc1	tento
masakr	masakr	k1gInSc4	masakr
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
filmu	film	k1gInSc6	film
Slon	slon	k1gMnSc1	slon
režisér	režisér	k1gMnSc1	režisér
Gus	Gus	k1gMnSc1	Gus
Van	vana	k1gFnPc2	vana
Sant	Sant	k1gMnSc1	Sant
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
masakru	masakr	k1gInSc6	masakr
a	a	k8xC	a
o	o	k7c6	o
životu	život	k1gInSc2	život
Rachel	Rachela	k1gFnPc2	Rachela
Scottové	Scottová	k1gFnSc2	Scottová
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
z	z	k7c2	z
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
film	film	k1gInSc1	film
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Ashamed	Ashamed	k1gInSc4	Ashamed
od	od	k7c2	od
režiséra	režisér	k1gMnSc2	režisér
Braina	Brain	k1gMnSc2	Brain
Baugha	Baugh	k1gMnSc2	Baugh
<g/>
.	.	kIx.	.
</s>
<s>
Cassie	Cassie	k1gFnSc1	Cassie
Bernallová	Bernallová	k1gFnSc1	Bernallová
<g/>
,	,	kIx,	,
17	[number]	k4	17
Steven	Stevno	k1gNnPc2	Stevno
Curnow	Curnow	k1gFnPc2	Curnow
<g/>
,	,	kIx,	,
14	[number]	k4	14
Corey	Corea	k1gFnSc2	Corea
DePooter	DePootra	k1gFnPc2	DePootra
<g/>
,	,	kIx,	,
17	[number]	k4	17
Kelly	Kella	k1gMnSc2	Kella
Fleming	Fleming	k1gInSc4	Fleming
<g/>
,	,	kIx,	,
16	[number]	k4	16
Matthew	Matthew	k1gFnPc2	Matthew
Kechter	Kechtra	k1gFnPc2	Kechtra
<g/>
,	,	kIx,	,
16	[number]	k4	16
Daniel	Daniel	k1gMnSc1	Daniel
Mauser	Mauser	k1gMnSc1	Mauser
<g/>
,	,	kIx,	,
15	[number]	k4	15
Daniel	Daniel	k1gMnSc1	Daniel
Rohrbough	Rohrbough	k1gMnSc1	Rohrbough
<g/>
,	,	kIx,	,
15	[number]	k4	15
William	William	k1gInSc1	William
David	David	k1gMnSc1	David
"	"	kIx"	"
<g/>
Dave	Dav	k1gInSc5	Dav
<g/>
"	"	kIx"	"
Sanders	Sandersa	k1gFnPc2	Sandersa
<g/>
,	,	kIx,	,
47	[number]	k4	47
Rachel	Rachel	k1gMnSc1	Rachel
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
17	[number]	k4	17
Isaiah	Isaiaha	k1gFnPc2	Isaiaha
Shoels	Shoelsa	k1gFnPc2	Shoelsa
<g/>
,	,	kIx,	,
18	[number]	k4	18
John	John	k1gMnSc1	John
Tomlin	Tomlina	k1gFnPc2	Tomlina
<g/>
,	,	kIx,	,
16	[number]	k4	16
Lauren	Laurno	k1gNnPc2	Laurno
Townsend	Townsenda	k1gFnPc2	Townsenda
<g/>
,	,	kIx,	,
18	[number]	k4	18
Kyle	Kyle	k1gNnSc2	Kyle
Velasquez	Velasqueza	k1gFnPc2	Velasqueza
<g/>
,	,	kIx,	,
16	[number]	k4	16
Eric	Eric	k1gFnSc1	Eric
Harris	Harris	k1gFnPc2	Harris
si	se	k3xPyFc3	se
vedl	vést	k5eAaImAgInS	vést
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgInS	představovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
unese	unést	k5eAaPmIp3nS	unést
letadlo	letadlo	k1gNnSc4	letadlo
a	a	k8xC	a
havaruje	havarovat	k5eAaPmIp3nS	havarovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c4	v
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
City	city	k1gNnSc2	city
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Virginia	Virginium	k1gNnPc4	Virginium
Tech	Tech	k?	Tech
Masakr	masakr	k1gInSc4	masakr
na	na	k7c4	na
Sandy	Sand	k1gInPc4	Sand
Hook	Hook	k1gMnSc1	Hook
Elementary	Elementara	k1gFnSc2	Elementara
School	School	k1gInSc4	School
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Masakr	masakr	k1gInSc4	masakr
na	na	k7c6	na
Columbine	Columbin	k1gInSc5	Columbin
High	High	k1gInSc4	High
School	School	k1gInSc4	School
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Columbine	Columbin	k1gInSc5	Columbin
High	High	k1gInSc1	High
School	School	k1gInSc1	School
Tragedy	Trageda	k1gMnSc2	Trageda
-	-	kIx~	-
Tragédie	tragédie	k1gFnSc1	tragédie
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
Columbine	Columbin	k1gMnSc5	Columbin
</s>
