<p>
<s>
Olympic	Olympice	k1gFnPc2	Olympice
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Karkulka	Karkulka	k1gFnSc1	Karkulka
(	(	kIx(	(
<g/>
Karlínský	karlínský	k2eAgInSc1d1	karlínský
Kulturní	kulturní	k2eAgInSc1d1	kulturní
Kabaret	kabaret	k1gInSc1	kabaret
<g/>
)	)	kIx)	)
Miloslav	Miloslav	k1gMnSc1	Miloslav
Růžek	Růžek	k1gMnSc1	Růžek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgMnPc1d1	původní
členové	člen	k1gMnPc1	člen
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
zakladatele	zakladatel	k1gMnPc4	zakladatel
české	český	k2eAgFnSc2d1	Česká
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Želva	želva	k1gFnSc1	želva
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc4d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
přelomových	přelomový	k2eAgNnPc2d1	přelomové
alb	album	k1gNnPc2	album
českého	český	k2eAgInSc2d1	český
bigbítu	bigbít	k1gInSc2	bigbít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slabším	slabý	k2eAgNnSc6d2	slabší
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
prošla	projít	k5eAaPmAgFnS	projít
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nastává	nastávat	k5eAaImIp3nS	nastávat
další	další	k2eAgNnSc1d1	další
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
tvorby	tvorba	k1gFnSc2	tvorba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
konceptuální	konceptuální	k2eAgFnSc2d1	konceptuální
albové	albový	k2eAgFnPc4d1	albová
trilogie	trilogie	k1gFnPc4	trilogie
Prázdniny	prázdniny	k1gFnPc4	prázdniny
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
Laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
už	už	k6eAd1	už
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
baskytarista	baskytarista	k1gMnSc1	baskytarista
Milan	Milan	k1gMnSc1	Milan
Broum	Broum	k1gInSc4	Broum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
vnesl	vnést	k5eAaPmAgMnS	vnést
nejen	nejen	k6eAd1	nejen
moderní	moderní	k2eAgNnSc4d1	moderní
vidění	vidění	k1gNnSc4	vidění
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
skvělé	skvělý	k2eAgInPc1d1	skvělý
instrumentální	instrumentální	k2eAgInPc1d1	instrumentální
výkony	výkon	k1gInPc1	výkon
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc2	který
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
už	už	k9	už
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
albu	album	k1gNnSc6	album
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Olympiku	Olympik	k1gInSc2	Olympik
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byl	být	k5eAaImAgInS	být
Marathón	Marathón	k1gInSc1	Marathón
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
skupina	skupina	k1gFnSc1	skupina
koketovala	koketovat	k5eAaImAgFnS	koketovat
i	i	k9	i
s	s	k7c7	s
heavy	heav	k1gMnPc4	heav
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc7	její
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
polohou	poloha	k1gFnSc7	poloha
jsou	být	k5eAaImIp3nP	být
melodické	melodický	k2eAgFnPc1d1	melodická
rockové	rockový	k2eAgFnPc1d1	rocková
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
kvalitními	kvalitní	k2eAgInPc7d1	kvalitní
texty	text	k1gInPc7	text
a	a	k8xC	a
nápaditými	nápaditý	k2eAgFnPc7d1	nápaditá
aranžemi	aranže	k1gFnPc7	aranže
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc4	text
dodávali	dodávat	k5eAaImAgMnP	dodávat
zejména	zejména	k9	zejména
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Rytíř	Rytíř	k1gMnSc1	Rytíř
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Vrba	Vrba	k1gMnSc1	Vrba
(	(	kIx(	(
<g/>
v	v	k7c6	v
období	období	k1gNnSc6	období
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
byl	být	k5eAaImAgMnS	být
téměř	téměř	k6eAd1	téměř
výhradním	výhradní	k2eAgMnSc7d1	výhradní
textařem	textař	k1gMnSc7	textař
Pavel	Pavel	k1gMnSc1	Pavel
Chrastina	chrastina	k1gFnSc1	chrastina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
skupina	skupina	k1gFnSc1	skupina
oslavila	oslavit	k5eAaPmAgFnS	oslavit
své	své	k1gNnSc4	své
50	[number]	k4	50
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
halovou	halový	k2eAgFnSc4d1	halová
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůrou	šňůra	k1gFnSc7	šňůra
a	a	k8xC	a
vydáním	vydání	k1gNnSc7	vydání
kolekcí	kolekce	k1gFnPc2	kolekce
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
koncertě	koncert	k1gInSc6	koncert
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Milan	Milan	k1gMnSc1	Milan
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
bubeník	bubeník	k1gMnSc1	bubeník
Martin	Martin	k1gMnSc1	Martin
Vajgl	vajgl	k1gInSc4	vajgl
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
následně	následně	k6eAd1	následně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013-2015	[number]	k4	2013-2015
natáčí	natáčet	k5eAaImIp3nP	natáčet
novou	nový	k2eAgFnSc4d1	nová
albovou	albový	k2eAgFnSc4d1	albová
trilogii	trilogie	k1gFnSc4	trilogie
Souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
s	s	k7c7	s
programem	program	k1gInSc7	program
Permanent	permanent	k1gInSc1	permanent
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
akustické	akustický	k2eAgNnSc1d1	akustické
turné	turné	k1gNnSc1	turné
Plugged	Plugged	k1gMnSc1	Plugged
Unplugged	Unplugged	k1gMnSc1	Unplugged
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
Olympic	Olympice	k1gInPc2	Olympice
v	v	k7c6	v
zaplněné	zaplněný	k2eAgFnSc6d1	zaplněná
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
slaví	slavit	k5eAaImIp3nS	slavit
55	[number]	k4	55
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
televizními	televizní	k2eAgFnPc7d1	televizní
kamerami	kamera	k1gFnPc7	kamera
pro	pro	k7c4	pro
live	live	k1gNnSc4	live
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
vzniká	vznikat	k5eAaImIp3nS	vznikat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
Olympiku	Olympik	k1gInSc2	Olympik
s	s	k7c7	s
názvem	název	k1gInSc7	název
Trilobit	trilobit	k1gMnSc1	trilobit
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
−	−	k?	−
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Broum	Broum	k1gInSc1	Broum
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
−	−	k?	−
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Valenta	Valenta	k1gMnSc1	Valenta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
−	−	k?	−
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Vajgl	vajgl	k1gInSc1	vajgl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
-dosud	osudo	k1gNnPc2	-dosudo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dřívější	dřívější	k2eAgFnPc1d1	dřívější
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Kaplan	Kaplan	k1gMnSc1	Kaplan
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
−	−	k?	−
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
−	−	k?	−
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Laurent	Laurent	k1gMnSc1	Laurent
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
−	−	k?	−
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Klein	Klein	k1gMnSc1	Klein
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
−	−	k?	−
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Chrastina	chrastina	k1gFnSc1	chrastina
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hauser	Hauser	k1gMnSc1	Hauser
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Chvalkovský	Chvalkovský	k2eAgMnSc1d1	Chvalkovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Petráš	Petráš	k1gMnSc1	Petráš
<g/>
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Ringo	Ringo	k1gMnSc1	Ringo
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Pacák	Pacák	k1gMnSc1	Pacák
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hejduk	Hejduk	k1gMnSc1	Hejduk
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Peroutka	Peroutka	k1gMnSc1	Peroutka
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Berka	Berka	k1gMnSc1	Berka
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Bobek	Bobek	k1gMnSc1	Bobek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miki	Miki	k1gNnSc1	Miki
Volek	Volek	k1gMnSc1	Volek
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Yvonne	Yvonnout	k5eAaImIp3nS	Yvonnout
Přenosilová	Přenosilová	k1gFnSc1	Přenosilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Růžek	Růžek	k1gMnSc1	Růžek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
–	–	k?	–
saxofon	saxofon	k1gInSc1	saxofon
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
−	−	k?	−
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
kapely	kapela	k1gFnSc2	kapela
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Klempíř	Klempíř	k1gMnSc1	Klempíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
–	–	k?	–
jonika	jonika	k1gFnSc1	jonika
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Smeták	Smeták	k1gMnSc1	Smeták
–	–	k?	–
kontrabas	kontrabas	k1gInSc1	kontrabas
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Laufer	Laufer	k1gMnSc1	Laufer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Záskoky	záskok	k1gInPc4	záskok
===	===	k?	===
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Kumžák	Kumžák	k1gMnSc1	Kumžák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
–	–	k?	–
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
záskok	záskok	k1gInSc1	záskok
během	během	k7c2	během
hospitalizace	hospitalizace	k1gFnSc2	hospitalizace
Jiřího	Jiří	k1gMnSc2	Jiří
Valenty	Valenta	k1gMnSc2	Valenta
</s>
</p>
<p>
<s>
==	==	k?	==
Časová	časový	k2eAgFnSc1d1	časová
osa	osa	k1gFnSc1	osa
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
==	==	k?	==
</s>
</p>
<p>
<s>
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
víc	hodně	k6eAd2	hodně
své	svůj	k3xOyFgFnSc2	svůj
lásky	láska	k1gFnSc2	láska
</s>
</p>
<p>
<s>
Želva	želva	k1gFnSc1	želva
</s>
</p>
<p>
<s>
Snad	snad	k9	snad
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
zavinil	zavinit	k5eAaPmAgMnS	zavinit
já	já	k3xPp1nSc1	já
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Mareš	Mareš	k1gMnSc1	Mareš
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Štaidl	Štaidl	k1gMnSc1	Štaidl
<g/>
:	:	kIx,	:
Hvězda	Hvězda	k1gMnSc1	Hvězda
na	na	k7c6	na
vrbě	vrba	k1gFnSc6	vrba
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Šváb	Šváb	k1gMnSc1	Šváb
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
dvanáctiletým	dvanáctiletý	k2eAgMnSc7d1	dvanáctiletý
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Starkou	starka	k1gFnSc7	starka
</s>
</p>
<p>
<s>
Slzy	slza	k1gFnPc1	slza
tvý	tvý	k?	tvý
mámy	máma	k1gFnSc2	máma
</s>
</p>
<p>
<s>
Jasná	jasný	k2eAgFnSc1d1	jasná
zpráva	zpráva	k1gFnSc1	zpráva
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
za	za	k7c4	za
mlada	mlado	k1gNnPc4	mlado
</s>
</p>
<p>
<s>
Dávno	dávno	k6eAd1	dávno
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologie	chronologie	k1gFnSc1	chronologie
nahrávek	nahrávka	k1gFnPc2	nahrávka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Želva	želva	k1gFnSc1	želva
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Pták	pták	k1gMnSc1	pták
Rosomák	rosomák	k1gMnSc1	rosomák
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
Jedeme	jet	k5eAaImIp1nP	jet
jedeme	jet	k5eAaImIp1nP	jet
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc4	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
Olympic	Olympice	k1gInPc2	Olympice
4	[number]	k4	4
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Marathón	Marathón	k1gInSc1	Marathón
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Prázdniny	prázdniny	k1gFnPc4	prázdniny
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Laboratoř	laboratoř	k1gFnSc1	laboratoř
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
/	/	kIx~	/
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Kanagom	kanagom	k1gInSc1	kanagom
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Bigbít	bigbít	k1gInSc1	bigbít
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Když	když	k8xS	když
ti	ty	k3xPp2nSc3	ty
svítí	svítit	k5eAaImIp3nS	svítit
zelená	zelený	k2eAgFnSc1d1	zelená
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Ó	ó	k0	ó
jé	jé	k0	jé
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Dávno	dávno	k1gNnSc1	dávno
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Brejle	brejle	k1gFnPc1	brejle
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Karavana	karavana	k1gFnSc1	karavana
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Dám	dát	k5eAaPmIp1nS	dát
si	se	k3xPyFc3	se
tě	ty	k3xPp2nSc4	ty
klonovat	klonovat	k5eAaImF	klonovat
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Trilogy	Triloga	k1gFnPc1	Triloga
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Znovunatočená	Znovunatočený	k2eAgFnSc1d1	Znovunatočený
alba	alba	k1gFnSc1	alba
Prázdniny	prázdniny	k1gFnPc1	prázdniny
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
Laboratoř	laboratoř	k1gFnSc1	laboratoř
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Sopka	sopka	k1gFnSc1	sopka
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Back	Back	k1gMnSc1	Back
To	to	k9	to
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nikdy	nikdy	k6eAd1	nikdy
nevydané	vydaný	k2eNgNnSc4d1	nevydané
album	album	k1gNnSc4	album
natočené	natočený	k2eAgNnSc4d1	natočené
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Souhvězdí	souhvězdí	k1gNnPc4	souhvězdí
šílenců	šílenec	k1gMnPc2	šílenec
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Souhvězdí	souhvězdí	k1gNnPc4	souhvězdí
drsňáků	drsňák	k1gMnPc2	drsňák
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
Souhvězdí	souhvězdí	k1gNnPc4	souhvězdí
romantiků	romantik	k1gMnPc2	romantik
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
Trilobit	trilobit	k1gMnSc1	trilobit
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	alba	k1gFnSc1	alba
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
===	===	k?	===
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Overhead	Overhead	k1gInSc1	Overhead
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Holidays	Holidays	k1gInSc4	Holidays
On	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
The	The	k1gMnSc1	The
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Laboratory	Laborator	k1gInPc4	Laborator
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Hidden	Hiddna	k1gFnPc2	Hiddna
In	In	k1gMnSc1	In
Your	Your	k1gMnSc1	Your
Mind	Mind	k1gMnSc1	Mind
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Živáky	Živák	k1gInPc4	Živák
===	===	k?	===
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
N	N	kA	N
<g/>
'	'	kIx"	'
<g/>
Roll	Roll	k1gMnSc1	Roll
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
znovu	znovu	k6eAd1	znovu
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Rokenrol	rokenrol	k1gInSc1	rokenrol
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Olympic	Olympice	k1gInPc2	Olympice
v	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Live	Live	k1gFnSc1	Live
–	–	k?	–
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
lítá	lítat	k5eAaImIp3nS	lítat
vzhůru	vzhůru	k6eAd1	vzhůru
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Unplugged	Unplugged	k1gInSc1	Unplugged
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Ondráš	Ondráš	k1gMnSc1	Ondráš
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Záznam	záznam	k1gInSc1	záznam
představení	představení	k1gNnSc1	představení
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Trilogy	Triloga	k1gFnSc2	Triloga
Tour	Toura	k1gFnPc2	Toura
Live	Liv	k1gFnSc2	Liv
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Audio	audio	k2eAgInSc1d1	audio
záznam	záznam	k1gInSc1	záznam
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
jen	jen	k9	jen
pro	pro	k7c4	pro
digitální	digitální	k2eAgInSc4d1	digitální
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Retro	retro	k1gNnSc1	retro
1	[number]	k4	1
Želva	želva	k1gFnSc1	želva
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Retro	retro	k1gNnSc1	retro
2	[number]	k4	2
Pták	pták	k1gMnSc1	pták
Rosomák	rosomák	k1gMnSc1	rosomák
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Retro	retro	k1gNnSc7	retro
3	[number]	k4	3
Jedeme	jet	k5eAaImIp1nP	jet
jedeme	jet	k5eAaImIp1nP	jet
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Retro	retro	k1gNnSc1	retro
4	[number]	k4	4
Olympic	Olympice	k1gFnPc2	Olympice
4	[number]	k4	4
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Retro	retro	k1gNnSc1	retro
5	[number]	k4	5
Marathón	Marathón	k1gInSc1	Marathón
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Retro	retro	k1gNnSc3	retro
6	[number]	k4	6
Trilogy	Trilog	k1gInPc4	Trilog
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Retro	retro	k1gNnSc1	retro
7	[number]	k4	7
Kanagom	kanagom	k1gInSc1	kanagom
<g/>
,	,	kIx,	,
Bigbít	bigbít	k1gInSc1	bigbít
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Retro	retro	k1gNnSc1	retro
8	[number]	k4	8
Když	když	k8xS	když
ti	ty	k3xPp2nSc3	ty
svítí	svítit	k5eAaImIp3nP	svítit
zelená	zelený	k2eAgNnPc1d1	zelené
<g/>
,	,	kIx,	,
Ó	ó	k0	ó
jé	jé	k0	jé
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Retro	retro	k1gNnSc1	retro
9	[number]	k4	9
Dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
Brejle	brejle	k1gFnPc1	brejle
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Retro	retro	k1gNnSc1	retro
10	[number]	k4	10
Karavana	karavana	k1gFnSc1	karavana
<g/>
,	,	kIx,	,
Dám	dát	k5eAaPmIp1nS	dát
si	se	k3xPyFc3	se
tě	ty	k3xPp2nSc4	ty
klonovat	klonovat	k5eAaImF	klonovat
<g/>
,	,	kIx,	,
Sopka	sopka	k1gFnSc1	sopka
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Výběry	výběr	k1gInPc4	výběr
===	===	k?	===
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
Handful	Handful	k1gInSc1	Handful
(	(	kIx(	(
<g/>
Artia	Artia	k1gFnSc1	Artia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Určeno	určit	k5eAaPmNgNnS	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
12	[number]	k4	12
nej	nej	k?	nej
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Akorát	akorát	k6eAd1	akorát
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
MC	MC	kA	MC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
25	[number]	k4	25
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Jako	jako	k8xC	jako
za	za	k7c4	za
mlada	mlad	k1gMnSc4	mlad
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nově	nově	k6eAd1	nově
natočené	natočený	k2eAgFnSc2d1	natočená
verze	verze	k1gFnSc2	verze
starých	starý	k2eAgInPc2d1	starý
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Na	na	k7c4	na
2	[number]	k4	2
LP	LP	kA	LP
méně	málo	k6eAd2	málo
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Balady	balada	k1gFnPc1	balada
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Singly	singl	k1gInPc4	singl
I	i	k8xC	i
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Vlak	vlak	k1gInSc1	vlak
co	co	k3yInSc4	co
nikde	nikde	k6eAd1	nikde
nestaví	stavit	k5eNaImIp3nS	stavit
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Petr	Petr	k1gMnSc1	Petr
Hejduk	Hejduk	k1gMnSc1	Hejduk
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
písniček	písnička	k1gFnPc2	písnička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pro	pro	k7c4	pro
Olympic	Olympice	k1gFnPc2	Olympice
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
bubeník	bubeník	k1gMnSc1	bubeník
Petr	Petr	k1gMnSc1	Petr
Hejduk	Hejduk	k1gMnSc1	Hejduk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Singly	singl	k1gInPc4	singl
I	i	k8xC	i
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Singly	singl	k1gInPc7	singl
II	II	kA	II
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Singly	singl	k1gInPc7	singl
III	III	kA	III
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Singly	singl	k1gInPc7	singl
IV	IV	kA	IV
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Singly	singl	k1gInPc7	singl
V	V	kA	V
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Singly	singl	k1gInPc7	singl
VI	VI	kA	VI
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Singly	singl	k1gInPc7	singl
VII	VII	kA	VII
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
...	...	k?	...
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
z	z	k7c2	z
Olympicu	Olympicus	k1gInSc2	Olympicus
1	[number]	k4	1
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nově	nově	k6eAd1	nově
natočené	natočený	k2eAgFnSc2d1	natočená
verze	verze	k1gFnSc2	verze
starých	starý	k2eAgInPc2d1	starý
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
...	...	k?	...
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
z	z	k7c2	z
Olympicu	Olympicus	k1gInSc2	Olympicus
2	[number]	k4	2
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nově	nově	k6eAd1	nově
natočené	natočený	k2eAgFnSc2d1	natočená
verze	verze	k1gFnSc2	verze
starých	starý	k2eAgInPc2d1	starý
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Stejskání	Stejskání	k1gNnSc1	Stejskání
(	(	kIx(	(
<g/>
BEST	BEST	kA	BEST
I.A.	I.A.	k1gMnSc1	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nově	nově	k6eAd1	nově
natočené	natočený	k2eAgFnSc2d1	natočená
verze	verze	k1gFnSc2	verze
starých	starý	k2eAgInPc2d1	starý
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
–	–	k?	–
43	[number]	k4	43
jasných	jasný	k2eAgFnPc2d1	jasná
hitových	hitový	k2eAgFnPc2d1	hitová
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Doplněná	doplněný	k2eAgFnSc1d1	doplněná
reedice	reedice	k1gFnSc1	reedice
supraphonského	supraphonský	k2eAgInSc2d1	supraphonský
výběru	výběr	k1gInSc2	výběr
z	z	k7c2	z
r.	r.	kA	r.
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Rarity	rarita	k1gFnPc1	rarita
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
boxu	box	k1gInSc2	box
Olympic	Olympice	k1gFnPc2	Olympice
komplet	komplet	k1gInSc1	komplet
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k9	jen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
digitálního	digitální	k2eAgInSc2d1	digitální
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
66	[number]	k4	66
nej	nej	k?	nej
+	+	kIx~	+
1	[number]	k4	1
</s>
</p>
<p>
<s>
===	===	k?	===
Boxy	box	k1gInPc4	box
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Olympic	Olympice	k1gInPc2	Olympice
komplet	komplet	k6eAd1	komplet
(	(	kIx(	(
<g/>
14	[number]	k4	14
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Všechna	všechen	k3xTgFnSc1	všechen
alba	alba	k1gFnSc1	alba
do	do	k7c2	do
r.	r.	kA	r.
1990	[number]	k4	1990
+	+	kIx~	+
živák	živák	k1gMnSc1	živák
Olympic	Olympic	k1gMnSc1	Olympic
v	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
+	+	kIx~	+
kompilace	kompilace	k1gFnSc1	kompilace
Rarity	rarita	k1gFnSc2	rarita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Olympic	Olympice	k1gFnPc2	Olympice
50	[number]	k4	50
–	–	k?	–
Hity	hit	k1gInPc1	hit
<g/>
,	,	kIx,	,
Singly	singl	k1gInPc1	singl
<g/>
,	,	kIx,	,
Rarity	rarita	k1gFnPc1	rarita
(	(	kIx(	(
<g/>
5	[number]	k4	5
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
Trilogie	trilogie	k1gFnSc1	trilogie
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
(	(	kIx(	(
<g/>
3	[number]	k4	3
LP	LP	kA	LP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
===	===	k?	===
</s>
</p>
<p>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Remasterovaná	Remasterovaný	k2eAgFnSc1d1	Remasterovaná
reedice	reedice	k1gFnSc1	reedice
všech	všecek	k3xTgFnPc2	všecek
alb	alba	k1gFnPc2	alba
do	do	k7c2	do
r.	r.	kA	r.
1990	[number]	k4	1990
s	s	k7c7	s
bonusy	bonus	k1gInPc7	bonus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Želva	želva	k1gFnSc1	želva
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Pták	pták	k1gMnSc1	pták
Rosomák	rosomák	k1gMnSc1	rosomák
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Zlatá	zlatá	k1gFnSc5	zlatá
edice	edice	k1gFnSc2	edice
–	–	k?	–
Jedeme	jet	k5eAaImIp1nP	jet
jedeme	jet	k5eAaImIp1nP	jet
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc4	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Olympic	Olympice	k1gFnPc2	Olympice
4	[number]	k4	4
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Marathón	Marathón	k1gInSc1	Marathón
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Prázdniny	prázdniny	k1gFnPc1	prázdniny
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Laboratoř	laboratoř	k1gFnSc1	laboratoř
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Kanagom	kanagom	k1gInSc1	kanagom
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Bigbít	bigbít	k1gInSc1	bigbít
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
Když	když	k8xS	když
ti	ty	k3xPp2nSc3	ty
svítí	svítit	k5eAaImIp3nS	svítit
zelená	zelený	k2eAgFnSc1d1	zelená
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
edice	edice	k1gFnSc1	edice
–	–	k?	–
O	O	kA	O
<g/>
,	,	kIx,	,
jé	jé	k0	jé
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Video	video	k1gNnSc1	video
===	===	k?	===
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Lucerna	lucerna	k1gFnSc1	lucerna
Live	Liv	k1gFnSc2	Liv
–	–	k?	–
VHS	VHS	kA	VHS
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Unplugged	Unplugged	k1gInSc1	Unplugged
–	–	k?	–
VHS	VHS	kA	VHS
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Prázdniny	prázdniny	k1gFnPc4	prázdniny
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
let	léto	k1gNnPc2	léto
Olympiku	Olympik	k1gInSc2	Olympik
–	–	k?	–
VHS	VHS	kA	VHS
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Ulice	ulice	k1gFnSc1	ulice
<g/>
/	/	kIx~	/
<g/>
Čtvrtstoletí	čtvrtstoletí	k1gNnSc1	čtvrtstoletí
Olympiku	Olympik	k1gInSc2	Olympik
–	–	k?	–
VHS	VHS	kA	VHS
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Olympic	Olympice	k1gInPc2	Olympice
slaví	slavit	k5eAaImIp3nS	slavit
40	[number]	k4	40
let	léto	k1gNnPc2	léto
–	–	k?	–
DVD	DVD	kA	DVD
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Olympic	Olympice	k1gInPc2	Olympice
1963	[number]	k4	1963
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
–	–	k?	–
DVD	DVD	kA	DVD
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Olympic	Olympice	k1gInPc2	Olympice
1973	[number]	k4	1973
<g/>
/	/	kIx~	/
<g/>
1983	[number]	k4	1983
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
DVD	DVD	kA	DVD
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Prázdniny	prázdniny	k1gFnPc4	prázdniny
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
Ulice	ulice	k1gFnPc4	ulice
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
DVD	DVD	kA	DVD
–	–	k?	–
BEST	BEST	kA	BEST
I.A.	I.A.	k1gFnSc2	I.A.
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Trilogy	Triloga	k1gFnSc2	Triloga
Tour	Toura	k1gFnPc2	Toura
Live	Liv	k1gFnSc2	Liv
–	–	k?	–
DVD	DVD	kA	DVD
–	–	k?	–
(	(	kIx(	(
<g/>
KPS	KPS	kA	KPS
Media	medium	k1gNnSc2	medium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnPc1d1	hudební
ceny	cena	k1gFnPc1	cena
==	==	k?	==
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Grammy	Gramma	k1gFnSc2	Gramma
–	–	k?	–
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Ceny	cena	k1gFnPc4	cena
Melodie	melodie	k1gFnSc2	melodie
–	–	k?	–
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
lyra	lyra	k1gFnSc1	lyra
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Olympic	Olympice	k1gFnPc2	Olympice
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Tůma	Tůma	k1gMnSc1	Tůma
<g/>
:	:	kIx,	:
Olympic	Olympic	k1gMnSc1	Olympic
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
hudebním	hudební	k2eAgInSc6d1	hudební
slovníku	slovník	k1gInSc6	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Český	český	k2eAgInSc4d1	český
hudební	hudební	k2eAgInSc4d1	hudební
slovník	slovník	k1gInSc4	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Bezr	Bezr	k1gMnSc1	Bezr
<g/>
:	:	kIx,	:
Projděte	projít	k5eAaPmRp2nP	projít
se	se	k3xPyFc4	se
zlatě	zlatě	k6eAd1	zlatě
reeditovanou	reeditovaný	k2eAgFnSc7d1	reeditovaná
historií	historie	k1gFnSc7	historie
skupiny	skupina	k1gFnSc2	skupina
Olympic	Olympice	k1gFnPc2	Olympice
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Jandou	Janda	k1gMnSc7	Janda
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
rockmag	rockmag	k1gInSc1	rockmag
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
</p>
