<s>
Olympic	Olympice	k1gFnPc2	Olympice
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Karkulka	Karkulka	k1gFnSc1	Karkulka
(	(	kIx(	(
<g/>
Karlínský	karlínský	k2eAgInSc1d1	karlínský
Kulturní	kulturní	k2eAgInSc1d1	kulturní
Kabaret	kabaret	k1gInSc1	kabaret
<g/>
)	)	kIx)	)
Miloslav	Miloslav	k1gMnSc1	Miloslav
Růžek	Růžek	k1gMnSc1	Růžek
<g/>
.	.	kIx.	.
</s>
