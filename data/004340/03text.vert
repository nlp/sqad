<s>
Petrolejové	petrolejový	k2eAgFnPc4d1	petrolejová
lampy	lampa	k1gFnPc4	lampa
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
režisérem	režisér	k1gMnSc7	režisér
Jurajem	Juraj	k1gMnSc7	Juraj
Herzem	Herz	k1gMnSc7	Herz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
psychologického	psychologický	k2eAgInSc2d1	psychologický
románu	román	k1gInSc2	román
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Havlíčka	Havlíček	k1gMnSc4	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
výběru	výběr	k1gInSc2	výběr
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
Filmový	filmový	k2eAgInSc4d1	filmový
festival	festival	k1gInSc4	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
z	z	k7c2	z
malého	malý	k2eAgNnSc2d1	malé
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
velmi	velmi	k6eAd1	velmi
živá	živý	k2eAgFnSc1d1	živá
dívka	dívka	k1gFnSc1	dívka
Štěpa	Štěpa	k1gFnSc1	Štěpa
(	(	kIx(	(
<g/>
Iva	Iva	k1gFnSc1	Iva
Janžurová	Janžurový	k2eAgFnSc1d1	Janžurová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
čekání	čekání	k1gNnSc6	čekání
na	na	k7c4	na
nápadníka	nápadník	k1gMnSc4	nápadník
vezme	vzít	k5eAaPmIp3nS	vzít
fešáka	fešák	k1gMnSc2	fešák
oficíra	oficír	k1gMnSc4	oficír
Pavla	Pavel	k1gMnSc4	Pavel
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Čepek	Čepek	k1gMnSc1	Čepek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
však	však	k9	však
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
je	být	k5eAaImIp3nS	být
nakažen	nakazit	k5eAaPmNgMnS	nakazit
syfilidou	syfilida	k1gFnSc7	syfilida
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
uspokojit	uspokojit	k5eAaPmF	uspokojit
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nS	trpět
depresemi	deprese	k1gFnPc7	deprese
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
progresivní	progresivní	k2eAgFnSc4d1	progresivní
paralýza	paralýza	k1gFnSc1	paralýza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
příběh	příběh	k1gInSc4	příběh
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Václava	Václav	k1gMnSc4	Václav
Šaška	Šašek	k1gMnSc4	Šašek
oproti	oproti	k7c3	oproti
románu	román	k1gInSc3	román
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
potlačení	potlačení	k1gNnSc1	potlačení
regionální	regionální	k2eAgFnSc2d1	regionální
roviny	rovina	k1gFnSc2	rovina
–	–	k?	–
o	o	k7c6	o
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nemluví	mluvit	k5eNaImIp3nS	mluvit
přímo	přímo	k6eAd1	přímo
jako	jako	k8xC	jako
o	o	k7c6	o
Jilemnici	Jilemnice	k1gFnSc6	Jilemnice
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podpořil	podpořit	k5eAaPmAgInS	podpořit
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotné	samotný	k2eAgNnSc1d1	samotné
natáčení	natáčení	k1gNnSc1	natáčení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
–	–	k?	–
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
epická	epický	k2eAgFnSc1d1	epická
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zredukována	zredukovat	k5eAaPmNgFnS	zredukovat
na	na	k7c4	na
naprosté	naprostý	k2eAgNnSc4d1	naprosté
minimum	minimum	k1gNnSc4	minimum
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
Iva	Iva	k1gFnSc1	Iva
Janžurová	Janžurový	k2eAgFnSc1d1	Janžurová
jistou	jistý	k2eAgFnSc7d1	jistá
volbou	volba	k1gFnSc7	volba
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
však	však	k9	však
jasno	jasno	k6eAd1	jasno
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
hlavní	hlavní	k2eAgFnSc2d1	hlavní
mužské	mužský	k2eAgFnSc2d1	mužská
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
Eduard	Eduard	k1gMnSc1	Eduard
Cupák	Cupák	k1gMnSc1	Cupák
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Čepkem	Čepek	k1gMnSc7	Čepek
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Petra	Petr	k1gMnSc2	Petr
Čepka	Čepek	k1gMnSc2	Čepek
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
navyklého	navyklý	k2eAgInSc2d1	navyklý
způsobu	způsob	k1gInSc2	způsob
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výrazné	výrazný	k2eAgFnSc3d1	výrazná
tváři	tvář	k1gFnSc3	tvář
nemusel	muset	k5eNaImAgInS	muset
příliš	příliš	k6eAd1	příliš
mimicky	mimicky	k6eAd1	mimicky
projevovat	projevovat	k5eAaImF	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Režisérovi	režisérův	k2eAgMnPc1d1	režisérův
dlouho	dlouho	k6eAd1	dlouho
trvalo	trvat	k5eAaImAgNnS	trvat
<g/>
,	,	kIx,	,
než	než	k8xS	než
mu	on	k3xPp3gMnSc3	on
vštípil	vštípit	k5eAaPmAgMnS	vštípit
svou	svůj	k3xOyFgFnSc4	svůj
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
roli	role	k1gFnSc4	role
Pavla	Pavel	k1gMnSc2	Pavel
Maliny	Malina	k1gMnSc2	Malina
<g/>
.	.	kIx.	.
</s>
<s>
Ivu	Iva	k1gFnSc4	Iva
Janžurovou	Janžurový	k2eAgFnSc4d1	Janžurová
znal	znát	k5eAaImAgMnS	znát
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
debutního	debutní	k2eAgInSc2d1	debutní
dlouhometrážního	dlouhometrážní	k2eAgInSc2d1	dlouhometrážní
filmu	film	k1gInSc2	film
Znamení	znamení	k1gNnSc2	znamení
Raka	rak	k1gMnSc2	rak
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokládal	pokládat	k5eAaImAgMnS	pokládat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
českou	český	k2eAgFnSc4d1	Česká
herečku	herečka	k1gFnSc4	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Plánoval	plánovat	k5eAaImAgMnS	plánovat
ji	on	k3xPp3gFnSc4	on
dosadit	dosadit	k5eAaPmF	dosadit
i	i	k9	i
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
zamýšleného	zamýšlený	k2eAgInSc2d1	zamýšlený
filmu	film	k1gInSc2	film
Morgiana	Morgiana	k1gFnSc1	Morgiana
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Herz	Herz	k1gMnSc1	Herz
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
nebyl	být	k5eNaImAgInS	být
na	na	k7c4	na
film	film	k1gInSc4	film
kladen	klást	k5eAaImNgInS	klást
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
finanční	finanční	k2eAgInSc1d1	finanční
ani	ani	k8xC	ani
časový	časový	k2eAgInSc1d1	časový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
ideální	ideální	k2eAgNnSc4d1	ideální
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
slunečného	slunečný	k2eAgNnSc2d1	slunečné
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
nepřicházelo	přicházet	k5eNaImAgNnS	přicházet
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
počáteční	počáteční	k2eAgFnSc1d1	počáteční
scéna	scéna	k1gFnSc1	scéna
v	v	k7c6	v
parku	park	k1gInSc6	park
točila	točit	k5eAaImAgFnS	točit
po	po	k7c6	po
týdnech	týden	k1gInPc6	týden
s	s	k7c7	s
umělým	umělý	k2eAgNnSc7d1	umělé
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
padalo	padat	k5eAaImAgNnS	padat
listí	listí	k1gNnSc1	listí
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Černoch	Černoch	k1gMnSc1	Černoch
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
tvorbu	tvorba	k1gFnSc4	tvorba
Juraje	Juraj	k1gInSc2	Juraj
Herze	Herze	k1gFnSc2	Herze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
svérázná	svérázný	k2eAgFnSc1d1	svérázná
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
pozitivně	pozitivně	k6eAd1	pozitivně
vnímaná	vnímaný	k2eAgFnSc1d1	vnímaná
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
slíbenou	slíbený	k2eAgFnSc4d1	slíbená
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
hrál	hrát	k5eAaImAgMnS	hrát
začínajícího	začínající	k2eAgMnSc4d1	začínající
zpěváka	zpěvák	k1gMnSc4	zpěvák
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
upíše	upsat	k5eAaPmIp3nS	upsat
ďáblu	ďábel	k1gMnSc3	ďábel
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
událostem	událost	k1gFnPc3	událost
po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
jaru	jar	k1gInSc6	jar
1968	[number]	k4	1968
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
normalizaci	normalizace	k1gFnSc6	normalizace
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Černoch	Černoch	k1gMnSc1	Černoch
pak	pak	k6eAd1	pak
dostal	dostat	k5eAaPmAgMnS	dostat
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
Synáčka	synáček	k1gMnSc4	synáček
<g/>
"	"	kIx"	"
v	v	k7c6	v
Petrolejových	petrolejový	k2eAgFnPc6d1	petrolejová
lampách	lampa	k1gFnPc6	lampa
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
zkušený	zkušený	k2eAgMnSc1d1	zkušený
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
režisérovi	režisér	k1gMnSc3	režisér
Herzovi	Herz	k1gMnSc6	Herz
nijak	nijak	k6eAd1	nijak
napřekáželo	napřekážet	k5eAaPmAgNnS	napřekážet
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Černoch	Černoch	k1gMnSc1	Černoch
se	se	k3xPyFc4	se
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
závěrečné	závěrečný	k2eAgInPc4d1	závěrečný
postsynchrony	postsynchron	k1gInPc4	postsynchron
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
filmu	film	k1gInSc2	film
jej	on	k3xPp3gMnSc4	on
mluví	mluvit	k5eAaImIp3nS	mluvit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Satoranský	Satoranský	k2eAgMnSc1d1	Satoranský
<g/>
.	.	kIx.	.
</s>
<s>
Scenárista	scenárista	k1gMnSc1	scenárista
Václav	Václav	k1gMnSc1	Václav
Šašek	Šašek	k1gMnSc1	Šašek
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
román	román	k1gInSc1	román
Petrolejové	petrolejový	k2eAgFnSc2d1	petrolejová
lampy	lampa	k1gFnSc2	lampa
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Havlíčka	Havlíček	k1gMnSc4	Havlíček
a	a	k8xC	a
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
schváleno	schválit	k5eAaPmNgNnS	schválit
jeho	jeho	k3xOp3gNnSc1	jeho
natáčení	natáčení	k1gNnSc1	natáčení
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
přečetl	přečíst	k5eAaPmAgMnS	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pochybách	pochyba	k1gFnPc6	pochyba
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
povede	povést	k5eAaPmIp3nS	povést
románovou	románový	k2eAgFnSc4d1	románová
předlohu	předloha	k1gFnSc4	předloha
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
vypustit	vypustit	k5eAaPmF	vypustit
několik	několik	k4yIc4	několik
motivů	motiv	k1gInPc2	motiv
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
text	text	k1gInSc1	text
pro	pro	k7c4	pro
filmové	filmový	k2eAgInPc4d1	filmový
účely	účel	k1gInPc4	účel
"	"	kIx"	"
<g/>
osekat	osekat	k5eAaPmF	osekat
až	až	k9	až
na	na	k7c4	na
dřeň	dřeň	k1gFnSc4	dřeň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
psal	psát	k5eAaImAgInS	psát
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
nedovedl	dovést	k5eNaPmAgMnS	dovést
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
(	(	kIx(	(
<g/>
Štěpu	Štěpa	k1gFnSc4	Štěpa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
románu	román	k1gInSc6	román
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
sice	sice	k8xC	sice
jako	jako	k8xS	jako
poněkud	poněkud	k6eAd1	poněkud
hřmotná	hřmotný	k2eAgFnSc1d1	hřmotná
a	a	k8xC	a
neohrabaná	ohrabaný	k2eNgFnSc1d1	neohrabaná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
povahou	povaha	k1gFnSc7	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herza	k1gFnPc2	Herza
román	román	k1gInSc1	román
zřejmě	zřejmě	k6eAd1	zřejmě
nečetl	číst	k5eNaImAgInS	číst
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
zatížen	zatížit	k5eAaPmNgInS	zatížit
fixní	fixní	k2eAgFnSc7d1	fixní
představou	představa	k1gFnSc7	představa
specifické	specifický	k2eAgFnPc1d1	specifická
role	role	k1gFnPc1	role
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
dosadil	dosadit	k5eAaPmAgInS	dosadit
Ivu	Iva	k1gFnSc4	Iva
Janžurovou	Janžurový	k2eAgFnSc4d1	Janžurová
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
volbu	volba	k1gFnSc4	volba
Václav	Václav	k1gMnSc1	Václav
Šašek	Šašek	k1gMnSc1	Šašek
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
<g/>
,	,	kIx,	,
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
herečku	herečka	k1gFnSc4	herečka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obsazením	obsazení	k1gNnSc7	obsazení
role	role	k1gFnSc2	role
hejtmana	hejtman	k1gMnSc4	hejtman
Maliny	malina	k1gFnSc2	malina
Petrem	Petr	k1gMnSc7	Petr
Čepkem	Čepek	k1gMnSc7	Čepek
se	se	k3xPyFc4	se
Šašek	Šašek	k1gMnSc1	Šašek
s	s	k7c7	s
Jurajem	Juraj	k1gMnSc7	Juraj
Herzem	Herz	k1gMnSc7	Herz
shodl	shodnout	k5eAaPmAgMnS	shodnout
a	a	k8xC	a
domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čepek	Čepek	k1gMnSc1	Čepek
zde	zde	k6eAd1	zde
předvedl	předvést	k5eAaPmAgMnS	předvést
vrcholný	vrcholný	k2eAgInSc4d1	vrcholný
výkon	výkon	k1gInSc4	výkon
své	svůj	k3xOyFgFnSc2	svůj
herecké	herecký	k2eAgFnSc2d1	herecká
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Šašek	Šašek	k1gMnSc1	Šašek
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ústřední	ústřední	k2eAgMnSc1d1	ústřední
dramaturg	dramaturg	k1gMnSc1	dramaturg
barrandovského	barrandovský	k2eAgNnSc2d1	Barrandovské
studia	studio	k1gNnSc2	studio
Ludvík	Ludvík	k1gMnSc1	Ludvík
Toman	Toman	k1gMnSc1	Toman
nechal	nechat	k5eAaPmAgMnS	nechat
z	z	k7c2	z
filmu	film	k1gInSc2	film
vystřihnout	vystřihnout	k5eAaPmF	vystřihnout
jednu	jeden	k4xCgFnSc4	jeden
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
velmi	velmi	k6eAd1	velmi
pěkná	pěkný	k2eAgFnSc1d1	pěkná
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
moment	moment	k1gInSc1	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Malina	Malina	k1gMnSc1	Malina
chce	chtít	k5eAaImIp3nS	chtít
pokusit	pokusit	k5eAaPmF	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skok	k1gInSc7	skok
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
a	a	k8xC	a
nenajde	najít	k5eNaPmIp3nS	najít
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Nalezne	naleznout	k5eAaPmIp3nS	naleznout
ho	on	k3xPp3gMnSc4	on
Štěpa	Štěpa	k1gFnSc1	Štěpa
a	a	k8xC	a
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
ho	on	k3xPp3gMnSc4	on
odnáší	odnášet	k5eAaImIp3nS	odnášet
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Nevyzpytatelný	vyzpytatelný	k2eNgMnSc1d1	nevyzpytatelný
Toman	Toman	k1gMnSc1	Toman
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dojemné	dojemný	k2eAgFnSc6d1	dojemná
scéně	scéna	k1gFnSc6	scéna
nepochopitelně	pochopitelně	k6eNd1	pochopitelně
viděl	vidět	k5eAaImAgMnS	vidět
sexuální	sexuální	k2eAgInSc4d1	sexuální
akt	akt	k1gInSc4	akt
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
ji	on	k3xPp3gFnSc4	on
vystřihnout	vystřihnout	k5eAaPmF	vystřihnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
promítnut	promítnut	k2eAgInSc1d1	promítnut
sovětské	sovětský	k2eAgFnSc3d1	sovětská
filmařské	filmařský	k2eAgFnSc3d1	filmařská
delegaci	delegace	k1gFnSc3	delegace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vedoucí	vedoucí	k1gFnSc1	vedoucí
ho	on	k3xPp3gMnSc4	on
očernila	očernit	k5eAaPmAgFnS	očernit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
problém	problém	k1gInSc1	problém
syfilis	syfilis	k1gFnSc1	syfilis
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
témat	téma	k1gNnPc2	téma
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Naprosto	naprosto	k6eAd1	naprosto
nepochopila	pochopit	k5eNaPmAgFnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
snímek	snímek	k1gInSc1	snímek
je	být	k5eAaImIp3nS	být
především	především	k9	především
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
jiném	jiný	k2eAgNnSc6d1	jiné
<g/>
.	.	kIx.	.
</s>
