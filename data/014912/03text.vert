<s>
Puchratka	puchratka	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
</s>
<s>
Puchratka	puchratka	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
nižší	nízký	k2eAgFnPc1d2
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Thallobionta	Thallobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
ruduchy	ruducha	k1gFnPc1
(	(	kIx(
<g/>
Rhodophyta	Rhodophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
Florideophycidae	Florideophycidae	k6eAd1
Řád	řád	k1gInSc1
</s>
<s>
Gigartinales	Gigartinales	k1gInSc1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Gigartinaceae	Gigartinaceae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
ChondrusStack	ChondrusStack	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Binomické	binomický	k2eAgFnPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Chondrus	Chondrus	k1gMnSc1
crispus	crispus	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Puchratka	puchratka	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Chondrus	Chondrus	k1gMnSc1
crispus	crispus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
irský	irský	k2eAgInSc1d1
mech	mech	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
mořské	mořský	k2eAgFnPc4d1
červené	červený	k2eAgFnPc4d1
řasy	řasa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
fialové	fialový	k2eAgFnSc2d1
po	po	k7c4
velmi	velmi	k6eAd1
světlou	světlý	k2eAgFnSc4d1
až	až	k8xS
skoro	skoro	k6eAd1
průsvitnou	průsvitný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Irský	irský	k2eAgInSc1d1
mech	mech	k1gInSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
drobná	drobný	k2eAgFnSc1d1
mořská	mořský	k2eAgFnSc1d1
řasa	řasa	k1gFnSc1
<g/>
,	,	kIx,
dorůstá	dorůstat	k5eAaImIp3nS
jen	jen	k9
asi	asi	k9
20	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrůstá	vyrůstat	k5eAaImIp3nS
ze	z	k7c2
základny	základna	k1gFnSc2
uchycené	uchycený	k2eAgFnSc2d1
na	na	k7c6
mořském	mořský	k2eAgNnSc6d1
dně	dno	k1gNnSc6
a	a	k8xC
její	její	k3xOp3gFnSc1
stélka	stélka	k1gFnSc1
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
větví	větvit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odnože	odnož	k1gInSc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
značně	značně	k6eAd1
lišit	lišit	k5eAaImF
tloušťkou	tloušťka	k1gFnSc7
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
i	i	k8xC
barvou	barva	k1gFnSc7
(	(	kIx(
<g/>
tmavě	tmavě	k6eAd1
zelená	zelený	k2eAgNnPc1d1
<g/>
,	,	kIx,
tmavě	tmavě	k6eAd1
červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
fialová	fialový	k2eAgFnSc1d1
<g/>
,	,	kIx,
hnědá	hnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nažloutlá	nažloutlý	k2eAgFnSc1d1
či	či	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usušená	usušený	k2eAgFnSc1d1
řasa	řasa	k1gFnSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
nažloutlá	nažloutlý	k2eAgFnSc1d1
a	a	k8xC
průsvitná	průsvitný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Irský	irský	k2eAgInSc1d1
mech	mech	k1gInSc1
je	být	k5eAaImIp3nS
hojně	hojně	k6eAd1
rozšířen	rozšířit	k5eAaPmNgInS
po	po	k7c6
celém	celý	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Irska	Irsko	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Islandu	Island	k1gInSc2
<g/>
,	,	kIx,
Faerských	Faerský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
západního	západní	k2eAgInSc2d1
Baltu	Balt	k1gInSc2
až	až	k9
ke	k	k7c3
Španělsku	Španělsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Kanady	Kanada	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nepotvrzené	potvrzený	k2eNgInPc4d1
nálezy	nález	k1gInPc4
pochází	pocházet	k5eAaImIp3nS
i	i	k9
z	z	k7c2
Kalifornie	Kalifornie	k1gFnSc2
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červený	červený	k2eAgInSc1d1
pigment	pigment	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
irskému	irský	k2eAgInSc3d1
mechu	mech	k1gInSc3
lépe	dobře	k6eAd2
zachytit	zachytit	k5eAaPmF
modrozelené	modrozelený	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
to	ten	k3xDgNnSc1
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
proniká	pronikat	k5eAaImIp3nS
do	do	k7c2
větších	veliký	k2eAgFnPc2d2
hloubek	hloubka	k1gFnPc2
(	(	kIx(
<g/>
200	#num#	k4
m	m	kA
pod	pod	k7c4
hladinu	hladina	k1gFnSc4
a	a	k8xC
hlouběji	hluboko	k6eAd2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
hloubce	hloubka	k1gFnSc6
mech	mech	k1gInSc1
hojně	hojně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Puchratka	puchratka	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
jako	jako	k9
zdroj	zdroj	k1gInSc1
karagenanu	karagenanout	k5eAaPmIp1nS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
látka	látka	k1gFnSc1
podobná	podobný	k2eAgFnSc1d1
agaru	agar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Želatinový	želatinový	k2eAgInSc1d1
extrakt	extrakt	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
doplněk	doplněk	k1gInSc1
stravy	strava	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
želé	želé	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
zahuštění	zahuštění	k1gNnSc6
a	a	k8xC
stabilizaci	stabilizace	k1gFnSc3
potravinových	potravinový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
(	(	kIx(
<g/>
šlehačka	šlehačka	k1gFnSc1
<g/>
,	,	kIx,
zmrzlina	zmrzlina	k1gFnSc1
<g/>
,	,	kIx,
masné	masný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
jako	jako	k9
emulgátor	emulgátor	k1gInSc1
v	v	k7c6
textilním	textilní	k2eAgInSc6d1
a	a	k8xC
farmaceutickém	farmaceutický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
potravinářství	potravinářství	k1gNnSc6
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
E	E	kA
<g/>
407	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Irsku	Irsko	k1gNnSc6
a	a	k8xC
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
Skotska	Skotsko	k1gNnSc2
se	se	k3xPyFc4
řasa	řasa	k1gFnSc1
uvaří	uvařit	k5eAaPmIp3nS
v	v	k7c6
mléce	mléko	k1gNnSc6
<g/>
,	,	kIx,
přecedí	přecedit	k5eAaPmIp3nS
<g/>
,	,	kIx,
osladí	osladit	k5eAaPmIp3nS
cukrem	cukr	k1gInSc7
a	a	k8xC
ochutí	ochuť	k1gFnSc7
vanilkou	vanilka	k1gFnSc7
<g/>
,	,	kIx,
skořicí	skořice	k1gFnSc7
<g/>
,	,	kIx,
whiskey	whiskey	k1gInPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
brandy	brandy	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Výsledný	výsledný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
je	být	k5eAaImIp3nS
želatinová	želatinový	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
podobná	podobný	k2eAgFnSc1d1
např.	např.	kA
dezertu	dezert	k1gInSc2
Panna	Panna	k1gFnSc1
cotta	cotta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Chondrus	Chondrus	k1gMnSc1
crispus	crispus	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
F.	F.	kA
Börgesen	Börgesen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botany	Botana	k1gFnSc2
of	of	k?
the	the	k?
Faröes	Faröes	k1gMnSc1
based	based	k1gMnSc1
upon	upon	k1gMnSc1
Danish	Danish	k1gMnSc1
investigations	investigationsa	k1gFnPc2
Part	parta	k1gFnPc2
II	II	kA
(	(	kIx(
<g/>
Copenhagen	Copenhagen	k1gInSc1
Reprint	reprint	k1gInSc1
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1903	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
6105	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Marine	Marin	k1gInSc5
Algae	Algae	k1gNnSc2
of	of	k?
the	the	k?
Faröes	Faröes	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
35	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Taylor	Taylor	k1gMnSc1
72	#num#	k4
<g/>
"	"	kIx"
<g/>
>	>	kIx)
<g/>
W.	W.	kA
R.	R.	kA
Taylor	Taylor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marine	Marin	k1gInSc5
Algae	Algae	k1gNnSc2
of	of	k?
the	the	k?
Northeastern	Northeastern	k1gMnSc1
Coast	Coast	k1gMnSc1
of	of	k?
North	North	k1gMnSc1
America	America	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Michigan	Michigan	k1gInSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Ann	Ann	k1gMnSc1
Arbor	Arbor	k1gMnSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
472	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4904	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Morton	Morton	k1gInSc1
<g/>
,	,	kIx,
O.	O.	kA
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marine	Marin	k1gInSc5
Algae	Algae	k1gFnSc3
of	of	k?
Northern	Northern	k1gNnSc4
Ireland	Irelando	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ulster	Ulster	k1gInSc1
Museum	museum	k1gNnSc4
ISBN	ISBN	kA
0	#num#	k4
900761	#num#	k4
28	#num#	k4
8	#num#	k4
<g/>
↑	↑	k?
name	nam	k1gInSc2
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
Roeck-Holtzhauer	Roeck-Holtzhauer	k1gInSc1
91	#num#	k4
<g/>
"	"	kIx"
<g/>
Roeck-Holtzhauer	Roeck-Holtzhauer	k1gInSc1
<g/>
,	,	kIx,
Y.	Y.	kA
<g/>
de	de	k?
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uses	Uses	k1gInSc1
of	of	k?
seaweeds	seaweeds	k1gInSc1
in	in	k?
Cosmetics	Cosmetics	k1gInSc1
<g/>
.	.	kIx.
in	in	k?
Guiry	Guira	k1gFnSc2
<g/>
,	,	kIx,
M.	M.	kA
<g/>
D.	D.	kA
and	and	k?
Blunden	Blundna	k1gFnPc2
<g/>
,	,	kIx,
G.	G.	kA
1991	#num#	k4
Seaweed	Seaweed	k1gInSc1
Resources	Resources	k1gInSc4
in	in	k?
Europe	Europ	k1gInSc5
<g/>
:	:	kIx,
Uses	Uses	k1gInSc1
and	and	k?
Potential	Potential	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
92947	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
↑	↑	k?
Stegenga	Stegeng	k1gMnSc4
<g/>
,	,	kIx,
H.	H.	kA
<g/>
,	,	kIx,
Bolton	Bolton	k1gInSc1
<g/>
,	,	kIx,
J.J.	J.J.	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Anderson	Anderson	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
J.	J.	kA
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seaweeds	Seaweeds	k1gInSc1
of	of	k?
the	the	k?
South	South	k1gMnSc1
African	African	k1gMnSc1
West	West	k1gMnSc1
Coast	Coast	k1gMnSc1
<g/>
.	.	kIx.
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hall	Hall	k1gMnSc1
<g/>
,	,	kIx,
A.V.	A.V.	k1gMnSc1
Bolus	bolus	k1gInSc1
Herbarium	Herbarium	k1gNnSc1
Number	Number	k1gInSc1
18	#num#	k4
Cape	capat	k5eAaImIp3nS
Town	Town	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7992	#num#	k4
<g/>
-	-	kIx~
<g/>
1793	#num#	k4
<g/>
-X	-X	k?
<g/>
↑	↑	k?
Feum	Feum	k1gMnSc1
à	à	k?
Feamainn	Feamainn	k1gMnSc1
(	(	kIx(
<g/>
DVD	DVD	kA
<g/>
,	,	kIx,
Scottish	Scottish	k1gInSc1
Gaelic	Gaelice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Comhairle	Comhairle	k1gNnSc1
nan	nan	k?
Eilean	Eilean	k1gMnSc1
Siar	Siar	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
|	|	kIx~
Gastronomie	gastronomie	k1gFnPc1
</s>
