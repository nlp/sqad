<s>
Mlada	Mlada	k1gFnSc1	Mlada
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
930	[number]	k4	930
<g/>
/	/	kIx~	/
<g/>
935	[number]	k4	935
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
994	[number]	k4	994
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
dcera	dcera	k1gFnSc1	dcera
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
choti	choť	k1gFnPc1	choť
Biagoty	Biagota	k1gFnSc2	Biagota
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
abatyše	abatyše	k1gFnSc1	abatyše
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Mladě	mladě	k6eAd1	mladě
<g/>
,	,	kIx,	,
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
vzdělané	vzdělaný	k2eAgFnSc3d1	vzdělaná
dámě	dáma	k1gFnSc3	dáma
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
Kosmas	Kosmas	k1gMnSc1	Kosmas
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
v	v	k7c6	v
svatém	svatý	k2eAgNnSc6d1	svaté
písmu	písmo	k1gNnSc6	písmo
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
<g/>
,	,	kIx,	,
křesťanskému	křesťanský	k2eAgNnSc3d1	křesťanské
náboženství	náboženství	k1gNnSc3	náboženství
oddaná	oddaný	k2eAgFnSc1d1	oddaná
a	a	k8xC	a
veškerou	veškerý	k3xTgFnSc7	veškerý
poctivostí	poctivost	k1gFnSc7	poctivost
mravů	mrav	k1gInPc2	mrav
ozdobená	ozdobený	k2eAgNnPc1d1	ozdobené
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sestry	sestra	k1gFnSc2	sestra
byla	být	k5eAaImAgFnS	být
předurčena	předurčit	k5eAaPmNgFnS	předurčit
pro	pro	k7c4	pro
církevní	církevní	k2eAgFnSc4d1	církevní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
965	[number]	k4	965
Mlada	Mlado	k1gNnSc2	Mlado
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
církevního	církevní	k2eAgNnSc2d1	církevní
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
Čech	Čechy	k1gFnPc2	Čechy
zřízením	zřízení	k1gNnSc7	zřízení
biskupství	biskupství	k1gNnPc2	biskupství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Opatskou	opatský	k2eAgFnSc4d1	opatská
berlu	berla	k1gFnSc4	berla
přijala	přijmout	k5eAaPmAgFnS	přijmout
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
967	[number]	k4	967
vrátila	vrátit	k5eAaPmAgFnS	vrátit
z	z	k7c2	z
duchovně-diplomatické	duchovněiplomatický	k2eAgFnSc2d1	duchovně-diplomatický
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
abatyší	abatyše	k1gFnSc7	abatyše
nově	nově	k6eAd1	nově
založeného	založený	k2eAgInSc2d1	založený
kláštera	klášter	k1gInSc2	klášter
benediktinek	benediktinka	k1gFnPc2	benediktinka
u	u	k7c2	u
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Maria	Mario	k1gMnSc2	Mario
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
po	po	k7c6	po
roce	rok	k1gInSc6	rok
983	[number]	k4	983
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pozdější	pozdní	k2eAgFnSc2d2	pozdější
tradice	tradice	k1gFnSc2	tradice
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
994	[number]	k4	994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
význam	význam	k1gInSc1	význam
založení	založení	k1gNnSc2	založení
biskupství	biskupství	k1gNnSc2	biskupství
byl	být	k5eAaImAgMnS	být
neocenitelný	ocenitelný	k2eNgMnSc1d1	neocenitelný
<g/>
.	.	kIx.	.
</s>
<s>
Mlada	Mlada	k1gFnSc1	Mlada
byla	být	k5eAaImAgFnS	být
poté	poté	k6eAd1	poté
uctívána	uctíván	k2eAgFnSc1d1	uctívána
jako	jako	k8xC	jako
světice	světice	k1gFnSc1	světice
a	a	k8xC	a
prohlášena	prohlášen	k2eAgFnSc1d1	prohlášena
za	za	k7c4	za
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
ostatky	ostatek	k1gInPc1	ostatek
leží	ležet	k5eAaImIp3nP	ležet
ve	v	k7c6	v
svatojiřském	svatojiřský	k2eAgInSc6d1	svatojiřský
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Emanuela	Emanuel	k1gMnSc2	Emanuel
Vlčka	Vlček	k1gMnSc2	Vlček
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc2	jejich
původ	původ	k1gInSc1	původ
není	být	k5eNaImIp3nS	být
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
BUBEN	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
M.	M.	kA	M.
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
;	;	kIx,	;
KUKLA	Kukla	k1gMnSc1	Kukla
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
A.	A.	kA	A.
<g/>
.	.	kIx.	.
</s>
<s>
Svatí	svatý	k1gMnPc1	svatý
spojují	spojovat	k5eAaImIp3nP	spojovat
národy	národ	k1gInPc4	národ
:	:	kIx,	:
portréty	portrét	k1gInPc4	portrét
evropských	evropský	k2eAgMnPc2d1	evropský
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
rozš	rozš	k1gInSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
195	[number]	k4	195
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85846	[number]	k4	85846
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
KUBÍN	Kubín	k1gMnSc1	Kubín
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
přemyslovských	přemyslovský	k2eAgInPc2d1	přemyslovský
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Katolická	katolický	k2eAgFnSc1d1	katolická
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
;	;	kIx,	;
Togga	Togga	k1gFnSc1	Togga
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
372	[number]	k4	372
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87258	[number]	k4	87258
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
:	:	kIx,	:
vstup	vstup	k1gInSc1	vstup
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
530	[number]	k4	530
<g/>
–	–	k?	–
<g/>
935	[number]	k4	935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
658	[number]	k4	658
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kosmova	Kosmův	k2eAgFnSc1d1	Kosmova
kronika	kronika	k1gFnSc1	kronika
I	i	k9	i
<g/>
.22	.22	k4	.22
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
též	též	k9	též
Jindřich	Jindřich	k1gMnSc1	Jindřich
Heimburský	Heimburský	k2eAgMnSc1d1	Heimburský
<g/>
,	,	kIx,	,
FRB	FRB	kA	FRB
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s.	s.	k?	s.
307	[number]	k4	307
<g/>
.	.	kIx.	.
anonymní	anonymní	k2eAgFnSc1d1	anonymní
sestra	sestra	k1gFnSc1	sestra
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
sv.	sv.	kA	sv.
Voršily	Voršila	k1gFnSc2	Voršila
–	–	k?	–
Blahoslavená	blahoslavený	k2eAgFnSc1d1	blahoslavená
Mlada	Mlada	k1gFnSc1	Mlada
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
abatyše	abatyše	k1gFnSc2	abatyše
u	u	k7c2	u
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Hradča-nech	Hradča	k1gInPc6	Hradča-n
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vročení	vročení	k1gNnSc2	vročení
-	-	kIx~	-
1912	[number]	k4	1912
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
Václava	Václav	k1gMnSc2	Václav
Hájka	Hájek	k1gMnSc2	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
Kronika	kronika	k1gFnSc1	kronika
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
Flajšhans	Flajšhans	k1gInSc1	Flajšhans
<g/>
,	,	kIx,	,
ČAVU	ČAVU	kA	ČAVU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
tom	ten	k3xDgNnSc6	ten
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ad	ad	k7c4	ad
a.	a.	k?	a.
967	[number]	k4	967
P.	P.	kA	P.
Vondruška	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
–	–	k?	–
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
<g/>
...	...	k?	...
I.	I.	kA	I.
–	–	k?	–
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Kalendarium	Kalendarium	k1gNnSc4	Kalendarium
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
–	–	k?	–
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
tom	ten	k3xDgNnSc6	ten
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
s.	s.	k?	s.
197	[number]	k4	197
<g/>
,	,	kIx,	,
215	[number]	k4	215
<g/>
,	,	kIx,	,
kalendářní	kalendářní	k2eAgNnSc1d1	kalendářní
zařazení	zařazení	k1gNnSc1	zařazení
in	in	k?	in
Kalendarium	Kalendarium	k1gNnSc1	Kalendarium
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
s.	s.	k?	s.
18	[number]	k4	18
<g/>
,	,	kIx,	,
166	[number]	k4	166
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
–	–	k?	–
Pečenková	Pečenkový	k2eAgFnSc1d1	Pečenková
<g/>
,	,	kIx,	,
Běla	Běla	k1gFnSc1	Běla
–	–	k?	–
Ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
Mlada	Mlada	k1gFnSc1	Mlada
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
,	,	kIx,	,
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
prvního	první	k4xOgInSc2	první
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
družina	družina	k1gFnSc1	družina
dam	dáma	k1gFnPc2	dáma
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
Birnbaumová	Birnbaumová	k1gFnSc1	Birnbaumová
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Alžběta	Alžběta	k1gFnSc1	Alžběta
–	–	k?	–
Dobrava	Dobrava	k1gFnSc1	Dobrava
a	a	k8xC	a
Mlada	Mlada	k1gFnSc1	Mlada
<g/>
,	,	kIx,	,
in	in	k?	in
Královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
kněžny	kněžna	k1gFnSc2	kněžna
a	a	k8xC	a
velké	velký	k2eAgFnSc2d1	velká
ženy	žena	k1gFnSc2	žena
české	český	k2eAgFnSc2d1	Česká
–	–	k?	–
ed	ed	k?	ed
<g/>
.	.	kIx.	.
prof.	prof.	kA	prof.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Karel	Karel	k1gMnSc1	Karel
Stloukal	Stloukal	k1gMnSc1	Stloukal
<g/>
,	,	kIx,	,
Jos	Jos	k1gMnSc1	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
s.	s.	k?	s.
55	[number]	k4	55
–	–	k?	–
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Ryneš	Rynat	k5eAaBmIp2nS	Rynat
V.	V.	kA	V.
Mlada	Mlada	k1gFnSc1	Mlada
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Kadlec	Kadlec	k1gMnSc1	Kadlec
J.	J.	kA	J.
Založení	založení	k1gNnSc4	založení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Roztoky	Roztoky	k1gInPc1	Roztoky
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Piťha	Piťha	k1gMnSc1	Piťha
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
–	–	k?	–
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
svatí	svatý	k2eAgMnPc1d1	svatý
<g/>
,	,	kIx,	,
AVED	AVED	kA	AVED
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
s.	s.	k?	s.
192	[number]	k4	192
Čechura	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
/	/	kIx~	/
Mikulec	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
/	/	kIx~	/
Stellner	Stellner	k1gMnSc1	Stellner
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
–	–	k?	–
Lexikon	lexikon	k1gNnSc4	lexikon
českých	český	k2eAgFnPc2d1	Česká
panovnických	panovnický	k2eAgFnPc2d1	panovnická
dynastií	dynastie	k1gFnPc2	dynastie
<g/>
,	,	kIx,	,
AKROPOLIS	Akropolis	k1gFnSc1	Akropolis
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
s.	s.	k?	s.
23	[number]	k4	23
<g/>
,	,	kIx,	,
110	[number]	k4	110
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
←	←	k?	←
<g/>
141	[number]	k4	141
<g/>
)	)	kIx)	)
Pracný	pracný	k2eAgMnSc1d1	pracný
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
–	–	k?	–
Český	český	k2eAgInSc4d1	český
kalendář	kalendář	k1gInSc4	kalendář
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
EWA	EWA	kA	EWA
EDITION	EDITION	kA	EDITION
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g />
.	.	kIx.	.
</s>
<s>
34	[number]	k4	34
–	–	k?	–
35	[number]	k4	35
Hrudníková	hrudníkový	k2eAgFnSc1d1	hrudníkový
OP	op	k1gMnSc1	op
<g/>
,	,	kIx,	,
Mirjam	Mirjam	k1gFnSc1	Mirjam
–	–	k?	–
Řeholní	řeholní	k2eAgInSc4d1	řeholní
život	život	k1gInSc4	život
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Kostelní	kostelní	k2eAgFnSc1d1	kostelní
Vydří	vydří	k2eAgFnSc1d1	vydří
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
s.	s.	k?	s.
35	[number]	k4	35
Ravik	Ravika	k1gFnPc2	Ravika
<g/>
,	,	kIx,	,
Slavomír	Slavomíra	k1gFnPc2	Slavomíra
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
REGIA	REGIA	kA	REGIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
s.	s.	k?	s.
551	[number]	k4	551
–	–	k?	–
553	[number]	k4	553
</s>
