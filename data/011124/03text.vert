<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Ludwik	Ludwik	k1gMnSc1	Ludwik
Łazarz	Łazarz	k1gMnSc1	Łazarz
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1859	[number]	k4	1859
Bělostok	Bělostok	k1gInSc1	Bělostok
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
oční	oční	k2eAgMnSc1d1	oční
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
umělého	umělý	k2eAgInSc2d1	umělý
jazyka	jazyk	k1gInSc2	jazyk
určeného	určený	k2eAgMnSc2d1	určený
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
pozadí	pozadí	k1gNnSc4	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1859	[number]	k4	1859
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
městě	město	k1gNnSc6	město
Bělostoku	Bělostok	k1gInSc2	Bělostok
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
součást	součást	k1gFnSc1	součást
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
křestní	křestní	k2eAgNnSc1d1	křestní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Eliezer	Eliezer	k1gInSc4	Eliezer
(	(	kIx(	(
<g/>
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
Lejzer	Lejzra	k1gFnPc2	Lejzra
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
Lazar	Lazar	k1gMnSc1	Lazar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Byl	být	k5eAaImAgInS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
22	[number]	k4	22
<g/>
letého	letý	k2eAgMnSc2d1	letý
učitele	učitel	k1gMnSc2	učitel
jazyků	jazyk	k1gInPc2	jazyk
Mordechaje	Mordechaje	k1gMnSc1	Mordechaje
(	(	kIx(	(
<g/>
Marka	Marek	k1gMnSc4	Marek
<g/>
)	)	kIx)	)
Zamenhofa	Zamenhof	k1gMnSc4	Zamenhof
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc4	jeho
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgFnSc2d2	mladší
manželky	manželka	k1gFnSc2	manželka
Liby	Liba	k1gFnSc2	Liba
Rochly	Rochly	k1gFnSc2	Rochly
(	(	kIx(	(
<g/>
Rozálie	Rozálie	k1gFnSc1	Rozálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
narodilo	narodit	k5eAaPmAgNnS	narodit
dalších	další	k2eAgInPc2d1	další
10	[number]	k4	10
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
2	[number]	k4	2
záhy	záhy	k6eAd1	záhy
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
rodný	rodný	k2eAgInSc4d1	rodný
jazyk	jazyk	k1gInSc4	jazyk
považoval	považovat	k5eAaImAgMnS	považovat
ruštinu	ruština	k1gFnSc4	ruština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mluvil	mluvit	k5eAaImAgMnS	mluvit
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
běloruštinu	běloruština	k1gFnSc4	běloruština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tehdy	tehdy	k6eAd1	tehdy
nebyla	být	k5eNaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
která	který	k3yQgFnSc1	který
možná	možná	k9	možná
měla	mít	k5eAaImAgFnS	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
fonologii	fonologie	k1gFnSc4	fonologie
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odmalička	odmalička	k6eAd1	odmalička
mluvil	mluvit	k5eAaImAgMnS	mluvit
ale	ale	k8xC	ale
také	také	k9	také
jidiš	jidiš	k6eAd1	jidiš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
častěji	často	k6eAd2	často
polsky	polsky	k6eAd1	polsky
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
později	pozdě	k6eAd2	pozdě
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
hovořil	hovořit	k5eAaImAgMnS	hovořit
plynně	plynně	k6eAd1	plynně
také	také	k9	také
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ne	ne	k9	ne
tak	tak	k6eAd1	tak
přirozeně	přirozeně	k6eAd1	přirozeně
jako	jako	k8xS	jako
jidiš	jidiš	k6eAd1	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
také	také	k9	také
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
italštinu	italština	k1gFnSc4	italština
<g/>
,	,	kIx,	,
španělštinu	španělština	k1gFnSc4	španělština
a	a	k8xC	a
litevštinu	litevština	k1gFnSc4	litevština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bělostoku	Bělostok	k1gInSc6	Bělostok
žily	žít	k5eAaImAgFnP	žít
kromě	kromě	k7c2	kromě
jidiš	jidiš	k6eAd1	jidiš
hovořící	hovořící	k2eAgFnSc2d1	hovořící
židovské	židovský	k2eAgFnSc2d1	židovská
většiny	většina	k1gFnSc2	většina
ještě	ještě	k9	ještě
další	další	k2eAgFnPc1d1	další
tři	tři	k4xCgFnPc1	tři
jazykové	jazykový	k2eAgFnPc1d1	jazyková
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
.	.	kIx.	.
</s>
<s>
Trápilo	trápit	k5eAaImAgNnS	trápit
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
musel	muset	k5eAaImAgMnS	muset
pozorovat	pozorovat	k5eAaImF	pozorovat
jejich	jejich	k3xOp3gFnPc4	jejich
časté	častý	k2eAgFnPc4d1	častá
vzájemné	vzájemný	k2eAgFnPc4d1	vzájemná
neshody	neshoda	k1gFnPc4	neshoda
<g/>
.	.	kIx.	.
</s>
<s>
Došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
a	a	k8xC	a
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
jsou	být	k5eAaImIp3nP	být
jazykové	jazykový	k2eAgFnPc4d1	jazyková
bariéry	bariéra	k1gFnPc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odstranit	odstranit	k5eAaPmF	odstranit
zavedením	zavedení	k1gNnSc7	zavedení
nového	nový	k2eAgInSc2d1	nový
společného	společný	k2eAgInSc2d1	společný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
hrál	hrát	k5eAaImAgInS	hrát
úlohu	úloha	k1gFnSc4	úloha
neutrálního	neutrální	k2eAgInSc2d1	neutrální
komunikačního	komunikační	k2eAgInSc2d1	komunikační
prostředku	prostředek	k1gInSc2	prostředek
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
různého	různý	k2eAgInSc2d1	různý
etnického	etnický	k2eAgInSc2d1	etnický
a	a	k8xC	a
jazykového	jazykový	k2eAgNnSc2d1	jazykové
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnPc1	práce
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
jazyce	jazyk	k1gInSc6	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
umělého	umělý	k2eAgInSc2d1	umělý
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
pokusil	pokusit	k5eAaPmAgMnS	pokusit
již	již	k6eAd1	již
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgFnSc4	první
koncepci	koncepce	k1gFnSc4	koncepce
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
"	"	kIx"	"
<g/>
Lingwe	Lingwe	k1gNnPc1	Lingwe
Uniwersala	Uniwersala	k1gFnPc2	Uniwersala
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
získal	získat	k5eAaPmAgMnS	získat
příznivce	příznivec	k1gMnPc4	příznivec
svého	své	k1gNnSc2	své
projektu	projekt	k1gInSc6	projekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dospělí	dospělí	k1gMnPc1	dospělí
reagovali	reagovat	k5eAaBmAgMnP	reagovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
vesměs	vesměs	k6eAd1	vesměs
s	s	k7c7	s
posměšky	posměšek	k1gInPc7	posměšek
<g/>
,	,	kIx,	,
či	či	k8xC	či
ho	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
přímo	přímo	k6eAd1	přímo
za	za	k7c4	za
blázna	blázen	k1gMnSc4	blázen
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
si	se	k3xPyFc3	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vynutil	vynutit	k5eAaPmAgInS	vynutit
slib	slib	k1gInSc1	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokud	dokud	k8xS	dokud
neukončí	ukončit	k5eNaPmIp3nS	ukončit
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgInSc3	svůj
projektu	projekt	k1gInSc3	projekt
věnovat	věnovat	k5eAaImF	věnovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Ludvík	Ludvík	k1gMnSc1	Ludvík
měl	mít	k5eAaImAgMnS	mít
primární	primární	k2eAgInSc4d1	primární
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
poslechl	poslechnout	k5eAaPmAgMnS	poslechnout
otcovo	otcův	k2eAgNnSc4d1	otcovo
přání	přání	k1gNnSc4	přání
zajistit	zajistit	k5eAaPmF	zajistit
si	se	k3xPyFc3	se
existenci	existence	k1gFnSc4	existence
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Židé	Žid	k1gMnPc1	Žid
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
carské	carský	k2eAgFnSc6d1	carská
říši	říš	k1gFnSc6	říš
směli	smět	k5eAaImAgMnP	smět
studovat	studovat	k5eAaImF	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
a	a	k8xC	a
přivydělávat	přivydělávat	k5eAaImF	přivydělávat
si	se	k3xPyFc3	se
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
trpěl	trpět	k5eAaImAgInS	trpět
zimou	zima	k1gFnSc7	zima
i	i	k9	i
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1881	[number]	k4	1881
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
atentátu	atentát	k1gInSc6	atentát
zabit	zabit	k2eAgMnSc1d1	zabit
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
nacionalistické	nacionalistický	k2eAgFnPc1d1	nacionalistická
nálady	nálada	k1gFnPc1	nálada
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
přiostřily	přiostřit	k5eAaPmAgFnP	přiostřit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
poté	poté	k6eAd1	poté
Ludvík	Ludvík	k1gMnSc1	Ludvík
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
prázdniny	prázdniny	k1gFnPc4	prázdniny
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzdory	navzdory	k7c3	navzdory
dohodě	dohoda	k1gFnSc3	dohoda
mezitím	mezitím	k6eAd1	mezitím
otec	otec	k1gMnSc1	otec
projekt	projekt	k1gInSc4	projekt
jeho	on	k3xPp3gInSc2	on
jazyka	jazyk	k1gInSc2	jazyk
spálil	spálit	k5eAaPmAgMnS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
materiál	materiál	k1gInSc1	materiál
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
židovského	židovský	k2eAgNnSc2d1	Židovské
proticarského	proticarský	k2eAgNnSc2d1	proticarský
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
těžká	těžký	k2eAgFnSc1d1	těžká
ztráta	ztráta	k1gFnSc1	ztráta
však	však	k8xC	však
Ludvíka	Ludvík	k1gMnSc4	Ludvík
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
snu	sen	k1gInSc2	sen
neodradila	odradit	k5eNaPmAgFnS	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následného	následný	k2eAgNnSc2d1	následné
studia	studio	k1gNnSc2	studio
medicíny	medicína	k1gFnSc2	medicína
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
tajně	tajně	k6eAd1	tajně
po	po	k7c6	po
paměti	paměť	k1gFnSc6	paměť
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
znovu	znovu	k6eAd1	znovu
zrekonstruoval	zrekonstruovat	k5eAaPmAgMnS	zrekonstruovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jej	on	k3xPp3gMnSc4	on
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
a	a	k8xC	a
prověřoval	prověřovat	k5eAaImAgMnS	prověřovat
na	na	k7c6	na
překladech	překlad	k1gInPc6	překlad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1886	[number]	k4	1886
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
s	s	k7c7	s
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgFnSc4d2	mladší
Kejlou	Kejlá	k1gFnSc4	Kejlá
(	(	kIx(	(
<g/>
Klarou	Klara	k1gFnSc7	Klara
<g/>
)	)	kIx)	)
Silbernik	Silbernik	k1gInSc1	Silbernik
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
obchodníka	obchodník	k1gMnSc2	obchodník
z	z	k7c2	z
litevského	litevský	k2eAgInSc2d1	litevský
Kaunasu	Kaunas	k1gInSc2	Kaunas
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
pobývala	pobývat	k5eAaImAgFnS	pobývat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
spřátelili	spřátelit	k5eAaPmAgMnP	spřátelit
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
svěřil	svěřit	k5eAaPmAgInS	svěřit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
kupodivu	kupodivu	k6eAd1	kupodivu
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
podporou	podpora	k1gFnSc7	podpora
a	a	k8xC	a
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
oznámili	oznámit	k5eAaPmAgMnP	oznámit
své	svůj	k3xOyFgNnSc4	svůj
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
učebnici	učebnice	k1gFnSc4	učebnice
svého	svůj	k3xOyFgInSc2	svůj
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
jazyka	jazyk	k1gInSc2	jazyk
Ludvík	Ludvík	k1gMnSc1	Ludvík
obtížně	obtížně	k6eAd1	obtížně
hledal	hledat	k5eAaImAgMnS	hledat
vydavatele	vydavatel	k1gMnPc4	vydavatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
byl	být	k5eAaImAgMnS	být
odmítnut	odmítnut	k2eAgMnSc1d1	odmítnut
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
ruské	ruský	k2eAgNnSc4d1	ruské
vydání	vydání	k1gNnSc4	vydání
souhlas	souhlas	k1gInSc4	souhlas
cenzora	cenzor	k1gMnSc2	cenzor
–	–	k?	–
naštěstí	naštěstí	k6eAd1	naštěstí
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgInSc1d1	známý
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
považoval	považovat	k5eAaImAgMnS	považovat
dílko	dílko	k1gNnSc4	dílko
za	za	k7c4	za
neškodnou	škodný	k2eNgFnSc4d1	neškodná
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
vydat	vydat	k5eAaPmF	vydat
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1887	[number]	k4	1887
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Kelter	Keltra	k1gFnPc2	Keltra
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Doufající	doufající	k2eAgFnSc1d1	doufající
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
učebnice	učebnice	k1gFnSc2	učebnice
zněl	znět	k5eAaImAgInS	znět
"	"	kIx"	"
<g/>
Unua	Unuum	k1gNnPc1	Unuum
libro	libra	k1gFnSc5	libra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
jazykový	jazykový	k2eAgInSc1d1	jazykový
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
esperanto	esperanto	k1gNnSc1	esperanto
<g/>
"	"	kIx"	"
přešel	přejít	k5eAaPmAgInS	přejít
z	z	k7c2	z
pseudonymu	pseudonym	k1gInSc2	pseudonym
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
této	tento	k3xDgFnSc2	tento
učebnice	učebnice	k1gFnSc2	učebnice
udělal	udělat	k5eAaPmAgMnS	udělat
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
několik	několik	k4yIc4	několik
překladů	překlad	k1gInPc2	překlad
světové	světový	k2eAgFnSc2d1	světová
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jazyk	jazyk	k1gInSc4	jazyk
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
využívat	využívat	k5eAaImF	využívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
mohla	moct	k5eAaImAgFnS	moct
vyjít	vyjít	k5eAaPmF	vyjít
především	především	k9	především
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
jeho	on	k3xPp3gMnSc2	on
tchána	tchán	k1gMnSc2	tchán
a	a	k8xC	a
snoubenky	snoubenka	k1gFnSc2	snoubenka
Kláry	Klára	k1gFnSc2	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
padla	padnout	k5eAaImAgFnS	padnout
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jejího	její	k3xOp3gNnSc2	její
věna	věno	k1gNnSc2	věno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
verzi	verze	k1gFnSc6	verze
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyšla	vyjít	k5eAaPmAgFnS	vyjít
učebnice	učebnice	k1gFnPc4	učebnice
i	i	k8xC	i
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
polské	polský	k2eAgFnSc2d1	polská
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
veškerých	veškerý	k3xTgNnPc2	veškerý
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
tvůrce	tvůrce	k1gMnSc4	tvůrce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
iniciátora	iniciátor	k1gMnSc4	iniciátor
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
během	během	k7c2	během
roku	rok	k1gInSc2	rok
posílali	posílat	k5eAaImAgMnP	posílat
své	svůj	k3xOyFgMnPc4	svůj
kritiky	kritik	k1gMnPc4	kritik
a	a	k8xC	a
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
případné	případný	k2eAgFnPc4d1	případná
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
konečnou	konečný	k2eAgFnSc4d1	konečná
podobu	podoba	k1gFnSc4	podoba
základů	základ	k1gInPc2	základ
jazyka	jazyk	k1gMnSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
náhlém	náhlý	k2eAgInSc6d1	náhlý
rozpadu	rozpad	k1gInSc6	rozpad
slibně	slibně	k6eAd1	slibně
se	se	k3xPyFc4	se
rozvíjejícího	rozvíjející	k2eAgNnSc2d1	rozvíjející
hnutí	hnutí	k1gNnSc2	hnutí
kolem	kolem	k7c2	kolem
jazyka	jazyk	k1gInSc2	jazyk
Volapük	volapük	k1gInSc4	volapük
nebylo	být	k5eNaImAgNnS	být
celkové	celkový	k2eAgNnSc1d1	celkové
klima	klima	k1gNnSc1	klima
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
nového	nový	k2eAgInSc2d1	nový
jazyka	jazyk	k1gInSc2	jazyk
příznivé	příznivý	k2eAgFnPc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Ludvík	Ludvík	k1gMnSc1	Ludvík
očekával	očekávat	k5eAaImAgMnS	očekávat
opravdu	opravdu	k6eAd1	opravdu
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
napětím	napětí	k1gNnSc7	napětí
první	první	k4xOgFnSc2	první
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ty	ten	k3xDgFnPc1	ten
přišly	přijít	k5eAaPmAgFnP	přijít
záhy	záhy	k6eAd1	záhy
a	a	k8xC	a
v	v	k7c6	v
překvapivě	překvapivě	k6eAd1	překvapivě
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
vesměs	vesměs	k6eAd1	vesměs
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Povzbuzen	povzbuzen	k2eAgMnSc1d1	povzbuzen
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
již	již	k6eAd1	již
v	v	k7c6	v
r.	r.	kA	r.
1888	[number]	k4	1888
své	svůj	k3xOyFgFnSc2	svůj
odpovědi	odpověď	k1gFnSc2	odpověď
na	na	k7c4	na
došlé	došlý	k2eAgInPc4d1	došlý
dopisy	dopis	k1gInPc4	dopis
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
publikace	publikace	k1gFnSc2	publikace
"	"	kIx"	"
<g/>
Dua	duo	k1gNnSc2	duo
Libro	libra	k1gFnSc5	libra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
vytvořením	vytvoření	k1gNnSc7	vytvoření
esperanta	esperanto	k1gNnSc2	esperanto
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
práce	práce	k1gFnPc4	práce
filozofů	filozof	k1gMnPc2	filozof
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
či	či	k8xC	či
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
jazyku	jazyk	k1gInSc6	jazyk
věnoval	věnovat	k5eAaImAgInS	věnovat
spis	spis	k1gInSc1	spis
Panglottia	Panglottium	k1gNnSc2	Panglottium
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k6eAd1	málo
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jméno	jméno	k1gNnSc1	jméno
Ludvík	Ludvík	k1gMnSc1	Ludvík
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
až	až	k9	až
v	v	k7c6	v
17	[number]	k4	17
letech	let	k1gInPc6	let
jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
tzv.	tzv.	kA	tzv.
gójské	gójský	k2eAgNnSc1d1	gójský
jméno	jméno	k1gNnSc1	jméno
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
anglického	anglický	k2eAgMnSc2d1	anglický
myslitele	myslitel	k1gMnSc2	myslitel
a	a	k8xC	a
lingvisty	lingvista	k1gMnSc2	lingvista
Francise	Francise	k1gFnSc1	Francise
Lodwicka	Lodwicka	k1gFnSc1	Lodwicka
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
-	-	kIx~	-
<g/>
1694	[number]	k4	1694
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
dočetl	dočíst	k5eAaPmAgMnS	dočíst
právě	právě	k9	právě
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgInS	být
mj.	mj.	kA	mj.
autorem	autor	k1gMnSc7	autor
univerzální	univerzální	k2eAgFnSc2d1	univerzální
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
plánového	plánový	k2eAgInSc2d1	plánový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
Celou	celý	k2eAgFnSc7d1	celá
svojí	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,	,	kIx,	,
cvičebnic	cvičebnice	k1gFnPc2	cvičebnice
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
nutných	nutný	k2eAgNnPc2d1	nutné
pro	pro	k7c4	pro
učení	učení	k1gNnSc4	učení
se	se	k3xPyFc4	se
novému	nový	k2eAgInSc3d1	nový
jazyku	jazyk	k1gInSc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
důležité	důležitý	k2eAgFnSc2d1	důležitá
považoval	považovat	k5eAaImAgMnS	považovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jazyk	jazyk	k1gInSc1	jazyk
byl	být	k5eAaImAgInS	být
spjat	spjat	k2eAgInSc1d1	spjat
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
psal	psát	k5eAaImAgInS	psát
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnSc1d1	literární
hodnota	hodnota	k1gFnSc1	hodnota
těchto	tento	k3xDgFnPc2	tento
básní	báseň	k1gFnPc2	báseň
není	být	k5eNaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
popularizační	popularizační	k2eAgFnSc1d1	popularizační
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k8xS	jako
redaktor	redaktor	k1gMnSc1	redaktor
prvního	první	k4xOgInSc2	první
esperantského	esperantský	k2eAgInSc2d1	esperantský
časopisu	časopis	k1gInSc2	časopis
La	la	k1gNnSc2	la
Esperantisto	esperantista	k1gMnSc5	esperantista
a	a	k8xC	a
přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
La	la	k1gNnSc2	la
Revuo	Revuo	k1gNnSc1	Revuo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
Zamenhofovy	Zamenhofův	k2eAgInPc1d1	Zamenhofův
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
pojednání	pojednání	k1gNnSc1	pojednání
<g/>
,	,	kIx,	,
předmluvy	předmluva	k1gFnPc1	předmluva
k	k	k7c3	k
různým	různý	k2eAgNnPc3d1	různé
dílům	dílo	k1gNnPc3	dílo
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc7	dopis
aj.	aj.	kA	aj.
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
sbírce	sbírka	k1gFnSc6	sbírka
Originala	Originala	k1gFnSc2	Originala
Verkaro	Verkara	k1gFnSc5	Verkara
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnSc1d1	původní
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filologická	filologický	k2eAgFnSc1d1	filologická
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Meždunarodnyj	Meždunarodnyj	k1gInSc1	Meždunarodnyj
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
mezi	mezi	k7c4	mezi
esperantisty	esperantista	k1gMnPc4	esperantista
běžně	běžně	k6eAd1	běžně
zvaná	zvaný	k2eAgNnPc1d1	zvané
Unua	Unuum	k1gNnPc1	Unuum
Libro	libra	k1gFnSc5	libra
(	(	kIx(	(
<g/>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
učebnici	učebnice	k1gFnSc4	učebnice
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
učebnici	učebnice	k1gFnSc4	učebnice
vydal	vydat	k5eAaPmAgMnS	vydat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
Doktor	doktor	k1gMnSc1	doktor
Doufající	doufající	k2eAgMnSc1d1	doufající
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
čtyřicetistránkovou	čtyřicetistránkový	k2eAgFnSc4d1	čtyřicetistránková
brožuru	brožura	k1gFnSc4	brožura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
předmluvu	předmluva	k1gFnSc4	předmluva
<g/>
,	,	kIx,	,
šestnáct	šestnáct	k4xCc4	šestnáct
mluvnických	mluvnický	k2eAgNnPc2d1	mluvnické
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
cvičebnici	cvičebnice	k1gFnSc4	cvičebnice
a	a	k8xC	a
univerzální	univerzální	k2eAgInSc4d1	univerzální
slovník	slovník	k1gInSc4	slovník
s	s	k7c7	s
917	[number]	k4	917
kmeny	kmen	k1gInPc7	kmen
a	a	k8xC	a
několik	několik	k4yIc4	několik
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
polské	polský	k2eAgNnSc1d1	polské
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnPc1d1	německá
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnPc1d1	francouzská
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydání	vydání	k1gNnSc1	vydání
anglické	anglický	k2eAgNnSc1d1	anglické
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dua	duo	k1gNnPc1	duo
Libro	libra	k1gFnSc5	libra
(	(	kIx(	(
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fundamenta	Fundamenta	k1gFnSc1	Fundamenta
Krestomatio	Krestomatio	k6eAd1	Krestomatio
(	(	kIx(	(
<g/>
Základní	základní	k2eAgFnSc1d1	základní
čítanka	čítanka	k1gFnSc1	čítanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
různé	různý	k2eAgInPc4d1	různý
slohy	sloh	k1gInPc4	sloh
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
vývoj	vývoj	k1gInSc4	vývoj
jazyka	jazyk	k1gInSc2	jazyk
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
legendy	legenda	k1gFnPc4	legenda
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgFnPc4d1	vědecká
a	a	k8xC	a
populárně	populárně	k6eAd1	populárně
vědecké	vědecký	k2eAgInPc4d1	vědecký
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
pojednání	pojednání	k1gNnSc1	pojednání
aj.	aj.	kA	aj.
Všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
od	od	k7c2	od
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
samotného	samotný	k2eAgMnSc2d1	samotný
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
zkontrolovány	zkontrolován	k2eAgInPc4d1	zkontrolován
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
esej	esej	k1gFnSc1	esej
Esenco	Esenco	k6eAd1	Esenco
Kaj	kát	k5eAaImRp2nS	kát
Estonteco	Estonteco	k1gNnSc4	Estonteco
de	de	k?	de
la	la	k1gNnSc2	la
Ideo	idea	k1gFnSc5	idea
de	de	k?	de
la	la	k1gNnSc7	la
Lingvo	Lingvo	k1gNnSc4	Lingvo
Internacia	Internacia	k1gFnSc1	Internacia
(	(	kIx(	(
<g/>
Podstata	podstata	k1gFnSc1	podstata
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
ideje	idea	k1gFnSc2	idea
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fundamento	Fundamento	k1gNnSc1	Fundamento
de	de	k?	de
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
Základ	základ	k1gInSc1	základ
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filologické	filologický	k2eAgNnSc1d1	filologické
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
mluvnice	mluvnice	k1gFnSc1	mluvnice
<g/>
,	,	kIx,	,
cvičebnice	cvičebnice	k1gFnSc1	cvičebnice
a	a	k8xC	a
univerzální	univerzální	k2eAgInSc1d1	univerzální
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
závazné	závazný	k2eAgNnSc1d1	závazné
jak	jak	k8xC	jak
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
esperantisty	esperantista	k1gMnPc4	esperantista
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
nelze	lze	k6eNd1	lze
nic	nic	k3yNnSc1	nic
škrtat	škrtat	k5eAaImF	škrtat
ani	ani	k8xC	ani
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
přidávat	přidávat	k5eAaImF	přidávat
a	a	k8xC	a
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
toto	tento	k3xDgNnSc4	tento
kodifikační	kodifikační	k2eAgNnSc4d1	kodifikační
dílo	dílo	k1gNnSc4	dílo
další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lingvaj	Lingvaj	k1gInSc1	Lingvaj
Respondoj	Respondoj	k1gInSc1	Respondoj
(	(	kIx(	(
<g/>
Jazykové	jazykový	k2eAgFnSc2d1	jazyková
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rady	rada	k1gFnPc4	rada
o	o	k7c6	o
správném	správný	k2eAgNnSc6d1	správné
užívání	užívání	k1gNnSc6	užívání
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proverbaro	Proverbara	k1gFnSc5	Proverbara
Esperanta	esperanto	k1gNnPc1	esperanto
(	(	kIx(	(
<g/>
Esperantská	esperantský	k2eAgNnPc1d1	esperantské
přísloví	přísloví	k1gNnPc1	přísloví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vortoj	Vortoj	k1gInSc1	Vortoj
de	de	k?	de
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
(	(	kIx(	(
<g/>
Slova	slovo	k1gNnSc2	slovo
Zamenhofova	Zamenhofův	k2eAgNnSc2d1	Zamenhofův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Zamenhofovy	Zamenhofův	k2eAgInPc4d1	Zamenhofův
kongresové	kongresový	k2eAgInPc4d1	kongresový
projevy	projev	k1gInPc4	projev
</s>
</p>
<p>
<s>
Originala	Originat	k5eAaPmAgFnS	Originat
Verkaro	Verkara	k1gFnSc5	Verkara
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnSc1d1	původní
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sebrané	sebraný	k2eAgInPc1d1	sebraný
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
předmluvy	předmluva	k1gFnPc1	předmluva
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
atp.	atp.	kA	atp.
Toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
redigoval	redigovat	k5eAaImAgMnS	redigovat
redakce	redakce	k1gFnSc2	redakce
prof.	prof.	kA	prof.
J.	J.	kA	J.
Dietterle	Dietterle	k1gFnSc1	Dietterle
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
vyjmenovaných	vyjmenovaný	k2eAgNnPc2d1	vyjmenované
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
autorem	autor	k1gMnSc7	autor
či	či	k8xC	či
spoluautorem	spoluautor	k1gMnSc7	spoluautor
několika	několik	k4yIc2	několik
slovníků	slovník	k1gInPc2	slovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poezie	poezie	k1gFnSc2	poezie
===	===	k?	===
</s>
</p>
<p>
<s>
Mia	Mia	k?	Mia
Penso	Pensa	k1gFnSc5	Pensa
(	(	kIx(	(
<g/>
Má	můj	k3xOp1gFnSc1	můj
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ho	ho	k0	ho
<g/>
,	,	kIx,	,
Mia	Mia	k1gMnSc1	Mia
Kor	Kor	k1gMnSc1	Kor
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc4	srdce
moje	můj	k3xOp1gNnSc4	můj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Espero	Espero	k1gNnSc1	Espero
(	(	kIx(	(
<g/>
Naděje	naděje	k1gFnSc1	naděje
–	–	k?	–
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
hymnou	hymna	k1gFnSc7	hymna
esperantistů	esperantista	k1gMnPc2	esperantista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Vojo	Vojo	k1gNnSc1	Vojo
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Al	ala	k1gFnPc2	ala
la	la	k1gNnSc2	la
Fratoj	Fratoj	k1gInSc4	Fratoj
(	(	kIx(	(
<g/>
Bratřím	bratřit	k5eAaImIp1nS	bratřit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Preĝ	Preĝ	k?	Preĝ
sub	sub	k7c4	sub
la	la	k1gNnSc4	la
Verda	Verdo	k1gNnSc2	Verdo
Standardo	Standardo	k1gNnSc1	Standardo
(	(	kIx(	(
<g/>
Modlitba	modlitba	k1gFnSc1	modlitba
pod	pod	k7c7	pod
zeleným	zelený	k2eAgInSc7d1	zelený
praporem	prapor	k1gInSc7	prapor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pluvo	Pluvo	k1gNnSc1	Pluvo
(	(	kIx(	(
<g/>
Déšť	déšť	k1gInSc1	déšť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc4	překlad
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
jeho	jeho	k3xOp3gInPc4	jeho
překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právě	právě	k9	právě
díky	díky	k7c3	díky
překladům	překlad	k1gInPc3	překlad
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
jazyk	jazyk	k1gInSc4	jazyk
rozvinout	rozvinout	k5eAaPmF	rozvinout
<g/>
,	,	kIx,	,
rozšířit	rozšířit	k5eAaPmF	rozšířit
jeho	jeho	k3xOp3gFnSc4	jeho
pružnost	pružnost	k1gFnSc4	pružnost
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
tvořit	tvořit	k5eAaImF	tvořit
první	první	k4xOgInPc4	první
obraty	obrat	k1gInPc4	obrat
<g/>
,	,	kIx,	,
metafory	metafora	k1gFnPc4	metafora
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Přeložil	přeložit	k5eAaPmAgInS	přeložit
řadu	řada	k1gFnSc4	řada
děl	dělo	k1gNnPc2	dělo
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
překlad	překlad	k1gInSc4	překlad
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Malnova	Malnov	k1gInSc2	Malnov
Testamento	Testamento	k1gNnSc1	Testamento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
jím	on	k3xPp3gMnSc7	on
přeloženým	přeložený	k2eAgNnPc3d1	přeložené
významným	významný	k2eAgNnPc3d1	významné
dílům	dílo	k1gNnPc3	dílo
patří	patřit	k5eAaImIp3nS	patřit
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
Hamleto	Hamlet	k2eAgNnSc1d1	Hamlet
<g/>
)	)	kIx)	)
od	od	k7c2	od
W.	W.	kA	W.
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
Dickensův	Dickensův	k2eAgInSc1d1	Dickensův
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Batalo	Batala	k1gFnSc5	Batala
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Vivo	vivo	k6eAd1	vivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
klasiky	klasika	k1gFnSc2	klasika
pak	pak	k6eAd1	pak
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Goethovo	Goethův	k2eAgNnSc4d1	Goethovo
dílo	dílo	k1gNnSc4	dílo
Ifigenie	Ifigenie	k1gFnSc2	Ifigenie
v	v	k7c6	v
Tauridě	Taurida	k1gFnSc6	Taurida
(	(	kIx(	(
<g/>
Ifigenio	Ifigenio	k1gNnSc1	Ifigenio
En	En	k1gFnSc2	En
Taurido	Taurida	k1gFnSc5	Taurida
<g/>
)	)	kIx)	)
a	a	k8xC	a
Schillerovy	Schillerův	k2eAgMnPc4d1	Schillerův
Loupežníky	loupežník	k1gMnPc4	loupežník
(	(	kIx(	(
<g/>
La	la	k1gNnPc6	la
Rabistoj	Rabistoj	k1gInSc4	Rabistoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Revizora	revizor	k1gMnSc4	revizor
(	(	kIx(	(
<g/>
La	la	k1gNnSc2	la
Revizoro	Revizora	k1gFnSc5	Revizora
<g/>
)	)	kIx)	)
od	od	k7c2	od
N.	N.	kA	N.
V.	V.	kA	V.
Gogola	Gogol	k1gMnSc2	Gogol
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
díly	díl	k1gInPc4	díl
pohádek	pohádka	k1gFnPc2	pohádka
(	(	kIx(	(
<g/>
Fabeloj	Fabeloj	k1gInSc1	Fabeloj
<g/>
)	)	kIx)	)
od	od	k7c2	od
H.	H.	kA	H.
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Andersena	Andersen	k1gMnSc2	Andersen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInSc7	jeho
méně	málo	k6eAd2	málo
známé	známý	k2eAgInPc1d1	známý
překlady	překlad	k1gInPc1	překlad
patří	patřit	k5eAaImIp3nP	patřit
překlady	překlad	k1gInPc1	překlad
děl	dít	k5eAaImAgMnS	dít
Marta	Mars	k1gMnSc2	Mars
od	od	k7c2	od
E.	E.	kA	E.
Orzeszkowe	Orzeszkowe	k1gInSc1	Orzeszkowe
<g/>
,	,	kIx,	,
Georgo	Georgo	k1gNnSc1	Georgo
Dandin	Dandina	k1gFnPc2	Dandina
od	od	k7c2	od
J.	J.	kA	J.
B.	B.	kA	B.
Moliè	Moliè	k1gFnSc1	Moliè
a	a	k8xC	a
La	la	k1gNnSc1	la
Rabeno	Raben	k2eAgNnSc1d1	Raben
de	de	k?	de
Baharah	Baharaha	k1gFnPc2	Baharaha
od	od	k7c2	od
H.	H.	kA	H.
Heineho	Heine	k1gMnSc2	Heine
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gInPc2	jeho
překladů	překlad	k1gInPc2	překlad
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
originále	originál	k1gInSc6	originál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některá	některý	k3yIgNnPc4	některý
díla	dílo	k1gNnPc4	dílo
překládal	překládat	k5eAaImAgMnS	překládat
i	i	k8xC	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
překladů	překlad	k1gInPc2	překlad
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
jestliže	jestliže	k8xS	jestliže
jazyk	jazyk	k1gInSc1	jazyk
originálu	originál	k1gInSc2	originál
sám	sám	k3xTgInSc4	sám
nedostatečně	dostatečně	k6eNd1	dostatečně
ovládal	ovládat	k5eAaImAgInS	ovládat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
takovéto	takovýto	k3xDgInPc4	takovýto
překlady	překlad	k1gInPc4	překlad
používal	používat	k5eAaImAgInS	používat
jako	jako	k8xC	jako
pomůcku	pomůcka	k1gFnSc4	pomůcka
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
z	z	k7c2	z
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
hovořil	hovořit	k5eAaImAgMnS	hovořit
plynně	plynně	k6eAd1	plynně
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
a	a	k8xC	a
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
pasivně	pasivně	k6eAd1	pasivně
dobře	dobře	k6eAd1	dobře
ovládal	ovládat	k5eAaImAgInS	ovládat
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
a	a	k8xC	a
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnPc4d2	menší
znalosti	znalost	k1gFnPc4	znalost
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
italštině	italština	k1gFnSc6	italština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Výročí	výročí	k1gNnSc1	výročí
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
slaví	slavit	k5eAaImIp3nP	slavit
esperantisté	esperantista	k1gMnPc1	esperantista
každoročně	každoročně	k6eAd1	každoročně
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Zamenhofův	Zamenhofův	k2eAgInSc4d1	Zamenhofův
den	den	k1gInSc4	den
nebo	nebo	k8xC	nebo
také	také	k9	také
jako	jako	k9	jako
Den	den	k1gInSc4	den
esperantské	esperantský	k2eAgFnSc2d1	esperantská
knihy	kniha	k1gFnSc2	kniha
či	či	k8xC	či
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
dokumentováno	dokumentovat	k5eAaBmNgNnS	dokumentovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
památníků	památník	k1gInPc2	památník
a	a	k8xC	a
veřejných	veřejný	k2eAgNnPc2d1	veřejné
prostranství	prostranství	k1gNnPc2	prostranství
v	v	k7c6	v
54	[number]	k4	54
zemích	zem	k1gFnPc6	zem
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
jméno	jméno	k1gNnSc4	jméno
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc1	jeho
původní	původní	k2eAgInSc1d1	původní
pseudonym	pseudonym	k1gInSc1	pseudonym
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
byl	být	k5eAaImAgMnS	být
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročím	výročí	k1gNnSc7	výročí
jeho	on	k3xPp3gNnSc2	on
úmrtí	úmrtí	k1gNnSc2	úmrtí
UNESCO	UNESCO	kA	UNESCO
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
do	do	k7c2	do
oficiálního	oficiální	k2eAgInSc2d1	oficiální
seznamu	seznam	k1gInSc2	seznam
významných	významný	k2eAgNnPc2d1	významné
výročí	výročí	k1gNnPc2	výročí
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BOULTON	BOULTON	kA	BOULTON
<g/>
,	,	kIx,	,
Marjorie	Marjorie	k1gFnSc1	Marjorie
<g/>
.	.	kIx.	.
</s>
<s>
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
<g/>
:	:	kIx,	:
Creator	Creator	k1gInSc1	Creator
of	of	k?	of
Esperanto	esperanto	k1gNnSc1	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
Routledge	Routledge	k1gFnSc1	Routledge
&	&	k?	&
Kegan	Kegan	k1gMnSc1	Kegan
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
XII	XII	kA	XII
+	+	kIx~	+
223	[number]	k4	223
p.	p.	k?	p.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CENTASSI	CENTASSI	kA	CENTASSI
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
<g/>
.	.	kIx.	.
</s>
<s>
MASSON	MASSON	kA	MASSON
<g/>
,	,	kIx,	,
Henri	Henri	k1gNnSc2	Henri
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
proti	proti	k7c3	proti
Babylonu	Babylon	k1gInSc3	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
esperanta	esperanto	k1gNnSc2	esperanto
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jindřiška	Jindřiška	k1gFnSc1	Jindřiška
Drahotová	Drahotový	k2eAgFnSc1d1	Drahotová
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
nákladem	náklad	k1gInSc7	náklad
vlastním	vlastní	k2eAgInSc7d1	vlastní
–	–	k?	–
J.	J.	kA	J.
<g/>
Drahotová	Drahotový	k2eAgFnSc1d1	Drahotová
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
192	[number]	k4	192
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
Životopis	životopis	k1gInSc1	životopis
L.L.	L.L.	k1gMnSc2	L.L.
<g/>
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DREZEN	DREZEN	kA	DREZEN
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
<g/>
.	.	kIx.	.
</s>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
.	.	kIx.	.
</s>
<s>
Dresden	Dresdna	k1gFnPc2	Dresdna
:	:	kIx,	:
SAT	SAT	kA	SAT
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
)	)	kIx)	)
dostupné	dostupný	k2eAgFnPc1d1	dostupná
online	onlin	k1gInSc5	onlin
</s>
</p>
<p>
<s>
KARHAN	karhan	k1gInSc1	karhan
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
harmonii	harmonie	k1gFnSc3	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
A.	A.	kA	A.
Komenský	Komenský	k1gMnSc1	Komenský
-	-	kIx~	-
L.	L.	kA	L.
L.	L.	kA	L.
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
.	.	kIx.	.
</s>
<s>
Ilustr	Ilustr	k1gInSc1	Ilustr
<g/>
.	.	kIx.	.
</s>
<s>
Milada	Milada	k1gFnSc1	Milada
Broumová	Broumová	k1gFnSc1	Broumová
<g/>
.	.	kIx.	.
</s>
<s>
Dobřichovice	Dobřichovice	k1gFnPc1	Dobřichovice
:	:	kIx,	:
KAVA-PECH	KAVA-PECH	k1gFnSc1	KAVA-PECH
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
91	[number]	k4	91
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87169	[number]	k4	87169
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
–	–	k?	–
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
díle	dílo	k1gNnSc6	dílo
a	a	k8xC	a
odkazu	odkaz	k1gInSc6	odkaz
Komenského	Komenský	k1gMnSc2	Komenský
a	a	k8xC	a
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
podobností	podobnost	k1gFnPc2	podobnost
jejich	jejich	k3xOp3gInPc2	jejich
údělů	úděl	k1gInPc2	úděl
i	i	k8xC	i
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
závěry	závěr	k1gInPc1	závěr
autora	autor	k1gMnSc2	autor
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
budoucnosti	budoucnost	k1gFnSc3	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
bibliografie	bibliografie	k1gFnSc1	bibliografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KORZHENKOV	KORZHENKOV	kA	KORZHENKOV
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Life	Life	k1gInSc1	Life
of	of	k?	of
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
<g/>
.	.	kIx.	.
<g/>
The	The	k1gFnSc1	The
Life	Life	k1gFnSc1	Life
<g/>
,	,	kIx,	,
Works	Works	kA	Works
and	and	k?	and
Ideas	Ideas	k1gInSc1	Ideas
of	of	k?	of
the	the	k?	the
Author	Author	k1gInSc1	Author
of	of	k?	of
Esperanto	esperanto	k1gNnSc1	esperanto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
M.	M.	kA	M.
Richmond	Richmond	k1gMnSc1	Richmond
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
Tonkin	Tonkin	k1gInSc1	Tonkin
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
:	:	kIx,	:
Mondial	Mondial	k1gInSc1	Mondial
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9781595691675	[number]	k4	9781595691675
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KORŽENKOV	KORŽENKOV	kA	KORŽENKOV
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Homarano	Homarana	k1gFnSc5	Homarana
<g/>
:	:	kIx,	:
La	la	k1gNnSc4	la
vivo	vivo	k6eAd1	vivo
<g/>
,	,	kIx,	,
verkoj	verkoj	k1gInSc1	verkoj
kaj	kát	k5eAaImRp2nS	kát
ideoj	ideoj	k1gInSc1	ideoj
de	de	k?	de
d-ro	do	k1gNnSc1	d-ro
L.	L.	kA	L.
L.	L.	kA	L.
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
(	(	kIx(	(
<g/>
Příslušník	příslušník	k1gMnSc1	příslušník
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc1	myšlenka
dr	dr	kA	dr
<g/>
.	.	kIx.	.
L.L.	L.L.	k1gMnSc2	L.L.
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
<g/>
:	:	kIx,	:
Sezonoj	Sezonoj	k1gInSc1	Sezonoj
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
360	[number]	k4	360
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Série	série	k1gFnSc1	série
Scio	Scio	k1gMnSc1	Scio
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
609	[number]	k4	609
<g/>
-	-	kIx~	-
<g/>
95087	[number]	k4	95087
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KORŽENKOV	KORŽENKOV	kA	KORŽENKOV
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
esperanta	esperanto	k1gNnSc2	esperanto
do	do	k7c2	do
slovenštiny	slovenština	k1gFnSc2	slovenština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
P.	P.	kA	P.
Petrík	Petrík	k1gMnSc1	Petrík
<g/>
.	.	kIx.	.
</s>
<s>
Partizánske	Partizánske	k1gNnSc1	Partizánske
:	:	kIx,	:
Espero	Espero	k1gNnSc1	Espero
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
89366	[number]	k4	89366
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÜNZLI	KÜNZLI	kA	KÜNZLI
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gInSc1	Andreas
<g/>
.	.	kIx.	.
</s>
<s>
L.L.	L.L.	k?	L.L.
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
<g/>
,	,	kIx,	,
Hillelismus	Hillelismus	k1gInSc1	Hillelismus
(	(	kIx(	(
<g/>
Homaranismus	Homaranismus	k1gInSc1	Homaranismus
<g/>
)	)	kIx)	)
und	und	k?	und
die	die	k?	die
"	"	kIx"	"
<g/>
jüdische	jüdisch	k1gMnSc2	jüdisch
Frage	Frag	k1gMnSc2	Frag
<g/>
"	"	kIx"	"
in	in	k?	in
Ost-	Ost-	k1gFnSc1	Ost-
und	und	k?	und
Westeuropa	Westeuropa	k1gFnSc1	Westeuropa
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Serie	serie	k1gFnSc1	serie
Jüdische	Jüdisch	k1gFnSc2	Jüdisch
Kultur	kultura	k1gFnPc2	kultura
Bd	Bd	k1gFnSc7	Bd
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
<g/>
:	:	kIx,	:
Harrassowitz	Harrassowitz	k1gInSc1	Harrassowitz
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
527	[number]	k4	527
s.	s.	k?	s.
24	[number]	k4	24
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
447	[number]	k4	447
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6232	[number]	k4	6232
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MEMORLIBRO	MEMORLIBRO	kA	MEMORLIBRO
eldonita	eldonita	k1gFnSc1	eldonita
okaze	okaze	k1gFnSc1	okaze
de	de	k?	de
la	la	k1gNnSc1	la
centjara	centjara	k1gFnSc1	centjara
jubileo	jubileo	k1gNnSc1	jubileo
de	de	k?	de
la	la	k1gNnPc2	la
naskiĝ	naskiĝ	k?	naskiĝ
de	de	k?	de
d-ro	do	k1gMnSc1	d-ro
L.L.	L.L.	k1gMnSc1	L.L.
<g/>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
(	(	kIx(	(
<g/>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
kniha	kniha	k1gFnSc1	kniha
vydaná	vydaný	k2eAgFnSc1d1	vydaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnPc2	výročí
narození	narození	k1gNnSc2	narození
dr	dr	kA	dr
<g/>
.	.	kIx.	.
L.L.	L.L.	k1gMnSc2	L.L.
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Red	Red	k?	Red
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
Lapenna	Lapenna	k1gFnSc1	Lapenna
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Universala	Universala	k1gMnSc1	Universala
Esperanto-Asocio	Esperanto-Asocio	k1gMnSc1	Esperanto-Asocio
–	–	k?	–
Centro	Centro	k1gNnSc4	Centro
pri	pri	k?	pri
esploro	esplora	k1gFnSc5	esplora
kaj	kát	k5eAaImRp2nS	kát
dokumentado	dokumentada	k1gFnSc5	dokumentada
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
103	[number]	k4	103
s.	s.	k?	s.
Kolekce	kolekce	k1gFnSc2	kolekce
esejí	esej	k1gFnPc2	esej
významných	významný	k2eAgMnPc2d1	významný
esperantistů	esperantista	k1gMnPc2	esperantista
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
)	)	kIx)	)
dostupné	dostupný	k2eAgFnPc1d1	dostupná
online	onlin	k1gMnSc5	onlin
</s>
</p>
<p>
<s>
PRIVAT	PRIVAT	kA	PRIVAT
<g/>
,	,	kIx,	,
Edmond	Edmond	k1gMnSc1	Edmond
<g/>
.	.	kIx.	.
</s>
<s>
Life	Life	k1gFnSc1	Life
of	of	k?	of
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
<g/>
.	.	kIx.	.
</s>
<s>
Inventor	Inventor	k1gInSc1	Inventor
of	of	k?	of
Esperanto	esperanto	k1gNnSc1	esperanto
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
esp	esp	k?	esp
<g/>
.	.	kIx.	.
originálu	originál	k1gInSc2	originál
Vivo	vivo	k6eAd1	vivo
de	de	k?	de
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
(	(	kIx(	(
<g/>
Život	život	k1gInSc1	život
Zamenhofův	Zamenhofův	k2eAgInSc1d1	Zamenhofův
<g/>
)	)	kIx)	)
přel	přít	k5eAaImAgInS	přít
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Elliot	Elliot	k1gInSc1	Elliot
<g/>
.	.	kIx.	.
</s>
<s>
Red	Red	k?	Red
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
Lins	Lins	k1gInSc1	Lins
<g/>
.	.	kIx.	.
</s>
<s>
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
<g/>
:	:	kIx,	:
UEA	UEA	kA	UEA
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-92-9017-097-6	[number]	k4	978-92-9017-097-6
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ŠUPICHOVÁ	ŠUPICHOVÁ	kA	ŠUPICHOVÁ
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
L.L.	L.L.	k1gMnSc1	L.L.
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
35	[number]	k4	35
s.	s.	k?	s.
–	–	k?	–
Edice	edice	k1gFnSc2	edice
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
č.	č.	k?	č.
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZIÓLKOWSKA	ZIÓLKOWSKA	kA	ZIÓLKOWSKA
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Esperanto	esperanto	k1gNnSc4	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
polštiny	polština	k1gFnSc2	polština
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
Dratwer	Dratwer	k1gInSc1	Dratwer
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gFnSc1	Warszawa
:	:	kIx,	:
Wiedza	Wiedza	k1gFnSc1	Wiedza
Powszechna	Powszechna	k1gFnSc1	Powszechna
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
331	[number]	k4	331
s.	s.	k?	s.
(	(	kIx(	(
<g/>
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
a	a	k8xC	a
polštině	polština	k1gFnSc6	polština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
česká	český	k2eAgFnSc1d1	Česká
mutace	mutace	k1gFnSc1	mutace
vícejazyčného	vícejazyčný	k2eAgInSc2d1	vícejazyčný
informačního	informační	k2eAgInSc2d1	informační
webu	web	k1gInSc2	web
organizace	organizace	k1gFnSc2	organizace
E	E	kA	E
<g/>
@	@	kIx~	@
<g/>
I	i	k9	i
–	–	k?	–
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc1	přehled
děl	dít	k5eAaBmAgInS	dít
<g/>
,	,	kIx,	,
citáty	citát	k1gInPc1	citát
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnPc1	fotogalerie
<g/>
,	,	kIx,	,
odkazy	odkaz	k1gInPc1	odkaz
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
<g/>
.	.	kIx.	.
<g/>
life	lifat	k5eAaPmIp3nS	lifat
česká	český	k2eAgFnSc1d1	Česká
mutace	mutace	k1gFnSc1	mutace
vícejazyčného	vícejazyčný	k2eAgInSc2d1	vícejazyčný
informačního	informační	k2eAgInSc2d1	informační
webu	web	k1gInSc2	web
UEA	UEA	kA	UEA
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gInSc4	Zamenhof
a	a	k8xC	a
zamenhofologie	zamenhofologie	k1gFnPc4	zamenhofologie
internetový	internetový	k2eAgMnSc1d1	internetový
průvodce	průvodce	k1gMnSc1	průvodce
s	s	k7c7	s
odkazy	odkaz	k1gInPc7	odkaz
na	na	k7c4	na
primární	primární	k2eAgInPc4d1	primární
zdroje	zdroj	k1gInPc4	zdroj
(	(	kIx(	(
<g/>
komp.	komp.	k?	komp.
R.	R.	kA	R.
Dumain	Dumain	k1gInSc1	Dumain
<g/>
)	)	kIx)	)
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
přehled	přehled	k1gInSc1	přehled
internetových	internetový	k2eAgInPc2d1	internetový
zdrojů	zdroj	k1gInPc2	zdroj
o	o	k7c6	o
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Zamenhofovi	Zamenhof	k1gMnSc3	Zamenhof
výběrová	výběrový	k2eAgFnSc1d1	výběrová
bibliografie	bibliografie	k1gFnSc1	bibliografie
na	na	k7c6	na
webu	web	k1gInSc6	web
Esperanto	esperanto	k1gNnSc1	esperanto
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
s	s	k7c7	s
odkazy	odkaz	k1gInPc7	odkaz
na	na	k7c4	na
multimediální	multimediální	k2eAgInPc4d1	multimediální
dokumenty	dokument	k1gInPc4	dokument
(	(	kIx(	(
<g/>
komp.	komp.	k?	komp.
P.	P.	kA	P.
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
dr	dr	kA	dr
<g/>
.	.	kIx.	.
L.L.	L.L.	k1gMnSc4	L.L.
Zamenhofa	Zamenhof	k1gMnSc4	Zamenhof
<g/>
,	,	kIx,	,
iniciátora	iniciátor	k1gMnSc4	iniciátor
jazyka	jazyk	k1gInSc2	jazyk
esperanto	esperanto	k1gNnSc1	esperanto
videozáznam	videozáznam	k1gInSc4	videozáznam
přednášky	přednáška	k1gFnSc2	přednáška
J.	J.	kA	J.
Křížkové	Křížková	k1gFnSc2	Křížková
přednesené	přednesený	k2eAgFnSc2d1	přednesená
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
na	na	k7c6	na
Konferenci	konference	k1gFnSc6	konference
ČES	ČES	kA	ČES
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
</s>
</p>
<p>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Röllinger	Röllinger	k1gMnSc1	Röllinger
-	-	kIx~	-
Monumente	monument	k1gInSc5	monument
pri	pri	k?	pri
Esperanto	esperanto	k1gNnSc4	esperanto
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
dokumentace	dokumentace	k1gFnSc1	dokumentace
1044	[number]	k4	1044
památníků	památník	k1gInPc2	památník
a	a	k8xC	a
veřejných	veřejný	k2eAgNnPc2d1	veřejné
prostranství	prostranství	k1gNnPc2	prostranství
v	v	k7c6	v
54	[number]	k4	54
zemích	zem	k1gFnPc6	zem
nesoucí	nesoucí	k2eAgNnSc1d1	nesoucí
jméno	jméno	k1gNnSc1	jméno
Esperanto	esperanto	k1gNnSc1	esperanto
nebo	nebo	k8xC	nebo
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
</s>
</p>
