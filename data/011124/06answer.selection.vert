<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Ludwik	Ludwik	k1gMnSc1	Ludwik
Łazarz	Łazarz	k1gMnSc1	Łazarz
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1859	[number]	k4	1859
Bělostok	Bělostok	k1gInSc1	Bělostok
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
oční	oční	k2eAgMnSc1d1	oční
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
umělého	umělý	k2eAgInSc2d1	umělý
jazyka	jazyk	k1gInSc2	jazyk
určeného	určený	k2eAgMnSc2d1	určený
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
