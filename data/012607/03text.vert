<p>
<s>
Gloster	Gloster	k1gMnSc1	Gloster
Meteor	meteor	k1gInSc4	meteor
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
britským	britský	k2eAgMnSc7d1	britský
proudovým	proudový	k2eAgInSc7d1	proudový
stíhacím	stíhací	k2eAgInSc7d1	stíhací
letounem	letoun	k1gInSc7	letoun
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
koncem	konec	k1gInSc7	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
byla	být	k5eAaImAgFnS	být
vedle	vedle	k7c2	vedle
Německa	Německo	k1gNnSc2	Německo
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
proudového	proudový	k2eAgInSc2d1	proudový
pohonu	pohon	k1gInSc2	pohon
letadel	letadlo	k1gNnPc2	letadlo
nejdál	daleko	k6eAd3	daleko
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
vděčí	vděčit	k5eAaImIp3nS	vděčit
především	především	k6eAd1	především
konstruktéru	konstruktér	k1gMnSc3	konstruktér
Franku	Frank	k1gMnSc3	Frank
Whittleovi	Whittleus	k1gMnSc3	Whittleus
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
zkušební	zkušební	k2eAgInSc1d1	zkušební
letoun	letoun	k1gInSc1	letoun
G	G	kA	G
<g/>
.40	.40	k4	.40
Squirt	Squirta	k1gFnPc2	Squirta
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
W	W	kA	W
<g/>
.1	.1	k4	.1
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
experimentální	experimentální	k2eAgInSc4d1	experimentální
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
dal	dát	k5eAaPmAgInS	dát
základ	základ	k1gInSc1	základ
vzniku	vznik	k1gInSc2	vznik
proudových	proudový	k2eAgNnPc2d1	proudové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vývojem	vývoj	k1gInSc7	vývoj
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
motorů	motor	k1gInPc2	motor
výrobců	výrobce	k1gMnPc2	výrobce
Power	Power	k1gInSc1	Power
Jets	Jetsa	k1gFnPc2	Jetsa
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
Rover	rover	k1gMnSc1	rover
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc4	metropolitan
Vickers	Vickersa	k1gFnPc2	Vickersa
a	a	k8xC	a
de	de	k?	de
Havilland	Havilland	k1gInSc1	Havilland
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
našla	najít	k5eAaPmAgFnS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
až	až	k6eAd1	až
v	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Gloster	Gloster	k1gInSc1	Gloster
připravovala	připravovat	k5eAaImAgFnS	připravovat
nový	nový	k2eAgInSc4d1	nový
letoun	letoun	k1gInSc4	letoun
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
jako	jako	k9	jako
Meteor	meteor	k1gInSc1	meteor
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
specifikace	specifikace	k1gFnSc2	specifikace
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
letectví	letectví	k1gNnSc2	letectví
F.	F.	kA	F.
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
již	již	k6eAd1	již
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pátý	pátý	k4xOgMnSc1	pátý
z	z	k7c2	z
osmi	osm	k4xCc2	osm
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
prototypů	prototyp	k1gInPc2	prototyp
letounu	letoun	k1gInSc2	letoun
označeného	označený	k2eAgInSc2d1	označený
Gloster	Gloster	k1gInSc1	Gloster
F.	F.	kA	F.
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
H	H	kA	H
Meteor	meteor	k1gInSc1	meteor
(	(	kIx(	(
<g/>
výr	výr	k1gMnSc1	výr
<g/>
.	.	kIx.	.
č.	č.	k?	č.
DG	dg	kA	dg
<g/>
206	[number]	k4	206
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zalétán	zalétat	k5eAaPmNgInS	zalétat
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
RAF	raf	k0	raf
Cranwell	Cranwell	k1gMnSc1	Cranwell
továrním	tovární	k2eAgMnSc7d1	tovární
pilotem	pilot	k1gMnSc7	pilot
Michaelem	Michael	k1gMnSc7	Michael
Dauntem	Daunt	k1gMnSc7	Daunt
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
dva	dva	k4xCgInPc1	dva
proudové	proudový	k2eAgInPc1d1	proudový
motory	motor	k1gInPc1	motor
Halford	Halforda	k1gFnPc2	Halforda
H	H	kA	H
<g/>
.1	.1	k4	.1
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
až	až	k9	až
8,0	[number]	k4	8,0
kN	kN	k?	kN
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
přejmenovány	přejmenován	k2eAgInPc1d1	přejmenován
na	na	k7c4	na
de	de	k?	de
Havilland	Havilland	k1gInSc4	Havilland
Goblin	Goblin	k2eAgInSc4d1	Goblin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
postavený	postavený	k2eAgInSc1d1	postavený
letoun	letoun	k1gInSc1	letoun
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
DG	dg	kA	dg
<g/>
202	[number]	k4	202
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
využíván	využíván	k2eAgInSc4d1	využíván
k	k	k7c3	k
pojížděcím	pojížděcí	k2eAgFnPc3d1	pojížděcí
zkouškám	zkouška	k1gFnPc3	zkouška
s	s	k7c7	s
motory	motor	k1gInPc7	motor
Whittle	Whittle	k1gFnSc2	Whittle
W	W	kA	W
<g/>
.2	.2	k4	.2
<g/>
B	B	kA	B
s	s	k7c7	s
radiálním	radiální	k2eAgInSc7d1	radiální
(	(	kIx(	(
<g/>
odstředivým	odstředivý	k2eAgInSc7d1	odstředivý
<g/>
)	)	kIx)	)
kompresorem	kompresor	k1gInSc7	kompresor
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
6,2	[number]	k4	6,2
kN	kN	k?	kN
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
až	až	k9	až
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
s	s	k7c7	s
instalovanými	instalovaný	k2eAgFnPc7d1	instalovaná
pohonnými	pohonný	k2eAgFnPc7d1	pohonná
jednotkami	jednotka	k1gFnPc7	jednotka
Whittle	Whittle	k1gFnSc2	Whittle
W	W	kA	W
<g/>
.2	.2	k4	.2
<g/>
B	B	kA	B
<g/>
/	/	kIx~	/
<g/>
23	[number]	k4	23
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
po	po	k7c6	po
6,8	[number]	k4	6,8
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
výroba	výroba	k1gFnSc1	výroba
těchto	tento	k3xDgFnPc2	tento
pohonných	pohonný	k2eAgFnPc2d1	pohonná
jednotek	jednotka	k1gFnPc2	jednotka
probíhala	probíhat	k5eAaImAgFnS	probíhat
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Rover	rover	k1gMnSc1	rover
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
výrobu	výroba	k1gFnSc4	výroba
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
převzal	převzít	k5eAaPmAgMnS	převzít
Rolls-Royce	Rolls-Royce	k1gMnSc1	Rolls-Royce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
započaly	započnout	k5eAaPmAgFnP	započnout
zkoušky	zkouška	k1gFnPc4	zkouška
třetího	třetí	k4xOgInSc2	třetí
prototypu	prototyp	k1gInSc2	prototyp
DG	dg	kA	dg
<g/>
204	[number]	k4	204
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
motorů	motor	k1gInPc2	motor
F.	F.	kA	F.
<g/>
2	[number]	k4	2
společnosti	společnost	k1gFnSc2	společnost
Metropolitan-Vickers	Metropolitan-Vickersa	k1gFnPc2	Metropolitan-Vickersa
s	s	k7c7	s
axiálním	axiální	k2eAgInSc7d1	axiální
(	(	kIx(	(
<g/>
osovým	osový	k2eAgInSc7d1	osový
<g/>
)	)	kIx)	)
kompresorem	kompresor	k1gInSc7	kompresor
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
po	po	k7c6	po
8,0	[number]	k4	8,0
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
stroje	stroj	k1gInPc1	stroj
malé	malý	k2eAgFnSc2d1	malá
série	série	k1gFnSc2	série
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
15	[number]	k4	15
kusů	kus	k1gInPc2	kus
Gloster	Gloster	k1gInSc4	Gloster
Meteor	meteor	k1gInSc1	meteor
F	F	kA	F
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
I.	I.	kA	I.
létaly	létat	k5eAaImAgInP	létat
s	s	k7c7	s
motory	motor	k1gInPc7	motor
Rolls-Royce	Rolls-Royce	k1gFnSc2	Rolls-Royce
Welland	Welland	k1gInSc1	Welland
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
.2	.2	k4	.2
<g/>
B	B	kA	B
<g/>
/	/	kIx~	/
<g/>
23	[number]	k4	23
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
však	však	k9	však
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
rychlosti	rychlost	k1gFnSc2	rychlost
pouze	pouze	k6eAd1	pouze
656	[number]	k4	656
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nepřekonávaly	překonávat	k5eNaImAgInP	překonávat
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
letouny	letoun	k1gInPc1	letoun
s	s	k7c7	s
pístovými	pístový	k2eAgInPc7d1	pístový
motory	motor	k1gInPc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
USA	USA	kA	USA
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Muroc	Muroc	k1gInSc1	Muroc
AFB	AFB	kA	AFB
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
proudový	proudový	k2eAgInSc4d1	proudový
Bell	bell	k1gInSc4	bell
P-59	P-59	k1gFnSc1	P-59
Airacomet	Airacomet	k1gInSc1	Airacomet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
porovnávací	porovnávací	k2eAgFnPc4d1	porovnávací
zkoušky	zkouška	k1gFnPc4	zkouška
ve	v	k7c4	v
Farnborough	Farnborough	k1gInSc4	Farnborough
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1944	[number]	k4	1944
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
Meteor	meteor	k1gInSc1	meteor
F	F	kA	F
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
následovaly	následovat	k5eAaImAgInP	následovat
sériové	sériový	k2eAgInPc1d1	sériový
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
byla	být	k5eAaImAgFnS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
pozměněným	pozměněný	k2eAgInSc7d1	pozměněný
odsouvacím	odsouvací	k2eAgInSc7d1	odsouvací
krytem	kryt	k1gInSc7	kryt
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
lepší	dobrý	k2eAgInSc4d2	lepší
výhled	výhled	k1gInSc4	výhled
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
15	[number]	k4	15
sériových	sériový	k2eAgFnPc2d1	sériová
Mk	Mk	k1gFnPc2	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
bylo	být	k5eAaImAgNnS	být
poháněno	poháněn	k2eAgNnSc1d1	poháněno
motory	motor	k1gInPc1	motor
Welland	Wellanda	k1gFnPc2	Wellanda
I	I	kA	I
<g/>
,	,	kIx,	,
od	od	k7c2	od
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
stroje	stroj	k1gInPc1	stroj
firma	firma	k1gFnSc1	firma
Gloster	Gloster	k1gInSc1	Gloster
instalovala	instalovat	k5eAaBmAgFnS	instalovat
nové	nový	k2eAgInPc4d1	nový
motory	motor	k1gInPc4	motor
Rolls-Royce	Rolls-Royce	k1gMnSc1	Rolls-Royce
Derwent	Derwent	k1gMnSc1	Derwent
I	i	k9	i
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
8,9	[number]	k4	8,9
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
měla	mít	k5eAaImAgFnS	mít
rovněž	rovněž	k9	rovněž
zvětšeny	zvětšen	k2eAgFnPc4d1	zvětšena
palivové	palivový	k2eAgFnPc4d1	palivová
nádrže	nádrž	k1gFnPc4	nádrž
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc1	konstrukce
letounu	letoun	k1gInSc2	letoun
byla	být	k5eAaImAgFnS	být
zesílena	zesílen	k2eAgFnSc1d1	zesílena
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
ukončená	ukončená	k1gFnSc1	ukončená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
počtu	počet	k1gInSc2	počet
280	[number]	k4	280
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výroba	výroba	k1gFnSc1	výroba
typu	typ	k1gInSc2	typ
Meteor	meteor	k1gInSc1	meteor
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.4	.4	k4	.4
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
535	[number]	k4	535
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Mk	Mk	k?	Mk
<g/>
.4	.4	k4	.4
dostaly	dostat	k5eAaPmAgInP	dostat
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
výkonnějších	výkonný	k2eAgInPc6d2	výkonnější
motorech	motor	k1gInPc6	motor
Derwent	Derwent	k1gInSc4	Derwent
V	V	kA	V
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
po	po	k7c6	po
15,5	[number]	k4	15,5
kN	kN	k?	kN
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
v	v	k7c6	v
prodloužených	prodloužený	k2eAgFnPc6d1	prodloužená
motorových	motorový	k2eAgFnPc6d1	motorová
gondolách	gondola	k1gFnPc6	gondola
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
Mk	Mk	k1gFnSc4	Mk
<g/>
.4	.4	k4	.4
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
dvojice	dvojice	k1gFnSc1	dvojice
strojů	stroj	k1gInPc2	stroj
Meteor	meteor	k1gInSc1	meteor
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.4	.4	k4	.4
<g/>
,	,	kIx,	,
upravená	upravený	k2eAgFnSc1d1	upravená
z	z	k7c2	z
typu	typ	k1gInSc2	typ
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
vybrána	vybrán	k2eAgFnSc1d1	vybrána
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
světového	světový	k2eAgInSc2d1	světový
rychlostního	rychlostní	k2eAgInSc2d1	rychlostní
rekordu	rekord	k1gInSc2	rekord
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
držel	držet	k5eAaImAgInS	držet
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
Me	Me	k1gFnSc4	Me
209-II	[number]	k4	209-II
s	s	k7c7	s
pilotem	pilot	k1gMnSc7	pilot
Fritzem	Fritz	k1gMnSc7	Fritz
Wendelem	Wendel	k1gMnSc7	Wendel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
letounů	letoun	k1gInPc2	letoun
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
hlavňová	hlavňový	k2eAgFnSc1d1	hlavňová
výzbroj	výzbroj	k1gFnSc1	výzbroj
<g/>
,	,	kIx,	,
výstřelné	výstřelný	k2eAgInPc1d1	výstřelný
otvory	otvor	k1gInPc1	otvor
byly	být	k5eAaImAgInP	být
zakrytovány	zakrytovat	k5eAaImNgInP	zakrytovat
a	a	k8xC	a
motory	motor	k1gInPc1	motor
byly	být	k5eAaImAgInP	být
vyladěny	vyladit	k5eAaPmNgInP	vyladit
na	na	k7c4	na
tah	tah	k1gInSc4	tah
16,03	[number]	k4	16,03
kN	kN	k?	kN
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgFnPc2	tři
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
Group	Group	k1gMnSc1	Group
Captain	Captain	k1gMnSc1	Captain
H.	H.	kA	H.
Wilson	Wilson	k1gMnSc1	Wilson
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
čtyř	čtyři	k4xCgInPc2	čtyři
nízkých	nízký	k2eAgInPc2d1	nízký
průletů	průlet	k1gInPc2	průlet
nad	nad	k7c7	nad
měřenou	měřený	k2eAgFnSc7d1	měřená
tratí	trať	k1gFnSc7	trať
v	v	k7c6	v
Herne	Hern	k1gInSc5	Hern
Bay	Bay	k1gFnSc4	Bay
průměrné	průměrný	k2eAgFnSc3d1	průměrná
rychlosti	rychlost	k1gFnSc3	rychlost
975,24	[number]	k4	975,24
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
tak	tak	k6eAd1	tak
absolutní	absolutní	k2eAgInSc1d1	absolutní
světový	světový	k2eAgInSc1d1	světový
rekord	rekord	k1gInSc1	rekord
pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
tohoto	tento	k3xDgInSc2	tento
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
došlo	dojít	k5eAaPmAgNnS	dojít
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
s	s	k7c7	s
ještě	ještě	k9	ještě
rozsáhleji	rozsáhle	k6eAd2	rozsáhle
upraveným	upravený	k2eAgInSc7d1	upravený
strojem	stroj	k1gInSc7	stroj
Meteor	meteor	k1gInSc1	meteor
Mk	Mk	k1gFnSc2	Mk
<g/>
.4	.4	k4	.4
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
Cpt	Cpt	k1gMnSc2	Cpt
"	"	kIx"	"
<g/>
Teddy	Tedda	k1gMnSc2	Tedda
<g/>
"	"	kIx"	"
Donaldson	Donaldson	k1gInSc1	Donaldson
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
rychlosti	rychlost	k1gFnSc2	rychlost
991,33	[number]	k4	991,33
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Sériové	sériový	k2eAgFnPc1d1	sériová
"	"	kIx"	"
<g/>
čtyřky	čtyřka	k1gFnPc1	čtyřka
<g/>
"	"	kIx"	"
přicházely	přicházet	k5eAaImAgFnP	přicházet
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
RAF	raf	k0	raf
koncem	konec	k1gInSc7	konec
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
série	série	k1gFnSc1	série
čísel	číslo	k1gNnPc2	číslo
RA473	RA473	k1gFnSc2	RA473
až	až	k8xS	až
RA493	RA493	k1gFnSc2	RA493
verze	verze	k1gFnSc2	verze
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.4	.4	k4	.4
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
pokusné	pokusný	k2eAgInPc4d1	pokusný
a	a	k8xC	a
vývojové	vývojový	k2eAgInPc4d1	vývojový
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Meteorem	meteor	k1gInSc7	meteor
sériového	sériový	k2eAgNnSc2d1	sériové
čísla	číslo	k1gNnSc2	číslo
RA490	RA490	k1gFnSc2	RA490
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
1947	[number]	k4	1947
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
experimentech	experiment	k1gInPc6	experiment
s	s	k7c7	s
proudovými	proudový	k2eAgInPc7d1	proudový
motory	motor	k1gInPc7	motor
s	s	k7c7	s
axiálním	axiální	k2eAgInSc7d1	axiální
kompresorem	kompresor	k1gInSc7	kompresor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
upravených	upravený	k2eAgFnPc2d1	upravená
motorových	motorový	k2eAgFnPc2d1	motorová
gondol	gondola	k1gFnPc2	gondola
byla	být	k5eAaImAgFnS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
dvojice	dvojice	k1gFnSc1	dvojice
pohonných	pohonný	k2eAgFnPc2d1	pohonná
jednotek	jednotka	k1gFnPc2	jednotka
Metro-Vick	Metro-Vicka	k1gFnPc2	Metro-Vicka
F.	F.	kA	F.
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Beryl	beryl	k1gInSc1	beryl
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
po	po	k7c6	po
17,1	[number]	k4	17,1
kN	kN	k?	kN
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
stíhací	stíhací	k2eAgInSc4d1	stíhací
létající	létající	k2eAgInSc4d1	létající
člun	člun	k1gInSc4	člun
Saunders-Roe	Saunders-Ro	k1gInSc2	Saunders-Ro
SR	SR	kA	SR
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušky	zkouška	k1gFnPc1	zkouška
probíhaly	probíhat	k5eAaImAgFnP	probíhat
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
letů	let	k1gInPc2	let
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
závadě	závada	k1gFnSc3	závada
hydrauliky	hydraulika	k1gFnSc2	hydraulika
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
nouzovému	nouzový	k2eAgNnSc3d1	nouzové
přistání	přistání	k1gNnSc3	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
pak	pak	k6eAd1	pak
tento	tento	k3xDgInSc4	tento
Meteor	meteor	k1gInSc4	meteor
sloužil	sloužit	k5eAaImAgMnS	sloužit
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
experimentům	experiment	k1gInPc3	experiment
s	s	k7c7	s
motory	motor	k1gInPc7	motor
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInPc4d1	budoucí
letouny	letoun	k1gInPc4	letoun
kategorie	kategorie	k1gFnSc2	kategorie
VTOL	VTOL	kA	VTOL
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
předním	přední	k2eAgInSc7d1	přední
nosníkem	nosník	k1gInSc7	nosník
křídla	křídlo	k1gNnSc2	křídlo
nesl	nést	k5eAaImAgInS	nést
v	v	k7c6	v
gondolách	gondola	k1gFnPc6	gondola
motory	motor	k1gInPc4	motor
Rolls-Royce	Rolls-Royka	k1gFnSc3	Rolls-Royka
Nene	Nen	k1gFnSc2	Nen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
výstupní	výstupní	k2eAgFnPc1d1	výstupní
trysky	tryska	k1gFnPc1	tryska
ústil	ústit	k5eAaImAgInS	ústit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
délky	délka	k1gFnSc2	délka
gondol	gondola	k1gFnPc2	gondola
šikmo	šikmo	k6eAd1	šikmo
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
experimentálních	experimentální	k2eAgInPc2d1	experimentální
Meteorů	meteor	k1gInPc2	meteor
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.4	.4	k4	.4
(	(	kIx(	(
<g/>
RA	ra	k0	ra
<g/>
491	[number]	k4	491
<g/>
)	)	kIx)	)
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
upravenými	upravený	k2eAgFnPc7d1	upravená
motorovými	motorový	k2eAgFnPc7d1	motorová
gondolami	gondola	k1gFnPc7	gondola
a	a	k8xC	a
nosníky	nosník	k1gInPc7	nosník
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
létal	létat	k5eAaImAgInS	létat
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
s	s	k7c7	s
pokusnými	pokusný	k2eAgInPc7d1	pokusný
proudovými	proudový	k2eAgInPc7d1	proudový
motory	motor	k1gInPc7	motor
s	s	k7c7	s
axiálními	axiální	k2eAgInPc7d1	axiální
kompresory	kompresor	k1gInPc7	kompresor
typu	typ	k1gInSc2	typ
Rolls-Royce	Rolls-Royec	k1gInSc2	Rolls-Royec
Avon	Avona	k1gFnPc2	Avona
RA	ra	k0	ra
<g/>
.2	.2	k4	.2
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
RA	ra	k0	ra
<g/>
.3	.3	k4	.3
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
po	po	k7c4	po
28	[number]	k4	28
až	až	k9	až
33	[number]	k4	33
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
SNECMA	SNECMA	kA	SNECMA
v	v	k7c6	v
letovém	letový	k2eAgInSc6d1	letový
režimu	režim	k1gInSc6	režim
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
proudové	proudový	k2eAgInPc4d1	proudový
motory	motor	k1gInPc4	motor
SNECMA	SNECMA	kA	SNECMA
Atar	Atar	k1gInSc4	Atar
101B	[number]	k4	101B
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
26	[number]	k4	26
kN	kN	k?	kN
<g/>
,	,	kIx,	,
také	také	k9	také
s	s	k7c7	s
axiálními	axiální	k2eAgInPc7d1	axiální
kompresory	kompresor	k1gInPc7	kompresor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
instalaci	instalace	k1gFnSc6	instalace
pohonných	pohonný	k2eAgFnPc2d1	pohonná
jednotek	jednotka	k1gFnPc2	jednotka
Atar	Atar	k1gMnSc1	Atar
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgInS	být
stroj	stroj	k1gInSc1	stroj
vybaven	vybavit	k5eAaPmNgInS	vybavit
přídí	příď	k1gFnSc7	příď
s	s	k7c7	s
kabinou	kabina	k1gFnSc7	kabina
z	z	k7c2	z
varianty	varianta	k1gFnSc2	varianta
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.8	.8	k4	.8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variantu	varianta	k1gFnSc4	varianta
Mk	Mk	k1gFnSc4	Mk
<g/>
.4	.4	k4	.4
nahradil	nahradit	k5eAaPmAgInS	nahradit
v	v	k7c6	v
řadové	řadový	k2eAgFnSc6d1	řadová
službě	služba	k1gFnSc6	služba
Meteor	meteor	k1gInSc1	meteor
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.8	.8	k4	.8
s	s	k7c7	s
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
trupem	trup	k1gInSc7	trup
a	a	k8xC	a
překonstruovaným	překonstruovaný	k2eAgNnSc7d1	překonstruované
ovládáním	ovládání	k1gNnSc7	ovládání
ocasních	ocasní	k2eAgFnPc2d1	ocasní
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
byly	být	k5eAaImAgInP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
zdokonalenými	zdokonalený	k2eAgInPc7d1	zdokonalený
motory	motor	k1gInPc7	motor
Derwent	Derwent	k1gMnSc1	Derwent
a	a	k8xC	a
vystřelovacím	vystřelovací	k2eAgNnSc7d1	vystřelovací
sedadlem	sedadlo	k1gNnSc7	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
podílela	podílet	k5eAaImAgFnS	podílet
kromě	kromě	k7c2	kromě
firmy	firma	k1gFnSc2	firma
Gloster	Glostra	k1gFnPc2	Glostra
také	také	k9	také
společnost	společnost	k1gFnSc1	společnost
Armstrong	Armstrong	k1gMnSc1	Armstrong
Whitworth	Whitworth	k1gMnSc1	Whitworth
<g/>
,	,	kIx,	,
licenční	licenční	k2eAgFnSc1d1	licenční
produkce	produkce	k1gFnSc1	produkce
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
1	[number]	k4	1
183	[number]	k4	183
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
kusy	kus	k1gInPc7	kus
byl	být	k5eAaImAgInS	být
typ	typ	k1gInSc1	typ
Mk	Mk	k1gFnSc4	Mk
<g/>
.8	.8	k4	.8
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
variantou	varianta	k1gFnSc7	varianta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
32	[number]	k4	32
pravidelných	pravidelný	k2eAgMnPc2d1	pravidelný
a	a	k8xC	a
11	[number]	k4	11
záložních	záložní	k2eAgMnPc2d1	záložní
squadron	squadron	k1gInSc4	squadron
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
vydržela	vydržet	k5eAaPmAgFnS	vydržet
u	u	k7c2	u
frontových	frontový	k2eAgFnPc2d1	frontová
denních	denní	k2eAgFnPc2d1	denní
stíhacích	stíhací	k2eAgFnPc2d1	stíhací
jednotek	jednotka	k1gFnPc2	jednotka
RAF	raf	k0	raf
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
experimentálním	experimentální	k2eAgInSc7d1	experimentální
Meteorem	meteor	k1gInSc7	meteor
<g/>
,	,	kIx,	,
létajícím	létající	k2eAgMnSc7d1	létající
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
proudovými	proudový	k2eAgInPc7d1	proudový
motory	motor	k1gInPc7	motor
s	s	k7c7	s
axiálními	axiální	k2eAgInPc7d1	axiální
kompresory	kompresor	k1gInPc7	kompresor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
stal	stát	k5eAaPmAgInS	stát
exemplář	exemplář	k1gInSc1	exemplář
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.8	.8	k4	.8
(	(	kIx(	(
<g/>
WA	WA	kA	WA
<g/>
820	[number]	k4	820
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
širokých	široký	k2eAgFnPc6d1	široká
gondolách	gondola	k1gFnPc6	gondola
pod	pod	k7c7	pod
křídlem	křídlo	k1gNnSc7	křídlo
zabudovány	zabudovat	k5eAaPmNgInP	zabudovat
motory	motor	k1gInPc1	motor
Metro-Vick	Metro-Vicka	k1gFnPc2	Metro-Vicka
F.	F.	kA	F.
<g/>
9	[number]	k4	9
Sapphire	Sapphir	k1gInSc5	Sapphir
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
34	[number]	k4	34
kN	kN	k?	kN
každý	každý	k3xTgInSc4	každý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
probíhala	probíhat	k5eAaImAgFnS	probíhat
u	u	k7c2	u
motorářské	motorářský	k2eAgFnSc2d1	motorářská
firmy	firma	k1gFnSc2	firma
Armstrong	Armstrong	k1gMnSc1	Armstrong
Siddeley	Siddelea	k1gFnSc2	Siddelea
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravený	upravený	k2eAgInSc1d1	upravený
letoun	letoun	k1gInSc1	letoun
létal	létat	k5eAaImAgInS	létat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
instruktážním	instruktážní	k2eAgInSc7d1	instruktážní
drakem	drak	k1gInSc7	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
pak	pak	k6eAd1	pak
létal	létat	k5eAaImAgInS	létat
jeden	jeden	k4xCgInSc1	jeden
F	F	kA	F
Mk	Mk	k1gFnSc7	Mk
<g/>
.8	.8	k4	.8
(	(	kIx(	(
<g/>
WA	WA	kA	WA
<g/>
982	[number]	k4	982
<g/>
)	)	kIx)	)
s	s	k7c7	s
miniaturními	miniaturní	k2eAgInPc7d1	miniaturní
proudovými	proudový	k2eAgInPc7d1	proudový
motory	motor	k1gInPc7	motor
Rolls-Royce	Rolls-Royce	k1gMnSc1	Rolls-Royce
Soar	Soar	k1gMnSc1	Soar
<g/>
,	,	kIx,	,
vyvíjenými	vyvíjený	k2eAgInPc7d1	vyvíjený
pro	pro	k7c4	pro
bezpilotní	bezpilotní	k2eAgInPc4d1	bezpilotní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
pokusně	pokusně	k6eAd1	pokusně
instalovanými	instalovaný	k2eAgFnPc7d1	instalovaná
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
denní	denní	k2eAgFnSc2d1	denní
stíhací	stíhací	k2eAgFnSc2d1	stíhací
verze	verze	k1gFnSc2	verze
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
také	také	k9	také
dvoumístná	dvoumístný	k2eAgFnSc1d1	dvoumístná
cvičná	cvičný	k2eAgFnSc1d1	cvičná
varianta	varianta	k1gFnSc1	varianta
Meteor	meteor	k1gInSc1	meteor
T	T	kA	T
Mk	Mk	k1gFnSc1	Mk
<g/>
.7	.7	k4	.7
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgInSc1d1	stíhací
průzkumný	průzkumný	k2eAgInSc4d1	průzkumný
Meteor	meteor	k1gInSc4	meteor
FR	fr	k0	fr
Mk	Mk	k1gFnSc7	Mk
<g/>
.9	.9	k4	.9
s	s	k7c7	s
fotopřístroji	fotopřístroj	k1gInPc7	fotopřístroj
v	v	k7c6	v
přídi	příď	k1gFnSc6	příď
a	a	k8xC	a
dvoumístná	dvoumístný	k2eAgFnSc1d1	dvoumístná
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
stíhání	stíhání	k1gNnSc4	stíhání
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
za	za	k7c2	za
ztížených	ztížený	k2eAgFnPc2d1	ztížená
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc4	meteor
NF	NF	kA	NF
<g/>
.14	.14	k4	.14
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
motory	motor	k1gInPc7	motor
Rolls-Royce	Rolls-Royce	k1gFnSc1	Rolls-Royce
Derwent	Derwent	k1gInSc1	Derwent
8	[number]	k4	8
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
po	po	k7c4	po
16,26	[number]	k4	16,26
kN	kN	k?	kN
<g/>
,	,	kIx,	,
vybavená	vybavený	k2eAgFnSc1d1	vybavená
radarem	radar	k1gInSc7	radar
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
APS-	APS-	k1gFnSc1	APS-
<g/>
57	[number]	k4	57
<g/>
,	,	kIx,	,
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
označovaným	označovaný	k2eAgMnSc7d1	označovaný
jako	jako	k9	jako
AI	AI	kA	AI
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
</s>
<s>
Letouny	letoun	k1gInPc1	letoun
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k6eAd1	rovněž
vybaveny	vybavit	k5eAaPmNgInP	vybavit
vzad	vzad	k6eAd1	vzad
odsouvatelným	odsouvatelný	k2eAgInSc7d1	odsouvatelný
krytem	kryt	k1gInSc7	kryt
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
odklápěný	odklápěný	k2eAgInSc4d1	odklápěný
kryt	kryt	k1gInSc4	kryt
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
výztuhami	výztuha	k1gFnPc7	výztuha
<g/>
,	,	kIx,	,
převzatý	převzatý	k2eAgInSc1d1	převzatý
z	z	k7c2	z
T	T	kA	T
Mk	Mk	k1gFnSc2	Mk
<g/>
.7	.7	k4	.7
a	a	k8xC	a
křidélka	křidélko	k1gNnSc2	křidélko
s	s	k7c7	s
pružinovými	pružinový	k2eAgFnPc7d1	pružinová
odlehčovacími	odlehčovací	k2eAgFnPc7d1	odlehčovací
ploškami	ploška	k1gFnPc7	ploška
<g/>
.	.	kIx.	.
</s>
<s>
NF	NF	kA	NF
<g/>
.14	.14	k4	.14
zůstal	zůstat	k5eAaPmAgMnS	zůstat
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgInSc2	první
letu	let	k1gInSc2	let
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1954	[number]	k4	1954
v	v	k7c6	v
prvoliniové	prvoliniový	k2eAgFnSc6d1	prvoliniový
službě	služba	k1gFnSc6	služba
RAF	raf	k0	raf
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
60	[number]	k4	60
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Seletar	Seletara	k1gFnPc2	Seletara
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
přezbrojena	přezbrojen	k2eAgFnSc1d1	přezbrojen
na	na	k7c4	na
letouny	letoun	k1gInPc4	letoun
Gloster	Glostra	k1gFnPc2	Glostra
Javelin	Javelina	k1gFnPc2	Javelina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
noční	noční	k2eAgFnSc2d1	noční
stíhací	stíhací	k2eAgFnSc2d1	stíhací
verze	verze	k1gFnSc2	verze
NF	NF	kA	NF
<g/>
.14	.14	k4	.14
tvořily	tvořit	k5eAaImAgInP	tvořit
Meteory	meteor	k1gInPc1	meteor
NF	NF	kA	NF
Mk	Mk	k1gFnSc1	Mk
<g/>
.11	.11	k4	.11
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
pomocí	pomocí	k7c2	pomocí
minimálních	minimální	k2eAgFnPc2d1	minimální
úprav	úprava	k1gFnPc2	úprava
cvičného	cvičný	k2eAgNnSc2d1	cvičné
provedení	provedení	k1gNnSc2	provedení
T	T	kA	T
Mk	Mk	k1gFnSc2	Mk
<g/>
.7	.7	k4	.7
zástavbou	zástavba	k1gFnSc7	zástavba
radaru	radar	k1gInSc2	radar
AI	AI	kA	AI
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zabral	zabrat	k5eAaPmAgMnS	zabrat
celou	celý	k2eAgFnSc4d1	celá
příď	příď	k1gFnSc4	příď
letounu	letoun	k1gInSc2	letoun
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
přemístění	přemístění	k1gNnSc3	přemístění
čtyř	čtyři	k4xCgMnPc2	čtyři
20	[number]	k4	20
mm	mm	kA	mm
kanónů	kanón	k1gInPc2	kanón
do	do	k7c2	do
vnějších	vnější	k2eAgFnPc2d1	vnější
částí	část	k1gFnPc2	část
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tyto	tento	k3xDgFnPc1	tento
stroje	stroj	k1gInSc2	stroj
převzala	převzít	k5eAaPmAgFnS	převzít
29	[number]	k4	29
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tropikalizací	tropikalizace	k1gFnSc7	tropikalizace
typu	typ	k1gInSc2	typ
NF	NF	kA	NF
<g/>
.11	.11	k4	.11
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
varianta	varianta	k1gFnSc1	varianta
NF	NF	kA	NF
Mk	Mk	k1gFnSc1	Mk
<g/>
.13	.13	k4	.13
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
NF	NF	kA	NF
Mk	Mk	k1gFnSc1	Mk
<g/>
.12	.12	k4	.12
již	již	k9	již
nesla	nést	k5eAaImAgFnS	nést
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
přídi	příď	k1gFnSc6	příď
radar	radar	k1gInSc1	radar
APS-	APS-	k1gFnSc1	APS-
<g/>
57	[number]	k4	57
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nasazení	nasazení	k1gNnSc1	nasazení
==	==	k?	==
</s>
</p>
<p>
<s>
Proudové	proudový	k2eAgInPc1d1	proudový
stíhací	stíhací	k2eAgInPc1d1	stíhací
stroje	stroj	k1gInPc1	stroj
Meteor	meteor	k1gInSc4	meteor
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
užity	užít	k5eAaPmNgFnP	užít
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
a	a	k8xC	a
jen	jen	k9	jen
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
nestřetly	střetnout	k5eNaPmAgInP	střetnout
s	s	k7c7	s
německými	německý	k2eAgFnPc7d1	německá
proudovými	proudový	k2eAgFnPc7d1	proudová
stíhačkami	stíhačka	k1gFnPc7	stíhačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
RAF	raf	k0	raf
přezbrojování	přezbrojování	k1gNnSc3	přezbrojování
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrony	squadron	k1gInPc1	squadron
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
používala	používat	k5eAaImAgFnS	používat
stroje	stroj	k1gInPc1	stroj
Supermarine	Supermarin	k1gInSc5	Supermarin
Spitfire	Spitfir	k1gInSc5	Spitfir
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
IX	IX	kA	IX
<g/>
,	,	kIx,	,
na	na	k7c4	na
Meteory	meteor	k1gInPc4	meteor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
nebojové	bojový	k2eNgInPc1d1	nebojový
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
výcviku	výcvik	k1gInSc3	výcvik
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
následovala	následovat	k5eAaImAgFnS	následovat
dodávka	dodávka	k1gFnSc1	dodávka
pěti	pět	k4xCc2	pět
plně	plně	k6eAd1	plně
bojeschopných	bojeschopný	k2eAgInPc2d1	bojeschopný
proudových	proudový	k2eAgInPc2d1	proudový
stíhacích	stíhací	k2eAgInPc2d1	stíhací
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jimi	on	k3xPp3gInPc7	on
vyzbrojen	vyzbrojen	k2eAgInSc4d1	vyzbrojen
jeden	jeden	k4xCgInSc4	jeden
roj	roj	k1gInSc4	roj
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrony	squadron	k1gInPc1	squadron
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Manstonu	Manston	k1gInSc6	Manston
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Wing	Winga	k1gFnPc2	Winga
Commandera	Commander	k1gMnSc2	Commander
Andrewa	Andrewus	k1gMnSc2	Andrewus
McDowella	McDowell	k1gMnSc2	McDowell
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgInPc1d1	zbývající
dva	dva	k4xCgInPc1	dva
roje	roj	k1gInPc1	roj
používaly	používat	k5eAaImAgInP	používat
nadále	nadále	k6eAd1	nadále
Spitfiry	Spitfira	k1gFnSc2	Spitfira
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
bojovému	bojový	k2eAgInSc3d1	bojový
letu	let	k1gInSc6	let
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
Flying	Flying	k1gInSc1	Flying
Officer	Officer	k1gInSc1	Officer
Mckenzie	Mckenzie	k1gFnSc1	Mckenzie
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
hlídkovat	hlídkovat	k5eAaImF	hlídkovat
nad	nad	k7c7	nad
Kentem	Kent	k1gInSc7	Kent
proti	proti	k7c3	proti
letounovým	letounový	k2eAgFnPc3d1	letounová
střelám	střela	k1gFnPc3	střela
Fieseler	Fieseler	k1gInSc4	Fieseler
Fi-	Fi-	k1gFnPc4	Fi-
<g/>
103	[number]	k4	103
(	(	kIx(	(
<g/>
V-	V-	k1gFnSc1	V-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
držela	držet	k5eAaImAgFnS	držet
od	od	k7c2	od
rána	ráno	k1gNnSc2	ráno
do	do	k7c2	do
večera	večer	k1gInSc2	večer
nepřetržitě	přetržitě	k6eNd1	přetržitě
dvoučlenné	dvoučlenný	k2eAgFnPc1d1	dvoučlenná
hlídky	hlídka	k1gFnPc1	hlídka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
hlídkový	hlídkový	k2eAgInSc4d1	hlídkový
let	let	k1gInSc4	let
první	první	k4xOgFnSc2	první
dvojice	dvojice	k1gFnSc2	dvojice
trval	trvat	k5eAaImAgInS	trvat
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
dvojice	dvojice	k1gFnSc1	dvojice
určená	určený	k2eAgFnSc1d1	určená
ke	k	k7c3	k
střídání	střídání	k1gNnSc3	střídání
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
s	s	k7c7	s
piloty	pilot	k1gInPc7	pilot
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
V-1	V-1	k1gFnSc1	V-1
na	na	k7c6	na
Meteoru	meteor	k1gInSc6	meteor
zničil	zničit	k5eAaPmAgInS	zničit
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
Flying	Flying	k1gInSc1	Flying
Officer	Officer	k1gInSc4	Officer
"	"	kIx"	"
<g/>
Dixie	Dixie	k1gFnSc1	Dixie
<g/>
"	"	kIx"	"
Dean	Dean	k1gInSc1	Dean
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
selhání	selhání	k1gNnSc6	selhání
kanónů	kanón	k1gInPc2	kanón
odklonil	odklonit	k5eAaPmAgInS	odklonit
levým	levý	k2eAgInSc7d1	levý
koncem	konec	k1gInSc7	konec
křídla	křídlo	k1gNnSc2	křídlo
letounovou	letounový	k2eAgFnSc4d1	letounová
pumu	puma	k1gFnSc4	puma
do	do	k7c2	do
vývrtky	vývrtka	k1gFnSc2	vývrtka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
sestřelil	sestřelit	k5eAaPmAgInS	sestřelit
Flying	Flying	k1gInSc1	Flying
Officer	Officer	k1gMnSc1	Officer
J.	J.	kA	J.
Roger	Roger	k1gMnSc1	Roger
V-1	V-1	k1gFnSc2	V-1
palubními	palubní	k2eAgInPc7d1	palubní
kanóny	kanón	k1gInPc7	kanón
Hispano	Hispana	k1gFnSc5	Hispana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
byla	být	k5eAaImAgFnS	být
squadrona	squadrona	k1gFnSc1	squadrona
kompletně	kompletně	k6eAd1	kompletně
přezbrojena	přezbrojit	k5eAaPmNgFnS	přezbrojit
na	na	k7c4	na
Meteory	meteor	k1gInPc4	meteor
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
jednotka	jednotka	k1gFnSc1	jednotka
první	první	k4xOgFnSc4	první
ztrátu	ztráta	k1gFnSc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
nouzové	nouzový	k2eAgNnSc4d1	nouzové
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Great	Great	k1gInSc4	Great
Chart	charta	k1gFnPc2	charta
u	u	k7c2	u
Ashfordu	Ashford	k1gInSc2	Ashford
smrtelně	smrtelně	k6eAd1	smrtelně
havaroval	havarovat	k5eAaPmAgMnS	havarovat
Flight	Flight	k2eAgMnSc1d1	Flight
Sergeant	Sergeant	k1gMnSc1	Sergeant
D.	D.	kA	D.
Gregg	Gregg	k1gMnSc1	Gregg
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
třináctou	třináctý	k4xOgFnSc4	třináctý
a	a	k8xC	a
poslední	poslední	k2eAgFnSc6d1	poslední
V-1	V-1	k1gFnSc6	V-1
na	na	k7c6	na
Meteoru	meteor	k1gInSc6	meteor
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrony	squadron	k1gInPc4	squadron
Flying	Flying	k1gInSc1	Flying
Officer	Officer	k1gMnSc1	Officer
Miller	Miller	k1gMnSc1	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
vyslala	vyslat	k5eAaPmAgFnS	vyslat
čtyři	čtyři	k4xCgInPc4	čtyři
Meteory	meteor	k1gInPc4	meteor
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Debdenu	Debdeno	k1gNnSc6	Debdeno
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Essex	Essex	k1gInSc4	Essex
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
bojových	bojový	k2eAgFnPc2d1	bojová
zkoušek	zkouška	k1gFnPc2	zkouška
USAAF	USAAF	kA	USAAF
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
tohoto	tento	k3xDgNnSc2	tento
cvičení	cvičení	k1gNnSc2	cvičení
bylo	být	k5eAaImAgNnS	být
pomoci	pomoct	k5eAaPmF	pomoct
americkým	americký	k2eAgMnPc3d1	americký
stíhacím	stíhací	k2eAgMnPc3d1	stíhací
pilotům	pilot	k1gMnPc3	pilot
na	na	k7c6	na
letounech	letoun	k1gInPc6	letoun
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
P-51	P-51	k1gMnSc1	P-51
Mustang	mustang	k1gMnSc1	mustang
vyvinout	vyvinout	k5eAaPmF	vyvinout
taktiku	taktika	k1gFnSc4	taktika
proti	proti	k7c3	proti
německým	německý	k2eAgInPc3d1	německý
proudovým	proudový	k2eAgInPc3d1	proudový
strojům	stroj	k1gInPc3	stroj
Messerschmitt	Messerschmittum	k1gNnPc2	Messerschmittum
Me	Me	k1gFnSc1	Me
262	[number]	k4	262
a	a	k8xC	a
poskytnout	poskytnout	k5eAaPmF	poskytnout
střelcům	střelec	k1gMnPc3	střelec
bombardérů	bombardér	k1gInPc2	bombardér
praxi	praxe	k1gFnSc4	praxe
v	v	k7c6	v
zaměřování	zaměřování	k1gNnSc6	zaměřování
rychle	rychle	k6eAd1	rychle
letících	letící	k2eAgInPc2d1	letící
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
začala	začít	k5eAaPmAgFnS	začít
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
postupně	postupně	k6eAd1	postupně
přebírat	přebírat	k5eAaImF	přebírat
modernější	moderní	k2eAgInPc4d2	modernější
Meteory	meteor	k1gInPc4	meteor
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
pak	pak	k6eAd1	pak
jednotka	jednotka	k1gFnSc1	jednotka
odeslala	odeslat	k5eAaPmAgFnS	odeslat
tři	tři	k4xCgInPc4	tři
Meteory	meteor	k1gInPc4	meteor
Mk	Mk	k1gFnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Melsbroek	Melsbroky	k1gFnPc2	Melsbroky
u	u	k7c2	u
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
84	[number]	k4	84
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
2	[number]	k4	2
<g/>
.	.	kIx.	.
taktické	taktický	k2eAgFnSc2d1	taktická
letecké	letecký	k2eAgFnSc2d1	letecká
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
TAF	TAF	kA	TAF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
Meteory	meteor	k1gInPc1	meteor
přemístily	přemístit	k5eAaPmAgInP	přemístit
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
do	do	k7c2	do
Gilze	Gilze	k1gFnSc2	Gilze
Rijen	Rijna	k1gFnPc2	Rijna
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
ke	k	k7c3	k
zbytku	zbytek	k1gInSc3	zbytek
squadrony	squadron	k1gInPc4	squadron
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
útvar	útvar	k1gInSc1	útvar
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Kluis	Kluis	k1gFnSc2	Kluis
u	u	k7c2	u
Nijmegenu	Nijmegen	k1gInSc2	Nijmegen
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
Quakenbrucku	Quakenbruck	k1gInSc2	Quakenbruck
u	u	k7c2	u
Brém	Brémy	k1gFnPc2	Brémy
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Fassburgu	Fassburg	k1gInSc2	Fassburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
jednotka	jednotka	k1gFnSc1	jednotka
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
další	další	k2eAgFnSc4d1	další
ztrátu	ztráta	k1gFnSc4	ztráta
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
dvou	dva	k4xCgMnPc2	dva
jejích	její	k3xOp3gInPc2	její
Meteorů	meteor	k1gInPc2	meteor
s	s	k7c7	s
piloty	pilot	k1gInPc7	pilot
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Ldr	Ldr	k1gMnSc2	Ldr
Wattse	Watts	k1gMnSc2	Watts
a	a	k8xC	a
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
Sgt	Sgt	k1gMnSc1	Sgt
Carmella	Carmella	k1gMnSc1	Carmella
při	při	k7c6	při
letu	let	k1gInSc6	let
v	v	k7c6	v
sevřené	sevřený	k2eAgFnSc6d1	sevřená
formaci	formace	k1gFnSc6	formace
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	s	k7c7	s
616	[number]	k4	616
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
přemístila	přemístit	k5eAaPmAgFnS	přemístit
do	do	k7c2	do
Lüneburgu	Lüneburg	k1gInSc2	Lüneburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
zastihl	zastihnout	k5eAaPmAgInS	zastihnout
rozkaz	rozkaz	k1gInSc1	rozkaz
o	o	k7c4	o
zastavení	zastavení	k1gNnSc4	zastavení
veškerých	veškerý	k3xTgFnPc2	veškerý
útočných	útočný	k2eAgFnPc2d1	útočná
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
přezbrojila	přezbrojit	k5eAaPmAgFnS	přezbrojit
na	na	k7c4	na
Meteory	meteor	k1gInPc4	meteor
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
druhá	druhý	k4xOgFnSc1	druhý
jednotka	jednotka	k1gFnSc1	jednotka
RAF	raf	k0	raf
<g/>
,	,	kIx,	,
504	[number]	k4	504
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
se	s	k7c7	s
základnou	základna	k1gFnSc7	základna
v	v	k7c6	v
Colerne	Colern	k1gInSc5	Colern
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
tři	tři	k4xCgInPc1	tři
Meteory	meteor	k1gInPc1	meteor
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
obdržela	obdržet	k5eAaPmAgFnS	obdržet
také	také	k9	také
541	[number]	k4	541
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
dálkového	dálkový	k2eAgInSc2d1	dálkový
průzkumu	průzkum	k1gInSc2	průzkum
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
Spitfiry	Spitfira	k1gFnPc1	Spitfira
PR	pr	k0	pr
19	[number]	k4	19
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
kompletně	kompletně	k6eAd1	kompletně
přezbrojena	přezbrojit	k5eAaPmNgFnS	přezbrojit
na	na	k7c4	na
vhodnější	vhodný	k2eAgInSc4d2	vhodnější
fotoprůzkumný	fotoprůzkumný	k2eAgInSc4d1	fotoprůzkumný
neozbrojený	ozbrojený	k2eNgInSc4d1	neozbrojený
typ	typ	k1gInSc4	typ
Meteor	meteor	k1gInSc1	meteor
PR	pr	k0	pr
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meteory	meteor	k1gInPc4	meteor
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
americkými	americký	k2eAgFnPc7d1	americká
P-80	P-80	k1gFnPc7	P-80
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
delšího	dlouhý	k2eAgNnSc2d2	delší
pokračování	pokračování	k1gNnSc2	pokračování
války	válka	k1gFnSc2	válka
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
jistě	jistě	k9	jistě
ke	k	k7c3	k
konfrontaci	konfrontace	k1gFnSc3	konfrontace
s	s	k7c7	s
německými	německý	k2eAgInPc7d1	německý
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
Mk	Mk	k1gFnSc1	Mk
<g/>
.4	.4	k4	.4
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
nadějný	nadějný	k2eAgInSc1d1	nadějný
stroj	stroj	k1gInSc1	stroj
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
do	do	k7c2	do
států	stát	k1gInPc2	stát
jako	jako	k9	jako
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
koupila	koupit	k5eAaPmAgFnS	koupit
100	[number]	k4	100
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
výcvik	výcvik	k1gInSc4	výcvik
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
prvních	první	k4xOgInPc6	první
proudových	proudový	k2eAgInPc6d1	proudový
letounech	letoun	k1gInPc6	letoun
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
s	s	k7c7	s
britskými	britský	k2eAgMnPc7d1	britský
letouny	letoun	k1gMnPc7	letoun
Gloster	Gloster	k1gInSc4	Gloster
Meteor	meteor	k1gInSc1	meteor
T	T	kA	T
<g/>
.7	.7	k4	.7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
operační	operační	k2eAgFnSc1d1	operační
proudová	proudový	k2eAgFnSc1d1	proudová
jednotka	jednotka	k1gFnSc1	jednotka
Kongelige	Kongelige	k1gFnSc1	Kongelige
Danske	Danske	k1gFnSc1	Danske
Flyvevå	Flyvevå	k1gFnSc1	Flyvevå
s	s	k7c7	s
Meteory	meteor	k1gInPc7	meteor
F.	F.	kA	F.
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
dvacítkou	dvacítka	k1gFnSc7	dvacítka
Meteorů	meteor	k1gInPc2	meteor
F.	F.	kA	F.
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
Eskadrillerne	Eskadrillern	k1gInSc5	Eskadrillern
byly	být	k5eAaImAgFnP	být
dislokovány	dislokovat	k5eAaBmNgFnP	dislokovat
v	v	k7c6	v
Kastrupu	Kastrup	k1gInSc6	Kastrup
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
ukázala	ukázat	k5eAaPmAgFnS	ukázat
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
jednotka	jednotka	k1gFnSc1	jednotka
stíhacích	stíhací	k2eAgInPc2d1	stíhací
letounů	letoun	k1gInPc2	letoun
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1952	[number]	k4	1952
proto	proto	k8xC	proto
dánské	dánský	k2eAgNnSc4d1	dánské
letectvo	letectvo	k1gNnSc4	letectvo
převzalo	převzít	k5eAaPmAgNnS	převzít
první	první	k4xOgNnSc1	první
Meteor	meteor	k1gInSc4	meteor
NF	NF	kA	NF
<g/>
.11	.11	k4	.11
<g/>
,	,	kIx,	,
během	během	k7c2	během
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
dodán	dodán	k2eAgInSc1d1	dodán
zbytek	zbytek	k1gInSc1	zbytek
objednaných	objednaný	k2eAgInPc2d1	objednaný
strojů	stroj	k1gInPc2	stroj
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
uživatelem	uživatel	k1gMnSc7	uživatel
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
argentinské	argentinský	k2eAgNnSc1d1	argentinské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Splnění	splnění	k1gNnSc1	splnění
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnPc4d1	velká
objednávky	objednávka	k1gFnPc4	objednávka
představovalo	představovat	k5eAaImAgNnS	představovat
pro	pro	k7c4	pro
výrobce	výrobce	k1gMnPc4	výrobce
kapacitní	kapacitní	k2eAgInSc4d1	kapacitní
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
zpětným	zpětný	k2eAgInSc7d1	zpětný
odkupem	odkup	k1gInSc7	odkup
padesáti	padesát	k4xCc2	padesát
strojů	stroj	k1gInPc2	stroj
z	z	k7c2	z
přebytků	přebytek	k1gInPc2	přebytek
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
letouny	letoun	k1gInPc1	letoun
prošly	projít	k5eAaPmAgInP	projít
generálkou	generálka	k1gFnSc7	generálka
a	a	k8xC	a
staly	stát	k5eAaPmAgInP	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
dodávky	dodávka	k1gFnPc1	dodávka
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc1d1	zbylý
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
letouny	letoun	k1gInPc1	letoun
začaly	začít	k5eAaPmAgInP	začít
přicházet	přicházet	k5eAaImF	přicházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
jednotka	jednotka	k1gFnSc1	jednotka
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Argentinské	argentinský	k2eAgInPc1d1	argentinský
Meteory	meteor	k1gInPc1	meteor
se	se	k3xPyFc4	se
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
FAA	FAA	kA	FAA
udržely	udržet	k5eAaPmAgInP	udržet
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meteory	meteor	k1gInPc1	meteor
F	F	kA	F
Mk	Mk	k1gFnSc7	Mk
<g/>
.8	.8	k4	.8
a	a	k8xC	a
T	T	kA	T
Mk	Mk	k1gFnSc1	Mk
<g/>
.7	.7	k4	.7
byly	být	k5eAaImAgFnP	být
77	[number]	k4	77
<g/>
.	.	kIx.	.
squadronou	squadrona	k1gFnSc7	squadrona
RAAF	RAAF	kA	RAAF
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c4	v
Korejské	korejský	k2eAgInPc4d1	korejský
válce	válec	k1gInPc4	válec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
bojoval	bojovat	k5eAaImAgInS	bojovat
proti	proti	k7c3	proti
sovětským	sovětský	k2eAgInPc3d1	sovětský
proudovým	proudový	k2eAgInPc3d1	proudový
letounům	letoun	k1gInPc3	letoun
MiG-	MiG-	k1gMnSc2	MiG-
<g/>
15	[number]	k4	15
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
Meteory	meteor	k1gInPc1	meteor
77	[number]	k4	77
<g/>
.	.	kIx.	.
squadrona	squadrona	k1gFnSc1	squadrona
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Dicka	Dicek	k1gMnSc2	Dicek
Cresswella	Cresswell	k1gMnSc2	Cresswell
začala	začít	k5eAaPmAgFnS	začít
přebírat	přebírat	k5eAaImF	přebírat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
dosavadními	dosavadní	k2eAgInPc7d1	dosavadní
letouny	letoun	k1gInPc7	letoun
Mustang	mustang	k1gMnSc1	mustang
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
Pusanu	Pusan	k1gInSc2	Pusan
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Iwakuni	Iwakueň	k1gFnSc3	Iwakueň
<g/>
.	.	kIx.	.
</s>
<s>
Gloster	Gloster	k1gInSc1	Gloster
Meteory	meteor	k1gInPc1	meteor
dorazily	dorazit	k5eAaPmAgInP	dorazit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
na	na	k7c6	na
letadlové	letadlový	k2eAgFnSc6d1	letadlová
lodi	loď	k1gFnSc6	loď
HMS	HMS	kA	HMS
Warrior	Warrior	k1gInSc1	Warrior
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
skóre	skóre	k1gNnSc1	skóre
77	[number]	k4	77
<g/>
.	.	kIx.	.
squadrony	squadron	k1gInPc1	squadron
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
čtyřech	čtyři	k4xCgMnPc6	čtyři
sestřelů	sestřel	k1gInPc2	sestřel
MiGů-	MiGů-	k1gFnSc4	MiGů-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poprvé	poprvé	k6eAd1	poprvé
skóroval	skórovat	k5eAaBmAgMnS	skórovat
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
O	o	k7c6	o
Bruce	Bruka	k1gFnSc6	Bruka
Gogerly	Gogerla	k1gFnSc2	Gogerla
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Elyana	Elyana	k1gFnSc1	Elyana
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1951	[number]	k4	1951
a	a	k8xC	a
o	o	k7c6	o
poslední	poslední	k2eAgFnSc6d1	poslední
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Sgt	Sgt	k1gMnSc1	Sgt
George	Georg	k1gMnSc2	Georg
Hale	hala	k1gFnSc6	hala
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
na	na	k7c6	na
F	F	kA	F
Mk	Mk	k1gFnSc1	Mk
<g/>
.8	.8	k4	.8
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
851	[number]	k4	851
<g/>
)	)	kIx)	)
pojmenovaném	pojmenovaný	k2eAgMnSc6d1	pojmenovaný
"	"	kIx"	"
<g/>
Halestorm	Halestorm	k1gInSc4	Halestorm
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
Meteory	meteor	k1gInPc7	meteor
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
i	i	k9	i
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1955	[number]	k4	1955
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
dva	dva	k4xCgInPc1	dva
izraelské	izraelský	k2eAgInPc1d1	izraelský
Meteory	meteor	k1gInPc1	meteor
nad	nad	k7c7	nad
pouští	poušť	k1gFnSc7	poušť
Negev	Negev	k1gFnSc4	Negev
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
egyptské	egyptský	k2eAgFnPc4d1	egyptská
stíhačky	stíhačka	k1gFnPc4	stíhačka
Vampire	Vampir	k1gInSc5	Vampir
<g/>
.	.	kIx.	.
</s>
<s>
Izraelským	izraelský	k2eAgMnPc3d1	izraelský
pilotům	pilot	k1gMnPc3	pilot
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
egyptských	egyptský	k2eAgInPc2d1	egyptský
strojů	stroj	k1gInPc2	stroj
poškodit	poškodit	k5eAaPmF	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1955	[number]	k4	1955
byly	být	k5eAaImAgInP	být
čtyři	čtyři	k4xCgInPc1	čtyři
egyptské	egyptský	k2eAgInPc1d1	egyptský
stroje	stroj	k1gInPc1	stroj
Vampire	Vampir	k1gInSc5	Vampir
spatřeny	spatřen	k2eAgInPc1d1	spatřen
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
překročily	překročit	k5eAaPmAgFnP	překročit
izraelsko-egyptskou	izraelskogyptský	k2eAgFnSc4d1	izraelsko-egyptská
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
poslal	poslat	k5eAaPmAgMnS	poslat
dva	dva	k4xCgInPc4	dva
Meteory	meteor	k1gInPc4	meteor
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
F.	F.	kA	F.
<g/>
8	[number]	k4	8
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
FR.	FR.	k1gMnSc1	FR.
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
párem	pár	k1gInSc7	pár
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
stíhaček	stíhačka	k1gFnPc2	stíhačka
se	se	k3xPyFc4	se
kapitán	kapitán	k1gMnSc1	kapitán
Aaron	Aaron	k1gMnSc1	Aaron
Yoali	Yoale	k1gFnSc4	Yoale
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
zezadu	zezadu	k6eAd1	zezadu
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
dvojici	dvojice	k1gFnSc3	dvojice
strojů	stroj	k1gInPc2	stroj
Vampire	Vampir	k1gInSc5	Vampir
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
400	[number]	k4	400
m	m	kA	m
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
<g/>
.	.	kIx.	.
</s>
<s>
Yoali	Yoal	k1gMnPc1	Yoal
<g/>
,	,	kIx,	,
krytý	krytý	k2eAgInSc1d1	krytý
kapitánem	kapitán	k1gMnSc7	kapitán
Jo	jo	k9	jo
<g/>
'	'	kIx"	'
<g/>
ašem	ašem	k6eAd1	ašem
Cidonem	Cidon	k1gInSc7	Cidon
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
druhého	druhý	k4xOgInSc2	druhý
egyptského	egyptský	k2eAgInSc2d1	egyptský
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
podařilo	podařit	k5eAaPmAgNnS	podařit
sestřelit	sestřelit	k5eAaPmF	sestřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1956	[number]	k4	1956
se	se	k3xPyFc4	se
izraelská	izraelský	k2eAgFnSc1d1	izraelská
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
egyptské	egyptský	k2eAgFnSc2d1	egyptská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
delegace	delegace	k1gFnSc2	delegace
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
delegace	delegace	k1gFnSc1	delegace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
egyptského	egyptský	k2eAgMnSc2d1	egyptský
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
letadle	letadlo	k1gNnSc6	letadlo
Iljušin	iljušin	k1gInSc1	iljušin
Il-	Il-	k1gFnSc2	Il-
<g/>
14	[number]	k4	14
vracet	vracet	k5eAaImF	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3	[number]	k4	3
000	[number]	k4	000
m	m	kA	m
nad	nad	k7c7	nad
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
se	se	k3xPyFc4	se
k	k	k7c3	k
Il-	Il-	k1gFnSc3	Il-
<g/>
14	[number]	k4	14
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
dvě	dva	k4xCgFnPc4	dva
izraelské	izraelský	k2eAgFnPc4d1	izraelská
stíhačky	stíhačka	k1gFnPc4	stíhačka
Meteor	meteor	k1gInSc1	meteor
NF	NF	kA	NF
<g/>
.13	.13	k4	.13
a	a	k8xC	a
egyptské	egyptský	k2eAgNnSc4d1	egyptské
dopravní	dopravní	k2eAgNnSc4d1	dopravní
letadlo	letadlo	k1gNnSc4	letadlo
sestřelily	sestřelit	k5eAaPmAgFnP	sestřelit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
akce	akce	k1gFnSc1	akce
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
cíl	cíl	k1gInSc1	cíl
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
se	se	k3xPyFc4	se
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgMnSc1d1	egyptský
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
prodloužit	prodloužit	k5eAaPmF	prodloužit
a	a	k8xC	a
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sestřeleného	sestřelený	k2eAgNnSc2d1	sestřelené
letadla	letadlo	k1gNnSc2	letadlo
se	se	k3xPyFc4	se
nenacházel	nacházet	k5eNaImAgMnS	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
arabsko-izraelské	arabskozraelský	k2eAgFnSc2d1	arabsko-izraelská
války	válka	k1gFnSc2	válka
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1956	[number]	k4	1956
zničily	zničit	k5eAaPmAgInP	zničit
Meteory	meteor	k1gInPc1	meteor
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
radarovou	radarový	k2eAgFnSc4d1	radarová
stanici	stanice	k1gFnSc4	stanice
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
dobývání	dobývání	k1gNnSc6	dobývání
pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
a	a	k8xC	a
zničení	zničení	k1gNnSc2	zničení
8	[number]	k4	8
<g/>
.	.	kIx.	.
palestinské	palestinský	k2eAgFnSc2d1	palestinská
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uživatelé	uživatel	k1gMnPc1	uživatel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
uživatelé	uživatel	k1gMnPc1	uživatel
===	===	k?	===
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
Argentina	Argentina	k1gFnSc1	Argentina
</s>
</p>
<p>
<s>
Argentinské	argentinský	k2eAgNnSc1d1	argentinské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
Austrálie	Austrálie	k1gFnSc2	Austrálie
</s>
</p>
<p>
<s>
Royal	Royal	k1gMnSc1	Royal
Australian	Australian	k1gMnSc1	Australian
Air	Air	k1gMnSc1	Air
Force	force	k1gFnSc4	force
</s>
</p>
<p>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
Belgie	Belgie	k1gFnSc1	Belgie
</s>
</p>
<p>
<s>
Belgické	belgický	k2eAgNnSc1d1	Belgické
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Biafra	Biafra	k1gMnSc1	Biafra
Biafra	Biafra	k1gMnSc1	Biafra
</s>
</p>
<p>
<s>
Biaferské	Biaferský	k2eAgNnSc1d1	Biaferský
letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
2	[number]	k4	2
Meteory	meteor	k1gInPc1	meteor
NF	NF	kA	NF
<g/>
.14	.14	k4	.14
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dopravit	dopravit	k5eAaPmF	dopravit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
Brazilské	brazilský	k2eAgNnSc1d1	brazilské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
Dánsko	Dánsko	k1gNnSc1	Dánsko
</s>
</p>
<p>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
království	království	k1gNnSc1	království
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
</s>
</p>
<p>
<s>
Ekvádorské	ekvádorský	k2eAgNnSc1d1	ekvádorské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
Francie	Francie	k1gFnSc2	Francie
</s>
</p>
<p>
<s>
Armée	Armée	k1gFnSc1	Armée
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
air	air	k?	air
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
Izrael	Izrael	k1gInSc1	Izrael
</s>
</p>
<p>
<s>
Izraelské	izraelský	k2eAgNnSc1d1	izraelské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
unie	unie	k1gFnSc1	unie
</s>
</p>
<p>
<s>
South	South	k1gMnSc1	South
African	African	k1gMnSc1	African
Air	Air	k1gMnSc1	Air
Force	force	k1gFnSc4	force
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Royal	Royal	k1gMnSc1	Royal
Canadian	Canadian	k1gMnSc1	Canadian
Air	Air	k1gMnSc1	Air
Force	force	k1gFnSc1	force
(	(	kIx(	(
<g/>
1	[number]	k4	1
×	×	k?	×
Meteor	meteor	k1gInSc1	meteor
F.	F.	kA	F.
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
×	×	k?	×
Meteor	meteor	k1gInSc1	meteor
T	T	kA	T
<g/>
.7	.7	k4	.7
-	-	kIx~	-
jen	jen	k6eAd1	jen
testy	test	k1gInPc1	test
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Luftwaffe	Luftwaff	k1gMnSc5	Luftwaff
(	(	kIx(	(
<g/>
Meteor	meteor	k1gInSc4	meteor
TT	TT	kA	TT
<g/>
.20	.20	k4	.20
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
</s>
</p>
<p>
<s>
Nizozemské	nizozemský	k2eAgNnSc1d1	Nizozemské
královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Nizozemské	nizozemský	k2eAgNnSc1d1	Nizozemské
námořní	námořní	k2eAgNnSc1d1	námořní
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
Norsko	Norsko	k1gNnSc1	Norsko
</s>
</p>
<p>
<s>
Norské	norský	k2eAgNnSc1d1	norské
královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
</s>
</p>
<p>
<s>
Royal	Royal	k1gMnSc1	Royal
New	New	k1gMnSc1	New
Zealand	Zealanda	k1gFnPc2	Zealanda
Air	Air	k1gMnSc1	Air
Force	force	k1gFnSc2	force
</s>
</p>
<p>
<s>
Sýrie	Sýrie	k1gFnSc1	Sýrie
Sýrie	Sýrie	k1gFnSc1	Sýrie
</s>
</p>
<p>
<s>
Syrské	syrský	k2eAgFnPc1d1	Syrská
arabské	arabský	k2eAgFnPc1d1	arabská
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
</s>
</p>
<p>
<s>
Fleet	Fleet	k1gMnSc1	Fleet
Air	Air	k1gMnSc1	Air
Arm	Arm	k1gMnSc1	Arm
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
</s>
</p>
<p>
<s>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Army	Arma	k1gFnSc2	Arma
Air	Air	k1gMnSc1	Air
Forces	Forces	k1gInSc1	Forces
(	(	kIx(	(
<g/>
testy	testa	k1gFnSc2	testa
1	[number]	k4	1
kusu	kus	k1gInSc2	kus
F	F	kA	F
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
I	I	kA	I
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Civilní	civilní	k2eAgMnPc1d1	civilní
uživatelé	uživatel	k1gMnPc1	uživatel
===	===	k?	===
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Classic	Classic	k1gMnSc1	Classic
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
</s>
</p>
<p>
<s>
DERA	drát	k5eAaImSgMnS	drát
Llanbedr	Llanbedr	k1gInSc4	Llanbedr
</s>
</p>
<p>
<s>
Flight	Flight	k2eAgInSc1d1	Flight
Refuelling	Refuelling	k1gInSc1	Refuelling
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Martin-Baker	Martin-Baker	k1gInSc1	Martin-Baker
Aircraft	Aircrafta	k1gFnPc2	Aircrafta
Company	Compana	k1gFnSc2	Compana
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
Švédsko	Švédsko	k1gNnSc1	Švédsko
</s>
</p>
<p>
<s>
Svensk	Svensk	k1gInSc1	Svensk
Flygtjänst	Flygtjänst	k1gInSc1	Flygtjänst
AB	AB	kA	AB
(	(	kIx(	(
<g/>
3	[number]	k4	3
×	×	k?	×
Meteor	meteor	k1gInSc1	meteor
T	T	kA	T
<g/>
.7	.7	k4	.7
<g/>
,	,	kIx,	,
4	[number]	k4	4
×	×	k?	×
Meteor	meteor	k1gInSc1	meteor
TT	TT	kA	TT
<g/>
.20	.20	k4	.20
užívané	užívaný	k2eAgInPc1d1	užívaný
jako	jako	k8xC	jako
vlečné	vlečný	k2eAgInPc1d1	vlečný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgInPc1d1	hlavní
technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
F.	F.	kA	F.
<g/>
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
===	===	k?	===
</s>
</p>
<p>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
1	[number]	k4	1
muž	muž	k1gMnSc1	muž
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
<g/>
:	:	kIx,	:
13,11	[number]	k4	13,11
m	m	kA	m
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
12,58	[number]	k4	12,58
m	m	kA	m
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
3,96	[number]	k4	3,96
m	m	kA	m
</s>
</p>
<p>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
<g/>
:	:	kIx,	:
34,74	[number]	k4	34,74
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
vlastního	vlastní	k2eAgInSc2d1	vlastní
letounu	letoun	k1gInSc2	letoun
<g/>
:	:	kIx,	:
4771	[number]	k4	4771
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
6314	[number]	k4	6314
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
2	[number]	k4	2
×	×	k?	×
proudový	proudový	k2eAgInSc1d1	proudový
motor	motor	k1gInSc1	motor
Rolls-Royce	Rolls-Royec	k1gInSc2	Rolls-Royec
W	W	kA	W
<g/>
.2	.2	k4	.2
<g/>
B	B	kA	B
<g/>
/	/	kIx~	/
<g/>
23	[number]	k4	23
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
768	[number]	k4	768
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
13	[number]	k4	13
100	[number]	k4	100
m	m	kA	m
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
<g/>
:	:	kIx,	:
1610	[number]	k4	1610
km	km	kA	km
</s>
</p>
<p>
<s>
===	===	k?	===
Výzbroj	výzbroj	k1gInSc4	výzbroj
===	===	k?	===
</s>
</p>
<p>
<s>
4	[number]	k4	4
×	×	k?	×
kanón	kanón	k1gInSc4	kanón
Hispano	Hispana	k1gFnSc5	Hispana
Mk	Mk	k1gFnSc3	Mk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ráže	ráže	k1gFnPc4	ráže
20	[number]	k4	20
mm	mm	kA	mm
</s>
</p>
<p>
<s>
===	===	k?	===
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
NF	NF	kA	NF
<g/>
.	.	kIx.	.
<g/>
Mk	Mk	k1gFnSc1	Mk
<g/>
.11	.11	k4	.11
===	===	k?	===
</s>
</p>
<p>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
1	[number]	k4	1
muž	muž	k1gMnSc1	muž
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
14,78	[number]	k4	14,78
m	m	kA	m
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
<g/>
:	:	kIx,	:
13,10	[number]	k4	13,10
m	m	kA	m
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
3,96	[number]	k4	3,96
m	m	kA	m
</s>
</p>
<p>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
<g/>
:	:	kIx,	:
34,40	[number]	k4	34,40
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
prázdného	prázdný	k2eAgInSc2d1	prázdný
letounu	letoun	k1gInSc2	letoun
<g/>
:	:	kIx,	:
6300	[number]	k4	6300
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
8960	[number]	k4	8960
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
2	[number]	k4	2
×	×	k?	×
proudový	proudový	k2eAgInSc1d1	proudový
motor	motor	k1gInSc1	motor
Rolls-Royce	Rolls-Royce	k1gMnSc1	Rolls-Royce
Derwent	Derwent	k1gMnSc1	Derwent
8	[number]	k4	8
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
931	[number]	k4	931
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
13	[number]	k4	13
400	[number]	k4	400
m	m	kA	m
</s>
</p>
<p>
<s>
Stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
:	:	kIx,	:
28,20	[number]	k4	28,20
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
<g/>
:	:	kIx,	:
1470	[number]	k4	1470
km	km	kA	km
</s>
</p>
<p>
<s>
===	===	k?	===
Výzbroj	výzbroj	k1gInSc4	výzbroj
===	===	k?	===
</s>
</p>
<p>
<s>
4	[number]	k4	4
×	×	k?	×
kanón	kanón	k1gInSc1	kanón
ráže	ráže	k1gFnSc2	ráže
20	[number]	k4	20
mm	mm	kA	mm
</s>
</p>
<p>
<s>
Neřízené	řízený	k2eNgFnPc1d1	neřízená
rakety	raketa	k1gFnPc1	raketa
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VÁLKA	Válka	k1gMnSc1	Válka
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
.	.	kIx.	.
</s>
<s>
Stíhací	stíhací	k2eAgNnPc1d1	stíhací
letadla	letadlo	k1gNnPc1	letadlo
<g/>
:	:	kIx,	:
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
/	/	kIx~	/
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
-	-	kIx~	-
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
110	[number]	k4	110
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DONALD	Donald	k1gMnSc1	Donald
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
proudové	proudový	k2eAgInPc1d1	proudový
letouny	letoun	k1gInPc1	letoun
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
<g/>
&	&	k?	&
<g/>
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7237	[number]	k4	7237
<g/>
-	-	kIx~	-
<g/>
135	[number]	k4	135
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GENF	GENF	kA	GENF
<g/>
,	,	kIx,	,
S.	S.	kA	S.
A.	A.	kA	A.
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ivanka	Ivanka	k1gFnSc1	Ivanka
pri	pri	k?	pri
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
:	:	kIx,	:
Slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85711	[number]	k4	85711
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
108	[number]	k4	108
<g/>
,	,	kIx,	,
156	[number]	k4	156
<g/>
,	,	kIx,	,
196	[number]	k4	196
<g/>
,	,	kIx,	,
200	[number]	k4	200
<g/>
,	,	kIx,	,
212	[number]	k4	212
<g/>
,	,	kIx,	,
241	[number]	k4	241
a	a	k8xC	a
242	[number]	k4	242
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GUNSTON	GUNSTON	kA	GUNSTON
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
<g/>
.	.	kIx.	.
</s>
<s>
Bojová	bojový	k2eAgNnPc1d1	bojové
letadla	letadlo	k1gNnPc1	letadlo
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
<g/>
&	&	k?	&
<g/>
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
479	[number]	k4	479
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7237	[number]	k4	7237
<g/>
-	-	kIx~	-
<g/>
203	[number]	k4	203
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ETHELL	ETHELL	kA	ETHELL
<g/>
,	,	kIx,	,
Jefrey	Jefrea	k1gFnSc2	Jefrea
<g/>
;	;	kIx,	;
PRICE	PRICE	kA	PRICE
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
.	.	kIx.	.
</s>
<s>
Trysková	tryskový	k2eAgNnPc1d1	tryskové
letadla	letadlo	k1gNnPc1	letadlo
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7193	[number]	k4	7193
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHMID	SCHMID	kA	SCHMID
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc4	letadlo
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
Stíhací	stíhací	k2eAgNnPc1d1	stíhací
a	a	k8xC	a
bombardovací	bombardovací	k2eAgNnPc1d1	bombardovací
letadla	letadlo	k1gNnPc1	letadlo
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Fraus	fraus	k1gFnSc1	fraus
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85784	[number]	k4	85784
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GREEN	GREEN	kA	GREEN
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
;	;	kIx,	;
SWANBOROUGH	SWANBOROUGH	kA	SWANBOROUGH
<g/>
,	,	kIx,	,
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
.	.	kIx.	.
</s>
<s>
Kamufláže	kamufláž	k1gFnSc2	kamufláž
Vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
<g/>
&	&	k?	&
<g/>
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7237	[number]	k4	7237
<g/>
-	-	kIx~	-
<g/>
438	[number]	k4	438
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JACKSON	JACKSON	kA	JACKSON
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnPc1d1	moderní
vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
190	[number]	k4	190
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
106	[number]	k4	106
<g/>
-	-	kIx~	-
<g/>
109	[number]	k4	109
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BALOUS	BALOUS	kA	BALOUS
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
Mk	Mk	k1gFnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Letectví	letectví	k1gNnSc1	letectví
a	a	k8xC	a
kosmonautika	kosmonautika	k1gFnSc1	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Leden	leden	k1gInSc1	leden
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
74	[number]	k4	74
a	a	k8xC	a
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
1156	[number]	k4	1156
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc4	meteor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kamufláže	kamufláž	k1gFnPc1	kamufláž
letounu	letoun	k1gInSc2	letoun
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
</s>
</p>
<p>
<s>
Fotogalerie	Fotogalerie	k1gFnPc1	Fotogalerie
letounu	letoun	k1gInSc2	letoun
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
</s>
</p>
