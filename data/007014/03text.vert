<s>
Černobílá	černobílý	k2eAgFnSc1d1	černobílá
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
kategorie	kategorie	k1gFnSc1	kategorie
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
skutečné	skutečný	k2eAgInPc1d1	skutečný
odstíny	odstín	k1gInPc1	odstín
barev	barva	k1gFnPc2	barva
objektů	objekt	k1gInPc2	objekt
převedou	převést	k5eAaPmIp3nP	převést
na	na	k7c4	na
škálu	škála	k1gFnSc4	škála
od	od	k7c2	od
černé	černá	k1gFnSc2	černá
po	po	k7c4	po
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc1	historie
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Heliografie	heliografie	k1gFnSc2	heliografie
<g/>
.	.	kIx.	.
</s>
<s>
Heliografie	heliografie	k1gFnSc1	heliografie
nebo	nebo	k8xC	nebo
niepceotypie	niepceotypie	k1gFnSc1	niepceotypie
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
fotografická	fotografický	k2eAgFnSc1d1	fotografická
technika	technika	k1gFnSc1	technika
pro	pro	k7c4	pro
zhotovení	zhotovení	k1gNnSc4	zhotovení
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
reprodukcí	reprodukce	k1gFnPc2	reprodukce
grafických	grafický	k2eAgInPc2d1	grafický
listů	list	k1gInPc2	list
a	a	k8xC	a
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
helios	helios	k1gInSc1	helios
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
grafé	grafý	k2eAgNnSc1d1	grafý
(	(	kIx(	(
<g/>
psaní	psaní	k1gNnSc1	psaní
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
vynálezu	vynález	k1gInSc2	vynález
heliografie	heliografie	k1gFnSc2	heliografie
byl	být	k5eAaImAgMnS	být
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépec	k1gMnSc4	Niépec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
fotochemickou	fotochemický	k2eAgFnSc7d1	fotochemická
cestou	cesta	k1gFnSc7	cesta
reprodukce	reprodukce	k1gFnSc2	reprodukce
pro	pro	k7c4	pro
kamenotisk	kamenotisk	k1gInSc4	kamenotisk
(	(	kIx(	(
<g/>
litografii	litografie	k1gFnSc4	litografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
litografickou	litografický	k2eAgFnSc4d1	litografická
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
vrstvou	vrstva	k1gFnSc7	vrstva
světlocitlivé	světlocitlivý	k2eAgFnSc2d1	světlocitlivá
fermeže	fermež	k1gFnSc2	fermež
kopíroval	kopírovat	k5eAaImAgInS	kopírovat
roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
pomocí	pomocí	k7c2	pomocí
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
kresby	kresba	k1gFnSc2	kresba
a	a	k8xC	a
grafické	grafický	k2eAgInPc4d1	grafický
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
papír	papír	k1gInSc1	papír
byl	být	k5eAaImAgInS	být
zprůsvitněn	zprůsvitnit	k5eAaPmNgInS	zprůsvitnit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
použil	použít	k5eAaPmAgInS	použít
malou	malý	k2eAgFnSc4d1	malá
cameru	camera	k1gFnSc4	camera
obscuru	obscur	k1gInSc2	obscur
a	a	k8xC	a
papír	papír	k1gInSc4	papír
se	s	k7c7	s
světlocitlivou	světlocitlivý	k2eAgFnSc7d1	světlocitlivá
vrstvou	vrstva	k1gFnSc7	vrstva
chloridu	chlorid	k1gInSc2	chlorid
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
snímky	snímek	k1gInPc1	snímek
nebyly	být	k5eNaImAgInP	být
ustálené	ustálený	k2eAgInPc1d1	ustálený
a	a	k8xC	a
obraz	obraz	k1gInSc1	obraz
časem	časem	k6eAd1	časem
zmizel	zmizet	k5eAaPmAgInS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
pokusů	pokus	k1gInPc2	pokus
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
světlocitlivými	světlocitlivý	k2eAgFnPc7d1	světlocitlivá
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
podložkami	podložka	k1gFnPc7	podložka
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépce	k1gFnSc1	Niépce
roku	rok	k1gInSc6	rok
1822	[number]	k4	1822
reprodukci	reprodukce	k1gFnSc4	reprodukce
mědirytiny	mědirytina	k1gFnSc2	mědirytina
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
na	na	k7c4	na
vrstvu	vrstva	k1gFnSc4	vrstva
přírodního	přírodní	k2eAgInSc2d1	přírodní
asfaltu	asfalt	k1gInSc2	asfalt
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
v	v	k7c6	v
petroleji	petrolej	k1gInSc6	petrolej
<g/>
,	,	kIx,	,
nanesenou	nanesený	k2eAgFnSc4d1	nanesená
na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Reprodukce	reprodukce	k1gFnSc1	reprodukce
se	se	k3xPyFc4	se
však	však	k9	však
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépce	k1gFnSc1	Niépce
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
podložku	podložka	k1gFnSc4	podložka
využíval	využívat	k5eAaPmAgInS	využívat
litografický	litografický	k2eAgInSc1d1	litografický
kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
zinkové	zinkový	k2eAgFnPc1d1	zinková
desky	deska	k1gFnPc1	deska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
leptání	leptání	k1gNnSc6	leptání
nebo	nebo	k8xC	nebo
rytí	rytí	k1gNnSc6	rytí
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
reprodukcí	reprodukce	k1gFnSc7	reprodukce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
na	na	k7c4	na
měděnou	měděný	k2eAgFnSc4d1	měděná
destičku	destička	k1gFnSc4	destička
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépka	k1gFnSc6	Niépka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
muže	muž	k1gMnSc2	muž
vedoucího	vedoucí	k1gMnSc2	vedoucí
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépec	k1gMnSc4	Niépec
rovněž	rovněž	k9	rovněž
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
camerou	camerý	k2eAgFnSc7d1	camerý
obscurou	obscura	k1gFnSc7	obscura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
snímek	snímek	k1gInSc1	snímek
na	na	k7c4	na
kámen	kámen	k1gInSc4	kámen
s	s	k7c7	s
asfaltovou	asfaltový	k2eAgFnSc7d1	asfaltová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgInSc1d1	zachycující
pohled	pohled	k1gInSc1	pohled
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pracovny	pracovna	k1gFnSc2	pracovna
na	na	k7c4	na
boční	boční	k2eAgNnPc4d1	boční
křídlo	křídlo	k1gNnSc4	křídlo
domu	dům	k1gInSc2	dům
a	a	k8xC	a
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
rovněž	rovněž	k9	rovněž
nezachoval	zachovat	k5eNaPmAgMnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
však	však	k9	však
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
za	za	k7c2	za
slunného	slunný	k2eAgInSc2d1	slunný
dne	den	k1gInSc2	den
Niépce	Niépce	k1gFnSc2	Niépce
snímek	snímek	k1gInSc1	snímek
pohledu	pohled	k1gInSc2	pohled
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
s	s	k7c7	s
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
stromem	strom	k1gInSc7	strom
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
býval	bývat	k5eAaImAgInS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc4d1	dochovaná
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgMnS	použít
cameru	camrat	k5eAaPmIp1nS	camrat
obscuru	obscura	k1gFnSc4	obscura
s	s	k7c7	s
předsádkou	předsádka	k1gFnSc7	předsádka
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
snímku	snímek	k1gInSc2	snímek
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
163	[number]	k4	163
<g/>
×	×	k?	×
<g/>
203	[number]	k4	203
mm	mm	kA	mm
trvala	trvat	k5eAaImAgFnS	trvat
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
slunce	slunce	k1gNnSc1	slunce
postupně	postupně	k6eAd1	postupně
osvětlovalo	osvětlovat	k5eAaImAgNnS	osvětlovat
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
snímal	snímat	k5eAaImAgInS	snímat
na	na	k7c4	na
asfaltovou	asfaltový	k2eAgFnSc4d1	asfaltová
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
osvětlených	osvětlený	k2eAgFnPc6d1	osvětlená
částech	část	k1gFnPc6	část
tvrdla	tvrdnout	k5eAaImAgFnS	tvrdnout
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
bílé	bílý	k2eAgFnPc4d1	bílá
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Stíny	stín	k1gInPc1	stín
jsou	být	k5eAaImIp3nP	být
tmavé	tmavý	k2eAgFnPc1d1	tmavá
plochy	plocha	k1gFnPc1	plocha
cínové	cínový	k2eAgFnSc2d1	cínová
podložky	podložka	k1gFnSc2	podložka
po	po	k7c6	po
smytí	smytí	k1gNnSc6	smytí
neosvětleného	osvětlený	k2eNgInSc2d1	neosvětlený
asfaltu	asfalt	k1gInSc2	asfalt
roztokem	roztok	k1gInSc7	roztok
levandulového	levandulový	k2eAgInSc2d1	levandulový
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
terpentýnu	terpentýn	k1gInSc2	terpentýn
<g/>
.	.	kIx.	.
</s>
<s>
Heliografie	heliografie	k1gFnSc1	heliografie
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dokonalejší	dokonalý	k2eAgFnSc7d2	dokonalejší
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
daguerrotypií	daguerrotypie	k1gFnSc7	daguerrotypie
<g/>
,	,	kIx,	,
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
z	z	k7c2	z
heliografie	heliografie	k1gFnSc2	heliografie
Francouzem	Francouz	k1gMnSc7	Francouz
Louisem	Louis	k1gMnSc7	Louis
Daguerrem	Daguerr	k1gMnSc7	Daguerr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
a	a	k8xC	a
kalotypií	kalotypie	k1gFnSc7	kalotypie
<g/>
,	,	kIx,	,
objevenou	objevený	k2eAgFnSc7d1	objevená
Angličanem	Angličan	k1gMnSc7	Angličan
Williamem	William	k1gInSc7	William
Foxem	fox	k1gInSc7	fox
Talbotem	Talbot	k1gInSc7	Talbot
nezávisle	závisle	k6eNd1	závisle
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Daguerrotypie	daguerrotypie	k1gFnSc2	daguerrotypie
<g/>
.	.	kIx.	.
</s>
<s>
Daguerrotypie	daguerrotypie	k1gFnSc1	daguerrotypie
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
le	le	k?	le
daguerréotype	daguerréotyp	k1gInSc5	daguerréotyp
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
daguerreotype	daguerreotyp	k1gInSc5	daguerreotyp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
prakticky	prakticky	k6eAd1	prakticky
užívaný	užívaný	k2eAgInSc4d1	užívaný
komplexní	komplexní	k2eAgInSc4d1	komplexní
fotografický	fotografický	k2eAgInSc4d1	fotografický
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
výtvarník	výtvarník	k1gMnSc1	výtvarník
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Louis	Louis	k1gMnSc1	Louis
Daguerre	Daguerr	k1gInSc5	Daguerr
po	po	k7c4	po
23	[number]	k4	23
let	léto	k1gNnPc2	léto
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
završil	završit	k5eAaPmAgInS	završit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
vynález	vynález	k1gInSc4	vynález
od	od	k7c2	od
Daguerra	Daguerr	k1gMnSc2	Daguerr
a	a	k8xC	a
Isidora	Isidor	k1gMnSc2	Isidor
Niepce	Niepec	k1gMnSc2	Niepec
(	(	kIx(	(
<g/>
syna	syn	k1gMnSc2	syn
zemřelého	zemřelý	k1gMnSc2	zemřelý
Nicéphora	Nicéphor	k1gMnSc2	Nicéphor
Niepceho	Niepce	k1gMnSc2	Niepce
<g/>
,	,	kIx,	,
Daguerrova	Daguerrův	k2eAgMnSc2d1	Daguerrův
předchůdce	předchůdce	k1gMnSc2	předchůdce
a	a	k8xC	a
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
<g/>
)	)	kIx)	)
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
doživotní	doživotní	k2eAgFnSc4d1	doživotní
rentu	renta	k1gFnSc4	renta
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1839	[number]	k4	1839
pak	pak	k6eAd1	pak
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zasedání	zasedání	k1gNnSc6	zasedání
darovala	darovat	k5eAaPmAgFnS	darovat
daguerrotypii	daguerrotypie	k1gFnSc4	daguerrotypie
celému	celý	k2eAgInSc3d1	celý
světu	svět	k1gInSc3	svět
bez	bez	k7c2	bez
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
o	o	k7c4	o
pět	pět	k4xCc4	pět
dnů	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
Daguerre	Daguerr	k1gInSc5	Daguerr
v	v	k7c4	v
zastoupení	zastoupení	k1gNnSc4	zastoupení
nechal	nechat	k5eAaPmAgMnS	nechat
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
jediným	jediný	k2eAgInSc7d1	jediný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
měl	mít	k5eAaImAgInS	mít
patent	patent	k1gInSc1	patent
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ji	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
volně	volně	k6eAd1	volně
používat	používat	k5eAaImF	používat
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
Daguerrův	Daguerrův	k2eAgMnSc1d1	Daguerrův
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
Francouz	Francouz	k1gMnSc1	Francouz
Antoine	Antoin	k1gInSc5	Antoin
Claudet	Claudet	k1gInSc1	Claudet
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
procesu	proces	k1gInSc2	proces
daguerrotypie	daguerrotypie	k1gFnSc2	daguerrotypie
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
právě	právě	k9	právě
tento	tento	k3xDgMnSc1	tento
Daguerrův	Daguerrův	k2eAgMnSc1d1	Daguerrův
student	student	k1gMnSc1	student
Antoine	Antoin	k1gInSc5	Antoin
Claudet	Claudet	k1gInSc1	Claudet
pomocí	pomocí	k7c2	pomocí
chloru	chlor	k1gInSc2	chlor
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
bromu	brom	k1gInSc2	brom
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
kromě	kromě	k7c2	kromě
jódu	jód	k1gInSc2	jód
získal	získat	k5eAaPmAgMnS	získat
větší	veliký	k2eAgFnSc4d2	veliký
rychlost	rychlost	k1gFnSc4	rychlost
chemické	chemický	k2eAgFnSc2d1	chemická
akce	akce	k1gFnSc2	akce
–	–	k?	–
tedy	tedy	k8xC	tedy
kratší	krátký	k2eAgInSc4d2	kratší
expoziční	expoziční	k2eAgInSc4d1	expoziční
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
červené	červený	k2eAgNnSc4d1	červené
(	(	kIx(	(
<g/>
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
<g/>
)	)	kIx)	)
osvětlení	osvětlení	k1gNnSc4	osvětlení
fotografické	fotografický	k2eAgFnSc2d1	fotografická
temné	temný	k2eAgFnSc2d1	temná
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
Brit	Brit	k1gMnSc1	Brit
Henry	henry	k1gInSc2	henry
Fox	fox	k1gInSc1	fox
Talbot	Talbot	k1gMnSc1	Talbot
objevil	objevit	k5eAaPmAgMnS	objevit
techniku	technika	k1gFnSc4	technika
kalotypie	kalotypie	k1gFnSc2	kalotypie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
negativu	negativ	k1gInSc2	negativ
neomezené	omezený	k2eNgNnSc1d1	neomezené
množství	množství	k1gNnSc1	množství
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
přišly	přijít	k5eAaPmAgFnP	přijít
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
dokonalejší	dokonalý	k2eAgFnPc4d2	dokonalejší
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Daguerrotypie	daguerrotypie	k1gFnSc1	daguerrotypie
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
dalšího	další	k2eAgInSc2d1	další
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
slepou	slepý	k2eAgFnSc7d1	slepá
uličkou	ulička	k1gFnSc7	ulička
dějin	dějiny	k1gFnPc2	dějiny
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kalotypie	Kalotypie	k1gFnSc2	Kalotypie
<g/>
.	.	kIx.	.
</s>
<s>
Kalotypie	Kalotypie	k1gFnSc1	Kalotypie
<g/>
,	,	kIx,	,
calotypie	calotypie	k1gFnSc1	calotypie
nebo	nebo	k8xC	nebo
také	také	k9	také
talbotypie	talbotypie	k1gFnSc1	talbotypie
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
vyspělejší	vyspělý	k2eAgFnSc1d2	vyspělejší
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
argyrotypie	argyrotypie	k1gFnSc1	argyrotypie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fotografická	fotografický	k2eAgFnSc1d1	fotografická
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
objevil	objevit	k5eAaPmAgMnS	objevit
britský	britský	k2eAgMnSc1d1	britský
vynálezce	vynálezce	k1gMnSc1	vynálezce
William	William	k1gInSc4	William
Fox	fox	k1gInSc4	fox
Talbot	Talbota	k1gFnPc2	Talbota
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
kalotypie	kalotypie	k1gFnSc2	kalotypie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
základu	základ	k1gInSc2	základ
κ	κ	k?	κ
-	-	kIx~	-
'	'	kIx"	'
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
'	'	kIx"	'
a	a	k8xC	a
τ	τ	k?	τ
-	-	kIx~	-
'	'	kIx"	'
<g/>
zážitek	zážitek	k1gInSc1	zážitek
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Talbot	Talbot	k1gInSc1	Talbot
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgInS	dát
postup	postup	k1gInSc1	postup
patentovat	patentovat	k5eAaBmF	patentovat
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1841	[number]	k4	1841
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
jej	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
vydané	vydaný	k2eAgNnSc1d1	vydané
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
své	svůj	k3xOyFgFnSc2	svůj
metody	metoda	k1gFnSc2	metoda
reguloval	regulovat	k5eAaImAgInS	regulovat
přísnými	přísný	k2eAgNnPc7d1	přísné
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
jejímu	její	k3xOp3gNnSc3	její
většímu	veliký	k2eAgNnSc3d2	veliký
rozšíření	rozšíření	k1gNnSc3	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Kalotypie	Kalotypie	k1gFnSc1	Kalotypie
nahradila	nahradit	k5eAaPmAgFnS	nahradit
daguerrotypii	daguerrotypie	k1gFnSc3	daguerrotypie
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
Francouzem	Francouz	k1gMnSc7	Francouz
Louisem	Louis	k1gMnSc7	Louis
Daguerrem	Daguerr	k1gMnSc7	Daguerr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
byla	být	k5eAaImAgFnS	být
kalotypie	kalotypie	k1gFnSc1	kalotypie
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kolodiovým	kolodiový	k2eAgInSc7d1	kolodiový
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Frederick	Frederick	k1gMnSc1	Frederick
Scott	Scott	k1gMnSc1	Scott
Archer	Archra	k1gFnPc2	Archra
<g/>
.	.	kIx.	.
</s>
<s>
Talbot	Talbot	k1gInSc1	Talbot
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
nejprve	nejprve	k6eAd1	nejprve
metodu	metoda	k1gFnSc4	metoda
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Photogenic	Photogenice	k1gFnPc2	Photogenice
Drawing	Drawing	k1gInSc1	Drawing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
fotogenické	fotogenický	k2eAgFnPc1d1	fotogenická
kresby	kresba	k1gFnPc1	kresba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
papír	papír	k1gInSc4	papír
kladl	klást	k5eAaImAgMnS	klást
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
části	část	k1gFnPc4	část
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
herbáře	herbář	k1gInSc2	herbář
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
ploché	plochý	k2eAgInPc4d1	plochý
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgInSc1d1	negativní
obraz	obraz	k1gInSc1	obraz
svých	svůj	k3xOyFgFnPc2	svůj
stínokreseb	stínokresba	k1gFnPc2	stínokresba
mohl	moct	k5eAaImAgInS	moct
převést	převést	k5eAaPmF	převést
okopírováním	okopírování	k1gNnSc7	okopírování
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
metodu	metoda	k1gFnSc4	metoda
fotogenické	fotogenický	k2eAgFnSc2d1	fotogenická
kresby	kresba	k1gFnSc2	kresba
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
kreslící	kreslící	k2eAgInSc1d1	kreslící
papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
natřel	natřít	k5eAaPmAgInS	natřít
roztokem	roztok	k1gInSc7	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
<g/>
,	,	kIx,	,
osušil	osušit	k5eAaPmAgMnS	osušit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
namočil	namočit	k5eAaPmAgMnS	namočit
do	do	k7c2	do
jodidu	jodid	k1gInSc2	jodid
draselného	draselný	k2eAgMnSc2d1	draselný
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
usušen	usušit	k5eAaPmNgMnS	usušit
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
upotřebením	upotřebení	k1gNnSc7	upotřebení
se	se	k3xPyFc4	se
papír	papír	k1gInSc1	papír
zcitlivil	zcitlivit	k5eAaPmAgInS	zcitlivit
roztokem	roztok	k1gInSc7	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
a	a	k8xC	a
kyselinou	kyselina	k1gFnSc7	kyselina
gallovou	gallová	k1gFnSc7	gallová
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
papír	papír	k1gInSc1	papír
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
při	při	k7c6	při
světle	světlo	k1gNnSc6	světlo
svíčky	svíčka	k1gFnSc2	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
Pozitiv	pozitiv	k1gMnSc1	pozitiv
se	se	k3xPyFc4	se
získal	získat	k5eAaPmAgMnS	získat
kopírováním	kopírování	k1gNnSc7	kopírování
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
různá	různý	k2eAgFnSc1d1	různá
délka	délka	k1gFnSc1	délka
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
barevný	barevný	k2eAgInSc4d1	barevný
tón	tón	k1gInSc4	tón
pozitivu	pozitiv	k1gInSc2	pozitiv
od	od	k7c2	od
karmínové	karmínový	k2eAgFnSc2d1	karmínová
a	a	k8xC	a
purpurové	purpurový	k2eAgFnSc2d1	purpurová
po	po	k7c4	po
hnědočernou	hnědočerný	k2eAgFnSc4d1	hnědočerná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
Gustave	Gustav	k1gMnSc5	Gustav
Le	Le	k1gMnPc3	Le
Gray	Graa	k1gFnSc2	Graa
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
kalotypii	kalotypie	k1gFnSc4	kalotypie
voskováním	voskování	k1gNnPc3	voskování
<g/>
.	.	kIx.	.
</s>
<s>
Papír	papír	k1gInSc1	papír
nechal	nechat	k5eAaPmAgInS	nechat
nasáknout	nasáknout	k5eAaPmF	nasáknout
horkým	horký	k2eAgInSc7d1	horký
voskem	vosk	k1gInSc7	vosk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
izoloval	izolovat	k5eAaBmAgInS	izolovat
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
preparátu	preparát	k1gInSc2	preparát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jodování	jodování	k1gNnSc6	jodování
a	a	k8xC	a
usušení	usušení	k1gNnSc6	usušení
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
papír	papír	k1gInSc1	papír
zcitlivil	zcitlivit	k5eAaImAgInS	zcitlivit
v	v	k7c6	v
dusičnanu	dusičnan	k1gInSc6	dusičnan
stříbrném	stříbrný	k1gInSc6	stříbrný
a	a	k8xC	a
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgNnSc1d1	octové
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uchování	uchování	k1gNnSc6	uchování
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
a	a	k8xC	a
chladnu	chladnout	k5eAaImIp1nS	chladnout
si	se	k3xPyFc3	se
papír	papír	k1gInSc4	papír
svou	svůj	k3xOyFgFnSc4	svůj
citlivost	citlivost	k1gFnSc4	citlivost
udržel	udržet	k5eAaPmAgInS	udržet
minimálně	minimálně	k6eAd1	minimálně
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
fotografování	fotografování	k1gNnSc2	fotografování
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
fotograf	fotograf	k1gMnSc1	fotograf
Josef	Josef	k1gMnSc1	Josef
Krtička	Krtička	k1gMnSc1	Krtička
nazýval	nazývat	k5eAaImAgMnS	nazývat
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
své	svůj	k3xOyFgFnSc2	svůj
kalotypie	kalotypie	k1gFnSc2	kalotypie
"	"	kIx"	"
<g/>
oblesky	oblesky	k6eAd1	oblesky
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
M.	M.	kA	M.
V.	V.	kA	V.
Lobethal	Lobethal	k1gMnSc1	Lobethal
zase	zase	k9	zase
"	"	kIx"	"
<g/>
světelné	světelný	k2eAgInPc1d1	světelný
obrazy	obraz	k1gInPc1	obraz
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přesnosti	přesnost	k1gFnSc6	přesnost
a	a	k8xC	a
jemnosti	jemnost	k1gFnPc4	jemnost
detailů	detail	k1gInPc2	detail
i	i	k8xC	i
polotónů	polotón	k1gInPc2	polotón
kalotypii	kalotypie	k1gFnSc4	kalotypie
překonala	překonat	k5eAaPmAgFnS	překonat
až	až	k9	až
archerotypie	archerotypie	k1gFnSc1	archerotypie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mokrý	mokrý	k2eAgInSc1d1	mokrý
kolodiový	kolodiový	k2eAgInSc1d1	kolodiový
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
Scott	Scott	k1gMnSc1	Scott
Archer	Archra	k1gFnPc2	Archra
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
se	se	k3xPyFc4	se
však	však	k9	však
používala	používat	k5eAaImAgFnS	používat
až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kolodiový	Kolodiový	k2eAgInSc4d1	Kolodiový
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Kolodiový	Kolodiový	k2eAgInSc1d1	Kolodiový
proces	proces	k1gInSc1	proces
(	(	kIx(	(
<g/>
archerotypie	archerotypie	k1gFnSc1	archerotypie
nebo	nebo	k8xC	nebo
mokrý	mokrý	k2eAgInSc1d1	mokrý
proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
fotografický	fotografický	k2eAgInSc4d1	fotografický
proces	proces	k1gInSc4	proces
Angličana	Angličan	k1gMnSc2	Angličan
Fredericka	Fredericko	k1gNnSc2	Fredericko
Scotta	Scott	k1gMnSc2	Scott
Archera	Archer	k1gMnSc2	Archer
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
mokrý	mokrý	k2eAgInSc1d1	mokrý
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podstatou	podstata	k1gFnSc7	podstata
procesu	proces	k1gInSc2	proces
bylo	být	k5eAaImAgNnS	být
exponování	exponování	k1gNnSc1	exponování
a	a	k8xC	a
vyvolávání	vyvolávání	k1gNnSc1	vyvolávání
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
<g/>
.	.	kIx.	.
</s>
<s>
Zajistil	zajistit	k5eAaPmAgInS	zajistit
tvorbu	tvorba	k1gFnSc4	tvorba
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
negativu	negativ	k1gInSc2	negativ
umožňujícího	umožňující	k2eAgInSc2d1	umožňující
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
reprodukci	reprodukce	k1gFnSc4	reprodukce
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
nahradil	nahradit	k5eAaPmAgMnS	nahradit
předešlé	předešlý	k2eAgInPc4d1	předešlý
postupy	postup	k1gInPc4	postup
daguerrotypii	daguerrotypie	k1gFnSc3	daguerrotypie
a	a	k8xC	a
především	především	k9	především
kalotypii	kalotypie	k1gFnSc4	kalotypie
a	a	k8xC	a
využíval	využívat	k5eAaImAgInS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1851	[number]	k4	1851
až	až	k9	až
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
obměněného	obměněný	k2eAgInSc2d1	obměněný
mokrého	mokrý	k2eAgInSc2d1	mokrý
kolodiového	kolodiový	k2eAgInSc2d1	kolodiový
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
přímé	přímý	k2eAgInPc4d1	přímý
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
pannotypie	pannotypie	k1gFnSc1	pannotypie
<g/>
,	,	kIx,	,
ambrotypie	ambrotypie	k1gFnSc1	ambrotypie
a	a	k8xC	a
ferrotypie	ferrotypie	k1gFnSc1	ferrotypie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
procesy	proces	k1gInPc1	proces
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
želatinovým	želatinový	k2eAgInSc7d1	želatinový
fotografickým	fotografický	k2eAgInSc7d1	fotografický
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Suchý	suchý	k2eAgInSc1d1	suchý
želatinový	želatinový	k2eAgInSc1d1	želatinový
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
želatinový	želatinový	k2eAgInSc1d1	želatinový
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
fotografický	fotografický	k2eAgInSc4d1	fotografický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Richard	Richard	k1gMnSc1	Richard
Leach	Leach	k1gMnSc1	Leach
Maddox	Maddox	k1gInSc4	Maddox
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vynález	vynález	k1gInSc1	vynález
přinesl	přinést	k5eAaPmAgInS	přinést
do	do	k7c2	do
fotografického	fotografický	k2eAgInSc2d1	fotografický
průmyslu	průmysl	k1gInSc2	průmysl
lehké	lehký	k2eAgFnSc2d1	lehká
bromostříbrné	bromostříbrný	k2eAgFnSc2d1	bromostříbrný
suché	suchý	k2eAgFnSc2d1	suchá
želatinové	želatinový	k2eAgFnSc2d1	želatinová
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
proces	proces	k1gInSc1	proces
nahradil	nahradit	k5eAaPmAgInS	nahradit
mokrý	mokrý	k2eAgInSc1d1	mokrý
kolodiový	kolodiový	k2eAgInSc1d1	kolodiový
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
Frederick	Fredericko	k1gNnPc2	Fredericko
Scott	Scotta	k1gFnPc2	Scotta
Archer	Archra	k1gFnPc2	Archra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obrázky	obrázek	k1gInPc1	obrázek
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
světelné	světelný	k2eAgInPc1d1	světelný
expozici	expozice	k1gFnSc4	expozice
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
,	,	kIx,	,
desky	deska	k1gFnPc4	deska
však	však	k9	však
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
zcitlivené	zcitlivený	k2eAgFnPc1d1	zcitlivený
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
expoziční	expoziční	k2eAgInSc4d1	expoziční
čas	čas	k1gInSc4	čas
a	a	k8xC	a
zpracovány	zpracován	k2eAgInPc1d1	zpracován
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
mokré	mokrý	k2eAgNnSc1d1	mokré
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
suchého	suchý	k2eAgInSc2d1	suchý
procesu	proces	k1gInSc2	proces
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
momentem	moment	k1gInSc7	moment
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
fotografování	fotografování	k1gNnSc2	fotografování
<g/>
.	.	kIx.	.
</s>
<s>
Maddox	Maddox	k1gInSc1	Maddox
nahradil	nahradit	k5eAaPmAgInS	nahradit
těžké	těžký	k2eAgFnPc4d1	těžká
mokré	mokrý	k2eAgFnPc4d1	mokrá
kolodiové	kolodiový	k2eAgFnPc4d1	kolodiová
desky	deska	k1gFnPc4	deska
za	za	k7c4	za
suché	suchý	k2eAgFnPc4d1	suchá
želatinové	želatinový	k2eAgFnPc4d1	želatinová
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nevyžadovaly	vyžadovat	k5eNaImAgFnP	vyžadovat
mobilní	mobilní	k2eAgFnSc4d1	mobilní
temnou	temný	k2eAgFnSc4d1	temná
komoru	komora	k1gFnSc4	komora
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
zdraví	zdraví	k1gNnSc4	zdraví
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
"	"	kIx"	"
<g/>
mokré	mokrý	k2eAgInPc4d1	mokrý
<g/>
"	"	kIx"	"
kolodiové	kolodiový	k2eAgInPc4d1	kolodiový
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
hledat	hledat	k5eAaImF	hledat
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
časopisu	časopis	k1gInSc2	časopis
British	British	k1gInSc1	British
Journal	Journal	k1gMnSc2	Journal
of	of	k?	of
Photography	Photographa	k1gMnSc2	Photographa
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
popisoval	popisovat	k5eAaImAgMnS	popisovat
senzibilizující	senzibilizující	k2eAgFnSc2d1	senzibilizující
chemické	chemický	k2eAgFnSc2d1	chemická
sloučeniny	sloučenina	k1gFnSc2	sloučenina
bromid	bromid	k1gInSc1	bromid
kademnatý	kademnatý	k2eAgInSc1d1	kademnatý
a	a	k8xC	a
dusičnan	dusičnan	k1gInSc1	dusičnan
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
želatinou	želatina	k1gFnSc7	želatina
<g/>
,	,	kIx,	,
transparentní	transparentní	k2eAgFnSc7d1	transparentní
látkou	látka	k1gFnSc7	látka
používanou	používaný	k2eAgFnSc7d1	používaná
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
cukrovinek	cukrovinka	k1gFnPc2	cukrovinka
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Bennett	Bennett	k1gMnSc1	Bennett
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
první	první	k4xOgFnPc4	první
želatinové	želatinový	k2eAgFnPc4d1	želatinová
suché	suchý	k2eAgFnPc4d1	suchá
desky	deska	k1gFnPc4	deska
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
emulze	emulze	k1gFnSc1	emulze
na	na	k7c4	na
celuloidový	celuloidový	k2eAgInSc4d1	celuloidový
svitkový	svitkový	k2eAgInSc4d1	svitkový
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
suchých	suchý	k2eAgFnPc2d1	suchá
desek	deska	k1gFnPc2	deska
byly	být	k5eAaImAgFnP	být
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
:	:	kIx,	:
fotografové	fotograf	k1gMnPc1	fotograf
krajináři	krajinář	k1gMnPc7	krajinář
mohli	moct	k5eAaImAgMnP	moct
používat	používat	k5eAaImF	používat
komerční	komerční	k2eAgFnSc2d1	komerční
suché	suchý	k2eAgFnSc2d1	suchá
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
připravovali	připravovat	k5eAaImAgMnP	připravovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
emulze	emulze	k1gFnPc4	emulze
v	v	k7c6	v
mobilní	mobilní	k2eAgFnSc6d1	mobilní
temné	temný	k2eAgFnSc6d1	temná
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Negativy	negativ	k1gInPc1	negativ
nemusely	muset	k5eNaImAgInP	muset
být	být	k5eAaImF	být
vyvolány	vyvolat	k5eAaPmNgInP	vyvolat
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
velikost	velikost	k1gFnSc4	velikost
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
zmenšit	zmenšit	k5eAaPmF	zmenšit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vešly	vejít	k5eAaPmAgFnP	vejít
"	"	kIx"	"
<g/>
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
schovat	schovat	k5eAaPmF	schovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Celuloid	celuloid	k1gInSc1	celuloid
<g/>
.	.	kIx.	.
</s>
