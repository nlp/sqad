<s>
Heliografie	heliografie	k1gFnSc1	heliografie
nebo	nebo	k8xC	nebo
niepceotypie	niepceotypie	k1gFnSc1	niepceotypie
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
fotografická	fotografický	k2eAgFnSc1d1	fotografická
technika	technika	k1gFnSc1	technika
pro	pro	k7c4	pro
zhotovení	zhotovení	k1gNnSc4	zhotovení
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
reprodukcí	reprodukce	k1gFnPc2	reprodukce
grafických	grafický	k2eAgInPc2d1	grafický
listů	list	k1gInPc2	list
a	a	k8xC	a
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
<g/>
.	.	kIx.	.
</s>
