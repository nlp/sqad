<s>
Delší	dlouhý	k2eAgInPc1d2	delší
vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgInP	být
zejména	zejména	k9	zejména
ve	v	k7c6	v
středověku	středověk	k1gInSc2	středověk
jasným	jasný	k2eAgInSc7d1	jasný
symbolem	symbol	k1gInSc7	symbol
vyššího	vysoký	k2eAgNnSc2d2	vyšší
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kratší	krátký	k2eAgInPc1d2	kratší
vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgFnP	být
především	především	k6eAd1	především
symbolem	symbol	k1gInSc7	symbol
podřízenosti	podřízenost	k1gFnSc2	podřízenost
a	a	k8xC	a
zejména	zejména	k9	zejména
poddanství	poddanství	k1gNnSc1	poddanství
<g/>
.	.	kIx.	.
</s>
