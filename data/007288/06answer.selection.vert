<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
druh	druh	k1gInSc4	druh
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
zpravidla	zpravidla	k6eAd1	zpravidla
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
po	po	k7c6	po
vodní	vodní	k2eAgFnSc6d1	vodní
hladině	hladina	k1gFnSc6	hladina
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
i	i	k9	i
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
)	)	kIx)	)
na	na	k7c6	na
principu	princip	k1gInSc6	princip
Archimédova	Archimédův	k2eAgInSc2d1	Archimédův
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
