<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
druh	druh	k1gInSc4	druh
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
zpravidla	zpravidla	k6eAd1	zpravidla
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
po	po	k7c6	po
vodní	vodní	k2eAgFnSc6d1	vodní
hladině	hladina	k1gFnSc6	hladina
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
i	i	k9	i
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
)	)	kIx)	)
na	na	k7c6	na
principu	princip	k1gInSc6	princip
Archimédova	Archimédův	k2eAgInSc2d1	Archimédův
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc4	loď
plující	plující	k2eAgFnPc4d1	plující
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
označujeme	označovat	k5eAaImIp1nP	označovat
termínem	termín	k1gInSc7	termín
plavidla	plavidlo	k1gNnPc4	plavidlo
hladinová	hladinový	k2eAgNnPc4d1	hladinové
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc4	loď
umožňující	umožňující	k2eAgInSc4d1	umožňující
pohyb	pohyb	k1gInSc4	pohyb
i	i	k9	i
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
termínem	termín	k1gInSc7	termín
plavidla	plavidlo	k1gNnPc4	plavidlo
ponorná	ponorný	k2eAgNnPc4d1	ponorné
resp.	resp.	kA	resp.
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
loď	loď	k1gFnSc1	loď
i	i	k8xC	i
prostředek	prostředek	k1gInSc1	prostředek
k	k	k7c3	k
volnému	volný	k2eAgInSc3d1	volný
pohybu	pohyb	k1gInSc3	pohyb
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
prostředí	prostředí	k1gNnSc6	prostředí
–	–	k?	–
vizte	vidět	k5eAaImRp2nP	vidět
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
<g/>
,	,	kIx,	,
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
hladinové	hladinový	k2eAgFnSc2d1	hladinová
obchodní	obchodní	k2eAgFnSc1d1	obchodní
osobní	osobní	k2eAgFnSc1d1	osobní
dálková	dálkový	k2eAgFnSc1d1	dálková
linková	linkový	k2eAgFnSc1d1	Linková
místní	místní	k2eAgFnSc1d1	místní
převozní	převozní	k2eAgFnSc1d1	převozní
výletní	výletní	k2eAgFnSc1d1	výletní
nákladní	nákladní	k2eAgFnSc1d1	nákladní
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
kusového	kusový	k2eAgNnSc2d1	kusové
zboží	zboží	k1gNnSc2	zboží
(	(	kIx(	(
<g/>
general	generat	k5eAaImAgMnS	generat
cargo	cargo	k1gMnSc1	cargo
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
hromadných	hromadný	k2eAgInPc2d1	hromadný
substrátů	substrát	k1gInPc2	substrát
(	(	kIx(	(
<g/>
bulk	bulk	k1gMnSc1	bulk
carrier	carrier	k1gMnSc1	carrier
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
kontejnerové	kontejnerový	k2eAgInPc1d1	kontejnerový
tankery	tanker	k1gInPc1	tanker
smíšené	smíšený	k2eAgInPc1d1	smíšený
remorkéry	remorkér	k1gInPc1	remorkér
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
válečné	válečná	k1gFnSc2	válečná
speciální	speciální	k2eAgFnSc2d1	speciální
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgFnSc2d1	sportovní
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgFnSc2d1	policejní
<g/>
,	,	kIx,	,
požární	požární	k2eAgFnSc2d1	požární
<g/>
,	,	kIx,	,
nemocniční	nemocniční	k2eAgMnSc1d1	nemocniční
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
ponorné	ponorný	k2eAgFnSc2d1	ponorná
(	(	kIx(	(
<g/>
ponorky	ponorka	k1gFnSc2	ponorka
<g/>
)	)	kIx)	)
válečné	válečný	k2eAgFnSc2d1	válečná
vědecké	vědecký	k2eAgFnSc2d1	vědecká
a	a	k8xC	a
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
<g/>
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
oceánů	oceán	k1gInPc2	oceán
a	a	k8xC	a
moří	moře	k1gNnPc2	moře
<g/>
)	)	kIx)	)
pracovní	pracovní	k2eAgFnSc1d1	pracovní
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
vyzdvihování	vyzdvihování	k1gNnSc4	vyzdvihování
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
vraků	vrak	k1gInPc2	vrak
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
První	první	k4xOgInSc1	první
archeologicky	archeologicky	k6eAd1	archeologicky
doložená	doložený	k2eAgNnPc1d1	doložené
vesla	veslo	k1gNnPc1	veslo
jsou	být	k5eAaImIp3nP	být
stará	starý	k2eAgNnPc1d1	staré
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
a	a	k8xC	a
první	první	k4xOgFnPc1	první
doložené	doložený	k2eAgFnPc1d1	doložená
lodě	loď	k1gFnPc1	loď
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
přes	přes	k7c4	přes
8000	[number]	k4	8000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vydlabané	vydlabaný	k2eAgInPc1d1	vydlabaný
kmeny	kmen	k1gInPc1	kmen
stromů	strom	k1gInPc2	strom
–	–	k?	–
monoxyly	monoxyl	k1gInPc4	monoxyl
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
kánoe	kánoe	k1gFnSc2	kánoe
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
ale	ale	k8xC	ale
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
dřeva	dřevo	k1gNnSc2	dřevo
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
lodě	loď	k1gFnPc4	loď
i	i	k9	i
z	z	k7c2	z
rákosu	rákos	k1gInSc2	rákos
a	a	k8xC	a
papyru	papyr	k1gInSc2	papyr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
dochována	dochován	k2eAgFnSc1d1	dochována
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
ceremoniální	ceremoniální	k2eAgFnSc1d1	ceremoniální
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ale	ale	k9	ale
nemusela	muset	k5eNaImAgFnS	muset
být	být	k5eAaImF	být
schopná	schopný	k2eAgFnSc1d1	schopná
plavby	plavba	k1gFnPc1	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k9	jistě
části	část	k1gFnPc1	část
lodí	loď	k1gFnPc2	loď
byly	být	k5eAaImAgFnP	být
i	i	k9	i
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
neustále	neustále	k6eAd1	neustále
zdokonalovala	zdokonalovat	k5eAaImAgFnS	zdokonalovat
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
pohon	pohon	k1gInSc1	pohon
silou	síla	k1gFnSc7	síla
lidských	lidský	k2eAgInPc2d1	lidský
svalů	sval	k1gInPc2	sval
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgNnSc1d2	dokonalejší
oplachtění	oplachtění	k1gNnSc1	oplachtění
plachetnic	plachetnice	k1gFnPc2	plachetnice
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
parníků	parník	k1gInPc2	parník
a	a	k8xC	a
pak	pak	k6eAd1	pak
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ohromných	ohromný	k2eAgFnPc2d1	ohromná
zaoceánských	zaoceánský	k2eAgFnPc2d1	zaoceánská
a	a	k8xC	a
námořních	námořní	k2eAgFnPc2d1	námořní
i	i	k8xC	i
říčních	říční	k2eAgFnPc2d1	říční
lodí	loď	k1gFnPc2	loď
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
i	i	k9	i
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
sportovní	sportovní	k2eAgFnPc1d1	sportovní
lodě	loď	k1gFnPc1	loď
–	–	k?	–
jachty	jachta	k1gFnSc2	jachta
<g/>
,	,	kIx,	,
kánoe	kánoe	k1gFnPc1	kánoe
<g/>
,	,	kIx,	,
kajaky	kajak	k1gInPc1	kajak
<g/>
,	,	kIx,	,
rafty	rafta	k1gFnPc1	rafta
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
lodí	loď	k1gFnPc2	loď
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaPmAgNnS	využívat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
nahrazeno	nahrazen	k2eAgNnSc1d1	nahrazeno
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
lodí	loď	k1gFnPc2	loď
prakticky	prakticky	k6eAd1	prakticky
výhradně	výhradně	k6eAd1	výhradně
ocelí	ocelit	k5eAaImIp3nS	ocelit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
speciálních	speciální	k2eAgInPc6d1	speciální
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
využívány	využívat	k5eAaPmNgInP	využívat
i	i	k9	i
kompozitní	kompozitní	k2eAgInPc1d1	kompozitní
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
skelný	skelný	k2eAgInSc1d1	skelný
laminát	laminát	k1gInSc1	laminát
nebo	nebo	k8xC	nebo
uhlíkové	uhlíkový	k2eAgNnSc1d1	uhlíkové
kompozity	kompozitum	k1gNnPc7	kompozitum
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgFnSc4d1	lodní
posádku	posádka	k1gFnSc4	posádka
tvoří	tvořit	k5eAaImIp3nP	tvořit
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
služba	služba	k1gFnSc1	služba
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
jejího	její	k3xOp3gInSc2	její
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvoří	tvořit	k5eAaImIp3nP	tvořit
lodní	lodní	k2eAgMnPc1d1	lodní
důstojníci	důstojník	k1gMnPc1	důstojník
včetně	včetně	k7c2	včetně
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
,	,	kIx,	,
kormidelníci	kormidelník	k1gMnPc5	kormidelník
<g/>
,	,	kIx,	,
lodníci	lodník	k1gMnPc5	lodník
<g/>
,	,	kIx,	,
plavčíci	plavčík	k1gMnPc5	plavčík
<g/>
,	,	kIx,	,
strojníci	strojník	k1gMnPc5	strojník
<g/>
,	,	kIx,	,
mazači	mazač	k1gMnPc5	mazač
<g/>
,	,	kIx,	,
topiči	topič	k1gMnPc5	topič
<g/>
,	,	kIx,	,
stevardi	stevard	k1gMnPc1	stevard
a	a	k8xC	a
kuchaři	kuchař	k1gMnPc1	kuchař
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
profesí	profes	k1gFnPc2	profes
není	být	k5eNaImIp3nS	být
konečný	konečný	k2eAgInSc1d1	konečný
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
typu	typ	k1gInSc6	typ
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
účelu	účel	k1gInSc3	účel
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
seznam	seznam	k1gInSc1	seznam
největších	veliký	k2eAgFnPc2d3	veliký
osobních	osobní	k2eAgFnPc2d1	osobní
lodí	loď	k1gFnPc2	loď
plavidlo	plavidlo	k1gNnSc4	plavidlo
ponorka	ponorka	k1gFnSc1	ponorka
lodní	lodní	k2eAgFnSc1d1	lodní
šroub	šroub	k1gInSc4	šroub
plachta	plachta	k1gFnSc1	plachta
<g/>
,	,	kIx,	,
ráhno	ráhno	k1gNnSc1	ráhno
veslo	veslo	k1gNnSc1	veslo
<g/>
,	,	kIx,	,
pádlo	pádlo	k1gNnSc1	pádlo
kýl	kýla	k1gFnPc2	kýla
<g/>
,	,	kIx,	,
příď	příď	k1gFnSc1	příď
<g/>
,	,	kIx,	,
záď	záď	k1gFnSc1	záď
<g/>
,	,	kIx,	,
paluba	paluba	k1gFnSc1	paluba
<g/>
,	,	kIx,	,
podpalubí	podpalubí	k1gNnSc1	podpalubí
parník	parník	k1gInSc1	parník
<g/>
,	,	kIx,	,
plachetnice	plachetnice	k1gFnSc1	plachetnice
<g/>
,	,	kIx,	,
kajak	kajak	k1gInSc1	kajak
<g/>
,	,	kIx,	,
kánoe	kánoe	k1gFnSc1	kánoe
<g/>
,	,	kIx,	,
katamarán	katamarán	k1gInSc1	katamarán
<g/>
,	,	kIx,	,
hausbót	hausbót	k1gInSc1	hausbót
přívoz	přívoz	k1gInSc1	přívoz
vor	vor	k1gInSc4	vor
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
loď	loď	k1gFnSc1	loď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
loď	loď	k1gFnSc1	loď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
loď	loď	k1gFnSc1	loď
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gFnSc1	téma
Loď	loď	k1gFnSc1	loď
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
