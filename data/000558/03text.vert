<s>
Únor	únor	k1gInSc1	únor
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
druhý	druhý	k4xOgInSc4	druhý
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
28	[number]	k4	28
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
má	mít	k5eAaImIp3nS	mít
29	[number]	k4	29
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
měl	mít	k5eAaImAgInS	mít
únor	únor	k1gInSc4	únor
i	i	k9	i
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
měsíce	měsíc	k1gInSc2	měsíc
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
při	při	k7c6	při
tání	tání	k1gNnSc6	tání
ledu	led	k1gInSc2	led
ponořují	ponořovat	k5eAaImIp3nP	ponořovat
ledové	ledový	k2eAgFnPc1d1	ledová
kry	kra	k1gFnPc1	kra
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
(	(	kIx(	(
<g/>
únor	únor	k1gInSc4	únor
=	=	kIx~	=
nořiti	nořit	k5eAaImF	nořit
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
byl	být	k5eAaImAgMnS	být
únor	únor	k1gInSc4	únor
posledním	poslední	k2eAgInSc7d1	poslední
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
počet	počet	k1gInSc1	počet
dnů	den	k1gInPc2	den
a	a	k8xC	a
právě	právě	k9	právě
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
přidával	přidávat	k5eAaImAgMnS	přidávat
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
z	z	k7c2	z
juliánského	juliánský	k2eAgInSc2d1	juliánský
na	na	k7c4	na
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
zavedený	zavedený	k2eAgInSc4d1	zavedený
postup	postup	k1gInSc4	postup
neměnil	měnit	k5eNaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Varianty	varianta	k1gFnPc1	varianta
původně	původně	k6eAd1	původně
římského	římský	k2eAgNnSc2d1	římské
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
latinského	latinský	k2eAgInSc2d1	latinský
<g/>
)	)	kIx)	)
názvu	název	k1gInSc2	název
Februarius	Februarius	k1gInSc1	Februarius
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Únor	únor	k1gInSc1	únor
začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xS	jako
březen	březen	k1gInSc4	březen
a	a	k8xC	a
listopad	listopad	k1gInSc4	listopad
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přestupného	přestupný	k2eAgInSc2d1	přestupný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xC	jako
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Únor	únor	k1gInSc1	únor
začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k9	jako
loňský	loňský	k2eAgInSc4d1	loňský
červen	červen	k1gInSc4	červen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
úroků	úrok	k1gInPc2	úrok
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
metod	metoda	k1gFnPc2	metoda
se	se	k3xPyFc4	se
únor	únor	k1gInSc1	únor
bere	brát	k5eAaImIp3nS	brát
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
měsíce	měsíc	k1gInPc1	měsíc
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
únoru	únor	k1gInSc3	únor
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
řada	řada	k1gFnSc1	řada
pranostik	pranostika	k1gFnPc2	pranostika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
seznamu	seznam	k1gInSc6	seznam
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
jen	jen	k6eAd1	jen
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
měsíci	měsíc	k1gInSc3	měsíc
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
</s>
<s>
Vynechány	vynechán	k2eAgFnPc1d1	vynechána
jsou	být	k5eAaImIp3nP	být
pranostiky	pranostika	k1gFnPc1	pranostika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
ke	k	k7c3	k
konkrétním	konkrétní	k2eAgInPc3d1	konkrétní
únorovým	únorový	k2eAgInPc3d1	únorový
dnům	den	k1gInPc3	den
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hromnice	hromnice	k1gFnSc1	hromnice
<g/>
,	,	kIx,	,
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Háty	Háta	k1gFnSc2	Háta
<g/>
/	/	kIx~	/
<g/>
Agáty	Agáta	k1gFnSc2	Agáta
nebo	nebo	k8xC	nebo
sv.	sv.	kA	sv.
Marka	marka	k1gFnSc1	marka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Únor	únor	k1gInSc1	únor
bílý	bílý	k2eAgInSc1d1	bílý
–	–	k?	–
pole	pole	k1gNnPc1	pole
sílí	sílit	k5eAaImIp3nP	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Sněhový	sněhový	k2eAgInSc1d1	sněhový
únor	únor	k1gInSc1	únor
–	–	k?	–
sílí	sílet	k5eAaImIp3nS	sílet
úhor	úhor	k1gInSc1	úhor
<g/>
.	.	kIx.	.
</s>
<s>
Únorová	únorový	k2eAgFnSc1d1	únorová
voda	voda	k1gFnSc1	voda
–	–	k?	–
pro	pro	k7c4	pro
pole	pole	k1gNnSc4	pole
škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Únor	únor	k1gInSc1	únor
–	–	k?	–
úmor	úmor	k1gInSc1	úmor
<g/>
:	:	kIx,	:
kdyby	kdyby	kYmCp3nS	kdyby
mohl	moct	k5eAaImAgMnS	moct
<g/>
,	,	kIx,	,
umořil	umořit	k5eAaPmAgMnS	umořit
by	by	kYmCp3nS	by
v	v	k7c6	v
krávě	kráva	k1gFnSc6	kráva
tele	tele	k1gNnSc1	tele
a	a	k8xC	a
v	v	k7c6	v
kobyle	kobyla	k1gFnSc6	kobyla
hříbě	hříbě	k1gNnSc1	hříbě
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
měl	mít	k5eAaImAgMnS	mít
únor	únor	k1gInSc4	únor
tu	tu	k6eAd1	tu
moc	moc	k6eAd1	moc
jako	jako	k8xS	jako
leden	leden	k1gInSc4	leden
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
by	by	kYmCp3nS	by
v	v	k7c6	v
krávě	kráva	k1gFnSc6	kráva
zmrznout	zmrznout	k5eAaPmF	zmrznout
tele	tele	k1gNnSc4	tele
<g/>
.	.	kIx.	.
</s>
<s>
Netrkne	trknout	k5eNaPmIp3nS	trknout
<g/>
-li	i	k?	-li
únor	únor	k1gInSc4	únor
rohem	roh	k1gInSc7	roh
<g/>
,	,	kIx,	,
šlehne	šlehnout	k5eAaPmIp3nS	šlehnout
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
mráz	mráz	k1gInSc1	mráz
ostro	ostro	k6eAd1	ostro
drží	držet	k5eAaImIp3nS	držet
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
dlouho	dlouho	k6eAd1	dlouho
již	již	k6eAd1	již
nepodrží	podržet	k5eNaPmIp3nS	podržet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
záhy	záhy	k6eAd1	záhy
taje	tát	k5eAaImIp3nS	tát
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
neroztaje	roztát	k5eNaPmIp3nS	roztát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
únor	únor	k1gInSc1	únor
vodu	voda	k1gFnSc4	voda
spustí	spustit	k5eAaPmIp3nS	spustit
<g/>
,	,	kIx,	,
ledem	led	k1gInSc7	led
ji	on	k3xPp3gFnSc4	on
březen	březen	k1gInSc1	březen
zahustí	zahustit	k5eAaPmIp3nS	zahustit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tě	ty	k3xPp2nSc4	ty
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
zašimrá	zašimrat	k5eAaPmIp3nS	zašimrat
komár	komár	k1gMnSc1	komár
za	za	k7c7	za
ušima	ucho	k1gNnPc7	ucho
<g/>
,	,	kIx,	,
poběhneš	poběhnout	k5eAaPmIp2nS	poběhnout
jistě	jistě	k9	jistě
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
ke	k	k7c3	k
kamnům	kamna	k1gNnPc3	kamna
s	s	k7c7	s
ušima	ucho	k1gNnPc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
<g/>
-li	i	k?	-li
kočka	kočka	k1gFnSc1	kočka
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
jistě	jistě	k6eAd1	jistě
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
poleze	polézt	k5eAaPmIp3nS	polézt
za	za	k7c4	za
kamna	kamna	k1gNnPc4	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
únor	únor	k1gInSc1	únor
honí	honit	k5eAaImIp3nP	honit
mraky	mrak	k1gInPc1	mrak
<g/>
,	,	kIx,	,
staví	stavit	k5eAaImIp3nS	stavit
březen	březen	k1gInSc4	březen
sněhuláky	sněhulák	k1gMnPc4	sněhulák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
led	led	k1gInSc1	led
–	–	k?	–
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nanesou	nanést	k5eAaBmIp3nP	nanést
včely	včela	k1gFnPc1	včela
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Teplý	teplý	k2eAgInSc1d1	teplý
únor	únor	k1gInSc1	únor
–	–	k?	–
studené	studený	k2eAgNnSc4d1	studené
jaro	jaro	k1gNnSc4	jaro
<g/>
,	,	kIx,	,
teplé	teplý	k2eAgNnSc4d1	teplé
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
si	se	k3xPyFc3	se
únor	únor	k1gInSc4	únor
zazelená	zazelenat	k5eAaPmIp3nS	zazelenat
–	–	k?	–
březen	březen	k1gInSc4	březen
si	se	k3xPyFc3	se
hájí	hájit	k5eAaImIp3nP	hájit
<g/>
;	;	kIx,	;
co	co	k8xS	co
si	se	k3xPyFc3	se
duben	duben	k1gInSc1	duben
zazelená	zazelenat	k5eAaPmIp3nS	zazelenat
–	–	k?	–
květen	květen	k1gInSc1	květen
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
spálí	spálit	k5eAaPmIp3nS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
mlh	mlha	k1gFnPc2	mlha
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
přivodí	přivodit	k5eAaPmIp3nS	přivodit
mokré	mokrý	k2eAgNnSc1d1	mokré
léto	léto	k1gNnSc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
větrové	vítr	k1gInPc1wB	vítr
na	na	k7c4	na
konec	konec	k1gInSc4	konec
února	únor	k1gInSc2	únor
uhodí	uhodit	k5eAaPmIp3nS	uhodit
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
obilí	obilí	k1gNnSc2	obilí
se	se	k3xPyFc4	se
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
neurodí	urodit	k5eNaPmIp3nS	urodit
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
únor	únor	k1gInSc1	únor
honí	honit	k5eAaImIp3nP	honit
mraky	mrak	k1gInPc1	mrak
<g/>
,	,	kIx,	,
staví	stavit	k5eAaPmIp3nS	stavit
březen	březen	k1gInSc4	březen
sněhuláky	sněhulák	k1gMnPc7	sněhulák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
-li	i	k?	-li
vítr	vítr	k1gInSc1	vítr
neburácí	burácet	k5eNaImIp3nS	burácet
<g/>
,	,	kIx,	,
jistě	jistě	k9	jistě
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
krovy	krov	k1gInPc4	krov
kácí	kácet	k5eAaImIp3nP	kácet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
mráz	mráz	k1gInSc1	mráz
ostro	ostro	k6eAd1	ostro
drží	držet	k5eAaImIp3nS	držet
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
dlouho	dlouho	k6eAd1	dlouho
již	již	k6eAd1	již
nepodrží	podržet	k5eNaPmIp3nS	podržet
<g/>
.	.	kIx.	.
</s>
<s>
Můžeš	moct	k5eAaImIp2nS	moct
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
vysléci	vysléct	k5eAaPmF	vysléct
do	do	k7c2	do
košile	košile	k1gFnSc2	košile
<g/>
,	,	kIx,	,
připravuj	připravovat	k5eAaImRp2nS	připravovat
si	se	k3xPyFc3	se
na	na	k7c4	na
únor	únor	k1gInSc4	únor
kožich	kožich	k1gInSc1	kožich
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
zima	zima	k1gFnSc1	zima
a	a	k8xC	a
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
prý	prý	k9	prý
horký	horký	k2eAgInSc4d1	horký
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
když	když	k8xS	když
skřivan	skřivan	k1gMnSc1	skřivan
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
zima	zima	k1gFnSc1	zima
potom	potom	k6eAd1	potom
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
skřivánek	skřivánek	k1gMnSc1	skřivánek
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
nosem	nos	k1gInSc7	nos
slívá	slívat	k5eAaImIp3nS	slívat
<g/>
.	.	kIx.	.
</s>
<s>
Skřivánek	Skřivánek	k1gMnSc1	Skřivánek
<g/>
-li	i	k?	-li
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
zpívat	zpívat	k5eAaImF	zpívat
počne	počnout	k5eAaPmIp3nS	počnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
jistě	jistě	k6eAd1	jistě
umlkne	umlknout	k5eAaPmIp3nS	umlknout
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
únor	únor	k1gInSc1	únor
mírný	mírný	k2eAgInSc1d1	mírný
ve	v	k7c4	v
své	svůj	k3xOyFgNnSc4	svůj
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
připílí	připílit	k5eAaPmIp3nS	připílit
s	s	k7c7	s
mrazem	mráz	k1gInSc7	mráz
březen	březen	k1gInSc1	březen
i	i	k8xC	i
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
mušky	muška	k1gFnSc2	muška
lítajú	lítajú	k?	lítajú
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
marcu	marcus	k1gInSc6	marcus
robky	robka	k1gFnSc2	robka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
chuchajú	chuchajú	k?	chuchajú
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
února	únor	k1gInSc2	únor
měsíce	měsíc	k1gInSc2	měsíc
jest	být	k5eAaImIp3nS	být
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
nepohrdej	pohrdat	k5eNaImRp2nS	pohrdat
hned	hned	k6eAd1	hned
pící	píce	k1gFnSc7	píce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ti	ty	k3xPp2nSc3	ty
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
ráda	rád	k2eAgFnSc1d1	ráda
zima	zima	k1gFnSc1	zima
po	po	k7c6	po
Veliké	veliký	k2eAgFnSc6d1	veliká
noci	noc	k1gFnSc6	noc
zahrává	zahrávat	k5eAaImIp3nS	zahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
<g/>
-li	i	k?	-li
severňák	severňák	k1gMnSc1	severňák
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
váti	vát	k5eAaImF	vát
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
musí	muset	k5eAaImIp3nS	muset
přec	přec	k9	přec
jenom	jenom	k6eAd1	jenom
státi	stát	k5eAaPmF	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
prudký	prudký	k2eAgInSc4d1	prudký
severníček	severníček	k1gInSc4	severníček
–	–	k?	–
hojné	hojný	k2eAgFnSc2d1	hojná
úrody	úroda	k1gFnSc2	úroda
bývá	bývat	k5eAaImIp3nS	bývat
poslíček	poslíček	k1gMnSc1	poslíček
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
půlnoční	půlnoční	k2eAgInPc1d1	půlnoční
větrové	vítr	k1gInPc1wB	vítr
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
silné	silný	k2eAgNnSc4d1	silné
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
dobrá	dobrý	k2eAgFnSc1d1	dobrá
čáka	čáka	k1gFnSc1	čáka
úrody	úroda	k1gFnSc2	úroda
na	na	k7c4	na
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
;	;	kIx,	;
pakli	pakli	k8xS	pakli
ale	ale	k9	ale
ne	ne	k9	ne
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
přicházejí	přicházet	k5eAaImIp3nP	přicházet
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
máji	máj	k1gInSc6	máj
a	a	k8xC	a
škodí	škodit	k5eAaImIp3nP	škodit
vínu	víno	k1gNnSc3	víno
a	a	k8xC	a
stromům	strom	k1gInPc3	strom
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
únor	únor	k1gInSc4	únor
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
únor	únor	k1gInSc4	únor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
Únor	únor	k1gInSc1	únor
1948	[number]	k4	1948
</s>
