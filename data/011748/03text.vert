<p>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
jsou	být	k5eAaImIp3nP	být
pohoří	pohoří	k1gNnSc4	pohoří
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
Žilinský	žilinský	k2eAgInSc1d1	žilinský
a	a	k8xC	a
Prešovský	prešovský	k2eAgInSc1d1	prešovský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
Malopolské	malopolský	k2eAgNnSc1d1	Malopolské
vojvodství	vojvodství	k1gNnSc1	vojvodství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byly	být	k5eAaImAgFnP	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
<g/>
)	)	kIx)	)
a	a	k8xC	a
Polska	Polska	k1gFnSc1	Polska
(	(	kIx(	(
<g/>
Rysy	rys	k1gInPc1	rys
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Tatry	Tatra	k1gFnSc2	Tatra
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
slova	slovo	k1gNnSc2	slovo
Tritri	Tritr	k1gFnSc2	Tritr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
skály	skála	k1gFnPc1	skála
či	či	k8xC	či
skalní	skalní	k2eAgInPc1d1	skalní
štíty	štít	k1gInPc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
dovození	dovození	k1gNnSc4	dovození
od	od	k7c2	od
keltského	keltský	k2eAgNnSc2d1	keltské
slova	slovo	k1gNnSc2	slovo
tamtra	tamtrum	k1gNnSc2	tamtrum
(	(	kIx(	(
<g/>
hnědý	hnědý	k2eAgInSc1d1	hnědý
či	či	k8xC	či
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
též	též	k9	též
připomíná	připomínat	k5eAaImIp3nS	připomínat
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
kamen	kamna	k1gNnPc2	kamna
či	či	k8xC	či
štěrk	štěrk	k1gInSc4	štěrk
–	–	k?	–
toltry	toltr	k1gInPc4	toltr
<g/>
.	.	kIx.	.
</s>
<s>
Tatry	Tatra	k1gFnSc2	Tatra
by	by	kYmCp3nP	by
pak	pak	k6eAd1	pak
znamenaly	znamenat	k5eAaImAgFnP	znamenat
skalnaté	skalnatý	k2eAgFnPc1d1	skalnatá
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
999	[number]	k4	999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
české	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
sahalo	sahat	k5eAaImAgNnS	sahat
až	až	k9	až
po	po	k7c6	po
"	"	kIx"	"
<g/>
Tritri	Tritr	k1gFnSc6	Tritr
montes	montesa	k1gFnPc2	montesa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
Tatry	Tatra	k1gFnSc2	Tatra
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1086	[number]	k4	1086
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
německého	německý	k2eAgMnSc2d1	německý
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
ohraničil	ohraničit	k5eAaPmAgMnS	ohraničit
pražské	pražský	k2eAgNnSc4d1	Pražské
biskupství	biskupství	k1gNnSc4	biskupství
horami	hora	k1gFnPc7	hora
"	"	kIx"	"
<g/>
Tritri	Tritr	k1gInPc7	Tritr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1125	[number]	k4	1125
se	se	k3xPyFc4	se
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
název	název	k1gInSc4	název
Tatri	Tatr	k1gFnSc2	Tatr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
785	[number]	k4	785
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
610	[number]	k4	610
km2	km2	k4	km2
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
a	a	k8xC	a
175	[number]	k4	175
km2	km2	k4	km2
na	na	k7c6	na
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
jsou	být	k5eAaImIp3nP	být
jediným	jediný	k2eAgNnSc7d1	jediné
pohořím	pohoří	k1gNnSc7	pohoří
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
alpínský	alpínský	k2eAgInSc4d1	alpínský
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
celého	celý	k2eAgInSc2d1	celý
karpatského	karpatský	k2eAgInSc2d1	karpatský
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
25	[number]	k4	25
vrcholů	vrchol	k1gInPc2	vrchol
vyšších	vysoký	k2eAgInPc2d2	vyšší
než	než	k8xS	než
2	[number]	k4	2
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
Tater	Tatra	k1gFnPc2	Tatra
byla	být	k5eAaImAgFnS	být
vymodelována	vymodelovat	k5eAaPmNgFnS	vymodelovat
vodou	voda	k1gFnSc7	voda
respektive	respektive	k9	respektive
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Tater	Tatra	k1gFnPc2	Tatra
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
Váh	Váh	k1gInSc1	Váh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Dunajec	Dunajec	k1gInSc1	Dunajec
nebo	nebo	k8xC	nebo
Poprad	Poprad	k1gInSc1	Poprad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
===	===	k?	===
</s>
</p>
<p>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
a	a	k8xC	a
z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
2	[number]	k4	2
podcelky	podcelek	k1gInPc4	podcelek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnPc1d1	východní
Tatry	Tatra	k1gFnPc1	Tatra
-	-	kIx~	-
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
:	:	kIx,	:
ve	v	k7c6	v
Spišském	spišský	k2eAgInSc6d1	spišský
regionu	region	k1gInSc6	region
(	(	kIx(	(
<g/>
Prešovský	prešovský	k2eAgInSc4d1	prešovský
kraj	kraj	k1gInSc4	kraj
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
území	území	k1gNnSc6	území
Malopolského	malopolský	k2eAgNnSc2d1	Malopolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnPc1d1	západní
Tatry	Tatra	k1gFnPc1	Tatra
-	-	kIx~	-
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
:	:	kIx,	:
Liptově	Liptův	k2eAgInSc6d1	Liptův
a	a	k8xC	a
Oravě	Orava	k1gFnSc3	Orava
(	(	kIx(	(
<g/>
Žilinský	žilinský	k2eAgInSc1d1	žilinský
a	a	k8xC	a
Prešovský	prešovský	k2eAgInSc1d1	prešovský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Malopolského	malopolský	k2eAgNnSc2d1	Malopolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
.	.	kIx.	.
<g/>
Hranici	hranice	k1gFnSc3	hranice
mezi	mezi	k7c7	mezi
Západními	západní	k2eAgFnPc7d1	západní
a	a	k8xC	a
Východními	východní	k2eAgFnPc7d1	východní
Tatrami	Tatra	k1gFnPc7	Tatra
tvoří	tvořit	k5eAaImIp3nP	tvořit
Tichá	tichý	k2eAgFnSc1d1	tichá
dolina	dolina	k1gFnSc1	dolina
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
Dolina	dolina	k1gFnSc1	dolina
Suchej	Suchej	k?	Suchej
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
mapa	mapa	k1gFnSc1	mapa
a	a	k8xC	a
tabulka	tabulka	k1gFnSc1	tabulka
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
rozdělení	rozdělení	k1gNnSc3	rozdělení
Východních	východní	k2eAgFnPc2d1	východní
a	a	k8xC	a
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vrcholů	vrchol	k1gInPc2	vrchol
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
okrsků	okrsek	k1gInPc2	okrsek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Štíty	štít	k1gInPc1	štít
a	a	k8xC	a
vrcholy	vrchol	k1gInPc1	vrchol
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Tater	Tatra	k1gFnPc2	Tatra
je	být	k5eAaImIp3nS	být
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2655	[number]	k4	2655
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
vrcholy	vrchol	k1gInPc7	vrchol
podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
jsou	být	k5eAaImIp3nP	být
Gerlachovská	Gerlachovský	k2eAgNnPc1d1	Gerlachovský
veža	vežum	k1gNnPc1	vežum
(	(	kIx(	(
<g/>
2642	[number]	k4	2642
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lomnický	lomnický	k2eAgInSc1d1	lomnický
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2632	[number]	k4	2632
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ľadový	Ľadový	k2eAgInSc1d1	Ľadový
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2627	[number]	k4	2627
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pyšný	pyšný	k2eAgInSc1d1	pyšný
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2621	[number]	k4	2621
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zadný	Zadný	k1gMnSc1	Zadný
Gerlach	Gerlach	k1gMnSc1	Gerlach
(	(	kIx(	(
<g/>
2616	[number]	k4	2616
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lavínový	Lavínový	k2eAgInSc1d1	Lavínový
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2606	[number]	k4	2606
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malý	malý	k2eAgInSc1d1	malý
Ľadový	Ľadový	k2eAgInSc1d1	Ľadový
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2602	[number]	k4	2602
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kotlový	kotlový	k2eAgInSc1d1	kotlový
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2601	[number]	k4	2601
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lavínová	Lavínový	k2eAgFnSc1d1	Lavínový
veža	veža	k1gFnSc1	veža
(	(	kIx(	(
<g/>
2600	[number]	k4	2600
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgNnSc1d1	přístupné
po	po	k7c6	po
turistických	turistický	k2eAgFnPc6d1	turistická
trasách	trasa	k1gFnPc6	trasa
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
následující	následující	k2eAgInPc1d1	následující
vrcholy	vrchol	k1gInPc1	vrchol
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Svinica	Svinica	k1gFnSc1	Svinica
(	(	kIx(	(
<g/>
2301	[number]	k4	2301
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kriváň	Kriváň	k1gInSc1	Kriváň
(	(	kIx(	(
<g/>
2495	[number]	k4	2495
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kôprovský	Kôprovský	k2eAgInSc1d1	Kôprovský
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2363	[number]	k4	2363
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rysy	rys	k1gInPc1	rys
(	(	kIx(	(
<g/>
2503	[number]	k4	2503
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hraniční	hraniční	k2eAgInSc4d1	hraniční
vrchol	vrchol	k1gInSc4	vrchol
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
možný	možný	k2eAgInSc1d1	možný
přechod	přechod	k1gInSc1	přechod
hranice	hranice	k1gFnSc2	hranice
</s>
</p>
<p>
<s>
Predné	Predný	k2eAgNnSc1d1	Predné
Solisko	solisko	k1gNnSc1	solisko
(	(	kIx(	(
<g/>
2093	[number]	k4	2093
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Východná	Východný	k2eAgFnSc1d1	Východná
Vysoká	vysoká	k1gFnSc1	vysoká
(	(	kIx(	(
<g/>
2429	[number]	k4	2429
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1	slavkovský
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2452	[number]	k4	2452
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Svišťovka	Svišťovka	k1gFnSc1	Svišťovka
(	(	kIx(	(
<g/>
2037	[number]	k4	2037
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jahňací	Jahňací	k2eAgInSc1d1	Jahňací
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
2230	[number]	k4	2230
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
Na	na	k7c4	na
Gerlachovský	Gerlachovský	k2eAgInSc4d1	Gerlachovský
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
Vysokú	Vysokú	k1gFnSc4	Vysokú
(	(	kIx(	(
<g/>
2547	[number]	k4	2547
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ľadový	Ľadový	k2eAgInSc1d1	Ľadový
či	či	k8xC	či
Lomnický	lomnický	k2eAgInSc1d1	lomnický
štít	štít	k1gInSc1	štít
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dostat	dostat	k5eAaPmF	dostat
jen	jen	k9	jen
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
horského	horský	k2eAgMnSc2d1	horský
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Lomnický	lomnický	k2eAgInSc4d1	lomnický
štít	štít	k1gInSc4	štít
vede	vést	k5eAaImIp3nS	vést
lanovka	lanovka	k1gFnSc1	lanovka
z	z	k7c2	z
Tatranské	tatranský	k2eAgFnSc2d1	Tatranská
Lomnice	Lomnice	k1gFnSc2	Lomnice
přes	přes	k7c4	přes
Skalnaté	skalnatý	k2eAgNnSc4d1	skalnaté
pleso	pleso	k1gNnSc4	pleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Západních	západní	k2eAgFnPc6d1	západní
Tatrách	Tatra	k1gFnPc6	Tatra
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
tyto	tento	k3xDgInPc1	tento
vrcholy	vrchol	k1gInPc1	vrchol
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bystrá	bystrý	k2eAgFnSc1d1	bystrá
(	(	kIx(	(
<g/>
2248	[number]	k4	2248
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jakubina	Jakubina	k1gFnSc1	Jakubina
(	(	kIx(	(
<g/>
2194	[number]	k4	2194
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Baranec	Baranec	k1gInSc1	Baranec
(	(	kIx(	(
<g/>
2185	[number]	k4	2185
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Baníkov	Baníkov	k1gInSc1	Baníkov
(	(	kIx(	(
<g/>
2178	[number]	k4	2178
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plačlivô	Plačlivô	k?	Plačlivô
<g/>
/	/	kIx~	/
<g/>
Plačlivé	plačlivý	k2eAgFnSc2d1	plačlivý
(	(	kIx(	(
<g/>
2125	[number]	k4	2125
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ostrý	ostrý	k2eAgInSc1d1	ostrý
Roháč	roháč	k1gInSc1	roháč
(	(	kIx(	(
<g/>
2088	[number]	k4	2088
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Volovec	Volovec	k1gInSc1	Volovec
(	(	kIx(	(
<g/>
2063	[number]	k4	2063
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brestová	Brestová	k1gFnSc1	Brestová
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sivý	sivý	k2eAgInSc1d1	sivý
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Údolí	údolí	k1gNnSc6	údolí
===	===	k?	===
</s>
</p>
<p>
<s>
Tatranská	tatranský	k2eAgNnPc1d1	Tatranské
údolí	údolí	k1gNnPc1	údolí
jsou	být	k5eAaImIp3nP	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
protipólem	protipól	k1gInSc7	protipól
tatranských	tatranský	k2eAgInPc2d1	tatranský
hřebenů	hřeben	k1gInPc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
asi	asi	k9	asi
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Východných	Východný	k2eAgFnPc6d1	Východná
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Batizovská	Batizovský	k2eAgFnSc1d1	Batizovská
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Bielovodská	Bielovodský	k2eAgFnSc1d1	Bielovodská
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Kežmarskej	Kežmarskej	k?	Kežmarskej
Bielej	Bielej	k1gFnSc1	Bielej
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
siedmich	siedmicha	k1gFnPc2	siedmicha
prameňov	prameňov	k1gInSc4	prameňov
</s>
</p>
<p>
<s>
Furkotská	Furkotský	k2eAgFnSc1d1	Furkotská
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Javorová	javorový	k2eAgFnSc1d1	Javorová
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Kôprová	Kôprový	k2eAgFnSc1d1	Kôprová
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Studená	studený	k2eAgFnSc1d1	studená
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Mengusovská	Mengusovský	k2eAgFnSc1d1	Mengusovská
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Mlynická	Mlynický	k2eAgFnSc1d1	Mlynická
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Starolesnianska	Starolesniansko	k1gNnPc1	Starolesniansko
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Tichá	tichý	k2eAgFnSc1d1	tichá
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Velická	Velický	k2eAgFnSc1d1	Velická
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Studená	studený	k2eAgFnSc1d1	studená
dolinana	dolinana	k1gFnSc1	dolinana
polské	polský	k2eAgFnPc1d1	polská
straně	strana	k1gFnSc3	strana
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Chochołowska	Chochołowsk	k1gInSc2	Chochołowsk
z	z	k7c2	z
Jarząbczou	Jarząbcza	k1gFnSc7	Jarząbcza
a	a	k8xC	a
Starobociańskou	Starobociańska	k1gFnSc7	Starobociańska
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Lejowa	Lejow	k1gInSc2	Lejow
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Kościeliska	Kościeliska	k1gFnSc1	Kościeliska
z	z	k7c2	z
Tomanowou	Tomanowa	k1gMnSc7	Tomanowa
a	a	k8xC	a
Miętusią	Miętusią	k1gMnSc7	Miętusią
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Małej	Małej	k1gInSc1	Małej
Łąki	Łąk	k1gFnSc2	Łąk
<g/>
,	,	kIx,	,
Zza	Zza	k1gMnSc2	Zza
Bramką	Bramką	k1gMnSc2	Bramką
<g/>
,	,	kIx,	,
Strążyska	Strążysek	k1gMnSc2	Strążysek
<g/>
,	,	kIx,	,
Ku	k	k7c3	k
Dziurze	Dziurze	k1gFnSc5	Dziurze
<g/>
,	,	kIx,	,
Białego	Białega	k1gFnSc5	Białega
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Bystrej	Bystrej	k?	Bystrej
z	z	k7c2	z
Kondratowou	Kondratowý	k2eAgFnSc4d1	Kondratowý
<g/>
,	,	kIx,	,
Goryczkowou	Goryczkowý	k2eAgFnSc4d1	Goryczkowý
<g/>
,	,	kIx,	,
Kasprowou	Kasprowý	k2eAgFnSc4d1	Kasprowý
i	i	k8xC	i
Jaworzyńskou	Jaworzyńský	k2eAgFnSc4d1	Jaworzyńský
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Olczyska	Olczysk	k1gInSc2	Olczysk
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Suchej	Suchej	k?	Suchej
Wody	Wody	k1gInPc1	Wody
Gąsienicowej	Gąsienicowej	k1gInSc1	Gąsienicowej
s	s	k7c7	s
Dolinou	dolina	k1gFnSc7	dolina
Gąsienicowou	Gąsienicowý	k2eAgFnSc7d1	Gąsienicowý
a	a	k8xC	a
s	s	k7c7	s
Dolinou	dolina	k1gFnSc7	dolina
Pańszczycy	Pańszczyca	k1gFnSc2	Pańszczyca
</s>
</p>
<p>
<s>
Dolina	dolina	k1gFnSc1	dolina
Filipka	Filipka	k1gFnSc1	Filipka
</s>
</p>
<p>
<s>
hraniční	hraniční	k2eAgFnSc1d1	hraniční
Dolina	dolina	k1gFnSc1	dolina
Bialky	Bialka	k1gFnSc2	Bialka
s	s	k7c7	s
Dolinou	dolina	k1gFnSc7	dolina
Rybiego	Rybiego	k6eAd1	Rybiego
Potoku	potok	k1gInSc3	potok
<g/>
,	,	kIx,	,
Dolinou	dolina	k1gFnSc7	dolina
Pięciu	Pięcium	k1gNnSc6	Pięcium
Stawów	Stawów	k1gMnSc1	Stawów
Polskich	Polskich	k1gMnSc1	Polskich
<g/>
,	,	kIx,	,
Doliną	Doliną	k1gMnSc1	Doliną
Roztoki	Roztok	k1gFnSc2	Roztok
a	a	k8xC	a
Dolinou	dolina	k1gFnSc7	dolina
Waksmundzkou	Waksmundzka	k1gFnSc7	Waksmundzka
(	(	kIx(	(
<g/>
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
straně	strana	k1gFnSc6	strana
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Doliny	dolina	k1gFnSc2	dolina
Bialky	Bialka	k1gFnSc2	Bialka
Bielovodská	Bielovodský	k2eAgFnSc1d1	Bielovodská
dolina	dolina	k1gFnSc1	dolina
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
částmi	část	k1gFnPc7	část
<g/>
)	)	kIx)	)
<g/>
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Západních	západní	k2eAgFnPc6d1	západní
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Žiarska	Žiarska	k1gFnSc1	Žiarska
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Račkova	Račkův	k2eAgFnSc1d1	Račkova
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Jamnícka	Jamnícka	k1gFnSc1	Jamnícka
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Bystrá	bystrý	k2eAgFnSc1d1	bystrá
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Kamenistá	Kamenistý	k2eAgFnSc1d1	Kamenistá
dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Roháčska	Roháčska	k1gFnSc1	Roháčska
dolina	dolina	k1gFnSc1	dolina
a	a	k8xC	a
jinéBielovodská	jinéBielovodský	k2eAgFnSc1d1	jinéBielovodský
dolina	dolina	k1gFnSc1	dolina
je	být	k5eAaImIp3nS	být
jediné	jediné	k1gNnSc4	jediné
údolí	údolí	k1gNnSc2	údolí
Tater	Tatra	k1gFnPc2	Tatra
alpského	alpský	k2eAgInSc2d1	alpský
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
tatranské	tatranský	k2eAgNnSc1d1	Tatranské
údolí	údolí	k1gNnSc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
v	v	k7c6	v
Západních	západní	k2eAgFnPc6d1	západní
Tatrách	Tatra	k1gFnPc6	Tatra
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
strmé	strmý	k2eAgFnPc4d1	strmá
než	než	k8xS	než
údolí	údolí	k1gNnPc1	údolí
ve	v	k7c6	v
Východních	východní	k2eAgFnPc6d1	východní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
také	také	k9	také
hezké	hezký	k2eAgInPc1d1	hezký
a	a	k8xC	a
obklopené	obklopený	k2eAgInPc1d1	obklopený
vysokými	vysoký	k2eAgInPc7d1	vysoký
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodopády	vodopád	k1gInPc4	vodopád
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
vodopády	vodopád	k1gInPc4	vodopád
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vodopády	vodopád	k1gInPc1	vodopád
Studeného	Studeného	k2eAgInSc2d1	Studeného
potoka	potok	k1gInSc2	potok
</s>
</p>
<p>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
vodopád	vodopád	k1gInSc1	vodopád
na	na	k7c6	na
Malém	malý	k2eAgInSc6d1	malý
Studeném	studený	k2eAgInSc6d1	studený
potoce	potok	k1gInSc6	potok
</s>
</p>
<p>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
Skok	skok	k1gInSc1	skok
v	v	k7c6	v
Mlynickej	Mlynickej	k1gFnSc6	Mlynickej
doline	dolinout	k5eAaPmIp3nS	dolinout
</s>
</p>
<p>
<s>
Kmeťov	Kmeťov	k1gInSc1	Kmeťov
vodopád	vodopád	k1gInSc1	vodopád
v	v	k7c6	v
Kôprovskej	Kôprovskej	k1gFnSc6	Kôprovskej
dolině	dolina	k1gFnSc6	dolina
-	-	kIx~	-
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
tatranský	tatranský	k2eAgInSc1d1	tatranský
vodopád	vodopád	k1gInSc1	vodopád
</s>
</p>
<p>
<s>
===	===	k?	===
Jeskyně	jeskyně	k1gFnSc2	jeskyně
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Tater	Tatra	k1gFnPc2	Tatra
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
vody	voda	k1gFnSc2	voda
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
330	[number]	k4	330
zmapovaných	zmapovaný	k2eAgFnPc2d1	zmapovaná
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
krasové	krasový	k2eAgFnPc4d1	krasová
jeskyně	jeskyně	k1gFnPc4	jeskyně
s	s	k7c7	s
krápníkovou	krápníkový	k2eAgFnSc7d1	krápníková
výzdobou	výzdoba	k1gFnSc7	výzdoba
a	a	k8xC	a
s	s	k7c7	s
malými	malý	k2eAgNnPc7d1	malé
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgFnPc2	tento
jeskyň	jeskyně	k1gFnPc2	jeskyně
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
</s>
</p>
<p>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
měsíčního	měsíční	k2eAgInSc2d1	měsíční
stínu	stín	k1gInSc2	stín
-	-	kIx~	-
(	(	kIx(	(
<g/>
delka	delka	k6eAd1	delka
26555	[number]	k4	26555
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wielka	Wielka	k1gMnSc1	Wielka
Śnieżna	Śnieżn	k1gInSc2	Śnieżn
–	–	k?	–
nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
22749	[number]	k4	22749
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wysoka	Wysoka	k1gFnSc1	Wysoka
–	–	k?	–
Za	za	k7c4	za
Siedmiu	Siedmium	k1gNnSc3	Siedmium
Progami	Proga	k1gFnPc7	Proga
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
11660	[number]	k4	11660
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Śnieżna	Śnieżen	k2eAgNnPc1d1	Śnieżen
Studnia	Studnium	k1gNnPc1	Studnium
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
11000	[number]	k4	11000
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
</s>
</p>
<p>
<s>
Wielka	Wielka	k1gMnSc1	Wielka
Śnieżna	Śnieżn	k1gInSc2	Śnieżn
–	–	k?	–
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
(	(	kIx(	(
<g/>
824	[number]	k4	824
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Śnieżna	Śnieżen	k2eAgNnPc1d1	Śnieżen
Studnia	Studnium	k1gNnPc1	Studnium
(	(	kIx(	(
<g/>
hloubka	hloubka	k1gFnSc1	hloubka
759	[number]	k4	759
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bańdzioch	Bańdzioch	k1gInSc1	Bańdzioch
Kominiarski	Kominiarsk	k1gFnSc2	Kominiarsk
(	(	kIx(	(
<g/>
hloubka	hloubka	k1gFnSc1	hloubka
562	[number]	k4	562
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
</s>
</p>
<p>
<s>
Belianska	Beliansko	k1gNnPc4	Beliansko
jaskyňa	jaskyňa	k6eAd1	jaskyňa
–	–	k?	–
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
1752	[number]	k4	1752
m	m	kA	m
<g/>
,	,	kIx,	,
přístupných	přístupný	k2eAgFnPc2d1	přístupná
1275	[number]	k4	1275
m	m	kA	m
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vchod	vchod	k1gInSc1	vchod
v	v	k7c6	v
Tatranskej	Tatranskej	k?	Tatranskej
Kotline	Kotlin	k1gInSc5	Kotlin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Tater	Tatra	k1gFnPc2	Tatra
leží	ležet	k5eAaImIp3nP	ležet
dva	dva	k4xCgInPc1	dva
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tatranský	tatranský	k2eAgInSc1d1	tatranský
národný	národný	k2eAgInSc1d1	národný
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
TANAP	TANAP	kA	TANAP
<g/>
)	)	kIx)	)
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
Tatrzański	Tatrzański	k6eAd1	Tatrzański
Park	park	k1gInSc1	park
Narodowy	Narodowa	k1gFnSc2	Narodowa
(	(	kIx(	(
<g/>
TPN	TPN	kA	TPN
<g/>
)	)	kIx)	)
v	v	k7c6	v
PolskuTANAP	PolskuTANAP	k1gFnSc6	PolskuTANAP
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
TPN	TPN	kA	TPN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
parku	park	k1gInSc3	park
přičleněno	přičleněn	k2eAgNnSc1d1	přičleněno
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
k	k	k7c3	k
6	[number]	k4	6
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
TANAPu	TANAPus	k1gInSc2	TANAPus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
1045	[number]	k4	1045
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
území	území	k1gNnSc4	území
738	[number]	k4	738
km2	km2	k4	km2
<g/>
,	,	kIx,	,
ochranné	ochranný	k2eAgNnSc4d1	ochranné
pásmo	pásmo	k1gNnSc4	pásmo
307	[number]	k4	307
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TPN	TPN	kA	TPN
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
212	[number]	k4	212
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochranářsky	ochranářsky	k6eAd1	ochranářsky
nejhodnotnějším	hodnotný	k2eAgMnSc7d3	nejhodnotnější
je	být	k5eAaImIp3nS	být
pás	pás	k1gInSc4	pás
Tater	Tatra	k1gFnPc2	Tatra
55	[number]	k4	55
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
17	[number]	k4	17
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
Tater	Tatra	k1gFnPc2	Tatra
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Horské	Horské	k2eAgInPc1d1	Horské
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgMnPc4d1	porostlý
převážně	převážně	k6eAd1	převážně
smrkovými	smrkový	k2eAgInPc7d1	smrkový
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
partiích	partie	k1gFnPc6	partie
kosodřevinou	kosodřevina	k1gFnSc7	kosodřevina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pásmem	pásmo	k1gNnSc7	pásmo
kosodřeviny	kosodřevina	k1gFnSc2	kosodřevina
leží	ležet	k5eAaImIp3nS	ležet
pásmo	pásmo	k1gNnSc1	pásmo
bohatých	bohatý	k2eAgFnPc2d1	bohatá
subalpínských	subalpínský	k2eAgFnPc2d1	subalpínská
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
alpínských	alpínský	k2eAgInPc2d1	alpínský
trávníků	trávník	k1gInPc2	trávník
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
vysokohorskou	vysokohorský	k2eAgFnSc7d1	vysokohorská
květenou	květena	k1gFnSc7	květena
s	s	k7c7	s
několika	několik	k4yIc7	několik
tatranskými	tatranský	k2eAgInPc7d1	tatranský
endemity	endemit	k1gInPc7	endemit
<g/>
.	.	kIx.	.
</s>
<s>
Druhově	druhově	k6eAd1	druhově
nejbohatší	bohatý	k2eAgFnPc1d3	nejbohatší
jsou	být	k5eAaImIp3nP	být
Belianské	Belianský	k2eAgFnPc1d1	Belianská
Tatry	Tatra	k1gFnPc1	Tatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgNnSc4d1	typické
zástupce	zástupce	k1gMnSc1	zástupce
tatranských	tatranský	k2eAgMnPc2d1	tatranský
savců	savec	k1gMnPc2	savec
patří	patřit	k5eAaImIp3nS	patřit
kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
<g/>
,	,	kIx,	,
svišť	svišť	k1gMnSc1	svišť
horský	horský	k2eAgMnSc1d1	horský
a	a	k8xC	a
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
horských	horský	k2eAgInPc2d1	horský
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
zde	zde	k6eAd1	zde
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
orel	orel	k1gMnSc1	orel
skalní	skalní	k2eAgMnSc1d1	skalní
<g/>
,	,	kIx,	,
pěvuška	pěvuška	k1gFnSc1	pěvuška
podhorní	podhorní	k2eAgFnSc1d1	podhorní
<g/>
,	,	kIx,	,
linduška	linduška	k1gFnSc1	linduška
horská	horský	k2eAgFnSc1d1	horská
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
dostat	dostat	k5eAaPmF	dostat
po	po	k7c6	po
označených	označený	k2eAgFnPc6d1	označená
stezkách	stezka	k1gFnPc6	stezka
jsou	být	k5eAaImIp3nP	být
Rysy	rys	k1gInPc1	rys
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
Orla	Orel	k1gMnSc4	Orel
Perć	Perć	k1gMnSc4	Perć
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jsou	být	k5eAaImIp3nP	být
stezky	stezka	k1gFnPc1	stezka
v	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
nad	nad	k7c7	nad
horskými	horský	k2eAgFnPc7d1	horská
chatami	chata	k1gFnPc7	chata
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Prolomit	prolomit	k5eAaPmF	prolomit
zákaz	zákaz	k1gInSc4	zákaz
ohrožení	ohrožení	k1gNnSc2	ohrožení
finančního	finanční	k2eAgInSc2d1	finanční
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
jsou	být	k5eAaImIp3nP	být
stezky	stezka	k1gFnPc1	stezka
otevřené	otevřený	k2eAgFnPc1d1	otevřená
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastějšími	častý	k2eAgMnPc7d3	nejčastější
turistickými	turistický	k2eAgMnPc7d1	turistický
návštěvníky	návštěvník	k1gMnPc7	návštěvník
jsou	být	k5eAaImIp3nP	být
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Slováci	Slovák	k1gMnPc1	Slovák
a	a	k8xC	a
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
národy	národ	k1gInPc1	národ
také	také	k9	také
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
oběti	oběť	k1gFnPc4	oběť
těchto	tento	k3xDgInPc2	tento
hor.	hor.	k?	hor.
</s>
</p>
<p>
<s>
==	==	k?	==
Počasí	počasí	k1gNnSc1	počasí
==	==	k?	==
</s>
</p>
<p>
<s>
Počasí	počasí	k1gNnSc1	počasí
má	mít	k5eAaImIp3nS	mít
převážně	převážně	k6eAd1	převážně
horský	horský	k2eAgInSc4d1	horský
až	až	k8xS	až
vysokohorský	vysokohorský	k2eAgInSc4d1	vysokohorský
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
musí	muset	k5eAaImIp3nP	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
náhlými	náhlý	k2eAgFnPc7d1	náhlá
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
pří	přít	k5eAaImIp3nS	přít
výstupech	výstup	k1gInPc6	výstup
na	na	k7c4	na
vrcholy	vrchol	k1gInPc4	vrchol
nebo	nebo	k8xC	nebo
při	při	k7c6	při
přechodech	přechod	k1gInPc6	přechod
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
tatranskými	tatranský	k2eAgNnPc7d1	Tatranské
údolími	údolí	k1gNnPc7	údolí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
trvají	trvat	k5eAaImIp3nP	trvat
většinou	většina	k1gFnSc7	většina
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1000	[number]	k4	1000
m	m	kA	m
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejvhodnějším	vhodný	k2eAgNnSc7d3	nejvhodnější
obdobím	období	k1gNnSc7	období
pro	pro	k7c4	pro
turistické	turistický	k2eAgInPc4d1	turistický
výstupy	výstup	k1gInPc4	výstup
v	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
je	být	k5eAaImIp3nS	být
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
počasí	počasí	k1gNnSc1	počasí
nejstabilnější	stabilní	k2eAgNnSc1d3	nejstabilnější
kvůli	kvůli	k7c3	kvůli
nižším	nízký	k2eAgFnPc3d2	nižší
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
i	i	k9	i
výborná	výborný	k2eAgFnSc1d1	výborná
viditelnost	viditelnost	k1gFnSc1	viditelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nízké	nízký	k2eAgFnPc1d1	nízká
Tatry	Tatra	k1gFnPc1	Tatra
</s>
</p>
<p>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
Tatry	Tatra	k1gFnPc1	Tatra
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Tatry	Tatra	k1gFnSc2	Tatra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tatry	Tatra	k1gFnSc2	Tatra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Tatry	Tatra	k1gFnSc2	Tatra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
na	na	k7c6	na
serveru	server	k1gInSc6	server
www.tatry.cz	www.tatry.cza	k1gFnPc2	www.tatry.cza
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Polské	polský	k2eAgFnPc1d1	polská
Tatry	Tatra	k1gFnPc1	Tatra
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
polské	polský	k2eAgFnSc6d1	polská
části	část	k1gFnSc6	část
Tater	Tatra	k1gFnPc2	Tatra
na	na	k7c4	na
DušeKarpat	DušeKarpat	k1gFnSc4	DušeKarpat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
na	na	k7c6	na
serveru	server	k1gInSc6	server
www.laviny.sk	www.laviny.sk	k1gInSc1	www.laviny.sk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Komentované	komentovaný	k2eAgFnPc4d1	komentovaná
fotografie	fotografia	k1gFnPc4	fotografia
z	z	k7c2	z
Vysokých	vysoká	k1gFnPc2	vysoká
<g/>
,	,	kIx,	,
Nízkých	nízký	k2eAgFnPc2d1	nízká
a	a	k8xC	a
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
</s>
</p>
