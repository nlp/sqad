<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Keys	Keysa	k1gFnPc2	Keysa
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1965	[number]	k4	1965
Burlington	Burlington	k1gInSc1	Burlington
<g/>
,	,	kIx,	,
Vermont	Vermont	k1gInSc1	Vermont
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Peter	Peter	k1gMnSc1	Peter
Pisarczyk	Pisarczyk	k1gMnSc1	Pisarczyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
klávesista	klávesista	k1gMnSc1	klávesista
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Georgem	Georg	k1gMnSc7	Georg
Clintonem	Clinton	k1gMnSc7	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Billyho	Billy	k1gMnSc2	Billy
Powella	Powell	k1gMnSc2	Powell
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
klávesistou	klávesista	k1gMnSc7	klávesista
skupiny	skupina	k1gFnSc2	skupina
Lynyrd	Lynyrd	k1gMnSc1	Lynyrd
Skynyrd	Skynyrd	k1gMnSc1	Skynyrd
<g/>
.	.	kIx.	.
</s>
</p>
