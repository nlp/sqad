<s>
Autoradiopuhelin	Autoradiopuhelin	k2eAgInSc1d1
</s>
<s>
ARP	ARP	kA
(	(	kIx(
<g/>
název	název	k1gInSc1
Autoradiopuhelin	Autoradiopuhelina	k1gFnPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
finštiny	finština	k1gFnSc2
<g/>
,	,	kIx,
česky	česky	k6eAd1
Radiotelefon	radiotelefon	k1gInSc1
do	do	k7c2
auta	auto	k1gNnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
komerčně	komerčně	k6eAd1
ovládaná	ovládaný	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
mobilní	mobilní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
ve	v	k7c6
Finsku	Finsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologie	technologie	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
nulté	nultý	k4xOgFnSc2
generace	generace	k1gFnSc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
měla	mít	k5eAaImAgFnS
buňky	buňka	k1gFnPc4
<g/>
,	,	kIx,
přechod	přechod	k1gInSc1
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
nebyl	být	k5eNaImAgInS
souvislý	souvislý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
a	a	k8xC
výstavba	výstavba	k1gFnSc1
začala	začít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spuštěna	spuštěn	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
a	a	k8xC
dosáhla	dosáhnout	k5eAaPmAgFnS
100	#num#	k4
<g/>
%	%	kIx~
pokrytí	pokrytí	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
se	s	k7c7
140	#num#	k4
základnovými	základnový	k2eAgFnPc7d1
stanicemi	stanice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
ARP	ARP	kA
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2000	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
NMT	NMT	kA
<g/>
9000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ARP	ARP	kA
byl	být	k5eAaImAgInS
úspěch	úspěch	k1gInSc4
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgMnS
velké	velký	k2eAgFnPc4d1
popularity	popularita	k1gFnPc4
(	(	kIx(
<g/>
10800	#num#	k4
uživatelů	uživatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
,	,	kIx,
35000	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
příliš	příliš	k6eAd1
přeplněným	přeplněný	k2eAgMnSc7d1
a	a	k8xC
byl	být	k5eAaImAgInS
postupně	postupně	k6eAd1
nahrazen	nahradit	k5eAaPmNgInS
modernější	moderní	k2eAgFnSc7d2
technologií	technologie	k1gFnSc7
NMT	NMT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
ARP	ARP	kA
bylo	být	k5eAaImAgNnS
dlouho	dlouho	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
mobilní	mobilní	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
se	s	k7c7
100	#num#	k4
<g/>
%	%	kIx~
pokrytím	pokrytí	k1gNnSc7
a	a	k8xC
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
populární	populární	k2eAgNnSc1d1
mezi	mezi	k7c7
několika	několik	k4yIc7
skupinami	skupina	k1gFnPc7
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
ARP	ARP	kA
fungoval	fungovat	k5eAaImAgInS
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
150	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
147.9	147.9	k4
-	-	kIx~
154.875	154.875	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenosový	přenosový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
se	se	k3xPyFc4
pohyboval	pohybovat	k5eAaImAgInS
mezi	mezi	k7c4
1	#num#	k4
až	až	k9
5	#num#	k4
watty	watt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používal	používat	k5eAaImAgMnS
se	se	k3xPyFc4
poloduplexní	poloduplexeň	k1gFnSc7
přenos	přenos	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
že	že	k8xS
nešlo	jít	k5eNaImAgNnS
vysílat	vysílat	k5eAaImF
a	a	k8xC
přijímat	přijímat	k5eAaImF
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
analogový	analogový	k2eAgMnSc1d1
<g/>
,	,	kIx,
neměl	mít	k5eNaImAgInS
žádné	žádný	k3yNgNnSc4
kódování	kódování	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
hovory	hovor	k1gInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
odposlouchávány	odposlouchávat	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínalo	začínat	k5eAaImAgNnS
se	se	k3xPyFc4
s	s	k7c7
ručním	ruční	k2eAgNnSc7d1
přepínacím	přepínací	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
vše	všechen	k3xTgNnSc1
zautomatizováno	zautomatizován	k2eAgNnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
klesl	klesnout	k5eAaPmAgInS
počet	počet	k1gInSc1
uživatelů	uživatel	k1gMnPc2
na	na	k7c4
980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ARP	ARP	kA
nepodporoval	podporovat	k5eNaImAgMnS
předávaní	předávaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
hovory	hovor	k1gInPc7
přerušovaly	přerušovat	k5eAaImAgInP
při	při	k7c6
přesunu	přesun	k1gInSc6
do	do	k7c2
nové	nový	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
buňky	buňka	k1gFnSc2
byl	být	k5eAaImAgInS
přibližně	přibližně	k6eAd1
30	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
ARP	ARP	kA
mobilní	mobilní	k2eAgInPc1d1
terminály	terminál	k1gInPc1
byly	být	k5eAaImAgInP
velmi	velmi	k6eAd1
velké	velký	k2eAgFnPc1d1
a	a	k8xC
vešly	vejít	k5eAaPmAgFnP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
do	do	k7c2
kufrů	kufr	k1gInPc2
aut	auto	k1gNnPc2
<g/>
,	,	kIx,
se	s	k7c7
sluchátky	sluchátko	k1gNnPc7
a	a	k8xC
mikrofonem	mikrofon	k1gInSc7
poblíž	poblíž	k6eAd1
řidičova	řidičův	k2eAgNnSc2d1
sedadla	sedadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ARP	ARP	kA
bylo	být	k5eAaImAgNnS
také	také	k6eAd1
nákladné	nákladný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Standardy	standard	k1gInPc1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
0G	0G	k4
</s>
<s>
PTT	PTT	kA
</s>
<s>
MTS	MTS	kA
</s>
<s>
IMTS	IMTS	kA
</s>
<s>
AMTS	AMTS	kA
</s>
<s>
AMR	AMR	kA
</s>
<s>
0.5	0.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
Autotel	Autotel	k1gMnSc1
<g/>
/	/	kIx~
<g/>
PALM	PALM	kA
</s>
<s>
ARP	ARP	kA
1G	1G	k4
</s>
<s>
NMT	NMT	kA
</s>
<s>
AMPS	AMPS	kA
2G	2G	k4
</s>
<s>
GSM	GSM	kA
</s>
<s>
cdmaOne	cdmaOnout	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
iDEN	iDEN	k?
</s>
<s>
D-AMPS	D-AMPS	k?
<g/>
/	/	kIx~
<g/>
IS-	IS-	k1gFnSc1
<g/>
136	#num#	k4
<g/>
/	/	kIx~
<g/>
TDMA	TDMA	kA
</s>
<s>
PDC	PDC	kA
</s>
<s>
2.5	2.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
GPRS	GPRS	kA
</s>
<s>
2.75	2.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
CDMA2000	CDMA2000	k1gFnSc1
1	#num#	k4
<g/>
xRTT	xRTT	k?
</s>
<s>
EDGE	EDGE	kA
</s>
<s>
EGPRS	EGPRS	kA
3G	3G	k4
</s>
<s>
W-CDMA	W-CDMA	k?
</s>
<s>
UMTS	UMTS	kA
</s>
<s>
FOMA	FOMA	kA
</s>
<s>
CDMA2000	CDMA2000	k4
1	#num#	k4
<g/>
xEV	xEV	k?
</s>
<s>
TD-SCDMA	TD-SCDMA	k?
</s>
<s>
3.5	3.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSDPA	HSDPA	kA
</s>
<s>
3.75	3.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSUPA	HSUPA	kA
Pre-	Pre-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
G	G	kA
</s>
<s>
Mobile	mobile	k1gNnSc1
WiMAX	WiMAX	k1gFnSc2
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
e	e	k0
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
(	(	kIx(
<g/>
E-UTRA	E-UTRA	k1gMnSc1
<g/>
)	)	kIx)
4G	4G	k4
</s>
<s>
WiMAX-Advanced	WiMAX-Advanced	k1gMnSc1
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
Advanced	Advanced	k1gInSc4
5G	5G	k4
</s>
<s>
eMBB	eMBB	k?
</s>
<s>
URLLC	URLLC	kA
</s>
<s>
MMTC	MMTC	kA
</s>
