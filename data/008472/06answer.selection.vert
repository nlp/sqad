<s>
Kirk	Kirk	k1gMnSc1	Kirk
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Michiganské	michiganský	k2eAgFnSc6d1	Michiganská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
Duke	Duke	k1gFnSc6	Duke
University	universita	k1gFnSc2	universita
a	a	k8xC	a
po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
skotské	skotská	k1gFnSc6	skotská
University	universita	k1gFnSc2	universita
of	of	k?	of
St.	st.	kA	st.
Andrews	Andrews	k1gInSc1	Andrews
<g/>
.	.	kIx.	.
</s>
