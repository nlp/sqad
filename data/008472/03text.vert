<p>
<s>
Russell	Russell	k1gMnSc1	Russell
Kirk	Kirk	k1gMnSc1	Kirk
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Plymouth	Plymouth	k1gInSc1	Plymouth
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
Mecosta	Mecosta	k1gMnSc1	Mecosta
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politický	politický	k2eAgMnSc1d1	politický
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svými	svůj	k3xOyFgMnPc7	svůj
názory	názor	k1gInPc4	názor
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
americký	americký	k2eAgInSc1d1	americký
konzervatismus	konzervatismus	k1gInSc1	konzervatismus
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Kirk	Kirk	k1gMnSc1	Kirk
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Michiganské	michiganský	k2eAgFnSc6d1	Michiganská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
Duke	Duke	k1gFnSc6	Duke
University	universita	k1gFnSc2	universita
a	a	k8xC	a
po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
skotské	skotská	k1gFnSc6	skotská
University	universita	k1gFnSc2	universita
of	of	k?	of
St.	st.	kA	st.
Andrews	Andrews	k1gInSc1	Andrews
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
asi	asi	k9	asi
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
dílo	dílo	k1gNnSc1	dílo
The	The	k1gFnSc2	The
Conservative	Conservativ	k1gInSc5	Conservativ
Mind	Minda	k1gFnPc2	Minda
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
jako	jako	k8xS	jako
Konzervativní	konzervativní	k2eAgNnSc4d1	konzervativní
smýšlení	smýšlení	k1gNnSc4	smýšlení
<g/>
,	,	kIx,	,
překládáno	překládat	k5eAaImNgNnS	překládat
občas	občas	k6eAd1	občas
i	i	k9	i
jako	jako	k9	jako
Konzervativní	konzervativní	k2eAgNnSc4d1	konzervativní
myšlení	myšlení	k1gNnSc4	myšlení
nebo	nebo	k8xC	nebo
Konzervativní	konzervativní	k2eAgFnSc4d1	konzervativní
mysl	mysl	k1gFnSc4	mysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Russell	Russell	k1gMnSc1	Russell
Kirk	Kirk	k1gMnSc1	Kirk
byl	být	k5eAaImAgMnS	být
pravidelným	pravidelný	k2eAgMnSc7d1	pravidelný
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
časopisu	časopis	k1gInSc2	časopis
National	National	k1gFnSc1	National
Review	Review	k1gFnSc1	Review
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
až	až	k9	až
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
vydavatelem	vydavatel	k1gMnSc7	vydavatel
čtvrtletníku	čtvrtletník	k1gInSc2	čtvrtletník
Modern	Modern	k1gMnSc1	Modern
Age	Age	k1gMnSc1	Age
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kirk	Kirk	k1gMnSc1	Kirk
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
novinových	novinový	k2eAgInPc2d1	novinový
a	a	k8xC	a
časopisových	časopisový	k2eAgInPc2d1	časopisový
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
změna	změna	k1gFnSc1	změna
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Civilizovaná	civilizovaný	k2eAgFnSc1d1	civilizovaná
společnost	společnost	k1gFnSc1	společnost
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgFnPc4	svůj
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
třídy	třída	k1gFnPc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
neoddělitelně	oddělitelně	k6eNd1	oddělitelně
spjaté	spjatý	k2eAgInPc4d1	spjatý
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
často	často	k6eAd1	často
obhajoval	obhajovat	k5eAaImAgInS	obhajovat
a	a	k8xC	a
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
názory	názor	k1gInPc4	názor
Edmunda	Edmund	k1gMnSc4	Edmund
Burka	burka	k1gFnSc1	burka
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Burke	Burke	k1gInSc1	Burke
uznával	uznávat	k5eAaImAgInS	uznávat
užitečnost	užitečnost	k1gFnSc4	užitečnost
některých	některý	k3yIgInPc2	některý
předsudků	předsudek	k1gInPc2	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odpůrcem	odpůrce	k1gMnSc7	odpůrce
marxismu	marxismus	k1gInSc2	marxismus
a	a	k8xC	a
multikulturalismu	multikulturalismus	k1gInSc2	multikulturalismus
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
vývoz	vývoz	k1gInSc4	vývoz
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
stavěl	stavět	k5eAaImAgMnS	stavět
se	se	k3xPyFc4	se
kriticky	kriticky	k6eAd1	kriticky
ke	k	k7c3	k
spojenectví	spojenectví	k1gNnSc3	spojenectví
konzervatismu	konzervatismus	k1gInSc2	konzervatismus
a	a	k8xC	a
libertariánství	libertariánství	k1gNnSc2	libertariánství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
amerických	americký	k2eAgFnPc6d1	americká
neokonzervativcích	neokonzervativec	k1gMnPc6	neokonzervativec
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stavějí	stavět	k5eAaImIp3nP	stavět
za	za	k7c4	za
myšlenku	myšlenka	k1gFnSc4	myšlenka
"	"	kIx"	"
<g/>
demokratického	demokratický	k2eAgInSc2d1	demokratický
globalismu	globalismus	k1gInSc2	globalismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
příliš	příliš	k6eAd1	příliš
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
osvícenskému	osvícenský	k2eAgNnSc3d1	osvícenské
myšlení	myšlení	k1gNnSc3	myšlení
a	a	k8xC	a
evropské	evropský	k2eAgFnSc3d1	Evropská
levici	levice	k1gFnSc3	levice
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zapomínají	zapomínat	k5eAaImIp3nP	zapomínat
na	na	k7c4	na
zájmy	zájem	k1gInPc4	zájem
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
chybou	chyba	k1gFnSc7	chyba
je	být	k5eAaImIp3nS	být
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
<g/>
.	.	kIx.	.
<g/>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
čestné	čestný	k2eAgFnSc2d1	čestná
rady	rada	k1gFnSc2	rada
Občanského	občanský	k2eAgInSc2d1	občanský
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Konzervatismus	konzervatismus	k1gInSc1	konzervatismus
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Russell	Russell	k1gMnSc1	Russell
Kirk	Kirk	k1gMnSc1	Kirk
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Fingerland	Fingerlanda	k1gFnPc2	Fingerlanda
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc4	možnost
konservatismu	konservatismus	k1gInSc2	konservatismus
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
,	,	kIx,	,
doc	doc	kA	doc
</s>
</p>
<p>
<s>
https://web.archive.org/web/20071006074915/http://www.obcinst.cz/clanek.asp?id=507	[url]	k4	https://web.archive.org/web/20071006074915/http://www.obcinst.cz/clanek.asp?id=507
Jan	Jan	k1gMnSc1	Jan
Fingerland	Fingerland	k1gInSc1	Fingerland
<g/>
:	:	kIx,	:
KONZERVATISMUS	konzervatismus	k1gInSc1	konzervatismus
RUSSELLA	RUSSELLA	kA	RUSSELLA
KIRKA	KIRKA	kA	KIRKA
A	a	k9	a
MICHAELA	Michaela	k1gFnSc1	Michaela
OAKESHOTTA	OAKESHOTTA	kA	OAKESHOTTA
</s>
</p>
