<s>
Intel	Intel	kA	Intel
8085	[number]	k4	8085
je	být	k5eAaImIp3nS	být
osmibitový	osmibitový	k2eAgInSc1d1	osmibitový
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
firmy	firma	k1gFnSc2	firma
Intel	Intel	kA	Intel
uvedený	uvedený	k2eAgInSc4d1	uvedený
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Dodával	dodávat	k5eAaImAgInS	dodávat
se	se	k3xPyFc4	se
v	v	k7c6	v
40	[number]	k4	40
<g/>
pinovém	pinový	k2eAgInSc6d1	pinový
DIP	DIP	kA	DIP
provedení	provedení	k1gNnSc2	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Pětka	pětka	k1gFnSc1	pětka
v	v	k7c6	v
názvu	název	k1gInSc6	název
znamenala	znamenat	k5eAaImAgNnP	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
procesor	procesor	k1gInSc1	procesor
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
<g/>
voltové	voltový	k2eAgNnSc1d1	voltové
napětí	napětí	k1gNnSc1	napětí
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
+5	+5	k4	+5
V	V	kA	V
<g/>
,	,	kIx,	,
−	−	k?	−
V	V	kA	V
a	a	k8xC	a
+12	+12	k4	+12
V	V	kA	V
u	u	k7c2	u
Intel	Intel	kA	Intel
8080	[number]	k4	8080
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8085	[number]	k4	8085
byl	být	k5eAaImAgInS	být
občas	občas	k6eAd1	občas
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
jako	jako	k9	jako
mikrořadič	mikrořadič	k1gInSc4	mikrořadič
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
menšímu	malý	k2eAgInSc3d2	menší
počtu	počet	k1gInSc3	počet
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
běhu	běh	k1gInSc3	běh
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
řadič	řadič	k1gInSc1	řadič
se	s	k7c7	s
8085	[number]	k4	8085
mohl	moct	k5eAaImAgInS	moct
těšit	těšit	k5eAaImF	těšit
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
do	do	k7c2	do
rozšiřující	rozšiřující	k2eAgFnSc2d1	rozšiřující
řadičové	řadičové	k?	řadičové
karty	karta	k1gFnSc2	karta
DECTape	DECTap	k1gInSc5	DECTap
vydržel	vydržet	k5eAaPmAgMnS	vydržet
sloužit	sloužit	k5eAaImF	sloužit
často	často	k6eAd1	často
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
životnost	životnost	k1gFnSc4	životnost
těchto	tento	k3xDgInPc2	tento
výrobků	výrobek	k1gInPc2	výrobek
i	i	k8xC	i
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
tepal	tepat	k5eAaImAgInS	tepat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
zastíněn	zastínit	k5eAaPmNgInS	zastínit
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
procesorem	procesor	k1gInSc7	procesor
Zilog	Zilog	k1gInSc4	Zilog
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
éru	éra	k1gFnSc4	éra
osmibitových	osmibitový	k2eAgInPc2d1	osmibitový
domácích	domácí	k2eAgInPc2d1	domácí
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
von	von	k1gInSc1	von
Neumannova	Neumannův	k2eAgFnSc1d1	Neumannova
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
bitová	bitový	k2eAgFnSc1d1	bitová
adresová	adresový	k2eAgFnSc1d1	adresová
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
bitová	bitový	k2eAgFnSc1d1	bitová
datová	datový	k2eAgFnSc1d1	datová
a	a	k8xC	a
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
.	.	kIx.	.
</s>
<s>
Registry	registr	k1gInPc1	registr
A	A	kA	A
(	(	kIx(	(
<g/>
střadač	střadač	k1gInSc1	střadač
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
accumulator	accumulator	k1gInSc1	accumulator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
H	H	kA	H
a	a	k8xC	a
L.	L.	kA	L.
Speciální	speciální	k2eAgInPc1d1	speciální
registry	registr	k1gInPc1	registr
jako	jako	k8xC	jako
Program	program	k1gInSc1	program
Counter	Countra	k1gFnPc2	Countra
<g/>
,	,	kIx,	,
Stack	Stack	k1gInSc1	Stack
Pointer	pointer	k1gInSc1	pointer
<g/>
,	,	kIx,	,
Flag	flag	k1gInSc1	flag
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc1	tři
maskovatelná	maskovatelný	k2eAgNnPc1d1	maskovatelný
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
nemaskovatelné	maskovatelný	k2eNgNnSc1d1	nemaskovatelné
přerušení	přerušení	k1gNnSc1	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Dopředně	dopředně	k6eAd1	dopředně
kompatibilní	kompatibilní	k2eAgInSc1d1	kompatibilní
s	s	k7c7	s
Intel	Intel	kA	Intel
8080	[number]	k4	8080
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
pomalejším	pomalý	k2eAgFnPc3d2	pomalejší
pamětem	paměť	k1gFnPc3	paměť
přes	přes	k7c4	přes
čekací	čekací	k2eAgInPc4d1	čekací
stavy	stav	k1gInPc4	stav
(	(	kIx(	(
<g/>
Wait	Wait	k2eAgInSc4d1	Wait
states	states	k1gInSc4	states
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
–	–	k?	–
DMA	dmout	k5eAaImSgInS	dmout
<g/>
.	.	kIx.	.
</s>
<s>
Taktovací	taktovací	k2eAgFnSc1d1	taktovací
frekvence	frekvence	k1gFnSc1	frekvence
3,07	[number]	k4	3,07
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
výpočetní	výpočetní	k2eAgInSc4d1	výpočetní
výkon	výkon	k1gInSc4	výkon
0	[number]	k4	0
37	[number]	k4	37
MIPS	MIPS	kA	MIPS
<g/>
.	.	kIx.	.
6	[number]	k4	6
500	[number]	k4	500
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgInSc1d1	výrobní
proces	proces	k1gInSc1	proces
3	[number]	k4	3
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Binárně	binárně	k6eAd1	binárně
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
s	s	k7c7	s
8080	[number]	k4	8080
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
tvrdému	tvrdý	k2eAgNnSc3d1	tvrdé
záření	záření	k1gNnSc3	záření
byla	být	k5eAaImAgFnS	být
jistá	jistý	k2eAgFnSc1d1	jistá
verze	verze	k1gFnSc1	verze
8085	[number]	k4	8085
vybrána	vybrat	k5eAaPmNgFnS	vybrat
jako	jako	k9	jako
procesor	procesor	k1gInSc1	procesor
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
misích	mise	k1gFnPc6	mise
NASA	NASA	kA	NASA
a	a	k8xC	a
ESA	eso	k1gNnSc2	eso
<g/>
,	,	kIx,	,
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
fyziku	fyzika	k1gFnSc4	fyzika
vesmíru	vesmír	k1gInSc2	vesmír
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
strojírenských	strojírenský	k2eAgFnPc6d1	strojírenská
školách	škola	k1gFnPc6	škola
Jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
je	být	k5eAaImIp3nS	být
8085	[number]	k4	8085
používán	používat	k5eAaImNgMnS	používat
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
mikroprocesorové	mikroprocesorový	k2eAgFnSc2d1	mikroprocesorová
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
emulátor	emulátor	k1gInSc1	emulátor
tohoto	tento	k3xDgInSc2	tento
procesoru	procesor	k1gInSc2	procesor
–	–	k?	–
GNUSim	GNUSim	k1gInSc1	GNUSim
<g/>
8085	[number]	k4	8085
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
1.3	[number]	k4	1.3
<g/>
.6	.6	k4	.6
/	/	kIx~	/
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Intel	Intel	kA	Intel
8085	[number]	k4	8085
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
