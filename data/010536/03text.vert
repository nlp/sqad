<p>
<s>
Povodí	povodí	k1gNnSc1	povodí
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
voda	voda	k1gFnSc1	voda
odtéká	odtékat	k5eAaImIp3nS	odtékat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
řeky	řeka	k1gFnSc2	řeka
či	či	k8xC	či
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
povodími	povodí	k1gNnPc7	povodí
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rozvodí	rozvodí	k1gNnSc1	rozvodí
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
povodí	povodí	k1gNnPc1	povodí
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
moře	moře	k1gNnSc2	moře
či	či	k8xC	či
oceánu	oceán	k1gInSc2	oceán
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
úmoří	úmoří	k1gNnSc4	úmoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povodí	povodí	k1gNnSc1	povodí
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
pro	pro	k7c4	pro
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
toků	tok	k1gInPc2	tok
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
svými	svůj	k3xOyFgInPc7	svůj
biogeochemickými	biogeochemický	k2eAgInPc7d1	biogeochemický
cykly	cyklus	k1gInPc7	cyklus
navázána	navázat	k5eAaPmNgFnS	navázat
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
při	při	k7c6	při
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
toků	tok	k1gInPc2	tok
lze	lze	k6eAd1	lze
vycházet	vycházet	k5eAaImF	vycházet
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
hydrologické	hydrologický	k2eAgFnSc2d1	hydrologická
bilance	bilance	k1gFnSc2	bilance
povodí	povodí	k1gNnSc2	povodí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
srážkami	srážka	k1gFnPc7	srážka
a	a	k8xC	a
průtokem	průtok	k1gInSc7	průtok
na	na	k7c6	na
konci	konec	k1gInSc6	konec
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
toků	tok	k1gInPc2	tok
včetně	včetně	k7c2	včetně
veškerých	veškerý	k3xTgNnPc2	veškerý
protipovodňových	protipovodňový	k2eAgNnPc2d1	protipovodňové
opatření	opatření	k1gNnPc2	opatření
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
povodí	povodí	k1gNnPc2	povodí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
povodí	povodí	k1gNnSc1	povodí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
:	:	kIx,	:
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
6	[number]	k4	6
915	[number]	k4	915
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řádovost	řádovost	k1gFnSc4	řádovost
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
posloupnost	posloupnost	k1gFnSc1	posloupnost
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hydrologické	hydrologický	k2eAgNnSc4d1	hydrologické
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
používaný	používaný	k2eAgInSc1d1	používaný
systém	systém	k1gInSc1	systém
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
toky	tok	k1gInPc7	tok
1	[number]	k4	1
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
ty	ten	k3xDgInPc4	ten
vodní	vodní	k2eAgInPc4d1	vodní
toky	tok	k1gInPc4	tok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přímo	přímo	k6eAd1	přímo
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
či	či	k8xC	či
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nezohledňuje	zohledňovat	k5eNaImIp3nS	zohledňovat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gInSc1	jeho
průtok	průtok	k1gInSc1	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Přítoky	přítok	k1gInPc4	přítok
toků	tok	k1gInPc2	tok
1	[number]	k4	1
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
toky	tok	k1gInPc1	tok
2	[number]	k4	2
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
přítoky	přítok	k1gInPc4	přítok
toků	tok	k1gInPc2	tok
2	[number]	k4	2
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c7	za
toky	tok	k1gInPc7	tok
3	[number]	k4	3
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hierarchie	hierarchie	k1gFnSc1	hierarchie
takto	takto	k6eAd1	takto
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
i	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
povodní	povodeň	k1gFnPc2	povodeň
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezodtoké	bezodtoký	k2eAgFnSc6d1	bezodtoká
oblasti	oblast	k1gFnSc6	oblast
==	==	k?	==
</s>
</p>
<p>
<s>
Ne	ne	k9	ne
všechna	všechen	k3xTgNnPc1	všechen
povodí	povodí	k1gNnPc1	povodí
nutně	nutně	k6eAd1	nutně
náležejí	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
nějakému	nějaký	k3yIgNnSc3	nějaký
úmoří	úmoří	k1gNnSc1	úmoří
–	–	k?	–
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
či	či	k8xC	či
Austrálii	Austrálie	k1gFnSc6	Austrálie
existují	existovat	k5eAaImIp3nP	existovat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
postrádající	postrádající	k2eAgInSc4d1	postrádající
odtok	odtok	k1gInSc4	odtok
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
území	území	k1gNnPc1	území
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
bezodtoké	bezodtoký	k2eAgFnPc1d1	bezodtoká
oblasti	oblast	k1gFnPc1	oblast
nebo	nebo	k8xC	nebo
též	též	k9	též
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
odtokem	odtok	k1gInSc7	odtok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dílčí	dílčí	k2eAgNnPc1d1	dílčí
povodí	povodí	k1gNnPc1	povodí
</s>
</p>
<p>
<s>
Rozvodí	rozvodí	k1gNnSc1	rozvodí
</s>
</p>
<p>
<s>
Úmoří	úmoří	k1gNnSc1	úmoří
</s>
</p>
<p>
<s>
Povodí	povodí	k1gNnSc1	povodí
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
povodí	povodí	k1gNnSc2	povodí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
povodí	povodí	k1gNnSc2	povodí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Úvodí	úvodí	k1gNnSc2	úvodí
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
světových	světový	k2eAgNnPc2d1	světové
povodí	povodí	k1gNnPc2	povodí
</s>
</p>
