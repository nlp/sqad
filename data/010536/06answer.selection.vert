<s>
Povodí	povodí	k1gNnSc1	povodí
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
voda	voda	k1gFnSc1	voda
odtéká	odtékat	k5eAaImIp3nS	odtékat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
řeky	řeka	k1gFnSc2	řeka
či	či	k8xC	či
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
