<s>
Psychologie	psychologie	k1gFnSc1	psychologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ψ	ψ	k?	ψ
(	(	kIx(	(
<g/>
psyché	psyché	k1gFnSc1	psyché
<g/>
)	)	kIx)	)
=	=	kIx~	=
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
dech	dech	k1gInSc1	dech
a	a	k8xC	a
-λ	-λ	k?	-λ
(	(	kIx(	(
<g/>
-logia	ogia	k1gFnSc1	-logia
<g/>
)	)	kIx)	)
=	=	kIx~	=
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
lidské	lidský	k2eAgNnSc4d1	lidské
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
mentální	mentální	k2eAgInPc4d1	mentální
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
tělesné	tělesný	k2eAgNnSc4d1	tělesné
dění	dění	k1gNnSc4	dění
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gInPc2	jejich
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
interakcí	interakce	k1gFnPc2	interakce
(	(	kIx(	(
<g/>
souhrnně	souhrnně	k6eAd1	souhrnně
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
psychika	psychika	k1gFnSc1	psychika
<g/>
)	)	kIx)	)
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
popsat	popsat	k5eAaPmF	popsat
<g/>
,	,	kIx,	,
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
a	a	k8xC	a
predikovat	predikovat	k5eAaBmF	predikovat
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
psychologie	psychologie	k1gFnSc2	psychologie
je	být	k5eAaImIp3nS	být
také	také	k9	také
získané	získaný	k2eAgInPc4d1	získaný
poznatky	poznatek	k1gInPc4	poznatek
využít	využít	k5eAaPmF	využít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
lidské	lidský	k2eAgFnSc2d1	lidská
spokojenosti	spokojenost	k1gFnSc2	spokojenost
a	a	k8xC	a
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
psychoterapie	psychoterapie	k1gFnSc2	psychoterapie
jich	on	k3xPp3gMnPc2	on
lze	lze	k6eAd1	lze
navíc	navíc	k6eAd1	navíc
využít	využít	k5eAaPmF	využít
i	i	k9	i
k	k	k7c3	k
léčebným	léčebný	k2eAgInPc3d1	léčebný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	s	k7c7	s
psychologií	psychologie	k1gFnSc7	psychologie
výzkumně	výzkumně	k6eAd1	výzkumně
i	i	k9	i
prakticky	prakticky	k6eAd1	prakticky
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xC	jako
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
provádějící	provádějící	k2eAgFnSc4d1	provádějící
psychoterapii	psychoterapie	k1gFnSc4	psychoterapie
je	být	k5eAaImIp3nS	být
psychoterapeut	psychoterapeut	k1gMnSc1	psychoterapeut
(	(	kIx(	(
<g/>
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
bývají	bývat	k5eAaImIp3nP	bývat
tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
směšovány	směšován	k2eAgInPc1d1	směšován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psychologové	psycholog	k1gMnPc1	psycholog
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
porozumět	porozumět	k5eAaPmF	porozumět
roli	role	k1gFnSc4	role
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
individuálním	individuální	k2eAgInSc6d1	individuální
i	i	k8xC	i
sociálním	sociální	k2eAgNnSc6d1	sociální
chování	chování	k1gNnSc6	chování
včetně	včetně	k7c2	včetně
porozumění	porozumění	k1gNnSc2	porozumění
fyziologickým	fyziologický	k2eAgInPc3d1	fyziologický
a	a	k8xC	a
neurobiologickým	urobiologický	k2eNgInPc3d1	neurobiologický
procesům	proces	k1gInPc3	proces
podmiňujícím	podmiňující	k2eAgMnSc6d1	podmiňující
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Psychologové	psycholog	k1gMnPc1	psycholog
objevili	objevit	k5eAaPmAgMnP	objevit
a	a	k8xC	a
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
řadu	řada	k1gFnSc4	řada
klíčových	klíčový	k2eAgInPc2d1	klíčový
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
konceptů	koncept	k1gInPc2	koncept
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
percepce	percepce	k1gFnSc1	percepce
<g/>
,	,	kIx,	,
kognice	kognice	k1gFnSc1	kognice
<g/>
,	,	kIx,	,
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
motivace	motivace	k1gFnSc1	motivace
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
interpersonální	interpersonální	k2eAgInPc4d1	interpersonální
vztahy	vztah	k1gInPc4	vztah
či	či	k8xC	či
podvědomí	podvědomí	k1gNnPc4	podvědomí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
získávání	získávání	k1gNnSc3	získávání
jsou	být	k5eAaImIp3nP	být
vyvíjeny	vyvíjet	k5eAaImNgFnP	vyvíjet
empirické	empirický	k2eAgFnPc1d1	empirická
metody	metoda	k1gFnPc1	metoda
výzkumu	výzkum	k1gInSc2	výzkum
pro	pro	k7c4	pro
objevování	objevování	k1gNnSc4	objevování
a	a	k8xC	a
vysvětlování	vysvětlování	k1gNnSc1	vysvětlování
příčinných	příčinný	k2eAgInPc2d1	příčinný
i	i	k8xC	i
korelačních	korelační	k2eAgInPc2d1	korelační
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
proměnnými	proměnná	k1gFnPc7	proměnná
<g/>
,	,	kIx,	,
i	i	k8xC	i
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
prostému	prostý	k2eAgInSc3d1	prostý
popisu	popis	k1gInSc3	popis
a	a	k8xC	a
interpretaci	interpretace	k1gFnSc3	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
sociální	sociální	k2eAgFnPc4d1	sociální
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
se	se	k3xPyFc4	se
však	však	k9	však
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
plynulém	plynulý	k2eAgNnSc6d1	plynulé
rozmezí	rozmezí	k1gNnSc6	rozmezí
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výzkum	výzkum	k1gInSc4	výzkum
z	z	k7c2	z
věd	věda	k1gFnPc2	věda
přírodních	přírodní	k2eAgFnPc2d1	přírodní
(	(	kIx(	(
<g/>
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
např.	např.	kA	např.
zákonů	zákon	k1gInPc2	zákon
lidského	lidský	k2eAgNnSc2d1	lidské
vnímání	vnímání	k1gNnSc2	vnímání
a	a	k8xC	a
percepce	percepce	k1gFnSc2	percepce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
i	i	k8xC	i
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
až	až	k9	až
po	po	k7c4	po
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Psychologické	psychologický	k2eAgInPc1d1	psychologický
poznatky	poznatek	k1gInPc1	poznatek
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
zjištění	zjištění	k1gNnSc6	zjištění
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
vznik	vznik	k1gInSc4	vznik
zákonů	zákon	k1gInPc2	zákon
regulujících	regulující	k2eAgInPc2d1	regulující
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
náhled	náhled	k1gInSc4	náhled
na	na	k7c4	na
homosexualitu	homosexualita	k1gFnSc4	homosexualita
či	či	k8xC	či
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
za	za	k7c2	za
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
trestně	trestně	k6eAd1	trestně
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
většina	většina	k1gFnSc1	většina
psychologů	psycholog	k1gMnPc2	psycholog
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
psychodiagnostických	psychodiagnostický	k2eAgInPc6d1	psychodiagnostický
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
léčením	léčení	k1gNnSc7	léčení
mentálních	mentální	k2eAgFnPc2d1	mentální
poruch	porucha	k1gFnPc2	porucha
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
potíží	potíž	k1gFnPc2	potíž
formou	forma	k1gFnSc7	forma
psychoterapie	psychoterapie	k1gFnPc1	psychoterapie
či	či	k8xC	či
poradenství	poradenství	k1gNnPc1	poradenství
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
používá	používat	k5eAaImIp3nS	používat
aplikované	aplikovaný	k2eAgInPc4d1	aplikovaný
psychologické	psychologický	k2eAgInPc4d1	psychologický
poznatky	poznatek	k1gInPc4	poznatek
například	například	k6eAd1	například
v	v	k7c6	v
personalistice	personalistika	k1gFnSc6	personalistika
nebo	nebo	k8xC	nebo
marketingu	marketing	k1gInSc6	marketing
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
psychologů	psycholog	k1gMnPc2	psycholog
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zabývá	zabývat	k5eAaImIp3nS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
i	i	k8xC	i
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologické	psychologický	k2eAgInPc4d1	psychologický
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
dnes	dnes	k6eAd1	dnes
existuje	existovat	k5eAaImIp3nS	existovat
nejméně	málo	k6eAd3	málo
pět	pět	k4xCc4	pět
významných	významný	k2eAgInPc2d1	významný
a	a	k8xC	a
vlivných	vlivný	k2eAgInPc2d1	vlivný
teoretických	teoretický	k2eAgInPc2d1	teoretický
přístupů	přístup	k1gInPc2	přístup
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgMnSc7d1	vlastní
a	a	k8xC	a
svébytným	svébytný	k2eAgInSc7d1	svébytný
způsobem	způsob	k1gInSc7	způsob
definuje	definovat	k5eAaBmIp3nS	definovat
svůj	svůj	k3xOyFgInSc4	svůj
vědecký	vědecký	k2eAgInSc4d1	vědecký
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
psychologii	psychologie	k1gFnSc4	psychologie
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
definicí	definice	k1gFnSc7	definice
samotné	samotný	k2eAgFnSc2d1	samotná
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdílná	rozdílný	k2eAgNnPc4d1	rozdílné
témata	téma	k1gNnPc4	téma
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
často	často	k6eAd1	často
svébytné	svébytný	k2eAgInPc4d1	svébytný
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
přístupy	přístup	k1gInPc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Seamona	Seamon	k1gMnSc2	Seamon
a	a	k8xC	a
Kenricka	Kenricka	k1gFnSc1	Kenricka
lze	lze	k6eAd1	lze
různé	různý	k2eAgInPc4d1	různý
přístupy	přístup	k1gInPc4	přístup
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
odlišným	odlišný	k2eAgFnPc3d1	odlišná
mapám	mapa	k1gFnPc3	mapa
stejného	stejný	k2eAgNnSc2d1	stejné
města	město	k1gNnSc2	město
–	–	k?	–
každá	každý	k3xTgFnSc1	každý
využívá	využívat	k5eAaImIp3nS	využívat
jinou	jiný	k2eAgFnSc4d1	jiná
perspektivu	perspektiva	k1gFnSc4	perspektiva
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
linie	linie	k1gFnPc4	linie
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c6	na
topografii	topografie	k1gFnSc6	topografie
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
na	na	k7c4	na
podzemní	podzemní	k2eAgFnSc4d1	podzemní
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
přístupy	přístup	k1gInPc1	přístup
vznikaly	vznikat	k5eAaImAgInP	vznikat
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
jednotné	jednotný	k2eAgNnSc4d1	jednotné
psychologické	psychologický	k2eAgNnSc4d1	psychologické
paradigma	paradigma	k1gNnSc4	paradigma
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
vyhraňovaly	vyhraňovat	k5eAaImAgFnP	vyhraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
však	však	k9	však
mezi	mezi	k7c7	mezi
přístupy	přístup	k1gInPc7	přístup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
opět	opět	k6eAd1	opět
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgNnSc3d1	pozvolné
sbližování	sbližování	k1gNnSc3	sbližování
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
chápány	chápat	k5eAaImNgInP	chápat
spíše	spíše	k9	spíše
jako	jako	k9	jako
různé	různý	k2eAgInPc1d1	různý
aspekty	aspekt	k1gInPc1	aspekt
téhož	týž	k3xTgInSc2	týž
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
teoretické	teoretický	k2eAgInPc4d1	teoretický
přístupy	přístup	k1gInPc4	přístup
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
patří	patřit	k5eAaImIp3nS	patřit
následující	následující	k2eAgFnSc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Biologická	biologický	k2eAgFnSc1d1	biologická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Biologický	biologický	k2eAgInSc1d1	biologický
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
neurofyziologických	neurofyziologický	k2eAgInPc2d1	neurofyziologický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
technickými	technický	k2eAgInPc7d1	technický
<g/>
"	"	kIx"	"
aspekty	aspekt	k1gInPc7	aspekt
vnímání	vnímání	k1gNnSc2	vnímání
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
neurofyziologické	neurofyziologický	k2eAgInPc1d1	neurofyziologický
procesy	proces	k1gInPc1	proces
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
prožívání	prožívání	k1gNnSc2	prožívání
<g/>
,	,	kIx,	,
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
díky	díky	k7c3	díky
možnosti	možnost	k1gFnSc3	možnost
aplikace	aplikace	k1gFnSc2	aplikace
biologických	biologický	k2eAgInPc2d1	biologický
experimentů	experiment	k1gInPc2	experiment
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
myšlenkových	myšlenkový	k2eAgInPc2d1	myšlenkový
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
postupy	postup	k1gInPc4	postup
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
pozitronová	pozitronový	k2eAgFnSc1d1	pozitronová
emisní	emisní	k2eAgFnSc1d1	emisní
tomografie	tomografie	k1gFnSc1	tomografie
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgFnSc1d1	funkční
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
přístup	přístup	k1gInSc1	přístup
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
evoluční	evoluční	k2eAgFnSc1d1	evoluční
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
genetické	genetický	k2eAgInPc4d1	genetický
základy	základ	k1gInPc4	základ
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
představitelé	představitel	k1gMnPc1	představitel
vesměs	vesměs	k6eAd1	vesměs
navázali	navázat	k5eAaPmAgMnP	navázat
na	na	k7c4	na
Darwinovo	Darwinův	k2eAgNnSc4d1	Darwinovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
svázaný	svázaný	k2eAgInSc1d1	svázaný
s	s	k7c7	s
biologickou	biologický	k2eAgFnSc7d1	biologická
psychologií	psychologie	k1gFnSc7	psychologie
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
psychologických	psychologický	k2eAgFnPc2d1	psychologická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Biologický	biologický	k2eAgInSc1d1	biologický
přístup	přístup	k1gInSc1	přístup
chápe	chápat	k5eAaImIp3nS	chápat
například	například	k6eAd1	například
depresi	deprese	k1gFnSc4	deprese
jako	jako	k8xS	jako
poruchu	porucha	k1gFnSc4	porucha
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
abnormálních	abnormální	k2eAgFnPc2d1	abnormální
změn	změna	k1gFnPc2	změna
nepříznivě	příznivě	k6eNd1	příznivě
ovlivňujících	ovlivňující	k2eAgMnPc2d1	ovlivňující
hladinu	hladina	k1gFnSc4	hladina
neurotransmiterů	neurotransmiter	k1gMnPc2	neurotransmiter
<g/>
.	.	kIx.	.
</s>
<s>
Biologický	biologický	k2eAgInSc1d1	biologický
přístup	přístup	k1gInSc1	přístup
přispěl	přispět	k5eAaPmAgInS	přispět
i	i	k9	i
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dětská	dětský	k2eAgFnSc1d1	dětská
amnézie	amnézie	k1gFnSc1	amnézie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zčásti	zčásti	k6eAd1	zčásti
způsobena	způsobit	k5eAaPmNgFnS	způsobit
nezralostí	nezralost	k1gFnSc7	nezralost
hipokampu	hipokamp	k1gInSc2	hipokamp
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
upevňování	upevňování	k1gNnSc4	upevňování
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
mozku	mozek	k1gInSc2	mozek
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
roku	rok	k1gInSc2	rok
až	až	k6eAd1	až
dvou	dva	k4xCgInPc2	dva
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
ještě	ještě	k6eAd1	ještě
plně	plně	k6eAd1	plně
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
tohoto	tento	k3xDgInSc2	tento
přístupu	přístup	k1gInSc2	přístup
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
Roger	Roger	k1gMnSc1	Roger
W.	W.	kA	W.
Sperry	Sperr	k1gInPc1	Sperr
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
James	James	k1gMnSc1	James
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
pak	pak	k6eAd1	pak
František	František	k1gMnSc1	František
Koukolík	Koukolík	k1gMnSc1	Koukolík
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Behaviorismus	behaviorismus	k1gInSc1	behaviorismus
<g/>
.	.	kIx.	.
</s>
<s>
Behaviorální	behaviorální	k2eAgInSc1d1	behaviorální
přístup	přístup	k1gInSc1	přístup
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
behavioristický	behavioristický	k2eAgMnSc1d1	behavioristický
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dominantním	dominantní	k2eAgMnSc7d1	dominantní
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
omezil	omezit	k5eAaPmAgMnS	omezit
se	se	k3xPyFc4	se
na	na	k7c4	na
objektivně	objektivně	k6eAd1	objektivně
pozorovatelná	pozorovatelný	k2eAgNnPc4d1	pozorovatelné
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
zcela	zcela	k6eAd1	zcela
vynechal	vynechat	k5eAaPmAgInS	vynechat
lidskou	lidský	k2eAgFnSc4d1	lidská
mysl	mysl	k1gFnSc4	mysl
(	(	kIx(	(
<g/>
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xS	jako
black	black	k6eAd1	black
box	box	k1gInSc4	box
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tzv.	tzv.	kA	tzv.
model	model	k1gInSc1	model
S-R	S-R	k1gFnSc1	S-R
(	(	kIx(	(
<g/>
stimulus	stimulus	k1gInSc1	stimulus
<g/>
–	–	k?	–
<g/>
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
odpůrci	odpůrce	k1gMnPc1	odpůrce
označován	označován	k2eAgMnSc1d1	označován
proto	proto	k8xC	proto
jako	jako	k9	jako
"	"	kIx"	"
<g/>
psychologie	psychologie	k1gFnSc1	psychologie
bez	bez	k7c2	bez
duše	duše	k1gFnSc2	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
striktní	striktní	k2eAgInSc1d1	striktní
behaviorismus	behaviorismus	k1gInSc1	behaviorismus
spíše	spíše	k9	spíše
historií	historie	k1gFnSc7	historie
<g/>
,	,	kIx,	,
navázaly	navázat	k5eAaPmAgInP	navázat
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
neobehaviorismus	neobehaviorismus	k1gInSc4	neobehaviorismus
či	či	k8xC	či
teorie	teorie	k1gFnPc4	teorie
sociálního	sociální	k2eAgNnSc2d1	sociální
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
model	model	k1gInSc1	model
S-O-R	S-O-R	k1gFnSc2	S-O-R
(	(	kIx(	(
<g/>
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
objekt	objekt	k1gInSc4	objekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
představitele	představitel	k1gMnPc4	představitel
behaviorální	behaviorální	k2eAgFnSc2d1	behaviorální
psychologie	psychologie	k1gFnSc2	psychologie
patří	patřit	k5eAaImIp3nS	patřit
její	její	k3xOp3gMnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
John	John	k1gMnSc1	John
B.	B.	kA	B.
Watson	Watson	k1gMnSc1	Watson
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Burrhus	Burrhus	k1gMnSc1	Burrhus
Frederic	Frederic	k1gMnSc1	Frederic
Skinner	Skinner	k1gMnSc1	Skinner
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Bandura	bandur	k1gMnSc2	bandur
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
behaviorismem	behaviorismus	k1gInSc7	behaviorismus
souvisejí	souviset	k5eAaImIp3nP	souviset
i	i	k9	i
ruští	ruský	k2eAgMnPc1d1	ruský
reflexologové	reflexolog	k1gMnPc1	reflexolog
Sečenov	Sečenovo	k1gNnPc2	Sečenovo
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Petrovič	Petrovič	k1gMnSc1	Petrovič
Pavlov	Pavlovo	k1gNnPc2	Pavlovo
či	či	k8xC	či
Bechtěrev	Bechtěrva	k1gFnPc2	Bechtěrva
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Psychodynamický	psychodynamický	k2eAgInSc1d1	psychodynamický
přístup	přístup	k1gInSc1	přístup
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
též	též	k9	též
jako	jako	k8xS	jako
hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
)	)	kIx)	)
vesměs	vesměs	k6eAd1	vesměs
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
a	a	k8xC	a
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
především	především	k6eAd1	především
nevědomé	vědomý	k2eNgFnPc1d1	nevědomá
oblasti	oblast	k1gFnPc1	oblast
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
dominantní	dominantní	k2eAgInSc4d1	dominantní
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
chování	chování	k1gNnSc6	chování
a	a	k8xC	a
myšlení	myšlení	k1gNnSc6	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
malým	malý	k2eAgFnPc3d1	malá
možnostem	možnost	k1gFnPc3	možnost
ověřování	ověřování	k1gNnSc2	ověřování
hypotéz	hypotéza	k1gFnPc2	hypotéza
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejkontroverznějším	kontroverzní	k2eAgInPc3d3	nejkontroverznější
psychologickým	psychologický	k2eAgInPc3d1	psychologický
proudům	proud	k1gInPc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
osobnosti	osobnost	k1gFnPc4	osobnost
psychodynamického	psychodynamický	k2eAgInSc2d1	psychodynamický
přístupu	přístup	k1gInSc2	přístup
patří	patřit	k5eAaImIp3nS	patřit
zakladatel	zakladatel	k1gMnSc1	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Adler	Adler	k1gMnSc1	Adler
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karen	Karen	k1gInSc1	Karen
<g />
.	.	kIx.	.
</s>
<s>
Horneyová	Horneyová	k1gFnSc1	Horneyová
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
či	či	k8xC	či
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fenomenologický	fenomenologický	k2eAgInSc1d1	fenomenologický
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
humanisticky	humanisticky	k6eAd1	humanisticky
a	a	k8xC	a
filozoficky	filozoficky	k6eAd1	filozoficky
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
smysl	smysl	k1gInSc4	smysl
lidského	lidský	k2eAgNnSc2d1	lidské
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
prožívání	prožívání	k1gNnSc1	prožívání
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgNnSc1d1	lidské
svědomí	svědomí	k1gNnSc1	svědomí
<g/>
,	,	kIx,	,
osamění	osamění	k1gNnSc1	osamění
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
nedeterministický	deterministický	k2eNgInSc1d1	nedeterministický
a	a	k8xC	a
ústředním	ústřední	k2eAgInSc7d1	ústřední
konceptem	koncept	k1gInSc7	koncept
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
pojem	pojem	k1gInSc1	pojem
svobodná	svobodný	k2eAgFnSc1d1	svobodná
vůle	vůle	k1gFnSc1	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
a	a	k8xC	a
americký	americký	k2eAgInSc1d1	americký
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
:	:	kIx,	:
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
dominantní	dominantní	k2eAgFnSc1d1	dominantní
zejména	zejména	k9	zejména
existenciální	existenciální	k2eAgFnSc1d1	existenciální
analýza	analýza	k1gFnSc1	analýza
a	a	k8xC	a
daseinsanalýza	daseinsanalýza	k1gFnSc1	daseinsanalýza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
psychologové	psycholog	k1gMnPc1	psycholog
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
prožívání	prožívání	k1gNnSc1	prožívání
úzkosti	úzkost	k1gFnSc2	úzkost
<g/>
,	,	kIx,	,
odloučení	odloučení	k1gNnSc2	odloučení
<g/>
,	,	kIx,	,
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
přijetí	přijetí	k1gNnSc1	přijetí
apod.	apod.	kA	apod.
Americký	americký	k2eAgInSc1d1	americký
směr	směr	k1gInSc1	směr
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
humanistická	humanistický	k2eAgFnSc1d1	humanistická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
)	)	kIx)	)
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
aspekty	aspekt	k1gInPc4	aspekt
lidské	lidský	k2eAgFnSc2d1	lidská
přirozenosti	přirozenost	k1gFnSc2	přirozenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
fenomenologické	fenomenologický	k2eAgFnSc3d1	fenomenologická
psychologii	psychologie	k1gFnSc3	psychologie
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
i	i	k9	i
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
konstruktivistické	konstruktivistický	k2eAgInPc4d1	konstruktivistický
směry	směr	k1gInPc4	směr
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
má	mít	k5eAaImIp3nS	mít
fenomenologický	fenomenologický	k2eAgInSc4d1	fenomenologický
přístup	přístup	k1gInSc4	přístup
omezené	omezený	k2eAgFnSc2d1	omezená
možnosti	možnost	k1gFnSc2	možnost
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgMnPc7d1	klíčový
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
Ludwig	Ludwig	k1gMnSc1	Ludwig
Binswanger	Binswanger	k1gMnSc1	Binswanger
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc1	tvůrce
daseinsanalýzy	daseinsanalýza	k1gFnSc2	daseinsanalýza
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Medard	Medard	k1gMnSc1	Medard
Boss	boss	k1gMnSc1	boss
(	(	kIx(	(
<g/>
představitel	představitel	k1gMnSc1	představitel
daseinsanalýzy	daseinsanalýza	k1gFnSc2	daseinsanalýza
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
či	či	k8xC	či
Viktor	Viktor	k1gMnSc1	Viktor
Frankl	Frankl	k1gMnSc1	Frankl
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc1	tvůrce
logoterapie	logoterapie	k1gFnSc2	logoterapie
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Gestaltismus	Gestaltismus	k1gInSc1	Gestaltismus
<g/>
.	.	kIx.	.
</s>
<s>
Gestalt	Gestalt	k1gInSc1	Gestalt
psychologie	psychologie	k1gFnSc2	psychologie
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
tvarová	tvarový	k2eAgFnSc1d1	tvarová
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
od	od	k7c2	od
německého	německý	k2eAgMnSc2d1	německý
Gestalt	Gestalt	k1gInSc4	Gestalt
=	=	kIx~	=
tvar	tvar	k1gInSc4	tvar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
historická	historický	k2eAgFnSc1d1	historická
psychologická	psychologický	k2eAgFnSc1d1	psychologická
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
období	období	k1gNnSc6	období
rozvoje	rozvoj	k1gInSc2	rozvoj
behaviorismu	behaviorismus	k1gInSc2	behaviorismus
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
jejího	její	k3xOp3gNnSc2	její
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
zkoumání	zkoumání	k1gNnSc4	zkoumání
psychických	psychický	k2eAgInPc2d1	psychický
fenoménů	fenomén	k1gInPc2	fenomén
celostní	celostní	k2eAgFnSc2d1	celostní
povahy	povaha	k1gFnSc2	povaha
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
metody	metoda	k1gFnSc2	metoda
introspekce	introspekce	k1gFnSc2	introspekce
<g/>
.	.	kIx.	.
</s>
<s>
Percepční	percepční	k2eAgFnPc1d1	percepční
zkušenosti	zkušenost	k1gFnPc1	zkušenost
podle	podle	k7c2	podle
gestalt	gestalta	k1gFnPc2	gestalta
psychologie	psychologie	k1gFnSc2	psychologie
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
vzorcích	vzorec	k1gInPc6	vzorec
vznikajících	vznikající	k2eAgInPc2d1	vznikající
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
působení	působení	k1gNnSc2	působení
podnětů	podnět	k1gInPc2	podnět
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
zážitků	zážitek	k1gInPc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgNnSc7d1	typické
tvrzením	tvrzení	k1gNnSc7	tvrzení
gestalt	gestaltum	k1gNnPc2	gestaltum
psychologie	psychologie	k1gFnSc2	psychologie
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychické	psychický	k2eAgInPc4d1	psychický
jevy	jev	k1gInPc4	jev
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
pouhou	pouhý	k2eAgFnSc4d1	pouhá
sumu	suma	k1gFnSc4	suma
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vynořují	vynořovat	k5eAaImIp3nP	vynořovat
se	se	k3xPyFc4	se
v	v	k7c6	v
duševním	duševní	k2eAgInSc6d1	duševní
světě	svět	k1gInSc6	svět
jako	jako	k8xC	jako
fenomény	fenomén	k1gInPc1	fenomén
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
–	–	k?	–
že	že	k8xS	že
"	"	kIx"	"
<g/>
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
součet	součet	k1gInSc1	součet
částí	část	k1gFnPc2	část
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
podíváme	podívat	k5eAaPmIp1nP	podívat
na	na	k7c4	na
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
uvidíme	uvidět	k5eAaPmIp1nP	uvidět
spíše	spíše	k9	spíše
dva	dva	k4xCgInPc4	dva
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
než	než	k8xS	než
jako	jako	k9	jako
šest	šest	k4xCc4	šest
malých	malý	k2eAgInPc2d1	malý
obrázků	obrázek	k1gInPc2	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Gestalt	Gestalt	k2eAgMnSc1d1	Gestalt
psychologové	psycholog	k1gMnPc1	psycholog
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
dále	daleko	k6eAd2	daleko
vnímáním	vnímání	k1gNnSc7	vnímání
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
sledovali	sledovat	k5eAaImAgMnP	sledovat
proměny	proměna	k1gFnPc4	proměna
barev	barva	k1gFnPc2	barva
při	při	k7c6	při
změnách	změna	k1gFnPc6	změna
osvětlení	osvětlení	k1gNnSc2	osvětlení
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc4	základ
současného	současný	k2eAgInSc2d1	současný
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kognitivní	kognitivní	k2eAgFnSc2d1	kognitivní
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Gestaltisté	Gestaltista	k1gMnPc1	Gestaltista
byli	být	k5eAaImAgMnP	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněni	ovlivnit	k5eAaPmNgMnP	ovlivnit
filosofickou	filosofický	k2eAgFnSc7d1	filosofická
fenomenologií	fenomenologie	k1gFnSc7	fenomenologie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
objevem	objev	k1gInSc7	objev
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Max	Max	k1gMnSc1	Max
Wertheimer	Wertheimer	k1gMnSc1	Wertheimer
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Koffka	Koffka	k1gFnSc1	Koffka
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
či	či	k8xC	či
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Köhler	Köhler	k1gMnSc1	Köhler
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pokračovatele	pokračovatel	k1gMnPc4	pokračovatel
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
lingvistu	lingvista	k1gMnSc4	lingvista
Noama	Noam	k1gMnSc4	Noam
Chomskeho	Chomske	k1gMnSc4	Chomske
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kognitivní	kognitivní	k2eAgFnSc2d1	kognitivní
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
psychologie	psychologie	k1gFnSc1	psychologie
věnuje	věnovat	k5eAaImIp3nS	věnovat
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
mentální	mentální	k2eAgFnSc4d1	mentální
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
poznávacím	poznávací	k2eAgInPc3d1	poznávací
procesům	proces	k1gInPc3	proces
<g/>
,	,	kIx,	,
a	a	k8xC	a
myšlení	myšlení	k1gNnSc1	myšlení
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
<s>
Lidskou	lidský	k2eAgFnSc4d1	lidská
psychiku	psychika	k1gFnSc4	psychika
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
systém	systém	k1gInSc1	systém
zpracování	zpracování	k1gNnSc2	zpracování
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
část	část	k1gFnSc4	část
terminologie	terminologie	k1gFnSc2	terminologie
si	se	k3xPyFc3	se
vypůjčuje	vypůjčovat	k5eAaImIp3nS	vypůjčovat
například	například	k6eAd1	například
z	z	k7c2	z
počítačové	počítačový	k2eAgFnSc2d1	počítačová
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nP	vytvářet
tzv	tzv	kA	tzv
kognitivní	kognitivní	k2eAgFnPc4d1	kognitivní
mapy	mapa	k1gFnPc4	mapa
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedle	vedle	k6eAd1	vedle
například	například	k6eAd1	například
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc2d1	kulturní
antropologie	antropologie	k1gFnSc2	antropologie
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
současných	současný	k2eAgFnPc2d1	současná
multidisciplinárních	multidisciplinární	k2eAgFnPc2d1	multidisciplinární
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
kognitivní	kognitivní	k2eAgFnSc2d1	kognitivní
psychologie	psychologie	k1gFnSc2	psychologie
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Albert	Albert	k1gMnSc1	Albert
Ellis	Ellis	k1gFnSc2	Ellis
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aaron	Aaron	k1gMnSc1	Aaron
Těmkin	Těmkin	k1gMnSc1	Těmkin
Beck	Beck	k1gMnSc1	Beck
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Simon	Simon	k1gMnSc1	Simon
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
či	či	k8xC	či
Allen	Allen	k1gMnSc1	Allen
Newell	Newell	k1gMnSc1	Newell
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
;	;	kIx,	;
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgMnS	být
Edward	Edward	k1gMnSc1	Edward
C.	C.	kA	C.
Tolman	Tolman	k1gMnSc1	Tolman
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologické	psychologický	k2eAgFnSc2d1	psychologická
disciplíny	disciplína	k1gFnSc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc4d1	široké
pole	pole	k1gNnSc4	pole
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
se	se	k3xPyFc4	se
výzkumem	výzkum	k1gInSc7	výzkum
od	od	k7c2	od
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
možnosti	možnost	k1gFnPc4	možnost
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
osobnostní	osobnostní	k2eAgFnPc4d1	osobnostní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
biologické	biologický	k2eAgNnSc4d1	biologické
pozadí	pozadí	k1gNnSc4	pozadí
lidského	lidský	k2eAgNnSc2d1	lidské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předmětu	předmět	k1gInSc2	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dílčí	dílčí	k2eAgInPc4d1	dílčí
obory	obor	k1gInPc4	obor
<g/>
:	:	kIx,	:
Základní	základní	k2eAgFnSc7d1	základní
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
nejobecnější	obecní	k2eAgInSc4d3	obecní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
teoretické	teoretický	k2eAgFnPc1d1	teoretická
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
zejména	zejména	k9	zejména
biologická	biologický	k2eAgFnSc1d1	biologická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
obecná	obecný	k2eAgFnSc1d1	obecná
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
vývojová	vývojový	k2eAgFnSc1d1	vývojová
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
sem	sem	k6eAd1	sem
bývá	bývat	k5eAaImIp3nS	bývat
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
i	i	k9	i
psychopatologie	psychopatologie	k1gFnSc1	psychopatologie
a	a	k8xC	a
psychologie	psychologie	k1gFnSc1	psychologie
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Aplikované	aplikovaný	k2eAgInPc1d1	aplikovaný
–	–	k?	–
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
psychické	psychický	k2eAgInPc4d1	psychický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
praxí	praxe	k1gFnSc7	praxe
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
praktickou	praktický	k2eAgFnSc4d1	praktická
aplikaci	aplikace	k1gFnSc4	aplikace
obecných	obecný	k2eAgInPc2d1	obecný
teoretických	teoretický	k2eAgInPc2d1	teoretický
přístupů	přístup	k1gInPc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
například	například	k6eAd1	například
klinická	klinický	k2eAgFnSc1d1	klinická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
forenzní	forenzní	k2eAgFnSc1d1	forenzní
psychologie	psychologie	k1gFnSc1	psychologie
či	či	k8xC	či
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
–	–	k?	–
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
ještě	ještě	k9	ještě
uvádějí	uvádět	k5eAaImIp3nP	uvádět
tuto	tento	k3xDgFnSc4	tento
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
disciplíny	disciplína	k1gFnPc4	disciplína
odvozené	odvozený	k2eAgFnPc4d1	odvozená
od	od	k7c2	od
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
úzkým	úzký	k2eAgInSc7d1	úzký
oborem	obor	k1gInSc7	obor
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
psychometrika	psychometrika	k1gFnSc1	psychometrika
<g/>
,	,	kIx,	,
psycholingvistika	psycholingvistika	k1gFnSc1	psycholingvistika
či	či	k8xC	či
psychodiagnostika	psychodiagnostika	k1gFnSc1	psychodiagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
psychologických	psychologický	k2eAgFnPc2d1	psychologická
disciplín	disciplína	k1gFnPc2	disciplína
významné	významný	k2eAgFnSc2d1	významná
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
rozsahu	rozsah	k1gInSc2	rozsah
zaměření	zaměření	k1gNnSc2	zaměření
psychologie	psychologie	k1gFnSc2	psychologie
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
například	například	k6eAd1	například
následující	následující	k2eAgFnPc4d1	následující
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Biologická	biologický	k2eAgFnSc1d1	biologická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Klinická	klinický	k2eAgFnSc1d1	klinická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
Psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
<g/>
,	,	kIx,	,
Poradenství	poradenství	k1gNnSc1	poradenství
a	a	k8xC	a
Psychopatologie	psychopatologie	k1gFnSc1	psychopatologie
<g/>
.	.	kIx.	.
</s>
<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
psychologie	psychologie	k1gFnSc1	psychologie
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
patologickém	patologický	k2eAgNnSc6d1	patologické
fungování	fungování	k1gNnSc6	fungování
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
a	a	k8xC	a
využívá	využívat	k5eAaImIp3nS	využívat
psychodiagnostické	psychodiagnostický	k2eAgInPc4d1	psychodiagnostický
postupy	postup	k1gInPc4	postup
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
určování	určování	k1gNnSc3	určování
jejich	jejich	k3xOp3gInSc2	jejich
typu	typ	k1gInSc2	typ
a	a	k8xC	a
příčiny	příčina	k1gFnPc4	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zjistit	zjistit	k5eAaPmF	zjistit
pacientovy	pacientův	k2eAgFnPc4d1	pacientova
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
případně	případně	k6eAd1	případně
navrhnout	navrhnout	k5eAaPmF	navrhnout
možnosti	možnost	k1gFnPc4	možnost
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
léčby	léčba	k1gFnSc2	léčba
psychologickými	psychologický	k2eAgInPc7d1	psychologický
prostředky	prostředek	k1gInPc7	prostředek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
duševní	duševní	k2eAgFnSc4d1	duševní
poruchu	porucha	k1gFnSc4	porucha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
zdravému	zdravý	k1gMnSc3	zdravý
člověku	člověk	k1gMnSc3	člověk
v	v	k7c6	v
psychické	psychický	k2eAgFnSc6d1	psychická
nouzi	nouze	k1gFnSc6	nouze
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
spíše	spíše	k9	spíše
o	o	k7c6	o
poradenství	poradenství	k1gNnSc6	poradenství
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
manželském	manželský	k2eAgNnSc6d1	manželské
<g/>
,	,	kIx,	,
rodinném	rodinný	k2eAgNnSc6d1	rodinné
<g/>
,	,	kIx,	,
školním	školní	k2eAgNnSc6d1	školní
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teoretickým	teoretický	k2eAgInSc7d1	teoretický
oborem	obor	k1gInSc7	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přímo	přímo	k6eAd1	přímo
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
duševním	duševní	k2eAgNnSc7d1	duševní
zdravím	zdraví	k1gNnSc7	zdraví
a	a	k8xC	a
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
charakteristiky	charakteristika	k1gFnPc4	charakteristika
duševních	duševní	k2eAgFnPc2d1	duševní
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
psychopatologie	psychopatologie	k1gFnPc4	psychopatologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obecná	obecný	k2eAgFnSc1d1	obecná
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
psychologie	psychologie	k1gFnSc1	psychologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
základními	základní	k2eAgInPc7d1	základní
duševními	duševní	k2eAgInPc7d1	duševní
jevy	jev	k1gInPc7	jev
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
kognitivní	kognitivní	k2eAgMnSc1d1	kognitivní
a	a	k8xC	a
emocionální	emocionální	k2eAgInPc1d1	emocionální
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
většinou	většinou	k6eAd1	většinou
vjemů	vjem	k1gInPc2	vjem
od	od	k7c2	od
vnímání	vnímání	k1gNnSc2	vnímání
přes	přes	k7c4	přes
myšlení	myšlení	k1gNnSc4	myšlení
či	či	k8xC	či
prožívání	prožívání	k1gNnSc4	prožívání
lidí	člověk	k1gMnPc2	člověk
až	až	k9	až
po	po	k7c4	po
chování	chování	k1gNnSc4	chování
jako	jako	k8xC	jako
následek	následek	k1gInSc4	následek
uvedených	uvedený	k2eAgInPc2d1	uvedený
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
zájmy	zájem	k1gInPc4	zájem
obecné	obecný	k2eAgFnSc2d1	obecná
psychologie	psychologie	k1gFnSc2	psychologie
patří	patřit	k5eAaImIp3nS	patřit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
příbuzných	příbuzný	k2eAgNnPc2d1	příbuzné
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
klíčovými	klíčový	k2eAgInPc7d1	klíčový
pojmy	pojem	k1gInPc7	pojem
jsou	být	k5eAaImIp3nP	být
psychika	psychika	k1gFnSc1	psychika
<g/>
,	,	kIx,	,
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
prožívání	prožívání	k1gNnSc1	prožívání
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgInPc1d1	psychický
jevy	jev	k1gInPc1	jev
(	(	kIx(	(
<g/>
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
procesy	proces	k1gInPc1	proces
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
spíše	spíše	k9	spíše
záležitostí	záležitost	k1gFnSc7	záležitost
psychologie	psychologie	k1gFnSc2	psychologie
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
spánek	spánek	k1gInSc4	spánek
a	a	k8xC	a
sny	sen	k1gInPc4	sen
<g/>
,	,	kIx,	,
senzorické	senzorický	k2eAgInPc4d1	senzorický
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologie	psychologie	k1gFnSc2	psychologie
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologie	psychologie	k1gFnSc2	psychologie
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sociální	sociální	k2eAgFnSc1d1	sociální
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologie	psychologie	k1gFnSc2	psychologie
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vývojová	vývojový	k2eAgFnSc1d1	vývojová
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologie	psychologie	k1gFnSc2	psychologie
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc1	historie
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
Ebbinghaus	Ebbinghaus	k1gMnSc1	Ebbinghaus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychologie	psychologie	k1gFnSc1	psychologie
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
minulost	minulost	k1gFnSc4	minulost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátkou	krátký	k2eAgFnSc4d1	krátká
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
psychologické	psychologický	k2eAgInPc4d1	psychologický
poznatky	poznatek	k1gInPc4	poznatek
zajímali	zajímat	k5eAaImAgMnP	zajímat
už	už	k6eAd1	už
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
a	a	k8xC	a
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
řada	řada	k1gFnSc1	řada
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
označit	označit	k5eAaPmF	označit
jako	jako	k9	jako
psychologické	psychologický	k2eAgInPc1d1	psychologický
Jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
věda	věda	k1gFnSc1	věda
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
psychologie	psychologie	k1gFnSc1	psychologie
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
věd	věda	k1gFnPc2	věda
přírodních	přírodní	k2eAgFnPc2d1	přírodní
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
<g/>
)	)	kIx)	)
vydělila	vydělit	k5eAaPmAgFnS	vydělit
z	z	k7c2	z
filozofie	filozofie	k1gFnSc2	filozofie
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
patrná	patrný	k2eAgFnSc1d1	patrná
dvě	dva	k4xCgNnPc4	dva
hlavní	hlavní	k2eAgNnPc4d1	hlavní
paradigmata	paradigma	k1gNnPc4	paradigma
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
duchovědné	duchovědný	k2eAgNnSc1d1	duchovědný
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
filozofické	filozofický	k2eAgFnSc6d1	filozofická
<g/>
)	)	kIx)	)
a	a	k8xC	a
přírodovědné	přírodovědný	k2eAgNnSc4d1	Přírodovědné
(	(	kIx(	(
<g/>
objektivistické	objektivistický	k2eAgNnSc4d1	objektivistické
<g/>
,	,	kIx,	,
experimentálně	experimentálně	k6eAd1	experimentálně
podložené	podložený	k2eAgInPc1d1	podložený
a	a	k8xC	a
odvozené	odvozený	k2eAgInPc1d1	odvozený
spíše	spíše	k9	spíše
z	z	k7c2	z
věd	věda	k1gFnPc2	věda
přírodních	přírodní	k2eAgFnPc2d1	přírodní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
zdroje	zdroj	k1gInPc1	zdroj
vlivů	vliv	k1gInPc2	vliv
<g/>
,	,	kIx,	,
kulminujících	kulminující	k2eAgFnPc2d1	kulminující
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
psychologii	psychologie	k1gFnSc6	psychologie
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
antické	antický	k2eAgFnSc6d1	antická
a	a	k8xC	a
předrenesanční	předrenesanční	k2eAgFnSc6d1	předrenesanční
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přece	přece	k9	přece
jen	jen	k9	jen
primárně	primárně	k6eAd1	primárně
historie	historie	k1gFnSc1	historie
idejí	idea	k1gFnPc2	idea
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgNnSc1d1	začínající
renesancí	renesance	k1gFnSc7	renesance
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
duchovní	duchovní	k2eAgFnSc4d1	duchovní
atmosféru	atmosféra	k1gFnSc4	atmosféra
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
času	čas	k1gInSc2	čas
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
psychologie	psychologie	k1gFnSc1	psychologie
součástí	součást	k1gFnPc2	součást
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jejího	její	k3xOp3gMnSc2	její
zakladatele	zakladatel	k1gMnSc2	zakladatel
ve	v	k7c6	v
filozofických	filozofický	k2eAgFnPc6d1	filozofická
vědách	věda	k1gFnPc6	věda
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
(	(	kIx(	(
<g/>
384	[number]	k4	384
<g/>
–	–	k?	–
<g/>
322	[number]	k4	322
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výzva	výzva	k1gFnSc1	výzva
adresovaná	adresovaný	k2eAgFnSc1d1	adresovaná
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
prostupující	prostupující	k2eAgFnSc4d1	prostupující
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
jeho	on	k3xPp3gNnSc2	on
psychologického	psychologický	k2eAgNnSc2d1	psychologické
myšlení	myšlení	k1gNnSc2	myšlení
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Staň	stanout	k5eAaPmRp2nS	stanout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
jsi	být	k5eAaImIp2nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnSc4d1	snažící
se	se	k3xPyFc4	se
o	o	k7c4	o
kauzální	kauzální	k2eAgInSc4d1	kauzální
výklad	výklad	k1gInSc4	výklad
psychických	psychický	k2eAgInPc2d1	psychický
jevů	jev	k1gInPc2	jev
a	a	k8xC	a
přírodovědně	přírodovědně	k6eAd1	přírodovědně
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
rozvinout	rozvinout	k5eAaPmF	rozvinout
díky	díky	k7c3	díky
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
,	,	kIx,	,
fyziologii	fyziologie	k1gFnSc6	fyziologie
<g/>
,	,	kIx,	,
evoluční	evoluční	k2eAgFnSc6d1	evoluční
biologii	biologie	k1gFnSc6	biologie
<g/>
,	,	kIx,	,
atomismu	atomismus	k1gInSc6	atomismus
<g/>
,	,	kIx,	,
kvantifikujícím	kvantifikující	k2eAgInSc6d1	kvantifikující
a	a	k8xC	a
laboratorním	laboratorní	k2eAgInPc3d1	laboratorní
přístupům	přístup	k1gInPc3	přístup
včetně	včetně	k7c2	včetně
přístrojových	přístrojový	k2eAgMnPc2d1	přístrojový
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
použitým	použitý	k2eAgFnPc3d1	použitá
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
vědeckých	vědecký	k2eAgInPc2d1	vědecký
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
vědecké	vědecký	k2eAgFnSc2d1	vědecká
psychologie	psychologie	k1gFnSc2	psychologie
byl	být	k5eAaImAgInS	být
podpořen	podpořit	k5eAaPmNgInS	podpořit
také	také	k9	také
kritickým	kritický	k2eAgInSc7d1	kritický
empirismem	empirismus	k1gInSc7	empirismus
<g/>
,	,	kIx,	,
asocianismem	asocianismus	k1gInSc7	asocianismus
a	a	k8xC	a
materialismem	materialismus	k1gInSc7	materialismus
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
vědecké	vědecký	k2eAgFnSc2d1	vědecká
psychologie	psychologie	k1gFnSc2	psychologie
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
datovány	datovat	k5eAaImNgInP	datovat
rokem	rok	k1gInSc7	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Wundt	Wundt	k1gMnSc1	Wundt
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
laboratoř	laboratoř	k1gFnSc4	laboratoř
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Wundt	Wundt	k1gMnSc1	Wundt
se	se	k3xPyFc4	se
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
míry	míra	k1gFnSc2	míra
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
introspekci	introspekce	k1gFnSc4	introspekce
(	(	kIx(	(
<g/>
pozorování	pozorování	k1gNnSc2	pozorování
a	a	k8xC	a
zaznamenávání	zaznamenávání	k1gNnSc2	zaznamenávání
povahy	povaha	k1gFnSc2	povaha
svého	svůj	k3xOyFgNnSc2	svůj
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
cítění	cítění	k1gNnSc2	cítění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
laboratoře	laboratoř	k1gFnSc2	laboratoř
měl	mít	k5eAaImAgInS	mít
takový	takový	k3xDgInSc1	takový
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
určovaly	určovat	k5eAaImAgFnP	určovat
experimentální	experimentální	k2eAgFnPc1d1	experimentální
disciplíny	disciplína	k1gFnPc1	disciplína
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
přírodovědné	přírodovědný	k2eAgNnSc1d1	Přírodovědné
<g/>
)	)	kIx)	)
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc1d1	vědní
a	a	k8xC	a
co	co	k9	co
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
<g/>
–	–	k?	–
<g/>
li	li	k8xS	li
se	se	k3xPyFc4	se
psychologie	psychologie	k1gFnSc1	psychologie
stát	stát	k5eAaImF	stát
uznanou	uznaný	k2eAgFnSc7d1	uznaná
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
i	i	k9	i
ona	onen	k3xDgNnPc4	onen
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
pracovat	pracovat	k5eAaImF	pracovat
experimentálně	experimentálně	k6eAd1	experimentálně
<g/>
,	,	kIx,	,
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
práce	práce	k1gFnSc1	práce
nabízela	nabízet	k5eAaImAgFnS	nabízet
záruku	záruka	k1gFnSc4	záruka
že	že	k8xS	že
objektivní	objektivní	k2eAgFnSc4d1	objektivní
metodou	metoda	k1gFnSc7	metoda
budou	být	k5eAaImBp3nP	být
získány	získán	k2eAgInPc1d1	získán
poznatky	poznatek	k1gInPc1	poznatek
<g/>
,	,	kIx,	,
spolehlivě	spolehlivě	k6eAd1	spolehlivě
ověřitelné	ověřitelný	k2eAgFnPc1d1	ověřitelná
a	a	k8xC	a
opakovatelné	opakovatelný	k2eAgFnPc1d1	opakovatelná
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
fyzice	fyzika	k1gFnSc3	fyzika
nebo	nebo	k8xC	nebo
filozofii	filozofie	k1gFnSc3	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Wundta	Wundt	k1gInSc2	Wundt
jsou	být	k5eAaImIp3nP	být
svými	svůj	k3xOyFgInPc7	svůj
výzkumy	výzkum	k1gInPc1	výzkum
důležití	důležitý	k2eAgMnPc1d1	důležitý
fyziolog	fyziolog	k1gMnSc1	fyziolog
Hering	Hering	k1gInSc1	Hering
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Ernst	Ernst	k1gMnSc1	Ernst
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
působící	působící	k2eAgInPc1d1	působící
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
studovali	studovat	k5eAaImAgMnP	studovat
počitky	počitek	k1gInPc4	počitek
a	a	k8xC	a
vjemy	vjem	k1gInPc4	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Külpe	Külpat	k5eAaPmIp3nS	Külpat
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
Würzburské	Würzburský	k2eAgFnSc2d1	Würzburská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
Ebbinghaus	Ebbinghaus	k1gInSc1	Ebbinghaus
paměť	paměť	k1gFnSc4	paměť
a	a	k8xC	a
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
evropská	evropský	k2eAgFnSc1d1	Evropská
psychologie	psychologie	k1gFnSc1	psychologie
byla	být	k5eAaImAgFnS	být
značnou	značný	k2eAgFnSc7d1	značná
měrou	míra	k1gFnSc7wR	míra
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgFnSc1d1	experimentální
<g/>
,	,	kIx,	,
elementová	elementový	k2eAgFnSc1d1	elementová
<g/>
,	,	kIx,	,
asocianistická	asocianistický	k2eAgFnSc1d1	asocianistický
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc7d1	hlavní
metodou	metoda	k1gFnSc7	metoda
byla	být	k5eAaImAgFnS	být
introspekce	introspekce	k1gFnSc1	introspekce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jiné	jiný	k2eAgFnPc1d1	jiná
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
též	též	k9	též
užívaly	užívat	k5eAaImAgFnP	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
procesy	proces	k1gInPc4	proces
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
James	James	k1gMnSc1	James
přenášel	přenášet	k5eAaImAgMnS	přenášet
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
Američani	Američan	k1gMnPc1	Američan
<g/>
,	,	kIx,	,
evropskou	evropský	k2eAgFnSc4d1	Evropská
psychologii	psychologie	k1gFnSc3	psychologie
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
však	však	k9	však
dále	daleko	k6eAd2	daleko
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
spíše	spíše	k9	spíše
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
nutnost	nutnost	k1gFnSc4	nutnost
studia	studio	k1gNnSc2	studio
návyků	návyk	k1gInPc2	návyk
<g/>
,	,	kIx,	,
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
introspekce	introspekce	k1gFnSc1	introspekce
se	se	k3xPyFc4	se
neosvědčila	osvědčit	k5eNaPmAgFnS	osvědčit
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
rychlých	rychlý	k2eAgInPc2d1	rychlý
duševních	duševní	k2eAgInPc2d1	duševní
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
vývoji	vývoj	k1gInSc6	vývoj
psychologie	psychologie	k1gFnSc2	psychologie
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
směry	směr	k1gInPc4	směr
strukturalismus	strukturalismus	k1gInSc1	strukturalismus
(	(	kIx(	(
<g/>
rozkládání	rozkládání	k1gNnSc1	rozkládání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
analýza	analýza	k1gFnSc1	analýza
duševních	duševní	k2eAgFnPc2d1	duševní
struktur	struktura	k1gFnPc2	struktura
<g/>
)	)	kIx)	)
a	a	k8xC	a
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
(	(	kIx(	(
<g/>
studium	studium	k1gNnSc1	studium
operací	operace	k1gFnPc2	operace
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
sloužících	sloužící	k2eAgInPc2d1	sloužící
adaptaci	adaptace	k1gFnSc4	adaptace
organismu	organismus	k1gInSc2	organismus
na	na	k7c4	na
okolní	okolní	k2eAgNnSc4d1	okolní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
fungování	fungování	k1gNnSc4	fungování
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
propagátorem	propagátor	k1gMnSc7	propagátor
strukturalistického	strukturalistický	k2eAgInSc2d1	strukturalistický
směru	směr	k1gInSc2	směr
byl	být	k5eAaImAgMnS	být
E.	E.	kA	E.
B.	B.	kA	B.
Titchener	Titchener	k1gMnSc1	Titchener
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
z	z	k7c2	z
Cornellovy	Cornellův	k2eAgFnSc2d1	Cornellova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Wundtův	Wundtův	k2eAgMnSc1d1	Wundtův
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
směr	směr	k1gInSc4	směr
psychologie	psychologie	k1gFnSc2	psychologie
zavedl	zavést	k5eAaPmAgMnS	zavést
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jiní	jiný	k2eAgMnPc1d1	jiný
psychologové	psycholog	k1gMnPc1	psycholog
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
analytickým	analytický	k2eAgInSc7d1	analytický
přístupem	přístup	k1gInSc7	přístup
strukturalismu	strukturalismus	k1gInSc2	strukturalismus
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Wiliam	Wiliam	k6eAd1	Wiliam
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
analýzu	analýza	k1gFnSc4	analýza
prvků	prvek	k1gInPc2	prvek
vědomí	vědomí	k1gNnSc2	vědomí
měl	mít	k5eAaImAgInS	mít
klást	klást	k5eAaImF	klást
menší	malý	k2eAgInSc1d2	menší
důraz	důraz	k1gInSc1	důraz
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
větší	veliký	k2eAgFnSc1d2	veliký
pozornost	pozornost	k1gFnSc1	pozornost
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
jeho	jeho	k3xOp3gFnSc1	jeho
proměnlivé	proměnlivý	k2eAgFnSc6d1	proměnlivá
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc6d1	osobní
podstatě	podstata	k1gFnSc6	podstata
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
přístup	přístup	k1gInSc1	přístup
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zejména	zejména	k9	zejména
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
rámec	rámec	k1gInSc1	rámec
psychologie	psychologie	k1gFnSc2	psychologie
o	o	k7c6	o
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
coby	coby	k?	coby
předmět	předmět	k1gInSc1	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
je	být	k5eAaImIp3nS	být
teorií	teorie	k1gFnSc7	teorie
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
metodou	metoda	k1gFnSc7	metoda
psychoterapie	psychoterapie	k1gFnSc2	psychoterapie
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
spíše	spíše	k9	spíše
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
lékařské	lékařský	k2eAgFnSc2d1	lékařská
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
částečně	částečně	k6eAd1	částečně
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
hypnotismu	hypnotismus	k1gInSc2	hypnotismus
a	a	k8xC	a
zakládala	zakládat	k5eAaImAgFnS	zakládat
se	se	k3xPyFc4	se
na	na	k7c6	na
klinických	klinický	k2eAgFnPc6d1	klinická
zkušenostech	zkušenost	k1gFnPc6	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
a	a	k8xC	a
spadají	spadat	k5eAaPmIp3nP	spadat
na	na	k7c4	na
přelom	přelom	k1gInSc4	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
znamenala	znamenat	k5eAaImAgFnS	znamenat
určitou	určitý	k2eAgFnSc4d1	určitá
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
definoval	definovat	k5eAaBmAgMnS	definovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vědu	věda	k1gFnSc4	věda
o	o	k7c4	o
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
"	"	kIx"	"
–	–	k?	–
psychologie	psychologie	k1gFnSc1	psychologie
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
nepracovala	pracovat	k5eNaImAgFnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
je	být	k5eAaImIp3nS	být
nevědomí	nevědomí	k1gNnSc1	nevědomí
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
postoje	postoj	k1gInPc1	postoj
<g/>
,	,	kIx,	,
podněty	podnět	k1gInPc1	podnět
<g/>
,	,	kIx,	,
přání	přání	k1gNnPc1	přání
<g/>
,	,	kIx,	,
motivy	motiv	k1gInPc1	motiv
a	a	k8xC	a
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
nejsme	být	k5eNaImIp1nP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
.	.	kIx.	.
</s>
<s>
Nevědomé	vědomý	k2eNgFnPc1d1	nevědomá
myšlenky	myšlenka	k1gFnPc1	myšlenka
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
mnoha	mnoho	k4c7	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
snech	sen	k1gInPc6	sen
<g/>
,	,	kIx,	,
přeřeknutích	přeřeknutí	k1gNnPc6	přeřeknutí
a	a	k8xC	a
chybných	chybný	k2eAgInPc6d1	chybný
úkonech	úkon	k1gInPc6	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
volných	volný	k2eAgFnPc2d1	volná
asociací	asociace	k1gFnPc2	asociace
je	být	k5eAaImIp3nS	být
pacient	pacient	k1gMnSc1	pacient
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
cokoli	cokoli	k3yInSc4	cokoli
mu	on	k3xPp3gMnSc3	on
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
mysl	mysl	k1gFnSc4	mysl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zvědomění	zvědomění	k1gNnSc3	zvědomění
nevědomých	vědomý	k2eNgNnPc2d1	nevědomé
přání	přání	k1gNnPc2	přání
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
si	se	k3xPyFc3	se
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
vědomi	vědom	k2eAgMnPc1d1	vědom
některých	některý	k3yIgInPc2	některý
důležitých	důležitý	k2eAgInPc2d1	důležitý
aspektů	aspekt	k1gInPc2	aspekt
svého	své	k1gNnSc2	své
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
zvědomění	zvědomění	k1gNnSc1	zvědomění
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
léčení	léčení	k1gNnSc3	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
chování	chování	k1gNnSc2	chování
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
nevědomé	vědomý	k2eNgInPc1d1	nevědomý
<g/>
.	.	kIx.	.
</s>
<s>
Nevědomím	nevědomí	k1gNnSc7	nevědomí
rozumí	rozumět	k5eAaImIp3nS	rozumět
Freud	Freud	k1gInSc1	Freud
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
strachy	strach	k1gInPc1	strach
a	a	k8xC	a
tužby	tužba	k1gFnPc1	tužba
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
si	se	k3xPyFc3	se
jedinec	jedinec	k1gMnSc1	jedinec
není	být	k5eNaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
které	který	k3yQgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
jeho	jeho	k3xOp3gNnSc4	jeho
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
je	být	k5eAaImIp3nS	být
agresivní	agresivní	k2eAgNnSc1d1	agresivní
chování	chování	k1gNnSc1	chování
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
vrozeném	vrozený	k2eAgInSc6d1	vrozený
instinktu	instinkt	k1gInSc6	instinkt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
navázala	navázat	k5eAaPmAgFnS	navázat
Neopsychoanalýza	Neopsychoanalýza	k1gFnSc1	Neopsychoanalýza
<g/>
,	,	kIx,	,
odmítající	odmítající	k2eAgInPc4d1	odmítající
instinktivní	instinktivní	k2eAgInPc4d1	instinktivní
a	a	k8xC	a
biologické	biologický	k2eAgInPc4d1	biologický
základy	základ	k1gInPc4	základ
klasické	klasický	k2eAgFnSc2d1	klasická
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
a	a	k8xC	a
hledající	hledající	k2eAgInSc1d1	hledající
zdroj	zdroj	k1gInSc1	zdroj
konfliktů	konflikt	k1gInPc2	konflikt
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
raných	raný	k2eAgInPc6d1	raný
dětských	dětský	k2eAgInPc6d1	dětský
zážitcích	zážitek	k1gInPc6	zážitek
i	i	k8xC	i
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
jedince	jedinko	k6eAd1	jedinko
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
dané	daný	k2eAgFnSc2d1	daná
kultury	kultura	k1gFnSc2	kultura
<g/>
;	;	kIx,	;
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
metodu	metoda	k1gFnSc4	metoda
volných	volný	k2eAgFnPc2d1	volná
asociací	asociace	k1gFnPc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Behaviorismus	behaviorismus	k1gInSc1	behaviorismus
<g/>
.	.	kIx.	.
</s>
<s>
Odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
anglického	anglický	k2eAgInSc2d1	anglický
behaviour	behavioura	k1gFnPc2	behavioura
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
chování	chování	k1gNnSc1	chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
strukturalismus	strukturalismus	k1gInSc4	strukturalismus
a	a	k8xC	a
funkcionalismus	funkcionalismus	k1gInSc4	funkcionalismus
byl	být	k5eAaImAgInS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
systematický	systematický	k2eAgInSc1d1	systematický
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
předmětu	předmět	k1gInSc3	předmět
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
psychologii	psychologie	k1gFnSc4	psychologie
chápali	chápat	k5eAaImAgMnP	chápat
jako	jako	k8xC	jako
vědu	věda	k1gFnSc4	věda
o	o	k7c4	o
vědomé	vědomý	k2eAgFnPc4d1	vědomá
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
školy	škola	k1gFnPc1	škola
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
novými	nový	k2eAgFnPc7d1	nová
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
psychologii	psychologie	k1gFnSc4	psychologie
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
získal	získat	k5eAaPmAgInS	získat
asi	asi	k9	asi
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
behaviorismus	behaviorismus	k1gInSc1	behaviorismus
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
John	John	k1gMnSc1	John
B.	B.	kA	B.
Watson	Watson	k1gMnSc1	Watson
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
psychologie	psychologie	k1gFnSc2	psychologie
je	být	k5eAaImIp3nS	být
vědomá	vědomý	k2eAgFnSc1d1	vědomá
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
,	,	kIx,	,
a	a	k8xC	a
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
ani	ani	k9	ani
s	s	k7c7	s
psychoanalýzou	psychoanalýza	k1gFnSc7	psychoanalýza
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
studium	studium	k1gNnSc1	studium
lidské	lidský	k2eAgFnSc2d1	lidská
mysli	mysl	k1gFnSc2	mysl
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychologie	psychologie	k1gFnSc1	psychologie
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
psychologie	psychologie	k1gFnSc1	psychologie
dětí	dítě	k1gFnPc2	dítě
jsou	být	k5eAaImIp3nP	být
samostatné	samostatný	k2eAgFnPc4d1	samostatná
vědecké	vědecký	k2eAgFnPc4d1	vědecká
disciplíny	disciplína	k1gFnPc4	disciplína
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
psychologii	psychologie	k1gFnSc4	psychologie
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
studiích	studio	k1gNnPc6	studio
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
o	o	k7c4	o
vědomí	vědomí	k1gNnSc4	vědomí
nehovořil	hovořit	k5eNaImAgMnS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
že	že	k8xS	že
chování	chování	k1gNnSc3	chování
je	být	k5eAaImIp3nS	být
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
a	a	k8xC	a
vědomí	vědomí	k1gNnSc1	vědomí
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
introspekce	introspekce	k1gFnSc1	introspekce
přestávala	přestávat	k5eAaImAgFnS	přestávat
stačit	stačit	k5eAaBmF	stačit
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
uchytil	uchytit	k5eAaPmAgMnS	uchytit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Watsona	Watson	k1gMnSc2	Watson
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
všechno	všechen	k3xTgNnSc4	všechen
chování	chování	k1gNnSc4	chování
výsledkem	výsledek	k1gInSc7	výsledek
podmiňování	podmiňování	k1gNnSc2	podmiňování
a	a	k8xC	a
prostředí	prostředí	k1gNnSc2	prostředí
formuje	formovat	k5eAaImIp3nS	formovat
chování	chování	k1gNnSc1	chování
jedince	jedinko	k6eAd1	jedinko
posilováním	posilování	k1gNnSc7	posilování
určitých	určitý	k2eAgInPc2d1	určitý
návyků	návyk	k1gInPc2	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Dáme	dát	k5eAaPmIp1nP	dát
<g/>
-li	i	k?	-li
např.	např.	kA	např.
dítěti	dítě	k1gNnSc6	dítě
sušenku	sušenka	k1gMnSc4	sušenka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přestalo	přestat	k5eAaPmAgNnS	přestat
plakat	plakat	k5eAaImF	plakat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
chování	chování	k1gNnSc4	chování
tak	tak	k6eAd1	tak
spíše	spíše	k9	spíše
posílíme	posílit	k5eAaPmIp1nP	posílit
–	–	k?	–
odměníme	odměnit	k5eAaPmIp1nP	odměnit
<g/>
.	.	kIx.	.
</s>
<s>
Odměněná	odměněný	k2eAgFnSc1d1	odměněná
reakce	reakce	k1gFnSc1	reakce
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejmenší	malý	k2eAgFnSc4d3	nejmenší
jednotku	jednotka	k1gFnSc4	jednotka
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgInPc4d2	složitější
vzorce	vzorec	k1gInPc4	vzorec
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Behavioristé	behaviorista	k1gMnPc1	behaviorista
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c6	o
psychologických	psychologický	k2eAgInPc6d1	psychologický
jevech	jev	k1gInPc6	jev
v	v	k7c6	v
pojmech	pojem	k1gInPc6	pojem
podnětu	podnět	k1gInSc2	podnět
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
–	–	k?	–
<g/>
R	R	kA	R
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Stimulus	stimulus	k1gInSc1	stimulus
<g/>
–	–	k?	–
<g/>
Response	response	k1gFnSc2	response
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
podnět	podnět	k1gInSc1	podnět
–	–	k?	–
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
nebere	brát	k5eNaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
duševní	duševní	k2eAgInPc4d1	duševní
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
o	o	k7c6	o
lidském	lidský	k2eAgNnSc6d1	lidské
vědomí	vědomí	k1gNnSc6	vědomí
někdy	někdy	k6eAd1	někdy
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
neproniknutelné	proniknutelný	k2eNgFnSc6d1	neproniknutelná
schránce	schránka	k1gFnSc6	schránka
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
black	black	k1gInSc1	black
box	box	k1gInSc1	box
<g/>
)	)	kIx)	)
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
pouze	pouze	k6eAd1	pouze
vstupy	vstup	k1gInPc7	vstup
a	a	k8xC	a
výstupy	výstup	k1gInPc7	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
předností	přednost	k1gFnSc7	přednost
behavioristické	behavioristický	k2eAgFnSc2d1	behavioristická
psychologie	psychologie	k1gFnSc2	psychologie
tak	tak	k9	tak
byl	být	k5eAaImAgInS	být
psychologický	psychologický	k2eAgInSc1d1	psychologický
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
jasně	jasně	k6eAd1	jasně
vyjádřitelné	vyjádřitelný	k2eAgInPc4d1	vyjádřitelný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kritiky	kritika	k1gFnSc2	kritika
radikálního	radikální	k2eAgInSc2d1	radikální
behaviorismu	behaviorismus	k1gInSc2	behaviorismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
neobehaviorismus	neobehaviorismus	k1gInSc1	neobehaviorismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
překonat	překonat	k5eAaPmF	překonat
mechanické	mechanický	k2eAgNnSc4d1	mechanické
chápání	chápání	k1gNnSc4	chápání
psychiky	psychika	k1gFnSc2	psychika
<g/>
.	.	kIx.	.
</s>
<s>
Neobehaviorismus	Neobehaviorismus	k1gInSc1	Neobehaviorismus
upravil	upravit	k5eAaPmAgInS	upravit
rovnici	rovnice	k1gFnSc4	rovnice
S-O-R	S-O-R	k1gFnPc2	S-O-R
(	(	kIx(	(
<g/>
stimulus-object-response	stimulusbjectesponse	k1gFnSc1	stimulus-object-response
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
intervenující	intervenující	k2eAgFnSc1d1	intervenující
proměnná	proměnná	k1gFnSc1	proměnná
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
lidské	lidský	k2eAgFnSc2d1	lidská
mysli	mysl	k1gFnSc2	mysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
dominantním	dominantní	k2eAgInSc7d1	dominantní
směrem	směr	k1gInSc7	směr
behaviorismus	behaviorismus	k1gInSc1	behaviorismus
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
například	například	k6eAd1	například
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byla	být	k5eAaImAgFnS	být
populární	populární	k2eAgFnSc1d1	populární
tvarová	tvarový	k2eAgFnSc1d1	tvarová
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
měla	mít	k5eAaImAgFnS	mít
hluboko	hluboko	k6eAd1	hluboko
i	i	k9	i
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
a	a	k8xC	a
stále	stále	k6eAd1	stále
existovalo	existovat	k5eAaImAgNnS	existovat
více	hodně	k6eAd2	hodně
dalších	další	k2eAgInPc2d1	další
psychologických	psychologický	k2eAgInPc2d1	psychologický
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
psychologii	psychologie	k1gFnSc4	psychologie
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
a	a	k8xC	a
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
staré	starý	k2eAgInPc1d1	starý
postupy	postup	k1gInPc1	postup
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
dostačující	dostačující	k2eAgFnPc1d1	dostačující
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
rozvojem	rozvoj	k1gInSc7	rozvoj
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
A.	A.	kA	A.
Simon	Simon	k1gMnSc1	Simon
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
na	na	k7c6	na
konci	konec	k1gInSc6	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
publikovali	publikovat	k5eAaBmAgMnP	publikovat
řadu	řada	k1gFnSc4	řada
prací	práce	k1gFnPc2	práce
uvádějících	uvádějící	k2eAgFnPc2d1	uvádějící
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
psychologické	psychologický	k2eAgInPc4d1	psychologický
jevy	jev	k1gInPc4	jev
simulovány	simulován	k2eAgInPc4d1	simulován
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přístupu	přístup	k1gInSc2	přístup
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
zpracování	zpracování	k1gNnSc6	zpracování
informací	informace	k1gFnPc2	informace
přehodnocena	přehodnocen	k2eAgFnSc1d1	přehodnocena
řada	řada	k1gFnSc1	řada
psychologických	psychologický	k2eAgInPc2d1	psychologický
konceptů	koncept	k1gInPc2	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Noam	Noam	k6eAd1	Noam
Chomsky	Chomsky	k1gFnSc1	Chomsky
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kniha	kniha	k1gFnSc1	kniha
Syntaktické	syntaktický	k2eAgFnSc2d1	syntaktická
struktury	struktura	k1gFnSc2	struktura
(	(	kIx(	(
<g/>
v	v	k7c6	v
ang	ang	k?	ang
<g/>
.	.	kIx.	.
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Syntactic	Syntactice	k1gFnPc2	Syntactice
Structures	Structuresa	k1gFnPc2	Structuresa
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
dala	dát	k5eAaPmAgFnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
základním	základní	k2eAgFnPc3d1	základní
analýzám	analýza	k1gFnPc3	analýza
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
psycholingvistiky	psycholingvistika	k1gFnSc2	psycholingvistika
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Lingvisté	lingvista	k1gMnPc1	lingvista
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
zabývat	zabývat	k5eAaImF	zabývat
mentálními	mentální	k2eAgFnPc7d1	mentální
strukturami	struktura	k1gFnPc7	struktura
potřebnými	potřebný	k2eAgFnPc7d1	potřebná
pro	pro	k7c4	pro
porozumění	porozumění	k1gNnSc4	porozumění
jazyku	jazyk	k1gMnSc3	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
produkci	produkce	k1gFnSc4	produkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
výrazně	výrazně	k6eAd1	výrazně
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
neuropsychologie	neuropsychologie	k1gFnPc4	neuropsychologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
těžiště	těžiště	k1gNnSc1	těžiště
zájmu	zájem	k1gInSc2	zájem
psychologů	psycholog	k1gMnPc2	psycholog
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
do	do	k7c2	do
stejného	stejný	k2eAgNnSc2d1	stejné
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
původně	původně	k6eAd1	původně
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
,	,	kIx,	,
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
duševních	duševní	k2eAgFnPc2d1	duševní
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
a	a	k8xC	a
lepšími	dobrý	k2eAgInPc7d2	lepší
nástroji	nástroj	k1gInPc7	nástroj
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
vznikalo	vznikat	k5eAaImAgNnS	vznikat
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
dílčích	dílčí	k2eAgInPc2d1	dílčí
psychologických	psychologický	k2eAgInPc2d1	psychologický
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
již	již	k6eAd1	již
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
tak	tak	k6eAd1	tak
ostrém	ostrý	k2eAgInSc6d1	ostrý
sporu	spor	k1gInSc6	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
už	už	k6eAd1	už
nejsou	být	k5eNaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
psychologické	psychologický	k2eAgInPc4d1	psychologický
přístupy	přístup	k1gInPc4	přístup
tolik	tolik	k6eAd1	tolik
vyhraněné	vyhraněný	k2eAgMnPc4d1	vyhraněný
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
historických	historický	k2eAgInPc2d1	historický
směrů	směr	k1gInPc2	směr
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
mezi	mezi	k7c4	mezi
psychology	psycholog	k1gMnPc4	psycholog
populární	populární	k2eAgFnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
modelování	modelování	k1gNnSc2	modelování
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
psychometrických	psychometrický	k2eAgInPc2d1	psychometrický
postupů	postup	k1gInPc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
stále	stále	k6eAd1	stále
populárnější	populární	k2eAgInPc4d2	populárnější
také	také	k9	také
postmoderní	postmoderní	k2eAgInPc4d1	postmoderní
psychologické	psychologický	k2eAgInPc4d1	psychologický
směry	směr	k1gInPc4	směr
jako	jako	k8xS	jako
například	například	k6eAd1	například
diskurzivní	diskurzivní	k2eAgFnSc1d1	diskurzivní
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
světa	svět	k1gInSc2	svět
jednotlivcem	jednotlivec	k1gMnSc7	jednotlivec
a	a	k8xC	a
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
budování	budování	k1gNnSc2	budování
individuální	individuální	k2eAgFnSc2d1	individuální
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kritická	kritický	k2eAgFnSc1d1	kritická
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
kritizující	kritizující	k2eAgFnSc4d1	kritizující
mainstreamovou	mainstreamový	k2eAgFnSc4d1	mainstreamová
psychologii	psychologie	k1gFnSc4	psychologie
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
jejího	její	k3xOp3gNnSc2	její
nahlížení	nahlížení	k1gNnSc2	nahlížení
na	na	k7c6	na
psychopatologii	psychopatologie	k1gFnSc6	psychopatologie
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
psychologie	psychologie	k1gFnSc2	psychologie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
psychologii	psychologie	k1gFnSc4	psychologie
na	na	k7c6	na
několika	několik	k4yIc6	několik
státních	státní	k2eAgFnPc6d1	státní
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc6d1	soukromá
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
po	po	k7c6	po
pětiletém	pětiletý	k2eAgNnSc6d1	pětileté
studiu	studio	k1gNnSc6	studio
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tříletém	tříletý	k2eAgInSc6d1	tříletý
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
a	a	k8xC	a
dvouletém	dvouletý	k2eAgInSc6d1	dvouletý
navazujícím	navazující	k2eAgInSc6d1	navazující
magisterském	magisterský	k2eAgInSc6d1	magisterský
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
pětiletém	pětiletý	k2eAgNnSc6d1	pětileté
magisterském	magisterský	k2eAgNnSc6d1	magisterské
studiu	studio	k1gNnSc6	studio
<g/>
)	)	kIx)	)
vykonávat	vykonávat	k5eAaImF	vykonávat
povolání	povolání	k1gNnSc4	povolání
psychologa	psycholog	k1gMnSc2	psycholog
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
postgraduální	postgraduální	k2eAgFnSc2d1	postgraduální
přípravy	příprava	k1gFnSc2	příprava
i	i	k9	i
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
klinického	klinický	k2eAgMnSc2d1	klinický
psychologa	psycholog	k1gMnSc2	psycholog
či	či	k8xC	či
psychoterapeuta	psychoterapeut	k1gMnSc2	psychoterapeut
<g/>
:	:	kIx,	:
Karlova	Karlův	k2eAgFnSc1d1	Karlova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
a	a	k8xC	a
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
–	–	k?	–
Fakulta	fakulta	k1gFnSc1	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Akreditace	akreditace	k1gFnSc1	akreditace
tradičního	tradiční	k2eAgNnSc2d1	tradiční
pětiletého	pětiletý	k2eAgNnSc2d1	pětileté
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
psychologie	psychologie	k1gFnSc2	psychologie
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
–	–	k?	–
akreditace	akreditace	k1gFnSc1	akreditace
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
jen	jen	k9	jen
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
a	a	k8xC	a
na	na	k7c4	na
dostudování	dostudování	k1gNnSc4	dostudování
stávajících	stávající	k2eAgMnPc2d1	stávající
studentů	student	k1gMnPc2	student
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Pražská	pražský	k2eAgFnSc1d1	Pražská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
psychosociálních	psychosociální	k2eAgNnPc2d1	psychosociální
studií	studio	k1gNnPc2	studio
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
udělilo	udělit	k5eAaPmAgNnS	udělit
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
licenci	licence	k1gFnSc4	licence
k	k	k7c3	k
pětiletému	pětiletý	k2eAgNnSc3d1	pětileté
studiu	studio	k1gNnSc3	studio
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
vyhovující	vyhovující	k2eAgMnSc1d1	vyhovující
českým	český	k2eAgFnPc3d1	Česká
normám	norma	k1gFnPc3	norma
University	universita	k1gFnSc2	universita
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
udělilo	udělit	k5eAaPmAgNnS	udělit
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
licenci	licence	k1gFnSc4	licence
k	k	k7c3	k
dvouletému	dvouletý	k2eAgNnSc3d1	dvouleté
návaznému	návazný	k2eAgNnSc3d1	návazné
magisterskému	magisterský	k2eAgNnSc3d1	magisterské
studiu	studio	k1gNnSc3	studio
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
vyhovující	vyhovující	k2eAgMnSc1d1	vyhovující
českým	český	k2eAgFnPc3d1	Česká
normám	norma	k1gFnPc3	norma
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
Absolvent	absolvent	k1gMnSc1	absolvent
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
v	v	k7c6	v
marketingu	marketing	k1gInSc6	marketing
<g/>
,	,	kIx,	,
personalistice	personalistika	k1gFnSc3	personalistika
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
vykonávat	vykonávat	k5eAaImF	vykonávat
povolání	povolání	k1gNnSc3	povolání
psychologa	psycholog	k1gMnSc2	psycholog
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
postgraduální	postgraduální	k2eAgFnSc2d1	postgraduální
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
klinického	klinický	k2eAgMnSc2d1	klinický
psychologa	psycholog	k1gMnSc2	psycholog
<g/>
:	:	kIx,	:
všechny	všechen	k3xTgFnPc4	všechen
výše	výše	k1gFnPc4	výše
uvedené	uvedený	k2eAgFnPc4d1	uvedená
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
–	–	k?	–
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
jednooborové	jednooborový	k2eAgNnSc1d1	jednooborové
studium	studium	k1gNnSc1	studium
od	od	k7c2	od
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
–	–	k?	–
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
–	–	k?	–
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
jednooborové	jednooborový	k2eAgNnSc1d1	jednooborové
i	i	k8xC	i
dvouoborové	dvouoborový	k2eAgNnSc1d1	dvouoborový
studium	studium	k1gNnSc1	studium
<g/>
)	)	kIx)	)
University	universita	k1gFnPc1	universita
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
program	program	k1gInSc1	program
psychologie	psychologie	k1gFnSc2	psychologie
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
s	s	k7c7	s
akreditací	akreditace	k1gFnPc2	akreditace
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
může	moct	k5eAaImIp3nS	moct
nalézt	nalézt	k5eAaBmF	nalézt
uplatnění	uplatnění	k1gNnSc4	uplatnění
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
personalistice	personalistika	k1gFnSc3	personalistika
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
opět	opět	k6eAd1	opět
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
vykonávat	vykonávat	k5eAaImF	vykonávat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
jen	jen	k9	jen
o	o	k7c4	o
zaměření	zaměření	k1gNnSc4	zaměření
na	na	k7c6	na
psychologii	psychologie	k1gFnSc6	psychologie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jiného	jiný	k2eAgNnSc2d1	jiné
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
psychologii	psychologie	k1gFnSc4	psychologie
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
studijní	studijní	k2eAgInSc4d1	studijní
obor	obor	k1gInSc4	obor
<g/>
:	:	kIx,	:
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
–	–	k?	–
psychologie	psychologie	k1gFnSc1	psychologie
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
psychologie	psychologie	k1gFnSc2	psychologie
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
–	–	k?	–
personální	personální	k2eAgInSc1d1	personální
a	a	k8xC	a
interkulturní	interkulturní	k2eAgInSc1d1	interkulturní
management	management	k1gInSc1	management
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
</s>
