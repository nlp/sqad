<s>
Psychologie	psychologie	k1gFnSc1	psychologie
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
sociální	sociální	k2eAgFnPc4d1	sociální
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
se	se	k3xPyFc4	se
však	však	k9	však
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
plynulém	plynulý	k2eAgNnSc6d1	plynulé
rozmezí	rozmezí	k1gNnSc6	rozmezí
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výzkum	výzkum	k1gInSc4	výzkum
z	z	k7c2	z
věd	věda	k1gFnPc2	věda
přírodních	přírodní	k2eAgFnPc2d1	přírodní
(	(	kIx(	(
<g/>
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
např.	např.	kA	např.
zákonů	zákon	k1gInPc2	zákon
lidského	lidský	k2eAgNnSc2d1	lidské
vnímání	vnímání	k1gNnSc2	vnímání
a	a	k8xC	a
percepce	percepce	k1gFnSc2	percepce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
i	i	k8xC	i
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
až	až	k9	až
po	po	k7c4	po
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
