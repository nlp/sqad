<s>
Světový	světový	k2eAgInSc1d1	světový
fond	fond	k1gInSc1	fond
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
World	World	k1gInSc1	World
Wide	Wide	k1gNnSc2	Wide
Fund	fund	k1gInSc1	fund
for	forum	k1gNnPc2	forum
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
WWF	WWF	kA	WWF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Světový	světový	k2eAgInSc1d1	světový
fond	fond	k1gInSc1	fond
divočiny	divočina	k1gFnSc2	divočina
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
World	World	k1gInSc1	World
Wildlife	Wildlif	k1gInSc5	Wildlif
Fund	fund	k1gInSc1	fund
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
podporující	podporující	k2eAgFnSc4d1	podporující
ochranu	ochrana	k1gFnSc4	ochrana
divoké	divoký	k2eAgFnSc2d1	divoká
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
má	mít	k5eAaImIp3nS	mít
ohroženou	ohrožený	k2eAgFnSc4d1	ohrožená
pandu	panda	k1gFnSc4	panda
velkou	velký	k2eAgFnSc4d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
budování	budování	k1gNnSc4	budování
takového	takový	k3xDgInSc2	takový
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
budou	být	k5eAaImBp3nP	být
lidé	člověk	k1gMnPc1	člověk
žít	žít	k5eAaImF	žít
v	v	k7c6	v
harmonii	harmonie	k1gFnSc6	harmonie
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
oficiálně	oficiálně	k6eAd1	oficiálně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Panda	panda	k1gFnSc1	panda
velká	velký	k2eAgFnSc1d1	velká
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
pro	pro	k7c4	pro
logo	logo	k1gNnSc4	logo
jako	jako	k8xC	jako
celosvětově	celosvětově	k6eAd1	celosvětově
známý	známý	k2eAgInSc4d1	známý
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
a	a	k8xC	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
tisk	tisk	k1gInSc4	tisk
černobílého	černobílý	k2eAgNnSc2d1	černobílé
loga	logo	k1gNnSc2	logo
byly	být	k5eAaImAgFnP	být
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
fondu	fond	k1gInSc2	fond
stál	stát	k5eAaImAgMnS	stát
Brit	Brit	k1gMnSc1	Brit
Peter	Peter	k1gMnSc1	Peter
Scott	Scott	k1gMnSc1	Scott
a	a	k8xC	a
britský	britský	k2eAgMnSc1d1	britský
biolog	biolog	k1gMnSc1	biolog
Julian	Julian	k1gMnSc1	Julian
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
po	po	k7c6	po
Africe	Afrika	k1gFnSc6	Afrika
svědkem	svědek	k1gMnSc7	svědek
mizení	mizení	k1gNnSc2	mizení
přírodních	přírodní	k2eAgInPc2d1	přírodní
druhů	druh	k1gInPc2	druh
i	i	k8xC	i
stanovišť	stanoviště	k1gNnPc2	stanoviště
příhodných	příhodný	k2eAgNnPc2d1	příhodné
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
prvotní	prvotní	k2eAgInSc4d1	prvotní
rozvoj	rozvoj	k1gInSc4	rozvoj
fondu	fond	k1gInSc2	fond
hrál	hrát	k5eAaImAgMnS	hrát
holandský	holandský	k2eAgMnSc1d1	holandský
princ	princ	k1gMnSc1	princ
Bernhard	Bernhard	k1gMnSc1	Bernhard
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
fondu	fond	k1gInSc2	fond
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
významné	významný	k2eAgInPc4d1	významný
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
realizaci	realizace	k1gFnSc6	realizace
projektů	projekt	k1gInPc2	projekt
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
vládami	vláda	k1gFnPc7	vláda
i	i	k8xC	i
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
podniky	podnik	k1gInPc7	podnik
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
prvním	první	k4xOgFnPc3	první
úspěchům	úspěch	k1gInPc3	úspěch
patřilo	patřit	k5eAaImAgNnS	patřit
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Doñ	Doñ	k1gFnSc2	Doñ
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
se	se	k3xPyFc4	se
také	také	k9	také
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
Galapág	Galapágy	k1gFnPc2	Galapágy
<g/>
,	,	kIx,	,
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
zákazu	zákaz	k1gInSc3	zákaz
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
slonovinou	slonovina	k1gFnSc7	slonovina
a	a	k8xC	a
záchraně	záchrana	k1gFnSc3	záchrana
asijských	asijský	k2eAgMnPc2d1	asijský
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
zájmu	zájem	k1gInSc2	zájem
<g/>
:	:	kIx,	:
klimatické	klimatický	k2eAgFnPc1d1	klimatická
změny	změna	k1gFnPc1	změna
pralesy	prales	k1gInPc4	prales
moře	moře	k1gNnSc4	moře
živočišné	živočišný	k2eAgInPc4d1	živočišný
druhy	druh	k1gInPc4	druh
toxické	toxický	k2eAgFnSc2d1	toxická
chemikálie	chemikálie	k1gFnSc2	chemikálie
udržitelný	udržitelný	k2eAgInSc1d1	udržitelný
rozvoj	rozvoj	k1gInSc1	rozvoj
Má	mít	k5eAaImIp3nS	mít
pobočky	pobočka	k1gFnSc2	pobočka
ve	v	k7c6	v
42	[number]	k4	42
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pobočku	pobočka	k1gFnSc4	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Světový	světový	k2eAgInSc4d1	světový
fond	fond	k1gInSc4	fond
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
portál	portál	k1gInSc4	portál
</s>
