<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
také	také	k9
Republika	republika	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
zkratkou	zkratka	k1gFnSc7
JAR	Jara	k1gFnPc2
<g/>
,	,	kIx,
zkráceným	zkrácený	k2eAgInSc7d1
názvem	název	k1gInSc7
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nejjižnější	jižní	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>