<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
moderním	moderní	k2eAgInSc6d1
státu	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
búrské	búrský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Transvaal	Transvaal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Republic	Republice	k1gFnPc2
of	of	k?
South	South	k1gInSc1
AfricaRepubliek	AfricaRepubliek	k1gInSc1
van	van	k1gInSc1
Suid-Afrika	Suid-Afrik	k1gMnSc2
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaNkosi	HymnaNkose	k1gFnSc3
Sikelel	Sikelel	k1gMnSc1
iAfrica	iAfrica	k1gMnSc1
/	/	kIx~
Die	Die	k1gMnSc1
Stem	sto	k4xCgNnSc7
van	van	k1gInSc4
Suid	Suid	k1gInSc1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Motto	motto	k1gNnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
ke	k	k7c3
e	e	k0
<g/>
:	:	kIx,
ǀ	ǀ	k1gNnPc1
ǁ	ǁ	k1gFnPc2
(	(	kIx(
<g/>
Jednota	jednota	k1gFnSc1
v	v	k7c6
různosti	různost	k1gFnSc6
<g/>
)	)	kIx)
Geografie	geografie	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
(	(	kIx(
<g/>
výkonná	výkonný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
zákonodárná	zákonodárný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bloemfontein	Bloemfontein	k1gMnSc1
(	(	kIx(
<g/>
justice	justice	k1gFnSc1
<g/>
)	)	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
219	#num#	k4
912	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
2,5	2,5	k4
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Mafadi	Mafad	k1gMnPc1
(	(	kIx(
<g/>
3450	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+2	+2	k4
Poloha	poloha	k1gFnSc1
</s>
<s>
29	#num#	k4
<g/>
°	°	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
°	°	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
54	#num#	k4
841	#num#	k4
552	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
41	#num#	k4
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
170	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▲	▲	k?
0,709	0,709	k4
(	(	kIx(
<g/>
vysoký	vysoký	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
114	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
afrikánština	afrikánština	k1gFnSc1
<g/>
,	,	kIx,
ndebele	ndebel	k1gInPc1
<g/>
,	,	kIx,
setswanština	setswanština	k1gFnSc1
<g/>
,	,	kIx,
zuluština	zuluština	k1gFnSc1
<g/>
,	,	kIx,
xhoština	xhoština	k1gFnSc1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
sothština	sothština	k1gFnSc1
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
sothština	sothština	k1gFnSc1
<g/>
,	,	kIx,
tsonga	tsonga	k1gFnSc1
<g/>
,	,	kIx,
svazijština	svazijština	k1gFnSc1
<g/>
,	,	kIx,
vendština	vendština	k1gFnSc1
(	(	kIx(
<g/>
úřední	úřední	k2eAgNnSc1d1
<g/>
)	)	kIx)
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Křesťanství	křesťanství	k1gNnSc4
celkem	celkem	k6eAd1
68	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
43,7	43,7	k4
%	%	kIx~
<g/>
;	;	kIx,
(	(	kIx(
<g/>
Siónští	siónský	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
11,1	11,1	k4
%	%	kIx~
<g/>
,	,	kIx,
letniční	letniční	k2eAgFnSc1d1
8,2	8,2	k4
%	%	kIx~
<g/>
,	,	kIx,
katolíci	katolík	k1gMnPc1
7,1	7,1	k4
%	%	kIx~
<g/>
,	,	kIx,
metodisté	metodista	k1gMnPc1
6,8	6,8	k4
%	%	kIx~
<g/>
,	,	kIx,
holandská	holandský	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
6,7	6,7	k4
%	%	kIx~
<g/>
,	,	kIx,
anglikáni	anglikán	k1gMnPc1
3,8	3,8	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
1,5	1,5	k4
%	%	kIx~
<g/>
,	,	kIx,
hinduisté	hinduista	k1gMnPc1
1,4	1,4	k4
%	%	kIx~
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnSc1d1
17,2	17,2	k4
,	,	kIx,
<g/>
původní	původní	k2eAgNnPc4d1
kmenová	kmenový	k2eAgNnPc4d1
náboženství	náboženství	k1gNnPc4
28,5	28,5	k4
%	%	kIx~
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
parlamentní	parlamentní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
(	(	kIx(
<g/>
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Cyril	Cyril	k1gMnSc1
Ramaphosa	Ramaphosa	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zástupce	zástupce	k1gMnSc1
prezidenta	prezident	k1gMnSc2
</s>
<s>
funkce	funkce	k1gFnSc1
neobsazena	obsazen	k2eNgFnSc1d1
Měna	měna	k1gFnSc1
</s>
<s>
jihoafrický	jihoafrický	k2eAgInSc1d1
rand	rand	k1gInSc1
(	(	kIx(
<g/>
ZAR	ZAR	kA
<g/>
)	)	kIx)
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
209	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
USD	USD	kA
(	(	kIx(
<g/>
87	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
Giniho	Gini	k1gMnSc2
koeficient	koeficient	k1gInSc1
</s>
<s>
63,0	63,0	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
710	#num#	k4
ZAF	ZAF	kA
ZA	za	k7c4
MPZ	MPZ	kA
</s>
<s>
ZA	za	k7c4
Telefonní	telefonní	k2eAgMnPc4d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+27	+27	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
za	za	k7c4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
také	také	k9
Republika	republika	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
zkratkou	zkratka	k1gFnSc7
JAR	Jara	k1gFnPc2
<g/>
,	,	kIx,
zkráceným	zkrácený	k2eAgInSc7d1
názvem	název	k1gInSc7
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nejjižnější	jižní	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
59	#num#	k4
miliony	milion	k4xCgInPc7
obyvateli	obyvatel	k1gMnPc7
je	být	k5eAaImIp3nS
24	#num#	k4
<g/>
.	.	kIx.
nejlidnatější	lidnatý	k2eAgFnSc1d3
zemí	zem	k1gFnSc7
světa	svět	k1gInSc2
a	a	k8xC
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
1	#num#	k4
221	#num#	k4
037	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
(	(	kIx(
<g/>
471	#num#	k4
445	#num#	k4
čtverečních	čtvereční	k2eAgFnPc2d1
mil	míle	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
:	:	kIx,
Pretorii	Pretorie	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
výkonná	výkonný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
,	,	kIx,
Bloemfontein	Bloemfontein	k1gInSc1
(	(	kIx(
<g/>
justice	justice	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
moc	moc	k6eAd1
zákonodárná	zákonodárný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgNnSc7d3
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Johannesburg	Johannesburg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
80	#num#	k4
<g/>
%	%	kIx~
Jihoafričanů	Jihoafričan	k1gMnPc2
je	být	k5eAaImIp3nS
černoškého	černošký	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
rozdělených	rozdělená	k1gFnPc2
mezi	mezi	k7c4
různé	různý	k2eAgFnPc4d1
etnické	etnický	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
mluvící	mluvící	k2eAgFnPc4d1
různými	různý	k2eAgInPc7d1
africkými	africký	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgFnSc6d1
populaci	populace	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nP
největší	veliký	k2eAgFnPc1d3
africké	africký	k2eAgFnPc1d1
komunity	komunita	k1gFnPc1
evropských	evropský	k2eAgInPc2d1
<g/>
,	,	kIx,
asijských	asijský	k2eAgInPc2d1
a	a	k8xC
mnohonárodních	mnohonárodní	k2eAgInPc2d1
předků	předek	k1gInPc2
-	-	kIx~
je	být	k5eAaImIp3nS
zemí	zem	k1gFnSc7
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
podílem	podíl	k1gInSc7
bílého	bílý	k2eAgNnSc2d1
a	a	k8xC
asiatského	asiatský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jihu	jih	k1gInSc6
je	být	k5eAaImIp3nS
ohraničena	ohraničen	k2eAgFnSc1d1
2	#num#	k4
798	#num#	k4
kilometry	kilometr	k1gInPc7
pobřeží	pobřeží	k1gNnSc4
táhnoucí	táhnoucí	k2eAgNnSc4d1
se	se	k3xPyFc4
podél	podél	k7c2
jižního	jižní	k2eAgInSc2d1
Atlantiku	Atlantik	k1gInSc2
a	a	k8xC
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
její	její	k3xOp3gMnPc1
severní	severní	k2eAgMnPc1d1
sousedi	soused	k1gMnPc1
jsou	být	k5eAaImIp3nP
Namibie	Namibie	k1gFnSc1
<g/>
,	,	kIx,
Botswana	Botswana	k1gFnSc1
a	a	k8xC
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc1d1
a	a	k8xC
severovýchodní	severovýchodní	k2eAgMnSc1d1
pak	pak	k6eAd1
Mosambik	Mosambik	k1gInSc1
a	a	k8xC
Svazijsko	Svazijsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
jihoafrického	jihoafrický	k2eAgNnSc2d1
území	území	k1gNnSc2
leží	ležet	k5eAaImIp3nS
samostatné	samostatný	k2eAgNnSc1d1
království	království	k1gNnSc1
Lesotho	Lesot	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejjižnější	jižní	k2eAgFnSc1d3
země	země	k1gFnSc1
na	na	k7c6
pevnině	pevnina	k1gFnSc6
Starého	Starého	k2eAgInSc2d1
světa	svět	k1gInSc2
či	či	k8xC
východní	východní	k2eAgFnSc2d1
polokoule	polokoule	k1gFnSc2
a	a	k8xC
nejlidnatější	lidnatý	k2eAgFnSc2d3
země	zem	k1gFnSc2
ležící	ležící	k2eAgInSc4d1
zcela	zcela	k6eAd1
jižně	jižně	k6eAd1
od	od	k7c2
rovníku	rovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
je	být	k5eAaImIp3nS
multietnická	multietnický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
zahrnující	zahrnující	k2eAgFnSc1d1
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
jazyků	jazyk	k1gInPc2
a	a	k8xC
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
pluralitní	pluralitní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
v	v	k7c6
ústavě	ústava	k1gFnSc6
uznánými	uznáný	k2eAgInPc7d1
11	#num#	k4
úředními	úřední	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
je	být	k5eAaImIp3nS
čtvrté	čtvrtý	k4xOgNnSc1
nejvyšší	vysoký	k2eAgNnSc1d3
číslo	číslo	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
jsou	být	k5eAaImIp3nP
dvěma	dva	k4xCgFnPc7
nejvíce	nejvíce	k6eAd1,k6eAd3
používanými	používaný	k2eAgInPc7d1
prvními	první	k4xOgInPc7
jazyky	jazyk	k1gInPc7
zuluština	zuluština	k1gFnSc1
(	(	kIx(
<g/>
22,7	22,7	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
xhoština	xhoština	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
16,0	16,0	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Dva	dva	k4xCgMnPc1
další	další	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
evropského	evropský	k2eAgMnSc4d1
původu	původa	k1gMnSc4
<g/>
:	:	kIx,
afrikánština	afrikánština	k1gFnSc1
(	(	kIx(
<g/>
13,5	13,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
z	z	k7c2
nizozemštiny	nizozemština	k1gFnSc2
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
první	první	k4xOgInSc1
jazyk	jazyk	k1gInSc1
většiny	většina	k1gFnSc2
barevných	barevný	k2eAgMnPc2d1
a	a	k8xC
bílých	bílý	k2eAgMnPc2d1
Jihoafričanů	Jihoafričan	k1gMnPc2
a	a	k8xC
angličtina	angličtina	k1gFnSc1
(	(	kIx(
<g/>
9,6	9,6	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
zase	zase	k9
odráží	odrážet	k5eAaImIp3nS
dědictví	dědictví	k1gNnSc1
britského	britský	k2eAgInSc2d1
kolonialismu	kolonialismus	k1gInSc2
a	a	k8xC
běžně	běžně	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ve	v	k7c6
veřejném	veřejný	k2eAgInSc6d1
i	i	k8xC
komerčním	komerční	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mála	málo	k1gNnSc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nikdy	nikdy	k6eAd1
nezažila	zažít	k5eNaPmAgFnS
státní	státní	k2eAgInSc4d1
převrat	převrat	k1gInSc4
a	a	k8xC
její	její	k3xOp3gFnSc1
demokracie	demokracie	k1gFnSc1
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
stoletou	stoletý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
rasové	rasový	k2eAgFnSc2d1
segregace	segregace	k1gFnSc2
(	(	kIx(
<g/>
apartheidu	apartheid	k1gInSc2
<g/>
)	)	kIx)
zavedené	zavedený	k2eAgFnPc1d1
roku	rok	k1gInSc2
1948	#num#	k4
byli	být	k5eAaImAgMnP
z	z	k7c2
demokratického	demokratický	k2eAgInSc2d1
procesu	proces	k1gInSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
vyřazeni	vyřazen	k2eAgMnPc1d1
černoši	černoch	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
JAR	jar	k1gFnSc1
parlamentní	parlamentní	k2eAgFnSc1d1
demokracií	demokracie	k1gFnSc7
<g/>
,	,	kIx,
byť	byť	k8xS
důsledky	důsledek	k1gInPc4
apartheidu	apartheid	k1gInSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
rozdělené	rozdělený	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
a	a	k8xC
ekonomické	ekonomický	k2eAgFnSc2d1
nerovnosti	nerovnost	k1gFnSc2
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
přetrvávají	přetrvávat	k5eAaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
předmětem	předmět	k1gInSc7
politických	politický	k2eAgInPc2d1
a	a	k8xC
společenských	společenský	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
černá	černý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
snažila	snažit	k5eAaImAgFnS
domáhat	domáhat	k5eAaImF
se	se	k3xPyFc4
více	hodně	k6eAd2
práv	práv	k2eAgMnSc1d1
od	od	k7c2
dominantní	dominantní	k2eAgFnSc2d1
bílé	bílý	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
hrála	hrát	k5eAaImAgFnS
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
historii	historie	k1gFnSc6
a	a	k8xC
politice	politika	k1gFnSc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
zavedla	zavést	k5eAaPmAgFnS
apartheid	apartheid	k1gInSc4
a	a	k8xC
institucionalizovala	institucionalizovat	k5eAaImAgFnS
předchozí	předchozí	k2eAgFnSc4d1
rasovou	rasový	k2eAgFnSc4d1
segregaci	segregace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
a	a	k8xC
často	často	k6eAd1
násilném	násilný	k2eAgInSc6d1
boji	boj	k1gInSc6
Afrického	africký	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
(	(	kIx(
<g/>
ANC	ANC	kA
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgMnPc2d1
aktivistů	aktivista	k1gMnPc2
proti	proti	k7c3
apartheidu	apartheid	k1gInSc3
uvnitř	uvnitř	k6eAd1
i	i	k9
vně	vně	k7c2
země	zem	k1gFnSc2
začalo	začít	k5eAaPmAgNnS
od	od	k7c2
poloviny	polovina	k1gFnSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
rušení	rušení	k1gNnSc2
diskriminačních	diskriminační	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
mají	mít	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
etnické	etnický	k2eAgFnPc4d1
a	a	k8xC
jazykové	jazykový	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
politické	politický	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
liberální	liberální	k2eAgFnSc6d1
demokracii	demokracie	k1gFnSc6
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
parlamentní	parlamentní	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
a	a	k8xC
devět	devět	k4xCc4
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
označována	označovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
duhový	duhový	k2eAgInSc1d1
národ	národ	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
popisuje	popisovat	k5eAaImIp3nS
multikulturní	multikulturní	k2eAgFnSc4d1
rozmanitost	rozmanitost	k1gFnSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
apartheidu	apartheid	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
rozvojová	rozvojový	k2eAgFnSc1d1
země	země	k1gFnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
na	na	k7c4
113	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
indexu	index	k1gInSc6
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
sedmém	sedmý	k4xOgNnSc6
nejvyšším	vysoký	k2eAgMnSc7d3
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světová	světový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
ji	on	k3xPp3gFnSc4
klasifikovala	klasifikovat	k5eAaImAgFnS
jako	jako	k9
nově	nově	k6eAd1
industrializovanou	industrializovaný	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
s	s	k7c7
druhou	druhý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
ekonomikou	ekonomika	k1gFnSc7
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
33	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgInSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
má	mít	k5eAaImIp3nS
také	také	k9
nejvíce	nejvíce	k6eAd1,k6eAd3
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
má	mít	k5eAaImIp3nS
status	status	k1gInSc4
střední	střední	k2eAgFnSc2d1
mocnosti	mocnost	k1gFnSc2
<g/>
;	;	kIx,
udržuje	udržovat	k5eAaImIp3nS
si	se	k3xPyFc3
významný	významný	k2eAgInSc4d1
regionální	regionální	k2eAgInSc4d1
vliv	vliv	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
Společenství	společenství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
skupiny	skupina	k1gFnPc1
G	G	kA
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Kriminalita	kriminalita	k1gFnSc1
<g/>
,	,	kIx,
chudoba	chudoba	k1gFnSc1
a	a	k8xC
nerovnost	nerovnost	k1gFnSc1
však	však	k9
zůstávají	zůstávat	k5eAaImIp3nP
všudypřítomné	všudypřítomný	k2eAgFnPc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zhruba	zhruba	k6eAd1
čtvrtina	čtvrtina	k1gFnSc1
populace	populace	k1gFnSc2
je	být	k5eAaImIp3nS
nezaměstnaná	zaměstnaný	k2eNgFnSc1d1
a	a	k8xC
žije	žít	k5eAaImIp3nS
z	z	k7c2
méně	málo	k6eAd2
než	než	k8xS
1,25	1,25	k4
amerického	americký	k2eAgInSc2d1
dolaru	dolar	k1gInSc2
na	na	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
velkému	velký	k2eAgInSc3d1
počtu	počet	k1gInSc3
úředních	úřední	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
má	mít	k5eAaImIp3nS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
více	hodně	k6eAd2
oficiálních	oficiální	k2eAgInPc2d1
názvů	název	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Republic	Republice	k1gFnPc2
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Republiek	Republiek	k1gInSc1
van	van	k1gInSc1
Suid-Afrika	Suid-Afrika	k1gFnSc1
(	(	kIx(
<g/>
afrikánsky	afrikánsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
iRiphabliki	iRiphabliki	k6eAd1
yeSewula	yeSewout	k5eAaPmAgFnS
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
jižní	jižní	k2eAgInPc1d1
ndebele	ndebel	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
iRiphabliki	iRiphablike	k1gFnSc4
yomZantsi	yomZantse	k1gFnSc4
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
xhošsky	xhošsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
iRiphabhuliki	iRiphabhuliki	k6eAd1
yaseNingizimu	yaseNingizimat	k5eAaPmIp1nS
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
zulu	zulu	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rephaboliki	Rephaboliki	k1gNnSc1
ya	ya	k?
Afrika-Borwa	Afrika-Borw	k1gInSc2
(	(	kIx(
<g/>
sotho	sotze	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Repabliki	Repabliki	k6eAd1
ya	ya	k?
Afrika-Borwa	Afrika-Borwa	k1gFnSc1
(	(	kIx(
<g/>
jižní	jižní	k2eAgNnSc1d1
sotho	sotze	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rephaboliki	Rephaboliki	k1gNnSc1
ya	ya	k?
Aforika	Aforik	k1gMnSc2
Borwa	Borwus	k1gMnSc2
(	(	kIx(
<g/>
setswanštinsky	setswanštinsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
iRiphabhulikhi	iRiphabhulikhi	k6eAd1
yeNingizimu	yeNingizimat	k5eAaPmIp1nS
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
swati	swati	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Riphabuḽ	Riphabuḽ	k6eAd1
ya	ya	k?
Afurika	Afurika	k1gFnSc1
Tshipembe	Tshipemb	k1gInSc5
(	(	kIx(
<g/>
venda	vendo	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Riphabliki	Riphabliki	k6eAd1
ra	ra	k0
Afrika	Afrika	k1gFnSc1
Dzonga	Dzonga	k1gFnSc1
(	(	kIx(
<g/>
tsonga	tsonga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pravěká	pravěký	k2eAgFnSc1d1
archeologie	archeologie	k1gFnSc1
</s>
<s>
Migrace	migrace	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
formovaly	formovat	k5eAaImAgFnP
tzv.	tzv.	kA
moderní	moderní	k2eAgInSc1d1
Duhový	duhový	k2eAgInSc1d1
národ	národ	k1gInSc1
</s>
<s>
V	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jedny	jeden	k4xCgFnPc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
archeologických	archeologický	k2eAgFnPc2d1
a	a	k8xC
fosilních	fosilní	k2eAgNnPc2d1
nalezišť	naleziště	k1gNnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Archeologové	archeolog	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
rozsáhlé	rozsáhlý	k2eAgInPc4d1
fosilní	fosilní	k2eAgInPc4d1
pozůstatky	pozůstatek	k1gInPc4
v	v	k7c6
krasových	krasový	k2eAgFnPc6d1
jeskyních	jeskyně	k1gFnPc6
v	v	k7c6
provincii	provincie	k1gFnSc6
Gauteng	Gautenga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
označena	označen	k2eAgFnSc1d1
za	za	k7c4
„	„	k?
<g/>
kolébku	kolébka	k1gFnSc4
lidstva	lidstvo	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
archeologická	archeologický	k2eAgNnPc4d1
místa	místo	k1gNnPc4
patří	patřit	k5eAaImIp3nP
Sterkfontein	Sterkfontein	k1gMnSc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc4
z	z	k7c2
nejbohatších	bohatý	k2eAgNnPc2d3
míst	místo	k1gNnPc2
s	s	k7c7
fosiliemi	fosilie	k1gFnPc7
kmene	kmen	k1gInSc2
hominini	hominin	k2eAgMnPc1d1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgNnPc4d1
místa	místo	k1gNnPc4
patří	patřit	k5eAaImIp3nS
Swartkrans	Swartkrans	k1gInSc1
<g/>
,	,	kIx,
jeskyně	jeskyně	k1gFnSc1
Kromdraai	Kromdraai	k1gNnSc2
<g/>
,	,	kIx,
jeskyně	jeskyně	k1gFnSc2
Coopers	Coopersa	k1gFnPc2
a	a	k8xC
Malapa	Malapa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raymond	Raymond	k1gMnSc1
Dart	Dart	k1gMnSc1
identifikoval	identifikovat	k5eAaBmAgMnS
první	první	k4xOgFnSc4
fosilii	fosilie	k1gFnSc4
homininů	hominin	k1gInPc2
objevenou	objevený	k2eAgFnSc4d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
-	-	kIx~
Taungské	Taungský	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
(	(	kIx(
<g/>
nalezené	nalezený	k2eAgInPc1d1
poblíž	poblíž	k7c2
Taungu	Taung	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc1
homininů	hominin	k1gInPc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
lokalit	lokalita	k1gFnPc2
Makapansgat	Makapansgat	k2eAgMnSc1d1
v	v	k7c6
provincii	provincie	k1gFnSc6
Limpopo	Limpopa	k1gFnSc5
<g/>
,	,	kIx,
Cornelia	Cornelium	k1gNnPc1
a	a	k8xC
Florisbad	Florisbad	k1gInSc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
Border	Border	k1gInSc1
Cave	Cav	k1gFnSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
KwaZulu-Natal	KwaZulu-Natal	k1gMnSc1
<g/>
,	,	kIx,
jeskyně	jeskyně	k1gFnSc1
ústí	ústí	k1gNnSc2
řeky	řeka	k1gFnSc2
Klasies	Klasiesa	k1gFnPc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Východní	východní	k2eAgNnSc4d1
Kapsko	Kapsko	k1gNnSc4
a	a	k8xC
Pinnacle	Pinnacle	k1gNnSc4
Point	pointa	k1gFnPc2
<g/>
,	,	kIx,
Elandsfontein	Elandsfonteina	k1gFnPc2
a	a	k8xC
Die	Die	k1gFnSc4
Kelders	Keldersa	k1gFnPc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Západní	západní	k2eAgNnSc4d1
Kapsko	Kapsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgInPc1
nálezy	nález	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
existovaly	existovat	k5eAaImAgInP
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
hominidů	hominid	k1gMnPc2
zhruba	zhruba	k6eAd1
před	před	k7c7
třemi	tři	k4xCgInPc7
miliony	milion	k4xCgInPc7
lety	let	k1gInPc7
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
druhem	druh	k1gInSc7
Australopithecus	Australopithecus	k1gMnSc1
africanus	africanus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Následovaly	následovat	k5eAaImAgInP
druhy	druh	k1gInPc1
jako	jako	k8xC,k8xS
Australopithecus	Australopithecus	k1gMnSc1
sediba	sediba	k1gMnSc1
i	i	k8xC
Paranthropus	Paranthropus	k1gMnSc1
robustus	robustus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejstarším	starý	k2eAgMnPc3d3
zástupcům	zástupce	k1gMnPc3
lidí	člověk	k1gMnPc2
patří	patřit	k5eAaImIp3nS
nově	nově	k6eAd1
objevený	objevený	k2eAgInSc1d1
druh	druh	k1gInSc1
Homo	Homo	k6eAd1
naledi	naledit	k5eAaPmRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
JAR	jaro	k1gNnPc2
vyskytoval	vyskytovat	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
zručný	zručný	k2eAgMnSc1d1
<g/>
,	,	kIx,
člověk	člověk	k1gMnSc1
vzpřímený	vzpřímený	k2eAgMnSc1d1
i	i	k9
člověk	člověk	k1gMnSc1
moudrý	moudrý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
obývali	obývat	k5eAaImAgMnP
jižní	jižní	k2eAgFnSc4d1
Afriku	Afrika	k1gFnSc4
po	po	k7c4
dobu	doba	k1gFnSc4
nejméně	málo	k6eAd3
170	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Různí	různý	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
lokalizovali	lokalizovat	k5eAaBmAgMnP
oblázkové	oblázkový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Vaal	Vaala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bantuská	bantuský	k2eAgFnSc1d1
expazne	expaznout	k5eAaPmIp3nS
</s>
<s>
Kopec	kopec	k1gInSc1
Mapungubwe	Mapungubw	k1gFnSc2
<g/>
,	,	kIx,
místo	místo	k1gNnSc1
původního	původní	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
království	království	k1gNnSc2
Mapungubwe	Mapungubwe	k1gNnSc2
</s>
<s>
Osady	osada	k1gFnPc1
lidí	člověk	k1gMnPc2
hovořících	hovořící	k2eAgFnPc2d1
bantuskými	bantuský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
využívali	využívat	k5eAaPmAgMnP,k5eAaImAgMnP
v	v	k7c6
zemědělství	zemědělství	k1gNnSc1
a	a	k8xC
pastevectví	pastevectví	k1gNnSc1
železo	železo	k1gNnSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
přítomné	přítomný	k2eAgInPc1d1
již	již	k9
ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
jižně	jižně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Limpopo	Limpopa	k1gFnSc5
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
severní	severní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
s	s	k7c7
Botswanou	Botswana	k1gFnSc7
a	a	k8xC
Zimbabwe	Zimbabwe	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysídlili	vysídlit	k5eAaPmAgMnP
<g/>
,	,	kIx,
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
pohltili	pohltit	k5eAaPmAgMnP
původní	původní	k2eAgFnPc4d1
Khoisany	Khoisana	k1gFnPc4
<g/>
,	,	kIx,
národy	národ	k1gInPc4
Khoikhoiů	Khoikhoi	k1gMnPc2
a	a	k8xC
Sanů	San	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bantuové	Bantu	k1gMnPc1
se	se	k3xPyFc4
pomalu	pomalu	k6eAd1
přesunuli	přesunout	k5eAaPmAgMnP
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
nejstarší	starý	k2eAgNnPc1d3
místa	místo	k1gNnPc1
zpracování	zpracování	k1gNnSc2
železa	železo	k1gNnSc2
v	v	k7c6
současné	současný	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
KwaZulu-Natal	KwaZulu-Natal	k1gMnSc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
doby	doba	k1gFnSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1050	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejjižnější	jižní	k2eAgFnSc7d3
skupinou	skupina	k1gFnSc7
byli	být	k5eAaImAgMnP
Xhosové	Xhos	k1gMnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
jazyk	jazyk	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
určité	určitý	k2eAgInPc4d1
jazykové	jazykový	k2eAgInPc4d1
rysy	rys	k1gInPc4
dřívějších	dřívější	k2eAgInPc2d1
Khoisanů	Khoisan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xhosové	Xhosová	k1gFnSc6
dosáhly	dosáhnout	k5eAaPmAgInP
Great	Great	k2eAgInSc4d1
Fish	Fish	k1gInSc4
River	Rivra	k1gFnPc2
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Východní	východní	k2eAgNnSc4d1
Kapsko	Kapsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
migraci	migrace	k1gFnSc6
tyto	tento	k3xDgFnPc1
větší	veliký	k2eAgFnPc1d2
populace	populace	k1gFnPc1
doby	doba	k1gFnSc2
železné	železný	k2eAgFnPc1d1
vysídlily	vysídlit	k5eAaPmAgInP
nebo	nebo	k8xC
asimilovaly	asimilovat	k5eAaBmAgInP
dřívější	dřívější	k2eAgInPc1d1
národy	národ	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
provincii	provincie	k1gFnSc6
Mpumalanga	Mpumalang	k1gMnSc2
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
několik	několik	k4yIc4
kamenných	kamenný	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
spolu	spolu	k6eAd1
s	s	k7c7
kamenným	kamenný	k2eAgNnSc7d1
uspořádáním	uspořádání	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
pojmenovány	pojmenovat	k5eAaPmNgInP
Adamův	Adamův	k2eAgInSc1d1
kalendář	kalendář	k1gInSc4
<g/>
,	,	kIx,
ruiny	ruina	k1gFnPc1
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
stvořené	stvořený	k2eAgFnPc4d1
lidmi	člověk	k1gMnPc7
kultury	kultura	k1gFnSc2
Bokoni	Bokoň	k1gFnSc6
s	s	k7c7
jazykem	jazyk	k1gInSc7
severní	severní	k2eAgFnSc2d1
Sotho	Sot	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Portugalský	portugalský	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
</s>
<s>
Portugalský	portugalský	k2eAgMnSc1d1
průzkumník	průzkumník	k1gMnSc1
Bartolomeu	Bartolomeus	k1gInSc2
Dias	Diasa	k1gFnPc2
vysadil	vysadit	k5eAaPmAgMnS
kříž	kříž	k1gInSc4
v	v	k7c6
Cape	capat	k5eAaImIp3nS
Point	pointa	k1gFnPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
jako	jako	k9
první	první	k4xOgInSc4
úspěšně	úspěšně	k6eAd1
obeplul	obeplout	k5eAaPmAgMnS
mys	mys	k1gInSc4
Dobré	dobrý	k2eAgFnSc2d1
naděje	naděje	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
kontaktu	kontakt	k1gInSc2
s	s	k7c7
Evropou	Evropa	k1gFnSc7
byly	být	k5eAaImAgFnP
dominantní	dominantní	k2eAgFnSc7d1
etnickou	etnický	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
národy	národ	k1gInPc1
hovořící	hovořící	k2eAgInPc1d1
bantusky	bantusky	k6eAd1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
stěhovali	stěhovat	k5eAaImAgMnP
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
částí	část	k1gFnPc2
Afriky	Afrika	k1gFnSc2
asi	asi	k9
před	před	k7c7
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvěma	dva	k4xCgFnPc7
hlavními	hlavní	k2eAgFnPc7d1
historickými	historický	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
byli	být	k5eAaImAgMnP
Xhosové	Xhos	k1gMnPc1
a	a	k8xC
Zulové	Zul	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1487	#num#	k4
vedl	vést	k5eAaImAgMnS
portugalský	portugalský	k2eAgMnSc1d1
průzkumník	průzkumník	k1gMnSc1
Bartolomeu	Bartolome	k1gMnSc3
Dias	Dias	k1gInSc4
první	první	k4xOgFnSc4
evropskou	evropský	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
přistát	přistát	k5eAaPmF,k5eAaImF
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
přistál	přistát	k5eAaImAgMnS,k5eAaPmAgMnS
v	v	k7c6
zálivu	záliv	k1gInSc6
Walfisch	Walfischa	k1gFnPc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
známý	známý	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Walvis	Walvis	k1gInSc1
Bay	Bay	k1gFnSc2
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
Namibii	Namibie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
nejvzdálenějšího	vzdálený	k2eAgInSc2d3
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1485	#num#	k4
jeho	jeho	k3xOp3gNnSc1
předchůdce	předchůdce	k1gMnSc1
<g/>
,	,	kIx,
portugalský	portugalský	k2eAgMnSc1d1
navigátor	navigátor	k1gMnSc1
Diogo	Diogo	k1gMnSc1
Cã	Cã	k1gMnSc1
(	(	kIx(
<g/>
Cape	capat	k5eAaImIp3nS
Cross	Cross	k1gInSc4
<g/>
,	,	kIx,
severně	severně	k6eAd1
od	od	k7c2
zátoky	zátoka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dias	Dias	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
západním	západní	k2eAgNnSc7d1
pobřežím	pobřeží	k1gNnSc7
jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
8	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1488	#num#	k4
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
předcházely	předcházet	k5eAaImAgFnP
bouře	bouř	k1gFnPc1
bránící	bránící	k2eAgFnPc1d1
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
plavbě	plavba	k1gFnSc6
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
plul	plout	k5eAaImAgInS
z	z	k7c2
dohledu	dohled	k1gInSc2
pevniny	pevnina	k1gFnSc2
a	a	k8xC
obeplul	obeplout	k5eAaPmAgMnS
nejjižnější	jižní	k2eAgInSc4d3
bod	bod	k1gInSc4
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
jej	on	k3xPp3gMnSc4
viděl	vidět	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1488	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
nazýval	nazývat	k5eAaImAgInS
Rio	Rio	k1gFnSc4
do	do	k7c2
Infante	infant	k1gMnSc5
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
dnešní	dnešní	k2eAgMnSc1d1
Groot	Groot	k1gMnSc1
River	River	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
svém	svůj	k3xOyFgInSc6
návratu	návrat	k1gInSc6
uviděl	uvidět	k5eAaPmAgInS
mys	mys	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
pojmenoval	pojmenovat	k5eAaPmAgMnS
Cabo	Cabo	k1gMnSc1
das	das	k?
Tormentas	Tormentas	k1gMnSc1
(	(	kIx(
<g/>
mys	mys	k1gInSc1
bouří	bouř	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
král	král	k1gMnSc1
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c6
Cabo	Cabo	k6eAd1
da	da	k?
Boa	boa	k1gFnSc1
Esperança	Esperança	k1gFnSc1
neboli	neboli	k8xC
Mys	mys	k1gInSc1
Dobré	dobrý	k2eAgFnSc2d1
naděje	naděje	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
vedl	vést	k5eAaImAgInS
k	k	k7c3
bohatství	bohatství	k1gNnSc3
Východní	východní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Diasův	Diasův	k2eAgInSc1d1
výkon	výkon	k1gInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
zvěčněn	zvěčnit	k5eAaPmNgInS
v	v	k7c6
portugalské	portugalský	k2eAgFnSc6d1
epické	epický	k2eAgFnSc6d1
básni	báseň	k1gFnSc6
Lusovci	Lusovec	k1gMnPc1
(	(	kIx(
<g/>
1572	#num#	k4
<g/>
)	)	kIx)
od	od	k7c2
Luíse	Luíse	k1gFnSc2
de	de	k?
Camõ	Camõ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgMnPc2d1
asi	asi	k9
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
zde	zde	k6eAd1
přistávaly	přistávat	k5eAaImAgFnP
lodě	loď	k1gFnPc1
pouze	pouze	k6eAd1
z	z	k7c2
Portugalska	Portugalsko	k1gNnSc2
<g/>
;	;	kIx,
Portugalci	Portugalec	k1gMnPc1
zde	zde	k6eAd1
však	však	k9
žádné	žádný	k3yNgFnPc4
osady	osada	k1gFnPc4
po	po	k7c6
střetech	střet	k1gInPc6
s	s	k7c7
původními	původní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
nezakládali	zakládat	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Holandská	holandský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Kapská	kapský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
a	a	k8xC
Búrské	búrský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Malba	malba	k1gFnSc1
Charlese	Charles	k1gMnSc2
Davidsona	Davidson	k1gMnSc2
Bella	Bell	k1gMnSc2
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
van	vana	k1gFnPc2
Riebeeck	Riebeeck	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
založil	založit	k5eAaPmAgMnS
první	první	k4xOgFnSc4
evropskou	evropský	k2eAgFnSc4d1
osadu	osada	k1gFnSc4
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
přijíždí	přijíždět	k5eAaImIp3nS
do	do	k7c2
Table	tablo	k1gNnSc6
Bay	Bay	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1652	#num#	k4
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začala	začít	k5eAaPmAgFnS
námořní	námořní	k2eAgFnSc1d1
síla	síla	k1gFnSc1
Portugalska	Portugalsko	k1gNnSc2
upadat	upadat	k5eAaPmF,k5eAaImF
a	a	k8xC
angličtí	anglický	k2eAgMnPc1d1
a	a	k8xC
holandští	holandský	k2eAgMnPc1d1
obchodníci	obchodník	k1gMnPc1
soutěžili	soutěžit	k5eAaImAgMnP
o	o	k7c4
vyloučení	vyloučení	k1gNnSc4
Lisabonu	Lisabon	k1gInSc2
z	z	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
lukrativního	lukrativní	k2eAgInSc2d1
monopolu	monopol	k1gInSc2
na	na	k7c4
obchod	obchod	k1gInSc4
s	s	k7c7
kořením	koření	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Zástupci	zástupce	k1gMnPc1
britské	britský	k2eAgFnSc2d1
východoindické	východoindický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
měli	mít	k5eAaImAgMnP
sporadicky	sporadicky	k6eAd1
zájem	zájem	k1gInSc4
o	o	k7c4
mys	mys	k1gInSc4
při	při	k7c6
hledání	hledání	k1gNnSc6
opatření	opatření	k1gNnSc2
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
1601	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
začali	začít	k5eAaPmAgMnP
upřednostňovat	upřednostňovat	k5eAaImF
ostrov	ostrov	k1gInSc4
Ascension	Ascension	k1gInSc1
a	a	k8xC
Svatou	svatý	k2eAgFnSc4d1
Helenu	Helena	k1gFnSc4
jako	jako	k8xC,k8xS
alternativní	alternativní	k2eAgNnPc4d1
útočiště	útočiště	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Nizozemský	nizozemský	k2eAgInSc1d1
zájem	zájem	k1gInSc1
vzrostl	vzrůst	k5eAaPmAgInS
po	po	k7c6
roce	rok	k1gInSc6
1647	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c6
mysu	mys	k1gInSc6
na	na	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
ztroskotali	ztroskotat	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
zaměstnanci	zaměstnanec	k1gMnPc1
Nizozemské	nizozemský	k2eAgFnSc2d1
východoindické	východoindický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
VOC	VOC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořníci	námořník	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
přežít	přežít	k5eAaPmF
získáním	získání	k1gNnSc7
čerstvé	čerstvý	k2eAgFnSc2d1
vody	voda	k1gFnSc2
a	a	k8xC
masa	maso	k1gNnSc2
od	od	k7c2
domorodců	domorodec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
na	na	k7c4
úrodnou	úrodný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
zaseli	zasít	k5eAaPmAgMnP
zeleninu	zelenina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Holandska	Holandsko	k1gNnSc2
příznivě	příznivě	k6eAd1
informovali	informovat	k5eAaBmAgMnP
o	o	k7c6
potenciálu	potenciál	k1gInSc6
mysu	mys	k1gInSc2
jako	jako	k8xC,k8xS
o	o	k7c6
„	„	k?
<g/>
skladišti	skladiště	k1gNnSc6
a	a	k8xC
zahradě	zahrada	k1gFnSc6
<g/>
“	“	k?
k	k	k7c3
zásobování	zásobování	k1gNnSc3
lodí	loď	k1gFnPc2
plujících	plující	k2eAgFnPc2d1
na	na	k7c4
dlouhé	dlouhý	k2eAgFnPc4d1
plavby	plavba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1652	#num#	k4
<g/>
,	,	kIx,
asi	asi	k9
150	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
objevení	objevení	k1gNnSc6
cesty	cesta	k1gFnSc2
kolem	kolem	k7c2
mysu	mys	k1gInSc2
<g/>
,	,	kIx,
připluli	připlout	k5eAaPmAgMnP
do	do	k7c2
Stolové	stolový	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
tři	tři	k4xCgFnPc4
lodě	loď	k1gFnPc1
s	s	k7c7
90	#num#	k4
lidmi	člověk	k1gMnPc7
různé	různý	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
zkušeným	zkušený	k2eAgMnSc7d1
obchodníkem	obchodník	k1gMnSc7
a	a	k8xC
cestovatelem	cestovatel	k1gMnSc7
Janem	Jan	k1gMnSc7
van	van	k1gInSc4
Riebeeckem	Riebeeck	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
zde	zde	k6eAd1
jménem	jméno	k1gNnSc7
Nizozemské	nizozemský	k2eAgFnSc2d1
východoindické	východoindický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
vybudovali	vybudovat	k5eAaPmAgMnP
první	první	k4xOgFnSc4
osadu	osada	k1gFnSc4
a	a	k8xC
zásobovací	zásobovací	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
u	u	k7c2
mysu	mys	k1gInSc2
Dobré	dobrý	k2eAgFnSc2d1
naděje	naděje	k1gFnSc2
-	-	kIx~
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Vznikly	vzniknout	k5eAaPmAgInP
zde	zde	k6eAd1
zahrady	zahrada	k1gFnPc4
<g/>
,	,	kIx,
zemědělské	zemědělský	k2eAgFnPc4d1
plantáže	plantáž	k1gFnPc4
a	a	k8xC
vinice	vinice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časem	časem	k6eAd1
se	se	k3xPyFc4
mys	mys	k1gInSc1
stal	stát	k5eAaPmAgInS
domovem	domov	k1gInSc7
velké	velká	k1gFnSc2
populace	populace	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
vrijlieden	vrijlieden	k2eAgInSc1d1
<g/>
,	,	kIx,
známých	známý	k2eAgFnPc2d1
také	také	k9
jako	jako	k9
'	'	kIx"
<g/>
vrijburgers	vrijburgers	k6eAd1
(	(	kIx(
<g/>
doslovně	doslovně	k6eAd1
„	„	k?
<g/>
svobodní	svobodný	k2eAgMnPc1d1
občané	občan	k1gMnPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
bývalé	bývalý	k2eAgMnPc4d1
zaměstnance	zaměstnanec	k1gMnPc4
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
po	po	k7c6
plnění	plnění	k1gNnSc6
svých	svůj	k3xOyFgFnPc2
smluv	smlouva	k1gFnPc2
pobývali	pobývat	k5eAaImAgMnP
na	na	k7c6
holandských	holandský	k2eAgNnPc6d1
územích	území	k1gNnPc6
v	v	k7c6
zámoří	zámoří	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Nizozemští	nizozemský	k2eAgMnPc1d1
obchodníci	obchodník	k1gMnPc1
také	také	k9
na	na	k7c4
těžkou	těžký	k2eAgFnSc4d1
práci	práce	k1gFnSc4
přes	přes	k7c4
Východoindickou	východoindický	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
nechali	nechat	k5eAaPmAgMnP
přivážet	přivážet	k5eAaImF
otroky	otrok	k1gMnPc4
namísto	namísto	k7c2
místních	místní	k2eAgInPc2d1
Khoisanů	Khoisan	k1gInPc2
–	–	k?
skupin	skupina	k1gFnPc2
Khoikhoiů	Khoikhoi	k1gInPc2
a	a	k8xC
Sanů	San	k1gInPc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
označovaných	označovaný	k2eAgFnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
jako	jako	k8xC,k8xS
Hotentoti	Hotentot	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavně	hlavně	k9
z	z	k7c2
Indonésie	Indonésie	k1gFnSc2
<g/>
,	,	kIx,
Madagaskaru	Madagaskar	k1gInSc2
a	a	k8xC
částí	část	k1gFnPc2
východní	východní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgNnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
komunit	komunita	k1gFnPc2
smíšené	smíšený	k2eAgFnSc2d1
rasy	rasa	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
vznikly	vzniknout	k5eAaPmAgFnP
prostřednictvím	prostřednictvím	k7c2
svazků	svazek	k1gInPc2
mezi	mezi	k7c7
vrijburgery	vrijburger	k1gInPc7
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnPc7
otroky	otrok	k1gMnPc7
a	a	k8xC
různými	různý	k2eAgInPc7d1
domorodými	domorodý	k2eAgInPc7d1
národy	národ	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vývoji	vývoj	k1gInSc3
nové	nový	k2eAgFnSc2d1
etnické	etnický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Barevných	barevný	k2eAgMnPc2d1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
většina	většina	k1gFnSc1
přijala	přijmout	k5eAaPmAgFnS
nizozemský	nizozemský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
a	a	k8xC
křesťanskou	křesťanský	k2eAgFnSc4d1
víru	víra	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
pár	pár	k4xCyI
let	léto	k1gNnPc2
později	pozdě	k6eAd2
začali	začít	k5eAaPmAgMnP
zakládat	zakládat	k5eAaImF
v	v	k7c6
okolí	okolí	k1gNnSc6
další	další	k2eAgFnSc2d1
farmy	farma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Evropy	Evropa	k1gFnSc2
přijížděli	přijíždět	k5eAaImAgMnP
další	další	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
<g/>
,	,	kIx,
zejména	zejména	k9
Němci	Němec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1688	#num#	k4
sem	sem	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
200	#num#	k4
francouzských	francouzský	k2eAgMnPc2d1
Hugenotů	hugenot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
připlouvalo	připlouvat	k5eAaImAgNnS
k	k	k7c3
jihoafrickým	jihoafrický	k2eAgInPc3d1
břehům	břeh	k1gInPc3
přes	přes	k7c4
80	#num#	k4
lodí	loď	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
;	;	kIx,
také	také	k9
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
sem	sem	k6eAd1
byly	být	k5eAaImAgFnP
zavlečeny	zavlečen	k2eAgFnPc1d1
neštovice	neštovice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
způsobily	způsobit	k5eAaPmAgFnP
zdecimování	zdecimování	k1gNnPc1
nejen	nejen	k6eAd1
osadníků	osadník	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zejména	zejména	k9
domorodců	domorodec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expanze	expanze	k1gFnSc1
nizozemských	nizozemský	k2eAgMnPc2d1
kolonistů	kolonista	k1gMnPc2
na	na	k7c4
východ	východ	k1gInSc4
zahájila	zahájit	k5eAaPmAgFnS
sérii	série	k1gFnSc3
válek	válka	k1gFnPc2
s	s	k7c7
na	na	k7c4
jihozápad	jihozápad	k1gInSc4
migrujícím	migrující	k2eAgInSc7d1
kmenem	kmen	k1gInSc7
Xhosů	Xhos	k1gInPc2
<g/>
,	,	kIx,
známé	známý	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
války	válka	k1gFnSc2
s	s	k7c7
Xhosy	Xhos	k1gInPc7
<g/>
,	,	kIx,
protože	protože	k8xS
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
soutěžily	soutěžit	k5eAaImAgFnP
o	o	k7c4
pastviny	pastvina	k1gFnPc4
nezbytné	zbytný	k2eNgFnPc1d1,k2eAgFnPc1d1
k	k	k7c3
pastvě	pastva	k1gFnSc3
dobytka	dobytek	k1gInSc2
poblíž	poblíž	k6eAd1
Great	Great	k2eAgMnSc1d1
Fish	Fish	k1gMnSc1
River	River	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Vrijburgerové	Vrijburger	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
stali	stát	k5eAaPmAgMnP
nezávislými	závislý	k2eNgMnPc7d1
farmáři	farmář	k1gMnPc7
na	na	k7c4
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
známí	známý	k1gMnPc1
jako	jako	k8xC,k8xS
Búrové	Búr	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
někteří	některý	k3yIgMnPc1
si	se	k3xPyFc3
osvojili	osvojit	k5eAaPmAgMnP
polokočovný	polokočovný	k2eAgInSc4d1
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
byly	být	k5eAaImAgInP
označování	označování	k1gNnSc4
jako	jako	k9
trekboerové	trekboerové	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Búrové	Búr	k1gMnPc1
vytvářeli	vytvářet	k5eAaImAgMnP
volné	volný	k2eAgFnPc4d1
milice	milice	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
nazývali	nazývat	k5eAaImAgMnP
komanda	komando	k1gNnSc2
a	a	k8xC
vytvářeli	vytvářet	k5eAaImAgMnP
spojenectví	spojenectví	k1gNnSc4
se	s	k7c7
skupinami	skupina	k1gFnPc7
Khoisanů	Khoisan	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
odráželi	odrážet	k5eAaImAgMnP
nájezdy	nájezd	k1gInPc7
Xhosů	Xhos	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
zahájily	zahájit	k5eAaPmAgFnP
krvavé	krvavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
bezvýsledné	bezvýsledný	k2eAgInPc4d1
útoky	útok	k1gInPc4
a	a	k8xC
sporadické	sporadický	k2eAgNnSc4d1
násilí	násilí	k1gNnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
doprovázené	doprovázený	k2eAgFnPc1d1
krádežemi	krádež	k1gFnPc7
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zůstávalo	zůstávat	k5eAaImAgNnS
po	po	k7c4
několik	několik	k4yIc4
desetiletí	desetiletí	k1gNnPc2
běžné	běžný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Britská	britský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
a	a	k8xC
Velký	velký	k2eAgInSc1d1
trek	trek	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Invaze	invaze	k1gFnSc2
do	do	k7c2
Kapské	kapský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
<g/>
,	,	kIx,
Kapská	kapský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
<g/>
,	,	kIx,
Velký	velký	k2eAgInSc1d1
trek	trek	k1gInSc1
<g/>
,	,	kIx,
Britské	britský	k2eAgNnSc1d1
Bečuánsko	Bečuánsko	k1gNnSc1
a	a	k8xC
Kolonie	kolonie	k1gFnSc1
Natal	Natal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
útoku	útok	k1gInSc2
Zuluů	Zulu	k1gMnPc2
na	na	k7c4
búrský	búrský	k2eAgInSc4d1
tábor	tábor	k1gInSc4
v	v	k7c6
únoru	únor	k1gInSc6
1838	#num#	k4
</s>
<s>
Langlaagt	Langlaagt	k1gInSc1
<g/>
,	,	kIx,
farma	farma	k1gFnSc1
poblíž	poblíž	k7c2
Johannesburgu	Johannesburg	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
našlo	najít	k5eAaPmAgNnS
první	první	k4xOgNnSc1
zlato	zlato	k1gNnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1795	#num#	k4
až	až	k9
1803	#num#	k4
okupovala	okupovat	k5eAaBmAgFnS
Kapské	kapský	k2eAgNnSc4d1
Město	město	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránila	zabránit	k5eAaPmAgFnS
obsazení	obsazení	k1gNnSc4
První	první	k4xOgFnSc7
Francouzskou	francouzský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
napadla	napadnout	k5eAaPmAgFnS
Nizozemí	Nizozemí	k1gNnSc4
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Nizozemská	nizozemský	k2eAgFnSc1d1
Východoindická	východoindický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
bankrot	bankrot	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
krátký	krátký	k2eAgInSc4d1
návrat	návrat	k1gInSc4
k	k	k7c3
nizozemské	nizozemský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
Batávské	batávský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
byl	být	k5eAaImAgInS
mys	mys	k1gInSc1
znovu	znovu	k6eAd1
obsazen	obsadit	k5eAaPmNgInS
Brity	Brit	k1gMnPc7
v	v	k7c6
roce	rok	k1gInSc6
1806	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
skončení	skončení	k1gNnSc6
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
bylo	být	k5eAaImAgNnS
Kapské	kapský	k2eAgNnSc1d1
město	město	k1gNnSc1
formálně	formálně	k6eAd1
postoupeno	postoupit	k5eAaPmNgNnS
Velké	velký	k2eAgFnSc2d1
Británii	Británie	k1gFnSc3
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Britské	britský	k2eAgNnSc1d1
osidlování	osidlování	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
začalo	začít	k5eAaPmAgNnS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1818	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
příchodem	příchod	k1gInSc7
4000	#num#	k4
osadníků	osadník	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1820	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Noví	nový	k2eAgMnPc1d1
kolonisté	kolonista	k1gMnPc1
byli	být	k5eAaImAgMnP
vyzýváni	vyzývat	k5eAaImNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
usadili	usadit	k5eAaPmAgMnP
z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
kvůli	kvůli	k7c3
zvětšení	zvětšení	k1gNnSc3
počtu	počet	k1gInSc2
evropských	evropský	k2eAgFnPc2d1
pracovních	pracovní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
posílení	posílení	k1gNnSc4
příhraničních	příhraniční	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
proti	proti	k7c3
nájezdům	nájezd	k1gInPc3
Xhosů	Xhos	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prvních	první	k4xOgNnPc6
dvou	dva	k4xCgNnPc6
desetiletích	desetiletí	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
sjednotil	sjednotit	k5eAaPmAgMnS
zulské	zulský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
náčelník	náčelník	k1gMnSc1
Šaka	Šaka	k1gMnSc1
a	a	k8xC
začal	začít	k5eAaPmAgMnS
dobývat	dobývat	k5eAaImF
nové	nový	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
brutální	brutální	k2eAgFnSc6d1
výbojné	výbojný	k2eAgFnSc6d1
politice	politika	k1gFnSc6
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
1	#num#	k4
000	#num#	k4
000	#num#	k4
až	až	k9
2	#num#	k4
000	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
válečníci	válečník	k1gMnPc1
zpustošili	zpustošit	k5eAaPmAgMnP
vnitrozemskou	vnitrozemský	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vylidněna	vylidnit	k5eAaPmNgFnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
ovšem	ovšem	k9
usnadnili	usnadnit	k5eAaPmAgMnP
následnou	následný	k2eAgFnSc4d1
búrskou	búrský	k2eAgFnSc4d1
a	a	k8xC
britskou	britský	k2eAgFnSc4d1
kolonizaci	kolonizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
období	období	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
mfecane	mfecanout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odnož	odnož	k1gInSc4
Zuluů	Zulu	k1gMnPc2
<g/>
,	,	kIx,
národ	národ	k1gInSc4
Matabele	Matabel	k1gInSc2
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgInS
větší	veliký	k2eAgFnSc4d2
říši	říše	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
velké	velký	k2eAgFnPc4d1
části	část	k1gFnPc4
Vysokého	vysoký	k2eAgInSc2d1
Veldu	Veld	k1gInSc2
pod	pod	k7c7
králem	král	k1gMnSc7
Mzilikazim	Mzilikazima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
mnoho	mnoho	k4c1
holandských	holandský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
z	z	k7c2
Kapské	kapský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
podrobeni	podroben	k2eAgMnPc1d1
britské	britský	k2eAgFnSc3d1
kontrole	kontrola	k1gFnSc3
<g/>
,	,	kIx,
odešlo	odejít	k5eAaPmAgNnS
v	v	k7c6
řadě	řada	k1gFnSc6
skupin	skupina	k1gFnPc2
migrantů	migrant	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
známými	známý	k1gMnPc7
jako	jako	k8xC,k8xS
Voortrekkeři	Voortrekker	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
Hledač	hledač	k1gMnSc1
cesty	cesta	k1gFnSc2
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Průkopník	průkopník	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Británie	Británie	k1gFnSc1
zakázala	zakázat	k5eAaPmAgFnS
obchodování	obchodování	k1gNnSc4
s	s	k7c7
otroky	otrok	k1gMnPc7
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1833	#num#	k4
otroctví	otroctví	k1gNnPc2
zrušila	zrušit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Búrům	Búr	k1gMnPc3
otroci	otrok	k1gMnPc1
z	z	k7c2
plantáží	plantáž	k1gFnPc2
utíkali	utíkat	k5eAaImAgMnP
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
farmy	farma	k1gFnPc1
zanikaly	zanikat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Búrové	Búr	k1gMnPc1
migrovali	migrovat	k5eAaImAgMnP
do	do	k7c2
budoucích	budoucí	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Natal	Natal	k1gInSc1
<g/>
,	,	kIx,
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
a	a	k8xC
Transvaal	Transvaal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
založili	založit	k5eAaPmAgMnP
búrské	búrský	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
<g/>
:	:	kIx,
Transvaalskou	Transvaalský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
provincie	provincie	k1gFnPc4
Gauteng	Gautenga	k1gFnPc2
<g/>
,	,	kIx,
Limpopo	Limpopa	k1gFnSc5
<g/>
,	,	kIx,
Mpumalanga	Mpumalanga	k1gFnSc1
a	a	k8xC
Severozápad	severozápad	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
republiku	republika	k1gFnSc4
Natalia	Natalium	k1gNnSc2
(	(	kIx(
<g/>
KwaZulu-Natal	KwaZulu-Natal	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Oranžský	oranžský	k2eAgInSc1d1
svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
(	(	kIx(
<g/>
svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
docházelo	docházet	k5eAaImAgNnS
ke	k	k7c3
střetům	střet	k1gInPc3
s	s	k7c7
místním	místní	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objev	objev	k1gInSc1
diamantů	diamant	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
a	a	k8xC
zlata	zlato	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
odstartoval	odstartovat	k5eAaPmAgMnS
minerální	minerální	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
a	a	k8xC
zvýšil	zvýšit	k5eAaPmAgInS
ekonomický	ekonomický	k2eAgInSc4d1
růst	růst	k1gInSc4
a	a	k8xC
imigraci	imigrace	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zintenzivnilo	zintenzivnit	k5eAaPmAgNnS
britské	britský	k2eAgNnSc1d1
úsilí	úsilí	k1gNnSc1
o	o	k7c4
získání	získání	k1gNnSc4
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
domorodým	domorodý	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boj	boj	k1gInSc1
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
těchto	tento	k3xDgInPc2
důležitých	důležitý	k2eAgInPc2d1
ekonomických	ekonomický	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
byl	být	k5eAaImAgMnS
faktorem	faktor	k1gInSc7
ve	v	k7c6
vztazích	vztah	k1gInPc6
mezi	mezi	k7c7
Evropany	Evropan	k1gMnPc7
a	a	k8xC
domorodým	domorodý	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
a	a	k8xC
také	také	k9
mezi	mezi	k7c4
Búry	Búr	k1gMnPc4
a	a	k8xC
Brity	Brit	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1876	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Thomas	Thomas	k1gMnSc1
François	François	k1gFnPc2
Burgers	Burgers	k1gInSc4
z	z	k7c2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
tranvaalské	tranvaalský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
válku	válka	k1gFnSc4
načelníkovi	načelníkův	k2eAgMnPc1d1
Sekhukhunemu	Sekhukhunemo	k1gNnSc3
a	a	k8xC
kmeni	kmen	k1gInSc3
Pedi	Pedi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekhukhuneovi	Sekhukhuneus	k1gMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1876	#num#	k4
transvaalskou	transvaalský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
porazit	porazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
útok	útok	k1gInSc1
Lydenburského	Lydenburský	k2eAgInSc2d1
dobrovolnického	dobrovolnický	k2eAgInSc2d1
sboru	sbor	k1gInSc2
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
odražen	odražen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1877	#num#	k4
podepsaly	podepsat	k5eAaPmAgInP
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
v	v	k7c6
Botshabelo	Botshabela	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Neschopnost	neschopnost	k1gFnSc1
Búrů	Búr	k1gMnPc2
podmanit	podmanit	k5eAaPmF
si	se	k3xPyFc3
náčelníka	náčelník	k1gMnSc4
Sekhukhuneho	Sekhukhune	k1gMnSc4
a	a	k8xC
kmen	kmen	k1gInSc4
vedla	vést	k5eAaImAgFnS
k	k	k7c3
odchodu	odchod	k1gInSc3
Burgerse	Burgerse	k1gFnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Paula	Paul	k1gMnSc2
Krugera	Kruger	k1gMnSc2
a	a	k8xC
britské	britský	k2eAgFnSc3d1
anexi	anexe	k1gFnSc3
Jihoafrické	jihoafrický	k2eAgFnSc2d1
(	(	kIx(
<g/>
Transvaalské	Transvaalský	k2eAgFnSc2d1
<g/>
)	)	kIx)
republiky	republika	k1gFnSc2
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1877	#num#	k4
sirem	sir	k1gMnSc7
Theophilem	Theophil	k1gMnSc7
Shepstoneem	Shepstoneus	k1gMnSc7
<g/>
,	,	kIx,
tajemníkem	tajemník	k1gMnSc7
pro	pro	k7c4
domácí	domácí	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Natalu	Natal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1878	#num#	k4
a	a	k8xC
1879	#num#	k4
byly	být	k5eAaImAgFnP
úspěšně	úspěšně	k6eAd1
odraženy	odrazit	k5eAaPmNgInP
tři	tři	k4xCgInPc1
britské	britský	k2eAgInPc1d1
útoky	útok	k1gInPc1
<g/>
,	,	kIx,
dokud	dokud	k8xS
Sir	sir	k1gMnSc1
Garnet	Garnet	k1gMnSc1
Wolseley	Wolselea	k1gFnSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
1879	#num#	k4
neporazil	porazit	k5eNaPmAgMnS
Sekhukhuneho	Sekhukhune	k1gMnSc4
s	s	k7c7
armádou	armáda	k1gFnSc7
2	#num#	k4
000	#num#	k4
britských	britský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
Búrů	Búr	k1gMnPc2
a	a	k8xC
10	#num#	k4
000	#num#	k4
Swazijců	Swazijce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Britsko-zulská	britsko-zulský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1879	#num#	k4
mezi	mezi	k7c7
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
a	a	k8xC
Zululandem	Zululando	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
zavedení	zavedení	k1gNnSc6
federace	federace	k1gFnSc2
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
lorda	lord	k1gMnSc2
Carnarvonem	Carnarvon	k1gInSc7
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
podobné	podobný	k2eAgNnSc1d1
politické	politický	k2eAgNnSc1d1
úsilí	úsilí	k1gNnSc1
spojené	spojený	k2eAgNnSc1d1
s	s	k7c7
vojenskými	vojenský	k2eAgInPc7d1
taženími	tažení	k1gNnPc7
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
uspět	uspět	k5eAaPmF
u	u	k7c2
afrických	africký	k2eAgNnPc2d1
království	království	k1gNnPc2
<g/>
,	,	kIx,
kmenových	kmenový	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
a	a	k8xC
búrských	búrský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
byl	být	k5eAaImAgMnS
sir	sir	k1gMnSc1
Henry	Henry	k1gMnSc1
Bartle	Bartl	k1gMnSc5
Frere	Frer	k1gInSc5
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
jako	jako	k8xS,k8xC
britský	britský	k2eAgMnSc1d1
vysoký	vysoký	k2eAgMnSc1d1
komisař	komisař	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
takové	takový	k3xDgInPc4
plány	plán	k1gInPc4
uskutečnil	uskutečnit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
překážkami	překážka	k1gFnPc7
byla	být	k5eAaImAgFnS
přítomnost	přítomnost	k1gFnSc1
nezávislých	závislý	k2eNgInPc2d1
búrských	búrský	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
království	království	k1gNnSc2
Zululand	Zululanda	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národ	národ	k1gInSc1
Zulu	Zulu	k1gMnSc1
porazil	porazit	k5eAaPmAgMnS
Brity	Brit	k1gMnPc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Isandlwany	Isandlwana	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
válku	válka	k1gFnSc4
nakonec	nakonec	k6eAd1
prohráli	prohrát	k5eAaPmAgMnP
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
ukončení	ukončení	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
Zuluů	Zulu	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Búrské	búrský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
První	první	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
a	a	k8xC
Druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byla	být	k5eAaImAgFnS
vzpoura	vzpoura	k1gFnSc1
Búrů	Búr	k1gMnPc2
proti	proti	k7c3
britské	britský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
v	v	k7c6
Transvaalu	Transvaal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Búrské	búrský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
úspěšně	úspěšně	k6eAd1
odolávaly	odolávat	k5eAaImAgFnP
britským	britský	k2eAgInPc3d1
zásahům	zásah	k1gInPc3
během	během	k7c2
první	první	k4xOgFnSc2
búrské	búrský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1880	#num#	k4
<g/>
–	–	k?
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
taktiky	taktika	k1gFnSc2
partyzánské	partyzánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dobře	dobře	k6eAd1
odpovídala	odpovídat	k5eAaImAgFnS
místním	místní	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
a	a	k8xC
Britové	Brit	k1gMnPc1
byli	být	k5eAaImAgMnP
poraženi	porazit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátili	vrátit	k5eAaPmAgMnP
se	se	k3xPyFc4
s	s	k7c7
větším	veliký	k2eAgInSc7d2
počtem	počet	k1gInSc7
<g/>
,	,	kIx,
většími	veliký	k2eAgFnPc7d2
zkušenostmi	zkušenost	k1gFnPc7
a	a	k8xC
novou	nový	k2eAgFnSc7d1
strategií	strategie	k1gFnSc7
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
búrské	búrský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
–	–	k?
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sice	sice	k8xC
utrpěli	utrpět	k5eAaPmAgMnP
těžké	těžký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
kvůli	kvůli	k7c3
opotřebovávací	opotřebovávací	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
;	;	kIx,
nicméně	nicméně	k8xC
nakonec	nakonec	k6eAd1
byli	být	k5eAaImAgMnP
úspěšní	úspěšný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
britských	britský	k2eAgInPc6d1
koncentračních	koncentrační	k2eAgInPc6d1
táborech	tábor	k1gInPc6
byly	být	k5eAaImAgFnP
uvězněny	uvězněn	k2eAgFnPc1d1
desítky	desítka	k1gFnPc1
tisíc	tisíc	k4xCgInSc1
Búrů	Búr	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřelo	zemřít	k5eAaPmAgNnS
zde	zde	k6eAd1
přes	přes	k7c4
27	#num#	k4
000	#num#	k4
búrských	búrský	k2eAgFnPc2d1
žen	žena	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
anektovalo	anektovat	k5eAaBmAgNnS
nezávislé	závislý	k2eNgNnSc1d1
búrské	búrský	k2eAgNnSc1d1
republiky	republika	k1gFnSc2
Transvaal	Transvaal	k1gInSc1
a	a	k8xC
Oranžský	oranžský	k2eAgInSc1d1
svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1
</s>
<s>
V	v	k7c6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
protibritská	protibritský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
bílých	bílý	k2eAgMnPc2d1
Jihoafričanů	Jihoafričan	k1gMnPc2
zaměřovala	zaměřovat	k5eAaImAgFnS
na	na	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
holandských	holandský	k2eAgNnPc2d1
a	a	k8xC
britských	britský	k2eAgNnPc2d1
koloniálních	koloniální	k2eAgNnPc2d1
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
rasová	rasový	k2eAgFnSc1d1
segregace	segregace	k1gFnSc1
většinou	většinou	k6eAd1
neformální	formální	k2eNgFnSc1d1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
některá	některý	k3yIgFnSc1
legislativa	legislativa	k1gFnSc1
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
osídlení	osídlení	k1gNnSc2
a	a	k8xC
pohybu	pohyb	k1gInSc2
domorodců	domorodec	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
nativním	nativní	k2eAgNnSc6d1
umístění	umístění	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
a	a	k8xC
systému	systém	k1gInSc2
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osm	osm	k4xCc1
let	léto	k1gNnPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
druhé	druhý	k4xOgFnSc2
búrské	búrský	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
vyjednávání	vyjednávání	k1gNnSc2
byla	být	k5eAaImAgFnS
zákonem	zákon	k1gInSc7
britského	britský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
<g/>
)	)	kIx)
zaručena	zaručen	k2eAgFnSc1d1
nominální	nominální	k2eAgFnSc1d1
nezávislost	nezávislost	k1gFnSc1
a	a	k8xC
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
získala	získat	k5eAaPmAgFnS
autonomní	autonomní	k2eAgInSc4d1
statut	statut	k1gInSc4
dominia	dominion	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
provincie	provincie	k1gFnPc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnovaly	zahrnovat	k5eAaImAgInP
Kapsko	Kapsko	k1gNnSc4
<g/>
,	,	kIx,
Transvaal	Transvaal	k1gInSc4
a	a	k8xC
kolonii	kolonie	k1gFnSc4
Natal	Natal	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
Oranžský	oranžský	k2eAgInSc1d1
svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
půdě	půda	k1gFnSc6
domorodců	domorodec	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1913	#num#	k4
přísně	přísně	k6eAd1
omezil	omezit	k5eAaPmAgInS
vlastnictví	vlastnictví	k1gNnSc4
půdy	půda	k1gFnSc2
černochy	černoch	k1gMnPc4
<g/>
;	;	kIx,
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc6
domorodci	domorodec	k1gMnPc7
ovládali	ovládat	k5eAaImAgMnP
pouze	pouze	k6eAd1
sedm	sedm	k4xCc4
procent	procento	k1gNnPc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
půdy	půda	k1gFnSc2
vyhrazené	vyhrazený	k2eAgFnPc1d1
pro	pro	k7c4
domorodé	domorodý	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
nepatrně	patrně	k6eNd1,k6eAd1
zvýšeno	zvýšen	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Automaticky	automaticky	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
země	zem	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
války	válka	k1gFnSc2
obsadila	obsadit	k5eAaPmAgFnS
a	a	k8xC
okupovala	okupovat	k5eAaBmAgFnS
tehdejší	tehdejší	k2eAgFnSc4d1
Německou	německý	k2eAgFnSc4d1
jihozápadní	jihozápadní	k2eAgFnSc4d1
Afriku	Afrika	k1gFnSc4
–	–	k?
dnešní	dnešní	k2eAgFnSc4d1
Namibii	Namibie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
britskými	britský	k2eAgMnPc7d1
dominii	dominion	k1gNnPc7
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
poválečných	poválečný	k2eAgFnPc2d1
mírových	mírový	k2eAgFnPc2d1
konferencí	konference	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgMnPc6,k3yIgMnPc6,k3yRgMnPc6
získala	získat	k5eAaPmAgFnS
jihozápadní	jihozápadní	k2eAgFnSc4d1
Afriku	Afrika	k1gFnSc4
jako	jako	k8xS,k8xC
mandátní	mandátní	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
Westminsterský	Westminsterský	k2eAgInSc1d1
statut	statut	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
učinil	učinit	k5eAaImAgInS,k5eAaPmAgInS
z	z	k7c2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
unie	unie	k1gFnSc2
formálně	formálně	k6eAd1
nezávislý	závislý	k2eNgInSc1d1
stát	stát	k1gInSc1
v	v	k7c6
personální	personální	k2eAgFnSc6d1
unii	unie	k1gFnSc6
se	s	k7c7
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
a	a	k8xC
dalšími	další	k2eAgNnPc7d1
britskými	britský	k2eAgNnPc7d1
dominii	dominion	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statut	statut	k1gInSc1
zrušil	zrušit	k5eAaPmAgInS
poslední	poslední	k2eAgFnSc4d1
pravomoci	pravomoc	k1gFnPc4
parlamentu	parlament	k1gInSc2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
vydávat	vydávat	k5eAaPmF,k5eAaImF
právní	právní	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
o	o	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suverénní	suverénní	k2eAgInPc1d1
a	a	k8xC
nezávislý	závislý	k2eNgInSc1d1
statut	statut	k1gInSc1
země	zem	k1gFnSc2
byl	být	k5eAaImAgInS
o	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
dodatečně	dodatečně	k6eAd1
potvrzen	potvrdit	k5eAaPmNgInS
domácími	domácí	k2eAgInPc7d1
zákony	zákon	k1gInPc7
Status	status	k1gInSc1
of	of	k?
the	the	k?
Union	union	k1gInSc1
Act	Act	k1gFnSc1
1934	#num#	k4
a	a	k8xC
Royal	Royal	k1gMnSc1
Executive	Executiv	k1gInSc5
Functions	Functions	k1gInSc4
and	and	k?
Seals	Seals	k1gInSc1
Act	Act	k1gFnSc1
of	of	k?
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
rok	rok	k1gInSc4
se	se	k3xPyFc4
South	South	k1gInSc1
African	Africany	k1gInPc2
Party	party	k1gFnSc1
a	a	k8xC
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
spojily	spojit	k5eAaPmAgFnP
a	a	k8xC
vytvořily	vytvořit	k5eAaPmAgFnP
sjednocenou	sjednocený	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
usilující	usilující	k2eAgInSc4d1
o	o	k7c6
smíření	smíření	k1gNnSc6
mezi	mezi	k7c7
Afrikánci	Afrikánec	k1gMnPc7
a	a	k8xC
anglicky	anglicky	k6eAd1
mluvícími	mluvící	k2eAgMnPc7d1
bělochy	běloch	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
se	se	k3xPyFc4
strana	strana	k1gFnSc1
rozdělila	rozdělit	k5eAaPmAgFnS
kvůli	kvůli	k7c3
vstupu	vstup	k1gInSc3
země	zem	k1gFnSc2
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
společenství	společenství	k1gNnSc4
národů	národ	k1gInPc2
a	a	k8xC
spojenec	spojenec	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nesla	nést	k5eAaImAgFnS
s	s	k7c7
nelibostí	nelibost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc4
apartheidu	apartheid	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Apartheid	apartheid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Pro	pro	k7c4
použití	použití	k1gNnSc1
bílými	bílý	k2eAgFnPc7d1
osobami	osoba	k1gFnPc7
<g/>
"	"	kIx"
–	–	k?
značka	značka	k1gFnSc1
z	z	k7c2
doby	doba	k1gFnSc2
apartheid	apartheid	k1gInSc1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
a	a	k8xC
afrikánštině	afrikánština	k1gFnSc6
</s>
<s>
Významným	významný	k2eAgInSc7d1
přelomem	přelom	k1gInSc7
ve	v	k7c6
vývoji	vývoj	k1gInSc6
Jihoafrické	jihoafrický	k2eAgFnSc2d1
unie	unie	k1gFnSc2
byla	být	k5eAaImAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
industrializace	industrializace	k1gFnSc1
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
objevením	objevení	k1gNnSc7
ložisek	ložisko	k1gNnPc2
diamantů	diamant	k1gInPc2
v	v	k7c4
Kimberley	Kimberlea	k1gFnPc4
a	a	k8xC
zlata	zlato	k1gNnSc2
ve	v	k7c6
Witwatersrandu	Witwatersrando	k1gNnSc6
v	v	k7c6
Transvaalu	Transvaal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvoj	rozvoj	k1gInSc1
průmyslu	průmysl	k1gInSc2
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
umocněn	umocnit	k5eAaPmNgInS
v	v	k7c6
době	doba	k1gFnSc6
velké	velký	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
díky	díky	k7c3
vysokým	vysoký	k2eAgFnPc3d1
cenám	cena	k1gFnPc3
zlata	zlato	k1gNnSc2
a	a	k8xC
diamantů	diamant	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
trval	trvat	k5eAaImAgInS
nepřetržitě	přetržitě	k6eNd1
až	až	k9
do	do	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pozitivní	pozitivní	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
však	však	k9
byl	být	k5eAaImAgInS
v	v	k7c6
příkrém	příkrý	k2eAgInSc6d1
rozporu	rozpor	k1gInSc6
s	s	k7c7
trendy	trend	k1gInPc7
vývoje	vývoj	k1gInSc2
politického	politický	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
ve	v	k7c6
volbách	volba	k1gFnPc6
zvítězila	zvítězit	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posílila	posílit	k5eAaPmAgFnS
rasovou	rasový	k2eAgFnSc4d1
segregaci	segregace	k1gFnSc4
zahájenou	zahájený	k2eAgFnSc4d1
pod	pod	k7c7
holandskou	holandský	k2eAgFnSc7d1
a	a	k8xC
britskou	britský	k2eAgFnSc7d1
koloniální	koloniální	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanadský	kanadský	k2eAgInSc1d1
indiánský	indiánský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
posloužil	posloužit	k5eAaPmAgInS
jako	jako	k9
rámec	rámec	k1gInSc1
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nacionalistická	nacionalistický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
klasifikovala	klasifikovat	k5eAaImAgFnS
všechny	všechen	k3xTgInPc4
národy	národ	k1gInPc4
do	do	k7c2
tří	tři	k4xCgInPc2
ras	rasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
každou	každý	k3xTgFnSc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
vyvinula	vyvinout	k5eAaPmAgFnS
práva	práv	k2eAgFnSc1d1
a	a	k8xC
omezení	omezení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílá	bílý	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
(	(	kIx(
<g/>
méně	málo	k6eAd2
než	než	k8xS
20	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
ovládala	ovládat	k5eAaImAgFnS
větší	veliký	k2eAgFnSc4d2
černošskou	černošský	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legálně	legálně	k6eAd1
institucionalizovaná	institucionalizovaný	k2eAgFnSc1d1
segregace	segregace	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
apartheid	apartheid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
běloši	běloch	k1gMnPc1
měli	mít	k5eAaImAgMnP
nejvyšší	vysoký	k2eAgFnSc4d3
životní	životní	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
srovnatelnou	srovnatelný	k2eAgFnSc4d1
se	s	k7c7
západními	západní	k2eAgInPc7d1
národy	národ	k1gInPc7
prvního	první	k4xOgInSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
černá	černý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
zůstávala	zůstávat	k5eAaImAgFnS
znevýhodněna	znevýhodnit	k5eAaPmNgFnS
téměř	téměř	k6eAd1
všemi	všecek	k3xTgInPc7
standardy	standard	k1gInPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
příjmu	příjem	k1gInSc2
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
bydlení	bydlení	k1gNnSc2
a	a	k8xC
střední	střední	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charta	charta	k1gFnSc1
svobody	svoboda	k1gFnSc2
přijatá	přijatý	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
Kongresem	kongres	k1gInSc7
aliance	aliance	k1gFnSc2
požadovala	požadovat	k5eAaImAgFnS
nerasovou	rasový	k2eNgFnSc4d1
společnost	společnost	k1gFnSc4
a	a	k8xC
ukončení	ukončení	k1gNnSc4
diskriminace	diskriminace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Republika	republika	k1gFnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1961	#num#	k4
byla	být	k5eAaImAgFnS
země	zem	k1gFnPc4
po	po	k7c6
těsném	těsný	k2eAgNnSc6d1
referendu	referendum	k1gNnSc6
prohlášena	prohlášen	k2eAgFnSc1d1
republikou	republika	k1gFnSc7
a	a	k8xC
změnila	změnit	k5eAaPmAgFnS
název	název	k1gInSc4
na	na	k7c4
Jihoafrickou	jihoafrický	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britská	britský	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
Natal	Natal	k1gInSc1
hlasovala	hlasovat	k5eAaImAgFnS
převážně	převážně	k6eAd1
proti	proti	k7c3
tomuto	tento	k3xDgInSc3
návrhu	návrh	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
ztratila	ztratit	k5eAaPmAgFnS
titul	titul	k1gInSc4
královny	královna	k1gFnSc2
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
a	a	k8xC
prezidentem	prezident	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
poslední	poslední	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
Charles	Charles	k1gMnSc1
Robberts	Robbertsa	k1gFnPc2
Swart	Swarta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
ústupek	ústupek	k1gInSc1
Westminsterskému	Westminsterský	k2eAgInSc3d1
systému	systém	k1gInSc3
zůstal	zůstat	k5eAaPmAgInS
parlamentní	parlamentní	k2eAgInSc1d1
systém	systém	k1gInSc1
vlády	vláda	k1gFnSc2
zachován	zachovat	k5eAaPmNgInS
až	až	k9
do	do	k7c2
reformy	reforma	k1gFnSc2
ústavy	ústava	k1gFnSc2
P.	P.	kA
W.	W.	kA
Bothou	Botha	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zrušila	zrušit	k5eAaPmAgFnS
funkci	funkce	k1gFnSc4
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
a	a	k8xC
nastolila	nastolit	k5eAaPmAgFnS
téměř	téměř	k6eAd1
jedinečné	jedinečný	k2eAgNnSc4d1
„	„	k?
<g/>
silné	silný	k2eAgNnSc1d1
předsednictví	předsednictví	k1gNnSc1
<g/>
“	“	k?
odpovědné	odpovědný	k2eAgFnSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
tlakem	tlak	k1gInSc7
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Společenství	společenství	k1gNnSc2
národů	národ	k1gInPc2
se	se	k3xPyFc4
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
z	z	k7c2
organizace	organizace	k1gFnSc2
stáhla	stáhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
a	a	k8xC
znovu	znovu	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
připojila	připojit	k5eAaPmAgFnS
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
byly	být	k5eAaImAgInP
zřizovány	zřizován	k2eAgInPc1d1
takzvané	takzvaný	k2eAgInPc1d1
bantustany	bantustan	k1gInPc1
<g/>
,	,	kIx,
samosprávná	samosprávný	k2eAgNnPc1d1
území	území	k1gNnPc1
vyhrazená	vyhrazený	k2eAgNnPc1d1
pro	pro	k7c4
původní	původní	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
odpor	odpor	k1gInSc4
uvnitř	uvnitř	k7c2
i	i	k9
vně	vně	k6eAd1
země	země	k1gFnSc1
vláda	vláda	k1gFnSc1
uzákonila	uzákonit	k5eAaPmAgFnS
pokračování	pokračování	k1gNnSc4
apartheidu	apartheid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpečnostní	bezpečnostní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
zakročily	zakročit	k5eAaPmAgFnP
proti	proti	k7c3
vnitřnímu	vnitřní	k2eAgInSc3d1
disentu	disent	k1gInSc3
a	a	k8xC
násilí	násilí	k1gNnSc4
se	se	k3xPyFc4
rozšířilo	rozšířit	k5eAaPmAgNnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
partyzánský	partyzánský	k2eAgInSc4d1
až	až	k8xS
teroristický	teroristický	k2eAgInSc4d1
boj	boj	k1gInSc4
vedly	vést	k5eAaImAgFnP
organizace	organizace	k1gFnPc1
proti	proti	k7c3
apartheidu	apartheid	k1gInSc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Africký	africký	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
kongres	kongres	k1gInSc1
(	(	kIx(
<g/>
ANC	ANC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Azanská	Azanský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
AZAPO	AZAPO	kA
<g/>
)	)	kIx)
a	a	k8xC
Panafrikanský	Panafrikanský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
(	(	kIx(
<g/>
PAC	pac	k1gFnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
městské	městský	k2eAgFnSc2d1
sabotáže	sabotáž	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc4
tři	tři	k4xCgFnPc4
soupeřící	soupeřící	k2eAgFnPc4d1
hnutí	hnutí	k1gNnSc2
odporu	odpor	k1gInSc6
se	se	k3xPyFc4
také	také	k9
zapojily	zapojit	k5eAaPmAgFnP
do	do	k7c2
občasných	občasný	k2eAgInPc2d1
mezifrakčních	mezifrakční	k2eAgInPc2d1
střetů	střet	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
soupeřili	soupeřit	k5eAaImAgMnP
o	o	k7c4
domácí	domácí	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Apartheid	apartheid	k1gInSc1
byl	být	k5eAaImAgInS
stále	stále	k6eAd1
kontroverznější	kontroverzní	k2eAgMnSc1d2
a	a	k8xC
několik	několik	k4yIc4
zemí	zem	k1gFnPc2
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
rasové	rasový	k2eAgFnSc3d1
politice	politika	k1gFnSc3
začalo	začít	k5eAaPmAgNnS
bojkotovat	bojkotovat	k5eAaImF
obchod	obchod	k1gInSc4
s	s	k7c7
jihoafrickou	jihoafrický	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
opatření	opatření	k1gNnPc1
byla	být	k5eAaImAgNnP
později	pozdě	k6eAd2
rozšířena	rozšířit	k5eAaPmNgFnS
na	na	k7c4
mezinárodní	mezinárodní	k2eAgFnPc4d1
sankce	sankce	k1gFnPc4
a	a	k8xC
prodej	prodej	k1gInSc4
podílu	podíl	k1gInSc2
zahraničními	zahraniční	k2eAgMnPc7d1
investory	investor	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
zasedání	zasedání	k1gNnSc6
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
Olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
JAR	jar	k1gFnSc4
suspendovat	suspendovat	k5eAaPmF
a	a	k8xC
tuto	tento	k3xDgFnSc4
zemi	zem	k1gFnSc4
vyloučit	vyloučit	k5eAaPmF
z	z	k7c2
olympijského	olympijský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
zruší	zrušit	k5eAaPmIp3nS
rasovou	rasový	k2eAgFnSc4d1
segregaci	segregace	k1gFnSc4
barevných	barevný	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
až	až	k8xS
říjnu	říjen	k1gInSc6
1975	#num#	k4
se	se	k3xPyFc4
jednotky	jednotka	k1gFnPc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
zapojily	zapojit	k5eAaPmAgInP
do	do	k7c2
konečné	konečný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
Angolské	angolský	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
straně	strana	k1gFnSc6
organizace	organizace	k1gFnSc2
UNITA	unita	k1gMnSc1
Jonase	Jonas	k1gMnSc5
Savimbiho	Savimbiha	k1gMnSc5
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
zatlačena	zatlačit	k5eAaPmNgFnS
zpět	zpět	k6eAd1
vojáky	voják	k1gMnPc7
nasazenými	nasazený	k2eAgMnPc7d1
z	z	k7c2
Kuby	Kuba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paralelně	paralelně	k6eAd1
s	s	k7c7
tímto	tento	k3xDgInSc7
konfiktem	konfikt	k1gInSc7
probíhala	probíhat	k5eAaImAgFnS
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
Namibie	Namibie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
zahájila	zahájit	k5eAaPmAgFnS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
program	program	k1gInSc1
vývoje	vývoj	k1gInSc2
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgNnSc6d1
desetiletí	desetiletí	k1gNnSc6
vyrobila	vyrobit	k5eAaPmAgFnS
šest	šest	k4xCc4
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
apartheidu	apartheid	k1gInSc2
</s>
<s>
De	De	k?
Klerk	Klerk	k1gInSc1
a	a	k8xC
Mandela	Mandela	k1gFnSc1
v	v	k7c6
Davosu	Davos	k1gInSc6
<g/>
,	,	kIx,
1992	#num#	k4
</s>
<s>
Mahlabatininská	Mahlabatininský	k2eAgFnSc1d1
deklarace	deklarace	k1gFnSc1
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
Mangosuthu	Mangosuth	k1gMnSc3
Buthelezi	Butheleze	k1gFnSc4
a	a	k8xC
Harry	Harra	k1gFnSc2
Schwarz	Schwarz	k1gMnSc1
<g/>
,	,	kIx,
zakotvila	zakotvit	k5eAaPmAgFnS
zásady	zásada	k1gFnPc4
mírového	mírový	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
moci	moc	k1gFnSc2
a	a	k8xC
rovnosti	rovnost	k1gFnSc2
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c6
první	první	k4xOgFnSc6
z	z	k7c2
takových	takový	k3xDgFnPc2
dohod	dohoda	k1gFnPc2
mezi	mezi	k7c7
černými	černý	k1gMnPc7
a	a	k8xC
bílými	bílý	k2eAgMnPc7d1
politickými	politický	k2eAgMnPc7d1
vůdci	vůdce	k1gMnPc7
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
W.	W.	kA
De	De	k?
Klerk	Klerk	k1gInSc1
nakonec	nakonec	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
dvoustranné	dvoustranný	k2eAgFnPc4d1
diskuse	diskuse	k1gFnPc4
s	s	k7c7
Nelsonem	Nelson	k1gMnSc7
Mandelou	Mandelý	k2eAgFnSc4d1
o	o	k7c6
přechodu	přechod	k1gInSc6
politik	politika	k1gFnPc2
a	a	k8xC
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
učinila	učinit	k5eAaPmAgFnS,k5eAaImAgFnS
vláda	vláda	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
první	první	k4xOgInSc4
krok	krok	k1gInSc4
k	k	k7c3
odstranění	odstranění	k1gNnSc3
diskriminace	diskriminace	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
zrušila	zrušit	k5eAaPmAgFnS
zákaz	zákaz	k1gInSc4
ANC	ANC	kA
a	a	k8xC
dalších	další	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
27	#num#	k4
letech	léto	k1gNnPc6
odnětí	odnětí	k1gNnSc2
svobody	svoboda	k1gFnSc2
za	za	k7c4
sabotáž	sabotáž	k1gFnSc4
byl	být	k5eAaImAgMnS
Nelson	Nelson	k1gMnSc1
Mandela	Mandel	k1gMnSc4
propuštěn	propuštěn	k2eAgMnSc1d1
z	z	k7c2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následoval	následovat	k5eAaImAgInS
vyjednávací	vyjednávací	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
souhlasem	souhlas	k1gInSc7
bílých	bílý	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
v	v	k7c6
referendu	referendum	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
vláda	vláda	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
jednáních	jednání	k1gNnPc6
o	o	k7c6
ukončení	ukončení	k1gNnSc6
apartheidu	apartheid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
také	také	k9
zničila	zničit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
jaderný	jaderný	k2eAgInSc4d1
arzenál	arzenál	k1gInSc4
a	a	k8xC
přistoupila	přistoupit	k5eAaPmAgFnS
ke	k	k7c3
Smlouvě	smlouva	k1gFnSc3
o	o	k7c6
nešíření	nešíření	k1gNnSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
proběhly	proběhnout	k5eAaPmAgFnP
první	první	k4xOgFnPc1
univerzální	univerzální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
ANC	ANC	kA
vyhrál	vyhrát	k5eAaPmAgMnS
drtivou	drtivý	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
u	u	k7c2
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
připojila	připojit	k5eAaPmAgFnS
ke	k	k7c3
Společenství	společenství	k1gNnSc3
národů	národ	k1gInPc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
členem	člen	k1gInSc7
Jihoafrického	jihoafrický	k2eAgNnSc2d1
rozvojového	rozvojový	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
(	(	kIx(
<g/>
SADC	SADC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
po	po	k7c6
pádu	pád	k1gInSc6
apartheidu	apartheid	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
nezaměstnanost	nezaměstnanost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
mnoho	mnoho	k4c1
černochů	černoch	k1gMnPc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
úrovně	úroveň	k1gFnSc2
střední	střední	k2eAgFnSc2d1
i	i	k8xC
vyšší	vysoký	k2eAgFnSc2d2
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
celková	celkový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
nezaměstnanosti	nezaměstnanost	k1gFnSc2
černochů	černoch	k1gMnPc2
se	se	k3xPyFc4
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1994	#num#	k4
a	a	k8xC
2003	#num#	k4
se	se	k3xPyFc4
podle	podle	k7c2
oficiálních	oficiální	k2eAgMnPc2d1
ukazatelů	ukazatel	k1gMnPc2
zhoršila	zhoršit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
pomocí	pomocí	k7c2
rozšířených	rozšířený	k2eAgFnPc2d1
definic	definice	k1gFnPc2
významně	významně	k6eAd1
poklesla	poklesnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Chudoba	Chudoba	k1gMnSc1
mezi	mezi	k7c7
bílými	bílé	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
byla	být	k5eAaImAgNnP
dříve	dříve	k6eAd2
vzácná	vzácný	k2eAgNnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
zvýšila	zvýšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Současná	současný	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
snažila	snažit	k5eAaImAgFnS
dosáhnout	dosáhnout	k5eAaPmF
měnové	měnový	k2eAgFnSc3d1
a	a	k8xC
fiskální	fiskální	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zajistila	zajistit	k5eAaPmAgFnS
přerozdělení	přerozdělení	k1gNnSc4
bohatství	bohatství	k1gNnSc2
i	i	k9
hospodářský	hospodářský	k2eAgInSc4d1
růst	růst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
(	(	kIx(
<g/>
HDI	HDI	kA
<g/>
)	)	kIx)
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1995	#num#	k4
a	a	k8xC
2005	#num#	k4
klesl	klesnout	k5eAaPmAgInS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
do	do	k7c2
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
ustavičně	ustavičně	k6eAd1
rostl	růst	k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Vrcholu	vrchol	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
69	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
lze	lze	k6eAd1
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
připsat	připsat	k5eAaPmF
pandemii	pandemie	k1gFnSc4
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
způsobila	způsobit	k5eAaPmAgFnS
pokles	pokles	k1gInSc4
průměrné	průměrný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
z	z	k7c2
vysokého	vysoký	k2eAgInSc2d1
bodu	bod	k1gInSc2
62,25	62,25	k4
roku	rok	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
na	na	k7c4
minimum	minimum	k1gNnSc4
52,57	52,57	k4
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
selhání	selhání	k1gNnSc4
vlády	vláda	k1gFnSc2
podniknout	podniknout	k5eAaPmF
kroky	krok	k1gInPc4
v	v	k7c6
řešení	řešení	k1gNnSc6
pandemii	pandemie	k1gFnSc4
během	během	k7c2
jejího	její	k3xOp3gInSc2
začátku	začátek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
bylo	být	k5eAaImAgNnS
při	při	k7c6
nepokojích	nepokoj	k1gInPc6
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
lidí	člověk	k1gMnPc2
zabito	zabít	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Středisko	středisko	k1gNnSc4
pro	pro	k7c4
práva	právo	k1gNnPc4
na	na	k7c4
bydlení	bydlení	k1gNnSc4
a	a	k8xC
vystěhování	vystěhování	k1gNnSc1
odhadlo	odhadnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
domovů	domov	k1gInPc2
bylo	být	k5eAaImAgNnS
vyhnáno	vyhnat	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
cíle	cíl	k1gInPc4
patřili	patřit	k5eAaImAgMnP
hlavně	hlavně	k9
<g />
.	.	kIx.
</s>
<s hack="1">
legální	legální	k2eAgFnSc1d1
i	i	k8xC
nelegální	legální	k2eNgMnSc1d1
migranti	migrant	k1gMnPc1
a	a	k8xC
uprchlíci	uprchlík	k1gMnPc1
žádající	žádající	k2eAgMnPc1d1
o	o	k7c4
azyl	azyl	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
třetina	třetina	k1gFnSc1
obětí	oběť	k1gFnPc2
byli	být	k5eAaImAgMnP
občané	občan	k1gMnPc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průzkumu	průzkum	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
dospěl	dochvít	k5eAaPmAgInS
Jihoafrický	jihoafrický	k2eAgInSc1d1
migrační	migrační	k2eAgInSc1d1
projekt	projekt	k1gInSc1
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
Jihoafričané	Jihoafričan	k1gMnPc1
jsou	být	k5eAaImIp3nP
proti	proti	k7c3
přistěhovalectví	přistěhovalectví	k1gNnSc3
více	hodně	k6eAd2
než	než	k8xS
jakákoli	jakýkoli	k3yIgFnSc1
jiná	jiný	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Vysoký	vysoký	k2eAgMnSc1d1
komisař	komisař	k1gMnSc1
OSN	OSN	kA
pro	pro	k7c4
uprchlíky	uprchlík	k1gMnPc4
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
požádalo	požádat	k5eAaPmAgNnS
o	o	k7c4
azyl	azyl	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
000	#num#	k4
uprchlíků	uprchlík	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
rok	rok	k1gInSc4
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Tito	tento	k3xDgMnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
lidé	člověk	k1gMnPc1
pocházeli	pocházet	k5eAaImAgMnP
hlavně	hlavně	k9
ze	z	k7c2
Zimbabwe	Zimbabw	k1gFnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
mnozí	mnohý	k2eAgMnPc1d1
pocházejí	pocházet	k5eAaImIp3nP
také	také	k9
z	z	k7c2
Burundi	Burundi	k1gNnSc2
<g/>
,	,	kIx,
Konžské	konžský	k2eAgFnSc2d1
demokratické	demokratický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Rwandy	Rwanda	k1gFnSc2
<g/>
,	,	kIx,
Eritreje	Eritrea	k1gFnSc2
<g/>
,	,	kIx,
Etiopie	Etiopie	k1gFnSc2
a	a	k8xC
Somálska	Somálsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Soutěž	soutěž	k1gFnSc1
o	o	k7c4
zaměstnání	zaměstnání	k1gNnSc4
<g/>
,	,	kIx,
obchodní	obchodní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgFnPc4d1
služby	služba	k1gFnPc4
a	a	k8xC
bydlení	bydlení	k1gNnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
vedla	vést	k5eAaImAgFnS
k	k	k7c3
napětí	napětí	k1gNnSc3
mezi	mezi	k7c7
uprchlíky	uprchlík	k1gMnPc7
a	a	k8xC
hostitelskými	hostitelský	k2eAgFnPc7d1
komunitami	komunita	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
xenofobie	xenofobie	k1gFnSc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
problémem	problém	k1gInSc7
<g/>
,	,	kIx,
nedávné	dávný	k2eNgNnSc1d1
násilí	násilí	k1gNnSc1
nebylo	být	k5eNaImAgNnS
tak	tak	k6eAd1
rozšířené	rozšířený	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
původně	původně	k6eAd1
obávalo	obávat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
však	však	k9
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
stále	stále	k6eAd1
potýká	potýkat	k5eAaImIp3nS
s	s	k7c7
rasovými	rasový	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
jedním	jeden	k4xCgNnSc7
z	z	k7c2
navrhovaných	navrhovaný	k2eAgNnPc2d1
řešení	řešení	k1gNnPc2
bylo	být	k5eAaImAgNnS
přijetí	přijetí	k1gNnSc1
legislativy	legislativa	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
projednávaný	projednávaný	k2eAgInSc4d1
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
z	z	k7c2
nenávisti	nenávist	k1gFnSc2
a	a	k8xC
zákon	zákon	k1gInSc1
o	o	k7c6
nenávistných	nenávistný	k2eAgInPc6d1,k2eNgInPc6d1
projevech	projev	k1gInPc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
bude	být	k5eAaImBp3nS
prosazovat	prosazovat	k5eAaImF
zákaz	zákaz	k1gInSc1
jihoafrického	jihoafrický	k2eAgInSc2d1
rasismu	rasismus	k1gInSc2
a	a	k8xC
závazek	závazek	k1gInSc4
rovnosti	rovnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Geografie	geografie	k1gFnSc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
zobrazující	zobrazující	k2eAgMnSc1d1
hlavní	hlavní	k2eAgMnSc1d1
topografické	topografický	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
:	:	kIx,
centrální	centrální	k2eAgFnSc4d1
náhorní	náhorní	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
lemovanou	lemovaný	k2eAgFnSc4d1
Velkým	velký	k2eAgInSc7d1
srázem	sráz	k1gInSc7
a	a	k8xC
kapskými	kapský	k2eAgFnPc7d1
horami	hora	k1gFnPc7
v	v	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
rohu	roh	k1gInSc6
země	zem	k1gFnSc2
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1
zeměpisné	zeměpisný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlustá	tlustý	k2eAgFnSc1d1
čára	čára	k1gFnSc1
sleduje	sledovat	k5eAaImIp3nS
směr	směr	k1gInSc4
Velkého	velký	k2eAgInSc2d1
srázu	sráz	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ohraničuje	ohraničovat	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc4d1
náhorní	náhorní	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
této	tento	k3xDgFnSc2
linie	linie	k1gFnSc2
<g/>
,	,	kIx,
zbarvená	zbarvený	k2eAgFnSc1d1
červeně	červeně	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
Dračí	dračí	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sráz	sráz	k1gInSc1
stoupá	stoupat	k5eAaImIp3nS
do	do	k7c2
nejvyššího	vysoký	k2eAgInSc2d3
bodu	bod	k1gInSc2
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
000	#num#	k4
m	m	kA
(	(	kIx(
<g/>
9	#num#	k4
800	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
Dračí	dračí	k2eAgFnPc1d1
hory	hora	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
provincií	provincie	k1gFnSc7
KwaZulu-Natal	KwaZulu-Natal	k1gMnSc1
a	a	k8xC
státem	stát	k1gInSc7
Lesotho	Lesot	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
z	z	k7c2
regionů	region	k1gInPc2
uvedených	uvedený	k2eAgInPc2d1
na	na	k7c6
mapě	mapa	k1gFnSc6
nemá	mít	k5eNaImIp3nS
přesně	přesně	k6eAd1
vymezenou	vymezený	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
případů	případ	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sráz	sráz	k1gInSc1
nebo	nebo	k8xC
pohoří	pohoří	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
jasnou	jasný	k2eAgFnSc4d1
dělicí	dělicí	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
známější	známý	k2eAgFnPc1d2
oblasti	oblast	k1gFnPc1
jsou	být	k5eAaImIp3nP
zbarveny	zbarven	k2eAgFnPc1d1
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
jednoduše	jednoduše	k6eAd1
označeny	označit	k5eAaPmNgInP
svými	svůj	k3xOyFgNnPc7
jmény	jméno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Pohoří	pohoří	k1gNnSc1
Swartberg	Swartberg	k1gInSc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Západní	západní	k2eAgNnSc4d1
Kapsko	Kapsko	k1gNnSc4
</s>
<s>
Kvetoucí	kvetoucí	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
v	v	k7c6
Namaqualandu	Namaqualand	k1gInSc6
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
nejjižnějším	jižní	k2eAgInSc7d3
státem	stát	k1gInSc7
Afriky	Afrika	k1gFnSc2
s	s	k7c7
dlouhým	dlouhý	k2eAgNnSc7d1
pobřežím	pobřeží	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
500	#num#	k4
km	km	kA
podél	podél	k7c2
dvou	dva	k4xCgInPc2
oceánů	oceán	k1gInPc2
(	(	kIx(
<g/>
jižní	jižní	k2eAgInSc1d1
Atlantik	Atlantik	k1gInSc1
a	a	k8xC
indický	indický	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
demografické	demografický	k2eAgFnSc2d1
ročenky	ročenka	k1gFnSc2
OSN	OSN	kA
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
1	#num#	k4
219	#num#	k4
912	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
24	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
přibližně	přibližně	k6eAd1
stejná	stejný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
jako	jako	k8xC,k8xS
Kolumbie	Kolumbie	k1gFnSc1
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
třikrát	třikrát	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
čtyřikrát	čtyřikrát	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
pětkrát	pětkrát	k6eAd1
větší	veliký	k2eAgNnSc1d2
než	než	k8xS
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
15,5	15,5	k4
<g/>
×	×	k?
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Afriky	Afrika	k1gFnSc2
zabírá	zabírat	k5eAaImIp3nS
asi	asi	k9
4	#num#	k4
%	%	kIx~
rozlohy	rozloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mafadi	Mafad	k1gMnPc1
v	v	k7c6
Dračích	dračí	k2eAgFnPc6d1
horách	hora	k1gFnPc6
je	být	k5eAaImIp3nS
s	s	k7c7
výškou	výška	k1gFnSc7
3450	#num#	k4
m	m	kA
(	(	kIx(
<g/>
11	#num#	k4
320	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
ostrovů	ostrov	k1gInPc2
Prince	princ	k1gMnSc2
Edwarda	Edward	k1gMnSc2
leží	ležet	k5eAaImIp3nP
země	zem	k1gFnPc1
mezi	mezi	k7c7
zeměpisnými	zeměpisný	k2eAgFnPc7d1
šířkami	šířka	k1gFnPc7
22	#num#	k4
<g/>
°	°	k?
a	a	k8xC
35	#num#	k4
<g/>
°	°	k?
jižní	jižní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
a	a	k8xC
16	#num#	k4
<g/>
°	°	k?
až	až	k9
33	#num#	k4
<g/>
°	°	k?
východní	východní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
rozlehlé	rozlehlý	k2eAgFnSc2d1
<g/>
,	,	kIx,
na	na	k7c6
většině	většina	k1gFnSc6
míst	místo	k1gNnPc2
téměř	téměř	k6eAd1
rovné	rovný	k2eAgFnSc2d1
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
mezi	mezi	k7c7
1	#num#	k4
000	#num#	k4
m	m	kA
(	(	kIx(
<g/>
3	#num#	k4
300	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
a	a	k8xC
2	#num#	k4
100	#num#	k4
m	m	kA
(	(	kIx(
<g/>
6	#num#	k4
900	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc4d3
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
mírně	mírně	k6eAd1
se	se	k3xPyFc4
svažující	svažující	k2eAgMnPc1d1
dolů	dolů	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
o	o	k7c4
něco	něco	k3yInSc4
méně	málo	k6eAd2
znatelně	znatelně	k6eAd1
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
jihozápad	jihozápad	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
je	být	k5eAaImIp3nS
obklopena	obklopit	k5eAaPmNgFnS
Velkým	velký	k2eAgInSc7d1
srázem	sráz	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
jehož	jehož	k3xOyRp3gInSc1
východní	východní	k2eAgInSc1d1
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc1d3
úsek	úsek	k1gInSc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Dračí	dračí	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
a	a	k8xC
jihozápadní	jihozápadní	k2eAgFnSc1d1
část	část	k1gFnSc1
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1	#num#	k4
100	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
800	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
přilehlá	přilehlý	k2eAgFnSc1d1
nížina	nížina	k1gFnSc1
dole	dole	k6eAd1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
700	#num#	k4
<g/>
–	–	k?
800	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
-	-	kIx~
viz	vidět	k5eAaImRp2nS
mapa	mapa	k1gFnSc1
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgInPc1d1
jako	jako	k8xS,k8xC
Velké	velká	k1gFnPc4
Karru	Karr	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
řídce	řídce	k6eAd1
rozmístěných	rozmístěný	k2eAgFnPc2d1
křovin	křovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
velké	velká	k1gFnSc2
karru	karr	k1gInSc2
mizí	mizet	k5eAaImIp3nS
v	v	k7c4
ještě	ještě	k6eAd1
sušší	suchý	k2eAgInSc4d2
a	a	k8xC
vyprahlější	vyprahlý	k2eAgInSc4d2,k2eAgInSc4d1
Bushmanland	Bushmanland	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
na	na	k7c6
samém	samý	k3xTgInSc6
severozápadě	severozápad	k1gInSc6
země	zem	k1gFnSc2
nakonec	nakonec	k6eAd1
přechází	přecházet	k5eAaImIp3nS
v	v	k7c4
poušť	poušť	k1gFnSc4
Kalahari	Kalahar	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středovýchodní	středovýchodní	k2eAgFnSc1d1
a	a	k8xC
nejvyšší	vysoký	k2eAgFnSc1d3
část	část	k1gFnSc1
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
jako	jako	k9
vysoký	vysoký	k2eAgInSc4d1
Veld	Veld	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
relativně	relativně	k6eAd1
dobře	dobře	k6eAd1
zavlažovaná	zavlažovaný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
komerčních	komerční	k2eAgFnPc2d1
zemědělských	zemědělský	k2eAgFnPc2d1
půd	půda	k1gFnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
největší	veliký	k2eAgFnSc4d3
aglomeraci	aglomerace	k1gFnSc4
(	(	kIx(
<g/>
Gauteng	Gauteng	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
vysokého	vysoký	k2eAgInSc2d1
Veldu	Veld	k1gInSc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
od	od	k7c2
25	#num#	k4
<g/>
°	°	k?
30	#num#	k4
<g/>
'	'	kIx"
jižní	jižní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
svažuje	svažovat	k5eAaImIp3nS
dolů	dolů	k6eAd1
do	do	k7c2
Bushveldu	Bushveld	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nakonec	nakonec	k6eAd1
ustupuje	ustupovat	k5eAaImIp3nS
nížinné	nížinný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
Limpopo	Limpopa	k1gFnSc5
nebo	nebo	k8xC
nízký	nízký	k2eAgInSc1d1
Veld	Veld	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pobřežní	pobřežní	k2eAgInSc1d1
pás	pás	k1gInSc1
pod	pod	k7c7
Velkým	velký	k2eAgInSc7d1
srázem	sráz	k1gInSc7
<g/>
,	,	kIx,
pohybující	pohybující	k2eAgInPc4d1
se	se	k3xPyFc4
ve	v	k7c6
směru	směr	k1gInSc6
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
od	od	k7c2
severovýchodu	severovýchod	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
nízkého	nízký	k2eAgInSc2d1
veldu	veld	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Limpopo	Limpopa	k1gFnSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
nízkého	nízký	k2eAgInSc2d1
veldu	veld	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Mpumalanga	Mpumalanga	k1gFnSc1
pod	pod	k7c7
dračími	dračí	k2eAgFnPc7d1
horami	hora	k1gFnPc7
(	(	kIx(
<g/>
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
srázu	sráz	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
84	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
teplejší	teplý	k2eAgFnSc1d2
<g/>
,	,	kIx,
suchší	suchší	k2eAgFnSc1d1
a	a	k8xC
méně	málo	k6eAd2
intenzivně	intenzivně	k6eAd1
kultivovaný	kultivovaný	k2eAgInSc1d1
než	než	k8xS
vysoký	vysoký	k2eAgInSc1d1
veld	veld	k1gInSc1
nad	nad	k7c7
srázem	sráz	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Krugerův	Krugerův	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
provinciích	provincie	k1gFnPc6
Limpopo	Limpopa	k1gFnSc5
a	a	k8xC
Mpumalanga	Mpumalanga	k1gFnSc1
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
zabírá	zabírat	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
nízkého	nízký	k2eAgInSc2d1
veldu	veld	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pokrývá	pokrývat	k5eAaImIp3nS
19	#num#	k4
633	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Jižně	jižně	k6eAd1
od	od	k7c2
nízkého	nízký	k2eAgInSc2d1
veldu	veld	k1gInSc2
se	se	k3xPyFc4
roční	roční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
při	při	k7c6
přechodu	přechod	k1gInSc6
do	do	k7c2
KwaZulu	KwaZul	k1gInSc2
zvyšují	zvyšovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirozená	přirozený	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zejména	zejména	k9
u	u	k7c2
pobřeží	pobřeží	k1gNnSc2
subtropicky	subtropicky	k6eAd1
horká	horký	k2eAgFnSc1d1
a	a	k8xC
vlhká	vlhký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
KwaZulu-Natal	KwaZulu-Natal	k1gMnSc1
–	–	k?
Lesotho	Lesot	k1gMnSc4
tvoří	tvořit	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnSc4d3
část	část	k1gFnSc4
Velkého	velký	k2eAgInSc2d1
srázu	sráz	k1gInSc2
-	-	kIx~
Dračí	dračí	k2eAgFnPc1d1
Hory	hora	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
dosahují	dosahovat	k5eAaImIp3nP
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
přes	přes	k7c4
3	#num#	k4
000	#num#	k4
m	m	kA
(	(	kIx(
<g/>
9	#num#	k4
800	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Podnebí	podnebí	k1gNnSc6
na	na	k7c6
úpatí	úpatí	k1gNnSc6
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
Dračích	dračí	k2eAgFnPc2d1
hor	hora	k1gFnPc2
je	být	k5eAaImIp3nS
mírné	mírný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
Witwatersrandu	Witwatersrand	k1gInSc2
představuje	představovat	k5eAaImIp3nS
největší	veliký	k2eAgFnSc4d3
koncentraci	koncentrace	k1gFnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
Johannesburg	Johannesburg	k1gInSc1
<g/>
,	,	kIx,
Pretorie	Pretorie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
má	mít	k5eAaImIp3nS
obrovské	obrovský	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
zlata	zlato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
známá	známý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
Stolová	stolový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
zvedající	zvedající	k2eAgFnSc1d1
se	se	k3xPyFc4
z	z	k7c2
moře	moře	k1gNnSc2
do	do	k7c2
tisícimetrové	tisícimetrový	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
nad	nad	k7c4
Kapské	kapský	k2eAgNnSc4d1
Město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
stále	stále	k6eAd1
žije	žít	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
divoké	divoký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
její	její	k3xOp3gInSc1
výskyt	výskyt	k1gInSc1
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
vlivem	vlivem	k7c2
civilizačních	civilizační	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
ohrožen	ohrozit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
účely	účel	k1gInPc4
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
celkem	celkem	k6eAd1
13	#num#	k4
národních	národní	k2eAgInPc2d1
parků	park	k1gInPc2
s	s	k7c7
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejznámější	známý	k2eAgMnPc1d3
jsou	být	k5eAaImIp3nP
Krugerův	Krugerův	k2eAgInSc4d1
NP	NP	kA
<g/>
,	,	kIx,
NP	NP	kA
Kalahari	Kalahari	k1gNnSc4
Gemsbok	Gemsbok	k1gInSc1
a	a	k8xC
rovněž	rovněž	k9
Addo	Addo	k1gMnSc1
Elefant	elefant	k1gMnSc1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Port	porta	k1gFnPc2
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Klima	klima	k1gNnSc1
</s>
<s>
Köppenova	Köppenův	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
podnebí	podnebí	k1gNnSc2
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
má	mít	k5eAaImIp3nS
obecně	obecně	k6eAd1
mírné	mírný	k2eAgNnSc1d1
klima	klima	k1gNnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
Atlantickým	atlantický	k2eAgInSc7d1
a	a	k8xC
Indickým	indický	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
obklopena	obklopit	k5eAaPmNgFnS
ze	z	k7c2
tří	tři	k4xCgFnPc2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
také	také	k9
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
klimaticky	klimaticky	k6eAd1
mírnější	mírný	k2eAgFnSc6d2
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
a	a	k8xC
protože	protože	k8xS
její	její	k3xOp3gFnSc1
průměrná	průměrný	k2eAgFnSc1d1
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
stabilně	stabilně	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
směrem	směr	k1gInSc7
na	na	k7c4
sever	sever	k1gInSc4
(	(	kIx(
<g/>
k	k	k7c3
rovníku	rovník	k1gInSc3
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
různorodá	různorodý	k2eAgFnSc1d1
topografie	topografie	k1gFnSc1
a	a	k8xC
oceánský	oceánský	k2eAgInSc1d1
vliv	vliv	k1gInSc1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
velké	velký	k2eAgFnSc3d1
rozmanitosti	rozmanitost	k1gFnSc3
klimatických	klimatický	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatická	klimatický	k2eAgNnPc1d1
pásma	pásmo	k1gNnPc1
sahají	sahat	k5eAaImIp3nP
od	od	k7c2
extrémní	extrémní	k2eAgFnSc2d1
pouště	poušť	k1gFnSc2
jižní	jižní	k2eAgFnSc2d1
Namibské	Namibský	k2eAgFnSc2d1
pouště	poušť	k1gFnSc2
na	na	k7c6
nejvzdálenějším	vzdálený	k2eAgInSc6d3
severozápadě	severozápad	k1gInSc6
po	po	k7c4
svěží	svěží	k2eAgNnSc4d1
subtropické	subtropický	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
na	na	k7c6
východě	východ	k1gInSc6
podél	podél	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Mosambikem	Mosambik	k1gInSc7
a	a	k8xC
Indickým	indický	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimy	Zima	k1gMnPc4
se	se	k3xPyFc4
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
vyskytují	vyskytovat	k5eAaImIp3nP
mezi	mezi	k7c7
červnem	červen	k1gInSc7
a	a	k8xC
srpnem	srpen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Nejzazší	zadní	k2eAgInSc1d3
jihozápad	jihozápad	k1gInSc1
má	mít	k5eAaImIp3nS
pozoruhodně	pozoruhodně	k6eAd1
podobné	podobný	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
jako	jako	k8xS,k8xC
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
s	s	k7c7
vlhkými	vlhký	k2eAgFnPc7d1
zimami	zima	k1gFnPc7
a	a	k8xC
horkými	horký	k2eAgFnPc7d1
<g/>
,	,	kIx,
suchými	suchý	k2eAgNnPc7d1
léty	léto	k1gNnPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
biom	biom	k1gInSc1
kapské	kapský	k2eAgFnSc2d1
květenné	květenný	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dominuje	dominovat	k5eAaImIp3nS
fynbos	fynbos	k1gInSc1
<g/>
,	,	kIx,
křovin	křovina	k1gFnPc2
a	a	k8xC
houštiny	houština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
také	také	k9
produkuje	produkovat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
vína	víno	k1gNnSc2
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
zejména	zejména	k9
svým	svůj	k3xOyFgInSc7
větrem	vítr	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
fouká	foukat	k5eAaImIp3nS
přerušovaně	přerušovaně	k6eAd1
téměř	téměř	k6eAd1
po	po	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
síle	síla	k1gFnSc3
tohoto	tento	k3xDgInSc2
větru	vítr	k1gInSc2
byl	být	k5eAaImAgMnS
průjezd	průjezd	k1gInSc4
okolo	okolo	k7c2
mysu	mys	k1gInSc2
Dobré	dobrý	k2eAgFnSc2d1
naděje	naděje	k1gFnSc2
pro	pro	k7c4
námořníky	námořník	k1gMnPc4
obzvláště	obzvláště	k6eAd1
zrádný	zrádný	k2eAgMnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
mnoha	mnoho	k4c3
ztroskotáním	ztroskotání	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
srážky	srážka	k1gFnPc1
rovnoměrněji	rovnoměrně	k6eAd2
rozloženy	rozložit	k5eAaPmNgFnP
po	po	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
zelenou	zelený	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Garden	Gardno	k1gNnPc2
Route	Rout	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
je	být	k5eAaImIp3nS
obzvláště	obzvláště	k6eAd1
plochý	plochý	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
středu	střed	k1gInSc6
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Vaal	Vaal	k1gInSc1
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
Veld	Veld	k1gInSc1
lépe	dobře	k6eAd2
zavodněn	zavodněn	k2eAgInSc1d1
a	a	k8xC
nezažívá	zažívat	k5eNaImIp3nS
subtropické	subtropický	k2eAgInPc4d1
tepelné	tepelný	k2eAgInPc4d1
extrémy	extrém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johannesburg	Johannesburg	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
středu	střed	k1gInSc6
vysokého	vysoký	k2eAgInSc2d1
Veldu	Veld	k1gInSc2
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
1740	#num#	k4
m	m	kA
(	(	kIx(
<g/>
5,709	5,709	k4
ft	ft	k?
<g/>
)	)	kIx)
m.	m.	k?
<g/>
n.	n.	k?
<g/>
m.	m.	k?
a	a	k8xC
přijímá	přijímat	k5eAaImIp3nS
roční	roční	k2eAgFnPc4d1
srážky	srážka	k1gFnPc4
760	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
29,9	29,9	k4
palce	palec	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimy	Zima	k1gMnPc7
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
jsou	být	k5eAaImIp3nP
chladné	chladný	k2eAgNnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
sníh	sníh	k1gInSc1
je	být	k5eAaImIp3nS
vzácný	vzácný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1
Dračí	dračí	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
tvoří	tvořit	k5eAaImIp3nP
jihovýchodní	jihovýchodní	k2eAgInSc4d1
sráz	sráz	k1gInSc4
vysokého	vysoký	k2eAgInSc2d1
Veldu	Veld	k1gInSc2
<g/>
,	,	kIx,
nabízejí	nabízet	k5eAaImIp3nP
v	v	k7c6
zimě	zima	k1gFnSc6
omezené	omezený	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
lyžování	lyžování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejchladnějším	chladný	k2eAgNnSc7d3
místem	místo	k1gNnSc7
na	na	k7c6
jihoafrickém	jihoafrický	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
je	být	k5eAaImIp3nS
Buffelsfontein	Buffelsfontein	k1gInSc1
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Kapsku	Kapsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
zaznamenána	zaznamenán	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
−	−	k?
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
−	−	k?
<g/>
4,2	4,2	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Ostrovy	ostrov	k1gInPc1
prince	princ	k1gMnSc2
Edvarda	Edvard	k1gMnSc2
mají	mít	k5eAaImIp3nP
nižší	nízký	k2eAgFnPc1d2
průměrné	průměrný	k2eAgFnPc1d1
roční	roční	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
Buffelsfontein	Buffelsfontein	k1gInSc1
má	mít	k5eAaImIp3nS
chladnější	chladný	k2eAgInPc4d2
extrémy	extrém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hlubokém	hluboký	k2eAgNnSc6d1
vnitrozemí	vnitrozemí	k1gNnSc6
jihoafrického	jihoafrický	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
jsou	být	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnPc1d3
teploty	teplota	k1gFnPc1
<g/>
:	:	kIx,
teplota	teplota	k1gFnSc1
51,7	51,7	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
125,06	125,06	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
zaznamenána	zaznamenat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
severního	severní	k2eAgNnSc2d1
Kapska	Kapsko	k1gNnSc2
v	v	k7c6
Kalahari	Kalahar	k1gInSc6
poblíž	poblíž	k7c2
Upingtonu	Upington	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
tato	tento	k3xDgFnSc1
teplota	teplota	k1gFnSc1
je	být	k5eAaImIp3nS
neoficiální	neoficiální	k2eAgFnSc1d1,k2eNgFnSc1d1
a	a	k8xC
nebyla	být	k5eNaImAgFnS
zaznamenána	zaznamenat	k5eAaPmNgFnS
se	s	k7c7
standardním	standardní	k2eAgNnSc7d1
vybavením	vybavení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
je	být	k5eAaImIp3nS
48,8	48,8	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
119,84	119,84	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
ve	v	k7c6
Vioolsdrifu	Vioolsdrif	k1gInSc6
v	v	k7c6
lednu	leden	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
města	město	k1gNnPc1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
984	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Durban	Durban	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
531	#num#	k4
300	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
i	i	k8xC
s	s	k7c7
předměstími	předměstí	k1gNnPc7
3	#num#	k4
800	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Johannesburg	Johannesburg	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
975	#num#	k4
500	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
i	i	k8xC
s	s	k7c7
předměstími	předměstí	k1gNnPc7
5	#num#	k4
550	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
(	(	kIx(
<g/>
741	#num#	k4
600	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc1
vlády	vláda	k1gFnSc2
v	v	k7c6
Pretorii	Pretorie	k1gFnSc6
</s>
<s>
Budova	budova	k1gFnSc1
parlamentu	parlament	k1gInSc2
v	v	k7c6
Kapském	kapský	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc4
zákonodárného	zákonodárný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Johannesburgu	Johannesburg	k1gInSc6
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
parlamentní	parlamentní	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
takových	takový	k3xDgFnPc2
republik	republika	k1gFnPc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
prezident	prezident	k1gMnSc1
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
i	i	k8xC
hlavou	hlava	k1gFnSc7
vlády	vláda	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
působení	působení	k1gNnSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
důvěře	důvěra	k1gFnSc6
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonná	výkonný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
,	,	kIx,
zákonodárná	zákonodárný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
i	i	k8xC
soudnictví	soudnictví	k1gNnSc1
podléhají	podléhat	k5eAaImIp3nP
ústavě	ústava	k1gFnSc3
a	a	k8xC
vrchní	vrchní	k2eAgInPc1d1
soudy	soud	k1gInPc1
mají	mít	k5eAaImIp3nP
pravomoc	pravomoc	k1gFnSc4
strhávat	strhávat	k5eAaImF
výkonné	výkonný	k2eAgFnPc4d1
akce	akce	k1gFnPc4
a	a	k8xC
rozhodnutí	rozhodnutí	k1gNnPc4
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
protiústavní	protiústavní	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
<g/>
,	,	kIx,
dolní	dolní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
400	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
volena	volit	k5eAaImNgFnS
každých	každý	k3xTgNnPc2
pět	pět	k4xCc4
let	léto	k1gNnPc2
na	na	k7c6
bázi	báze	k1gFnSc6
proporčního	proporční	k2eAgInSc2d1
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
bez	bez	k7c2
jakékoliv	jakýkoliv	k3yIgFnSc2
vstupní	vstupní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
provincií	provincie	k1gFnPc2
s	s	k7c7
90	#num#	k4
členy	člen	k1gInPc7
tvoří	tvořit	k5eAaImIp3nP
horní	horní	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
každý	každý	k3xTgMnSc1
z	z	k7c2
devíti	devět	k4xCc2
zemských	zemský	k2eAgInPc2d1
zákonodárných	zákonodárný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
volí	volit	k5eAaImIp3nS
deset	deset	k4xCc4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
každé	každý	k3xTgFnSc6
provincii	provincie	k1gFnSc6
zvoleno	zvolit	k5eAaPmNgNnS
deset	deset	k4xCc1
zástupců	zástupce	k1gMnPc2
rovněž	rovněž	k9
na	na	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnSc2
do	do	k7c2
obou	dva	k4xCgFnPc2
komor	komora	k1gFnPc2
probíhají	probíhat	k5eAaImIp3nP
vždy	vždy	k6eAd1
současně	současně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsilnější	silný	k2eAgFnSc1d3
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
příp	příp	kA
<g/>
.	.	kIx.
koalice	koalice	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
formuje	formovat	k5eAaImIp3nS
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
čele	čelo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
každých	každý	k3xTgFnPc6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
volí	volit	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
shromáždění	shromáždění	k1gNnSc2
jednoho	jeden	k4xCgMnSc2
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
za	za	k7c2
prezidenta	prezident	k1gMnSc2
<g/>
;	;	kIx,
prezident	prezident	k1gMnSc1
tedy	tedy	k9
vykonává	vykonávat	k5eAaImIp3nS
funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
stejné	stejný	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
Shromáždění	shromáždění	k1gNnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
prezident	prezident	k1gMnSc1
nesmí	smět	k5eNaImIp3nS
vykonávat	vykonávat	k5eAaImF
ve	v	k7c6
funkci	funkce	k1gFnSc6
více	hodně	k6eAd2
než	než	k8xS
dvě	dva	k4xCgNnPc4
funkční	funkční	k2eAgNnPc4d1
období	období	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Prezident	prezident	k1gMnSc1
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
viceprezidenta	viceprezident	k1gMnSc4
a	a	k8xC
ministry	ministr	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
tvoří	tvořit	k5eAaImIp3nP
kabinet	kabinet	k1gInSc4
složený	složený	k2eAgInSc4d1
z	z	k7c2
ministerstev	ministerstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidenta	prezident	k1gMnSc4
a	a	k8xC
kabinet	kabinet	k1gInSc4
může	moct	k5eAaImIp3nS
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
odvolat	odvolat	k5eAaPmF
na	na	k7c6
základě	základ	k1gInSc6
vyslovení	vyslovení	k1gNnSc4
nedůvěry	nedůvěra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
konaných	konaný	k2eAgInPc2d1
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
získal	získat	k5eAaPmAgInS
ANC	ANC	kA
57,5	57,5	k4
<g/>
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
230	#num#	k4
křesel	křeslo	k1gNnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
hlavní	hlavní	k2eAgFnSc1d1
opozice	opozice	k1gFnSc1
<g/>
,	,	kIx,
Demokratická	demokratický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
DA	DA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
20,77	20,77	k4
<g/>
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
84	#num#	k4
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovníci	bojovník	k1gMnPc1
za	za	k7c4
ekonomickou	ekonomický	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
(	(	kIx(
<g/>
Economic	Economice	k1gInPc2
Freedom	Freedom	k1gInSc1
Fighters	Fighters	k1gInSc1
(	(	kIx(
<g/>
EFF	EFF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
založil	založit	k5eAaPmAgMnS
Julius	Julius	k1gMnSc1
Malema	Malem	k1gMnSc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
mladého	mladý	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
ANC	ANC	kA
(	(	kIx(
<g/>
ANC	ANC	kA
Youth	Youth	k1gInSc1
League	League	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
z	z	k7c2
ANC	ANC	kA
vyloučen	vyloučit	k5eAaPmNgInS
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
10,79	10,79	k4
<g/>
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
44	#num#	k4
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ANC	ANC	kA
je	být	k5eAaImIp3nS
od	od	k7c2
konce	konec	k1gInSc2
apartheidu	apartheid	k1gInSc2
vládnoucí	vládnoucí	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
května	květen	k1gInSc2
2009	#num#	k4
do	do	k7c2
února	únor	k1gInSc2
2018	#num#	k4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
republiky	republika	k1gFnSc2
Jacob	Jacoba	k1gFnPc2
Zuma	Zuma	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
okamžitou	okamžitý	k2eAgFnSc4d1
rezignaci	rezignace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zumu	Zumus	k1gInSc2
provází	provázet	k5eAaImIp3nS
řada	řada	k1gFnSc1
korupčních	korupční	k2eAgInPc2d1
skandálů	skandál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejzávažnější	závažný	k2eAgFnSc1d3
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
renovace	renovace	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
soukromého	soukromý	k2eAgInSc2d1
majetku	majetek	k1gInSc2
ze	z	k7c2
státní	státní	k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
funkce	funkce	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byl	být	k5eAaImAgInS
Jihoafrickým	jihoafrický	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
zvolen	zvolen	k2eAgMnSc1d1
někdejší	někdejší	k2eAgMnSc1d1
viceprezident	viceprezident	k1gMnSc1
a	a	k8xC
předseda	předseda	k1gMnSc1
vládní	vládní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Africký	africký	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
kongres	kongres	k1gInSc1
(	(	kIx(
<g/>
ANC	ANC	kA
<g/>
)	)	kIx)
Cyril	Cyril	k1gMnSc1
Ramaphosa	Ramaphosa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
nemá	mít	k5eNaImIp3nS
právně	právně	k6eAd1
definované	definovaný	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtá	čtvrtá	k1gFnSc1
kapitola	kapitola	k1gFnSc1
ústavy	ústava	k1gFnSc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
sídlem	sídlo	k1gNnSc7
parlamentu	parlament	k1gInSc2
je	být	k5eAaImIp3nS
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
parlamentní	parlamentní	k2eAgInSc1d1
akt	akt	k1gInSc1
přijatý	přijatý	k2eAgInSc1d1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
čl	čl	kA
<g/>
.	.	kIx.
76	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
a	a	k8xC
5	#num#	k4
může	moct	k5eAaImIp3nS
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
sídlo	sídlo	k1gNnSc1
parlamentu	parlament	k1gInSc2
je	být	k5eAaImIp3nS
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Tři	tři	k4xCgFnPc1
vládní	vládní	k2eAgFnPc1d1
sekce	sekce	k1gFnPc1
země	zem	k1gFnSc2
jsou	být	k5eAaImIp3nP
rozděleny	rozdělit	k5eAaPmNgFnP
do	do	k7c2
různých	různý	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
sídlo	sídlo	k1gNnSc4
parlamentu	parlament	k1gInSc2
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
legislativy	legislativa	k1gFnSc2
<g/>
;	;	kIx,
Pretorie	Pretorie	k1gFnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
sídlo	sídlo	k1gNnSc1
prezidenta	prezident	k1gMnSc2
a	a	k8xC
kabinetu	kabinet	k1gInSc2
administrativním	administrativní	k2eAgNnSc7d1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
;	;	kIx,
a	a	k8xC
Bloemfontein	Bloemfontein	k1gInSc1
je	být	k5eAaImIp3nS
jako	jako	k9
sídlo	sídlo	k1gNnSc1
Nejvyššího	vysoký	k2eAgInSc2d3
odvolacího	odvolací	k2eAgInSc2d1
soudu	soud	k1gInSc2
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
soudnictví	soudnictví	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Ústavní	ústavní	k2eAgInSc1d1
soud	soud	k1gInSc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Johannesburgu	Johannesburg	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
ambasád	ambasáda	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Pretorii	Pretorie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
konaly	konat	k5eAaImAgInP
tisíce	tisíc	k4xCgInSc2
protestů	protest	k1gInPc2
a	a	k8xC
nepokojů	nepokoj	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ji	on	k3xPp3gFnSc4
dle	dle	k7c2
analytika	analytik	k1gMnSc2
Imraana	Imraan	k1gMnSc2
Buccuse	Buccuse	k1gFnSc2
činilo	činit	k5eAaImAgNnS
„	„	k?
<g/>
zemí	zem	k1gFnPc2
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
protesty	protest	k1gInPc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
řadě	řada	k1gFnSc3
případů	případ	k1gInPc2
politické	politický	k2eAgFnSc2d1
represe	represe	k1gFnSc2
i	i	k8xC
hrozeb	hrozba	k1gFnPc2
budoucí	budoucí	k2eAgFnSc2d1
represe	represe	k1gFnSc2
v	v	k7c6
rozporu	rozpor	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
ústavou	ústava	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
některé	některý	k3yIgMnPc4
analytiky	analytik	k1gMnPc4
a	a	k8xC
organizace	organizace	k1gFnPc4
občanské	občanský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
nebo	nebo	k8xC
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
existovat	existovat	k5eAaImF
nové	nový	k2eAgNnSc1d1
ovzduší	ovzduší	k1gNnSc1
politické	politický	k2eAgFnSc2d1
represe	represe	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
pokles	pokles	k1gInSc1
politické	politický	k2eAgFnSc2d1
tolerance	tolerance	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
umístila	umístit	k5eAaPmAgFnS
na	na	k7c6
pátém	pátý	k4xOgNnSc6
místě	místo	k1gNnSc6
ze	z	k7c2
48	#num#	k4
zemí	zem	k1gFnPc2
subsaharské	subsaharský	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
na	na	k7c6
Ibrahimově	Ibrahimův	k2eAgInSc6d1
indexu	index	k1gInSc6
africké	africký	k2eAgFnSc2d1
správy	správa	k1gFnSc2
věcí	věc	k1gFnPc2
veřejných	veřejný	k2eAgFnPc2d1
(	(	kIx(
<g/>
Ibrahim	Ibrahima	k1gFnPc2
Index	index	k1gInSc1
of	of	k?
African	African	k1gInSc1
Governance	Governance	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
dobrých	dobrý	k2eAgMnPc2d1
výsledků	výsledek	k1gInPc2
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
právního	právní	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
transparentnosti	transparentnost	k1gFnSc2
<g/>
,	,	kIx,
korupce	korupce	k1gFnSc2
<g/>
,	,	kIx,
participace	participace	k1gFnSc2
a	a	k8xC
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pohořela	pohořet	k5eAaPmAgFnS
relativně	relativně	k6eAd1
slabým	slabý	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
africkou	africký	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
legalizovala	legalizovat	k5eAaBmAgFnS
manželství	manželství	k1gNnSc4
osob	osoba	k1gFnPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dozvuky	dozvuk	k1gInPc1
apartheidu	apartheid	k1gInSc2
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
promítají	promítat	k5eAaImIp3nP
i	i	k9
do	do	k7c2
majetkových	majetkový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
i	i	k9
přes	přes	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
oficiální	oficiální	k2eAgInSc4d1
konec	konec	k1gInSc4
vlastnilo	vlastnit	k5eAaImAgNnS
těsně	těsně	k6eAd1
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
zrušení	zrušení	k1gNnSc6
80	#num#	k4
%	%	kIx~
pozemků	pozemek	k1gInPc2
a	a	k8xC
půdy	půda	k1gFnSc2
bělošské	bělošský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
nicméně	nicméně	k8xC
představuje	představovat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
jen	jen	k9
9	#num#	k4
%	%	kIx~
z	z	k7c2
celé	celý	k2eAgFnSc2d1
jihoafrické	jihoafrický	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Pokus	pokus	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c4
restituční	restituční	k2eAgNnSc4d1
navrácení	navrácení	k1gNnSc4
této	tento	k3xDgFnSc2
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
černošskému	černošský	k2eAgNnSc3d1
a	a	k8xC
původnímu	původní	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
apartheidní	apartheidní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
odebrána	odebrán	k2eAgFnSc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
zemědělských	zemědělský	k2eAgFnPc2d1
farem	farma	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgMnPc6
jihoafrická	jihoafrický	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
stojí	stát	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
pozemkové	pozemkový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
snažila	snažit	k5eAaImAgFnS
od	od	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
řešit	řešit	k5eAaImF
tamní	tamní	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
formou	forma	k1gFnSc7
odkoupení	odkoupení	k1gNnSc2
a	a	k8xC
následného	následný	k2eAgNnSc2d1
převedení	převedení	k1gNnSc2
do	do	k7c2
státního	státní	k2eAgInSc2d1
majetkového	majetkový	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládní	vládní	k2eAgInSc1d1
audit	audit	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
nicméně	nicméně	k8xC
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
strategie	strategie	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
dobrovolném	dobrovolný	k2eAgInSc6d1
prodeji	prodej	k1gInSc6
skončila	skončit	k5eAaPmAgFnS
fiaskem	fiasko	k1gNnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
podíl	podíl	k1gInSc1
půdy	půda	k1gFnSc2
vlastněné	vlastněný	k2eAgMnPc4d1
bělochy	běloch	k1gMnPc4
se	se	k3xPyFc4
během	běh	k1gInSc7
dvou	dva	k4xCgFnPc2
dekád	dekáda	k1gFnPc2
snížil	snížit	k5eAaPmAgInS
zcela	zcela	k6eAd1
minimálně	minimálně	k6eAd1
z	z	k7c2
80	#num#	k4
%	%	kIx~
na	na	k7c4
72	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
tak	tak	k6eAd1
jihoafrický	jihoafrický	k2eAgInSc4d1
parlament	parlament	k1gInSc4
poměrem	poměr	k1gInSc7
241	#num#	k4
ku	k	k7c3
83	#num#	k4
schválil	schválit	k5eAaPmAgInS
článek	článek	k1gInSc1
25	#num#	k4
Ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
něhož	jenž	k3xRgInSc2
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
bez	bez	k7c2
náhrady	náhrada	k1gFnSc2
vyvlastnit	vyvlastnit	k5eAaPmF
ty	ten	k3xDgInPc4
pozemky	pozemek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
během	během	k7c2
apartheidu	apartheid	k1gInSc2
prokazatelně	prokazatelně	k6eAd1
ukradeny	ukraden	k2eAgFnPc1d1
nebo	nebo	k8xC
zabrány	zabrán	k2eAgFnPc1d1
černošskému	černošský	k2eAgNnSc3d1
a	a	k8xC
původnímu	původní	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Apartheidní	Apartheidní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
svém	svůj	k3xOyFgInSc6
oficiálním	oficiální	k2eAgInSc6d1
konci	konec	k1gInSc6
nepřestala	přestat	k5eNaPmAgFnS
promítat	promítat	k5eAaImF
ani	ani	k8xC
ve	v	k7c6
vzdělávacím	vzdělávací	k2eAgInSc6d1
sytému	sytý	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
ní	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
segregované	segregovaný	k2eAgFnPc4d1
školy	škola	k1gFnPc4
pro	pro	k7c4
bělochy	běloch	k1gMnPc4
záměrně	záměrně	k6eAd1
více	hodně	k6eAd2
financovány	financován	k2eAgInPc1d1
než	než	k8xS
ty	ten	k3xDgInPc1
pro	pro	k7c4
černochy	černoch	k1gMnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
promítlo	promítnout	k5eAaPmAgNnS
na	na	k7c6
nízkém	nízký	k2eAgInSc6d1
stavu	stav	k1gInSc6
kvality	kvalita	k1gFnSc2
škol	škola	k1gFnPc2
ve	v	k7c6
většinově	většinově	k6eAd1
chudých	chudý	k2eAgFnPc6d1
černošských	černošský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
a	a	k8xC
nízkém	nízký	k2eAgInSc6d1
podílu	podíl	k1gInSc6
černochů	černoch	k1gMnPc2
dosahujících	dosahující	k2eAgFnPc2d1
univerzitního	univerzitní	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
proto	proto	k8xC
i	i	k9
s	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
ohledem	ohled	k1gInSc7
na	na	k7c4
vysokou	vysoký	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
nerovnost	nerovnost	k1gFnSc4
rozhodla	rozhodnout	k5eAaPmAgFnS
ve	v	k7c6
snaze	snaha	k1gFnSc6
narovnat	narovnat	k5eAaPmF
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
zavedení	zavedení	k1gNnSc4
afirmativní	afirmativní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
(	(	kIx(
<g/>
pozivní	pozivní	k2eAgFnSc1d1
diskriminace	diskriminace	k1gFnSc1
<g/>
)	)	kIx)
po	po	k7c6
vzoru	vzor	k1gInSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
cílem	cíl	k1gInSc7
rozšířit	rozšířit	k5eAaPmF
vzdělanou	vzdělaný	k2eAgFnSc4d1
černošskou	černošský	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
praxe	praxe	k1gFnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
kritikou	kritika	k1gFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
oponentů	oponent	k1gMnPc2
afirmativní	afirmativní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gInSc7
vedlejším	vedlejší	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
diskriminace	diskriminace	k1gFnSc2
bělošských	bělošský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
</s>
<s>
Představitelé	představitel	k1gMnPc1
zemí	zem	k1gFnPc2
BRICS	BRICS	kA
na	na	k7c6
10	#num#	k4
<g/>
.	.	kIx.
sjezdu	sjezd	k1gInSc2
v	v	k7c6
Johannesburgu	Johannesburg	k1gInSc6
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
jako	jako	k9
Jihoafrická	jihoafrický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Jan	Jan	k1gMnSc1
Smuts	Smutsa	k1gFnPc2
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
preambule	preambule	k1gFnSc2
Charty	charta	k1gFnSc2
OSN	OSN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Africké	africký	k2eAgFnSc2d1
unie	unie	k1gFnSc2
(	(	kIx(
<g/>
AU	au	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
má	mít	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
největší	veliký	k2eAgFnSc4d3
ekonomiku	ekonomika	k1gFnSc4
ze	z	k7c2
všech	všecek	k3xTgMnPc2
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
zakládajícím	zakládající	k2eAgMnSc7d1
členem	člen	k1gMnSc7
nového	nový	k2eAgNnSc2d1
partnerství	partnerství	k1gNnSc2
AU	au	k0
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
NEPAD	padnout	k5eNaPmDgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
desetiletí	desetiletí	k1gNnSc6
hrála	hrát	k5eAaImAgFnS
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
prostředníka	prostředník	k1gMnSc2
v	v	k7c6
afrických	africký	k2eAgInPc6d1
konfliktech	konflikt	k1gInPc6
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Burundi	Burundi	k1gNnSc6
<g/>
,	,	kIx,
Demokratické	demokratický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Komorách	komora	k1gFnPc6
a	a	k8xC
Zimbabwe	Zimbabwe	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
apartheidu	apartheid	k1gInSc2
byla	být	k5eAaImAgFnS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
znovu	znovu	k6eAd1
přijata	přijmout	k5eAaPmNgFnS
do	do	k7c2
Společenství	společenství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
skupiny	skupina	k1gFnSc2
77	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
předsedala	předsedat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
také	také	k6eAd1
členem	člen	k1gInSc7
Jihoafrického	jihoafrický	k2eAgNnSc2d1
rozvojového	rozvojový	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
(	(	kIx(
<g/>
SADC	SADC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jihoatlantické	jihoatlantický	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
míru	mír	k1gInSc2
a	a	k8xC
spolupráce	spolupráce	k1gFnSc2
<g/>
,	,	kIx,
Jihoafrické	jihoafrický	k2eAgFnSc2d1
celní	celní	k2eAgFnSc2d1
unie	unie	k1gFnSc2
(	(	kIx(
<g/>
SACU	SACU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Antarktického	antarktický	k2eAgInSc2d1
smluvního	smluvní	k2eAgInSc2d1
paktu	pakt	k1gInSc2
(	(	kIx(
<g/>
ATS	ATS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
WTO	WTO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mezinárodního	mezinárodní	k2eAgInSc2d1
měnového	měnový	k2eAgInSc2d1
fondu	fond	k1gInSc2
(	(	kIx(
<g/>
MMF	MMF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
G	G	kA
<g/>
20	#num#	k4
<g/>
,	,	kIx,
G8	G8	k1gFnSc1
+	+	kIx~
5	#num#	k4
a	a	k8xC
Asociace	asociace	k1gFnSc1
pro	pro	k7c4
správu	správa	k1gFnSc4
přístavů	přístav	k1gInPc2
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
a	a	k8xC
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
jihoafrický	jihoafrický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Jacob	Jacoba	k1gFnPc2
Zuma	Zuma	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Chu	Chu	k1gMnSc1
Ťin-tchao	Ťin-tchao	k6eAd1
rozšířili	rozšířit	k5eAaPmAgMnP
dvoustranné	dvoustranný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
zeměmi	zem	k1gFnPc7
dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
podepsali	podepsat	k5eAaPmAgMnP
Pekingskou	pekingský	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
povýšila	povýšit	k5eAaPmAgFnS
dřívější	dřívější	k2eAgFnSc4d1
„	„	k?
<g/>
strategické	strategický	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
<g/>
“	“	k?
s	s	k7c7
Čínou	Čína	k1gFnSc7
na	na	k7c4
vyšší	vysoký	k2eAgFnSc4d2
úroveň	úroveň	k1gFnSc4
„	„	k?
<g/>
komplexního	komplexní	k2eAgNnSc2d1
partnerství	partnerství	k1gNnSc2
“	“	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
hospodářských	hospodářský	k2eAgFnPc6d1
i	i	k8xC
politických	politický	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
posílení	posílení	k1gNnPc2
výměn	výměna	k1gFnPc2
mezi	mezi	k7c7
jejich	jejich	k3xOp3gFnPc7
vládnoucími	vládnoucí	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
a	a	k8xC
zákonodárnými	zákonodárný	k2eAgInPc7d1
sbory	sbor	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
formálně	formálně	k6eAd1
připojila	připojit	k5eAaPmAgFnS
k	k	k7c3
seskupení	seskupení	k1gNnSc3
zemí	zem	k1gFnPc2
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
a	a	k8xC
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
BRICS	BRICS	kA
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
Zuma	Zuma	k1gMnSc1
označil	označit	k5eAaPmAgMnS
za	za	k7c4
největší	veliký	k2eAgMnPc4d3
obchodní	obchodní	k2eAgMnPc4d1
partnery	partner	k1gMnPc4
země	zem	k1gFnSc2
a	a	k8xC
také	také	k9
za	za	k7c4
největší	veliký	k2eAgMnPc4d3
obchodní	obchodní	k2eAgMnPc4d1
partnery	partner	k1gMnPc4
s	s	k7c7
Afrikou	Afrika	k1gFnSc7
jako	jako	k8xC,k8xS
celkem	celek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zuma	Zuma	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
členské	členský	k2eAgFnSc2d1
země	zem	k1gFnSc2
BRICS	BRICS	kA
budou	být	k5eAaImBp3nP
navzájem	navzájem	k6eAd1
spolupracovat	spolupracovat	k5eAaImF
také	také	k9
prostřednictvím	prostřednictvím	k7c2
OSN	OSN	kA
<g/>
,	,	kIx,
Skupiny	skupina	k1gFnPc1
dvaceti	dvacet	k4xCc2
(	(	kIx(
<g/>
G	G	kA
<g/>
20	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
fóra	fórum	k1gNnSc2
IBSA	IBSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
South	Southa	k1gFnPc2
African	Africany	k1gInPc2
National	National	k1gFnSc1
Defence	Defence	k1gFnSc1
Force	force	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vojáci	voják	k1gMnPc1
SANDF	SANDF	kA
</s>
<s>
Jihoafrické	jihoafrický	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
(	(	kIx(
<g/>
SANDF	SANDF	kA
<g/>
)	)	kIx)
vznikly	vzniknout	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
dobrovolnická	dobrovolnický	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
bývalých	bývalý	k2eAgFnPc2d1
jihoafrických	jihoafrický	k2eAgFnPc2d1
obranných	obranný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
sil	síla	k1gFnPc2
afrických	africký	k2eAgFnPc2d1
nacionalistických	nacionalistický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
(	(	kIx(
<g/>
Umkhonto	Umkhont	k2eAgNnSc1d1
we	we	k?
Sizwe	Sizwe	k1gNnSc1
a	a	k8xC
Poqo	Poqo	k1gNnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
bývalých	bývalý	k2eAgFnPc2d1
obranných	obranný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Bantustanů	Bantustan	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
SANDF	SANDF	kA
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
větve	větev	k1gFnPc4
-	-	kIx~
jihoafrickou	jihoafrický	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
jihoafrické	jihoafrický	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
,	,	kIx,
jihoafrické	jihoafrický	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc4
a	a	k8xC
jihoafrickou	jihoafrický	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
zdravotní	zdravotní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
SANDF	SANDF	kA
staly	stát	k5eAaPmAgInP
hlavní	hlavní	k2eAgInPc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
mírovou	mírový	k2eAgFnSc7d1
silou	síla	k1gFnSc7
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byly	být	k5eAaImAgFnP
zapojeny	zapojit	k5eAaPmNgFnP
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
do	do	k7c2
operací	operace	k1gFnPc2
v	v	k7c6
Lesothu	Lesoth	k1gInSc6
<g/>
,	,	kIx,
DR	dr	kA
Kongu	Kongo	k1gNnSc6
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Burundi	Burundi	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Sloužily	sloužit	k5eAaImAgFnP
také	také	k9
v	v	k7c6
mnohonárodních	mnohonárodní	k2eAgFnPc6d1
mírových	mírový	k2eAgFnPc6d1
silách	síla	k1gFnPc6
OSN	OSN	kA
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
zásahová	zásahový	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
sil	síla	k1gFnPc2
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
africkou	africký	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
úspěšně	úspěšně	k6eAd1
vyvinula	vyvinout	k5eAaPmAgFnS
jaderné	jaderný	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
první	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
(	(	kIx(
<g/>
následovaná	následovaný	k2eAgFnSc1d1
Ukrajinou	Ukrajina	k1gFnSc7
<g/>
)	)	kIx)
s	s	k7c7
jadernými	jaderný	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
dobrovolně	dobrovolně	k6eAd1
vzdala	vzdát	k5eAaPmAgFnS
<g/>
,	,	kIx,
zrušila	zrušit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
program	program	k1gInSc4
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
procesu	proces	k1gInSc2
podepsala	podepsat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
nešíření	nešíření	k1gNnSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
začala	začít	k5eAaPmAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
program	program	k1gInSc1
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
bývalého	bývalý	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
de	de	k?
Klerka	Klerk	k1gInSc2
bylo	být	k5eAaImAgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
vybudovat	vybudovat	k5eAaPmF
„	„	k?
<g/>
jaderný	jaderný	k2eAgInSc4d1
odstrašující	odstrašující	k2eAgInSc4d1
prostředek	prostředek	k1gInSc4
<g/>
“	“	k?
přijato	přijmout	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
na	na	k7c6
pozadí	pozadí	k1gNnSc6
sovětské	sovětský	k2eAgFnSc2d1
expanzivní	expanzivní	k2eAgFnSc2d1
hrozby	hrozba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
údajně	údajně	k6eAd1
provedla	provést	k5eAaPmAgFnS
jaderný	jaderný	k2eAgInSc4d1
test	test	k1gInSc4
nad	nad	k7c7
Atlantikem	Atlantik	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
ačkoli	ačkoli	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
oficiálně	oficiálně	k6eAd1
odmítáno	odmítán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
de	de	k?
Klerk	Klerk	k1gInSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
„	„	k?
<g/>
nikdy	nikdy	k6eAd1
neprovedla	provést	k5eNaPmAgFnS
tajný	tajný	k2eAgInSc4d1
jaderný	jaderný	k2eAgInSc4d1
test	test	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
Šest	šest	k4xCc1
jaderných	jaderný	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
bylo	být	k5eAaImAgNnS
dokončeno	dokončit	k5eAaPmNgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1980	#num#	k4
až	až	k9
1990	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
všechna	všechen	k3xTgFnSc1
byla	být	k5eAaImAgFnS
demontována	demontovat	k5eAaBmNgFnS
ještě	ještě	k9
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
země	zem	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
podepsala	podepsat	k5eAaPmAgFnS
Smlouvu	smlouva	k1gFnSc4
o	o	k7c6
nešíření	nešíření	k1gNnSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
podepsala	podepsat	k5eAaPmAgFnS
Smlouvu	smlouva	k1gFnSc4
OSN	OSN	kA
o	o	k7c6
zákazu	zákaz	k1gInSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Správní	správní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
země	zem	k1gFnSc2
</s>
<s>
Každá	každý	k3xTgFnSc1
z	z	k7c2
devíti	devět	k4xCc2
provincií	provincie	k1gFnPc2
je	být	k5eAaImIp3nS
řízena	řídit	k5eAaImNgFnS
jednokomorovým	jednokomorový	k2eAgInSc7d1
zákonodárným	zákonodárný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
volen	volit	k5eAaImNgInS
každých	každý	k3xTgNnPc2
pět	pět	k4xCc4
let	léto	k1gNnPc2
poměrným	poměrný	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
seznamu	seznam	k1gInSc2
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonodárce	zákonodárce	k1gMnSc1
volí	volit	k5eAaImIp3nS
premiéra	premiér	k1gMnSc4
za	za	k7c4
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
a	a	k8xC
premiér	premiér	k1gMnSc1
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
výkonnou	výkonný	k2eAgFnSc4d1
radu	rada	k1gFnSc4
jako	jako	k8xC,k8xS
provinční	provinční	k2eAgInSc1d1
kabinet	kabinet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravomoci	pravomoc	k1gFnSc2
provinčních	provinční	k2eAgFnPc2d1
vlád	vláda	k1gFnPc2
jsou	být	k5eAaImIp3nP
omezeny	omezit	k5eAaPmNgFnP
na	na	k7c4
témata	téma	k1gNnPc4
uvedená	uvedený	k2eAgNnPc4d1
v	v	k7c6
ústavě	ústav	k1gInSc6
<g/>
;	;	kIx,
tato	tento	k3xDgNnPc1
témata	téma	k1gNnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
oblasti	oblast	k1gFnPc1
jako	jako	k8xC,k8xS
zdraví	zdraví	k1gNnSc1
<g/>
,	,	kIx,
vzdělávání	vzdělávání	k1gNnSc1
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgNnSc1d1
bydlení	bydlení	k1gNnSc1
a	a	k8xC
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Provincie	provincie	k1gFnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
52	#num#	k4
okresů	okres	k1gInPc2
<g/>
:	:	kIx,
8	#num#	k4
metropolitních	metropolitní	k2eAgFnPc2d1
a	a	k8xC
44	#num#	k4
okresních	okresní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresní	okresní	k2eAgFnSc2d1
obce	obec	k1gFnSc2
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
205	#num#	k4
místních	místní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metropolitní	metropolitní	k2eAgFnPc4d1
obce	obec	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
řídí	řídit	k5eAaImIp3nP
největší	veliký	k2eAgFnSc2d3
městské	městský	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
<g/>
,	,	kIx,
plní	plnit	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
okresních	okresní	k2eAgFnPc2d1
i	i	k8xC
místních	místní	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
9	#num#	k4
autonomních	autonomní	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
číslu	číslo	k1gNnSc3
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
v	v	k7c6
závorce	závorka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Kapsko	Kapsko	k1gNnSc1
(	(	kIx(
<g/>
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgNnSc1d1
Kapsko	Kapsko	k1gNnSc1
(	(	kIx(
<g/>
Kimberley	Kimberlea	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Kapsko	Kapsko	k1gNnSc1
(	(	kIx(
<g/>
Bisho	Bis	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
KwaZulu-Natal	KwaZulu-Natal	k1gMnSc1
(	(	kIx(
<g/>
Pietermaritzburg¹	Pietermaritzburg¹	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
(	(	kIx(
<g/>
Bloemfontein	Bloemfontein	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
(	(	kIx(
<g/>
Mafikeng	Mafikeng	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Gauteng	Gauteng	k1gMnSc1
(	(	kIx(
<g/>
Johannesburg	Johannesburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Mpumalanga	Mpumalanga	k1gFnSc1
(	(	kIx(
<g/>
Nelspruit	Nelspruit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Limpopo	Limpopa	k1gFnSc5
(	(	kIx(
<g/>
Polokwane	Polokwan	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ekonomika	ekonomik	k1gMnSc4
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roční	roční	k2eAgInSc1d1
osobní	osobní	k2eAgInSc1d1
příjem	příjem	k1gInSc1
na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
podle	podle	k7c2
rasové	rasový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
úrovní	úroveň	k1gFnSc7
bílých	bílý	k2eAgMnPc2d1
</s>
<s>
Centrum	centrum	k1gNnSc1
Johannesburgu	Johannesburg	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
burza	burza	k1gFnSc1
na	na	k7c6
africkém	africký	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
</s>
<s>
Nákupní	nákupní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
v	v	k7c6
Kapském	kapský	k2eAgNnSc6d1
Městě	město	k1gNnSc6
v	v	k7c6
květnu	květen	k1gInSc6
2014	#num#	k4
</s>
<s>
JAR	jar	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
afrického	africký	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
stabilně	stabilně	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvyspělejších	vyspělý	k2eAgFnPc2d3
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dobře	dobře	k6eAd1
vyvinutý	vyvinutý	k2eAgInSc1d1
obchod	obchod	k1gInSc1
se	s	k7c7
službami	služba	k1gFnPc7
a	a	k8xC
je	být	k5eAaImIp3nS
již	již	k6eAd1
dlouho	dlouho	k6eAd1
industrializovaná	industrializovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HDP	HDP	kA
na	na	k7c4
obyvatele	obyvatel	k1gMnSc4
měl	mít	k5eAaImAgMnS
však	však	k9
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
začátkem	začátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
let	léto	k1gNnPc2
sestupnou	sestupný	k2eAgFnSc4d1
tendenci	tendence	k1gFnSc4
<g/>
,	,	kIx,
od	od	k7c2
konce	konec	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
zase	zase	k9
stoupá	stoupat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JAR	jar	k1gFnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
přibližně	přibližně	k6eAd1
25	#num#	k4
%	%	kIx~
HDP	HDP	kA
celého	celý	k2eAgInSc2d1
afrického	africký	k2eAgInSc2d1
světadílu	světadíl	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
též	též	k9
drží	držet	k5eAaImIp3nS
prvenství	prvenství	k1gNnSc4
ve	v	k7c6
spotřebě	spotřeba	k1gFnSc6
i	i	k8xC
výrobě	výroba	k1gFnSc6
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HDI	HDI	kA
<g/>
,	,	kIx,
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
do	do	k7c2
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
stoupal	stoupat	k5eAaImAgInS
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
klesat	klesat	k5eAaImF
<g/>
,	,	kIx,
ke	k	k7c3
stoupající	stoupající	k2eAgFnSc3d1
tendenci	tendence	k1gFnSc3
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInSc6
poklesu	pokles	k1gInSc6
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1995	#num#	k4
a	a	k8xC
2005	#num#	k4
mělo	mít	k5eAaImAgNnS
největší	veliký	k2eAgInSc4d3
podíl	podíl	k1gInSc4
drastické	drastický	k2eAgNnSc1d1
zkrácení	zkrácení	k1gNnSc1
délky	délka	k1gFnSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
61,5	61,5	k4
let	léto	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
oproti	oproti	k7c3
51	#num#	k4
letům	léto	k1gNnPc3
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
způsobené	způsobený	k2eAgFnSc2d1
především	především	k6eAd1
pandemií	pandemie	k1gFnSc7
AIDS	AIDS	kA
a	a	k8xC
selháním	selhání	k1gNnSc7
vlády	vláda	k1gFnSc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
ní	on	k3xPp3gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
JAR	jar	k1gFnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1
naleziště	naleziště	k1gNnSc4
nerostných	nerostný	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nemá	mít	k5eNaImIp3nS
ropu	ropa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
se	se	k3xPyFc4
soustředí	soustředit	k5eAaPmIp3nS
na	na	k7c4
pěstování	pěstování	k1gNnSc4
kukuřice	kukuřice	k1gFnSc2
<g/>
,	,	kIx,
pšenice	pšenice	k1gFnSc2
<g/>
,	,	kIx,
cukrové	cukrový	k2eAgFnSc2d1
třtiny	třtina	k1gFnSc2
a	a	k8xC
produkci	produkce	k1gFnSc4
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
kuřat	kuře	k1gNnPc2
<g/>
,	,	kIx,
skotu	skot	k1gInSc2
<g/>
,	,	kIx,
ovcí	ovce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavodňovací	zavodňovací	k2eAgInPc1d1
systémy	systém	k1gInPc1
nejsou	být	k5eNaImIp3nP
dostatečné	dostatečný	k2eAgInPc1d1
a	a	k8xC
rovněž	rovněž	k9
eroze	eroze	k1gFnSc1
půdy	půda	k1gFnSc2
představuje	představovat	k5eAaImIp3nS
závažný	závažný	k2eAgInSc1d1
problém	problém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převážně	převážně	k6eAd1
jihoafričtí	jihoafrický	k2eAgMnPc1d1
černoši	černoch	k1gMnPc1
nemají	mít	k5eNaImIp3nP
pro	pro	k7c4
sebe	sebe	k3xPyFc4
dostatek	dostatek	k1gInSc4
orné	orný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
a	a	k8xC
musejí	muset	k5eAaImIp3nP
hledat	hledat	k5eAaImF
práci	práce	k1gFnSc4
mimo	mimo	k7c4
své	svůj	k3xOyFgNnSc4
bydliště	bydliště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Vývoz	vývoz	k1gInSc1
představoval	představovat	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
přibližně	přibližně	k6eAd1
30	#num#	k4
%	%	kIx~
HDP	HDP	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
výrazný	výrazný	k2eAgInSc1d1
nárůst	nárůst	k1gInSc1
oproti	oproti	k7c3
počátku	počátek	k1gInSc3
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgNnSc4d1
mezinárodní	mezinárodní	k2eAgFnPc4d1
obchodní	obchodní	k2eAgFnPc4d1
partnery	partner	k1gMnPc7
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
–	–	k?
kromě	kromě	k7c2
dalších	další	k2eAgFnPc2d1
afrických	africký	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
–	–	k?
patří	patřit	k5eAaImIp3nS
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Španělsko	Španělsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
obchod	obchod	k1gInSc4
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
státy	stát	k1gInPc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
má	mít	k5eAaImIp3nS
výrazný	výrazný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
členství	členství	k1gNnSc2
JAR	jaro	k1gNnPc2
v	v	k7c6
JACU	JACU	kA
a	a	k8xC
JARS	JARS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyváží	vyvážit	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
diamanty	diamant	k1gInPc1
<g/>
,	,	kIx,
zlato	zlato	k1gNnSc1
(	(	kIx(
<g/>
největší	veliký	k2eAgFnPc1d3
zásoby	zásoba	k1gFnPc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
některé	některý	k3yIgFnPc4
rudy	ruda	k1gFnPc4
<g/>
,	,	kIx,
strojírenské	strojírenský	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dováží	dovážit	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
potraviny	potravina	k1gFnPc4
<g/>
,	,	kIx,
výrobky	výrobek	k1gInPc1
chemického	chemický	k2eAgNnSc2d1
a	a	k8xC
ropného	ropný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
strojírenské	strojírenský	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
JAR	jar	k1gFnSc1
nemá	mít	k5eNaImIp3nS
světové	světový	k2eAgNnSc4d1
prvenství	prvenství	k1gNnSc4
pouze	pouze	k6eAd1
v	v	k7c6
zásobách	zásoba	k1gFnPc6
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
v	v	k7c6
produkci	produkce	k1gFnSc6
platiny	platina	k1gFnSc2
<g/>
,	,	kIx,
rud	ruda	k1gFnPc2
chromu	chrom	k1gInSc2
a	a	k8xC
vanadia	vanadium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
těžba	těžba	k1gFnSc1
černého	černý	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
i	i	k8xC
železné	železný	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zemi	zem	k1gFnSc6
jsou	být	k5eAaImIp3nP
největší	veliký	k2eAgFnPc1d3
tepelné	tepelný	k2eAgFnPc1d1
elektrárny	elektrárna	k1gFnPc1
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
Transvaalu	Transvaal	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JAR	jar	k1gFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
téměř	téměř	k6eAd1
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
produkce	produkce	k1gFnSc2
elektřiny	elektřina	k1gFnSc2
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
Afriku	Afrika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
JAR	jar	k1gInSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1
komerční	komerční	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
na	na	k7c6
africkém	africký	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
<g/>
,	,	kIx,
JE	být	k5eAaImIp3nS
Koeberg	Koeberg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
státem	stát	k1gInSc7
vlastněný	vlastněný	k2eAgMnSc1d1
dodavatel	dodavatel	k1gMnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
(	(	kIx(
<g/>
Eskom	Eskom	k1gInSc1
<g/>
)	)	kIx)
začal	začít	k5eAaPmAgInS
pociťovat	pociťovat	k5eAaImF
nedostatek	nedostatek	k1gInSc1
kapacit	kapacita	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
výroby	výroba	k1gFnSc2
a	a	k8xC
rozvodu	rozvod	k1gInSc2
elektřiny	elektřina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
neschopnosti	neschopnost	k1gFnSc3
plnit	plnit	k5eAaImF
běžné	běžný	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
spotřebitelů	spotřebitel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozsáhlým	rozsáhlý	k2eAgInPc3d1
výpadkům	výpadek	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
zasáhly	zasáhnout	k5eAaPmAgInP
celou	celý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spouštěcím	spouštěcí	k2eAgInSc7d1
impulsem	impuls	k1gInSc7
byla	být	k5eAaImAgFnS
porucha	porucha	k1gFnSc1
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
elektrárně	elektrárna	k1gFnSc6
Koeberg	Koeberg	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
situace	situace	k1gFnSc1
ještě	ještě	k6eAd1
zhoršila	zhoršit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodavatel	dodavatel	k1gMnSc1
byl	být	k5eAaImAgMnS
široce	široko	k6eAd1
kritizován	kritizován	k2eAgMnSc1d1
kvůli	kvůli	k7c3
neschopnosti	neschopnost	k1gFnSc3
adekvátně	adekvátně	k6eAd1
udržovat	udržovat	k5eAaImF
svoje	svůj	k3xOyFgFnPc4
elektrárny	elektrárna	k1gFnPc4
<g/>
,	,	kIx,
nemluvě	nemluva	k1gFnSc6
o	o	k7c6
výstavbě	výstavba	k1gFnSc6
nových	nový	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
zajistily	zajistit	k5eAaPmAgFnP
dostačující	dostačující	k2eAgFnSc4d1
energetickou	energetický	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Korupce	korupce	k1gFnSc1
</s>
<s>
Korupce	korupce	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
velkým	velký	k2eAgInSc7d1
problémem	problém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
například	například	k6eAd1
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Pravin	Pravina	k1gFnPc2
Gordhan	Gordhany	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
vládního	vládní	k2eAgInSc2d1
korupčního	korupční	k2eAgInSc2d1
skandálu	skandál	k1gInSc2
odhalil	odhalit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
z	z	k7c2
vedení	vedení	k1gNnSc2
ANC	ANC	kA
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
napojeni	napojit	k5eAaPmNgMnP
na	na	k7c4
jihoafrického	jihoafrický	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
Jacoba	Jacoba	k1gFnSc1
Zumu	Zumus	k1gInSc3
<g/>
,	,	kIx,
zpronevěřili	zpronevěřit	k5eAaPmAgMnP
státní	státní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
v	v	k7c6
celkové	celkový	k2eAgFnSc6d1
výši	výše	k1gFnSc6,k1gFnSc6wB
kolem	kolem	k7c2
350	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Jacob	Jacoba	k1gFnPc2
Zuma	Zumum	k1gNnSc2
byl	být	k5eAaImAgInS
kvůli	kvůli	k7c3
korupčnímu	korupční	k2eAgInSc3d1
skandálu	skandál	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgMnS
vládních	vládní	k2eAgFnPc2d1
armádních	armádní	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
několika	několik	k4yIc2
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
odvolán	odvolán	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
z	z	k7c2
funkce	funkce	k1gFnSc2
jihoafrického	jihoafrický	k2eAgMnSc2d1
viceprezidenta	viceprezident	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
musel	muset	k5eAaImAgMnS
Zuma	Zuma	k1gMnSc1
kvůli	kvůli	k7c3
řadě	řada	k1gFnSc3
korupčních	korupční	k2eAgInPc2d1
skandálů	skandál	k1gInPc2
odstoupit	odstoupit	k5eAaPmF
také	také	k9
z	z	k7c2
funkce	funkce	k1gFnSc2
jihoafrického	jihoafrický	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
hustoty	hustota	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
</s>
<s>
<	<	kIx(
<g/>
1	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
30	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
100	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
300	#num#	k4
<g/>
–	–	k?
<g/>
1000	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
1000	#num#	k4
<g/>
–	–	k?
<g/>
3000	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
>	>	kIx)
<g/>
3000	#num#	k4
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
</s>
<s>
Dominantní	dominantní	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černoši	černoch	k1gMnPc1
Barevní	barevný	k2eAgMnPc1d1
Indové	Ind	k1gMnPc1
nebo	nebo	k8xC
Asiati	Asiat	k1gMnPc1
Běloši	běloch	k1gMnPc1
Nedominantní	dominantní	k2eNgMnPc4d1
</s>
<s>
Muži	muž	k1gMnPc1
z	z	k7c2
kmene	kmen	k1gInSc2
Zulu	Zulu	k1gMnSc1
</s>
<s>
V	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
žije	žít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
55	#num#	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
lidí	člověk	k1gMnPc2
různého	různý	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
jazyků	jazyk	k1gInPc2
a	a	k8xC
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
novější	nový	k2eAgInSc1d2
intercenzální	intercenzální	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
byl	být	k5eAaImAgInS
proveden	provést	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
vzrostl	vzrůst	k5eAaPmAgInS
z	z	k7c2
11,5	11,5	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
52	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
130	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
také	také	k9
odhadem	odhad	k1gInSc7
zhruba	zhruba	k6eAd1
5	#num#	k4
milionů	milion	k4xCgInPc2
ilegálních	ilegální	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
okolních	okolní	k2eAgInPc2d1
afrických	africký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
ze	z	k7c2
Zimbabwe	Zimbabw	k1gFnSc2
(	(	kIx(
<g/>
až	až	k6eAd1
3	#num#	k4
miliony	milion	k4xCgInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
k	k	k7c3
sérii	série	k1gFnSc3
nepokojů	nepokoj	k1gInPc2
proti	proti	k7c3
přistěhovalcům	přistěhovalec	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistika	statistika	k1gFnSc1
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
žádá	žádat	k5eAaImIp3nS
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
při	při	k7c6
sčítání	sčítání	k1gNnSc6
popsali	popsat	k5eAaPmAgMnP
v	v	k7c6
pěti	pět	k4xCc6
rasových	rasový	k2eAgFnPc6d1
populačních	populační	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
černoši	černoch	k1gMnPc1
79,2	79,2	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
běloši	běloch	k1gMnPc1
8,9	8,9	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
především	především	k9
Afrikánci	Afrikánec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
barevní	barevný	k2eAgMnPc1d1
8,9	8,9	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
asiaté	asiat	k1gMnPc1
2,5	2,5	k4
<g/>
%	%	kIx~
a	a	k8xC
ostatní	ostatní	k2eAgMnPc4d1
či	či	k8xC
blíže	blízce	k6eAd2
neurčení	neurčení	k1gNnSc4
0,5	0,5	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
žijí	žít	k5eAaImIp3nP
také	také	k9
Khoisanové	Khoisanový	k2eAgFnPc1d1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
označovaní	označovaný	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
Křováci	Křovák	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
běloši	běloch	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
22	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
už	už	k6eAd1
to	ten	k3xDgNnSc1
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
jen	jen	k9
16	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
nominálně	nominálně	k6eAd1
však	však	k9
bělošská	bělošský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
narostla	narůst	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
bělošského	bělošský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
procentuálně	procentuálně	k6eAd1
klesl	klesnout	k5eAaPmAgInS
v	v	k7c6
důsledku	důsledek	k1gInSc6
nárůstu	nárůst	k1gInSc2
černošských	černošský	k2eAgFnPc2d1
a	a	k8xC
domorodých	domorodý	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nominálnímu	nominální	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
až	až	k9
po	po	k7c6
pádu	pád	k1gInSc6
apartheidu	apartheid	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
část	část	k1gFnSc1
bělošské	bělošský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
rozhodla	rozhodnout	k5eAaPmAgFnS
emigrovat	emigrovat	k5eAaBmF
<g/>
,	,	kIx,
zejména	zejména	k9
do	do	k7c2
anglofonních	anglofonní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
nebo	nebo	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skokový	skokový	k2eAgInSc1d1
pád	pád	k1gInSc1
přišel	přijít	k5eAaPmAgInS
těsně	těsně	k6eAd1
po	po	k7c6
konci	konec	k1gInSc6
aparthedního	aparthední	k2eAgInSc2d1
režimu	režim	k1gInSc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1995	#num#	k4
a	a	k8xC
1996	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zemi	zem	k1gFnSc4
opustilo	opustit	k5eAaPmAgNnS
na	na	k7c4
800	#num#	k4
000	#num#	k4
bělochů	běloch	k1gMnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
měl	mít	k5eAaImAgInS
následně	následně	k6eAd1
stagnující	stagnující	k2eAgFnSc4d1
nebo	nebo	k8xC
vzrůstající	vzrůstající	k2eAgFnSc4d1
tendenci	tendence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
2020	#num#	k4
v	v	k7c6
zemi	zem	k1gFnSc6
žilo	žít	k5eAaImAgNnS
4	#num#	k4
680	#num#	k4
000	#num#	k4
příslušníků	příslušník	k1gMnPc2
bílé	bílý	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
oproti	oproti	k7c3
5	#num#	k4
220	#num#	k4
000	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
hostí	hostit	k5eAaImIp3nS
i	i	k9
značnou	značný	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
uprchlíků	uprchlík	k1gMnPc2
a	a	k8xC
žadatelů	žadatel	k1gMnPc2
o	o	k7c4
azyl	azyl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
World	World	k1gInSc4
Refugee	Refugee	k1gNnSc2
Survey	Survea	k1gFnSc2
2008	#num#	k4
<g/>
,	,	kIx,
publikovaného	publikovaný	k2eAgInSc2d1
americkým	americký	k2eAgInSc7d1
Výborem	výbor	k1gInSc7
pro	pro	k7c4
uprchlíky	uprchlík	k1gMnPc4
a	a	k8xC
přistěhovalce	přistěhovalec	k1gMnPc4
<g/>
,	,	kIx,
činila	činit	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
populace	populace	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
přibližně	přibližně	k6eAd1
144	#num#	k4
700	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
Skupiny	skupina	k1gFnPc1
uprchlíků	uprchlík	k1gMnPc2
a	a	k8xC
žadatelů	žadatel	k1gMnPc2
o	o	k7c4
azyl	azyl	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
přes	přes	k7c4
10	#num#	k4
000	#num#	k4
zahrnovaly	zahrnovat	k5eAaImAgFnP
lidi	člověk	k1gMnPc4
ze	z	k7c2
Zimbabwe	Zimbabw	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
48	#num#	k4
400	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
DR	dr	kA
Konga	Kongo	k1gNnSc2
(	(	kIx(
<g/>
24	#num#	k4
800	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Somálska	Somálsko	k1gNnSc2
(	(	kIx(
<g/>
12	#num#	k4
900	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
populace	populace	k1gFnPc1
žily	žít	k5eAaImAgFnP
hlavně	hlavně	k6eAd1
v	v	k7c6
Johannesburgu	Johannesburg	k1gInSc6
<g/>
,	,	kIx,
Pretorii	Pretorie	k1gFnSc6
<g/>
,	,	kIx,
Durbanu	Durban	k1gInSc6
<g/>
,	,	kIx,
Kapském	kapský	k2eAgNnSc6d1
Městě	město	k1gNnSc6
a	a	k8xC
Port	port	k1gInSc1
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Označení	označení	k1gNnSc4
„	„	k?
<g/>
barevní	barevný	k2eAgMnPc1d1
<g/>
“	“	k?
(	(	kIx(
<g/>
Coloureds	Coloureds	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
používáno	používat	k5eAaImNgNnS
pro	pro	k7c4
obyvatelstvo	obyvatelstvo	k1gNnSc4
míšeného	míšený	k2eAgInSc2d1
bantuského	bantuský	k2eAgInSc2d1
<g/>
,	,	kIx,
khoiského	khoiský	k2eAgInSc2d1
a	a	k8xC
evropského	evropský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
politiky	politika	k1gFnSc2
apartheidu	apartheid	k1gInSc2
měli	mít	k5eAaImAgMnP
barevní	barevný	k2eAgMnPc1d1
větší	veliký	k2eAgNnPc1d2
práva	právo	k1gNnPc1
než	než	k8xS
černoši	černoch	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
Barevní	barevný	k2eAgMnPc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1950	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
neměli	mít	k5eNaImAgMnP
volební	volební	k2eAgNnPc4d1
práva	právo	k1gNnPc4
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
černoši	černoch	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
1983	#num#	k4
jim	on	k3xPp3gInPc3
byla	být	k5eAaImAgFnS
povolena	povolen	k2eAgFnSc1d1
účast	účast	k1gFnSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jazyky	jazyk	k1gInPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
s	s	k7c7
dominantními	dominantní	k2eAgInPc7d1
jihoafrickými	jihoafrický	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
dle	dle	k7c2
oblasti	oblast	k1gFnSc2
</s>
<s>
zuluština	zuluština	k1gFnSc1
(	(	kIx(
<g/>
22,7	22,7	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
xhoština	xhoština	k1gFnSc1
(	(	kIx(
<g/>
16,0	16,0	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
afrikánština	afrikánština	k1gFnSc1
(	(	kIx(
<g/>
13,5	13,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
angličtina	angličtina	k1gFnSc1
(	(	kIx(
<g/>
9,6	9,6	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
severní	severní	k2eAgFnSc1d1
sotho	sotze	k6eAd1
(	(	kIx(
<g/>
9,1	9,1	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
setswanština	setswanština	k1gFnSc1
(	(	kIx(
<g/>
8,0	8,0	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
jižní	jižní	k2eAgNnSc1d1
sotho	sotze	k6eAd1
(	(	kIx(
<g/>
7,6	7,6	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
tsongština	tsongština	k1gFnSc1
(	(	kIx(
<g/>
4,5	4,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
svazijština	svazijština	k1gFnSc1
(	(	kIx(
<g/>
2,5	2,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
vendština	vendština	k1gFnSc1
(	(	kIx(
<g/>
2,4	2,4	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
jižní	jižní	k2eAgFnSc1d1
ndbelština	ndbelština	k1gFnSc1
(	(	kIx(
<g/>
2,1	2,1	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
nedominantní	dominantní	k2eNgFnSc1d1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
uznává	uznávat	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
11	#num#	k4
úředních	úřední	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závorce	závorka	k1gFnSc6
je	být	k5eAaImIp3nS
uvedeno	uveden	k2eAgNnSc1d1
procento	procento	k1gNnSc1
rodilých	rodilý	k2eAgMnPc2d1
mluvčích	mluvčí	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
udávají	udávat	k5eAaImIp3nP
daný	daný	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
jako	jako	k8xS,k8xC
svoji	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
řeč	řeč	k1gFnSc4
<g/>
:	:	kIx,
zuluština	zuluština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
24	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
xhoština	xhoština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
16	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
afrikánština	afrikánština	k1gFnSc1
(	(	kIx(
<g/>
necelých	celý	k2eNgInPc2d1
14	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
cca	cca	kA
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
sesothština	sesothština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
9	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
setswanština	setswanština	k1gFnSc1
neboli	neboli	k8xC
bečuánština	bečuánština	k1gFnSc1
(	(	kIx(
<g/>
cac	cac	k?
8	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc1d1
<g/>
)	)	kIx)
sesothština	sesothština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
7,5	7,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tsongština	tsongština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
4,5	4,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
siswati	siswat	k1gMnPc5
<g/>
/	/	kIx~
<g/>
svazijština	svazijština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
2,5	2,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tsivendština	tsivendština	k1gFnSc1
(	(	kIx(
<g/>
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
jižní	jižní	k2eAgFnSc1d1
ndbelština	ndbelština	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
2	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
jazyky	jazyk	k1gInPc4
jsou	být	k5eAaImIp3nP
formálně	formálně	k6eAd1
rovnocenné	rovnocenný	k2eAgInPc1d1
<g/>
,	,	kIx,
nápisy	nápis	k1gInPc1
v	v	k7c6
těchto	tento	k3xDgInPc6
jazycích	jazyk	k1gInPc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
např.	např.	kA
na	na	k7c6
jihoafrických	jihoafrický	k2eAgFnPc6d1
mincích	mince	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
komunikace	komunikace	k1gFnSc1
však	však	k9
probíhá	probíhat	k5eAaImIp3nS
ve	v	k7c6
třech	tři	k4xCgInPc6
nejrozšířenějších	rozšířený	k2eAgInPc6d3
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
zulština	zulština	k1gFnSc1
<g/>
,	,	kIx,
xhoština	xhoština	k1gFnSc1
a	a	k8xC
afrikánština	afrikánština	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
zejména	zejména	k9
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
jako	jako	k9
jazyk	jazyk	k1gInSc4
obchodu	obchod	k1gInSc2
a	a	k8xC
vzdělání	vzdělání	k1gNnSc2
v	v	k7c4
JAR	jar	k1gInSc4
dominantní	dominantní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
také	také	k9
nejrozšířenějším	rozšířený	k2eAgNnSc7d3
druhým	druhý	k4xOgInSc7
jazykem	jazyk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angličtina	angličtina	k1gFnSc1
v	v	k7c4
JAR	jar	k1gInSc4
tak	tak	k6eAd1
obecně	obecně	k6eAd1
plní	plnit	k5eAaImIp3nS
úlohu	úloha	k1gFnSc4
lingua	linguus	k1gMnSc2
franca	francus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
čínskou	čínský	k2eAgFnSc7d1
expanzí	expanze	k1gFnSc7
do	do	k7c2
Afriky	Afrika	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
začíná	začínat	k5eAaImIp3nS
prosazovat	prosazovat	k5eAaImF
i	i	k9
čínština	čínština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1
Jihoafričané	Jihoafričan	k1gMnPc1
mohou	moct	k5eAaImIp3nP
také	také	k9
hovořit	hovořit	k5eAaImF
evropskými	evropský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
jako	jako	k8xS,k8xC
italština	italština	k1gFnSc1
<g/>
,	,	kIx,
portugalština	portugalština	k1gFnSc1
(	(	kIx(
<g/>
používaná	používaný	k2eAgFnSc1d1
i	i	k8xC
černými	černý	k2eAgFnPc7d1
Angolány	Angolán	k2eAgMnPc4d1
a	a	k8xC
Mozambičany	Mozambičan	k1gMnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nizozemština	nizozemština	k1gFnSc1
<g/>
,	,	kIx,
němčina	němčina	k1gFnSc1
a	a	k8xC
řečtina	řečtina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
neoficiálních	oficiální	k2eNgInPc2d1,k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
jsou	být	k5eAaImIp3nP
v	v	k7c4
JAR	jar	k1gFnSc4
také	také	k9
dále	daleko	k6eAd2
hojně	hojně	k6eAd1
rozšířeny	rozšířen	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
přístavních	přístavní	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
gudžarátština	gudžarátština	k1gFnSc1
<g/>
,	,	kIx,
hindština	hindština	k1gFnSc1
<g/>
,	,	kIx,
tamilština	tamilština	k1gFnSc1
<g/>
,	,	kIx,
telugština	telugština	k1gFnSc1
a	a	k8xC
urdština	urdština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzsky	francouzsky	k6eAd1
mluví	mluvit	k5eAaImIp3nS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
migranti	migrant	k1gMnPc1
z	z	k7c2
frankofonní	frankofonní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Náboženství	náboženství	k1gNnSc1
v	v	k7c6
Jížní	Jížní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
víra	víra	k1gFnSc1
</s>
<s>
procent	procento	k1gNnPc2
</s>
<s>
Protestantismus	protestantismus	k1gInSc1
</s>
<s>
73,2	73,2	k4
%	%	kIx~
</s>
<s>
Bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
</s>
<s>
14,9	14,9	k4
%	%	kIx~
</s>
<s>
Katolicismus	katolicismus	k1gInSc1
</s>
<s>
7,4	7,4	k4
%	%	kIx~
</s>
<s>
Islám	islám	k1gInSc1
</s>
<s>
1,7	1,7	k4
%	%	kIx~
</s>
<s>
Hinduismus	hinduismus	k1gInSc1
</s>
<s>
1,1	1,1	k4
%	%	kIx~
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
1,7	1,7	k4
%	%	kIx~
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
křesťané	křesťan	k1gMnPc1
79,8	79,8	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
členy	člen	k1gMnPc7
různých	různý	k2eAgFnPc2d1
protestantských	protestantský	k2eAgFnPc2d1
denominací	denominace	k1gFnPc2
(	(	kIx(
<g/>
široce	široko	k6eAd1
definovaných	definovaný	k2eAgInPc2d1
tak	tak	k8xS,k8xC
<g/>
,	,	kIx,
že	že	k8xS
zahrnují	zahrnovat	k5eAaImIp3nP
synkretické	synkretický	k2eAgFnPc1d1
africké	africký	k2eAgFnPc1d1
církve	církev	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
menšinu	menšina	k1gFnSc4
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
křesťanské	křesťanský	k2eAgNnSc4d1
vyznání	vyznání	k1gNnSc4
patří	patřit	k5eAaImIp3nS
Sionská	sionský	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
(	(	kIx(
<g/>
11,1	11,1	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Letniční	letniční	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
Charismatické	charismatický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
8,2	8,2	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolictví	katolictví	k1gNnSc1
(	(	kIx(
<g/>
7,1	7,1	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Metodismus	metodismus	k1gInSc1
(	(	kIx(
<g/>
6,8	6,8	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
holandská	holandský	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
(	(	kIx(
<g/>
Nederduits	Nederduits	k1gInSc1
Gereformeerde	Gereformeerd	k1gInSc5
Kerk	Kerk	k1gMnSc1
<g/>
;	;	kIx,
6,7	6,7	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Anglikánství	anglikánství	k1gNnSc4
(	(	kIx(
<g/>
3,8	3,8	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
zbývajících	zbývající	k2eAgFnPc2d1
křesťanských	křesťanský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
představovali	představovat	k5eAaImAgMnP
dalších	další	k2eAgInPc2d1
36	#num#	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimové	muslim	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
1,5	1,5	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
hinduisté	hinduista	k1gMnPc1
1,2	1,2	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
tradiční	tradiční	k2eAgNnPc4d1
africká	africký	k2eAgNnPc4d1
náboženství	náboženství	k1gNnPc4
0,3	0,3	k4
<g/>
%	%	kIx~
a	a	k8xC
judaismus	judaismus	k1gInSc1
0,2	0,2	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
15,1	15,1	k4
<g/>
%	%	kIx~
se	se	k3xPyFc4
k	k	k7c3
žádné	žádný	k3yNgFnSc3
náboženské	náboženský	k2eAgFnSc3d1
příslušnosti	příslušnost	k1gFnSc3
nepřihlásilo	přihlásit	k5eNaPmAgNnS
<g/>
,	,	kIx,
0,6	0,6	k4
<g/>
%	%	kIx~
bylo	být	k5eAaImAgNnS
„	„	k?
<g/>
ostatních	ostatní	k2eAgInPc2d1
<g/>
“	“	k?
a	a	k8xC
1,4	1,4	k4
<g/>
%	%	kIx~
bylo	být	k5eAaImAgNnS
„	„	k?
<g/>
nespecifikovaných	specifikovaný	k2eNgInPc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgMnSc1d3
z	z	k7c2
křesťanských	křesťanský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
tvořily	tvořit	k5eAaImAgFnP
africké	africký	k2eAgFnPc1d1
církve	církev	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
že	že	k9
mnoho	mnoho	k4c1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
neprohlásily	prohlásit	k5eNaPmAgFnP
žádnou	žádný	k3yNgFnSc4
příslušnost	příslušnost	k1gFnSc4
k	k	k7c3
jakémukoli	jakýkoli	k3yIgNnSc3
organizovanému	organizovaný	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
<g/>
,	,	kIx,
dodržovalo	dodržovat	k5eAaImAgNnS
tradiční	tradiční	k2eAgNnPc1d1
africká	africký	k2eAgNnPc1d1
náboženství	náboženství	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
je	být	k5eAaImIp3nS
200	#num#	k4
000	#num#	k4
domorodých	domorodý	k2eAgMnPc2d1
tradičních	tradiční	k2eAgMnPc2d1
léčitelů	léčitel	k1gMnPc2
a	a	k8xC
až	až	k9
60	#num#	k4
<g/>
%	%	kIx~
Jihoafričanů	Jihoafričan	k1gMnPc2
se	se	k3xPyFc4
s	s	k7c7
těmito	tento	k3xDgMnPc7
léčiteli	léčitel	k1gMnPc7
radí	radit	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
obecně	obecně	k6eAd1
nazývaných	nazývaný	k2eAgFnPc2d1
sangomas	sangomasa	k1gFnPc2
nebo	nebo	k8xC
inyangas	inyangasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
léčitelé	léčitel	k1gMnPc1
používají	používat	k5eAaImIp3nP
k	k	k7c3
usnadnění	usnadnění	k1gNnSc3
uzdravení	uzdravení	k1gNnSc2
klientů	klient	k1gMnPc2
kombinaci	kombinace	k1gFnSc3
duchovních	duchovní	k2eAgInPc2d1
přesvědčení	přesvědčení	k1gNnSc4
předků	předek	k1gMnPc2
a	a	k8xC
přesvědčení	přesvědčení	k1gNnSc2
o	o	k7c6
duchovních	duchovní	k2eAgFnPc6d1
a	a	k8xC
léčivých	léčivý	k2eAgFnPc6d1
vlastnostech	vlastnost	k1gFnPc6
místní	místní	k2eAgFnSc2d1
fauny	fauna	k1gFnSc2
a	a	k8xC
flóry	flóra	k1gFnSc2
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
známých	známý	k2eAgNnPc2d1
jako	jako	k8xC,k8xS
muti	muti	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
lidí	člověk	k1gMnPc2
má	mít	k5eAaImIp3nS
synkretické	synkretický	k2eAgFnPc4d1
náboženské	náboženský	k2eAgFnPc4d1
praktiky	praktika	k1gFnPc4
kombinující	kombinující	k2eAgFnSc2d1
křesťanské	křesťanský	k2eAgFnSc2d1
a	a	k8xC
domorodé	domorodý	k2eAgInPc4d1
vlivy	vliv	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jihoafričtí	jihoafrický	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
pochází	pocházet	k5eAaImIp3nP
především	především	k9
z	z	k7c2
řad	řada	k1gFnPc2
barevných	barevný	k2eAgFnPc2d1
a	a	k8xC
z	z	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
popisováni	popisovat	k5eAaImNgMnP
jako	jako	k9
Indové	Ind	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
černí	černý	k2eAgMnPc1d1
nebo	nebo	k8xC
bílí	bílý	k2eAgMnPc1d1
jihoafričtí	jihoafrický	k2eAgMnPc1d1
konvertité	konvertita	k1gMnPc1
i	i	k8xC
další	další	k2eAgFnPc1d1
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
částí	část	k1gFnPc2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
Jihoafričtí	jihoafrický	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
víra	víra	k1gFnSc1
je	být	k5eAaImIp3nS
nejrychleji	rychle	k6eAd3
rostoucím	rostoucí	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
konvertitů	konvertita	k1gMnPc2
v	v	k7c4
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
počet	počet	k1gInSc1
černých	černý	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
vzrostl	vzrůst	k5eAaPmAgInS
šestinásobně	šestinásobně	k6eAd1
<g/>
,	,	kIx,
z	z	k7c2
12	#num#	k4
000	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
na	na	k7c4
74	#num#	k4
700	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
domovem	domov	k1gInSc7
značné	značný	k2eAgFnSc2d1
židovské	židovský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
dorazila	dorazit	k5eAaPmAgFnS
jako	jako	k9
menšina	menšina	k1gFnSc1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
evropskými	evropský	k2eAgMnPc7d1
osadníky	osadník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
populace	populace	k1gFnSc1
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
počtem	počet	k1gInSc7
120	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
dnes	dnes	k6eAd1
jich	on	k3xPp3gMnPc2
zůstalo	zůstat	k5eAaPmAgNnS
jen	jen	k9
asi	asi	k9
67	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
emigroval	emigrovat	k5eAaBmAgInS
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
do	do	k7c2
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
tato	tento	k3xDgNnPc1
čísla	číslo	k1gNnPc1
činí	činit	k5eAaImIp3nP
židovskou	židovský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
dvanáctou	dvanáctý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hinduismus	hinduismus	k1gInSc4
vyznávají	vyznávat	k5eAaImIp3nP
jihoafričtí	jihoafrický	k2eAgMnPc1d1
Indové	Ind	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
z	z	k7c2
Indického	indický	k2eAgInSc2d1
subkontinentu	subkontinent	k1gInSc2
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přiváželi	přivážet	k5eAaImAgMnP
Nizozemci	Nizozemec	k1gMnPc1
do	do	k7c2
jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
jako	jako	k8xC,k8xS
otroky	otrok	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1
náboženskou	náboženský	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
byl	být	k5eAaImAgInS
Desmond	Desmond	k1gInSc1
Tutu	tut	k1gInSc2
<g/>
,	,	kIx,
anglikánský	anglikánský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
proslul	proslout	k5eAaPmAgMnS
jako	jako	k9
oponent	oponent	k1gMnSc1
apartheidu	apartheid	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
druhým	druhý	k4xOgMnSc7
Jihoafričanem	Jihoafričan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
obdržel	obdržet	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zdravotnictví	zdravotnictví	k1gNnSc1
</s>
<s>
Problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
<g/>
;	;	kIx,
počet	počet	k1gInSc1
nakažených	nakažený	k2eAgMnPc2d1
dnes	dnes	k6eAd1
dosahuje	dosahovat	k5eAaImIp3nS
5,2	5,2	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vyvinula	vyvinout	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
projektů	projekt	k1gInPc2
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
ale	ale	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
nemají	mít	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
organizace	organizace	k1gFnSc2
Human	Human	k1gInSc1
Rights	Rights	k1gInSc1
Watch	Watcha	k1gFnPc2
se	se	k3xPyFc4
Jihoafričanky	Jihoafričanka	k1gFnPc1
bojí	bát	k5eAaImIp3nP
rodit	rodit	k5eAaImF
ve	v	k7c6
státních	státní	k2eAgFnPc6d1
nemocnicích	nemocnice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
JAR	jar	k1gFnSc4
v	v	k7c6
letech	léto	k1gNnPc6
1998	#num#	k4
až	až	k9
2007	#num#	k4
stoupla	stoupnout	k5eAaPmAgFnS
úmrtnost	úmrtnost	k1gFnSc1
rodiček	rodička	k1gFnPc2
čtyřnásobně	čtyřnásobně	k6eAd1
<g/>
,	,	kIx,
ze	z	k7c2
150	#num#	k4
na	na	k7c4
625	#num#	k4
na	na	k7c4
sto	sto	k4xCgNnSc4
tisíc	tisíc	k4xCgInPc2
narozených	narozený	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
JAR	jar	k1gInSc1
tak	tak	k6eAd1
nemá	mít	k5eNaImIp3nS
šanci	šance	k1gFnSc4
splnit	splnit	k5eAaPmF
cíl	cíl	k1gInSc4
rozvoje	rozvoj	k1gInSc2
stanovený	stanovený	k2eAgInSc4d1
OSN	OSN	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
předpokládá	předpokládat	k5eAaImIp3nS
snížení	snížení	k1gNnSc4
úmrtnosti	úmrtnost	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
na	na	k7c4
38	#num#	k4
na	na	k7c6
každých	každý	k3xTgInPc2
sto	sto	k4xCgNnSc4
tisíc	tisíc	k4xCgInPc2
porodů	porod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
Střední	střední	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
71	#num#	k4
let	léto	k1gNnPc2
pro	pro	k7c4
bílé	bílý	k2eAgMnPc4d1
Jihoafričany	Jihoafričan	k1gMnPc4
a	a	k8xC
48	#num#	k4
lety	let	k1gInPc7
pro	pro	k7c4
černé	černý	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kriminalita	kriminalita	k1gFnSc1
</s>
<s>
Cedule	cedule	k1gFnSc1
v	v	k7c4
JAR	jar	k1gFnSc4
varující	varující	k2eAgFnSc4d1
před	před	k7c7
únosy	únos	k1gInPc7
</s>
<s>
Míra	Míra	k1gFnSc1
vražd	vražda	k1gFnPc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnoty	hodnota	k1gFnSc2
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jsou	být	k5eAaImIp3nP
pravděpodobně	pravděpodobně	k6eAd1
podhodnoceny	podhodnotit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kříže	kříž	k1gInPc1
v	v	k7c6
Polokwane	Polokwan	k1gMnSc5
na	na	k7c4
památku	památka	k1gFnSc4
zavražděných	zavražděný	k2eAgMnPc2d1
farmářů	farmář	k1gMnPc2
</s>
<s>
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
pro	pro	k7c4
období	období	k1gNnSc4
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
vypracovaného	vypracovaný	k2eAgNnSc2d1
OSN	OSN	kA
je	být	k5eAaImIp3nS
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
celosvětově	celosvětově	k6eAd1
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
ve	v	k7c6
vraždách	vražda	k1gFnPc6
a	a	k8xC
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c4
přepadení	přepadení	k1gNnSc4
a	a	k8xC
znásilnění	znásilnění	k1gNnSc4
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
obyvatele	obyvatel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c4
JAR	jar	k1gInSc4
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc4
den	den	k1gInSc4
zavražděno	zavraždit	k5eAaPmNgNnS
50	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
Nahlášený	nahlášený	k2eAgInSc1d1
počet	počet	k1gInSc1
znásilnění	znásilnění	k1gNnSc2
ročně	ročně	k6eAd1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
40	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
znásilnění	znásilnění	k1gNnSc2
odhadoval	odhadovat	k5eAaImAgInS
na	na	k7c4
500	#num#	k4
000	#num#	k4
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znásilnění	znásilnění	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
častým	častý	k2eAgInSc7d1
problémem	problém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
třetina	třetina	k1gFnSc1
z	z	k7c2
4000	#num#	k4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
jednom	jeden	k4xCgInSc6
průzkumu	průzkum	k1gInSc6
dotazovány	dotazovat	k5eAaImNgInP
<g/>
,	,	kIx,
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
uplynulém	uplynulý	k2eAgInSc6d1
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
znásilněna	znásilněn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
je	být	k5eAaImIp3nS
zemí	zem	k1gFnSc7
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
výskytem	výskyt	k1gInSc7
dětských	dětský	k2eAgNnPc2d1
a	a	k8xC
kojeneckých	kojenecký	k2eAgNnPc2d1
znásilnění	znásilnění	k1gNnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násilníkům	násilník	k1gMnPc3
hrozí	hrozit	k5eAaImIp3nP
dva	dva	k4xCgInPc1
roky	rok	k1gInPc1
za	za	k7c7
mřížemi	mříž	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
Nebezpečná	bezpečný	k2eNgFnSc1d1
pověra	pověra	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
sex	sex	k1gInSc4
s	s	k7c7
pannou	panna	k1gFnSc7
léčí	léčit	k5eAaImIp3nP
AIDS	aids	k1gInSc4
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
ke	k	k7c3
znásilňování	znásilňování	k1gNnSc3
nezletilých	zletilý	k2eNgFnPc2d1,k2eAgFnPc2d1
dívek	dívka	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
znásilněno	znásilnit	k5eAaPmNgNnS
až	až	k9
200	#num#	k4
000	#num#	k4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnoho	mnoho	k4c1
(	(	kIx(
<g/>
zejména	zejména	k6eAd1
majetných	majetný	k2eAgMnPc2d1
<g/>
)	)	kIx)
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
proto	proto	k8xC
stahuje	stahovat	k5eAaImIp3nS
do	do	k7c2
bezpečnějších	bezpečný	k2eAgFnPc2d2
čtvrtí	čtvrt	k1gFnPc2
a	a	k8xC
střežených	střežený	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
gated	gated	k1gMnSc1
communities	communities	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
Spousta	spousta	k1gFnSc1
emigrantů	emigrant	k1gMnPc2
z	z	k7c2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
také	také	k9
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zločin	zločin	k1gMnSc1
byl	být	k5eAaImAgMnS
rozhodujícím	rozhodující	k2eAgMnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
důvodem	důvod	k1gInSc7
pro	pro	k7c4
jejich	jejich	k3xOp3gInSc4
odchod	odchod	k1gInSc4
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
Velkým	velký	k2eAgInSc7d1
problémem	problém	k1gInSc7
jsou	být	k5eAaImIp3nP
nadále	nadále	k6eAd1
i	i	k9
vraždy	vražda	k1gFnPc1
farmářů	farmář	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
jako	jako	k9
výsledek	výsledek	k1gInSc4
nerovného	rovný	k2eNgInSc2d1
majetkovému	majetkový	k2eAgNnSc3d1
uspořádání	uspořádání	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přes	přes	k7c4
rozšířené	rozšířený	k2eAgInPc4d1
narativy	narativ	k1gInPc4
žádná	žádný	k3yNgFnSc1
průkazná	průkazný	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
nenaznačuje	naznačovat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
tyto	tento	k3xDgMnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
útoky	útok	k1gInPc7
disproporčně	disproporčně	k6eAd1
postihovaly	postihovat	k5eAaImAgInP
více	hodně	k6eAd2
bělošské	bělošský	k2eAgMnPc4d1
farmáře	farmář	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
jihoafrické	jihoafrický	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
a	a	k8xC
analytických	analytický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
jsou	být	k5eAaImIp3nP
vraždy	vražda	k1gFnPc4
farmářů	farmář	k1gMnPc2
součástí	součást	k1gFnPc2
širší	široký	k2eAgNnSc4d2
kriminální	kriminální	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
problematiky	problematika	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
nemají	mít	k5eNaImIp3nP
prý	prý	k9
rasový	rasový	k2eAgInSc4d1
podtext	podtext	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
organizace	organizace	k1gFnSc1
Genocide	Genocid	k1gInSc5
Watch	Watch	k1gInSc1
teoretizovala	teoretizovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
útoky	útok	k1gInPc1
na	na	k7c4
farmy	farma	k1gFnPc4
představují	představovat	k5eAaImIp3nP
včasné	včasný	k2eAgInPc1d1
varovné	varovný	k2eAgInPc1d1
příznaky	příznak	k1gInPc1
genocidy	genocida	k1gFnSc2
Afrikánců	Afrikánec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrickou	jihoafrický	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
kritizovala	kritizovat	k5eAaImAgFnS
za	za	k7c4
nečinnost	nečinnost	k1gFnSc4
a	a	k8xC
upozornila	upozornit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
byli	být	k5eAaImAgMnP
„	„	k?
<g/>
farmáři	farmář	k1gMnPc1
evropského	evropský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
ve	v	k7c6
zprávě	zpráva	k1gFnSc6
byli	být	k5eAaImAgMnP
zahrnuti	zahrnut	k2eAgMnPc1d1
i	i	k8xC
neafrikánští	afrikánský	k2eNgMnPc1d1
farmáři	farmář	k1gMnPc1
evropského	evropský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
)	)	kIx)
vražděni	vraždit	k5eAaImNgMnP
ve	v	k7c6
čtyřikrát	čtyřikrát	k6eAd1
větších	veliký	k2eAgInPc6d2
počtech	počet	k1gInPc6
než	než	k8xS
v	v	k7c6
jihoafrické	jihoafrický	k2eAgFnSc6d1
populaci	populace	k1gFnSc6
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
bylo	být	k5eAaImAgNnS
68	#num#	k4
606	#num#	k4
ze	z	k7c2
749	#num#	k4
637	#num#	k4
lidí	člověk	k1gMnPc2
v	v	k7c6
odvětví	odvětví	k1gNnSc6
zemědělství	zemědělství	k1gNnSc2
a	a	k8xC
myslivosti	myslivost	k1gFnSc2
bělochů	běloch	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
bylo	být	k5eAaImAgNnS
při	při	k7c6
tisících	tisíc	k4xCgInPc6,k4xOgInPc6
farmářských	farmářský	k2eAgInPc6d1
útoků	útok	k1gInPc2
zavražděno	zavraždit	k5eAaPmNgNnS
téměř	téměř	k6eAd1
3	#num#	k4
000	#num#	k4
zemědělců	zemědělec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
(	(	kIx(
<g/>
legálně	legálně	k6eAd1
i	i	k9
ilegálně	ilegálně	k6eAd1
<g/>
)	)	kIx)
držených	držený	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
a	a	k8xC
problémy	problém	k1gInPc4
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
regulací	regulace	k1gFnSc7
–	–	k?
země	země	k1gFnSc1
tak	tak	k6eAd1
drží	držet	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
počtu	počet	k1gInSc6
vražd	vražda	k1gFnPc2
a	a	k8xC
zabití	zabití	k1gNnSc1
způsobených	způsobený	k2eAgFnPc2d1
střelnými	střelný	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
na	na	k7c4
100	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
hned	hned	k6eAd1
za	za	k7c7
Kolumbií	Kolumbie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
Denně	denně	k6eAd1
je	být	k5eAaImIp3nS
střelnou	střelný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
zabito	zabít	k5eAaPmNgNnS
v	v	k7c6
průměru	průměr	k1gInSc6
18	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nepříjemně	příjemně	k6eNd1
to	ten	k3xDgNnSc4
dokazují	dokazovat	k5eAaImIp3nP
informace	informace	k1gFnPc4
přímo	přímo	k6eAd1
od	od	k7c2
policie	policie	k1gFnSc2
z	z	k7c2
JAR	jaro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
den	den	k1gInSc4
umírá	umírat	k5eAaImIp3nS
v	v	k7c4
JAR	jar	k1gInSc4
průměrně	průměrně	k6eAd1
jeden	jeden	k4xCgMnSc1
až	až	k8xS
dva	dva	k4xCgMnPc1
policisté	policista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgMnSc7d1
z	z	k7c2
problémů	problém	k1gInPc2
jsou	být	k5eAaImIp3nP
únosy	únos	k1gInPc1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
179	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Nadine	Nadinout	k5eAaPmIp3nS
Gordimerová	Gordimerová	k1gFnSc1
</s>
<s>
Nadine	Nadinout	k5eAaPmIp3nS
Gordimerová	Gordimerový	k2eAgFnSc1d1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
literaturu	literatura	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
se	se	k3xPyFc4
stejné	stejný	k2eAgFnSc2d1
pocty	pocta	k1gFnSc2
dostalo	dostat	k5eAaPmAgNnS
jihoafrickému	jihoafrický	k2eAgMnSc3d1
rodákovi	rodák	k1gMnSc3
Johnu	John	k1gMnSc3
Maxwellu	maxwell	k1gInSc2
Coetzeeovi	Coetzeeus	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prozaik	prozaik	k1gMnSc1
Peter	Peter	k1gMnSc1
Abrahams	Abrahams	k1gInSc4
působil	působit	k5eAaImAgMnS
na	na	k7c6
Jamajce	Jamajka	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gFnPc2
knih	kniha	k1gFnPc2
je	být	k5eAaImIp3nS
zpracováním	zpracování	k1gNnSc7
vzpomínek	vzpomínka	k1gFnPc2
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosadil	prosadit	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
spisovatel	spisovatel	k1gMnSc1
Alan	Alan	k1gMnSc1
Paton	Paton	k1gMnSc1
<g/>
,	,	kIx,
k	k	k7c3
nejznámějším	známý	k2eAgFnPc3d3
jeho	jeho	k3xOp3gNnSc4
knihám	kniha	k1gFnPc3
patří	patřit	k5eAaImIp3nS
Cry	Cry	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Beloved	Beloved	k1gMnSc1
Country	country	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
i	i	k9
dvakrát	dvakrát	k6eAd1
zfilmována	zfilmován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
André	André	k1gMnSc1
Brink	Brink	k1gMnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
těm	ten	k3xDgMnPc3
autorům	autor	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
krom	krom	k7c2
angličtiny	angličtina	k1gFnSc2
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
afrikánštinu	afrikánština	k1gFnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
básník	básník	k1gMnSc1
Breyten	Breyten	k2eAgInSc4d1
Breytenbach	Breytenbach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Zambii	Zambie	k1gFnSc6
<g/>
,	,	kIx,
jihoafrickým	jihoafrický	k2eAgMnSc7d1
spisovatelem	spisovatel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Wilbur	Wilbur	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
dobrodružných	dobrodružný	k2eAgInPc2d1
bestsellerů	bestseller	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fotograf	fotograf	k1gMnSc1
Kevin	Kevin	k1gMnSc1
Carter	Carter	k1gMnSc1
získal	získat	k5eAaPmAgMnS
za	za	k7c4
zdokumentování	zdokumentování	k1gNnSc4
hladomoru	hladomor	k1gInSc2
v	v	k7c6
Súdánu	Súdán	k1gInSc6
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gInSc7
skupiny	skupina	k1gFnSc2
fotožurnalistů	fotožurnalista	k1gMnPc2
známé	známý	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
Bang-Bang	Bang-Bang	k1gInSc1
Club	club	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejznámějším	známý	k2eAgMnPc3d3
režisérům	režisér	k1gMnPc3
patří	patřit	k5eAaImIp3nP
Neill	Neill	k1gMnSc1
Blomkamp	Blomkamp	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamie	Jamie	k1gFnSc1
Uys	Uys	k1gFnSc2
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
snímkem	snímek	k1gInSc7
Bohové	bůh	k1gMnPc1
musejí	muset	k5eAaImIp3nP
být	být	k5eAaImF
šílení	šílený	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejslavnější	slavný	k2eAgFnSc7d3
jihoafrickou	jihoafrický	k2eAgFnSc7d1
herečkou	herečka	k1gFnSc7
je	být	k5eAaImIp3nS
Charlize	Charlize	k1gFnSc1
Theronová	Theronová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc4
Mumie	mumie	k1gFnSc2
proslavil	proslavit	k5eAaPmAgMnS
herce	herec	k1gMnSc4
Arnolda	Arnold	k1gMnSc4
Voslooa	Vosloous	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3
jihoafrickou	jihoafrický	k2eAgFnSc7d1
zpěvačkou	zpěvačka	k1gFnSc7
je	být	k5eAaImIp3nS
Miriam	Miriam	k1gFnSc1
Makeba	Makeba	k1gFnSc1
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgFnSc1d1
též	též	k9
Mama	mama	k1gFnSc1
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
zpěvačkou	zpěvačka	k1gFnSc7
černé	černý	k2eAgFnSc2d1
pleti	pleť	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
obdržela	obdržet	k5eAaPmAgFnS
americkou	americký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Grammy	Gramma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
zpěvák	zpěvák	k1gMnSc1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
Troye	Troye	k1gFnSc7
Sivan	Sivana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Věda	věda	k1gFnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Shuttleworth	Shuttleworth	k1gMnSc1
v	v	k7c6
kosmu	kosmos	k1gInSc6
</s>
<s>
Christiaan	Christiaan	k1gMnSc1
Barnard	Barnard	k1gMnSc1
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
má	mít	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
největší	veliký	k2eAgFnSc4d3
vědeckou	vědecký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
svědčí	svědčit	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
řady	řada	k1gFnSc2
jejích	její	k3xOp3gMnPc2
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
byť	byť	k8xS
mnozí	mnohý	k2eAgMnPc1d1
se	se	k3xPyFc4
prosadili	prosadit	k5eAaPmAgMnP
v	v	k7c6
institucích	instituce	k1gFnPc6
v	v	k7c6
Británii	Británie	k1gFnSc6
či	či	k8xC
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
:	:	kIx,
Max	Max	k1gMnSc1
Theiler	Theiler	k1gMnSc1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
Nobelovou	Nobelová	k1gFnSc4
cenu	cena	k1gFnSc4
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
a	a	k8xC
lékařství	lékařství	k1gNnSc4
za	za	k7c4
vyvinutí	vyvinutí	k1gNnSc4
vakcíny	vakcína	k1gFnSc2
proti	proti	k7c3
žluté	žlutý	k2eAgFnSc3d1
zimnici	zimnice	k1gFnSc3
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgMnSc7
nositelem	nositel	k1gMnSc7
této	tento	k3xDgFnSc2
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
lékařství	lékařství	k1gNnSc4
pro	pro	k7c4
Jihoafričana	Jihoafričan	k1gMnSc4
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
převzal	převzít	k5eAaPmAgMnS
Allan	Allan	k1gMnSc1
McLeod	McLeoda	k1gFnPc2
Cormack	Cormack	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
položil	položit	k5eAaPmAgMnS
teoretické	teoretický	k2eAgInPc4d1
základy	základ	k1gInPc4
pro	pro	k7c4
zkonstruování	zkonstruování	k1gNnSc4
výpočetního	výpočetní	k2eAgInSc2d1
tomografu	tomograf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
stejnou	stejný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
obdržel	obdržet	k5eAaPmAgInS
Sydney	Sydney	k1gNnSc4
Brenner	Brennero	k1gNnPc2
<g/>
,	,	kIx,
především	především	k9
za	za	k7c4
výzkum	výzkum	k1gInSc4
genetického	genetický	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jihoafrický	jihoafrický	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
Christiaan	Christiaan	k1gMnSc1
Barnard	Barnard	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
na	na	k7c6
světě	svět	k1gInSc6
úspěšnou	úspěšný	k2eAgFnSc4d1
transplantaci	transplantace	k1gFnSc4
srdce	srdce	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
nikoli	nikoli	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
doma	doma	k6eAd1
-	-	kIx~
v	v	k7c6
Groote	Groot	k1gInSc5
Schuur	Schuur	k1gMnSc1
Hospital	Hospital	k1gMnSc1
v	v	k7c6
Kapském	kapský	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informatik	informatik	k1gMnSc1
Seymour	Seymour	k1gMnSc1
Papert	Papert	k1gMnSc1
byl	být	k5eAaImAgMnS
průkopníkem	průkopník	k1gMnSc7
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
a	a	k8xC
tvůrcem	tvůrce	k1gMnSc7
programovacího	programovací	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
Logo	logo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antropolog	antropolog	k1gMnSc1
a	a	k8xC
paleontolog	paleontolog	k1gMnSc1
Raymond	Raymond	k1gMnSc1
Dart	Dart	k1gMnSc1
proslul	proslout	k5eAaPmAgMnS
jako	jako	k9
objevitel	objevitel	k1gMnSc1
tzv.	tzv.	kA
Taungského	Taungský	k2eAgNnSc2d1
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
první	první	k4xOgFnSc2
známé	známý	k2eAgFnSc2d1
lebky	lebka	k1gFnSc2
hominidů	hominid	k1gMnPc2
rodu	rod	k1gInSc2
Australopithecus	Australopithecus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objev	objev	k1gInSc1
znamenal	znamenat	k5eAaImAgInS
průlom	průlom	k1gInSc4
a	a	k8xC
základ	základ	k1gInSc4
teorie	teorie	k1gFnSc2
o	o	k7c6
původu	původ	k1gInSc6
lidstva	lidstvo	k1gNnSc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
dnes	dnes	k6eAd1
většina	většina	k1gFnSc1
vědecké	vědecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
prokázaný	prokázaný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Dartův	Dartův	k2eAgInSc4d1
objev	objev	k1gInSc4
vykopávkami	vykopávka	k1gFnPc7
navazoval	navazovat	k5eAaImAgMnS
zejména	zejména	k9
Robert	Robert	k1gMnSc1
Broom	Broom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
JAR	Jara	k1gFnPc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zakladatel	zakladatel	k1gMnSc1
mykologie	mykologie	k1gFnSc2
Christiaan	Christiaan	k1gMnSc1
Hendrik	Hendrik	k1gMnSc1
Persoon	Persoon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Southern	Southern	k1gNnSc1
African	Africana	k1gFnPc2
Large	Large	k1gNnSc2
Telescope	Telescop	k1gInSc5
je	být	k5eAaImIp3nS
největším	veliký	k2eAgInSc7d3
teleskopem	teleskop	k1gInSc7
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
měla	mít	k5eAaImAgFnS
JAR	jar	k1gFnSc1
jediná	jediný	k2eAgFnSc1d1
z	z	k7c2
afrických	africký	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
i	i	k9
vlastní	vlastní	k2eAgInSc4d1
jaderný	jaderný	k2eAgInSc4d1
program	program	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
africkou	africký	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
dokázala	dokázat	k5eAaPmAgFnS
vyrobit	vyrobit	k5eAaPmF
jadernou	jaderný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svého	svůj	k3xOyFgInSc2
jaderného	jaderný	k2eAgInSc2d1
arzenálu	arzenál	k1gInSc2
se	se	k3xPyFc4
však	však	k9
na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vzdala	vzdát	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Kuchyně	kuchyně	k1gFnSc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
rozmanitá	rozmanitý	k2eAgFnSc1d1
<g/>
;	;	kIx,
na	na	k7c6
jídlech	jídlo	k1gNnPc6
z	z	k7c2
mnoha	mnoho	k4c3
kultur	kultura	k1gFnPc2
si	se	k3xPyFc3
pochutnávají	pochutnávat	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
turisti	turist	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
chtějí	chtít	k5eAaImIp3nP
ochutnat	ochutnat	k5eAaPmF
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
dostupných	dostupný	k2eAgNnPc2d1
jídel	jídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
hlavně	hlavně	k9
na	na	k7c4
masu	masa	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
populární	populární	k2eAgNnSc1d1
jihoafrické	jihoafrický	k2eAgNnSc1d1
společenské	společenský	k2eAgNnSc1d1
setkání	setkání	k1gNnSc1
známé	známý	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
braai	braai	k6eAd1
-	-	kIx~
variace	variace	k1gFnSc1
grilování	grilování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
stal	stát	k5eAaPmAgMnS
významný	významný	k2eAgMnSc1d1
producent	producent	k1gMnSc1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
některé	některý	k3yIgFnPc1
z	z	k7c2
nejlepších	dobrý	k2eAgFnPc2d3
vinic	vinice	k1gFnPc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
údolích	údolí	k1gNnPc6
poblíž	poblíž	k7c2
měst	město	k1gNnPc2
Stellenbosch	Stellenboscha	k1gFnPc2
<g/>
,	,	kIx,
Franschhoek	Franschhoky	k1gFnPc2
<g/>
,	,	kIx,
Paarl	Paarlum	k1gNnPc2
a	a	k8xC
Barrydale	Barrydala	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
180	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Wayde	Wayde	k6eAd1
van	van	k1gInSc1
Niekerk	Niekerk	k1gInSc1
</s>
<s>
Gary	Gara	k1gFnPc1
Player	Playra	k1gFnPc2
</s>
<s>
Nejpopulárnějšími	populární	k2eAgInPc7d3
sporty	sport	k1gInPc7
v	v	k7c6
zemi	zem	k1gFnSc6
jsou	být	k5eAaImIp3nP
mezi	mezi	k7c7
bílými	bílý	k2eAgMnPc7d1
Jihoafričany	Jihoafričan	k1gMnPc7
kriket	kriket	k1gInSc4
a	a	k8xC
ragby	ragby	k1gNnSc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
černošskými	černošský	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
pak	pak	k6eAd1
fotbal	fotbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
pořádala	pořádat	k5eAaImAgFnS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc1
africká	africký	k2eAgFnSc1d1
země	země	k1gFnSc1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JAR	jar	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
26	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
i	i	k9
jiné	jiný	k2eAgFnSc2d1
africké	africký	k2eAgFnSc2d1
země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
nejúspěšnější	úspěšný	k2eAgMnSc1d3
v	v	k7c6
atletice	atletika	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nasbírala	nasbírat	k5eAaPmAgFnS
devět	devět	k4xCc4
nejcennějších	cenný	k2eAgInPc2d3
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
vůbec	vůbec	k9
první	první	k4xOgFnSc1
jihoafrická	jihoafrický	k2eAgFnSc1d1
olympijská	olympijský	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
atletická	atletický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlato	zlato	k1gNnSc1
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
metrů	metr	k1gInPc2
získal	získat	k5eAaPmAgInS
Reggie	Reggie	k1gFnSc2
Walker	Walker	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
na	na	k7c6
hrách	hra	k1gFnPc6
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgMnS
maratonský	maratonský	k2eAgInSc4d1
běh	běh	k1gInSc4
Ken	Ken	k1gMnSc1
McArthur	McArthur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
triumfoval	triumfovat	k5eAaBmAgMnS
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
400	#num#	k4
metrů	metr	k1gInPc2
Bevil	Bevil	k1gMnSc1
Rudd	Rudd	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
roku	rok	k1gInSc2
1928	#num#	k4
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
110	#num#	k4
metrů	metr	k1gInPc2
překážek	překážka	k1gFnPc2
Sydney	Sydney	k1gNnSc2
Atkinson	Atkinsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Esther	Esthra	k1gFnPc2
Brandová	Brandový	k2eAgFnSc1d1
vyhrála	vyhrát	k5eAaPmAgFnS
závod	závod	k1gInSc4
ve	v	k7c6
skoku	skok	k1gInSc6
vysokém	vysoký	k2eAgInSc6d1
na	na	k7c6
hrách	hra	k1gFnPc6
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1964	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
jihoafričtí	jihoafrický	k2eAgMnPc1d1
sportovci	sportovec	k1gMnPc1
na	na	k7c4
olympiádu	olympiáda	k1gFnSc4
nemohli	moct	k5eNaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
další	další	k2eAgNnSc1d1
zlato	zlato	k1gNnSc1
přišlo	přijít	k5eAaPmAgNnS
až	až	k9
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
maratónský	maratónský	k2eAgInSc1d1
běh	běh	k1gInSc1
v	v	k7c6
Atlantě	Atlanta	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
Josia	Josius	k1gMnSc2
Thugwane	Thugwan	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Londýně	Londýn	k1gInSc6
2012	#num#	k4
uspěla	uspět	k5eAaPmAgFnS
běžkyně	běžkyně	k1gFnSc1
na	na	k7c4
800	#num#	k4
metrů	metr	k1gInPc2
Caster	Castra	k1gFnPc2
Semenyaová	Semenyaový	k2eAgFnSc1d1
<g/>
,	,	kIx,
byť	byť	k8xS
zlato	zlato	k1gNnSc4
převzala	převzít	k5eAaPmAgFnS
až	až	k9
dodatečně	dodatečně	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
dopingové	dopingový	k2eAgFnSc6d1
diskvalifikaci	diskvalifikace	k1gFnSc6
vítězné	vítězný	k2eAgFnSc2d1
soupeřky	soupeřka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
182	#num#	k4
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
proběhla	proběhnout	k5eAaPmAgFnS
jasně	jasně	k9
první	první	k4xOgMnSc1
také	také	k9
za	za	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
v	v	k7c6
Riu	Riu	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k9
nejúspěšnější	úspěšný	k2eAgInSc1d3
jihoafrickou	jihoafrický	k2eAgFnSc7d1
atletkou	atletka	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
byť	byť	k8xS
jsou	být	k5eAaImIp3nP
její	její	k3xOp3gInPc4
úspěchy	úspěch	k1gInPc4
hodně	hodně	k6eAd1
diskutované	diskutovaný	k2eAgNnSc1d1
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
intersex	intersex	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
183	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
Riu	Riu	k1gFnSc4
sbírku	sbírka	k1gFnSc4
zlatých	zlatá	k1gFnPc2
rozšířil	rozšířit	k5eAaPmAgMnS
i	i	k9
běžec	běžec	k1gMnSc1
na	na	k7c4
400	#num#	k4
metrů	metr	k1gInPc2
Wayde	Wayd	k1gInSc5
Van	vana	k1gFnPc2
Niekerk	Niekerk	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
navíc	navíc	k6eAd1
novým	nový	k2eAgInSc7d1
světovým	světový	k2eAgInSc7d1
rekordem	rekord	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tak	tak	k6eAd1
uzmul	uzmout	k5eAaPmAgInS
legendárnímu	legendární	k2eAgMnSc3d1
Michaelu	Michael	k1gMnSc3
Johnsonovi	Johnson	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
184	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgMnS
doménou	doména	k1gFnSc7
JAR	Jara	k1gFnPc2
olympijský	olympijský	k2eAgInSc4d1
box	box	k1gInSc4
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
zlatá	zlatá	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
tenis	tenis	k1gInSc1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
z	z	k7c2
meziválečné	meziválečný	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
po	po	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
plavání	plavání	k1gNnSc1
(	(	kIx(
<g/>
např.	např.	kA
Penny	penny	k1gFnSc1
Heynsová	Heynsová	k1gFnSc1
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oscar	Oscar	k1gMnSc1
Pistorius	Pistorius	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
první	první	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
paralympijskou	paralympijský	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
záře	záře	k1gFnSc1
však	však	k9
pohasla	pohasnout	k5eAaPmAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
kriminálního	kriminální	k2eAgInSc2d1
činu	čin	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
185	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gary	Gar	k1gMnPc4
Player	Playero	k1gNnPc2
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
neslavnějších	slavný	k2eNgMnPc2d2
golfistů	golfista	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
třetím	třetí	k4xOgMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
dosáhl	dosáhnout	k5eAaPmAgMnS
grand	grand	k1gMnSc1
slamu	slam	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
Automobilový	automobilový	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
Jody	jod	k1gInPc1
Scheckter	Schecktra	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kork	Kork	k1gInSc1
Ballington	Ballington	k1gInSc4
je	být	k5eAaImIp3nS
čtyřnásobným	čtyřnásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
závodech	závod	k1gInPc6
silničních	silniční	k2eAgInPc2d1
motocyklů	motocykl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
titul	titul	k1gInSc4
mají	mít	k5eAaImIp3nP
Jon	Jon	k1gFnPc1
Ekerold	Ekeroldo	k1gNnPc2
a	a	k8xC
Brad	brada	k1gFnPc2
Binder	Binder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boxeři	boxer	k1gMnPc1
Gerrie	Gerrie	k1gFnSc1
Coetzee	Coetzee	k1gFnSc1
a	a	k8xC
Corrie	Corrie	k1gFnSc2
Sanders	Sandersa	k1gFnPc2
byli	být	k5eAaImAgMnP
profesionálními	profesionální	k2eAgMnPc7d1
mistry	mistr	k1gMnPc7
světa	svět	k1gInSc2
v	v	k7c6
těžké	těžký	k2eAgFnSc6d1
váze	váha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
JAR	jar	k1gInSc4
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
úspěšný	úspěšný	k2eAgMnSc1d1
kanadský	kanadský	k2eAgMnSc1d1
basketbalista	basketbalista	k1gMnSc1
Steve	Steve	k1gMnSc1
Nash	Nash	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdec	jezdec	k1gInSc1
na	na	k7c6
horském	horský	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
Greg	Greg	k1gMnSc1
Minnaar	Minnaar	k1gMnSc1
je	být	k5eAaImIp3nS
trojnásobným	trojnásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Team	team	k1gInSc1
Dimension	Dimension	k1gInSc4
Data	datum	k1gNnSc2
je	být	k5eAaImIp3nS
úspěšná	úspěšný	k2eAgFnSc1d1
jihoafrická	jihoafrický	k2eAgFnSc1d1
profesionální	profesionální	k2eAgFnSc1d1
stáj	stáj	k1gFnSc1
v	v	k7c6
silniční	silniční	k2eAgFnSc6d1
cyklistice	cyklistika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenista	tenista	k1gMnSc1
Johan	Johan	k1gMnSc1
Kriek	Kriek	k1gMnSc1
dvakrát	dvakrát	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisté	tenista	k1gMnPc1
JAR	Jara	k1gFnPc2
získali	získat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
pověstnou	pověstný	k2eAgFnSc4d1
„	„	k?
<g/>
salátovou	salátový	k2eAgFnSc4d1
mísu	mísa	k1gFnSc4
<g/>
“	“	k?
Davis	Davis	k1gFnSc3
Cupu	cup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conrad	Conrada	k1gFnPc2
Stoltz	Stoltz	k1gMnSc1
je	být	k5eAaImIp3nS
trojnásobným	trojnásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
triatlonu	triatlon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafričtí	jihoafrický	k2eAgMnPc1d1
ragbisté	ragbista	k1gMnPc1
získali	získat	k5eAaPmAgMnP
tři	tři	k4xCgInPc4
tituly	titul	k1gInPc4
mistrů	mistr	k1gMnPc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
a	a	k8xC
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Danie	Danie	k1gFnSc1
Craven	Cravna	k1gFnPc2
byl	být	k5eAaImAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
Afričan	Afričan	k1gMnSc1
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
ragbyové	ragbyový	k2eAgFnSc2d1
síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
Anketu	anketa	k1gFnSc4
o	o	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
ragbistu	ragbista	k1gMnSc4
světa	svět	k1gInSc2
vyhráli	vyhrát	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
Jihoafričané	Jihoafričan	k1gMnPc1
<g/>
:	:	kIx,
Schalk	Schalk	k1gMnSc1
Burger	Burger	k1gMnSc1
a	a	k8xC
Bryan	Bryan	k1gMnSc1
Habana	Habana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jihoafrický	jihoafrický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
tak	tak	k6eAd1
na	na	k7c4
nátlak	nátlak	k1gInSc4
své	svůj	k3xOyFgFnSc2
vládní	vládní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-02-14	2018-02-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GDP	GDP	kA
per	pero	k1gNnPc2
capita	capitum	k1gNnSc2
<g/>
,	,	kIx,
PPP	PPP	kA
(	(	kIx(
<g/>
current	current	k1gMnSc1
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
$	$	kIx~
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Číselník	číselník	k1gInSc1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
CZEM	CZEM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.czso.cz	www.czso.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jižní	jižní	k2eAgFnSc1d1
Afirka	Afirka	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Atlas	Atlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
netway	netwaa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
LIŠČÁK	LIŠČÁK	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
BOHÁČ	Boháč	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc4
států	stát	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
územních	územní	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
rozšířené	rozšířený	k2eAgNnSc1d1
a	a	k8xC
přepracované	přepracovaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
111	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86918	#num#	k4
<g/>
-	-	kIx~
<g/>
57	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Census	census	k1gInSc1
2011	#num#	k4
<g/>
:	:	kIx,
Census	census	k1gInSc1
in	in	k?
brief	brief	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pretoria	Pretorium	k1gNnPc1
<g/>
:	:	kIx,
Statistics	Statistics	k1gInSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
13	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
621413885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
23	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
South	Southa	k1gFnPc2
Africa	Afric	k1gInSc2
Fast	Fast	k2eAgInSc1d1
Facts	Facts	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SouthAfrica	SouthAfrica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
19	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
African	African	k1gMnSc1
Maritime	Maritim	k1gInSc5
Safety	Safet	k2eAgInPc1d1
Authority	Authorit	k1gInPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
Maritime	Maritim	k1gInSc5
Safety	Safet	k2eAgInPc1d1
Authority	Authorit	k1gInPc7
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Coastline	Coastlin	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Guy	Guy	k1gMnSc1
Arnold	Arnold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesotho	Lesot	k1gMnSc4
<g/>
:	:	kIx,
Year	Year	k1gMnSc1
In	In	k1gMnSc1
Review	Review	k1gMnSc1
1996	#num#	k4
–	–	k?
Britannica	Britannica	k1gMnSc1
Online	Onlin	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SERINO	SERINO	kA
<g/>
,	,	kIx,
Kenichi	Kenichi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
Apartheid	apartheid	k1gInSc4
Haunts	Haunts	k1gInSc1
a	a	k8xC
New	New	k1gFnSc1
Generation	Generation	k1gInSc1
of	of	k?
South	South	k1gInSc1
Africans	Africans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Atlantic	Atlantice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
still	still	k1gMnSc1
a	a	k8xC
chronically	chronicalla	k1gMnSc2
racially	racialla	k1gFnSc2
divided	divided	k1gMnSc1
nation	nation	k1gInSc1
<g/>
,	,	kIx,
finds	finds	k1gInSc1
survey	survea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rainbow	Rainbow	k1gFnSc1
Nation	Nation	k1gInSc1
–	–	k?
dream	dream	k1gInSc1
or	or	k?
reality	realita	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
18	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
August	August	k1gMnSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Bank	bank	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WAUGH	WAUGH	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geography	Geograph	k1gInPc4
<g/>
:	:	kIx,
An	An	k1gMnSc1
Integrated	Integrated	k1gMnSc1
Approach	Approach	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Nelson	Nelson	k1gMnSc1
Thornes	Thornes	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
444706	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Manufacturing	Manufacturing	k1gInSc4
industries	industries	k1gInSc1
(	(	kIx(
<g/>
chapter	chapter	k1gInSc1
19	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
World	World	k1gMnSc1
development	development	k1gMnSc1
(	(	kIx(
<g/>
chapter	chapter	k1gInSc1
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
563	#num#	k4
<g/>
,	,	kIx,
576	#num#	k4
<g/>
–	–	k?
<g/>
579	#num#	k4
<g/>
,	,	kIx,
633	#num#	k4
<g/>
,	,	kIx,
640	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
book	book	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Oficiální	oficiální	k2eAgFnPc4d1
webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
Commonwealthu	Commonwealth	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
COOPER	COOPER	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
F	F	kA
<g/>
;	;	kIx,
ANTKIEWICZ	ANTKIEWICZ	kA
<g/>
,	,	kIx,
Agata	Agata	k1gFnSc1
<g/>
;	;	kIx,
SHAW	SHAW	kA
<g/>
,	,	kIx,
Timothy	Timotha	k1gMnSc2
M.	M.	kA
Lessons	Lessons	k1gInSc1
from	from	k1gInSc1
<g/>
/	/	kIx~
<g/>
for	forum	k1gNnPc2
BRICSAM	BRICSAM	kA
about	about	k2eAgInSc1d1
South-North	South-North	k1gInSc1
Relations	Relations	k1gInSc1
at	at	k?
the	the	k?
Start	start	k1gInSc1
of	of	k?
the	the	k?
21	#num#	k4
<g/>
st	st	kA
Century	Centura	k1gFnSc2
<g/>
:	:	kIx,
Economic	Economic	k1gMnSc1
Size	Siz	k1gFnSc2
Trumps	Trumps	k1gInSc1
All	All	k1gFnSc2
Else	Elsa	k1gFnSc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Studies	Studies	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
10	#num#	k4
December	December	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
675	#num#	k4
<g/>
,	,	kIx,
687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1468	.1468	k4
<g/>
-	-	kIx~
<g/>
2486.2007	2486.2007	k4
<g/>
.00730	.00730	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LYNCH	LYNCH	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
A.	A.	kA
Trade	Trad	k1gInSc5
and	and	k?
Globalization	Globalization	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
Regional	Regional	k1gMnSc1
Trade	Trad	k1gInSc5
Agreements	Agreements	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Rowman	Rowman	k1gMnSc1
&	&	k?
Littlefield	Littlefield	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7425	#num#	k4
<g/>
-	-	kIx~
<g/>
6689	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
51	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Unemployment	Unemployment	k1gInSc1
Rate	Rate	k1gNnSc6
Increases	Increases	k1gInSc1
to	ten	k3xDgNnSc1
23.5	23.5	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
www.bloomberg.com	www.bloomberg.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomberg	Bloomberg	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
May	May	k1gMnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
May	May	k1gMnSc1
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HDI	HDI	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNDP	UNDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
19	#num#	k4
December	December	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WYMER	WYMER	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
;	;	kIx,
SINGER	SINGER	kA
<g/>
,	,	kIx,
R.	R.	kA
The	The	k1gFnSc1
Middle	Middle	k1gFnSc1
Stone	ston	k1gInSc5
Age	Age	k1gMnSc3
at	at	k?
Klasies	Klasies	k1gMnSc1
River	River	k1gMnSc1
Mouth	Mouth	k1gMnSc1
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Chicago	Chicago	k1gNnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
-	-	kIx~
<g/>
76103	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Deacon	Deacon	k1gInSc1
<g/>
,	,	kIx,
HJ	HJ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc4
Klasies	Klasies	k1gMnSc1
River	River	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stellenbosch	Stellenbosch	k1gInSc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CENTRE	centr	k1gInSc5
<g/>
,	,	kIx,
UNESCO	Unesco	k1gNnSc1
World	Worlda	k1gFnPc2
Heritage	Heritage	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fossil	Fossil	k1gMnSc1
Hominid	hominid	k1gMnSc1
Sites	Sites	k1gMnSc1
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
UNESCO	Unesco	k1gNnSc1
WORLD	WORLD	kA
HERITAGE	HERITAGE	kA
<g/>
.	.	kIx.
whc	whc	k?
<g/>
.	.	kIx.
<g/>
unesco	unesco	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BROKER	broker	k1gMnSc1
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgMnSc1d1
P.	P.	kA
Hominid	hominid	k1gMnSc1
Evolution	Evolution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yale-New	Yale-New	k1gFnSc1
Haven	Havna	k1gFnPc2
Teachers	Teachersa	k1gFnPc2
Institute	institut	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
An	An	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
World	Worlda	k1gFnPc2
History	Histor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Langer	Langer	k1gMnSc1
William	William	k1gInSc1
L.	L.	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
th	th	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
Houghton	Houghton	k1gInSc1
Mifflin	Mifflina	k1gFnPc2
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
395	#num#	k4
<g/>
-	-	kIx~
<g/>
13592	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
LEAKEY	LEAKEY	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
Seymour	Seymour	k1gMnSc1
Bazett	Bazett	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stone	ston	k1gInSc5
age	age	k?
Africa	Africum	k1gNnPc4
<g/>
:	:	kIx,
an	an	k?
outline	outlin	k1gInSc5
of	of	k?
prehistory	prehistor	k1gInPc1
in	in	k?
Africa	Afric	k1gInSc2
<g/>
.	.	kIx.
reprint	reprint	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Negro	Negro	k1gNnSc1
Universities	Universitiesa	k1gFnPc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780837120225	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Stone	ston	k1gInSc5
Age	Age	k1gFnPc1
cultures	cultures	k1gMnSc1
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
79	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ALFRED	Alfred	k1gMnSc1
<g/>
,	,	kIx,
Luke	Luke	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Bakoni	Bakon	k1gMnPc1
<g/>
:	:	kIx,
From	From	k1gInSc1
prosperity	prosperita	k1gFnSc2
to	ten	k3xDgNnSc1
extinction	extinction	k1gInSc1
in	in	k?
a	a	k8xC
generation	generation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adam	Adam	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Calendar	Calendar	k1gInSc1
in	in	k?
Waterval	Waterval	k1gFnSc2
Boven	Bovna	k1gFnPc2
<g/>
,	,	kIx,
Mpumalanga	Mpumalanga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DOMVILLE-FIFE	DOMVILLE-FIFE	k1gMnSc1
<g/>
,	,	kIx,
C.W.	C.W.	k1gMnSc1
The	The	k1gMnSc2
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
the	the	k?
British	British	k1gInSc1
Empire	empir	k1gInSc5
the	the	k?
first	first	k1gMnSc1
encyclopedic	encyclopedic	k1gMnSc1
record	record	k1gMnSc1
of	of	k?
the	the	k?
greatest	greatest	k1gInSc1
empire	empir	k1gInSc5
in	in	k?
the	the	k?
history	histor	k1gInPc1
of	of	k?
the	the	k?
world	world	k1gInSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Rankin	Rankin	k1gMnSc1
<g/>
,	,	kIx,
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MACKENZIE	MACKENZIE	kA
<g/>
,	,	kIx,
W.	W.	kA
Douglas	Douglas	k1gInSc1
<g/>
;	;	kIx,
STEAD	STEAD	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
:	:	kIx,
Its	Its	k1gMnSc1
History	Histor	k1gInPc4
<g/>
,	,	kIx,
Heroes	Heroes	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Wars	Wars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Co-Operative	Co-Operativ	k1gInSc5
Publishing	Publishing	k1gInSc4
Company	Compan	k1gInPc7
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PAKEMAN	PAKEMAN	kA
<g/>
,	,	kIx,
SA	SA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nations	Nations	k1gInSc1
of	of	k?
the	the	k?
Modern	Modern	k1gMnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
Ceylon	Ceylon	k1gInSc1
<g/>
.	.	kIx.
1964	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Frederick	Frederick	k1gInSc1
A	a	k8xC
Praeger	Praeger	k1gInSc1
<g/>
,	,	kIx,
Publishers	Publishers	k1gInSc1
S.	S.	kA
18	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
WILMOT	WILMOT	kA
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
<g/>
;	;	kIx,
JOHN	John	k1gMnSc1
CENTLIVRES	CENTLIVRES	kA
CHASE	chasa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gInPc7
of	of	k?
the	the	k?
Colony	colon	k1gNnPc7
of	of	k?
the	the	k?
Cape	capat	k5eAaImIp3nS
of	of	k?
Good	Good	k1gInSc1
Hope	Hope	k1gFnSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
Its	Its	k1gMnSc2
Discovery	Discovera	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
Year	Year	k1gInSc1
1819	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Claremont	Claremont	k1gMnSc1
<g/>
:	:	kIx,
David	David	k1gMnSc1
Philip	Philip	k1gMnSc1
(	(	kIx(
<g/>
Pty	Pty	k1gMnSc1
<g/>
)	)	kIx)
Ltd	ltd	kA
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
144	#num#	k4
<g/>
-	-	kIx~
<g/>
83015	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
–	–	k?
<g/>
548	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KAPLAN	Kaplan	k1gMnSc1
<g/>
,	,	kIx,
Irving	Irving	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Area	area	k1gFnSc1
Handbook	handbook	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Republic	Republice	k1gFnPc2
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
46	#num#	k4
<g/>
–	–	k?
<g/>
771	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
African	African	k1gInSc1
History	Histor	k1gMnPc4
Timeline	Timelin	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
West	West	k2eAgInSc1d1
Chester	Chester	k1gInSc1
University	universita	k1gFnSc2
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
HUNT	hunt	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dutch	Dutch	k1gMnSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
:	:	kIx,
Early	earl	k1gMnPc4
Settlers	Settlersa	k1gFnPc2
at	at	k?
the	the	k?
Cape	capat	k5eAaImIp3nS
<g/>
,	,	kIx,
1652	#num#	k4
<g/>
–	–	k?
<g/>
1708	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Campbell	Campbella	k1gFnPc2
Heather-Ann	Heather-Anna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
904744	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WORDEN	WORDEN	kA
<g/>
,	,	kIx,
Nigel	Nigel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavery	Slavera	k1gFnSc2
in	in	k?
Dutch	Dutch	k1gMnSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
August	August	k1gMnSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
15266	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
NELSON	Nelson	k1gMnSc1
<g/>
,	,	kIx,
Harold	Harold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
:	:	kIx,
A	a	k9
Country	country	k2eAgInPc4d1
Study	stud	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
237	#num#	k4
<g/>
–	–	k?
<g/>
317	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
STAPLETON	STAPLETON	kA
<g/>
,	,	kIx,
Timothy	Timoth	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Military	Militara	k1gFnPc1
History	Histor	k1gInPc4
of	of	k?
South	South	k1gInSc1
Africa	Africa	k1gFnSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
the	the	k?
Dutch-Khoi	Dutch-Khoi	k1gNnSc7
Wars	Warsa	k1gFnPc2
to	ten	k3xDgNnSc1
the	the	k?
End	End	k1gFnSc2
of	of	k?
Apartheid	apartheid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Santa	Santa	k1gFnSc1
Barbara	Barbara	k1gFnSc1
<g/>
:	:	kIx,
Praeger	Praeger	k1gInSc1
Security	Securita	k1gFnSc2
International	International	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
36589	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KEEGAN	KEEGAN	kA
<g/>
,	,	kIx,
Timothy	Timoth	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Colonial	Colonial	k1gMnSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
and	and	k?
the	the	k?
Origins	Origins	k1gInSc1
of	of	k?
the	the	k?
Racial	Racial	k1gMnSc1
Order	Order	k1gMnSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
David	David	k1gMnSc1
Philip	Philip	k1gMnSc1
Publishers	Publishers	k1gInSc1
(	(	kIx(
<g/>
Pty	Pty	k1gMnSc1
<g/>
)	)	kIx)
Ltd	ltd	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8139	#num#	k4
<g/>
-	-	kIx~
<g/>
1735	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
85	#num#	k4
<g/>
–	–	k?
<g/>
86	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
LLOYD	LLOYD	kA
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1
Owen	Owen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
British	British	k1gMnSc1
Empire	empir	k1gInSc5
<g/>
,	,	kIx,
1558	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
873133	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
201	#num#	k4
<g/>
–	–	k?
<g/>
203	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Shaka	Shaka	k1gMnSc1
<g/>
:	:	kIx,
Zulu	Zulu	k1gMnSc1
Chieftain	Chieftain	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historynet	Historynet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
9	#num#	k4
February	Februara	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Shaka	Shaka	k1gMnSc1
(	(	kIx(
<g/>
Zulu	Zulu	k1gMnSc1
chief	chief	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
W.	W.	kA
D.	D.	kA
Rubinstein	Rubinstein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genocide	Genocid	k1gInSc5
<g/>
:	:	kIx,
A	A	kA
History	Histor	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Pearson	Pearson	k1gMnSc1
Longman	Longman	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
582	#num#	k4
<g/>
-	-	kIx~
<g/>
50601	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ING	ing	kA
ALEXANDR	Alexandr	k1gMnSc1
ZIMÁK	zimák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
184	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Období	období	k1gNnSc2
evropské	evropský	k2eAgFnSc2d1
kolonizace	kolonizace	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Williams	Williams	k1gInSc1
<g/>
,	,	kIx,
Garner	Garner	k1gMnSc1
F.	F.	kA
The	The	k1gMnSc1
Diamond	Diamond	k1gMnSc1
Mines	Mines	k1gMnSc1
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
Vol	vol	k6eAd1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
B.	B.	kA
F	F	kA
Buck	Buck	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
Chapter	Chaptra	k1gFnPc2
XX	XX	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gInSc1
African	Africana	k1gFnPc2
Military	Militara	k1gFnSc2
History	Histor	k1gInPc1
Society	societa	k1gFnSc2
–	–	k?
Journal-	Journal-	k1gFnSc2
THE	THE	kA
SEKUKUNI	SEKUKUNI	kA
WARS	WARS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
5	#num#	k4
of	of	k?
the	the	k?
worst	worst	k1gMnSc1
atrocities	atrocities	k1gMnSc1
carried	carried	k1gMnSc1
out	out	k?
by	by	k9
the	the	k?
British	British	k1gInSc1
Empire	empir	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
.	.	kIx.
19	#num#	k4
January	Januara	k1gFnSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BOND	bond	k1gInSc1
<g/>
,	,	kIx,
Patrick	Patrick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cities	Cities	k1gInSc1
of	of	k?
gold	gold	k1gInSc1
<g/>
,	,	kIx,
townships	townships	k1gInSc1
of	of	k?
coal	coal	k1gInSc1
<g/>
:	:	kIx,
essays	essays	k6eAd1
on	on	k3xPp3gMnSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
new	new	k?
urban	urban	k1gInSc1
crisis	crisis	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Africa	Africa	k1gFnSc1
World	Worlda	k1gFnPc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
86543	#num#	k4
<g/>
-	-	kIx~
<g/>
611	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
140	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
report	report	k1gInSc1
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
report	report	k1gInSc1
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
</s>
<s>
report	report	k1gInSc1
<g/>
↑	↑	k?
DE	DE	k?
VILLIERS	VILLIERS	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Abraham	Abraham	k1gMnSc1
Jacob	Jacoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Transvaal	Transvaal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Chatto	Chatto	k1gNnSc1
&	&	k?
Windus	Windus	k1gMnSc1
<g/>
,	,	kIx,
1896	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
(	(	kIx(
<g/>
n	n	k0
<g/>
46	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
EB	EB	kA
<g/>
1911	#num#	k4
<g/>
↑	↑	k?
Native	Natiev	k1gFnSc2
Land	Land	k1gMnSc1
Act	Act	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Race	Race	k1gNnSc2
Relations	Relationsa	k1gFnPc2
<g/>
,	,	kIx,
19	#num#	k4
June	jun	k1gMnSc5
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
14	#num#	k4
October	October	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gloria	Gloria	k1gFnSc1
Galloway	Gallowaa	k1gFnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Chieft	Chieft	k2eAgInSc1d1
Reflect	Reflect	k2eAgInSc1d1
on	on	k3xPp3gInSc1
Apartheid	apartheid	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
The	The	k1gMnSc1
Globe	globus	k1gInSc5
and	and	k?
Mail	mail	k1gInSc1
<g/>
,	,	kIx,
11	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
↑	↑	k?
Beinart	Beinart	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twentieth-century	Twentieth-centura	k1gFnSc2
South	Southa	k1gFnPc2
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
p.	p.	k?
202	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
289318	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GIBSON	GIBSON	kA
<g/>
,	,	kIx,
Nigel	Nigel	k1gMnSc1
<g/>
;	;	kIx,
ALEXANDER	Alexandra	k1gFnPc2
<g/>
,	,	kIx,
Amanda	Amanda	k1gFnSc1
<g/>
;	;	kIx,
MNGXITAMA	MNGXITAMA	kA
<g/>
,	,	kIx,
Andile	Andila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biko	bika	k1gFnSc5
Lives	Livesa	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Contesting	Contesting	k1gInSc1
the	the	k?
Legacies	Legacies	k1gMnSc1
of	of	k?
Steve	Steve	k1gMnSc1
Biko	bika	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hampshire	Hampshir	k1gInSc5
<g/>
:	:	kIx,
Palgrave	Palgrav	k1gInSc5
Macmillan	Macmillan	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
230	#num#	k4
<g/>
-	-	kIx~
<g/>
60649	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
138	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Switzer	Switzer	k1gInSc1
<g/>
,	,	kIx,
Les	les	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Resistance	Resistanec	k1gInPc4
Press	Press	k1gInSc1
<g/>
:	:	kIx,
Alternative	Alternativ	k1gInSc5
Voices	Voicesa	k1gFnPc2
in	in	k?
the	the	k?
Last	Last	k2eAgInSc4d1
Generation	Generation	k1gInSc4
Under	Under	k1gInSc1
Apartheid	apartheid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Issue	Issu	k1gInSc2
74	#num#	k4
of	of	k?
Research	Research	k1gMnSc1
in	in	k?
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
studies	studies	k1gMnSc1
<g/>
:	:	kIx,
Africa	Africa	k1gMnSc1
series	series	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Ohio	Ohio	k1gNnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
89680	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MITCHELL	MITCHELL	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Native	Natiev	k1gFnSc2
vs	vs	k?
Settler	Settler	k1gMnSc1
<g/>
:	:	kIx,
Ethnic	Ethnice	k1gFnPc2
Conflict	Conflict	k1gInSc1
in	in	k?
Israel	Israel	k1gInSc4
<g/>
/	/	kIx~
<g/>
Palestine	Palestin	k1gMnSc5
<g/>
,	,	kIx,
Northern	Northern	k1gNnSc1
Ireland	Irelanda	k1gFnPc2
and	and	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Westport	Westport	k1gInSc1
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
31357	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
194	#num#	k4
<g/>
–	–	k?
<g/>
196	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRIDGLAND	BRIDGLAND	kA
<g/>
,	,	kIx,
Fred	Fred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
War	War	k1gFnSc1
for	forum	k1gNnPc2
Africa	Africum	k1gNnSc2
<g/>
:	:	kIx,
Twelve	Twelev	k1gFnPc4
months	months	k6eAd1
that	that	k2eAgInSc4d1
transformed	transformed	k1gInSc4
a	a	k8xC
continent	continent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibraltar	Gibraltar	k1gInSc1
<g/>
:	:	kIx,
Ashanti	Ashant	k1gMnPc1
Publishing	Publishing	k1gInSc4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
874800	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LANDGREN	LANDGREN	kA
<g/>
,	,	kIx,
Signe	Sign	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Embargo	embargo	k1gNnSc1
Disimplemented	Disimplemented	k1gMnSc1
<g/>
:	:	kIx,
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Military	Militar	k1gInPc7
Industry	Industra	k1gFnSc2
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
829127	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KÖSSL	KÖSSL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
KRÁTKÝ	Krátký	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
II	II	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
s.	s.	k?
207	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
Profile	profil	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nti	Nti	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2	#num#	k4
October	October	k1gInSc1
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PIKE	PIKE	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nuclear	Nucleara	k1gFnPc2
Weapons	Weaponsa	k1gFnPc2
Program	program	k1gInSc1
(	(	kIx(
<g/>
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Globalsecurity	Globalsecurita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Post-Apartheid	Post-Apartheid	k1gInSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
First	First	k1gFnSc4
Ten	ten	k3xDgInSc1
Years	Years	k1gInSc1
–	–	k?
Unemployment	Unemployment	k1gInSc1
and	and	k?
the	the	k?
Labor	Labor	k1gInSc1
Market	market	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMF	IMF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zuma	Zuma	k1gMnSc1
surprised	surprised	k1gMnSc1
at	at	k?
level	level	k1gInSc1
of	of	k?
white	white	k5eAaPmIp2nP
poverty	povert	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
April	April	k1gInSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
Nations	Nationsa	k1gFnPc2
Development	Development	k1gMnSc1
Programme	Programme	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
November	November	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2015	#num#	k4
United	United	k1gInSc1
Nations	Nationsa	k1gFnPc2
Human	Human	k1gInSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gInSc1
African	African	k1gInSc1
Life	Lif	k1gMnSc2
Expectancy	Expectanca	k1gMnSc2
at	at	k?
Birth	Birth	k1gMnSc1
<g/>
,	,	kIx,
World	World	k1gMnSc1
Bank	bank	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ridicule	Ridicule	k?
succeeds	succeeds	k6eAd1
where	wher	k1gInSc5
leadership	leadership	k1gMnSc1
failed	failed	k1gMnSc1
on	on	k3xPp3gMnSc1
AIDS	AIDS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Race	Race	k1gNnSc2
Relations	Relationsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
November	November	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cbignore	Cbignor	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
Broke-on-Broke	Broke-on-Broke	k1gFnSc1
Violence	Violence	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
COHRE	COHRE	kA
statement	statement	k1gInSc4
on	on	k3xPp3gMnSc1
Xenophobic	Xenophobic	k1gMnSc1
Attacks	Attacksa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Southern	Southern	k1gInSc1
African	African	k1gInSc1
Migration	Migration	k1gInSc1
Project	Project	k1gInSc4
<g/>
;	;	kIx,
INSTITUTE	institut	k1gInSc5
FOR	forum	k1gNnPc2
DEMOCRACY	DEMOCRACY	kA
IN	IN	kA
SOUTH	SOUTH	kA
AFRICA	AFRICA	kA
<g/>
;	;	kIx,
QUEEN	QUEEN	kA
<g/>
'	'	kIx"
<g/>
S	s	k7c7
UNIVERSITY	universita	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
perfect	perfect	k1gMnSc1
storm	storm	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
realities	realities	k1gInSc1
of	of	k?
xenophobia	xenophobia	k1gFnSc1
in	in	k?
contemporary	contemporara	k1gFnSc2
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Jonathan	Jonathan	k1gMnSc1
Crush	Crush	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Idasa	Idasa	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
30	#num#	k4
July	Jula	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
920118	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
United	United	k1gInSc1
Nations	Nations	k1gInSc4
High	High	k1gInSc1
Commissioner	Commissionra	k1gFnPc2
for	forum	k1gNnPc2
Refugees	Refugees	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNHCR	UNHCR	kA
Global	globat	k5eAaImAgMnS
Appeal	Appeal	k1gMnSc1
2011	#num#	k4
–	–	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNHCR	UNHCR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Harris	Harris	k1gInSc1
<g/>
,	,	kIx,
Bronwyn	Bronwyn	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arranging	Arranging	k1gInSc1
prejudice	prejudice	k1gFnSc2
<g/>
:	:	kIx,
Exploring	Exploring	k1gInSc1
hate	hat	k1gInSc2
crime	crimat	k5eAaPmIp3nS
in	in	k?
post-apartheid	post-apartheid	k1gInSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cape	capat	k5eAaImIp3nS
Town	Town	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Traum	Traum	k1gInSc1
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Contextualising	Contextualising	k1gInSc1
the	the	k?
hate	hatat	k5eAaPmIp3nS
speech	speech	k1gInSc1
debate	debat	k1gInSc5
<g/>
:	:	kIx,
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
and	and	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Comparative	Comparativ	k1gInSc5
and	and	k?
International	International	k1gMnSc1
Law	Law	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Southern	Southern	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
47	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
64	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Statistics	Statistics	k1gInSc1
Division	Division	k1gInSc4
–	–	k?
Demographic	Demographice	k1gInPc2
and	and	k?
Social	Social	k1gInSc1
Statistics	Statistics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Country	country	k2eAgInSc4d1
Comparison	Comparison	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
How	How	k1gFnSc1
big	big	k?
is	is	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gInSc1
Africa	Africum	k1gNnSc2
Gateway	Gatewaa	k1gFnSc2
<g/>
.	.	kIx.
23	#num#	k4
November	November	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
12	#num#	k4
December	December	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
McCarthy	McCartha	k1gFnSc2
<g/>
,	,	kIx,
T.	T.	kA
&	&	k?
Rubidge	Rubidge	k1gInSc1
<g/>
,	,	kIx,
B.	B.	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
story	story	k1gFnSc1
of	of	k?
earth	earth	k1gInSc1
and	and	k?
life	life	k1gInSc1
<g/>
.	.	kIx.
p.	p.	k?
263	#num#	k4
<g/>
,	,	kIx,
267	#num#	k4
<g/>
–	–	k?
<g/>
268	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Struik	Struik	k1gInSc1
Publishers	Publishers	k1gInSc4
<g/>
,	,	kIx,
Cape	capat	k5eAaImIp3nS
Town	Town	k1gMnSc1
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Atlas	Atlas	k1gMnSc1
of	of	k?
Southern	Southern	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
p.	p.	k?
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Readers	Readersa	k1gFnPc2
Digest	Digest	k1gFnSc1
Association	Association	k1gInSc1
<g/>
,	,	kIx,
Cape	capat	k5eAaImIp3nS
Town	Town	k1gMnSc1
<g/>
↑	↑	k?
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannica	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Micropaedia	Micropaedium	k1gNnSc2
Vol	vol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
III	III	kA
<g/>
,	,	kIx,
p.	p.	k?
655	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helen	Helena	k1gFnPc2
Hemingway	Hemingwaa	k1gFnSc2
Benton	Benton	k1gInSc1
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Atlas	Atlas	k1gInSc1
of	of	k?
Southern	Southern	k1gInSc1
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
p.	p.	k?
186	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Readers	Readersa	k1gFnPc2
Digest	Digest	k1gFnSc1
Association	Association	k1gInSc1
<g/>
,	,	kIx,
Cape	capat	k5eAaImIp3nS
Town	Town	k1gMnSc1
<g/>
↑	↑	k?
Kruger	Kruger	k1gInSc1
National	National	k1gMnSc1
Park	park	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
18	#num#	k4
December	December	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Atlas	Atlas	k1gInSc1
of	of	k?
Southern	Southern	k1gInSc1
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
p.	p.	k?
151	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Readers	Readersa	k1gFnPc2
Digest	Digest	k1gFnSc1
Association	Association	k1gInSc1
<g/>
,	,	kIx,
Cape	capat	k5eAaImIp3nS
Town	Town	k1gMnSc1
<g/>
↑	↑	k?
These	these	k1gFnSc2
are	ar	k1gInSc5
the	the	k?
lowest	lowest	k1gMnSc1
ever	ever	k1gMnSc1
temperatures	temperatures	k1gMnSc1
recorded	recorded	k1gMnSc1
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
July	Jula	k1gFnSc2
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
geography	geograph	k1gInPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Safrica	Safrica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
8	#num#	k4
June	jun	k1gMnSc5
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gInSc1
Africa	Africa	k1gMnSc1
yearbook	yearbook	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
South	South	k1gInSc1
African	African	k1gInSc1
Communication	Communication	k1gInSc4
Service	Service	k1gFnSc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780797035447	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Term	term	k1gInSc1
Limits	Limits	k1gInSc1
in	in	k?
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
.	.	kIx.
6	#num#	k4
April	April	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
June	jun	k1gMnSc5
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PLESNÍK	PLESNÍK	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zuma	Zum	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
klan	klan	k1gInSc4
JAR	jaro	k1gNnPc2
téměř	téměř	k6eAd1
vyplenili	vyplenit	k5eAaPmAgMnP
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borgis	borgis	k1gInSc1
<g/>
,	,	kIx,
<g/>
a.s.	a.s.	k?
<g/>
,	,	kIx,
17.2	17.2	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20.2	20.2	k4
<g/>
.2018	.2018	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VAN	van	k1gInSc1
ZUYDAM	ZUYDAM	kA
<g/>
,	,	kIx,
Schalk	Schalk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
vládní	vládní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
žádá	žádat	k5eAaImIp3nS
odchod	odchod	k1gInSc4
prezidenta	prezident	k1gMnSc2
Zumy	Zuma	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
<g/>
/	/	kIx~
<g/>
AP	ap	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
13.2	13.2	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20.2	20.2	k4
<g/>
.2018	.2018	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Cyril	Cyril	k1gMnSc1
Ramaphosa	Ramaphosa	k1gFnSc1
je	být	k5eAaImIp3nS
novým	nový	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
JAR	Jara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradil	nahradit	k5eAaPmAgMnS
Zumu	Zum	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
zpravy	zprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
aktualne	aktualnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
<g/>
a.s.	a.s.	k?
<g/>
,	,	kIx,
15.2	15.2	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20.2	20.2	k4
<g/>
.2018	.2018	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Chapter	Chapter	k1gInSc1
4	#num#	k4
–	–	k?
Parliament	Parliament	k1gMnSc1
<g/>
.	.	kIx.
www.info.gov.za	www.info.gov.za	k1gFnSc1
<g/>
.	.	kIx.
19	#num#	k4
August	August	k1gMnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
30	#num#	k4
May	May	k1gMnSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BUCCUS	BUCCUS	kA
<g/>
,	,	kIx,
Imraan	Imraan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mercury	Mercura	k1gFnPc1
<g/>
:	:	kIx,
Rethinking	Rethinking	k1gInSc1
the	the	k?
crisis	crisis	k1gInSc1
of	of	k?
local	local	k1gInSc1
democracy	democraca	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abahlali	Abahlali	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
J.	J.	kA
Duncan	Duncan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Return	Return	k1gMnSc1
of	of	k?
State	status	k1gInSc5
Repression	Repression	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
Civil	civil	k1gMnSc1
Society	societa	k1gFnSc2
Information	Information	k1gInSc1
Services	Services	k1gInSc1
<g/>
,	,	kIx,
31	#num#	k4
May	May	k1gMnSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
30	#num#	k4
June	jun	k1gMnSc5
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Increasing	Increasing	k1gInSc4
police	police	k1gFnSc2
repression	repression	k1gInSc1
highlighted	highlighted	k1gInSc1
by	by	kYmCp3nS
recent	recent	k1gInSc4
case	case	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
of	of	k?
Expression	Expression	k1gInSc1
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
20	#num#	k4
January	Januara	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BUCCUS	BUCCUS	kA
<g/>
,	,	kIx,
Imraan	Imraan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Political	Political	k1gFnSc1
tolerance	tolerance	k1gFnSc1
on	on	k3xPp3gMnSc1
the	the	k?
wane	wane	k1gNnSc1
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SA	SA	kA
Reconciliation	Reconciliation	k1gInSc4
Barometer	Barometra	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
recent	recent	k1gInSc1
performance	performance	k1gFnSc2
in	in	k?
the	the	k?
Ibrahim	Ibrahim	k1gInSc1
Index	index	k1gInSc1
of	of	k?
African	African	k1gInSc1
Governance	Governance	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mo	Mo	k1gMnSc1
Ibrahim	Ibrahim	k1gMnSc1
Foundation	Foundation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
18	#num#	k4
February	Februara	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SA	SA	kA
marriage	marriagat	k5eAaPmIp3nS
law	law	k?
signed	signed	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
30	#num#	k4
November	November	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
June	jun	k1gMnSc5
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JONES	JONES	kA
<g/>
,	,	kIx,
Rachel	Rachel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apartheid	apartheid	k1gInSc1
ended	ended	k1gInSc4
29	#num#	k4
years	yearsa	k1gFnPc2
ago	aga	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
has	hasit	k5eAaImRp2nS
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
changed	changed	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MORTON	MORTON	kA
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
begins	beginsa	k1gFnPc2
seizing	seizing	k1gInSc1
white-owned	white-owned	k1gMnSc1
farms	farms	k1gInSc1
in	in	k?
land	land	k1gInSc1
redistribution	redistribution	k1gInSc1
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OSBORNE	OSBORNE	kA
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
votes	votes	k1gMnSc1
through	through	k1gMnSc1
motion	motion	k1gInSc4
that	that	k5eAaPmF
could	could	k6eAd1
lead	lead	k6eAd1
to	ten	k3xDgNnSc1
seizure	seizur	k1gMnSc5
of	of	k?
land	land	k6eAd1
from	from	k6eAd1
white	white	k5eAaPmIp2nP
farmers	farmers	k6eAd1
without	without	k5eAaPmF,k5eAaImF
compensation	compensation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BANK	bank	k1gInSc1
<g/>
,	,	kIx,
Christiena	Christien	k2eAgFnSc1d1
Maria	Maria	k1gFnSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
<g/>
;	;	kIx,
MPHAHLANI	MPHAHLANI	kA
<g/>
,	,	kIx,
Jafta	Jafta	k1gFnSc1
<g/>
;	;	kIx,
MOLOI	MOLOI	kA
<g/>
,	,	kIx,
Kholeka	Kholeka	k1gMnSc1
C.	C.	kA
Affirmative	Affirmativ	k1gInSc5
Action	Action	k1gInSc1
Application	Application	k1gInSc1
or	or	k?
Black	Black	k1gInSc1
and	and	k?
White	Whit	k1gInSc5
in	in	k?
South	South	k1gInSc1
Africa	Africa	k1gMnSc1
Higher	Highra	k1gFnPc2
Education	Education	k1gInSc4
Institutions	Institutions	k1gInSc1
<g/>
:	:	kIx,
Is	Is	k1gFnSc1
it	it	k?
the	the	k?
way	way	k?
forward	forward	k1gInSc1
or	or	k?
not	nota	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Education	Education	k1gInSc1
and	and	k?
Learning	Learning	k1gInSc1
(	(	kIx(
<g/>
EduLearn	EduLearn	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
288	#num#	k4
<g/>
–	–	k?
<g/>
295	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2302	#num#	k4
<g/>
-	-	kIx~
<g/>
9277	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.115	10.115	k4
<g/>
91	#num#	k4
<g/>
/	/	kIx~
<g/>
edulearn	edulearna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
v	v	k7c6
<g/>
9	#num#	k4
<g/>
i	i	k9
<g/>
4.249	4.249	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MOMASOH	MOMASOH	kA
CLETUS	CLETUS	kA
<g/>
,	,	kIx,
Muluh	Muluh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Affirmative	Affirmativ	k1gInSc5
action	action	k1gInSc1
policy	policy	k1gInPc1
in	in	k?
higher	highra	k1gFnPc2
education	education	k1gInSc1
<g/>
:	:	kIx,
impact	impact	k1gInSc1
and	and	k?
perceptions	perceptions	k1gInSc1
of	of	k?
the	the	k?
use	usus	k1gInSc5
of	of	k?
race	race	k5eAaImRp2nP
as	as	k9
part	part	k1gInSc4
of	of	k?
the	the	k?
criteria	criterium	k1gNnSc2
for	forum	k1gNnPc2
admission	admission	k1gInSc1
at	at	k?
the	the	k?
University	universita	k1gFnSc2
of	of	k?
Cape	capat	k5eAaImIp3nS
Town	Town	k1gInSc1
<g/>
.	.	kIx.
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thesis	Thesis	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
the	the	k?
Western	Western	kA
Cape	capat	k5eAaImIp3nS
<g/>
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Affirmative	Affirmativ	k1gInSc5
Action	Action	k1gInSc4
in	in	k?
South	South	k1gInSc1
African	African	k1gMnSc1
Universities	Universities	k1gMnSc1
<g/>
:	:	kIx,
What	What	k2eAgInSc1d1
Does	Does	k1gInSc1
Race	Race	k1gNnSc1
Represent	Represent	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OHRH	OHRH	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-07-01	2013-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Inequalities	Inequalities	k1gInSc1
Complicate	Complicat	k1gInSc5
S.	S.	kA
Africa	Afric	k2eAgFnSc1d1
College	College	k1gFnSc1
Admissions	Admissionsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
magazine	magazinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Schlesinger	Schlesinger	k1gInSc1
<g/>
,	,	kIx,
Stephen	Stephen	k1gInSc1
E.	E.	kA
Act	Act	k1gMnSc2
of	of	k?
Creation	Creation	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Founding	Founding	k1gInSc1
of	of	k?
the	the	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Story	story	k1gFnSc1
of	of	k?
Superpowers	Superpowers	k1gInSc1
<g/>
,	,	kIx,
Secret	Secret	k1gInSc1
Agents	Agentsa	k1gFnPc2
<g/>
,	,	kIx,
Wartime	Wartim	k1gInSc5
Allies	Allies	k1gMnSc1
and	and	k?
Enemies	Enemies	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Their	Their	k1gMnSc1
Quest	Quest	k1gMnSc1
for	forum	k1gNnPc2
a	a	k8xC
Peaceful	Peacefula	k1gFnPc2
World	Worlda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
:	:	kIx,
Westview	Westview	k1gMnSc1
<g/>
,	,	kIx,
Perseus	Perseus	k1gMnSc1
Books	Booksa	k1gFnPc2
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
-	-	kIx~
<g/>
3275	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
236	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
<g/>
,	,	kIx,
South	South	k1gMnSc1
Africa	Afric	k1gInSc2
upgrade	upgrade	k1gInSc1
relations	relations	k6eAd1
to	ten	k3xDgNnSc1
"	"	kIx"
<g/>
comprehensive	comprehensivat	k5eAaPmIp3nS
strategic	strategic	k1gMnSc1
partnership	partnership	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Capetown	Capetown	k1gNnSc1
<g/>
.	.	kIx.
<g/>
china-consulate	china-consulat	k1gMnSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
25	#num#	k4
August	August	k1gMnSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
New	New	k1gFnSc1
era	era	k?
as	as	k9
South	South	k1gInSc1
Africa	Africum	k1gNnSc2
joins	joinsa	k1gFnPc2
BRICS	BRICS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Southafrica	Southafrica	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
<g/>
,	,	kIx,
11	#num#	k4
April	April	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
18	#num#	k4
April	April	k1gInSc1
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRICS	BRICS	kA
information	information	k1gInSc1
portal	portat	k5eAaPmAgInS,k5eAaImAgInS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SA	SA	kA
brings	brings	k1gInSc1
'	'	kIx"
<g/>
unique	unique	k1gInSc1
attributes	attributes	k1gInSc1
<g/>
'	'	kIx"
to	ten	k3xDgNnSc4
BRICS	BRICS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Southafrica	Southafrica	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
<g/>
,	,	kIx,
14	#num#	k4
April	April	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
9	#num#	k4
July	Jula	k1gFnSc2
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Constitution	Constitution	k1gInSc1
of	of	k?
the	the	k?
Republic	Republice	k1gFnPc2
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
Act	Act	k1gMnSc1
200	#num#	k4
of	of	k?
1993	#num#	k4
(	(	kIx(
<g/>
Section	Section	k1gInSc1
224	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
Government	Government	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
12	#num#	k4
June	jun	k1gMnSc5
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
L.	L.	kA
B.	B.	kA
van	vana	k1gFnPc2
Stade	Stad	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rationalisation	Rationalisation	k1gInSc1
in	in	k?
the	the	k?
SANDF	SANDF	kA
<g/>
:	:	kIx,
The	The	k1gMnSc2
Next	Next	k2eAgInSc1d1
Challenge	Challenge	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
Security	Securita	k1gFnPc1
Studies	Studies	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
16	#num#	k4
March	March	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Defence	Defenec	k1gMnPc4
Act	Act	k1gFnSc2
42	#num#	k4
of	of	k?
2002	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
Government	Government	k1gMnSc1
<g/>
,	,	kIx,
12	#num#	k4
February	Februara	k1gFnSc2
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
24	#num#	k4
June	jun	k1gMnSc5
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
LEKOTA	LEKOTA	kA
<g/>
,	,	kIx,
Mosiuoa	Mosiuoum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Address	Address	k1gInSc4
by	by	k9
the	the	k?
Minister	Minister	k1gInSc1
of	of	k?
Defence	Defence	k1gFnSc2
at	at	k?
a	a	k8xC
media	medium	k1gNnSc2
breakfast	breakfast	k1gMnSc1
at	at	k?
Defence	Defence	k1gFnSc2
Headquarters	Headquartersa	k1gFnPc2
<g/>
,	,	kIx,
Pretoria	Pretorium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
Defence	Defence	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
September	September	k1gInSc1
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Roy	Roy	k1gMnSc1
E.	E.	kA
Horton	Horton	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Out	Out	k1gFnSc1
of	of	k?
(	(	kIx(
<g/>
South	South	k1gMnSc1
<g/>
)	)	kIx)
Africa	Afric	k1gInSc2
<g/>
:	:	kIx,
Pretoria	Pretorium	k1gNnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Nuclear	Nuclear	k1gInSc1
Weapons	Weapons	k1gInSc1
Experience	Experience	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USAF	USAF	kA
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
National	National	k1gMnSc1
Security	Securita	k1gFnSc2
Studies	Studies	k1gInSc1
<g/>
,	,	kIx,
October	October	k1gInSc1
1999	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Educational	Educational	k1gMnPc2
Foundation	Foundation	k1gInSc4
for	forum	k1gNnPc2
Nuclear	Nucleara	k1gFnPc2
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
Atomic	Atomic	k1gMnSc1
Scientists	Scientists	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Educational	Educational	k1gFnSc1
Foundation	Foundation	k1gInSc1
for	forum	k1gNnPc2
Nuclear	Nucleara	k1gFnPc2
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
May	May	k1gMnSc1
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
South	Southa	k1gFnPc2
Africa	Africa	k1gMnSc1
comes	comes	k1gMnSc1
clean	clean	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DODSON	DODSON	kA
<g/>
,	,	kIx,
Christine	Christin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Atlantic	Atlantice	k1gFnPc2
Nuclear	Nuclear	k1gMnSc1
Event	Event	k1gMnSc1
(	(	kIx(
<g/>
National	National	k1gMnSc1
Security	Securita	k1gFnSc2
Council	Council	k1gInSc1
<g/>
,	,	kIx,
Memorandum	memorandum	k1gNnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	Georg	k1gInSc2
Washington	Washington	k1gInSc1
University	universita	k1gFnSc2
under	undra	k1gFnPc2
Freedom	Freedom	k1gInSc1
of	of	k?
Information	Information	k1gInSc1
Act	Act	k1gMnSc1
Request	Request	k1gMnSc1
<g/>
,	,	kIx,
22	#num#	k4
October	October	k1gInSc1
1979	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chapter	Chapter	k1gInSc1
XXVI	XXVI	kA
<g/>
:	:	kIx,
Disarmament	Disarmament	k1gMnSc1
–	–	k?
No	no	k9
<g/>
.	.	kIx.
9	#num#	k4
Treaty	Treata	k1gFnSc2
on	on	k3xPp3gMnSc1
the	the	k?
Prohibition	Prohibition	k1gInSc1
of	of	k?
Nuclear	Nuclear	k1gInSc1
Weapons	Weapons	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Nations	Nations	k1gInSc4
Treaty	Treata	k1gFnSc2
Collection	Collection	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
July	Jula	k1gFnSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
hdrstats	hdrstats	k1gInSc1
<g/>
.	.	kIx.
<g/>
undp	undp	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
25	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
hdrstats	hdrstats	k1gInSc1
<g/>
.	.	kIx.
<g/>
undp	undp	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
27	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
:	:	kIx,
válka	válka	k1gFnSc1
o	o	k7c4
ceny	cena	k1gFnPc4
nových	nový	k2eAgInPc2d1
jaderných	jaderný	k2eAgInPc2d1
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zuma	Zumum	k1gNnSc2
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
klan	klan	k1gInSc4
JAR	jaro	k1gNnPc2
téměř	téměř	k6eAd1
zruinovali	zruinovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prezident	prezident	k1gMnSc1
JAR	jar	k1gFnSc4
má	mít	k5eAaImIp3nS
problém	problém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
obviněn	obvinit	k5eAaPmNgMnS
ze	z	k7c2
stovek	stovka	k1gFnPc2
případů	případ	k1gInPc2
korupce	korupce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nový	nový	k2eAgMnSc1d1
jihoafrický	jihoafrický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
chce	chtít	k5eAaImIp3nS
zakročit	zakročit	k5eAaPmF
proti	proti	k7c3
korupci	korupce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
ale	ale	k8xC
v	v	k7c6
politice	politika	k1gFnSc6
neváhal	váhat	k5eNaImAgMnS
obohatit	obohatit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Community	Communit	k2eAgFnPc4d1
Survey	Survea	k1gFnPc4
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistics	Statisticsa	k1gFnPc2
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.scielo.org.za/scielo.php?pid=S0018-229X2011000200001&	http://www.scielo.org.za/scielo.php?pid=S0018-229X2011000200001&	k1gMnSc1
<g/>
↑	↑	k?
International	International	k1gMnSc1
Herald	Herald	k1gMnSc1
Tribune	tribun	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
May	May	k1gMnSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Escape	Escap	k1gInSc5
From	From	k1gInSc1
Mugabe	Mugab	k1gInSc5
<g/>
:	:	kIx,
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Exodus	Exodus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
More	mor	k1gInSc5
illegals	illegals	k1gInSc1
set	sto	k4xCgNnPc2
to	ten	k3xDgNnSc1
flood	flood	k1gInSc1
SA	SA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fin	Fina	k1gFnPc2
<g/>
24	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
South	South	k1gMnSc1
African	African	k1gMnSc1
mob	mob	k?
kills	kills	k1gInSc1
migrants	migrants	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
12	#num#	k4
May	May	k1gMnSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
May	May	k1gMnSc1
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BEARAK	BEARAK	kA
<g/>
,	,	kIx,
Barry	Barr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Immigrants	Immigrants	k1gInSc1
Fleeing	Fleeing	k1gInSc4
Fury	Fura	k1gFnSc2
of	of	k?
South	South	k1gInSc1
African	African	k1gMnSc1
Mobs	Mobs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
23	#num#	k4
May	May	k1gMnSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
August	August	k1gMnSc1
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LEHOHLA	LEHOHLA	kA
<g/>
,	,	kIx,
Pali	Pali	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Debate	Debat	k1gInSc5
over	over	k1gInSc1
race	rac	k1gInPc1
and	and	k?
censuses	censuses	k1gInSc1
not	nota	k1gFnPc2
peculiar	peculiara	k1gFnPc2
to	ten	k3xDgNnSc4
SA	SA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc1
Report	report	k1gInSc4
<g/>
.	.	kIx.
5	#num#	k4
May	May	k1gMnSc1
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
14	#num#	k4
August	August	k1gMnSc1
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Study	stud	k1gInPc1
Commission	Commission	k1gInSc1
on	on	k3xPp3gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
Policy	Polica	k1gMnSc2
toward	toward	k1gMnSc1
Southern	Southern	k1gMnSc1
Africa	Africa	k1gMnSc1
(	(	kIx(
<g/>
U.	U.	kA
<g/>
S.	S.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
:	:	kIx,
time	time	k1gInSc1
running	running	k1gInSc1
out	out	k?
<g/>
:	:	kIx,
the	the	k?
report	report	k1gInSc1
of	of	k?
the	the	k?
Study	stud	k1gInPc1
Commission	Commission	k1gInSc1
on	on	k3xPp3gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
Policy	Polica	k1gMnSc2
Toward	Toward	k1gMnSc1
Southern	Southern	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
California	Californium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
520	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4547	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mid-year	Mid-year	k1gInSc1
population	population	k1gInSc1
estimates	estimates	k1gInSc1
1995	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
The	The	k1gMnSc1
People	People	k1gMnSc1
of	of	k?
South	South	k1gMnSc1
Africa	Afric	k1gInSc2
Population	Population	k1gInSc1
census	census	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
Census	census	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mid-year	Mid-year	k1gInSc1
population	population	k1gInSc1
estimates	estimates	k1gInSc1
2020	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
World	Worlda	k1gFnPc2
Refugee	Refuge	k1gInPc1
Survey	Survea	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
www.refugees.org	www.refugees.org	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Committee	Committee	k1gNnSc4
for	forum	k1gNnPc2
Refugees	Refugeesa	k1gFnPc2
and	and	k?
Immigrants	Immigrantsa	k1gFnPc2
<g/>
,	,	kIx,
19	#num#	k4
June	jun	k1gMnSc5
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
19	#num#	k4
October	October	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORÁKOVÁ	Horáková	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národ	národ	k1gInSc1
<g/>
,	,	kIx,
kultura	kultura	k1gFnSc1
a	a	k8xC
etnicita	etnicita	k1gFnSc1
v	v	k7c6
postapartheidní	postapartheidní	k2eAgFnSc6d1
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
:	:	kIx,
GAUDEAMUS	GAUDEAMUS	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
275	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7041	#num#	k4
<g/>
-	-	kIx~
<g/>
836	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Religions	Religions	k1gInSc1
in	in	k?
South	South	k1gInSc1
Africa	Africa	k1gMnSc1
–	–	k?
PEW-GRF	PEW-GRF	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
–	–	k?
Section	Section	k1gInSc1
I.	I.	kA
Religious	Religious	k1gInSc4
Demography	Demographa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BENTLEY	BENTLEY	kA
<g/>
,	,	kIx,
Wessel	Wessel	k1gMnSc1
<g/>
;	;	kIx,
DION	DION	kA
ANGUS	ANGUS	kA
FORSTER	FORSTER	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Methodism	Methodism	k1gMnSc1
in	in	k?
Southern	Southern	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Celebration	Celebration	k1gInSc1
of	of	k?
Wesleyan	Wesleyan	k1gInSc1
Mission	Mission	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
AcadSA	AcadSA	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
920212	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
God	God	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
mission	mission	k1gInSc1
in	in	k?
our	our	k?
context	context	k1gInSc1
<g/>
,	,	kIx,
healing	healing	k1gInSc1
and	and	k?
transforming	transforming	k1gInSc1
responses	responses	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
97	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
van	van	k1gInSc1
Wyk	Wyk	k1gFnSc1
<g/>
,	,	kIx,
Ben-Erik	Ben-Erik	k1gMnSc1
<g/>
;	;	kIx,
VAN	van	k1gInSc1
OUDTSHOORN	OUDTSHOORN	kA
<g/>
,	,	kIx,
GERICKE	GERICKE	kA
N.	N.	kA
Medicinal	Medicinal	k1gFnPc2
Plants	Plantsa	k1gFnPc2
of	of	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pretoria	Pretorium	k1gNnPc1
<g/>
:	:	kIx,
Briza	Briza	k1gFnSc1
Publications	Publicationsa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
875093	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
State	status	k1gInSc5
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
,	,	kIx,
15	#num#	k4
September	September	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
In	In	k1gMnSc2
South	South	k1gInSc4
Africa	Africum	k1gNnSc2
<g/>
,	,	kIx,
many	mana	k1gFnSc2
blacks	blacksa	k1gFnPc2
convert	convert	k1gMnSc1
to	ten	k3xDgNnSc1
Islam	Islam	k1gInSc1
/	/	kIx~
The	The	k1gMnSc1
Christian	Christian	k1gMnSc1
Science	Science	k1gFnSc2
Monitor	monitor	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Muslims	Muslims	k1gInSc1
say	say	k?
their	their	k1gInSc1
faith	faitha	k1gFnPc2
growing	growing	k1gInSc1
fast	fast	k1gMnSc1
in	in	k?
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Religionnewsblog	Religionnewsblog	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gInSc1
African	African	k1gMnSc1
Jewish	Jewish	k1gInSc1
History	Histor	k1gInPc1
and	and	k?
Information	Information	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Rebecca	Rebecc	k1gInSc2
Weiner	Weinra	k1gFnPc2
Rebecca	Rebecca	k1gMnSc1
Weiner	Weiner	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Jewish	Jewish	k1gInSc1
Virtual	Virtual	k1gInSc1
Library	Librara	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.tyden.cz/rubriky/zahranici/afrika/jihoafricanky-se-boji-rodit-ve-statnich-nemocnicich_208962.html	http://www.tyden.cz/rubriky/zahranici/afrika/jihoafricanky-se-boji-rodit-ve-statnich-nemocnicich_208962.html	k1gInSc1
<g/>
↑	↑	k?
Peoples	Peoples	k1gInSc1
Budget	budget	k1gInSc1
Coalition	Coalition	k1gInSc1
Comments	Comments	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
Budget	budget	k1gInSc1
<g/>
.	.	kIx.
www.hsrc.ac.za	www.hsrc.ac.za	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
16	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Office	Office	kA
on	on	k3xPp3gMnSc1
Drugs	Drugs	k1gInSc1
and	and	k?
Crime	Crim	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Global	globat	k5eAaImAgMnS
Study	stud	k1gInPc1
on	on	k3xPp3gMnSc1
Homicide	Homicid	k1gInSc5
2019	#num#	k4
(	(	kIx(
<g/>
Vienna	Vienn	k1gInSc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Homicide	Homicid	k1gInSc5
trends	trendsa	k1gFnPc2
<g/>
,	,	kIx,
patterns	patternsa	k1gFnPc2
and	and	k?
criminal	criminat	k5eAaPmAgMnS,k5eAaImAgMnS
justice	justice	k1gFnPc4
response	response	k1gFnSc2
/	/	kIx~
Booklet	Booklet	k1gInSc1
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
25,26	25,26	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
How	How	k1gMnSc1
dangerous	dangerous	k1gMnSc1
is	is	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
17	#num#	k4
May	May	k1gMnSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sexuální	sexuální	k2eAgInPc1d1
násilí	násilí	k1gNnSc4
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
národní	národní	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
<g/>
,	,	kIx,
varoval	varovat	k5eAaImAgMnS
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-01-13	2019-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cyril	Cyril	k1gMnSc1
Ramaphosa	Ramaphosa	k1gFnSc1
urges	urges	k1gInSc4
action	action	k1gInSc1
against	against	k1gFnSc1
'	'	kIx"
<g/>
rape	rape	k1gNnSc1
crisis	crisis	k1gFnSc2
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
2019-01-12	2019-01-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SOUTH	SOUTH	kA
AFRICA	AFRICA	kA
<g/>
:	:	kIx,
One	One	k1gMnSc1
in	in	k?
four	four	k1gMnSc1
men	men	k?
rape	rape	k1gNnSc2
<g/>
↑	↑	k?
PERRY	PERRY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oprah	Oprah	k1gMnSc1
scandal	scandat	k5eAaPmAgMnS
rocks	rocks	k6eAd1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TIME	TIME	kA
<g/>
.	.	kIx.
5	#num#	k4
November	November	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
May	May	k1gMnSc1
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Baby	baba	k1gFnSc2
rapes	rapes	k1gMnSc1
shock	shock	k1gMnSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
11	#num#	k4
December	December	k1gInSc1
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
IRIN	IRIN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
SOUTH	SOUTH	kA
AFRICA	AFRICA	kA
<g/>
:	:	kIx,
Focus	Focus	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
virgin	virgin	k2eAgInSc1d1
myth	myth	k1gInSc1
and	and	k?
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
<g/>
.	.	kIx.
www.irinnews.org	www.irinnews.org	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IRIN	IRIN	kA
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
shame	shamat	k5eAaPmIp3nS
<g/>
:	:	kIx,
the	the	k?
rise	rise	k1gInSc1
of	of	k?
child	child	k6eAd1
rape	rape	k1gNnSc1
<g/>
.	.	kIx.
www.independent.co.uk	www.independent.co.uk	k1gMnSc1
<g/>
.	.	kIx.
independent	independent	k1gMnSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CHUTEL	CHUTEL	kA
<g/>
,	,	kIx,
Lynsey	Lynsey	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
gated	gated	k1gMnSc1
communities	communities	k1gMnSc1
are	ar	k1gInSc5
building	building	k1gInSc1
higher	highra	k1gFnPc2
walls	wallsa	k1gFnPc2
in	in	k?
an	an	k?
already	alreada	k1gFnSc2
divided	divided	k1gMnSc1
society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quartz	Quartz	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEMANSKI	LEMANSKI	kA
<g/>
,	,	kIx,
Charlotte	Charlott	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spaces	Spaces	k1gMnSc1
of	of	k?
Exclusivity	Exclusivita	k1gFnSc2
or	or	k?
Connection	Connection	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Linkages	Linkages	k1gInSc1
between	between	k2eAgInSc1d1
a	a	k8xC
Gated	Gated	k1gInSc1
Community	Communita	k1gFnSc2
and	and	k?
its	its	k?
Poorer	Poorer	k1gMnSc1
Neighbour	Neighbour	k1gMnSc1
in	in	k?
a	a	k8xC
Cape	capat	k5eAaImIp3nS
Town	Town	k1gMnSc1
Master	master	k1gMnSc1
Plan	plan	k1gInSc4
Development	Development	k1gInSc1
<g/>
:	:	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Urban	Urban	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
420980500495937	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Independent	independent	k1gMnSc1
Newspapers	Newspapers	k1gInSc1
Online	Onlin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
SA	SA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
woes	woes	k6eAd1
spark	spark	k1gInSc1
another	anothra	k1gFnPc2
exodus	exodus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iol	Iol	k1gFnSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
za	za	k7c4
<g/>
,	,	kIx,
6	#num#	k4
October	October	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Counting	Counting	k1gInSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
crimes	crimes	k1gInSc1
<g/>
,	,	kIx,
Mail	mail	k1gInSc1
and	and	k?
Guardian	Guardian	k1gInSc1
<g/>
,	,	kIx,
Retrieved	Retrieved	k1gInSc1
2	#num#	k4
October	Octobero	k1gNnPc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Recent	Recent	k1gInSc4
spate	spate	k5eAaPmIp2nP
of	of	k?
farm	farm	k6eAd1
attacks	attacks	k6eAd1
dispels	dispels	k1gInSc1
‘	‘	k?
<g/>
racially	racialla	k1gFnSc2
motivated	motivated	k1gMnSc1
<g/>
’	’	k?
rhetoric	rhetoric	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
South	South	k1gMnSc1
African	African	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-25	2019-04-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Are	ar	k1gInSc5
protesters	protesters	k6eAd1
right	right	k2eAgMnSc1d1
on	on	k3xPp3gMnSc1
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
farm	farm	k1gMnSc1
murder	murder	k1gMnSc1
rate	ratat	k5eAaPmIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
GREEF	GREEF	kA
<g/>
,	,	kIx,
Kimon	kimono	k1gNnPc2
de	de	k?
<g/>
;	;	kIx,
KARASZ	KARASZ	kA
<g/>
,	,	kIx,
Palko	Palko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gInSc1
Cites	Cites	k1gInSc4
False	False	k1gFnSc2
Claims	Claims	k1gInSc1
of	of	k?
Widespread	Widespread	k1gInSc1
Attacks	Attacks	k1gInSc1
on	on	k3xPp3gMnSc1
White	Whit	k1gInSc5
Farmers	Farmersa	k1gFnPc2
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
(	(	kIx(
<g/>
Published	Published	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
South	South	k1gInSc1
Africa	Africa	k1gMnSc1
rejects	rejectsa	k1gFnPc2
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tweet	tweet	k1gInSc4
on	on	k3xPp3gMnSc1
farmer	farmer	k1gMnSc1
killings	killingsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WASHINGTON	Washington	k1gInSc1
<g/>
,	,	kIx,
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
1100	#num#	k4
Connecticut	Connecticut	k1gInSc1
Ave	ave	k1gNnSc2
NW	NW	kA
Suite	Suit	k1gInSc5
1300	#num#	k4
<g/>
B	B	kA
<g/>
;	;	kIx,
DC	DC	kA
20036	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PolitiFact	PolitiFact	k1gInSc1
-	-	kIx~
Trump	Trump	k1gInSc1
tweets	tweets	k6eAd1
incorrect	incorrect	k2eAgMnSc1d1
on	on	k3xPp3gMnSc1
South	South	k1gMnSc1
African	African	k1gMnSc1
land	land	k1gMnSc1
seizures	seizures	k1gMnSc1
<g/>
,	,	kIx,
farmer	farmer	k1gMnSc1
killings	killings	k1gInSc1
<g/>
.	.	kIx.
@	@	kIx~
<g/>
politifact	politifact	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
'	'	kIx"
<g/>
More	mor	k1gInSc5
black	black	k1gInSc1
farm	farm	k1gInPc3
workers	workers	k6eAd1
are	ar	k1gInSc5
killed	killed	k1gInSc1
than	than	k1gMnSc1
white	white	k5eAaPmIp2nP
farm	farm	k6eAd1
workers	workers	k6eAd1
<g/>
'	'	kIx"
-	-	kIx~
Johan	Johan	k1gMnSc1
Burger	Burger	k1gMnSc1
<g/>
.	.	kIx.
702	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KUMWENDA	KUMWENDA	kA
<g/>
,	,	kIx,
Olivia	Olivium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farm	Farm	k1gInSc1
murders	murders	k6eAd1
highlight	highlight	k2eAgInSc1d1
apartheid	apartheid	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
toxic	toxic	k1gMnSc1
legacy	legaca	k1gFnSc2
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Farm	Farm	k1gInSc1
murders	murdersa	k1gFnPc2
decreasing	decreasing	k1gInSc1
<g/>
,	,	kIx,
says	says	k1gInSc1
Phiyega	Phiyega	k1gFnSc1
<g/>
.	.	kIx.
www.iol.co.za	www.iol.co.za	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Over	Over	k1gMnSc1
1000	#num#	k4
Boer	Boer	k1gInSc1
Farmers	Farmers	k1gInSc4
in	in	k?
South	South	k1gInSc1
Africa	Africa	k1gMnSc1
Have	Have	k1gFnPc2
Been	Been	k1gMnSc1
Murdered	Murdered	k1gMnSc1
Since	Since	k1gMnSc1
1991	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genocide	Genocid	k1gInSc5
Watch	Watch	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
30	#num#	k4
December	December	k1gInSc1
2005	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LESTRADA-JEFFERIS	LESTRADA-JEFFERIS	k1gMnSc1
<g/>
,	,	kIx,
Joyce	Joyce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Employment	Employment	k1gInSc1
trends	trends	k1gInSc4
in	in	k?
agriculture	agricultur	k1gMnSc5
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
StatsSA	StatsSA	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
98	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MCDOUGALL	MCDOUGALL	kA
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Times	Times	k1gInSc1
Online	Onlin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
White	Whit	k1gInSc5
farmers	farmersit	k5eAaPmRp2nS
'	'	kIx"
<g/>
being	being	k1gMnSc1
wiped	wiped	k1gMnSc1
out	out	k?
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Sunday	Sundaa	k1gFnSc2
Times	Timesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
28	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
www.sacc-ct.org.za	www.sacc-ct.org.za	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
24	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.policymic.com/articles/60561/18-people-get-murdered-every-day-in-south-africa-now-tell-me-how-guns-don-t-kill-people%5B%5D	http://www.policymic.com/articles/60561/18-people-get-murdered-every-day-in-south-africa-now-tell-me-how-guns-don-t-kill-people%5B%5D	k4
<g/>
↑	↑	k?
ISS	ISS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crime	Crim	k1gInSc5
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
country	country	k2eAgInSc1d1
and	and	k?
cities	cities	k1gInSc1
profile	profil	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
Security	Securit	k1gInPc4
Studies	Studies	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
South	South	k1gMnSc1
African	African	k1gMnSc1
Wine	Winus	k1gMnSc5
Guide	Guid	k1gMnSc5
<g/>
:	:	kIx,
Stellenbosch	Stellenbosch	k1gInSc1
<g/>
,	,	kIx,
Constantia	Constantia	k1gFnSc1
<g/>
,	,	kIx,
Walker	Walker	k1gMnSc1
Bay	Bay	k1gFnSc2
and	and	k?
more	mor	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thewinedoctor	Thewinedoctor	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Fotbalový	fotbalový	k2eAgInSc1d1
šampionát	šampionát	k1gInSc1
začíná	začínat	k5eAaImIp3nS
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
na	na	k7c6
africkém	africký	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
sport	sport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Savinovová	Savinovová	k1gFnSc1
přijde	přijít	k5eAaPmIp3nS
kvůli	kvůli	k7c3
dopingu	doping	k1gInSc2
o	o	k7c4
tituly	titul	k1gInPc4
z	z	k7c2
OH	OH	kA
<g/>
,	,	kIx,
MS	MS	kA
a	a	k8xC
ME	ME	kA
<g/>
,	,	kIx,
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
získá	získat	k5eAaPmIp3nS
Semenyaová	Semenyaová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Semenyaová	Semenyaová	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
testů	test	k1gInPc2
hermafrodit	hermafrodit	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemá	mít	k5eNaImIp3nS
vaječníky	vaječník	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
varlata	varle	k1gNnPc1
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bolt	Bolt	k1gMnSc1
mu	on	k3xPp3gMnSc3
gratuloval	gratulovat	k5eAaImAgMnS
a	a	k8xC
překonaný	překonaný	k2eAgMnSc1d1
rekordman	rekordman	k1gMnSc1
žasl	žasnout	k5eAaImAgMnS
<g/>
:	:	kIx,
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
masakr	masakr	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-15	2016-08-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pistorius	Pistorius	k1gMnSc1
byl	být	k5eAaImAgMnS
za	za	k7c4
zabití	zabití	k1gNnSc4
z	z	k7c2
nedbalosti	nedbalost	k1gFnSc2
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
k	k	k7c3
pěti	pět	k4xCc3
letům	let	k1gInPc3
vězení	vězení	k1gNnSc1
|	|	kIx~
Svět	svět	k1gInSc1
|	|	kIx~
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gary	Gary	k1gInPc1
Player	Player	k1gInSc1
|	|	kIx~
Biography	Biographa	k1gFnPc1
<g/>
,	,	kIx,
Titles	Titles	k1gInSc1
<g/>
,	,	kIx,
&	&	k?
Facts	Facts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnPc1
Britannica	Britannic	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.world.ru	https://www.world.r	k1gInSc2
<g/>
gby	gby	k?
<g/>
/	/	kIx~
<g/>
halloffame	halloffam	k1gInSc5
<g/>
/	/	kIx~
<g/>
inductees	inducteesa	k1gFnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HULEC	HULEC	kA
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZIMÁK	zimák	k1gInSc1
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
184	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Africká	africký	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Cape	capat	k5eAaImIp3nS
Point	pointa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
představitelů	představitel	k1gMnPc2
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
-	-	kIx~
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc1
Report	report	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BTI	BTI	kA
2010	#num#	k4
—	—	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
Country	country	k2eAgInSc4d1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gütersloh	Gütersloh	k1gMnSc1
<g/>
:	:	kIx,
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bureau	Bureau	k6eAd1
of	of	k?
African	African	k1gInSc1
Affairs	Affairs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Background	Background	k1gInSc1
Note	Note	k1gInSc4
<g/>
:	:	kIx,
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
2011-06-02	2011-06-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
South	South	k1gInSc1
Africa	Afric	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-07-05	2011-07-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
ČR	ČR	kA
v	v	k7c6
Pretorii	Pretorie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrnná	souhrnný	k2eAgFnSc1d1
teritoriální	teritoriální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Businessinfo	Businessinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-02-15	2011-02-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BUNDY	bunda	k1gFnPc1
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
J	J	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Africká	africký	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
AU	au	k0
<g/>
)	)	kIx)
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
Angola	Angola	k1gFnSc1
•	•	k?
Benin	Benin	k1gInSc1
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Burundi	Burundi	k1gNnSc4
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Eritrea	Eritrea	k1gFnSc1
•	•	k?
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Gabon	Gabon	k1gInSc1
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Guinea	guinea	k1gFnPc2
•	•	k?
Guinea-Bissau	Guinea-Bissaum	k1gNnSc6
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kapverdy	Kapverda	k1gFnSc2
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Komory	komora	k1gFnSc2
•	•	k?
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Konžská	konžský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Lesotho	Lesot	k1gMnSc2
•	•	k?
Libérie	Libérie	k1gFnSc2
•	•	k?
Libye	Libye	k1gFnSc2
•	•	k?
Madagaskar	Madagaskar	k1gInSc1
•	•	k?
Malawi	Malawi	k1gNnSc2
•	•	k?
Mali	Mali	k1gNnSc2
•	•	k?
Mauricius	Mauricius	k1gMnSc1
•	•	k?
Mauritánie	Mauritánie	k1gFnSc2
•	•	k?
Maroko	Maroko	k1gNnSc4
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc1
•	•	k?
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Rwanda	Rwanda	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Seychely	Seychely	k1gFnPc4
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Somálsko	Somálsko	k1gNnSc4
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Súdán	Súdán	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
Togo	Togo	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Zambie	Zambie	k1gFnSc1
•	•	k?
Západní	západní	k2eAgFnSc1d1
Sahara	Sahara	k1gFnSc1
•	•	k?
Zimbabwe	Zimbabwe	k1gNnSc1
</s>
<s>
G20	G20	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Commonwealth	Commonwealth	k1gInSc1
(	(	kIx(
<g/>
Společenství	společenství	k1gNnSc1
národů	národ	k1gInPc2
<g/>
)	)	kIx)
Řádné	řádný	k2eAgFnPc1d1
členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Barbados	Barbados	k1gInSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gMnSc2
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Fidži	Fidž	k1gFnSc6
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Kiribati	Kiribati	k1gFnSc2
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lesotho	Lesot	k1gMnSc2
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Malawi	Malawi	k1gNnSc4
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Mauricius	Mauricius	k1gInSc1
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Nauru	Naur	k1gInSc2
•	•	k?
Nigérie	Nigérie	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Papua	Papua	k1gFnSc1
Nová	nový	k2eAgFnSc1d1
Guinea	guinea	k1gFnSc7
•	•	k?
Rwanda	Rwanda	k1gFnSc1
•	•	k?
Samoa	Samoa	k1gFnSc1
•	•	k?
Seychely	Seychely	k1gFnPc4
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Sv.	sv.	kA
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Sv.	sv.	kA
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
Šalamounovy	Šalamounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
Tonga	Tonga	k1gFnSc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Vanuatu	Vanuat	k1gInSc2
•	•	k?
Zambie	Zambie	k1gFnSc1
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Zimbabwe	Zimbabwe	k1gFnSc1
Závislá	závislý	k2eAgFnSc1d1
území	území	k1gNnSc4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
•	•	k?
Anguilla	Anguilla	k1gFnSc1
•	•	k?
Bermudy	Bermudy	k1gFnPc4
•	•	k?
Britské	britský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Falklandy	Falklanda	k1gFnSc2
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Georgie	Georgie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgInPc1d1
Sandwichovy	Sandwichův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Montserrat	Montserrat	k1gInSc4
•	•	k?
Pitcairnovy	Pitcairnův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Svatá	svatá	k1gFnSc1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
•	•	k?
Turks	Turks	k1gInSc1
a	a	k8xC
Caicos	Caicos	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Ashmorův	Ashmorův	k2eAgInSc1d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Australské	australský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Heardův	Heardův	k2eAgMnSc1d1
a	a	k8xC
MacDonaldovy	Macdonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Ostrovy	ostrov	k1gInPc4
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
•	•	k?
Norfolk	Norfolk	k1gInSc1
•	•	k?
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Niue	Niu	k1gInSc2
•	•	k?
Rossova	Rossův	k2eAgFnSc1d1
dependence	dependence	k1gFnSc1
•	•	k?
Tokelau	Tokelaus	k1gInSc2
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Africe	Afrika	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
Angola	Angola	k1gFnSc1
•	•	k?
Benin	Benin	k1gInSc1
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Burundi	Burundi	k1gNnSc4
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Eritrea	Eritrea	k1gFnSc1
•	•	k?
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Gabon	Gabon	k1gInSc1
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Guinea	guinea	k1gFnPc2
•	•	k?
Guinea-Bissau	Guinea-Bissaum	k1gNnSc6
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kapverdy	Kapverda	k1gFnSc2
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Komory	komora	k1gFnSc2
•	•	k?
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Konžská	konžský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Lesotho	Lesot	k1gMnSc2
•	•	k?
Libérie	Libérie	k1gFnSc2
•	•	k?
Libye	Libye	k1gFnSc2
•	•	k?
Madagaskar	Madagaskar	k1gInSc1
•	•	k?
Malawi	Malawi	k1gNnSc2
•	•	k?
Mali	Mali	k1gNnSc2
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Mauricius	Mauricius	k1gMnSc1
•	•	k?
Mauritánie	Mauritánie	k1gFnSc2
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc1
•	•	k?
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Rwanda	Rwanda	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Seychely	Seychely	k1gFnPc4
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Somálsko	Somálsko	k1gNnSc4
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Súdán	Súdán	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
Togo	Togo	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Zambie	Zambie	k1gFnSc1
•	•	k?
Zimbabwe	Zimbabwe	k1gNnSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Mayotte	Mayott	k1gMnSc5
•	•	k?
Réunion	Réunion	k1gInSc4
•	•	k?
Roztroušené	roztroušený	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
)	)	kIx)
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Pantelleria	Pantellerium	k1gNnPc4
•	•	k?
Pelagické	pelagický	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
)	)	kIx)
<g/>
Jemen	Jemen	k1gInSc4
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Sokotra	Sokotra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Portugalsko	Portugalsko	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Madeira	Madeira	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Ceuta	Ceuta	k1gFnSc1
•	•	k?
Kanárské	kanárský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Melilla	Melilla	k1gFnSc1
•	•	k?
Španělské	španělský	k2eAgFnSc2d1
severoafrické	severoafrický	k2eAgFnSc2d1
državy	država	k1gFnSc2
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
<g/>
)	)	kIx)
Maroko	Maroko	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Západní	západní	k2eAgFnSc1d1
Sahara	Sahara	k1gFnSc1
<g/>
)	)	kIx)
Územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
se	se	k3xPyFc4
spornýmmezinárodním	spornýmmezinárodní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Saharská	saharský	k2eAgFnSc1d1
arabská	arabský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Somaliland	Somaliland	k1gInSc1
</s>
<s>
BRICS	BRICS	kA
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128494	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4078012-0	4078012-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2167	#num#	k4
3042	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79023005	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
159069376	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79023005	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
