<s>
Louis	Louis	k1gMnSc1
Claude	Claud	k1gInSc5
Marie	Maria	k1gFnSc2
Richard	Richard	k1gMnSc1
</s>
<s>
Louis	Louis	k1gMnSc1
Claude	Claud	k1gInSc5
Marie	Maria	k1gFnSc2
Richard	Richard	k1gMnSc1
Narození	narození	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1754	#num#	k4
<g/>
Versailles	Versailles	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1821	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
66	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Paříž	Paříž	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
botanik	botanik	k1gMnSc1
<g/>
,	,	kIx,
pteridolog	pteridolog	k1gMnSc1
<g/>
,	,	kIx,
bryolog	bryolog	k1gMnSc1
a	a	k8xC
botanický	botanický	k2eAgMnSc1d1
ilustrátor	ilustrátor	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Achille	Achilles	k1gMnSc5
Richard	Richard	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Claude	Claude	k6eAd1
Richard	Richard	k1gMnSc1
(	(	kIx(
<g/>
dědeček	dědeček	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Louis	Louis	k1gMnSc1
Claude	Claude	k1gInSc1
Marie	Marie	k1gFnSc1
Richard	Richard	k1gMnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1754	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Auteuil	Auteuil	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1821	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
botanik	botanik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Richard	Richard	k1gMnSc1
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
;	;	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
předků	předek	k1gMnPc2
pečoval	pečovat	k5eAaImAgInS
o	o	k7c4
ménagerii	ménagerie	k1gFnSc4
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
..	..	k?
Odcestoval	odcestovat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1781	#num#	k4
na	na	k7c4
Guynanu	Guynana	k1gFnSc4
a	a	k8xC
Antily	Antily	k1gFnPc4
a	a	k8xC
zde	zde	k6eAd1
popsal	popsat	k5eAaPmAgMnS
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
<g/>
,	,	kIx,
rod	rod	k1gInSc1
orchidejí	orchidea	k1gFnPc2
Liparis	Liparis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Německý	německý	k2eAgMnSc1d1
botanik	botanik	k1gMnSc1
Carl	Carl	k1gMnSc1
Sigismund	Sigismund	k1gMnSc1
Kunth	Kunth	k1gMnSc1
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenoval	pojmenovat	k5eAaPmAgInS
rostlinný	rostlinný	k2eAgInSc1d1
rod	rod	k1gInSc1
Richardia	Richardium	k1gNnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
Araceae	Aracea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Louis	Louis	k1gMnSc1
Claude	Claud	k1gInSc5
Marie	Marie	k1gFnSc1
Richard	Richard	k1gMnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
Mezinárodním	mezinárodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
jmen	jméno	k1gNnPc2
rostlin	rostlina	k1gFnPc2
zkratku	zkratka	k1gFnSc4
Rich	Richa	k1gFnPc2
<g/>
..	..	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
zkratka	zkratka	k1gFnSc1
L.C.	L.C.	k1gMnSc1
<g/>
Rich	Rich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
Achille	Achille	k1gMnSc1
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1794	#num#	k4
<g/>
–	–	k?
<g/>
1852	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
také	také	k9
botanik	botanik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Demonstrations	Demonstrations	k6eAd1
botaniques	botaniques	k1gInSc1
<g/>
,	,	kIx,
ou	ou	k0
analyse	analysa	k1gFnSc6
du	du	k?
fruit	fruita	k1gFnPc2
<g/>
,	,	kIx,
1808	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Analyse	analysa	k1gFnSc3
botanique	botaniquat	k5eAaPmIp3nS
des	des	k1gNnSc1
embryons	embryonsa	k1gFnPc2
endorhizes	endorhizesa	k1gFnPc2
ou	ou	k0
monocotylédonés	monocotylédonés	k6eAd1
<g/>
,	,	kIx,
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
De	De	k?
Orchideis	Orchideis	k1gInSc1
europaeis	europaeis	k1gInSc1
annotationes	annotationes	k1gInSc1
…	…	k?
<g/>
,	,	kIx,
1817	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Commentatio	Commentatio	k1gMnSc1
botanica	botanica	k1gMnSc1
de	de	k?
Coniferis	Coniferis	k1gInSc1
et	et	k?
Cycadeis	Cycadeis	k1gInSc1
<g/>
,	,	kIx,
1826	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
De	De	k?
Musaceis	Musaceis	k1gInSc1
commentatio	commentatio	k1gMnSc1
botanica	botanica	k1gMnSc1
…	…	k?
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
1831	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Zander	Zander	k1gMnSc1
<g/>
,	,	kIx,
Fritz	Fritz	k1gMnSc1
Encke	Encke	k1gFnPc2
<g/>
,	,	kIx,
Günther	Günthra	k1gFnPc2
Buchheim	Buchheima	k1gFnPc2
<g/>
,	,	kIx,
Siegmund	Siegmund	k1gMnSc1
Seybold	Seybold	k1gMnSc1
<g/>
:	:	kIx,
Handwörterbuch	Handwörterbuch	k1gMnSc1
der	drát	k5eAaImRp2nS
Pflanzennamen	Pflanzennamen	k1gInSc1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eugen	Eugen	k2eAgInSc1d1
Ulmer	Ulmer	k1gInSc1
<g/>
,	,	kIx,
Stuttgart	Stuttgart	k1gInSc1
1984	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8001	#num#	k4
<g/>
-	-	kIx~
<g/>
5042	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zander	Zander	k1gInSc1
udává	udávat	k5eAaImIp3nS
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1754	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRUMMITT	BRUMMITT	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Kenneth	Kenneth	k1gMnSc1
<g/>
;	;	kIx,
POWELL	POWELL	kA
<g/>
,	,	kIx,
C.	C.	kA
E.	E.	kA
Authors	Authorsa	k1gFnPc2
of	of	k?
Plant	planta	k1gFnPc2
Names	Names	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kew	Kew	k1gFnSc1
<g/>
:	:	kIx,
Royal	Royal	k1gInSc1
Botanical	Botanical	k1gFnSc2
Gardens	Gardensa	k1gFnPc2
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84246	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
85	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
jmen	jméno	k1gNnPc2
rostlin	rostlina	k1gFnPc2
je	být	k5eAaImIp3nS
zapracován	zapracovat	k5eAaPmNgMnS
do	do	k7c2
seznamu	seznam	k1gInSc2
botaniků	botanik	k1gMnPc2
a	a	k8xC
mykologů	mykolog	k1gMnPc2
dle	dle	k7c2
zkratek	zkratka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Louis	Louis	k1gMnSc1
Claude	Claud	k1gInSc5
Marie	Maria	k1gFnSc2
Richard	Richard	k1gMnSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002159468	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
117527599	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2139	#num#	k4
2830	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83188728	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
73844203	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83188728	#num#	k4
</s>
