<s>
Mumlava	Mumlava	k1gFnSc1	Mumlava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Mummel	Mummel	k1gInSc1	Mummel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
horská	horský	k2eAgFnSc1d1	horská
řeka	řeka	k1gFnSc1	řeka
protékající	protékající	k2eAgFnSc4d1	protékající
Krkonošemi	Krkonoše	k1gFnPc7	Krkonoše
a	a	k8xC	a
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
Jizery	Jizera	k1gFnSc2	Jizera
hraničící	hraničící	k2eAgInPc1d1	hraničící
s	s	k7c7	s
Jizerskými	jizerský	k2eAgFnPc7d1	Jizerská
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Semily	Semily	k1gInPc1	Semily
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
12,2	[number]	k4	12,2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
51,6	[number]	k4	51,6
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Velká	velký	k2eAgFnSc1d1	velká
Mumlava	Mumlava	k1gFnSc1	Mumlava
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
Harrachově	Harrachov	k1gInSc6	Harrachov
louce	louka	k1gFnSc6	louka
pod	pod	k7c7	pod
vrcholy	vrchol	k1gInPc1	vrchol
Kotel	kotel	k1gInSc1	kotel
a	a	k8xC	a
Harrachovy	Harrachov	k1gInPc1	Harrachov
kameny	kámen	k1gInPc7	kámen
u	u	k7c2	u
turistického	turistický	k2eAgInSc2d1	turistický
rozcestníku	rozcestník	k1gInSc2	rozcestník
"	"	kIx"	"
<g/>
U	u	k7c2	u
Růženčiny	Růženčin	k2eAgFnSc2d1	Růženčina
zahrádky	zahrádka	k1gFnSc2	zahrádka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Malá	malý	k2eAgFnSc1d1	malá
Mumlava	Mumlava	k1gFnSc1	Mumlava
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
Mumlavské	Mumlavský	k2eAgFnSc6d1	Mumlavská
louce	louka	k1gFnSc6	louka
na	na	k7c4	na
úpatí	úpatí	k1gNnSc4	úpatí
hor	hora	k1gFnPc2	hora
Sokolník	sokolník	k1gMnSc1	sokolník
a	a	k8xC	a
Violík	Violík	k1gMnSc1	Violík
u	u	k7c2	u
turistického	turistický	k2eAgInSc2d1	turistický
rozcestníku	rozcestník	k1gInSc2	rozcestník
"	"	kIx"	"
<g/>
Labská	labský	k2eAgFnSc1d1	Labská
louka	louka	k1gFnSc1	louka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pramen	pramen	k1gInSc1	pramen
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
řeky	řeka	k1gFnPc1	řeka
pramení	pramenit	k5eAaImIp3nP	pramenit
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
cca	cca	kA	cca
1370	[number]	k4	1370
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
Řeky	řeka	k1gFnSc2	řeka
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
náhorní	náhorní	k2eAgFnSc2d1	náhorní
pláně	pláň	k1gFnSc2	pláň
protékají	protékat	k5eAaImIp3nP	protékat
kaňonovitým	kaňonovitý	k2eAgNnSc7d1	kaňonovité
údolím	údolí	k1gNnSc7	údolí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
důl	důl	k1gInSc1	důl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
turistického	turistický	k2eAgInSc2d1	turistický
rozcestníku	rozcestník	k1gInSc2	rozcestník
"	"	kIx"	"
<g/>
Krakonošova	Krakonošův	k2eAgFnSc1d1	Krakonošova
snídaně	snídaně	k1gFnSc1	snídaně
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1030	[number]	k4	1030
m	m	kA	m
n.	n.	k?	n.
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
řeky	řeka	k1gFnPc1	řeka
spojí	spojit	k5eAaPmIp3nP	spojit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
níže	nízce	k6eAd2	nízce
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přidávají	přidávat	k5eAaImIp3nP	přidávat
potoky	potok	k1gInPc4	potok
Smrková	smrkový	k2eAgFnSc1d1	smrková
strouha	strouha	k1gFnSc1	strouha
a	a	k8xC	a
Vosecký	Vosecký	k1gMnSc1	Vosecký
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
kilometr	kilometr	k1gInSc4	kilometr
před	před	k7c7	před
Harrachovem	Harrachov	k1gInSc7	Harrachov
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
turistický	turistický	k2eAgInSc4d1	turistický
cíl	cíl	k1gInSc4	cíl
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
8	[number]	k4	8
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
protéká	protékat	k5eAaImIp3nS	protékat
jižní	jižní	k2eAgFnSc7d1	jižní
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
Harrachov	Harrachov	k1gInSc1	Harrachov
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
ústí	ústit	k5eAaImIp3nS	ústit
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
Jizery	Jizera	k1gFnSc2	Jizera
blízko	blízko	k7c2	blízko
hotýlku	hotýlek	k1gInSc2	hotýlek
Na	na	k7c6	na
Mýtě	mýto	k1gNnSc6	mýto
pod	pod	k7c7	pod
Kořenovem	Kořenov	k1gInSc7	Kořenov
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pstruhovou	pstruhový	k2eAgFnSc4d1	pstruhová
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
od	od	k7c2	od
Harrachova	Harrachov	k1gInSc2	Harrachov
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
je	být	k5eAaImIp3nS	být
experty	expert	k1gMnPc4	expert
využíván	využíván	k2eAgInSc1d1	využíván
pro	pro	k7c4	pro
vodáckou	vodácký	k2eAgFnSc4d1	vodácká
turistiku	turistika	k1gFnSc4	turistika
<g/>
.	.	kIx.	.
zleva	zleva	k6eAd1	zleva
–	–	k?	–
Ryzí	ryzí	k2eAgInSc4d1	ryzí
potok	potok	k1gInSc4	potok
zprava	zprava	k6eAd1	zprava
–	–	k?	–
Smrková	smrkový	k2eAgFnSc1d1	smrková
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
Vosecký	Vosecký	k1gMnSc1	Vosecký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Lubošská	Lubošský	k2eAgFnSc1d1	Lubošský
bystřina	bystřina	k1gFnSc1	bystřina
<g/>
,	,	kIx,	,
Orlí	orlí	k2eAgFnSc1d1	orlí
ručej	ručej	k1gFnSc1	ručej
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Milnice	Milnika	k1gFnSc6	Milnika
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
1,82	[number]	k4	1,82
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
</s>
