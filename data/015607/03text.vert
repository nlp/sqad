<s>
Bedřich	Bedřich	k1gMnSc1
Frejka	Frejka	k1gFnSc1
(	(	kIx(
<g/>
ortoped	ortoped	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
MUDr.	MUDr.	kA
Bedřich	Bedřich	k1gMnSc1
Frejka	Frejka	k1gFnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
Narození	narození	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1890	#num#	k4
<g/>
Útěchovice	Útěchovice	k1gFnSc2
pod	pod	k7c4
StražištěmRakousko-Uhersko	StražištěmRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
81	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
BrnoČeskoslovensko	BrnoČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgNnSc2d1
Povolání	povolání	k1gNnSc2
</s>
<s>
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
a	a	k8xC
chirurg	chirurg	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Frejka	Frejka	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Frejka	Frejka	k1gFnSc1
(	(	kIx(
<g/>
chemik	chemik	k1gMnSc1
<g/>
)	)	kIx)
bratr	bratr	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Frejka	Frejka	k1gFnSc1
bratr	bratr	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
MUDr.	MUDr.	kA
Bedřich	Bedřich	k1gMnSc1
Frejka	Frejka	k1gFnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1890	#num#	k4
<g/>
,	,	kIx,
Útěchovice	Útěchovice	k1gFnSc1
pod	pod	k7c7
Stražištěm	stražiště	k1gNnSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
a	a	k8xC
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
specialista	specialista	k1gMnSc1
ortopedie	ortopedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
lesnické	lesnický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
na	na	k7c6
Lékařské	lékařský	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byl	být	k5eAaImAgInS
válečným	válečný	k2eAgMnSc7d1
chirurgem	chirurg	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
lékařské	lékařský	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
jako	jako	k9
chirurg	chirurg	k1gMnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
jako	jako	k8xS,k8xC
ortoped	ortoped	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
zde	zde	k6eAd1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
docenta	docent	k1gMnSc2
a	a	k8xC
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Brna	Brno	k1gNnSc2
na	na	k7c4
lékařskou	lékařský	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Masarykovy	Masarykův	k2eAgFnSc2d1
Univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	s	k7c7
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1933	#num#	k4
stal	stát	k5eAaPmAgInS
přednostou	přednosta	k1gMnSc7
nově	nově	k6eAd1
otevřené	otevřený	k2eAgFnSc2d1
ortopedické	ortopedický	k2eAgFnSc2d1
kliniky	klinika	k1gFnSc2
Lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
Univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přínos	přínos	k1gInSc1
</s>
<s>
Frejkova	Frejkův	k2eAgFnSc1d1
peřinka	peřinka	k1gFnSc1
–	–	k?
tzv.	tzv.	kA
široké	široký	k2eAgNnSc1d1
balení	balení	k1gNnSc1
pro	pro	k7c4
léčbu	léčba	k1gFnSc4
vývojové	vývojový	k2eAgFnSc2d1
vady	vada	k1gFnSc2
kyčlí	kyčel	k1gFnPc2
(	(	kIx(
<g/>
dysplazie	dysplazie	k1gFnSc1
<g/>
)	)	kIx)
u	u	k7c2
novorozenců	novorozenec	k1gMnPc2
a	a	k8xC
kojenců	kojenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
účinnější	účinný	k2eAgMnSc1d2
než	než	k8xS
široké	široký	k2eAgNnSc1d1
balení	balení	k1gNnSc1
z	z	k7c2
plen	plena	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
stlačí	stlačit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
získala	získat	k5eAaPmAgFnS
pojmenovaní	pojmenovaný	k2eAgMnPc1d1
po	po	k7c6
něm	on	k3xPp3gMnSc6
a	a	k8xC
stále	stále	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
Velká	velká	k1gFnSc1
Chyška	chyška	k1gFnSc1
↑	↑	k?
Matrika	matrika	k1gFnSc1
doktorů	doktor	k1gMnPc2
české	český	k2eAgFnSc2d1
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Bedřich	Bedřich	k1gMnSc1
Frejka	Frejka	k1gFnSc1
(	(	kIx(
<g/>
ortoped	ortoped	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Frejka	Frejka	k1gFnSc1
Bedřich	Bedřich	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1031808	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5805	#num#	k4
7983	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83711907	#num#	k4
</s>
