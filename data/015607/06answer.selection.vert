<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
lesnické	lesnický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
na	na	k7c6
Lékařské	lékařský	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>