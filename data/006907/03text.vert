<s>
Morelia	Morelia	k1gFnSc1	Morelia
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Michoacán	Michoacán	k2eAgMnSc1d1	Michoacán
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Guayangareo	Guayangareo	k6eAd1	Guayangareo
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Morelia	Morelia	k1gFnSc1	Morelia
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
a	a	k8xC	a
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
Michoacánu	Michoacán	k2eAgFnSc4d1	Michoacán
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
20	[number]	k4	20
největších	veliký	k2eAgNnPc2d3	veliký
mexických	mexický	k2eAgNnPc2d1	mexické
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
žilo	žít	k5eAaImAgNnS	žít
597	[number]	k4	597
511	[number]	k4	511
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
okolo	okolo	k7c2	okolo
Morelie	Morelie	k1gFnSc2	Morelie
pak	pak	k6eAd1	pak
806	[number]	k4	806
822	[number]	k4	822
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Michoacánu	Michoacán	k2eAgFnSc4d1	Michoacán
představuje	představovat	k5eAaImIp3nS	představovat
Morelia	Morelius	k1gMnSc4	Morelius
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc4	jeho
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
politické	politický	k2eAgNnSc4d1	politické
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
a	a	k8xC	a
společenské	společenský	k2eAgNnSc4d1	společenské
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
18	[number]	k4	18
mexických	mexický	k2eAgFnPc2d1	mexická
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
prvního	první	k4xOgMnSc2	první
místokrále	místokrál	k1gMnSc2	místokrál
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Španělsku	Španělsko	k1gNnSc6	Španělsko
Antoniem	Antonio	k1gMnSc7	Antonio
de	de	k?	de
Mendoza	Mendoza	k1gFnSc1	Mendoza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předkolumbovském	předkolumbovský	k2eAgNnSc6d1	předkolumbovské
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
budoucí	budoucí	k2eAgFnSc2d1	budoucí
Morelie	Morelie	k1gFnSc2	Morelie
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Guayangareo	Guayangareo	k1gNnSc1	Guayangareo
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
rocích	rok	k1gInPc6	rok
evropské	evropský	k2eAgFnSc2d1	Evropská
kolonizace	kolonizace	k1gFnSc2	kolonizace
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Mechuacán	Mechuacán	k2eAgInSc1d1	Mechuacán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1545	[number]	k4	1545
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Valladolid	Valladolid	k1gInSc4	Valladolid
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
španělského	španělský	k2eAgNnSc2d1	španělské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Morelia	Morelius	k1gMnSc2	Morelius
dostalo	dostat	k5eAaPmAgNnS	dostat
město	město	k1gNnSc1	město
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
mexické	mexický	k2eAgFnSc2d1	mexická
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
zdejším	zdejší	k2eAgMnSc6d1	zdejší
rodákovi	rodák	k1gMnSc6	rodák
-	-	kIx~	-
Josém	José	k1gNnSc6	José
María	Marí	k1gInSc2	Marí
Morelosovi	Morelos	k1gMnSc3	Morelos
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
figuruje	figurovat	k5eAaImIp3nS	figurovat
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
Morelie	Morelie	k1gFnSc2	Morelie
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
kulturního	kulturní	k2eAgNnSc2d1	kulturní
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
ukázkovým	ukázkový	k2eAgInSc7d1	ukázkový
příkladem	příklad	k1gInSc7	příklad
španělské	španělský	k2eAgFnSc2d1	španělská
renesanční	renesanční	k2eAgFnSc2d1	renesanční
architektury	architektura	k1gFnSc2	architektura
obohacené	obohacený	k2eAgFnSc2d1	obohacená
o	o	k7c6	o
prvky	prvek	k1gInPc7	prvek
původního	původní	k2eAgNnSc2d1	původní
mezoamerického	mezoamerický	k2eAgNnSc2d1	mezoamerický
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zástavba	zástavba	k1gFnSc1	zástavba
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
stavby	stavba	k1gFnPc4	stavba
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
či	či	k8xC	či
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Morelii	Morelie	k1gFnSc6	Morelie
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
mexického	mexický	k2eAgInSc2d1	mexický
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Agustín	Agustín	k1gInSc1	Agustín
de	de	k?	de
Iturbide	Iturbid	k1gInSc5	Iturbid
-	-	kIx~	-
první	první	k4xOgMnSc1	první
mexický	mexický	k2eAgMnSc1d1	mexický
císař	císař	k1gMnSc1	císař
José	José	k1gNnSc2	José
María	María	k1gMnSc1	María
Morelos	Morelos	k1gMnSc1	Morelos
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
mexické	mexický	k2eAgFnSc2d1	mexická
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Felipe	Felip	k1gInSc5	Felip
Calderón	Calderón	k1gMnSc1	Calderón
-	-	kIx~	-
mexický	mexický	k2eAgMnSc1d1	mexický
prezident	prezident	k1gMnSc1	prezident
</s>
