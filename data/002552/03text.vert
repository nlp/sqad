<s>
Valentin	Valentin	k1gMnSc1	Valentin
Louis	Louis	k1gMnSc1	Louis
Georges	Georges	k1gMnSc1	Georges
Eugè	Eugè	k1gMnSc1	Eugè
Marcel	Marcel	k1gMnSc1	Marcel
Proust	Proust	k1gMnSc1	Proust
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
maʁ	maʁ	k?	maʁ
pʁ	pʁ	k?	pʁ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
Auteuil	Auteuil	k1gInSc1	Auteuil
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
monumentálním	monumentální	k2eAgInSc7d1	monumentální
cyklem	cyklus	k1gInSc7	cyklus
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
dílech	dílo	k1gNnPc6	dílo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1913	[number]	k4	1913
až	až	k9	až
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Marcel	Marcel	k1gMnSc1	Marcel
Proust	Proust	k1gMnSc1	Proust
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
v	v	k7c4	v
Auteuil	Auteuil	k1gInSc4	Auteuil
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
pařížském	pařížský	k2eAgInSc6d1	pařížský
obvodu	obvod	k1gInSc6	obvod
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svého	svůj	k3xOyFgMnSc2	svůj
prastrýce	prastrýc	k1gMnSc2	prastrýc
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
frankfurtská	frankfurtský	k2eAgFnSc1d1	Frankfurtská
smlouva	smlouva	k1gFnSc1	smlouva
formálně	formálně	k6eAd1	formálně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
francouzsko-pruskou	francouzskoruský	k2eAgFnSc4d1	francouzsko-pruská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
narození	narození	k1gNnSc1	narození
bylo	být	k5eAaImAgNnS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
násilnostmi	násilnost	k1gFnPc7	násilnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
provázely	provázet	k5eAaImAgFnP	provázet
potlačení	potlačení	k1gNnSc4	potlačení
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
dětství	dětství	k1gNnSc2	dětství
spadalo	spadat	k5eAaImAgNnS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
konsolidace	konsolidace	k1gFnSc2	konsolidace
Třetí	třetí	k4xOgFnSc2	třetí
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
motivů	motiv	k1gInPc2	motiv
z	z	k7c2	z
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
společenských	společenský	k2eAgFnPc2d1	společenská
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
úpadku	úpadek	k1gInSc2	úpadek
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
vzestupu	vzestup	k1gInSc2	vzestup
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c4	v
Francii	Francie	k1gFnSc4	Francie
během	během	k7c2	během
Třetí	třetí	k4xOgFnSc2	třetí
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
fin	fin	k?	fin
de	de	k?	de
siè	siè	k?	siè
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Achille	Achilles	k1gMnSc5	Achilles
Adrien	Adriena	k1gFnPc2	Adriena
Proust	Proust	k1gMnSc1	Proust
byl	být	k5eAaImAgMnS	být
prominentní	prominentní	k2eAgMnSc1d1	prominentní
patolog	patolog	k1gMnSc1	patolog
a	a	k8xC	a
epidemiolog	epidemiolog	k1gMnSc1	epidemiolog
<g/>
,	,	kIx,	,
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
opatření	opatření	k1gNnPc4	opatření
proti	proti	k7c3	proti
šíření	šíření	k1gNnSc3	šíření
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
mnoha	mnoho	k4c2	mnoho
článků	článek	k1gInPc2	článek
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Jeanne	Jeann	k1gInSc5	Jeann
Clémence	Clémenec	k1gInPc4	Clémenec
Weilová	Weilová	k1gFnSc1	Weilová
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
a	a	k8xC	a
kultivované	kultivovaný	k2eAgFnSc2d1	kultivovaná
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Alsaska	Alsasko	k1gNnSc2	Alsasko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
gramotná	gramotný	k2eAgFnSc1d1	gramotná
a	a	k8xC	a
sečtělá	sečtělý	k2eAgFnSc1d1	sečtělá
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
dopisy	dopis	k1gInPc1	dopis
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
znalosti	znalost	k1gFnSc3	znalost
angličtiny	angličtina	k1gFnSc2	angličtina
mohla	moct	k5eAaImAgFnS	moct
pomáhat	pomáhat	k5eAaImF	pomáhat
synovi	syn	k1gMnSc3	syn
s	s	k7c7	s
překládáním	překládání	k1gNnSc7	překládání
próz	próza	k1gFnPc2	próza
Johna	John	k1gMnSc2	John
Ruskina	Ruskin	k1gMnSc2	Ruskin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
postihla	postihnout	k5eAaPmAgFnS	postihnout
Prousta	Prousta	k1gFnSc1	Prousta
první	první	k4xOgFnSc1	první
vážná	vážný	k2eAgFnSc1d1	vážná
ataka	ataka	k1gFnSc1	ataka
astmatu	astma	k1gNnSc2	astma
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
neduživé	duživý	k2eNgNnSc4d1	neduživé
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Proust	Proust	k1gMnSc1	Proust
trávíval	trávívat	k5eAaImAgMnS	trávívat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
prázdniny	prázdniny	k1gFnPc4	prázdniny
v	v	k7c4	v
Illiers	Illiers	k1gInSc4	Illiers
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vesnice	vesnice	k1gFnSc1	vesnice
a	a	k8xC	a
také	také	k9	také
dům	dům	k1gInSc4	dům
Proustova	Proustův	k2eAgMnSc2d1	Proustův
prastrýce	prastrýc	k1gMnSc2	prastrýc
v	v	k7c4	v
Auteuil	Auteuil	k1gInSc4	Auteuil
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
smyšlené	smyšlený	k2eAgNnSc4d1	smyšlené
město	město	k1gNnSc4	město
Combray	Combraa	k1gFnSc2	Combraa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
některé	některý	k3yIgFnPc1	některý
důležité	důležitý	k2eAgFnPc1d1	důležitá
scény	scéna	k1gFnPc1	scéna
z	z	k7c2	z
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgMnSc2	stý
výročí	výročí	k1gNnSc3	výročí
Proustova	Proustův	k2eAgNnSc2d1	Proustovo
narození	narození	k1gNnSc2	narození
bylo	být	k5eAaImAgNnS	být
Illiers	Illiers	k1gInSc1	Illiers
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Illiers-Combray	Illiers-Combra	k1gMnPc4	Illiers-Combra
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Proust	Proust	k1gMnSc1	Proust
stal	stát	k5eAaPmAgMnS	stát
žákem	žák	k1gMnSc7	žák
prestižního	prestižní	k2eAgInSc2d1	prestižní
Lycée	Lycé	k1gInSc2	Lycé
Condorcet	Condorcet	k1gFnSc2	Condorcet
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
často	často	k6eAd1	často
chyběl	chybět	k5eAaImAgMnS	chybět
<g/>
;	;	kIx,	;
přesto	přesto	k8xC	přesto
vynikal	vynikat	k5eAaImAgMnS	vynikat
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
spolužákům	spolužák	k1gMnPc3	spolužák
získal	získat	k5eAaPmAgInS	získat
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
salonů	salon	k1gInPc2	salon
vyšší	vysoký	k2eAgFnSc2d2	vyšší
buržoazie	buržoazie	k1gFnSc2	buržoazie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
bohatý	bohatý	k2eAgInSc4d1	bohatý
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
Hledání	hledání	k1gNnSc4	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
chabé	chabý	k2eAgNnSc4d1	chabé
zdraví	zdraví	k1gNnSc4	zdraví
Proust	Proust	k1gMnSc1	Proust
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
sloužil	sloužit	k5eAaImAgInS	sloužit
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
armádě	armáda	k1gFnSc6	armáda
u	u	k7c2	u
76	[number]	k4	76
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
v	v	k7c4	v
Orléans	Orléans	k1gInSc4	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
Zážitek	zážitek	k1gInSc4	zážitek
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
epizodu	epizoda	k1gFnSc4	epizoda
ve	v	k7c6	v
Světě	svět	k1gInSc6	svět
Guermantových	Guermantův	k2eAgFnPc2d1	Guermantův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
salony	salon	k1gInPc4	salon
Madame	madame	k1gFnSc2	madame
Straus	Straus	k1gInSc1	Straus
(	(	kIx(	(
<g/>
vdovy	vdova	k1gFnPc1	vdova
po	po	k7c6	po
Georgesu	Georges	k1gInSc6	Georges
Bizetovi	Bizetův	k2eAgMnPc1d1	Bizetův
a	a	k8xC	a
matky	matka	k1gFnPc1	matka
Proustova	Proustův	k2eAgMnSc2d1	Proustův
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Jacquese	Jacques	k1gMnSc2	Jacques
Bizeta	Bizet	k1gMnSc2	Bizet
<g/>
)	)	kIx)	)
a	a	k8xC	a
Madame	madame	k1gFnSc1	madame
de	de	k?	de
Arman	Arman	k1gMnSc1	Arman
Caillavet	Caillavet	k1gMnSc1	Caillavet
(	(	kIx(	(
<g/>
matku	matka	k1gFnSc4	matka
jeho	jeho	k3xOp3gMnSc2	jeho
přítele	přítel	k1gMnSc2	přítel
Gastona	Gaston	k1gMnSc2	Gaston
Armana	Arman	k1gMnSc2	Arman
de	de	k?	de
Caillavet	Caillavet	k1gInSc1	Caillavet
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnSc2	jehož
snoubenky	snoubenka	k1gFnSc2	snoubenka
Jeanne	Jeann	k1gInSc5	Jeann
Pouquetové	Pouquetový	k2eAgNnSc4d1	Pouquetový
byl	být	k5eAaImAgMnS	být
zamilován	zamilován	k2eAgMnSc1d1	zamilován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
paní	paní	k1gFnSc4	paní
Arman	Armana	k1gFnPc2	Armana
de	de	k?	de
Caillavet	Caillavet	k1gMnSc1	Caillavet
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Anatolem	Anatol	k1gInSc7	Anatol
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
milencem	milenec	k1gMnSc7	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
věnovat	věnovat	k5eAaImF	věnovat
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
,	,	kIx,	,
Proust	Proust	k1gFnSc4	Proust
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
získal	získat	k5eAaPmAgInS	získat
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Mazarinově	Mazarinův	k2eAgFnSc6d1	Mazarinova
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
ale	ale	k8xC	ale
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
nemocenské	mocenský	k2eNgFnSc6d1	mocenský
neschopence	neschopenka	k1gFnSc6	neschopenka
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
knihovny	knihovna	k1gFnSc2	knihovna
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Proust	Proust	k1gMnSc1	Proust
byl	být	k5eAaImAgMnS	být
skrytý	skrytý	k2eAgMnSc1d1	skrytý
homosexuál	homosexuál	k1gMnSc1	homosexuál
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
evropských	evropský	k2eAgMnPc2d1	evropský
romanopisců	romanopisec	k1gMnPc2	romanopisec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
osobami	osoba	k1gFnPc7	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
otevřeně	otevřeně	k6eAd1	otevřeně
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
a	a	k8xC	a
rodinné	rodinný	k2eAgNnSc1d1	rodinné
prostředí	prostředí	k1gNnSc1	prostředí
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
změnily	změnit	k5eAaPmAgInP	změnit
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
1900	[number]	k4	1900
a	a	k8xC	a
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1903	[number]	k4	1903
se	se	k3xPyFc4	se
Proustův	Proustův	k2eAgMnSc1d1	Proustův
bratr	bratr	k1gMnSc1	bratr
Robert	Robert	k1gMnSc1	Robert
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
opustil	opustit	k5eAaPmAgMnS	opustit
rodinný	rodinný	k2eAgInSc4d1	rodinný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
měl	mít	k5eAaImAgInS	mít
Proust	Proust	k1gFnSc4	Proust
silnou	silný	k2eAgFnSc4d1	silná
citovou	citový	k2eAgFnSc4d1	citová
vazbu	vazba	k1gFnSc4	vazba
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
a	a	k8xC	a
zanechala	zanechat	k5eAaPmAgFnS	zanechat
mu	on	k3xPp3gMnSc3	on
značné	značný	k2eAgNnSc1d1	značné
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
vystačilo	vystačit	k5eAaBmAgNnS	vystačit
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Proustovo	Proustův	k2eAgNnSc1d1	Proustovo
zdraví	zdraví	k1gNnSc1	zdraví
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ložnici	ložnice	k1gFnSc6	ložnice
obložené	obložený	k2eAgFnSc6d1	obložená
korkem	korek	k1gInSc7	korek
<g/>
;	;	kIx,	;
během	během	k7c2	během
dne	den	k1gInSc2	den
spal	spát	k5eAaImAgMnS	spát
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
svého	svůj	k3xOyFgInSc2	svůj
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Proust	Proust	k1gMnSc1	Proust
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
literárních	literární	k2eAgInPc2d1	literární
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
a	a	k8xC	a
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
publikoval	publikovat	k5eAaBmAgMnS	publikovat
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
školních	školní	k2eAgFnPc2d1	školní
studií	studie	k1gFnPc2	studie
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Revue	revue	k1gFnSc2	revue
verte	verte	k1gNnSc2	verte
a	a	k8xC	a
La	la	k1gNnSc2	la
Revue	revue	k1gFnSc2	revue
lilas	lilas	k1gMnSc1	lilas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
Proust	Proust	k1gInSc1	Proust
otiskoval	otiskovat	k5eAaImAgInS	otiskovat
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
společenský	společenský	k2eAgInSc1d1	společenský
sloupek	sloupek	k1gInSc1	sloupek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Le	Le	k1gMnSc1	Le
Mensuel	Mensuel	k1gMnSc1	Mensuel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
literární	literární	k2eAgFnSc2d1	literární
revue	revue	k1gFnSc2	revue
Le	Le	k1gMnSc1	Le
Banquet	Banquet	k1gMnSc1	Banquet
a	a	k8xC	a
během	během	k7c2	během
několika	několik	k4yIc2	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
Proust	Proust	k1gMnSc1	Proust
pravidelně	pravidelně	k6eAd1	pravidelně
publikoval	publikovat	k5eAaBmAgMnS	publikovat
své	svůj	k3xOyFgFnPc4	svůj
drobné	drobný	k2eAgFnPc4d1	drobná
prózy	próza	k1gFnPc4	próza
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
časopise	časopis	k1gInSc6	časopis
a	a	k8xC	a
v	v	k7c4	v
prestižní	prestižní	k2eAgNnSc4d1	prestižní
La	la	k1gNnSc4	la
Revue	revue	k1gFnSc2	revue
Blanche	Blanch	k1gInSc2	Blanch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Les	les	k1gInSc1	les
Plaisirs	Plaisirsa	k1gFnPc2	Plaisirsa
et	et	k?	et
les	les	k1gInSc1	les
Jours	Jours	k1gInSc1	Jours
(	(	kIx(	(
<g/>
Rozkoše	rozkoš	k1gFnPc1	rozkoš
a	a	k8xC	a
dni	den	k1gInPc1	den
<g/>
)	)	kIx)	)
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
raných	raný	k2eAgNnPc2d1	rané
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
předmluvu	předmluva	k1gFnSc4	předmluva
Anatola	Anatola	k1gFnSc1	Anatola
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnSc2	kresba
Madame	madame	k1gFnSc2	madame
Lemaire	Lemair	k1gInSc5	Lemair
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
přepychově	přepychově	k6eAd1	přepychově
vypravena	vypraven	k2eAgNnPc1d1	vypraveno
<g/>
.	.	kIx.	.
</s>
<s>
Neměla	mít	k5eNaImAgFnS	mít
však	však	k9	však
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
Proust	Proust	k1gMnSc1	Proust
také	také	k9	také
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vydán	vydat	k5eAaPmNgInS	vydat
až	až	k9	až
posmrtně	posmrtně	k6eAd1	posmrtně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jean	Jean	k1gMnSc1	Jean
Santeuil	Santeuil	k1gMnSc1	Santeuil
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
témat	téma	k1gNnPc2	téma
později	pozdě	k6eAd2	pozdě
rozvinutých	rozvinutý	k2eAgMnPc2d1	rozvinutý
v	v	k7c6	v
Hledání	hledání	k1gNnSc6	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
našlo	najít	k5eAaPmAgNnS	najít
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
vyjádření	vyjádření	k1gNnSc4	vyjádření
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
nedokončeném	dokončený	k2eNgNnSc6d1	nedokončené
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tajemství	tajemství	k1gNnSc2	tajemství
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
nezbytnosti	nezbytnost	k1gFnSc2	nezbytnost
reflexe	reflexe	k1gFnSc2	reflexe
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
rodičů	rodič	k1gMnPc2	rodič
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jean	Jean	k1gMnSc1	Jean
Santeuil	Santeuila	k1gFnPc2	Santeuila
docela	docela	k6eAd1	docela
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ostrém	ostrý	k2eAgInSc6d1	ostrý
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
zbožňování	zbožňování	k1gNnSc3	zbožňování
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
jsou	být	k5eAaImIp3nP	být
rodiče	rodič	k1gMnPc1	rodič
zobrazeni	zobrazit	k5eAaPmNgMnP	zobrazit
v	v	k7c4	v
Hledání	hledání	k1gNnSc4	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slabém	slabý	k2eAgNnSc6d1	slabé
přijetí	přijetí	k1gNnSc6	přijetí
Les	les	k1gInSc4	les
Plaisirs	Plaisirs	k1gInSc1	Plaisirs
et	et	k?	et
les	les	k1gInSc1	les
Jours	Joursa	k1gFnPc2	Joursa
a	a	k8xC	a
potížích	potíž	k1gFnPc6	potíž
s	s	k7c7	s
řešením	řešení	k1gNnSc7	řešení
zápletky	zápletka	k1gFnSc2	zápletka
Proust	Proust	k1gFnSc1	Proust
na	na	k7c6	na
próze	próza	k1gFnSc6	próza
Jean	Jean	k1gMnSc1	Jean
Santeuil	Santeuil	k1gMnSc1	Santeuil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
přestal	přestat	k5eAaPmAgMnS	přestat
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
Proust	Proust	k1gMnSc1	Proust
věnoval	věnovat	k5eAaPmAgMnS	věnovat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
čtení	čtení	k1gNnSc2	čtení
Carlyla	Carlyla	k1gMnSc2	Carlyla
<g/>
,	,	kIx,	,
Emersona	Emerson	k1gMnSc2	Emerson
a	a	k8xC	a
Ruskina	Ruskin	k1gMnSc2	Ruskin
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
četby	četba	k1gFnSc2	četba
Proust	Proust	k1gInSc1	Proust
začal	začít	k5eAaPmAgInS	začít
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
teorie	teorie	k1gFnPc4	teorie
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
role	role	k1gFnSc2	role
umělce	umělec	k1gMnSc2	umělec
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Čase	čas	k1gInSc6	čas
znovu	znovu	k6eAd1	znovu
nalezeném	nalezený	k2eAgInSc6d1	nalezený
Proustův	Proustův	k2eAgMnSc1d1	Proustův
hrdina	hrdina	k1gMnSc1	hrdina
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
překládal	překládat	k5eAaImAgMnS	překládat
Ruskinovo	Ruskinův	k2eAgNnSc4d1	Ruskinův
dílo	dílo	k1gNnSc4	dílo
Sesame	Sesam	k1gInSc5	Sesam
and	and	k?	and
Lilies	Lilies	k1gInSc1	Lilies
(	(	kIx(	(
<g/>
Sezam	sezam	k1gInSc1	sezam
a	a	k8xC	a
lilie	lilie	k1gFnSc1	lilie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ruskinovo	Ruskinův	k2eAgNnSc1d1	Ruskinův
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Prousta	Proust	k1gMnSc4	Proust
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
;	;	kIx,	;
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zná	znát	k5eAaImIp3nS	znát
nazpaměť	nazpaměť	k6eAd1	nazpaměť
několik	několik	k4yIc4	několik
Ruskinových	Ruskinův	k2eAgFnPc2d1	Ruskinův
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
The	The	k1gFnSc2	The
Seven	Seven	k2eAgInSc1d1	Seven
Lamps	Lamps	k1gInSc1	Lamps
of	of	k?	of
Architecture	Architectur	k1gMnSc5	Architectur
(	(	kIx(	(
<g/>
Sedm	sedm	k4xCc1	sedm
lamp	lampa	k1gFnPc2	lampa
architektury	architektura	k1gFnSc2	architektura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Bible	bible	k1gFnSc2	bible
of	of	k?	of
Amiens	Amiens	k1gInSc1	Amiens
(	(	kIx(	(
<g/>
Amienská	Amienský	k2eAgFnSc1d1	Amienský
bible	bible	k1gFnSc1	bible
<g/>
)	)	kIx)	)
a	a	k8xC	a
Praeterita	Praeterita	k1gFnSc1	Praeterita
<g/>
.	.	kIx.	.
</s>
<s>
Proust	Proust	k1gMnSc1	Proust
začal	začít	k5eAaPmAgMnS	začít
překládat	překládat	k5eAaImF	překládat
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
dvě	dva	k4xCgNnPc4	dva
Ruskinova	Ruskinův	k2eAgNnPc4d1	Ruskinův
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
omezen	omezit	k5eAaPmNgInS	omezit
nedokonalou	dokonalý	k2eNgFnSc7d1	nedokonalá
znalostí	znalost	k1gFnSc7	znalost
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc1	překlad
byly	být	k5eAaImAgFnP	být
skupinovým	skupinový	k2eAgNnSc7d1	skupinové
dílem	dílo	k1gNnSc7	dílo
<g/>
:	:	kIx,	:
načrtnuty	načrtnut	k2eAgFnPc1d1	načrtnuta
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
revidovány	revidován	k2eAgInPc1d1	revidován
Proustem	Proust	k1gInSc7	Proust
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Marií	Maria	k1gFnSc7	Maria
Nordlingerovou	Nordlingerův	k2eAgFnSc7d1	Nordlingerův
(	(	kIx(	(
<g/>
anglickou	anglický	k2eAgFnSc7d1	anglická
sestřenicí	sestřenice	k1gFnSc7	sestřenice
jeho	jeho	k3xOp3gMnSc2	jeho
přítele	přítel	k1gMnSc2	přítel
Reynalda	Reynald	k1gMnSc2	Reynald
Hahna	Hahn	k1gMnSc2	Hahn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
doladěny	doladěn	k2eAgInPc1d1	doladěn
opět	opět	k6eAd1	opět
Proustem	Proust	k1gMnSc7	Proust
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
tázán	tázán	k2eAgInSc1d1	tázán
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
překladatelskou	překladatelský	k2eAgFnSc4d1	překladatelská
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
Proust	Proust	k1gMnSc1	Proust
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
znám	znát	k5eAaImIp1nS	znát
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
;	;	kIx,	;
tvrdím	tvrdit	k5eAaImIp1nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
znám	znám	k2eAgMnSc1d1	znám
Ruskina	Ruskina	k1gMnSc1	Ruskina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
The	The	k1gFnSc1	The
Bible	bible	k1gFnSc2	bible
of	of	k?	of
Amiens	Amiens	k1gInSc1	Amiens
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
Proustovým	Proustův	k2eAgInSc7d1	Proustův
úvodem	úvod	k1gInSc7	úvod
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
překlad	překlad	k1gInSc1	překlad
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
úvod	úvod	k1gInSc1	úvod
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
hodnoceny	hodnotit	k5eAaImNgFnP	hodnotit
<g/>
;	;	kIx,	;
Henri	Henri	k1gNnSc7	Henri
Bergson	Bergson	k1gMnSc1	Bergson
považoval	považovat	k5eAaImAgMnS	považovat
Proustův	Proustův	k2eAgInSc4d1	Proustův
úvod	úvod	k1gInSc4	úvod
za	za	k7c4	za
"	"	kIx"	"
<g/>
důležitý	důležitý	k2eAgInSc4d1	důležitý
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
Ruskinově	Ruskinův	k2eAgFnSc3d1	Ruskinův
psychologii	psychologie	k1gFnSc3	psychologie
<g/>
"	"	kIx"	"
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
pochválil	pochválit	k5eAaPmAgMnS	pochválit
i	i	k9	i
překlad	překlad	k1gInSc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vydání	vydání	k1gNnSc2	vydání
této	tento	k3xDgFnSc2	tento
publikace	publikace	k1gFnSc2	publikace
Proust	Proust	k1gMnSc1	Proust
už	už	k6eAd1	už
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
překladu	překlad	k1gInSc6	překlad
Ruskinovy	Ruskinův	k2eAgFnSc2d1	Ruskinův
Sesame	Sesam	k1gInSc5	Sesam
and	and	k?	and
Lilies	Lilies	k1gMnSc1	Lilies
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
Prousta	Prousta	k1gMnSc1	Prousta
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
Ruskina	Ruskino	k1gNnSc2	Ruskino
<g/>
)	)	kIx)	)
nejvíce	hodně	k6eAd3	hodně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Saint-Simon	Saint-Simon	k1gMnSc1	Saint-Simon
<g/>
,	,	kIx,	,
Montaigne	Montaign	k1gMnSc5	Montaign
<g/>
,	,	kIx,	,
Stendhal	Stendhal	k1gMnSc1	Stendhal
<g/>
,	,	kIx,	,
Flaubert	Flaubert	k1gMnSc1	Flaubert
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Eliot	Eliot	k1gMnSc1	Eliot
<g/>
,	,	kIx,	,
Fjodor	Fjodor	k1gMnSc1	Fjodor
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
a	a	k8xC	a
Lev	Lev	k1gMnSc1	Lev
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
Prousta	Proust	k1gMnSc2	Proust
jako	jako	k8xS	jako
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časopisech	časopis	k1gInPc6	časopis
pastiše	pastiš	k1gFnSc2	pastiš
jiných	jiný	k2eAgMnPc2d1	jiný
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
Proust	Proust	k1gInSc1	Proust
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
fragmentech	fragment	k1gInPc6	fragment
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
později	pozdě	k6eAd2	pozdě
splynuly	splynout	k5eAaPmAgFnP	splynout
pod	pod	k7c7	pod
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Contre	Contr	k1gMnSc5	Contr
Saint-Beuve	Saint-Beuv	k1gMnSc5	Saint-Beuv
<g/>
.	.	kIx.	.
</s>
<s>
Proust	Proust	k1gMnSc1	Proust
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
příteli	přítel	k1gMnSc3	přítel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pracuji	pracovat	k5eAaImIp1nS	pracovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
šlechtě	šlechta	k1gFnSc6	šlechta
<g/>
,	,	kIx,	,
pařížském	pařížský	k2eAgInSc6d1	pařížský
románu	román	k1gInSc6	román
<g/>
,	,	kIx,	,
eseji	esej	k1gFnSc6	esej
o	o	k7c6	o
Sainte-Beuvovi	Sainte-Beuva	k1gMnSc6	Sainte-Beuva
a	a	k8xC	a
Flaubertovi	Flaubert	k1gMnSc6	Flaubert
<g/>
,	,	kIx,	,
eseji	esej	k1gInPc7	esej
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
eseji	esej	k1gInPc7	esej
o	o	k7c4	o
pederastii	pederastie	k1gFnSc4	pederastie
(	(	kIx(	(
<g/>
nebude	být	k5eNaImBp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
ji	on	k3xPp3gFnSc4	on
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
vitráži	vitráž	k1gFnSc6	vitráž
<g/>
,	,	kIx,	,
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
náhrobních	náhrobní	k2eAgInPc6d1	náhrobní
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
románu	román	k1gInSc6	román
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
různorodých	různorodý	k2eAgInPc2d1	různorodý
fragmentů	fragment	k1gInPc2	fragment
Proust	Proust	k1gMnSc1	Proust
začal	začít	k5eAaPmAgMnS	začít
utvářet	utvářet	k5eAaImF	utvářet
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
pak	pak	k6eAd1	pak
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pracoval	pracovat	k5eAaImAgMnS	pracovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
náčrt	náčrt	k1gInSc1	náčrt
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c4	na
vypravěče	vypravěč	k1gMnPc4	vypravěč
v	v	k7c6	v
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
trpícího	trpící	k2eAgInSc2d1	trpící
nespavostí	nespavost	k1gFnSc7	nespavost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
až	až	k8xS	až
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
ráno	ráno	k6eAd1	ráno
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
měl	mít	k5eAaImAgInS	mít
končit	končit	k5eAaImF	končit
kritikou	kritika	k1gFnSc7	kritika
Sainte-Beuva	Sainte-Beuvo	k1gNnSc2	Sainte-Beuvo
a	a	k8xC	a
odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
jeho	jeho	k3xOp3gFnSc2	jeho
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
umělcovy	umělcův	k2eAgFnSc2d1	umělcova
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
životopis	životopis	k1gInSc4	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
měnící	měnící	k2eAgNnSc1d1	měnící
pojetí	pojetí	k1gNnSc1	pojetí
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
přiměly	přimět	k5eAaPmAgFnP	přimět
Prousta	Prousta	k1gMnSc1	Prousta
přejít	přejít	k5eAaPmF	přejít
k	k	k7c3	k
podstatně	podstatně	k6eAd1	podstatně
jinému	jiný	k2eAgInSc3d1	jiný
projektu	projekt	k1gInSc3	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ovšem	ovšem	k9	ovšem
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
mnoho	mnoho	k4c4	mnoho
shodných	shodný	k2eAgNnPc2d1	shodné
témat	téma	k1gNnPc2	téma
a	a	k8xC	a
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
začal	začít	k5eAaPmAgMnS	začít
spisovatel	spisovatel	k1gMnSc1	spisovatel
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
Hledání	hledání	k1gNnSc4	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
À	À	k?	À
la	la	k1gNnPc2	la
recherche	recherche	k1gNnPc2	recherche
du	du	k?	du
temps	temps	k6eAd1	temps
perdu	perdu	k6eAd1	perdu
<g/>
)	)	kIx)	)
vznikal	vznikat	k5eAaImAgInS	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
a	a	k8xC	a
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
byl	být	k5eAaImAgInS	být
upravován	upravován	k2eAgInSc1d1	upravován
až	až	k9	až
do	do	k7c2	do
Proustovy	Proustův	k2eAgFnSc2d1	Proustova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
dílů	díl	k1gInPc2	díl
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
rozsahu	rozsah	k1gInSc6	rozsah
přibližně	přibližně	k6eAd1	přibližně
3200	[number]	k4	3200
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Proust	Proust	k1gMnSc1	Proust
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
Goncourtovu	Goncourtův	k2eAgFnSc4d1	Goncourtova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
definitivně	definitivně	k6eAd1	definitivně
dokončené	dokončený	k2eAgInPc4d1	dokončený
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
svazky	svazek	k1gInPc4	svazek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
autor	autor	k1gMnSc1	autor
stačil	stačit	k5eAaBmAgMnS	stačit
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
zapracovat	zapracovat	k5eAaPmF	zapracovat
všechny	všechen	k3xTgFnPc4	všechen
korektury	korektura	k1gFnPc4	korektura
<g/>
.	.	kIx.	.
</s>
<s>
Du	Du	k?	Du
côté	côté	k6eAd1	côté
de	de	k?	de
chez	chez	k1gMnSc1	chez
Swann	Swann	k1gMnSc1	Swann
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
Swannových	Swannová	k1gFnPc2	Swannová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
À	À	k?	À
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
ombre	ombr	k1gInSc5	ombr
des	des	k1gNnSc7	des
jeunes	jeunes	k1gInSc1	jeunes
filles	filles	k1gInSc1	filles
en	en	k?	en
fleurs	fleurs	k1gInSc1	fleurs
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
dívek	dívka	k1gFnPc2	dívka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
Le	Le	k1gMnPc2	Le
Côté	Côtá	k1gFnSc2	Côtá
de	de	k?	de
Guermantes	Guermantes	k1gInSc1	Guermantes
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
Guermantových	Guermantův	k2eAgMnPc2d1	Guermantův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
1921	[number]	k4	1921
Sodome	Sodom	k1gInSc5	Sodom
et	et	k?	et
Gomorrhe	Gomorrhe	k1gNnPc1	Gomorrhe
(	(	kIx(	(
<g/>
Sodoma	Sodoma	k1gFnSc1	Sodoma
a	a	k8xC	a
Gomora	Gomora	k1gFnSc1	Gomora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
/	/	kIx~	/
<g/>
1922	[number]	k4	1922
La	la	k1gNnPc2	la
Prisonniè	Prisonniè	k1gFnPc2	Prisonniè
(	(	kIx(	(
<g/>
Uvězněná	uvězněný	k2eAgFnSc1d1	uvězněná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
Albertine	Albertin	k1gMnSc5	Albertin
disparue	disparuus	k1gMnSc5	disparuus
(	(	kIx(	(
<g/>
Zmizelá	zmizelý	k2eAgFnSc1d1	Zmizelá
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
Le	Le	k1gFnPc2	Le
Temps	Tempsa	k1gFnPc2	Tempsa
retrouvé	retrouvá	k1gFnSc2	retrouvá
(	(	kIx(	(
<g/>
Čas	čas	k1gInSc1	čas
znovu	znovu	k6eAd1	znovu
nalezený	nalezený	k2eAgMnSc1d1	nalezený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
</s>
