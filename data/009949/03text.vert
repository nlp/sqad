<p>
<s>
Hadrián	Hadrián	k1gMnSc1	Hadrián
I.	I.	kA	I.
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
700	[number]	k4	700
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
795	[number]	k4	795
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
772	[number]	k4	772
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hadrián	Hadrián	k1gMnSc1	Hadrián
I.	I.	kA	I.
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
římského	římský	k2eAgMnSc2d1	římský
šlechtice	šlechtic	k1gMnSc2	šlechtic
Theodora	Theodor	k1gMnSc2	Theodor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pontifikátu	pontifikát	k1gInSc2	pontifikát
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
Štěpána	Štěpán	k1gMnSc2	Štěpán
III	III	kA	III
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgInS	získat
svěcení	svěcení	k1gNnSc4	svěcení
jáhna	jáhen	k1gMnSc2	jáhen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
odpůrcům	odpůrce	k1gMnPc3	odpůrce
nevýhodných	výhodný	k2eNgFnPc2d1	nevýhodná
dohod	dohoda	k1gFnPc2	dohoda
mezi	mezi	k7c7	mezi
svatým	svatý	k2eAgInSc7d1	svatý
stolcem	stolec	k1gInSc7	stolec
a	a	k8xC	a
Langobardským	Langobardský	k2eAgNnSc7d1	Langobardský
královstvím	království	k1gNnSc7	království
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
tyto	tento	k3xDgFnPc4	tento
dohody	dohoda	k1gFnPc4	dohoda
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
po	po	k7c6	po
langobardském	langobardský	k2eAgMnSc6d1	langobardský
králi	král	k1gMnSc6	král
Desideriovi	Desiderius	k1gMnSc6	Desiderius
navrácení	navrácení	k1gNnSc4	navrácení
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
ironií	ironie	k1gFnSc7	ironie
tímto	tento	k3xDgInSc7	tento
úkolem	úkol	k1gInSc7	úkol
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Pavla	Pavel	k1gMnSc4	Pavel
Afiartu	Afiart	k1gInSc2	Afiart
<g/>
,	,	kIx,	,
papežského	papežský	k2eAgMnSc2d1	papežský
komořího	komoří	k1gMnSc2	komoří
a	a	k8xC	a
otevřeného	otevřený	k2eAgMnSc2d1	otevřený
stoupence	stoupenec	k1gMnSc2	stoupenec
Langobardů	Langobard	k1gMnPc2	Langobard
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
zbavil	zbavit	k5eAaPmAgInS	zbavit
tohoto	tento	k3xDgMnSc2	tento
langobardského	langobardský	k2eAgMnSc2d1	langobardský
špeha	špeh	k1gMnSc2	špeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c4	v
den	den	k1gInSc4	den
svého	svůj	k3xOyFgNnSc2	svůj
vysvěcení	vysvěcení	k1gNnSc2	vysvěcení
nechal	nechat	k5eAaPmAgMnS	nechat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
propustit	propustit	k5eAaPmF	propustit
odpůrce	odpůrce	k1gMnSc4	odpůrce
Langobargů	Langobarg	k1gInPc2	Langobarg
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
tam	tam	k6eAd1	tam
nechal	nechat	k5eAaPmAgInS	nechat
Afiarta	Afiart	k1gMnSc4	Afiart
pozavírat	pozavírat	k5eAaPmF	pozavírat
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
rozběhlo	rozběhnout	k5eAaPmAgNnS	rozběhnout
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Afiarta	Afiart	k1gMnSc4	Afiart
zosnoval	zosnovat	k5eAaPmAgMnS	zosnovat
vraždu	vražda	k1gFnSc4	vražda
urozeného	urozený	k2eAgMnSc4d1	urozený
Římana	Říman	k1gMnSc4	Říman
a	a	k8xC	a
odpůrce	odpůrce	k1gMnSc1	odpůrce
Langobardů	Langobard	k1gMnPc2	Langobard
Sergia	Sergius	k1gMnSc2	Sergius
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
i	i	k9	i
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
písemně	písemně	k6eAd1	písemně
požádal	požádat	k5eAaPmAgMnS	požádat
Afiartu	Afiarta	k1gFnSc4	Afiarta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Ravenny	Ravenna	k1gFnSc2	Ravenna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgInS	nechat
zatknout	zatknout	k5eAaPmF	zatknout
místním	místní	k2eAgMnSc7d1	místní
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Lvem	Lev	k1gMnSc7	Lev
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
provinilce	provinilec	k1gMnSc4	provinilec
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Afiartu	Afiarta	k1gFnSc4	Afiarta
nenáviděl	návidět	k5eNaImAgInS	návidět
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
později	pozdě	k6eAd2	pozdě
mezi	mezi	k7c4	mezi
arcibiskupen	arcibiskupen	k2eAgInSc4d1	arcibiskupen
Lvem	lev	k1gInSc7	lev
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
Hadriánem	Hadrián	k1gMnSc7	Hadrián
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
prohlubující	prohlubující	k2eAgFnSc3d1	prohlubující
osobní	osobní	k2eAgFnSc3d1	osobní
nevraživosti	nevraživost	k1gFnSc3	nevraživost
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
Hadrian	Hadrian	k1gMnSc1	Hadrian
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
zlomení	zlomení	k1gNnSc4	zlomení
langobardské	langobardský	k2eAgFnSc2d1	langobardská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
si	se	k3xPyFc3	se
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
nejprve	nejprve	k6eAd1	nejprve
přiklonit	přiklonit	k5eAaPmF	přiklonit
bavorského	bavorský	k2eAgMnSc4d1	bavorský
vévodu	vévoda	k1gMnSc4	vévoda
Tassila	Tassila	k1gMnSc4	Tassila
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
rodem	rod	k1gInSc7	rod
Agilolfingů	Agilolfing	k1gInPc2	Agilolfing
a	a	k8xC	a
papežskou	papežský	k2eAgFnSc7d1	Papežská
kurií	kurie	k1gFnSc7	kurie
byly	být	k5eAaImAgFnP	být
tradičně	tradičně	k6eAd1	tradičně
dobré	dobrý	k2eAgFnPc1d1	dobrá
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
vévoda	vévoda	k1gMnSc1	vévoda
Tassilo	Tassilo	k1gFnSc2	Tassilo
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
bratrancem	bratranec	k1gMnSc7	bratranec
franského	franský	k2eAgMnSc4d1	franský
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc4	Karel
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
dceru	dcera	k1gFnSc4	dcera
Desideria	desiderium	k1gNnSc2	desiderium
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
potenciálním	potenciální	k2eAgMnSc7d1	potenciální
spojencem	spojenec	k1gMnSc7	spojenec
Langobardů	Langobard	k1gInPc2	Langobard
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svatodušních	svatodušní	k2eAgInPc6d1	svatodušní
svátcích	svátek	k1gInPc6	svátek
roku	rok	k1gInSc2	rok
772	[number]	k4	772
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
a	a	k8xC	a
pomazal	pomazat	k5eAaPmAgMnS	pomazat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
Tassilova	Tassilův	k2eAgMnSc2d1	Tassilův
syna	syn	k1gMnSc2	syn
Theoda	Theod	k1gMnSc2	Theod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
vysoká	vysoká	k1gFnSc1	vysoká
pocta	pocta	k1gFnSc1	pocta
pro	pro	k7c4	pro
bavorský	bavorský	k2eAgInSc4d1	bavorský
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
773	[number]	k4	773
vyslal	vyslat	k5eAaPmAgMnS	vyslat
papež	papež	k1gMnSc1	papež
poselstvo	poselstvo	k1gNnSc1	poselstvo
k	k	k7c3	k
franskému	franský	k2eAgMnSc3d1	franský
králi	král	k1gMnSc3	král
Karlovi	Karel	k1gMnSc3	Karel
(	(	kIx(	(
<g/>
Velikému	veliký	k2eAgInSc3d1	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zakročil	zakročit	k5eAaPmAgInS	zakročit
proti	proti	k7c3	proti
bezpráví	bezpráví	k1gNnSc3	bezpráví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
páchá	páchat	k5eAaImIp3nS	páchat
Desiderius	Desiderius	k1gInSc1	Desiderius
vůči	vůči	k7c3	vůči
papežské	papežský	k2eAgFnSc3d1	Papežská
svrchovanosti	svrchovanost	k1gFnSc3	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
po	po	k7c6	po
neúspěšných	úspěšný	k2eNgNnPc6d1	neúspěšné
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
langobardským	langobardský	k2eAgMnSc7d1	langobardský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
překročil	překročit	k5eAaPmAgMnS	překročit
Alpy	Alpy	k1gFnPc4	Alpy
a	a	k8xC	a
Langobardské	Langobardský	k2eAgNnSc4d1	Langobardský
království	království	k1gNnPc4	království
dobyl	dobýt	k5eAaPmAgInS	dobýt
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
korunu	koruna	k1gFnSc4	koruna
jejich	jejich	k3xOp3gMnPc2	jejich
králů	král	k1gMnPc2	král
vsadil	vsadit	k5eAaPmAgInS	vsadit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Hadrianem	Hadrian	k1gMnSc7	Hadrian
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Velikým	veliký	k2eAgMnSc7d1	veliký
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
těšily	těšit	k5eAaImAgFnP	těšit
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
úctě	úcta	k1gFnSc3	úcta
i	i	k8xC	i
přátelství	přátelství	k1gNnSc3	přátelství
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
dát	dát	k5eAaPmF	dát
těmto	tento	k3xDgFnPc3	tento
emocím	emoce	k1gFnPc3	emoce
průchod	průchod	k1gInSc4	průchod
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
papež	papež	k1gMnSc1	papež
ustavičně	ustavičně	k6eAd1	ustavičně
požadoval	požadovat	k5eAaImAgMnS	požadovat
úplné	úplný	k2eAgNnSc4d1	úplné
navrácení	navrácení	k1gNnSc4	navrácení
zabraných	zabraný	k2eAgNnPc2d1	zabrané
území	území	k1gNnPc2	území
Langobardy	Langobarda	k1gFnSc2	Langobarda
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Karel	Karel	k1gMnSc1	Karel
mu	on	k3xPp3gMnSc3	on
jich	on	k3xPp3gMnPc2	on
vrátil	vrátit	k5eAaPmAgInS	vrátit
jen	jen	k9	jen
část	část	k1gFnSc4	část
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
ponechal	ponechat	k5eAaPmAgMnS	ponechat
pod	pod	k7c7	pod
franskou	franský	k2eAgFnSc7d1	Franská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Hadrian	Hadrian	k1gMnSc1	Hadrian
I.	I.	kA	I.
získal	získat	k5eAaPmAgMnS	získat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
nad	nad	k7c7	nad
langobardským	langobardský	k2eAgNnSc7d1	langobardský
vévodstvím	vévodství	k1gNnSc7	vévodství
Spoleto	Spolet	k2eAgNnSc1d1	Spoleto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
Beneventa	Benevento	k1gNnSc2	Benevento
bylo	být	k5eAaImAgNnS	být
nad	nad	k7c4	nad
jeho	jeho	k3xOp3gFnPc4	jeho
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Karel	Karel	k1gMnSc1	Karel
jeho	jeho	k3xOp3gFnSc4	jeho
pomoc	pomoc	k1gFnSc4	pomoc
nevyslyšel	vyslyšet	k5eNaPmAgMnS	vyslyšet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
781	[number]	k4	781
učinil	učinit	k5eAaPmAgMnS	učinit
papež	papež	k1gMnSc1	papež
gesto	gesto	k1gNnSc4	gesto
vstřícnosti	vstřícnost	k1gFnPc4	vstřícnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
Karlovy	Karlův	k2eAgMnPc4d1	Karlův
syny	syn	k1gMnPc4	syn
Ludvíka	Ludvík	k1gMnSc4	Ludvík
a	a	k8xC	a
Karlomana	Karloman	k1gMnSc4	Karloman
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
aktu	akt	k1gInSc6	akt
přijal	přijmout	k5eAaPmAgInS	přijmout
jméno	jméno	k1gNnSc4	jméno
Pipin	pipina	k1gFnPc2	pipina
<g/>
)	)	kIx)	)
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
pomazal	pomazat	k5eAaPmAgMnS	pomazat
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
oba	dva	k4xCgInPc4	dva
jmenované	jmenovaný	k2eAgInPc4d1	jmenovaný
syny	syn	k1gMnPc7	syn
učinil	učinit	k5eAaImAgMnS	učinit
v	v	k7c4	v
Akvitánii	Akvitánie	k1gFnSc4	Akvitánie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc4	Itálie
mladšími	mladý	k2eAgMnPc7d2	mladší
králi	král	k1gMnPc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovšem	ovšem	k9	ovšem
výpravu	výprava	k1gFnSc4	výprava
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Itálie	Itálie	k1gFnSc2	Itálie
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
papeže	papež	k1gMnSc2	papež
až	až	k8xS	až
roku	rok	k1gInSc2	rok
786	[number]	k4	786
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
Hadrian	Hadrian	k1gMnSc1	Hadrian
získal	získat	k5eAaPmAgMnS	získat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
město	město	k1gNnSc1	město
Capuyu	Capuyus	k1gInSc2	Capuyus
<g/>
,	,	kIx,	,
beneventský	beneventský	k2eAgMnSc1d1	beneventský
vévoda	vévoda	k1gMnSc1	vévoda
Arichis	Arichis	k1gFnSc2	Arichis
se	se	k3xPyFc4	se
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
střetu	střet	k1gInSc2	střet
vyplatil	vyplatit	k5eAaPmAgInS	vyplatit
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Grimoalda	Grimoald	k1gMnSc4	Grimoald
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Karlovi	Karel	k1gMnSc3	Karel
jako	jako	k8xC	jako
rukojmí	rukojmí	k1gMnPc4	rukojmí
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
podřídil	podřídit	k5eAaPmAgMnS	podřídit
franské	franský	k2eAgFnSc3d1	Franská
svrchovanosti	svrchovanost	k1gFnSc3	svrchovanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
Hadrián	Hadrián	k1gMnSc1	Hadrián
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
pokusil	pokusit	k5eAaPmAgMnS	pokusit
sehrát	sehrát	k5eAaPmF	sehrát
roli	role	k1gFnSc4	role
vyjednavače	vyjednavač	k1gMnSc2	vyjednavač
a	a	k8xC	a
prostředníka	prostředník	k1gMnSc2	prostředník
mezi	mezi	k7c7	mezi
bavorským	bavorský	k2eAgMnSc7d1	bavorský
vévodou	vévoda	k1gMnSc7	vévoda
Tassilem	Tassil	k1gMnSc7	Tassil
a	a	k8xC	a
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
rostlo	růst	k5eAaImAgNnS	růst
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Tassilo	Tassit	k5eAaImAgNnS	Tassit
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
788	[number]	k4	788
franským	franský	k2eAgFnPc3d1	Franská
králem	král	k1gMnSc7	král
sesazen	sesadit	k5eAaPmNgMnS	sesadit
a	a	k8xC	a
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pontifikát	pontifikát	k1gInSc4	pontifikát
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
víry	víra	k1gFnSc2	víra
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
obrazoborectví	obrazoborectví	k1gNnSc3	obrazoborectví
(	(	kIx(	(
<g/>
ikonokasmus	ikonokasmus	k1gInSc4	ikonokasmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
součástí	součást	k1gFnPc2	součást
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
Byzantském	byzantský	k2eAgNnSc6d1	byzantské
císařství	císařství	k1gNnSc6	císařství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
poselstvo	poselstvo	k1gNnSc1	poselstvo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
i	i	k9	i
na	na	k7c6	na
Druhém	druhý	k4xOgInSc6	druhý
nikajském	nikajský	k2eAgInSc6d1	nikajský
koncilu	koncil	k1gInSc6	koncil
roku	rok	k1gInSc2	rok
787	[number]	k4	787
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
veřejně	veřejně	k6eAd1	veřejně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
císařovnu	císařovna	k1gFnSc4	císařovna
-	-	kIx~	-
regentku	regentka	k1gFnSc4	regentka
Irenu	Irena	k1gFnSc4	Irena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odmítl	odmítnout	k5eAaPmAgInS	odmítnout
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
herezi	hereze	k1gFnSc4	hereze
adopcionismu	adopcionismus	k1gInSc2	adopcionismus
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
učilo	učit	k5eAaImAgNnS	učit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
přijetím	přijetí	k1gNnSc7	přijetí
křtu	křest	k1gInSc2	křest
do	do	k7c2	do
něj	on	k3xPp3gMnSc4	on
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Duch	duch	k1gMnSc1	duch
svatý	svatý	k1gMnSc1	svatý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
adoptován	adoptovat	k5eAaPmNgMnS	adoptovat
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
katolicismu	katolicismus	k1gInSc2	katolicismus
bludnou	bludný	k2eAgFnSc4d1	bludná
myšlenku	myšlenka	k1gFnSc4	myšlenka
propagovali	propagovat	k5eAaImAgMnP	propagovat
vysocí	vysoký	k2eAgMnPc1d1	vysoký
hodnostáři	hodnostář	k1gMnPc1	hodnostář
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Elipadus	Elipadus	k1gMnSc1	Elipadus
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
z	z	k7c2	z
Toleda	Toledo	k1gNnSc2	Toledo
<g/>
,	,	kIx,	,
a	a	k8xC	a
urgelský	urgelský	k2eAgMnSc1d1	urgelský
biskup	biskup	k1gMnSc1	biskup
Felix	Felix	k1gMnSc1	Felix
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
druhý	druhý	k4xOgMnSc1	druhý
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
dostavit	dostavit	k5eAaPmF	dostavit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtská	frankfurtský	k2eAgFnSc1d1	Frankfurtská
synoda	synoda	k1gFnSc1	synoda
roku	rok	k1gInSc2	rok
794	[number]	k4	794
toto	tento	k3xDgNnSc1	tento
učení	učení	k1gNnSc1	učení
zavrhal	zavrhat	k5eAaImAgInS	zavrhat
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
synodě	synoda	k1gFnSc6	synoda
<g/>
,	,	kIx,	,
iniciované	iniciovaný	k2eAgNnSc1d1	iniciované
Karlem	Karel	k1gMnSc7	Karel
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
odmítnuty	odmítnut	k2eAgInPc1d1	odmítnut
výsledky	výsledek	k1gInPc1	výsledek
z	z	k7c2	z
Druhého	druhý	k4xOgInSc2	druhý
nikajského	nikajský	k2eAgInSc2d1	nikajský
koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
franský	franský	k2eAgMnSc1d1	franský
panovník	panovník	k1gMnSc1	panovník
požadoval	požadovat	k5eAaImAgMnS	požadovat
exkomunikovat	exkomunikovat	k5eAaBmF	exkomunikovat
byzantského	byzantský	k2eAgMnSc4d1	byzantský
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
papež	papež	k1gMnSc1	papež
Hadrián	Hadrián	k1gMnSc1	Hadrián
I.	I.	kA	I.
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
závěrům	závěr	k1gInPc3	závěr
nikajské	nikajský	k2eAgFnSc2d1	nikajský
synody	synoda	k1gFnSc2	synoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
dal	dát	k5eAaPmAgMnS	dát
Hadrián	Hadrián	k1gMnSc1	Hadrián
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
svoji	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c4	na
Byzancii	Byzancie	k1gFnSc4	Byzancie
<g/>
,	,	kIx,	,
když	když	k8xS	když
zrušil	zrušit	k5eAaPmAgMnS	zrušit
datování	datování	k1gNnSc4	datování
listin	listina	k1gFnPc2	listina
podle	podle	k7c2	podle
byzantského	byzantský	k2eAgInSc2d1	byzantský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
datování	datování	k1gNnSc2	datování
vlády	vláda	k1gFnSc2	vláda
císářů	císář	k1gMnPc2	císář
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
gesto	gesto	k1gNnSc1	gesto
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
naznačit	naznačit	k5eAaPmF	naznačit
<g/>
,	,	kIx,	,
komu	kdo	k3yRnSc3	kdo
vděčí	vděčit	k5eAaImIp3nS	vděčit
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
komu	kdo	k3yRnSc3	kdo
nevděčí	vděčit	k5eNaImIp3nS	vděčit
<g/>
)	)	kIx)	)
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
papežství	papežství	k1gNnSc2	papežství
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
římských	římský	k2eAgMnPc2d1	římský
akvaduktů	akvadukt	k1gInPc2	akvadukt
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
chátrajících	chátrající	k2eAgFnPc2d1	chátrající
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
obohatil	obohatit	k5eAaPmAgMnS	obohatit
mnohé	mnohý	k2eAgFnPc4d1	mnohá
církevní	církevní	k2eAgFnPc4d1	církevní
budovy	budova	k1gFnPc4	budova
o	o	k7c4	o
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
výzdobu	výzdoba	k1gFnSc4	výzdoba
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
papež	papež	k1gMnSc1	papež
začal	začít	k5eAaPmAgMnS	začít
razit	razit	k5eAaImF	razit
i	i	k9	i
vlastní	vlastní	k2eAgFnPc4d1	vlastní
mince	mince	k1gFnPc4	mince
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
pátým	pátý	k4xOgNnSc7	pátý
nejdéle	dlouho	k6eAd3	dlouho
sloužícím	sloužící	k2eAgMnSc7d1	sloužící
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
v	v	k7c6	v
Bazilice	bazilika	k1gFnSc6	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HÄGERMANN	HÄGERMANN	kA	HÄGERMANN
<g/>
,	,	kIx,	,
Dieter	Dieter	k1gMnSc1	Dieter
;	;	kIx,	;
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Aleš	Aleš	k1gMnSc1	Aleš
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
-	-	kIx~	-
vládce	vládce	k1gMnSc1	vládce
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
JEDIN	JEDIN	kA	JEDIN
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnPc1d1	Malé
dějiny	dějiny	k1gFnPc1	dějiny
koncilů	koncil	k1gInPc2	koncil
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
katolická	katolický	k2eAgFnSc1d1	katolická
charita	charita	k1gFnSc1	charita
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Dolista	Dolista	k1gMnSc1	Dolista
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hadrián	Hadrián	k1gMnSc1	Hadrián
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pope	pop	k1gMnSc5	pop
Adrian	Adrian	k1gMnSc1	Adrian
I	i	k9	i
na	na	k7c4	na
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
britannica	britannica	k1gFnSc1	britannica
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Adrian	Adrian	k1gMnSc1	Adrian
I.	I.	kA	I.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dominika	Dominik	k1gMnSc4	Dominik
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
významné	významný	k2eAgInPc1d1	významný
procesy	proces	k1gInPc1	proces
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
církvi	církev	k1gFnSc6	církev
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
X.	X.	kA	X.
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.717	.717	k4	.717
</s>
</p>
