<s>
ENIAC	ENIAC	kA	ENIAC
(	(	kIx(	(
<g/>
Electronic	Electronice	k1gFnPc2	Electronice
Numerical	Numerical	k1gFnSc2	Numerical
Integrator	Integrator	k1gMnSc1	Integrator
And	Anda	k1gFnPc2	Anda
Computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc7	první
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
turingovsky	turingovsky	k6eAd1	turingovsky
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
,	,	kIx,	,
elektronkový	elektronkový	k2eAgInSc1d1	elektronkový
počítač	počítač	k1gInSc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
v	v	k7c6	v
Penn	Penna	k1gFnPc2	Penna
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
dokončen	dokončit	k5eAaPmNgInS	dokončit
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
armádu	armáda	k1gFnSc4	armáda
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Maryland	Maryland	k1gInSc1	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
Chlazení	chlazení	k1gNnSc1	chlazení
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
dva	dva	k4xCgInPc1	dva
letecké	letecký	k2eAgInPc1d1	letecký
motory	motor	k1gInPc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
představení	představení	k1gNnSc6	představení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Giant	Giant	k1gMnSc1	Giant
Brain	Brain	k1gMnSc1	Brain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
mozek	mozek	k1gInSc1	mozek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
ENIAC	ENIAC	kA	ENIAC
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
první	první	k4xOgFnSc3	první
generaci	generace	k1gFnSc3	generace
počítačů	počítač	k1gMnPc2	počítač
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
části	část	k1gFnSc3	část
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
některé	některý	k3yIgInPc1	některý
jeho	jeho	k3xOp3gInPc1	jeho
kontrolní	kontrolní	k2eAgInPc1d1	kontrolní
panely	panel	k1gInPc1	panel
<g/>
)	)	kIx)	)
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
ENIAC	ENIAC	kA	ENIAC
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
výpočty	výpočet	k1gInPc4	výpočet
palebných	palebný	k2eAgFnPc2d1	palebná
tabulek	tabulka	k1gFnPc2	tabulka
pro	pro	k7c4	pro
dělostřelectvo	dělostřelectvo	k1gNnSc4	dělostřelectvo
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
dříve	dříve	k6eAd2	dříve
nežli	nežli	k8xS	nežli
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
stroj	stroj	k1gInSc4	stroj
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
využit	využít	k5eAaPmNgInS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
až	až	k9	až
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
představení	představení	k1gNnSc6	představení
počítače	počítač	k1gInSc2	počítač
pozval	pozvat	k5eAaPmAgInS	pozvat
Pentagon	Pentagon	k1gInSc1	Pentagon
matematické	matematický	k2eAgFnSc2d1	matematická
a	a	k8xC	a
elektrotechnické	elektrotechnický	k2eAgMnPc4d1	elektrotechnický
špičkové	špičkový	k2eAgMnPc4d1	špičkový
vědce	vědec	k1gMnPc4	vědec
a	a	k8xC	a
formou	forma	k1gFnSc7	forma
48	[number]	k4	48
přednášek	přednáška	k1gFnPc2	přednáška
jim	on	k3xPp3gMnPc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
přednosti	přednost	k1gFnPc4	přednost
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
probíhaly	probíhat	k5eAaImAgFnP	probíhat
mezi	mezi	k7c7	mezi
8	[number]	k4	8
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1946	[number]	k4	1946
a	a	k8xC	a
polovina	polovina	k1gFnSc1	polovina
těchto	tento	k3xDgFnPc2	tento
přednášek	přednáška	k1gFnPc2	přednáška
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
přímo	přímo	k6eAd1	přímo
tvůrci	tvůrce	k1gMnSc3	tvůrce
ENIACu	ENIAC	k1gMnSc3	ENIAC
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
úplně	úplně	k6eAd1	úplně
první	první	k4xOgFnPc4	první
přednášky	přednáška	k1gFnPc4	přednáška
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
ENIAC	ENIAC	kA	ENIAC
neziskovou	ziskový	k2eNgFnSc7d1	nezisková
organizací	organizace	k1gFnSc7	organizace
IEEE	IEEE	kA	IEEE
přidán	přidán	k2eAgInSc1d1	přidán
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
klíčových	klíčový	k2eAgInPc2d1	klíčový
elektronických	elektronický	k2eAgInPc2d1	elektronický
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
IEEE	IEEE	kA	IEEE
milestones	milestones	k1gMnSc1	milestones
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
seznamu	seznam	k1gInSc6	seznam
tak	tak	k6eAd1	tak
figurují	figurovat	k5eAaImIp3nP	figurovat
převratné	převratný	k2eAgInPc4d1	převratný
elektronické	elektronický	k2eAgInPc4d1	elektronický
vynálezy	vynález	k1gInPc4	vynález
lidstva	lidstvo	k1gNnSc2	lidstvo
jako	jako	k8xC	jako
například	například	k6eAd1	například
Voltův	Voltův	k2eAgInSc1d1	Voltův
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Karolíně	Karolína	k1gFnSc6	Karolína
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tranzistor	tranzistor	k1gInSc4	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Čestné	čestný	k2eAgNnSc1d1	čestné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pak	pak	k6eAd1	pak
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
za	za	k7c4	za
vynalezení	vynalezení	k1gNnSc4	vynalezení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
komunikace	komunikace	k1gFnSc2	komunikace
nebo	nebo	k8xC	nebo
motoru	motor	k1gInSc2	motor
na	na	k7c4	na
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
výrobu	výroba	k1gFnSc4	výroba
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
financovala	financovat	k5eAaBmAgFnS	financovat
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
jej	on	k3xPp3gMnSc4	on
generálmajor	generálmajor	k1gMnSc1	generálmajor
Gladeon	Gladeon	k1gMnSc1	Gladeon
Marcus	Marcus	k1gMnSc1	Marcus
Barnes	Barnes	k1gMnSc1	Barnes
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc7d1	vlastní
výrobou	výroba	k1gFnSc7	výroba
počítače	počítač	k1gInSc2	počítač
pak	pak	k6eAd1	pak
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pověřena	pověřit	k5eAaPmNgFnS	pověřit
Penn	Penna	k1gFnPc2	Penna
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Výrobou	výroba	k1gFnSc7	výroba
byly	být	k5eAaImAgFnP	být
za	za	k7c4	za
univerzitu	univerzita	k1gFnSc4	univerzita
pověřeni	pověřit	k5eAaPmNgMnP	pověřit
John	John	k1gMnSc1	John
Mauchly	Mauchly	k1gMnSc1	Mauchly
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Presper	Presper	k1gMnSc1	Presper
Eckert	Eckert	k1gMnSc1	Eckert
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
tým	tým	k1gInSc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
získal	získat	k5eAaPmAgInS	získat
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
Project	Project	k1gInSc1	Project
PX	PX	kA	PX
<g/>
"	"	kIx"	"
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
trvala	trvat	k5eAaImAgFnS	trvat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
se	se	k3xPyFc4	se
o	o	k7c6	o
počítači	počítač	k1gInSc6	počítač
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
až	až	k6eAd1	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
vyčíslena	vyčíslit	k5eAaPmNgFnS	vyčíslit
na	na	k7c4	na
500	[number]	k4	500
000	[number]	k4	000
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
by	by	kYmCp3nS	by
stál	stát	k5eAaImAgMnS	stát
6	[number]	k4	6
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
převzala	převzít	k5eAaPmAgFnS	převzít
oficiálně	oficiálně	k6eAd1	oficiálně
počítač	počítač	k1gInSc4	počítač
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
jej	on	k3xPp3gMnSc4	on
armáda	armáda	k1gFnSc1	armáda
vypnula	vypnout	k5eAaPmAgFnS	vypnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
údržby	údržba	k1gFnSc2	údržba
a	a	k8xC	a
zvětšení	zvětšení	k1gNnSc2	zvětšení
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
"	"	kIx"	"
<g/>
Aberdeen	Aberdeen	k2eAgInSc1d1	Aberdeen
Proving	Proving	k1gInSc1	Proving
Ground	Grounda	k1gFnPc2	Grounda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
města	město	k1gNnSc2	město
Aberdeen	Aberdena	k1gFnPc2	Aberdena
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Maryland	Maryland	k1gInSc1	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jej	on	k3xPp3gNnSc2	on
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1947	[number]	k4	1947
opět	opět	k6eAd1	opět
aktivovali	aktivovat	k5eAaBmAgMnP	aktivovat
a	a	k8xC	a
fungoval	fungovat	k5eAaImAgMnS	fungovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
(	(	kIx(	(
<g/>
s	s	k7c7	s
mírnými	mírný	k2eAgFnPc7d1	mírná
každodenními	každodenní	k2eAgFnPc7d1	každodenní
opravami	oprava	k1gFnPc7	oprava
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
definitivně	definitivně	k6eAd1	definitivně
vypnuli	vypnout	k5eAaPmAgMnP	vypnout
<g/>
.	.	kIx.	.
</s>
<s>
ENIAC	ENIAC	kA	ENIAC
dokázal	dokázat	k5eAaPmAgInS	dokázat
provádět	provádět	k5eAaImF	provádět
podmíněné	podmíněný	k2eAgInPc4d1	podmíněný
výpočty	výpočet	k1gInPc4	výpočet
<g/>
,	,	kIx,	,
iterace	iterace	k1gFnPc4	iterace
(	(	kIx(	(
<g/>
loopy	loopa	k1gFnPc4	loopa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odskakovat	odskakovat	k5eAaImF	odskakovat
do	do	k7c2	do
podprogramů	podprogram	k1gInPc2	podprogram
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
nakonfigurován	nakonfigurován	k2eAgMnSc1d1	nakonfigurován
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1948	[number]	k4	1948
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
přepínačů	přepínač	k1gInPc2	přepínač
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pomocí	pomocí	k7c2	pomocí
děrných	děrný	k2eAgInPc2d1	děrný
štítků	štítek	k1gInPc2	štítek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
"	"	kIx"	"
<g/>
programování	programování	k1gNnSc1	programování
<g/>
"	"	kIx"	"
trvalo	trvat	k5eAaImAgNnS	trvat
mnoho	mnoho	k4c4	mnoho
hodin	hodina	k1gFnPc2	hodina
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
desetimístnými	desetimístný	k2eAgNnPc7d1	desetimístné
čísly	číslo	k1gNnPc7	číslo
prováděl	provádět	k5eAaImAgInS	provádět
357	[number]	k4	357
(	(	kIx(	(
<g/>
násobení	násobení	k1gNnSc2	násobení
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
35	[number]	k4	35
(	(	kIx(	(
<g/>
dělení	dělení	k1gNnSc2	dělení
<g/>
)	)	kIx)	)
operací	operace	k1gFnPc2	operace
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
operace	operace	k1gFnPc1	operace
s	s	k7c7	s
menšími	malý	k2eAgNnPc7d2	menší
čísly	číslo	k1gNnPc7	číslo
prováděl	provádět	k5eAaImAgInS	provádět
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgInS	používat
desítkovou	desítkový	k2eAgFnSc4d1	desítková
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInPc1d1	dnešní
počítače	počítač	k1gInPc1	počítač
používají	používat	k5eAaImIp3nP	používat
dvojkovou	dvojkový	k2eAgFnSc4d1	dvojková
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
desítkové	desítkový	k2eAgNnSc4d1	desítkové
bylo	být	k5eAaImAgNnS	být
atypické	atypický	k2eAgNnSc1d1	atypické
i	i	k9	i
v	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
-	-	kIx~	-
jediný	jediný	k2eAgInSc1d1	jediný
jiný	jiný	k2eAgInSc1d1	jiný
počítač	počítač	k1gInSc1	počítač
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
používal	používat	k5eAaImAgInS	používat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Harvard	Harvard	k1gInSc1	Harvard
Mark	Mark	k1gMnSc1	Mark
I	I	kA	I
-	-	kIx~	-
IBM	IBM	kA	IBM
ASCC	ASCC	kA	ASCC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ENIAC	ENIAC	kA	ENIAC
byl	být	k5eAaImAgInS	být
programován	programovat	k5eAaImNgInS	programovat
pomocí	pomocí	k7c2	pomocí
přepínačů	přepínač	k1gInPc2	přepínač
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
modifikován	modifikovat	k5eAaBmNgMnS	modifikovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
provádět	provádět	k5eAaImF	provádět
program	program	k1gInSc1	program
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
ROM	ROM	kA	ROM
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
funkčních	funkční	k2eAgFnPc2d1	funkční
tabulek	tabulka	k1gFnPc2	tabulka
čtených	čtený	k2eAgFnPc2d1	čtená
z	z	k7c2	z
primitivního	primitivní	k2eAgNnSc2d1	primitivní
čtecího	čtecí	k2eAgNnSc2d1	čtecí
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zásah	zásah	k1gInSc1	zásah
sice	sice	k8xC	sice
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
paralelní	paralelní	k2eAgNnSc4d1	paralelní
provádění	provádění	k1gNnSc4	provádění
operací	operace	k1gFnPc2	operace
a	a	k8xC	a
snížil	snížit	k5eAaPmAgInS	snížit
šestinásobně	šestinásobně	k6eAd1	šestinásobně
jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
doba	doba	k1gFnSc1	doba
programování	programování	k1gNnSc2	programování
se	se	k3xPyFc4	se
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
ze	z	k7c2	z
dnů	den	k1gInPc2	den
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
oprávnilo	oprávnit	k5eAaPmAgNnS	oprávnit
tuto	tento	k3xDgFnSc4	tento
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byla	být	k5eAaImAgFnS	být
pětinásobně	pětinásobně	k6eAd1	pětinásobně
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
jeho	jeho	k3xOp3gFnSc1	jeho
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
jeho	jeho	k3xOp3gFnSc1	jeho
hlavní	hlavní	k2eAgFnSc1d1	hlavní
paměť	paměť	k1gFnSc1	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
21	[number]	k4	21
501	[number]	k4	501
elektronek	elektronka	k1gFnPc2	elektronka
<g/>
,	,	kIx,	,
7200	[number]	k4	7200
krystalových	krystalový	k2eAgFnPc2d1	krystalová
diod	dioda	k1gFnPc2	dioda
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
relé	relé	k1gNnSc2	relé
<g/>
,	,	kIx,	,
70	[number]	k4	70
000	[number]	k4	000
rezistorů	rezistor	k1gInPc2	rezistor
<g/>
,	,	kIx,	,
10	[number]	k4	10
000	[number]	k4	000
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
ručně	ručně	k6eAd1	ručně
pájených	pájený	k2eAgInPc2d1	pájený
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
vážil	vážit	k5eAaImAgMnS	vážit
30	[number]	k4	30
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
zabíral	zabírat	k5eAaImAgMnS	zabírat
63	[number]	k4	63
m	m	kA	m
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
2,6	[number]	k4	2,6
m	m	kA	m
×	×	k?	×
0,9	[number]	k4	0,9
m	m	kA	m
×	×	k?	×
26	[number]	k4	26
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spotřebovával	spotřebovávat	k5eAaImAgMnS	spotřebovávat
150	[number]	k4	150
kW	kW	kA	kW
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
stál	stát	k5eAaImAgInS	stát
500	[number]	k4	500
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc4	vstup
i	i	k8xC	i
výstup	výstup	k1gInSc4	výstup
obstarávaly	obstarávat	k5eAaImAgInP	obstarávat
děrné	děrný	k2eAgInPc1d1	děrný
štítky	štítek	k1gInPc1	štítek
a	a	k8xC	a
tisk	tisk	k1gInSc1	tisk
se	se	k3xPyFc4	se
prováděl	provádět	k5eAaImAgInS	provádět
na	na	k7c6	na
specializovaném	specializovaný	k2eAgInSc6d1	specializovaný
stroji	stroj	k1gInSc6	stroj
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
IBM	IBM	kA	IBM
405	[number]	k4	405
nebo	nebo	k8xC	nebo
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ENIAC	ENIAC	kA	ENIAC
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
poruchový	poruchový	k2eAgInSc1d1	poruchový
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
několik	několik	k4yIc1	několik
vakuových	vakuový	k2eAgFnPc2d1	vakuová
elektronek	elektronka	k1gFnPc2	elektronka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
závadám	závada	k1gFnPc3	závada
na	na	k7c6	na
elektronkách	elektronka	k1gFnPc6	elektronka
docházelo	docházet	k5eAaImAgNnS	docházet
nejčastěji	často	k6eAd3	často
při	při	k7c6	při
zapínání	zapínání	k1gNnSc6	zapínání
nebo	nebo	k8xC	nebo
vypínání	vypínání	k1gNnSc6	vypínání
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
<g/>
,	,	kIx,	,
pouliční	pouliční	k2eAgNnPc1d1	pouliční
světla	světlo	k1gNnPc1	světlo
Philadelphie	Philadelphia	k1gFnSc2	Philadelphia
slabě	slabě	k6eAd1	slabě
poblikávala	poblikávat	k5eAaImAgFnS	poblikávat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
jednoduše	jednoduše	k6eAd1	jednoduše
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
nákladně	nákladně	k6eAd1	nákladně
<g/>
)	)	kIx)	)
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
závada	závada	k1gFnSc1	závada
na	na	k7c6	na
elektronkách	elektronka	k1gFnPc6	elektronka
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
nalezení	nalezení	k1gNnSc3	nalezení
stačilo	stačit	k5eAaBmAgNnS	stačit
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
modifikace	modifikace	k1gFnSc2	modifikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zabíralo	zabírat	k5eAaImAgNnS	zabírat
odstraňování	odstraňování	k1gNnSc1	odstraňování
jeho	jeho	k3xOp3gFnPc2	jeho
závad	závada	k1gFnPc2	závada
až	až	k9	až
polovinu	polovina	k1gFnSc4	polovina
užitného	užitný	k2eAgInSc2d1	užitný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
následovníkem	následovník	k1gMnSc7	následovník
ENIACu	ENIACus	k1gInSc2	ENIACus
byl	být	k5eAaImAgMnS	být
počítač	počítač	k1gInSc4	počítač
MANIAC	MANIAC	kA	MANIAC
(	(	kIx(	(
<g/>
Mathematical	Mathematical	k1gMnSc1	Mathematical
Analyser	Analyser	k1gMnSc1	Analyser
Numerical	Numerical	k1gMnSc1	Numerical
Integrator	Integrator	k1gMnSc1	Integrator
And	Anda	k1gFnPc2	Anda
Computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
sestaven	sestaven	k2eAgInSc1d1	sestaven
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumannem	Neumann	k1gMnSc7	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
konstruován	konstruovat	k5eAaImNgInS	konstruovat
k	k	k7c3	k
matematickým	matematický	k2eAgInPc3d1	matematický
výpočtům	výpočet	k1gInPc3	výpočet
popisujícím	popisující	k2eAgInPc3d1	popisující
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
děje	děj	k1gInPc1	děj
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
využit	využit	k2eAgInSc1d1	využit
i	i	k9	i
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
