<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
následovníkem	následovník	k1gMnSc7	následovník
ENIACu	ENIACus	k1gInSc2	ENIACus
byl	být	k5eAaImAgMnS	být
počítač	počítač	k1gInSc4	počítač
MANIAC	MANIAC	kA	MANIAC
(	(	kIx(	(
<g/>
Mathematical	Mathematical	k1gMnSc1	Mathematical
Analyser	Analyser	k1gMnSc1	Analyser
Numerical	Numerical	k1gMnSc1	Numerical
Integrator	Integrator	k1gMnSc1	Integrator
And	Anda	k1gFnPc2	Anda
Computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
sestaven	sestaven	k2eAgInSc1d1	sestaven
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
John	John	k1gMnSc1	John
von	von	k1gInSc1	von
Neumannem	Neumann	k1gMnSc7	Neumann
<g/>
.	.	kIx.	.
</s>
