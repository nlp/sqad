<p>
<s>
Izák	Izák	k1gMnSc1	Izák
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
צ	צ	k?	צ
<g/>
ְ	ְ	k?	ְ
<g/>
ח	ח	k?	ח
<g/>
ָ	ָ	k?	ָ
<g/>
ק	ק	k?	ק
<g/>
,	,	kIx,	,
Jicchak	Jicchak	k1gInSc1	Jicchak
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
smát	smát	k5eAaImF	smát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
biblickým	biblický	k2eAgMnSc7d1	biblický
patriarchou	patriarcha	k1gMnSc7	patriarcha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Sáry	Sára	k1gFnSc2	Sára
a	a	k8xC	a
patriarchy	patriarcha	k1gMnSc2	patriarcha
Abraháma	Abrahám	k1gMnSc4	Abrahám
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Rebeky	Rebeka	k1gFnSc2	Rebeka
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Ezaua	Ezau	k1gMnSc2	Ezau
a	a	k8xC	a
patriarchy	patriarcha	k1gMnSc2	patriarcha
Jákoba	Jákob	k1gMnSc2	Jákob
<g/>
.	.	kIx.	.
</s>
<s>
Izák	Izák	k1gMnSc1	Izák
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
obětován	obětovat	k5eAaBmNgMnS	obětovat
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Abrahámem	Abrahám	k1gMnSc7	Abrahám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Hospodin	Hospodin	k1gMnSc1	Hospodin
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
okamžiku	okamžik	k1gInSc6	okamžik
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Izák	Izák	k1gMnSc1	Izák
nebyl	být	k5eNaImAgMnS	být
jedináčkem	jedináček	k1gMnSc7	jedináček
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
měl	mít	k5eAaImAgMnS	mít
ještě	ještě	k9	ještě
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
jménem	jméno	k1gNnSc7	jméno
Izmael	Izmael	k1gMnSc1	Izmael
(	(	kIx(	(
<g/>
Jišmael	Jišmael	k1gMnSc1	Jišmael
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
otrokyní	otrokyně	k1gFnSc7	otrokyně
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Hagarou	Hagara	k1gFnSc7	Hagara
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
měl	mít	k5eAaImAgMnS	mít
Izák	Izák	k1gMnSc1	Izák
ještě	ještě	k9	ještě
dalších	další	k2eAgNnPc2d1	další
šest	šest	k4xCc1	šest
a	a	k8xC	a
i	i	k9	i
více	hodně	k6eAd2	hodně
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
"	"	kIx"	"
<g/>
dědicem	dědic	k1gMnSc7	dědic
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
Izák	Izák	k1gMnSc1	Izák
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
Abraham	Abraham	k1gMnSc1	Abraham
měl	mít	k5eAaImAgMnS	mít
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Sárou	Sára	k1gFnSc7	Sára
a	a	k8xC	a
Hospodin	Hospodin	k1gMnSc1	Hospodin
ho	on	k3xPp3gInSc4	on
určil	určit	k5eAaPmAgMnS	určit
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc4	jeho
dědice	dědic	k1gMnPc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Izákovo	Izákův	k2eAgNnSc1d1	Izákovo
narození	narození	k1gNnSc1	narození
bylo	být	k5eAaImAgNnS	být
zaslíbené	zaslíbený	k2eAgNnSc1d1	zaslíbené
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
Abrahám	Abrahám	k1gMnSc1	Abrahám
a	a	k8xC	a
Sára	Sára	k1gFnSc1	Sára
totiž	totiž	k9	totiž
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
velice	velice	k6eAd1	velice
staří	starý	k2eAgMnPc1d1	starý
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abrahám	Abrahám	k1gMnSc1	Abrahám
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
zkoušen	zkoušen	k2eAgInSc1d1	zkoušen
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
obětovat	obětovat	k5eAaBmF	obětovat
Izáka	Izák	k1gMnSc4	Izák
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
chtěl	chtít	k5eAaImAgMnS	chtít
vědět	vědět	k5eAaImF	vědět
jestli	jestli	k8xS	jestli
mu	on	k3xPp3gNnSc3	on
Abrahám	Abrahám	k1gMnSc1	Abrahám
opravdu	opravdu	k6eAd1	opravdu
důvěřuje	důvěřovat	k5eAaImIp3nS	důvěřovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Abrahám	Abrahám	k1gMnSc1	Abrahám
s	s	k7c7	s
důvěrou	důvěra	k1gFnSc7	důvěra
uposlechl	uposlechnout	k5eAaPmAgInS	uposlechnout
a	a	k8xC	a
šel	jít	k5eAaImAgInS	jít
Izáka	Izák	k1gMnSc4	Izák
obětovat	obětovat	k5eAaBmF	obětovat
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Morija	Morij	k1gInSc2	Morij
(	(	kIx(	(
<g/>
též	též	k9	též
Sion	Sion	k1gInSc1	Sion
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
Šalomounův	Šalomounův	k2eAgInSc1d1	Šalomounův
chrám	chrám	k1gInSc1	chrám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
řekl	říct	k5eAaPmAgMnS	říct
anděl	anděl	k1gMnSc1	anděl
Abrahámovi	Abrahám	k1gMnSc3	Abrahám
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
zkoušce	zkouška	k1gFnSc6	zkouška
uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
ať	ať	k9	ať
místo	místo	k1gNnSc1	místo
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
obětuje	obětovat	k5eAaBmIp3nS	obětovat
berana	beran	k1gMnSc4	beran
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Významem	význam	k1gInSc7	význam
tohoto	tento	k3xDgInSc2	tento
mýtu	mýtus	k1gInSc2	mýtus
je	být	k5eAaImIp3nS	být
zákaz	zákaz	k1gInSc1	zákaz
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
jednou	jednou	k6eAd1	jednou
provždy	provždy	k6eAd1	provždy
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Synové	syn	k1gMnPc1	syn
Jákob	Jákob	k1gMnSc1	Jákob
a	a	k8xC	a
Ezau	Ezau	k1gMnSc1	Ezau
==	==	k?	==
</s>
</p>
<p>
<s>
Izák	Izák	k1gMnSc1	Izák
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Rebeku	Rebeka	k1gFnSc4	Rebeka
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
60	[number]	k4	60
let	léto	k1gNnPc2	léto
zplodil	zplodit	k5eAaPmAgMnS	zplodit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
:	:	kIx,	:
Ezaua	Ezau	k1gMnSc2	Ezau
–	–	k?	–
o	o	k7c4	o
chvíli	chvíle	k1gFnSc4	chvíle
staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
zarostlý	zarostlý	k2eAgInSc1d1	zarostlý
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jákoba	Jákob	k1gMnSc4	Jákob
<g/>
.	.	kIx.	.
</s>
<s>
Otcovo	otcův	k2eAgNnSc4d1	otcovo
požehnání	požehnání	k1gNnSc4	požehnání
měl	mít	k5eAaImAgInS	mít
správně	správně	k6eAd1	správně
dostat	dostat	k5eAaPmF	dostat
Ezau	Eza	k1gMnSc3	Eza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Rebeka	Rebeka	k1gFnSc1	Rebeka
chtěla	chtít	k5eAaImAgFnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
požehnání	požehnání	k1gNnSc4	požehnání
dostal	dostat	k5eAaPmAgMnS	dostat
Jákob	Jákob	k1gMnSc1	Jákob
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Rebeka	Rebeka	k1gFnSc1	Rebeka
s	s	k7c7	s
Jákobem	Jákob	k1gMnSc7	Jákob
přelstili	přelstít	k5eAaPmAgMnP	přelstít
už	už	k9	už
starého	starý	k2eAgNnSc2d1	staré
a	a	k8xC	a
slepého	slepý	k2eAgMnSc2d1	slepý
Izáka	Izák	k1gMnSc2	Izák
a	a	k8xC	a
otcovo	otcův	k2eAgNnSc4d1	otcovo
požehnání	požehnání	k1gNnSc4	požehnání
získal	získat	k5eAaPmAgMnS	získat
Jákob	Jákob	k1gMnSc1	Jákob
<g/>
.	.	kIx.	.
</s>
<s>
Ezau	Ezau	k1gMnSc1	Ezau
se	se	k3xPyFc4	se
rozzuřil	rozzuřit	k5eAaPmAgMnS	rozzuřit
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
ho	on	k3xPp3gMnSc4	on
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Jákob	Jákob	k1gMnSc1	Jákob
utekl	utéct	k5eAaPmAgMnS	utéct
za	za	k7c7	za
matčiným	matčin	k2eAgMnSc7d1	matčin
bratrem	bratr	k1gMnSc7	bratr
Lábanem	Lában	k1gMnSc7	Lában
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Ráchel	Ráchel	k1gFnSc2	Ráchel
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
sestře	sestra	k1gFnSc3	sestra
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jí	on	k3xPp3gFnSc3	on
Laban	Laban	k1gInSc1	Laban
podstrčil	podstrčit	k5eAaPmAgInS	podstrčit
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
místo	místo	k7c2	místo
Ráchel	Ráchel	k1gFnSc2	Ráchel
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
strýci	strýc	k1gMnPc1	strýc
Jákob	Jákob	k1gMnSc1	Jákob
14	[number]	k4	14
let	let	k1gInSc1	let
sloužil	sloužit	k5eAaImAgInS	sloužit
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
léta	léto	k1gNnPc4	léto
za	za	k7c4	za
brav	brav	k1gInSc4	brav
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
Hospodin	Hospodin	k1gMnSc1	Hospodin
povolal	povolat	k5eAaPmAgMnS	povolat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Izák	Izák	k1gMnSc1	Izák
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
umění	umění	k1gNnSc6	umění
bývá	bývat	k5eAaImIp3nS	bývat
zobrazováno	zobrazovat	k5eAaImNgNnS	zobrazovat
hlavně	hlavně	k9	hlavně
obětování	obětování	k1gNnSc1	obětování
Izáka	Izák	k1gMnSc2	Izák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Abrahám	Abrahám	k1gMnSc1	Abrahám
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
chystá	chystat	k5eAaImIp3nS	chystat
obětovat	obětovat	k5eAaBmF	obětovat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
svázaného	svázaný	k2eAgInSc2d1	svázaný
na	na	k7c6	na
obětním	obětní	k2eAgInSc6d1	obětní
stole	stol	k1gInSc6	stol
a	a	k8xC	a
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
anděl	anděl	k1gMnSc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
Izákovo	Izákův	k2eAgNnSc4d1	Izákovo
požehnání	požehnání	k1gNnSc4	požehnání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c6	na
starém	starý	k2eAgMnSc6d1	starý
slepém	slepý	k2eAgMnSc6d1	slepý
Izákovi	Izák	k1gMnSc6	Izák
lstivě	lstivě	k6eAd1	lstivě
získává	získávat	k5eAaImIp3nS	získávat
Jákob	Jákob	k1gMnSc1	Jákob
místo	místo	k1gNnSc4	místo
Ezaua	Ezau	k1gMnSc2	Ezau
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
příběhu	příběh	k1gInSc2	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vyjádřeno	vyjádřen	k2eAgNnSc1d1	vyjádřeno
odtržení	odtržení	k1gNnSc1	odtržení
od	od	k7c2	od
pohanských	pohanský	k2eAgInPc2d1	pohanský
kultů	kult	k1gInPc2	kult
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidské	lidský	k2eAgFnPc1d1	lidská
oběti	oběť	k1gFnPc1	oběť
byly	být	k5eAaImAgFnP	být
někdy	někdy	k6eAd1	někdy
součástí	součást	k1gFnSc7	součást
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
tím	ten	k3xDgNnSc7	ten
jasně	jasně	k6eAd1	jasně
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
své	svůj	k3xOyFgNnSc4	svůj
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
a	a	k8xC	a
jasný	jasný	k2eAgInSc4d1	jasný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Izák	Izák	k1gMnSc1	Izák
je	být	k5eAaImIp3nS	být
také	také	k9	také
předobrazem	předobraz	k1gInSc7	předobraz
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
poslušnosti	poslušnost	k1gFnSc6	poslušnost
svému	svůj	k3xOyFgMnSc3	svůj
Otci	otec	k1gMnSc3	otec
přinesl	přinést	k5eAaPmAgMnS	přinést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
tu	tu	k6eAd1	tu
ale	ale	k8xC	ale
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
beránek	beránek	k1gInSc1	beránek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gFnSc4	jeho
oběť	oběť	k1gFnSc4	oběť
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
beránkem	beránek	k1gMnSc7	beránek
byl	být	k5eAaImAgMnS	být
Kristus	Kristus	k1gMnSc1	Kristus
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Izák	Izák	k1gMnSc1	Izák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Isaak	Isaak	k1gInSc1	Isaak
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
