<s>
Izák	Izák	k1gMnSc1	Izák
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Rebeku	Rebeka	k1gFnSc4	Rebeka
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
60	[number]	k4	60
let	léto	k1gNnPc2	léto
zplodil	zplodit	k5eAaPmAgMnS	zplodit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
:	:	kIx,	:
Ezaua	Ezau	k1gMnSc2	Ezau
–	–	k?	–
o	o	k7c4	o
chvíli	chvíle	k1gFnSc4	chvíle
staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
zarostlý	zarostlý	k2eAgInSc1d1	zarostlý
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jákoba	Jákob	k1gMnSc4	Jákob
<g/>
.	.	kIx.	.
</s>
