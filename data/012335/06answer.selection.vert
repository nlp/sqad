<s>
Zvrat	zvrat	k1gInSc1	zvrat
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Suplex	Suplex	k1gInSc1	Suplex
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zápasnický	zápasnický	k2eAgInSc1d1	zápasnický
chvat	chvat	k1gInSc1	chvat
(	(	kIx(	(
<g/>
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
úpolových	úpolův	k2eAgInPc6d1	úpolův
sportech	sport	k1gInPc6	sport
a	a	k8xC	a
v	v	k7c6	v
MMA	MMA	kA	MMA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gInSc6	jehož
provádění	provádění	k1gNnSc6	provádění
přehazuje	přehazovat	k5eAaImIp3nS	přehazovat
zápasník	zápasník	k1gMnSc1	zápasník
soupeře	soupeř	k1gMnSc2	soupeř
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
hruď	hruď	k1gFnSc4	hruď
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
zvrací	zvracet	k5eAaImIp3nS	zvracet
vzad	vzad	k6eAd1	vzad
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
mostem	most	k1gInSc7	most
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
záklonem	záklon	k1gInSc7	záklon
<g/>
.	.	kIx.	.
</s>
