<s>
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Opírá	opírat	k5eAaImIp3nS	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
principy	princip	k1gInPc4	princip
skautingu	skauting	k1gInSc2	skauting
<g/>
,	,	kIx,	,
chlapeckého	chlapecký	k2eAgNnSc2d1	chlapecké
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
odvážného	odvážný	k2eAgNnSc2d1	odvážné
táborového	táborový	k2eAgNnSc2d1	táborové
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
Jirka	Jirka	k1gMnSc1	Jirka
<g/>
,	,	kIx,	,
Vilík	Vilík	k1gMnSc1	Vilík
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
Rikitan	Rikitan	k1gInSc1	Rikitan
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
psal	psát	k5eAaImAgMnS	psát
text	text	k1gInSc4	text
v	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
pak	pak	k6eAd1	pak
vycházel	vycházet	k5eAaImAgInS	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Románu	román	k1gInSc3	román
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vytknout	vytknout	k5eAaPmF	vytknout
přílišné	přílišný	k2eAgNnSc4d1	přílišné
zdůrazňování	zdůrazňování	k1gNnSc4	zdůrazňování
kultu	kult	k1gInSc2	kult
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
ostrou	ostrý	k2eAgFnSc4d1	ostrá
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
skvostnými	skvostný	k2eAgMnPc7d1	skvostný
hochy	hoch	k1gMnPc7	hoch
<g/>
"	"	kIx"	"
a	a	k8xC	a
obyčejnými	obyčejný	k2eAgMnPc7d1	obyčejný
chlapci	chlapec	k1gMnPc7	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
Jirka	Jirka	k1gFnSc1	Jirka
a	a	k8xC	a
Vilík	Vilík	k1gMnSc1	Vilík
poperou	poprat	k5eAaPmIp3nP	poprat
s	s	k7c7	s
chlapci	chlapec	k1gMnPc7	chlapec
z	z	k7c2	z
Nové	Nové	k2eAgFnSc2d1	Nové
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
po	po	k7c6	po
rvačce	rvačka	k1gFnSc6	rvačka
je	on	k3xPp3gNnSc4	on
osloví	oslovit	k5eAaPmIp3nS	oslovit
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
říká	říkat	k5eAaImIp3nS	říkat
Rikitan	Rikitan	k1gInSc4	Rikitan
<g/>
.	.	kIx.	.
</s>
<s>
Poví	povědět	k5eAaPmIp3nS	povědět
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
sehnali	sehnat	k5eAaPmAgMnP	sehnat
další	další	k2eAgMnPc4d1	další
hochy	hoch	k1gMnPc4	hoch
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
chtěli	chtít	k5eAaImAgMnP	chtít
zkusit	zkusit	k5eAaPmF	zkusit
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
sejdou	sejít	k5eAaPmIp3nP	sejít
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Chlapci	chlapec	k1gMnPc1	chlapec
se	se	k3xPyFc4	se
domluvili	domluvit	k5eAaPmAgMnP	domluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
"	"	kIx"	"
<g/>
bratrstvem	bratrstvo	k1gNnSc7	bratrstvo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
upraví	upravit	k5eAaPmIp3nS	upravit
si	se	k3xPyFc3	se
opuštěný	opuštěný	k2eAgInSc4d1	opuštěný
altán	altán	k1gInSc4	altán
<g/>
,	,	kIx,	,
vydělají	vydělat	k5eAaPmIp3nP	vydělat
si	se	k3xPyFc3	se
na	na	k7c6	na
stejnokroje	stejnokroj	k1gInSc2	stejnokroj
a	a	k8xC	a
s	s	k7c7	s
ručními	ruční	k2eAgInPc7d1	ruční
vozíky	vozík	k1gInPc7	vozík
jedou	jet	k5eAaImIp3nP	jet
na	na	k7c4	na
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
táboře	tábor	k1gInSc6	tábor
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávěn	k2eAgInSc4d1	vyprávěn
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
Royovi	Roy	k1gMnSc6	Roy
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
starého	starý	k2eAgMnSc2d1	starý
indiána	indián	k1gMnSc2	indián
loví	lovit	k5eAaImIp3nS	lovit
"	"	kIx"	"
<g/>
bobříky	bobřík	k1gMnPc7	bobřík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chlapci	chlapec	k1gMnPc1	chlapec
také	také	k9	také
začnou	začít	k5eAaPmIp3nP	začít
lovit	lovit	k5eAaImF	lovit
bobříky	bobřík	k1gMnPc4	bobřík
(	(	kIx(	(
<g/>
plní	plnit	k5eAaImIp3nS	plnit
různé	různý	k2eAgInPc4d1	různý
úkoly	úkol	k1gInPc4	úkol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
světa	svět	k1gInSc2	svět
začne	začít	k5eAaPmIp3nS	začít
motat	motat	k5eAaImF	motat
záhadná	záhadný	k2eAgFnSc1d1	záhadná
Zelená	zelený	k2eAgFnSc1d1	zelená
příšera	příšera	k1gFnSc1	příšera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
táboráku	táborák	k1gInSc6	táborák
je	být	k5eAaImIp3nS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
tajemství	tajemství	k1gNnSc1	tajemství
Zelené	Zelené	k2eAgFnSc2d1	Zelené
příšery	příšera	k1gFnSc2	příšera
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poděkováno	poděkován	k2eAgNnSc1d1	poděkováno
Rikitanovi	Rikitanův	k2eAgMnPc1d1	Rikitanův
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
se	s	k7c7	s
chlapci	chlapec	k1gMnPc7	chlapec
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
určité	určitý	k2eAgFnPc4d1	určitá
analogie	analogie	k1gFnPc4	analogie
k	k	k7c3	k
Setonovu	Setonův	k2eAgInSc3d1	Setonův
románu	román	k1gInSc3	román
Dva	dva	k4xCgMnPc1	dva
divoši	divoch	k1gMnPc1	divoch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
napsal	napsat	k5eAaPmAgInS	napsat
Foglar	Foglar	k1gInSc1	Foglar
pokračování	pokračování	k1gNnSc2	pokračování
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
románu	román	k1gInSc2	román
s	s	k7c7	s
názvem	název	k1gInSc7	název
Strach	strach	k1gInSc1	strach
nad	nad	k7c7	nad
Bobří	bobří	k2eAgFnSc7d1	bobří
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nacistická	nacistický	k2eAgFnSc1d1	nacistická
okupace	okupace	k1gFnSc1	okupace
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
budoucí	budoucí	k2eAgFnSc4d1	budoucí
existenci	existence	k1gFnSc4	existence
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Foglarovy	Foglarův	k2eAgFnSc2d1	Foglarova
tvorby	tvorba	k1gFnSc2	tvorba
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
výjimku	výjimka	k1gFnSc4	výjimka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
neurčené	určený	k2eNgFnSc6d1	neurčená
době	doba	k1gFnSc6	doba
a	a	k8xC	a
chod	chod	k1gInSc4	chod
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
nijak	nijak	k6eAd1	nijak
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
Literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Hoši	hoch	k1gMnPc1	hoch
od	od	k7c2	od
Bobří	bobří	k2eAgFnSc2d1	bobří
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
foglar	foglar	k1gMnSc1	foglar
<g/>
web.deg.cz	web.deg.cz	k1gMnSc1	web.deg.cz
<g/>
)	)	kIx)	)
</s>
