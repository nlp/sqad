<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
(	(	kIx(	(
<g/>
z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
Bilgames	Bilgames	k1gInSc4	Bilgames
–	–	k?	–
výhonek	výhonek	k1gInSc1	výhonek
stromu	strom	k1gInSc2	strom
mésu	mésu	k5eAaPmIp1nS	mésu
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
strom	strom	k1gInSc1	strom
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
Gilgamos	Gilgamos	k1gInSc1	Gilgamos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
epos	epos	k1gInSc4	epos
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
líčící	líčící	k2eAgNnSc4d1	líčící
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
sumerského	sumerský	k2eAgMnSc2d1	sumerský
krále	král	k1gMnSc2	král
Gilgameše	Gilgameše	k1gMnSc2	Gilgameše
<g/>
.	.	kIx.	.
</s>
