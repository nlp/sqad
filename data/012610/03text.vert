<p>
<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
(	(	kIx(	(
<g/>
z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
Bilgames	Bilgames	k1gInSc4	Bilgames
–	–	k?	–
výhonek	výhonek	k1gInSc1	výhonek
stromu	strom	k1gInSc2	strom
mésu	mésu	k5eAaPmIp1nS	mésu
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
strom	strom	k1gInSc1	strom
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
Gilgamos	Gilgamos	k1gInSc1	Gilgamos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
epos	epos	k1gInSc4	epos
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
líčící	líčící	k2eAgNnSc4d1	líčící
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
sumerského	sumerský	k2eAgMnSc2d1	sumerský
krále	král	k1gMnSc2	král
Gilgameše	Gilgameše	k1gMnSc2	Gilgameše
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dochované	dochovaný	k2eAgNnSc4d1	dochované
epické	epický	k2eAgNnSc4d1	epické
dílo	dílo	k1gNnSc4	dílo
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
sumerské	sumerský	k2eAgFnSc2d1	sumerská
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
Gilgames	Gilgames	k1gMnSc1	Gilgames
přesunul	přesunout	k5eAaPmAgMnS	přesunout
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
známějším	známý	k2eAgNnSc7d2	známější
jménem	jméno	k1gNnSc7	jméno
Gilgameš	Gilgameš	k1gFnSc2	Gilgameš
<g/>
)	)	kIx)	)
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
akkadské	akkadský	k2eAgFnSc2d1	akkadská
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgInPc4d1	obdobný
eposy	epos	k1gInPc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
chetitské	chetitský	k2eAgFnSc6d1	Chetitská
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
mytologiích	mytologie	k1gFnPc6	mytologie
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Překladem	překlad	k1gInSc7	překlad
a	a	k8xC	a
úpravami	úprava	k1gFnPc7	úprava
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
podstatným	podstatný	k2eAgFnPc3d1	podstatná
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
verze	verze	k1gFnPc1	verze
mohou	moct	k5eAaImIp3nP	moct
výrazně	výrazně	k6eAd1	výrazně
odlišovat	odlišovat	k5eAaImF	odlišovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Claudius	Claudius	k1gMnSc1	Claudius
Aelianus	Aelianus	k1gMnSc1	Aelianus
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zvláštnosti	zvláštnost	k1gFnSc2	zvláštnost
zvířat	zvíře	k1gNnPc2	zvíře
popisuje	popisovat	k5eAaImIp3nS	popisovat
nechtěného	chtěný	k2eNgMnSc4d1	nechtěný
vnuka	vnuk	k1gMnSc4	vnuk
babylónského	babylónský	k2eAgMnSc4d1	babylónský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
hozen	hodit	k5eAaPmNgMnS	hodit
z	z	k7c2	z
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
zachráněn	zachráněn	k2eAgInSc1d1	zachráněn
orlem	orel	k1gMnSc7	orel
a	a	k8xC	a
vychován	vychovat	k5eAaPmNgMnS	vychovat
zahradníkem	zahradník	k1gMnSc7	zahradník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
165	[number]	k4	165
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
středoperských	středoperský	k2eAgInPc6d1	středoperský
textech	text	k1gInPc6	text
echejců	echejec	k1gMnPc2	echejec
a	a	k8xC	a
manichejců	manichejec	k1gMnPc2	manichejec
vztahujících	vztahující	k2eAgNnPc2d1	vztahující
se	se	k3xPyFc4	se
ke	k	k7c3	k
Gn	Gn	k1gFnSc3	Gn
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
k	k	k7c3	k
první	první	k4xOgFnSc3	první
knize	kniha	k1gFnSc3	kniha
Henochově	Henochův	k2eAgFnSc3d1	Henochova
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Theodor	Theodor	k1gMnSc1	Theodor
bar	bar	k1gInSc4	bar
Kóní	Kóní	k1gFnSc2	Kóní
považuje	považovat	k5eAaImIp3nS	považovat
jakéhosi	jakýsi	k3yIgMnSc4	jakýsi
Gmígmóse	Gmígmós	k1gMnSc4	Gmígmós
za	za	k7c4	za
současníka	současník	k1gMnSc4	současník
Abraháma	Abrahám	k1gMnSc4	Abrahám
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sumerská	sumerský	k2eAgFnSc1d1	sumerská
verze	verze	k1gFnSc1	verze
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pět	pět	k4xCc4	pět
epických	epický	k2eAgFnPc2d1	epická
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
dochované	dochovaný	k2eAgInPc1d1	dochovaný
opisy	opis	k1gInPc1	opis
pocházejí	pocházet	k5eAaImIp3nP	pocházet
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
1700	[number]	k4	1700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
a	a	k8xC	a
Akka	Akka	k1gMnSc1	Akka
===	===	k?	===
</s>
</p>
<p>
<s>
115	[number]	k4	115
veršů	verš	k1gInPc2	verš
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
z	z	k7c2	z
Nippuru	Nippur	k1gInSc2	Nippur
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Akkově	Akkov	k1gInSc6	Akkov
(	(	kIx(	(
<g/>
panovník	panovník	k1gMnSc1	panovník
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc1	dynastie
Kiše	Kiše	k1gFnSc1	Kiše
<g/>
)	)	kIx)	)
obléhání	obléhání	k1gNnSc1	obléhání
Uruku	Uruk	k1gInSc2	Uruk
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
obléhání	obléhání	k1gNnSc2	obléhání
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Akkův	Akkův	k2eAgInSc1d1	Akkův
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
zavlažovacích	zavlažovací	k2eAgInPc2d1	zavlažovací
kanálů	kanál	k1gInPc2	kanál
Uruku	Uruk	k1gInSc2	Uruk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tehdy	tehdy	k6eAd1	tehdy
prakticky	prakticky	k6eAd1	prakticky
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
splnění	splnění	k1gNnSc4	splnění
tohoto	tento	k3xDgInSc2	tento
požadavku	požadavek	k1gInSc2	požadavek
se	se	k3xPyFc4	se
radilo	radit	k5eAaImAgNnS	radit
"	"	kIx"	"
<g/>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
starších	starší	k1gMnPc2	starší
v	v	k7c6	v
Uruku	Uruk	k1gInSc6	Uruk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
splnit	splnit	k5eAaPmF	splnit
Akkova	Akkův	k2eAgNnSc2d1	Akkův
ultimáta	ultimátum	k1gNnSc2	ultimátum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
shromáždění	shromáždění	k1gNnSc1	shromáždění
mladších	mladý	k2eAgMnPc2d2	mladší
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
shromáždění	shromáždění	k1gNnSc2	shromáždění
mladších	mladý	k2eAgMnPc2d2	mladší
bojovníků	bojovník	k1gMnPc2	bojovník
se	se	k3xPyFc4	se
Enkidu	Enkid	k1gInSc3	Enkid
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
nejprve	nejprve	k6eAd1	nejprve
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
(	(	kIx(	(
<g/>
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
vede	vést	k5eAaImIp3nS	vést
Gilgamesův	Gilgamesův	k2eAgMnSc1d1	Gilgamesův
královský	královský	k2eAgMnSc1d1	královský
velitel	velitel	k1gMnSc1	velitel
Birchurtura	Birchurtura	k1gFnSc1	Birchurtura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
tohoto	tento	k3xDgNnSc2	tento
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
urucké	urucký	k2eAgNnSc1d1	urucký
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Enkiduem	Enkidu	k1gInSc7	Enkidu
pokusí	pokusit	k5eAaPmIp3nS	pokusit
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
obležení	obležení	k1gNnSc2	obležení
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
bitva	bitva	k1gFnSc1	bitva
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
uruckého	urucký	k2eAgInSc2d1	urucký
pohledu	pohled	k1gInSc2	pohled
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
především	především	k6eAd1	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
hradbách	hradba	k1gFnPc6	hradba
objeví	objevit	k5eAaPmIp3nS	objevit
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
je	být	k5eAaImIp3nS	být
Akka	Akka	k1gMnSc1	Akka
zajat	zajmout	k5eAaPmNgMnS	zajmout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Gilgames	Gilgames	k1gMnSc1	Gilgames
ho	on	k3xPp3gNnSc4	on
zcela	zcela	k6eAd1	zcela
nečekaně	nečekaně	k6eAd1	nečekaně
nezabije	zabít	k5eNaPmIp3nS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
nějakou	nějaký	k3yIgFnSc4	nějaký
neznámou	známý	k2eNgFnSc4d1	neznámá
službu	služba	k1gFnSc4	služba
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
ho	on	k3xPp3gMnSc4	on
propustí	propustit	k5eAaPmIp3nS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
(	(	kIx(	(
<g/>
časově	časově	k6eAd1	časově
následujících	následující	k2eAgInPc6d1	následující
<g/>
)	)	kIx)	)
mýtech	mýtus	k1gInPc6	mýtus
nikde	nikde	k6eAd1	nikde
neobjevil	objevit	k5eNaPmAgInS	objevit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
a	a	k8xC	a
Chuvava	Chuvava	k1gFnSc1	Chuvava
===	===	k?	===
</s>
</p>
<p>
<s>
Sumerská	sumerský	k2eAgFnSc1d1	sumerská
epická	epický	k2eAgFnSc1d1	epická
skladba	skladba	k1gFnSc1	skladba
známá	známý	k2eAgFnSc1d1	známá
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
(	(	kIx(	(
<g/>
základní	základní	k2eAgInSc1d1	základní
text	text	k1gInSc1	text
má	mít	k5eAaImIp3nS	mít
202	[number]	k4	202
veršů	verš	k1gInPc2	verš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
opisů	opis	k1gInPc2	opis
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
mezopotámských	mezopotámský	k2eAgNnPc2d1	mezopotámský
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Uruku	Uruk	k1gInSc6	Uruk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
s	s	k7c7	s
Enkiduem	Enkidu	k1gInSc7	Enkidu
připravují	připravovat	k5eAaImIp3nP	připravovat
na	na	k7c4	na
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Chuvavovi	Chuvava	k1gMnSc3	Chuvava
(	(	kIx(	(
<g/>
též	též	k6eAd1	též
Chuwawa	Chuwawa	k1gFnSc1	Chuwawa
<g/>
,	,	kIx,	,
Chumbaba	Chumbaba	k1gFnSc1	Chumbaba
<g/>
,	,	kIx,	,
Chubibi	Chubibi	k1gNnSc1	Chubibi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
tažení	tažení	k1gNnSc4	tažení
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Gilgames	Gilgames	k1gMnSc1	Gilgames
viděl	vidět	k5eAaImAgMnS	vidět
po	po	k7c6	po
Eufratu	Eufrat	k1gInSc6	Eufrat
plavat	plavat	k5eAaImF	plavat
mrtvoly	mrtvola	k1gFnPc4	mrtvola
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
sen	sen	k1gInSc1	sen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
lekl	leknout	k5eAaPmAgMnS	leknout
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
tažení	tažení	k1gNnSc1	tažení
mu	on	k3xPp3gMnSc3	on
mělo	mít	k5eAaImAgNnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
(	(	kIx(	(
<g/>
jinde	jinde	k6eAd1	jinde
slávu	sláva	k1gFnSc4	sláva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
výpravu	výprava	k1gFnSc4	výprava
si	se	k3xPyFc3	se
vzali	vzít	k5eAaPmAgMnP	vzít
padesát	padesát	k4xCc4	padesát
průvodců	průvodce	k1gMnPc2	průvodce
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
svobodné	svobodný	k2eAgMnPc4d1	svobodný
občany	občan	k1gMnPc4	občan
města	město	k1gNnSc2	město
Uruku	Uruk	k1gInSc6	Uruk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neměli	mít	k5eNaImAgMnP	mít
co	co	k3yRnSc1	co
ztratit	ztratit	k5eAaPmF	ztratit
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
druhé	druhý	k4xOgFnSc2	druhý
verze	verze	k1gFnSc2	verze
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vojáky	voják	k1gMnPc4	voják
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gilgames	Gilgames	k1gMnSc1	Gilgames
to	ten	k3xDgNnSc4	ten
oznámil	oznámit	k5eAaPmAgMnS	oznámit
napřed	napřed	k6eAd1	napřed
slunečnímu	sluneční	k2eAgMnSc3d1	sluneční
bohu	bůh	k1gMnSc3	bůh
Utovi	Utoev	k1gFnSc3	Utoev
(	(	kIx(	(
<g/>
Šamaš	Šamaš	k1gMnSc1	Šamaš
<g/>
,	,	kIx,	,
Šamšu	Šamšu	k1gMnSc1	Šamšu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
(	(	kIx(	(
<g/>
půjčil	půjčit	k5eAaPmAgMnS	půjčit
<g/>
)	)	kIx)	)
sedm	sedm	k4xCc1	sedm
démonických	démonický	k2eAgMnPc2d1	démonický
bratrů	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
překonal	překonat	k5eAaPmAgInS	překonat
sedmero	sedmero	k1gNnSc4	sedmero
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
kácet	kácet	k5eAaImF	kácet
vybraný	vybraný	k2eAgInSc1d1	vybraný
cedr	cedr	k1gInSc1	cedr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
zjistil	zjistit	k5eAaPmAgMnS	zjistit
Chuvava	Chuvava	k1gFnSc1	Chuvava
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
svého	svůj	k3xOyFgMnSc4	svůj
pomocníka	pomocník	k1gMnSc4	pomocník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
uspal	uspat	k5eAaPmAgMnS	uspat
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
probudil	probudit	k5eAaPmAgMnS	probudit
Enkidu	Enkid	k1gInSc2	Enkid
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
probudil	probudit	k5eAaPmAgInS	probudit
Gilgamese	Gilgamese	k1gFnPc4	Gilgamese
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
mu	on	k3xPp3gMnSc3	on
rozmlouvat	rozmlouvat	k5eAaImF	rozmlouvat
snahu	snaha	k1gFnSc4	snaha
zabít	zabít	k5eAaPmF	zabít
Chuvavu	Chuvava	k1gFnSc4	Chuvava
<g/>
.	.	kIx.	.
</s>
<s>
Gilgames	Gilgames	k1gMnSc1	Gilgames
však	však	k9	však
Enkidua	Enkidua	k1gMnSc1	Enkidua
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
výpravu	výprava	k1gFnSc4	výprava
nevzdávali	vzdávat	k5eNaImAgMnP	vzdávat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
došli	dojít	k5eAaPmAgMnP	dojít
až	až	k9	až
k	k	k7c3	k
Chuvavovi	Chuvava	k1gMnSc3	Chuvava
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stál	stát	k5eAaImAgInS	stát
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
domem	dům	k1gInSc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
podvodně	podvodně	k6eAd1	podvodně
(	(	kIx(	(
<g/>
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
mu	on	k3xPp3gMnSc3	on
slíbil	slíbit	k5eAaPmAgMnS	slíbit
své	svůj	k3xOyFgFnPc4	svůj
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
za	za	k7c2	za
ženy	žena	k1gFnSc2	žena
<g/>
)	)	kIx)	)
vylákali	vylákat	k5eAaPmAgMnP	vylákat
jeho	jeho	k3xOp3gFnPc2	jeho
sedm	sedm	k4xCc1	sedm
ochranných	ochranný	k2eAgFnPc2d1	ochranná
září	zář	k1gFnPc2	zář
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
Chuvava	Chuvava	k1gFnSc1	Chuvava
bezmocný	bezmocný	k2eAgMnSc1d1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
Gilgames	Gilgames	k1gMnSc1	Gilgames
ho	on	k3xPp3gMnSc4	on
nejprve	nejprve	k6eAd1	nejprve
nechtěl	chtít	k5eNaImAgMnS	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Enkidu	Enkida	k1gFnSc4	Enkida
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
vystavovali	vystavovat	k5eAaImAgMnP	vystavovat
velkému	velký	k2eAgNnSc3d1	velké
riziku	riziko	k1gNnSc3	riziko
a	a	k8xC	a
usekl	useknout	k5eAaPmAgMnS	useknout
mu	on	k3xPp3gMnSc3	on
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
hlavu	hlava	k1gFnSc4	hlava
odnesli	odnést	k5eAaPmAgMnP	odnést
Enlilovi	Enlilův	k2eAgMnPc1d1	Enlilův
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
rozzlobil	rozzlobit	k5eAaPmAgMnS	rozzlobit
a	a	k8xC	a
záře	zář	k1gFnPc4	zář
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
různým	různý	k2eAgFnPc3d1	různá
přírodním	přírodní	k2eAgFnPc3d1	přírodní
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
akkadskou	akkadský	k2eAgFnSc7d1	akkadská
a	a	k8xC	a
chetitskou	chetitský	k2eAgFnSc4d1	Chetitská
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sumerští	sumerský	k2eAgMnPc1d1	sumerský
vládci	vládce	k1gMnPc1	vládce
podnikali	podnikat	k5eAaImAgMnP	podnikat
válečné	válečný	k2eAgFnPc4d1	válečná
výpravy	výprava	k1gFnPc4	výprava
pro	pro	k7c4	pro
cedrové	cedrový	k2eAgNnSc4d1	cedrové
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Sumeru	Sumer	k1gInSc6	Sumer
velmi	velmi	k6eAd1	velmi
ceněné	ceněný	k2eAgFnPc1d1	ceněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
a	a	k8xC	a
nebeský	nebeský	k2eAgMnSc1d1	nebeský
býk	býk	k1gMnSc1	býk
===	===	k?	===
</s>
</p>
<p>
<s>
Epická	epický	k2eAgFnSc1d1	epická
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
v	v	k7c6	v
několika	několik	k4yIc6	několik
fragmentech	fragment	k1gInPc6	fragment
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
přibližně	přibližně	k6eAd1	přibližně
140	[number]	k4	140
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
popisováno	popisován	k2eAgNnSc1d1	popisováno
setkání	setkání	k1gNnSc1	setkání
Gilgamese	Gilgamese	k1gFnSc2	Gilgamese
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Inannou	Inanný	k2eAgFnSc7d1	Inanný
(	(	kIx(	(
<g/>
Ištar	Ištara	k1gFnPc2	Ištara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tak	tak	k6eAd1	tak
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
manželství	manželství	k1gNnPc4	manželství
(	(	kIx(	(
<g/>
někde	někde	k6eAd1	někde
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
známost	známost	k1gFnSc4	známost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gilgames	Gilgames	k1gMnSc1	Gilgames
ji	on	k3xPp3gFnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
(	(	kIx(	(
<g/>
vypočítával	vypočítávat	k5eAaImAgMnS	vypočítávat
jí	on	k3xPp3gFnSc3	on
bývalé	bývalý	k2eAgFnSc3d1	bývalá
milence	milenka	k1gFnSc3	milenka
a	a	k8xC	a
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
udělala	udělat	k5eAaPmAgFnS	udělat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ji	on	k3xPp3gFnSc4	on
urazil	urazit	k5eAaPmAgInS	urazit
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
–	–	k?	–
boha	bůh	k1gMnSc4	bůh
Ana	Ana	k1gMnSc4	Ana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poslal	poslat	k5eAaPmAgMnS	poslat
proti	proti	k7c3	proti
Uruku	Uruek	k1gMnSc3	Uruek
a	a	k8xC	a
Gilgamesovi	Gilgames	k1gMnSc3	Gilgames
nebeského	nebeský	k2eAgMnSc2d1	nebeský
býka	býk	k1gMnSc2	býk
(	(	kIx(	(
<g/>
sum	suma	k1gFnPc2	suma
<g/>
.	.	kIx.	.
</s>
<s>
Gugalanna	Gugalanna	k1gFnSc1	Gugalanna
<g/>
,	,	kIx,	,
gug-anna	gugnna	k1gFnSc1	gug-anna
<g/>
,	,	kIx,	,
akk	akk	k?	akk
<g/>
.	.	kIx.	.
alap	alap	k1gMnSc1	alap
šami	šam	k1gFnSc2	šam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
po	po	k7c4	po
přemlouvání	přemlouvání	k1gNnSc4	přemlouvání
a	a	k8xC	a
vyhrožování	vyhrožování	k1gNnSc4	vyhrožování
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Anuovi	Anua	k1gMnSc3	Anua
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
nechtělo	chtít	k5eNaImAgNnS	chtít
býka	býk	k1gMnSc4	býk
proti	proti	k7c3	proti
Gilgamesovi	Gilgames	k1gMnSc3	Gilgames
posílat	posílat	k5eAaImF	posílat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
Gilgamese	Gilgamesa	k1gFnSc3	Gilgamesa
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
namítal	namítat	k5eAaImAgMnS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
sešle	seslat	k5eAaPmIp3nS	seslat
na	na	k7c4	na
lidstvo	lidstvo	k1gNnSc4	lidstvo
neúrodu	neúroda	k1gFnSc4	neúroda
na	na	k7c6	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zda	zda	k8xS	zda
Inanna	Inanna	k1gFnSc1	Inanna
udělala	udělat	k5eAaPmAgFnS	udělat
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
jednoho	jeden	k4xCgInSc2	jeden
nemůžou	nemůžou	k?	nemůžou
trpět	trpět	k5eAaImF	trpět
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
teprve	teprve	k9	teprve
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ujištění	ujištění	k1gNnSc6	ujištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
učinila	učinit	k5eAaPmAgFnS	učinit
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc3	on
vydá	vydat	k5eAaPmIp3nS	vydat
býka	býk	k1gMnSc4	býk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inanna	Inanna	k6eAd1	Inanna
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
i	i	k9	i
s	s	k7c7	s
nebeským	nebeský	k2eAgMnSc7d1	nebeský
býkem	býk	k1gMnSc7	býk
<g/>
,	,	kIx,	,
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
Ururku	Ururek	k1gInSc3	Ururek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
býk	býk	k1gMnSc1	býk
začal	začít	k5eAaPmAgMnS	začít
řádit	řádit	k5eAaImF	řádit
<g/>
.	.	kIx.	.
</s>
<s>
Gilgames	Gilgames	k1gMnSc1	Gilgames
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zrovna	zrovna	k6eAd1	zrovna
hodoval	hodovat	k5eAaImAgMnS	hodovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
s	s	k7c7	s
Enkidem	Enkid	k1gInSc7	Enkid
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
ho	on	k3xPp3gMnSc4	on
porazili	porazit	k5eAaPmAgMnP	porazit
a	a	k8xC	a
po	po	k7c6	po
přihlížející	přihlížející	k2eAgFnSc6d1	přihlížející
Inanně	Inanně	k1gFnSc6	Inanně
hodil	hodit	k5eAaPmAgMnS	hodit
kýtu	kýta	k1gFnSc4	kýta
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
přirození	přirozený	k2eAgMnPc1d1	přirozený
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
hází	házet	k5eAaImIp3nS	házet
Enkidu	Enkida	k1gFnSc4	Enkida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozplakala	rozplakat	k5eAaPmAgFnS	rozplakat
a	a	k8xC	a
pak	pak	k6eAd1	pak
Gilgamese	Gilgamese	k1gFnSc1	Gilgamese
proklela	proklít	k5eAaPmAgFnS	proklít
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
býka	býk	k1gMnSc2	býk
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
věnoval	věnovat	k5eAaImAgMnS	věnovat
sirotkům	sirotek	k1gMnPc3	sirotek
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hlavu	hlava	k1gFnSc4	hlava
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
–	–	k?	–
(	(	kIx(	(
<g/>
Lugulbandovi	Lugulband	k1gMnSc6	Lugulband
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ninivské	ninivský	k2eAgFnSc6d1	ninivský
verzi	verze	k1gFnSc6	verze
eposu	epos	k1gInSc2	epos
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
součástí	součást	k1gFnPc2	součást
tabulky	tabulka	k1gFnSc2	tabulka
č.	č.	k?	č.
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
jak	jak	k8xS	jak
zde	zde	k6eAd1	zde
Inanna	Inanna	k1gFnSc1	Inanna
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ostatní	ostatní	k2eAgNnSc1d1	ostatní
fungování	fungování	k1gNnSc1	fungování
této	tento	k3xDgFnSc2	tento
bohyně	bohyně	k1gFnSc2	bohyně
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
bylo	být	k5eAaImAgNnS	být
jiné	jiný	k2eAgNnSc1d1	jiné
–	–	k?	–
kladné	kladný	k2eAgInPc1d1	kladný
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
omezit	omezit	k5eAaPmF	omezit
kult	kult	k1gInSc4	kult
Inanny	Inanna	k1gFnSc2	Inanna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gilgameš	Gilgameš	k1gFnSc4	Gilgameš
<g/>
,	,	kIx,	,
Enkidu	Enkida	k1gFnSc4	Enkida
a	a	k8xC	a
podsvětí	podsvětí	k1gNnSc4	podsvětí
===	===	k?	===
</s>
</p>
<p>
<s>
Sumerská	sumerský	k2eAgFnSc1d1	sumerská
epická	epický	k2eAgFnSc1d1	epická
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
300	[number]	k4	300
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
neshodují	shodovat	k5eNaImIp3nP	shodovat
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
si	se	k3xPyFc3	se
i	i	k9	i
protiřečí	protiřečit	k5eAaImIp3nS	protiřečit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
začíná	začínat	k5eAaImIp3nS	začínat
vylíčením	vylíčení	k1gNnSc7	vylíčení
dávnověku	dávnověk	k1gInSc2	dávnověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bohové	bůh	k1gMnPc1	bůh
vládli	vládnout	k5eAaImAgMnP	vládnout
společně	společně	k6eAd1	společně
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Eufratu	Eufrat	k1gInSc2	Eufrat
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
strom	strom	k1gInSc1	strom
chuluppu	chulupp	k1gInSc2	chulupp
(	(	kIx(	(
<g/>
chalub	chalub	k1gInSc1	chalub
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
vrba	vrba	k1gFnSc1	vrba
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
dub	dub	k1gInSc4	dub
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
také	také	k9	také
topol	topol	k1gInSc1	topol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvrátil	vyvrátit	k5eAaPmAgInS	vyvrátit
jižní	jižní	k2eAgInSc1d1	jižní
vítr	vítr	k1gInSc1	vítr
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
plavil	plavit	k5eAaImAgInS	plavit
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc4	strom
našla	najít	k5eAaPmAgFnS	najít
bohyně	bohyně	k1gFnSc1	bohyně
Inanna	Inanna	k1gFnSc1	Inanna
<g/>
,	,	kIx,	,
vylovila	vylovit	k5eAaPmAgFnS	vylovit
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
zasadila	zasadit	k5eAaPmAgFnS	zasadit
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zahradě	zahrada	k1gFnSc6	zahrada
v	v	k7c6	v
Uruku	Uruk	k1gInSc6	Uruk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
tohoto	tento	k3xDgInSc2	tento
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
usadill	usadilnout	k5eAaPmAgMnS	usadilnout
pták-démon	ptákémon	k1gMnSc1	pták-démon
Anzu	Anzus	k1gInSc2	Anzus
<g/>
,	,	kIx,	,
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
had	had	k1gMnSc1	had
a	a	k8xC	a
v	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
větrná	větrný	k2eAgFnSc1d1	větrná
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
Lilú	Lilú	k1gFnSc1	Lilú
<g/>
,	,	kIx,	,
lilou	lilá	k1gFnSc4	lilá
<g/>
,	,	kIx,	,
akk	akk	k?	akk
<g/>
.	.	kIx.	.
lilitu	lilita	k1gMnSc4	lilita
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
hebr.	hebr.	k?	hebr.
Lilith	Lilith	k1gMnSc1	Lilith
<g/>
)	)	kIx)	)
–	–	k?	–
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Bohyně	bohyně	k1gFnSc1	bohyně
Inanna	Inanna	k1gFnSc1	Inanna
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
stromu	strom	k1gInSc2	strom
chtěla	chtít	k5eAaImAgFnS	chtít
udělat	udělat	k5eAaPmF	udělat
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
lůžko	lůžko	k1gNnSc1	lůžko
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
bytosti	bytost	k1gFnPc1	bytost
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
vadily	vadit	k5eAaImAgInP	vadit
<g/>
,	,	kIx,	,
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
boha	bůh	k1gMnSc2	bůh
Utu	Utu	k1gMnSc2	Utu
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
nevyhověl	vyhovět	k5eNaPmAgMnS	vyhovět
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
požádat	požádat	k5eAaPmF	požádat
Gilgamese	Gilgamese	k1gFnSc1	Gilgamese
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zabil	zabít	k5eAaPmAgMnS	zabít
hada	had	k1gMnSc4	had
a	a	k8xC	a
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
Anzua	Anzuum	k1gNnPc4	Anzuum
a	a	k8xC	a
Lilúu	Lilúa	k1gFnSc4	Lilúa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vyvrátil	vyvrátit	k5eAaPmAgInS	vyvrátit
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
osekal	osekat	k5eAaPmAgMnS	osekat
větve	větev	k1gFnSc2	větev
a	a	k8xC	a
kmen	kmen	k1gInSc1	kmen
věnoval	věnovat	k5eAaPmAgInS	věnovat
bohyni	bohyně	k1gFnSc4	bohyně
(	(	kIx(	(
<g/>
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
verzích	verze	k1gFnPc6	verze
bohyni	bohyně	k1gFnSc3	bohyně
prostě	prostě	k9	prostě
vadily	vadit	k5eAaImAgFnP	vadit
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	s	k7c7	s
stromem	strom	k1gInSc7	strom
neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgInPc4	žádný
plány	plán	k1gInPc4	plán
–	–	k?	–
strom	strom	k1gInSc4	strom
pak	pak	k6eAd1	pak
dala	dát	k5eAaPmAgFnS	dát
Gilgamešovi	Gilgameš	k1gMnSc3	Gilgameš
celý	celý	k2eAgInSc4d1	celý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
si	se	k3xPyFc3	se
udělal	udělat	k5eAaPmAgMnS	udělat
palici	palice	k1gFnSc4	palice
a	a	k8xC	a
z	z	k7c2	z
pařezu	pařez	k1gInSc2	pařez
kouli	koule	k1gFnSc4	koule
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
buben	buben	k1gInSc4	buben
a	a	k8xC	a
paličku	palička	k1gFnSc4	palička
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
výklady	výklad	k1gInPc4	výklad
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
mohl	moct	k5eAaImAgMnS	moct
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc1d1	přesný
výklad	výklad	k1gInSc1	výklad
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
magické	magický	k2eAgInPc4d1	magický
předměty	předmět	k1gInPc4	předmět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kouli	koule	k1gFnSc4	koule
pak	pak	k6eAd1	pak
odpaloval	odpalovat	k5eAaImAgInS	odpalovat
palicí	palice	k1gFnSc7	palice
<g/>
,	,	kIx,	,
tomuto	tento	k3xDgInSc3	tento
sportu	sport	k1gInSc3	sport
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
mladíky	mladík	k1gInPc7	mladík
z	z	k7c2	z
Uruku	Uruk	k1gInSc2	Uruk
věnoval	věnovat	k5eAaPmAgMnS	věnovat
velice	velice	k6eAd1	velice
náruživě	náruživě	k6eAd1	náruživě
a	a	k8xC	a
místní	místní	k2eAgFnPc1d1	místní
dívky	dívka	k1gFnPc1	dívka
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
jim	on	k3xPp3gFnPc3	on
musely	muset	k5eAaImAgFnP	muset
nosit	nosit	k5eAaImF	nosit
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
chleba	chléb	k1gInSc2	chléb
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
paličkou	palička	k1gFnSc7	palička
a	a	k8xC	a
bubínkem	bubínek	k1gInSc7	bubínek
honil	honit	k5eAaImAgMnS	honit
Urucké	Urucké	k2eAgMnSc4d1	Urucké
muže	muž	k1gMnSc4	muž
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
pak	pak	k6eAd1	pak
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
plnit	plnit	k5eAaImF	plnit
manželské	manželský	k2eAgFnPc4d1	manželská
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
bohům	bůh	k1gMnPc3	bůh
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
bohové	bůh	k1gMnPc1	bůh
slyšeli	slyšet	k5eAaImAgMnP	slyšet
jejich	jejich	k3xOp3gInSc4	jejich
nářek	nářek	k1gInSc4	nářek
<g/>
)	)	kIx)	)
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
předměty	předmět	k1gInPc1	předmět
propadly	propadnout	k5eAaPmAgInP	propadnout
podsvětí	podsvětí	k1gNnSc4	podsvětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Enkidu	Enkid	k1gMnSc3	Enkid
se	se	k3xPyFc4	se
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
předměty	předmět	k1gInPc1	předmět
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
vynese	vynést	k5eAaPmIp3nS	vynést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
pravidla	pravidlo	k1gNnPc4	pravidlo
podsvětí	podsvětí	k1gNnSc1	podsvětí
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dělat	dělat	k5eAaImF	dělat
obráceně	obráceně	k6eAd1	obráceně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musel	muset	k5eAaImAgMnS	muset
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaBmIp2nS	Gilgamat
chtěl	chtít	k5eAaImAgMnS	chtít
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
k	k	k7c3	k
bohům	bůh	k1gMnPc3	bůh
(	(	kIx(	(
<g/>
Enlil	Enlil	k1gMnSc1	Enlil
<g/>
,	,	kIx,	,
Nanu	Nanus	k1gInSc2	Nanus
a	a	k8xC	a
Enki	Enk	k1gFnSc2	Enk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
prosba	prosba	k1gFnSc1	prosba
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
k	k	k7c3	k
Utuovi	Utua	k1gMnSc3	Utua
a	a	k8xC	a
Enkidu	Enkid	k1gMnSc3	Enkid
vyletěl	vyletět	k5eAaPmAgInS	vyletět
komínem	komín	k1gInSc7	komín
ven	ven	k6eAd1	ven
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
(	(	kIx(	(
<g/>
jinde	jinde	k6eAd1	jinde
otevřel	otevřít	k5eAaPmAgMnS	otevřít
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
vylezl	vylézt	k5eAaPmAgMnS	vylézt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
zda	zda	k8xS	zda
vyletěl	vyletět	k5eAaPmAgMnS	vyletět
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
možná	možná	k9	možná
jeho	jeho	k3xOp3gInSc4	jeho
stín	stín	k1gInSc4	stín
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
napořád	napořád	k6eAd1	napořád
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
mýtům	mýtus	k1gInPc3	mýtus
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
dost	dost	k6eAd1	dost
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
či	či	k8xC	či
za	za	k7c7	za
nějakým	nějaký	k3yIgInSc7	nějaký
účelem	účel	k1gInSc7	účel
<g/>
.	.	kIx.	.
</s>
<s>
Enkidu	Enkid	k1gMnSc3	Enkid
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Gilgamešem	Gilgameš	k1gMnSc7	Gilgameš
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
zemřelým	zemřelý	k2eAgInSc7d1	zemřelý
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
zásluh	zásluha	k1gFnPc2	zásluha
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
počet	počet	k1gInSc1	počet
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
též	též	k9	též
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
starají	starat	k5eAaImIp3nP	starat
pozůstalí	pozůstalý	k1gMnPc1	pozůstalý
a	a	k8xC	a
jak	jak	k8xC	jak
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
starali	starat	k5eAaImAgMnP	starat
o	o	k7c4	o
předky	předek	k1gMnPc4	předek
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
koná	konat	k5eAaImIp3nS	konat
smuteční	smuteční	k2eAgInPc4d1	smuteční
obřady	obřad	k1gInPc4	obřad
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
naznačeno	naznačit	k5eAaPmNgNnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
bohyně	bohyně	k1gFnSc1	bohyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
tohoto	tento	k3xDgInSc2	tento
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
ve	v	k7c6	v
XII	XII	kA	XII
<g/>
.	.	kIx.	.
tabulce	tabulka	k1gFnSc3	tabulka
akkadské	akkadský	k2eAgFnSc2d1	akkadská
verze	verze	k1gFnSc2	verze
tohoto	tento	k3xDgInSc2	tento
eposu	epos	k1gInSc2	epos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gilgamešova	Gilgamešův	k2eAgFnSc1d1	Gilgamešův
smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Sumerský	sumerský	k2eAgInSc4d1	sumerský
epos	epos	k1gInSc4	epos
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
305	[number]	k4	305
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
eposu	epos	k1gInSc2	epos
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
několik	několik	k4yIc1	několik
menších	malý	k2eAgFnPc2d2	menší
částí	část	k1gFnPc2	část
v	v	k7c6	v
Ninive	Ninive	k1gNnSc6	Ninive
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
epos	epos	k1gInSc1	epos
líčí	líčit	k5eAaImIp3nS	líčit
poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
Gilgamešova	Gilgamešův	k2eAgInSc2d1	Gilgamešův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaBmIp2nS	Gilgamat
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
těžce	těžce	k6eAd1	těžce
(	(	kIx(	(
<g/>
nevyléčitelně	vyléčitelně	k6eNd1	vyléčitelně
<g/>
)	)	kIx)	)
nemocen	nemocen	k2eAgMnSc1d1	nemocen
a	a	k8xC	a
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
sen	sen	k1gInSc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
snu	sen	k1gInSc6	sen
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
shromáždění	shromáždění	k1gNnPc4	shromáždění
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
probírají	probírat	k5eAaImIp3nP	probírat
jeho	jeho	k3xOp3gNnSc1	jeho
slavné	slavný	k2eAgInPc1d1	slavný
činy	čin	k1gInPc1	čin
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
konstatují	konstatovat	k5eAaBmIp3nP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
získat	získat	k5eAaPmF	získat
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
stane	stanout	k5eAaPmIp3nS	stanout
správcem	správce	k1gMnSc7	správce
podsvětí	podsvětí	k1gNnSc2	podsvětí
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
zemřelé	zemřelá	k1gFnPc4	zemřelá
<g/>
,	,	kIx,	,
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
jejich	jejich	k3xOp3gFnPc4	jejich
pře	pře	k1gFnPc4	pře
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
sen	sen	k1gInSc1	sen
opakoval	opakovat	k5eAaImAgInS	opakovat
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
rádce	rádce	k1gMnSc1	rádce
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemře	zemřít	k5eAaPmIp3nS	zemřít
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
bohů	bůh	k1gMnPc2	bůh
Anunnakú	Anunnakú	k1gFnSc2	Anunnakú
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
stále	stále	k6eAd1	stále
nebyl	být	k5eNaImAgMnS	být
smířen	smířit	k5eAaPmNgMnS	smířit
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
neznámá	známý	k2eNgFnSc1d1	neznámá
část	část	k1gFnSc1	část
textu	text	k1gInSc2	text
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Enkim	Enki	k1gNnSc7	Enki
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
smíří	smířit	k5eAaPmIp3nS	smířit
<g/>
.	.	kIx.	.
</s>
<s>
Svolá	svolat	k5eAaPmIp3nS	svolat
všechny	všechen	k3xTgMnPc4	všechen
urucké	urucký	k2eAgMnPc4d1	urucký
dělníky	dělník	k1gMnPc4	dělník
a	a	k8xC	a
poručí	poručit	k5eAaPmIp3nP	poručit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
odvodněném	odvodněný	k2eAgInSc6d1	odvodněný
Eufratu	Eufrat	k1gInSc6	Eufrat
postavili	postavit	k5eAaPmAgMnP	postavit
hrobku	hrobka	k1gFnSc4	hrobka
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
i	i	k8xC	i
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
doprovodem	doprovod	k1gInSc7	doprovod
(	(	kIx(	(
<g/>
dvořani	dvořan	k1gMnPc1	dvořan
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dary	dar	k1gInPc1	dar
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
dveře	dveře	k1gFnPc1	dveře
zapečetěny	zapečetit	k5eAaPmNgFnP	zapečetit
a	a	k8xC	a
Eufrat	Eufrat	k1gInSc1	Eufrat
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
koryta	koryto	k1gNnSc2	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zcela	zcela	k6eAd1	zcela
zapomenuta	zapomenut	k2eAgFnSc1d1	zapomenuta
<g/>
,	,	kIx,	,
v	v	k7c6	v
akkadské	akkadský	k2eAgFnSc6d1	akkadská
verzi	verze	k1gFnSc6	verze
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
vynechána	vynechat	k5eAaPmNgFnS	vynechat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
verze	verze	k1gFnSc1	verze
==	==	k?	==
</s>
</p>
<p>
<s>
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
verze	verze	k1gFnSc1	verze
eposu	epos	k1gInSc2	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Sumerských	sumerský	k2eAgInPc2d1	sumerský
mýtů	mýtus	k1gInPc2	mýtus
a	a	k8xC	a
eposů	epos	k1gInPc2	epos
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
byly	být	k5eAaImAgFnP	být
vybrány	vybrat	k5eAaPmNgFnP	vybrat
Akkadům	Akkad	k1gInPc3	Akkad
známé	známý	k2eAgFnSc2d1	známá
části	část	k1gFnSc2	část
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
eposu	epos	k1gInSc2	epos
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
poněkud	poněkud	k6eAd1	poněkud
křečovité	křečovitý	k2eAgInPc1d1	křečovitý
a	a	k8xC	a
návaznost	návaznost	k1gFnSc1	návaznost
děje	dít	k5eAaImIp3nS	dít
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
mýty	mýtus	k1gInPc7	mýtus
(	(	kIx(	(
<g/>
kapitolami	kapitola	k1gFnPc7	kapitola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
velmi	velmi	k6eAd1	velmi
chatrná	chatrný	k2eAgFnSc1d1	chatrná
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vytvořit	vytvořit	k5eAaPmF	vytvořit
jednotný	jednotný	k2eAgInSc4d1	jednotný
celek	celek	k1gInSc4	celek
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
předloha	předloha	k1gFnSc1	předloha
byla	být	k5eAaImAgFnS	být
psána	psát	k5eAaImNgFnS	psát
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
různými	různý	k2eAgMnPc7d1	různý
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
tento	tento	k3xDgInSc1	tento
epos	epos	k1gInSc1	epos
prošel	projít	k5eAaPmAgInS	projít
složitým	složitý	k2eAgInSc7d1	složitý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Sín-leke-uníní	Sínekeníní	k2eAgMnSc1d1	Sín-leke-uníní
(	(	kIx(	(
<g/>
babylónský	babylónský	k2eAgMnSc1d1	babylónský
kněz	kněz	k1gMnSc1	kněz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kánonická	kánonický	k2eAgFnSc1d1	kánonický
podoba	podoba	k1gFnSc1	podoba
<g/>
"	"	kIx"	"
vydržela	vydržet	k5eAaPmAgFnS	vydržet
až	až	k9	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
opisů	opis	k1gInPc2	opis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
(	(	kIx(	(
<g/>
asi	asi	k9	asi
250	[number]	k4	250
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dvanácti	dvanáct	k4xCc6	dvanáct
tabulkách	tabulka	k1gFnPc6	tabulka
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zhruba	zhruba	k6eAd1	zhruba
3000	[number]	k4	3000
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
nalezených	nalezený	k2eAgFnPc2d1	nalezená
částí	část	k1gFnPc2	část
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Ninive	Ninive	k1gNnSc2	Ninive
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
krále	král	k1gMnSc2	král
Aššurbanipala	Aššurbanipal	k1gMnSc2	Aššurbanipal
<g/>
,	,	kIx,	,
chybějící	chybějící	k2eAgFnPc1d1	chybějící
části	část	k1gFnPc1	část
byly	být	k5eAaImAgFnP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
z	z	k7c2	z
Chetitských	chetitský	k2eAgFnPc2d1	Chetitská
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
akkadských	akkadský	k2eAgFnPc2d1	akkadská
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akkadštině	akkadština	k1gFnSc6	akkadština
se	se	k3xPyFc4	se
epos	epos	k1gInSc1	epos
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
"	"	kIx"	"
<g/>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
hlubinu	hlubina	k1gFnSc4	hlubina
zřel	zřít	k5eAaImAgMnS	zřít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ša	ša	k?	ša
nagba	nagb	k1gMnSc4	nagb
ímuru	ímura	k1gMnSc4	ímura
–	–	k?	–
dle	dle	k7c2	dle
překladu	překlad	k1gInSc2	překlad
L.	L.	kA	L.
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
I.	I.	kA	I.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
Uruku	Uruk	k1gInSc2	Uruk
tyranizuje	tyranizovat	k5eAaImIp3nS	tyranizovat
své	svůj	k3xOyFgFnPc4	svůj
poddané	poddaná	k1gFnPc4	poddaná
<g/>
,	,	kIx,	,
svými	svůj	k3xOyFgInPc7	svůj
přehnanými	přehnaný	k2eAgInPc7d1	přehnaný
stavitelskými	stavitelský	k2eAgInPc7d1	stavitelský
plány	plán	k1gInPc7	plán
a	a	k8xC	a
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obtěžuje	obtěžovat	k5eAaImIp3nS	obtěžovat
urucké	urucký	k2eAgFnPc4d1	urucká
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
naříkají	naříkat	k5eAaBmIp3nP	naříkat
a	a	k8xC	a
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
si	se	k3xPyFc3	se
k	k	k7c3	k
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stvoří	stvořit	k5eAaPmIp3nP	stvořit
divokého	divoký	k2eAgMnSc4d1	divoký
člověka	člověk	k1gMnSc4	člověk
Enkidua	Enkiduus	k1gMnSc4	Enkiduus
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
zbavit	zbavit	k5eAaPmF	zbavit
Urucké	Urucký	k2eAgNnSc4d1	Urucký
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
útlaku	útlak	k1gInSc2	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Enkidua	Enkidua	k1gFnSc1	Enkidua
stvoří	stvořit	k5eAaPmIp3nS	stvořit
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
bohyně	bohyně	k1gFnSc2	bohyně
Aruru	Arur	k1gInSc2	Arur
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tabulka	tabulka	k1gFnSc1	tabulka
končí	končit	k5eAaImIp3nS	končit
Gilgamešovými	Gilgamešův	k2eAgInPc7d1	Gilgamešův
sny	sen	k1gInPc7	sen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
Gilgamešovými	Gilgamešův	k2eAgMnPc7d1	Gilgamešův
rádci	rádce	k1gMnPc7	rádce
vyloženy	vyložit	k5eAaPmNgFnP	vyložit
jako	jako	k8xC	jako
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
přijde	přijít	k5eAaPmIp3nS	přijít
silný	silný	k2eAgMnSc1d1	silný
partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
II	II	kA	II
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Enkidu	Enkida	k1gFnSc4	Enkida
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
a	a	k8xC	a
horách	hora	k1gFnPc6	hora
s	s	k7c7	s
divokou	divoký	k2eAgFnSc7d1	divoká
zvěří	zvěř	k1gFnSc7	zvěř
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
lovcům	lovec	k1gMnPc3	lovec
pasti	past	k1gFnSc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Lovci	lovec	k1gMnPc1	lovec
si	se	k3xPyFc3	se
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
Gilgamešovi	Gilgamešův	k2eAgMnPc1d1	Gilgamešův
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
lovci	lovec	k1gMnPc1	lovec
poradí	poradit	k5eAaPmIp3nP	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
napajedlu	napajedlo	k1gNnSc3	napajedlo
přivedli	přivést	k5eAaPmAgMnP	přivést
krásnou	krásný	k2eAgFnSc4d1	krásná
nevěstku	nevěstka	k1gFnSc4	nevěstka
Šamchat	Šamchat	k1gFnSc2	Šamchat
(	(	kIx(	(
<g/>
akkadsky	akkadsky	k6eAd1	akkadsky
smyslná	smyslný	k2eAgFnSc1d1	smyslná
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rostlá	rostlý	k2eAgFnSc1d1	rostlá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Enkidua	Enkidua	k1gFnSc1	Enkidua
svede	svést	k5eAaPmIp3nS	svést
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
zcivilizuje	zcivilizovat	k5eAaPmIp3nS	zcivilizovat
(	(	kIx(	(
<g/>
milují	milovat	k5eAaImIp3nP	milovat
se	se	k3xPyFc4	se
šest	šest	k4xCc1	šest
dní	den	k1gInPc2	den
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
nocí	noc	k1gFnPc2	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uruku	Uruk	k1gInSc6	Uruk
se	se	k3xPyFc4	se
Enkidu	Enkid	k1gInSc2	Enkid
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Gilgamešem	Gilgameš	k1gMnSc7	Gilgameš
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
utká	utkat	k5eAaPmIp3nS	utkat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
vyrovnané	vyrovnaný	k2eAgFnPc1d1	vyrovnaná
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
Uruku	Uruk	k1gInSc6	Uruk
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
verzí	verze	k1gFnPc2	verze
Enkidu	Enkid	k1gInSc3	Enkid
Gilgameše	Gilgameše	k1gFnSc4	Gilgameše
potkal	potkat	k5eAaPmAgMnS	potkat
<g/>
,	,	kIx,	,
když	když	k8xS	když
šel	jít	k5eAaImAgMnS	jít
za	za	k7c7	za
milenkou	milenka	k1gFnSc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
verze	verze	k1gFnSc2	verze
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
u	u	k7c2	u
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
když	když	k8xS	když
Enkidu	Enkida	k1gFnSc4	Enkida
přicházel	přicházet	k5eAaImAgInS	přicházet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
podniknou	podniknout	k5eAaPmIp3nP	podniknout
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
Chumbabovi	Chumbaba	k1gMnSc3	Chumbaba
(	(	kIx(	(
<g/>
Chuvava	Chuvava	k1gFnSc1	Chuvava
<g/>
,	,	kIx,	,
Chuwawa	Chuwawa	k1gFnSc1	Chuwawa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rádci	rádce	k1gMnPc1	rádce
je	on	k3xPp3gInPc4	on
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
nápadu	nápad	k1gInSc2	nápad
zrazují	zrazovat	k5eAaImIp3nP	zrazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
III	III	kA	III
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Gilgamešovi	Gilgamešův	k2eAgMnPc1d1	Gilgamešův
rádci	rádce	k1gMnPc1	rádce
smíří	smířit	k5eAaPmIp3nP	smířit
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
jejich	jejich	k3xOp3gFnSc2	jejich
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
mu	on	k3xPp3gMnSc3	on
rady	rada	k1gFnPc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
radí	radit	k5eAaImIp3nS	radit
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
–	–	k?	–
bohyní	bohyně	k1gFnSc7	bohyně
Ninsunou	Ninsuna	k1gFnSc7	Ninsuna
(	(	kIx(	(
<g/>
Ninsumunnou	Ninsumunný	k2eAgFnSc4d1	Ninsumunný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
obětuje	obětovat	k5eAaBmIp3nS	obětovat
bohovi	bůh	k1gMnSc3	bůh
Šamašovi	Šamaš	k1gMnSc3	Šamaš
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
jim	on	k3xPp3gMnPc3	on
cenné	cenný	k2eAgFnPc4d1	cenná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
IV	IV	kA	IV
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
tabulka	tabulka	k1gFnSc1	tabulka
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
této	tento	k3xDgFnSc2	tento
tabulky	tabulka	k1gFnSc2	tabulka
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
nečitelné	čitelný	k2eNgFnPc1d1	nečitelná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
s	s	k7c7	s
Enkiduem	Enkidu	k1gInSc7	Enkidu
sami	sám	k3xTgMnPc1	sám
vydají	vydat	k5eAaPmIp3nP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
(	(	kIx(	(
<g/>
poloha	poloha	k1gFnSc1	poloha
cedrového	cedrový	k2eAgInSc2d1	cedrový
lesa	les	k1gInSc2	les
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnozí	mnohý	k2eAgMnPc1d1	mnohý
badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ležel	ležet	k5eAaImAgMnS	ležet
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
poškození	poškození	k1gNnSc3	poškození
neví	vědět	k5eNaImIp3nS	vědět
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
konci	konec	k1gInSc6	konec
tabulky	tabulka	k1gFnSc2	tabulka
má	mít	k5eAaImIp3nS	mít
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
sny	sen	k1gInPc4	sen
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
tři	tři	k4xCgInPc4	tři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Enkidu	Enkida	k1gFnSc4	Enkida
vyloží	vyložit	k5eAaPmIp3nP	vyložit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
výprava	výprava	k1gFnSc1	výprava
bude	být	k5eAaImBp3nS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V.	V.	kA	V.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
výprava	výprava	k1gFnSc1	výprava
dorazí	dorazit	k5eAaPmIp3nS	dorazit
až	až	k9	až
do	do	k7c2	do
cedrového	cedrový	k2eAgInSc2d1	cedrový
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začnou	začít	k5eAaPmIp3nP	začít
kácet	kácet	k5eAaImF	kácet
vybraný	vybraný	k2eAgInSc4d1	vybraný
cedr	cedr	k1gInSc4	cedr
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velice	velice	k6eAd1	velice
pečlivě	pečlivě	k6eAd1	pečlivě
popsán	popsán	k2eAgInSc1d1	popsán
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Chumbaba	Chumbaba	k1gMnSc1	Chumbaba
nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
posmíval	posmívat	k5eAaImAgMnS	posmívat
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
chytil	chytit	k5eAaPmAgMnS	chytit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
je	být	k5eAaImIp3nS	být
pustit	pustit	k5eAaPmF	pustit
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
do	do	k7c2	do
věci	věc	k1gFnSc2	věc
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Gilgamešovy	Gilgamešův	k2eAgFnSc2d1	Gilgamešův
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
,	,	kIx,	,
vložili	vložit	k5eAaPmAgMnP	vložit
bohové	bůh	k1gMnPc1	bůh
–	–	k?	–
především	především	k9	především
Šamaš	Šamaš	k1gInSc4	Šamaš
a	a	k8xC	a
seslali	seslat	k5eAaPmAgMnP	seslat
na	na	k7c4	na
Chumbabu	Chumbaba	k1gFnSc4	Chumbaba
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
zabránil	zabránit	k5eAaPmAgInS	zabránit
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
zachytl	zachytnout	k5eAaPmAgMnS	zachytnout
padajícího	padající	k2eAgMnSc4d1	padající
Gilgameše	Gilgameše	k1gMnSc4	Gilgameše
a	a	k8xC	a
Enkidua	Enkiduus	k1gMnSc4	Enkiduus
<g/>
.	.	kIx.	.
</s>
<s>
Chumbaba	Chumbaba	k1gFnSc1	Chumbaba
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
prosí	prosit	k5eAaImIp3nS	prosit
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
slitovat	slitovat	k5eAaPmF	slitovat
<g/>
,	,	kIx,	,
Enkidu	Enkida	k1gFnSc4	Enkida
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
rozmluví	rozmluvit	k5eAaPmIp3nS	rozmluvit
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mu	on	k3xPp3gNnSc3	on
useknou	useknout	k5eAaPmIp3nP	useknout
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c6	na
voru	vor	k1gInSc6	vor
dopraví	dopravit	k5eAaPmIp3nS	dopravit
do	do	k7c2	do
Uruku	Uruk	k1gInSc2	Uruk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
VI	VI	kA	VI
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
do	do	k7c2	do
Gilgameše	Gilgameše	k1gFnSc2	Gilgameše
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
Ištar	Ištar	k1gInSc1	Ištar
(	(	kIx(	(
<g/>
Inanna	Inanna	k1gFnSc1	Inanna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
poměrně	poměrně	k6eAd1	poměrně
neuctivým	uctivý	k2eNgInSc7d1	neuctivý
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
milence	milenec	k1gMnPc4	milenec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Ištar	Ištar	k1gMnSc1	Ištar
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
jich	on	k3xPp3gInPc2	on
nabažila	nabažit	k5eAaPmAgFnS	nabažit
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odehnala	odehnat	k5eAaPmAgFnS	odehnat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Ištar	Ištar	k1gInSc4	Ištar
urazilo	urazit	k5eAaPmAgNnS	urazit
a	a	k8xC	a
obrátila	obrátit	k5eAaPmAgFnS	obrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
Ana	Ana	k1gMnSc4	Ana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
dal	dát	k5eAaPmAgMnS	dát
strašného	strašný	k2eAgMnSc4d1	strašný
nebeského	nebeský	k2eAgMnSc4d1	nebeský
býka	býk	k1gMnSc4	býk
(	(	kIx(	(
<g/>
Alú	Alú	k1gMnSc1	Alú
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
An	An	k?	An
váhá	váhat	k5eAaImIp3nS	váhat
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
býka	býk	k1gMnSc2	býk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Ištar	Ištar	k1gMnSc1	Ištar
začne	začít	k5eAaPmIp3nS	začít
vyhrožovat	vyhrožovat	k5eAaImF	vyhrožovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypustí	vypustit	k5eAaPmIp3nS	vypustit
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
a	a	k8xC	a
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajistila	zajistit	k5eAaPmAgFnS	zajistit
dost	dost	k6eAd1	dost
potravy	potrava	k1gFnSc2	potrava
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
netrpěli	trpět	k5eNaImAgMnP	trpět
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
nebeský	nebeský	k2eAgMnSc1d1	nebeský
býk	býk	k1gMnSc1	býk
zničí	zničit	k5eAaPmIp3nS	zničit
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
ji	on	k3xPp3gFnSc4	on
An	An	k1gFnSc4	An
býka	býk	k1gMnSc2	býk
<g/>
.	.	kIx.	.
</s>
<s>
Býk	býk	k1gMnSc1	býk
začal	začít	k5eAaPmAgMnS	začít
pustošit	pustošit	k5eAaImF	pustošit
okolí	okolí	k1gNnSc4	okolí
Uruku	Uruk	k1gInSc2	Uruk
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
a	a	k8xC	a
Enkidu	Enkida	k1gFnSc4	Enkida
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
svedli	svést	k5eAaPmAgMnP	svést
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
ho	on	k3xPp3gMnSc4	on
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
vytrhli	vytrhnout	k5eAaPmAgMnP	vytrhnout
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
ho	on	k3xPp3gNnSc4	on
Šamašovi	Šamašův	k2eAgMnPc1d1	Šamašův
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
uviděla	uvidět	k5eAaPmAgFnS	uvidět
Ištar	Ištar	k1gInSc4	Ištar
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
Gilgamešovi	Gilgameš	k1gMnSc3	Gilgameš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
její	její	k3xOp3gFnSc2	její
pomsty	pomsta	k1gFnSc2	pomsta
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
zaslechl	zaslechnout	k5eAaPmAgInS	zaslechnout
Enkidu	Enkida	k1gFnSc4	Enkida
<g/>
,	,	kIx,	,
hodil	hodit	k5eAaImAgMnS	hodit
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
kýtu	kýta	k1gFnSc4	kýta
ze	z	k7c2	z
zabitého	zabitý	k2eAgMnSc2d1	zabitý
býka	býk	k1gMnSc2	býk
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
přirození	přirození	k1gNnSc1	přirození
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ištar	Ištar	k1gInSc4	Ištar
zmizela	zmizet	k5eAaPmAgFnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
to	ten	k3xDgNnSc4	ten
oslavili	oslavit	k5eAaPmAgMnP	oslavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
VII	VII	kA	VII
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
tabulka	tabulka	k1gFnSc1	tabulka
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
doplněn	doplnit	k5eAaPmNgInS	doplnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
chetitských	chetitský	k2eAgInPc2d1	chetitský
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ištar	Ištara	k1gFnPc2	Ištara
se	se	k3xPyFc4	se
odebrala	odebrat	k5eAaPmAgFnS	odebrat
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
<g/>
,	,	kIx,	,
bohové	bůh	k1gMnPc1	bůh
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
činy	čin	k1gInPc4	čin
(	(	kIx(	(
<g/>
zabití	zabití	k1gNnSc1	zabití
Chumbaby	Chumbaba	k1gFnSc2	Chumbaba
a	a	k8xC	a
hození	hození	k1gNnSc2	hození
kýty	kýta	k1gFnSc2	kýta
–	–	k?	–
přirození	přirození	k1gNnSc1	přirození
–	–	k?	–
po	po	k7c4	po
Ištar	Ištar	k1gInSc4	Ištar
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
volba	volba	k1gFnSc1	volba
padne	padnout	k5eAaPmIp3nS	padnout
na	na	k7c4	na
Enkidua	Enkiduus	k1gMnSc4	Enkiduus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ihned	ihned	k6eAd1	ihned
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
sdělí	sdělit	k5eAaPmIp3nP	sdělit
Gilgamešovi	Gilgamešův	k2eAgMnPc1d1	Gilgamešův
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
verzí	verze	k1gFnPc2	verze
se	se	k3xPyFc4	se
sen	sen	k1gInSc1	sen
zdá	zdát	k5eAaPmIp3nS	zdát
Enkidovi	Enkid	k1gMnSc6	Enkid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Enkidu	Enkid	k1gMnSc3	Enkid
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
různě	různě	k6eAd1	různě
zlobí	zlobit	k5eAaImIp3nS	zlobit
především	především	k9	především
na	na	k7c6	na
Šamchat	Šamchat	k1gFnSc6	Šamchat
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
Šamchat	Šamchat	k1gMnPc1	Šamchat
odpustí	odpustit	k5eAaPmIp3nP	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdá	zdát	k5eAaPmIp3nS	zdát
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
půjde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
nejprve	nejprve	k6eAd1	nejprve
Gilgameš	Gilgameš	k1gMnPc1	Gilgameš
u	u	k7c2	u
nemocného	nemocný	k1gMnSc2	nemocný
Enkidua	Enkiduus	k1gMnSc2	Enkiduus
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
všechno	všechen	k3xTgNnSc1	všechen
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
plakat	plakat	k5eAaImF	plakat
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Enkidu	Enkid	k1gMnSc3	Enkid
naposled	naposled	k6eAd1	naposled
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
,	,	kIx,	,
lituje	litovat	k5eAaImIp3nS	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
zemřít	zemřít	k5eAaPmF	zemřít
smrtí	smrt	k1gFnSc7	smrt
válečníka	válečník	k1gMnSc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zase	zase	k9	zase
usne	usnout	k5eAaPmIp3nS	usnout
<g/>
,	,	kIx,	,
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Enkidu	Enkid	k1gMnSc3	Enkid
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
poté	poté	k6eAd1	poté
vykřikne	vykřiknout	k5eAaPmIp3nS	vykřiknout
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
výkřik	výkřik	k1gInSc1	výkřik
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
tabulky	tabulka	k1gFnSc2	tabulka
je	být	k5eAaImIp3nS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
popisovány	popisován	k2eAgInPc1d1	popisován
smuteční	smuteční	k2eAgInPc1d1	smuteční
obřady	obřad	k1gInPc1	obřad
a	a	k8xC	a
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sumerské	sumerský	k2eAgFnSc2d1	sumerská
verze	verze	k1gFnSc2	verze
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
přítele	přítel	k1gMnSc4	přítel
oplakával	oplakávat	k5eAaImAgMnS	oplakávat
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
nocí	noc	k1gFnPc2	noc
<g/>
,	,	kIx,	,
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nemyl	mýt	k5eNaImAgMnS	mýt
<g/>
,	,	kIx,	,
nejedl	jíst	k5eNaImAgMnS	jíst
a	a	k8xC	a
nepil	pít	k5eNaImAgMnS	pít
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
akkadské	akkadský	k2eAgFnSc6d1	akkadská
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
IX	IX	kA	IX
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
dostane	dostat	k5eAaPmIp3nS	dostat
strach	strach	k1gInSc1	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
marné	marný	k2eAgNnSc1d1	marné
<g/>
.	.	kIx.	.
</s>
<s>
Opustí	opustit	k5eAaPmIp3nS	opustit
Uruk	Uruk	k1gInSc4	Uruk
a	a	k8xC	a
bloudí	bloudit	k5eAaImIp3nP	bloudit
pološílený	pološílený	k2eAgInSc4d1	pološílený
děsem	děs	k1gInSc7	děs
ze	z	k7c2	z
smrtelného	smrtelný	k2eAgInSc2d1	smrtelný
konce	konec	k1gInSc2	konec
krajinou	krajina	k1gFnSc7	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
navrací	navracet	k5eAaBmIp3nS	navracet
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
přišel	přijít	k5eAaPmAgInS	přijít
Enkidu	Enkida	k1gFnSc4	Enkida
–	–	k?	–
jaký	jaký	k9	jaký
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
k	k	k7c3	k
Utnapištimu	Utnapištim	k1gInSc3	Utnapištim
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Utanapištim	Utanapištim	k1gMnSc1	Utanapištim
<g/>
,	,	kIx,	,
Uta-napištim	Utaapištim	k1gMnSc1	Uta-napištim
<g/>
)	)	kIx)	)
který	který	k3yIgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
potopu	potopa	k1gFnSc4	potopa
světa	svět	k1gInSc2	svět
a	a	k8xC	a
zná	znát	k5eAaImIp3nS	znát
tajemství	tajemství	k1gNnSc4	tajemství
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něho	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
Mašu	Mašus	k1gInSc2	Mašus
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
vede	vést	k5eAaImIp3nS	vést
velmi	velmi	k6eAd1	velmi
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Bránu	brána	k1gFnSc4	brána
u	u	k7c2	u
hory	hora	k1gFnSc2	hora
Mašu	Mašus	k1gInSc2	Mašus
střeží	střežit	k5eAaImIp3nS	střežit
Girtablilu	Girtablila	k1gFnSc4	Girtablila
(	(	kIx(	(
<g/>
lidé-štíři	lidé-štíř	k1gMnPc1	lidé-štíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
Gilgamešovi	Gilgameš	k1gMnSc3	Gilgameš
chovají	chovat	k5eAaImIp3nP	chovat
zpočátku	zpočátku	k6eAd1	zpočátku
nepřátelsky	přátelsky	k6eNd1	přátelsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
je	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
pustili	pustit	k5eAaPmAgMnP	pustit
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k9	ještě
řeknou	říct	k5eAaPmIp3nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
dvanáct	dvanáct	k4xCc4	dvanáct
dvouhodin	dvouhodina	k1gFnPc2	dvouhodina
temnotou	temnota	k1gFnSc7	temnota
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dramaticky	dramaticky	k6eAd1	dramaticky
popisována	popisován	k2eAgFnSc1d1	popisována
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
jde	jít	k5eAaImIp3nS	jít
stanoveným	stanovený	k2eAgInSc7d1	stanovený
časem	čas	k1gInSc7	čas
močálem	močál	k1gInSc7	močál
beznaděje	beznaděj	k1gFnSc2	beznaděj
až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
popis	popis	k1gInSc1	popis
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
popisu	popis	k1gInSc2	popis
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházely	nacházet	k5eAaImAgInP	nacházet
stromy	strom	k1gInPc1	strom
z	z	k7c2	z
(	(	kIx(	(
<g/>
polo	polo	k6eAd1	polo
<g/>
)	)	kIx)	)
<g/>
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rodily	rodit	k5eAaImAgFnP	rodit
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Středozemní	středozemní	k2eAgInSc1d1	středozemní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
X.	X.	kA	X.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Gilgameš	Gilgameš	k1gMnPc1	Gilgameš
seznámí	seznámit	k5eAaPmIp3nP	seznámit
se	s	k7c7	s
Siduri	Sidur	k1gInPc7	Sidur
(	(	kIx(	(
<g/>
šenkýřka	šenkýřka	k1gFnSc1	šenkýřka
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zahradě	zahrada	k1gFnSc6	zahrada
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
Gilgameše	Gilgameše	k1gFnSc1	Gilgameše
zpočátku	zpočátku	k6eAd1	zpočátku
nepozná	poznat	k5eNaPmIp3nS	poznat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
promluví	promluvit	k5eAaPmIp3nS	promluvit
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
co	co	k8xS	co
je	být	k5eAaImIp3nS	být
zač	zač	k6eAd1	zač
<g/>
.	.	kIx.	.
</s>
<s>
Ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
Gilgameše	Gilgameše	k1gFnSc7	Gilgameše
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
vypadá	vypadat	k5eAaPmIp3nS	vypadat
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
Enkidua	Enkidu	k1gInSc2	Enkidu
nemyl	mýt	k5eNaImAgInS	mýt
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
oblečen	obléct	k5eAaPmNgMnS	obléct
do	do	k7c2	do
lví	lví	k2eAgFnSc2d1	lví
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
–	–	k?	–
on	on	k3xPp3gMnSc1	on
jí	on	k3xPp3gFnSc3	on
vylíčí	vylíčit	k5eAaPmIp3nS	vylíčit
svá	svůj	k3xOyFgNnPc4	svůj
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
(	(	kIx(	(
<g/>
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
Enkidua	Enkiduum	k1gNnSc2	Enkiduum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
)	)	kIx)	)
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
Utnapištimu	Utnapištimo	k1gNnSc3	Utnapištimo
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
snaží	snažit	k5eAaImIp3nS	snažit
rozmluvit	rozmluvit	k5eAaPmF	rozmluvit
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
přívoz	přívoz	k1gInSc1	přívoz
a	a	k8xC	a
že	že	k8xS	že
co	co	k9	co
ona	onen	k3xDgFnSc1	onen
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
moře	moře	k1gNnSc4	moře
překročil	překročit	k5eAaPmAgInS	překročit
pouze	pouze	k6eAd1	pouze
Šamaš	Šamaš	k1gInSc1	Šamaš
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
Uršanabiho	Uršanabi	k1gMnSc4	Uršanabi
(	(	kIx(	(
<g/>
převozník	převozník	k1gMnSc1	převozník
přes	přes	k7c4	přes
vody	voda	k1gFnPc4	voda
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaBmIp2nS	Gilgamat
ho	on	k3xPp3gInSc4	on
najde	najít	k5eAaPmIp3nS	najít
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
Uršunabim	Uršunabi	k1gNnSc7	Uršunabi
poperou	poprat	k5eAaPmIp3nP	poprat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
převeze	převézt	k5eAaPmIp3nS	převézt
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaBmIp2nS	Gilgamat
musí	muset	k5eAaImIp3nS	muset
nařezat	nařezat	k5eAaPmF	nařezat
300	[number]	k4	300
třicetimetrových	třicetimetrový	k2eAgFnPc2d1	třicetimetrová
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
učiní	učinit	k5eAaPmIp3nS	učinit
<g/>
,	,	kIx,	,
vyplují	vyplout	k5eAaPmIp3nP	vyplout
a	a	k8xC	a
odstrkují	odstrkovat	k5eAaImIp3nP	odstrkovat
se	se	k3xPyFc4	se
tyčemi	tyč	k1gFnPc7	tyč
–	–	k?	–
pokaždé	pokaždé	k6eAd1	pokaždé
jinou	jiný	k2eAgFnSc7d1	jiná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
voda	voda	k1gFnSc1	voda
nepokapala	pokapat	k5eNaPmAgFnS	pokapat
ruce	ruka	k1gFnPc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
tabulky	tabulka	k1gFnSc2	tabulka
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
zachovalé	zachovalý	k2eAgFnSc2d1	zachovalá
části	část	k1gFnSc2	část
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
Utnapištim	Utnapištim	k1gMnSc1	Utnapištim
zpozoruje	zpozorovat	k5eAaPmIp3nS	zpozorovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
opakuje	opakovat	k5eAaImIp3nS	opakovat
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
dotazování	dotazování	k1gNnSc2	dotazování
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
takto	takto	k6eAd1	takto
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Utanapištim	Utanapištim	k1gInSc1	Utanapištim
Gilgamešovi	Gilgameš	k1gMnSc3	Gilgameš
vysvětlí	vysvětlit	k5eAaPmIp3nP	vysvětlit
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
XI	XI	kA	XI
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tabulka	tabulka	k1gFnSc1	tabulka
o	o	k7c6	o
potopě	potopa	k1gFnSc6	potopa
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Gilgameš	Gilgamat	k5eAaBmIp2nS	Gilgamat
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
zpočátku	zpočátku	k6eAd1	zpočátku
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
tolik	tolik	k6eAd1	tolik
podobní	podobný	k2eAgMnPc1d1	podobný
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
ne	ne	k9	ne
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
bozi	bůh	k1gMnPc1	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uta-napištim	Utaapištim	k6eAd1	Uta-napištim
mu	on	k3xPp3gMnSc3	on
odvypráví	odvyprávit	k5eAaPmIp3nS	odvyprávit
mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
potopě	potopa	k1gFnSc6	potopa
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
příběh	příběh	k1gInSc4	příběh
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
akkadských	akkadský	k2eAgInPc6d1	akkadský
a	a	k8xC	a
sumerských	sumerský	k2eAgInPc6d1	sumerský
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zařazen	zařadit	k5eAaPmNgInS	zařadit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
text	text	k1gInSc1	text
lépe	dobře	k6eAd2	dobře
vypravěčům	vypravěč	k1gMnPc3	vypravěč
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
bohové	bůh	k1gMnPc1	bůh
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
seslat	seslat	k5eAaPmF	seslat
na	na	k7c4	na
lidstvo	lidstvo	k1gNnSc4	lidstvo
potopu	potopa	k1gFnSc4	potopa
Ea	Ea	k1gMnPc2	Ea
to	ten	k3xDgNnSc1	ten
prozradí	prozradit	k5eAaPmIp3nS	prozradit
Uta-napištimu	Utaapištima	k1gFnSc4	Uta-napištima
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
<g />
.	.	kIx.	.
</s>
<s>
mu	on	k3xPp3gNnSc3	on
poselství	poselství	k1gNnSc3	poselství
po	po	k7c6	po
"	"	kIx"	"
<g/>
rákosové	rákosový	k2eAgFnSc6d1	rákosová
chýši	chýš	k1gFnSc6	chýš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
sestaví	sestavit	k5eAaPmIp3nS	sestavit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
přesného	přesný	k2eAgInSc2d1	přesný
návodu	návod	k1gInSc2	návod
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
vezme	vzít	k5eAaPmIp3nS	vzít
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
příbuzné	příbuzná	k1gFnPc4	příbuzná
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
zlato	zlato	k1gNnSc4	zlato
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
šest	šest	k4xCc1	šest
dní	den	k1gInPc2	den
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
nocí	noc	k1gFnPc2	noc
prší	pršet	k5eAaImIp3nS	pršet
<g/>
,	,	kIx,	,
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
obrovský	obrovský	k2eAgInSc1d1	obrovský
močál	močál	k1gInSc1	močál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
dešťů	dešť	k1gInPc2	dešť
Uta-napištim	Utaapištim	k1gInSc1	Uta-napištim
vypustí	vypustit	k5eAaPmIp3nS	vypustit
holubici	holubice	k1gFnSc4	holubice
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vlaštovku	vlaštovka	k1gFnSc4	vlaštovka
–	–	k?	–
ty	ten	k3xDgMnPc4	ten
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
to	ten	k3xDgNnSc1	ten
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
s	s	k7c7	s
havranem	havran	k1gMnSc7	havran
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
již	již	k6eAd1	již
opadla	opadnout	k5eAaPmAgFnS	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
Enlil	Enlil	k1gInSc1	Enlil
poté	poté	k6eAd1	poté
udělá	udělat	k5eAaPmIp3nS	udělat
Uta-napištima	Utaapištim	k1gMnSc4	Uta-napištim
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
nesmrtelnými	smrtelný	k2eNgFnPc7d1	nesmrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Uta-napištim	Utaapištim	k6eAd1	Uta-napištim
říká	říkat	k5eAaImIp3nS	říkat
Gilgamešovi	Gilgameš	k1gMnSc3	Gilgameš
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
podstoupit	podstoupit	k5eAaPmF	podstoupit
zkoušku	zkouška	k1gFnSc4	zkouška
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
tj.	tj.	kA	tj.
nespat	spát	k5eNaImF	spát
šest	šest	k4xCc1	šest
dní	den	k1gInPc2	den
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zkoušce	zkouška	k1gFnSc6	zkouška
neobstojí	obstát	k5eNaPmIp3nS	obstát
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
usne	usnout	k5eAaPmIp3nS	usnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spal	spát	k5eAaImAgMnS	spát
sedm	sedm	k4xCc4	sedm
nocí	noc	k1gFnPc2	noc
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
až	až	k9	až
podle	podle	k7c2	podle
chlebů	chléb	k1gInPc2	chléb
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
každé	každý	k3xTgNnSc4	každý
ráno	ráno	k1gNnSc4	ráno
dávali	dávat	k5eAaImAgMnP	dávat
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zanechat	zanechat	k5eAaPmF	zanechat
svého	svůj	k3xOyFgInSc2	svůj
honu	hon	k1gInSc2	hon
na	na	k7c4	na
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Uta-napištimu	Utaapištima	k1gFnSc4	Uta-napištima
mu	on	k3xPp3gMnSc3	on
prozradí	prozradit	k5eAaPmIp3nS	prozradit
tajemství	tajemství	k1gNnSc4	tajemství
bohů	bůh	k1gMnPc2	bůh
<g/>
:	:	kIx,	:
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
moře	moře	k1gNnSc2	moře
roste	růst	k5eAaImIp3nS	růst
rostlina	rostlina	k1gFnSc1	rostlina
omlazení	omlazení	k1gNnPc2	omlazení
<g/>
,	,	kIx,	,
Gilgameš	Gilgameš	k1gFnPc2	Gilgameš
ji	on	k3xPp3gFnSc4	on
získá	získat	k5eAaPmIp3nS	získat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
sežere	sežrat	k5eAaPmIp3nS	sežrat
had	had	k1gMnSc1	had
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cíl	cíl	k1gInSc1	cíl
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
a	a	k8xC	a
chlubí	chlubit	k5eAaImIp3nP	chlubit
se	se	k3xPyFc4	se
Uršanabimu	Uršanabim	k1gInSc2	Uršanabim
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
<g/>
,	,	kIx,	,
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
kolem	kolem	k6eAd1	kolem
Uruku	Uruka	k1gFnSc4	Uruka
vystavět	vystavět	k5eAaPmF	vystavět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
XII	XII	kA	XII
<g/>
.	.	kIx.	.
tabulka	tabulka	k1gFnSc1	tabulka
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
tabulka	tabulka	k1gFnSc1	tabulka
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
eposu	epos	k1gInSc2	epos
přidána	přidat	k5eAaPmNgFnS	přidat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sumerských	sumerský	k2eAgFnPc2d1	sumerská
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
do	do	k7c2	do
eposu	epos	k1gInSc2	epos
nezapadá	zapadat	k5eNaImIp3nS	zapadat
a	a	k8xC	a
i	i	k9	i
její	její	k3xOp3gNnSc4	její
umístění	umístění	k1gNnSc4	umístění
je	být	k5eAaImIp3nS	být
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tabulce	tabulka	k1gFnSc6	tabulka
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
živý	živý	k1gMnSc1	živý
Enkidu	Enkid	k1gInSc2	Enkid
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
VII	VII	kA	VII
<g/>
.	.	kIx.	.
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
výrazných	výrazný	k2eAgNnPc2d1	výrazné
narušení	narušení	k1gNnPc2	narušení
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
autora	autor	k1gMnSc4	autor
této	tento	k3xDgFnSc2	tento
tabulky	tabulka	k1gFnSc2	tabulka
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
písař	písař	k1gMnSc1	písař
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
)	)	kIx)	)
Nabú-zukup-kénu	Nabúukupéna	k1gFnSc4	Nabú-zukup-kéna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
písař	písař	k1gMnSc1	písař
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
Sargona	Sargon	k1gMnSc2	Sargon
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
Gilgameš	Gilgameš	k1gFnSc1	Gilgameš
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
dva	dva	k4xCgInPc1	dva
magické	magický	k2eAgInPc1d1	magický
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
pukku	pukku	k6eAd1	pukku
a	a	k8xC	a
mekku	mekku	k6eAd1	mekku
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
pro	pro	k7c4	pro
stížnosti	stížnost	k1gFnPc4	stížnost
uruckých	urucký	k2eAgFnPc2d1	urucká
žen	žena	k1gFnPc2	žena
spadnou	spadnout	k5eAaPmIp3nP	spadnout
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
Enkidu	Enkida	k1gFnSc4	Enkida
pokusí	pokusit	k5eAaPmIp3nS	pokusit
vynést	vynést	k5eAaPmF	vynést
<g/>
,	,	kIx,	,
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
proto	proto	k8xC	proto
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neřídí	řídit	k5eNaImIp3nS	řídit
se	se	k3xPyFc4	se
Gilgamešovými	Gilgamešův	k2eAgInPc7d1	Gilgamešův
pokyny	pokyn	k1gInPc7	pokyn
<g/>
,	,	kIx,	,
poruší	porušit	k5eAaPmIp3nP	porušit
zákony	zákon	k1gInPc1	zákon
podsvětí	podsvětí	k1gNnSc1	podsvětí
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
dělat	dělat	k5eAaImF	dělat
vše	všechen	k3xTgNnSc1	všechen
obráceně	obráceně	k6eAd1	obráceně
<g/>
)	)	kIx)	)
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
poté	poté	k6eAd1	poté
prosí	prosit	k5eAaImIp3nS	prosit
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Enkidua	Enkidua	k1gFnSc1	Enkidua
propustili	propustit	k5eAaPmAgMnP	propustit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
tam	tam	k6eAd1	tam
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
musel	muset	k5eAaImAgMnS	muset
někdo	někdo	k3yInSc1	někdo
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Akkadsko-sumerské	Akkadskoumerský	k2eAgFnSc6d1	Akkadsko-sumerský
mytologii	mytologie	k1gFnSc6	mytologie
poměrně	poměrně	k6eAd1	poměrně
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Gilgamešem	Gilgameš	k1gMnSc7	Gilgameš
a	a	k8xC	a
líčí	líčit	k5eAaImIp3nS	líčit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bídně	bídně	k6eAd1	bídně
se	se	k3xPyFc4	se
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
známé	známý	k2eAgFnPc1d1	známá
verze	verze	k1gFnPc1	verze
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
verzí	verze	k1gFnPc2	verze
se	se	k3xPyFc4	se
našla	najít	k5eAaPmAgFnS	najít
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
stejných	stejný	k2eAgFnPc2d1	stejná
nebo	nebo	k8xC	nebo
podobných	podobný	k2eAgFnPc2d1	podobná
verzí	verze	k1gFnPc2	verze
v	v	k7c6	v
několika	několik	k4yIc6	několik
sumerských	sumerský	k2eAgFnPc6d1	sumerská
a	a	k8xC	a
akkadských	akkadský	k2eAgNnPc6d1	akkadské
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
verze	verze	k1gFnPc1	verze
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
chetitských	chetitský	k2eAgMnPc2d1	chetitský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
eposu	epos	k1gInSc2	epos
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
Turecku	Turecko	k1gNnSc3	Turecko
<g/>
,	,	kIx,	,
Palestině	Palestina	k1gFnSc3	Palestina
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
texty	text	k1gInPc1	text
pocházejí	pocházet	k5eAaImIp3nP	pocházet
vesměs	vesměs	k6eAd1	vesměs
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fragmenty	fragment	k1gInPc4	fragment
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zkrácené	zkrácený	k2eAgFnPc1d1	zkrácená
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
takto	takto	k6eAd1	takto
nalezených	nalezený	k2eAgFnPc6d1	nalezená
částech	část	k1gFnPc6	část
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
úpravy	úprava	k1gFnPc4	úprava
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vydání	vydání	k1gNnSc1	vydání
===	===	k?	===
</s>
</p>
<p>
<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Matouš	Matouš	k1gMnSc1	Matouš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
–	–	k?	–
148	[number]	k4	148
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-204-0281-0	[number]	k4	80-204-0281-0
</s>
</p>
<p>
<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prosecký	Prosecký	k2eAgMnSc1d1	Prosecký
<g/>
,	,	kIx,	,
Hruška	Hruška	k1gMnSc1	Hruška
a	a	k8xC	a
Rychtařík	Rychtařík	k1gMnSc1	Rychtařík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
–	–	k?	–
410	[number]	k4	410
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7106-517-X	[number]	k4	80-7106-517-X
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
–	–	k?	–
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byl	být	k5eAaImAgInS	být
Sumer	Sumer	k1gInSc1	Sumer
<g/>
;	;	kIx,	;
Panorama	panorama	k1gNnSc1	panorama
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
–	–	k?	–
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
<g/>
;	;	kIx,	;
Albatros	albatros	k1gMnSc1	albatros
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Henrieta	Henriet	k2eAgFnSc1d1	Henrieta
Mc	Mc	k1gFnSc1	Mc
Callová	Callová	k1gFnSc1	Callová
–	–	k?	–
Mezopotamské	mezopotamský	k2eAgInPc1d1	mezopotamský
mýty	mýtus	k1gInPc1	mýtus
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
Ivo	Ivo	k1gMnSc1	Ivo
Šmoldas	Šmoldas	k1gMnSc1	Šmoldas
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Prosecký	Prosecký	k2eAgMnSc1d1	Prosecký
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Součková	Součková	k1gFnSc1	Součková
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Břeňová	Břeňová	k1gFnSc1	Břeňová
–	–	k?	–
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
mytologie	mytologie	k1gFnSc2	mytologie
starověkého	starověký	k2eAgInSc2d1	starověký
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
<g/>
;	;	kIx,	;
Libri	Libr	k1gFnSc2	Libr
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Prosecký	Prosecký	k2eAgMnSc1d1	Prosecký
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Součková	Součková	k1gFnSc1	Součková
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Matouš	Matouš	k1gMnSc1	Matouš
–	–	k?	–
Mýty	mýtus	k1gInPc1	mýtus
staré	starý	k2eAgFnSc2d1	stará
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
;	;	kIx,	;
Odeon	odeon	k1gInSc1	odeon
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
The	The	k?	The
Evolution	Evolution	k1gInSc1	Evolution
of	of	k?	of
the	the	k?	the
Gilgamesh	Gilgamesh	k1gInSc1	Gilgamesh
Epic	Epic	k1gFnSc1	Epic
–	–	k?	–
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Gilgameš	Gilgamat	k5eAaPmIp2nS	Gilgamat
</s>
</p>
<p>
<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
(	(	kIx(	(
<g/>
oratorium	oratorium	k1gNnSc1	oratorium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sumer	Sumer	k1gMnSc1	Sumer
</s>
</p>
<p>
<s>
Bible	bible	k1gFnSc1	bible
</s>
</p>
<p>
<s>
Ur	Ur	k1gInSc1	Ur
</s>
</p>
<p>
<s>
Uruk	Uruk	k6eAd1	Uruk
</s>
</p>
<p>
<s>
Babylon	Babylon	k1gInSc1	Babylon
</s>
</p>
<p>
<s>
Aššurbanipal	Aššurbanipat	k5eAaImAgMnS	Aššurbanipat
</s>
</p>
<p>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
</s>
</p>
<p>
<s>
Sumersko-akkadská	Sumerskokkadský	k2eAgFnSc1d1	Sumersko-akkadský
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Epos	epos	k1gInSc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgamešův	k2eAgMnPc1d1	Gilgamešův
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.grimoar.cz/gilgames/glgms.php?	[url]	k?	http://www.grimoar.cz/gilgames/glgms.php?
</s>
</p>
<p>
<s>
http://www.znovu.cz/pohadky/Gilgames.htm	[url]	k6eAd1	http://www.znovu.cz/pohadky/Gilgames.htm
</s>
</p>
<p>
<s>
http://gilgames.ic.cz/	[url]	k?	http://gilgames.ic.cz/
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Odkaz	odkaz	k1gInSc1	odkaz
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
stejné	stejný	k2eAgFnPc4d1	stejná
stránky	stránka	k1gFnPc4	stránka
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgNnPc4d1	předchozí
<g/>
,	,	kIx,	,
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
strany	strana	k1gFnSc2	strana
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.wsu.edu/~dee/MESO/GILG.HTM	[url]	k?	http://www.wsu.edu/~dee/MESO/GILG.HTM
</s>
</p>
