<p>
<s>
James	James	k1gMnSc1	James
Buchanan	Buchanan	k1gMnSc1	Buchanan
[	[	kIx(	[
<g/>
bjúkhenen	bjúkhenen	k2eAgMnSc1d1	bjúkhenen
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1791	[number]	k4	1791
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
starým	starý	k1gMnSc7	starý
mládencem	mládenec	k1gMnSc7	mládenec
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
)	)	kIx)	)
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
z	z	k7c2	z
Pennsylvanie	Pennsylvanie	k1gFnSc2	Pennsylvanie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
selhání	selhání	k1gNnSc4	selhání
během	během	k7c2	během
zatažení	zatažení	k1gNnSc2	zatažení
země	zem	k1gFnSc2	zem
do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yInSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgMnPc2d3	nejhorší
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Buchanan	Buchanan	k1gMnSc1	Buchanan
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Cove	Cove	k1gFnSc6	Cove
Gap	Gap	k1gFnSc2	Gap
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
v	v	k7c6	v
obchodnické	obchodnický	k2eAgFnSc6d1	obchodnická
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
funkci	funkce	k1gFnSc4	funkce
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
Ruska	Rusko	k1gNnSc2	Rusko
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Andrewa	Andrewus	k1gMnSc2	Andrewus
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Jamese	Jamese	k1gFnSc2	Jamese
K.	K.	kA	K.
Polka	Polka	k1gFnSc1	Polka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
Pensylvánii	Pensylvánie	k1gFnSc4	Pensylvánie
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
komorách	komora	k1gFnPc6	komora
parlamentu	parlament	k1gInSc2	parlament
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Franklina	Franklin	k2eAgInSc2d1	Franklin
Pierce	Pierec	k1gInSc2	Pierec
zastával	zastávat	k5eAaImAgInS	zastávat
úřad	úřad	k1gInSc1	úřad
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
nebyl	být	k5eNaImAgMnS	být
přímo	přímo	k6eAd1	přímo
zatažen	zatáhnout	k5eAaPmNgMnS	zatáhnout
do	do	k7c2	do
neúspěšné	úspěšný	k2eNgFnSc2d1	neúspěšná
politiky	politika	k1gFnSc2	politika
tohoto	tento	k3xDgMnSc2	tento
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
kandidátem	kandidát	k1gMnSc7	kandidát
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
však	však	k9	však
spíše	spíše	k9	spíše
opakoval	opakovat	k5eAaImAgMnS	opakovat
chyby	chyba	k1gFnSc2	chyba
předchozího	předchozí	k2eAgMnSc2d1	předchozí
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
USA	USA	kA	USA
<g/>
,	,	kIx,	,
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
zájmy	zájem	k1gInPc4	zájem
otrokářů	otrokář	k1gMnPc2	otrokář
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
násilné	násilný	k2eAgInPc1d1	násilný
střety	střet	k1gInPc1	střet
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kansas	Kansas	k1gInSc4	Kansas
ohledně	ohledně	k7c2	ohledně
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
povolení	povolení	k1gNnSc4	povolení
či	či	k8xC	či
zakázání	zakázání	k1gNnSc4	zakázání
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
vyřešit	vyřešit	k5eAaPmF	vyřešit
povolením	povolení	k1gNnSc7	povolení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
státu	stát	k1gInSc3	stát
Utah	Utah	k1gInSc1	Utah
povolal	povolat	k5eAaPmAgInS	povolat
federální	federální	k2eAgNnPc4d1	federální
vojska	vojsko	k1gNnPc4	vojsko
kvůli	kvůli	k7c3	kvůli
údajné	údajný	k2eAgFnSc3d1	údajná
revoltě	revolta	k1gFnSc3	revolta
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
podezření	podezření	k1gNnSc1	podezření
se	se	k3xPyFc4	se
nepotvrdilo	potvrdit	k5eNaPmAgNnS	potvrdit
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Buchanan	Buchanan	k1gMnSc1	Buchanan
později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
omluvil	omluvit	k5eAaPmAgMnS	omluvit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Napětí	napětí	k1gNnSc1	napětí
v	v	k7c6	v
USA	USA	kA	USA
mezi	mezi	k7c7	mezi
severem	sever	k1gInSc7	sever
a	a	k8xC	a
jihem	jih	k1gInSc7	jih
se	se	k3xPyFc4	se
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
přiostřilo	přiostřit	k5eAaPmAgNnS	přiostřit
a	a	k8xC	a
země	země	k1gFnSc1	země
již	již	k6eAd1	již
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
odstupu	odstup	k1gInSc2	odstup
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
řekl	říct	k5eAaPmAgMnS	říct
svému	svůj	k3xOyFgMnSc3	svůj
nástupci	nástupce	k1gMnSc3	nástupce
Abrahamu	Abraham	k1gMnSc3	Abraham
Lincolnovi	Lincoln	k1gMnSc3	Lincoln
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vážený	vážený	k2eAgMnSc5d1	vážený
pane	pan	k1gMnSc5	pan
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
<g/>
-li	i	k?	-li
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
tak	tak	k6eAd1	tak
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
mohu	moct	k5eAaImIp1nS	moct
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jste	být	k5eAaImIp2nP	být
skutečně	skutečně	k6eAd1	skutečně
šťastný	šťastný	k2eAgMnSc1d1	šťastný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
James	James	k1gMnSc1	James
Buchanan	Buchanany	k1gInPc2	Buchanany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
