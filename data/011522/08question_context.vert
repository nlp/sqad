<s>
James	James	k1gMnSc1	James
Buchanan	Buchanan	k1gMnSc1	Buchanan
[	[	kIx(	[
<g/>
bjúkhenen	bjúkhenen	k2eAgMnSc1d1	bjúkhenen
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1791	[number]	k4	1791
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
starým	starý	k1gMnSc7	starý
mládencem	mládenec	k1gMnSc7	mládenec
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
)	)	kIx)	)
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
z	z	k7c2	z
Pennsylvanie	Pennsylvanie	k1gFnSc2	Pennsylvanie
<g/>
.	.	kIx.	.
</s>
