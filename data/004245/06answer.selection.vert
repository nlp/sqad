<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
nejvýraznějšími	výrazný	k2eAgFnPc7d3	nejvýraznější
dominantami	dominanta	k1gFnPc7	dominanta
jsou	být	k5eAaImIp3nP	být
hrad	hrad	k1gInSc4	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
obklopena	obklopit	k5eAaPmNgFnS	obklopit
domy	dům	k1gInPc1	dům
královské	královský	k2eAgFnSc2d1	královská
stoliční	stoliční	k2eAgFnSc2d1	stoliční
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
