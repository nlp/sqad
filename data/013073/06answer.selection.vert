<s>
Cesium	cesium	k1gNnSc1	cesium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cs	Cs	k1gFnSc2	Cs
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Caesium	Caesium	k1gNnSc1	Caesium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
reaktivitou	reaktivita	k1gFnSc7	reaktivita
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
nízkým	nízký	k2eAgInSc7d1	nízký
redoxním	redoxní	k2eAgInSc7d1	redoxní
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
</s>
