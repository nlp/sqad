<p>
<s>
Cesium	cesium	k1gNnSc1	cesium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cs	Cs	k1gFnSc2	Cs
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Caesium	Caesium	k1gNnSc1	Caesium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
reaktivitou	reaktivita	k1gFnSc7	reaktivita
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
nízkým	nízký	k2eAgInSc7d1	nízký
redoxním	redoxní	k2eAgInSc7d1	redoxní
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Cesium	cesium	k1gNnSc1	cesium
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
stříbrnozlatý	stříbrnozlatý	k2eAgInSc1d1	stříbrnozlatý
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
krájet	krájet	k5eAaImF	krájet
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejměkčí	měkký	k2eAgInSc4d3	nejměkčí
prvek	prvek	k1gInSc4	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
zároveň	zároveň	k6eAd1	zároveň
druhý	druhý	k4xOgInSc1	druhý
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
i	i	k8xC	i
varu	var	k1gInSc2	var
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
po	po	k7c6	po
rtuti	rtuť	k1gFnSc6	rtuť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lithia	lithium	k1gNnSc2	lithium
<g/>
,	,	kIx,	,
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
má	mít	k5eAaImIp3nS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rubidiem	rubidium	k1gNnSc7	rubidium
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vede	vést	k5eAaImIp3nS	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
parách	para	k1gFnPc6	para
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jednoatomových	jednoatomový	k2eAgFnPc2d1	jednoatomový
částic	částice	k1gFnPc2	částice
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
molekuly	molekula	k1gFnPc1	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
mají	mít	k5eAaImIp3nP	mít
modrozelenou	modrozelený	k2eAgFnSc4d1	modrozelená
až	až	k9	až
zelenošedou	zelenošedý	k2eAgFnSc4d1	zelenošedá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
na	na	k7c4	na
temně	temně	k6eAd1	temně
modrý	modrý	k2eAgInSc4d1	modrý
roztok	roztok	k1gInSc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc4d1	elementární
kovové	kovový	k2eAgNnSc4d1	kovové
cesium	cesium	k1gNnSc4	cesium
lze	lze	k6eAd1	lze
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchovávat	uchovávat	k5eAaImF	uchovávat
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
jako	jako	k8xS	jako
petrolej	petrolej	k1gInSc1	petrolej
nebo	nebo	k8xC	nebo
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yQgFnPc7	který
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesium	cesium	k1gNnSc1	cesium
mimořádně	mimořádně	k6eAd1	mimořádně
rychle	rychle	k6eAd1	rychle
až	až	k9	až
explozivně	explozivně	k6eAd1	explozivně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
na	na	k7c4	na
superoxid	superoxid	k1gInSc4	superoxid
cesný	cesný	k1gMnSc1	cesný
a	a	k8xC	a
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
cesný	cesný	k2eAgInSc4d1	cesný
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Cesium	cesium	k1gNnSc1	cesium
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stupni	stupeň	k1gInSc6	stupeň
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
CsI	CsI	k1gFnSc1	CsI
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
cesia	cesium	k1gNnSc2	cesium
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
exotermní	exotermní	k2eAgNnSc1d1	exotermní
<g/>
,	,	kIx,	,
že	že	k8xS	že
unikající	unikající	k2eAgInSc1d1	unikající
vodík	vodík	k1gInSc1	vodík
reakčním	reakční	k2eAgNnSc7d1	reakční
teplem	teplo	k1gNnSc7	teplo
samovolně	samovolně	k6eAd1	samovolně
explozivně	explozivně	k6eAd1	explozivně
vzplane	vzplanout	k5eAaPmIp3nS	vzplanout
<g/>
.	.	kIx.	.
</s>
<s>
Cesium	cesium	k1gNnSc1	cesium
se	se	k3xPyFc4	se
také	také	k9	také
za	za	k7c2	za
mírného	mírný	k2eAgNnSc2d1	mírné
zahřátí	zahřátí	k1gNnSc2	zahřátí
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
na	na	k7c4	na
hydrid	hydrid	k1gInSc4	hydrid
cesný	cesný	k1gMnSc1	cesný
CsH	CsH	k1gMnSc1	CsH
<g/>
,	,	kIx,	,
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
na	na	k7c4	na
nitrid	nitrid	k1gInSc4	nitrid
cesný	cesný	k2eAgInSc4d1	cesný
Cs	Cs	k1gFnSc7	Cs
<g/>
3	[number]	k4	3
<g/>
N	N	kA	N
nebo	nebo	k8xC	nebo
azid	azid	k1gInSc4	azid
cesný	cesný	k2eAgInSc4d1	cesný
CsN	CsN	k1gFnSc7	CsN
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímo	přímo	k6eNd1	přímo
se	se	k3xPyFc4	se
také	také	k9	také
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
cesia	cesium	k1gNnSc2	cesium
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
modře	modř	k1gFnSc2	modř
až	až	k9	až
fialově	fialově	k6eAd1	fialově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Cesium	cesium	k1gNnSc1	cesium
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
německým	německý	k2eAgMnSc7d1	německý
chemikem	chemik	k1gMnSc7	chemik
Robertem	Robert	k1gMnSc7	Robert
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Bunsenem	Bunsen	k1gMnSc7	Bunsen
a	a	k8xC	a
německým	německý	k2eAgMnSc7d1	německý
fyzikem	fyzik	k1gMnSc7	fyzik
Gustavem	Gustav	k1gMnSc7	Gustav
R.	R.	kA	R.
Kirchhoffem	Kirchhoff	k1gInSc7	Kirchhoff
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
jimi	on	k3xPp3gMnPc7	on
objevené	objevený	k2eAgFnPc1d1	objevená
spektrální	spektrální	k2eAgFnPc1d1	spektrální
analýzy	analýza	k1gFnPc1	analýza
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
cesium	cesium	k1gNnSc4	cesium
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
dürkheimských	dürkheimský	k2eAgFnPc6d1	dürkheimský
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rubidiem	rubidium	k1gNnSc7	rubidium
<g/>
.	.	kIx.	.
</s>
<s>
Cesium	cesium	k1gNnSc1	cesium
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
dvou	dva	k4xCgFnPc2	dva
modrých	modrý	k2eAgFnPc2d1	modrá
čar	čára	k1gFnPc2	čára
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
jako	jako	k8xS	jako
modrošedý	modrošedý	k2eAgInSc1d1	modrošedý
–	–	k?	–
latinsky	latinsky	k6eAd1	latinsky
caesius	caesius	k1gInSc4	caesius
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgNnSc1d1	čisté
cesium	cesium	k1gNnSc1	cesium
se	se	k3xPyFc4	se
Robertu	Robert	k1gMnSc3	Robert
Bunsenovi	Bunsen	k1gMnSc3	Bunsen
nepodařilo	podařit	k5eNaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
<g/>
,	,	kIx,	,
připravil	připravit	k5eAaPmAgMnS	připravit
pouze	pouze	k6eAd1	pouze
cesný	cesný	k2eAgInSc4d1	cesný
amalgám	amalgám	k1gInSc4	amalgám
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc4d1	kovové
cesium	cesium	k1gNnSc4	cesium
poprvé	poprvé	k6eAd1	poprvé
získal	získat	k5eAaPmAgMnS	získat
Carl	Carl	k1gMnSc1	Carl
Setterberg	Setterberg	k1gMnSc1	Setterberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
směsi	směs	k1gFnSc2	směs
kyanidu	kyanid	k1gInSc2	kyanid
cesného	cesné	k1gNnSc2	cesné
a	a	k8xC	a
kyanidu	kyanid	k1gInSc2	kyanid
barnatého	barnatý	k2eAgInSc2d1	barnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
velké	velký	k2eAgFnSc3d1	velká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
sloučeniny	sloučenina	k1gFnPc4	sloučenina
cesia	cesium	k1gNnSc2	cesium
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Cs	Cs	k1gFnSc2	Cs
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesium	cesium	k1gNnSc1	cesium
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
jak	jak	k8xC	jak
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tak	tak	k9	tak
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
mg	mg	kA	mg
Cs	Cs	k1gFnSc2	Cs
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
2,6	[number]	k4	2,6
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
úroveň	úroveň	k1gFnSc4	úroveň
jako	jako	k8xS	jako
brom	brom	k1gInSc4	brom
<g/>
,	,	kIx,	,
hafnium	hafnium	k1gNnSc4	hafnium
a	a	k8xC	a
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
0,5	[number]	k4	0,5
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
výskyt	výskyt	k1gInSc1	výskyt
1	[number]	k4	1
atomu	atom	k1gInSc2	atom
cesia	cesium	k1gNnSc2	cesium
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minerálech	minerál	k1gInPc6	minerál
provází	provázet	k5eAaImIp3nS	provázet
cesium	cesium	k1gNnSc1	cesium
obvykle	obvykle	k6eAd1	obvykle
ostatní	ostatní	k2eAgInPc1d1	ostatní
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Minerál	minerál	k1gInSc1	minerál
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
výskytem	výskyt	k1gInSc7	výskyt
cesia	cesium	k1gNnSc2	cesium
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
polucit	polucit	k5eAaBmF	polucit
CsSi	CsSi	k1gNnSc4	CsSi
<g/>
2	[number]	k4	2
<g/>
AlO	ala	k1gFnSc5	ala
<g/>
6	[number]	k4	6
nebo	nebo	k8xC	nebo
i	i	k9	i
(	(	kIx(	(
<g/>
Cs	Cs	k1gFnSc1	Cs
<g/>
,	,	kIx,	,
<g/>
Na	na	k7c6	na
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
Si	se	k3xPyFc3	se
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
12.2	[number]	k4	12.2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c6	o
</s>
</p>
<p>
<s>
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
drúzách	drúza	k1gFnPc6	drúza
ostrova	ostrov	k1gInSc2	ostrov
Elby	Elba	k1gFnSc2	Elba
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
v	v	k7c6	v
minerálu	minerál	k1gInSc6	minerál
lepidolitu	lepidolit	k1gInSc2	lepidolit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
hlinito-křemičitan	hlinitořemičitan	k1gInSc4	hlinito-křemičitan
lithno-draselný	lithnoraselný	k2eAgInSc4d1	lithno-draselný
KLi	KLi	k1gFnSc7	KLi
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
AlSi	AlS	k1gInSc3	AlS
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
minerálu	minerál	k1gInSc6	minerál
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
cesia	cesium	k1gNnSc2	cesium
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
hodnoty	hodnota	k1gFnSc2	hodnota
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
(	(	kIx(	(
<g/>
asi	asi	k9	asi
okolo	okolo	k7c2	okolo
0,015	[number]	k4	0,015
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
karnalitu	karnalit	k1gInSc6	karnalit
KCl	KCl	k1gFnSc2	KCl
<g/>
.	.	kIx.	.
<g/>
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2.6	[number]	k4	2.6
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgNnSc1d1	elementární
cesium	cesium	k1gNnSc1	cesium
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztavené	roztavený	k2eAgFnSc2d1	roztavená
směsi	směs	k1gFnSc2	směs
60	[number]	k4	60
%	%	kIx~	%
chloridu	chlorid	k1gInSc6	chlorid
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
chloridu	chlorid	k1gInSc2	chlorid
cesného	cesný	k2eAgInSc2d1	cesný
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
750	[number]	k4	750
°	°	k?	°
<g/>
C.	C.	kA	C.
Vápník	vápník	k1gInSc4	vápník
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
ve	v	k7c6	v
sběrné	sběrný	k2eAgFnSc6d1	sběrná
nádobě	nádoba	k1gFnSc6	nádoba
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
cesia	cesium	k1gNnPc4	cesium
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
od	od	k7c2	od
cesia	cesium	k1gNnSc2	cesium
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
Elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
železné	železný	k2eAgFnSc6d1	železná
katodě	katoda	k1gFnSc6	katoda
a	a	k8xC	a
grafitové	grafitový	k2eAgFnSc3d1	grafitová
anodě	anoda	k1gFnSc3	anoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
plynný	plynný	k2eAgInSc1d1	plynný
chlor	chlor	k1gInSc1	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
kov	kov	k1gInSc4	kov
však	však	k9	však
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
okolo	okolo	k7c2	okolo
pěti	pět	k4xCc2	pět
tun	tuna	k1gFnPc2	tuna
cesia	cesium	k1gNnSc2	cesium
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železná	železný	k2eAgFnSc1d1	železná
katoda	katoda	k1gFnSc1	katoda
2	[number]	k4	2
Cs	Cs	k1gFnPc2	Cs
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
e-	e-	k?	e-
→	→	k?	→
2	[number]	k4	2
Cs	Cs	k1gFnPc2	Cs
</s>
</p>
<p>
<s>
Grafitová	grafitový	k2eAgFnSc1d1	grafitová
anoda	anoda	k1gFnSc1	anoda
2	[number]	k4	2
Cl-	Cl-	k1gMnPc2	Cl-
→	→	k?	→
Cl	Cl	k1gFnSc7	Cl
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
e-Lepší	e-Lepší	k1gNnSc2	e-Lepší
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
chemickou	chemický	k2eAgFnSc4d1	chemická
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
zahříváním	zahřívání	k1gNnSc7	zahřívání
hydroxidu	hydroxid	k1gInSc2	hydroxid
cesného	cesné	k1gNnSc2	cesné
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
cesného	cesný	k1gMnSc2	cesný
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
hořčíkem	hořčík	k1gInSc7	hořčík
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
vodíku	vodík	k1gInSc2	vodík
nebo	nebo	k8xC	nebo
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
vápníkem	vápník	k1gInSc7	vápník
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
redukovadlo	redukovadlo	k1gNnSc1	redukovadlo
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
zirkonium	zirkonium	k1gNnSc1	zirkonium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
cesia	cesium	k1gNnSc2	cesium
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
zahříváním	zahřívání	k1gNnSc7	zahřívání
chloridu	chlorid	k1gInSc2	chlorid
cesného	cesný	k2eAgInSc2d1	cesný
s	s	k7c7	s
azidem	azid	k1gInSc7	azid
barnatým	barnatý	k2eAgInSc7d1	barnatý
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Baryum	baryum	k1gNnSc1	baryum
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
rozkladem	rozklad	k1gInSc7	rozklad
azidu	azid	k1gInSc2	azid
vytěsňuje	vytěsňovat	k5eAaImIp3nS	vytěsňovat
z	z	k7c2	z
chloridu	chlorid	k1gInSc2	chlorid
cesného	cesné	k1gNnSc2	cesné
cesium	cesium	k1gNnSc1	cesium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svých	svůj	k3xOyFgFnPc2	svůj
par	para	k1gFnPc2	para
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
na	na	k7c6	na
chladnějších	chladný	k2eAgFnPc6d2	chladnější
stěnách	stěna	k1gFnPc6	stěna
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
mimořádné	mimořádný	k2eAgFnSc3d1	mimořádná
nestálosti	nestálost	k1gFnSc3	nestálost
a	a	k8xC	a
reaktivitě	reaktivita	k1gFnSc3	reaktivita
má	mít	k5eAaImIp3nS	mít
kovové	kovový	k2eAgNnSc4d1	kovové
cesium	cesium	k1gNnSc4	cesium
jen	jen	k6eAd1	jen
minimální	minimální	k2eAgNnSc4d1	minimální
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nízký	nízký	k2eAgInSc1d1	nízký
ionizační	ionizační	k2eAgInSc1d1	ionizační
potenciál	potenciál	k1gInSc1	potenciál
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
jeho	on	k3xPp3gNnSc2	on
uplatnění	uplatnění	k1gNnSc2	uplatnění
ve	v	k7c6	v
fotočláncích	fotočlánek	k1gInPc6	fotočlánek
<g/>
,	,	kIx,	,
sloužících	sloužící	k2eAgInPc2d1	sloužící
pro	pro	k7c4	pro
přímou	přímý	k2eAgFnSc4d1	přímá
přeměnu	přeměna	k1gFnSc4	přeměna
světelné	světelný	k2eAgFnSc2d1	světelná
energie	energie	k1gFnSc2	energie
v	v	k7c4	v
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
perspektivním	perspektivní	k2eAgNnSc7d1	perspektivní
médiem	médium	k1gNnSc7	médium
pro	pro	k7c4	pro
iontové	iontový	k2eAgInPc4d1	iontový
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pohonné	pohonný	k2eAgFnPc1d1	pohonná
jednotky	jednotka	k1gFnPc1	jednotka
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
elektronek	elektronka	k1gFnPc2	elektronka
a	a	k8xC	a
fotonek	fotonka	k1gFnPc2	fotonka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
kov	kov	k1gInSc1	kov
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
elektrony	elektron	k1gInPc4	elektron
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
světlem	světlo	k1gNnSc7	světlo
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
<g/>
)	)	kIx)	)
<g/>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
katodových	katodový	k2eAgFnPc2d1	katodová
trubic	trubice	k1gFnPc2	trubice
<g/>
,	,	kIx,	,
pracujících	pracující	k2eAgInPc2d1	pracující
s	s	k7c7	s
nízkotlakou	nízkotlaký	k2eAgFnSc7d1	nízkotlaká
náplní	náplň	k1gFnSc7	náplň
inertního	inertní	k2eAgInSc2d1	inertní
plynu	plyn	k1gInSc2	plyn
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
cesia	cesium	k1gNnSc2	cesium
jako	jako	k8xS	jako
getru	getr	k1gInSc2	getr
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
látky	látka	k1gFnSc2	látka
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
a	a	k8xC	a
odstranění	odstranění	k1gNnSc3	odstranění
posledních	poslední	k2eAgInPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
přimíšených	přimíšený	k2eAgInPc2d1	přimíšený
reaktivních	reaktivní	k2eAgInPc2d1	reaktivní
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
<g/>
většina	většina	k1gFnSc1	většina
Cs	Cs	k1gFnSc2	Cs
jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
velmi	velmi	k6eAd1	velmi
hustých	hustý	k2eAgInPc2d1	hustý
výplachů	výplach	k1gInPc2	výplach
pro	pro	k7c4	pro
hlubinné	hlubinný	k2eAgNnSc4d1	hlubinné
vrtání	vrtání	k1gNnSc4	vrtání
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
mravenčanu	mravenčan	k1gInSc2	mravenčan
cesného	cesné	k1gNnSc2	cesné
(	(	kIx(	(
<g/>
HCOOCs	HCOOCs	k1gInSc1	HCOOCs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
až	až	k9	až
2,3	[number]	k4	2,3
g	g	kA	g
<g/>
·	·	k?	·
<g/>
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
do	do	k7c2	do
přístrojů	přístroj	k1gInPc2	přístroj
pro	pro	k7c4	pro
noční	noční	k2eAgNnSc4d1	noční
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fotonásobičích	fotonásobič	k1gInPc6	fotonásobič
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
přijímačíchizotop	přijímačíchizotop	k1gInSc4	přijímačíchizotop
137	[number]	k4	137
<g/>
Cs	Cs	k1gFnPc2	Cs
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
30,08	[number]	k4	30,08
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
nedestruktivním	destruktivní	k2eNgNnSc6d1	nedestruktivní
zkoušení	zkoušení	k1gNnSc6	zkoušení
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
defektoskopii	defektoskopie	k1gFnSc4	defektoskopie
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
ozařování	ozařování	k1gNnSc6	ozařování
rakovinných	rakovinný	k2eAgFnPc2d1	rakovinná
nádorůOd	nádorůOda	k1gFnPc2	nádorůOda
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
definována	definován	k2eAgFnSc1d1	definována
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
1	[number]	k4	1
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
frekvence	frekvence	k1gFnSc2	frekvence
emitovaného	emitovaný	k2eAgNnSc2d1	emitované
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
izotopu	izotop	k1gInSc2	izotop
133	[number]	k4	133
<g/>
Cs	Cs	k1gFnPc2	Cs
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
9	[number]	k4	9
192	[number]	k4	192
631	[number]	k4	631
770	[number]	k4	770
period	perioda	k1gFnPc2	perioda
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hladinami	hladina	k1gFnPc7	hladina
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgFnPc4d1	jemná
struktury	struktura	k1gFnPc4	struktura
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
tohoto	tento	k3xDgInSc2	tento
izotopu	izotop	k1gInSc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
atomy	atom	k1gInPc4	atom
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
a	a	k8xC	a
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
cesný	cesný	k2eAgInSc1d1	cesný
CsH	CsH	k1gFnSc4	CsH
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
nestálý	stálý	k2eNgInSc1d1	nestálý
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
i	i	k8xC	i
se	s	k7c7	s
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
mírně	mírně	k6eAd1	mírně
zahřátého	zahřátý	k2eAgNnSc2d1	zahřáté
kovového	kovový	k2eAgNnSc2d1	kovové
cesia	cesium	k1gNnSc2	cesium
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
<g/>
Superoxid	superoxid	k1gInSc4	superoxid
cesný	cesný	k2eAgInSc4d1	cesný
CsO	CsO	k1gFnSc7	CsO
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
na	na	k7c6	na
vlhkém	vlhký	k2eAgInSc6d1	vlhký
vzduchu	vzduch	k1gInSc6	vzduch
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
silného	silný	k2eAgNnSc2d1	silné
redukčního	redukční	k2eAgNnSc2d1	redukční
činidla	činidlo	k1gNnSc2	činidlo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jemnou	jemný	k2eAgFnSc7d1	jemná
oxidací	oxidace	k1gFnPc2	oxidace
odštěpí	odštěpit	k5eAaPmIp3nS	odštěpit
jeden	jeden	k4xCgInSc4	jeden
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
přejde	přejít	k5eAaPmIp3nS	přejít
v	v	k7c4	v
peroxid	peroxid	k1gInSc4	peroxid
cesný	cesný	k1gMnSc1	cesný
a	a	k8xC	a
silnější	silný	k2eAgFnSc1d2	silnější
oxidací	oxidace	k1gFnSc7	oxidace
odštěpí	odštěpit	k5eAaPmIp3nS	odštěpit
dva	dva	k4xCgInPc4	dva
kyslíky	kyslík	k1gInPc4	kyslík
a	a	k8xC	a
přejde	přejít	k5eAaPmIp3nS	přejít
v	v	k7c4	v
oxid	oxid	k1gInSc4	oxid
cesný	cesný	k2eAgInSc4d1	cesný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
cesia	cesium	k1gNnSc2	cesium
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
i	i	k8xC	i
oxidy	oxid	k1gInPc1	oxid
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
suboxidy	suboxid	k1gInPc4	suboxid
<g/>
)	)	kIx)	)
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
Cs	Cs	k1gFnSc1	Cs
<g/>
7	[number]	k4	7
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
Cs	Cs	k1gFnSc1	Cs
<g/>
4	[number]	k4	4
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
Cs	Cs	k1gFnSc1	Cs
<g/>
7	[number]	k4	7
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Cs	Cs	k1gFnSc1	Cs
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
Cs	Cs	k1gFnSc1	Cs
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
a	a	k8xC	a
Cs	Cs	k1gFnSc1	Cs
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Superoxid	superoxid	k1gInSc1	superoxid
cesný	cesný	k1gMnSc1	cesný
vzniká	vznikat	k5eAaImIp3nS	vznikat
hořením	hoření	k1gNnSc7	hoření
cesia	cesium	k1gNnSc2	cesium
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
nebo	nebo	k8xC	nebo
i	i	k9	i
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
samovolné	samovolný	k2eAgFnSc6d1	samovolná
oxidaci	oxidace	k1gFnSc6	oxidace
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
<g/>
Cs	Cs	k1gFnSc1	Cs
+	+	kIx~	+
O2	O2	k1gMnSc1	O2
→	→	k?	→
CsO	CsO	k1gMnSc1	CsO
<g/>
2	[number]	k4	2
<g/>
Hydroxid	hydroxid	k1gInSc1	hydroxid
cesný	cesný	k2eAgInSc1d1	cesný
CsOH	CsOH	k1gFnSc4	CsOH
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
analogických	analogický	k2eAgFnPc2d1	analogická
sloučenin	sloučenina	k1gFnPc2	sloučenina
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
málo	málo	k6eAd1	málo
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezeně	omezeně	k6eAd1	omezeně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
zásada	zásada	k1gFnSc1	zásada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgInPc4d1	silný
žíravé	žíravý	k2eAgInPc4d1	žíravý
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
cesia	cesium	k1gNnSc2	cesium
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
cesného	cesné	k1gNnSc2	cesné
<g/>
,	,	kIx,	,
peroxidu	peroxid	k1gInSc2	peroxid
cesného	cesné	k1gNnSc2	cesné
nebo	nebo	k8xC	nebo
superoxidu	superoxid	k1gInSc2	superoxid
cesného	cesný	k1gMnSc2	cesný
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
cesného	cesné	k1gNnSc2	cesné
či	či	k8xC	či
podvojnou	podvojný	k2eAgFnSc7d1	podvojná
záměnou	záměna	k1gFnSc7	záměna
mezi	mezi	k7c7	mezi
cesnou	cesný	k2eAgFnSc7d1	cesný
solí	sůl	k1gFnSc7	sůl
a	a	k8xC	a
hydroxidem	hydroxid	k1gInSc7	hydroxid
kovu	kov	k1gInSc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Soli	sůl	k1gFnSc6	sůl
====	====	k?	====
</s>
</p>
<p>
<s>
Cesné	Cesný	k2eAgFnPc1d1	Cesný
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
a	a	k8xC	a
jen	jen	k9	jen
několik	několik	k4yIc1	několik
je	být	k5eAaImIp3nS	být
nerozpustných	rozpustný	k2eNgMnPc6d1	nerozpustný
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgMnPc4	všechen
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
soli	sůl	k1gFnSc2	sůl
barevný	barevný	k2eAgMnSc1d1	barevný
(	(	kIx(	(
<g/>
manganistany	manganistan	k1gInPc1	manganistan
<g/>
,	,	kIx,	,
chromany	chroman	k1gInPc1	chroman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cesné	Cesné	k1gNnSc1	Cesné
soli	sůl	k1gFnSc2	sůl
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
nesnadno	snadno	k6eNd1	snadno
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
50	[number]	k4	50
lety	let	k1gInPc7	let
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
nebyly	být	k5eNaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
žádné	žádný	k3yNgInPc1	žádný
komplexy	komplex	k1gInPc1	komplex
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
vůbec	vůbec	k9	vůbec
schopny	schopen	k2eAgFnPc1d1	schopna
tvořit	tvořit	k5eAaImF	tvořit
komplexy	komplex	k1gInPc4	komplex
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
cesný	cesný	k2eAgInSc4d1	cesný
CsCl	CsCl	k1gInSc4	CsCl
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
cesný	cesný	k2eAgInSc1d1	cesný
i	i	k8xC	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
cesné	cesný	k2eAgFnPc1d1	cesný
halogenidy	halogenida	k1gFnPc1	halogenida
mají	mít	k5eAaImIp3nP	mít
silný	silný	k2eAgInSc4d1	silný
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
polyhalogenidů	polyhalogenid	k1gInPc2	polyhalogenid
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
kyseliny	kyselina	k1gFnPc1	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnPc1d1	chlorovodíková
s	s	k7c7	s
uhličitanem	uhličitan	k1gInSc7	uhličitan
cesným	cesný	k2eAgInSc7d1	cesný
nebo	nebo	k8xC	nebo
hydroxidem	hydroxid	k1gInSc7	hydroxid
cesným	cesný	k2eAgInSc7d1	cesný
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
cesné	cesný	k2eAgInPc1d1	cesný
halogenidy	halogenid	k1gInPc1	halogenid
nemají	mít	k5eNaImIp3nP	mít
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
<g/>
Dusičnan	dusičnan	k1gInSc4	dusičnan
cesný	cesný	k2eAgInSc4d1	cesný
CsNO	CsNO	k1gFnSc7	CsNO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
dusičnanu	dusičnan	k1gInSc3	dusičnan
draselnému	draselný	k2eAgNnSc3d1	draselné
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
kyseliny	kyselina	k1gFnPc1	kyselina
dusičné	dusičný	k2eAgFnPc1d1	dusičná
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
cesným	cesný	k2eAgInSc7d1	cesný
nebo	nebo	k8xC	nebo
uhličitanem	uhličitan	k1gInSc7	uhličitan
cesným	cesný	k2eAgInSc7d1	cesný
<g/>
.	.	kIx.	.
<g/>
Uhličitan	uhličitan	k1gInSc1	uhličitan
cesný	cesný	k1gMnSc1	cesný
Cs	Cs	k1gMnSc1	Cs
<g/>
2	[number]	k4	2
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
síranu	síran	k1gInSc2	síran
cesného	cesný	k2eAgInSc2d1	cesný
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
barnatým	barnatý	k2eAgInSc7d1	barnatý
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
odpařením	odpaření	k1gNnSc7	odpaření
s	s	k7c7	s
uhličitanem	uhličitan	k1gInSc7	uhličitan
amonným	amonný	k2eAgInSc7d1	amonný
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
cesného	cesné	k1gNnSc2	cesné
se	se	k3xPyFc4	se
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
<g/>
Síran	síran	k1gInSc1	síran
cesný	cesný	k1gMnSc1	cesný
Cs	Cs	k1gMnSc1	Cs
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
podobá	podobat	k5eAaImIp3nS	podobat
síranu	síran	k1gInSc2	síran
draselnému	draselný	k2eAgInSc3d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
smíšené	smíšený	k2eAgFnPc4d1	smíšená
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
uhličitanu	uhličitan	k1gInSc2	uhličitan
cesného	cesné	k1gNnSc2	cesné
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
cesného	cesný	k1gMnSc2	cesný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
<g/>
Fosforečnan	fosforečnan	k1gInSc4	fosforečnan
cesný	cesný	k2eAgInSc4d1	cesný
Cs	Cs	k1gFnSc7	Cs
<g/>
3	[number]	k4	3
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
cesia	cesium	k1gNnSc2	cesium
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
cesné	cesný	k2eAgFnPc1d1	cesný
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
cesné	cesný	k2eAgInPc4d1	cesný
alkoholáty	alkoholát	k1gInPc4	alkoholát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
cesným	cesný	k2eAgFnPc3d1	cesný
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
cesných	cesný	k2eAgFnPc2d1	cesný
sloučenin	sloučenina	k1gFnPc2	sloučenina
tzv.	tzv.	kA	tzv.
crowny	crowna	k1gFnSc2	crowna
a	a	k8xC	a
kryptáty	kryptát	k1gInPc4	kryptát
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
cesných	cesný	k2eAgFnPc2d1	cesný
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jursík	Jursík	k1gMnSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cesium	cesium	k1gNnSc4	cesium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cesium	cesium	k1gNnSc4	cesium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
