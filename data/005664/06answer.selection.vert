<s>
Trenčkot	trenčkot	k1gInSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
pláště	plášť	k1gInSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
vyráběného	vyráběný	k2eAgMnSc2d1
z	z	k7c2
odolné	odolný	k2eAgFnSc2d1
bavlny	bavlna	k1gFnSc2
<g/>
,	,	kIx,
popelínu	popelín	k1gInSc2
<g/>
,	,	kIx,
gabardénu	gabardén	k1gInSc2
<g/>
,	,	kIx,
vlny	vlna	k1gFnSc2
či	či	k8xC
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>