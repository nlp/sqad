<s>
Trenčkot	trenčkot	k1gInSc1	trenčkot
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
Trench	Trench	k1gMnSc1	Trench
coat	coat	k1gMnSc1	coat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
svrchního	svrchní	k2eAgInSc2d1	svrchní
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Trenčkot	trenčkot	k1gInSc1	trenčkot
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vyráběného	vyráběný	k2eAgMnSc2d1	vyráběný
z	z	k7c2	z
odolné	odolný	k2eAgFnSc2d1	odolná
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
popelínu	popelín	k1gInSc2	popelín
<g/>
,	,	kIx,	,
gabardénu	gabardén	k1gInSc2	gabardén
<g/>
,	,	kIx,	,
vlny	vlna	k1gFnSc2	vlna
či	či	k8xC	či
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
mechanická	mechanický	k2eAgFnSc1d1	mechanická
odolnost	odolnost	k1gFnSc1	odolnost
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
,	,	kIx,	,
nepromokavost	nepromokavost	k1gFnSc1	nepromokavost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dosahováno	dosahovat	k5eAaImNgNnS	dosahovat
impregnací	impregnace	k1gFnSc7	impregnace
tkaniny	tkanina	k1gFnSc2	tkanina
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
zapletením	zapletení	k1gNnSc7	zapletení
do	do	k7c2	do
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojená	spojený	k2eAgFnSc1d1	spojená
paropropustnost	paropropustnost	k1gFnSc1	paropropustnost
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vyjímatelnou	vyjímatelný	k2eAgFnSc4d1	vyjímatelná
vložku	vložka	k1gFnSc4	vložka
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
trenčkotu	trenčkot	k1gInSc2	trenčkot
obvykle	obvykle	k6eAd1	obvykle
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
kolen	kolna	k1gFnPc2	kolna
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezení	vynalezení	k1gNnSc1	vynalezení
trenčkotu	trenčkot	k1gInSc2	trenčkot
si	se	k3xPyFc3	se
nárokují	nárokovat	k5eAaImIp3nP	nárokovat
dvě	dva	k4xCgFnPc1	dva
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
Aquascutum	Aquascutum	k1gNnSc1	Aquascutum
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
produkovala	produkovat	k5eAaImAgFnS	produkovat
užitné	užitný	k2eAgInPc4d1	užitný
kabáty	kabát	k1gInPc4	kabát
pro	pro	k7c4	pro
armádní	armádní	k2eAgMnPc4d1	armádní
důstojníky	důstojník	k1gMnPc4	důstojník
v	v	k7c6	v
Krymské	krymský	k2eAgFnSc6d1	Krymská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kabáty	kabát	k1gInPc7	kabát
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgInSc1d1	spojen
příběh	příběh	k1gInSc1	příběh
dvou	dva	k4xCgMnPc2	dva
britských	britský	k2eAgMnPc2d1	britský
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
kabátům	kabát	k1gInPc3	kabát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
nelišily	lišit	k5eNaImAgFnP	lišit
od	od	k7c2	od
těch	ten	k3xDgInPc2	ten
užívaných	užívaný	k2eAgInPc2d1	užívaný
ruskou	ruský	k2eAgFnSc7d1	ruská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
pochodovali	pochodovat	k5eAaImAgMnP	pochodovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
vytratit	vytratit	k5eAaPmF	vytratit
a	a	k8xC	a
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
vlastním	vlastní	k2eAgFnPc3d1	vlastní
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
užívá	užívat	k5eAaImIp3nS	užívat
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
nepromokavosti	nepromokavost	k1gFnSc2	nepromokavost
patentovanou	patentovaný	k2eAgFnSc4d1	patentovaná
impregnaci	impregnace	k1gFnSc4	impregnace
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
společností	společnost	k1gFnSc7	společnost
je	být	k5eAaImIp3nS	být
Burberry	Burberra	k1gFnSc2	Burberra
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nepromokavosti	nepromokavost	k1gFnPc4	nepromokavost
použitím	použití	k1gNnSc7	použití
patentovaného	patentovaný	k2eAgInSc2d1	patentovaný
nepromokavého	promokavý	k2eNgInSc2d1	nepromokavý
gabardénu	gabardén	k1gInSc2	gabardén
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Thomas	Thomas	k1gMnSc1	Thomas
Burberry	Burberra	k1gFnSc2	Burberra
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
vzor	vzor	k1gInSc4	vzor
trenčkotu	trenčkot	k1gInSc2	trenčkot
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
začal	začít	k5eAaPmAgMnS	začít
dodávat	dodávat	k5eAaImF	dodávat
trenčkot	trenčkot	k1gInSc4	trenčkot
pro	pro	k7c4	pro
britské	britský	k2eAgMnPc4d1	britský
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Trenčkoty	trenčkot	k1gInPc1	trenčkot
se	se	k3xPyFc4	se
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
i	i	k9	i
mezi	mezi	k7c4	mezi
civilní	civilní	k2eAgNnSc4d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgInP	dočkat
trenčkoty	trenčkot	k1gInPc1	trenčkot
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc3	jejich
použití	použití	k1gNnSc3	použití
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
armády	armáda	k1gFnPc4	armáda
<g/>
,	,	kIx,	,
příkladmo	příkladmo	k6eAd1	příkladmo
ruskou	ruský	k2eAgFnSc4d1	ruská
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
<g/>
,	,	kIx,	,
americkou	americký	k2eAgFnSc4d1	americká
<g/>
.	.	kIx.	.
</s>
<s>
Trenčkot	trenčkot	k1gInSc1	trenčkot
byl	být	k5eAaImAgInS	být
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
proti	proti	k7c3	proti
dešti	dešť	k1gInSc3	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
módní	módní	k2eAgInSc1d1	módní
doplněk	doplněk	k1gInSc1	doplněk
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gFnSc7	jejich
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
<g/>
:	:	kIx,	:
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trenčkot	trenčkot	k1gInSc1	trenčkot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
trenčkotu	trenčkot	k1gInSc2	trenčkot
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
trenčkotu	trenčkot	k1gInSc2	trenčkot
od	od	k7c2	od
Burberryho	Burberry	k1gMnSc2	Burberry
</s>
