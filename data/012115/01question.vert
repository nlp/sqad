<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
světová	světový	k2eAgFnSc1d1	světová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
?	?	kIx.	?
</s>
