<s>
Vydán	vydán	k2eAgMnSc1d1	vydán
byl	být	k5eAaImAgMnS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
kopií	kopie	k1gFnPc2	kopie
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejrychleji	rychle	k6eAd3	rychle
prodávaným	prodávaný	k2eAgInSc7d1	prodávaný
herním	herní	k2eAgInSc7d1	herní
titulem	titul	k1gInSc7	titul
<g/>
.	.	kIx.	.
</s>
