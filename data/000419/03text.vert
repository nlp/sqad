<s>
Héra	Héra	k1gFnSc1	Héra
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἥ	Ἥ	k?	Ἥ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
olympské	olympský	k2eAgFnSc2d1	Olympská
dvanáctky	dvanáctka	k1gFnSc2	dvanáctka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
manželkou	manželka	k1gFnSc7	manželka
Dia	Dia	k1gFnSc7	Dia
a	a	k8xC	a
patronkou	patronka	k1gFnSc7	patronka
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
zrození	zrození	k1gNnSc2	zrození
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Kronos	Kronos	k1gMnSc1	Kronos
ji	on	k3xPp3gFnSc4	on
snědl	sníst	k5eAaPmAgMnS	sníst
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
povstáním	povstání	k1gNnSc7	povstání
svych	svych	k?	svych
dětí	dítě	k1gFnPc2	dítě
Její	její	k3xOp3gInSc4	její
kult	kult	k1gInSc4	kult
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	let	k1gInPc6	let
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
do	do	k7c2	do
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
přijetí	přijetí	k1gNnSc2	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
postavení	postavení	k1gNnSc1	postavení
je	být	k5eAaImIp3nS	být
krajně	krajně	k6eAd1	krajně
netypické	typický	k2eNgNnSc1d1	netypické
<g/>
,	,	kIx,	,
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
vlastně	vlastně	k9	vlastně
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
bohyni	bohyně	k1gFnSc4	bohyně
(	(	kIx(	(
<g/>
ženské	ženský	k2eAgNnSc4d1	ženské
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
její	její	k3xOp3gFnSc1	její
moc	moc	k1gFnSc1	moc
nikoli	nikoli	k9	nikoli
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
mýty	mýtus	k1gInPc1	mýtus
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
Héru	Héra	k1gFnSc4	Héra
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
rozporuplnou	rozporuplný	k2eAgFnSc4d1	rozporuplná
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
pantheonu	pantheon	k1gInSc6	pantheon
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
protějškem	protějšek	k1gInSc7	protějšek
bohyně	bohyně	k1gFnSc1	bohyně
Iuno	Iuno	k1gFnSc1	Iuno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
schopnosti	schopnost	k1gFnPc4	schopnost
patřilo	patřit	k5eAaImAgNnS	patřit
měnit	měnit	k5eAaImF	měnit
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
viděla	vidět	k5eAaImAgFnS	vidět
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Vládla	vládnout	k5eAaImAgFnS	vládnout
mlhám	mlha	k1gFnPc3	mlha
<g/>
,	,	kIx,	,
bouřím	bouř	k1gFnPc3	bouř
<g/>
,	,	kIx,	,
bleskům	blesk	k1gInPc3	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládla	vládnout	k5eAaImAgFnS	vládnout
i	i	k9	i
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
měla	mít	k5eAaImAgFnS	mít
především	především	k6eAd1	především
manželství	manželství	k1gNnSc4	manželství
a	a	k8xC	a
právě	právě	k9	právě
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
modlily	modlit	k5eAaImAgFnP	modlit
manželky	manželka	k1gFnPc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
velice	velice	k6eAd1	velice
žárlivá	žárlivý	k2eAgFnSc1d1	žárlivá
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
četné	četný	k2eAgFnSc2d1	četná
milenky	milenka	k1gFnSc2	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nymfu	nymfa	k1gFnSc4	nymfa
Io	Io	k1gFnSc2	Io
musel	muset	k5eAaImAgInS	muset
Zeus	Zeus	k1gInSc1	Zeus
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c4	v
krávu	kráva	k1gFnSc4	kráva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
Héra	Héra	k1gFnSc1	Héra
nenašla	najít	k5eNaPmAgFnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zase	zase	k9	zase
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohyně	bohyně	k1gFnSc1	bohyně
Leto	Leto	k6eAd1	Leto
čeká	čekat	k5eAaImIp3nS	čekat
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
zakázala	zakázat	k5eAaPmAgFnS	zakázat
všem	všecek	k3xTgInPc3	všecek
ostrovům	ostrov	k1gInPc3	ostrov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
dovolily	dovolit	k5eAaPmAgFnP	dovolit
zůstat	zůstat	k5eAaPmF	zůstat
a	a	k8xC	a
porodit	porodit	k5eAaPmF	porodit
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
bohů	bůh	k1gMnPc2	bůh
se	se	k3xPyFc4	se
Rheie	Rheie	k1gFnSc1	Rheie
a	a	k8xC	a
Kronovi	Kronův	k2eAgMnPc1d1	Kronův
narodila	narodit	k5eAaPmAgFnS	narodit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
bratr	bratr	k1gMnSc1	bratr
Zeus	Zeusa	k1gFnPc2	Zeusa
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
mýtu	mýtus	k1gInSc2	mýtus
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
kukačku	kukačka	k1gFnSc4	kukačka
<g/>
,	,	kIx,	,
svedl	svést	k5eAaPmAgInS	svést
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gNnSc3	on
syna	syn	k1gMnSc4	syn
Area	Ares	k1gMnSc4	Ares
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
však	však	k9	však
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
Olympané	Olympan	k1gMnPc1	Olympan
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k1gNnSc7	další
je	být	k5eAaImIp3nS	být
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
Héra	Héra	k1gFnSc1	Héra
zplodila	zplodit	k5eAaPmAgFnS	zplodit
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
natruc	natruc	k6eAd1	natruc
Diovým	Diův	k2eAgInPc3d1	Diův
záletům	zálet	k1gInPc3	zálet
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
bohyně	bohyně	k1gFnSc1	bohyně
věčného	věčný	k2eAgNnSc2d1	věčné
mládí	mládí	k1gNnSc2	mládí
Hébé	Hébé	k1gFnSc2	Hébé
a	a	k8xC	a
Eileithýia	Eileithýium	k1gNnSc2	Eileithýium
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
otcovství	otcovství	k1gNnSc6	otcovství
není	být	k5eNaImIp3nS	být
také	také	k9	také
zcela	zcela	k6eAd1	zcela
jasno	jasno	k6eAd1	jasno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
Héřiny	Héřin	k2eAgFnPc4d1	Héřina
vlastnosti	vlastnost	k1gFnPc4	vlastnost
rozhodně	rozhodně	k6eAd1	rozhodně
patřila	patřit	k5eAaImAgFnS	patřit
žárlivost	žárlivost	k1gFnSc4	žárlivost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yQgInPc7	který
ji	on	k3xPp3gFnSc4	on
Zeus	Zeus	k1gInSc1	Zeus
podvedl	podvést	k5eAaPmAgInS	podvést
a	a	k8xC	a
pronásledovala	pronásledovat	k5eAaImAgNnP	pronásledovat
také	také	k6eAd1	také
potomky	potomek	k1gMnPc7	potomek
manželova	manželův	k2eAgNnSc2d1	manželovo
vzplanutí	vzplanutí	k1gNnSc2	vzplanutí
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
slavným	slavný	k2eAgMnPc3d1	slavný
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
její	její	k3xOp3gNnSc1	její
pronásledování	pronásledování	k1gNnSc1	pronásledování
Herakla	Herakles	k1gMnSc2	Herakles
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
důstojná	důstojný	k2eAgFnSc1d1	důstojná
dáma	dáma	k1gFnSc1	dáma
či	či	k8xC	či
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
emblémovým	emblémový	k2eAgNnSc7d1	emblémový
zvířetem	zvíře	k1gNnSc7	zvíře
byla	být	k5eAaImAgFnS	být
kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
páv	páv	k1gMnSc1	páv
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
kukačka	kukačka	k1gFnSc1	kukačka
a	a	k8xC	a
jeřáb	jeřáb	k1gInSc1	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
atributy	atribut	k1gInPc4	atribut
patřilo	patřit	k5eAaImAgNnS	patřit
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
,	,	kIx,	,
obětní	obětní	k2eAgInSc4d1	obětní
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
obětní	obětní	k2eAgFnSc1d1	obětní
miska	miska	k1gFnSc1	miska
<g/>
,	,	kIx,	,
krb	krb	k1gInSc1	krb
či	či	k8xC	či
granátové	granátový	k2eAgNnSc1d1	granátové
jablko	jablko	k1gNnSc1	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Heraia	Heraia	k1gFnSc1	Heraia
-	-	kIx~	-
novoroční	novoroční	k2eAgFnSc2d1	novoroční
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
chrámu	chrám	k1gInSc3	chrám
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
<g/>
,	,	kIx,	,
taženém	tažený	k2eAgInSc6d1	tažený
voly	vůl	k1gMnPc4	vůl
<g/>
.	.	kIx.	.
</s>
<s>
Ohňové	ohňový	k2eAgFnPc1d1	ohňová
slavnosti	slavnost	k1gFnPc1	slavnost
v	v	k7c6	v
Boiótii	Boiótie	k1gFnSc6	Boiótie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
připomínaly	připomínat	k5eAaImAgFnP	připomínat
obětování	obětování	k1gNnSc4	obětování
Plataei	Platae	k1gFnSc2	Platae
<g/>
,	,	kIx,	,
Diovy	Diův	k2eAgFnSc2d1	Diova
milenky	milenka	k1gFnSc2	milenka
(	(	kIx(	(
<g/>
slavnosti	slavnost	k1gFnSc2	slavnost
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
každé	každý	k3xTgInPc1	každý
čtyři	čtyři	k4xCgInPc1	čtyři
roky	rok	k1gInPc1	rok
pod	pod	k7c7	pod
Olympem	Olymp	k1gInSc7	Olymp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
,	,	kIx,	,
Theogonia	Theogonium	k1gNnSc2	Theogonium
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Héra	Héra	k1gFnSc1	Héra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Héré	Hérá	k1gFnSc2	Hérá
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Héra	Héra	k1gFnSc1	Héra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Héra	Héra	k1gFnSc1	Héra
na	na	k7c6	na
antika	antika	k1gFnSc1	antika
<g/>
.	.	kIx.	.
<g/>
avonet	avonet	k1gInSc1	avonet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
