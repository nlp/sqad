<s>
Héra	Héra	k1gFnSc1	Héra
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἥ	Ἥ	k?	Ἥ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
olympské	olympský	k2eAgFnSc2d1	Olympská
dvanáctky	dvanáctka	k1gFnSc2	dvanáctka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
manželkou	manželka	k1gFnSc7	manželka
Dia	Dia	k1gFnSc7	Dia
a	a	k8xC	a
patronkou	patronka	k1gFnSc7	patronka
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
zrození	zrození	k1gNnSc2	zrození
<g/>
.	.	kIx.	.
</s>

