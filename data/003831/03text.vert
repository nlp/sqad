<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
krvinka	krvinka	k1gFnSc1	krvinka
či	či	k8xC	či
leukocyt	leukocyt	k1gInSc1	leukocyt
je	být	k5eAaImIp3nS	být
krevní	krevní	k2eAgFnSc1d1	krevní
buňka	buňka	k1gFnSc1	buňka
mnohých	mnohý	k2eAgMnPc2d1	mnohý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
fungování	fungování	k1gNnSc4	fungování
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
zpravidla	zpravidla	k6eAd1	zpravidla
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
virům	vir	k1gInPc3	vir
<g/>
,	,	kIx,	,
bakteriím	bakterie	k1gFnPc3	bakterie
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
patogenům	patogen	k1gInPc3	patogen
či	či	k8xC	či
částicím	částice	k1gFnPc3	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nádorovým	nádorový	k2eAgFnPc3d1	nádorová
buňkám	buňka	k1gFnPc3	buňka
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
všem	všecek	k3xTgMnPc3	všecek
organismu	organismus	k1gInSc6	organismus
cizím	cizí	k2eAgMnPc3d1	cizí
materiálům	materiál	k1gInPc3	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
typů	typ	k1gInPc2	typ
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zejména	zejména	k9	zejména
tzv.	tzv.	kA	tzv.
fagocytární	fagocytární	k2eAgInPc1d1	fagocytární
(	(	kIx(	(
<g/>
pohlcující	pohlcující	k2eAgFnPc1d1	pohlcující
<g/>
)	)	kIx)	)
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
a	a	k8xC	a
nespecifických	specifický	k2eNgInPc2d1	nespecifický
způsobů	způsob	k1gInPc2	způsob
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
choroboplodným	choroboplodný	k2eAgInPc3d1	choroboplodný
zárodkům	zárodek	k1gInPc3	zárodek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
nespecifické	specifický	k2eNgFnSc3d1	nespecifická
obraně	obrana	k1gFnSc3	obrana
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
makrofágy	makrofág	k1gInPc4	makrofág
nebo	nebo	k8xC	nebo
neutrofily	neutrofil	k1gMnPc4	neutrofil
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
rafinovanější	rafinovaný	k2eAgFnPc4d2	rafinovanější
metody	metoda	k1gFnPc4	metoda
vyhledání	vyhledání	k1gNnSc2	vyhledání
a	a	k8xC	a
usmrcení	usmrcení	k1gNnSc2	usmrcení
patogenních	patogenní	k2eAgInPc2d1	patogenní
organizmů	organizmus	k1gInPc2	organizmus
či	či	k8xC	či
nádorových	nádorový	k2eAgFnPc2d1	nádorová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
7,4	[number]	k4	7,4
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
na	na	k7c4	na
litr	litr	k1gInSc4	litr
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
než	než	k8xS	než
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
;	;	kIx,	;
zrají	zrát	k5eAaImIp3nP	zrát
především	především	k9	především
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c6	v
brzlíku	brzlík	k1gInSc6	brzlík
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
nefungují	fungovat	k5eNaImIp3nP	fungovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc4	ten
vyvolat	vyvolat	k5eAaPmF	vyvolat
vážná	vážné	k1gNnPc4	vážné
onemocnění	onemocnění	k1gNnSc2	onemocnění
spadající	spadající	k2eAgNnSc4d1	spadající
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
autoimunity	autoimunita	k1gFnSc2	autoimunita
či	či	k8xC	či
imunitní	imunitní	k2eAgFnSc2d1	imunitní
nedostatečnosti	nedostatečnost	k1gFnSc2	nedostatečnost
<g/>
,	,	kIx,	,
dožívají	dožívat	k5eAaImIp3nP	dožívat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
až	až	k8xS	až
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Imunita	imunita	k1gFnSc1	imunita
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
velice	velice	k6eAd1	velice
pozvolna	pozvolna	k6eAd1	pozvolna
a	a	k8xC	a
i	i	k9	i
organizmy	organizmus	k1gInPc1	organizmus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
imunitní	imunitní	k2eAgFnPc4d1	imunitní
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
schopný	schopný	k2eAgInSc1d1	schopný
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
cizí	cizí	k2eAgFnPc4d1	cizí
od	od	k7c2	od
svého	své	k1gNnSc2	své
<g/>
.	.	kIx.	.
</s>
<s>
Imunita	imunita	k1gFnSc1	imunita
zprostředkovaná	zprostředkovaný	k2eAgFnSc1d1	zprostředkovaná
buňkami	buňka	k1gFnPc7	buňka
však	však	k9	však
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
není	být	k5eNaImIp3nS	být
vlastnost	vlastnost	k1gFnSc1	vlastnost
vyhrazená	vyhrazený	k2eAgFnSc1d1	vyhrazená
pouze	pouze	k6eAd1	pouze
člověku	člověk	k1gMnSc6	člověk
<g/>
;	;	kIx,	;
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
krvinkami	krvinka	k1gFnPc7	krvinka
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
u	u	k7c2	u
primitivních	primitivní	k2eAgInPc2d1	primitivní
organizmů	organizmus	k1gInPc2	organizmus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
améby	améba	k1gFnPc1	améba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odhaleny	odhalen	k2eAgFnPc1d1	odhalena
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
buňky	buňka	k1gFnPc1	buňka
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
masy	masa	k1gFnSc2	masa
améboidních	améboidní	k2eAgFnPc2d1	améboidní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
odstraňující	odstraňující	k2eAgFnSc2d1	odstraňující
patogenní	patogenní	k2eAgFnSc2d1	patogenní
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
u	u	k7c2	u
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
omezují	omezovat	k5eAaImIp3nP	omezovat
na	na	k7c4	na
fagocytující	fagocytující	k2eAgFnPc4d1	fagocytující
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
v	v	k7c6	v
cévách	céva	k1gFnPc6	céva
či	či	k8xC	či
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
,	,	kIx,	,
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
tzv.	tzv.	kA	tzv.
nespecifickou	specifický	k2eNgFnSc4d1	nespecifická
(	(	kIx(	(
<g/>
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
<g/>
)	)	kIx)	)
imunitu	imunita	k1gFnSc4	imunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
představují	představovat	k5eAaImIp3nP	představovat
předky	předek	k1gInPc1	předek
lidských	lidský	k2eAgInPc2d1	lidský
makrofágů	makrofág	k1gInPc2	makrofág
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
od	od	k7c2	od
primitivních	primitivní	k2eAgInPc2d1	primitivní
houbovců	houbovec	k1gInPc2	houbovec
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
hmyz	hmyz	k1gInSc4	hmyz
až	až	k9	až
k	k	k7c3	k
obratlovcům	obratlovec	k1gMnPc3	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
přehledu	přehled	k1gInSc2	přehled
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kořeny	kořen	k1gInPc1	kořen
nespecifické	specifický	k2eNgFnSc2d1	nespecifická
buněčné	buněčný	k2eAgFnSc2d1	buněčná
imunity	imunita	k1gFnSc2	imunita
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vystopovat	vystopovat	k5eAaPmF	vystopovat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
tzv.	tzv.	kA	tzv.
specifické	specifický	k2eAgFnSc2d1	specifická
čili	čili	k8xC	čili
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
představována	představován	k2eAgFnSc1d1	představována
především	především	k6eAd1	především
T-lymfocyty	Tymfocyt	k1gInPc7	T-lymfocyt
a	a	k8xC	a
B-lymfocyty	Bymfocyt	k1gInPc7	B-lymfocyt
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Nepochybně	pochybně	k6eNd1	pochybně
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
imunitou	imunita	k1gFnSc7	imunita
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
mnoho	mnoho	k4c4	mnoho
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
u	u	k7c2	u
nejstarších	starý	k2eAgMnPc2d3	nejstarší
čelistnatých	čelistnatý	k2eAgMnPc2d1	čelistnatý
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
:	:	kIx,	:
všichni	všechen	k3xTgMnPc1	všechen
čelistnatci	čelistnatec	k1gMnPc1	čelistnatec
(	(	kIx(	(
<g/>
Gnathostomata	Gnathostoma	k1gNnPc1	Gnathostoma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
obratlovci	obratlovec	k1gMnPc1	obratlovec
bez	bez	k7c2	bez
mihulí	mihule	k1gFnPc2	mihule
a	a	k8xC	a
sliznatek	sliznatka	k1gFnPc2	sliznatka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgInPc1d1	podobný
T-buněčné	Tuněčný	k2eAgInPc1d1	T-buněčný
a	a	k8xC	a
B-buněčné	Buněčný	k2eAgInPc1d1	B-buněčný
receptory	receptor	k1gInPc1	receptor
<g/>
,	,	kIx,	,
MHC	MHC	kA	MHC
komplexy	komplex	k1gInPc1	komplex
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
i	i	k9	i
unikátní	unikátní	k2eAgInSc4d1	unikátní
jev	jev	k1gInSc4	jev
označovaný	označovaný	k2eAgInSc4d1	označovaný
V	V	kA	V
<g/>
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
J	J	kA	J
rekombinace	rekombinace	k1gFnSc2	rekombinace
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
u	u	k7c2	u
mihulí	mihule	k1gFnPc2	mihule
a	a	k8xC	a
sliznatek	sliznatka	k1gFnPc2	sliznatka
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
buňky	buňka	k1gFnPc1	buňka
nerozeznatelné	rozeznatelný	k2eNgFnPc1d1	nerozeznatelná
od	od	k7c2	od
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
opakovaně	opakovaně	k6eAd1	opakovaně
nalézány	nalézat	k5eAaImNgFnP	nalézat
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
určitý	určitý	k2eAgInSc4d1	určitý
způsob	způsob	k1gInSc4	způsob
rekombinace	rekombinace	k1gFnSc1	rekombinace
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
povrchové	povrchový	k2eAgFnPc4d1	povrchová
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
odlišný	odlišný	k2eAgInSc4d1	odlišný
od	od	k7c2	od
V	V	kA	V
<g/>
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
J	J	kA	J
rekombinace	rekombinace	k1gFnSc2	rekombinace
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
však	však	k9	však
nemají	mít	k5eNaImIp3nP	mít
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
TCR	TCR	kA	TCR
<g/>
,	,	kIx,	,
BCR	BCR	kA	BCR
a	a	k8xC	a
MHC	MHC	kA	MHC
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
krvinka	krvinka	k1gFnSc1	krvinka
čili	čili	k8xC	čili
leukocyt	leukocyt	k1gInSc1	leukocyt
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
sběrný	sběrný	k2eAgInSc1d1	sběrný
termín	termín	k1gInSc1	termín
popisující	popisující	k2eAgInSc1d1	popisující
původně	původně	k6eAd1	původně
ty	ten	k3xDgFnPc1	ten
krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
červenými	červený	k2eAgFnPc7d1	červená
krvinkami	krvinka	k1gFnPc7	krvinka
světlejší	světlý	k2eAgFnSc4d2	světlejší
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
známe	znát	k5eAaImIp1nP	znát
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
typy	typ	k1gInPc1	typ
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
vývojem	vývoj	k1gInSc7	vývoj
i	i	k8xC	i
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
obraně	obrana	k1gFnSc6	obrana
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
plnohodnotné	plnohodnotný	k2eAgFnPc1d1	plnohodnotná
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
typické	typický	k2eAgFnPc1d1	typická
eukaryotické	eukaryotický	k2eAgFnPc1d1	eukaryotická
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
původ	původ	k1gInSc4	původ
z	z	k7c2	z
hematopoetické	hematopoetický	k2eAgFnSc2d1	hematopoetický
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
monocyty	monocyt	k1gInPc1	monocyt
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rozměrů	rozměr	k1gInPc2	rozměr
někdy	někdy	k6eAd1	někdy
až	až	k9	až
30	[number]	k4	30
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
třeba	třeba	k9	třeba
některé	některý	k3yIgInPc1	některý
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pouhých	pouhý	k2eAgInPc2d1	pouhý
6	[number]	k4	6
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
granulocyty	granulocyt	k1gInPc1	granulocyt
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
jádro	jádro	k1gNnSc4	jádro
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
nezralé	zralý	k2eNgInPc1d1	nezralý
granulocyty	granulocyt	k1gInPc1	granulocyt
bývají	bývat	k5eAaImIp3nP	bývat
tyčkovitého	tyčkovitý	k2eAgMnSc2d1	tyčkovitý
a	a	k8xC	a
různě	různě	k6eAd1	různě
pokřiveného	pokřivený	k2eAgInSc2d1	pokřivený
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgInPc1d1	dospělý
granulocyty	granulocyt	k1gInPc1	granulocyt
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
zralosti	zralost	k1gFnSc2	zralost
jsou	být	k5eAaImIp3nP	být
laločnaté	laločnatý	k2eAgInPc1d1	laločnatý
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
segmentované	segmentovaný	k2eAgFnPc4d1	segmentovaná
(	(	kIx(	(
<g/>
dělené	dělený	k2eAgFnPc4d1	dělená
na	na	k7c4	na
několik	několik	k4yIc4	několik
často	často	k6eAd1	často
propojených	propojený	k2eAgInPc2d1	propojený
segmentů	segment	k1gInPc2	segment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
například	například	k6eAd1	například
eosinofilní	eosinofilní	k2eAgInPc1d1	eosinofilní
granulocyty	granulocyt	k1gInPc1	granulocyt
mívají	mívat	k5eAaImIp3nP	mívat
jádra	jádro	k1gNnPc4	jádro
dvoulaločnatá	dvoulaločnatý	k2eAgNnPc4d1	dvoulaločnatý
<g/>
,	,	kIx,	,
neutrofily	neutrofil	k1gMnPc4	neutrofil
mají	mít	k5eAaImIp3nP	mít
jádra	jádro	k1gNnPc1	jádro
mnoholaločnatá	mnoholaločnatý	k2eAgNnPc1d1	mnoholaločnatý
<g/>
,	,	kIx,	,
jádra	jádro	k1gNnPc1	jádro
monocytů	monocyt	k1gInPc2	monocyt
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
jednolaločnatá	jednolaločnatý	k2eAgNnPc1d1	jednolaločnatý
či	či	k8xC	či
ledvinitá	ledvinitý	k2eAgNnPc1d1	ledvinité
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jádra	jádro	k1gNnPc1	jádro
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
malých	malý	k2eAgFnPc2d1	malá
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
řazených	řazený	k2eAgFnPc2d1	řazená
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
agranulocyty	agranulocyt	k1gInPc1	agranulocyt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
kulovitá	kulovitý	k2eAgNnPc1d1	kulovité
a	a	k8xC	a
zabírají	zabírat	k5eAaImIp3nP	zabírat
většinu	většina	k1gFnSc4	většina
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
již	již	k6eAd1	již
objevily	objevit	k5eAaPmAgInP	objevit
termíny	termín	k1gInPc1	termín
granulocyt	granulocyt	k1gInSc1	granulocyt
a	a	k8xC	a
agranulocyt	agranulocyt	k1gInSc1	agranulocyt
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
rysem	rys	k1gInSc7	rys
mnoha	mnoho	k4c2	mnoho
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
totiž	totiž	k9	totiž
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
granula	granula	k1gFnSc1	granula
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vnitrobuněčné	vnitrobuněčný	k2eAgInPc1d1	vnitrobuněčný
váčky	váček	k1gInPc1	váček
ohraničené	ohraničený	k2eAgInPc1d1	ohraničený
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Granulocyty	Granulocyt	k1gInPc1	Granulocyt
granula	granula	k1gFnSc1	granula
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
agranulocyty	agranulocyt	k1gInPc1	agranulocyt
jich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
málo	málo	k6eAd1	málo
nebo	nebo	k8xC	nebo
je	on	k3xPp3gMnPc4	on
nemají	mít	k5eNaImIp3nP	mít
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
granula	granula	k1gFnSc1	granula
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
granulocytů	granulocyt	k1gInPc2	granulocyt
barví	barvit	k5eAaImIp3nP	barvit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
bazofily	bazofil	k1gInPc1	bazofil
(	(	kIx(	(
<g/>
barví	barvit	k5eAaImIp3nP	barvit
se	se	k3xPyFc4	se
modře	modro	k6eAd1	modro
zásaditou	zásaditý	k2eAgFnSc7d1	zásaditá
methylenovou	methylenový	k2eAgFnSc7d1	methylenová
modří	modř	k1gFnSc7	modř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eosinofily	eosinofil	k1gMnPc7	eosinofil
(	(	kIx(	(
<g/>
barví	barvit	k5eAaImIp3nP	barvit
se	se	k3xPyFc4	se
červeně	červeně	k6eAd1	červeně
kyselými	kyselý	k2eAgNnPc7d1	kyselé
barvivy	barvivo	k1gNnPc7	barvivo
–	–	k?	–
eosinem	eosin	k1gInSc7	eosin
<g/>
)	)	kIx)	)
a	a	k8xC	a
neutrofily	neutrofil	k1gMnPc4	neutrofil
(	(	kIx(	(
<g/>
barví	barvit	k5eAaImIp3nS	barvit
se	se	k3xPyFc4	se
oběma	dva	k4xCgMnPc7	dva
typy	typ	k1gInPc4	typ
barviv	barvivo	k1gNnPc2	barvivo
fialově	fialově	k6eAd1	fialově
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Granula	granula	k1gFnSc1	granula
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
i	i	k9	i
makrofágy	makrofág	k1gInPc1	makrofág
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
ostatních	ostatní	k2eAgFnPc2d1	ostatní
organel	organela	k1gFnPc2	organela
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
výbavu	výbava	k1gFnSc4	výbava
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
buňky	buňka	k1gFnPc1	buňka
živočišných	živočišný	k2eAgNnPc2d1	živočišné
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
malé	malý	k2eAgInPc1d1	malý
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
mají	mít	k5eAaImIp3nP	mít
zakrnělé	zakrnělý	k2eAgNnSc4d1	zakrnělé
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
a	a	k8xC	a
Golgiho	Golgi	k1gMnSc2	Golgi
aparát	aparát	k1gInSc1	aparát
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
mívají	mívat	k5eAaImIp3nP	mívat
mnoho	mnoho	k4c4	mnoho
ribozomů	ribozom	k1gInPc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Makrofágy	makrofág	k1gInPc1	makrofág
a	a	k8xC	a
monocyty	monocyt	k1gInPc1	monocyt
naopak	naopak	k6eAd1	naopak
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
lysozomů	lysozom	k1gInPc2	lysozom
účastnících	účastnící	k2eAgMnPc2d1	účastnící
se	se	k3xPyFc4	se
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
trávení	trávení	k1gNnSc6	trávení
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
a	a	k8xC	a
Golgiho	Golgi	k1gMnSc4	Golgi
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
imunita	imunita	k1gFnSc1	imunita
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
obranyschopnost	obranyschopnost	k1gFnSc1	obranyschopnost
živočichů	živočich	k1gMnPc2	živočich
proti	proti	k7c3	proti
různých	různý	k2eAgFnPc6d1	různá
negativním	negativní	k2eAgInPc3d1	negativní
elementům	element	k1gInPc3	element
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
patogenní	patogenní	k2eAgInPc4d1	patogenní
organizmy	organizmus	k1gInPc4	organizmus
či	či	k8xC	či
nádorové	nádorový	k2eAgFnPc4d1	nádorová
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
případech	případ	k1gInPc6	případ
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
na	na	k7c6	na
podložce	podložka	k1gFnSc6	podložka
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
améboidního	améboidní	k2eAgInSc2d1	améboidní
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
přilnout	přilnout	k5eAaPmF	přilnout
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
povrchům	povrch	k1gInPc3	povrch
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
opustit	opustit	k5eAaPmF	opustit
krevní	krevní	k2eAgNnSc4d1	krevní
řečiště	řečiště	k1gNnSc4	řečiště
a	a	k8xC	a
vycestovat	vycestovat	k5eAaPmF	vycestovat
do	do	k7c2	do
okolní	okolní	k2eAgFnSc2d1	okolní
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
diapedézy	diapedéza	k1gFnSc2	diapedéza
leukocytů	leukocyt	k1gInPc2	leukocyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
buněčná	buněčný	k2eAgFnSc1d1	buněčná
imunita	imunita	k1gFnSc1	imunita
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
či	či	k8xC	či
obratlovců	obratlovec	k1gMnPc2	obratlovec
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obměnách	obměna	k1gFnPc6	obměna
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
projevovat	projevovat	k5eAaImF	projevovat
jako	jako	k9	jako
fagocytóza	fagocytóza	k1gFnSc1	fagocytóza
(	(	kIx(	(
<g/>
pohlcení	pohlcení	k1gNnPc2	pohlcení
a	a	k8xC	a
strávení	strávení	k1gNnPc2	strávení
<g/>
)	)	kIx)	)
cizorodých	cizorodý	k2eAgFnPc2d1	cizorodá
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
enkapsulace	enkapsulace	k1gFnSc1	enkapsulace
(	(	kIx(	(
<g/>
uzavření	uzavření	k1gNnSc1	uzavření
patogenu	patogen	k1gInSc2	patogen
do	do	k7c2	do
váčku	váček	k1gInSc2	váček
<g/>
,	,	kIx,	,
určité	určitý	k2eAgFnSc2d1	určitá
uzliny	uzlina	k1gFnSc2	uzlina
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cytotoxické	cytotoxický	k2eAgInPc1d1	cytotoxický
účinky	účinek	k1gInPc1	účinek
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
usmrcení	usmrcení	k1gNnSc2	usmrcení
jiné	jiný	k2eAgFnSc2d1	jiná
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
nebo	nebo	k8xC	nebo
hemolymfy	hemolymf	k1gInPc4	hemolymf
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
mnohých	mnohý	k2eAgMnPc2d1	mnohý
dalších	další	k2eAgMnPc2d1	další
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
obecně	obecně	k6eAd1	obecně
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
a	a	k8xC	a
specifickou	specifický	k2eAgFnSc4d1	specifická
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
hrají	hrát	k5eAaImIp3nP	hrát
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
bílé	bílý	k2eAgFnSc2d1	bílá
krvinky	krvinka	k1gFnSc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrozené	vrozený	k2eAgFnSc6d1	vrozená
imunitě	imunita	k1gFnSc6	imunita
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
granulocytů	granulocyt	k1gInPc2	granulocyt
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
NK	NK	kA	NK
buňky	buňka	k1gFnSc2	buňka
<g/>
;	;	kIx,	;
adaptivní	adaptivní	k2eAgFnSc1d1	adaptivní
(	(	kIx(	(
<g/>
specifická	specifický	k2eAgFnSc1d1	specifická
<g/>
)	)	kIx)	)
imunita	imunita	k1gFnSc1	imunita
je	být	k5eAaImIp3nS	být
doménou	doména	k1gFnSc7	doména
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Vrozená	vrozený	k2eAgFnSc1d1	vrozená
imunita	imunita	k1gFnSc1	imunita
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
náhodné	náhodný	k2eAgNnSc4d1	náhodné
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
patogenů	patogen	k1gInPc2	patogen
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
aktivace	aktivace	k1gFnSc1	aktivace
tzv.	tzv.	kA	tzv.
komplementu	komplement	k1gInSc2	komplement
<g/>
,	,	kIx,	,
odstraňování	odstraňování	k1gNnSc4	odstraňování
nalezených	nalezený	k2eAgInPc2d1	nalezený
patogenů	patogen	k1gInPc2	patogen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fagocytózou	fagocytóza	k1gFnSc7	fagocytóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
imunita	imunita	k1gFnSc1	imunita
schopna	schopen	k2eAgFnSc1d1	schopna
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
MHC	MHC	kA	MHC
komplexech	komplex	k1gInPc6	komplex
"	"	kIx"	"
<g/>
vystavovat	vystavovat	k5eAaImF	vystavovat
<g/>
"	"	kIx"	"
antigen	antigen	k1gInSc4	antigen
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
aktivovat	aktivovat	k5eAaBmF	aktivovat
lymfocyty	lymfocyt	k1gInPc4	lymfocyt
(	(	kIx(	(
<g/>
specifickou	specifický	k2eAgFnSc4d1	specifická
imunitu	imunita	k1gFnSc4	imunita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgFnSc1d1	specifická
čili	čili	k8xC	čili
adaptivní	adaptivní	k2eAgFnSc1d1	adaptivní
imunita	imunita	k1gFnSc1	imunita
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
propracovanější	propracovaný	k2eAgInSc4d2	propracovanější
způsob	způsob	k1gInSc4	způsob
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
patogenům	patogen	k1gInPc3	patogen
a	a	k8xC	a
nádorovým	nádorový	k2eAgFnPc3d1	nádorová
buňkám	buňka	k1gFnPc3	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Předně	předně	k6eAd1	předně
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
antigeny	antigen	k1gInPc4	antigen
na	na	k7c6	na
MHC	MHC	kA	MHC
komplexech	komplex	k1gInPc6	komplex
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
metody	metoda	k1gFnPc4	metoda
"	"	kIx"	"
<g/>
ušité	ušitý	k2eAgFnPc4d1	ušitá
na	na	k7c6	na
míru	mír	k1gInSc6	mír
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
konkrétní	konkrétní	k2eAgFnSc3d1	konkrétní
bakterii	bakterie	k1gFnSc3	bakterie
či	či	k8xC	či
viru	vir	k1gInSc3	vir
<g/>
;	;	kIx,	;
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopná	schopný	k2eAgFnSc1d1	schopná
tzv.	tzv.	kA	tzv.
imunologické	imunologický	k2eAgFnSc2d1	imunologická
paměti	paměť	k1gFnSc2	paměť
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nákaza	nákaza	k1gFnSc1	nákaza
opakovala	opakovat	k5eAaImAgFnS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
jsou	být	k5eAaImIp3nP	být
stálou	stálý	k2eAgFnSc7d1	stálá
součástí	součást	k1gFnSc7	součást
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
množství	množství	k1gNnSc1	množství
poměrně	poměrně	k6eAd1	poměrně
výrazně	výrazně	k6eAd1	výrazně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
mm	mm	kA	mm
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
1	[number]	k4	1
mikrolitr	mikrolitr	k1gInSc1	mikrolitr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
u	u	k7c2	u
zdravého	zdravý	k2eAgMnSc2d1	zdravý
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
4	[number]	k4	4
500	[number]	k4	500
do	do	k7c2	do
10	[number]	k4	10
000	[number]	k4	000
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
mikrolitr	mikrolitr	k1gInSc4	mikrolitr
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
uvádí	uvádět	k5eAaImIp3nS	uvádět
7	[number]	k4	7
400	[number]	k4	400
na	na	k7c4	na
1	[number]	k4	1
mm	mm	kA	mm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
7,4	[number]	k4	7,4
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
na	na	k7c4	na
litr	litr	k1gInSc4	litr
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
cca	cca	kA	cca
4,5	[number]	k4	4,5
litru	litr	k1gInSc2	litr
<g/>
)	)	kIx)	)
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
bílé	bílý	k2eAgFnPc4d1	bílá
krvinky	krvinka	k1gFnPc4	krvinka
pouhé	pouhý	k2eAgFnPc4d1	pouhá
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
45	[number]	k4	45
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
však	však	k9	však
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
počet	počet	k1gInSc4	počet
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
denní	denní	k2eAgFnSc7d1	denní
dobou	doba	k1gFnSc7	doba
(	(	kIx(	(
<g/>
ráno	ráno	k6eAd1	ráno
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
odpoledne	odpoledne	k6eAd1	odpoledne
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
aktivitou	aktivita	k1gFnSc7	aktivita
(	(	kIx(	(
<g/>
počty	počet	k1gInPc1	počet
stoupají	stoupat	k5eAaImIp3nP	stoupat
po	po	k7c6	po
fyzickém	fyzický	k2eAgInSc6d1	fyzický
výkonu	výkon	k1gInSc6	výkon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
je	být	k5eAaImIp3nS	být
i	i	k9	i
během	během	k7c2	během
horkého	horký	k2eAgNnSc2d1	horké
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
za	za	k7c2	za
intenzívního	intenzívní	k2eAgNnSc2d1	intenzívní
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
při	při	k7c6	při
sníženém	snížený	k2eAgNnSc6d1	snížené
množství	množství	k1gNnSc6	množství
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1	pohlaví
má	mít	k5eAaImIp3nS	mít
minimální	minimální	k2eAgInSc4d1	minimální
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
živočichů	živočich	k1gMnPc2	živočich
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
počty	počet	k1gInPc1	počet
velice	velice	k6eAd1	velice
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnSc4d2	vyšší
koncentraci	koncentrace	k1gFnSc4	koncentrace
leukocytů	leukocyt	k1gInPc2	leukocyt
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
u	u	k7c2	u
bažanta	bažant	k1gMnSc2	bažant
obecného	obecný	k2eAgMnSc2d1	obecný
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
mm	mm	kA	mm
<g/>
3	[number]	k4	3
asi	asi	k9	asi
8	[number]	k4	8
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
000	[number]	k4	000
leukocytů	leukocyt	k1gInPc2	leukocyt
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnPc4d2	nižší
hranice	hranice	k1gFnPc4	hranice
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc4d2	vyšší
hranice	hranice	k1gFnPc4	hranice
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
papoušků	papoušek	k1gMnPc2	papoušek
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
průměrně	průměrně	k6eAd1	průměrně
5000	[number]	k4	5000
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
000	[number]	k4	000
buněk	buňka	k1gFnPc2	buňka
<g/>
/	/	kIx~	/
<g/>
mm	mm	kA	mm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
větších	veliký	k2eAgInPc2d2	veliký
druhů	druh	k1gInPc2	druh
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
hodnoty	hodnota	k1gFnPc1	hodnota
blížící	blížící	k2eAgFnPc1d1	blížící
se	se	k3xPyFc4	se
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krvi	krev	k1gFnSc6	krev
želvy	želva	k1gFnSc2	želva
tereky	terek	k1gInPc1	terek
velké	velká	k1gFnSc2	velká
(	(	kIx(	(
<g/>
Podocnemis	Podocnemis	k1gFnSc1	Podocnemis
expansa	expansa	k1gFnSc1	expansa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
objemu	objem	k1gInSc6	objem
pouhých	pouhý	k2eAgFnPc2d1	pouhá
jen	jen	k6eAd1	jen
asi	asi	k9	asi
6000	[number]	k4	6000
<g/>
–	–	k?	–
<g/>
9000	[number]	k4	9000
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
možné	možný	k2eAgNnSc1d1	možné
uvést	uvést	k5eAaPmF	uvést
rybu	ryba	k1gFnSc4	ryba
hrdložábříka	hrdložábřík	k1gMnSc2	hrdložábřík
bílého	bílý	k1gMnSc2	bílý
(	(	kIx(	(
<g/>
Monopterus	Monopterus	k1gMnSc1	Monopterus
albus	albus	k1gMnSc1	albus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
1	[number]	k4	1
mm	mm	kA	mm
<g/>
3	[number]	k4	3
12	[number]	k4	12
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
000	[number]	k4	000
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
zjevně	zjevně	k6eAd1	zjevně
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
žádným	žádný	k3yNgNnPc3	žádný
jednoduchým	jednoduchý	k2eAgNnPc3d1	jednoduché
pravidlům	pravidlo	k1gNnPc3	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Zjištění	zjištění	k1gNnSc1	zjištění
přesného	přesný	k2eAgNnSc2d1	přesné
zastoupení	zastoupení	k1gNnSc2	zastoupení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
leukocytů	leukocyt	k1gInPc2	leukocyt
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
rozpočet	rozpočet	k1gInSc1	rozpočet
leukocytů	leukocyt	k1gInPc2	leukocyt
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jen	jen	k9	jen
diferenciál	diferenciál	k1gInSc1	diferenciál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
změněné	změněný	k2eAgInPc1d1	změněný
poměry	poměr	k1gInPc1	poměr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
(	(	kIx(	(
<g/>
tyfus	tyfus	k1gInSc1	tyfus
<g/>
,	,	kIx,	,
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc4d1	významný
rozdíly	rozdíl	k1gInPc4	rozdíl
napříč	napříč	k7c7	napříč
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
rozpočet	rozpočet	k1gInSc1	rozpočet
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
statistikou	statistika	k1gFnSc7	statistika
pro	pro	k7c4	pro
kapra	kapr	k1gMnSc4	kapr
obecného	obecný	k2eAgMnSc4d1	obecný
a	a	k8xC	a
pro	pro	k7c4	pro
bažanta	bažant	k1gMnSc4	bažant
obecného	obecný	k2eAgMnSc4d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
evidentně	evidentně	k6eAd1	evidentně
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
nejvíce	nejvíce	k6eAd1	nejvíce
neutrofilů	neutrofil	k1gMnPc2	neutrofil
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nS	tvořit
až	až	k9	až
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
všech	všecek	k3xTgMnPc2	všecek
bílých	bílý	k1gMnPc2	bílý
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
leukocyty	leukocyt	k1gInPc1	leukocyt
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
buněčnými	buněčný	k2eAgInPc7d1	buněčný
typy	typ	k1gInPc7	typ
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
<g/>
.	.	kIx.	.
</s>
<s>
Leukopoéza	leukopoéza	k1gFnSc1	leukopoéza
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tvorba	tvorba	k1gFnSc1	tvorba
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
stabilního	stabilní	k2eAgInSc2d1	stabilní
počtu	počet	k1gInSc2	počet
imunitních	imunitní	k2eAgFnPc2d1	imunitní
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
součástí	součást	k1gFnSc7	součást
širšího	široký	k2eAgInSc2d2	širší
procesu	proces	k1gInSc2	proces
krvetvorby	krvetvorba	k1gFnSc2	krvetvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
živočicha	živočich	k1gMnSc4	živočich
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
zpravidla	zpravidla	k6eAd1	zpravidla
nachází	nacházet	k5eAaImIp3nS	nacházet
zásoba	zásoba	k1gFnSc1	zásoba
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
dělí	dělit	k5eAaImIp3nS	dělit
a	a	k8xC	a
část	část	k1gFnSc4	část
jejich	jejich	k3xOp3gNnSc2	jejich
potomstva	potomstvo	k1gNnSc2	potomstvo
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
(	(	kIx(	(
<g/>
diferencuje	diferencovat	k5eAaImIp3nS	diferencovat
<g/>
)	)	kIx)	)
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
buněčný	buněčný	k2eAgInSc4d1	buněčný
typ	typ	k1gInSc4	typ
krevních	krevní	k2eAgFnPc2d1	krevní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
nebo	nebo	k8xC	nebo
právě	právě	k9	právě
bílé	bílý	k2eAgFnPc4d1	bílá
krvinky	krvinka	k1gFnPc4	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejjednodušších	jednoduchý	k2eAgMnPc2d3	nejjednodušší
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
leukopoézy	leukopoéza	k1gFnSc2	leukopoéza
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
stále	stále	k6eAd1	stále
neznámé	známý	k2eNgFnSc2d1	neznámá
a	a	k8xC	a
například	například	k6eAd1	například
u	u	k7c2	u
houbovců	houbovec	k1gInPc2	houbovec
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
amébocytů	amébocyt	k1gInPc2	amébocyt
zahalen	zahalen	k2eAgInSc4d1	zahalen
nejistotou	nejistota	k1gFnSc7	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
na	na	k7c6	na
nějakém	nějaký	k3yIgNnSc6	nějaký
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
místě	místo	k1gNnSc6	místo
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
s	s	k7c7	s
výraznějším	výrazný	k2eAgNnSc7d2	výraznější
členěním	členění	k1gNnSc7	členění
na	na	k7c4	na
orgány	orgán	k1gMnPc4	orgán
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
vznik	vznik	k1gInSc4	vznik
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
:	:	kIx,	:
coelomocyty	coelomocyt	k1gInPc1	coelomocyt
kroužkovců	kroužkovec	k1gMnPc2	kroužkovec
vznikají	vznikat	k5eAaImIp3nP	vznikat
například	například	k6eAd1	například
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
ohraničené	ohraničený	k2eAgFnSc6d1	ohraničená
části	část	k1gFnSc6	část
coelomu	coelom	k1gInSc2	coelom
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
na	na	k7c6	na
hrudní	hrudní	k2eAgFnSc6d1	hrudní
a	a	k8xC	a
laterální	laterální	k2eAgFnSc6d1	laterální
straně	strana	k1gFnSc6	strana
pobřišnice	pobřišnice	k1gFnSc2	pobřišnice
v	v	k7c6	v
hrudních	hrudní	k2eAgInPc6d1	hrudní
segmentech	segment	k1gInPc6	segment
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
vznikají	vznikat	k5eAaImIp3nP	vznikat
fagocytární	fagocytární	k2eAgFnPc1d1	fagocytární
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
bílých	bílý	k2eAgNnPc6d1	bílé
tělískách	tělísko	k1gNnPc6	tělísko
za	za	k7c7	za
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
míst	místo	k1gNnPc2	místo
tvorby	tvorba	k1gFnSc2	tvorba
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
buď	buď	k8xC	buď
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
probíhá	probíhat	k5eAaImIp3nS	probíhat
leukopoéza	leukopoéza	k1gFnSc1	leukopoéza
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
lymfě	lymfa	k1gFnSc6	lymfa
nepřetržitým	přetržitý	k2eNgNnSc7d1	nepřetržité
dělením	dělení	k1gNnSc7	dělení
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
(	(	kIx(	(
<g/>
Urochordata	Urochordata	k1gFnSc1	Urochordata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bezprostřední	bezprostřední	k2eAgMnPc1d1	bezprostřední
příbuzní	příbuzný	k1gMnPc1	příbuzný
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
již	již	k6eAd1	již
jisté	jistý	k2eAgFnPc1d1	jistá
"	"	kIx"	"
<g/>
mízní	mízní	k2eAgFnPc1d1	mízní
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
"	"	kIx"	"
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
hltanu	hltan	k1gInSc2	hltan
a	a	k8xC	a
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
u	u	k7c2	u
kopinatců	kopinatec	k1gMnPc2	kopinatec
se	se	k3xPyFc4	se
však	však	k9	však
vše	všechen	k3xTgNnSc1	všechen
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
coelomu	coelom	k1gInSc2	coelom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
složitější	složitý	k2eAgFnSc1d2	složitější
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgInSc4d2	širší
repertoár	repertoár	k1gInSc4	repertoár
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
než	než	k8xS	než
jen	jen	k9	jen
fagocytární	fagocytární	k2eAgFnPc4d1	fagocytární
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
zájmu	zájem	k1gInSc2	zájem
tvorba	tvorba	k1gFnSc1	tvorba
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
(	(	kIx(	(
<g/>
leukopoéza	leukopoéza	k1gFnSc1	leukopoéza
<g/>
)	)	kIx)	)
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
především	především	k6eAd1	především
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
a	a	k8xC	a
v	v	k7c6	v
brzlíku	brzlík	k1gInSc6	brzlík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnSc6d1	podobná
situaci	situace	k1gFnSc6	situace
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
zejména	zejména	k9	zejména
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
výjimek	výjimka	k1gFnPc2	výjimka
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
část	část	k1gFnSc1	část
leukopoézy	leukopoéza	k1gFnSc2	leukopoéza
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
zrání	zrání	k1gNnSc1	zrání
B-lymfocytů	Bymfocyt	k1gInPc2	B-lymfocyt
<g/>
)	)	kIx)	)
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Fabriciově	Fabriciův	k2eAgFnSc6d1	Fabriciova
burze	burza	k1gFnSc6	burza
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
těsně	těsně	k6eAd1	těsně
svázán	svázán	k2eAgInSc1d1	svázán
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
ostatních	ostatní	k2eAgFnPc2d1	ostatní
buněčných	buněčný	k2eAgFnPc2d1	buněčná
součástí	součást	k1gFnPc2	součást
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
a	a	k8xC	a
krevní	krevní	k2eAgFnPc4d1	krevní
destičky	destička	k1gFnPc4	destička
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
krevních	krevní	k2eAgInPc2d1	krevní
elementů	element	k1gInPc2	element
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
kmenových	kmenový	k2eAgFnPc2d1	kmenová
(	(	kIx(	(
<g/>
či	či	k8xC	či
někdy	někdy	k6eAd1	někdy
spíše	spíše	k9	spíše
progenitorových	progenitorův	k2eAgFnPc2d1	progenitorův
<g/>
)	)	kIx)	)
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgFnPc2d1	označovaná
termínem	termín	k1gInSc7	termín
hematopoetická	hematopoetický	k2eAgFnSc1d1	hematopoetický
kmenová	kmenový	k2eAgFnSc1d1	kmenová
buňka	buňka	k1gFnSc1	buňka
a	a	k8xC	a
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
pluripotence	pluripotence	k1gFnSc2	pluripotence
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dávají	dávat	k5eAaImIp3nP	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
několika	několik	k4yIc3	několik
buněčným	buněčný	k2eAgInPc3d1	buněčný
typům	typ	k1gInPc3	typ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
dělí	dělit	k5eAaImIp3nP	dělit
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
diferencují	diferencovat	k5eAaImIp3nP	diferencovat
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
typy	typ	k1gInPc4	typ
krevních	krevní	k2eAgInPc2d1	krevní
elementů	element	k1gInPc2	element
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
krevních	krevní	k2eAgFnPc2d1	krevní
progenitorových	progenitorův	k2eAgFnPc2d1	progenitorův
buněk	buňka	k1gFnPc2	buňka
<g/>
:	:	kIx,	:
myeloidní	myeloidní	k2eAgInSc1d1	myeloidní
progenitor	progenitor	k1gInSc1	progenitor
–	–	k?	–
dává	dávat	k5eAaImIp3nS	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
především	především	k6eAd1	především
granulocytům	granulocyt	k1gInPc3	granulocyt
(	(	kIx(	(
<g/>
eosinofil	eosinofil	k1gMnSc1	eosinofil
<g/>
,	,	kIx,	,
bazofil	bazofil	k1gInSc1	bazofil
<g/>
,	,	kIx,	,
neutrofil	neutrofil	k1gMnSc1	neutrofil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
makrofágům	makrofág	k1gInPc3	makrofág
a	a	k8xC	a
dendritickým	dendritický	k2eAgFnPc3d1	dendritická
buňkám	buňka	k1gFnPc3	buňka
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
však	však	k9	však
také	také	k9	také
žírným	žírný	k2eAgFnPc3d1	žírná
buňkám	buňka	k1gFnPc3	buňka
<g/>
,	,	kIx,	,
megakaryocytům	megakaryocyt	k1gInPc3	megakaryocyt
a	a	k8xC	a
erytroblastům	erytroblast	k1gInPc3	erytroblast
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
neřadí	řadit	k5eNaImIp3nS	řadit
k	k	k7c3	k
bílým	bílý	k2eAgFnPc3d1	bílá
krvinkám	krvinka	k1gFnPc3	krvinka
<g/>
)	)	kIx)	)
lymfoidní	lymfoidní	k2eAgInSc1d1	lymfoidní
progenitor	progenitor	k1gInSc1	progenitor
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
T-lymfocyty	Tymfocyt	k1gInPc1	T-lymfocyt
<g/>
,	,	kIx,	,
B-lymfocyty	Bymfocyt	k1gInPc1	B-lymfocyt
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
jejich	jejich	k3xOp3gFnPc1	jejich
podskupiny	podskupina	k1gFnPc1	podskupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
NK	NK	kA	NK
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
T-lymfocyty	Tymfocyt	k1gInPc1	T-lymfocyt
však	však	k9	však
nedozrávají	dozrávat	k5eNaImIp3nP	dozrávat
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
putují	putovat	k5eAaImIp3nP	putovat
do	do	k7c2	do
brzlíku	brzlík	k1gInSc2	brzlík
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tzv.	tzv.	kA	tzv.
dospívají	dospívat	k5eAaImIp3nP	dospívat
<g/>
.	.	kIx.	.
</s>
<s>
B-lymfocyty	Bymfocyt	k1gInPc1	B-lymfocyt
dospívají	dospívat	k5eAaImIp3nP	dospívat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
nebo	nebo	k8xC	nebo
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Peyerových	Peyerův	k2eAgInPc6d1	Peyerův
plátech	plát	k1gInPc6	plát
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
tzv.	tzv.	kA	tzv.
specifická	specifický	k2eAgFnSc1d1	specifická
granula	granula	k1gFnSc1	granula
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nebo	nebo	k8xC	nebo
absence	absence	k1gFnSc1	absence
těchto	tento	k3xDgFnPc2	tento
granul	granula	k1gFnPc2	granula
se	se	k3xPyFc4	se
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
granulocyty	granulocyt	k1gInPc4	granulocyt
<g/>
:	:	kIx,	:
V	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
specifická	specifický	k2eAgFnSc1d1	specifická
granula	granula	k1gFnSc1	granula
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
polymorfní	polymorfní	k2eAgNnSc4d1	polymorfní
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývají	nazývat	k5eAaImIp3nP	nazývat
polymorfonukleární	polymorfonukleární	k2eAgInPc4d1	polymorfonukleární
leukocyty	leukocyt	k1gInPc4	leukocyt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
barvitelnosti	barvitelnost	k1gFnSc2	barvitelnost
těchto	tento	k3xDgFnPc2	tento
granul	granula	k1gFnPc2	granula
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Pappenheima	Pappenheimum	k1gNnSc2	Pappenheimum
nebo	nebo	k8xC	nebo
Giemsa-Romakowski	Giemsa-Romakowski	k1gNnSc2	Giemsa-Romakowski
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
eosinofilní	eosinofilní	k2eAgInPc4d1	eosinofilní
granulocyty	granulocyt	k1gInPc4	granulocyt
<g/>
,	,	kIx,	,
basofilní	basofilní	k2eAgInPc4d1	basofilní
granulocyty	granulocyt	k1gInPc4	granulocyt
a	a	k8xC	a
neutrofilní	neutrofilní	k2eAgInPc4d1	neutrofilní
granulocyty	granulocyt	k1gInPc4	granulocyt
<g/>
.	.	kIx.	.
agranulocyty	agranulocyt	k1gInPc4	agranulocyt
<g/>
:	:	kIx,	:
Neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
specifická	specifický	k2eAgFnSc1d1	specifická
granula	granula	k1gFnSc1	granula
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádná	žádný	k3yNgFnSc1	žádný
granula	granula	k1gFnSc1	granula
<g/>
.	.	kIx.	.
</s>
<s>
Azurofilní	Azurofilní	k2eAgFnSc1d1	Azurofilní
granula	granula	k1gFnSc1	granula
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
agranulocytů	agranulocyt	k1gInPc2	agranulocyt
<g/>
,	,	kIx,	,
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
funkcí	funkce	k1gFnSc7	funkce
lyzosomům	lyzosom	k1gMnPc3	lyzosom
<g/>
.	.	kIx.	.
</s>
<s>
Agranulocyty	Agranulocyt	k1gInPc1	Agranulocyt
nemají	mít	k5eNaImIp3nP	mít
segmentované	segmentovaný	k2eAgNnSc4d1	segmentované
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
cytoplazma	cytoplazma	k1gNnSc1	cytoplazma
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nS	barvit
basofilně	basofilně	k6eAd1	basofilně
(	(	kIx(	(
<g/>
zásaditými	zásaditý	k2eAgNnPc7d1	zásadité
barvivy	barvivo	k1gNnPc7	barvivo
<g/>
,	,	kIx,	,
hematoxylinem	hematoxylin	k1gInSc7	hematoxylin
modře	modro	k6eAd1	modro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
lymfocyty	lymfocyt	k1gInPc7	lymfocyt
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
NK	NK	kA	NK
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monocyty	monocyt	k1gInPc1	monocyt
a	a	k8xC	a
makrofágy	makrofág	k1gInPc1	makrofág
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
krvinkami	krvinka	k1gFnPc7	krvinka
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
svázáno	svázat	k5eAaPmNgNnS	svázat
několik	několik	k4yIc1	několik
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
poruchy	porucha	k1gFnPc1	porucha
prostudovány	prostudovat	k5eAaPmNgFnP	prostudovat
u	u	k7c2	u
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
několik	několik	k4yIc4	několik
termínů	termín	k1gInPc2	termín
<g/>
.	.	kIx.	.
</s>
<s>
Leukopenie	leukopenie	k1gFnSc1	leukopenie
popisuje	popisovat	k5eAaImIp3nS	popisovat
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
jen	jen	k9	jen
určité	určitý	k2eAgFnSc2d1	určitá
skupiny	skupina	k1gFnSc2	skupina
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
–	–	k?	–
z	z	k7c2	z
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
termíny	termín	k1gInPc1	termín
jako	jako	k8xS	jako
lymfocytopenie	lymfocytopenie	k1gFnPc1	lymfocytopenie
(	(	kIx(	(
<g/>
málo	málo	k1gNnSc1	málo
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neutropenie	neutropenie	k1gFnSc1	neutropenie
(	(	kIx(	(
<g/>
málo	málo	k1gNnSc1	málo
neutrofilů	neutrofil	k1gMnPc2	neutrofil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eosinopenie	eosinopenie	k1gFnSc1	eosinopenie
(	(	kIx(	(
<g/>
málo	málo	k1gNnSc1	málo
eosinofilů	eosinofil	k1gMnPc2	eosinofil
<g/>
)	)	kIx)	)
či	či	k8xC	či
basopenie	basopenie	k1gFnSc1	basopenie
(	(	kIx(	(
<g/>
málo	málo	k1gNnSc1	málo
basofilů	basofil	k1gMnPc2	basofil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
leukocytóza	leukocytóza	k1gFnSc1	leukocytóza
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
přítomno	přítomen	k2eAgNnSc1d1	přítomno
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
množství	množství	k1gNnSc1	množství
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
:	:	kIx,	:
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
lymfocytóza	lymfocytóza	k1gFnSc1	lymfocytóza
<g/>
,	,	kIx,	,
neutrofilie	neutrofilie	k1gFnSc1	neutrofilie
<g/>
,	,	kIx,	,
eosinofilie	eosinofilie	k1gFnSc1	eosinofilie
a	a	k8xC	a
basofilie	basofilie	k1gFnSc1	basofilie
<g/>
.	.	kIx.	.
</s>
<s>
Leukostáze	Leukostáze	k1gFnSc1	Leukostáze
je	být	k5eAaImIp3nS	být
koagulace	koagulace	k1gFnSc1	koagulace
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Leukemie	leukemie	k1gFnPc4	leukemie
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
akutních	akutní	k2eAgFnPc2d1	akutní
a	a	k8xC	a
chronických	chronický	k2eAgFnPc2d1	chronická
nádorových	nádorový	k2eAgFnPc2d1	nádorová
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
rakovinným	rakovinný	k2eAgNnSc7d1	rakovinné
bujením	bujení	k1gNnSc7	bujení
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
.	.	kIx.	.
</s>
