<s>
Zvyk	zvyk	k1gInSc1	zvyk
pít	pít	k5eAaImF	pít
čaj	čaj	k1gInSc4	čaj
o	o	k7c6	o
páté	pátá	k1gFnSc6	pátá
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
od	od	k7c2	od
Francouzů	Francouz	k1gMnPc2	Francouz
pouze	pouze	k6eAd1	pouze
převzala	převzít	k5eAaPmAgFnS	převzít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
