<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
od	od	k7c2	od
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
bronzový	bronzový	k2eAgMnSc1d1	bronzový
Bludný	bludný	k2eAgMnSc1d1	bludný
balvan	balvan	k1gMnSc1	balvan
za	za	k7c4	za
pořad	pořad	k1gInSc4	pořad
Sama	sám	k3xTgFnSc1	sám
doma	doma	k6eAd1	doma
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
soustavné	soustavný	k2eAgFnSc3d1	soustavná
propagaci	propagace	k1gFnSc3	propagace
astrologie	astrologie	k1gFnSc2	astrologie
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
jurodivosti	jurodivost	k1gFnSc2	jurodivost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
