<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
českou	český	k2eAgFnSc7d1	Česká
veřejnoprávní	veřejnoprávní	k2eAgFnSc7d1	veřejnoprávní
televizí	televize	k1gFnSc7	televize
poskytující	poskytující	k2eAgFnSc1d1	poskytující
své	svůj	k3xOyFgNnSc4	svůj
vysílání	vysílání	k1gNnSc4	vysílání
celoplošně	celoplošně	k6eAd1	celoplošně
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
rámec	rámec	k1gInSc4	rámec
jejího	její	k3xOp3gNnSc2	její
fungování	fungování	k1gNnSc2	fungování
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
plnění	plnění	k1gNnSc2	plnění
úkolů	úkol	k1gInPc2	úkol
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
financování	financování	k1gNnSc2	financování
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
médium	médium	k1gNnSc1	médium
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
má	mít	k5eAaImIp3nS	mít
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
všem	všecek	k3xTgMnPc3	všecek
občanům	občan	k1gMnPc3	občan
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc3	vzdělání
a	a	k8xC	a
zábavě	zábava	k1gFnSc3	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Kavčích	kavčí	k2eAgFnPc6d1	kavčí
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
jako	jako	k8xC	jako
právnická	právnický	k2eAgFnSc1d1	právnická
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
majetek	majetek	k1gInSc4	majetek
převedený	převedený	k2eAgInSc4d1	převedený
z	z	k7c2	z
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
nejprve	nejprve	k6eAd1	nejprve
provozovala	provozovat	k5eAaImAgFnS	provozovat
vysílání	vysílání	k1gNnSc4	vysílání
na	na	k7c6	na
programech	program	k1gInPc6	program
ČTV	ČTV	kA	ČTV
a	a	k8xC	a
OK3	OK3	k1gFnSc1	OK3
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
část	část	k1gFnSc4	část
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
pořady	pořad	k1gInPc4	pořad
pro	pro	k7c4	pro
federální	federální	k2eAgInSc4d1	federální
československý	československý	k2eAgInSc4d1	československý
kanál	kanál	k1gInSc4	kanál
F1	F1	k1gFnSc2	F1
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
zánikem	zánik	k1gInSc7	zánik
federálního	federální	k2eAgInSc2d1	federální
programového	programový	k2eAgInSc2d1	programový
okruhu	okruh	k1gInSc2	okruh
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
televizních	televizní	k2eAgInPc2d1	televizní
kanálů	kanál	k1gInPc2	kanál
<g/>
:	:	kIx,	:
na	na	k7c6	na
frekvencích	frekvence	k1gFnPc6	frekvence
dřívějšího	dřívější	k2eAgMnSc2d1	dřívější
ČTV	ČTV	kA	ČTV
začal	začít	k5eAaPmAgInS	začít
nově	nově	k6eAd1	nově
vysílat	vysílat	k5eAaImF	vysílat
program	program	k1gInSc4	program
ČT	ČT	kA	ČT
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
na	na	k7c6	na
frekvencích	frekvence	k1gFnPc6	frekvence
bývalého	bývalý	k2eAgInSc2d1	bývalý
federálního	federální	k2eAgInSc2d1	federální
okruhu	okruh	k1gInSc2	okruh
F1	F1	k1gMnSc1	F1
začal	začít	k5eAaPmAgMnS	začít
vysílat	vysílat	k5eAaImF	vysílat
program	program	k1gInSc4	program
ČT2	ČT2	k1gFnSc2	ČT2
a	a	k8xC	a
program	program	k1gInSc1	program
OK3	OK3	k1gFnSc2	OK3
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c6	na
ČT	ČT	kA	ČT
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
kanál	kanál	k1gInSc1	kanál
ČT3	ČT3	k1gFnSc2	ČT3
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
zrušen	zrušen	k2eAgMnSc1d1	zrušen
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
frekvence	frekvence	k1gFnPc4	frekvence
byl	být	k5eAaImAgInS	být
přesunut	přesunut	k2eAgInSc1d1	přesunut
program	program	k1gInSc1	program
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musel	muset	k5eAaImAgInS	muset
uvolnit	uvolnit	k5eAaPmF	uvolnit
frekvenci	frekvence	k1gFnSc4	frekvence
bývalého	bývalý	k2eAgInSc2d1	bývalý
federálního	federální	k2eAgInSc2d1	federální
okruhu	okruh	k1gInSc2	okruh
pro	pro	k7c4	pro
soukromou	soukromý	k2eAgFnSc4d1	soukromá
televizi	televize	k1gFnSc4	televize
Nova	novum	k1gNnSc2	novum
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc4d1	následující
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
vysílala	vysílat	k5eAaImAgFnS	vysílat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
okruzích	okruh	k1gInPc6	okruh
–	–	k?	–
ČT1	ČT1	k1gFnPc1	ČT1
a	a	k8xC	a
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
programové	programový	k2eAgFnSc2d1	programová
nabídky	nabídka	k1gFnSc2	nabídka
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
ve	v	k7c4	v
13	[number]	k4	13
hodin	hodina	k1gFnPc2	hodina
spuštěno	spuštěn	k2eAgNnSc4d1	spuštěno
vysílání	vysílání	k1gNnSc4	vysílání
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
kanálu	kanál	k1gInSc2	kanál
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozšíření	rozšíření	k1gNnSc3	rozšíření
<g/>
,	,	kIx,	,
když	když	k8xS	když
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
v	v	k7c4	v
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
začal	začít	k5eAaPmAgMnS	začít
vysílat	vysílat	k5eAaImF	vysílat
sportovní	sportovní	k2eAgInSc4d1	sportovní
kanál	kanál	k1gInSc4	kanál
ČT4	ČT4	k1gFnSc1	ČT4
Sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
ČT	ČT	kA	ČT
sport	sport	k1gInSc4	sport
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
dlouho	dlouho	k6eAd1	dlouho
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
dětský	dětský	k2eAgInSc1d1	dětský
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
kanál	kanál	k1gInSc1	kanál
ČT	ČT	kA	ČT
:	:	kIx,	:
<g/>
D	D	kA	D
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
i	i	k9	i
kanál	kanál	k1gInSc1	kanál
ČT	ČT	kA	ČT
art	art	k?	art
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
kulturní	kulturní	k2eAgInPc4d1	kulturní
pořady	pořad	k1gInPc4	pořad
<g/>
.	.	kIx.	.
</s>
<s>
ČT	ČT	kA	ČT
:	:	kIx,	:
<g/>
D	D	kA	D
a	a	k8xC	a
ČT	ČT	kA	ČT
art	art	k?	art
od	od	k7c2	od
spuštění	spuštění	k1gNnSc2	spuštění
sdílejí	sdílet	k5eAaImIp3nP	sdílet
společnou	společný	k2eAgFnSc4d1	společná
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dětský	dětský	k2eAgInSc1d1	dětský
kanál	kanál	k1gInSc1	kanál
vysílá	vysílat	k5eAaImIp3nS	vysílat
od	od	k7c2	od
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
umělecký	umělecký	k2eAgInSc4d1	umělecký
kanál	kanál	k1gInSc4	kanál
ve	v	k7c6	v
zbylém	zbylý	k2eAgInSc6d1	zbylý
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
provozuje	provozovat	k5eAaImIp3nS	provozovat
šest	šest	k4xCc4	šest
televizních	televizní	k2eAgInPc2d1	televizní
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
pozemně	pozemně	k6eAd1	pozemně
na	na	k7c6	na
pěti	pět	k4xCc6	pět
digitálních	digitální	k2eAgFnPc6d1	digitální
frekvencích	frekvence	k1gFnPc6	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
plnoformátové	plnoformátový	k2eAgInPc1d1	plnoformátový
programy	program	k1gInPc1	program
(	(	kIx(	(
<g/>
ČT	ČT	kA	ČT
<g/>
1	[number]	k4	1
a	a	k8xC	a
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
čtyři	čtyři	k4xCgInPc4	čtyři
další	další	k2eAgInPc4d1	další
tematické	tematický	k2eAgInPc4d1	tematický
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc4d1	určené
užšímu	úzký	k2eAgNnSc3d2	užší
vybranému	vybraný	k2eAgNnSc3d1	vybrané
publiku	publikum	k1gNnSc3	publikum
(	(	kIx(	(
<g/>
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
art	art	k?	art
a	a	k8xC	a
ČT	ČT	kA	ČT
:	:	kIx,	:
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
programy	program	k1gInPc1	program
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
také	také	k9	také
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
satelitu	satelit	k1gInSc2	satelit
<g/>
,	,	kIx,	,
kabelových	kabelový	k2eAgFnPc2d1	kabelová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
IPTV	IPTV	kA	IPTV
<g/>
.	.	kIx.	.
</s>
<s>
Pořady	pořad	k1gInPc4	pořad
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
iVysílání	iVysílání	k1gNnSc2	iVysílání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc2	televize
divákům	divák	k1gMnPc3	divák
nabízí	nabízet	k5eAaImIp3nS	nabízet
živé	živý	k2eAgNnSc1d1	živé
vysílání	vysílání	k1gNnSc1	vysílání
vybraných	vybraný	k2eAgInPc2d1	vybraný
programů	program	k1gInPc2	program
a	a	k8xC	a
sledování	sledování	k1gNnSc2	sledování
pořadů	pořad	k1gInPc2	pořad
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
je	být	k5eAaImIp3nS	být
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
volený	volený	k2eAgInSc4d1	volený
Radou	rada	k1gFnSc7	rada
ČT	ČT	kA	ČT
na	na	k7c4	na
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
byl	být	k5eAaImAgMnS	být
Ivo	Ivo	k1gMnSc1	Ivo
Mathé	Mathý	k2eAgFnSc2d1	Mathý
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
management	management	k1gInSc1	management
dále	daleko	k6eAd2	daleko
tvoří	tvořit	k5eAaImIp3nS	tvořit
finanční	finanční	k2eAgMnSc1d1	finanční
a	a	k8xC	a
provozní	provozní	k2eAgMnSc1d1	provozní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
vývoje	vývoj	k1gInSc2	vývoj
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
programových	programový	k2eAgInPc2d1	programový
formátů	formát	k1gInPc2	formát
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
marketingu	marketing	k1gInSc2	marketing
a	a	k8xC	a
ředitelé	ředitel	k1gMnPc1	ředitel
televizních	televizní	k2eAgNnPc2d1	televizní
studií	studio	k1gNnPc2	studio
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
financování	financování	k1gNnSc1	financování
jsou	být	k5eAaImIp3nP	být
televizní	televizní	k2eAgInPc1d1	televizní
poplatky	poplatek	k1gInPc1	poplatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
%	%	kIx~	%
celkových	celkový	k2eAgInPc2d1	celkový
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
televizního	televizní	k2eAgInSc2d1	televizní
poplatku	poplatek	k1gInSc2	poplatek
činí	činit	k5eAaImIp3nS	činit
135	[number]	k4	135
Kč	Kč	kA	Kč
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
(	(	kIx(	(
<g/>
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
75	[number]	k4	75
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
100	[number]	k4	100
Kč	Kč	kA	Kč
a	a	k8xC	a
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
120	[number]	k4	120
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatelé	podnikatel	k1gMnPc1	podnikatel
tento	tento	k3xDgInSc4	tento
poplatek	poplatek	k1gInSc4	poplatek
odvádějí	odvádět	k5eAaImIp3nP	odvádět
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
televizní	televizní	k2eAgInSc4d1	televizní
přijímač	přijímač	k1gInSc4	přijímač
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
výnos	výnos	k1gInSc1	výnos
poplatků	poplatek	k1gInPc2	poplatek
do	do	k7c2	do
rozpočtu	rozpočet	k1gInSc2	rozpočet
ČT	ČT	kA	ČT
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
5,7	[number]	k4	5,7
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
výnosy	výnos	k1gInPc7	výnos
jsou	být	k5eAaImIp3nP	být
tržby	tržba	k1gFnSc2	tržba
za	za	k7c4	za
vlastní	vlastní	k2eAgInPc4d1	vlastní
výkony	výkon	k1gInPc4	výkon
a	a	k8xC	a
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
vysílání	vysílání	k1gNnSc4	vysílání
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
sponzoringu	sponzoring	k1gInSc2	sponzoring
<g/>
,	,	kIx,	,
prodej	prodej	k1gInSc4	prodej
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
pořadům	pořad	k1gInPc3	pořad
<g/>
,	,	kIx,	,
vydavatelskou	vydavatelský	k2eAgFnSc4d1	vydavatelská
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnPc1d1	televizní
a	a	k8xC	a
filmové	filmový	k2eAgFnPc1d1	filmová
služby	služba	k1gFnPc1	služba
poskytované	poskytovaný	k2eAgFnPc1d1	poskytovaná
externím	externí	k2eAgMnPc3d1	externí
zájemcům	zájemce	k1gMnPc3	zájemce
<g/>
,	,	kIx,	,
pronájem	pronájem	k1gInSc1	pronájem
volných	volný	k2eAgFnPc2d1	volná
prostor	prostora	k1gFnPc2	prostora
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podnikatelskou	podnikatelský	k2eAgFnSc4d1	podnikatelská
činnost	činnost	k1gFnSc4	činnost
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nP	muset
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
předmětem	předmět	k1gInSc7	předmět
činnosti	činnost	k1gFnSc2	činnost
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
současně	současně	k6eAd1	současně
nesmí	smět	k5eNaImIp3nS	smět
ohrozit	ohrozit	k5eAaPmF	ohrozit
její	její	k3xOp3gNnSc4	její
poslání	poslání	k1gNnSc4	poslání
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
dále	daleko	k6eAd2	daleko
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
na	na	k7c6	na
programech	program	k1gInPc6	program
ČT2	ČT2	k1gFnSc1	ČT2
a	a	k8xC	a
ČT	ČT	kA	ČT
sport	sport	k1gInSc1	sport
nesmí	smět	k5eNaImIp3nS	smět
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
0,5	[number]	k4	0,5
%	%	kIx~	%
denního	denní	k2eAgInSc2d1	denní
vysílacího	vysílací	k2eAgInSc2d1	vysílací
času	čas	k1gInSc2	čas
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Výnosy	výnos	k1gInPc1	výnos
z	z	k7c2	z
reklamy	reklama	k1gFnSc2	reklama
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
směřují	směřovat	k5eAaImIp3nP	směřovat
Státnímu	státní	k2eAgInSc3d1	státní
fondu	fond	k1gInSc3	fond
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
reklamní	reklamní	k2eAgInSc1d1	reklamní
výnos	výnos	k1gInSc1	výnos
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
vysílání	vysílání	k1gNnSc4	vysílání
pořadů	pořad	k1gInPc2	pořad
se	s	k7c7	s
sportovní	sportovní	k2eAgFnSc7d1	sportovní
tematikou	tematika	k1gFnSc7	tematika
(	(	kIx(	(
<g/>
reklamní	reklamní	k2eAgInSc1d1	reklamní
výnos	výnos	k1gInSc1	výnos
ČT	ČT	kA	ČT
sport	sport	k1gInSc1	sport
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanálech	kanál	k1gInPc6	kanál
ČT1	ČT1	k1gFnSc1	ČT1
a	a	k8xC	a
ČT24	ČT24	k1gFnSc1	ČT24
je	být	k5eAaImIp3nS	být
reklama	reklama	k1gFnSc1	reklama
zakázána	zakázán	k2eAgFnSc1d1	zakázána
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kulturních	kulturní	k2eAgInPc2d1	kulturní
a	a	k8xC	a
sportovních	sportovní	k2eAgInPc2d1	sportovní
přenosů	přenos	k1gInPc2	přenos
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
reklama	reklama	k1gFnSc1	reklama
jejich	jejich	k3xOp3gFnPc2	jejich
součástí	součást	k1gFnPc2	součást
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
podmínek	podmínka	k1gFnPc2	podmínka
vysílacích	vysílací	k2eAgNnPc2d1	vysílací
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
právo	právo	k1gNnSc1	právo
veřejnosti	veřejnost	k1gFnSc2	veřejnost
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
činnosti	činnost	k1gFnSc2	činnost
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Rada	rada	k1gFnSc1	rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Působnost	působnost	k1gFnSc1	působnost
Rady	rada	k1gFnSc2	rada
definuje	definovat	k5eAaBmIp3nS	definovat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
jmenování	jmenování	k1gNnSc2	jmenování
a	a	k8xC	a
odvolání	odvolání	k1gNnSc2	odvolání
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
,	,	kIx,	,
zřizování	zřizování	k1gNnSc1	zřizování
televizních	televizní	k2eAgNnPc2d1	televizní
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
schvalování	schvalování	k1gNnSc1	schvalování
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
kontroly	kontrola	k1gFnSc2	kontrola
hospodaření	hospodaření	k1gNnSc2	hospodaření
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
nebo	nebo	k8xC	nebo
předkládání	předkládání	k1gNnSc2	předkládání
Výroční	výroční	k2eAgFnSc2d1	výroční
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
Výroční	výroční	k2eAgFnSc2d1	výroční
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
hospodaření	hospodaření	k1gNnSc6	hospodaření
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
zejména	zejména	k9	zejména
stanoví	stanovit	k5eAaPmIp3nS	stanovit
podmínky	podmínka	k1gFnPc4	podmínka
naplňování	naplňování	k1gNnPc2	naplňování
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
a	a	k8xC	a
definuje	definovat	k5eAaBmIp3nS	definovat
působnost	působnost	k1gFnSc4	působnost
Rady	rada	k1gFnSc2	rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
jako	jako	k8xS	jako
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
například	například	k6eAd1	například
zákony	zákon	k1gInPc1	zákon
č.	č.	k?	č.
348	[number]	k4	348
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozhlasových	rozhlasový	k2eAgInPc6d1	rozhlasový
a	a	k8xC	a
televizních	televizní	k2eAgInPc6d1	televizní
poplatcích	poplatek	k1gInPc6	poplatek
<g/>
,	,	kIx,	,
č.	č.	k?	č.
231	[number]	k4	231
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
provozování	provozování	k1gNnSc4	provozování
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
a	a	k8xC	a
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
č.	č.	k?	č.
132	[number]	k4	132
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
audiovizuální	audiovizuální	k2eAgFnSc6d1	audiovizuální
službě	služba	k1gFnSc6	služba
na	na	k7c4	na
vyžádání	vyžádání	k1gNnPc4	vyžádání
<g/>
,	,	kIx,	,
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
č.	č.	k?	č.
106	[number]	k4	106
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
svobodném	svobodný	k2eAgInSc6d1	svobodný
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
nebo	nebo	k8xC	nebo
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
121	[number]	k4	121
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
autorský	autorský	k2eAgInSc1d1	autorský
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
dokumentem	dokument	k1gInSc7	dokument
je	být	k5eAaImIp3nS	být
Kodex	kodex	k1gInSc1	kodex
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vznik	vznik	k1gInSc1	vznik
předjímá	předjímat	k5eAaImIp3nS	předjímat
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Kodex	kodex	k1gInSc1	kodex
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
zásady	zásada	k1gFnPc4	zásada
naplňování	naplňování	k1gNnPc2	naplňování
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
závazné	závazný	k2eAgNnSc1d1	závazné
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
její	její	k3xOp3gMnPc4	její
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
porušení	porušení	k1gNnSc1	porušení
Kodexu	kodex	k1gInSc2	kodex
kvalifikuje	kvalifikovat	k5eAaBmIp3nS	kvalifikovat
jako	jako	k9	jako
porušení	porušení	k1gNnSc4	porušení
pracovní	pracovní	k2eAgFnSc2d1	pracovní
kázně	kázeň	k1gFnSc2	kázeň
podle	podle	k7c2	podle
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
právního	právní	k2eAgInSc2d1	právní
předpisu	předpis	k1gInSc2	předpis
<g/>
,	,	kIx,	,
zákoníku	zákoník	k1gInSc2	zákoník
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
prostoru	prostor	k1gInSc2	prostor
svobody	svoboda	k1gFnSc2	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
může	moct	k5eAaImIp3nS	moct
vyrůstat	vyrůstat	k5eAaImF	vyrůstat
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
kritickou	kritický	k2eAgFnSc4d1	kritická
reflexi	reflexe	k1gFnSc4	reflexe
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
vzdělává	vzdělávat	k5eAaImIp3nS	vzdělávat
a	a	k8xC	a
baví	bavit	k5eAaImIp3nS	bavit
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dílu	dílo	k1gNnSc3	dílo
i	i	k8xC	i
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
formám	forma	k1gFnPc3	forma
existence	existence	k1gFnSc2	existence
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citát	citát	k1gInSc4	citát
z	z	k7c2	z
Kodexu	kodex	k1gInSc2	kodex
ČT	ČT	kA	ČT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
dokumentem	dokument	k1gInSc7	dokument
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vznik	vznik	k1gInSc1	vznik
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Statut	statut	k1gInSc1	statut
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
jej	on	k3xPp3gInSc2	on
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
Rada	rada	k1gFnSc1	rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
definuje	definovat	k5eAaBmIp3nS	definovat
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
působnost	působnost	k1gFnSc4	působnost
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
či	či	k8xC	či
televizních	televizní	k2eAgNnPc2d1	televizní
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Statut	statut	k1gInSc1	statut
rovněž	rovněž	k9	rovněž
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
způsob	způsob	k1gInSc1	způsob
jmenování	jmenování	k1gNnSc2	jmenování
a	a	k8xC	a
odvolání	odvolání	k1gNnSc2	odvolání
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
televize	televize	k1gFnSc2	televize
působí	působit	k5eAaImIp3nS	působit
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
Televizní	televizní	k2eAgNnSc1d1	televizní
studio	studio	k1gNnSc1	studio
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
a	a	k8xC	a
Televizní	televizní	k2eAgNnSc1d1	televizní
studio	studio	k1gNnSc1	studio
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgNnPc1d1	televizní
studia	studio	k1gNnPc1	studio
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
tvorbě	tvorba	k1gFnSc6	tvorba
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
asi	asi	k9	asi
každé	každý	k3xTgNnSc1	každý
po	po	k7c6	po
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
poslanců	poslanec	k1gMnPc2	poslanec
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Koudelky	Koudelka	k1gMnSc2	Koudelka
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Petra	Petr	k1gMnSc2	Petr
Plevy	Pleva	k1gMnSc2	Pleva
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
zákon	zákon	k1gInSc1	zákon
nařídil	nařídit	k5eAaPmAgInS	nařídit
dohromady	dohromady	k6eAd1	dohromady
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
celostátním	celostátní	k2eAgNnSc6d1	celostátní
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
25	[number]	k4	25
minut	minuta	k1gFnPc2	minuta
regionálního	regionální	k2eAgNnSc2d1	regionální
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
studií	studie	k1gFnPc2	studie
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
jejich	jejich	k3xOp3gFnSc2	jejich
územní	územní	k2eAgFnSc2d1	územní
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
studia	studio	k1gNnSc2	studio
stojí	stát	k5eAaImIp3nS	stát
ředitel	ředitel	k1gMnSc1	ředitel
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
a	a	k8xC	a
odvolávaný	odvolávaný	k2eAgInSc1d1	odvolávaný
Radou	rada	k1gFnSc7	rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Povinnost	povinnost	k1gFnSc1	povinnost
podporovat	podporovat	k5eAaImF	podporovat
českou	český	k2eAgFnSc4d1	Česká
filmovou	filmový	k2eAgFnSc4d1	filmová
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
kulturní	kulturní	k2eAgInPc4d1	kulturní
projekty	projekt	k1gInPc4	projekt
České	český	k2eAgFnSc3d1	Česká
televizi	televize	k1gFnSc3	televize
ukládá	ukládat	k5eAaImIp3nS	ukládat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
domácí	domácí	k2eAgFnSc2d1	domácí
kinematografie	kinematografie	k1gFnSc2	kinematografie
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
představuje	představovat	k5eAaImIp3nS	představovat
největšího	veliký	k2eAgMnSc4d3	veliký
filmového	filmový	k2eAgMnSc4d1	filmový
koproducenta	koproducent	k1gMnSc4	koproducent
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
filmovými	filmový	k2eAgMnPc7d1	filmový
producenty	producent	k1gMnPc7	producent
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Filmového	filmový	k2eAgNnSc2d1	filmové
centra	centrum	k1gNnSc2	centrum
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
důležité	důležitý	k2eAgInPc4d1	důležitý
materiály	materiál	k1gInPc4	materiál
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
z	z	k7c2	z
činnosti	činnost	k1gFnSc2	činnost
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Částí	část	k1gFnSc7	část
svých	svůj	k3xOyFgFnPc2	svůj
archivních	archivní	k2eAgFnPc2d1	archivní
sbírek	sbírka	k1gFnPc2	sbírka
spadá	spadat	k5eAaImIp3nS	spadat
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
499	[number]	k4	499
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
archivnictví	archivnictví	k1gNnSc6	archivnictví
a	a	k8xC	a
spisové	spisový	k2eAgFnSc3d1	spisová
službě	služba	k1gFnSc3	služba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
specializované	specializovaný	k2eAgInPc4d1	specializovaný
archivy	archiv	k1gInPc4	archiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pečují	pečovat	k5eAaImIp3nP	pečovat
o	o	k7c4	o
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
je	být	k5eAaImIp3nS	být
akreditován	akreditovat	k5eAaBmNgInS	akreditovat
u	u	k7c2	u
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
a	a	k8xC	a
plní	plnit	k5eAaImIp3nP	plnit
příslušné	příslušný	k2eAgFnPc1d1	příslušná
zákonné	zákonný	k2eAgFnPc1d1	zákonná
povinnosti	povinnost	k1gFnPc1	povinnost
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
péče	péče	k1gFnSc1	péče
a	a	k8xC	a
zpřístupňování	zpřístupňování	k1gNnSc1	zpřístupňování
archiválií	archiválie	k1gFnPc2	archiválie
–	–	k?	–
Národního	národní	k2eAgNnSc2d1	národní
archivního	archivní	k2eAgNnSc2d1	archivní
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
zároveň	zároveň	k6eAd1	zároveň
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podporu	podpora	k1gFnSc4	podpora
všem	všecek	k3xTgInPc3	všecek
útvarům	útvar	k1gInPc3	útvar
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc4d1	nová
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
<g/>
)	)	kIx)	)
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
digitalizaci	digitalizace	k1gFnSc6	digitalizace
historických	historický	k2eAgInPc2d1	historický
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
charitativních	charitativní	k2eAgInPc2d1	charitativní
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
prospěšných	prospěšný	k2eAgFnPc2d1	prospěšná
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
osvětové	osvětový	k2eAgInPc1d1	osvětový
a	a	k8xC	a
benefiční	benefiční	k2eAgInPc1d1	benefiční
pořady	pořad	k1gInPc1	pořad
a	a	k8xC	a
bezplatné	bezplatný	k2eAgFnSc2d1	bezplatná
spotové	spotový	k2eAgFnSc2d1	spotová
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Nadací	nadace	k1gFnSc7	nadace
rozvoje	rozvoj	k1gInSc2	rozvoj
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
organizuje	organizovat	k5eAaBmIp3nS	organizovat
sbírkový	sbírkový	k2eAgInSc4d1	sbírkový
projekt	projekt	k1gInSc4	projekt
Pomozte	pomoct	k5eAaPmRp2nPwC	pomoct
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vlastním	vlastní	k2eAgInPc3d1	vlastní
charitativním	charitativní	k2eAgInPc3d1	charitativní
projektům	projekt	k1gInPc3	projekt
patří	patřit	k5eAaImIp3nS	patřit
Adventní	adventní	k2eAgInPc4d1	adventní
koncerty	koncert	k1gInPc4	koncert
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
benefičních	benefiční	k2eAgInPc2d1	benefiční
formátů	formát	k1gInPc2	formát
do	do	k7c2	do
programu	program	k1gInSc2	program
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
přenosy	přenos	k1gInPc1	přenos
a	a	k8xC	a
záznamy	záznam	k1gInPc1	záznam
z	z	k7c2	z
kulturně	kulturně	k6eAd1	kulturně
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
je	být	k5eAaImIp3nS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
nevládní	vládní	k2eNgFnSc2d1	nevládní
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
se	se	k3xPyFc4	se
v	v	k7c6	v
Kodexu	kodex	k1gInSc6	kodex
ČT	ČT	kA	ČT
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
vytvářet	vytvářet	k5eAaImF	vytvářet
a	a	k8xC	a
vysílat	vysílat	k5eAaImF	vysílat
populárně	populárně	k6eAd1	populárně
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
a	a	k8xC	a
osvětové	osvětový	k2eAgInPc4d1	osvětový
pořady	pořad	k1gInPc4	pořad
určené	určený	k2eAgInPc4d1	určený
různým	různý	k2eAgFnPc3d1	různá
věkovým	věkový	k2eAgFnPc3d1	věková
i	i	k8xC	i
zájmovým	zájmový	k2eAgFnPc3d1	zájmová
skupinám	skupina	k1gFnPc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
programových	programový	k2eAgInPc2d1	programový
typů	typ	k1gInPc2	typ
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tzv.	tzv.	kA	tzv.
pořad	pořad	k1gInSc1	pořad
přímo	přímo	k6eAd1	přímo
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
osvětový	osvětový	k2eAgMnSc1d1	osvětový
(	(	kIx(	(
<g/>
definovaný	definovaný	k2eAgMnSc1d1	definovaný
EBU	EBU	kA	EBU
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
žánrově	žánrově	k6eAd1	žánrově
bohatší	bohatý	k2eAgInPc1d2	bohatší
pořady	pořad	k1gInPc1	pořad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
prvek	prvek	k1gInSc1	prvek
přítomný	přítomný	k2eAgInSc1d1	přítomný
spíše	spíše	k9	spíše
obsahem	obsah	k1gInSc7	obsah
než	než	k8xS	než
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konkrétním	konkrétní	k2eAgFnPc3d1	konkrétní
vzdělávacím	vzdělávací	k2eAgFnPc3d1	vzdělávací
aktivitám	aktivita	k1gFnPc3	aktivita
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
patří	patřit	k5eAaImIp3nS	patřit
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	s	k7c7	s
základními	základní	k2eAgFnPc7d1	základní
<g/>
,	,	kIx,	,
středními	střední	k2eAgFnPc7d1	střední
školami	škola	k1gFnPc7	škola
a	a	k8xC	a
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
poskytování	poskytování	k1gNnSc4	poskytování
metodických	metodický	k2eAgInPc2d1	metodický
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgInPc2d1	pracovní
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
doplňkem	doplněk	k1gInSc7	doplněk
k	k	k7c3	k
audiovizuálním	audiovizuální	k2eAgFnPc3d1	audiovizuální
ukázkám	ukázka	k1gFnPc3	ukázka
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysokoškolské	vysokoškolský	k2eAgMnPc4d1	vysokoškolský
studenty	student	k1gMnPc4	student
organizuje	organizovat	k5eAaBmIp3nS	organizovat
například	například	k6eAd1	například
projekt	projekt	k1gInSc1	projekt
ČT	ČT	kA	ČT
start	start	k1gInSc1	start
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
organizacemi	organizace	k1gFnPc7	organizace
zaměřenými	zaměřený	k2eAgFnPc7d1	zaměřená
na	na	k7c4	na
televizní	televizní	k2eAgFnSc4d1	televizní
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
vysílání	vysílání	k1gNnSc1	vysílání
a	a	k8xC	a
nová	nový	k2eAgNnPc1d1	nové
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
vysílací	vysílací	k2eAgFnSc2d1	vysílací
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
profesní	profesní	k2eAgFnSc7d1	profesní
asociací	asociace	k1gFnSc7	asociace
národních	národní	k2eAgInPc2d1	národní
vysílatelů	vysílatel	k1gMnPc2	vysílatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
přes	přes	k7c4	přes
70	[number]	k4	70
aktivních	aktivní	k2eAgInPc2d1	aktivní
členů	člen	k1gInPc2	člen
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc2	padesát
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
přidružené	přidružený	k2eAgInPc4d1	přidružený
členy	člen	k1gInPc4	člen
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
zástupcem	zástupce	k1gMnSc7	zástupce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zasedl	zasednout	k5eAaPmAgMnS	zasednout
ve	v	k7c6	v
výkonném	výkonný	k2eAgInSc6d1	výkonný
výboru	výbor	k1gInSc6	výbor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
vedením	vedení	k1gNnSc7	vedení
Evropské	evropský	k2eAgFnSc2d1	Evropská
vysílací	vysílací	k2eAgFnSc2d1	vysílací
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Ivo	Ivo	k1gMnSc1	Ivo
Mathé	Mathý	k2eAgMnPc4d1	Mathý
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
až	až	k9	až
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
stal	stát	k5eAaPmAgMnS	stát
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
mandát	mandát	k1gInSc1	mandát
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Krize	krize	k1gFnSc2	krize
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2000	[number]	k4	2000
a	a	k8xC	a
2001	[number]	k4	2001
se	se	k3xPyFc4	se
budova	budova	k1gFnSc1	budova
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
stala	stát	k5eAaPmAgFnS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
tzv.	tzv.	kA	tzv.
televizní	televizní	k2eAgFnSc2d1	televizní
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
redakce	redakce	k1gFnSc1	redakce
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
vzbouřila	vzbouřit	k5eAaPmAgFnS	vzbouřit
proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
odvolání	odvolání	k1gNnSc2	odvolání
a	a	k8xC	a
volby	volba	k1gFnSc2	volba
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Radou	rada	k1gFnSc7	rada
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
samotnému	samotný	k2eAgMnSc3d1	samotný
řediteli	ředitel	k1gMnSc3	ředitel
Jiřímu	Jiří	k1gMnSc3	Jiří
Hodačovi	Hodač	k1gMnSc3	Hodač
a	a	k8xC	a
především	především	k9	především
proti	proti	k7c3	proti
jím	on	k3xPp3gMnSc7	on
jmenované	jmenovaný	k2eAgFnSc6d1	jmenovaná
ředitelce	ředitelka	k1gFnSc6	ředitelka
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
Janě	Jana	k1gFnSc3	Jana
Bobošíkové	Bobošíková	k1gFnSc3	Bobošíková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
vysíláno	vysílán	k2eAgNnSc4d1	vysíláno
dvojí	dvojí	k4xRgNnSc4	dvojí
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
sítích	síť	k1gFnPc6	síť
a	a	k8xC	a
vysílání	vysílání	k1gNnSc4	vysílání
protestujících	protestující	k2eAgMnPc2d1	protestující
redaktorů	redaktor	k1gMnPc2	redaktor
přes	přes	k7c4	přes
družici	družice	k1gFnSc4	družice
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
komory	komora	k1gFnPc1	komora
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
postavily	postavit	k5eAaPmAgFnP	postavit
spíše	spíše	k9	spíše
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
redaktorů	redaktor	k1gMnPc2	redaktor
přespávali	přespávat	k5eAaImAgMnP	přespávat
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
velínu	velín	k1gInSc2	velín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderátorka	moderátorka	k1gFnSc1	moderátorka
Daniela	Daniela	k1gFnSc1	Daniela
Drtinová	drtinový	k2eAgFnSc1d1	Drtinová
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zástupcem	zástupce	k1gMnSc7	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
Adamem	Adam	k1gMnSc7	Adam
Komersem	komers	k1gInSc7	komers
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
odevzdala	odevzdat	k5eAaPmAgFnS	odevzdat
Radě	rada	k1gFnSc6	rada
ČT	ČT	kA	ČT
stížnost	stížnost	k1gFnSc4	stížnost
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Podepsalo	podepsat	k5eAaPmAgNnS	podepsat
ji	on	k3xPp3gFnSc4	on
celkem	celkem	k6eAd1	celkem
24	[number]	k4	24
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
i	i	k9	i
Nora	Nora	k1gFnSc1	Nora
Fridrichová	Fridrichová	k1gFnSc1	Fridrichová
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
údajných	údajný	k2eAgInPc2d1	údajný
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
reportáží	reportáž	k1gFnPc2	reportáž
uváděli	uvádět	k5eAaImAgMnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
porušování	porušování	k1gNnSc1	porušování
objektivity	objektivita	k1gFnSc2	objektivita
a	a	k8xC	a
vyváženosti	vyváženost	k1gFnSc2	vyváženost
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kampaně	kampaň	k1gFnSc2	kampaň
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
prezidenta	prezident	k1gMnSc2	prezident
ČR	ČR	kA	ČR
a	a	k8xC	a
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovala	reagovat	k5eAaBmAgFnS	reagovat
skupina	skupina	k1gFnSc1	skupina
redaktorů	redaktor	k1gMnPc2	redaktor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
naopak	naopak	k6eAd1	naopak
odmítala	odmítat	k5eAaImAgFnS	odmítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
a	a	k8xC	a
během	během	k7c2	během
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
vystavena	vystaven	k2eAgFnSc1d1	vystavena
jakémukoli	jakýkoli	k3yIgInSc3	jakýkoli
tlaku	tlak	k1gInSc3	tlak
či	či	k8xC	či
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
.	.	kIx.	.
</s>
<s>
Kritiku	kritika	k1gFnSc4	kritika
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
ředitel	ředitel	k1gMnSc1	ředitel
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Šámal	Šámal	k1gMnSc1	Šámal
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
polemizoval	polemizovat	k5eAaImAgMnS	polemizovat
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
body	bod	k1gInPc7	bod
stížnosti	stížnost	k1gFnSc2	stížnost
a	a	k8xC	a
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
údajným	údajný	k2eAgNnSc7d1	údajné
zpolitizováním	zpolitizování	k1gNnSc7	zpolitizování
<g/>
.	.	kIx.	.
</s>
<s>
Stížnost	stížnost	k1gFnSc4	stížnost
novinářů	novinář	k1gMnPc2	novinář
mělo	mít	k5eAaImAgNnS	mít
posoudit	posoudit	k5eAaPmF	posoudit
pět	pět	k4xCc1	pět
osobností	osobnost	k1gFnPc2	osobnost
oslovených	oslovený	k2eAgMnPc2d1	oslovený
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
Petrem	Petr	k1gMnSc7	Petr
Dvořákam	Dvořákam	k1gInSc4	Dvořákam
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prorektor	prorektor	k1gMnSc1	prorektor
UK	UK	kA	UK
Michal	Michal	k1gMnSc1	Michal
Šobr	Šobr	k1gMnSc1	Šobr
<g/>
,	,	kIx,	,
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Etické	etický	k2eAgFnSc2d1	etická
komise	komise	k1gFnSc2	komise
Syndikátu	syndikát	k1gInSc2	syndikát
novinářů	novinář	k1gMnPc2	novinář
Barbora	Barbora	k1gFnSc1	Barbora
Osvaldová	Osvaldová	k1gFnSc1	Osvaldová
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
Jaromír	Jaromír	k1gMnSc1	Jaromír
Volek	Volek	k1gMnSc1	Volek
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
zpravodajové	zpravodaj	k1gMnPc1	zpravodaj
Rob	roba	k1gFnPc2	roba
Cameron	Cameron	k1gInSc4	Cameron
z	z	k7c2	z
BBC	BBC	kA	BBC
a	a	k8xC	a
Dana	Dana	k1gFnSc1	Dana
Schmidt	Schmidt	k1gMnSc1	Schmidt
z	z	k7c2	z
dánského	dánský	k2eAgInSc2d1	dánský
deníku	deník	k1gInSc2	deník
Politiken	Politikna	k1gFnPc2	Politikna
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
komise	komise	k1gFnSc1	komise
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
Michal	Michal	k1gMnSc1	Michal
Šobr	Šobr	k1gMnSc1	Šobr
a	a	k8xC	a
Dana	Dana	k1gFnSc1	Dana
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neshledala	shledat	k5eNaPmAgFnS	shledat
porušení	porušení	k1gNnSc4	porušení
objektivity	objektivita	k1gFnSc2	objektivita
a	a	k8xC	a
vyváženosti	vyváženost	k1gFnSc2	vyváženost
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
ČT	ČT	kA	ČT
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
České	český	k2eAgFnSc3d1	Česká
televizi	televize	k1gFnSc3	televize
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
kriticky	kriticky	k6eAd1	kriticky
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
ČT	ČT	kA	ČT
neplní	plnit	k5eNaImIp3nP	plnit
veřejnoprávní	veřejnoprávní	k2eAgFnSc4d1	veřejnoprávní
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
zasedají	zasedat	k5eAaImIp3nP	zasedat
minimálně	minimálně	k6eAd1	minimálně
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
se	s	k7c7	s
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
straně	strana	k1gFnSc3	strana
a	a	k8xC	a
například	například	k6eAd1	například
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
ČT	ČT	kA	ČT
je	být	k5eAaImIp3nS	být
bývalou	bývalý	k2eAgFnSc7d1	bývalá
mluvčí	mluvčí	k1gFnSc7	mluvčí
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
prezident	prezident	k1gMnSc1	prezident
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
státní	státní	k2eAgFnSc7d1	státní
institucí	instituce	k1gFnSc7	instituce
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
koncesionáři	koncesionář	k1gMnPc1	koncesionář
neměli	mít	k5eNaImAgMnP	mít
platit	platit	k5eAaImF	platit
135	[number]	k4	135
korun	koruna	k1gFnPc2	koruna
měsíčně	měsíčně	k6eAd1	měsíčně
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
provoz	provoz	k1gInSc1	provoz
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
hrazený	hrazený	k2eAgInSc1d1	hrazený
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkým	velký	k2eAgMnPc3d1	velký
kritikům	kritik	k1gMnPc3	kritik
ČT	ČT	kA	ČT
patří	patřit	k5eAaImIp3nP	patřit
senátor	senátor	k1gMnSc1	senátor
Jan	Jan	k1gMnSc1	Jan
Veleba	Veleba	k1gMnSc1	Veleba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
veřejnoprávní	veřejnoprávní	k2eAgFnSc4d1	veřejnoprávní
televizi	televize	k1gFnSc4	televize
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
protizemanovskou	protizemanovský	k2eAgFnSc4d1	protizemanovský
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
zrušit	zrušit	k5eAaPmF	zrušit
povinnost	povinnost	k1gFnSc4	povinnost
platit	platit	k5eAaImF	platit
koncesionářské	koncesionářský	k2eAgInPc4d1	koncesionářský
poplatky	poplatek	k1gInPc4	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
komentátoři	komentátor	k1gMnPc1	komentátor
vyjadřující	vyjadřující	k2eAgMnSc1d1	vyjadřující
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
web	web	k1gInSc4	web
Parlamentní	parlamentní	k2eAgInPc1d1	parlamentní
listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
pozastavují	pozastavovat	k5eAaImIp3nP	pozastavovat
nad	nad	k7c7	nad
objektivitou	objektivita	k1gFnSc7	objektivita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
příliš	příliš	k6eAd1	příliš
protiruské	protiruský	k2eAgNnSc4d1	protiruské
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
Petra	Petr	k1gMnSc2	Petr
Hájka	Hájek	k1gMnSc2	Hájek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
publikuje	publikovat	k5eAaBmIp3nS	publikovat
pro	pro	k7c4	pro
web	web	k1gInSc4	web
Parlamentní	parlamentní	k2eAgInPc1d1	parlamentní
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
také	také	k9	také
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
přítomna	přítomen	k2eAgFnSc1d1	přítomna
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
server	server	k1gInSc1	server
bs-life	bsif	k1gInSc5	bs-lif
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
lživou	lživý	k2eAgFnSc4d1	lživá
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
2000	[number]	k4	2000
ruských	ruský	k2eAgMnPc2d1	ruský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
opoziční	opoziční	k2eAgMnSc1d1	opoziční
blogger	blogger	k1gMnSc1	blogger
Ruslan	Ruslan	k1gMnSc1	Ruslan
Levijev	Levijev	k1gMnSc1	Levijev
označil	označit	k5eAaPmAgMnS	označit
údaje	údaj	k1gInPc4	údaj
za	za	k7c4	za
vymyšlené	vymyšlený	k2eAgNnSc4d1	vymyšlené
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zprávu	zpráva	k1gFnSc4	zpráva
z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
zdroje	zdroj	k1gInSc2	zdroj
o	o	k7c4	o
2000	[number]	k4	2000
padlých	padlý	k1gMnPc2	padlý
vojácích	voják	k1gMnPc6	voják
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgFnSc2d1	další
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
stanice	stanice	k1gFnSc2	stanice
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
zpravodajské	zpravodajský	k2eAgFnSc6d1	zpravodajská
stanici	stanice	k1gFnSc6	stanice
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
bs-life	bsif	k1gInSc5	bs-lif
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
později	pozdě	k6eAd2	pozdě
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
padlých	padlý	k1gMnPc2	padlý
vojáků	voják	k1gMnPc2	voják
bez	bez	k7c2	bez
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
Radě	rada	k1gFnSc6	rada
ČT	ČT	kA	ČT
doručena	doručen	k2eAgFnSc1d1	doručena
stížnost	stížnost	k1gFnSc1	stížnost
na	na	k7c4	na
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
o	o	k7c6	o
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
téměř	téměř	k6eAd1	téměř
pětisetstránkového	pětisetstránkový	k2eAgInSc2d1	pětisetstránkový
dokumentu	dokument	k1gInSc2	dokument
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
uveden	uveden	k2eAgMnSc1d1	uveden
Petr	Petr	k1gMnSc1	Petr
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
.	.	kIx.	.
</s>
<s>
Stížnost	stížnost	k1gFnSc1	stížnost
předkládá	předkládat	k5eAaImIp3nS	předkládat
výčet	výčet	k1gInSc4	výčet
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
autora	autor	k1gMnSc2	autor
dokládají	dokládat	k5eAaImIp3nP	dokládat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
porušování	porušování	k1gNnSc1	porušování
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ČT	ČT	kA	ČT
a	a	k8xC	a
Kodexu	kodex	k1gInSc2	kodex
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
ČT	ČT	kA	ČT
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
stížnost	stížnost	k1gFnSc1	stížnost
většinou	většinou	k6eAd1	většinou
hlasů	hlas	k1gInPc2	hlas
jako	jako	k8xS	jako
nedůvodnou	důvodný	k2eNgFnSc4d1	nedůvodná
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
jednání	jednání	k1gNnSc6	jednání
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádření	vyjádření	k1gNnSc4	vyjádření
Rady	rada	k1gFnSc2	rada
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
autor	autor	k1gMnSc1	autor
stížnosti	stížnost	k1gFnSc2	stížnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
následně	následně	k6eAd1	následně
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
formou	forma	k1gFnSc7	forma
otevřeného	otevřený	k2eAgInSc2d1	otevřený
dopisu	dopis	k1gInSc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rada	rada	k1gFnSc1	rada
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
stížnost	stížnost	k1gFnSc1	stížnost
ani	ani	k8xC	ani
její	její	k3xOp3gFnPc1	její
části	část	k1gFnPc1	část
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
žádnými	žádný	k3yNgInPc7	žádný
konkrétními	konkrétní	k2eAgInPc7d1	konkrétní
argumenty	argument	k1gInPc7	argument
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ukrajinsko-ruským	ukrajinskouský	k2eAgInSc7d1	ukrajinsko-ruský
konfliktem	konflikt	k1gInSc7	konflikt
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
s	s	k7c7	s
uprchlickou	uprchlický	k2eAgFnSc7d1	uprchlická
krizí	krize	k1gFnSc7	krize
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
objevovat	objevovat	k5eAaImF	objevovat
kritika	kritika	k1gFnSc1	kritika
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Kritické	kritický	k2eAgInPc1d1	kritický
články	článek	k1gInPc1	článek
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
rozsahu	rozsah	k1gInSc6	rozsah
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
například	například	k6eAd1	například
server	server	k1gInSc1	server
Parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
listy	lista	k1gFnSc2	lista
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
web	web	k1gInSc1	web
Myrotvorec	Myrotvorec	k1gInSc1	Myrotvorec
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
osobní	osobní	k2eAgNnPc4d1	osobní
data	datum	k1gNnPc4	datum
4068	[number]	k4	4068
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
akreditovaných	akreditovaný	k2eAgFnPc2d1	akreditovaná
u	u	k7c2	u
separatistické	separatistický	k2eAgFnSc2d1	separatistická
Doněcké	doněcký	k2eAgFnSc2d1	Doněcká
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
reportérů	reportér	k1gMnPc2	reportér
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Miroslava	Miroslav	k1gMnSc2	Miroslav
Karase	Karas	k1gMnSc2	Karas
a	a	k8xC	a
Lukáše	Lukáš	k1gMnSc2	Lukáš
Roganského	Roganský	k2eAgMnSc2d1	Roganský
<g/>
.	.	kIx.	.
</s>
<s>
Myrotvorec	Myrotvorec	k1gMnSc1	Myrotvorec
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tito	tento	k3xDgMnPc1	tento
novináři	novinář	k1gMnPc1	novinář
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
bojovníky	bojovník	k1gMnPc7	bojovník
teroristické	teroristický	k2eAgFnSc2d1	teroristická
organizace	organizace	k1gFnSc2	organizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
speciálního	speciální	k2eAgNnSc2d1	speciální
vysílání	vysílání	k1gNnSc2	vysílání
v	v	k7c4	v
den	den	k1gInSc4	den
amerických	americký	k2eAgFnPc2d1	americká
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
podle	podle	k7c2	podle
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
ČT	ČT	kA	ČT
porušila	porušit	k5eAaPmAgFnS	porušit
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neplnila	plnit	k5eNaImAgFnS	plnit
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
"	"	kIx"	"
<g/>
poskytovat	poskytovat	k5eAaImF	poskytovat
objektivní	objektivní	k2eAgMnSc1d1	objektivní
a	a	k8xC	a
vyvážené	vyvážený	k2eAgFnPc1d1	vyvážená
informace	informace	k1gFnPc1	informace
nezbytné	nezbytný	k2eAgFnPc1d1	nezbytná
pro	pro	k7c4	pro
svobodné	svobodný	k2eAgNnSc4d1	svobodné
vytváření	vytváření	k1gNnSc4	vytváření
názorů	názor	k1gInPc2	názor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
mělo	mít	k5eAaImAgNnS	mít
podle	podle	k7c2	podle
rady	rada	k1gFnSc2	rada
stranit	stranit	k5eAaImF	stranit
kandidátce	kandidátka	k1gFnSc3	kandidátka
Hillary	Hillara	k1gFnSc2	Hillara
Clintonové	Clintonová	k1gFnSc2	Clintonová
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
z	z	k7c2	z
několika	několik	k4yIc2	několik
hodinového	hodinový	k2eAgNnSc2d1	hodinové
speciálního	speciální	k2eAgNnSc2d1	speciální
vysílání	vysílání	k1gNnSc2	vysílání
vytkla	vytknout	k5eAaPmAgFnS	vytknout
televizi	televize	k1gFnSc3	televize
několik	několik	k4yIc4	několik
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
otevřeným	otevřený	k2eAgInSc7d1	otevřený
dopisem	dopis	k1gInSc7	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgMnSc6	který
zejména	zejména	k6eAd1	zejména
vytýkalo	vytýkat	k5eAaImAgNnS	vytýkat
Radě	rada	k1gFnSc3	rada
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehodnotila	hodnotit	k5eNaImAgFnS	hodnotit
celé	celý	k2eAgNnSc4d1	celé
12	[number]	k4	12
hodinové	hodinový	k2eAgNnSc4d1	hodinové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
od	od	k7c2	od
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
bronzový	bronzový	k2eAgMnSc1d1	bronzový
Bludný	bludný	k2eAgMnSc1d1	bludný
balvan	balvan	k1gMnSc1	balvan
za	za	k7c4	za
pořad	pořad	k1gInSc4	pořad
Sama	sám	k3xTgFnSc1	sám
doma	doma	k6eAd1	doma
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
soustavné	soustavný	k2eAgFnSc3d1	soustavná
propagaci	propagace	k1gFnSc3	propagace
astrologie	astrologie	k1gFnSc2	astrologie
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
jurodivosti	jurodivost	k1gFnSc2	jurodivost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
týkaly	týkat	k5eAaImAgFnP	týkat
i	i	k9	i
Bludné	bludný	k2eAgInPc4d1	bludný
balvany	balvan	k1gInPc4	balvan
udílené	udílený	k2eAgInPc4d1	udílený
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zlatý	zlatý	k2eAgInSc1d1	zlatý
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
družstev	družstvo	k1gNnPc2	družstvo
a	a	k8xC	a
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
byly	být	k5eAaImAgFnP	být
uděleny	udělit	k5eAaPmNgFnP	udělit
za	za	k7c4	za
pořad	pořad	k1gInSc4	pořad
Detektor	detektor	k1gInSc1	detektor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
zlatý	zlatý	k2eAgInSc1d1	zlatý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
České	český	k2eAgFnPc4d1	Česká
televizi	televize	k1gFnSc4	televize
udělen	udělit	k5eAaPmNgInS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
za	za	k7c4	za
"	"	kIx"	"
<g/>
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
pavědy	pavěda	k1gFnSc2	pavěda
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pořady	pořad	k1gInPc4	pořad
Tajný	tajný	k2eAgInSc1d1	tajný
projekt	projekt	k1gInSc1	projekt
voda	voda	k1gFnSc1	voda
–	–	k?	–
zkoumání	zkoumání	k1gNnSc2	zkoumání
nevysvětlitelných	vysvětlitelný	k2eNgInPc2d1	nevysvětlitelný
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Mayská	mayský	k2eAgNnPc1d1	mayské
proroctví	proroctví	k1gNnSc4	proroctví
a	a	k8xC	a
kruhy	kruh	k1gInPc4	kruh
v	v	k7c6	v
obilí	obilí	k1gNnSc6	obilí
<g/>
,	,	kIx,	,
Pološero	pološero	k1gNnSc1	pološero
<g/>
/	/	kIx~	/
<g/>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
UFO	UFO	kA	UFO
<g/>
,	,	kIx,	,
Pyramidy	pyramida	k1gFnPc1	pyramida
a	a	k8xC	a
Plný	plný	k2eAgInSc1d1	plný
signál	signál	k1gInSc1	signál
-	-	kIx~	-
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Kritizována	kritizován	k2eAgFnSc1d1	kritizována
je	být	k5eAaImIp3nS	být
také	také	k9	také
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČT	ČT	kA	ČT
nepropaguje	propagovat	k5eNaImIp3nS	propagovat
ateismus	ateismus	k1gInSc4	ateismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dominantně	dominantně	k6eAd1	dominantně
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
