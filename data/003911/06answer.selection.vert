<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Šplíchalová	Šplíchalová	k1gFnSc1	Šplíchalová
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1933	[number]	k4	1933
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
československého	československý	k2eAgNnSc2d1	Československé
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
