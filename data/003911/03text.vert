<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Šplíchalová	Šplíchalová	k1gFnSc1	Šplíchalová
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1933	[number]	k4	1933
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
československého	československý	k2eAgNnSc2d1	Československé
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
v	v	k7c6	v
dělnické	dělnický	k2eAgFnSc6d1	Dělnická
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
prožila	prožít	k5eAaPmAgFnS	prožít
dětství	dětství	k1gNnSc4	dětství
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
drsné	drsný	k2eAgNnSc1d1	drsné
prostředí	prostředí	k1gNnSc1	prostředí
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
rodina	rodina	k1gFnSc1	rodina
Olžiny	Olžin	k2eAgFnSc2d1	Olžina
starší	starý	k2eAgFnSc2d2	starší
sestry	sestra	k1gFnSc2	sestra
Jaroslavy	Jaroslava	k1gFnSc2	Jaroslava
(	(	kIx(	(
<g/>
matky	matka	k1gFnSc2	matka
samoživitelky	samoživitelka	k1gFnSc2	samoživitelka
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
každé	každý	k3xTgFnSc2	každý
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
samozřejmostí	samozřejmost	k1gFnPc2	samozřejmost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Olga	Olga	k1gFnSc1	Olga
své	svůj	k3xOyFgFnSc3	svůj
sestře	sestra	k1gFnSc3	sestra
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
s	s	k7c7	s
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
malé	malý	k2eAgFnPc4d1	malá
neteře	neteř	k1gFnPc4	neteř
a	a	k8xC	a
synovce	synovec	k1gMnPc4	synovec
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc1	dítě
ale	ale	k8xC	ale
také	také	k9	také
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Milíčův	Milíčův	k2eAgInSc4d1	Milíčův
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
Pitter	Pitter	k1gMnSc1	Pitter
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Šplíchalová	Šplíchalová	k1gFnSc1	Šplíchalová
tu	tu	k6eAd1	tu
trávila	trávit	k5eAaImAgFnS	trávit
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
a	a	k8xC	a
zde	zde	k6eAd1	zde
také	také	k9	také
získala	získat	k5eAaPmAgFnS	získat
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
silný	silný	k2eAgInSc4d1	silný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
život	život	k1gInSc4	život
rodiny	rodina	k1gFnSc2	rodina
velice	velice	k6eAd1	velice
skromný	skromný	k2eAgInSc1d1	skromný
<g/>
,	,	kIx,	,
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
matka	matka	k1gFnSc1	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
často	často	k6eAd1	často
filmová	filmový	k2eAgNnPc1d1	filmové
a	a	k8xC	a
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
vyučila	vyučit	k5eAaPmAgFnS	vyučit
v	v	k7c6	v
Baťově	Baťův	k2eAgFnSc6d1	Baťova
továrně	továrna	k1gFnSc6	továrna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rovněž	rovněž	k9	rovněž
později	pozdě	k6eAd2	pozdě
pracovala	pracovat	k5eAaImAgFnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
svých	svůj	k3xOyFgNnPc2	svůj
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
nadšenou	nadšený	k2eAgFnSc7d1	nadšená
divadelnicí	divadelnice	k1gFnSc7	divadelnice
a	a	k8xC	a
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
herecké	herecký	k2eAgInPc4d1	herecký
kurzy	kurz	k1gInPc4	kurz
profesorky	profesorka	k1gFnSc2	profesorka
Lydie	Lydie	k1gFnSc2	Lydie
Wegenerové	Wegenerová	k1gFnSc2	Wegenerová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
ostatními	ostatní	k2eAgMnPc7d1	ostatní
žáky	žák	k1gMnPc7	žák
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
ochotnickém	ochotnický	k2eAgNnSc6d1	ochotnické
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistujícím	existující	k2eNgNnSc6d1	neexistující
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
slupi	slup	k1gFnSc6	slup
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgNnPc2d1	různé
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
také	také	k9	také
jako	jako	k9	jako
účetní	účetní	k1gFnSc1	účetní
<g/>
,	,	kIx,	,
skladnice	skladnice	k1gFnSc1	skladnice
<g/>
,	,	kIx,	,
prodavačka	prodavačka	k1gFnSc1	prodavačka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
za	za	k7c2	za
něho	on	k3xPp3gNnSc2	on
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
doma	doma	k6eAd1	doma
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
přispěvatel	přispěvatel	k1gMnSc1	přispěvatel
do	do	k7c2	do
kulturních	kulturní	k2eAgInPc2d1	kulturní
časopisů	časopis	k1gInPc2	časopis
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
demokratizačním	demokratizační	k2eAgInSc6d1	demokratizační
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
se	se	k3xPyFc4	se
o	o	k7c4	o
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
plnohodnotných	plnohodnotný	k2eAgNnPc2d1	plnohodnotné
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
pracovala	pracovat	k5eAaImAgFnS	pracovat
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
jako	jako	k8xC	jako
uvaděčka	uvaděčka	k1gFnSc1	uvaděčka
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	se	k3xPyFc4	se
netajil	tajit	k5eNaImAgMnS	tajit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
povah	povaha	k1gFnPc2	povaha
a	a	k8xC	a
rodinného	rodinný	k2eAgNnSc2d1	rodinné
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
životní	životní	k2eAgInPc4d1	životní
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
krize	krize	k1gFnSc1	krize
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
Olga	Olga	k1gFnSc1	Olga
mnoho	mnoho	k6eAd1	mnoho
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
Oceňoval	oceňovat	k5eAaImAgMnS	oceňovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
zdánlivě	zdánlivě	k6eAd1	zdánlivě
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
zorientovala	zorientovat	k5eAaPmAgFnS	zorientovat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
intelektuálním	intelektuální	k2eAgNnSc6d1	intelektuální
prostředí	prostředí	k1gNnSc6	prostředí
přelomu	přelom	k1gInSc2	přelom
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
pozornou	pozorný	k2eAgFnSc7d1	pozorná
první	první	k4xOgFnSc7	první
čtenářkou	čtenářka	k1gFnSc7	čtenářka
a	a	k8xC	a
kritičkou	kritička	k1gFnSc7	kritička
jeho	jeho	k3xOp3gInPc2	jeho
esejistických	esejistický	k2eAgInPc2d1	esejistický
i	i	k8xC	i
dramatických	dramatický	k2eAgNnPc2d1	dramatické
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
platnou	platný	k2eAgFnSc7d1	platná
oporou	opora	k1gFnSc7	opora
a	a	k8xC	a
spolupracovnicí	spolupracovnice	k1gFnSc7	spolupracovnice
v	v	k7c6	v
těžkých	těžký	k2eAgInPc6d1	těžký
letech	let	k1gInPc6	let
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
a	a	k8xC	a
spolehlivou	spolehlivý	k2eAgFnSc7d1	spolehlivá
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
Havlovi	Havlův	k2eAgMnPc1d1	Havlův
zakoupili	zakoupit	k5eAaPmAgMnP	zakoupit
venkovskou	venkovský	k2eAgFnSc4d1	venkovská
usedlost	usedlost	k1gFnSc4	usedlost
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
zvané	zvaný	k2eAgFnSc2d1	zvaná
Hrádeček	hrádeček	k1gInSc4	hrádeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
nejen	nejen	k6eAd1	nejen
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
možnost	možnost	k1gFnSc4	možnost
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
některé	některý	k3yIgInPc4	některý
kontakty	kontakt	k1gInPc4	kontakt
a	a	k8xC	a
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
manželé	manžel	k1gMnPc1	manžel
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
chalupu	chalupa	k1gFnSc4	chalupa
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
až	až	k9	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
houbařka	houbařka	k1gFnSc1	houbařka
a	a	k8xC	a
milovnice	milovnice	k1gFnSc1	milovnice
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
–	–	k?	–
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
–	–	k?	–
Hrádeček	hrádeček	k1gInSc1	hrádeček
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ráda	rád	k2eAgFnSc1d1	ráda
zahradničila	zahradničit	k5eAaImAgFnS	zahradničit
a	a	k8xC	a
podnikala	podnikat	k5eAaImAgFnS	podnikat
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
psy	pes	k1gMnPc7	pes
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Chvíle	chvíle	k1gFnSc1	chvíle
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
odloučení	odloučení	k1gNnSc4	odloučení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
využíval	využívat	k5eAaPmAgMnS	využívat
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
,	,	kIx,	,
střídali	střídat	k5eAaImAgMnP	střídat
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
společenským	společenský	k2eAgInSc7d1	společenský
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
pohostinnými	pohostinný	k2eAgMnPc7d1	pohostinný
organizátory	organizátor	k1gMnPc7	organizátor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
armádami	armáda	k1gFnPc7	armáda
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
až	až	k9	až
do	do	k7c2	do
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
Václavu	Václav	k1gMnSc6	Václav
Havlovi	Havel	k1gMnSc3	Havel
znemožněno	znemožnit	k5eAaPmNgNnS	znemožnit
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
nesměla	smět	k5eNaImAgFnS	smět
uvádět	uvádět	k5eAaImF	uvádět
jeho	jeho	k3xOp3gFnPc4	jeho
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
postupně	postupně	k6eAd1	postupně
vytlačen	vytlačen	k2eAgMnSc1d1	vytlačen
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
veřejných	veřejný	k2eAgFnPc2d1	veřejná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
disidentů	disident	k1gMnPc2	disident
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
bojovník	bojovník	k1gMnSc1	bojovník
proti	proti	k7c3	proti
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
utužující	utužující	k2eAgFnSc3d1	utužující
totalitě	totalita	k1gFnSc3	totalita
byl	být	k5eAaImAgMnS	být
Havel	Havel	k1gMnSc1	Havel
pronásledován	pronásledovat	k5eAaImNgMnS	pronásledovat
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zadržován	zadržovat	k5eAaImNgMnS	zadržovat
<g/>
,	,	kIx,	,
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
vězněn	vězněn	k2eAgMnSc1d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
svému	svůj	k1gMnSc3	svůj
muži	muž	k1gMnSc3	muž
významnou	významný	k2eAgFnSc7d1	významná
oporou	opora	k1gFnSc7	opora
<g/>
,	,	kIx,	,
podporovala	podporovat	k5eAaImAgFnS	podporovat
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
disidentských	disidentský	k2eAgFnPc6d1	disidentská
aktivitách	aktivita	k1gFnPc6	aktivita
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odsouzení	odsouzení	k1gNnSc6	odsouzení
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
ke	k	k7c3	k
čtyř	čtyři	k4xCgMnPc2	čtyři
a	a	k8xC	a
půlletému	půlletý	k2eAgNnSc3d1	půlleté
vězení	vězení	k1gNnSc3	vězení
převzala	převzít	k5eAaPmAgFnS	převzít
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
švagrem	švagr	k1gMnSc7	švagr
Ivanem	Ivan	k1gMnSc7	Ivan
Havlem	Havel	k1gMnSc7	Havel
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
i	i	k9	i
povinnosti	povinnost	k1gFnPc4	povinnost
v	v	k7c6	v
samizdatové	samizdatový	k2eAgFnSc6d1	samizdatová
edici	edice	k1gFnSc6	edice
Expedice	expedice	k1gFnSc2	expedice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
řídil	řídit	k5eAaImAgMnS	řídit
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stíháním	stíhání	k1gNnSc7	stíhání
za	za	k7c4	za
převoz	převoz	k1gInSc4	převoz
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
tiskovin	tiskovina	k1gFnPc2	tiskovina
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
případu	případ	k1gInSc6	případ
"	"	kIx"	"
<g/>
Karavan	karavan	k1gInSc4	karavan
<g/>
"	"	kIx"	"
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
podvracení	podvracení	k1gNnSc2	podvracení
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
trestní	trestní	k2eAgNnSc1d1	trestní
stíhání	stíhání	k1gNnSc1	stíhání
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
až	až	k9	až
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
totalitního	totalitní	k2eAgInSc2d1	totalitní
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
byla	být	k5eAaImAgFnS	být
adresátkou	adresátka	k1gFnSc7	adresátka
myšlenkově	myšlenkově	k6eAd1	myšlenkově
hlubokých	hluboký	k2eAgInPc2d1	hluboký
<g/>
,	,	kIx,	,
filozoficky	filozoficky	k6eAd1	filozoficky
a	a	k8xC	a
existencionálně	existencionálně	k6eAd1	existencionálně
laděných	laděný	k2eAgInPc2d1	laděný
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
posílal	posílat	k5eAaImAgMnS	posílat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
letech	let	k1gInPc6	let
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
nejen	nejen	k6eAd1	nejen
jí	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
okruhu	okruh	k1gInSc2	okruh
filozoficky	filozoficky	k6eAd1	filozoficky
uvažujících	uvažující	k2eAgMnPc2d1	uvažující
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgFnPc7	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dopisů	dopis	k1gInPc2	dopis
promýšlel	promýšlet	k5eAaImAgMnS	promýšlet
různé	různý	k2eAgFnPc4d1	různá
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snažili	snažit	k5eAaImAgMnP	snažit
intelektuálně	intelektuálně	k6eAd1	intelektuálně
aktivovat	aktivovat	k5eAaBmF	aktivovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dopisy	dopis	k1gInPc1	dopis
Olze	Olga	k1gFnSc3	Olga
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
knih	kniha	k1gFnPc2	kniha
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vydanou	vydaný	k2eAgFnSc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
v	v	k7c6	v
Edici	edice	k1gFnSc6	edice
Expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
dále	daleko	k6eAd2	daleko
organizovala	organizovat	k5eAaBmAgFnS	organizovat
schůzky	schůzka	k1gFnSc2	schůzka
<g/>
,	,	kIx,	,
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
,	,	kIx,	,
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
činnosti	činnost	k1gFnPc4	činnost
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
podepsala	podepsat	k5eAaPmAgFnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nelehkých	lehký	k2eNgNnPc6d1	nelehké
letech	léto	k1gNnPc6	léto
životní	životní	k2eAgFnSc2d1	životní
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
,	,	kIx,	,
perzekuce	perzekuce	k1gFnSc2	perzekuce
a	a	k8xC	a
věznění	věznění	k1gNnSc4	věznění
manžela	manžel	k1gMnSc2	manžel
využívala	využívat	k5eAaPmAgFnS	využívat
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
plně	plně	k6eAd1	plně
a	a	k8xC	a
s	s	k7c7	s
chutí	chuť	k1gFnSc7	chuť
možnosti	možnost	k1gFnSc2	možnost
uniknout	uniknout	k5eAaPmF	uniknout
od	od	k7c2	od
tíhy	tíha	k1gFnSc2	tíha
všedních	všední	k2eAgInPc2d1	všední
dnů	den	k1gInPc2	den
do	do	k7c2	do
recesistických	recesistický	k2eAgInPc2d1	recesistický
podniků	podnik	k1gInPc2	podnik
přátelského	přátelský	k2eAgInSc2d1	přátelský
kroužku	kroužek	k1gInSc2	kroužek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
říkal	říkat	k5eAaImAgMnS	říkat
Hrobka	hrobka	k1gFnSc1	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
tuto	tento	k3xDgFnSc4	tento
společnost	společnost	k1gFnSc4	společnost
výstižně	výstižně	k6eAd1	výstižně
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgInS	být
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
více	hodně	k6eAd2	hodně
mých	můj	k3xOp1gMnPc2	můj
přátel	přítel	k1gMnPc2	přítel
–	–	k?	–
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
venku	venku	k6eAd1	venku
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
podmínky	podmínka	k1gFnPc4	podmínka
kruté	krutý	k2eAgFnPc4d1	krutá
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jako	jako	k9	jako
určitý	určitý	k2eAgInSc4d1	určitý
způsob	způsob	k1gInSc4	způsob
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
,	,	kIx,	,
<g/>
Hrobka	hrobka	k1gFnSc1	hrobka
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
veselé	veselý	k2eAgNnSc1d1	veselé
společenství	společenství	k1gNnSc1	společenství
manželek	manželka	k1gFnPc2	manželka
zavřených	zavřený	k2eAgMnPc2d1	zavřený
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zůstali	zůstat	k5eAaPmAgMnP	zůstat
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
organizátorek	organizátorka	k1gFnPc2	organizátorka
pestré	pestrý	k2eAgFnSc2d1	pestrá
škály	škála	k1gFnSc2	škála
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
vracela	vracet	k5eAaImAgFnS	vracet
se	se	k3xPyFc4	se
tak	tak	k9	tak
mj.	mj.	kA	mj.
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
tvůrčím	tvůrčí	k2eAgInPc3d1	tvůrčí
zájmům	zájem	k1gInPc3	zájem
z	z	k7c2	z
let	léto	k1gNnPc2	léto
ochotnického	ochotnický	k2eAgNnSc2d1	ochotnické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
životě	život	k1gInSc6	život
společenství	společenství	k1gNnPc2	společenství
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
nejen	nejen	k6eAd1	nejen
jako	jako	k8xC	jako
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
hostitelka	hostitelka	k1gFnSc1	hostitelka
kostýmovaných	kostýmovaný	k2eAgFnPc2d1	kostýmovaná
zahradních	zahradní	k2eAgFnPc2d1	zahradní
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
svých	svůj	k3xOyFgFnPc2	svůj
narozenin	narozeniny	k1gFnPc2	narozeniny
pořádala	pořádat	k5eAaImAgFnS	pořádat
na	na	k7c6	na
Hrádečku	hrádeček	k1gInSc6	hrádeček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
přinášela	přinášet	k5eAaImAgFnS	přinášet
mnoho	mnoho	k4c4	mnoho
nápadů	nápad	k1gInPc2	nápad
a	a	k8xC	a
inspirací	inspirace	k1gFnPc2	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
spoluzaložila	spoluzaložit	k5eAaPmAgFnS	spoluzaložit
samizdatový	samizdatový	k2eAgInSc4d1	samizdatový
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
videomagazín	videomagazín	k1gInSc4	videomagazín
Originální	originální	k2eAgFnSc2d1	originální
Videojournal	Videojournal	k1gFnSc2	Videojournal
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obrazově	obrazově	k6eAd1	obrazově
dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
činnost	činnost	k1gFnSc4	činnost
disentu	disent	k1gInSc2	disent
a	a	k8xC	a
otevřeně	otevřeně	k6eAd1	otevřeně
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
politické	politický	k2eAgFnSc3d1	politická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc3d1	kulturní
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
působila	působit	k5eAaImAgFnS	působit
<g/>
,	,	kIx,	,
věnovala	věnovat	k5eAaImAgFnS	věnovat
se	se	k3xPyFc4	se
především	především	k6eAd1	především
ekologickým	ekologický	k2eAgNnPc3d1	ekologické
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
iniciovala	iniciovat	k5eAaBmAgFnS	iniciovat
vznik	vznik	k1gInSc4	vznik
časopisu	časopis	k1gInSc2	časopis
O	o	k7c6	o
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
jako	jako	k9	jako
členka	členka	k1gFnSc1	členka
redakce	redakce	k1gFnSc2	redakce
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
především	především	k9	především
s	s	k7c7	s
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
a	a	k8xC	a
výrobními	výrobní	k2eAgFnPc7d1	výrobní
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
manželka	manželka	k1gFnSc1	manželka
prvního	první	k4xOgMnSc2	první
demokratického	demokratický	k2eAgMnSc2d1	demokratický
československého	československý	k2eAgMnSc2d1	československý
prezidenta	prezident	k1gMnSc2	prezident
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
intenzivně	intenzivně	k6eAd1	intenzivně
věnovala	věnovat	k5eAaPmAgFnS	věnovat
charitativním	charitativní	k2eAgFnPc3d1	charitativní
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
etapě	etapa	k1gFnSc6	etapa
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgFnSc1d1	rozvíjející
demokracie	demokracie	k1gFnSc1	demokracie
patřila	patřit	k5eAaImAgFnS	patřit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnPc3	jejich
průkopnicím	průkopnice	k1gFnPc3	průkopnice
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
založila	založit	k5eAaPmAgFnS	založit
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
z	z	k7c2	z
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
Výbor	výbor	k1gInSc1	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
projektům	projekt	k1gInPc3	projekt
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
založila	založit	k5eAaPmAgFnS	založit
Nadaci	nadace	k1gFnSc4	nadace
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
členy	člen	k1gMnPc4	člen
její	její	k3xOp3gFnSc2	její
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
Olga	Olga	k1gFnSc1	Olga
předsedala	předsedat	k5eAaImAgFnS	předsedat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
–	–	k?	–
Nadace	nadace	k1gFnSc2	nadace
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
(	(	kIx(	(
<g/>
VDV	VDV	kA	VDV
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
lidem	lid	k1gInSc7	lid
se	s	k7c7	s
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
<g/>
,	,	kIx,	,
lidem	lid	k1gInSc7	lid
opuštěným	opuštěný	k2eAgInSc7d1	opuštěný
a	a	k8xC	a
diskriminovaným	diskriminovaný	k2eAgInSc7d1	diskriminovaný
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
začlenění	začlenění	k1gNnSc6	začlenění
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
nadace	nadace	k1gFnSc2	nadace
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
brzy	brzy	k6eAd1	brzy
známá	známý	k2eAgFnSc1d1	známá
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k9	i
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pobočky	pobočka	k1gFnPc1	pobočka
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
hlavních	hlavní	k2eAgInPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
nadace	nadace	k1gFnSc2	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgNnPc4d1	vznikající
centra	centrum	k1gNnPc4	centrum
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
kombinovanými	kombinovaný	k2eAgFnPc7d1	kombinovaná
vadami	vada	k1gFnPc7	vada
a	a	k8xC	a
zjišťovala	zjišťovat	k5eAaImAgFnS	zjišťovat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
mohlo	moct	k5eAaImAgNnS	moct
ulehčit	ulehčit	k5eAaPmF	ulehčit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
vidět	vidět	k5eAaImF	vidět
mezi	mezi	k7c7	mezi
seniory	senior	k1gMnPc7	senior
a	a	k8xC	a
postiženými	postižený	k2eAgFnPc7d1	postižená
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Zajímala	zajímat	k5eAaImAgFnS	zajímat
ji	on	k3xPp3gFnSc4	on
transformace	transformace	k1gFnSc1	transformace
nemocnic	nemocnice	k1gFnPc2	nemocnice
na	na	k7c4	na
nestátní	státní	k2eNgFnPc4d1	nestátní
neziskové	ziskový	k2eNgFnPc4d1	nezisková
organizace	organizace	k1gFnPc4	organizace
a	a	k8xC	a
nabádala	nabádat	k5eAaImAgFnS	nabádat
ministry	ministr	k1gMnPc4	ministr
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
občanských	občanský	k2eAgNnPc2d1	občanské
sdružení	sdružení	k1gNnPc2	sdružení
působících	působící	k2eAgNnPc2d1	působící
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
si	se	k3xPyFc3	se
respekt	respekt	k1gInSc4	respekt
světových	světový	k2eAgMnPc2d1	světový
politiků	politik	k1gMnPc2	politik
i	i	k8xC	i
kulturních	kulturní	k2eAgFnPc2d1	kulturní
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Poznala	poznat	k5eAaPmAgFnS	poznat
se	se	k3xPyFc4	se
s	s	k7c7	s
vynikajícími	vynikající	k2eAgFnPc7d1	vynikající
osobnostmi	osobnost	k1gFnPc7	osobnost
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
spolkového	spolkový	k2eAgMnSc2d1	spolkový
prezidenta	prezident	k1gMnSc2	prezident
paní	paní	k1gFnSc2	paní
Christiane	Christian	k1gMnSc5	Christian
Herzogovou	Herzogův	k2eAgFnSc7d1	Herzogova
začaly	začít	k5eAaPmAgFnP	začít
organizovat	organizovat	k5eAaBmF	organizovat
pomoc	pomoc	k1gFnSc4	pomoc
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
trpící	trpící	k2eAgFnSc1d1	trpící
cystickou	cystický	k2eAgFnSc7d1	cystická
fibrózou	fibróza	k1gFnSc7	fibróza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pátému	pátý	k4xOgNnSc3	pátý
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
se	se	k3xPyFc4	se
Olga	Olga	k1gFnSc1	Olga
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Výbor	výbor	k1gInSc1	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
–	–	k?	–
Nadace	nadace	k1gFnSc2	nadace
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
bude	být	k5eAaImBp3nS	být
každoročně	každoročně	k6eAd1	každoročně
udělovat	udělovat	k5eAaImF	udělovat
Cenu	cena	k1gFnSc4	cena
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
osobnosti	osobnost	k1gFnSc2	osobnost
se	s	k7c7	s
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
postižením	postižení	k1gNnSc7	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
prestižní	prestižní	k2eAgFnSc1d1	prestižní
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
se	se	k3xPyFc4	se
však	však	k9	však
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
pouze	pouze	k6eAd1	pouze
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
jejího	její	k3xOp3gNnSc2	její
udělování	udělování	k1gNnSc2	udělování
–	–	k?	–
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
soška	soška	k1gFnSc1	soška
Olbrama	Olbram	k1gMnSc2	Olbram
Zoubka	Zoubek	k1gMnSc2	Zoubek
"	"	kIx"	"
<g/>
Povzbuzení	povzbuzení	k1gNnSc2	povzbuzení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
zastávala	zastávat	k5eAaImAgFnS	zastávat
práv	právo	k1gNnPc2	právo
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
projektu	projekt	k1gInSc2	projekt
Ceny	cena	k1gFnSc2	cena
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
je	být	k5eAaImIp3nS	být
propagace	propagace	k1gFnSc1	propagace
občanských	občanský	k2eAgNnPc2d1	občanské
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
,	,	kIx,	,
poskytujících	poskytující	k2eAgFnPc2d1	poskytující
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
zdravotně-sociální	zdravotněociální	k2eAgFnSc1d1	zdravotně-sociální
lidsky	lidsky	k6eAd1	lidsky
důstojným	důstojný	k2eAgInSc7d1	důstojný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
nových	nový	k2eAgFnPc2d1	nová
forem	forma	k1gFnPc2	forma
sociálních	sociální	k2eAgFnPc2d1	sociální
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nadaci	nadace	k1gFnSc4	nadace
je	být	k5eAaImIp3nS	být
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
udělování	udělování	k1gNnSc2	udělování
Ceny	cena	k1gFnSc2	cena
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
současně	současně	k6eAd1	současně
příležitostí	příležitost	k1gFnSc7	příležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
poděkovat	poděkovat	k5eAaPmF	poděkovat
významným	významný	k2eAgMnPc3d1	významný
sponzorům	sponzor	k1gMnPc3	sponzor
a	a	k8xC	a
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
udělila	udělit	k5eAaPmAgFnS	udělit
norská	norský	k2eAgFnSc1d1	norská
nadace	nadace	k1gFnSc1	nadace
Stiftelsen	Stiftelsen	k1gInSc4	Stiftelsen
Arets	Arets	k1gInSc1	Arets
Budeie	Budeie	k1gFnSc2	Budeie
Olze	Olga	k1gFnSc3	Olga
Havlové	Havlové	k2eAgFnSc4d1	Havlové
prestižní	prestižní	k2eAgFnSc4d1	prestižní
cenu	cena	k1gFnSc4	cena
Žena	žena	k1gFnSc1	žena
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
medailí	medaile	k1gFnSc7	medaile
Přemysla	Přemysl	k1gMnSc2	Přemysl
Pittra	Pittr	k1gMnSc2	Pittr
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
Ženou	žena	k1gFnSc7	žena
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
neúnavně	únavně	k6eNd1	únavně
věnovala	věnovat	k5eAaPmAgFnS	věnovat
vytváření	vytváření	k1gNnSc4	vytváření
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
ženu	žena	k1gFnSc4	žena
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
autoritou	autorita	k1gFnSc7	autorita
i	i	k8xC	i
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
její	její	k3xOp3gFnSc7	její
zásluhou	zásluha	k1gFnSc7	zásluha
přestali	přestat	k5eAaPmAgMnP	přestat
být	být	k5eAaImF	být
postižení	postižený	k1gMnPc1	postižený
"	"	kIx"	"
<g/>
neslušným	slušný	k2eNgNnSc7d1	neslušné
tématem	téma	k1gNnSc7	téma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
nemoci	nemoc	k1gFnSc6	nemoc
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
celý	celý	k2eAgInSc4d1	celý
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
stáli	stát	k5eAaImAgMnP	stát
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc7	on
mohli	moct	k5eAaImAgMnP	moct
vzdát	vzdát	k5eAaPmF	vzdát
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
položit	položit	k5eAaPmF	položit
květiny	květina	k1gFnPc4	květina
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
jižního	jižní	k2eAgNnSc2d1	jižní
křídla	křídlo	k1gNnSc2	křídlo
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
k	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
ostatkům	ostatek	k1gInPc3	ostatek
a	a	k8xC	a
podepsat	podepsat	k5eAaPmF	podepsat
se	se	k3xPyFc4	se
do	do	k7c2	do
kondolenčních	kondolenční	k2eAgInPc2d1	kondolenční
archů	arch	k1gInPc2	arch
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
hrobce	hrobka	k1gFnSc6	hrobka
Havlových	Havlová	k1gFnPc2	Havlová
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Vinohradském	vinohradský	k2eAgInSc6d1	vinohradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
jí	on	k3xPp3gFnSc7	on
byl	být	k5eAaImAgInS	být
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
Řád	řád	k1gInSc1	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
za	za	k7c4	za
vynikající	vynikající	k2eAgFnPc4d1	vynikající
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c6	o
demokracii	demokracie	k1gFnSc6	demokracie
a	a	k8xC	a
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
nese	nést	k5eAaImIp3nS	nést
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Ostravě-Porubě	Ostravě-Poruba	k1gFnSc6	Ostravě-Poruba
čestný	čestný	k2eAgInSc4d1	čestný
název	název	k1gInSc4	název
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Olze	Olga	k1gFnSc6	Olga
Havlové	Havlová	k1gFnSc2	Havlová
slavnostně	slavnostně	k6eAd1	slavnostně
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
praktická	praktický	k2eAgFnSc1d1	praktická
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Janských	janský	k2eAgFnPc6d1	Janská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
otevírala	otevírat	k5eAaImAgFnS	otevírat
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
po	po	k7c6	po
sobě	se	k3xPyFc3	se
nezanechala	zanechat	k5eNaPmAgFnS	zanechat
žádné	žádný	k3yNgInPc4	žádný
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
nenapsala	napsat	k5eNaPmAgFnS	napsat
knihu	kniha	k1gFnSc4	kniha
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
dočíst	dočíst	k5eAaPmF	dočíst
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
autorů	autor	k1gMnPc2	autor
Pavla	Pavel	k1gMnSc2	Pavel
Kosatíka	Kosatík	k1gMnSc2	Kosatík
<g/>
,	,	kIx,	,
Heleny	Helena	k1gFnSc2	Helena
Markové	Marková	k1gFnSc2	Marková
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzpomínkovém	vzpomínkový	k2eAgInSc6d1	vzpomínkový
sborníku	sborník	k1gInSc6	sborník
Síla	síla	k1gFnSc1	síla
věcnosti	věcnost	k1gFnSc2	věcnost
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
dokument	dokument	k1gInSc1	dokument
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cyklu	cyklus	k1gInSc2	cyklus
GEN	gen	kA	gen
–	–	k?	–
Galerie	galerie	k1gFnSc1	galerie
elity	elita	k1gFnSc2	elita
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
–	–	k?	–
k	k	k7c3	k
desátému	desátý	k4xOgNnSc3	desátý
výročí	výročí	k1gNnSc3	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
–	–	k?	–
dokument	dokument	k1gInSc1	dokument
Paní	paní	k1gFnSc1	paní
Olga	Olga	k1gFnSc1	Olga
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Příběhy	příběh	k1gInPc1	příběh
slavných	slavný	k2eAgMnPc2d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
režisér	režisér	k1gMnSc1	režisér
Miroslav	Miroslav	k1gMnSc1	Miroslav
Janek	Janek	k1gMnSc1	Janek
natočil	natočit	k5eAaBmAgMnS	natočit
dokument	dokument	k1gInSc4	dokument
OLGA	Olga	k1gFnSc1	Olga
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
na	na	k7c6	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
filmových	filmový	k2eAgFnPc2d1	filmová
cen	cena	k1gFnPc2	cena
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
Olze	Olga	k1gFnSc3	Olga
Havlové	Havlová	k1gFnSc2	Havlová
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
působiště	působiště	k1gNnSc2	působiště
Výboru	výbor	k1gInSc2	výbor
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
na	na	k7c6	na
Senovážném	Senovážný	k2eAgNnSc6d1	Senovážné
náměstí	náměstí	k1gNnSc6	náměstí
2	[number]	k4	2
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Olgy	Olga	k1gFnSc2	Olga
Havlové	Havlová	k1gFnSc2	Havlová
nová	nový	k2eAgFnSc1d1	nová
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
3	[number]	k4	3
na	na	k7c6	na
Žižkově-Vackově	Žižkově-Vackův	k2eAgNnSc6d1	Žižkově-Vackův
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Náš	náš	k3xOp1gInSc1	náš
záměr	záměr	k1gInSc1	záměr
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
–	–	k?	–
pomoc	pomoc	k1gFnSc4	pomoc
postiženým	postižený	k1gMnPc3	postižený
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
pomoci	pomoct	k5eAaPmF	pomoct
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Nerozdáváme	rozdávat	k5eNaImIp1nP	rozdávat
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
lidi	člověk	k1gMnPc4	člověk
–	–	k?	–
často	často	k6eAd1	často
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
–	–	k?	–
postaví	postavit	k5eAaPmIp3nS	postavit
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
pomáháme	pomáhat	k5eAaImIp1nP	pomáhat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc1	rozhovor
pro	pro	k7c4	pro
noviny	novina	k1gFnPc4	novina
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Neuznávám	uznávat	k5eNaImIp1nS	uznávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
tamti	tamten	k3xDgMnPc1	tamten
jsou	být	k5eAaImIp3nP	být
normální	normální	k2eAgMnPc1d1	normální
<g/>
,	,	kIx,	,
tito	tento	k3xDgMnPc1	tento
jsou	být	k5eAaImIp3nP	být
nenormální	normální	k2eNgMnPc1d1	nenormální
<g/>
.	.	kIx.	.
</s>
<s>
Chovám	chovat	k5eAaImIp1nS	chovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
dělat	dělat	k5eAaImF	dělat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
nač	nač	k6eAd1	nač
má	mít	k5eAaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
umí	umět	k5eAaImIp3nS	umět
dělat	dělat	k5eAaImF	dělat
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Kantůrkovou	kantůrkův	k2eAgFnSc7d1	Kantůrkova
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
nesmí	smět	k5eNaImIp3nS	smět
zabřednout	zabřednout	k5eAaPmF	zabřednout
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
sebelítosti	sebelítost	k1gFnSc2	sebelítost
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
zapomínat	zapomínat	k5eAaImF	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
hůř	zle	k6eAd2	zle
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
řádové	řádový	k2eAgFnPc1d1	řádová
sestry	sestra	k1gFnPc1	sestra
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
těžce	těžce	k6eAd1	těžce
mentálně	mentálně	k6eAd1	mentálně
a	a	k8xC	a
fyzicky	fyzicky	k6eAd1	fyzicky
postižené	postižený	k2eAgFnPc4d1	postižená
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
mám	mít	k5eAaImIp1nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesou	nést	k5eAaImIp3nP	nést
těžší	těžký	k2eAgInSc4d2	těžší
úděl	úděl	k1gInSc4	úděl
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
u	u	k7c2	u
výslechu	výslech	k1gInSc2	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
takto	takto	k6eAd1	takto
postižené	postižený	k2eAgFnPc4d1	postižená
děti	dítě	k1gFnPc4	dítě
znamená	znamenat	k5eAaImIp3nS	znamenat
vynaložit	vynaložit	k5eAaPmF	vynaložit
více	hodně	k6eAd2	hodně
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
obětavosti	obětavost	k1gFnSc2	obětavost
i	i	k8xC	i
pokory	pokora	k1gFnSc2	pokora
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
298	[number]	k4	298
<g/>
,	,	kIx,	,
Tichost	tichost	k1gFnSc1	tichost
paní	paní	k1gFnSc2	paní
Olgy	Olga	k1gFnSc2	Olga
<g/>
.	.	kIx.	.
</s>
