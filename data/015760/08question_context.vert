<s>
Kaczynski	Kaczynski	k6eAd1
začal	začít	k5eAaPmAgInS
rozesílat	rozesílat	k5eAaImF
bomby	bomba	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgFnPc1
bomby	bomba	k1gFnPc1
byly	být	k5eAaImAgFnP
velice	velice	k6eAd1
primitivní	primitivní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
další	další	k2eAgInPc1d1
útoky	útok	k1gInPc1
se	se	k3xPyFc4
odehrávaly	odehrávat	k5eAaImAgInP
často	často	k6eAd1
na	na	k7c6
školách	škola	k1gFnPc6
či	či	k8xC
u	u	k7c2
aerolinek	aerolinka	k1gFnPc2
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
zatím	zatím	k6eAd1
neznámý	známý	k2eNgMnSc1d1
útočník	útočník	k1gMnSc1
přezdívku	přezdívka	k1gFnSc4
Unabomber	Unabombra	k1gFnPc2
(	(	kIx(
<g/>
University	universita	k1gFnPc1
and	and	k?
Airline	Airlin	k1gInSc5
bomber	bombero	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>