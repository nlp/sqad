<s>
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gMnSc5
</s>
<s>
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gFnSc6
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Theodore	Theodor	k1gMnSc5
John	John	k1gMnSc1
Kaczynski	Kaczynski	k1gNnSc2
Narození	narození	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1942	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Evergreen	evergreen	k1gInSc1
Park	park	k1gInSc1
Bydliště	bydliště	k1gNnSc1
</s>
<s>
Chicago	Chicago	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
Lombard	lombard	k1gInSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
Unabomber	Unabomber	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
cabin	cabina	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
Lincoln	Lincoln	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
Evergreen	evergreen	k1gInSc1
Park	park	k1gInSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Poláci	Polák	k1gMnPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc5
</s>
<s>
Harvardova	Harvardův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
Michiganská	michiganský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
Harvard	Harvard	k1gInSc1
College	Colleg	k1gInSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
terorista	terorista	k1gMnSc1
<g/>
,	,	kIx,
anarchista	anarchista	k1gMnSc1
<g/>
,	,	kIx,
sériový	sériový	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
a	a	k8xC
ochránce	ochránce	k1gMnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Kalifornská	kalifornský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c4
Berkeley	Berkelea	k1gFnPc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
ateismus	ateismus	k1gInSc4
Rodiče	rodič	k1gMnPc1
</s>
<s>
Theodore	Theodor	k1gMnSc5
Richard	Richard	k1gMnSc1
Kaczynski	Kaczynsk	k1gFnSc2
a	a	k8xC
Wanda	Wando	k1gNnSc2
Dombek	Dombka	k1gFnPc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
David	David	k1gMnSc1
Kaczynski	Kaczynsk	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
odborný	odborný	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Theodore	Theodor	k1gMnSc5
John	John	k1gMnSc1
„	„	k?
<g/>
Ted	Ted	k1gMnSc1
<g/>
“	“	k?
Kaczynski	Kaczynsk	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
znám	znát	k5eAaImIp1nS
také	také	k9
jako	jako	k9
Unabomber	Unabomber	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
sociální	sociální	k2eAgMnSc1d1
kritik	kritik	k1gMnSc1
<g/>
,	,	kIx,
anarchista	anarchista	k1gMnSc1
a	a	k8xC
neoluddista	neoluddista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
rozesílal	rozesílat	k5eAaImAgMnS
po	po	k7c6
USA	USA	kA
podomácku	podomácku	k6eAd1
zhotovené	zhotovený	k2eAgFnPc4d1
bomby	bomba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
dohromady	dohromady	k6eAd1
zabily	zabít	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
osoby	osoba	k1gFnPc1
a	a	k8xC
23	#num#	k4
dalších	další	k2eAgFnPc2d1
zranily	zranit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
motiv	motiv	k1gInSc4
uvedl	uvést	k5eAaPmAgMnS
boj	boj	k1gInSc4
proti	proti	k7c3
technickému	technický	k2eAgInSc3d1
pokroku	pokrok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gMnSc5
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1942	#num#	k4
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
polské	polský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
Wandy	Wanda	k1gMnSc2
a	a	k8xC
Theodora	Theodor	k1gMnSc2
Richarda	Richard	k1gMnSc2
Kaczynských	Kaczynských	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaczynski	Kaczynsk	k1gInSc6
byl	být	k5eAaImAgMnS
zázračné	zázračný	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
IQ	iq	kA
mělo	mít	k5eAaImAgNnS
hodnotu	hodnota	k1gFnSc4
167	#num#	k4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
dovoleno	dovolit	k5eAaPmNgNnS
přeskočit	přeskočit	k5eAaPmF
šestou	šestý	k4xOgFnSc4
třídu	třída	k1gFnSc4
a	a	k8xC
zapsat	zapsat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
sedmé	sedmý	k4xOgFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
spolužáky	spolužák	k1gMnPc7
příliš	příliš	k6eAd1
nevycházel	vycházet	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
škole	škola	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
vysmívali	vysmívat	k5eAaImAgMnP
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
trávil	trávit	k5eAaImAgMnS
osamocený	osamocený	k2eAgMnSc1d1
<g/>
,	,	kIx,
zahloubený	zahloubený	k2eAgMnSc1d1
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
myšlenek	myšlenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
zatčení	zatčení	k1gNnSc6
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
naměřeno	naměřit	k5eAaBmNgNnS
IQ	iq	kA
136	#num#	k4
(	(	kIx(
<g/>
WAIS-R	WAIS-R	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
99	#num#	k4
<g/>
.	.	kIx.
percentil	percentit	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
)	)	kIx)
s	s	k7c7
VIQ	VIQ	kA
138	#num#	k4
a	a	k8xC
PIQ	PIQ	kA
124	#num#	k4
(	(	kIx(
<g/>
95	#num#	k4
<g/>
.	.	kIx.
percentil	percentit	k5eAaImAgInS,k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
diskrepance	diskrepance	k1gFnSc1
(	(	kIx(
<g/>
hodnota	hodnota	k1gFnSc1
v	v	k7c6
mládí	mládí	k1gNnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
geniality	genialita	k1gFnSc2
-	-	kIx~
přibližně	přibližně	k6eAd1
1	#num#	k4
ze	z	k7c2
100	#num#	k4
000	#num#	k4
a	a	k8xC
pouze	pouze	k6eAd1
99	#num#	k4
<g/>
.	.	kIx.
percentil	percentit	k5eAaImAgMnS,k5eAaPmAgMnS
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
vysvětluje	vysvětlovat	k5eAaImIp3nS
snížením	snížení	k1gNnSc7
inteligence	inteligence	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
paranoidní	paranoidní	k2eAgFnSc2d1
schizofrenie	schizofrenie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
16	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
Harvard	Harvard	k1gInSc4
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
posedlý	posedlý	k2eAgInSc1d1
matematikou	matematika	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
pokoji	pokoj	k1gInSc6
rád	rád	k6eAd1
počítal	počítat	k5eAaImAgInS
diferenciální	diferenciální	k2eAgFnPc4d1
rovnice	rovnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnohém	mnohé	k1gNnSc6
předčil	předčit	k5eAaBmAgInS,k5eAaPmAgInS
své	svůj	k3xOyFgMnPc4
spolužáky	spolužák	k1gMnPc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
dokonce	dokonce	k9
i	i	k9
svého	svůj	k3xOyFgMnSc4
profesora	profesor	k1gMnSc4
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
byl	být	k5eAaImAgMnS
logik	logik	k1gMnSc1
Willard	Willard	k1gMnSc1
Van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1959	#num#	k4
a	a	k8xC
1962	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Harvardu	Harvard	k1gInSc6
dobrovolně	dobrovolně	k6eAd1
účastnil	účastnit	k5eAaImAgMnS
několika	několik	k4yIc2
eticky	eticky	k6eAd1
pochybných	pochybný	k2eAgInPc2d1
experimentů	experiment	k1gInPc2
tajného	tajný	k2eAgInSc2d1
programu	program	k1gInSc2
MKULTRA	MKULTRA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Kaczynski	Kaczynski	k6eAd1
dostudoval	dostudovat	k5eAaPmAgInS
Harvard	Harvard	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgMnS
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
Michigan	Michigan	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
PhD	PhD	k1gFnSc2
<g/>
.	.	kIx.
v	v	k7c6
matematice	matematika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaczynského	Kaczynského	k2eAgFnSc7d1
doménou	doména	k1gFnSc7
byla	být	k5eAaImAgFnS
odnož	odnož	k1gFnSc1
komplexní	komplexní	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
geometrická	geometrický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
profesoři	profesor	k1gMnPc1
v	v	k7c6
Michiganu	Michigan	k1gInSc6
byli	být	k5eAaImAgMnP
zaujati	zaujat	k2eAgMnPc1d1
jeho	jeho	k3xOp3gInSc7
intelektem	intelekt	k1gInSc7
a	a	k8xC
motivací	motivace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
disertační	disertační	k2eAgFnSc2d1
práce	práce	k1gFnSc2
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
"	"	kIx"
<g/>
Hranice	hranice	k1gFnSc1
funkce	funkce	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
se	se	k3xPyFc4
Kaczynski	Kaczynske	k1gFnSc4
stal	stát	k5eAaPmAgMnS
odborným	odborný	k2eAgMnSc7d1
asistentem	asistent	k1gMnSc7
matematiky	matematika	k1gFnSc2
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
Berkeley	Berkelea	k1gFnSc2
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
nejmladším	mladý	k2eAgInSc7d3
v	v	k7c6
této	tento	k3xDgFnSc6
akademické	akademický	k2eAgFnSc6d1
hodnosti	hodnost	k1gFnSc6
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studentům	student	k1gMnPc3
se	se	k3xPyFc4
zdál	zdát	k5eAaImAgInS
nepříjemný	příjemný	k2eNgInSc1d1
<g/>
,	,	kIx,
během	během	k7c2
přednášek	přednáška	k1gFnPc2
často	často	k6eAd1
koktal	koktat	k5eAaImAgMnS
a	a	k8xC
mumlal	mumlat	k5eAaImAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
často	často	k6eAd1
nervózní	nervózní	k2eAgMnSc1d1
a	a	k8xC
nápady	nápad	k1gInPc1
studentů	student	k1gMnPc2
zavrhoval	zavrhovat	k5eAaImAgInS
jako	jako	k9
"	"	kIx"
<g/>
blbosti	blbost	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
analytiků	analytik	k1gMnPc2
se	se	k3xPyFc4
na	na	k7c6
pozdějších	pozdní	k2eAgInPc6d2
Kaczynského	Kaczynského	k2eAgInPc6d1
atentátech	atentát	k1gInPc6
podepisuje	podepisovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
účast	účast	k1gFnSc4
na	na	k7c6
pokusech	pokus	k1gInPc6
psychologa	psycholog	k1gMnSc2
Henryho	Henry	k1gMnSc2
Murraye	Murray	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nich	on	k3xPp3gMnPc6
studenti	student	k1gMnPc1
vedli	vést	k5eAaImAgMnP
tajné	tajný	k2eAgFnPc4d1
dlouhé	dlouhý	k2eAgFnPc4d1
debaty	debata	k1gFnPc4
o	o	k7c6
svých	svůj	k3xOyFgInPc6
postojích	postoj	k1gInPc6
a	a	k8xC
hodnotách	hodnota	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
rozhovory	rozhovor	k1gInPc4
byly	být	k5eAaImAgInP
přitom	přitom	k6eAd1
tajně	tajně	k6eAd1
nahrávány	nahráván	k2eAgInPc1d1
<g/>
,	,	kIx,
poté	poté	k6eAd1
byly	být	k5eAaImAgFnP
studentům	student	k1gMnPc3
připoutaným	připoutaný	k2eAgMnPc3d1
k	k	k7c3
židli	židle	k1gFnSc4
puštěny	puštěn	k2eAgFnPc1d1
a	a	k8xC
zesměšňovány	zesměšňován	k2eAgFnPc1d1
Murrayem	Murray	k1gInSc7
a	a	k8xC
Kaczynským	Kaczynský	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
tímto	tento	k3xDgInSc7
riskantním	riskantní	k2eAgInSc7d1
pokusem	pokus	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hrozí	hrozit	k5eAaImIp3nS
ataky	atak	k1gInPc4
schizofrenických	schizofrenický	k2eAgInPc2d1
stavů	stav	k1gInPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Kaczynski	Kaczynski	k1gNnSc4
stabilní	stabilní	k2eAgNnSc4d1
osobností	osobnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
to	ten	k3xDgNnSc1
již	již	k6eAd1
neplatilo	platit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
vysvětlení	vysvětlení	k1gNnSc2
z	z	k7c2
univerzity	univerzita	k1gFnSc2
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
v	v	k7c6
Montaně	Montana	k1gFnSc6
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1971	#num#	k4
se	se	k3xPyFc4
Kaczynski	Kaczynski	k1gNnSc2
přestěhoval	přestěhovat	k5eAaPmAgMnS
k	k	k7c3
rodičům	rodič	k1gMnPc3
do	do	k7c2
Lombardu	lombard	k1gInSc2
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgInS
do	do	k7c2
Lincolnu	Lincoln	k1gMnSc3
v	v	k7c6
Montaně	Montana	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
vedl	vést	k5eAaImAgMnS
jednoduchý	jednoduchý	k2eAgInSc4d1
život	život	k1gInSc4
za	za	k7c4
velmi	velmi	k6eAd1
málo	málo	k4c4
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
bez	bez	k7c2
elektřiny	elektřina	k1gFnSc2
a	a	k8xC
tekoucí	tekoucí	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získával	získávat	k5eAaImAgMnS
finanční	finanční	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
od	od	k7c2
své	svůj	k3xOyFgFnSc2
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
prostředky	prostředek	k1gInPc4
později	pozdě	k6eAd2
použil	použít	k5eAaPmAgInS
na	na	k7c4
zakoupení	zakoupení	k1gNnSc4
materiálů	materiál	k1gInPc2
do	do	k7c2
bomb	bomba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
pracoval	pracovat	k5eAaImAgInS
krátce	krátce	k6eAd1
s	s	k7c7
otcem	otec	k1gMnSc7
a	a	k8xC
bratrem	bratr	k1gMnSc7
v	v	k7c6
továrně	továrna	k1gFnSc6
na	na	k7c4
gumu	guma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kaczynski	Kaczynske	k1gFnSc4
chtěl	chtít	k5eAaImAgMnS
svým	svůj	k3xOyFgNnSc7
odtrhnutím	odtrhnutí	k1gNnSc7
od	od	k7c2
civilizace	civilizace	k1gFnSc2
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
moderní	moderní	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
dokáže	dokázat	k5eAaPmIp3nS
být	být	k5eAaImF
soběstačný	soběstačný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
krajiny	krajina	k1gFnSc2
však	však	k9
začal	začít	k5eAaPmAgInS
zasahovat	zasahovat	k5eAaImF
průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgMnSc6
z	z	k7c2
jeho	jeho	k3xOp3gNnPc2
oblíbených	oblíbený	k2eAgNnPc2d1
míst	místo	k1gNnPc2
v	v	k7c6
lese	les	k1gInSc6
chtěly	chtít	k5eAaImAgInP
úřady	úřad	k1gInPc1
vystavět	vystavět	k5eAaPmF
silnici	silnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
číst	číst	k5eAaImF
knihy	kniha	k1gFnPc4
o	o	k7c6
sociologii	sociologie	k1gFnSc6
a	a	k8xC
politické	politický	k2eAgFnSc3d1
filozofii	filozofie	k1gFnSc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
díla	dílo	k1gNnPc1
Jacquese	Jacques	k1gMnSc2
Ellula	Ellul	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
dospěl	dochvít	k5eAaPmAgInS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
jen	jen	k9
násilnými	násilný	k2eAgInPc7d1
útoky	útok	k1gInPc7
lze	lze	k6eAd1
upozornit	upozornit	k5eAaPmF
lidstvo	lidstvo	k1gNnSc4
na	na	k7c4
problém	problém	k1gInSc4
vyspělé	vyspělý	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bomby	bomba	k1gFnPc1
</s>
<s>
Kaczynski	Kaczynski	k6eAd1
začal	začít	k5eAaPmAgInS
rozesílat	rozesílat	k5eAaImF
bomby	bomba	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgFnPc1
bomby	bomba	k1gFnPc1
byly	být	k5eAaImAgFnP
velice	velice	k6eAd1
primitivní	primitivní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
příštích	příští	k2eAgNnPc2d1
17	#num#	k4
let	léto	k1gNnPc2
si	se	k3xPyFc3
začal	začít	k5eAaPmAgInS
počínat	počínat	k5eAaImF
obratněji	obratně	k6eAd2
a	a	k8xC
sestrojil	sestrojit	k5eAaPmAgMnS
bomby	bomba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zabily	zabít	k5eAaPmAgFnP
tři	tři	k4xCgMnPc4
lidi	člověk	k1gMnPc4
a	a	k8xC
zranily	zranit	k5eAaPmAgInP
23	#num#	k4
dalších	další	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
bomba	bomba	k1gFnSc1
byla	být	k5eAaImAgFnS
zaslána	zaslat	k5eAaPmNgFnS
poštou	pošta	k1gFnSc7
na	na	k7c6
konci	konec	k1gInSc6
května	květen	k1gInSc2
1978	#num#	k4
na	na	k7c4
Northwestern	Northwestern	k1gNnSc4
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policista	policista	k1gMnSc1
Terry	Terra	k1gFnSc2
Marker	marker	k1gInSc1
balíček	balíček	k1gInSc1
otevřel	otevřít	k5eAaPmAgInS
a	a	k8xC
ten	ten	k3xDgMnSc1
vzápětí	vzápětí	k6eAd1
explodoval	explodovat	k5eAaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marker	marker	k1gInSc1
utrpěl	utrpět	k5eAaPmAgInS
jen	jen	k9
povrchová	povrchový	k2eAgNnPc4d1
zranění	zranění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
další	další	k2eAgInPc1d1
útoky	útok	k1gInPc1
se	se	k3xPyFc4
odehrávaly	odehrávat	k5eAaImAgInP
často	často	k6eAd1
na	na	k7c6
školách	škola	k1gFnPc6
či	či	k8xC
u	u	k7c2
aerolinek	aerolinka	k1gFnPc2
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
zatím	zatím	k6eAd1
neznámý	známý	k2eNgMnSc1d1
útočník	útočník	k1gMnSc1
přezdívku	přezdívka	k1gFnSc4
Unabomber	Unabombra	k1gFnPc2
(	(	kIx(
<g/>
University	universita	k1gFnPc1
and	and	k?
Airline	Airlin	k1gInSc5
bomber	bombero	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
17	#num#	k4
let	léto	k1gNnPc2
zabil	zabít	k5eAaPmAgMnS
Unabomber	Unabomber	k1gInSc4
tři	tři	k4xCgFnPc4
osoby	osoba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majitele	majitel	k1gMnSc4
obchodu	obchod	k1gInSc2
s	s	k7c7
počítači	počítač	k1gInPc7
Hugha	Hugh	k1gMnSc2
Scruttona	Scrutton	k1gMnSc2
<g/>
,	,	kIx,
vedoucího	vedoucí	k1gMnSc2
reklamní	reklamní	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
Thomase	Thomas	k1gMnSc2
Mossera	Mosser	k1gMnSc2
a	a	k8xC
prezidenta	prezident	k1gMnSc2
lesnické	lesnický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
Gilberta	Gilbert	k1gMnSc2
Murraye	Murray	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Hledání	hledání	k1gNnSc1
a	a	k8xC
zatčení	zatčení	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1995	#num#	k4
poslal	poslat	k5eAaPmAgMnS
Unabomber	Unabomber	k1gMnSc1
FBI	FBI	kA
dopis	dopis	k1gInSc1
<g/>
,	,	kIx,
známý	známý	k2eAgInSc1d1
jako	jako	k8xC,k8xS
Unabomberův	Unabomberův	k2eAgInSc1d1
manifest	manifest	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zanechá	zanechat	k5eAaPmIp3nS
svých	svůj	k3xOyFgInPc2
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manifest	manifest	k1gInSc4
či	či	k8xC
jeho	jeho	k3xOp3gInPc4
úryvky	úryvek	k1gInPc4
otiskly	otisknout	k5eAaPmAgFnP
po	po	k7c6
dohodě	dohoda	k1gFnSc6
s	s	k7c7
FBI	FBI	kA
takové	takový	k3xDgInPc4
časopisy	časopis	k1gInPc4
a	a	k8xC
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Penthouse	Penthouse	k1gFnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
New	New	k1gMnPc2
York	York	k1gInSc4
Times	Timesa	k1gFnPc2
či	či	k8xC
The	The	k1gMnPc2
Washington	Washington	k1gInSc1
Post	posta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
FBI	FBI	kA
před	před	k7c7
uveřejněním	uveřejnění	k1gNnSc7
manifestu	manifest	k1gInSc2
prosila	prosit	k5eAaImAgFnS,k5eAaPmAgFnS
veřejnost	veřejnost	k1gFnSc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
při	při	k7c6
určování	určování	k1gNnSc6
pachatele	pachatel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospěla	dochvít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
útočník	útočník	k1gMnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Chicaga	Chicago	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
nadprůměrně	nadprůměrně	k6eAd1
inteligentní	inteligentní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manifest	manifest	k1gInSc4
si	se	k3xPyFc3
přečetl	přečíst	k5eAaPmAgMnS
i	i	k9
David	David	k1gMnSc1
Kaczynski	Kaczynsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznal	poznat	k5eAaPmAgMnS
některá	některý	k3yIgNnPc4
spojení	spojení	k1gNnPc4
a	a	k8xC
zděsil	zděsit	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
přes	přes	k7c4
17	#num#	k4
let	léto	k1gNnPc2
terorizoval	terorizovat	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
vlastní	vlastní	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
šel	jít	k5eAaImAgInS
na	na	k7c6
FBI	FBI	kA
<g/>
.	.	kIx.
</s>
<s>
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gMnSc5
byl	být	k5eAaImAgInS
zatčen	zatčen	k2eAgInSc4d1
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1996	#num#	k4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
chatrči	chatrč	k1gFnSc6
v	v	k7c6
Lincolnu	Lincoln	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Našli	najít	k5eAaPmAgMnP
rozdělané	rozdělaný	k2eAgFnPc4d1
bomby	bomba	k1gFnPc4
<g/>
,	,	kIx,
dopisy	dopis	k1gInPc1
adresované	adresovaný	k2eAgInPc1d1
FBI	FBI	kA
a	a	k8xC
článek	článek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
odpovídal	odpovídat	k5eAaImAgInS
Unabomberovu	Unabomberův	k2eAgFnSc4d1
manifestu	manifest	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Soudní	soudní	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
</s>
<s>
Kaczynského	Kaczynského	k2eAgMnPc1d1
právníci	právník	k1gMnPc1
prohlásili	prohlásit	k5eAaPmAgMnP
během	během	k7c2
soudu	soud	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gMnSc1
klient	klient	k1gMnSc1
trpí	trpět	k5eAaImIp3nS
duševní	duševní	k2eAgFnSc7d1
chorobou	choroba	k1gFnSc7
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
však	však	k9
Kaczynski	Kaczynske	k1gFnSc4
rázně	rázně	k6eAd1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudní	soudní	k2eAgInSc1d1
psychiatr	psychiatr	k1gMnSc1
konstatoval	konstatovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Kaczynski	Kaczynsk	k1gMnPc1
sice	sice	k8xC
trpí	trpět	k5eAaImIp3nP
paranoidní	paranoidní	k2eAgFnSc7d1
schizofrenií	schizofrenie	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
je	být	k5eAaImIp3nS
způsobilý	způsobilý	k2eAgInSc4d1
trestu	trest	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Technologické	technologický	k2eAgNnSc1d1
otroctví	otroctví	k1gNnSc1
Kaczynski	Kaczynsk	k1gFnSc2
vzpomíná	vzpomínat	k5eAaImIp3nS
na	na	k7c4
dva	dva	k4xCgMnPc4
vězeňské	vězeňský	k2eAgMnPc4d1
psychology	psycholog	k1gMnPc4
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
Jamese	Jamese	k1gFnSc1
Wattersona	Wattersona	k1gFnSc1
a	a	k8xC
Dr	dr	kA
<g/>
.	.	kIx.
Michaela	Michael	k1gMnSc4
Morrisona	Morrison	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
jej	on	k3xPp3gMnSc4
navštěvovali	navštěvovat	k5eAaImAgMnP
téměř	téměř	k6eAd1
každý	každý	k3xTgInSc4
den	den	k1gInSc4
po	po	k7c4
dobu	doba	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k3yNgInSc1
náznak	náznak	k1gInSc1
duševní	duševní	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
na	na	k7c6
něm	on	k3xPp3gMnSc6
nezaznamenali	zaznamenat	k5eNaPmAgMnP
a	a	k8xC
obvinili	obvinit	k5eAaPmAgMnP
soudního	soudní	k2eAgMnSc4d1
psychologa	psycholog	k1gMnSc4
z	z	k7c2
podjatosti	podjatost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1998	#num#	k4
se	se	k3xPyFc4
Kaczynski	Kaczynski	k1gNnSc2
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
sebevraždu	sebevražda	k1gFnSc4
oběšením	oběšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Kaczynski	Kaczynsk	k1gMnSc3
se	se	k3xPyFc4
dohodl	dohodnout	k5eAaPmAgMnS
s	s	k7c7
vyšetřovateli	vyšetřovatel	k1gMnPc7
a	a	k8xC
soudem	soud	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
vůči	vůči	k7c3
Unabomberovi	Unabomberův	k2eAgMnPc1d1
shovívaví	shovívavý	k2eAgMnPc1d1
a	a	k8xC
nenavrhnou	navrhnout	k5eNaPmIp3nP
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
požadoval	požadovat	k5eAaImAgMnS
státní	státní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1998	#num#	k4
byl	být	k5eAaImAgInS
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gInSc6
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
na	na	k7c4
doživotí	doživotí	k1gNnSc4
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
podmínečného	podmínečný	k2eAgNnSc2d1
propuštění	propuštění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
Kaczynski	Kaczynski	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
věznici	věznice	k1gFnSc6
ADX	ADX	kA
Florence	Florenc	k1gFnSc2
v	v	k7c6
Coloradu	Colorado	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píše	psát	k5eAaImIp3nS
odtud	odtud	k6eAd1
recenze	recenze	k1gFnSc1
na	na	k7c4
vědecké	vědecký	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
do	do	k7c2
New	New	k1gMnPc2
York	York	k1gInSc1
Times	Timesa	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
2010	#num#	k4
nechal	nechat	k5eAaPmAgMnS
vydat	vydat	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
výše	vysoce	k6eAd2
zmíněnou	zmíněný	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
Technologické	technologický	k2eAgNnSc1d1
otroctví	otroctví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
anarchističtí	anarchistický	k2eAgMnPc1d1
spisovatelé	spisovatel	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
John	John	k1gMnSc1
Zerzan	Zerzan	k1gMnSc1
či	či	k8xC
John	John	k1gMnSc1
Moore	Moor	k1gInSc5
přijali	přijmout	k5eAaPmAgMnP
Kaczynského	Kaczynského	k2eAgFnPc4d1
ideje	idea	k1gFnPc4
za	za	k7c4
své	svůj	k3xOyFgMnPc4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Nebojím	bát	k5eNaImIp1nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
bych	by	kYmCp1nS
se	se	k3xPyFc4
zdejšímu	zdejší	k2eAgInSc3d1
prostředí	prostředí	k1gNnSc6
nedokázal	dokázat	k5eNaPmAgMnS
přizpůsobit	přizpůsobit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediné	jediné	k1gNnSc1
<g/>
,	,	kIx,
čeho	co	k3yQnSc2,k3yRnSc2,k3yInSc2
se	se	k3xPyFc4
obávám	obávat	k5eAaImIp1nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jak	jak	k8xS,k8xC
roky	rok	k1gInPc4
běží	běžet	k5eAaImIp3nP
<g/>
,	,	kIx,
slábnou	slábnout	k5eAaImIp3nP
moje	můj	k3xOp1gInPc4
vzpomínky	vzpomínka	k1gFnPc1
na	na	k7c4
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
lesy	les	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c4
kontakt	kontakt	k1gInSc4
s	s	k7c7
divokou	divoký	k2eAgFnSc7d1
přírodou	příroda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gMnSc5
</s>
<s>
Jistou	jistý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byl	být	k5eAaImAgInS
Kaczynski	Kaczynske	k1gFnSc4
i	i	k8xC
podezřelým	podezřelý	k2eAgNnPc3d1
v	v	k7c6
případu	případ	k1gInSc6
sériového	sériový	k2eAgMnSc2d1
vraha	vrah	k1gMnSc2
Zodiaka	Zodiak	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
Zodiakových	Zodiakový	k2eAgFnPc2d1
vražd	vražda	k1gFnPc2
bydlel	bydlet	k5eAaImAgInS
v	v	k7c6
oblasti	oblast	k1gFnSc6
San	San	k1gMnSc1
Francisca	Francisca	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Zodiakem	zodiak	k1gInSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc7
oběťmi	oběť	k1gFnPc7
a	a	k8xC
Kaczynským	Kaczynská	k1gFnPc3
však	však	k9
nebylo	být	k5eNaImAgNnS
žádné	žádný	k3yNgNnSc1
spojení	spojení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
Zodiakem	zodiak	k1gInSc7
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
vylučuje	vylučovat	k5eAaImIp3nS
i	i	k9
znalecké	znalecký	k2eAgNnSc4d1
zkoumání	zkoumání	k1gNnSc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynsk	k1gMnSc5
v	v	k7c6
kultuře	kultura	k1gFnSc3
</s>
<s>
Norská	norský	k2eAgNnPc1d1
aggrotech	aggrot	k1gInPc6
kapela	kapela	k1gFnSc1
Combichrist	Combichrist	k1gInSc1
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
písni	píseň	k1gFnSc6
„	„	k?
<g/>
God	God	k1gFnSc2
Bless	Blessa	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
„	„	k?
<g/>
Bůh	bůh	k1gMnSc1
žehnej	žehnat	k5eAaImRp2nS
<g/>
“	“	k?
<g/>
)	)	kIx)
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
sériových	sériový	k2eAgMnPc2d1
vrahů	vrah	k1gMnPc2
a	a	k8xC
teroristů	terorista	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
je	být	k5eAaImIp3nS
i	i	k9
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynski	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PRISONER	PRISONER	kA
OF	OF	kA
RAGE	RAGE	kA
--	--	k?
A	A	kA
special	special	k1gInSc1
report	report	k1gInSc1
<g/>
.	.	kIx.
<g/>
;	;	kIx,
<g/>
From	From	k1gMnSc1
a	a	k8xC
Child	Child	k1gMnSc1
of	of	k?
Promise	Promise	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
Unabom	Unabom	k1gInSc1
Suspect	Suspect	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
KASSINOVE	KASSINOVE	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
<g/>
;	;	kIx,
TAFRATE	TAFRATE	kA
<g/>
,	,	kIx,
Raymond	Raymond	k1gMnSc1
Chip	Chip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anger	Anger	k1gInSc1
Management	management	k1gInSc4
<g/>
:	:	kIx,
The	The	k1gFnSc4
Complete	Comple	k1gNnSc2
Treatment	Treatment	k1gInSc4
Guidebook	Guidebook	k1gInSc4
for	forum	k1gNnPc2
Practitioners	Practitionersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreword	Foreword	k1gInSc4
by	by	kYmCp3nS
Albert	Albert	k1gMnSc1
Ellis	Ellis	k1gFnSc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atascadero	Atascadero	k1gNnSc1
<g/>
,	,	kIx,
California	Californium	k1gNnSc2
<g/>
:	:	kIx,
Impact	Impact	k2eAgInSc1d1
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
305	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
The	The	k1gMnSc1
Practical	Practical	k1gMnSc1
Therapist	Therapist	k1gMnSc1
Series	Series	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
886230	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accepting	Accepting	k1gInSc1
<g/>
,	,	kIx,
Adapting	Adapting	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Adjusting	Adjusting	k1gInSc1
#	#	kIx~
<g/>
15	#num#	k4
Forgiveness	Forgivenessa	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
234	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://paulcooijmans.com/psychology/unabombreport3.html	http://paulcooijmans.com/psychology/unabombreport3.html	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Chase	chasa	k1gFnSc3
<g/>
,	,	kIx,
Alston	Alston	k1gInSc1
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
and	and	k?
the	the	k?
Unabomber	Unabomber	k1gInSc1
The	The	k1gMnSc1
Education	Education	k1gInSc1
of	of	k?
an	an	k?
American	American	k1gMnSc1
Terrorist	Terrorist	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
W.	W.	kA
W.	W.	kA
Norton	Norton	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Unabomberův	Unabomberův	k2eAgInSc1d1
manifest	manifest	k1gInSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Lyricstime	Lyricstim	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
Combichrist	Combichrist	k1gMnSc1
-	-	kIx~
God	God	k1gFnSc1
Bless	Bless	k1gInSc1
<g/>
.	.	kIx.
www.lyricstime.com	www.lyricstime.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Diskografie	diskografie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Combichrist	Combichrist	k1gMnSc1
-	-	kIx~
God	God	k1gFnSc1
Bless	Blessa	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Theodore	Theodor	k1gMnSc5
Kaczynski	Kaczynski	k1gNnSc3
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
unabomberův	unabomberův	k2eAgInSc1d1
manifest	manifest	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Unabomber	Unabomber	k1gMnSc1
<g/>
:	:	kIx,
From	From	k1gMnSc1
Genius	genius	k1gMnSc1
to	ten	k3xDgNnSc4
Madman	Madman	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Text	text	k1gInSc1
Unabomberova	Unabomberův	k2eAgInSc2d1
dopisu	dopis	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Kaczynského	Kaczynského	k2eAgFnSc2d1
publikované	publikovaný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Fotoalbum	fotoalbum	k1gNnSc1
rodiny	rodina	k1gFnSc2
Kaczynských	Kaczynských	k2eAgFnSc2d1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Unabomber	Unabomber	k1gInSc1
<g/>
:	:	kIx,
Manifest	manifest	k1gInSc1
(	(	kIx(
<g/>
výpisky	výpiska	k1gFnPc1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
24	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19981002275	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
120548127	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0923	#num#	k4
7571	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
96050765	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
90723630	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
96050765	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
