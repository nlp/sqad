<s>
Dennis	Dennis	k1gFnSc1	Dennis
Gabor	Gabora	k1gFnPc2	Gabora
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Gábor	Gábor	k1gMnSc1	Gábor
Dénes	Dénes	k1gMnSc1	Dénes
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
maďarsko-britský	maďarskoritský	k2eAgMnSc1d1	maďarsko-britský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
objevil	objevit	k5eAaPmAgInS	objevit
princip	princip	k1gInSc1	princip
holografie	holografie	k1gFnSc2	holografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
jako	jako	k8xS	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
otce	otec	k1gMnSc2	otec
Bertalana	Bertalan	k1gMnSc2	Bertalan
Gábora	Gábor	k1gMnSc2	Gábor
<g/>
,	,	kIx,	,
ředitele	ředitel	k1gMnSc2	ředitel
důlní	důlní	k2eAgFnSc2d1	důlní
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
Adrienne	Adrienn	k1gInSc5	Adrienn
<g/>
.	.	kIx.	.
narozen	narozen	k2eAgMnSc1d1	narozen
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
probudil	probudit	k5eAaPmAgInS	probudit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
budovali	budovat	k5eAaImAgMnP	budovat
domácí	domácí	k1gMnPc4	domácí
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
reprodukovali	reprodukovat	k5eAaBmAgMnP	reprodukovat
moderní	moderní	k2eAgInPc4d1	moderní
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Rentgenova	Rentgenův	k2eAgNnSc2d1	Rentgenovo
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Technické	technický	k2eAgFnSc6d1	technická
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
elektroinženýrství	elektroinženýrství	k1gNnPc2	elektroinženýrství
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
fyzika	fyzika	k1gFnSc1	fyzika
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
tolik	tolik	k6eAd1	tolik
uznávaným	uznávaný	k2eAgInSc7d1	uznávaný
oborem	obor	k1gInSc7	obor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dokončil	dokončit	k5eAaPmAgInS	dokončit
studia	studio	k1gNnPc4	studio
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
1924	[number]	k4	1924
získal	získat	k5eAaPmAgInS	získat
diplom	diplom	k1gInSc4	diplom
na	na	k7c4	na
Technische	Technische	k1gNnSc4	Technische
Hochschule	Hochschule	k1gFnSc2	Hochschule
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
stále	stále	k6eAd1	stále
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
víc	hodně	k6eAd2	hodně
ho	on	k3xPp3gNnSc4	on
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
Německá	německý	k2eAgFnSc1d1	německá
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
přednášeli	přednášet	k5eAaImAgMnP	přednášet
fyzici	fyzik	k1gMnPc1	fyzik
jako	jako	k8xS	jako
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Planck	Plancka	k1gFnPc2	Plancka
<g/>
,	,	kIx,	,
Walther	Walthra	k1gFnPc2	Walthra
Hermann	Hermann	k1gMnSc1	Hermann
Nernst	Nernst	k1gMnSc1	Nernst
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
von	von	k1gInSc4	von
Laue	Lau	k1gInSc2	Lau
<g/>
.	.	kIx.	.
</s>
<s>
Profesí	profes	k1gFnSc7	profes
zůstal	zůstat	k5eAaPmAgInS	zůstat
elektroinženýrem	elektroinženýr	k1gMnSc7	elektroinženýr
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Doktorská	doktorský	k2eAgFnSc1d1	doktorská
práce	práce	k1gFnSc1	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
rychlého	rychlý	k2eAgInSc2d1	rychlý
osciloskopu	osciloskop	k1gInSc2	osciloskop
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
které	který	k3yIgNnSc4	který
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
první	první	k4xOgFnSc6	první
železem	železo	k1gNnSc7	železo
stíněnou	stíněný	k2eAgFnSc4d1	stíněná
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
čočku	čočka	k1gFnSc4	čočka
<g/>
.	.	kIx.	.
1927	[number]	k4	1927
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
Dr-	Dr-	k1gFnSc2	Dr-
<g/>
Ing.	ing.	kA	ing.
1927	[number]	k4	1927
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
firmě	firma	k1gFnSc3	firma
Siemens	siemens	k1gInSc1	siemens
&	&	k?	&
Halske	Halsk	k1gFnSc2	Halsk
AG	AG	kA	AG
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
prvních	první	k4xOgInPc2	první
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
vynálezů	vynález	k1gInPc2	vynález
byla	být	k5eAaImAgFnS	být
vysokotlaká	vysokotlaký	k2eAgFnSc1d1	vysokotlaká
křemenná	křemenný	k2eAgFnSc1d1	křemenná
rtuťová	rtuťový	k2eAgFnSc1d1	rtuťová
výbojka	výbojka	k1gFnSc1	výbojka
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
židovský	židovský	k2eAgInSc4d1	židovský
původ	původ	k1gInSc4	původ
opustil	opustit	k5eAaPmAgMnS	opustit
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
získat	získat	k5eAaPmF	získat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
u	u	k7c2	u
British	Britisha	k1gFnPc2	Britisha
Thomson-Houston	Thomson-Houston	k1gInSc1	Thomson-Houston
Co	co	k3yQnSc1	co
<g/>
.	.	kIx.	.
v	v	k7c6	v
Rugby	rugby	k1gNnSc6	rugby
<g/>
.	.	kIx.	.
</s>
<s>
BTH	BTH	kA	BTH
Research	Research	k1gMnSc1	Research
Laboratory	Laborator	k1gMnPc4	Laborator
1936	[number]	k4	1936
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Marjorií	Marjorie	k1gFnSc7	Marjorie
Louisou	Louisa	k1gFnSc7	Louisa
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
:	:	kIx,	:
Joseph	Joseph	k1gMnSc1	Joseph
Kennard	Kennard	k1gMnSc1	Kennard
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
:	:	kIx,	:
Louise	Louis	k1gMnSc2	Louis
Butler	Butler	k1gInSc1	Butler
of	of	k?	of
Rugby	rugby	k1gNnSc1	rugby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
První	první	k4xOgInPc1	první
články	článek	k1gInPc1	článek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Teorie	teorie	k1gFnSc1	teorie
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
stereoskopického	stereoskopický	k2eAgInSc2d1	stereoskopický
kinematografu	kinematograf	k1gInSc2	kinematograf
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
Základní	základní	k2eAgInPc1d1	základní
experimenty	experiment	k1gInPc1	experiment
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
holografie	holografie	k1gFnSc2	holografie
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
wavefront	wavefront	k1gMnSc1	wavefront
reconstruction	reconstruction	k1gInSc1	reconstruction
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
důsledkem	důsledek	k1gInSc7	důsledek
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
elektronového	elektronový	k2eAgInSc2d1	elektronový
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
AEI	AEI	kA	AEI
Research	Research	k1gInSc1	Research
Laboratory	Laborator	k1gMnPc7	Laborator
v	v	k7c6	v
Aldermastonu	Aldermaston	k1gInSc6	Aldermaston
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
Imperial	Imperial	k1gInSc4	Imperial
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Science	Science	k1gFnSc1	Science
&	&	k?	&
Technology	technolog	k1gMnPc7	technolog
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
elektronové	elektronový	k2eAgFnSc2d1	elektronová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
doktorandy	doktorand	k1gMnPc7	doktorand
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
Experimentální	experimentální	k2eAgFnSc2d1	experimentální
práce	práce	k1gFnSc2	práce
<g/>
:	:	kIx,	:
Langmuirův	Langmuirův	k2eAgInSc1d1	Langmuirův
paradox	paradox	k1gInSc1	paradox
holografický	holografický	k2eAgInSc1d1	holografický
mikroskop	mikroskop	k1gInSc1	mikroskop
Teoretické	teoretický	k2eAgFnSc2d1	teoretická
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
:	:	kIx,	:
teorie	teorie	k1gFnSc1	teorie
komunikace	komunikace	k1gFnSc2	komunikace
teorie	teorie	k1gFnSc2	teorie
plazmatu	plazma	k1gNnSc2	plazma
teorie	teorie	k1gFnSc2	teorie
magnetronu	magnetron	k1gInSc2	magnetron
schema	schema	k1gNnSc1	schema
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
1967	[number]	k4	1967
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
Zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
Imperial	Imperial	k1gMnSc1	Imperial
College	College	k1gNnPc2	College
<g />
.	.	kIx.	.
</s>
<s>
jakožto	jakožto	k8xS	jakožto
Senior	senior	k1gMnSc1	senior
Research	Research	k1gMnSc1	Research
Fellow	Fellow	k1gMnSc1	Fellow
Stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
Staff	Staff	k1gMnSc1	Staff
Scientist	Scientist	k1gMnSc1	Scientist
of	of	k?	of
CBS	CBS	kA	CBS
Laboratories	Laboratories	k1gMnSc1	Laboratories
<g/>
,	,	kIx,	,
Stamford	Stamford	k1gMnSc1	Stamford
<g/>
,	,	kIx,	,
Conn	Conn	k1gMnSc1	Conn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
presidentem	president	k1gMnSc7	president
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
přítelem	přítel	k1gMnSc7	přítel
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Petrem	Petr	k1gMnSc7	Petr
Goldmarkem	Goldmarek	k1gMnSc7	Goldmarek
na	na	k7c6	na
nových	nový	k2eAgNnPc6d1	nové
schématech	schéma	k1gNnPc6	schéma
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
zobrazování	zobrazování	k1gNnSc2	zobrazování
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
tráví	trávit	k5eAaImIp3nP	trávit
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
budoucnost	budoucnost	k1gFnSc4	budoucnost
industriální	industriální	k2eAgFnSc2d1	industriální
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zneklidněn	zneklidnit	k5eAaPmNgInS	zneklidnit
nerovnováhou	nerovnováha	k1gFnSc7	nerovnováha
mezi	mezi	k7c7	mezi
technologickým	technologický	k2eAgInSc7d1	technologický
růstem	růst	k1gInSc7	růst
a	a	k8xC	a
sociální	sociální	k2eAgFnSc7d1	sociální
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgInPc1	svůj
názory	názor	k1gInPc1	názor
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
knihách	kniha	k1gFnPc6	kniha
<g/>
:	:	kIx,	:
1963	[number]	k4	1963
<g/>
:	:	kIx,	:
Inventing	Inventing	k1gInSc1	Inventing
the	the	k?	the
Future	Futur	k1gMnSc5	Futur
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
Innovations	Innovations	k1gInSc1	Innovations
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Mature	Matur	k1gMnSc5	Matur
Society	societa	k1gFnPc4	societa
<g/>
,	,	kIx,	,
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
1956	[number]	k4	1956
<g/>
:	:	kIx,	:
Fellow	Fellow	k1gFnSc1	Fellow
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gInSc1	Royal
Society	societa	k1gFnSc2	societa
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
čestný	čestný	k2eAgMnSc1d1	čestný
člen	člen	k1gMnSc1	člen
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
1967	[number]	k4	1967
<g/>
:	:	kIx,	:
medaile	medaile	k1gFnSc2	medaile
Thomase	Thomas	k1gMnSc4	Thomas
Younga	Young	k1gMnSc4	Young
od	od	k7c2	od
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
společnosti	společnost	k1gFnSc2	společnost
Londýn	Londýn	k1gInSc1	Londýn
1967	[number]	k4	1967
<g/>
:	:	kIx,	:
cena	cena	k1gFnSc1	cena
Cristofora	Cristofor	k1gMnSc2	Cristofor
Colomba	Colomb	k1gMnSc2	Colomb
od	od	k7c2	od
Int	Int	k1gFnSc2	Int
<g/>
.	.	kIx.	.
</s>
<s>
Inst	Inst	k1gMnSc1	Inst
<g/>
.	.	kIx.	.
</s>
<s>
Communications	Communications	k1gInSc1	Communications
<g/>
,	,	kIx,	,
Genoa	Genoa	k1gFnSc1	Genoa
1968	[number]	k4	1968
<g/>
:	:	kIx,	:
medaile	medaile	k1gFnSc2	medaile
Alberta	Albert	k1gMnSc2	Albert
Michelsona	Michelson	k1gMnSc2	Michelson
od	od	k7c2	od
Franklinova	Franklinův	k2eAgInSc2d1	Franklinův
institutu	institut	k1gInSc2	institut
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
1968	[number]	k4	1968
<g/>
:	:	kIx,	:
Rumfordova	Rumfordův	k2eAgFnSc1d1	Rumfordova
medaile	medaile	k1gFnSc1	medaile
od	od	k7c2	od
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
čestný	čestný	k2eAgMnSc1d1	čestný
D.	D.	kA	D.
<g/>
Sc	Sc	k1gFnSc6	Sc
<g/>
.	.	kIx.	.
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Southampton	Southampton	k1gInSc1	Southampton
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
čestná	čestný	k2eAgFnSc1d1	čestná
medaile	medaile	k1gFnSc1	medaile
od	od	k7c2	od
Ústavu	ústav	k1gInSc2	ústav
elektrického	elektrický	k2eAgNnSc2d1	elektrické
a	a	k8xC	a
elektronického	elektronický	k2eAgNnSc2d1	elektronické
inženýrství	inženýrství	k1gNnSc2	inženýrství
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
titul	titul	k1gInSc1	titul
Commander	Commander	k1gInSc1	Commander
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
British	British	k1gInSc1	British
Empire	empir	k1gInSc5	empir
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
čestný	čestný	k2eAgMnSc1d1	čestný
D.	D.	kA	D.
<g/>
Sc	Sc	k1gFnSc6	Sc
<g/>
.	.	kIx.	.
na	na	k7c6	na
Technologické	technologický	k2eAgFnSc6d1	technologická
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Delftu	Delft	k1gInSc6	Delft
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Holweckova	Holweckův	k2eAgFnSc1d1	Holweckův
cena	cena	k1gFnSc1	cena
od	od	k7c2	od
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
společnosti	společnost	k1gFnSc2	společnost
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
GABOR	GABOR	kA	GABOR
<g/>
,	,	kIx,	,
Dennis	Dennis	k1gFnSc1	Dennis
<g/>
.	.	kIx.	.
</s>
<s>
Theory	Theor	k1gInPc1	Theor
of	of	k?	of
communication	communication	k1gInSc1	communication
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
IEEE	IEEE	kA	IEEE
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
93	[number]	k4	93
<g/>
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
429	[number]	k4	429
<g/>
-	-	kIx~	-
<g/>
457	[number]	k4	457
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dennis	Dennis	k1gFnSc2	Dennis
Gabor	Gabor	k1gInSc1	Gabor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Dennis	Dennis	k1gFnSc2	Dennis
Gabor	Gabora	k1gFnPc2	Gabora
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Gáborova	Gáborův	k2eAgFnSc1d1	Gáborova
autobiografie	autobiografie	k1gFnSc1	autobiografie
na	na	k7c6	na
Nobelprize	Nobelpriza	k1gFnSc6	Nobelpriza
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Seznam	seznam	k1gInSc4	seznam
židovských	židovský	k2eAgMnPc2d1	židovský
laureátů	laureát	k1gMnPc2	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c4	na
Israel	Israel	k1gInSc4	Israel
Science	Science	k1gFnSc1	Science
and	and	k?	and
Technology	technolog	k1gMnPc4	technolog
Homepage	Homepag	k1gFnSc2	Homepag
Životopis	životopis	k1gInSc4	životopis
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
obrázky	obrázek	k1gInPc7	obrázek
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Prof.	prof.	kA	prof.
Eugenii	Eugenie	k1gFnSc4	Eugenie
Katz	Katza	k1gFnPc2	Katza
<g/>
,	,	kIx,	,
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
univerzita	univerzita	k1gFnSc1	univerzita
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
</s>
