<s>
Madison	Madison	k1gNnSc1	Madison
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Wisconsin	Wisconsina	k1gFnPc2	Wisconsina
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činila	činit	k5eAaImAgFnS	činit
populace	populace	k1gFnSc1	populace
233	[number]	k4	233
209	[number]	k4	209
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Madison	Madison	k1gNnSc4	Madison
činí	činit	k5eAaImIp3nS	činit
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Wisconsin	Wisconsina	k1gFnPc2	Wisconsina
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Milwaukee	Milwaukee	k1gFnSc6	Milwaukee
<g/>
,	,	kIx,	,
a	a	k8xC	a
77	[number]	k4	77
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Madisonu	Madison	k1gInSc6	Madison
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
University	universita	k1gFnPc4	universita
of	of	k?	of
Wisconsin-Madison	Wisconsin-Madison	k1gMnSc1	Wisconsin-Madison
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gInSc1	Madison
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
federální	federální	k2eAgMnSc1d1	federální
soudce	soudce	k1gMnSc1	soudce
jménem	jméno	k1gNnSc7	jméno
James	James	k1gMnSc1	James
Duane	Duan	k1gInSc5	Duan
Doty	Dota	k1gMnSc2	Dota
tehdy	tehdy	k6eAd1	tehdy
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc1	tisíc
akrů	akr	k1gInPc2	akr
(	(	kIx(	(
<g/>
4	[number]	k4	4
km2	km2	k4	km2
<g/>
)	)	kIx)	)
bažin	bažina	k1gFnPc2	bažina
a	a	k8xC	a
lesů	les	k1gInPc2	les
na	na	k7c4	na
šíji	šíje	k1gFnSc4	šíje
mezi	mezi	k7c7	mezi
jezery	jezero	k1gNnPc7	jezero
Mendota	Mendot	k1gMnSc2	Mendot
a	a	k8xC	a
Monona	Monon	k1gMnSc2	Monon
<g/>
,	,	kIx,	,
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
vybudovat	vybudovat	k5eAaPmF	vybudovat
nové	nový	k2eAgNnSc4d1	nové
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
byl	být	k5eAaImAgInS	být
Madison	Madison	k1gInSc1	Madison
vybrán	vybrat	k5eAaPmNgInS	vybrat
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosud	dosud	k6eAd1	dosud
existoval	existovat	k5eAaImAgInS	existovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
mezi	mezi	k7c7	mezi
novými	nový	k2eAgNnPc7d1	nové
a	a	k8xC	a
rostoucími	rostoucí	k2eAgNnPc7d1	rostoucí
městy	město	k1gNnPc7	město
okolo	okolo	k7c2	okolo
Milwaukee	Milwauke	k1gFnSc2	Milwauke
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
starším	starší	k1gMnSc7	starší
Prairie	Prairie	k1gFnSc2	Prairie
du	du	k?	du
Chien	Chien	k1gInSc1	Chien
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
mezi	mezi	k7c7	mezi
vysoce	vysoce	k6eAd1	vysoce
obydlenou	obydlený	k2eAgFnSc7d1	obydlená
oblastí	oblast	k1gFnSc7	oblast
olověných	olověný	k2eAgInPc2d1	olověný
dolů	dol	k1gInPc2	dol
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
Green	Green	k1gInSc1	Green
Bay	Bay	k1gFnSc1	Bay
(	(	kIx(	(
<g/>
nejstarším	starý	k2eAgNnSc7d3	nejstarší
městem	město	k1gNnSc7	město
Wisconsinu	Wisconsin	k2eAgFnSc4d1	Wisconsin
<g/>
)	)	kIx)	)
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gInSc1	Madison
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
Otců	otec	k1gMnPc2	otec
zakladatelů	zakladatel	k1gMnPc2	zakladatel
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc1	Jamese
Madisona	Madisona	k1gFnSc1	Madisona
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
wisconsinského	wisconsinský	k2eAgInSc2d1	wisconsinský
kapitolu	kapitola	k1gFnSc4	kapitola
v	v	k7c6	v
Madisonu	Madison	k1gInSc6	Madison
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc1d1	položen
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
poprvé	poprvé	k6eAd1	poprvé
sešel	sejít	k5eAaPmAgMnS	sejít
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
stal	stát	k5eAaPmAgMnS	stát
Wisconsin	Wisconsin	k1gMnSc1	Wisconsin
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Madison	Madison	k1gMnSc1	Madison
i	i	k9	i
nadále	nadále	k6eAd1	nadále
jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
University	universita	k1gFnPc1	universita
of	of	k?	of
Wisconsin-Madison	Wisconsin-Madison	k1gInSc1	Wisconsin-Madison
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
byl	být	k5eAaImAgInS	být
Madison	Madison	k1gInSc1	Madison
napojen	napojen	k2eAgInSc1d1	napojen
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
byl	být	k5eAaImAgMnS	být
Madison	Madison	k1gMnSc1	Madison
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
;	;	kIx,	;
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žilo	žít	k5eAaImAgNnS	žít
6	[number]	k4	6
863	[number]	k4	863
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
jej	on	k3xPp3gMnSc4	on
armáda	armáda	k1gFnSc1	armáda
Unie	unie	k1gFnPc4	unie
používala	používat	k5eAaImAgFnS	používat
jako	jako	k9	jako
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
Wisconsin	Wisconsin	k1gInSc4	Wisconsin
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Camp	camp	k1gInSc1	camp
Randall	Randall	k1gInSc1	Randall
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xS	jako
výcvikový	výcvikový	k2eAgInSc1d1	výcvikový
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgFnSc1d1	vojenská
nemocnice	nemocnice	k1gFnSc1	nemocnice
a	a	k8xC	a
jako	jako	k9	jako
zajatecký	zajatecký	k2eAgInSc1d1	zajatecký
tábor	tábor	k1gInSc1	tábor
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tábor	tábor	k1gInSc1	tábor
přičleněn	přičlenit	k5eAaPmNgInS	přičlenit
k	k	k7c3	k
území	území	k1gNnSc3	území
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
vybudován	vybudován	k2eAgInSc4d1	vybudován
stadion	stadion	k1gInSc4	stadion
Camp	camp	k1gInSc4	camp
Randall	Randalla	k1gFnPc2	Randalla
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Madison	Madison	k1gMnSc1	Madison
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
městem	město	k1gNnSc7	město
Wisconsinu	Wisconsin	k2eAgInSc3d1	Wisconsin
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
více	hodně	k6eAd2	hodně
než	než	k8xS	než
desetinásobně	desetinásobně	k6eAd1	desetinásobně
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uprostřed	uprostřed	k7c2	uprostřed
okresu	okres	k1gInSc2	okres
Dane	Dan	k1gMnSc5	Dan
County	Counta	k1gFnSc2	Counta
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
jižním	jižní	k2eAgInSc6d1	jižní
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
<g/>
,	,	kIx,	,
123	[number]	k4	123
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Milwaukee	Milwauke	k1gFnSc2	Milwauke
<g/>
,	,	kIx,	,
195	[number]	k4	195
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
menší	malý	k2eAgNnPc4d2	menší
města	město	k1gNnPc4	město
Madison	Madisona	k1gFnPc2	Madisona
a	a	k8xC	a
Monona	Monona	k1gFnSc1	Monona
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vesnice	vesnice	k1gFnSc2	vesnice
Maple	Maple	k1gNnSc2	Maple
Bluff	Bluff	k1gInSc1	Bluff
a	a	k8xC	a
Shorewood	Shorewood	k1gInSc1	Shorewood
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgNnPc3	svůj
největším	veliký	k2eAgNnPc3d3	veliký
předměstím	předměstí	k1gNnPc3	předměstí
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
Middleton	Middleton	k1gInSc4	Middleton
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
městy	město	k1gNnPc7	město
Sun	Sun	kA	Sun
Prairie	Prairie	k1gFnPc1	Prairie
<g/>
,	,	kIx,	,
Fitchburg	Fitchburg	k1gInSc1	Fitchburg
a	a	k8xC	a
s	s	k7c7	s
vesnicemi	vesnice	k1gFnPc7	vesnice
McFarland	McFarlanda	k1gFnPc2	McFarlanda
<g/>
,	,	kIx,	,
Verona	Verona	k1gFnSc1	Verona
a	a	k8xC	a
Waunakee	Waunakee	k1gFnSc1	Waunakee
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
219,4	[number]	k4	219,4
km2	km2	k4	km2
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
177,9	[number]	k4	177,9
km2	km2	k4	km2
země	zem	k1gFnPc1	zem
a	a	k8xC	a
41,5	[number]	k4	41,5
km2	km2	k4	km2
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gInSc1	Madison
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
Městem	město	k1gNnSc7	město
čtyř	čtyři	k4xCgNnPc2	čtyři
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
čtyřem	čtyři	k4xCgNnPc3	čtyři
jezerům	jezero	k1gNnPc3	jezero
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Yahara	Yahara	k1gFnSc1	Yahara
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jezero	jezero	k1gNnSc4	jezero
Mendota	Mendota	k1gFnSc1	Mendota
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
jezero	jezero	k1gNnSc1	jezero
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
Monona	Monona	k1gFnSc1	Monona
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Třetí	třetí	k4xOgNnSc1	třetí
jezero	jezero	k1gNnSc1	jezero
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
Waubesa	Waubesa	k1gFnSc1	Waubesa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Druhé	druhý	k4xOgNnSc1	druhý
jezero	jezero	k1gNnSc1	jezero
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jezero	jezero	k1gNnSc1	jezero
Kegonsa	Kegons	k1gMnSc2	Kegons
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
První	první	k4xOgNnSc1	první
jezero	jezero	k1gNnSc1	jezero
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnSc2	jezero
Mendota	Mendot	k1gMnSc2	Mendot
a	a	k8xC	a
Monona	Monon	k1gMnSc2	Monon
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
území	území	k1gNnSc6	území
Madisonu	Madison	k1gInSc2	Madison
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnSc2	jezero
Waubesa	Waubesa	k1gFnSc1	Waubesa
a	a	k8xC	a
Kegonsa	Kegonsa	k1gFnSc1	Kegonsa
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Páté	pátá	k1gFnPc4	pátá
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc4d2	menší
jezero	jezero	k1gNnSc4	jezero
jménem	jméno	k1gNnSc7	jméno
Wingra	Wingr	k1gInSc2	Wingr
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Yahara	Yahar	k1gMnSc2	Yahar
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c4	na
šíji	šíje	k1gFnSc4	šíje
mezi	mezi	k7c7	mezi
jezery	jezero	k1gNnPc7	jezero
Mendota	Mendot	k1gMnSc2	Mendot
a	a	k8xC	a
Monona	Monon	k1gMnSc2	Monon
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Yahara	Yahara	k1gFnSc1	Yahara
<g/>
,	,	kIx,	,
protékající	protékající	k2eAgFnSc1d1	protékající
Madisonem	Madison	k1gInSc7	Madison
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
70	[number]	k4	70
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Rock	rock	k1gInSc1	rock
River	River	k1gInSc1	River
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
následně	následně	k6eAd1	následně
do	do	k7c2	do
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
jižní	jižní	k2eAgInSc1d1	jižní
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgNnSc1d1	vlhké
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
proměnlivým	proměnlivý	k2eAgNnSc7d1	proměnlivé
počasím	počasí	k1gNnSc7	počasí
a	a	k8xC	a
velkým	velký	k2eAgNnSc7d1	velké
sezonními	sezonní	k2eAgInPc7d1	sezonní
teplotními	teplotní	k2eAgInPc7d1	teplotní
výkyvy	výkyv	k1gInPc7	výkyv
-	-	kIx~	-
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
,	,	kIx,	,
se	s	k7c7	s
středními	střední	k2eAgInPc7d1	střední
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
značnými	značný	k2eAgFnPc7d1	značná
sněhovými	sněhový	k2eAgFnPc7d1	sněhová
srážkami	srážka	k1gFnPc7	srážka
<g/>
;	;	kIx,	;
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
často	často	k6eAd1	často
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
horních	horní	k2eAgFnPc2d1	horní
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
Běžnými	běžný	k2eAgMnPc7d1	běžný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vysoké	vysoký	k2eAgFnPc1d1	vysoká
hodnoty	hodnota	k1gFnPc1	hodnota
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
233	[number]	k4	233
209	[number]	k4	209
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
78,9	[number]	k4	78,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
7,3	[number]	k4	7,3
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
7,4	[number]	k4	7,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,9	[number]	k4	2,9
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,1	[number]	k4	3,1
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
6,8	[number]	k4	6,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
měl	mít	k5eAaImAgInS	mít
Madison	Madison	k1gInSc1	Madison
208	[number]	k4	208
504	[number]	k4	504
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
89	[number]	k4	89
019	[number]	k4	019
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
42	[number]	k4	42
462	[number]	k4	462
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
1169	[number]	k4	1169
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
92	[number]	k4	92
394	[number]	k4	394
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
tedy	tedy	k8xC	tedy
519,5	[number]	k4	519,5
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
89	[number]	k4	89
019	[number]	k4	019
domácností	domácnost	k1gFnPc2	domácnost
bylo	být	k5eAaImAgNnS	být
22,1	[number]	k4	22,1
<g/>
%	%	kIx~	%
takových	takový	k3xDgFnPc2	takový
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
žilo	žít	k5eAaImAgNnS	žít
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
dítě	dítě	k1gNnSc1	dítě
mladší	mladý	k2eAgMnPc1d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
37	[number]	k4	37
<g/>
%	%	kIx~	%
tvořily	tvořit	k5eAaImAgInP	tvořit
manželské	manželský	k2eAgInPc1d1	manželský
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
7,8	[number]	k4	7,8
<g/>
%	%	kIx~	%
obývala	obývat	k5eAaImAgFnS	obývat
žena	žena	k1gFnSc1	žena
bez	bez	k7c2	bez
manžela	manžel	k1gMnSc2	manžel
a	a	k8xC	a
52,3	[number]	k4	52,3
<g/>
%	%	kIx~	%
nebylo	být	k5eNaImAgNnS	být
rodinného	rodinný	k2eAgInSc2d1	rodinný
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
35,3	[number]	k4	35,3
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
domácností	domácnost	k1gFnPc2	domácnost
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
jednotlivci	jednotlivec	k1gMnSc6	jednotlivec
a	a	k8xC	a
v	v	k7c6	v
7,1	[number]	k4	7,1
<g/>
%	%	kIx~	%
žil	žít	k5eAaImAgMnS	žít
osamoceně	osamoceně	k6eAd1	osamoceně
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
domácnost	domácnost	k1gFnSc1	domácnost
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
2,19	[number]	k4	2,19
osobami	osoba	k1gFnPc7	osoba
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
2,87	[number]	k4	2,87
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
ze	z	k7c2	z
17,9	[number]	k4	17,9
<g/>
%	%	kIx~	%
osobami	osoba	k1gFnPc7	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pod	pod	k7c4	pod
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
21,4	[number]	k4	21,4
<g/>
%	%	kIx~	%
osobami	osoba	k1gFnPc7	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
18	[number]	k4	18
do	do	k7c2	do
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
32,2	[number]	k4	32,2
<g/>
%	%	kIx~	%
osobami	osoba	k1gFnPc7	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
44	[number]	k4	44
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
19,3	[number]	k4	19,3
<g/>
%	%	kIx~	%
osobami	osoba	k1gFnPc7	osoba
od	od	k7c2	od
45	[number]	k4	45
do	do	k7c2	do
64	[number]	k4	64
let	léto	k1gNnPc2	léto
a	a	k8xC	a
z	z	k7c2	z
9,2	[number]	k4	9,2
<g/>
%	%	kIx~	%
osobami	osoba	k1gFnPc7	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
staršími	starší	k1gMnPc7	starší
<g/>
.	.	kIx.	.
</s>
<s>
Věkový	věkový	k2eAgInSc1d1	věkový
medián	medián	k1gInSc1	medián
měl	mít	k5eAaImAgInS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
31	[number]	k4	31
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každých	každý	k3xTgInPc2	každý
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
96,6	[number]	k4	96,6
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každých	každý	k3xTgInPc2	každý
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyšším	vysoký	k2eAgInSc7d2	vyšší
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
95	[number]	k4	95
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Příjmový	příjmový	k2eAgInSc1d1	příjmový
medián	medián	k1gInSc1	medián
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
domácnost	domácnost	k1gFnSc4	domácnost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
měl	mít	k5eAaImAgInS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
41,941	[number]	k4	41,941
USD	USD	kA	USD
<g/>
,	,	kIx,	,
příjmový	příjmový	k2eAgInSc1d1	příjmový
medián	medián	k1gInSc1	medián
rodiny	rodina	k1gFnSc2	rodina
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
hodnotě	hodnota	k1gFnSc6	hodnota
59,840	[number]	k4	59,840
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
příjmový	příjmový	k2eAgInSc4d1	příjmový
medián	medián	k1gInSc4	medián
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
36,718	[number]	k4	36,718
USD	USD	kA	USD
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ženy	žena	k1gFnPc1	žena
30,551	[number]	k4	30,551
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Důchod	důchod	k1gInSc1	důchod
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
23,498	[number]	k4	23,498
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
5,8	[number]	k4	5,8
<g/>
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
15	[number]	k4	15
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žilo	žít	k5eAaImAgNnS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
11,4	[number]	k4	11,4
<g/>
%	%	kIx~	%
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
mladší	mladý	k2eAgMnPc1d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Madisonu	Madison	k1gInSc2	Madison
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
žilo	žít	k5eAaImAgNnS	žít
526	[number]	k4	526
742	[number]	k4	742
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
činilo	činit	k5eAaImAgNnS	činit
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Milwaukee	Milwaukee	k1gFnSc6	Milwaukee
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Dane	Dan	k1gMnSc5	Dan
County	Count	k1gMnPc4	Count
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejrychleji	rychle	k6eAd3	rychle
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Wisconsinu	Wisconsin	k2eAgInSc3d1	Wisconsin
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
desetiletí	desetiletí	k1gNnSc4	desetiletí
přibývá	přibývat	k5eAaImIp3nS	přibývat
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Lloyd	Lloyd	k1gMnSc1	Lloyd
Wright	Wright	k1gMnSc1	Wright
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Thornton	Thornton	k1gInSc1	Thornton
Wilder	Wilder	k1gInSc1	Wilder
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
nositel	nositel	k1gMnSc1	nositel
Pulitzerovy	Pulitzerův	k2eAgFnSc2d1	Pulitzerova
ceny	cena	k1gFnSc2	cena
John	John	k1gMnSc1	John
Bardeen	Bardeen	k1gInSc1	Bardeen
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
elektrotechnik	elektrotechnik	k1gMnSc1	elektrotechnik
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Gena	Gena	k1gFnSc1	Gena
Rowlandsová	Rowlandsová	k1gFnSc1	Rowlandsová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Howard	Howard	k1gMnSc1	Howard
M.	M.	kA	M.
Temin	Temin	k1gMnSc1	Temin
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Tyne	Tyne	k1gNnSc2	Tyne
Daly	dát	k5eAaPmAgInP	dát
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
herečka	herečka	k1gFnSc1	herečka
Chris	Chris	k1gFnSc2	Chris
Noth	Notha	k1gFnPc2	Notha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Eric	Eric	k1gFnSc4	Eric
Heiden	Heidna	k1gFnPc2	Heidna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pětinásobní	pětinásobný	k2eAgMnPc1d1	pětinásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
a	a	k8xC	a
cyklista	cyklista	k1gMnSc1	cyklista
Bradley	Bradlea	k1gFnSc2	Bradlea
Whitford	Whitford	k1gMnSc1	Whitford
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Phil	Phil	k1gMnSc1	Phil
Hellmuth	Hellmuth	k1gMnSc1	Hellmuth
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgMnSc1d1	profesionální
hráč	hráč	k1gMnSc1	hráč
pokru	pokr	k1gInSc2	pokr
Ainaro	Ainara	k1gFnSc5	Ainara
<g/>
,	,	kIx,	,
Východní	východní	k2eAgMnSc1d1	východní
Timor	Timor	k1gMnSc1	Timor
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Arcatao	Arcatao	k1gMnSc1	Arcatao
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Bac	bacit	k5eAaPmRp2nS	bacit
Giang	Giang	k1gInSc1	Giang
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
Cuzco	Cuzco	k6eAd1	Cuzco
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
Camagüey	Camagüea	k1gFnSc2	Camagüea
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Freiburg	Freiburg	k1gInSc1	Freiburg
im	im	k?	im
Breisgau	Breisgaus	k1gInSc2	Breisgaus
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Managua	Managuum	k1gNnSc2	Managuum
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Mantova	Mantova	k1gFnSc1	Mantova
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Obihiro	Obihiro	k1gNnSc1	Obihiro
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
</s>
