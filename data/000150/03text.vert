<s>
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k1gMnSc1	Nemo
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Finding	Finding	k1gInSc1	Finding
Nemo	Nemo	k6eAd1	Nemo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
počítačem	počítač	k1gInSc7	počítač
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
scénáře	scénář	k1gInSc2	scénář
byl	být	k5eAaImAgMnS	být
Andrew	Andrew	k1gMnSc1	Andrew
Stanton	Stanton	k1gInSc4	Stanton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lee	Lea	k1gFnSc3	Lea
Unkrichem	Unkrich	k1gMnSc7	Unkrich
ujal	ujmout	k5eAaPmAgMnS	ujmout
i	i	k9	i
režie	režie	k1gFnSc2	režie
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
studii	studie	k1gFnSc4	studie
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Pictures	Pictures	k1gMnSc1	Pictures
a	a	k8xC	a
Pixar	Pixar	k1gMnSc1	Pixar
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
přespříliš	přespříliš	k6eAd1	přespříliš
ochranářského	ochranářský	k2eAgMnSc2d1	ochranářský
rybího	rybí	k2eAgMnSc2d1	rybí
otce	otec	k1gMnSc2	otec
Marlina	Marlin	k2eAgMnSc2d1	Marlin
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc4	hlas
mu	on	k3xPp3gMnSc3	on
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Albert	Albert	k1gMnSc1	Albert
Brooks	Brooksa	k1gFnPc2	Brooksa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
rybkou	rybka	k1gFnSc7	rybka
jménem	jméno	k1gNnSc7	jméno
Dory	Dora	k1gFnSc2	Dora
(	(	kIx(	(
<g/>
Ellen	Ellen	k2eAgInSc1d1	Ellen
DeGeneres	DeGeneres	k1gInSc1	DeGeneres
<g/>
)	)	kIx)	)
hledá	hledat	k5eAaImIp3nS	hledat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Nema	Nem	k1gInSc2	Nem
(	(	kIx(	(
<g/>
Alexander	Alexandra	k1gFnPc2	Alexandra
Gould	Gould	k1gInSc1	Gould
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
Marlin	Marlin	k1gInSc1	Marlin
pomalu	pomalu	k6eAd1	pomalu
učí	učit	k5eAaImIp3nS	učit
riskovat	riskovat	k5eAaBmF	riskovat
a	a	k8xC	a
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
věcech	věc	k1gFnPc6	věc
postarat	postarat	k5eAaPmF	postarat
sám	sám	k3xTgMnSc1	sám
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
obdržel	obdržet	k5eAaPmAgInS	obdržet
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
recenze	recenze	k1gFnPc4	recenze
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
cenu	cena	k1gFnSc4	cena
akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tržní	tržní	k2eAgFnSc6d1	tržní
stránce	stránka	k1gFnSc6	stránka
film	film	k1gInSc1	film
uspěl	uspět	k5eAaPmAgInS	uspět
-	-	kIx~	-
celosvětově	celosvětově	k6eAd1	celosvětově
vydělal	vydělat	k5eAaPmAgInS	vydělat
přes	přes	k7c4	přes
864	[number]	k4	864
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejvíce	nejvíce	k6eAd1	nejvíce
prodávaným	prodávaný	k2eAgInSc7d1	prodávaný
filmem	film	k1gInSc7	film
na	na	k7c6	na
DVD	DVD	kA	DVD
díky	díky	k7c3	díky
40	[number]	k4	40
milionům	milion	k4xCgInPc3	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnSc7	kopie
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
ho	on	k3xPp3gMnSc4	on
Americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
institut	institut	k1gInSc1	institut
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
10	[number]	k4	10
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k6eAd1	Nemo
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgMnS	stát
první	první	k4xOgNnSc4	první
pixarovským	pixarovský	k2eAgInSc7d1	pixarovský
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neměl	mít	k5eNaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
maminku	maminka	k1gFnSc4	maminka
nebo	nebo	k8xC	nebo
tatínka	tatínek	k1gMnSc4	tatínek
neposlechli	poslechnout	k5eNaPmAgMnP	poslechnout
a	a	k8xC	a
pak	pak	k6eAd1	pak
jste	být	k5eAaImIp2nP	být
na	na	k7c4	na
truc	truc	k1gInSc4	truc
udělali	udělat	k5eAaPmAgMnP	udělat
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mělo	mít	k5eAaImAgNnS	mít
daleko	daleko	k6eAd1	daleko
méně	málo	k6eAd2	málo
příjemné	příjemný	k2eAgInPc4d1	příjemný
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
než	než	k8xS	než
jste	být	k5eAaImIp2nP	být
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
?	?	kIx.	?
</s>
<s>
Tak	tak	k9	tak
přesně	přesně	k6eAd1	přesně
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Poslouchejte	poslouchat	k5eAaImRp2nP	poslouchat
maminku	maminka	k1gFnSc4	maminka
a	a	k8xC	a
tatínka	tatínek	k1gMnSc4	tatínek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nikdy	nikdy	k6eAd1	nikdy
nevíte	vědět	k5eNaImIp2nP	vědět
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vás	vy	k3xPp2nPc4	vy
vaše	váš	k3xOp2gFnSc1	váš
vlastní	vlastní	k2eAgFnSc1d1	vlastní
hlava	hlava	k1gFnSc1	hlava
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
do	do	k7c2	do
akvárka	akvárek	k1gMnSc2	akvárek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
nebudete	být	k5eNaImBp2nP	být
poslouchat	poslouchat	k5eAaImF	poslouchat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
nezažijete	zažít	k5eNaPmIp2nP	zažít
takovou	takový	k3xDgFnSc4	takový
zábavu	zábava	k1gFnSc4	zábava
a	a	k8xC	a
nepoznáte	poznat	k5eNaPmIp2nP	poznat
tolik	tolik	k6eAd1	tolik
přátel	přítel	k1gMnPc2	přítel
jako	jako	k8xS	jako
Nemo	Nemo	k6eAd1	Nemo
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
na	na	k7c4	na
vás	vy	k3xPp2nPc4	vy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vší	všecek	k3xTgFnSc6	všecek
stručnosti	stručnost	k1gFnSc6	stručnost
poselství	poselství	k1gNnSc4	poselství
a	a	k8xC	a
vzkaz	vzkaz	k1gInSc4	vzkaz
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
podrobněji	podrobně	k6eAd2	podrobně
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
malého	malý	k2eAgNnSc2d1	malé
Nema	Nemum	k1gNnSc2	Nemum
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
Nemova	Nemův	k2eAgFnSc1d1	Nemova
matka	matka	k1gFnSc1	matka
i	i	k8xC	i
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Nemo	Nemo	k6eAd1	Nemo
jediný	jediný	k2eAgMnSc1d1	jediný
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
jej	on	k3xPp3gInSc4	on
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Nemův	Nemův	k2eAgMnSc1d1	Nemův
táta	táta	k1gMnSc1	táta
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
události	událost	k1gFnSc2	událost
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
ohledu	ohled	k1gInSc6	ohled
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
opatrný	opatrný	k2eAgMnSc1d1	opatrný
a	a	k8xC	a
přirozeně	přirozeně	k6eAd1	přirozeně
nedovoloval	dovolovat	k5eNaImAgInS	dovolovat
Nemovi	Nema	k1gMnSc3	Nema
podstupovat	podstupovat	k5eAaImF	podstupovat
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
risk	risk	k1gInSc1	risk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
dospívajícího	dospívající	k2eAgInSc2d1	dospívající
Nema	Nem	k1gInSc2	Nem
rozpalovalo	rozpalovat	k5eAaImAgNnS	rozpalovat
do	do	k7c2	do
ruda	rudo	k1gNnSc2	rudo
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
jeho	jeho	k3xOp3gFnSc4	jeho
vzdor	vzdor	k6eAd1	vzdor
projevit	projevit	k5eAaPmF	projevit
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k9	jen
pozvednutým	pozvednutý	k2eAgInPc3d1	pozvednutý
"	"	kIx"	"
<g/>
obočím	obočit	k5eAaPmIp1nS	obočit
<g/>
"	"	kIx"	"
a	a	k8xC	a
povzdechy	povzdech	k1gInPc1	povzdech
<g/>
.	.	kIx.	.
</s>
<s>
Nemo	Nemo	k6eAd1	Nemo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
typické	typický	k2eAgFnSc2d1	typická
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolužáky	spolužák	k1gMnPc7	spolužák
hecují	hecovat	k5eAaImIp3nP	hecovat
a	a	k8xC	a
povzbuzují	povzbuzovat	k5eAaImIp3nP	povzbuzovat
k	k	k7c3	k
neplechám	neplecha	k1gFnPc3	neplecha
<g/>
.	.	kIx.	.
</s>
<s>
Nemo	Nemo	k6eAd1	Nemo
podpořený	podpořený	k2eAgInSc1d1	podpořený
svým	svůj	k3xOyFgInSc7	svůj
vzdorem	vzdor	k1gInSc7	vzdor
proti	proti	k7c3	proti
otci	otec	k1gMnSc3	otec
se	se	k3xPyFc4	se
hrdě	hrdě	k6eAd1	hrdě
pustí	pustit	k5eAaPmIp3nS	pustit
do	do	k7c2	do
splnění	splnění	k1gNnSc2	splnění
úkolu	úkol	k1gInSc2	úkol
-	-	kIx~	-
přeplavat	přeplavat	k5eAaPmF	přeplavat
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
bezpečí	bezpečí	k1gNnSc2	bezpečí
útesu	útes	k1gInSc2	útes
až	až	k6eAd1	až
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
a	a	k8xC	a
dotknout	dotknout	k5eAaPmF	dotknout
se	se	k3xPyFc4	se
dna	dno	k1gNnSc2	dno
člunu	člun	k1gInSc2	člun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proplouval	proplouvat	k5eAaImAgInS	proplouvat
kolem	kolo	k1gNnSc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
tímhle	tenhle	k3xDgNnSc7	tenhle
by	by	kYmCp3nS	by
otec	otec	k1gMnSc1	otec
nikdy	nikdy	k6eAd1	nikdy
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
do	do	k7c2	do
toho	ten	k3xDgMnSc4	ten
pustil	pustit	k5eAaPmAgMnS	pustit
s	s	k7c7	s
odhodláním	odhodlání	k1gNnSc7	odhodlání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
risk	risk	k1gInSc1	risk
však	však	k9	však
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
brzy	brzy	k6eAd1	brzy
vymstít	vymstít	k5eAaPmF	vymstít
<g/>
.	.	kIx.	.
</s>
<s>
Člun	člun	k1gInSc1	člun
patřil	patřit	k5eAaImAgInS	patřit
sportovnímu	sportovní	k2eAgMnSc3d1	sportovní
potápěči	potápěč	k1gMnSc3	potápěč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g/>
,	,	kIx,	,
když	když	k8xS	když
Nema	Nema	k1gMnSc1	Nema
spatřil	spatřit	k5eAaPmAgMnS	spatřit
<g/>
,	,	kIx,	,
ulovil	ulovit	k5eAaPmAgMnS	ulovit
jej	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
dcerku	dcerka	k1gFnSc4	dcerka
do	do	k7c2	do
akvárka	akvárek	k1gMnSc2	akvárek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
důmyslného	důmyslný	k2eAgInSc2d1	důmyslný
triku	trik	k1gInSc2	trik
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
hledá	hledat	k5eAaImIp3nS	hledat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
přáteli	přítel	k1gMnPc7	přítel
cestu	cesta	k1gFnSc4	cesta
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
noví	nový	k2eAgMnPc1d1	nový
přátelé	přítel	k1gMnPc1	přítel
Nema	Nem	k1gInSc2	Nem
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
obohatili	obohatit	k5eAaPmAgMnP	obohatit
o	o	k7c4	o
cenné	cenný	k2eAgFnPc4d1	cenná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
šťastné	šťastný	k2eAgNnSc4d1	šťastné
shledání	shledání	k1gNnSc4	shledání
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
tečkou	tečka	k1gFnSc7	tečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdor	vzdor	k1gInSc1	vzdor
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
opravdu	opravdu	k9	opravdu
nemusí	muset	k5eNaImIp3nS	muset
vždy	vždy	k6eAd1	vždy
skončit	skončit	k5eAaPmF	skončit
tragicky	tragicky	k6eAd1	tragicky
<g/>
,	,	kIx,	,
ano	ano	k9	ano
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
do	do	k7c2	do
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
však	však	k9	však
dostávají	dostávat	k5eAaImIp3nP	dostávat
více	hodně	k6eAd2	hodně
vlastní	vlastní	k2eAgFnSc7d1	vlastní
zásluhou	zásluha	k1gFnSc7	zásluha
než	než	k8xS	než
zásluhou	zásluha	k1gFnSc7	zásluha
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
něco	něco	k3yInSc1	něco
takového	takový	k3xDgMnSc4	takový
podstupují	podstupovat	k5eAaImIp3nP	podstupovat
<g/>
.	.	kIx.	.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k6eAd1	Nemo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
o	o	k7c6	o
snaze	snaha	k1gFnSc6	snaha
kteréhokoliv	kterýkoliv	k3yIgNnSc2	kterýkoliv
dítěte	dítě	k1gNnSc2	dítě
osamostatnit	osamostatnit	k5eAaPmF	osamostatnit
se	se	k3xPyFc4	se
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
nám	my	k3xPp1nPc3	my
první	první	k4xOgInPc4	první
zprvu	zprvu	k6eAd1	zprvu
nešikovné	šikovný	k2eNgInPc4d1	nešikovný
kroky	krok	k1gInPc4	krok
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
snaze	snaha	k1gFnSc6	snaha
poznávat	poznávat	k5eAaImF	poznávat
svět	svět	k1gInSc4	svět
bez	bez	k7c2	bez
příkazů	příkaz	k1gInPc2	příkaz
a	a	k8xC	a
zákazů	zákaz	k1gInPc2	zákaz
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
úsudek	úsudek	k1gInSc4	úsudek
<g/>
,	,	kIx,	,
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
do	do	k7c2	do
života	život	k1gInSc2	život
odnesou	odnést	k5eAaPmIp3nP	odnést
to	ten	k3xDgNnSc4	ten
lepší	lepší	k1gNnSc4	lepší
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ty	ten	k3xDgMnPc4	ten
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
složil	složit	k5eAaPmAgMnS	složit
Thomas	Thomas	k1gMnSc1	Thomas
Newman	Newman	k1gMnSc1	Newman
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Rendyho	Rendy	k1gMnSc2	Rendy
Newmana	Newman	k1gMnSc2	Newman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skládal	skládat	k5eAaImAgMnS	skládat
hudbu	hudba	k1gFnSc4	hudba
skoro	skoro	k6eAd1	skoro
ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
filmům	film	k1gInPc3	film
od	od	k7c2	od
studia	studio	k1gNnSc2	studio
Pixar	Pixara	k1gFnPc2	Pixara
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k1gMnSc1	Nemo
<g/>
.	.	kIx.	.
</s>
<s>
Randy	rand	k1gInPc7	rand
Newman	Newman	k1gMnSc1	Newman
tehdy	tehdy	k6eAd1	tehdy
přišel	přijít	k5eAaPmAgMnS	přijít
i	i	k9	i
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
do	do	k7c2	do
titulků	titulek	k1gInPc2	titulek
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
zazpíval	zazpívat	k5eAaPmAgMnS	zazpívat
nějaký	nějaký	k3yIgMnSc1	nějaký
tehdy	tehdy	k6eAd1	tehdy
slavný	slavný	k2eAgMnSc1d1	slavný
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Beyond	Beyond	k1gInSc1	Beyond
The	The	k1gMnSc2	The
Sea	Sea	k1gMnSc2	Sea
<g/>
"	"	kIx"	"
tedy	tedy	k9	tedy
nazpíval	nazpívat	k5eAaPmAgInS	nazpívat
Robbie	Robbie	k1gFnSc2	Robbie
Williams	Williams	k1gInSc1	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaImNgInS	věnovat
památce	památka	k1gFnSc3	památka
Glenna	Glenno	k1gNnSc2	Glenno
McQuinna	McQuinn	k1gMnSc2	McQuinn
-	-	kIx~	-
animátora	animátor	k1gMnSc2	animátor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dokončení	dokončení	k1gNnSc6	dokončení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
příjmení	příjmení	k1gNnSc2	příjmení
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
fiktivní	fiktivní	k2eAgNnSc1d1	fiktivní
Auto	auto	k1gNnSc1	auto
Blesk	blesk	k1gInSc1	blesk
McQuinn	McQuinno	k1gNnPc2	McQuinno
<g/>
.	.	kIx.	.
</s>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
McQuinn	McQuinn	k1gMnSc1	McQuinn
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
animátor	animátor	k1gMnSc1	animátor
z	z	k7c2	z
Pixaru	Pixar	k1gInSc2	Pixar
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
produkoval	produkovat	k5eAaImAgMnS	produkovat
také	také	k9	také
velikán	velikán	k1gMnSc1	velikán
z	z	k7c2	z
Pixaru	Pixar	k1gInSc2	Pixar
John	John	k1gMnSc1	John
Lasseter	Lasseter	k1gMnSc1	Lasseter
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
zrežíroval	zrežírovat	k5eAaPmAgMnS	zrežírovat
film	film	k1gInSc4	film
Toy	Toy	k1gMnSc2	Toy
Story	story	k1gFnSc2	story
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
pokračování	pokračování	k1gNnSc2	pokračování
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Dory	Dora	k1gFnPc4	Dora
<g/>
.	.	kIx.	.
</s>
