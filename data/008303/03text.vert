<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
krásných	krásný	k2eAgMnPc2d1	krásný
srnců	srnec	k1gMnPc2	srnec
(	(	kIx(	(
<g/>
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
osmi	osm	k4xCc2	osm
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ota	Ota	k1gMnSc1	Ota
Pavel	Pavel	k1gMnSc1	Pavel
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
šťastné	šťastný	k2eAgNnSc4d1	šťastné
dětství	dětství	k1gNnSc4	dětství
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
i	i	k8xC	i
hořký	hořký	k2eAgInSc1d1	hořký
okupační	okupační	k2eAgInSc1d1	okupační
úděl	úděl	k1gInSc1	úděl
smíšené	smíšený	k2eAgFnSc2d1	smíšená
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
jako	jako	k8xC	jako
předloha	předloha	k1gFnSc1	předloha
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
autorův	autorův	k2eAgMnSc1d1	autorův
otec	otec	k1gMnSc1	otec
Leo	Leo	k1gMnSc1	Leo
Popper	Popper	k1gMnSc1	Popper
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ideální	ideální	k2eAgMnSc1d1	ideální
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
malá	malý	k2eAgNnPc4d1	malé
milostná	milostný	k2eAgNnPc4d1	milostné
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
a	a	k8xC	a
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
rybám	ryba	k1gFnPc3	ryba
<g/>
,	,	kIx,	,
vodě	voda	k1gFnSc3	voda
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
maminka	maminka	k1gFnSc1	maminka
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
k	k	k7c3	k
otcovým	otcův	k2eAgFnPc3d1	otcova
avantýrám	avantýra	k1gFnPc3	avantýra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
strejda	strejda	k1gMnSc1	strejda
Prošek	Prošek	k1gMnSc1	Prošek
z	z	k7c2	z
Luhu	luh	k1gInSc2	luh
pod	pod	k7c7	pod
Branovem	Branov	k1gInSc7	Branov
<g/>
,	,	kIx,	,
všeuměl	všeuměl	k1gMnSc1	všeuměl
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
pytláků	pytlák	k1gMnPc2	pytlák
<g/>
,	,	kIx,	,
sžitý	sžitý	k2eAgMnSc1d1	sžitý
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
u	u	k7c2	u
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
a	a	k8xC	a
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
povídce	povídka	k1gFnSc6	povídka
Nejdražší	drahý	k2eAgFnSc1d3	nejdražší
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
tatínek	tatínek	k1gMnSc1	tatínek
naletěl	naletět	k5eAaPmAgMnS	naletět
a	a	k8xC	a
koupil	koupit	k5eAaPmAgMnS	koupit
rybník	rybník	k1gInSc4	rybník
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
rybou	ryba	k1gFnSc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
nenechal	nechat	k5eNaPmAgMnS	nechat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
líbit	líbit	k5eAaImF	líbit
a	a	k8xC	a
pomstil	pomstít	k5eAaPmAgMnS	pomstít
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bývalému	bývalý	k2eAgMnSc3d1	bývalý
majiteli	majitel	k1gMnSc3	majitel
rybníka	rybník	k1gInSc2	rybník
prodal	prodat	k5eAaPmAgMnS	prodat
lednici	lednice	k1gFnSc4	lednice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
vnitřku	vnitřek	k1gInSc2	vnitřek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
udělal	udělat	k5eAaPmAgMnS	udělat
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
povídka	povídka	k1gFnSc1	povídka
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Švédska	Švédsko	k1gNnSc2	Švédsko
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
tatínkovu	tatínkův	k2eAgFnSc4d1	tatínkova
podnikavost	podnikavost	k1gFnSc4	podnikavost
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Electrolux	Electrolux	k1gInSc1	Electrolux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
vysavačů	vysavač	k1gInPc2	vysavač
a	a	k8xC	a
ledniček	lednička	k1gFnPc2	lednička
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
díky	díky	k7c3	díky
paní	paní	k1gFnSc3	paní
Irmě	Irma	k1gFnSc3	Irma
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgInS	být
tajně	tajně	k6eAd1	tajně
zamilován	zamilován	k2eAgInSc1d1	zamilován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
Smrt	smrt	k1gFnSc1	smrt
krásných	krásný	k2eAgMnPc2d1	krásný
srnců	srnec	k1gMnPc2	srnec
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejznámější	známý	k2eAgInPc4d3	nejznámější
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
odvážný	odvážný	k2eAgMnSc1d1	odvážný
otec	otec	k1gMnSc1	otec
dojel	dojet	k5eAaPmAgMnS	dojet
s	s	k7c7	s
Davidovou	Davidův	k2eAgFnSc7d1	Davidova
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
rozvrzaném	rozvrzaný	k2eAgInSc6d1	rozvrzaný
bicyklu	bicykl	k1gInSc6	bicykl
pro	pro	k7c4	pro
srnce	srnec	k1gMnPc4	srnec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jím	on	k3xPp3gNnSc7	on
nasytil	nasytit	k5eAaPmAgInS	nasytit
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
než	než	k8xS	než
odejdou	odejít	k5eAaPmIp3nP	odejít
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
se	se	k3xPyFc4	se
bál	bát	k5eAaImAgMnS	bát
i	i	k9	i
kurážný	kurážný	k2eAgMnSc1d1	kurážný
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pytláctví	pytláctví	k1gNnSc2	pytláctví
a	a	k8xC	a
přechovávání	přechovávání	k1gNnSc2	přechovávání
"	"	kIx"	"
<g/>
žida	žid	k1gMnSc2	žid
<g/>
"	"	kIx"	"
hrozila	hrozit	k5eAaImAgFnS	hrozit
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Proškovým	Proškův	k2eAgMnSc7d1	Proškův
divokým	divoký	k2eAgMnSc7d1	divoký
vlčákem	vlčák	k1gMnSc7	vlčák
Holanem	Holan	k1gMnSc7	Holan
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
skutečně	skutečně	k6eAd1	skutečně
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
možná	možná	k9	možná
právě	právě	k9	právě
ten	ten	k3xDgMnSc1	ten
srnec	srnec	k1gMnSc1	srnec
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc1	život
Jiřímu	Jiří	k1gMnSc3	Jiří
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
40	[number]	k4	40
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
Kapři	kapr	k1gMnPc1	kapr
pro	pro	k7c4	pro
Wehrmacht	wehrmacht	k1gFnSc4	wehrmacht
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zfilmována	zfilmovat	k5eAaPmNgFnS	zfilmovat
a	a	k8xC	a
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tatínkovi	tatínkův	k2eAgMnPc1d1	tatínkův
seberou	sebrat	k5eAaPmIp3nP	sebrat
jeho	on	k3xPp3gInSc4	on
milovaný	milovaný	k2eAgInSc4d1	milovaný
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nemůže	moct	k5eNaImIp3nS	moct
přenést	přenést	k5eAaPmF	přenést
přes	přes	k7c4	přes
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
výlovů	výlov	k1gInPc2	výlov
chodí	chodit	k5eAaImIp3nS	chodit
"	"	kIx"	"
<g/>
své	své	k1gNnSc4	své
<g/>
"	"	kIx"	"
kapříky	kapřík	k1gMnPc7	kapřík
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tajně	tajně	k6eAd1	tajně
všechny	všechen	k3xTgMnPc4	všechen
vyloví	vylovit	k5eAaPmIp3nS	vylovit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přijedou	přijet	k5eAaPmIp3nP	přijet
ten	ten	k3xDgInSc4	ten
spodní	spodní	k2eAgInSc4d1	spodní
buštěhradský	buštěhradský	k2eAgInSc4d1	buštěhradský
rybník	rybník	k1gInSc4	rybník
lovit	lovit	k5eAaImF	lovit
sítěmi	síť	k1gFnPc7	síť
rybáři	rybář	k1gMnPc1	rybář
v	v	k7c6	v
uniformách	uniforma	k1gFnPc6	uniforma
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
velká	velký	k2eAgFnSc1d1	velká
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
slibně	slibně	k6eAd1	slibně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
rybníce	rybník	k1gInSc6	rybník
nic	nic	k3yNnSc4	nic
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
muzika	muzika	k1gFnSc1	muzika
vlastně	vlastně	k9	vlastně
hraje	hrát	k5eAaImIp3nS	hrát
už	už	k6eAd1	už
jenom	jenom	k9	jenom
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
Davidovou	Davidův	k2eAgFnSc7d1	Davidova
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
kabátě	kabát	k1gInSc6	kabát
vypálil	vypálit	k5eAaPmAgInS	vypálit
Němcům	Němec	k1gMnPc3	Němec
rybník	rybník	k1gInSc4	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k8xS	jak
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
Vlky	Vlk	k1gMnPc7	Vlk
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tatínek	tatínek	k1gMnSc1	tatínek
prohrál	prohrát	k5eAaPmAgMnS	prohrát
soutěž	soutěž	k1gFnSc4	soutěž
s	s	k7c7	s
Vlkovými	Vlková	k1gFnPc7	Vlková
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
uloví	ulovit	k5eAaPmIp3nS	ulovit
větší	veliký	k2eAgFnSc4d2	veliký
štiku	štika	k1gFnSc4	štika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
tatínek	tatínek	k1gMnSc1	tatínek
hrdě	hrdě	k6eAd1	hrdě
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
své	svůj	k3xOyFgInPc4	svůj
úlovky	úlovek	k1gInPc4	úlovek
k	k	k7c3	k
Vlkovým	Vlková	k1gFnPc3	Vlková
<g/>
,	,	kIx,	,
všiml	všimnout	k5eAaPmAgMnS	všimnout
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gFnPc4	jejich
štiky	štika	k1gFnPc4	štika
visící	visící	k2eAgFnPc4d1	visící
před	před	k7c7	před
domem	dům	k1gInSc7	dům
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
otočil	otočit	k5eAaPmAgInS	otočit
a	a	k8xC	a
bez	bez	k7c2	bez
jediného	jediný	k2eAgNnSc2d1	jediné
slova	slovo	k1gNnSc2	slovo
zklamaně	zklamaně	k6eAd1	zklamaně
zamířil	zamířit	k5eAaPmAgInS	zamířit
zpátky	zpátky	k6eAd1	zpátky
k	k	k7c3	k
domovu	domov	k1gInSc3	domov
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
povídka	povídka	k1gFnSc1	povídka
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
přesně	přesně	k6eAd1	přesně
povahu	povaha	k1gFnSc4	povaha
otce	otec	k1gMnSc2	otec
Oty	Ota	k1gMnSc2	Ota
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šestou	šestý	k4xOgFnSc7	šestý
povídkou	povídka	k1gFnSc7	povídka
je	být	k5eAaImIp3nS	být
Otázka	otázka	k1gFnSc1	otázka
hmyzu	hmyz	k1gInSc2	hmyz
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tatínkových	tatínkův	k2eAgInPc6d1	tatínkův
poválečných	poválečný	k2eAgInPc6d1	poválečný
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
mucholapek	mucholapka	k1gFnPc2	mucholapka
Bomba	bomba	k1gFnSc1	bomba
–	–	k?	–
chemik	chemik	k1gMnSc1	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Udělá	udělat	k5eAaPmIp3nS	udělat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
terno	terno	k1gNnSc1	terno
<g/>
,	,	kIx,	,
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
obchodů	obchod	k1gInPc2	obchod
zapojí	zapojit	k5eAaPmIp3nS	zapojit
také	také	k9	také
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jak	jak	k6eAd1	jak
rychle	rychle	k6eAd1	rychle
uspěl	uspět	k5eAaPmAgMnS	uspět
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
rychle	rychle	k6eAd1	rychle
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
slávu	sláva	k1gFnSc4	sláva
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Přicházely	přicházet	k5eAaImAgFnP	přicházet
mu	on	k3xPp3gMnSc3	on
telegramy	telegram	k1gInPc4	telegram
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Pane	Pan	k1gMnSc5	Pan
Popper	Popper	k1gMnSc1	Popper
<g/>
.	.	kIx.	.
</s>
<s>
Mouchy	moucha	k1gFnPc1	moucha
nechcípají	chcípat	k5eNaImIp3nP	chcípat
<g/>
.	.	kIx.	.
</s>
<s>
Spravujou	spravovat	k5eAaImIp3nP	spravovat
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
Jděte	jít	k5eAaImRp2nP	jít
do	do	k7c2	do
prdele	prdel	k1gFnSc2	prdel
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
zázrakem	zázrak	k1gInSc7	zázrak
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
tutéž	týž	k3xTgFnSc4	týž
mucholapku	mucholapka	k1gFnSc4	mucholapka
začali	začít	k5eAaPmAgMnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
Holanďané	Holanďan	k1gMnPc1	Holanďan
jako	jako	k8xS	jako
Fly	Fly	k1gMnSc1	Fly
Killer	Killer	k1gMnSc1	Killer
(	(	kIx(	(
<g/>
zabíječ	zabíječ	k1gMnSc1	zabíječ
much	moucha	k1gFnPc2	moucha
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaplavili	zaplavit	k5eAaPmAgMnP	zaplavit
tím	ten	k3xDgNnSc7	ten
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prase	prase	k1gNnSc1	prase
nebude	být	k5eNaImBp3nS	být
<g/>
!	!	kIx.	!
</s>
<s>
je	být	k5eAaImIp3nS	být
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
komunismu	komunismus	k1gInSc2	komunismus
cenzurována	cenzurován	k2eAgFnSc1d1	cenzurována
<g/>
.	.	kIx.	.
</s>
<s>
Tatínek	tatínek	k1gMnSc1	tatínek
šel	jít	k5eAaImAgMnS	jít
krmit	krmit	k5eAaImF	krmit
prasata	prase	k1gNnPc4	prase
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
sděleno	sdělen	k2eAgNnSc1d1	sděleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jedno	jeden	k4xCgNnSc4	jeden
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Maminka	maminka	k1gFnSc1	maminka
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
a	a	k8xC	a
rok	rok	k1gInSc4	rok
krmili	krmit	k5eAaImAgMnP	krmit
prasata	prase	k1gNnPc4	prase
<g/>
.	.	kIx.	.
</s>
<s>
Tatínek	tatínek	k1gMnSc1	tatínek
už	už	k6eAd1	už
viděl	vidět	k5eAaImAgMnS	vidět
zabíjačku	zabíjačka	k1gFnSc4	zabíjačka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
prase	prase	k1gNnSc1	prase
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
konec	konec	k1gInSc4	konec
tatínkovi	tatínek	k1gMnSc3	tatínek
přišel	přijít	k5eAaPmAgMnS	přijít
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
dluží	dlužit	k5eAaImIp3nS	dlužit
530,52	[number]	k4	530,52
Ksč	Ksč	k1gFnPc2	Ksč
<g/>
.	.	kIx.	.
</s>
<s>
Tatínek	tatínek	k1gMnSc1	tatínek
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dopis	dopis	k1gInSc1	dopis
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
povídce	povídka	k1gFnSc6	povídka
Králíci	Králík	k1gMnPc1	Králík
s	s	k7c7	s
moudrýma	moudrý	k2eAgNnPc7d1	moudré
očima	oko	k1gNnPc7	oko
prodali	prodat	k5eAaPmAgMnP	prodat
Popperovi	Popperův	k2eAgMnPc1d1	Popperův
chatu	chata	k1gFnSc4	chata
a	a	k8xC	a
koupili	koupit	k5eAaPmAgMnP	koupit
si	se	k3xPyFc3	se
domeček	domeček	k1gInSc4	domeček
u	u	k7c2	u
Radotína	Radotín	k1gInSc2	Radotín
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jejich	jejich	k3xOp3gFnSc1	jejich
poslední	poslední	k2eAgFnSc1d1	poslední
štace	štace	k1gFnSc1	štace
tady	tady	k6eAd1	tady
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
štace	štace	k1gFnSc1	štace
šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
.	.	kIx.	.
</s>
<s>
Tatínek	tatínek	k1gMnSc1	tatínek
si	se	k3xPyFc3	se
zavedl	zavést	k5eAaPmAgMnS	zavést
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
chov	chov	k1gInSc4	chov
králíků	králík	k1gMnPc2	králík
šampanů	šampan	k1gInPc2	šampan
a	a	k8xC	a
maminka	maminka	k1gFnSc1	maminka
zatím	zatím	k6eAd1	zatím
úspěšně	úspěšně	k6eAd1	úspěšně
chovala	chovat	k5eAaImAgFnS	chovat
krocany	krocan	k1gMnPc4	krocan
<g/>
,	,	kIx,	,
slepice	slepice	k1gFnPc4	slepice
a	a	k8xC	a
prodávala	prodávat	k5eAaImAgNnP	prodávat
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Takhle	takhle	k6eAd1	takhle
to	ten	k3xDgNnSc1	ten
táhli	táhnout	k5eAaImAgMnP	táhnout
skoro	skoro	k6eAd1	skoro
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tatínek	tatínek	k1gMnSc1	tatínek
své	svůj	k3xOyFgMnPc4	svůj
králíky	králík	k1gMnPc4	králík
brával	brávat	k5eAaImAgMnS	brávat
na	na	k7c4	na
výlety	výlet	k1gInPc4	výlet
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
i	i	k9	i
na	na	k7c4	na
výstavu	výstava	k1gFnSc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
odešel	odejít	k5eAaPmAgInS	odejít
dotčený	dotčený	k2eAgInSc1d1	dotčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pustil	pustit	k5eAaPmAgMnS	pustit
na	na	k7c6	na
pasece	paseka	k1gFnSc6	paseka
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
ráno	ráno	k6eAd1	ráno
v	v	k7c6	v
tak	tak	k6eAd1	tak
zbědovaném	zbědovaný	k2eAgInSc6d1	zbědovaný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
maminka	maminka	k1gFnSc1	maminka
lekla	leknout	k5eAaPmAgFnS	leknout
a	a	k8xC	a
zavolala	zavolat	k5eAaPmAgFnS	zavolat
sanitku	sanitka	k1gFnSc4	sanitka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
ještě	ještě	k6eAd1	ještě
pověsil	pověsit	k5eAaPmAgMnS	pověsit
na	na	k7c4	na
vrátka	vrátka	k1gNnPc4	vrátka
cedulku	cedulka	k1gFnSc4	cedulka
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
PŘIJDU	přijít	k5eAaPmIp1nS	přijít
HNED	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nepřišel	přijít	k5eNaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
krásných	krásný	k2eAgMnPc2d1	krásný
srnců	srnec	k1gMnPc2	srnec
<g/>
,	,	kIx,	,
Databáze	databáze	k1gFnSc1	databáze
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
