<s>
Smrt	smrt	k1gFnSc1	smrt
krásných	krásný	k2eAgMnPc2d1	krásný
srnců	srnec	k1gMnPc2	srnec
(	(	kIx(	(
<g/>
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
osmi	osm	k4xCc2	osm
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ota	Ota	k1gMnSc1	Ota
Pavel	Pavel	k1gMnSc1	Pavel
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
šťastné	šťastný	k2eAgNnSc4d1	šťastné
dětství	dětství	k1gNnSc4	dětství
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
i	i	k8xC	i
hořký	hořký	k2eAgInSc1d1	hořký
okupační	okupační	k2eAgInSc1d1	okupační
úděl	úděl	k1gInSc1	úděl
smíšené	smíšený	k2eAgFnSc2d1	smíšená
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
