<s>
Palauská	Palauský	k2eAgFnSc1d1	Palauská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceáně	oceán	k1gInSc6	oceán
asi	asi	k9	asi
500	[number]	k4	500
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
