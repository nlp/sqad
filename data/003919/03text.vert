<s>
Palauská	Palauský	k2eAgFnSc1d1	Palauská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceáně	oceán	k1gInSc6	oceán
asi	asi	k9	asi
500	[number]	k4	500
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
nejzápadnější	západní	k2eAgFnSc4d3	nejzápadnější
součást	součást	k1gFnSc4	součást
souostroví	souostroví	k1gNnSc2	souostroví
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
bylo	být	k5eAaImAgNnS	být
Palau	Pala	k2eAgFnSc4d1	Pala
poručenským	poručenský	k2eAgNnSc7d1	poručenský
územím	území	k1gNnSc7	území
OSN	OSN	kA	OSN
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Palauská	Palauský	k2eAgFnSc1d1	Palauská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnSc7	metropol
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Koror	Korora	k1gFnPc2	Korora
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
však	však	k9	však
sídlo	sídlo	k1gNnSc1	sídlo
Ngerulmud	Ngerulmuda	k1gFnPc2	Ngerulmuda
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Melekeok	Melekeok	k1gInSc1	Melekeok
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Babelthuab	Babelthuab	k1gInSc1	Babelthuab
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
palauské	palauský	k2eAgFnSc2d1	palauský
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
zahaleny	zahalit	k5eAaPmNgInP	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Palau	Palaus	k1gInSc2	Palaus
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnPc1	Indonésie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
Guineje	Guinea	k1gFnPc1	Guinea
a	a	k8xC	a
Polynésie	Polynésie	k1gFnPc1	Polynésie
Před	před	k7c7	před
obdobím	období	k1gNnSc7	období
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
měli	mít	k5eAaImAgMnP	mít
Palauané	Palauan	k1gMnPc1	Palauan
komplexní	komplexní	k2eAgFnSc4d1	komplexní
matriarchální	matriarchální	k2eAgNnSc4d1	matriarchální
společenské	společenský	k2eAgNnSc4d1	společenské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
klanu	klan	k1gInSc2	klan
dědily	dědit	k5eAaImAgFnP	dědit
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ostrovy	ostrov	k1gInPc4	ostrov
spatřil	spatřit	k5eAaPmAgMnS	spatřit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
španělský	španělský	k2eAgMnSc1d1	španělský
objevitel	objevitel	k1gMnSc1	objevitel
Ruy	Ruy	k1gFnSc2	Ruy
López	López	k1gMnSc1	López
de	de	k?	de
Villalobos	Villalobos	k1gMnSc1	Villalobos
roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
si	se	k3xPyFc3	se
ostrovy	ostrov	k1gInPc1	ostrov
oficiálně	oficiálně	k6eAd1	oficiálně
nárokovalo	nárokovat	k5eAaImAgNnS	nárokovat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neučinilo	učinit	k5eNaPmAgNnS	učinit
žádný	žádný	k3yNgInSc4	žádný
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
průzkum	průzkum	k1gInSc4	průzkum
či	či	k8xC	či
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
,	,	kIx,	,
Palauské	Palauský	k2eAgInPc1d1	Palauský
ostrovy	ostrov	k1gInPc1	ostrov
byly	být	k5eAaImAgInP	být
součástí	součást	k1gFnSc7	součást
Španělské	španělský	k2eAgFnSc2d1	španělská
Východní	východní	k2eAgFnSc2d1	východní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
evropský	evropský	k2eAgInSc1d1	evropský
pokus	pokus	k1gInSc1	pokus
ostrovy	ostrov	k1gInPc4	ostrov
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
obchodovat	obchodovat	k5eAaImF	obchodovat
byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaPmNgInS	učinit
až	až	k9	až
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
útesu	útes	k1gInSc6	útes
u	u	k7c2	u
palauského	palauský	k2eAgInSc2d1	palauský
ostrova	ostrov	k1gInSc2	ostrov
Ulongu	Ulong	k1gInSc2	Ulong
(	(	kIx(	(
<g/>
skalnatý	skalnatý	k2eAgInSc1d1	skalnatý
ostrov	ostrov	k1gInSc1	ostrov
ležící	ležící	k2eAgInSc1d1	ležící
mezi	mezi	k7c7	mezi
Kororem	Koror	k1gInSc7	Koror
a	a	k8xC	a
Peleliu	Pelelium	k1gNnSc6	Pelelium
<g/>
)	)	kIx)	)
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
lodí	loď	k1gFnSc7	loď
Antelope	Antelop	k1gInSc5	Antelop
britský	britský	k2eAgMnSc1d1	britský
kapitán	kapitán	k1gMnSc1	kapitán
Henry	Henry	k1gMnSc1	Henry
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
kororského	kororský	k2eAgMnSc2d1	kororský
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
náčelníka	náčelník	k1gMnSc2	náčelník
Ibedula	Ibedulum	k1gNnSc2	Ibedulum
dokázal	dokázat	k5eAaPmAgMnS	dokázat
během	během	k7c2	během
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
Wilson	Wilson	k1gInSc1	Wilson
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
posádkou	posádka	k1gFnSc7	posádka
opravit	opravit	k5eAaPmF	opravit
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
pak	pak	k6eAd1	pak
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
náčelníkova	náčelníkův	k2eAgMnSc2d1	náčelníkův
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
prince	princ	k1gMnSc2	princ
Lebuu	Lebuus	k1gInSc2	Lebuus
<g/>
,	,	kIx,	,
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Lebua	Lebua	k1gMnSc1	Lebua
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
na	na	k7c4	na
pravé	pravý	k2eAgFnPc4d1	pravá
neštovice	neštovice	k1gFnPc4	neštovice
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
podnítila	podnítit	k5eAaPmAgFnS	podnítit
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
Britů	Brit	k1gMnPc2	Brit
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
Palau	Palaus	k1gInSc6	Palaus
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stali	stát	k5eAaPmAgMnP	stát
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
100	[number]	k4	100
let	léto	k1gNnPc2	léto
hlavním	hlavní	k2eAgMnSc7d1	hlavní
palauským	palauský	k2eAgMnSc7d1	palauský
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
<g/>
,	,	kIx,	,
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
nahrazeni	nahrazen	k2eAgMnPc1d1	nahrazen
Španěly	Španěly	k1gInPc4	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
španělští	španělský	k2eAgMnPc1d1	španělský
kněží	kněz	k1gMnPc1	kněz
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
šířit	šířit	k5eAaImF	šířit
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
Španělé	Španěl	k1gMnPc1	Španěl
si	se	k3xPyFc3	se
činili	činit	k5eAaImAgMnP	činit
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
nárok	nárok	k1gInSc4	nárok
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
britských	britský	k2eAgInPc6d1	britský
protestech	protest	k1gInPc6	protest
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
Německo	Německo	k1gNnSc1	Německo
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
některé	některý	k3yIgInPc4	některý
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vyřešením	vyřešení	k1gNnSc7	vyřešení
sporu	spor	k1gInSc2	spor
pověřen	pověřen	k2eAgMnSc1d1	pověřen
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
nárok	nárok	k1gInSc4	nárok
Španělska	Španělsko	k1gNnSc2	Španělsko
na	na	k7c4	na
Karolíny	Karolína	k1gFnPc4	Karolína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
zachování	zachování	k1gNnSc2	zachování
obchodních	obchodní	k2eAgFnPc2d1	obchodní
koncesí	koncese	k1gFnPc2	koncese
pro	pro	k7c4	pro
Brity	Brit	k1gMnPc4	Brit
a	a	k8xC	a
Němce	Němec	k1gMnPc4	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Španělští	španělský	k2eAgMnPc1d1	španělský
kapucínští	kapucínský	k2eAgMnPc1d1	kapucínský
kněží	kněz	k1gMnPc1	kněz
zde	zde	k6eAd1	zde
poté	poté	k6eAd1	poté
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
dva	dva	k4xCgInPc4	dva
kostely	kostel	k1gInPc4	kostel
<g/>
,	,	kIx,	,
zavedli	zavést	k5eAaPmAgMnP	zavést
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
používání	používání	k1gNnSc2	používání
latinské	latinský	k2eAgFnSc2d1	Latinská
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
vymýtili	vymýtit	k5eAaPmAgMnP	vymýtit
zdejší	zdejší	k2eAgFnPc4d1	zdejší
války	válka	k1gFnPc4	válka
mezi	mezi	k7c7	mezi
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c6	po
svojí	svůj	k3xOyFgFnSc6	svůj
porážce	porážka	k1gFnSc6	porážka
ve	v	k7c6	v
Španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
prodali	prodat	k5eAaPmAgMnP	prodat
Palau	Palaa	k1gFnSc4	Palaa
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
Karolín	Karolína	k1gFnPc2	Karolína
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1899	[number]	k4	1899
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
španělští	španělský	k2eAgMnPc1d1	španělský
vojáci	voják	k1gMnPc1	voják
opustili	opustit	k5eAaPmAgMnP	opustit
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
započalo	započnout	k5eAaPmAgNnS	započnout
s	s	k7c7	s
organizovaným	organizovaný	k2eAgNnSc7d1	organizované
využíváním	využívání	k1gNnSc7	využívání
zdejších	zdejší	k2eAgInPc2d1	zdejší
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zakládali	zakládat	k5eAaImAgMnP	zakládat
kokosové	kokosový	k2eAgFnPc4d1	kokosová
plantáže	plantáž	k1gFnPc4	plantáž
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
nutili	nutit	k5eAaImAgMnP	nutit
pracovat	pracovat	k5eAaImF	pracovat
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
západní	západní	k2eAgFnPc1d1	západní
nemoci	nemoc	k1gFnPc1	nemoc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
na	na	k7c4	na
zdejší	zdejší	k2eAgNnSc4d1	zdejší
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
zničující	zničující	k2eAgInSc1d1	zničující
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
Německo	Německo	k1gNnSc1	Německo
Palau	Palaus	k1gInSc2	Palaus
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Západních	západní	k2eAgFnPc2d1	západní
Karolín	Karolína	k1gFnPc2	Karolína
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
Nové	Nové	k2eAgFnSc3d1	Nové
Guineji	Guinea	k1gFnSc3	Guinea
(	(	kIx(	(
<g/>
mapa	mapa	k1gFnSc1	mapa
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
)	)	kIx)	)
a	a	k8xC	a
ovládalo	ovládat	k5eAaImAgNnS	ovládat
je	on	k3xPp3gMnPc4	on
až	až	k6eAd1	až
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gFnPc4	on
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Německa	Německo	k1gNnSc2	Německo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
získalo	získat	k5eAaPmAgNnS	získat
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
bylo	být	k5eAaImAgNnS	být
Palau	Palaa	k1gFnSc4	Palaa
součástí	součást	k1gFnPc2	součást
mandátního	mandátní	k2eAgNnSc2d1	mandátní
území	území	k1gNnSc2	území
Tichomořských	tichomořský	k2eAgInPc2d1	tichomořský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
svěřila	svěřit	k5eAaPmAgFnS	svěřit
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
30	[number]	k4	30
let	léto	k1gNnPc2	léto
japonské	japonský	k2eAgFnSc2d1	japonská
vlády	vláda	k1gFnSc2	vláda
prodělala	prodělat	k5eAaPmAgFnS	prodělat
palauská	palauský	k2eAgFnSc1d1	palauský
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
obrovské	obrovský	k2eAgFnSc2d1	obrovská
změny	změna	k1gFnSc2	změna
<g/>
:	:	kIx,	:
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
byla	být	k5eAaImAgFnS	být
vnucována	vnucován	k2eAgFnSc1d1	vnucována
japonština	japonština	k1gFnSc1	japonština
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
soukromého	soukromý	k2eAgMnSc2d1	soukromý
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc1	místo
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
kmenového	kmenový	k2eAgMnSc2d1	kmenový
<g/>
)	)	kIx)	)
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vesničtí	vesnický	k2eAgMnPc1d1	vesnický
náčelníci	náčelník	k1gMnPc1	náčelník
ztratili	ztratit	k5eAaPmAgMnP	ztratit
moc	moc	k6eAd1	moc
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
japonské	japonský	k2eAgFnSc2d1	japonská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
byrokracie	byrokracie	k1gFnSc2	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Palau	Palaus	k1gInSc6	Palaus
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
přistěhovaly	přistěhovat	k5eAaPmAgFnP	přistěhovat
tisíce	tisíc	k4xCgInPc1	tisíc
japonských	japonský	k2eAgMnPc2d1	japonský
<g/>
,	,	kIx,	,
korejských	korejský	k2eAgMnPc2d1	korejský
a	a	k8xC	a
okinawských	okinawský	k2eAgMnPc2d1	okinawský
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Palauané	Palauan	k1gMnPc1	Palauan
rovněž	rovněž	k9	rovněž
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
tradiční	tradiční	k2eAgInSc4d1	tradiční
dědický	dědický	k2eAgInSc4d1	dědický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ztratili	ztratit	k5eAaPmAgMnP	ztratit
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
prodejem	prodej	k1gInSc7	prodej
či	či	k8xC	či
konfiskacemi	konfiskace	k1gFnPc7	konfiskace
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1922	[number]	k4	1922
pak	pak	k6eAd1	pak
Japonsko	Japonsko	k1gNnSc1	Japonsko
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
Palau	Palaa	k1gFnSc4	Palaa
do	do	k7c2	do
Jihomořské	Jihomořský	k2eAgFnSc2d1	Jihomořský
správní	správní	k2eAgFnSc2d1	správní
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
Nan	Nan	k1gMnSc2	Nan
<g/>
'	'	kIx"	'
<g/>
jó	jó	k0	jó
Čó	Čó	k1gMnSc5	Čó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Palau	Palaus	k1gInSc2	Palaus
<g/>
,	,	kIx,	,
Karolíny	Karolína	k1gFnSc2	Karolína
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Mariany	Mariana	k1gFnSc2	Mariana
<g/>
.	.	kIx.	.
</s>
<s>
Koror	Koror	k1gMnSc1	Koror
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
stal	stát	k5eAaPmAgInS	stát
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
celého	celý	k2eAgNnSc2d1	celé
japonského	japonský	k2eAgNnSc2d1	Japonské
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
,	,	kIx,	,
moderním	moderní	k2eAgNnSc7d1	moderní
stylovým	stylový	k2eAgNnSc7d1	stylové
městem	město	k1gNnSc7	město
s	s	k7c7	s
dlážděnými	dlážděný	k2eAgFnPc7d1	dlážděná
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
a	a	k8xC	a
vodovodní	vodovodní	k2eAgFnSc7d1	vodovodní
sítí	síť	k1gFnSc7	síť
<g/>
,	,	kIx,	,
továrnami	továrna	k1gFnPc7	továrna
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc7	obchod
<g/>
,	,	kIx,	,
lázněmi	lázeň	k1gFnPc7	lázeň
<g/>
,	,	kIx,	,
restauracemi	restaurace	k1gFnPc7	restaurace
a	a	k8xC	a
lékárnami	lékárna	k1gFnPc7	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
Japonsko	Japonsko	k1gNnSc4	Japonsko
celé	celý	k2eAgNnSc4d1	celé
mandátní	mandátní	k2eAgNnSc4d1	mandátní
území	území	k1gNnSc4	území
anektovalo	anektovat	k5eAaBmAgNnS	anektovat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Japonsko	Japonsko	k1gNnSc1	Japonsko
izolovalo	izolovat	k5eAaBmAgNnS	izolovat
Palau	Palaa	k1gFnSc4	Palaa
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
a	a	k8xC	a
započalo	započnout	k5eAaPmAgNnS	započnout
zde	zde	k6eAd1	zde
se	s	k7c7	s
soustředěným	soustředěný	k2eAgNnSc7d1	soustředěné
budováním	budování	k1gNnSc7	budování
řady	řada	k1gFnSc2	řada
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
na	na	k7c6	na
Palau	Palaus	k1gInSc6	Palaus
soustředily	soustředit	k5eAaPmAgFnP	soustředit
japonské	japonský	k2eAgFnPc1d1	japonská
síly	síla	k1gFnPc1	síla
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
jižní	jižní	k2eAgFnPc4d1	jižní
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgFnPc2d1	poslední
fází	fáze	k1gFnPc2	fáze
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
zdejší	zdejší	k2eAgNnPc1d1	zdejší
japonská	japonský	k2eAgNnPc1d1	Japonské
opevnění	opevnění	k1gNnPc1	opevnění
stala	stát	k5eAaPmAgNnP	stát
terčem	terč	k1gInSc7	terč
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c6	na
zdejších	zdejší	k2eAgInPc6d1	zdejší
ostrovech	ostrov	k1gInPc6	ostrov
Peleliu	Pelelius	k1gMnSc3	Pelelius
(	(	kIx(	(
<g/>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Peleliu	Pelelium	k1gNnSc6	Pelelium
<g/>
)	)	kIx)	)
a	a	k8xC	a
Angaur	Angaur	k1gMnSc1	Angaur
(	(	kIx(	(
<g/>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Angaur	Angaur	k1gMnSc1	Angaur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
podstatně	podstatně	k6eAd1	podstatně
lidnatějších	lidnatý	k2eAgInPc6d2	lidnatější
ostrovech	ostrov	k1gInPc6	ostrov
Kororu	Koror	k1gInSc2	Koror
a	a	k8xC	a
Babelthuabu	Babelthuab	k1gInSc2	Babelthuab
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
Japonci	Japonec	k1gMnPc1	Japonec
přemístili	přemístit	k5eAaPmAgMnP	přemístit
většinu	většina	k1gFnSc4	většina
Palauanů	Palauan	k1gInPc2	Palauan
<g/>
)	)	kIx)	)
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
a	a	k8xC	a
japonské	japonský	k2eAgFnPc1d1	japonská
jednotky	jednotka	k1gFnPc1	jednotka
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
ostrovech	ostrov	k1gInPc6	ostrov
byly	být	k5eAaImAgInP	být
izolovány	izolovat	k5eAaBmNgInP	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
pak	pak	k6eAd1	pak
USA	USA	kA	USA
dobyly	dobýt	k5eAaPmAgFnP	dobýt
a	a	k8xC	a
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Peleliu	Pelelium	k1gNnSc6	Pelelium
a	a	k8xC	a
Angaur	Angaura	k1gFnPc2	Angaura
<g/>
.	.	kIx.	.
</s>
<s>
Izolované	izolovaný	k2eAgFnPc1d1	izolovaná
jednotky	jednotka	k1gFnPc1	jednotka
na	na	k7c6	na
ostatních	ostatní	k2eAgInPc6d1	ostatní
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
vzdaly	vzdát	k5eAaPmAgInP	vzdát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1947	[number]	k4	1947
Palauské	Palauský	k2eAgInPc1d1	Palauský
ostrovy	ostrov	k1gInPc1	ostrov
tvořily	tvořit	k5eAaImAgInP	tvořit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgNnSc2d1	zřízené
poručenského	poručenský	k2eAgNnSc2d1	poručenský
území	území	k1gNnSc2	území
Tichomořské	tichomořský	k2eAgInPc4d1	tichomořský
ostrovy	ostrov	k1gInPc4	ostrov
samostatný	samostatný	k2eAgInSc4d1	samostatný
distrikt	distrikt	k1gInSc4	distrikt
Palau	Palaus	k1gInSc2	Palaus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
ostrovních	ostrovní	k2eAgInPc2d1	ostrovní
distriktů	distrikt	k1gInPc2	distrikt
tohoto	tento	k3xDgNnSc2	tento
poručenského	poručenský	k2eAgNnSc2d1	poručenský
území	území	k1gNnSc2	území
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
připravit	připravit	k5eAaPmF	připravit
Palau	Palaa	k1gFnSc4	Palaa
na	na	k7c4	na
budoucí	budoucí	k2eAgFnSc4d1	budoucí
nezávislost	nezávislost	k1gFnSc4	nezávislost
vylepšily	vylepšit	k5eAaPmAgInP	vylepšit
USA	USA	kA	USA
zdejší	zdejší	k2eAgFnSc4d1	zdejší
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
a	a	k8xC	a
školství	školství	k1gNnSc4	školství
<g/>
.	.	kIx.	.
</s>
<s>
Palauané	Palauan	k1gMnPc1	Palauan
se	se	k3xPyFc4	se
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jazykových	jazykový	k2eAgInPc2d1	jazykový
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc2d1	kulturní
rozdílů	rozdíl	k1gInPc2	rozdíl
nepřipojit	připojit	k5eNaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
Federativním	federativní	k2eAgInPc3d1	federativní
státům	stát	k1gInPc3	stát
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Palau	Pala	k2eAgFnSc4d1	Pala
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1981	[number]	k4	1981
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
distriktu	distrikt	k1gInSc2	distrikt
Palau	Palaus	k1gInSc2	Palaus
na	na	k7c4	na
autonomní	autonomní	k2eAgFnSc4d1	autonomní
Palauskou	Palauský	k2eAgFnSc4d1	Palauská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nadále	nadále	k6eAd1	nadále
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
poručenským	poručenský	k2eAgNnSc7d1	poručenský
územím	území	k1gNnSc7	území
OSN	OSN	kA	OSN
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
podepsala	podepsat	k5eAaPmAgFnS	podepsat
Palauská	Palauský	k2eAgFnSc1d1	Palauská
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
USA	USA	kA	USA
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
volném	volný	k2eAgNnSc6d1	volné
přidružení	přidružení	k1gNnSc6	přidružení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
násilné	násilný	k2eAgFnSc3d1	násilná
smrti	smrt	k1gFnSc3	smrt
dvou	dva	k4xCgMnPc2	dva
palauských	palauský	k2eAgMnPc2d1	palauský
prezidentů	prezident	k1gMnPc2	prezident
(	(	kIx(	(
<g/>
Haruo	Haruo	k6eAd1	Haruo
Ignacia	Ignacia	k1gFnSc1	Ignacia
Remeliika	Remeliika	k1gFnSc1	Remeliika
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
a	a	k8xC	a
Lazaruse	Lazarus	k1gInSc6	Lazarus
Eitara	Eitara	k1gFnSc1	Eitara
Salii	Salie	k1gFnSc4	Salie
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1988	[number]	k4	1988
po	po	k7c6	po
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
osmi	osm	k4xCc6	osm
referendech	referendum	k1gNnPc6	referendum
a	a	k8xC	a
dodatku	dodatek	k1gInSc2	dodatek
k	k	k7c3	k
palauské	palauský	k2eAgFnSc3d1	palauský
ústavě	ústava	k1gFnSc3	ústava
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
volném	volný	k2eAgNnSc6d1	volné
přidružení	přidružení	k1gNnSc6	přidružení
schválena	schválit	k5eAaPmNgFnS	schválit
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Palauská	Palauský	k2eAgFnSc1d1	Palauská
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
plně	plně	k6eAd1	plně
samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
zrušila	zrušit	k5eAaPmAgFnS	zrušit
OSN	OSN	kA	OSN
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1994	[number]	k4	1994
poručenskou	poručenský	k2eAgFnSc4d1	Poručenská
správu	správa	k1gFnSc4	správa
nad	nad	k7c4	nad
Palau	Palaa	k1gFnSc4	Palaa
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
Palauské	Palauský	k2eAgFnSc2d1	Palauská
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
USA	USA	kA	USA
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
zde	zde	k6eAd1	zde
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
systém	systém	k1gInSc1	systém
a	a	k8xC	a
federativní	federativní	k2eAgNnSc1d1	federativní
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
volený	volený	k2eAgMnSc1d1	volený
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
s	s	k7c7	s
funkčním	funkční	k2eAgNnSc7d1	funkční
obdobím	období	k1gNnSc7	období
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Palauského	Palauský	k2eAgMnSc4d1	Palauský
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
každé	každý	k3xTgNnSc4	každý
4	[number]	k4	4
roky	rok	k1gInPc4	rok
palauské	palauský	k2eAgNnSc4d1	palauský
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
Tommy	Tomma	k1gFnSc2	Tomma
Esang	Esanga	k1gFnPc2	Esanga
Remengesau	Remengesaus	k1gInSc2	Remengesaus
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
funkce	funkce	k1gFnSc2	funkce
ujal	ujmout	k5eAaPmAgInS	ujmout
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgMnS	být
již	již	k9	již
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2001	[number]	k4	2001
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palau	Palau	k6eAd1	Palau
má	mít	k5eAaImIp3nS	mít
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
zvaný	zvaný	k2eAgInSc1d1	zvaný
Olbiil	Olbiil	k1gInSc1	Olbiil
Era	Era	k1gFnSc2	Era
Kelulau	Kelulaus	k1gInSc2	Kelulaus
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
sněmovny	sněmovna	k1gFnSc2	sněmovna
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
16	[number]	k4	16
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
1	[number]	k4	1
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
palauského	palauský	k2eAgInSc2d1	palauský
spolkového	spolkový	k2eAgInSc2d1	spolkový
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
9	[number]	k4	9
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
období	období	k1gNnSc4	období
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
ze	z	k7c2	z
16	[number]	k4	16
palauských	palauský	k2eAgInPc2d1	palauský
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
volené	volený	k2eAgMnPc4d1	volený
představitele	představitel	k1gMnPc4	představitel
<g/>
.	.	kIx.	.
</s>
<s>
Palau	Palau	k6eAd1	Palau
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
16	[number]	k4	16
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Aimeliik	Aimeliika	k1gFnPc2	Aimeliika
Airai	Airai	k1gNnPc2	Airai
Angaur	Angaur	k1gMnSc1	Angaur
Hatohobei	Hatohobe	k1gFnSc2	Hatohobe
Kayangel	Kayangel	k1gMnSc1	Kayangel
Koror	Korora	k1gFnPc2	Korora
Melekeok	Melekeok	k1gInSc1	Melekeok
Ngaraard	Ngaraard	k1gMnSc1	Ngaraard
Ngarchelong	Ngarchelong	k1gMnSc1	Ngarchelong
Ngardmau	Ngardmaus	k1gInSc2	Ngardmaus
Ngatpang	Ngatpang	k1gMnSc1	Ngatpang
Ngchesar	Ngchesar	k1gMnSc1	Ngchesar
Ngeremlengui	Ngeremlengu	k1gMnSc3	Ngeremlengu
Ngiwal	Ngiwal	k1gInSc4	Ngiwal
Peleliu	Pelelium	k1gNnSc3	Pelelium
Sonsorol	Sonsorola	k1gFnPc2	Sonsorola
Palau	Palaus	k1gInSc2	Palaus
je	být	k5eAaImIp3nS	být
skupinou	skupina	k1gFnSc7	skupina
26	[number]	k4	26
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
asi	asi	k9	asi
300	[number]	k4	300
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
všechny	všechen	k3xTgFnPc4	všechen
kromě	kromě	k7c2	kromě
6	[number]	k4	6
leží	ležet	k5eAaImIp3nS	ležet
uvnitř	uvnitř	k7c2	uvnitř
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
laguny	laguna	k1gFnSc2	laguna
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
bariérovým	bariérový	k2eAgInSc7d1	bariérový
útesem	útes	k1gInSc7	útes
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nejzápadnější	západní	k2eAgMnPc1d3	nejzápadnější
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ostrovních	ostrovní	k2eAgFnPc2d1	ostrovní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgNnPc2d1	tvořící
souostroví	souostroví	k1gNnPc2	souostroví
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
obydleno	obydlet	k5eAaPmNgNnS	obydlet
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Babelthuap	Babelthuap	k1gInSc1	Babelthuap
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
důležitým	důležitý	k2eAgInPc3d1	důležitý
ostrovům	ostrov	k1gInPc3	ostrov
náleží	náležet	k5eAaImIp3nS	náležet
Angaur	Angaur	k1gMnSc1	Angaur
<g/>
,	,	kIx,	,
Koror	Koror	k1gMnSc1	Koror
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
palauského	palauský	k2eAgNnSc2d1	palauský
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Peleliu	Pelelius	k1gMnSc3	Pelelius
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Babelthuapu	Babelthuap	k1gInSc2	Babelthuap
leží	ležet	k5eAaImIp3nS	ležet
korálový	korálový	k2eAgInSc4d1	korálový
atol	atol	k1gInSc4	atol
Kayangel	Kayangela	k1gFnPc2	Kayangela
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Kororu	Koror	k1gInSc2	Koror
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
státě	stát	k1gInSc6	stát
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
skupina	skupina	k1gFnSc1	skupina
asi	asi	k9	asi
300	[number]	k4	300
neobydlených	obydlený	k2eNgInPc2d1	neobydlený
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Rock	rock	k1gInSc1	rock
Islands	Islands	k1gInSc4	Islands
(	(	kIx(	(
<g/>
světové	světový	k2eAgNnSc4d1	světové
přírodní	přírodní	k2eAgNnSc4d1	přírodní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
600	[number]	k4	600
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Angaur	Angaura	k1gFnPc2	Angaura
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
skupina	skupina	k1gFnSc1	skupina
Jihozápadních	jihozápadní	k2eAgInPc2d1	jihozápadní
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
Palauské	Palauský	k2eAgFnSc2d1	Palauská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Palau	Palau	k6eAd1	Palau
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nP	sklízet
kokosové	kokosový	k2eAgInPc1d1	kokosový
ořechy	ořech	k1gInPc1	ořech
(	(	kIx(	(
<g/>
kopra	kopra	k1gFnSc1	kopra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc4	maniok
a	a	k8xC	a
tropické	tropický	k2eAgNnSc4d1	tropické
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
na	na	k7c6	na
podmořském	podmořský	k2eAgInSc6d1	podmořský
hřbetě	hřbet	k1gInSc6	hřbet
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
lemovány	lemován	k2eAgMnPc4d1	lemován
hlubokokomořským	hlubokokomořský	k2eAgInSc7d1	hlubokokomořský
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
hlubokým	hluboký	k2eAgInSc7d1	hluboký
8138	[number]	k4	8138
m.	m.	k?	m.
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
a	a	k8xC	a
Bruneje	Brunej	k1gInSc2	Brunej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nulový	nulový	k2eAgInSc4d1	nulový
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Palau	Palau	k6eAd1	Palau
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
ke	k	k7c3	k
koalici	koalice	k1gFnSc3	koalice
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
Válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
teroru	teror	k1gInSc3	teror
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Palau	Palaus	k1gInSc2	Palaus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Palau	Palaus	k1gInSc2	Palaus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Palau	Palaus	k1gInSc2	Palaus
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
FOSTER	FOSTER	kA	FOSTER
<g/>
,	,	kIx,	,
Sophie	Sophie	k1gFnSc1	Sophie
<g/>
;	;	kIx,	;
SHUSTER	SHUSTER	kA	SHUSTER
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Raymond	Raymond	k1gMnSc1	Raymond
<g/>
.	.	kIx.	.
</s>
<s>
Palau	Palau	k5eAaPmIp1nS	Palau
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
