<s>
Lenka	Lenka	k1gFnSc1	Lenka
Lanczová	Lanczová	k1gFnSc1	Lanczová
<g/>
,	,	kIx,	,
narozena	narozen	k2eAgFnSc1d1	narozena
jako	jako	k8xC	jako
Lenka	Lenka	k1gFnSc1	Lenka
Mužáková	Mužáková	k1gFnSc1	Mužáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1964	[number]	k4	1964
v	v	k7c6	v
Dačicích	Dačice	k1gFnPc6	Dačice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
románů	román	k1gInPc2	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
knihovně	knihovna	k1gFnSc6	knihovna
ve	v	k7c6	v
Slavonicích	Slavonik	k1gInPc6	Slavonik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
chodila	chodit	k5eAaImAgFnS	chodit
ve	v	k7c6	v
Slavonicích	Slavonice	k1gFnPc6	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
na	na	k7c6	na
Střední	střední	k2eAgFnSc6d1	střední
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
pracovala	pracovat	k5eAaImAgFnS	pracovat
rok	rok	k1gInSc4	rok
u	u	k7c2	u
Okresní	okresní	k2eAgFnSc2d1	okresní
správy	správa	k1gFnSc2	správa
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dívčích	dívčí	k2eAgInPc6d1	dívčí
románech	román	k1gInPc6	román
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
tam	tam	k6eAd1	tam
chtěla	chtít	k5eAaImAgFnS	chtít
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nevinného	vinný	k2eNgInSc2d1	nevinný
pokusu	pokus	k1gInSc2	pokus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
celoživotní	celoživotní	k2eAgInSc1d1	celoživotní
koníček	koníček	k1gInSc1	koníček
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Lenka	Lenka	k1gFnSc1	Lenka
Lanczová	Lanczový	k2eAgFnSc1d1	Lanczová
autorkou	autorka	k1gFnSc7	autorka
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
román	román	k1gInSc1	román
však	však	k9	však
vyšel	vyjít	k5eAaPmAgInS	vyjít
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
r.	r.	kA	r.
1989	[number]	k4	1989
změnila	změnit	k5eAaPmAgFnS	změnit
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
do	do	k7c2	do
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
edičních	ediční	k2eAgInPc2d1	ediční
plánů	plán	k1gInPc2	plán
kamenných	kamenný	k2eAgNnPc2d1	kamenné
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
nevešla	vejít	k5eNaPmAgFnS	vejít
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
českým	český	k2eAgMnPc3d1	český
nejproduktivnějším	produktivní	k2eAgMnPc3d3	nejproduktivnější
a	a	k8xC	a
nejčtenějším	čtený	k2eAgMnPc3d3	nejčtenější
spisovatelům	spisovatel	k1gMnPc3	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
jejích	její	k3xOp3gFnPc2	její
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
dospívající	dospívající	k2eAgFnSc1d1	dospívající
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
řeší	řešit	k5eAaImIp3nS	řešit
své	svůj	k3xOyFgInPc4	svůj
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
mezilidskými	mezilidský	k2eAgInPc7d1	mezilidský
vztahy	vztah	k1gInPc7	vztah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
způsobené	způsobený	k2eAgFnPc1d1	způsobená
její	její	k3xOp3gFnSc4	její
odlišností	odlišnost	k1gFnSc7	odlišnost
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
mladých	mladá	k1gFnPc6	mladá
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
knih	kniha	k1gFnPc2	kniha
čiší	čišet	k5eAaImIp3nS	čišet
jakési	jakýsi	k3yIgNnSc4	jakýsi
ponaučení	ponaučení	k1gNnSc4	ponaučení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
podání	podání	k1gNnSc3	podání
to	ten	k3xDgNnSc4	ten
čtenáři	čtenář	k1gMnPc1	čtenář
nevnímají	vnímat	k5eNaImIp3nP	vnímat
jako	jako	k9	jako
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
mentorování	mentorování	k1gNnSc4	mentorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dvoudílném	dvoudílný	k2eAgInSc6d1	dvoudílný
románu	román	k1gInSc6	román
Lucky	lucky	k6eAd1	lucky
Luk	luk	k1gInSc4	luk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
výjimkou	výjimka	k1gFnSc7	výjimka
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
starší	starší	k1gMnSc1	starší
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
když	když	k8xS	když
potkává	potkávat	k5eAaImIp3nS	potkávat
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
osudovou	osudový	k2eAgFnSc4d1	osudová
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
také	také	k9	také
potýká	potýkat	k5eAaImIp3nS	potýkat
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
představuje	představovat	k5eAaImIp3nS	představovat
každá	každý	k3xTgFnSc1	každý
kniha	kniha	k1gFnSc1	kniha
samostatný	samostatný	k2eAgInSc4d1	samostatný
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Lanczová	Lanczová	k1gFnSc1	Lanczová
vydala	vydat	k5eAaPmAgFnS	vydat
také	také	k9	také
tři	tři	k4xCgInPc4	tři
romány	román	k1gInPc4	román
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
-	-	kIx~	-
Manželky	manželka	k1gFnPc4	manželka
<g/>
,	,	kIx,	,
milenky	milenka	k1gFnPc4	milenka
<g/>
,	,	kIx,	,
zoufalky	zoufalka	k1gFnPc4	zoufalka
<g/>
,	,	kIx,	,
Milenky	Milenka	k1gFnPc4	Milenka
a	a	k8xC	a
hříšníci	hříšník	k1gMnPc1	hříšník
a	a	k8xC	a
Vstupenka	vstupenka	k1gFnSc1	vstupenka
do	do	k7c2	do
ráje	ráj	k1gInSc2	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
vydává	vydávat	k5eAaPmIp3nS	vydávat
průměrně	průměrně	k6eAd1	průměrně
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
vydala	vydat	k5eAaPmAgFnS	vydat
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
vesměs	vesměs	k6eAd1	vesměs
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Víkend	víkend	k1gInSc1	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jí	jíst	k5eAaImIp3nS	jíst
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
či	či	k8xC	či
spíše	spíše	k9	spíše
novela	novela	k1gFnSc1	novela
<g/>
)	)	kIx)	)
Andělé	anděl	k1gMnPc1	anděl
noci	noc	k1gFnSc2	noc
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
pěti	pět	k4xCc7	pět
autorkami	autorka	k1gFnPc7	autorka
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
než	než	k8xS	než
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
autorčina	autorčin	k2eAgFnSc1d1	autorčina
dcera	dcera	k1gFnSc1	dcera
Sandra	Sandra	k1gFnSc1	Sandra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorčiny	autorčin	k2eAgFnPc1d1	autorčina
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
seznamu	seznam	k1gInSc6	seznam
seřazeny	seřadit	k5eAaPmNgInP	seřadit
podle	podle	k7c2	podle
roku	rok	k1gInSc2	rok
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
předchozího	předchozí	k2eAgInSc2d1	předchozí
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
pokračování	pokračování	k1gNnSc1	pokračování
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
hned	hned	k6eAd1	hned
za	za	k7c4	za
úvodní	úvodní	k2eAgInSc4d1	úvodní
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Moje	můj	k3xOp1gFnSc1	můj
kniha	kniha	k1gFnSc1	kniha
obsadila	obsadit	k5eAaPmAgFnS	obsadit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
spisovateli	spisovatel	k1gMnPc7	spisovatel
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
její	její	k3xOp3gFnPc1	její
knihy	kniha	k1gFnPc1	kniha
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
stovce	stovka	k1gFnSc6	stovka
absolutního	absolutní	k2eAgNnSc2d1	absolutní
pořadí	pořadí	k1gNnSc2	pořadí
a	a	k8xC	a
15	[number]	k4	15
knih	kniha	k1gFnPc2	kniha
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
200	[number]	k4	200
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
Znamení	znamení	k1gNnSc2	znamení
blíženců	blíženec	k1gMnPc2	blíženec
41	[number]	k4	41
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
Postel	postel	k1gFnSc1	postel
plná	plný	k2eAgFnSc1d1	plná
růží	růžit	k5eAaImIp3nS	růžit
59	[number]	k4	59
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
náruč	náruč	k1gFnSc1	náruč
léta	léto	k1gNnSc2	léto
78	[number]	k4	78
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
kniha	kniha	k1gFnSc1	kniha
Tajná	tajný	k2eAgFnSc1d1	tajná
láska	láska	k1gFnSc1	láska
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Lenka	Lenka	k1gFnSc1	Lenka
Lanczová	Lanczový	k2eAgFnSc1d1	Lanczová
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
autorky	autorka	k1gFnSc2	autorka
Lehká	Lehká	k1gFnSc1	Lehká
četba	četba	k1gFnSc1	četba
zrovna	zrovna	k6eAd1	zrovna
dneska	dneska	k?	dneska
–	–	k?	–
tvorba	tvorba	k1gFnSc1	tvorba
Lenky	Lenka	k1gFnSc2	Lenka
Lanczové	Lanczový	k2eAgFnSc2d1	Lanczová
očima	oko	k1gNnPc7	oko
Ivo	Ivo	k1gMnSc1	Ivo
Fencla	Fencl	k1gMnSc2	Fencl
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
Bez	bez	k7c2	bez
sexu	sex	k1gInSc2	sex
by	by	kYmCp3nP	by
hrdinky	hrdinka	k1gFnPc1	hrdinka
nebyly	být	k5eNaImAgFnP	být
reálné	reálný	k2eAgFnPc1d1	reálná
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
autorka	autorka	k1gFnSc1	autorka
52	[number]	k4	52
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
–	–	k?	–
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
<g/>
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
