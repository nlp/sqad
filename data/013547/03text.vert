<s>
NGC	NGC	kA
86	#num#	k4
</s>
<s>
NGC	NGC	kA
86	#num#	k4
galaxie	galaxie	k1gFnSc2
NGC	NGC	kA
86	#num#	k4
<g/>
Pozorovací	pozorovací	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
spirální	spirální	k2eAgFnPc1d1
galaxie	galaxie	k1gFnPc1
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Guillaume	Guillaum	k1gInSc5
Bigourdan	Bigourdan	k1gInSc4
Datum	datum	k1gNnSc1
objevu	objev	k1gInSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1884	#num#	k4
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
h	h	k?
21	#num#	k4
<g/>
m	m	kA
28,6	28,6	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
+22	+22	k4
<g/>
°	°	k?
33	#num#	k4
<g/>
′	′	k?
23	#num#	k4
<g/>
″	″	k?
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Andromeda	Andromeda	k1gMnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
And	Anda	k1gFnPc2
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
14,8	14,8	k4
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
75,4	75,4	k4
Mpc	Mpc	k1gMnSc1
Rudý	rudý	k1gMnSc1
posuv	posuv	k1gInSc4
</s>
<s>
0,018	0,018	k4
65	#num#	k4
a	a	k8xC
188	#num#	k4
231	#num#	k4
Označení	označení	k1gNnPc2
v	v	k7c6
katalozích	katalog	k1gInPc6
New	New	k1gMnSc2
General	General	k1gMnSc2
Catalogue	Catalogu	k1gMnSc2
</s>
<s>
NGC	NGC	kA
86	#num#	k4
Principal	Principal	k1gFnSc2
Galaxies	Galaxiesa	k1gFnPc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
PGC	PGC	kA
1383	#num#	k4
Jiná	jiná	k1gFnSc1
označení	označení	k1gNnSc2
</s>
<s>
MCG	MCG	kA
+0	+0	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
PGC	PGC	kA
1383	#num#	k4
•	•	k?
ZWG	ZWG	kA
479.11	479.11	k4
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NGC	NGC	kA
86	#num#	k4
je	být	k5eAaImIp3nS
spirální	spirální	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Andromedy	Andromeda	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
objevil	objevit	k5eAaPmAgMnS
Guillaume	Guillaume	k1gMnSc1
Bigourdan	Bigourdan	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1884	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc4
zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
je	být	k5eAaImIp3nS
14,8	14,8	k4
<g/>
m.	m.	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
NGC	NGC	kA
86	#num#	k4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
