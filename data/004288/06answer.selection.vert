<s>
Romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
křídlovku	křídlovka	k1gFnSc4	křídlovka
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
hořký	hořký	k2eAgInSc1d1	hořký
poeticko-romantický	poetickoomantický	k2eAgInSc1d1	poeticko-romantický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
natočený	natočený	k2eAgInSc4d1	natočený
na	na	k7c4	na
námět	námět	k1gInSc4	námět
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
básně	báseň	k1gFnSc2	báseň
Františka	František	k1gMnSc2	František
Hrubína	Hrubín	k1gMnSc2	Hrubín
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
snímku	snímek	k1gInSc2	snímek
i	i	k8xC	i
spoluautorem	spoluautor	k1gMnSc7	spoluautor
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
