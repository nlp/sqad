<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gInSc1	Universitas
Masarykiana	Masarykiana	k1gFnSc1	Masarykiana
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
Muni	Muni	k?	Muni
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
MUNI	MUNI	k?	MUNI
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
používaná	používaný	k2eAgFnSc1d1	používaná
jakožto	jakožto	k8xS	jakožto
internetová	internetový	k2eAgFnSc1d1	internetová
doména	doména	k1gFnSc1	doména
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
především	především	k9	především
v	v	k7c6	v
méně	málo	k6eAd2	málo
formálních	formální	k2eAgInPc6d1	formální
kontextech	kontext	k1gInPc6	kontext
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
"	"	kIx"	"
<g/>
MU	MU	kA	MU
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ve	v	k7c6	v
statutu	statut	k1gInSc6	statut
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
v	v	k7c6	v
akreditovaných	akreditovaný	k2eAgInPc6d1	akreditovaný
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
devět	devět	k4xCc4	devět
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
své	své	k1gNnSc4	své
Mendelovo	Mendelův	k2eAgNnSc4d1	Mendelovo
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
kino	kino	k1gNnSc4	kino
Scala	scát	k5eAaImAgFnS	scát
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
centrum	centrum	k1gNnSc4	centrum
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
a	a	k8xC	a
polární	polární	k2eAgFnSc6d1	polární
stanici	stanice	k1gFnSc6	stanice
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
světových	světový	k2eAgFnPc2d1	světová
univerzit	univerzita	k1gFnPc2	univerzita
QS	QS	kA	QS
TopUniversities	TopUniversities	k1gInSc1	TopUniversities
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
v	v	k7c6	v
akreditovaných	akreditovaný	k2eAgInPc6d1	akreditovaný
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
rektorem	rektor	k1gMnSc7	rektor
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Bek	bek	k1gMnSc1	bek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
zejména	zejména	k9	zejména
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc2d2	pozdější
první	první	k4xOgFnSc2	první
prezident	prezident	k1gMnSc1	prezident
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
vědecké	vědecký	k2eAgFnSc2d1	vědecká
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
činnosti	činnost	k1gFnSc2	činnost
věnoval	věnovat	k5eAaImAgMnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
rozvoji	rozvoj	k1gInSc3	rozvoj
československých	československý	k2eAgFnPc2d1	Československá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
široké	široký	k2eAgFnSc2d1	široká
konkurence	konkurence	k1gFnSc2	konkurence
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
jediná	jediný	k2eAgFnSc1d1	jediná
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
rozvoji	rozvoj	k1gInSc3	rozvoj
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
konkurentku	konkurentka	k1gFnSc4	konkurentka
<g/>
.	.	kIx.	.
</s>
<s>
Zřízení	zřízení	k1gNnSc4	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
bylo	být	k5eAaImAgNnS	být
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
politických	politický	k2eAgFnPc2d1	politická
priorit	priorita	k1gFnPc2	priorita
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
podporu	podpor	k1gInSc2	podpor
řady	řada	k1gFnSc2	řada
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
studentů	student	k1gMnPc2	student
i	i	k8xC	i
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
volba	volba	k1gFnSc1	volba
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
všeobecně	všeobecně	k6eAd1	všeobecně
převládal	převládat	k5eAaImAgInS	převládat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
brněnští	brněnský	k2eAgMnPc1d1	brněnský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Brno	Brno	k1gNnSc4	Brno
politicky	politicky	k6eAd1	politicky
zcela	zcela	k6eAd1	zcela
ovládali	ovládat	k5eAaImAgMnP	ovládat
a	a	k8xC	a
báli	bát	k5eAaImAgMnP	bát
se	se	k3xPyFc4	se
oslabení	oslabení	k1gNnSc4	oslabení
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
zdrojem	zdroj	k1gInSc7	zdroj
nacionálních	nacionální	k2eAgInPc2d1	nacionální
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
tragickými	tragický	k2eAgInPc7d1	tragický
pouličními	pouliční	k2eAgInPc7d1	pouliční
střety	střet	k1gInPc7	střet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
tzv.	tzv.	kA	tzv.
Volkstagu	Volkstag	k1gInSc2	Volkstag
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
český	český	k2eAgMnSc1d1	český
dělník	dělník	k1gMnSc1	dělník
František	František	k1gMnSc1	František
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
a	a	k8xC	a
rozpad	rozpad	k1gInSc1	rozpad
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
přinesl	přinést	k5eAaPmAgInS	přinést
příznivější	příznivý	k2eAgFnSc4d2	příznivější
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
nové	nový	k2eAgFnSc2d1	nová
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
sídlo	sídlo	k1gNnSc4	sídlo
byla	být	k5eAaImAgFnS	být
navrhována	navrhován	k2eAgFnSc1d1	navrhována
např.	např.	kA	např.
i	i	k8xC	i
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přednost	přednost	k1gFnSc1	přednost
byla	být	k5eAaImAgFnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dána	dán	k2eAgFnSc1d1	dána
většímu	veliký	k2eAgNnSc3d2	veliký
a	a	k8xC	a
významnějšímu	významný	k2eAgNnSc3d2	významnější
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
zemskému	zemský	k2eAgNnSc3d1	zemské
městu	město	k1gNnSc3	město
–	–	k?	–
Brnu	Brno	k1gNnSc3	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zřízena	zřízen	k2eAgFnSc1d1	zřízena
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
universita	universita	k1gFnSc1	universita
zákonem	zákon	k1gInSc7	zákon
ze	z	k7c2	z
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
č.	č.	k?	č.
50	[number]	k4	50
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z.	z.	k?	z.
a	a	k8xC	a
n.	n.	k?	n.
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
měla	mít	k5eAaImAgFnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
fakulty	fakulta	k1gFnPc4	fakulta
(	(	kIx(	(
<g/>
právnickou	právnický	k2eAgFnSc4d1	právnická
<g/>
,	,	kIx,	,
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
<g/>
,	,	kIx,	,
přírodovědeckou	přírodovědecký	k2eAgFnSc4d1	Přírodovědecká
a	a	k8xC	a
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
zákon	zákon	k1gInSc1	zákon
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
areálu	areál	k1gInSc2	areál
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
realizovat	realizovat	k5eAaBmF	realizovat
pouze	pouze	k6eAd1	pouze
budovu	budova	k1gFnSc4	budova
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
okupace	okupace	k1gFnSc1	okupace
způsobila	způsobit	k5eAaPmAgFnS	způsobit
univerzitě	univerzita	k1gFnSc3	univerzita
těžké	těžký	k2eAgFnPc1d1	těžká
ztráty	ztráta	k1gFnPc1	ztráta
materiální	materiální	k2eAgFnPc1d1	materiální
i	i	k8xC	i
lidské	lidský	k2eAgFnPc1d1	lidská
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
z	z	k7c2	z
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
byla	být	k5eAaImAgFnS	být
popravena	popraven	k2eAgFnSc1d1	popravena
nebo	nebo	k8xC	nebo
umučena	umučen	k2eAgFnSc1d1	umučena
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
neblahé	blahý	k2eNgInPc1d1	neblahý
zásahy	zásah	k1gInPc1	zásah
následovaly	následovat	k5eAaImAgInP	následovat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgFnPc1d3	nejrozsáhlejší
čistky	čistka	k1gFnPc1	čistka
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
muselo	muset	k5eAaImAgNnS	muset
opustit	opustit	k5eAaPmF	opustit
46	[number]	k4	46
%	%	kIx~	%
studentů	student	k1gMnPc2	student
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
nesla	nést	k5eAaImAgFnS	nést
univerzita	univerzita	k1gFnSc1	univerzita
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
jméno	jméno	k1gNnSc1	jméno
Universita	universita	k1gFnSc1	universita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1953	[number]	k4	1953
a	a	k8xC	a
1964	[number]	k4	1964
stála	stát	k5eAaImAgFnS	stát
mimo	mimo	k7c4	mimo
svazek	svazek	k1gInSc4	svazek
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Obnovení	obnovení	k1gNnSc1	obnovení
svobodných	svobodný	k2eAgInPc2d1	svobodný
poměrů	poměr	k1gInPc2	poměr
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
univerzitě	univerzita	k1gFnSc6	univerzita
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
i	i	k8xC	i
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
"	"	kIx"	"
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
čtyři	čtyři	k4xCgFnPc1	čtyři
nové	nový	k2eAgFnPc1d1	nová
fakulty	fakulta	k1gFnPc1	fakulta
(	(	kIx(	(
<g/>
ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
<g/>
,	,	kIx,	,
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
univerzitní	univerzitní	k2eAgInSc4d1	univerzitní
kampus	kampus	k1gInSc4	kampus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
univerzita	univerzita	k1gFnSc1	univerzita
podle	podle	k7c2	podle
přílohy	příloha	k1gFnSc2	příloha
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
z	z	k7c2	z
1998	[number]	k4	1998
vedena	veden	k2eAgFnSc1d1	vedena
jako	jako	k8xS	jako
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
mylně	mylně	k6eAd1	mylně
uváděné	uváděný	k2eAgFnPc4d1	uváděná
v	v	k7c6	v
názvu	název	k1gInSc6	název
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
vypustilo	vypustit	k5eAaPmAgNnS	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
2016	[number]	k4	2016
tak	tak	k8xS	tak
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
přílohy	příloha	k1gFnSc2	příloha
zákona	zákon	k1gInSc2	zákon
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
státních	státní	k2eAgFnPc2d1	státní
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
u	u	k7c2	u
svého	svůj	k3xOyFgInSc2	svůj
názvu	název	k1gInSc2	název
neuváděla	uvádět	k5eNaImAgNnP	uvádět
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
administrativu	administrativa	k1gFnSc4	administrativa
studia	studio	k1gNnSc2	studio
svůj	svůj	k3xOyFgInSc1	svůj
vlastní	vlastní	k2eAgInSc1d1	vlastní
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
inovace	inovace	k1gFnSc1	inovace
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
evropskou	evropský	k2eAgFnSc4d1	Evropská
cenu	cena	k1gFnSc4	cena
EUNIS	EUNIS	kA	EUNIS
Elite	Elit	k1gInSc5	Elit
Award	Award	k1gInSc4	Award
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
je	být	k5eAaImIp3nS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Compostela	Compostela	k1gFnSc1	Compostela
Group	Group	k1gInSc1	Group
of	of	k?	of
Universities	Universities	k1gInSc1	Universities
a	a	k8xC	a
Utrecht	Utrecht	k2eAgInSc1d1	Utrecht
Network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vydává	vydávat	k5eAaPmIp3nS	vydávat
univerzita	univerzita	k1gFnSc1	univerzita
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
měsíčník	měsíčník	k1gInSc4	měsíčník
Muni	Muni	k?	Muni
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
stal	stát	k5eAaPmAgInS	stát
Firemním	firemní	k2eAgNnSc7d1	firemní
médiem	médium	k1gNnSc7	médium
roku	rok	k1gInSc2	rok
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
tiskovina	tiskovina	k1gFnSc1	tiskovina
veřejné	veřejný	k2eAgFnSc2d1	veřejná
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
Komorou	komora	k1gFnSc7	komora
PR	pr	k0	pr
<g/>
.	.	kIx.	.
</s>
<s>
Podpisem	podpis	k1gInSc7	podpis
memoranda	memorandum	k1gNnSc2	memorandum
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
spolkem	spolek	k1gInSc7	spolek
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
zavázala	zavázat	k5eAaPmAgFnS	zavázat
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
internetové	internetový	k2eAgFnSc2d1	internetová
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Rondo	rondo	k1gNnSc1	rondo
koná	konat	k5eAaImIp3nS	konat
hokejový	hokejový	k2eAgInSc1d1	hokejový
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
celou	celý	k2eAgFnSc4d1	celá
sérii	série	k1gFnSc4	série
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
fakult	fakulta	k1gFnPc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
9	[number]	k4	9
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
již	již	k6eAd1	již
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
PrF	PrF	k1gFnSc2	PrF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
LF	LF	kA	LF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
PřF	PřF	k1gFnSc1	PřF
<g/>
)	)	kIx)	)
a	a	k8xC	a
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
FF	ff	kA	ff
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
PdF	PdF	k1gFnSc1	PdF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
založení	založení	k1gNnSc1	založení
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
již	již	k9	již
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
tehdy	tehdy	k6eAd1	tehdy
jen	jen	k9	jen
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
úvah	úvaha	k1gFnPc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
změněné	změněný	k2eAgInPc4d1	změněný
politicko-hospodářské	politickoospodářský	k2eAgInPc4d1	politicko-hospodářský
poměry	poměr	k1gInPc4	poměr
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc2d1	ekonomicko-správní
fakulty	fakulta	k1gFnSc2	fakulta
(	(	kIx(	(
<g/>
ESF	ESF	kA	ESF
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
z	z	k7c2	z
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
vydělila	vydělit	k5eAaPmAgFnS	vydělit
Fakulta	fakulta	k1gFnSc1	fakulta
informatiky	informatika	k1gFnSc2	informatika
(	(	kIx(	(
<g/>
FI	fi	k0	fi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Fakulta	fakulta	k1gFnSc1	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
(	(	kIx(	(
<g/>
FSS	FSS	kA	FSS
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
dosud	dosud	k6eAd1	dosud
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
Fakulta	fakulta	k1gFnSc1	fakulta
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
(	(	kIx(	(
<g/>
FSpS	FSpS	k1gFnSc1	FSpS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
komunismu	komunismus	k1gInSc2	komunismus
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
a	a	k8xC	a
obnovena	obnoven	k2eAgFnSc1d1	obnovena
byla	být	k5eAaImAgFnS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
vyčleněna	vyčleněn	k2eAgFnSc1d1	vyčleněna
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
fungovala	fungovat	k5eAaImAgFnS	fungovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
institut	institut	k1gInSc4	institut
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
ještě	ještě	k6eAd1	ještě
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
FaF	FaF	k1gMnSc1	FaF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
farmacie	farmacie	k1gFnSc1	farmacie
realizované	realizovaný	k2eAgNnSc4d1	realizované
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
zrušena	zrušen	k2eAgNnPc1d1	zrušeno
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc1	studium
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
obnovení	obnovení	k1gNnSc3	obnovení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
fakulty	fakulta	k1gFnSc2	fakulta
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
už	už	k9	už
však	však	k9	však
jako	jako	k9	jako
součásti	součást	k1gFnPc1	součást
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
veterinární	veterinární	k2eAgFnSc2d1	veterinární
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nesoucí	nesoucí	k2eAgInSc4d1	nesoucí
název	název	k1gInSc4	název
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
a	a	k8xC	a
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
univerzita	univerzita	k1gFnSc1	univerzita
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rektorát	rektorát	k1gInSc4	rektorát
Kariérní	kariérní	k2eAgNnSc1d1	kariérní
centrum	centrum	k1gNnSc1	centrum
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Ústavy	ústava	k1gFnSc2	ústava
Ústav	ústava	k1gFnPc2	ústava
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
ÚVT	ÚVT	kA	ÚVT
<g/>
)	)	kIx)	)
Středoevropský	středoevropský	k2eAgInSc1d1	středoevropský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
(	(	kIx(	(
<g/>
CEITEC	CEITEC	kA	CEITEC
<g/>
)	)	kIx)	)
Jiná	jiný	k2eAgFnSc1d1	jiná
pracoviště	pracoviště	k1gNnSc4	pracoviště
Archiv	archiv	k1gInSc1	archiv
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Centrum	centrum	k1gNnSc1	centrum
jazykového	jazykový	k2eAgNnSc2d1	jazykové
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
(	(	kIx(	(
<g/>
CJV	CJV	kA	CJV
<g/>
)	)	kIx)	)
Centrum	centrum	k1gNnSc1	centrum
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
spolupráce	spolupráce	k1gFnSc2	spolupráce
(	(	kIx(	(
<g/>
CZS	CZS	kA	CZS
<g/>
)	)	kIx)	)
Středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
studentům	student	k1gMnPc3	student
se	s	k7c7	s
specifickými	specifický	k2eAgInPc7d1	specifický
nároky	nárok	k1gInPc7	nárok
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Teiresiás	Teiresiás	k1gInSc1	Teiresiás
<g/>
)	)	kIx)	)
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
transfer	transfer	k1gInSc4	transfer
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
CTT	CTT	kA	CTT
<g/>
)	)	kIx)	)
Institut	institut	k1gInSc1	institut
biostatistiky	biostatistika	k1gFnSc2	biostatistika
a	a	k8xC	a
analýz	analýza	k1gFnPc2	analýza
(	(	kIx(	(
<g/>
IBA	iba	k6eAd1	iba
<g/>
)	)	kIx)	)
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
muzeum	muzeum	k1gNnSc1	muzeum
Centrum	centrum	k1gNnSc1	centrum
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
inovací	inovace	k1gFnPc2	inovace
v	v	k7c6	v
informačních	informační	k2eAgFnPc6d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
technologiích	technologie	k1gFnPc6	technologie
(	(	kIx(	(
<g/>
CERIT	CERIT	kA	CERIT
<g/>
)	)	kIx)	)
Centrální	centrální	k2eAgFnSc1d1	centrální
řídící	řídící	k2eAgFnSc1d1	řídící
struktura	struktura	k1gFnSc1	struktura
projektu	projekt	k1gInSc2	projekt
CEITEC	CEITEC	kA	CEITEC
Univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
centrum	centrum	k1gNnSc1	centrum
Telč	Telč	k1gFnSc1	Telč
Účelová	účelový	k2eAgFnSc1d1	účelová
zařízení	zařízení	k1gNnPc2	zařízení
Správa	správa	k1gFnSc1	správa
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
menz	menza	k1gFnPc2	menza
(	(	kIx(	(
<g/>
SKM	SKM	kA	SKM
<g/>
)	)	kIx)	)
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Správa	správa	k1gFnSc1	správa
Univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
Bohunice	Bohunice	k1gFnPc1	Bohunice
(	(	kIx(	(
<g/>
UKB	UKB	kA	UKB
<g/>
)	)	kIx)	)
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
využívá	využívat	k5eAaImIp3nS	využívat
množství	množství	k1gNnSc4	množství
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Rektorát	rektorát	k1gInSc1	rektorát
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Kounicově	Kounicově	k1gMnSc6	Kounicově
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Žerotínově	Žerotínův	k2eAgNnSc6d1	Žerotínovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
na	na	k7c6	na
Kraví	kraví	k2eAgFnSc6d1	kraví
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
na	na	k7c6	na
souvisejících	související	k2eAgInPc6d1	související
pozemcích	pozemek	k1gInPc6	pozemek
mezi	mezi	k7c7	mezi
Žabovřeskami	Žabovřesky	k1gFnPc7	Žabovřesky
a	a	k8xC	a
Veveřím	veveří	k2eAgNnPc3d1	veveří
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
však	však	k9	však
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
provázely	provázet	k5eAaImAgInP	provázet
značné	značný	k2eAgInPc1d1	značný
průtahy	průtah	k1gInPc1	průtah
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
na	na	k7c6	na
zamýšleném	zamýšlený	k2eAgNnSc6d1	zamýšlené
Akademickém	akademický	k2eAgNnSc6d1	akademické
náměstí	náměstí	k1gNnSc6	náměstí
podařilo	podařit	k5eAaPmAgNnS	podařit
realizovat	realizovat	k5eAaBmF	realizovat
jen	jen	k9	jen
budovu	budova	k1gFnSc4	budova
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
kampus	kampus	k1gInSc1	kampus
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
Brně-Bohunicích	Brně-Bohunik	k1gInPc6	Brně-Bohunik
otevřen	otevřít	k5eAaPmNgInS	otevřít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
první	první	k4xOgNnSc4	první
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
výstavbě	výstavba	k1gFnSc6	výstavba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
moderní	moderní	k2eAgInSc4d1	moderní
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
a	a	k8xC	a
výzkumně-vývojové	výzkumněývojový	k2eAgNnSc4d1	výzkumně-vývojové
centrum	centrum	k1gNnSc4	centrum
zabírající	zabírající	k2eAgFnSc4d1	zabírající
plochu	plocha	k1gFnSc4	plocha
42	[number]	k4	42
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pěti	pět	k4xCc3	pět
tisícům	tisíc	k4xCgInPc3	tisíc
studentů	student	k1gMnPc2	student
a	a	k8xC	a
tisíci	tisíc	k4xCgInPc7	tisíc
pracovníků	pracovník	k1gMnPc2	pracovník
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
fakulty	fakulta	k1gFnSc2	fakulta
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ústav	ústava	k1gFnPc2	ústava
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Ústavu	ústav	k1gInSc2	ústav
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
síť	síť	k1gFnSc4	síť
studijních	studijní	k2eAgFnPc2d1	studijní
a	a	k8xC	a
počítačových	počítačový	k2eAgFnPc2d1	počítačová
studoven	studovna	k1gFnPc2	studovna
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
budov	budova	k1gFnPc2	budova
svých	svůj	k3xOyFgFnPc2	svůj
fakult	fakulta	k1gFnPc2	fakulta
nebo	nebo	k8xC	nebo
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
desítkami	desítka	k1gFnPc7	desítka
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
uživatelé	uživatel	k1gMnPc1	uživatel
přihlašují	přihlašovat	k5eAaImIp3nP	přihlašovat
vlastním	vlastní	k2eAgInSc7d1	vlastní
účtem	účet	k1gInSc7	účet
(	(	kIx(	(
<g/>
vázaným	vázaný	k2eAgInPc3d1	vázaný
typicky	typicky	k6eAd1	typicky
k	k	k7c3	k
osobnímu	osobní	k2eAgNnSc3d1	osobní
číslu	číslo	k1gNnSc3	číslo
učo	učo	k?	učo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
tiskovým	tiskový	k2eAgFnPc3d1	tisková
službám	služba	k1gFnPc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
centralizovaného	centralizovaný	k2eAgInSc2d1	centralizovaný
způsobu	způsob	k1gInSc2	způsob
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
jednotné	jednotný	k2eAgFnSc2d1	jednotná
instalace	instalace	k1gFnSc2	instalace
softwaru	software	k1gInSc2	software
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
úspoře	úspora	k1gFnSc3	úspora
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
omezení	omezení	k1gNnPc2	omezení
obtíží	obtíž	k1gFnPc2	obtíž
při	při	k7c6	při
provozu	provoz	k1gInSc6	provoz
studoven	studovna	k1gFnPc2	studovna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
počítačovou	počítačový	k2eAgFnSc7d1	počítačová
studovnou	studovna	k1gFnSc7	studovna
s	s	k7c7	s
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
provozem	provoz	k1gInSc7	provoz
je	být	k5eAaImIp3nS	být
celouniverzitní	celouniverzitní	k2eAgFnSc1d1	celouniverzitní
počítačová	počítačový	k2eAgFnSc1d1	počítačová
studovna	studovna	k1gFnSc1	studovna
(	(	kIx(	(
<g/>
CPS	CPS	kA	CPS
<g/>
)	)	kIx)	)
s	s	k7c7	s
centrálním	centrální	k2eAgNnSc7d1	centrální
umístěním	umístění	k1gNnSc7	umístění
na	na	k7c4	na
Komenského	Komenského	k2eAgNnSc4d1	Komenského
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
kapacitou	kapacita	k1gFnSc7	kapacita
145	[number]	k4	145
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
rektorů	rektor	k1gMnPc2	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
již	již	k6eAd1	již
32	[number]	k4	32
rektorů	rektor	k1gMnPc2	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
rektorem	rektor	k1gMnSc7	rektor
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
ekonomie	ekonomie	k1gFnSc2	ekonomie
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
brněnské	brněnský	k2eAgFnSc2d1	brněnská
univerzity	univerzita	k1gFnSc2	univerzita
osobně	osobně	k6eAd1	osobně
přispěl	přispět	k5eAaPmAgMnS	přispět
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
rektora	rektor	k1gMnSc2	rektor
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
jednoleté	jednoletý	k2eAgNnSc1d1	jednoleté
<g/>
,	,	kIx,	,
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
akademickému	akademický	k2eAgInSc3d1	akademický
roku	rok	k1gInSc3	rok
a	a	k8xC	a
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
střídali	střídat	k5eAaImAgMnP	střídat
předchozí	předchozí	k2eAgMnPc1d1	předchozí
děkani	děkan	k1gMnPc1	děkan
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
čtyř	čtyři	k4xCgFnPc2	čtyři
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
fakult	fakulta	k1gFnPc2	fakulta
(	(	kIx(	(
<g/>
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g/>
,	,	kIx,	,
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
<g/>
,	,	kIx,	,
filozofické	filozofický	k2eAgFnSc2d1	filozofická
a	a	k8xC	a
právnické	právnický	k2eAgFnSc2d1	právnická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
významné	významný	k2eAgInPc1d1	významný
osobnosti	osobnost	k1gFnPc4	osobnost
jako	jako	k8xS	jako
např.	např.	kA	např.
právníci	právník	k1gMnPc1	právník
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kallab	Kallab	k1gMnSc1	Kallab
nebo	nebo	k8xC	nebo
fyziolog	fyziolog	k1gMnSc1	fyziolog
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
brněnských	brněnský	k2eAgFnPc2d1	brněnská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
Edward	Edward	k1gMnSc1	Edward
Babák	Babák	k1gMnSc1	Babák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
zvolen	zvolit	k5eAaPmNgMnS	zvolit
stávající	stávající	k2eAgMnSc1d1	stávající
rektor	rektor	k1gMnSc1	rektor
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gMnSc1	jeho
protikandidát	protikandidát	k1gMnSc1	protikandidát
z	z	k7c2	z
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dominik	Dominik	k1gMnSc1	Dominik
nezískal	získat	k5eNaPmAgMnS	získat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
fašistickou	fašistický	k2eAgFnSc4d1	fašistická
orientaci	orientace	k1gFnSc4	orientace
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
nakonec	nakonec	k6eAd1	nakonec
zastával	zastávat	k5eAaImAgMnS	zastávat
pouze	pouze	k6eAd1	pouze
funkci	funkce	k1gFnSc4	funkce
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
uzavření	uzavření	k1gNnSc2	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
nacisty	nacista	k1gMnSc2	nacista
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
nemocného	mocný	k2eNgMnSc2d1	nemocný
Arna	Arne	k1gMnSc2	Arne
Nováka	Novák	k1gMnSc2	Novák
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
stál	stát	k5eAaImAgMnS	stát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
univerzity	univerzita	k1gFnSc2	univerzita
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prorektor	prorektor	k1gMnSc1	prorektor
a	a	k8xC	a
Novákův	Novákův	k2eAgMnSc1d1	Novákův
předchůdce	předchůdce	k1gMnSc1	předchůdce
v	v	k7c6	v
rektorské	rektorský	k2eAgFnSc6d1	Rektorská
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
botaniky	botanika	k1gFnSc2	botanika
Josef	Josef	k1gMnSc1	Josef
Podpěra	podpěra	k1gFnSc1	podpěra
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
byl	být	k5eAaImAgMnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
do	do	k7c2	do
rektorské	rektorský	k2eAgFnSc2d1	Rektorská
funkce	funkce	k1gFnSc2	funkce
instalován	instalován	k2eAgMnSc1d1	instalován
profesor	profesor	k1gMnSc1	profesor
patologické	patologický	k2eAgFnSc2d1	patologická
anatomie	anatomie	k1gFnSc2	anatomie
a	a	k8xC	a
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
děkan	děkan	k1gMnSc1	děkan
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
Václav	Václav	k1gMnSc1	Václav
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
ještě	ještě	k9	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
volbou	volba	k1gFnSc7	volba
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ročním	roční	k2eAgInSc6d1	roční
mandátu	mandát	k1gInSc6	mandát
profesora	profesor	k1gMnSc2	profesor
geometrie	geometrie	k1gFnSc2	geometrie
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc2d1	bývalý
dvojnásobného	dvojnásobný	k2eAgMnSc2d1	dvojnásobný
děkana	děkan	k1gMnSc2	děkan
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Ladislava	Ladislav	k1gMnSc2	Ladislav
Seiferta	Seifert	k1gMnSc2	Seifert
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
rektora	rektor	k1gMnSc2	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
první	první	k4xOgMnSc1	první
děkan	děkan	k1gMnSc1	děkan
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
František	František	k1gMnSc1	František
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
setrval	setrvat	k5eAaPmAgMnS	setrvat
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
11	[number]	k4	11
let	léto	k1gNnPc2	léto
a	a	k8xC	a
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
poslance	poslanec	k1gMnSc2	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
za	za	k7c4	za
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Trávníčka	Trávníček	k1gMnSc4	Trávníček
jako	jako	k9	jako
rektor	rektor	k1gMnSc1	rektor
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
děkan	děkan	k1gMnSc1	děkan
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
Theodor	Theodor	k1gMnSc1	Theodor
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
rovněž	rovněž	k9	rovněž
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
normalizace	normalizace	k1gFnSc2	normalizace
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
politické	politický	k2eAgInPc4d1	politický
postoje	postoj	k1gInPc4	postoj
v	v	k7c6	v
době	doba	k1gFnSc6	doba
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečňování	uskutečňování	k1gNnSc1	uskutečňování
normalizace	normalizace	k1gFnSc2	normalizace
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
letech	let	k1gInPc6	let
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
řídil	řídit	k5eAaImAgMnS	řídit
jako	jako	k9	jako
rektor	rektor	k1gMnSc1	rektor
profesor	profesor	k1gMnSc1	profesor
Jaromír	Jaromír	k1gMnSc1	Jaromír
Vašků	Vašek	k1gMnPc2	Vašek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
současně	současně	k6eAd1	současně
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
zakladatele	zakladatel	k1gMnSc2	zakladatel
Engliše	Engliš	k1gMnSc2	Engliš
<g/>
)	)	kIx)	)
prvním	první	k4xOgMnSc7	první
rektorem	rektor	k1gMnSc7	rektor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
před	před	k7c7	před
výkonem	výkon	k1gInSc7	výkon
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
nebyl	být	k5eNaImAgMnS	být
děkanem	děkan	k1gMnSc7	děkan
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
fakult	fakulta	k1gFnPc2	fakulta
(	(	kIx(	(
<g/>
působil	působit	k5eAaImAgMnS	působit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
jako	jako	k8xS	jako
proděkan	proděkan	k1gMnSc1	proděkan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obdobím	období	k1gNnSc7	období
postupného	postupný	k2eAgNnSc2d1	postupné
zmírňování	zmírňování	k1gNnSc2	zmírňování
normalizačních	normalizační	k2eAgInPc2d1	normalizační
tlaků	tlak	k1gInPc2	tlak
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgInSc1d1	spjat
mandát	mandát	k1gInSc1	mandát
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Kubáčka	Kubáček	k1gMnSc2	Kubáček
<g/>
,	,	kIx,	,
třetího	třetí	k4xOgMnSc2	třetí
nejdéle	dlouho	k6eAd3	dlouho
sloužícího	sloužící	k2eAgMnSc2d1	sloužící
rektora	rektor	k1gMnSc2	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
komunistickým	komunistický	k2eAgMnSc7d1	komunistický
rektorem	rektor	k1gMnSc7	rektor
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
československých	československý	k2eAgFnPc2d1	Československá
dějin	dějiny	k1gFnPc2	dějiny
Bedřich	Bedřich	k1gMnSc1	Bedřich
Čeřesňák	Čeřesňák	k1gMnSc1	Čeřesňák
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
akademickou	akademický	k2eAgFnSc7d1	akademická
obcí	obec	k1gFnSc7	obec
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
již	již	k6eAd1	již
politickými	politický	k2eAgMnPc7d1	politický
úředníky	úředník	k1gMnPc7	úředník
<g/>
)	)	kIx)	)
novým	nový	k2eAgMnSc7d1	nový
rektorem	rektor	k1gMnSc7	rektor
zvolen	zvolit	k5eAaPmNgMnS	zvolit
někdejší	někdejší	k2eAgMnSc1d1	někdejší
děkan	děkan	k1gMnSc1	děkan
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
(	(	kIx(	(
<g/>
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
disident	disident	k1gMnSc1	disident
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
Milan	Milan	k1gMnSc1	Milan
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
prvních	první	k4xOgMnPc2	první
demokraticky	demokraticky	k6eAd1	demokraticky
zvolených	zvolený	k2eAgMnPc2d1	zvolený
rektorů	rektor	k1gMnPc2	rektor
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
jmenován	jmenován	k2eAgInSc4d1	jmenován
prezidentem	prezident	k1gMnSc7	prezident
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
a	a	k8xC	a
setrval	setrvat	k5eAaPmAgMnS	setrvat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
coby	coby	k?	coby
devětašedesátiletý	devětašedesátiletý	k2eAgMnSc1d1	devětašedesátiletý
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
profesor	profesor	k1gMnSc1	profesor
fyziky	fyzika	k1gFnSc2	fyzika
Eduard	Eduard	k1gMnSc1	Eduard
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
rektorem	rektor	k1gMnSc7	rektor
v	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
tříletá	tříletý	k2eAgNnPc4d1	tříleté
funkční	funkční	k2eAgNnPc4d1	funkční
období	období	k1gNnPc4	období
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
rektorem	rektor	k1gMnSc7	rektor
stal	stát	k5eAaPmAgMnS	stát
někdejší	někdejší	k2eAgMnSc1d1	někdejší
Jelínkův	Jelínkův	k2eAgMnSc1d1	Jelínkův
proděkan	proděkan	k1gMnSc1	proděkan
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
informatiky	informatika	k1gFnSc2	informatika
Jiří	Jiří	k1gMnSc1	Jiří
Zlatuška	Zlatuška	k1gMnSc1	Zlatuška
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
děkana	děkan	k1gMnSc2	děkan
fakulty	fakulta	k1gFnSc2	fakulta
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
rektorem	rektor	k1gMnSc7	rektor
profesor	profesor	k1gMnSc1	profesor
politologie	politologie	k1gFnSc2	politologie
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
volební	volební	k2eAgFnSc6d1	volební
období	období	k1gNnSc6	období
rektorem	rektor	k1gMnSc7	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
docent	docent	k1gMnSc1	docent
muzikologie	muzikologie	k1gFnSc2	muzikologie
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Bek	bek	k1gMnSc1	bek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
mandát	mandát	k1gInSc1	mandát
vyprší	vypršet	k5eAaPmIp3nS	vypršet
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
držitelů	držitel	k1gMnPc2	držitel
čestných	čestný	k2eAgInPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
udělila	udělit	k5eAaPmAgFnS	udělit
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
doctor	doctor	k1gInSc4	doctor
honoris	honoris	k1gFnSc2	honoris
causa	causa	k1gFnSc1	causa
<g/>
)	)	kIx)	)
120	[number]	k4	120
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
nositelem	nositel	k1gMnSc7	nositel
čestného	čestný	k2eAgInSc2d1	čestný
doktorátu	doktorát	k1gInSc2	doktorát
byl	být	k5eAaImAgMnS	být
skladatel	skladatel	k1gMnSc1	skladatel
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
také	také	k9	také
syn	syn	k1gMnSc1	syn
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Masarykův	Masarykův	k2eAgMnSc1d1	Masarykův
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
funkci	funkce	k1gFnSc6	funkce
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
nebo	nebo	k8xC	nebo
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
univerzitě	univerzita	k1gFnSc3	univerzita
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
strategií	strategie	k1gFnPc2	strategie
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
posouzení	posouzení	k1gNnSc4	posouzení
a	a	k8xC	a
poradenství	poradenství	k1gNnSc4	poradenství
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
rada	rada	k1gFnSc1	rada
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
profesor	profesor	k1gMnSc1	profesor
Jiří	Jiří	k1gMnSc1	Jiří
Jiřičný	Jiřičný	k2eAgMnSc1d1	Jiřičný
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
Ústavu	ústav	k1gInSc2	ústav
molekulárního	molekulární	k2eAgInSc2d1	molekulární
výzkumu	výzkum	k1gInSc2	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
Univerzity	univerzita	k1gFnSc2	univerzita
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Peter	Peter	k1gMnSc1	Peter
Williamson	Williamson	k1gMnSc1	Williamson
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
studií	studie	k1gFnPc2	studie
managementu	management	k1gInSc2	management
na	na	k7c4	na
Cambridge	Cambridge	k1gFnPc4	Cambridge
Judge	Judg	k1gInSc2	Judg
Business	business	k1gInSc1	business
School	School	k1gInSc1	School
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Thomas	Thomas	k1gMnSc1	Thomas
Henzinger	Henzinger	k1gMnSc1	Henzinger
(	(	kIx(	(
<g/>
Švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
federální	federální	k2eAgInSc1d1	federální
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
)	)	kIx)	)
a	a	k8xC	a
profesorka	profesorka	k1gFnSc1	profesorka
Marie-Janine	Marie-Janin	k1gInSc5	Marie-Janin
Calic	Calic	k1gMnSc1	Calic
(	(	kIx(	(
<g/>
historička	historička	k1gFnSc1	historička
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
východoevropské	východoevropský	k2eAgFnPc4d1	východoevropská
a	a	k8xC	a
jihoevropské	jihoevropský	k2eAgFnPc4d1	jihoevropská
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
sešla	sejít	k5eAaPmAgFnS	sejít
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Jednatelkou	jednatelka	k1gFnSc7	jednatelka
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
molekulární	molekulární	k2eAgFnSc1d1	molekulární
bioložka	bioložka	k1gFnSc1	bioložka
Mary	Mary	k1gFnSc1	Mary
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Connel	Connel	k1gInSc4	Connel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
ERA	ERA	kA	ERA
Chair	Chair	k1gInSc1	Chair
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
rektorský	rektorský	k2eAgInSc1d1	rektorský
řetěz	řetěz	k1gInSc1	řetěz
s	s	k7c7	s
medailí	medaile	k1gFnSc7	medaile
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
rektorské	rektorský	k2eAgNnSc1d1	rektorské
žezlo	žezlo	k1gNnSc1	žezlo
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
