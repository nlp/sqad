<s>
Den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
přesný	přesný	k2eAgInSc1d1	přesný
význam	význam	k1gInSc1	význam
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
soumrakem	soumrak	k1gInSc7	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
Středověcí	středověký	k2eAgMnPc1d1	středověký
komputisté	komputista	k1gMnPc1	komputista
někdy	někdy	k6eAd1	někdy
nazývali	nazývat	k5eAaImAgMnP	nazývat
takovýto	takovýto	k3xDgInSc4	takovýto
den	den	k1gInSc4	den
dies	dies	k6eAd1	dies
usualis	usualis	k1gFnSc2	usualis
nebo	nebo	k8xC	nebo
artificialis	artificialis	k1gFnSc2	artificialis
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
světlý	světlý	k2eAgInSc4d1	světlý
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
nazýván	nazývat	k5eAaImNgInS	nazývat
dies	dies	k1gInSc1	dies
integer	integra	k1gFnPc2	integra
nebo	nebo	k8xC	nebo
také	také	k9	také
dies	dies	k6eAd1	dies
naturalis	naturalis	k1gFnSc4	naturalis
<g/>
,	,	kIx,	,
v	v	k7c6	v
církevním	církevní	k2eAgNnSc6d1	církevní
právu	právo	k1gNnSc6	právo
pak	pak	k6eAd1	pak
dies	dies	k6eAd1	dies
legitimus	legitimus	k1gMnSc1	legitimus
<g/>
.	.	kIx.	.
</s>
<s>
Okamžik	okamžik	k1gInSc1	okamžik
půlnoci	půlnoc	k1gFnSc2	půlnoc
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
okamžik	okamžik	k1gInSc1	okamžik
poledne	poledne	k1gNnSc2	poledne
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
teoretický	teoretický	k2eAgInSc1d1	teoretický
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
až	až	k9	až
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozvoje	rozvoj	k1gInSc2	rozvoj
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
rytmus	rytmus	k1gInSc1	rytmus
dne	den	k1gInSc2	den
určován	určovat	k5eAaImNgInS	určovat
světlem	světlo	k1gNnSc7	světlo
a	a	k8xC	a
tmou	tma	k1gFnSc7	tma
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
se	se	k3xPyFc4	se
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
horae	horae	k1gFnSc4	horae
inaequales	inaequalesa	k1gFnPc2	inaequalesa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hodiny	hodina	k1gFnPc4	hodina
nestejnoměrné	stejnoměrný	k2eNgFnPc4d1	nestejnoměrná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
hodin	hodina	k1gFnPc2	hodina
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
i	i	k9	i
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
,	,	kIx,	,
kolísala	kolísat	k5eAaImAgFnS	kolísat
i	i	k9	i
délka	délka	k1gFnSc1	délka
denní	denní	k2eAgFnSc2d1	denní
a	a	k8xC	a
noční	noční	k2eAgFnSc2d1	noční
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zimním	zimní	k2eAgInSc6d1	zimní
slunovratu	slunovrat	k1gInSc6	slunovrat
např.	např.	kA	např.
hodina	hodina	k1gFnSc1	hodina
dne	den	k1gInSc2	den
=	=	kIx~	=
30	[number]	k4	30
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
hodina	hodina	k1gFnSc1	hodina
noci	noc	k1gFnSc2	noc
90	[number]	k4	90
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
předěly	předěl	k1gInPc1	předěl
dne	den	k1gInSc2	den
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
matutinum	matutinum	k1gNnSc1	matutinum
(	(	kIx(	(
<g/>
rozbřesk	rozbřesk	k1gInSc1	rozbřesk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
prima	prima	k1gFnSc1	prima
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
h.	h.	k?	h.
tercia	tercia	k1gFnSc1	tercia
(	(	kIx(	(
<g/>
uprostřed	uprostřed	k7c2	uprostřed
dopoledne	dopoledne	k1gNnSc2	dopoledne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
h.	h.	k?	h.
sexta	sexta	k1gFnSc1	sexta
(	(	kIx(	(
<g/>
poledne	poledne	k1gNnSc1	poledne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
h.	h.	k?	h.
nona	nona	k1gFnSc1	nona
(	(	kIx(	(
<g/>
uprostřed	uprostřed	k7c2	uprostřed
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vespera	vespera	k1gFnSc1	vespera
(	(	kIx(	(
<g/>
hodina	hodina	k1gFnSc1	hodina
před	před	k7c7	před
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
completorium	completorium	k1gNnSc1	completorium
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
dne	den	k1gInSc2	den
dle	dle	k7c2	dle
horae	hora	k1gInSc2	hora
equales	equalesa	k1gFnPc2	equalesa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hodiny	hodina	k1gFnPc4	hodina
stejnoměrné	stejnoměrný	k2eAgFnPc4d1	stejnoměrná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
námi	my	k3xPp1nPc7	my
dnes	dnes	k6eAd1	dnes
používaného	používaný	k2eAgInSc2d1	používaný
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
den	den	k1gInSc4	den
dělíme	dělit	k5eAaImIp1nP	dělit
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
na	na	k7c4	na
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
německý	německý	k2eAgInSc1d1	německý
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existovalo	existovat	k5eAaImAgNnS	existovat
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
počítaly	počítat	k5eAaImAgFnP	počítat
od	od	k7c2	od
západu	západ	k1gInSc2	západ
slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
italský	italský	k2eAgMnSc1d1	italský
nebo	nebo	k8xC	nebo
český	český	k2eAgInSc1d1	český
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
způsob	způsob	k1gInSc1	způsob
byl	být	k5eAaImAgInS	být
však	však	k9	však
zatlačen	zatlačen	k2eAgInSc1d1	zatlačen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
západ	západ	k1gInSc1	západ
slunce	slunce	k1gNnSc2	slunce
byl	být	k5eAaImAgInS	být
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
sice	sice	k8xC	sice
jen	jen	k9	jen
nepatrně	nepatrně	k6eAd1	nepatrně
<g/>
,	,	kIx,	,
v	v	k7c6	v
delším	dlouhý	k2eAgInSc6d2	delší
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
však	však	k9	však
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
<g/>
:	:	kIx,	:
6	[number]	k4	6
-	-	kIx~	-
10	[number]	k4	10
h	h	k?	h
<g/>
,	,	kIx,	,
Dopoledne	dopoledne	k1gNnPc1	dopoledne
<g/>
:	:	kIx,	:
10	[number]	k4	10
-	-	kIx~	-
12	[number]	k4	12
h	h	k?	h
<g/>
,	,	kIx,	,
Poledne	poledne	k1gNnSc1	poledne
<g/>
:	:	kIx,	:
12	[number]	k4	12
h	h	k?	h
<g/>
,	,	kIx,	,
Odpoledne	odpoledne	k1gNnPc1	odpoledne
<g/>
:	:	kIx,	:
12	[number]	k4	12
-	-	kIx~	-
18	[number]	k4	18
h	h	k?	h
<g/>
,	,	kIx,	,
Večer	večer	k1gInSc1	večer
<g/>
:	:	kIx,	:
18	[number]	k4	18
-	-	kIx~	-
22	[number]	k4	22
h	h	k?	h
<g/>
,	,	kIx,	,
Noc	noc	k1gFnSc1	noc
<g/>
:	:	kIx,	:
22	[number]	k4	22
-	-	kIx~	-
6	[number]	k4	6
h.	h.	k?	h.
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
denních	denní	k2eAgNnPc2d1	denní
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
člověka	člověk	k1gMnSc2	člověk
přirozený	přirozený	k2eAgInSc4d1	přirozený
den	den	k1gInSc4	den
začíná	začínat	k5eAaImIp3nS	začínat
východem	východ	k1gInSc7	východ
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
lidé	člověk	k1gMnPc1	člověk
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
kulturními	kulturní	k2eAgFnPc7d1	kulturní
normami	norma	k1gFnPc7	norma
<g/>
,	,	kIx,	,
znalostmi	znalost	k1gFnPc7	znalost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pod	pod	k7c7	pod
civilizačním	civilizační	k2eAgInSc7d1	civilizační
tlakem	tlak	k1gInSc7	tlak
toto	tento	k3xDgNnSc4	tento
přirozené	přirozený	k2eAgNnSc1d1	přirozené
pojetí	pojetí	k1gNnSc1	pojetí
dne	den	k1gInSc2	den
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
počítáme	počítat	k5eAaImIp1nP	počítat
den	den	k1gInSc4	den
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
denní	denní	k2eAgFnSc1d1	denní
doba	doba	k1gFnSc1	doba
od	od	k7c2	od
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgMnS	být
počítán	počítán	k2eAgInSc4d1	počítán
den	den	k1gInSc4	den
různá	různý	k2eAgFnSc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejstarších	starý	k2eAgNnPc2d3	nejstarší
historických	historický	k2eAgNnPc2d1	historické
období	období	k1gNnPc2	období
lze	lze	k6eAd1	lze
však	však	k9	však
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
skrovným	skrovný	k2eAgInPc3d1	skrovný
dokladům	doklad	k1gInPc3	doklad
<g/>
,	,	kIx,	,
určit	určit	k5eAaPmF	určit
zpětně	zpětně	k6eAd1	zpětně
počátek	počátek	k1gInSc4	počátek
dne	den	k1gInSc2	den
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
výhradami	výhrada	k1gFnPc7	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
národy	národ	k1gInPc1	národ
s	s	k7c7	s
lunárním	lunární	k2eAgInSc7d1	lunární
kalendářem	kalendář	k1gInSc7	kalendář
začínaly	začínat	k5eAaImAgInP	začínat
den	den	k1gInSc4	den
od	od	k7c2	od
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
od	od	k7c2	od
setmění	setmění	k1gNnSc2	setmění
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
národy	národ	k1gInPc1	národ
řídící	řídící	k2eAgInPc1d1	řídící
se	s	k7c7	s
solárním	solární	k2eAgInSc7d1	solární
kalendářem	kalendář	k1gInSc7	kalendář
začínaly	začínat	k5eAaImAgInP	začínat
den	den	k1gInSc1	den
východem	východ	k1gInSc7	východ
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Babylónu	Babylón	k1gInSc6	Babylón
odborníci	odborník	k1gMnPc1	odborník
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
jako	jako	k9	jako
počátek	počátek	k1gInSc4	počátek
dne	den	k1gInSc2	den
pro	pro	k7c4	pro
lidový	lidový	k2eAgInSc4d1	lidový
kalendář	kalendář	k1gInSc4	kalendář
západ	západ	k1gInSc1	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
však	však	k9	však
prý	prý	k9	prý
počítali	počítat	k5eAaImAgMnP	počítat
dny	den	k1gInPc4	den
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
římských	římský	k2eAgMnPc2d1	římský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
byl	být	k5eAaImAgInS	být
den	den	k1gInSc4	den
v	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
životě	život	k1gInSc6	život
počítán	počítat	k5eAaImNgMnS	počítat
od	od	k7c2	od
východu	východ	k1gInSc2	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Athén	Athéna	k1gFnPc2	Athéna
a	a	k8xC	a
Delf	Delfy	k1gFnPc2	Delfy
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
dobách	doba	k1gFnPc6	doba
počítali	počítat	k5eAaImAgMnP	počítat
dny	den	k1gInPc4	den
od	od	k7c2	od
východu	východ	k1gInSc2	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
prosadil	prosadit	k5eAaPmAgMnS	prosadit
lunisolární	lunisolární	k2eAgInSc4d1	lunisolární
kalendář	kalendář	k1gInSc4	kalendář
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
začátek	začátek	k1gInSc4	začátek
dne	den	k1gInSc2	den
na	na	k7c4	na
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Nepočítali	počítat	k5eNaImAgMnP	počítat
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
od	od	k7c2	od
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
setmění	setmění	k1gNnSc2	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
začátek	začátek	k1gInSc1	začátek
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
používán	používat	k5eAaImNgInS	používat
až	až	k6eAd1	až
do	do	k7c2	do
přijetí	přijetí	k1gNnSc2	přijetí
Juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
a	a	k8xC	a
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
počítali	počítat	k5eAaImAgMnP	počítat
čas	čas	k1gInSc4	čas
podle	podle	k7c2	podle
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
zahajovali	zahajovat	k5eAaImAgMnP	zahajovat
den	den	k1gInSc4	den
při	při	k7c6	při
východu	východ	k1gInSc6	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
píše	psát	k5eAaImIp3nS	psát
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
začínali	začínat	k5eAaImAgMnP	začínat
svůj	svůj	k3xOyFgInSc4	svůj
den	den	k1gInSc4	den
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
právního	právní	k2eAgNnSc2d1	právní
a	a	k8xC	a
sakrálního	sakrální	k2eAgNnSc2d1	sakrální
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
lidem	lid	k1gInSc7	lid
postačovalo	postačovat	k5eAaImAgNnS	postačovat
počítání	počítání	k1gNnSc1	počítání
pouze	pouze	k6eAd1	pouze
světelného	světelný	k2eAgInSc2d1	světelný
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnPc1d1	noční
hodiny	hodina	k1gFnPc1	hodina
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
připočítávány	připočítávat	k5eAaImNgFnP	připočítávat
k	k	k7c3	k
předchozímu	předchozí	k2eAgInSc3d1	předchozí
nebo	nebo	k8xC	nebo
následujícímu	následující	k2eAgInSc3d1	následující
dni	den	k1gInSc3	den
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgNnPc1d2	častější
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
počítání	počítání	k1gNnSc1	počítání
dne	den	k1gInSc2	den
od	od	k7c2	od
východu	východ	k1gInSc2	východ
Slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
kalendář	kalendář	k1gInSc1	kalendář
počítal	počítat	k5eAaImAgInS	počítat
dny	den	k1gInPc7	den
od	od	k7c2	od
západu	západ	k1gInSc2	západ
do	do	k7c2	do
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
od	od	k7c2	od
soumraku	soumrak	k1gInSc2	soumrak
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
tři	tři	k4xCgFnPc1	tři
hvězdy	hvězda	k1gFnPc1	hvězda
druhé	druhý	k4xOgFnSc2	druhý
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
převzat	převzat	k2eAgMnSc1d1	převzat
i	i	k9	i
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
počítala	počítat	k5eAaImAgFnS	počítat
den	den	k1gInSc4	den
od	od	k7c2	od
nešpor	nešpora	k1gFnPc2	nešpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
používal	používat	k5eAaImAgInS	používat
tzv.	tzv.	kA	tzv.
italský	italský	k2eAgInSc4d1	italský
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
den	den	k1gInSc1	den
začínal	začínat	k5eAaImAgInS	začínat
navečer	navečer	k6eAd1	navečer
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
šera	šero	k1gNnSc2	šero
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
po	po	k7c6	po
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
ze	z	k7c2	z
dne	den	k1gInSc2	den
<g/>
"	"	kIx"	"
tedy	tedy	k9	tedy
znamenal	znamenat	k5eAaImAgMnS	znamenat
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
po	po	k7c6	po
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
večerní	večerní	k2eAgInSc4d1	večerní
čas	čas	k1gInSc4	čas
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
posunout	posunout	k5eAaPmF	posunout
zpět	zpět	k6eAd1	zpět
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
den	den	k1gInSc4	den
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
počítání	počítání	k1gNnSc6	počítání
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
dnů	den	k1gInPc2	den
jako	jako	k8xC	jako
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
Halloween	Halloween	k1gInSc4	Halloween
<g/>
,	,	kIx,	,
a	a	k8xC	a
předvečer	předvečer	k1gInSc4	předvečer
svátku	svátek	k1gInSc2	svátek
svaté	svatý	k2eAgFnSc2d1	svatá
Anežky	Anežka	k1gFnSc2	Anežka
jsou	být	k5eAaImIp3nP	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
staršího	starý	k2eAgInSc2d2	starší
modelu	model	k1gInSc2	model
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svátky	svátek	k1gInPc1	svátek
začínaly	začínat	k5eAaImAgInP	začínat
již	již	k6eAd1	již
večer	večer	k6eAd1	večer
před	před	k7c7	před
vlastním	vlastní	k2eAgInSc7d1	vlastní
dnem	den	k1gInSc7	den
jejich	jejich	k3xOp3gFnPc2	jejich
oslav	oslava	k1gFnPc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytlačován	vytlačován	k2eAgInSc1d1	vytlačován
tzv.	tzv.	kA	tzv.
německým	německý	k2eAgInSc7d1	německý
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
den	den	k1gInSc1	den
začínal	začínat	k5eAaImAgInS	začínat
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
běžná	běžný	k2eAgFnSc1d1	běžná
konvence	konvence	k1gFnSc1	konvence
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
den	den	k1gInSc1	den
začíná	začínat	k5eAaImIp3nS	začínat
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
časem	časem	k6eAd1	časem
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
dosažením	dosažení	k1gNnSc7	dosažení
hodiny	hodina	k1gFnSc2	hodina
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
Ramadánu	ramadán	k1gInSc2	ramadán
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
půst	půst	k1gInSc4	půst
od	od	k7c2	od
úsvitu	úsvit	k1gInSc2	úsvit
do	do	k7c2	do
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Damašský	damašský	k2eAgInSc1d1	damašský
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kopie	kopie	k1gFnPc4	kopie
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nalezena	naleznout	k5eAaPmNgFnS	naleznout
mezi	mezi	k7c7	mezi
svitky	svitek	k1gInPc7	svitek
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zachovávání	zachovávání	k1gNnSc4	zachovávání
Sabatu	sabat	k1gInSc2	sabat
že	že	k8xS	že
<g/>
,	,	kIx,	,
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
zabývat	zabývat	k5eAaImF	zabývat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
prací	práce	k1gFnSc7	práce
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sluneční	sluneční	k2eAgInSc1d1	sluneční
disk	disk	k1gInSc1	disk
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
rovnající	rovnající	k2eAgFnSc6d1	rovnající
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgInSc3	svůj
průměru	průměr	k1gInSc3	průměr
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
zřejmě	zřejmě	k6eAd1	zřejmě
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klášterní	klášterní	k2eAgNnPc4d1	klášterní
společenství	společenství	k1gNnPc4	společenství
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgNnPc4d1	zabývající
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
prací	práce	k1gFnSc7	práce
<g/>
,	,	kIx,	,
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
dne	den	k1gInSc2	den
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
noc	noc	k1gFnSc1	noc
nazývá	nazývat	k5eAaImIp3nS	nazývat
po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
dni	den	k1gInSc6	den
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
páteční	páteční	k2eAgFnSc1d1	páteční
noc	noc	k1gFnSc1	noc
<g/>
"	"	kIx"	"
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
mezi	mezi	k7c7	mezi
pátkem	pátek	k1gInSc7	pátek
a	a	k8xC	a
sobotou	sobota	k1gFnSc7	sobota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
opozici	opozice	k1gFnSc6	opozice
k	k	k7c3	k
židovskému	židovský	k2eAgInSc3d1	židovský
vzoru	vzor	k1gInSc3	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
občanskému	občanský	k2eAgInSc3d1	občanský
dni	den	k1gInSc3	den
může	moct	k5eAaImIp3nS	moct
často	často	k6eAd1	často
vést	vést	k5eAaImF	vést
k	k	k7c3	k
omylům	omyl	k1gInPc3	omyl
a	a	k8xC	a
nedorozuměním	nedorozumění	k1gNnSc7	nedorozumění
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
uvedené	uvedený	k2eAgFnPc1d1	uvedená
v	v	k7c4	v
chod	chod	k1gInSc4	chod
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
uváděny	uvádět	k5eAaImNgFnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastaly	nastat	k5eAaPmAgFnP	nastat
den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgMnPc1d1	televizní
průvodci	průvodce	k1gMnPc1	průvodce
také	také	k9	také
uvádějí	uvádět	k5eAaImIp3nP	uvádět
noční	noční	k2eAgInPc1d1	noční
programy	program	k1gInPc1	program
k	k	k7c3	k
předchozímu	předchozí	k2eAgInSc3d1	předchozí
dni	den	k1gInSc3	den
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
programovací	programovací	k2eAgInSc1d1	programovací
VCR	VCR	kA	VCR
systém	systém	k1gInSc1	systém
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
přísnou	přísný	k2eAgFnSc4d1	přísná
logiku	logika	k1gFnSc4	logika
začátku	začátek	k1gInSc2	začátek
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
v	v	k7c6	v
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
další	další	k2eAgNnSc1d1	další
spornou	sporný	k2eAgFnSc7d1	sporná
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
VCR	VCR	kA	VCR
sety	set	k2eAgInPc4d1	set
s	s	k7c7	s
dvanáctihodinovou	dvanáctihodinový	k2eAgFnSc7d1	dvanáctihodinová
notací	notace	k1gFnSc7	notace
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
označení	označení	k1gNnSc1	označení
takového	takový	k3xDgInSc2	takový
záznamu	záznam	k1gInSc2	záznam
jako	jako	k9	jako
"	"	kIx"	"
<g/>
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
AM	AM	kA	AM
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazy	výraz	k1gInPc1	výraz
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
dnes	dnes	k6eAd1	dnes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
včera	včera	k6eAd1	včera
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
zítra	zítra	k6eAd1	zítra
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
nejednoznačnými	jednoznačný	k2eNgInPc7d1	nejednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Platnost	platnost	k1gFnSc1	platnost
denních	denní	k2eAgMnPc2d1	denní
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
propustek	propustek	k1gInSc1	propustek
apod.	apod.	kA	apod.
může	moct	k5eAaImIp3nS	moct
končit	končit	k5eAaImF	končit
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
nebo	nebo	k8xC	nebo
zavírací	zavírací	k2eAgFnSc7d1	zavírací
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
dřívější	dřívější	k2eAgInSc4d1	dřívější
termín	termín	k1gInSc4	termín
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
daná	daný	k2eAgFnSc1d1	daná
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
od	od	k7c2	od
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
čas	čas	k1gInSc4	čas
25	[number]	k4	25
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
poslední	poslední	k2eAgFnSc1d1	poslední
hodina	hodina	k1gFnSc1	hodina
počítána	počítán	k2eAgFnSc1d1	počítána
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
služby	služba	k1gFnSc2	služba
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
také	také	k9	také
pro	pro	k7c4	pro
uspořádání	uspořádání	k1gNnSc4	uspořádání
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
doba	doba	k1gFnSc1	doba
jednoho	jeden	k4xCgNnSc2	jeden
otočení	otočení	k1gNnSc2	otočení
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
otočení	otočení	k1gNnSc1	otočení
sleduje	sledovat	k5eAaImIp3nS	sledovat
relativně	relativně	k6eAd1	relativně
vůči	vůči	k7c3	vůči
hvězdnému	hvězdný	k2eAgNnSc3d1	Hvězdné
pozadí	pozadí	k1gNnSc3	pozadí
<g/>
,	,	kIx,	,
a	a	k8xC	a
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
otočení	otočení	k1gNnSc1	otočení
sleduje	sledovat	k5eAaImIp3nS	sledovat
relativně	relativně	k6eAd1	relativně
vůči	vůči	k7c3	vůči
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
současně	současně	k6eAd1	současně
s	s	k7c7	s
otáčením	otáčení	k1gNnSc7	otáčení
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
i	i	k9	i
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
den	den	k1gInSc4	den
o	o	k7c6	o
cca	cca	kA	cca
čtyři	čtyři	k4xCgFnPc1	čtyři
minuty	minuta	k1gFnPc1	minuta
kratší	krátký	k2eAgFnPc1d2	kratší
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
rozdíl	rozdíl	k1gInSc4	rozdíl
za	za	k7c4	za
rok	rok	k1gInSc4	rok
dá	dát	k5eAaPmIp3nS	dát
dohromady	dohromady	k6eAd1	dohromady
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jedno	jeden	k4xCgNnSc4	jeden
otočení	otočení	k1gNnPc2	otočení
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
jedním	jeden	k4xCgInSc7	jeden
oběhem	oběh	k1gInSc7	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
trvá	trvat	k5eAaImIp3nS	trvat
přesně	přesně	k6eAd1	přesně
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
den	den	k1gInSc4	den
trvá	trvat	k5eAaImIp3nS	trvat
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
56	[number]	k4	56
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
4,09	[number]	k4	4,09
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
slapovému	slapový	k2eAgNnSc3d1	slapové
zpomalování	zpomalování	k1gNnSc3	zpomalování
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
velice	velice	k6eAd1	velice
zvolna	zvolna	k6eAd1	zvolna
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
(	(	kIx(	(
<g/>
rotace	rotace	k1gFnSc1	rotace
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
650	[number]	k4	650
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
trvalo	trvat	k5eAaImAgNnS	trvat
jedno	jeden	k4xCgNnSc1	jeden
otočení	otočení	k1gNnSc1	otočení
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
86	[number]	k4	86
400,002	[number]	k4	400,002
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
se	se	k3xPyFc4	se
o	o	k7c4	o
cca	cca	kA	cca
2	[number]	k4	2
milisekundy	milisekunda	k1gFnPc4	milisekunda
za	za	k7c4	za
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
změny	změna	k1gFnSc2	změna
orbitu	orbita	k1gFnSc4	orbita
Měsíce	měsíc	k1gInSc2	měsíc
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
+2,3	+2,3	k4	+2,3
ms	ms	k?	ms
<g/>
/	/	kIx~	/
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
+1,8	+1,8	k4	+1,8
ms	ms	k?	ms
<g/>
/	/	kIx~	/
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
období	období	k1gNnSc1	období
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
půlnocemi	půlnoc	k1gFnPc7	půlnoc
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
od	od	k7c2	od
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
den	den	k1gInSc1	den
má	mít	k5eAaImIp3nS	mít
právě	právě	k9	právě
86	[number]	k4	86
400	[number]	k4	400
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přidání	přidání	k1gNnSc4	přidání
přestupné	přestupný	k2eAgFnSc2d1	přestupná
sekundy	sekunda	k1gFnSc2	sekunda
86	[number]	k4	86
401	[number]	k4	401
s	s	k7c7	s
(	(	kIx(	(
<g/>
teoreticky	teoreticky	k6eAd1	teoreticky
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
též	též	k9	též
přestupnou	přestupný	k2eAgFnSc4d1	přestupná
sekundu	sekunda	k1gFnSc4	sekunda
ubrat	ubrat	k5eAaPmF	ubrat
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
den	den	k1gInSc1	den
o	o	k7c4	o
86	[number]	k4	86
399	[number]	k4	399
s	s	k7c7	s
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taková	takový	k3xDgFnSc1	takový
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
protiklad	protiklad	k1gInSc1	protiklad
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
dobu	doba	k1gFnSc4	doba
od	od	k7c2	od
východu	východ	k1gInSc2	východ
do	do	k7c2	do
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
delší	dlouhý	k2eAgInSc4d2	delší
než	než	k8xS	než
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
dvěma	dva	k4xCgInPc7	dva
faktory	faktor	k1gInPc7	faktor
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
Slunce	slunce	k1gNnSc1	slunce
není	být	k5eNaImIp3nS	být
bodový	bodový	k2eAgInSc4d1	bodový
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
rozměr	rozměr	k1gInSc1	rozměr
cca	cca	kA	cca
32	[number]	k4	32
úhlových	úhlový	k2eAgFnPc2d1	úhlová
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
zemská	zemský	k2eAgFnSc1d1	zemská
atmosféra	atmosféra	k1gFnSc1	atmosféra
láme	lámat	k5eAaImIp3nS	lámat
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
Slunce	slunce	k1gNnSc1	slunce
pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
<g/>
,	,	kIx,	,
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
krajinu	krajina	k1gFnSc4	krajina
jeho	on	k3xPp3gInSc2	on
rozptýlené	rozptýlený	k2eAgNnSc1d1	rozptýlené
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečný	skutečný	k2eAgInSc1d1	skutečný
soumrak	soumrak	k1gInSc1	soumrak
nastává	nastávat	k5eAaImIp3nS	nastávat
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
střed	střed	k1gInSc1	střed
Slunce	slunce	k1gNnSc2	slunce
cca	cca	kA	cca
50	[number]	k4	50
úhlových	úhlový	k2eAgFnPc2d1	úhlová
minut	minuta	k1gFnPc2	minuta
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
čas	čas	k1gInSc1	čas
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
poloze	poloha	k1gFnSc6	poloha
a	a	k8xC	a
roční	roční	k2eAgFnSc6d1	roční
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
minimálně	minimálně	k6eAd1	minimálně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
písmenem	písmeno	k1gNnSc7	písmeno
d	d	k?	d
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
d	d	k?	d
=	=	kIx~	=
24	[number]	k4	24
h	h	k?	h
=	=	kIx~	=
1440	[number]	k4	1440
min	mina	k1gFnPc2	mina
=	=	kIx~	=
86	[number]	k4	86
400	[number]	k4	400
s	s	k7c7	s
V	V	kA	V
Bibli	bible	k1gFnSc4	bible
jako	jako	k8xC	jako
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Boha	bůh	k1gMnSc4	bůh
nepodstatný	podstatný	k2eNgInSc1d1	nepodstatný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysloveno	vyslovit	k5eAaPmNgNnS	vyslovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
totéž	týž	k3xTgNnSc4	týž
jako	jako	k8xS	jako
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Žalm	žalm	k1gInSc1	žalm
90	[number]	k4	90
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
;	;	kIx,	;
2	[number]	k4	2
list	list	k1gInSc1	list
Petrův	Petrův	k2eAgInSc1d1	Petrův
3	[number]	k4	3
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
listu	list	k1gInSc2	list
Petrově	Petrův	k2eAgFnSc6d1	Petrova
3	[number]	k4	3
<g/>
,	,	kIx,	,
8	[number]	k4	8
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
líčen	líčen	k2eAgInSc4d1	líčen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
expertů	expert	k1gMnPc2	expert
na	na	k7c4	na
Bibli	bible	k1gFnSc4	bible
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
tento	tento	k3xDgInSc4	tento
text	text	k1gInSc4	text
jako	jako	k8xS	jako
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
některých	některý	k3yIgNnPc2	některý
proroctví	proroctví	k1gNnPc2	proroctví
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
například	například	k6eAd1	například
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
Daniel	Daniel	k1gMnSc1	Daniel
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Janovo	Janův	k2eAgNnSc4d1	Janovo
zjevení	zjevení	k1gNnSc4	zjevení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňován	k2eAgInPc4d1	zmiňován
dny	den	k1gInPc4	den
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
