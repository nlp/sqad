<s>
Stupnice	stupnice	k1gFnSc1
Traynard	Traynarda	k1gFnPc2
–	–	k?
se	se	k3xPyFc4
naopak	naopak	k6eAd1
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
popisu	popis	k1gInSc2
konkrétních	konkrétní	k2eAgMnPc2d1
a	a	k8xC
zcela	zcela	k6eAd1
klíčových	klíčový	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
sjezdů	sjezd	k1gInPc2
<g/>
:	:	kIx,
sklon	sklon	k1gInSc1
<g/>
,	,	kIx,
expozice	expozice	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
„	„	k?
<g/>
vzdušnost	vzdušnost	k1gFnSc1
<g/>
“	“	k?
sjezdu	sjezd	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kritická	kritický	k2eAgNnPc4d1
místa	místo	k1gNnPc4
(	(	kIx(
<g/>
skály	skála	k1gFnPc1
<g/>
,	,	kIx,
ledovcové	ledovcový	k2eAgFnPc1d1
trhliny	trhlina	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
různých	různý	k2eAgFnPc2d1
stupnic	stupnice	k1gFnPc2
obtížnosti	obtížnost	k1gFnSc2
v	v	k7c6
extrémním	extrémní	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
<g/>
.	.	kIx.
</s>