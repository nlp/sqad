<s>
Stupnice	stupnice	k1gFnSc1
obtížnosti	obtížnost	k1gFnSc2
(	(	kIx(
<g/>
extrémní	extrémní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
klasifikaci	klasifikace	k1gFnSc6
obtížnosti	obtížnost	k1gFnSc2
v	v	k7c6
extrémním	extrémní	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Stupnice	stupnice	k1gFnSc2
obtížnosti	obtížnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mitterkar	Mitterkar	k1gInSc1
v	v	k7c6
údolí	údolí	k1gNnSc6
Malfon	Malfon	k1gInSc1
<g/>
(	(	kIx(
<g/>
Arlberg	Arlberg	k1gInSc1
<g/>
,	,	kIx,
Austria	Austrium	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
různých	různý	k2eAgFnPc2d1
stupnic	stupnice	k1gFnPc2
obtížnosti	obtížnost	k1gFnSc2
v	v	k7c6
extrémním	extrémní	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
přistupuje	přistupovat	k5eAaImIp3nS
k	k	k7c3
hodnocení	hodnocení	k1gNnSc3
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stupnice	stupnice	k1gFnSc1
Blanchére	Blanchér	k1gMnSc5
–	–	k?
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
horolezecké	horolezecký	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
schopnostech	schopnost	k1gFnPc6
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
lyžařů	lyžař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Stupnice	stupnice	k1gFnSc1
Traynard	Traynarda	k1gFnPc2
–	–	k?
se	se	k3xPyFc4
naopak	naopak	k6eAd1
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
popisu	popis	k1gInSc2
konkrétních	konkrétní	k2eAgMnPc2d1
a	a	k8xC
zcela	zcela	k6eAd1
klíčových	klíčový	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
sjezdů	sjezd	k1gInPc2
<g/>
:	:	kIx,
sklon	sklon	k1gInSc1
<g/>
,	,	kIx,
expozice	expozice	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
„	„	k?
<g/>
vzdušnost	vzdušnost	k1gFnSc1
<g/>
“	“	k?
sjezdu	sjezd	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kritická	kritický	k2eAgNnPc4d1
místa	místo	k1gNnPc4
(	(	kIx(
<g/>
skály	skála	k1gFnPc1
<g/>
,	,	kIx,
ledovcové	ledovcový	k2eAgFnPc1d1
trhliny	trhlina	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
S1	S1	k4
–	–	k?
terén	terén	k1gInSc4
rovinného	rovinný	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
</s>
<s>
S2	S2	k4
–	–	k?
mírně	mírně	k6eAd1
skloněný	skloněný	k2eAgInSc4d1
terén	terén	k1gInSc4
do	do	k7c2
20	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
bez	bez	k7c2
strmých	strmý	k2eAgInPc2d1
úseků	úsek	k1gInPc2
</s>
<s>
S3	S3	k4
–	–	k?
otevřené	otevřený	k2eAgInPc4d1
široké	široký	k2eAgInPc4d1
svahy	svah	k1gInPc4
do	do	k7c2
35	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
se	s	k7c7
strmějšími	strmý	k2eAgInPc7d2
úseky	úsek	k1gInPc7
</s>
<s>
S4	S4	k4
–	–	k?
sklon	sklona	k1gFnPc2
do	do	k7c2
45	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
bez	bez	k7c2
větší	veliký	k2eAgFnSc2d2
expozice	expozice	k1gFnSc2
</s>
<s>
S5	S5	k4
–	–	k?
žlaby	žlab	k1gInPc1
a	a	k8xC
kuloáry	kuloár	k1gInPc1
se	s	k7c7
sklonem	sklon	k1gInSc7
45	#num#	k4
<g/>
°	°	k?
–	–	k?
55	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
35	#num#	k4
<g/>
°	°	k?
–	–	k?
45	#num#	k4
<g/>
°	°	k?
při	při	k7c6
velké	velký	k2eAgFnSc6d1
expozici	expozice	k1gFnSc6
</s>
<s>
S6	S6	k4
–	–	k?
sklon	sklon	k1gInSc1
nad	nad	k7c7
50	#num#	k4
<g/>
°	°	k?
při	při	k7c6
velké	velký	k2eAgFnSc6d1
expozici	expozice	k1gFnSc6
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
víc	hodně	k6eAd2
než	než	k8xS
55	#num#	k4
<g/>
°	°	k?
</s>
<s>
S7	S7	k4
–	–	k?
skoky	skok	k1gInPc1
přes	přes	k7c4
skalní	skalní	k2eAgInPc4d1
pásy	pás	k1gInPc4
a	a	k8xC
ledovcové	ledovcový	k2eAgInPc4d1
zlomy	zlom	k1gInPc4
na	na	k7c6
velmi	velmi	k6eAd1
strmých	strmý	k2eAgInPc6d1
svazích	svah	k1gInPc6
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lyžování	lyžování	k1gNnSc1
</s>
<s>
Skialpinismus	Skialpinismus	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
