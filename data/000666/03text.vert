<s>
Titania	Titanium	k1gNnSc2	Titanium
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
jsou	být	k5eAaImIp3nP	být
velikostí	velikost	k1gFnSc7	velikost
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
Oberon	Oberon	k1gInSc1	Oberon
<g/>
,	,	kIx,	,
Umbriel	Umbriel	k1gInSc1	Umbriel
a	a	k8xC	a
Ariel	Ariel	k1gInSc1	Ariel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
436	[number]	k4	436
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
577,8	[number]	k4	577,8
km	km	kA	km
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
cca	cca	kA	cca
3,526	[number]	k4	3,526
×	×	k?	×
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
kg	kg	kA	kg
<g/>
,	,	kIx,	,
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
8,7	[number]	k4	8,7
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
poloviny	polovina	k1gFnSc2	polovina
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
křemičitany	křemičitan	k1gInPc4	křemičitan
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
látky	látka	k1gFnPc1	látka
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
metanu	metan	k1gInSc2	metan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
útvarem	útvar	k1gInSc7	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
kaňon	kaňon	k1gInSc1	kaňon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
několikanásobně	několikanásobně	k6eAd1	několikanásobně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
svojí	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
poset	posít	k5eAaPmNgInS	posít
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
a	a	k8xC	a
prasklinami	prasklina	k1gFnPc7	prasklina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Williamem	William	k1gInSc7	William
Herschelem	Herschel	k1gInSc7	Herschel
již	již	k9	již
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgInSc1d2	podrobnější
průzkum	průzkum	k1gInSc1	průzkum
a	a	k8xC	a
snímky	snímek	k1gInPc1	snímek
měsíce	měsíc	k1gInSc2	měsíc
pořídila	pořídit	k5eAaPmAgFnS	pořídit
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proletěla	proletět	k5eAaPmAgFnS	proletět
369	[number]	k4	369
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
Uranovy	Uranův	k2eAgInPc4d1	Uranův
měsíce	měsíc	k1gInPc4	měsíc
nese	nést	k5eAaImIp3nS	nést
Titania	Titanium	k1gNnPc1	Titanium
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
díla	dílo	k1gNnSc2	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
podle	podle	k7c2	podle
manželky	manželka	k1gFnSc2	manželka
krále	král	k1gMnSc4	král
elfů	elf	k1gMnPc2	elf
Oberona	Oberon	k1gMnSc4	Oberon
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
<g/>
.	.	kIx.	.
</s>
