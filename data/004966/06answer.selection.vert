<s>
Nivchština	Nivchština	k1gFnSc1	Nivchština
nebo	nebo	k8xC	nebo
též	též	k9	též
giljačtina	giljačtina	k1gFnSc1	giljačtina
(	(	kIx(	(
<g/>
nivchsky	nivchsky	k6eAd1	nivchsky
<g/>
:	:	kIx,	:
Н	Н	k?	Н
д	д	k?	д
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
ニ	ニ	k?	ニ
<g/>
/	/	kIx~	/
<g/>
ギ	ギ	k?	ギ
<g/>
,	,	kIx,	,
nivufu-go	nivufuo	k1gMnSc1	nivufu-go
<g/>
/	/	kIx~	/
<g/>
girijaku-go	girijakuo	k1gMnSc1	girijaku-go
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osamocený	osamocený	k2eAgInSc1d1	osamocený
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
mluví	mluvit	k5eAaImIp3nS	mluvit
zhruba	zhruba	k6eAd1	zhruba
1000	[number]	k4	1000
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sachalin	Sachalin	k1gInSc1	Sachalin
a	a	k8xC	a
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
řeky	řeka	k1gFnSc2	řeka
Amur	Amur	k1gInSc1	Amur
<g/>
.	.	kIx.	.
</s>
