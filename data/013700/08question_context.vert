<s>
Alžběta	Alžběta	k1gFnSc1
Amálie	Amálie	k1gFnSc1
Evženie	Evženie	k1gFnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1837	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1898	#num#	k4
<g/>
,	,	kIx,
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
<g/>
:	:	kIx,
Elisabeth	Elisabeth	k1gFnSc1
Amalie	Amalie	k1gFnSc1
Eugenie	Eugenie	k1gFnSc1
<g/>
,	,	kIx,
Herzogin	Herzogin	k2eAgInSc1d1
in	in	k?
Bayern	Bayern	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
přezdívkou	přezdívka	k1gFnSc7
Sissi	Sissi	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
bavorská	bavorský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
z	z	k7c2
rodu	rod	k1gInSc2
Wittelsbachů	Wittelsbach	k1gInPc2
a	a	k8xC
po	po	k7c6
svém	svůj	k3xOyFgInSc6
sňatku	sňatek	k1gInSc6
s	s	k7c7
Františkem	František	k1gMnSc7
Josefem	Josef	k1gMnSc7
rakouská	rakouský	k2eAgFnSc1d1
císařovna	císařovna	k1gFnSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
a	a	k8xC
korunovaná	korunovaný	k2eAgFnSc1d1
uherská	uherský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Erzsébet	Erzsébet	k1gFnPc1
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Wittelsbach	Wittelsbach	k1gMnSc1
Erzsébet	Erzsébet		k1gFnPc1
magyar	magyar	k1gMnSc1
királyné	királyné	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>