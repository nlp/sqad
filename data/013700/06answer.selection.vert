<s>
Císařovna	císařovna	k1gFnSc1	císařovna
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
45	[number]	k4	45
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
nenechala	nechat	k5eNaPmAgFnS	nechat
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
portrétovat	portrétovat	k5eAaImF	portrétovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěla	chtít	k5eAaImAgFnS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
věčně	věčně	k6eAd1	věčně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
ve	v	k7c6	v
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
