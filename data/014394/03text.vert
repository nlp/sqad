<s>
Společenství	společenství	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Společenství	společenství	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
Vlajka	vlajka	k1gFnSc1
SNS	SNS	kA
</s>
<s>
Zkratka	zkratka	k1gFnSc1
</s>
<s>
SNS	SNS	kA
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Ukrajinská	ukrajinský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
a	a	k8xC
Běloruská	běloruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1991	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Minsk	Minsk	k1gInSc1
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
60	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
102	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Členové	člen	k1gMnPc1
</s>
<s>
9	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
přidružené	přidružený	k2eAgFnPc1d1
členství	členství	k1gNnSc2
Výkonný	výkonný	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Sergej	Sergej	k1gMnSc1
Lebeděv	Lebeděv	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.cis.minsk.by	www.cis.minsk.ba	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Společenství	společenství	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
SNS	SNS	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
С	С	kA
Н	Н	kA
Г	Г	kA
(	(	kIx(
<g/>
С	С	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sodružestvo	Sodružestvo	k1gNnSc1
nězavisimych	nězavisime	k2eNgFnPc2d1
gosudarstv	gosudarstvo	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
organizace	organizace	k1gFnSc1
zahrnující	zahrnující	k2eAgFnSc1d1
9	#num#	k4
z	z	k7c2
15	#num#	k4
bývalých	bývalý	k2eAgFnPc2d1
svazových	svazový	k2eAgFnPc2d1
republik	republika	k1gFnPc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
uskupení	uskupení	k1gNnSc2
</s>
<s>
SNS	SNS	kA
zahrnuje	zahrnovat	k5eAaImIp3nS
Arménii	Arménie	k1gFnSc4
<g/>
,	,	kIx,
Ázerbájdžán	Ázerbájdžán	k1gInSc4
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
<g/>
,	,	kIx,
Kazachstán	Kazachstán	k1gInSc1
<g/>
,	,	kIx,
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
,	,	kIx,
Moldavsko	Moldavsko	k1gNnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
Tádžikistán	Tádžikistán	k1gInSc1
a	a	k8xC
Uzbekistán	Uzbekistán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukrajina	Ukrajina	k1gFnSc1
přístupovou	přístupový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
nikdy	nikdy	k6eAd1
neratifikovala	ratifikovat	k5eNaBmAgFnS
a	a	k8xC
zachovala	zachovat	k5eAaPmAgFnS
si	se	k3xPyFc3
status	status	k1gInSc4
pozorovatele	pozorovatel	k1gMnSc4
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
anexí	anexe	k1gFnSc7
Krymu	Krym	k1gInSc2
Ruskem	Rusko	k1gNnSc7
oznámila	oznámit	k5eAaPmAgFnS
odchod	odchod	k1gInSc4
z	z	k7c2
tohoto	tento	k3xDgNnSc2
společenství	společenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turkmenistán	Turkmenistán	k1gInSc1
přerušil	přerušit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
stálé	stálý	k2eAgNnSc4d1
členství	členství	k1gNnSc4
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
přidruženým	přidružený	k2eAgInSc7d1
členem	člen	k1gInSc7
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
Gruzie	Gruzie	k1gFnSc1
odchod	odchod	k1gInSc4
ze	z	k7c2
SNS	SNS	kA
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
pak	pak	k6eAd1
gruzínský	gruzínský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
vzhledem	vzhledem	k7c3
k	k	k7c3
ozbrojenému	ozbrojený	k2eAgInSc3d1
konfliktu	konflikt	k1gInSc3
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
hlasováním	hlasování	k1gNnSc7
anuloval	anulovat	k5eAaBmAgMnS
smlouvy	smlouva	k1gFnSc2
o	o	k7c4
členství	členství	k1gNnSc4
v	v	k7c6
SNS	SNS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sídlem	sídlo	k1gNnSc7
Společenství	společenství	k1gNnSc2
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
je	být	k5eAaImIp3nS
běloruský	běloruský	k2eAgInSc1d1
Minsk	Minsk	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
SNS	SNS	kA
vzniklo	vzniknout	k5eAaPmAgNnS
na	na	k7c4
podzim	podzim	k1gInSc4
1991	#num#	k4
jako	jako	k8xS,k8xC
volný	volný	k2eAgInSc4d1
svazek	svazek	k1gInSc4
postsovětských	postsovětský	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
nově	nově	k6eAd1
získanou	získaný	k2eAgFnSc7d1
samostatností	samostatnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1991	#num#	k4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
setkání	setkání	k1gNnSc3
ruských	ruský	k2eAgMnPc2d1
<g/>
,	,	kIx,
ukrajinských	ukrajinský	k2eAgMnPc2d1
a	a	k8xC
běloruských	běloruský	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
Bělověžskou	Bělověžský	k2eAgFnSc7d1
dohodou	dohoda	k1gFnSc7
dohodli	dohodnout	k5eAaPmAgMnP
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
nové	nový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
otevřené	otevřený	k2eAgFnSc2d1
všem	všecek	k3xTgInPc3
postsovětským	postsovětský	k2eAgInPc3d1
státům	stát	k1gInPc3
(	(	kIx(
<g/>
za	za	k7c4
18	#num#	k4
dní	den	k1gInPc2
se	se	k3xPyFc4
SSSR	SSSR	kA
rozpadl	rozpadnout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1991	#num#	k4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
pak	pak	k6eAd1
v	v	k7c6
Alma-Atě	Alma-Ata	k1gFnSc6
do	do	k7c2
společenství	společenství	k1gNnSc2
přistoupilo	přistoupit	k5eAaPmAgNnS
dalších	další	k2eAgInPc2d1
osm	osm	k4xCc4
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
v	v	k7c6
Albertville	Albertvilla	k1gFnSc6
a	a	k8xC
Barceloně	Barcelona	k1gFnSc6
vystoupili	vystoupit	k5eAaPmAgMnP
sportovci	sportovec	k1gMnPc1
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
SNS	SNS	kA
poprvé	poprvé	k6eAd1
a	a	k8xC
naposled	naposled	k6eAd1
ve	v	k7c6
společných	společný	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
v	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgInSc2
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pobaltské	pobaltský	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
odmítly	odmítnout	k5eAaPmAgFnP
stát	stát	k5eAaPmF,k5eAaImF
součástí	součást	k1gFnSc7
SNS	SNS	kA
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
považovaly	považovat	k5eAaImAgInP
své	svůj	k3xOyFgNnSc4
předešlé	předešlý	k2eAgNnSc4d1
členství	členství	k1gNnSc4
v	v	k7c6
SSSR	SSSR	kA
za	za	k7c4
vnucené	vnucený	k2eAgFnPc4d1
a	a	k8xC
protiprávní	protiprávní	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Gruzie	Gruzie	k1gFnSc1
zprvu	zprvu	k6eAd1
taktéž	taktéž	k?
stála	stát	k5eAaImAgFnS
mimo	mimo	k7c4
SNS	SNS	kA
<g/>
,	,	kIx,
vstoupila	vstoupit	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
ze	z	k7c2
Společenství	společenství	k1gNnSc2
vystoupil	vystoupit	k5eAaPmAgMnS
Turkmenistán	Turkmenistán	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
tak	tak	k6eAd1
potvrdil	potvrdit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
izolacionistickou	izolacionistický	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
;	;	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
přidruženým	přidružený	k2eAgInSc7d1
členem	člen	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Organizace	organizace	k1gFnSc1
nemá	mít	k5eNaImIp3nS
příliš	příliš	k6eAd1
nadstátních	nadstátní	k2eAgFnPc2d1
pravomocí	pravomoc	k1gFnPc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
spíše	spíše	k9
symbolický	symbolický	k2eAgMnSc1d1
ve	v	k7c6
smyslu	smysl	k1gInSc6
návaznosti	návaznost	k1gFnSc2
na	na	k7c6
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působnost	působnost	k1gFnSc1
SNS	SNS	kA
se	se	k3xPyFc4
omezuje	omezovat	k5eAaImIp3nS
víceméně	víceméně	k9
na	na	k7c4
koordinaci	koordinace	k1gFnSc4
společných	společný	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
<g/>
,	,	kIx,
finančních	finanční	k2eAgFnPc2d1
<g/>
,	,	kIx,
legislativních	legislativní	k2eAgFnPc2d1
a	a	k8xC
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc7d3
dohodou	dohoda	k1gFnSc7
uskutečněnou	uskutečněný	k2eAgFnSc7d1
v	v	k7c6
rámci	rámec	k1gInSc6
SNS	SNS	kA
bylo	být	k5eAaImAgNnS
vytvoření	vytvoření	k1gNnSc1
společné	společný	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zóna	zóna	k1gFnSc1
měla	mít	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
začít	začít	k5eAaPmF
existovat	existovat	k5eAaImF
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
</s>
<s>
Georgia	Georgia	k1gFnSc1
to	ten	k3xDgNnSc1
leave	leavat	k5eAaPmIp3nS
alliance	alliance	k1gFnSc1
of	of	k?
ex-Soviet	ex-Soviet	k1gMnSc1
states	states	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Georgian	Georgian	k1gMnSc1
parliament	parliament	k1gMnSc1
votes	votes	k1gMnSc1
to	ten	k3xDgNnSc4
quit	quit	k1gMnSc1
CIS	cis	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
China	China	k1gFnSc1
View	View	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.britannica.com/EBchecked/topic/128945/Commonwealth-of-Independent-States-CIS	http://www.britannica.com/EBchecked/topic/128945/Commonwealth-of-Independent-States-CIS	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Postsovětské	postsovětský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
</s>
<s>
Blízké	blízký	k2eAgNnSc1d1
zahraničí	zahraničí	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Společenství	společenství	k1gNnSc2
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Výkonný	výkonný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
SNS	SNS	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Deklarace	deklarace	k1gFnSc1
z	z	k7c2
Alma-Aty	Alma-Ata	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Společenství	společenství	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
SNS	SNS	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc4
•	•	k?
Bělorusko	Bělorusko	k1gNnSc4
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Rusko	Rusko	k1gNnSc4
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
Přidružené	přidružený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc4
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Postsovětské	postsovětský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
Členské	členský	k2eAgInPc4d1
státy	stát	k1gInPc4
OSN	OSN	kA
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
Arménie	Arménie	k1gFnSc2
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
Estonsko	Estonsko	k1gNnSc4
•	•	k?
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc4
•	•	k?
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc4
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
Mezinárodně	mezinárodně	k6eAd1
neuznané	uznaný	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
Abcházie	Abcházie	k1gFnSc2
•	•	k?
Arcach	Arcach	k1gInSc1
Arcach	Arcacha	k1gFnPc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Podněstří	Podněstří	k1gMnSc1
Podněstří	Podněstří	k1gFnSc2
•	•	k?
DLR	DLR	kA
DLR	DLR	kA
•	•	k?
Luhanská	Luhanský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Zaniklé	zaniklý	k2eAgNnSc1d1
<g/>
:	:	kIx,
Republika	republika	k1gFnSc1
Gagauzsko	Gagauzsko	k1gNnSc1
•	•	k?
Čečenská	čečenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Ičkerie	Ičkerie	k1gFnSc2
•	•	k?
Republika	republika	k1gFnSc1
Krym	Krym	k1gInSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
</s>
<s>
Svaz	svaz	k1gInSc1
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Běloruska	Bělorusko	k1gNnSc2
•	•	k?
SNS	SNS	kA
(	(	kIx(
<g/>
Ekonomický	ekonomický	k2eAgInSc1d1
soud	soud	k1gInSc1
Společenství	společenství	k1gNnSc2
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
•	•	k?
Mezistátní	mezistátní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
•	•	k?
Zóna	zóna	k1gFnSc1
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
SNS	SNS	kA
<g/>
)	)	kIx)
•	•	k?
EAU	EAU	kA
•	•	k?
Společenství	společenství	k1gNnSc2
neuznaných	uznaný	k2eNgInPc2d1
států	stát	k1gInPc2
•	•	k?
OSKB	OSKB	kA
•	•	k?
Euroasijské	euroasijský	k2eAgNnSc1d1
ekonomické	ekonomický	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
•	•	k?
Jednotný	jednotný	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
prostor	prostor	k1gInSc1
(	(	kIx(
<g/>
Celní	celní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
jednotného	jednotný	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Eurasijský	Eurasijský	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
•	•	k?
GUAM	GUAM	kA
•	•	k?
Baltské	baltský	k2eAgNnSc4d1
shromáždění	shromáždění	k1gNnSc4
•	•	k?
Společenství	společenství	k1gNnSc2
střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
(	(	kIx(
<g/>
organizace	organizace	k1gFnSc1
přestala	přestat	k5eAaPmAgFnS
fungovat	fungovat	k5eAaImF
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
134501	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2128486-6	2128486-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2159	#num#	k4
2250	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
92019874	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131775197	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
92019874	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
