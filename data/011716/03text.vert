<p>
<s>
Doom	Doom	k1gMnSc1	Doom
<g/>
,	,	kIx,	,
čti	číst	k5eAaImRp2nS	číst
[	[	kIx(	[
<g/>
du	du	k?	du
<g/>
:	:	kIx,	:
<g/>
m	m	kA	m
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
akční	akční	k2eAgFnSc1d1	akční
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
společností	společnost	k1gFnPc2	společnost
ID	Ida	k1gFnPc2	Ida
Software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hru	hra	k1gFnSc4	hra
navázala	navázat	k5eAaPmAgFnS	navázat
další	další	k2eAgFnSc1d1	další
3	[number]	k4	3
pokračování	pokračování	k1gNnPc2	pokračování
<g/>
:	:	kIx,	:
Doom	Doom	k1gInSc1	Doom
II	II	kA	II
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Doom	Doom	k1gInSc1	Doom
3	[number]	k4	3
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
reboot	reboot	k1gMnSc1	reboot
Doom	Doom	k1gMnSc1	Doom
vyšel	vyjít	k5eAaPmAgMnS	vyjít
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hry	hra	k1gFnSc2	hra
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k9	i
film	film	k1gInSc4	film
a	a	k8xC	a
vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
komiksy	komiks	k1gInPc4	komiks
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Doom	Doom	k6eAd1	Doom
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
sci-fi	scii	k1gFnSc1	sci-fi
<g/>
/	/	kIx~	/
<g/>
horor	horor	k1gInSc1	horor
příběh	příběh	k1gInSc1	příběh
elitního	elitní	k2eAgInSc2d1	elitní
mariňáka	mariňáka	k?	mariňáka
vyslaného	vyslaný	k2eAgMnSc4d1	vyslaný
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
svého	svůj	k3xOyFgMnSc2	svůj
velícího	velící	k2eAgMnSc2d1	velící
důstojníka	důstojník	k1gMnSc2	důstojník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gFnPc1	jeho
jednotky	jednotka	k1gFnPc1	jednotka
začaly	začít	k5eAaPmAgFnP	začít
střílet	střílet	k5eAaImF	střílet
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mariňáky	mariňáky	k?	mariňáky
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc4	Mars
tím	ten	k3xDgNnSc7	ten
nejnudnějším	nudný	k2eAgNnSc7d3	nejnudnější
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
UAC	UAC	kA	UAC
(	(	kIx(	(
<g/>
Union	union	k1gInSc4	union
Aerospace	Aerospace	k1gFnSc2	Aerospace
Corporation	Corporation	k1gInSc1	Corporation
<g/>
)	)	kIx)	)
tajně	tajně	k6eAd1	tajně
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
s	s	k7c7	s
teleportací	teleportací	k2eAgFnSc4d1	teleportací
vytvořením	vytvoření	k1gNnSc7	vytvoření
brány	brána	k1gFnSc2	brána
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
měsíci	měsíc	k1gInPc7	měsíc
Marsu	Mars	k1gInSc2	Mars
<g/>
:	:	kIx,	:
Phobosem	Phobos	k1gInSc7	Phobos
a	a	k8xC	a
Deimosem	Deimos	k1gInSc7	Deimos
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
šlo	jít	k5eAaImAgNnS	jít
vše	všechen	k3xTgNnSc1	všechen
dobře	dobře	k6eAd1	dobře
-	-	kIx~	-
teleportace	teleportace	k1gFnSc1	teleportace
fungovala	fungovat	k5eAaImAgFnS	fungovat
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Cosi	cosi	k3yInSc1	cosi
se	se	k3xPyFc4	se
ale	ale	k9	ale
pokazilo	pokazit	k5eAaPmAgNnS	pokazit
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgInPc1d1	počítačový
systémy	systém	k1gInPc1	systém
na	na	k7c6	na
Phobosu	Phobos	k1gInSc6	Phobos
hlásily	hlásit	k5eAaImAgInP	hlásit
poruchu	porucha	k1gFnSc4	porucha
a	a	k8xC	a
Deimos	Deimos	k1gInSc4	Deimos
zmizel	zmizet	k5eAaPmAgMnS	zmizet
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
teleportační	teleportační	k2eAgFnSc2d1	teleportační
brány	brána	k1gFnSc2	brána
začli	začli	k?	začli
vylézat	vylézat	k5eAaImF	vylézat
démoni	démon	k1gMnPc1	démon
z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
nebo	nebo	k8xC	nebo
posedli	posednout	k5eAaPmAgMnP	posednout
všechny	všechen	k3xTgMnPc4	všechen
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
UAC	UAC	kA	UAC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdržíte	obdržet	k5eAaPmIp2nP	obdržet
zprávu	zpráva	k1gFnSc4	zpráva
od	od	k7c2	od
vědců	vědec	k1gMnPc2	vědec
se	s	k7c7	s
zběsilým	zběsilý	k2eAgNnSc7d1	zběsilé
voláním	volání	k1gNnSc7	volání
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
dalších	další	k2eAgMnPc2d1	další
vojáků	voják	k1gMnPc2	voják
odletíte	odletět	k5eAaPmIp2nP	odletět
na	na	k7c4	na
Phobos	Phobos	k1gInSc4	Phobos
<g/>
.	.	kIx.	.
</s>
<s>
Mariňák	Mariňák	k?	Mariňák
má	mít	k5eAaImIp3nS	mít
hlídat	hlídat	k5eAaImF	hlídat
obvod	obvod	k1gInSc1	obvod
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pistolí	pistol	k1gFnSc7	pistol
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
postupuje	postupovat	k5eAaImIp3nS	postupovat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
komunikace	komunikace	k1gFnSc1	komunikace
přeruší	přerušit	k5eAaPmIp3nS	přerušit
<g/>
,	,	kIx,	,
vyjde	vyjít	k5eAaPmIp3nS	vyjít
mariňákovi	mariňákův	k2eAgMnPc1d1	mariňákův
vše	všechen	k3xTgNnSc4	všechen
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
on	on	k3xPp3gMnSc1	on
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Mariňák	Mariňák	k?	Mariňák
sám	sám	k3xTgMnSc1	sám
nemůže	moct	k5eNaImIp3nS	moct
opustit	opustit	k5eAaPmF	opustit
Phobos	Phobos	k1gInSc4	Phobos
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
jediná	jediný	k2eAgFnSc1d1	jediná
cesta	cesta	k1gFnSc1	cesta
ven	ven	k6eAd1	ven
je	být	k5eAaImIp3nS	být
probojovat	probojovat	k5eAaPmF	probojovat
se	se	k3xPyFc4	se
komplexem	komplex	k1gInSc7	komplex
Phobosu	Phobos	k1gInSc2	Phobos
(	(	kIx(	(
<g/>
Celkem	celkem	k6eAd1	celkem
musí	muset	k5eAaImIp3nS	muset
projít	projít	k5eAaPmF	projít
27	[number]	k4	27
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
3	[number]	k4	3
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Expanze	expanze	k1gFnSc1	expanze
Ultimate	Ultimat	k1gInSc5	Ultimat
Doom	Doom	k1gInSc4	Doom
přidává	přidávat	k5eAaImIp3nS	přidávat
navíc	navíc	k6eAd1	navíc
jednu	jeden	k4xCgFnSc4	jeden
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
epizoda	epizoda	k1gFnSc1	epizoda
Knee-Deep	Knee-Deep	k1gInSc1	Knee-Deep
in	in	k?	in
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
(	(	kIx(	(
<g/>
Po	po	k7c6	po
kolena	koleno	k1gNnPc4	koleno
ve	v	k7c6	v
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
v	v	k7c4	v
shareware	shareware	k1gInSc4	shareware
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
výzkumných	výzkumný	k2eAgInPc6d1	výzkumný
<g/>
,	,	kIx,	,
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
a	a	k8xC	a
vojenských	vojenský	k2eAgInPc6d1	vojenský
high-tech	high	k1gInPc6	high-t
základnách	základna	k1gFnPc6	základna
<g/>
,	,	kIx,	,
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
,	,	kIx,	,
počítačových	počítačový	k2eAgNnPc6d1	počítačové
centrech	centrum	k1gNnPc6	centrum
a	a	k8xC	a
geologických	geologický	k2eAgFnPc2d1	geologická
anomálií	anomálie	k1gFnPc2	anomálie
na	na	k7c4	na
Phobosu	Phobosa	k1gFnSc4	Phobosa
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nP	muset
mariňák	mariňák	k?	mariňák
těmito	tento	k3xDgFnPc7	tento
budovami	budova	k1gFnPc7	budova
projít	projít	k5eAaPmF	projít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
probojuje	probojovat	k5eAaPmIp3nS	probojovat
celým	celý	k2eAgInSc7d1	celý
Phobosem	Phobos	k1gInSc7	Phobos
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
teleportu	teleport	k1gInSc2	teleport
vedoucí	vedoucí	k1gFnSc2	vedoucí
na	na	k7c4	na
Deimos	Deimos	k1gInSc4	Deimos
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
epizodě	epizoda	k1gFnSc6	epizoda
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Shores	Shores	k1gMnSc1	Shores
of	of	k?	of
Hell	Hell	k1gMnSc1	Hell
(	(	kIx(	(
<g/>
Břehy	břeh	k1gInPc1	břeh
pekla	peklo	k1gNnSc2	peklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mariňák	mariňák	k?	mariňák
musí	muset	k5eAaImIp3nS	muset
probojovat	probojovat	k5eAaPmF	probojovat
přes	přes	k7c4	přes
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
Deimosu	Deimosa	k1gFnSc4	Deimosa
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
těm	ten	k3xDgMnPc3	ten
na	na	k7c4	na
Phobosu	Phobosa	k1gFnSc4	Phobosa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zborcené	zborcený	k2eAgFnSc2d1	zborcená
a	a	k8xC	a
deformované	deformovaný	k2eAgFnSc2d1	deformovaná
invazí	invaze	k1gFnSc7	invaze
démonů	démon	k1gMnPc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
Zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Deimosu	Deimos	k1gInSc2	Deimos
porazí	porazit	k5eAaPmIp3nS	porazit
monstrózního	monstrózní	k2eAgMnSc4d1	monstrózní
Cyberdémona	Cyberdémon	k1gMnSc4	Cyberdémon
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
zmizelém	zmizelý	k2eAgInSc6d1	zmizelý
měsíci	měsíc	k1gInSc6	měsíc
<g/>
:	:	kIx,	:
vznáší	vznášet	k5eAaImIp3nS	vznášet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
samotným	samotný	k2eAgNnSc7d1	samotné
peklem	peklo	k1gNnSc7	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
epizoda	epizoda	k1gFnSc1	epizoda
s	s	k7c7	s
názvem	název	k1gInSc7	název
Inferno	inferno	k1gNnSc1	inferno
začíná	začínat	k5eAaImIp3nS	začínat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
mariňák	mariňák	k?	mariňák
sestoupá	sestoupat	k5eAaPmIp3nS	sestoupat
z	z	k7c2	z
Deimosu	Deimos	k1gInSc2	Deimos
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
probojuje	probojovat	k5eAaPmIp3nS	probojovat
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zde	zde	k6eAd1	zde
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
monstrózního	monstrózní	k2eAgMnSc4d1	monstrózní
Spider	Spider	k1gInSc1	Spider
Masterminda	Mastermind	k1gMnSc4	Mastermind
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
porazí	porazit	k5eAaPmIp3nS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
projde	projít	k5eAaPmIp3nS	projít
skrytým	skrytý	k2eAgInSc7d1	skrytý
průchodem	průchod	k1gInSc7	průchod
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
hořící	hořící	k2eAgNnSc1d1	hořící
město	město	k1gNnSc1	město
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
králíka	králík	k1gMnSc2	králík
nabodlá	nabodlat	k5eAaImIp3nS	nabodlat
na	na	k7c4	na
kůl	kůl	k1gInSc4	kůl
(	(	kIx(	(
<g/>
v	v	k7c6	v
Ultimate	Ultimat	k1gInSc5	Ultimat
Doomu	Doom	k1gInSc3	Doom
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k8xC	jako
domácí	domácí	k2eAgMnSc1d1	domácí
králík	králík	k1gMnSc1	králík
mariňáka	mariňáka	k?	mariňáka
<g/>
,	,	kIx,	,
Daisy	Dais	k1gInPc1	Dais
<g/>
)	)	kIx)	)
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
démoni	démon	k1gMnPc1	démon
napadli	napadnout	k5eAaPmAgMnP	napadnout
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
připravují	připravovat	k5eAaImIp3nP	připravovat
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
následující	následující	k2eAgInSc1d1	následující
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Doom	Doom	k1gInSc1	Doom
II	II	kA	II
<g/>
:	:	kIx,	:
Hell	Hell	k1gInSc4	Hell
on	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
expanzi	expanze	k1gFnSc6	expanze
Ultimate	Ultimat	k1gInSc5	Ultimat
Doom	Doom	k1gInPc3	Doom
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
epizodě	epizoda	k1gFnSc6	epizoda
Thy	Thy	k1gFnPc2	Thy
Flesh	Flesh	k1gMnSc1	Flesh
Consumed	Consumed	k1gMnSc1	Consumed
(	(	kIx(	(
<g/>
Tvé	tvůj	k3xOp2gNnSc1	tvůj
konzumované	konzumovaný	k2eAgNnSc1d1	konzumované
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
mariňák	mariňák	k?	mariňák
statečně	statečně	k6eAd1	statečně
postaví	postavit	k5eAaPmIp3nS	postavit
proti	proti	k7c3	proti
hordám	horda	k1gFnPc3	horda
démonů	démon	k1gMnPc2	démon
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
Spider	Spider	k1gMnSc1	Spider
Mastermind	Mastermind	k1gMnSc1	Mastermind
zaslal	zaslat	k5eAaPmAgMnS	zaslat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
skrytého	skrytý	k2eAgInSc2d1	skrytý
průchodu	průchod	k1gInSc2	průchod
<g/>
.	.	kIx.	.
</s>
<s>
Pozemské	pozemský	k2eAgFnPc1d1	pozemská
lokality	lokalita	k1gFnPc1	lokalita
této	tento	k3xDgFnSc2	tento
epizody	epizoda	k1gFnSc2	epizoda
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
high-tech	high	k1gInPc6	high-t
základen	základna	k1gFnPc2	základna
a	a	k8xC	a
démonických	démonický	k2eAgInPc2d1	démonický
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Grafický	grafický	k2eAgMnSc1d1	grafický
engine	enginout	k5eAaPmIp3nS	enginout
==	==	k?	==
</s>
</p>
<p>
<s>
Grafický	grafický	k2eAgMnSc1d1	grafický
engine	enginout	k5eAaPmIp3nS	enginout
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
revoluční	revoluční	k2eAgFnSc1d1	revoluční
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgFnSc3	žádný
jiné	jiný	k2eAgFnSc3d1	jiná
hře	hra	k1gFnSc3	hra
se	se	k3xPyFc4	se
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nepovedl	povést	k5eNaPmAgMnS	povést
takový	takový	k3xDgInSc4	takový
grafický	grafický	k2eAgInSc4d1	grafický
skok	skok	k1gInSc4	skok
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
první	první	k4xOgMnSc1	první
first	first	k1gMnSc1	first
<g/>
-	-	kIx~	-
<g/>
person	persona	k1gFnPc2	persona
<g/>
-	-	kIx~	-
<g/>
shooter	shooter	k1gInSc1	shooter
od	od	k7c2	od
ID	Ida	k1gFnPc2	Ida
software	software	k1gInSc4	software
s	s	k7c7	s
názvem	název	k1gInSc7	název
Wolfenstein	Wolfenstein	k1gInSc4	Wolfenstein
3D	[number]	k4	3D
tu	tu	k6eAd1	tu
přibyly	přibýt	k5eAaPmAgInP	přibýt
textury	textura	k1gFnPc4	textura
podlah	podlaha	k1gFnPc2	podlaha
a	a	k8xC	a
stropu	strop	k1gInSc2	strop
<g/>
,	,	kIx,	,
paralaxní	paralaxní	k2eAgFnSc1d1	paralaxní
obloha	obloha	k1gFnSc1	obloha
<g/>
,	,	kIx,	,
víceúrovňové	víceúrovňový	k2eAgFnPc1d1	víceúrovňová
mapy	mapa	k1gFnPc1	mapa
se	se	k3xPyFc4	se
schody	schod	k1gInPc7	schod
a	a	k8xC	a
výtahy	výtah	k1gInPc7	výtah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Engine	Enginout	k5eAaPmIp3nS	Enginout
hry	hra	k1gFnPc4	hra
je	být	k5eAaImIp3nS	být
3D	[number]	k4	3D
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
třetího	třetí	k4xOgMnSc2	třetí
rozměru	rozměr	k1gInSc2	rozměr
<g/>
;	;	kIx,	;
nelze	lze	k6eNd1	lze
tedy	tedy	k9	tedy
používat	používat	k5eAaImF	používat
některé	některý	k3yIgInPc4	některý
typické	typický	k2eAgInPc4d1	typický
rysy	rys	k1gInPc4	rys
3D	[number]	k4	3D
prostředí	prostředí	k1gNnSc2	prostředí
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vícepatrové	vícepatrový	k2eAgFnPc4d1	vícepatrová
místnosti	místnost	k1gFnPc4	místnost
apod.	apod.	kA	apod.
Rozlišení	rozlišení	k1gNnSc4	rozlišení
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
320	[number]	k4	320
×	×	k?	×
200	[number]	k4	200
px	px	k?	px
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
průměrné	průměrný	k2eAgNnSc1d1	průměrné
rozlišení	rozlišení	k1gNnSc4	rozlišení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doom	Doom	k1gMnSc1	Doom
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
hrou	hra	k1gFnSc7	hra
náročnou	náročný	k2eAgFnSc4d1	náročná
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
špičkový	špičkový	k2eAgInSc4d1	špičkový
engine	engin	k1gInSc5	engin
využilo	využít	k5eAaPmAgNnS	využít
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Heretic	Heretice	k1gFnPc2	Heretice
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Raven	Raven	k2eAgInSc1d1	Raven
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
a	a	k8xC	a
z	z	k7c2	z
Heretica	Heretic	k1gInSc2	Heretic
pak	pak	k6eAd1	pak
Hexen	Hexen	k2eAgInSc1d1	Hexen
<g/>
:	:	kIx,	:
Beyond	Beyond	k1gInSc1	Beyond
Heretic	Heretice	k1gFnPc2	Heretice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
Id	Ida	k1gFnPc2	Ida
Software	software	k1gInSc1	software
úspěšně	úspěšně	k6eAd1	úspěšně
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
revolučnosti	revolučnost	k1gFnSc6	revolučnost
hry	hra	k1gFnSc2	hra
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hry	hra	k1gFnPc4	hra
typu	typ	k1gInSc2	typ
first	first	k1gInSc1	first
<g/>
-	-	kIx~	-
<g/>
person	persona	k1gFnPc2	persona
<g/>
-	-	kIx~	-
<g/>
shooter	shooter	k1gInSc1	shooter
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc1	název
Důmovka	Důmovka	k1gFnSc1	Důmovka
<g/>
.	.	kIx.	.
<g/>
Grafický	grafický	k2eAgInSc1d1	grafický
engine	enginout	k5eAaPmIp3nS	enginout
Doom	Doom	k1gInSc1	Doom
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
platformy	platforma	k1gFnPc4	platforma
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
také	také	k9	také
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
můžete	moct	k5eAaImIp2nP	moct
zahrát	zahrát	k5eAaPmF	zahrát
například	například	k6eAd1	například
na	na	k7c6	na
ipodu	ipod	k1gInSc6	ipod
se	s	k7c7	s
zvláště	zvláště	k6eAd1	zvláště
upraveným	upravený	k2eAgInSc7d1	upravený
linuxem	linux	k1gInSc7	linux
–	–	k?	–
iPod	iPod	k1gInSc1	iPod
Linux	linux	k1gInSc1	linux
Project	Project	k1gInSc1	Project
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
mobilech	mobil	k1gInPc6	mobil
s	s	k7c7	s
OS	OS	kA	OS
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soubory	soubor	k1gInPc1	soubor
.	.	kIx.	.
<g/>
WAD	WAD	k?	WAD
(	(	kIx(	(
<g/>
iwady	iwada	k1gMnSc2	iwada
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
Doom	Dooma	k1gFnPc2	Dooma
(	(	kIx(	(
<g/>
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
klony	klon	k1gInPc1	klon
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
veškeré	veškerý	k3xTgFnPc4	veškerý
textury	textura	k1gFnPc4	textura
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
zdí	zeď	k1gFnPc2	zeď
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc4	mapa
úrovní	úroveň	k1gFnPc2	úroveň
a	a	k8xC	a
zvuky	zvuk	k1gInPc4	zvuk
uložené	uložený	k2eAgInPc4d1	uložený
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
.	.	kIx.	.
<g/>
WAD	WAD	k?	WAD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
editaci	editace	k1gFnSc6	editace
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
kategorií	kategorie	k1gFnPc2	kategorie
–	–	k?	–
některé	některý	k3yIgFnPc1	některý
upravují	upravovat	k5eAaImIp3nP	upravovat
úrovně	úroveň	k1gFnPc4	úroveň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
WadAuthor	WadAuthor	k1gInSc1	WadAuthor
<g/>
,	,	kIx,	,
DEU	DEU	kA	DEU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
upravují	upravovat	k5eAaImIp3nP	upravovat
zase	zase	k9	zase
grafiku	grafika	k1gFnSc4	grafika
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
NWT	NWT	kA	NWT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
editují	editovat	k5eAaImIp3nP	editovat
samotný	samotný	k2eAgInSc4d1	samotný
spustitelný	spustitelný	k2eAgInSc4d1	spustitelný
soubor	soubor	k1gInSc4	soubor
EXE	EXE	kA	EXE
(	(	kIx(	(
<g/>
doom	doom	k1gMnSc1	doom
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
nebo	nebo	k8xC	nebo
doom	doom	k1gInSc1	doom
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
hrou	hra	k1gFnSc7	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dehacked	Dehacked	k1gInSc1	Dehacked
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
Doom	Doom	k1gInSc1	Doom
není	být	k5eNaImIp3nS	být
plně	plně	k6eAd1	plně
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
velmi	velmi	k6eAd1	velmi
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
editaci	editace	k1gFnSc4	editace
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
2D	[number]	k4	2D
mapa	mapa	k1gFnSc1	mapa
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
rozměr	rozměr	k1gInSc4	rozměr
je	být	k5eAaImIp3nS	být
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
stanovená	stanovený	k2eAgFnSc1d1	stanovená
jako	jako	k8xS	jako
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
udávaná	udávaný	k2eAgFnSc1d1	udávaná
nad	nad	k7c7	nad
základní	základní	k2eAgFnSc7d1	základní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
mít	mít	k5eAaImF	mít
dva	dva	k4xCgInPc4	dva
prostory	prostor	k1gInPc4	prostor
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
portech	port	k1gInPc6	port
<g/>
,	,	kIx,	,
např.	např.	kA	např.
GZDoom	GZDoom	k1gInSc1	GZDoom
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
lze	lze	k6eAd1	lze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
upravené	upravený	k2eAgFnPc1d1	upravená
iwady	iwada	k1gFnPc1	iwada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
udělají	udělat	k5eAaPmIp3nP	udělat
z	z	k7c2	z
Dooma	Doomum	k1gNnSc2	Doomum
úplně	úplně	k6eAd1	úplně
jinou	jiný	k2eAgFnSc4d1	jiná
hru	hra	k1gFnSc4	hra
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
udělají	udělat	k5eAaPmIp3nP	udělat
z	z	k7c2	z
Dooma	Doomum	k1gNnSc2	Doomum
Half	halfa	k1gFnPc2	halfa
<g/>
-	-	kIx~	-
<g/>
Life	Life	k1gInSc1	Life
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
například	například	k6eAd1	například
stáhnout	stáhnout	k5eAaPmF	stáhnout
OpenPoom	OpenPoom	k1gInSc4	OpenPoom
iwad	iwada	k1gFnPc2	iwada
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
předělávkou	předělávka	k1gFnSc7	předělávka
klasického	klasický	k2eAgNnSc2d1	klasické
Dooma	Doomum	k1gNnSc2	Doomum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GPL	GPL	kA	GPL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Editace	editace	k1gFnSc1	editace
úrovní	úroveň	k1gFnPc2	úroveň
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
editoru	editor	k1gInSc6	editor
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
úroveň	úroveň	k1gFnSc1	úroveň
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
jako	jako	k9	jako
dvojrozměrná	dvojrozměrný	k2eAgFnSc1d1	dvojrozměrná
mapa	mapa	k1gFnSc1	mapa
<g/>
;	;	kIx,	;
přestože	přestože	k8xS	přestože
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
trojrozměrné	trojrozměrný	k2eAgNnSc1d1	trojrozměrné
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
sektorů	sektor	k1gInPc2	sektor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
oddělené	oddělený	k2eAgInPc4d1	oddělený
stěnami	stěna	k1gFnPc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
oboustranné	oboustranný	k2eAgInPc1d1	oboustranný
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
spojují	spojovat	k5eAaImIp3nP	spojovat
dva	dva	k4xCgInPc4	dva
sektory	sektor	k1gInPc4	sektor
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jednostranné	jednostranný	k2eAgNnSc1d1	jednostranné
(	(	kIx(	(
<g/>
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
needitovaný	editovaný	k2eNgInSc1d1	editovaný
prostor	prostor	k1gInSc1	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
stěně	stěna	k1gFnSc6	stěna
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
sice	sice	k8xC	sice
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
textura	textura	k1gFnSc1	textura
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
sektory	sektor	k1gInPc1	sektor
nenapojují	napojovat	k5eNaImIp3nP	napojovat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
dvě	dva	k4xCgFnPc4	dva
textury	textura	k1gFnPc4	textura
(	(	kIx(	(
<g/>
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
dole	dole	k6eAd1	dole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
u	u	k7c2	u
vytváření	vytváření	k1gNnSc2	vytváření
oken	okno	k1gNnPc2	okno
nebo	nebo	k8xC	nebo
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
(	(	kIx(	(
<g/>
sidedefs	sidedefs	k1gInSc1	sidedefs
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
uzlech	uzel	k1gInPc6	uzel
(	(	kIx(	(
<g/>
vertex	vertex	k1gInSc1	vertex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
rohy	roh	k1gInPc1	roh
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
sektorů	sektor	k1gInPc2	sektor
(	(	kIx(	(
<g/>
místností	místnost	k1gFnPc2	místnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
zde	zde	k6eAd1	zde
stýkat	stýkat	k5eAaImF	stýkat
ohromné	ohromný	k2eAgNnSc4d1	ohromné
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Sektory	sektor	k1gInPc1	sektor
nebo	nebo	k8xC	nebo
stěny	stěna	k1gFnPc1	stěna
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
překrývat	překrývat	k5eAaImF	překrývat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ale	ale	k9	ale
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
hry	hra	k1gFnSc2	hra
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
podivným	podivný	k2eAgInPc3d1	podivný
efektům	efekt	k1gInPc3	efekt
<g/>
;	;	kIx,	;
oba	dva	k4xCgInPc1	dva
překrývané	překrývaný	k2eAgInPc1d1	překrývaný
sektory	sektor	k1gInPc1	sektor
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
prolínat	prolínat	k5eAaImF	prolínat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
fungovat	fungovat	k5eAaImF	fungovat
nějaký	nějaký	k3yIgInSc4	nějaký
mechanismus	mechanismus	k1gInSc4	mechanismus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
snižující	snižující	k2eAgInSc4d1	snižující
se	se	k3xPyFc4	se
strop	strop	k1gInSc4	strop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
nadefinovat	nadefinovat	k5eAaPmF	nadefinovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
stěny	stěna	k1gFnSc2	stěna
(	(	kIx(	(
<g/>
klidně	klidně	k6eAd1	klidně
i	i	k9	i
průchozí	průchozí	k2eAgFnSc1d1	průchozí
<g/>
)	)	kIx)	)
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uvedla	uvést	k5eAaPmAgFnS	uvést
nějaký	nějaký	k3yIgInSc4	nějaký
sektor	sektor	k1gInSc4	sektor
v	v	k7c4	v
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
stačí	stačit	k5eAaBmIp3nS	stačit
jenom	jenom	k9	jenom
stěnu	stěna	k1gFnSc4	stěna
použít	použít	k5eAaPmF	použít
(	(	kIx(	(
<g/>
zmáčknutí	zmáčknutí	k1gNnSc4	zmáčknutí
tlačítka	tlačítko	k1gNnSc2	tlačítko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střelit	střelit	k5eAaPmF	střelit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc2	on
projít	projít	k5eAaPmF	projít
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
provázanosti	provázanost	k1gFnSc3	provázanost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
tag	tag	k1gInSc1	tag
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
level	level	k1gInSc1	level
Doomu	Doom	k1gInSc2	Doom
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
20	[number]	k4	20
tagů	tag	k1gInPc2	tag
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
ovládají	ovládat	k5eAaImIp3nP	ovládat
i	i	k9	i
více	hodně	k6eAd2	hodně
sektorů	sektor	k1gInPc2	sektor
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zbraně	zbraň	k1gFnPc1	zbraň
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
nepřátel	nepřítel	k1gMnPc2	nepřítel
slouží	sloužit	k5eAaImIp3nP	sloužit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
boxer	boxer	k1gInSc1	boxer
/	/	kIx~	/
motorová	motorový	k2eAgFnSc1d1	motorová
pila	pila	k1gFnSc1	pila
(	(	kIx(	(
<g/>
oboje	oboj	k1gFnSc2	oboj
pod	pod	k7c7	pod
jednou	jeden	k4xCgFnSc7	jeden
klávesou	klávesa	k1gFnSc7	klávesa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pistole	pistole	k1gFnSc1	pistole
</s>
</p>
<p>
<s>
brokovnice	brokovnice	k1gFnSc1	brokovnice
pumpovací	pumpovací	k2eAgFnSc2d1	pumpovací
</s>
</p>
<p>
<s>
rotační	rotační	k2eAgInSc1d1	rotační
kulomet	kulomet	k1gInSc1	kulomet
</s>
</p>
<p>
<s>
raketomet	raketomet	k1gInSc1	raketomet
</s>
</p>
<p>
<s>
plazmová	plazmový	k2eAgFnSc1d1	plazmová
puška	puška	k1gFnSc1	puška
</s>
</p>
<p>
<s>
BFG9000	BFG9000	k4	BFG9000
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
(	(	kIx(	(
<g/>
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
herní	herní	k2eAgFnSc6d1	herní
historii	historie	k1gFnSc6	historie
i	i	k9	i
nejspíš	nejspíš	k9	nejspíš
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
<g/>
)	)	kIx)	)
zbraň	zbraň	k1gFnSc1	zbraň
schopná	schopný	k2eAgFnSc1d1	schopná
zabíjet	zabíjet	k5eAaImF	zabíjet
hordy	horda	k1gFnPc4	horda
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
BFG	BFG	kA	BFG
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Tom	Tom	k1gMnSc1	Tom
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
člen	člen	k1gMnSc1	člen
Id	Ida	k1gFnPc2	Ida
Software	software	k1gInSc1	software
teamu	team	k1gInSc2	team
<g/>
)	)	kIx)	)
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
originální	originální	k2eAgFnSc2d1	originální
hry	hra	k1gFnSc2	hra
DOOM	DOOM	kA	DOOM
a	a	k8xC	a
v	v	k7c6	v
manuálu	manuál	k1gInSc6	manuál
hry	hra	k1gFnSc2	hra
DOOM	DOOM	kA	DOOM
2	[number]	k4	2
jako	jako	k8xS	jako
Big	Big	k1gFnPc1	Big
Fucking	Fucking	k1gInSc1	Fucking
Gun	Gun	k1gFnSc2	Gun
(	(	kIx(	(
<g/>
Kurevsky	Kurevsko	k1gNnPc7	Kurevsko
velká	velký	k2eAgFnSc1d1	velká
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
i	i	k9	i
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
adaptaci	adaptace	k1gFnSc6	adaptace
hry	hra	k1gFnSc2	hra
DOOM	DOOM	kA	DOOM
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
výklady	výklad	k1gInPc1	výklad
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Bio	Bio	k?	Bio
Force	force	k1gFnSc1	force
Gun	Gun	k1gFnSc1	Gun
(	(	kIx(	(
<g/>
biologická	biologický	k2eAgFnSc1d1	biologická
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Big	Big	k1gFnSc1	Big
Fragging	Fragging	k1gInSc1	Fragging
Gun	Gun	k1gFnSc1	Gun
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
tříštící	tříštící	k2eAgFnSc1d1	tříštící
puška	puška	k1gFnSc1	puška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Big	Big	k1gFnSc1	Big
Fat	fatum	k1gNnPc2	fatum
Gun	Gun	k1gFnSc2	Gun
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
tlustá	tlustý	k2eAgFnSc1d1	tlustá
puška	puška	k1gFnSc1	puška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Big	Big	k1gFnSc1	Big
Fun	Fun	k1gFnSc1	Fun
Gun	Gun	k1gFnSc1	Gun
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
zábavná	zábavný	k2eAgFnSc1d1	zábavná
puška	puška	k1gFnSc1	puška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Blast	Blast	k1gMnSc1	Blast
Field	Field	k1gMnSc1	Field
Gun	Gun	k1gMnSc1	Gun
(	(	kIx(	(
<g/>
Výbušná	výbušný	k2eAgFnSc1d1	výbušná
polní	polní	k2eAgFnSc1d1	polní
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepřátelé	nepřítel	k1gMnPc5	nepřítel
==	==	k?	==
</s>
</p>
<p>
<s>
Zombieman	Zombieman	k1gMnSc1	Zombieman
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
posedlý	posedlý	k2eAgMnSc1d1	posedlý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zelené	zelený	k2eAgInPc4d1	zelený
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
rudé	rudý	k2eAgFnPc4d1	rudá
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
uniformu	uniforma	k1gFnSc4	uniforma
má	mít	k5eAaImIp3nS	mít
zaskvrnělou	zaskvrnělý	k2eAgFnSc4d1	zaskvrnělý
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
útočnou	útočný	k2eAgFnSc4d1	útočná
pušku	puška	k1gFnSc4	puška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shotgun	Shotgun	k1gMnSc1	Shotgun
Guy	Guy	k1gMnSc1	Guy
-	-	kIx~	-
Taktéž	Taktéž	k?	Taktéž
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
plešatý	plešatý	k2eAgMnSc1d1	plešatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
černou	černý	k2eAgFnSc4d1	černá
uniformu	uniforma	k1gFnSc4	uniforma
a	a	k8xC	a
rudé	rudý	k2eAgFnPc4d1	rudá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
brokovnici	brokovnice	k1gFnSc4	brokovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Imp	Imp	k?	Imp
-	-	kIx~	-
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
démon	démon	k1gMnSc1	démon
posetý	posetý	k2eAgInSc1d1	posetý
bílými	bílý	k2eAgInPc7d1	bílý
ostny	osten	k1gInPc7	osten
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
střílí	střílet	k5eAaImIp3nS	střílet
ohnivé	ohnivý	k2eAgFnPc4d1	ohnivá
koule	koule	k1gFnPc4	koule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Demon	Demon	k1gNnSc1	Demon
-	-	kIx~	-
Narůžovělý	narůžovělý	k2eAgMnSc1d1	narůžovělý
démon	démon	k1gMnSc1	démon
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
pouze	pouze	k6eAd1	pouze
svými	svůj	k3xOyFgInPc7	svůj
pařáty	pařát	k1gInPc7	pařát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spectre	Spectr	k1gMnSc5	Spectr
-	-	kIx~	-
Neviditelný	viditelný	k2eNgInSc1d1	neviditelný
Demon	Demon	k1gInSc1	Demon
-	-	kIx~	-
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
šmouha	šmouha	k1gFnSc1	šmouha
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
stejné	stejný	k2eAgInPc4d1	stejný
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lost	Lost	k2eAgInSc1d1	Lost
Soul	Soul	k1gInSc1	Soul
-	-	kIx~	-
Létající	létající	k2eAgFnSc1d1	létající
hořící	hořící	k2eAgFnSc1d1	hořící
lebka	lebka	k1gFnSc1	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
naráží	narážet	k5eAaPmIp3nS	narážet
do	do	k7c2	do
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cacodemon	Cacodemon	k1gNnSc1	Cacodemon
-	-	kIx~	-
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámější	známý	k2eAgFnSc1d3	nejznámější
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
Dooma	Doomum	k1gNnSc2	Doomum
<g/>
.	.	kIx.	.
</s>
<s>
Létající	létající	k2eAgFnSc1d1	létající
červená	červený	k2eAgFnSc1d1	červená
koule	koule	k1gFnSc1	koule
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rohy	roh	k1gInPc4	roh
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
zelené	zelený	k2eAgNnSc1d1	zelené
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tlamy	tlama	k1gFnSc2	tlama
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
kulové	kulový	k2eAgInPc4d1	kulový
blesky	blesk	k1gInPc4	blesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Baron	baron	k1gMnSc1	baron
of	of	k?	of
Hell	Hell	k1gMnSc1	Hell
-	-	kIx~	-
Už	už	k6eAd1	už
větší	veliký	k2eAgMnSc1d2	veliký
démon	démon	k1gMnSc1	démon
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc4	noha
má	mít	k5eAaImIp3nS	mít
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
hnědou	hnědý	k2eAgFnSc7d1	hnědá
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
kopyta	kopyto	k1gNnSc2	kopyto
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
čert	čert	k1gMnSc1	čert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořejší	hořejší	k2eAgFnSc1d1	hořejší
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
má	mít	k5eAaImIp3nS	mít
narůžovělou	narůžovělý	k2eAgFnSc7d1	narůžovělá
<g/>
.	.	kIx.	.
</s>
<s>
Ruce	ruka	k1gFnPc1	ruka
mu	on	k3xPp3gMnSc3	on
hoří	hořet	k5eAaImIp3nP	hořet
zeleným	zelený	k2eAgInSc7d1	zelený
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zelených	zelený	k2eAgFnPc2d1	zelená
koulí	koule	k1gFnPc2	koule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cyberdemon	Cyberdemon	k1gNnSc1	Cyberdemon
-	-	kIx~	-
Velký	velký	k2eAgMnSc1d1	velký
démon	démon	k1gMnSc1	démon
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Baron	baron	k1gMnSc1	baron
of	of	k?	of
hell	helnout	k5eAaPmAgMnS	helnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
hnědý	hnědý	k2eAgInSc1d1	hnědý
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
má	mít	k5eAaImIp3nS	mít
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
robotickými	robotický	k2eAgInPc7d1	robotický
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kyborg	kyborg	k1gInSc1	kyborg
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
mechanické	mechanický	k2eAgFnSc2d1	mechanická
ruky	ruka	k1gFnSc2	ruka
s	s	k7c7	s
kanónem	kanón	k1gInSc7	kanón
místo	místo	k7c2	místo
prstů	prst	k1gInPc2	prst
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
rakety	raketa	k1gFnPc4	raketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spider	Spider	k1gMnSc1	Spider
Mastermind	Mastermind	k1gMnSc1	Mastermind
-	-	kIx~	-
Boss	boss	k1gMnSc1	boss
celé	celý	k2eAgFnSc2d1	celá
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vcelku	vcelku	k6eAd1	vcelku
děsivý	děsivý	k2eAgInSc4d1	děsivý
vzhled	vzhled	k1gInSc4	vzhled
-	-	kIx~	-
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
mozek	mozek	k1gInSc1	mozek
připevněný	připevněný	k2eAgInSc1d1	připevněný
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
mechanické	mechanický	k2eAgFnPc4d1	mechanická
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
i	i	k9	i
tvář	tvář	k1gFnSc1	tvář
-	-	kIx~	-
ostré	ostrý	k2eAgInPc4d1	ostrý
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
rudé	rudý	k2eAgFnPc4d1	rudá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
boji	boj	k1gInSc3	boj
používá	používat	k5eAaImIp3nS	používat
silný	silný	k2eAgInSc4d1	silný
rotační	rotační	k2eAgInSc4d1	rotační
kulomet	kulomet	k1gInSc4	kulomet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odezva	odezva	k1gFnSc1	odezva
==	==	k?	==
</s>
</p>
<p>
<s>
Doom	Doom	k1gMnSc1	Doom
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
nesmírně	smírně	k6eNd1	smírně
populární	populární	k2eAgFnSc1d1	populární
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
Doom	Doom	k1gMnSc1	Doom
nainstalovaný	nainstalovaný	k2eAgMnSc1d1	nainstalovaný
na	na	k7c6	na
více	hodně	k6eAd2	hodně
počítačích	počítač	k1gInPc6	počítač
na	na	k7c6	na
světě	svět	k1gInSc6	svět
než	než	k8xS	než
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
hry	hra	k1gFnSc2	hra
vedla	vést	k5eAaImAgFnS	vést
Billa	Bill	k1gMnSc4	Bill
Gatese	Gatese	k1gFnSc1	Gatese
k	k	k7c3	k
úvahám	úvaha	k1gFnPc3	úvaha
o	o	k7c6	o
koupi	koupě	k1gFnSc6	koupě
id	idy	k1gFnPc2	idy
Software	software	k1gInSc1	software
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
Doomu	Doom	k1gInSc2	Doom
jako	jako	k8xS	jako
prostředku	prostředek	k1gInSc2	prostředek
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
nového	nový	k2eAgInSc2d1	nový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fanoušků	fanoušek	k1gMnPc2	fanoušek
Doomu	Doom	k1gInSc2	Doom
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
rysy	rys	k1gInPc4	rys
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k1gMnSc1	jiný
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zdokonalit	zdokonalit	k5eAaPmF	zdokonalit
nebo	nebo	k8xC	nebo
zlepšit	zlepšit	k5eAaPmF	zlepšit
–	–	k?	–
nízké	nízký	k2eAgInPc4d1	nízký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
hardware	hardware	k1gInSc4	hardware
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc1d1	velká
možnosti	možnost	k1gFnPc1	možnost
měnit	měnit	k5eAaImF	měnit
hru	hra	k1gFnSc4	hra
(	(	kIx(	(
<g/>
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
editace	editace	k1gFnSc1	editace
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
importovat	importovat	k5eAaBmF	importovat
grafiku	grafika	k1gFnSc4	grafika
<g/>
,	,	kIx,	,
stahování	stahování	k1gNnSc4	stahování
dalších	další	k2eAgFnPc2d1	další
úrovní	úroveň	k1gFnPc2	úroveň
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
vyšly	vyjít	k5eAaPmAgInP	vyjít
programy	program	k1gInPc4	program
jako	jako	k8xS	jako
ZDOOM	ZDOOM	kA	ZDOOM
a	a	k8xC	a
Doom	Doo	k1gNnSc7	Doo
Legacy	Legaca	k1gFnSc2	Legaca
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
grafiku	grafika	k1gFnSc4	grafika
na	na	k7c4	na
dnešní	dnešní	k2eAgFnSc4d1	dnešní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Source	Sourec	k1gInPc1	Sourec
Porty	porta	k1gFnSc2	porta
<g/>
(	(	kIx(	(
<g/>
Enginy	Engina	k1gFnSc2	Engina
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
ZDOOM	ZDOOM	kA	ZDOOM
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
ZDoom	ZDoom	k1gInSc1	ZDoom
engine	enginout	k5eAaPmIp3nS	enginout
který	který	k3yQgInSc1	který
používá	používat	k5eAaImIp3nS	používat
původní	původní	k2eAgFnPc4d1	původní
datové	datový	k2eAgFnPc4d1	datová
WAD	WAD	k?	WAD
soubory	soubor	k1gInPc1	soubor
hra	hra	k1gFnSc1	hra
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nových	nový	k2eAgFnPc2d1	nová
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
původního	původní	k2eAgMnSc2d1	původní
Doom	Doom	k1gInSc4	Doom
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
a	a	k8xC	a
Doom	Doom	k1gInSc1	Doom
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
funguje	fungovat	k5eAaImIp3nS	fungovat
ZDoom	ZDoom	k1gInSc1	ZDoom
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
modifikace	modifikace	k1gFnSc1	modifikace
bezproblémově	bezproblémově	k6eAd1	bezproblémově
pod	pod	k7c7	pod
Windows	Windows	kA	Windows
XP	XP	kA	XP
a	a	k8xC	a
Windows	Windows	kA	Windows
Vista	vista	k2eAgMnSc1d1	vista
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
tak	tak	k6eAd1	tak
Doom	Doom	k1gFnPc3	Doom
i	i	k8xC	i
dnešním	dnešní	k2eAgMnPc3d1	dnešní
hráčům	hráč	k1gMnPc3	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možnost	možnost	k1gFnSc4	možnost
nastavení	nastavení	k1gNnSc2	nastavení
různých	různý	k2eAgInPc2d1	různý
módů	mód	k1gInPc2	mód
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
skákání	skákání	k1gNnSc1	skákání
<g/>
,	,	kIx,	,
rozhlížení	rozhlížení	k1gNnSc1	rozhlížení
myší	myš	k1gFnPc2	myš
nahoru	nahoru	k6eAd1	nahoru
i	i	k9	i
dolu	dol	k1gInSc2	dol
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgNnSc1d2	lepší
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
a	a	k8xC	a
stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
zbraních	zbraň	k1gFnPc6	zbraň
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
GZDOOM	GZDOOM	kA	GZDOOM
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
verzi	verze	k1gFnSc4	verze
ZDOOMu	ZDOOM	k2eAgFnSc4d1	ZDOOM
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
OPENGL	OPENGL	kA	OPENGL
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
autorem	autor	k1gMnSc7	autor
ZDOOMu	ZDOOMus	k1gInSc2	ZDOOMus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
zde	zde	k6eAd1	zde
koukat	koukat	k5eAaImF	koukat
plně	plně	k6eAd1	plně
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolu	dol	k1gInSc2	dol
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
MD	MD	kA	MD
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
MD	MD	kA	MD
<g/>
3	[number]	k4	3
modely	model	k1gInPc4	model
<g/>
;	;	kIx,	;
vysoké	vysoký	k2eAgNnSc4d1	vysoké
rozlišení	rozlišení	k1gNnSc4	rozlišení
textur	textura	k1gFnPc2	textura
<g/>
,	,	kIx,	,
3D	[number]	k4	3D
podlahy	podlaha	k1gFnSc2	podlaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
SKULLTAG	SKULLTAG	kA	SKULLTAG
===	===	k?	===
</s>
</p>
<p>
<s>
Kopie	kopie	k1gFnSc1	kopie
GZDOOMu	GZDOOMus	k1gInSc2	GZDOOMus
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
multiplayeru	multiplayer	k1gInSc2	multiplayer
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
32	[number]	k4	32
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zandronum	Zandronum	k1gInSc4	Zandronum
===	===	k?	===
</s>
</p>
<p>
<s>
Herní	herní	k2eAgFnSc1d1	herní
Engine	Engin	k1gInSc5	Engin
který	který	k3yIgInSc1	který
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
ZDoom	ZDoom	k1gInSc1	ZDoom
+	+	kIx~	+
Podpora	podpora	k1gFnSc1	podpora
Multiplayeru	Multiplayer	k1gInSc2	Multiplayer
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
64	[number]	k4	64
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
<g/>
:	:	kIx,	:
Kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
Nastavení	nastavení	k1gNnSc1	nastavení
<g/>
,	,	kIx,	,
MouseLook	MouseLook	k1gInSc1	MouseLook
,	,	kIx,	,
Podpora	podpora	k1gFnSc1	podpora
Modifikací	modifikace	k1gFnPc2	modifikace
co	co	k9	co
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
ZDoom	ZDoom	k1gInSc1	ZDoom
a	a	k8xC	a
Skulltag	Skulltag	k1gInSc1	Skulltag
</s>
</p>
<p>
<s>
===	===	k?	===
ZDaemon	ZDaemon	k1gInSc4	ZDaemon
===	===	k?	===
</s>
</p>
<p>
<s>
Engine	Enginout	k5eAaPmIp3nS	Enginout
ZDoom	ZDoom	k1gInSc1	ZDoom
využívá	využívat	k5eAaImIp3nS	využívat
velice	velice	k6eAd1	velice
populární	populární	k2eAgInSc1d1	populární
ZDaemon	ZDaemon	k1gInSc1	ZDaemon
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
multiplayerový	multiplayerový	k2eAgInSc4d1	multiplayerový
engine	enginout	k5eAaPmIp3nS	enginout
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnSc4d1	využívající
síť	síť	k1gFnSc4	síť
serverů	server	k1gInPc2	server
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
k	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
hry	hra	k1gFnSc2	hra
Doom	Dooma	k1gFnPc2	Dooma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
konta	konto	k1gNnSc2	konto
a	a	k8xC	a
nastavení	nastavení	k1gNnSc2	nastavení
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
datovým	datový	k2eAgInPc3d1	datový
souborům	soubor	k1gInPc3	soubor
WAD	WAD	k?	WAD
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hrát	hrát	k5eAaImF	hrát
klasické	klasický	k2eAgFnSc2d1	klasická
mapy	mapa	k1gFnSc2	mapa
Doom	Doom	k1gInSc4	Doom
<g/>
1	[number]	k4	1
a	a	k8xC	a
Doom	Doom	k1gInSc1	Doom
<g/>
2	[number]	k4	2
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
módy	mód	k1gInPc1	mód
typu	typ	k1gInSc2	typ
CTF	CTF	kA	CTF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Grafika	grafika	k1gFnSc1	grafika
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
Doomsday	Doomsdaa	k1gFnSc2	Doomsdaa
Enginu	Engin	k1gInSc2	Engin
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
Doomsday	Doomsda	k1gMnPc7	Doomsda
Enginem	Engin	k1gMnSc7	Engin
může	moct	k5eAaImIp3nS	moct
hra	hra	k1gFnSc1	hra
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
nainstalovaných	nainstalovaný	k2eAgNnPc6d1	nainstalované
vylepšeních	vylepšení	k1gNnPc6	vylepšení
vypadat	vypadat	k5eAaPmF	vypadat
velice	velice	k6eAd1	velice
moderně	moderně	k6eAd1	moderně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
modifikace	modifikace	k1gFnSc1	modifikace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jDoom	jDoom	k1gInSc1	jDoom
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nainstalovanými	nainstalovaný	k2eAgFnPc7d1	nainstalovaná
3D	[number]	k4	3D
modely	model	k1gInPc4	model
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
žádné	žádný	k3yNgInPc4	žádný
sprity	sprit	k1gInPc4	sprit
<g/>
;	;	kIx,	;
engine	enginout	k5eAaPmIp3nS	enginout
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
bázi	báze	k1gFnSc6	báze
jako	jako	k8xS	jako
populární	populární	k2eAgFnSc3d1	populární
Quake	Quake	k1gFnSc3	Quake
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
ozdoby	ozdoba	k1gFnPc1	ozdoba
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
monstra	monstrum	k1gNnPc1	monstrum
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
plně	plně	k6eAd1	plně
3	[number]	k4	3
<g/>
D.	D.	kA	D.
Dalším	další	k2eAgNnSc7d1	další
vylepšením	vylepšení	k1gNnSc7	vylepšení
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
dynamická	dynamický	k2eAgNnPc1d1	dynamické
světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
podobné	podobný	k2eAgInPc1d1	podobný
efekty	efekt	k1gInPc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
hry	hra	k1gFnSc2	hra
s	s	k7c7	s
Doomsday	Doomsda	k1gMnPc7	Doomsda
Enginem	Engin	k1gMnSc7	Engin
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
1680	[number]	k4	1680
×	×	k?	×
1050	[number]	k4	1050
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvuk	zvuk	k1gInSc1	zvuk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
systému	systém	k1gInSc2	systém
EAX	EAX	kA	EAX
5.1	[number]	k4	5.1
<g/>
.	.	kIx.	.
</s>
<s>
Přidána	přidán	k2eAgFnSc1d1	přidána
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
možnost	možnost	k1gFnSc1	možnost
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
skákat	skákat	k5eAaImF	skákat
či	či	k8xC	či
plně	plně	k6eAd1	plně
využít	využít	k5eAaPmF	využít
mouselook	mouselook	k1gInSc4	mouselook
<g/>
.	.	kIx.	.
</s>
<s>
Doomsday	Doomsdaa	k1gFnPc1	Doomsdaa
Engine	Engin	k1gInSc5	Engin
umí	umět	k5eAaImIp3nS	umět
podporu	podpora	k1gFnSc4	podpora
Oculus	Oculus	k1gMnSc1	Oculus
Rift	Rift	k1gMnSc1	Rift
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Doomu	Doom	k1gInSc6	Doom
vydali	vydat	k5eAaPmAgMnP	vydat
ID	Ida	k1gFnPc2	Ida
Software	software	k1gInSc1	software
ještě	ještě	k6eAd1	ještě
podobné	podobný	k2eAgFnSc2d1	podobná
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
sérii	série	k1gFnSc4	série
Quake	Quak	k1gFnSc2	Quak
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
her	hra	k1gFnPc2	hra
ale	ale	k8xC	ale
svojí	svůj	k3xOyFgFnSc7	svůj
originalitou	originalita	k1gFnSc7	originalita
a	a	k8xC	a
jednoduchostí	jednoduchost	k1gFnPc2	jednoduchost
nepředčila	předčít	k5eNaPmAgFnS	předčít
klasický	klasický	k2eAgInSc4d1	klasický
Doom	Doom	k1gInSc4	Doom
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
jednoduchosti	jednoduchost	k1gFnSc2	jednoduchost
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
Quake	Quake	k1gInSc1	Quake
3	[number]	k4	3
<g/>
:	:	kIx,	:
Arena	Aren	k1gInSc2	Aren
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
datadisky	datadisky	k6eAd1	datadisky
s	s	k7c7	s
úrovněmi	úroveň	k1gFnPc7	úroveň
jako	jako	k9	jako
byl	být	k5eAaImAgInS	být
Ultimate	Ultimat	k1gInSc5	Ultimat
Doom	Doom	k1gMnSc1	Doom
a	a	k8xC	a
Final	Final	k1gMnSc1	Final
Doom	Doom	k1gMnSc1	Doom
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
celé	celý	k2eAgFnSc2d1	celá
nové	nový	k2eAgFnSc2d1	nová
epizody	epizoda	k1gFnSc2	epizoda
vytvořené	vytvořený	k2eAgMnPc4d1	vytvořený
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Programy	program	k1gInPc1	program
<g/>
(	(	kIx(	(
<g/>
Editory	editor	k1gInPc1	editor
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
hrozně	hrozně	k6eAd1	hrozně
moc	moc	k6eAd1	moc
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
i	i	k9	i
hromady	hromada	k1gFnPc4	hromada
editačních	editační	k2eAgInPc2d1	editační
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
vám	vy	k3xPp2nPc3	vy
vypíšu	vypsat	k5eAaPmIp1nS	vypsat
pár	pár	k4xCyI	pár
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
budou	být	k5eAaImBp3nP	být
hodit	hodit	k5eAaPmF	hodit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
SLADE	slad	k1gInSc5	slad
3	[number]	k4	3
===	===	k?	===
</s>
</p>
<p>
<s>
SLADE	slad	k1gInSc5	slad
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgInSc2	který
můžete	moct	k5eAaImIp2nP	moct
editovat	editovat	k5eAaImF	editovat
skoro	skoro	k6eAd1	skoro
cokoli	cokoli	k3yInSc4	cokoli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výhody	výhoda	k1gFnPc1	výhoda
====	====	k?	====
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převést	převést	k5eAaPmF	převést
obrázky	obrázek	k1gInPc4	obrázek
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převést	převést	k5eAaPmF	převést
.	.	kIx.	.
<g/>
mid	mid	k?	mid
do	do	k7c2	do
.	.	kIx.	.
<g/>
mus	musa	k1gFnPc2	musa
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
editovat	editovat	k5eAaImF	editovat
paletu	paleta	k1gFnSc4	paleta
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pracovat	pracovat	k5eAaImF	pracovat
se	s	k7c7	s
skripty	skript	k1gInPc7	skript
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
editovat	editovat	k5eAaImF	editovat
mapy	mapa	k1gFnPc4	mapa
</s>
</p>
<p>
<s>
====	====	k?	====
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
====	====	k?	====
</s>
</p>
<p>
<s>
-	-	kIx~	-
Co	co	k3yQnSc4	co
zde	zde	k6eAd1	zde
dát	dát	k5eAaPmF	dát
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
===	===	k?	===
GZDoomBuilder	GZDoomBuildero	k1gNnPc2	GZDoomBuildero
===	===	k?	===
</s>
</p>
<p>
<s>
GZDoomBuilder	GZDoomBuilder	k1gInSc1	GZDoomBuilder
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
editovat	editovat	k5eAaImF	editovat
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
skripty	skript	k1gInPc4	skript
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
editor	editor	k1gInSc1	editor
není	být	k5eNaImIp3nS	být
jako	jako	k9	jako
Doom	Doom	k1gMnSc1	Doom
Builder	Builder	k1gMnSc1	Builder
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odbornější	odborný	k2eAgFnSc1d2	odbornější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výhody	výhoda	k1gFnPc1	výhoda
====	====	k?	====
</s>
</p>
<p>
<s>
-	-	kIx~	-
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
odborný	odborný	k2eAgInSc1d1	odborný
Render	Render	k1gInSc1	Render
<g/>
(	(	kIx(	(
<g/>
dynamická	dynamický	k2eAgNnPc1d1	dynamické
světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
slopes	slopes	k1gInSc1	slopes
<g/>
,	,	kIx,	,
předdefinované	předdefinovaný	k2eAgNnSc1d1	předdefinované
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pracovat	pracovat	k5eAaImF	pracovat
odborně	odborně	k6eAd1	odborně
se	s	k7c7	s
světly	světlo	k1gNnPc7	světlo
<g/>
,	,	kIx,	,
nebem	nebe	k1gNnSc7	nebe
<g/>
,	,	kIx,	,
Skyboxem	Skybox	k1gInSc7	Skybox
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
====	====	k?	====
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
====	====	k?	====
</s>
</p>
<p>
<s>
-	-	kIx~	-
Nejprvé	Nejprvý	k2eAgNnSc1d1	Nejprvý
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
lehké	lehký	k2eAgNnSc1d1	lehké
</s>
</p>
<p>
<s>
===	===	k?	===
DeHackEd	DeHackEdo	k1gNnPc2	DeHackEdo
===	===	k?	===
</s>
</p>
<p>
<s>
DeHackEd	DeHackEd	k6eAd1	DeHackEd
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tvořit	tvořit	k5eAaImF	tvořit
DeHackEd	DeHackEd	k1gInSc4	DeHackEd
<g/>
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
deh	deh	k?	deh
<g/>
)	)	kIx)	)
skripty	skript	k1gInPc4	skript
<g/>
(	(	kIx(	(
<g/>
patche	patche	k1gFnSc1	patche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vložíte	vložit	k5eAaPmIp2nP	vložit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
PWADu	PWADus	k1gInSc2	PWADus
<g/>
,	,	kIx,	,
a	a	k8xC	a
máte	mít	k5eAaImIp2nP	mít
hacknutou	hacknutý	k2eAgFnSc4d1	hacknutá
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
rovnou	rovnou	k6eAd1	rovnou
v	v	k7c6	v
DOSu	DOSus	k1gInSc6	DOSus
hacknete	hacknout	k5eAaPmIp2nP	hacknout
doom	doom	k6eAd1	doom
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
<g/>
,	,	kIx,	,
doom	doom	k1gInSc1	doom
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
====	====	k?	====
Výhody	výhoda	k1gFnPc1	výhoda
====	====	k?	====
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
změnit	změnit	k5eAaPmF	změnit
stringy	string	k1gInPc4	string
</s>
</p>
<p>
<s>
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hacknout	hacknout	k5eAaImF	hacknout
hru	hra	k1gFnSc4	hra
</s>
</p>
<p>
<s>
-	-	kIx~	-
Lze	lze	k6eAd1	lze
změnit	změnit	k5eAaPmF	změnit
Cheat-cody	Cheatoda	k1gFnPc4	Cheat-coda
</s>
</p>
<p>
<s>
====	====	k?	====
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
====	====	k?	====
</s>
</p>
<p>
<s>
-	-	kIx~	-
Není	být	k5eNaImIp3nS	být
lehké	lehký	k2eAgNnSc1d1	lehké
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc4	tento
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
programy	program	k1gInPc4	program
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cheaty	Cheat	k2eAgMnPc4d1	Cheat
==	==	k?	==
</s>
</p>
<p>
<s>
Psáno	psán	k2eAgNnSc1d1	psáno
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
na	na	k7c6	na
klávesnici	klávesnice	k1gFnSc6	klávesnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
IDDQD	IDDQD	kA	IDDQD
–	–	k?	–
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDKFA	IDKFA	kA	IDKFA
–	–	k?	–
Všechny	všechen	k3xTgFnPc1	všechen
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
munice	munice	k1gFnSc1	munice
<g/>
,	,	kIx,	,
klíče	klíč	k1gInSc2	klíč
a	a	k8xC	a
200	[number]	k4	200
<g/>
%	%	kIx~	%
brnění	brnění	k1gNnSc3	brnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDFA	IDFA	kA	IDFA
–	–	k?	–
Všechny	všechen	k3xTgFnPc1	všechen
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
munice	munice	k1gFnSc1	munice
a	a	k8xC	a
200	[number]	k4	200
<g/>
%	%	kIx~	%
brnění	brnění	k1gNnSc3	brnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDCLEVxy	IDCLEVx	k1gInPc1	IDCLEVx
–	–	k?	–
Okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
teleportace	teleportace	k1gFnSc1	teleportace
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
levelu	level	k1gInSc2	level
xy	xy	k?	xy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
x	x	k?	x
značí	značit	k5eAaImIp3nS	značit
číslo	číslo	k1gNnSc4	číslo
epizody	epizoda	k1gFnSc2	epizoda
a	a	k8xC	a
y	y	k?	y
číslo	číslo	k1gNnSc1	číslo
levelu	level	k1gInSc2	level
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDCLIP	IDCLIP	kA	IDCLIP
<g/>
,	,	kIx,	,
IDSPISPOPD	IDSPISPOPD	kA	IDSPISPOPD
–	–	k?	–
Procházení	procházení	k1gNnSc1	procházení
zdmi	zeď	k1gFnPc7	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDDT	IDDT	kA	IDDT
–	–	k?	–
První	první	k4xOgNnSc1	první
zadání	zadání	k1gNnPc2	zadání
odkryje	odkrýt	k5eAaPmIp3nS	odkrýt
celou	celý	k2eAgFnSc4d1	celá
mapu	mapa	k1gFnSc4	mapa
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
všechny	všechen	k3xTgMnPc4	všechen
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
/	/	kIx~	/
<g/>
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
třetí	třetí	k4xOgFnSc2	třetí
vrátí	vrátit	k5eAaPmIp3nP	vrátit
původní	původní	k2eAgFnSc4d1	původní
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDBEHOLDI	IDBEHOLDI	kA	IDBEHOLDI
–	–	k?	–
Neviditelnost	neviditelnost	k1gFnSc1	neviditelnost
(	(	kIx(	(
<g/>
dočasná	dočasný	k2eAgFnSc1d1	dočasná
–	–	k?	–
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDBEHOLDL	IDBEHOLDL	kA	IDBEHOLDL
–	–	k?	–
Noční	noční	k2eAgNnSc4d1	noční
vidění	vidění	k1gNnSc4	vidění
–	–	k?	–
osvětlení	osvětlení	k1gNnSc2	osvětlení
všech	všecek	k3xTgFnPc2	všecek
místností	místnost	k1gFnPc2	místnost
(	(	kIx(	(
<g/>
dočasné	dočasný	k2eAgFnSc2d1	dočasná
–	–	k?	–
120	[number]	k4	120
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDBEHOLDR	IDBEHOLDR	kA	IDBEHOLDR
–	–	k?	–
Radiační	radiační	k2eAgInSc4d1	radiační
oblek	oblek	k1gInSc4	oblek
–	–	k?	–
proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
typům	typ	k1gInPc3	typ
lávy	láva	k1gFnSc2	láva
(	(	kIx(	(
<g/>
dočasný	dočasný	k2eAgInSc1d1	dočasný
–	–	k?	–
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDBEHOLDV	IDBEHOLDV	kA	IDBEHOLDV
–	–	k?	–
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
(	(	kIx(	(
<g/>
dočasná	dočasný	k2eAgFnSc1d1	dočasná
–	–	k?	–
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDBEHOLDA	IDBEHOLDA	kA	IDBEHOLDA
–	–	k?	–
Mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	(
<g/>
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
level	level	k1gInSc4	level
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDBEHOLDS	IDBEHOLDS	kA	IDBEHOLDS
–	–	k?	–
Berserk	berserk	k1gMnSc1	berserk
–	–	k?	–
zesílená	zesílený	k2eAgFnSc1d1	zesílená
síla	síla	k1gFnSc1	síla
pěsti	pěst	k1gFnSc2	pěst
<g/>
,	,	kIx,	,
při	při	k7c6	při
rozběhu	rozběh	k1gInSc6	rozběh
ještě	ještě	k9	ještě
2	[number]	k4	2
<g/>
×	×	k?	×
silnější	silný	k2eAgFnSc1d2	silnější
(	(	kIx(	(
<g/>
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
level	level	k1gInSc4	level
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDCHOPPERS	IDCHOPPERS	kA	IDCHOPPERS
–	–	k?	–
Motorová	motorový	k2eAgFnSc1d1	motorová
pila	pila	k1gFnSc1	pila
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc1	zobrazení
hlášky	hláška	k1gFnSc2	hláška
"	"	kIx"	"
<g/>
...	...	k?	...
doesn	doesn	k1gInSc1	doesn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
suck	suck	k1gInSc1	suck
–	–	k?	–
GM	GM	kA	GM
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDMUSxx	IDMUSxx	k1gInSc1	IDMUSxx
–	–	k?	–
Změna	změna	k1gFnSc1	změna
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
xx	xx	k?	xx
značí	značit	k5eAaImIp3nS	značit
dvojčíslí	dvojčíslí	k1gNnSc4	dvojčíslí
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
01	[number]	k4	01
až	až	k9	až
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDMYPOS	IDMYPOS	kA	IDMYPOS
–	–	k?	–
Vypíše	vypsat	k5eAaPmIp3nS	vypsat
současnou	současný	k2eAgFnSc4d1	současná
pozici	pozice	k1gFnSc4	pozice
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Doom	Doom	k1gInSc1	Doom
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Doom	Dooma	k1gFnPc2	Dooma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
Idoom	Idoom	k1gInSc1	Idoom
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
nejstarší	starý	k2eAgFnSc1d3	nejstarší
aktualizovaná	aktualizovaný	k2eAgFnSc1d1	aktualizovaná
česká	český	k2eAgFnSc1d1	Česká
stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
Doom	Dooma	k1gFnPc2	Dooma
</s>
</p>
<p>
<s>
Doom	Doom	k1gInSc1	Doom
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
český	český	k2eAgInSc4d1	český
web	web	k1gInSc4	web
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
hře	hra	k1gFnSc3	hra
Doom	Doom	k1gInSc1	Doom
<g/>
,	,	kIx,	,
návody	návod	k1gInPc1	návod
<g/>
,	,	kIx,	,
novinky	novinka	k1gFnPc1	novinka
</s>
</p>
<p>
<s>
Leebigh	Leebigh	k1gInSc1	Leebigh
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Doom	Doo	k1gNnSc7	Doo
Blog	Blog	k1gInSc1	Blog
–	–	k?	–
blog	blog	k1gInSc1	blog
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
WADům	WADům	k?	WADům
k	k	k7c3	k
Doomovi	Doomův	k2eAgMnPc1d1	Doomův
</s>
</p>
<p>
<s>
Wiki	Wik	k1gFnPc1	Wik
české	český	k2eAgFnPc1d1	Česká
Doom	Doom	k1gInSc4	Doom
scény	scéna	k1gFnSc2	scéna
</s>
</p>
<p>
<s>
Liquid	Liquid	k1gInSc1	Liquid
DooM	DooM	k1gFnSc2	DooM
–	–	k?	–
hraní	hranit	k5eAaImIp3nS	hranit
Dooma	Dooma	k1gFnSc1	Dooma
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
Doom	Doom	k1gInSc4	Doom
rádio	rádio	k1gNnSc4	rádio
<g/>
,	,	kIx,	,
skeny	skeen	k2eAgFnPc4d1	skeen
recenzí	recenze	k1gFnSc7	recenze
na	na	k7c4	na
Dooma	Dooma	k1gNnSc4	Dooma
z	z	k7c2	z
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Fanouškovská	fanouškovský	k2eAgFnSc1d1	fanouškovská
stránka	stránka	k1gFnSc1	stránka
hry	hra	k1gFnSc2	hra
DOOM	DOOM	kA	DOOM
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
Doomovská	Doomovská	k1gFnSc1	Doomovská
wiki	wik	k1gFnSc2	wik
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
ID	Ida	k1gFnPc2	Ida
software	software	k1gInSc4	software
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
ZDoom	ZDoom	k1gInSc1	ZDoom
herního	herní	k2eAgInSc2d1	herní
portu	port	k1gInSc2	port
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
GZDoom	GZDoom	k1gInSc1	GZDoom
herního	herní	k2eAgInSc2d1	herní
portu	port	k1gInSc2	port
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
Zandronum	Zandronum	k1gInSc1	Zandronum
herního	herní	k2eAgInSc2d1	herní
portu	port	k1gInSc2	port
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
ZDaemon	ZDaemona	k1gFnPc2	ZDaemona
multiplayerového	multiplayerový	k2eAgInSc2d1	multiplayerový
herního	herní	k2eAgInSc2d1	herní
portu	port	k1gInSc2	port
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
ODAMEX	ODAMEX	kA	ODAMEX
multiplayerového	multiplayerový	k2eAgInSc2d1	multiplayerový
herního	herní	k2eAgInSc2d1	herní
portu	port	k1gInSc2	port
</s>
</p>
