<s>
Edward	Edward	k1gMnSc1	Edward
Mills	Millsa	k1gFnPc2	Millsa
Purcell	Purcell	k1gMnSc1	Purcell
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1912	[number]	k4	1912
Taylorville	Taylorville	k1gNnSc2	Taylorville
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1997	[number]	k4	1997
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
nezávislý	závislý	k2eNgInSc1d1	nezávislý
výzkum	výzkum	k1gInSc1	výzkum
nukleární	nukleární	k2eAgFnSc2d1	nukleární
magnetické	magnetický	k2eAgFnSc2d1	magnetická
rezonance	rezonance	k1gFnSc2	rezonance
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
a	a	k8xC	a
pevných	pevný	k2eAgFnPc6d1	pevná
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
</s>
