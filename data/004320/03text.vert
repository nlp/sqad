<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
CHKO	CHKO	kA	CHKO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
systému	systém	k1gInSc6	systém
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
Československa	Československo	k1gNnPc1	Československo
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
prvních	první	k4xOgInPc2	první
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
velkoplošné	velkoplošný	k2eAgNnSc4d1	velkoplošné
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yQgInSc4	jaký
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
CHKO	CHKO	kA	CHKO
jako	jako	k8xC	jako
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
území	území	k1gNnSc1	území
s	s	k7c7	s
harmonicky	harmonicky	k6eAd1	harmonicky
utvářenou	utvářený	k2eAgFnSc7d1	utvářená
krajinou	krajina	k1gFnSc7	krajina
<g/>
,	,	kIx,	,
charakteristicky	charakteristicky	k6eAd1	charakteristicky
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
reliéfem	reliéf	k1gInSc7	reliéf
<g/>
,	,	kIx,	,
významným	významný	k2eAgInSc7d1	významný
podílem	podíl	k1gInSc7	podíl
přirozených	přirozený	k2eAgInPc2d1	přirozený
ekosystémů	ekosystém	k1gInPc2	ekosystém
lesních	lesní	k2eAgInPc2d1	lesní
a	a	k8xC	a
trvalých	trvalý	k2eAgInPc2d1	trvalý
travních	travní	k2eAgInPc2d1	travní
porostů	porost	k1gInPc2	porost
s	s	k7c7	s
hojným	hojný	k2eAgNnSc7d1	hojné
zastoupením	zastoupení	k1gNnSc7	zastoupení
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
dochovanými	dochovaný	k2eAgFnPc7d1	dochovaná
památkami	památka	k1gFnPc7	památka
historického	historický	k2eAgNnSc2d1	historické
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
využívání	využívání	k1gNnSc2	využívání
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
podle	podle	k7c2	podle
zón	zóna	k1gFnPc2	zóna
odstupňované	odstupňovaný	k2eAgFnSc2d1	odstupňovaná
ochrany	ochrana	k1gFnSc2	ochrana
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
udržoval	udržovat	k5eAaImAgMnS	udržovat
a	a	k8xC	a
zlepšoval	zlepšovat	k5eAaImAgMnS	zlepšovat
jejich	jejich	k3xOp3gInSc4	jejich
přírodní	přírodní	k2eAgInSc4d1	přírodní
stav	stav	k1gInSc4	stav
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
zachovány	zachován	k2eAgInPc1d1	zachován
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
znovu	znovu	k6eAd1	znovu
vytvářeny	vytvářen	k2eAgFnPc4d1	vytvářena
optimální	optimální	k2eAgFnPc4d1	optimální
ekologické	ekologický	k2eAgFnPc4d1	ekologická
funkce	funkce	k1gFnPc4	funkce
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Rekreační	rekreační	k2eAgNnSc1d1	rekreační
využití	využití	k1gNnSc1	využití
CHKO	CHKO	kA	CHKO
je	být	k5eAaImIp3nS	být
přípustné	přípustný	k2eAgNnSc1d1	přípustné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepoškozuje	poškozovat	k5eNaImIp3nS	poškozovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
hodnoty	hodnota	k1gFnPc4	hodnota
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
CHKO	CHKO	kA	CHKO
se	se	k3xPyFc4	se
vyhlašují	vyhlašovat	k5eAaImIp3nP	vyhlašovat
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
26	[number]	k4	26
CHKO	CHKO	kA	CHKO
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
1	[number]	k4	1
076	[number]	k4	076
111	[number]	k4	111
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
a	a	k8xC	a
také	také	k6eAd1	také
nejnavštěvovanější	navštěvovaný	k2eAgFnSc4d3	nejnavštěvovanější
CHKO	CHKO	kA	CHKO
v	v	k7c6	v
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
CHKO	CHKO	kA	CHKO
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
nebo	nebo	k8xC	nebo
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
krajích	kraj	k1gInPc6	kraj
Československa	Československo	k1gNnSc2	Československo
2	[number]	k4	2
CHKO	CHKO	kA	CHKO
<g/>
,	,	kIx,	,
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
7	[number]	k4	7
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
evidováno	evidovat	k5eAaImNgNnS	evidovat
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc4d3	nejstarší
CHKO	CHKO	kA	CHKO
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byla	být	k5eAaImAgFnS	být
CHKO	CHKO	kA	CHKO
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
zřízená	zřízený	k2eAgFnSc1d1	zřízená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
CHKO	CHKO	kA	CHKO
pravomoci	pravomoc	k1gFnSc2	pravomoc
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vede	vést	k5eAaImIp3nS	vést
správní	správní	k2eAgNnSc4d1	správní
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dotčeným	dotčený	k2eAgInSc7d1	dotčený
orgánem	orgán	k1gInSc7	orgán
ve	v	k7c6	v
stavebním	stavební	k2eAgNnSc6d1	stavební
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Správy	správa	k1gFnSc2	správa
CHKO	CHKO	kA	CHKO
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
jedna	jeden	k4xCgFnSc1	jeden
zastřešující	zastřešující	k2eAgFnSc1d1	zastřešující
organizace	organizace	k1gFnSc1	organizace
–	–	k?	–
Agentura	agentura	k1gFnSc1	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
vyhlašována	vyhlašovat	k5eAaImNgFnS	vyhlašovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byl	být	k5eAaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
jen	jen	k9	jen
pro	pro	k7c4	pro
české	český	k2eAgInPc4d1	český
kraje	kraj	k1gInPc4	kraj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1955	[number]	k4	1955
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
SNR	SNR	kA	SNR
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
definovaly	definovat	k5eAaBmAgFnP	definovat
formy	forma	k1gFnPc1	forma
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chráněné	chráněný	k2eAgFnPc4d1	chráněná
krajinné	krajinný	k2eAgFnPc4d1	krajinná
oblasti	oblast	k1gFnPc4	oblast
byla	být	k5eAaImAgFnS	být
použita	použit	k2eAgFnSc1d1	použita
zkratka	zkratka	k1gFnSc1	zkratka
CHKO	CHKO	kA	CHKO
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
řazeny	řadit	k5eAaImNgFnP	řadit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
národními	národní	k2eAgInPc7d1	národní
parky	park	k1gInPc7	park
pod	pod	k7c4	pod
velkoplošná	velkoplošný	k2eAgNnPc4d1	velkoplošné
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
byly	být	k5eAaImAgInP	být
doplňovány	doplňovat	k5eAaImNgInP	doplňovat
řadou	řada	k1gFnSc7	řada
vyhlášek	vyhláška	k1gFnPc2	vyhláška
a	a	k8xC	a
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
upravovaly	upravovat	k5eAaImAgFnP	upravovat
mj.	mj.	kA	mj.
i	i	k8xC	i
plošný	plošný	k2eAgInSc4d1	plošný
rozsah	rozsah	k1gInSc4	rozsah
CHKO	CHKO	kA	CHKO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
změněné	změněný	k2eAgInPc4d1	změněný
názory	názor	k1gInPc4	názor
společnosti	společnost	k1gFnSc2	společnost
přijat	přijmout	k5eAaPmNgInS	přijmout
Českou	český	k2eAgFnSc7d1	Česká
národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
zákon	zákon	k1gInSc4	zákon
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
citovaný	citovaný	k2eAgInSc1d1	citovaný
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
názvosloví	názvosloví	k1gNnSc4	názvosloví
u	u	k7c2	u
CHKO	CHKO	kA	CHKO
nezměnil	změnit	k5eNaPmAgMnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
níže	nízce	k6eAd2	nízce
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
všech	všecek	k3xTgFnPc2	všecek
26	[number]	k4	26
CHKO	CHKO	kA	CHKO
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
zřízení	zřízení	k1gNnSc2	zřízení
a	a	k8xC	a
rozlohou	rozloha	k1gFnSc7	rozloha
<g/>
.	.	kIx.	.
</s>
