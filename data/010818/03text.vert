<p>
<s>
Lapis	lapis	k1gInSc1	lapis
lazuli	lazule	k1gFnSc3	lazule
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
,	,	kIx,	,
z	z	k7c2	z
perského	perský	k2eAgNnSc2d1	perské
ل	ل	k?	ل
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
modrý	modrý	k2eAgInSc1d1	modrý
kámen	kámen	k1gInSc1	kámen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hornina	hornina	k1gFnSc1	hornina
<g/>
,	,	kIx,	,
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ceněný	ceněný	k2eAgInSc1d1	ceněný
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
výraznou	výrazný	k2eAgFnSc4d1	výrazná
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Lapis	lapis	k1gInSc4	lapis
lazuli	lazout	k5eAaPmAgMnP	lazout
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
složenina	složenina	k1gFnSc1	složenina
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
lazuritu	lazurit	k1gInSc2	lazurit
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
kalcitu	kalcit	k1gInSc2	kalcit
<g/>
,	,	kIx,	,
sodalitu	sodalit	k1gInSc2	sodalit
<g/>
,	,	kIx,	,
pyritu	pyrit	k1gInSc2	pyrit
<g/>
,	,	kIx,	,
augitu	augit	k1gInSc2	augit
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc3	Rusko
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
a	a	k8xC	a
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hojná	hojný	k2eAgNnPc4d1	hojné
naleziště	naleziště	k1gNnPc4	naleziště
pyritu	pyrit	k1gInSc2	pyrit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lapis	lapis	k1gInSc4	lapis
lazuli	lazout	k5eAaPmAgMnP	lazout
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
lazurit	lazurit	k1gInSc4	lazurit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
nádherný	nádherný	k2eAgInSc1d1	nádherný
modrý	modrý	k2eAgInSc1d1	modrý
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Nejkvalitnější	kvalitní	k2eAgInSc1d3	nejkvalitnější
lapis	lapis	k1gInSc1	lapis
lazuli	lazule	k1gFnSc4	lazule
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
modrý	modrý	k2eAgInSc1d1	modrý
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
zlatavými	zlatavý	k2eAgFnPc7d1	zlatavá
skvrnkami	skvrnka	k1gFnPc7	skvrnka
tvořenými	tvořený	k2eAgFnPc7d1	tvořená
pyritem	pyrit	k1gInSc7	pyrit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
fantazie	fantazie	k1gFnSc2	fantazie
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
lapis	lapis	k1gInSc4	lapis
představit	představit	k5eAaPmF	představit
jako	jako	k8xS	jako
tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
kámen	kámen	k1gInSc1	kámen
posypaný	posypaný	k2eAgInSc1d1	posypaný
zlatými	zlatý	k2eAgInPc7d1	zlatý
flitry	flitr	k1gInPc7	flitr
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
méně	málo	k6eAd2	málo
kvalitním	kvalitní	k2eAgInSc6d1	kvalitní
lapisu	lapis	k1gInSc6	lapis
jsou	být	k5eAaImIp3nP	být
zřetelně	zřetelně	k6eAd1	zřetelně
viditelné	viditelný	k2eAgFnPc1d1	viditelná
žilky	žilka	k1gFnPc1	žilka
bílého	bílý	k2eAgInSc2d1	bílý
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
lapis	lapis	k1gInSc1	lapis
lazuli	lazule	k1gFnSc4	lazule
tmavší	tmavý	k2eAgFnSc1d2	tmavší
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
viditelná	viditelný	k2eAgNnPc4d1	viditelné
zrnka	zrnko	k1gNnPc4	zrnko
pyritu	pyrit	k1gInSc2	pyrit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
zlatě	zlatě	k6eAd1	zlatě
září	zářit	k5eAaImIp3nP	zářit
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
kvalitnější	kvalitní	k2eAgMnSc1d2	kvalitnější
<g/>
,	,	kIx,	,
vzácnější	vzácný	k2eAgMnSc1d2	vzácnější
a	a	k8xC	a
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
je	být	k5eAaImIp3nS	být
lapis	lapis	k1gInSc1	lapis
lazuli	lazule	k1gFnSc4	lazule
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
národní	národní	k2eAgInSc4d1	národní
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
mytologického	mytologický	k2eAgNnSc2d1	mytologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
lapis	lapis	k1gInSc1	lapis
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
mocný	mocný	k2eAgInSc4d1	mocný
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
báje	báj	k1gFnPc1	báj
dokonce	dokonce	k9	dokonce
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
při	při	k7c6	při
tradičních	tradiční	k2eAgInPc6d1	tradiční
šamanských	šamanský	k2eAgInPc6d1	šamanský
a	a	k8xC	a
čarodějnických	čarodějnický	k2eAgInPc6d1	čarodějnický
rituálech	rituál	k1gInPc6	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Lapis	lapis	k1gInSc1	lapis
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
nasát	nasát	k5eAaPmF	nasát
měsíční	měsíční	k2eAgFnSc4d1	měsíční
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
svému	svůj	k3xOyFgMnSc3	svůj
nositeli	nositel	k1gMnSc3	nositel
pak	pak	k6eAd1	pak
dodávat	dodávat	k5eAaImF	dodávat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
samotné	samotný	k2eAgNnSc1d1	samotné
jméno	jméno	k1gNnSc1	jméno
Lapis	lapis	k1gInSc1	lapis
lazuli	lazule	k1gFnSc3	lazule
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
dalo	dát	k5eAaPmAgNnS	dát
inspiraci	inspirace	k1gFnSc4	inspirace
a	a	k8xC	a
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
mnoha	mnoho	k4c2	mnoho
dnes	dnes	k6eAd1	dnes
mezinárodně	mezinárodně	k6eAd1	mezinárodně
používaných	používaný	k2eAgNnPc2d1	používané
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známé	známá	k1gFnPc4	známá
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
azur	azur	k1gInSc1	azur
<g/>
,	,	kIx,	,
azurový	azurový	k2eAgInSc1d1	azurový
<g/>
,	,	kIx,	,
azory	azor	k1gInPc1	azor
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
barevnou	barevný	k2eAgFnSc4d1	barevná
modrost	modrost	k1gFnSc4	modrost
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
francouzským	francouzský	k2eAgInSc7d1	francouzský
zkomolením	zkomolení	k1gNnSc7	zkomolení
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
lazuli	lazule	k1gFnSc4	lazule
<g/>
"	"	kIx"	"
majícího	mající	k2eAgMnSc2d1	mající
původ	původ	k1gMnSc1	původ
právě	právě	k9	právě
u	u	k7c2	u
Lapisu	lapis	k1gInSc2	lapis
lazuli	lazule	k1gFnSc4	lazule
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
francouzština	francouzština	k1gFnSc1	francouzština
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
první	první	k4xOgNnSc1	první
písmenko	písmenko	k1gNnSc1	písmenko
L	L	kA	L
jako	jako	k8xC	jako
člen	člen	k1gInSc4	člen
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
pokřivila	pokřivit	k5eAaPmAgFnS	pokřivit
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známého	známý	k2eAgNnSc2d1	známé
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Lapis	lapis	k1gInSc1	lapis
poté	poté	k6eAd1	poté
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
kámen	kámen	k1gInSc4	kámen
a	a	k8xC	a
právě	právě	k9	právě
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
má	mít	k5eAaImIp3nS	mít
základ	základ	k1gInSc4	základ
slovo	slovo	k1gNnSc4	slovo
Lapidárium	lapidárium	k1gNnSc1	lapidárium
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
sbírka	sbírka	k1gFnSc1	sbírka
či	či	k8xC	či
muzeum	muzeum	k1gNnSc1	muzeum
nerostů	nerost	k1gInPc2	nerost
<g/>
/	/	kIx~	/
<g/>
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
zajímavější	zajímavý	k2eAgMnSc1d2	zajímavější
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
lapidární	lapidární	k2eAgInPc4d1	lapidární
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
také	také	k9	také
nese	nést	k5eAaImIp3nS	nést
prapůvod	prapůvod	k1gInSc1	prapůvod
u	u	k7c2	u
modrého	modrý	k2eAgInSc2d1	modrý
Lapisu	lapis	k1gInSc2	lapis
lazuli	lazule	k1gFnSc4	lazule
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
zvykem	zvyk	k1gInSc7	zvyk
opatřovat	opatřovat	k5eAaImF	opatřovat
hroby	hrob	k1gInPc4	hrob
velice	velice	k6eAd1	velice
stručnou	stručný	k2eAgFnSc4d1	stručná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výstižnou	výstižný	k2eAgFnSc7d1	výstižná
informací	informace	k1gFnSc7	informace
o	o	k7c6	o
zesnulém	zesnulý	k1gMnSc6	zesnulý
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
se	se	k3xPyFc4	se
tesaly	tesat	k5eAaImAgFnP	tesat
do	do	k7c2	do
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
tedy	tedy	k9	tedy
znovu	znovu	k6eAd1	znovu
lapis	lapis	k1gInSc4	lapis
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
lapidárně	lapidárně	k6eAd1	lapidárně
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
se	se	k3xPyFc4	se
stručně	stručně	k6eAd1	stručně
až	až	k6eAd1	až
heslovitě	heslovitě	k6eAd1	heslovitě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jasným	jasný	k2eAgInSc7d1	jasný
a	a	k8xC	a
výstižným	výstižný	k2eAgInSc7d1	výstižný
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
se	se	k3xPyFc4	se
vyjadřovaly	vyjadřovat	k5eAaImAgInP	vyjadřovat
náhrobky	náhrobek	k1gInPc1	náhrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lapis	lapis	k1gInSc1	lapis
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
dozdobení	dozdobení	k1gNnSc3	dozdobení
zlaté	zlatý	k2eAgFnSc2d1	zlatá
posmrtné	posmrtný	k2eAgFnSc2d1	posmrtná
masky	maska	k1gFnSc2	maska
Tutanchamona	Tutanchamona	k1gFnSc1	Tutanchamona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
sérii	série	k1gFnSc6	série
Upíří	upíří	k2eAgInPc1d1	upíří
deníky	deník	k1gInPc1	deník
autorky	autorka	k1gFnSc2	autorka
L.	L.	kA	L.
J.	J.	kA	J.
Smithové	Smithové	k2eAgInSc4d1	Smithové
šperk	šperk	k1gInSc4	šperk
s	s	k7c7	s
kamenem	kámen	k1gInSc7	kámen
lapis	lapis	k1gInSc1	lapis
lazuli	lazule	k1gFnSc4	lazule
chrání	chránit	k5eAaImIp3nS	chránit
upíra	upír	k1gMnSc4	upír
před	před	k7c7	před
škodlivým	škodlivý	k2eAgInSc7d1	škodlivý
vlivem	vliv	k1gInSc7	vliv
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lapis	lapis	k1gInSc1	lapis
lazuli	lazout	k5eAaPmAgMnP	lazout
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
