<s>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
a	a	k8xC	a
případně	případně	k6eAd1	případně
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
odtržením	odtržení	k1gNnSc7	odtržení
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
atomů	atom	k1gInPc2	atom
plynu	plynout	k5eAaImIp1nS	plynout
<g/>
,	,	kIx,	,
či	či	k8xC	či
roztržením	roztržení	k1gNnSc7	roztržení
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
ionizací	ionizace	k1gFnPc2	ionizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
