<s>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
a	a	k8xC	a
případně	případně	k6eAd1	případně
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
odtržením	odtržení	k1gNnSc7	odtržení
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
atomů	atom	k1gInPc2	atom
plynu	plynout	k5eAaImIp1nS	plynout
<g/>
,	,	kIx,	,
či	či	k8xC	či
roztržením	roztržení	k1gNnSc7	roztržení
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
ionizací	ionizace	k1gFnPc2	ionizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
plazma	plazma	k1gNnSc4	plazma
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
kvazineutralitu	kvazineutralita	k1gFnSc4	kvazineutralita
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
skupenství	skupenství	k1gNnSc1	skupenství
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
také	také	k9	také
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
forma	forma	k1gFnSc1	forma
látky	látka	k1gFnSc2	látka
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
až	až	k9	až
99	[number]	k4	99
%	%	kIx~	%
pozorované	pozorovaný	k2eAgFnSc2d1	pozorovaná
atomární	atomární	k2eAgFnSc2d1	atomární
hmoty	hmota	k1gFnSc2	hmota
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
odlišných	odlišný	k2eAgFnPc6d1	odlišná
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
plazmatem	plazma	k1gNnSc7	plazma
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
například	například	k6eAd1	například
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
blesku	blesk	k1gInSc2	blesk
<g/>
,	,	kIx,	,
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
zářivek	zářivka	k1gFnPc2	zářivka
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
neonů	neon	k1gInPc2	neon
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
elektrickém	elektrický	k2eAgInSc6d1	elektrický
oblouku	oblouk	k1gInSc6	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
tvoří	tvořit	k5eAaImIp3nS	tvořit
také	také	k9	také
konvenční	konvenční	k2eAgFnPc4d1	konvenční
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
mlhoviny	mlhovina	k1gFnPc4	mlhovina
<g/>
,	,	kIx,	,
ionosféru	ionosféra	k1gFnSc4	ionosféra
<g/>
,	,	kIx,	,
či	či	k8xC	či
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Parametry	parametr	k1gInPc1	parametr
plazmatu	plazma	k1gNnSc2	plazma
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
formách	forma	k1gFnPc6	forma
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
ionizace	ionizace	k1gFnSc2	ionizace
plazmatu	plazma	k1gNnSc2	plazma
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
ionizovaných	ionizovaný	k2eAgFnPc2d1	ionizovaná
částic	částice	k1gFnPc2	částice
vůči	vůči	k7c3	vůči
celkovému	celkový	k2eAgInSc3d1	celkový
počtu	počet	k1gInSc2	počet
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
chování	chování	k1gNnSc4	chování
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
přiblížení	přiblížení	k1gNnSc6	přiblížení
odhadnout	odhadnout	k5eAaPmF	odhadnout
ze	z	k7c2	z
Sahovy	Sahův	k2eAgFnSc2d1	Sahův
rovnice	rovnice	k1gFnSc2	rovnice
pro	pro	k7c4	pro
jedenkrát	jedenkrát	k6eAd1	jedenkrát
ionizované	ionizovaný	k2eAgNnSc4d1	ionizované
plazma	plazma	k1gNnSc4	plazma
v	v	k7c6	v
termodynamické	termodynamický	k2eAgFnSc6d1	termodynamická
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
ionizace	ionizace	k1gFnSc2	ionizace
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
slabě	slabě	k6eAd1	slabě
ionizované	ionizovaný	k2eAgNnSc4d1	ionizované
plazma	plazma	k1gNnSc4	plazma
a	a	k8xC	a
silně	silně	k6eAd1	silně
ionizované	ionizovaný	k2eAgNnSc4d1	ionizované
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
slabě	slabě	k6eAd1	slabě
ionizovaném	ionizovaný	k2eAgNnSc6d1	ionizované
plazmatu	plazma	k1gNnSc6	plazma
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
zanedbatelně	zanedbatelně	k6eAd1	zanedbatelně
malá	malý	k2eAgNnPc1d1	malé
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
koncentrací	koncentrace	k1gFnSc7	koncentrace
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
silně	silně	k6eAd1	silně
ionizovaném	ionizovaný	k2eAgNnSc6d1	ionizované
plazmatu	plazma	k1gNnSc6	plazma
převládá	převládat	k5eAaImIp3nS	převládat
koncentrace	koncentrace	k1gFnSc1	koncentrace
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vysokoteplotní	vysokoteplotní	k2eAgNnSc1d1	vysokoteplotní
a	a	k8xC	a
nízkoteplotní	nízkoteplotní	k2eAgNnSc1d1	nízkoteplotní
plazma	plazma	k1gNnSc1	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoteplotní	vysokoteplotní	k2eAgFnSc1d1	vysokoteplotní
plazma	plazma	k1gFnSc1	plazma
má	mít	k5eAaImIp3nS	mít
střední	střední	k2eAgFnSc4d1	střední
energii	energie	k1gFnSc4	energie
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
100	[number]	k4	100
eV	eV	k?	eV
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
řádově	řádově	k6eAd1	řádově
106	[number]	k4	106
K.	K.	kA	K.
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
a	a	k8xC	a
při	při	k7c6	při
experimentech	experiment	k1gInPc6	experiment
s	s	k7c7	s
řízenou	řízený	k2eAgFnSc7d1	řízená
termonukleární	termonukleární	k2eAgFnSc7d1	termonukleární
syntézou	syntéza	k1gFnSc7	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Nízkoteplotní	nízkoteplotní	k2eAgFnSc1d1	nízkoteplotní
plazma	plazma	k1gFnSc1	plazma
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
např.	např.	kA	např.
v	v	k7c6	v
zářivkách	zářivka	k1gFnPc6	zářivka
a	a	k8xC	a
výbojkách	výbojka	k1gFnPc6	výbojka
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
elektrickém	elektrický	k2eAgInSc6d1	elektrický
oblouku	oblouk	k1gInSc6	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
teplota	teplota	k1gFnSc1	teplota
elektronů	elektron	k1gInPc2	elektron
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
teplota	teplota	k1gFnSc1	teplota
kladných	kladný	k2eAgMnPc2d1	kladný
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
volné	volný	k2eAgInPc4d1	volný
elektrické	elektrický	k2eAgInPc4d1	elektrický
náboje	náboj	k1gInPc4	náboj
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
vodivé	vodivý	k2eAgNnSc1d1	vodivé
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
elektrické	elektrický	k2eAgFnSc2d1	elektrická
vodivosti	vodivost	k1gFnSc2	vodivost
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
plazma	plazma	k1gNnSc4	plazma
i	i	k9	i
silné	silný	k2eAgNnSc1d1	silné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
silové	silový	k2eAgInPc1d1	silový
účinky	účinek	k1gInPc1	účinek
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
koncentrací	koncentrace	k1gFnSc7	koncentrace
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
i	i	k8xC	i
koeficienty	koeficient	k1gInPc4	koeficient
tepelné	tepelný	k2eAgFnSc2d1	tepelná
vodivosti	vodivost	k1gFnSc2	vodivost
a	a	k8xC	a
dynamické	dynamický	k2eAgFnSc2d1	dynamická
viskozity	viskozita	k1gFnSc2	viskozita
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
síly	síla	k1gFnPc1	síla
a	a	k8xC	a
dobrá	dobrý	k2eAgFnSc1d1	dobrá
elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
plazmatu	plazma	k1gNnSc2	plazma
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
blížící	blížící	k2eAgMnPc4d1	blížící
se	se	k3xPyFc4	se
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
)	)	kIx)	)
obvykle	obvykle	k6eAd1	obvykle
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hustota	hustota	k1gFnSc1	hustota
kladných	kladný	k2eAgInPc2d1	kladný
a	a	k8xC	a
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
se	se	k3xPyFc4	se
vyrovná	vyrovnat	k5eAaPmIp3nS	vyrovnat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kvazineutralita	kvazineutralita	k1gFnSc1	kvazineutralita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
významný	významný	k2eAgInSc4d1	významný
nadbytek	nadbytek	k1gInSc4	nadbytek
kladných	kladný	k2eAgInPc2d1	kladný
nebo	nebo	k8xC	nebo
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
složeno	složit	k5eAaPmNgNnS	složit
jen	jen	k9	jen
z	z	k7c2	z
kladných	kladný	k2eAgFnPc2d1	kladná
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
plazmatu	plazma	k1gNnSc6	plazma
hraje	hrát	k5eAaImIp3nS	hrát
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
dominantní	dominantní	k2eAgFnSc4d1	dominantní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
nabité	nabitý	k2eAgInPc1d1	nabitý
paprsky	paprsek	k1gInPc1	paprsek
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
elektronový	elektronový	k2eAgInSc1d1	elektronový
oblak	oblak	k1gInSc1	oblak
v	v	k7c6	v
Penningově	Penningově	k1gFnSc6	Penningově
pasti	past	k1gFnSc6	past
a	a	k8xC	a
pozitronové	pozitronový	k2eAgNnSc4d1	pozitronové
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hustoty	hustota	k1gFnSc2	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
plazma	plazma	k1gFnSc1	plazma
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
formách	forma	k1gFnPc6	forma
<g/>
:	:	kIx,	:
temný	temný	k2eAgInSc1d1	temný
výboj	výboj	k1gInSc1	výboj
<g/>
,	,	kIx,	,
doutnavý	doutnavý	k2eAgInSc1d1	doutnavý
výboj	výboj	k1gInSc1	výboj
a	a	k8xC	a
obloukový	obloukový	k2eAgInSc1d1	obloukový
výboj	výboj	k1gInSc1	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
plazmatem	plazma	k1gNnSc7	plazma
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
stlačení	stlačení	k1gNnSc3	stlačení
v	v	k7c6	v
kolmém	kolmý	k2eAgInSc6d1	kolmý
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pinch	pinch	k1gInSc4	pinch
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1	elektrický
proudy	proud	k1gInPc1	proud
obvykle	obvykle	k6eAd1	obvykle
následují	následovat	k5eAaImIp3nP	následovat
magnetické	magnetický	k2eAgFnPc1d1	magnetická
indukční	indukční	k2eAgFnPc1d1	indukční
čáry	čára	k1gFnPc1	čára
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
typický	typický	k2eAgInSc1d1	typický
zkroucený	zkroucený	k2eAgInSc1d1	zkroucený
tvar	tvar	k1gInSc1	tvar
způsobený	způsobený	k2eAgInSc1d1	způsobený
superpozicí	superpozice	k1gFnSc7	superpozice
vnějšího	vnější	k2eAgNnSc2d1	vnější
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
daného	daný	k2eAgInSc2d1	daný
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
vlastností	vlastnost	k1gFnPc2	vlastnost
plazmatu	plazma	k1gNnSc2	plazma
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
kvazineutralita	kvazineutralita	k1gFnSc1	kvazineutralita
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přibližnou	přibližný	k2eAgFnSc4d1	přibližná
rovnost	rovnost	k1gFnSc4	rovnost
koncentrací	koncentrace	k1gFnPc2	koncentrace
kladně	kladně	k6eAd1	kladně
nabitých	nabitý	k2eAgInPc2d1	nabitý
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
záporně	záporně	k6eAd1	záporně
nabitých	nabitý	k2eAgInPc2d1	nabitý
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
lineární	lineární	k2eAgInPc1d1	lineární
rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přítomnosti	přítomnost	k1gFnSc3	přítomnost
volných	volný	k2eAgFnPc2d1	volná
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
plazmatu	plazma	k1gNnSc2	plazma
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
prostorový	prostorový	k2eAgInSc4d1	prostorový
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
elektrostatické	elektrostatický	k2eAgNnSc4d1	elektrostatické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zpětně	zpětně	k6eAd1	zpětně
silově	silově	k6eAd1	silově
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
nabité	nabitý	k2eAgFnPc4d1	nabitá
částice	částice	k1gFnPc4	částice
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
kompenzace	kompenzace	k1gFnSc1	kompenzace
fluktuací	fluktuace	k1gFnPc2	fluktuace
hustoty	hustota	k1gFnSc2	hustota
náboje	náboj	k1gInPc1	náboj
a	a	k8xC	a
plazma	plazma	k1gFnSc1	plazma
se	se	k3xPyFc4	se
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kvazineutralitou	kvazineutralita	k1gFnSc7	kvazineutralita
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
Debyeovo	Debyeův	k2eAgNnSc4d1	Debyeův
stínění	stínění	k1gNnSc4	stínění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
například	například	k6eAd1	například
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
nese	nést	k5eAaImIp3nS	nést
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
kvazineutralitě	kvazineutralita	k1gFnSc3	kvazineutralita
plazmatu	plazma	k1gNnSc2	plazma
odstíněn	odstínit	k5eAaPmNgInS	odstínit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
označované	označovaný	k2eAgFnSc6d1	označovaná
jako	jako	k8xC	jako
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
stínící	stínící	k2eAgFnSc1d1	stínící
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
již	již	k9	již
plazma	plazma	k1gFnSc1	plazma
opět	opět	k6eAd1	opět
kvazineutrální	kvazineutrální	k2eAgFnSc1d1	kvazineutrální
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
homogenní	homogenní	k2eAgNnSc4d1	homogenní
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
plazmatu	plazma	k1gNnSc2	plazma
vložíme	vložit	k5eAaPmIp1nP	vložit
nepohyblivý	pohyblivý	k2eNgInSc4d1	nepohyblivý
náboj	náboj	k1gInSc4	náboj
q.	q.	k?	q.
Částice	částice	k1gFnSc2	částice
se	s	k7c7	s
souhlasným	souhlasný	k2eAgInSc7d1	souhlasný
nábojem	náboj	k1gInSc7	náboj
jsou	být	k5eAaImIp3nP	být
jím	on	k3xPp3gMnSc7	on
odpuzované	odpuzovaný	k2eAgFnPc1d1	odpuzovaný
<g/>
,	,	kIx,	,
s	s	k7c7	s
nesouhlasným	souhlasný	k2eNgInSc7d1	nesouhlasný
přitahované	přitahovaný	k2eAgFnPc1d1	přitahovaná
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
polarizuje	polarizovat	k5eAaImIp3nS	polarizovat
a	a	k8xC	a
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
vloženého	vložený	k2eAgInSc2d1	vložený
náboje	náboj	k1gInSc2	náboj
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
odstíní	odstínit	k5eAaPmIp3nS	odstínit
<g/>
.	.	kIx.	.
</s>
<s>
Odstíněním	odstínění	k1gNnSc7	odstínění
klesne	klesnout	k5eAaPmIp3nS	klesnout
potenciál	potenciál	k1gInSc1	potenciál
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
oproti	oproti	k7c3	oproti
potenciálu	potenciál	k1gInSc3	potenciál
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
e	e	k0	e
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
zvané	zvaný	k2eAgFnSc6d1	zvaná
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
stínící	stínící	k2eAgFnSc1d1	stínící
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Vypočítáme	vypočítat	k5eAaPmIp1nP	vypočítat
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
teplot	teplota	k1gFnPc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
kladných	kladný	k2eAgMnPc2d1	kladný
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc2	koncentrace
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
n_	n_	k?	n_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
permitivita	permitivita	k1gFnSc1	permitivita
vakua	vakuum	k1gNnSc2	vakuum
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximální	maximální	k2eAgFnSc1d1	maximální
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
elektrony	elektron	k1gInPc4	elektron
při	při	k7c6	při
fluktuaci	fluktuace	k1gFnSc6	fluktuace
je	být	k5eAaImIp3nS	být
také	také	k9	také
rovna	roven	k2eAgFnSc1d1	rovna
Debyeově	Debyeův	k2eAgFnSc3d1	Debyeova
délce	délka	k1gFnSc3	délka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
plazmatu	plazma	k1gNnSc2	plazma
může	moct	k5eAaImIp3nS	moct
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
délka	délka	k1gFnSc1	délka
nabývat	nabývat	k5eAaImF	nabývat
řádově	řádově	k6eAd1	řádově
odlišných	odlišný	k2eAgFnPc2d1	odlišná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
mezihvězdné	mezihvězdný	k2eAgNnSc4d1	mezihvězdné
plazma	plazma	k1gNnSc4	plazma
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
koroně	korona	k1gFnSc6	korona
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
řádově	řádově	k6eAd1	řádově
milimetry	milimetr	k1gInPc1	milimetr
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
hustém	hustý	k2eAgNnSc6d1	husté
plazmatu	plazma	k1gNnSc6	plazma
nanometry	nanometr	k1gInPc1	nanometr
až	až	k8xS	až
desítky	desítka	k1gFnPc1	desítka
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
se	se	k3xPyFc4	se
od	od	k7c2	od
čehokoli	cokoli	k3yInSc2	cokoli
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
něho	on	k3xPp3gNnSc2	on
vloženo	vložen	k2eAgNnSc1d1	vloženo
<g/>
,	,	kIx,	,
oddělí	oddělit	k5eAaPmIp3nP	oddělit
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
dvojvrstvou	dvojvrstvý	k2eAgFnSc4d1	dvojvrstvá
<g/>
.	.	kIx.	.
</s>
<s>
Přibližujeme	přibližovat	k5eAaImIp1nP	přibližovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
k	k	k7c3	k
izolované	izolovaný	k2eAgFnSc3d1	izolovaná
stěně	stěna	k1gFnSc3	stěna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
plazma	plazma	k1gNnSc4	plazma
<g/>
,	,	kIx,	,
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
Debyeovou	Debyeův	k2eAgFnSc7d1	Debyeova
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
plazma	plazma	k1gFnSc1	plazma
přestává	přestávat	k5eAaImIp3nS	přestávat
splňovat	splňovat	k5eAaImF	splňovat
kvazineutralitu	kvazineutralita	k1gFnSc4	kvazineutralita
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
nazýváme	nazývat	k5eAaImIp1nP	nazývat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
dvojvrstvou	dvojvrstvý	k2eAgFnSc4d1	dvojvrstvá
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
elektronů	elektron	k1gInPc2	elektron
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
koncentrace	koncentrace	k1gFnSc2	koncentrace
kladných	kladný	k2eAgInPc2d1	kladný
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stěny	stěna	k1gFnSc2	stěna
převládá	převládat	k5eAaImIp3nS	převládat
kladný	kladný	k2eAgInSc1d1	kladný
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ionty	ion	k1gInPc1	ion
jsou	být	k5eAaImIp3nP	být
urychlovány	urychlovat	k5eAaImNgInP	urychlovat
silným	silný	k2eAgNnSc7d1	silné
polem	pole	k1gNnSc7	pole
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
elektronů	elektron	k1gInPc2	elektron
naopak	naopak	k6eAd1	naopak
převládá	převládat	k5eAaImIp3nS	převládat
tepelný	tepelný	k2eAgInSc4d1	tepelný
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
brzdném	brzdný	k2eAgNnSc6d1	brzdné
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Elektrickou	elektrický	k2eAgFnSc4d1	elektrická
dvojvrstvu	dvojvrstvo	k1gNnSc3	dvojvrstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc4	dva
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
převažuje	převažovat	k5eAaImIp3nS	převažovat
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
záporný	záporný	k2eAgInSc4d1	záporný
<g/>
.	.	kIx.	.
</s>
<s>
Dvojvrstva	Dvojvrstvo	k1gNnSc2	Dvojvrstvo
vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
místy	místo	k1gNnPc7	místo
v	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
velké	velký	k2eAgNnSc4d1	velké
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Potenciál	potenciál	k1gInSc1	potenciál
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinu	většina	k1gFnSc4	většina
tohoto	tento	k3xDgNnSc2	tento
napětí	napětí	k1gNnSc2	napětí
obsáhne	obsáhnout	k5eAaPmIp3nS	obsáhnout
dvojvrstva	dvojvrstvo	k1gNnPc4	dvojvrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poruše	porucha	k1gFnSc6	porucha
v	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
tlumené	tlumený	k2eAgInPc1d1	tlumený
harmonické	harmonický	k2eAgInPc1d1	harmonický
kmity	kmit	k1gInPc1	kmit
jako	jako	k8xS	jako
časový	časový	k2eAgInSc1d1	časový
vývoj	vývoj	k1gInSc1	vývoj
koncentrace	koncentrace	k1gFnSc2	koncentrace
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tlumené	tlumený	k2eAgInPc1d1	tlumený
kmity	kmit	k1gInPc1	kmit
mají	mít	k5eAaImIp3nP	mít
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
frekvenci	frekvence	k1gFnSc4	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnSc3	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazmová	plazmový	k2eAgFnSc1d1	plazmová
frekvence	frekvence	k1gFnSc1	frekvence
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
odmocnině	odmocnina	k1gFnSc3	odmocnina
z	z	k7c2	z
koncentrace	koncentrace	k1gFnSc2	koncentrace
nosičů	nosič	k1gInPc2	nosič
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
omega	omega	k1gNnSc1	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}}}	}}}}}	k?	}}}}}
,	,	kIx,	,
kde	kde	k9	kde
e	e	k0	e
je	být	k5eAaImIp3nS	být
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
koncentrace	koncentrace	k1gFnSc1	koncentrace
nosičů	nosič	k1gInPc2	nosič
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
permitivita	permitivita	k1gFnSc1	permitivita
vakua	vakuum	k1gNnSc2	vakuum
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
hmotnost	hmotnost	k1gFnSc4	hmotnost
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
v	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
pohybovou	pohybový	k2eAgFnSc7d1	pohybová
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}}	}}}}	k?	}}}}
<g/>
{	{	kIx(	{
<g/>
dt	dt	k?	dt
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
Tlumení	tlumení	k1gNnSc1	tlumení
kmitů	kmit	k1gInPc2	kmit
je	být	k5eAaImIp3nS	být
popsané	popsaný	k2eAgNnSc1d1	popsané
časovou	časový	k2eAgFnSc7d1	časová
konstantou	konstanta	k1gFnSc7	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
frekvence	frekvence	k1gFnSc1	frekvence
srážek	srážka	k1gFnPc2	srážka
elektronů	elektron	k1gInPc2	elektron
s	s	k7c7	s
neutrálními	neutrální	k2eAgFnPc7d1	neutrální
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tlumené	tlumený	k2eAgInPc1d1	tlumený
harmonické	harmonický	k2eAgInPc1d1	harmonický
kmity	kmit	k1gInPc1	kmit
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
splněno	splněn	k2eAgNnSc1d1	splněno
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
2	[number]	k4	2
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gNnSc2	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
má	mít	k5eAaImIp3nS	mít
vývoj	vývoj	k1gInSc1	vývoj
koncentrace	koncentrace	k1gFnSc2	koncentrace
elektronů	elektron	k1gInPc2	elektron
aperiodický	aperiodický	k2eAgInSc4d1	aperiodický
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
plazmatu	plazma	k1gNnSc2	plazma
jsou	být	k5eAaImIp3nP	být
magnetické	magnetický	k2eAgFnPc1d1	magnetická
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plazmatu	plazma	k1gNnSc6	plazma
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
gradient	gradient	k1gInSc4	gradient
koncentrace	koncentrace	k1gFnSc2	koncentrace
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
-	-	kIx~	-
například	například	k6eAd1	například
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stacionárním	stacionární	k2eAgInSc6d1	stacionární
případě	případ	k1gInSc6	případ
dostává	dostávat	k5eAaImIp3nS	dostávat
pohybová	pohybový	k2eAgFnSc1d1	pohybová
rovnice	rovnice	k1gFnSc1	rovnice
pro	pro	k7c4	pro
nabité	nabitý	k2eAgFnPc4d1	nabitá
částice	částice	k1gFnPc4	částice
tvar	tvar	k1gInSc1	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
n	n	k0	n
−	−	k?	−
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
q	q	k?	q
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
kT	kT	k?	kT
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k1gFnSc1	nabla
n-m_	n_	k?	n-m_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
pro	pro	k7c4	pro
elektrony	elektron	k1gInPc4	elektron
i	i	k8xC	i
kladné	kladný	k2eAgInPc4d1	kladný
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
Vynásobením	vynásobení	k1gNnSc7	vynásobení
výrazem	výraz	k1gInSc7	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
)	)	kIx)	)
<g/>
}	}	kIx)	}
dostaneme	dostat	k5eAaPmIp1nP	dostat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
n	n	k0	n
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
D	D	kA	D
∇	∇	k?	∇
n	n	k0	n
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
-D	-D	k?	-D
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k1gMnSc1	nabla
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D	D	kA	D
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
difúzní	difúzní	k2eAgInSc1d1	difúzní
koeficient	koeficient	k1gInSc1	koeficient
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hustota	hustota	k1gFnSc1	hustota
toku	tok	k1gInSc2	tok
částic	částice	k1gFnPc2	částice
způsobená	způsobený	k2eAgFnSc1d1	způsobená
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
se	se	k3xPyFc4	se
sčítá	sčítat	k5eAaImIp3nS	sčítat
s	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
toku	tok	k1gInSc2	tok
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
gradientem	gradient	k1gInSc7	gradient
koncentrace	koncentrace	k1gFnSc2	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Aplikujeme	aplikovat	k5eAaBmIp1nP	aplikovat
<g/>
-li	i	k?	-li
tento	tento	k3xDgInSc4	tento
poznatek	poznatek	k1gInSc4	poznatek
na	na	k7c4	na
dvousložkové	dvousložkový	k2eAgNnSc4d1	dvousložkové
plazma	plazma	k1gNnSc4	plazma
uzavřené	uzavřený	k2eAgNnSc4d1	uzavřené
v	v	k7c6	v
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
hustota	hustota	k1gFnSc1	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
n	n	k0	n
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
n	n	k0	n
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}}	}}	k?	}}
<g/>
-D_	-D_	k?	-D_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
n	n	k0	n
:	:	kIx,	:
μ	μ	k?	μ
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
n	n	k0	n
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-n	-n	k?	-n
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}}	}}	k?	}}
<g/>
-D_	-D_	k?	-D_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Nyní	nyní	k6eAd1	nyní
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
n	n	k0	n
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
D_	D_	k1gFnSc1	D_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
-D_	-D_	k?	-D_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
n	n	k0	n
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
To	ten	k3xDgNnSc1	ten
však	však	k9	však
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehomogenní	homogenní	k2eNgFnSc1d1	nehomogenní
plazma	plazma	k1gFnSc1	plazma
<g />
.	.	kIx.	.
</s>
<s hack="1">
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
Ohmův	Ohmův	k2eAgInSc1d1	Ohmův
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
nenulovém	nulový	k2eNgNnSc6d1	nenulové
poli	pole	k1gNnSc6	pole
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
neteče	téct	k5eNaImIp3nS	téct
žádný	žádný	k3yNgInSc4	žádný
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ambipolární	ambipolární	k2eAgNnSc1d1	ambipolární
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc4	výsledek
můžeme	moct	k5eAaImIp1nP	moct
ještě	ještě	k6eAd1	ještě
upravit	upravit	k5eAaPmF	upravit
do	do	k7c2	do
vhodnějšího	vhodný	k2eAgInSc2d2	vhodnější
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
n	n	k0	n
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
μ	μ	k?	μ
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-D_	-D_	k?	-D_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
n	n	k0	n
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
D_	D_	k1gFnSc2	D_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
D_	D_	k1gFnSc1	D_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
D_	D_	k1gFnSc1	D_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gMnPc5	D_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
označuje	označovat	k5eAaImIp3nS	označovat
koeficient	koeficient	k1gInSc1	koeficient
ambipolární	ambipolární	k2eAgFnSc2d1	ambipolární
difúze	difúze	k1gFnSc2	difúze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ambipolární	ambipolární	k2eAgFnSc6d1	ambipolární
difúzi	difúze	k1gFnSc6	difúze
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
toku	tok	k1gInSc2	tok
elektronů	elektron	k1gInPc2	elektron
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
toku	tok	k1gInSc2	tok
kladných	kladný	k2eAgMnPc2d1	kladný
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kvazineutrality	kvazineutralita	k1gFnSc2	kvazineutralita
shodné	shodný	k2eAgFnSc2d1	shodná
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc2	jejich
driftové	driftový	k2eAgFnSc2d1	driftová
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
popisuje	popisovat	k5eAaImIp3nS	popisovat
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
způsobem	způsob	k1gInSc7	způsob
interakci	interakce	k1gFnSc3	interakce
plazmatu	plazma	k1gNnSc2	plazma
s	s	k7c7	s
izolovanou	izolovaný	k2eAgFnSc7d1	izolovaná
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Vezměme	vzít	k5eAaPmRp1nP	vzít
nejprve	nejprve	k6eAd1	nejprve
takovou	takový	k3xDgFnSc4	takový
konfiguraci	konfigurace	k1gFnSc4	konfigurace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
plazma	plazma	k1gNnSc1	plazma
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
dvěma	dva	k4xCgFnPc7	dva
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1	rovnoběžná
rovinnými	rovinný	k2eAgFnPc7d1	rovinná
stěnami	stěna	k1gFnPc7	stěna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l	l	kA	l
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
rekombinací	rekombinace	k1gFnPc2	rekombinace
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
rovnovážného	rovnovážný	k2eAgInSc2d1	rovnovážný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
plazmatu	plazma	k1gNnSc2	plazma
zdroj	zdroj	k1gInSc1	zdroj
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
například	například	k6eAd1	například
elektrickým	elektrický	k2eAgInSc7d1	elektrický
výbojem	výboj	k1gInSc7	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stěnám	stěna	k1gFnPc3	stěna
pronikají	pronikat	k5eAaImIp3nP	pronikat
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
ambipolární	ambipolární	k2eAgFnPc1d1	ambipolární
difuzí	difuze	k1gFnSc7	difuze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
z	z	k7c2	z
rovnice	rovnice	k1gFnSc2	rovnice
kontinuity	kontinuita	k1gFnSc2	kontinuita
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
ionizační	ionizační	k2eAgFnPc4d1	ionizační
frekvence	frekvence	k1gFnPc4	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
}	}	kIx)	}
a	a	k8xC	a
koeficient	koeficient	k1gInSc1	koeficient
difúze	difúze	k1gFnSc2	difúze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gMnSc6	D_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
koncentraci	koncentrace	k1gFnSc4	koncentrace
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
mezi	mezi	k7c7	mezi
stěnami	stěna	k1gFnPc7	stěna
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
D_	D_	k1gMnSc1	D_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}}	}}}	k?	}}}
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
koncentrace	koncentrace	k1gFnSc1	koncentrace
tedy	tedy	k9	tedy
klesá	klesat	k5eAaImIp3nS	klesat
ke	k	k7c3	k
stěnám	stěna	k1gFnPc3	stěna
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
cosinus	cosinus	k1gInSc1	cosinus
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
lze	lze	k6eAd1	lze
postupovat	postupovat	k5eAaImF	postupovat
pro	pro	k7c4	pro
válcovou	válcový	k2eAgFnSc4d1	válcová
konfigurace	konfigurace	k1gFnPc4	konfigurace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
používanější	používaný	k2eAgFnSc6d2	používanější
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dostáváme	dostávat	k5eAaImIp1nP	dostávat
závislost	závislost	k1gFnSc4	závislost
koncentrace	koncentrace	k1gFnSc2	koncentrace
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
válce	válec	k1gInSc2	válec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
r	r	kA	r
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
)	)	kIx)	)
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
J_	J_	k1gFnSc2	J_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
D_	D_	k1gMnSc1	D_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}}	}}}	k?	}}}
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
J	J	kA	J
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
J_	J_	k1gMnSc6	J_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
Besselova	Besselův	k2eAgFnSc1d1	Besselova
funkce	funkce	k1gFnSc1	funkce
nultého	nultý	k4xOgInSc2	nultý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
