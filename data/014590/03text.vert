<s>
Vince	Vince	k?
Clarke	Clarke	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vince	Vince	k?
Clarke	Clarke	k1gFnSc1
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rodné	rodný	k2eAgFnPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Vincent	Vincent	k1gMnSc1
John	John	k1gMnSc1
Martin	Martin	k1gMnSc1
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc4d1
</s>
<s>
Vince	Vince	k?
Clarke	Clarke	k1gNnSc1
Narození	narození	k1gNnPc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
(	(	kIx(
<g/>
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Basildon	Basildon	k1gInSc1
Žánry	žánr	k1gInPc5
</s>
<s>
Synth-pop	Synth-pop	k1gInSc1
<g/>
,	,	kIx,
new	new	k?
wave	wav	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
Hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
písničkář	písničkář	k1gMnSc1
<g/>
,	,	kIx,
DJ	DJ	kA
<g/>
,	,	kIx,
producent	producent	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
Vokály	vokál	k1gInPc1
<g/>
,	,	kIx,
syntetizátor	syntetizátor	k1gInSc1
<g/>
,	,	kIx,
klávesy	kláves	k1gInPc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
klavír	klavír	k1gInSc4
Aktivní	aktivní	k2eAgInSc4d1
roky	rok	k1gInPc7
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
Vydavatelé	vydavatel	k1gMnPc5
</s>
<s>
Mute	Mute	k?
<g/>
,	,	kIx,
Reset	Reset	k1gInSc1
<g/>
,	,	kIx,
Very	Ver	k2eAgInPc1d1
Records	Records	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Depeche	Depeche	k6eAd1
Mode	modus	k1gInSc5
<g/>
,	,	kIx,
Yazoo	Yazoa	k1gMnSc5
<g/>
,	,	kIx,
The	The	k1gMnSc5
Assembly	Assembly	k1gMnSc5
<g/>
,	,	kIx,
Erasure	Erasur	k1gMnSc5
<g/>
,	,	kIx,
VCMG	VCMG	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Marlow	Marlow	k1gMnSc1
<g/>
,	,	kIx,
All	All	k1gFnSc1
Hail	Hail	k1gInSc1
The	The	k1gFnSc2
Silence	silenka	k1gFnSc3
<g/>
,	,	kIx,
West	West	k1gInSc4
India	indium	k1gNnSc2
Company	Compana	k1gFnSc2
Web	web	k1gInSc1
</s>
<s>
www.vinceclarkemusic.com/intro/index.html	www.vinceclarkemusic.com/intro/index.html	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vincent	Vincent	k1gMnSc1
John	John	k1gMnSc1
Martin	Martin	k1gMnSc1
(	(	kIx(
<g/>
známý	známý	k1gMnSc1
jako	jako	k8xC,k8xS
Vince	Vince	k?
Clarke	Clarke	k1gInSc1
<g/>
,	,	kIx,
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglický	anglický	k2eAgMnSc1d1
synth-popový	synth-popový	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clarke	Clarke	k1gInSc1
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
skladatelem	skladatel	k1gMnSc7
a	a	k8xC
hudebníkem	hudebník	k1gMnSc7
skupiny	skupina	k1gFnSc2
Erasure	Erasur	k1gMnSc5
a	a	k8xC
dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgMnSc7d1
skladatelem	skladatel	k1gMnSc7
několika	několik	k4yIc2
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Depeche	Depech	k1gFnSc2
Mode	modus	k1gInSc5
<g/>
,	,	kIx,
Yazoo	Yazoo	k1gMnSc1
a	a	k8xC
The	The	k1gMnSc1
Assembly	Assembly	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Erasure	Erasur	k1gMnSc5
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
známý	známý	k2eAgMnSc1d1
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
pasivní	pasivní	k2eAgNnSc4d1
chování	chování	k1gNnSc4
na	na	k7c6
scéně	scéna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erasure	Erasure	k1gFnSc1
má	mít	k5eAaImIp3nS
přes	přes	k7c4
200	#num#	k4
písní	píseň	k1gFnPc2
a	a	k8xC
prodalo	prodat	k5eAaPmAgNnS
přes	přes	k7c4
25	#num#	k4
milionů	milion	k4xCgInPc2
alb	alba	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
housle	housle	k1gFnPc4
a	a	k8xC
poté	poté	k6eAd1
na	na	k7c4
klavír	klavír	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
elektronické	elektronický	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
při	při	k7c6
poslechu	poslech	k1gInSc6
kapely	kapela	k1gFnSc2
OMD	OMD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
hrál	hrát	k5eAaImAgMnS
ve	v	k7c6
skupině	skupina	k1gFnSc6
Depeche	Depech	k1gFnSc2
Mode	modus	k1gInSc5
<g/>
,	,	kIx,
poté	poté	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
krátce	krátce	k6eAd1
opustil	opustit	k5eAaPmAgMnS
skupinu	skupina	k1gFnSc4
a	a	k8xC
s	s	k7c7
Andym	Andym	k1gInSc1
Bellem	bell	k1gInSc7
vytvořil	vytvořit	k5eAaPmAgInS
skupinu	skupina	k1gFnSc4
Erasure	Erasur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duo	duo	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
prodejních	prodejní	k2eAgInPc2d1
aktů	akt	k1gInPc2
v	v	k7c6
britské	britský	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
s	s	k7c7
mezinárodními	mezinárodní	k2eAgInPc7d1
hity	hit	k1gInPc7
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Oh	oh	k0
L	L	kA
<g/>
'	'	kIx"
<g/>
amour	amour	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Sometimes	Sometimes	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Chains	Chains	k1gInSc1
of	of	k?
Love	lov	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Little	Little	k1gFnSc2
Respect	Respect	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Drama	drama	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Blue	Blue	k1gInSc1
Savannah	Savannah	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Chorus	chorus	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Love	lov	k1gInSc5
To	ten	k3xDgNnSc1
Hate	Hat	k1gMnSc4
You	You	k1gMnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Take	Tak	k1gFnSc2
A	a	k8xC
Chance	Chanec	k1gInSc2
On	on	k3xPp3gMnSc1
Me	Me	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Always	Always	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Depeche	Depeche	k6eAd1
Mode	modus	k1gInSc5
</s>
<s>
Roku	rok	k1gInSc2
1980	#num#	k4
založili	založit	k5eAaPmAgMnP
Vince	Vince	k?
Clarke	Clark	k1gInSc2
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Gore	Gor	k1gFnSc2
a	a	k8xC
Andrew	Andrew	k1gFnSc2
Fletcher	Fletchra	k1gFnPc2
skupinu	skupina	k1gFnSc4
Composition	Composition	k1gInSc4
of	of	k?
Sound	Sound	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ní	on	k3xPp3gFnSc6
zpočátku	zpočátku	k6eAd1
zpíval	zpívat	k5eAaImAgMnS
Clarke	Clarke	k1gInSc4
<g/>
,	,	kIx,
než	než	k8xS
přišel	přijít	k5eAaPmAgMnS
David	David	k1gMnSc1
Gahan	Gahan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
Depeche	Depeche	k1gFnSc4
Mode	modus	k1gInSc5
a	a	k8xC
Vince	Vince	k?
přijal	přijmout	k5eAaPmAgMnS
umělecké	umělecký	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Vince	Vince	k?
Clarke	Clarke	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1981	#num#	k4
vyšlo	vyjít	k5eAaPmAgNnS
album	album	k1gNnSc1
Speak	Speak	k1gInSc1
&	&	k?
Spell	Spell	k1gInSc4
<g/>
,	,	kIx,
většinu	většina	k1gFnSc4
skladeb	skladba	k1gFnPc2
složil	složit	k5eAaPmAgMnS
Clarke	Clarke	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
Clarke	Clark	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Yazoo	Yazoo	k6eAd1
</s>
<s>
Clarke	Clarke	k6eAd1
založil	založit	k5eAaPmAgMnS
se	s	k7c7
zpěvačkou	zpěvačka	k1gFnSc7
Alison	Alison	k1gMnSc1
Moyet	Moyet	k1gMnSc1
duo	duo	k1gNnSc1
Yazoo	Yazoo	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
USA	USA	kA
známé	známý	k2eAgFnPc4d1
jako	jako	k8xC,k8xS
Yaz	Yaz	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS
alba	alba	k1gFnSc1
Upstairs	Upstairs	k1gInSc1
at	at	k?
Eric	Eric	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
You	You	k1gMnSc1
and	and	k?
Me	Me	k1gMnSc1
Both	Both	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Assembly	Assembly	k1gFnSc1
</s>
<s>
Clarke	Clarke	k1gFnSc1
a	a	k8xC
Eric	Eric	k1gFnSc1
Radcliffe	Radcliff	k1gInSc5
založili	založit	k5eAaPmAgMnP
projekt	projekt	k1gInSc4
The	The	k1gFnSc2
Assembly	Assembly	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměrem	záměr	k1gInSc7
bylo	být	k5eAaImAgNnS
vydat	vydat	k5eAaPmF
každý	každý	k3xTgInSc4
singl	singl	k1gInSc4
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
zpěvákem	zpěvák	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Erasure	Erasur	k1gMnSc5
</s>
<s>
Roku	rok	k1gInSc2
1985	#num#	k4
založili	založit	k5eAaPmAgMnP
Clarke	Clarke	k1gFnSc4
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
Andy	Anda	k1gFnSc2
Bell	bell	k1gInSc1
duo	duo	k1gNnSc1
Erasure	Erasur	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
vydržel	vydržet	k5eAaPmAgMnS
Clarke	Clarke	k1gInSc4
mnohem	mnohem	k6eAd1
déle	dlouho	k6eAd2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2017	#num#	k4
vydali	vydat	k5eAaPmAgMnP
už	už	k6eAd1
17	#num#	k4
<g/>
.	.	kIx.
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
VCMG	VCMG	kA
</s>
<s>
Roku	rok	k1gInSc2
2012	#num#	k4
se	se	k3xPyFc4
Clarke	Clarke	k1gFnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Gore	Gor	k1gFnSc2
dali	dát	k5eAaPmAgMnP
znovu	znovu	k6eAd1
dohromady	dohromady	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pod	pod	k7c7
názvem	název	k1gInSc7
VCMG	VCMG	kA
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gFnSc2
iniciály	iniciála	k1gFnSc2
<g/>
)	)	kIx)
vydali	vydat	k5eAaPmAgMnP
album	album	k1gNnSc4
Ssss	Ssssa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Depeche	Depeche	k1gInSc1
Mode	modus	k1gInSc5
David	David	k1gMnSc1
Gahan	Gahana	k1gFnPc2
•	•	k?
Martin	Martin	k1gMnSc1
Gore	Gor	k1gFnSc2
•	•	k?
Andrew	Andrew	k1gFnSc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
Peter	Peter	k1gMnSc1
Gordeno	Gorden	k2eAgNnSc1d1
•	•	k?
Christian	Christian	k1gMnSc1
Eigner	Eigner	k1gMnSc1
Vince	Vince	k?
Clarke	Clark	k1gFnSc2
•	•	k?
Alan	Alan	k1gMnSc1
Wilder	Wilder	k1gMnSc1
Alba	alba	k1gFnSc1
</s>
<s>
Speak	Speak	k1gMnSc1
&	&	k?
Spell	Spell	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
A	a	k8xC
Broken	Broken	k1gInSc1
Frame	Fram	k1gInSc5
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Construction	Construction	k1gInSc1
Time	Time	k1gFnSc1
Again	Again	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Some	Some	k1gInSc1
Great	Great	k2eAgMnSc1d1
Reward	Reward	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Black	Black	k1gInSc1
Celebration	Celebration	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Music	Music	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
Masses	Masses	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Violator	Violator	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Songs	Songs	k1gInSc1
of	of	k?
Faith	Faith	k1gInSc1
and	and	k?
Devotion	Devotion	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ultra	ultra	k2eAgInSc2d1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Exciter	Exciter	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Playing	Playing	k1gInSc1
the	the	k?
Angel	angel	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sounds	Sounds	k1gInSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Delta	delta	k1gFnSc1
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spirit	Spirit	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Kompilace	kompilace	k1gFnSc1
</s>
<s>
People	People	k6eAd1
Are	ar	k1gInSc5
People	People	k1gMnSc6
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Singles	Singles	k1gInSc1
81	#num#	k4
<g/>
>	>	kIx)
<g/>
85	#num#	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Catching	Catching	k1gInSc1
Up	Up	k1gMnSc1
with	with	k1gMnSc1
Depeche	Depeche	k1gInSc4
Mode	modus	k1gInSc5
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
-	-	kIx~
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Greatest	Greatest	k1gInSc1
Hits	Hits	k1gInSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Singles	Singles	k1gInSc1
86	#num#	k4
<g/>
>	>	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
98	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Remixes	Remixes	k1gInSc1
81-04	81-04	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
Depeche	Depeche	k1gInSc1
Mode	modus	k1gInSc5
Volume	volum	k1gInSc5
1	#num#	k4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnPc1
Complete	Comple	k1gNnSc2
Depeche	Depeche	k1gNnSc2
Mode	modus	k1gInSc5
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Remixes	Remixes	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
81-11	81-11	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Záznamy	záznam	k1gInPc7
z	z	k7c2
koncertů	koncert	k1gInPc2
</s>
<s>
101	#num#	k4
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Songs	Songs	k1gInSc1
of	of	k?
Faith	Faith	k1gInSc1
and	and	k?
Devotion	Devotion	k1gInSc1
Live	Live	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Recording	Recording	k1gInSc1
the	the	k?
Angel	angel	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Recording	Recording	k1gInSc1
the	the	k?
Universe	Universe	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Singly	singl	k1gInPc1
</s>
<s>
Dreaming	Dreaming	k1gInSc1
of	of	k?
Me	Me	k1gFnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
New	New	k1gFnSc1
Life	Life	k1gFnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Just	just	k6eAd1
Can	Can	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
Get	Get	k1gMnSc1
Enough	Enough	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
See	See	k1gMnSc1
You	You	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Meaning	Meaning	k1gInSc1
of	of	k?
Love	lov	k1gInSc5
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leave	Leaev	k1gFnSc2
in	in	k?
Silence	silenka	k1gFnSc6
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Get	Get	k1gFnSc2
the	the	k?
Balance	balanc	k1gFnSc2
Right	Righta	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Everything	Everything	k1gInSc1
Counts	Counts	k1gInSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Love	lov	k1gInSc5
<g/>
,	,	kIx,
in	in	k?
Itself	Itself	k1gInSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Told	Told	k1gInSc1
You	You	k1gMnSc1
So	So	kA
(	(	kIx(
<g/>
only	onla	k1gMnSc2
in	in	k?
Spanish	Spanish	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
))	))	k?
•	•	k?
People	People	k1gFnSc1
Are	ar	k1gInSc5
People	People	k1gMnSc6
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Master	master	k1gMnSc1
and	and	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Servant	servanta	k1gFnPc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Blasphemous	Blasphemous	k1gInSc1
Rumours	Rumours	k1gInSc1
/	/	kIx~
Somebody	Someboda	k1gFnPc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Shake	Shake	k1gNnPc2
the	the	k?
Disease	Diseas	k1gInSc6
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
It	It	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Called	Called	k1gInSc1
a	a	k8xC
Heart	Heart	k1gInSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stripped	Stripped	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
A	a	k8xC
Question	Question	k1gInSc1
of	of	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lust	Lust	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
A	a	k8xC
Question	Question	k1gInSc1
of	of	k?
Time	Time	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
But	But	k1gFnSc2
Not	nota	k1gFnPc2
Tonight	Tonight	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Strangelove	Strangelov	k1gInSc5
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Never	Never	k1gInSc1
Let	let	k1gInSc1
Me	Me	k1gFnSc1
Down	Down	k1gMnSc1
Again	Again	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Behind	Behind	k1gMnSc1
the	the	k?
Wheel	Wheel	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Little	Little	k1gFnSc1
15	#num#	k4
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Strangelove	Strangelov	k1gInSc5
'	'	kIx"
<g/>
88	#num#	k4
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Everything	Everything	k1gInSc1
Counts	Counts	k1gInSc1
(	(	kIx(
<g/>
Live	Live	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Personal	Personal	k1gMnSc1
Jesus	Jesus	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Enjoy	Enjoa	k1gFnSc2
the	the	k?
Silence	silenka	k1gFnSc6
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Policy	Polica	k1gFnSc2
of	of	k?
Truth	Truth	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
World	World	k1gInSc1
in	in	k?
My	my	k3xPp1nPc1
Eyes	Eyesa	k1gFnPc2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
I	i	k9
Feel	Feel	k1gMnSc1
You	You	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Walking	Walking	k1gInSc1
in	in	k?
My	my	k3xPp1nPc1
Shoes	Shoes	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Condemnation	Condemnation	k1gInSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
In	In	k1gMnSc1
Your	Your	k1gMnSc1
Room	Room	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barrel	Barrel	k1gInSc1
of	of	k?
a	a	k8xC
Gun	Gun	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
It	It	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
No	no	k9
Good	Gooda	k1gFnPc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Home	Home	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Useless	Useless	k1gInSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Only	Only	k1gInPc1
When	Wheno	k1gNnPc2
I	i	k9
Lose	los	k1gInSc6
Myself	Myself	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dream	Dream	k1gInSc1
On	on	k3xPp3gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
I	i	k9
Feel	Feel	k1gMnSc1
Loved	Loved	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Freelove	Freelov	k1gInSc5
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Goodnight	Goodnight	k1gInSc1
Lovers	Lovers	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Enjoy	Enjoa	k1gFnSc2
the	the	k?
Silence	silenka	k1gFnSc6
'	'	kIx"
<g/>
0	#num#	k4
<g/>
4	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Precious	Precious	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
A	a	k8xC
Pain	Pain	k1gMnSc1
that	that	k1gMnSc1
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Used	Used	k1gInSc1
To	to	k9
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Suffer	Suffer	k1gMnSc1
Well	Well	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
John	John	k1gMnSc1
the	the	k?
Revelator	Revelator	k1gMnSc1
/	/	kIx~
Lillian	Lillian	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Martyr	martyr	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wrong	Wrong	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peace	Peace	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fragile	Fragila	k1gFnSc6
Tension	Tension	k1gInSc1
/	/	kIx~
Hole	hole	k6eAd1
to	ten	k3xDgNnSc4
Feed	Feed	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Personal	Personal	k1gMnSc1
Jesus	Jesus	k1gMnSc1
'	'	kIx"
<g/>
11	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Heaven	Heavna	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soothe	Soothe	k1gInSc1
My	my	k3xPp1nPc1
Soul	Soul	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Should	Should	k1gInSc1
Be	Be	k1gMnSc1
Higher	Highra	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Propagační	propagační	k2eAgInPc1d1
singly	singl	k1gInPc1
</s>
<s>
Speak	Speak	k1gMnSc1
&	&	k?
Spell	Spell	k1gMnSc1
sampler	sampler	k1gMnSc1
:	:	kIx,
Big	Big	k1gMnSc1
Muff	Muff	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Back	Back	k1gInSc1
to	ten	k3xDgNnSc1
Back	Back	k1gMnSc1
Hits	Hits	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
People	People	k1gFnSc1
Are	ar	k1gInSc5
People	People	k1gFnSc7
/	/	kIx~
A	a	k9
Question	Question	k1gInSc1
of	of	k?
Lust	Lust	k1gInSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Route	Rout	k1gInSc5
66	#num#	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
I	i	k9
Want	Want	k1gMnSc1
You	You	k1gMnSc1
Now	Now	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pleasure	Pleasur	k1gMnSc5
<g/>
,	,	kIx,
Little	Little	k1gFnPc1
Treasure	Treasur	k1gMnSc5
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Back	Back	k1gInSc1
to	ten	k3xDgNnSc1
Back	Back	k1gMnSc1
Hits	Hits	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Back	Back	k1gInSc1
Trax	Trax	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
One	One	k1gFnSc2
Caress	Caressa	k1gFnPc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Live	Live	k1gInSc1
<g/>
...	...	k?
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rush	Rush	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Behind	Behind	k1gMnSc1
the	the	k?
Wheel	Wheel	k1gMnSc1
'	'	kIx"
<g/>
11	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Koncertní	koncertní	k2eAgNnSc1d1
turné	turné	k1gNnSc1
</s>
<s>
1980	#num#	k4
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1981	#num#	k4
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UK	UK	kA
Tour	Tour	k1gInSc4
1981	#num#	k4
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1982	#num#	k4
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
See	See	k1gMnSc1
You	You	k1gMnSc1
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Broken	Broken	k1gInSc1
Frame	Fram	k1gInSc5
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Construction	Construction	k1gInSc1
Time	Time	k1gFnSc1
Again	Again	k1gMnSc1
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
-	-	kIx~
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Some	Some	k1gInSc1
Great	Great	k2eAgMnSc1d1
Reward	Reward	k1gMnSc1
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Black	Black	k1gInSc1
Celebration	Celebration	k1gInSc1
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Music	Music	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
Masses	Masses	k1gMnSc1
Tour	Tour	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
World	World	k1gInSc1
Violation	Violation	k1gInSc1
Tour	Tour	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devotional	Devotional	k1gMnSc1
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Exotic	Exotice	k1gFnPc2
Tour	Tour	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Summer	Summer	k1gMnSc1
Tour	Tour	k1gMnSc1
'	'	kIx"
<g/>
94	#num#	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Ultra	ultra	k2eAgMnSc1d1
Parties	Parties	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Singles	Singles	k1gMnSc1
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Exciter	Exciter	k1gMnSc1
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Touring	Touring	k1gInSc1
the	the	k?
Angel	angel	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tour	Tour	k1gInSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Delta	delta	k1gFnSc1
Machine	Machin	k1gInSc5
Tour	Tour	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Videozáznamy	videozáznam	k1gInPc1
</s>
<s>
The	The	k?
World	World	k1gInSc1
We	We	k1gMnSc2
Live	Liv	k1gMnSc2
in	in	k?
and	and	k?
Live	Live	k1gInSc1
in	in	k?
Hamburg	Hamburg	k1gInSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Some	Some	k1gInSc1
Great	Great	k2eAgMnSc1d1
Videos	Videos	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Strange	Strange	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
101	#num#	k4
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Strange	Strange	k1gInSc1
Too	Too	k1gFnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devotional	Devotional	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Videos	Videos	k1gInSc1
86	#num#	k4
<g/>
>	>	kIx)
<g/>
98	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
One	One	k1gMnSc1
Night	Night	k1gMnSc1
in	in	k?
Paris	Paris	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Touring	Touring	k1gInSc1
the	the	k?
Angel	angel	k1gMnSc1
<g/>
:	:	kIx,
Live	Live	k1gFnSc1
in	in	k?
Milan	Milan	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tour	Tour	k1gInSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc1
<g/>
:	:	kIx,
Live	Live	k1gFnSc1
in	in	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Tribute	tribut	k1gInSc5
alba	album	k1gNnPc4
</s>
<s>
I	i	k9
Sometimes	Sometimes	k1gMnSc1
Wish	Wish	k1gMnSc1
I	i	k8xC
Was	Was	k1gMnSc1
Famous	Famous	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
For	forum	k1gNnPc2
the	the	k?
Masses	Masses	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Color	Color	k1gInSc1
Theory	Theora	k1gFnSc2
Presents	Presentsa	k1gFnPc2
Depeche	Depech	k1gFnSc2
Mode	modus	k1gInSc5
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AKH-toong	AKH-toong	k1gMnSc1
BAY-bi	BAY-b	k1gFnSc2
Covered	Covered	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
(	(	kIx(
<g/>
Depeche	Depech	k1gInSc2
Mode	modus	k1gInSc5
covered	covered	k1gInSc4
U2	U2	k1gFnSc1
song	song	k1gInSc1
So	So	kA
Cruel	Cruel	k1gInSc1
<g/>
))	))	k?
Související	související	k2eAgFnSc1d1
</s>
<s>
Seznam	seznam	k1gInSc1
coververzí	coververze	k1gFnPc2
skladeb	skladba	k1gFnPc2
Depeche	Depech	k1gFnSc2
Mode	modus	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002152004	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
134348044	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1594	#num#	k4
9778	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006089004	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
11061775	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006089004	#num#	k4
</s>
