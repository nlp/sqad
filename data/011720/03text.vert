<p>
<s>
Chua-šan	Chua-šan	k1gInSc1	Chua-šan
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
华	华	k?	华
pchin-jin	pchinin	k1gMnSc1	pchin-jin
Huà	Huà	k1gMnSc1	Huà
shā	shā	k?	shā
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Květinová	květinový	k2eAgFnSc1d1	květinová
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
Pěti	pět	k4xCc2	pět
velkých	velká	k1gFnPc2	velká
hor.	hor.	k?	hor.
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Šen-si	Šene	k1gFnSc6	Šen-se
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
Si-an	Sina	k1gFnPc2	Si-ana
a	a	k8xC	a
asi	asi	k9	asi
800	[number]	k4	800
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Pekingu	Peking	k1gInSc2	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Chua-šan	Chua-šan	k1gInSc1	Chua-šan
je	být	k5eAaImIp3nS	být
posvátnou	posvátný	k2eAgFnSc7d1	posvátná
horou	hora	k1gFnSc7	hora
taoismu	taoismus	k1gInSc2	taoismus
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
taoistických	taoistický	k2eAgMnPc2d1	taoistický
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hora	hora	k1gFnSc1	hora
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
květinu	květina	k1gFnSc4	květina
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
i	i	k9	i
název	název	k1gInSc1	název
Květinová	květinový	k2eAgFnSc1d1	květinová
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
úpatí	úpatí	k1gNnSc6	úpatí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
taoistických	taoistický	k2eAgInPc2d1	taoistický
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
vrcholy	vrchol	k1gInPc4	vrchol
hory	hora	k1gFnSc2	hora
vedou	vést	k5eAaImIp3nP	vést
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
vytesané	vytesaný	k2eAgInPc4d1	vytesaný
schody	schod	k1gInPc4	schod
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
cílem	cíl	k1gInSc7	cíl
čínských	čínský	k2eAgInPc2d1	čínský
mileneckých	milenecký	k2eAgInPc2d1	milenecký
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zavěšují	zavěšovat	k5eAaImIp3nP	zavěšovat
na	na	k7c4	na
řetězy	řetěz	k1gInPc4	řetěz
kolem	kolem	k7c2	kolem
cesty	cesta	k1gFnSc2	cesta
zámky	zámek	k1gInPc1	zámek
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgFnSc1d1	symbolizující
trvalost	trvalost	k1gFnSc1	trvalost
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jižního	jižní	k2eAgInSc2d1	jižní
vrcholu	vrchol	k1gInSc2	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tzv.	tzv.	kA	tzv.
Sráz	sráz	k1gInSc1	sráz
tisíce	tisíc	k4xCgInPc1	tisíc
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
něhož	jenž	k3xRgNnSc2	jenž
vede	vést	k5eAaImIp3nS	vést
údajně	údajně	k6eAd1	údajně
nejnebezpečnější	bezpečný	k2eNgFnSc1d3	nejnebezpečnější
stezka	stezka	k1gFnSc1	stezka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
dřevěných	dřevěný	k2eAgNnPc2d1	dřevěné
prken	prkno	k1gNnPc2	prkno
upevněných	upevněný	k2eAgNnPc2d1	upevněné
na	na	k7c6	na
svislé	svislý	k2eAgFnSc6d1	svislá
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
</p>
