<s>
Jaká	jaký	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zkratka	zkratka	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
Úmluvu	úmluva	k1gFnSc4
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
?	?	kIx.
</s>