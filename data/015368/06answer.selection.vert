<s>
Úmluva	úmluva	k1gFnSc1
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Convention	Convention	k1gInSc4
on	on	k3xPp3gMnSc1
International	International	k1gMnSc1
Trade	Trad	k1gInSc5
in	in	k?
Endangered	Endangered	k1gMnSc1
Species	species	k1gFnSc2
of	of	k?
Wild	Wild	k1gInSc1
Fauna	fauna	k1gFnSc1
and	and	k?
Flora	Flora	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
CITES	CITES	kA
<g/>
;	;	kIx,
jinak	jinak	k6eAd1
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
Washingtonská	washingtonský	k2eAgFnSc1d1
úmluva	úmluva	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
dohod	dohoda	k1gFnPc2
chránících	chránící	k2eAgFnPc2d1
rostliny	rostlina	k1gFnPc4
a	a	k8xC
živočichy	živočich	k1gMnPc4
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>