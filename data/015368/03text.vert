<s>
Úmluva	úmluva	k1gFnSc1
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
</s>
<s>
Úmluva	úmluva	k1gFnSc1
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Convention	Convention	k1gInSc4
on	on	k3xPp3gMnSc1
International	International	k1gMnSc1
Trade	Trad	k1gInSc5
in	in	k?
Endangered	Endangered	k1gMnSc1
Species	species	k1gFnSc2
of	of	k?
Wild	Wild	k1gInSc1
Fauna	fauna	k1gFnSc1
and	and	k?
Flora	Flora	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
CITES	CITES	kA
<g/>
;	;	kIx,
jinak	jinak	k6eAd1
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
Washingtonská	washingtonský	k2eAgFnSc1d1
úmluva	úmluva	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
dohod	dohoda	k1gFnPc2
chránících	chránící	k2eAgFnPc2d1
rostliny	rostlina	k1gFnPc4
a	a	k8xC
živočichy	živočich	k1gMnPc4
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
smyslem	smysl	k1gInSc7
je	být	k5eAaImIp3nS
celosvětová	celosvětový	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
obchodu	obchod	k1gInSc2
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
smlouvy	smlouva	k1gFnSc2
</s>
<s>
CITES	CITES	kA
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
nejen	nejen	k6eAd1
exemplářů	exemplář	k1gInPc2
z	z	k7c2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
živočichů	živočich	k1gMnPc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
odchovaných	odchovaný	k2eAgFnPc2d1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
nebo	nebo	k8xC
vypěstovaných	vypěstovaný	k2eAgInPc2d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
žijí	žít	k5eAaImIp3nP
<g/>
/	/	kIx~
<g/>
rostou	růst	k5eAaImIp3nP
i	i	k9
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezahrnuje	zahrnovat	k5eNaImIp3nS
domestikovaná	domestikovaný	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
a	a	k8xC
kulturní	kulturní	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Smlouva	smlouva	k1gFnSc1
nabyla	nabýt	k5eAaPmAgFnS
platnosti	platnost	k1gFnSc3
v	v	k7c6
červenci	červenec	k1gInSc6
1975	#num#	k4
a	a	k8xC
přijalo	přijmout	k5eAaPmAgNnS
ji	on	k3xPp3gFnSc4
(	(	kIx(
<g/>
stav	stav	k1gInSc1
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
183	#num#	k4
smluvních	smluvní	k2eAgMnPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
CITES	CITES	kA
obsahuje	obsahovat	k5eAaImIp3nS
seznamy	seznam	k1gInPc4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
kterých	který	k3yRgInPc2,k3yQgInPc2,k3yIgInPc2
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
uvedeny	uveden	k2eAgInPc1d1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
přílohách	příloha	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
příloha	příloha	k1gFnSc1
I	i	k9
obsahuje	obsahovat	k5eAaImIp3nS
nejvíce	nejvíce	k6eAd1,k6eAd3
ohrožené	ohrožený	k2eAgInPc4d1
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgFnPc7
je	být	k5eAaImIp3nS
jakýkoliv	jakýkoliv	k3yIgInSc1
mezinárodní	mezinárodní	k2eAgInSc1d1
obchod	obchod	k1gInSc1
zakázán	zakázat	k5eAaPmNgInS
</s>
<s>
příloha	příloha	k1gFnSc1
II	II	kA
zahrnuje	zahrnovat	k5eAaImIp3nS
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
obchodování	obchodování	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
omezeno	omezit	k5eAaPmNgNnS
a	a	k8xC
podřízeno	podřídit	k5eAaPmNgNnS
dozoru	dozor	k1gInSc6
</s>
<s>
příloha	příloha	k1gFnSc1
III	III	kA
dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
druhy	druh	k1gInPc4
lokálně	lokálně	k6eAd1
ohrožené	ohrožený	k2eAgInPc1d1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgFnPc2
je	být	k5eAaImIp3nS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
kontrolován	kontrolován	k2eAgInSc1d1
a	a	k8xC
omezen	omezen	k2eAgInSc1d1
obchod	obchod	k1gInSc1
pouze	pouze	k6eAd1
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
regionech	region	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
EU	EU	kA
pohlíží	pohlížet	k5eAaImIp3nS
na	na	k7c4
některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
přísněji	přísně	k6eAd2
a	a	k8xC
proto	proto	k8xC
zavádí	zavádět	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
příloha	příloha	k1gFnSc1
a	a	k8xC
(	(	kIx(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS
I	i	k9
<g/>
)	)	kIx)
</s>
<s>
příloha	příloha	k1gFnSc1
b	b	k?
(	(	kIx(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS
II	II	kA
<g/>
)	)	kIx)
</s>
<s>
příloha	příloha	k1gFnSc1
c	c	k0
(	(	kIx(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS
III	III	kA
<g/>
)	)	kIx)
</s>
<s>
příloha	příloha	k1gFnSc1
d	d	k?
obsahuje	obsahovat	k5eAaImIp3nS
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
se	se	k3xPyFc4
hojně	hojně	k6eAd1
obchoduje	obchodovat	k5eAaImIp3nS
a	a	k8xC
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
sledovat	sledovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
jejich	jejich	k3xOp3gInSc4
dovoz	dovoz	k1gInSc4
se	se	k3xPyFc4
nežádá	žádat	k5eNaImIp3nS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
se	se	k3xPyFc4
vyplňuje	vyplňovat	k5eAaImIp3nS
statistické	statistický	k2eAgNnSc1d1
hlášení	hlášení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Databáze	databáze	k1gFnSc1
CITES	CITES	kA
je	být	k5eAaImIp3nS
spravovaná	spravovaný	k2eAgFnSc1d1
UNEP-WCMC	UNEP-WCMC	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Přínosy	přínos	k1gInPc1
CITES	CITES	kA
<g/>
:	:	kIx,
</s>
<s>
regulace	regulace	k1gFnSc1
obchodu	obchod	k1gInSc2
s	s	k7c7
chráněnými	chráněný	k2eAgInPc7d1
druhy	druh	k1gInPc7
</s>
<s>
zvýšení	zvýšení	k1gNnSc1
populací	populace	k1gFnPc2
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
ohrožených	ohrožený	k2eAgMnPc2d1
lovem	lov	k1gInSc7
</s>
<s>
Problémy	problém	k1gInPc1
CITES	CITES	kA
<g/>
:	:	kIx,
</s>
<s>
malá	malý	k2eAgFnSc1d1
flexibilita	flexibilita	k1gFnSc1
a	a	k8xC
zpolitikaření	zpolitikaření	k1gNnSc1
</s>
<s>
nezajišťuje	zajišťovat	k5eNaImIp3nS
ochranu	ochrana	k1gFnSc4
přírodního	přírodní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
</s>
<s>
pomalé	pomalý	k2eAgNnSc1d1
a	a	k8xC
často	často	k6eAd1
neprůhledné	průhledný	k2eNgNnSc4d1
vyřizování	vyřizování	k1gNnSc4
žádostí	žádost	k1gFnPc2
<g/>
,	,	kIx,
korupční	korupční	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
zvláště	zvláště	k6eAd1
v	v	k7c6
rozvojových	rozvojový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
chrání	chránit	k5eAaImIp3nP
i	i	k9
druhy	druh	k1gInPc1
běžné	běžný	k2eAgInPc1d1
jak	jak	k6eAd1
v	v	k7c6
přírodě	příroda	k1gFnSc6
tak	tak	k8xS,k8xC
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
chrání	chránit	k5eAaImIp3nP
i	i	k9
umělé	umělý	k2eAgFnPc1d1
hybridy	hybrida	k1gFnPc1
rostlin	rostlina	k1gFnPc2
</s>
<s>
brání	bránit	k5eAaImIp3nS
ex	ex	k6eAd1
situ	situ	k6eAd1
a	a	k8xC
in	in	k?
farm	farm	k6eAd1
ochraně	ochrana	k1gFnSc6
druhů	druh	k1gInPc2
</s>
<s>
Související	související	k2eAgFnSc1d1
legislativa	legislativa	k1gFnSc1
</s>
<s>
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
platí	platit	k5eAaImIp3nS
nařízení	nařízení	k1gNnSc1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
338	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1996	#num#	k4
o	o	k7c6
ochraně	ochrana	k1gFnSc6
volně	volně	k6eAd1
žijících	žijící	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
směrnicí	směrnice	k1gFnSc7
o	o	k7c6
obchodu	obchod	k1gInSc6
s	s	k7c7
nimi	on	k3xPp3gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
směrnice	směrnice	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
přísnější	přísný	k2eAgFnSc7d2
než	než	k8xS
samotná	samotný	k2eAgFnSc1d1
Washingtonská	washingtonský	k2eAgFnSc1d1
úmluva	úmluva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
CITES	CITES	kA
v	v	k7c6
ČR	ČR	kA
</s>
<s>
V	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
obchod	obchod	k1gInSc4
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
regulován	regulovat	k5eAaImNgInS
zákonem	zákon	k1gInSc7
100	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
zákonná	zákonný	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
je	být	k5eAaImIp3nS
přísnější	přísný	k2eAgFnSc1d2
než	než	k8xS
původní	původní	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
požaduje	požadovat	k5eAaImIp3nS
prokázat	prokázat	k5eAaPmF
původ	původ	k1gInSc4
jedinců	jedinec	k1gMnPc2
i	i	k8xC
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
soukromých	soukromý	k2eAgFnPc2d1
a	a	k8xC
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
je	být	k5eAaImIp3nS
připravována	připravován	k2eAgFnSc1d1
novelizace	novelizace	k1gFnSc1
uvedeného	uvedený	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
pasáže	pasáž	k1gFnPc1
této	tento	k3xDgFnSc2
novely	novela	k1gFnSc2
vyvolaly	vyvolat	k5eAaPmAgInP
protesty	protest	k1gInPc1
chovatelských	chovatelský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
pěstitelů	pěstitel	k1gMnPc2
kaktusů	kaktus	k1gInPc2
<g/>
,	,	kIx,
orchidejí	orchidea	k1gFnPc2
i	i	k8xC
odborníků	odborník	k1gMnPc2
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kučera	Kučera	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Konvence	konvence	k1gFnSc2
CITES	CITES	kA
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Barčiová	Barčiový	k2eAgFnSc1d1
L.	L.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
z	z	k7c2
konference	konference	k1gFnSc2
Otazníky	otazník	k1gInPc4
kolem	kolem	k7c2
CITES	CITES	kA
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZFJU	ZFJU	kA
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7040	#num#	k4
<g/>
-	-	kIx~
<g/>
965	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Doskočil	Doskočil	k1gMnSc1
L.	L.	kA
<g/>
:	:	kIx,
Celní	celní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
ČR	ČR	kA
a	a	k8xC
CITES	CITES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Barčiová	Barčiový	k2eAgFnSc1d1
L.	L.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
z	z	k7c2
konference	konference	k1gFnSc2
Otazníky	otazník	k1gInPc4
kolem	kolem	k7c2
CITES	CITES	kA
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZFJU	ZFJU	kA
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7040	#num#	k4
<g/>
-	-	kIx~
<g/>
965	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.teraristika.cz	www.teraristika.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bednář	Bednář	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Cites	Cites	k1gMnSc1
versus	versus	k7c1
EU	EU	kA
aneb	aneb	k?
Je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
v	v	k7c6
EU	EU	kA
opravdu	opravdu	k6eAd1
přínosná	přínosný	k2eAgFnSc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Referát	referát	k1gInSc1
na	na	k7c6
konferenci	konference	k1gFnSc6
Otazníky	otazník	k1gInPc4
kolem	kolem	k7c2
CITES	CITES	kA
2007	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
↑	↑	k?
Kunte	Kunte	k?
L.	L.	kA
<g/>
:	:	kIx,
Problematika	problematika	k1gFnSc1
ochrany	ochrana	k1gFnSc2
aridních	aridní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Mexika	Mexiko	k1gNnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
CITES	CITES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Referát	referát	k1gInSc1
na	na	k7c6
konferenci	konference	k1gFnSc6
Otazníky	otazník	k1gInPc4
kolem	kolem	k7c2
CITES	CITES	kA
2007	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
↑	↑	k?
Kubát	Kubát	k1gMnSc1
I.	I.	kA
<g/>
:	:	kIx,
Problémy	problém	k1gInPc1
s	s	k7c7
uplatňováním	uplatňování	k1gNnSc7
CITES	CITES	kA
v	v	k7c6
ZUU	ZUU	kA
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c7
Vl	Vl	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Referát	referát	k1gInSc1
na	na	k7c6
konferenci	konference	k1gFnSc6
Otazníky	otazník	k1gInPc4
kolem	kolem	k7c2
CITES	CITES	kA
2007	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
↑	↑	k?
Sekerka	sekerka	k1gFnSc1
P.	P.	kA
<g/>
:	:	kIx,
Cites	Cites	k1gMnSc1
-	-	kIx~
restrikce	restrikce	k1gFnSc1
a	a	k8xC
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
dál	daleko	k6eAd2
<g/>
?	?	kIx.
</s>
<s desamb="1">
ex	ex	k6eAd1
situ	situ	k6eAd1
<g/>
,	,	kIx,
in	in	k?
farm	farm	k1gInSc1
a	a	k8xC
in	in	k?
garden	gardna	k1gFnPc2
ochrana	ochrana	k1gFnSc1
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Barčiová	Barčiový	k2eAgFnSc1d1
L.	L.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
z	z	k7c2
konference	konference	k1gFnSc2
Otazníky	otazník	k1gInPc4
kolem	kolem	k7c2
CITES	CITES	kA
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZFJU	ZFJU	kA
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7040-965-7	978-80-7040-965-7	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
obsažených	obsažený	k2eAgFnPc2d1
v	v	k7c6
CITES	CITES	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Úmluva	úmluva	k1gFnSc1
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
seznam	seznam	k1gInSc1
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
CITES	CITES	kA
homepage	homepage	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Dokumenty	dokument	k1gInPc1
CITES	CITES	kA
v	v	k7c6
češtině	čeština	k1gFnSc6
na	na	k7c6
webu	web	k1gInSc6
Ministerstva	ministerstvo	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4242663-7	4242663-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80017747	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
175384421	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80017747	#num#	k4
</s>
