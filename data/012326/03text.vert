<p>
<s>
Incká	incký	k2eAgFnSc1d1	incká
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
kečuánsky	kečuánsky	k6eAd1	kečuánsky
Tahuantinsuyo	Tahuantinsuyo	k6eAd1	Tahuantinsuyo
<g/>
,	,	kIx,	,
novějším	nový	k2eAgInSc7d2	novější
přepisem	přepis	k1gInSc7	přepis
Tawantinsuyu	Tawantinsuyus	k1gInSc2	Tawantinsuyus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Čtyři	čtyři	k4xCgFnPc1	čtyři
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
španělsky	španělsky	k6eAd1	španělsky
Imperio	Imperio	k1gMnSc1	Imperio
Inca	Inca	k1gMnSc1	Inca
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
andských	andský	k2eAgFnPc6d1	andská
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešních	dnešní	k2eAgInPc2d1	dnešní
států	stát	k1gInPc2	stát
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc1d3	veliký
indiánská	indiánský	k2eAgFnSc1d1	indiánská
říše	říše	k1gFnSc1	říše
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
třetině	třetina	k1gFnSc6	třetina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
španělských	španělský	k2eAgMnPc2d1	španělský
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
<g/>
,	,	kIx,	,
vedených	vedený	k2eAgFnPc2d1	vedená
conquistadorem	conquistador	k1gMnSc7	conquistador
Franciskem	Francisko	k1gNnSc7	Francisko
Pizarrem	Pizarr	k1gMnSc7	Pizarr
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
využili	využít	k5eAaPmAgMnP	využít
rozepří	rozepře	k1gFnSc7	rozepře
ve	v	k7c6	v
vládnoucí	vládnoucí	k2eAgFnSc6d1	vládnoucí
rodině	rodina	k1gFnSc6	rodina
Inků	Ink	k1gMnPc2	Ink
(	(	kIx(	(
<g/>
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
panovnického	panovnický	k2eAgInSc2d1	panovnický
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
i	i	k9	i
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
jméno	jméno	k1gNnSc1	jméno
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
i	i	k8xC	i
vážných	vážný	k2eAgInPc2d1	vážný
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
rozporů	rozpor	k1gInPc2	rozpor
jejich	jejich	k3xOp3gFnSc2	jejich
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc2d1	mnohonárodnostní
<g/>
,	,	kIx,	,
kulturně	kulturně	k6eAd1	kulturně
různorodé	různorodý	k2eAgFnSc2d1	různorodá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
ji	on	k3xPp3gFnSc4	on
vojensky	vojensky	k6eAd1	vojensky
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
–	–	k?	–
pomohla	pomoct	k5eAaPmAgFnS	pomoct
jim	on	k3xPp3gMnPc3	on
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
i	i	k9	i
nadřazenost	nadřazenost	k1gFnSc1	nadřazenost
evropské	evropský	k2eAgFnSc2d1	Evropská
výzbroje	výzbroj	k1gFnSc2	výzbroj
a	a	k8xC	a
taktiky	taktika	k1gFnSc2	taktika
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
do	do	k7c2	do
incké	incký	k2eAgFnSc2d1	incká
populace	populace	k1gFnSc2	populace
přivlečených	přivlečený	k2eAgFnPc2d1	přivlečená
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
nakažlivých	nakažlivý	k2eAgFnPc2d1	nakažlivá
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepatrný	nepatrný	k2eAgInSc1d1	nepatrný
zbytek	zbytek	k1gInSc1	zbytek
říše	říš	k1gFnSc2	říš
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
boční	boční	k2eAgFnSc2d1	boční
linie	linie	k1gFnSc2	linie
Inků	Ink	k1gMnPc2	Ink
vydržel	vydržet	k5eAaPmAgMnS	vydržet
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
okolo	okolo	k7c2	okolo
pevnosti	pevnost	k1gFnSc2	pevnost
Vilcabamba	Vilcabamb	k1gMnSc2	Vilcabamb
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
dobyt	dobyt	k2eAgInSc4d1	dobyt
Španěly	Španěly	k1gInPc4	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
koloniální	koloniální	k2eAgMnPc1d1	koloniální
páni	pan	k1gMnPc1	pan
systematicky	systematicky	k6eAd1	systematicky
zničili	zničit	k5eAaPmAgMnP	zničit
většinu	většina	k1gFnSc4	většina
hmotných	hmotný	k2eAgInPc2d1	hmotný
i	i	k8xC	i
duchovních	duchovní	k2eAgInPc2d1	duchovní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
po	po	k7c6	po
Incích	Ink	k1gMnPc6	Ink
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
mity	mita	k1gFnSc2	mita
<g/>
,	,	kIx,	,
systému	systém	k1gInSc2	systém
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
ponechali	ponechat	k5eAaPmAgMnP	ponechat
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
zpřísnili	zpřísnit	k5eAaPmAgMnP	zpřísnit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědomí	vědomí	k1gNnSc6	vědomí
kečujských	kečujský	k2eAgMnPc2d1	kečujský
Indiánů	Indián	k1gMnPc2	Indián
však	však	k9	však
určité	určitý	k2eAgNnSc4d1	určité
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
incké	incký	k2eAgFnSc6d1	incká
státnosti	státnost	k1gFnSc6	státnost
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
oživeno	oživit	k5eAaPmNgNnS	oživit
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
španělské	španělský	k2eAgFnSc2d1	španělská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odkazu	odkaz	k1gInSc3	odkaz
Incké	incký	k2eAgFnSc2d1	incká
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
silně	silně	k6eAd1	silně
hlásí	hlásit	k5eAaImIp3nS	hlásit
zejména	zejména	k9	zejména
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
leželo	ležet	k5eAaImAgNnS	ležet
jádro	jádro	k1gNnSc1	jádro
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
a	a	k8xC	a
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
potomci	potomek	k1gMnPc1	potomek
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jej	on	k3xPp3gMnSc4	on
založili	založit	k5eAaPmAgMnP	založit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
říše	říš	k1gFnSc2	říš
===	===	k?	===
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
říše	říše	k1gFnSc1	říše
omezena	omezit	k5eAaPmNgFnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
město	město	k1gNnSc4	město
Cuzco	Cuzco	k6eAd1	Cuzco
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pupek	pupek	k1gInSc1	pupek
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Manco	Manco	k6eAd1	Manco
Capac	Capac	k1gFnSc1	Capac
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
tohoto	tento	k3xDgInSc2	tento
městského	městský	k2eAgInSc2d1	městský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
dalších	další	k2eAgMnPc2d1	další
osm	osm	k4xCc1	osm
Inků	Ink	k1gMnPc2	Ink
vládli	vládnout	k5eAaImAgMnP	vládnout
jen	jen	k9	jen
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
malou	malý	k2eAgFnSc7d1	malá
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
obsazovali	obsazovat	k5eAaImAgMnP	obsazovat
inčtí	incký	k2eAgMnPc1d1	incký
panovníci	panovník	k1gMnPc1	panovník
okolní	okolní	k2eAgNnPc4d1	okolní
údolí	údolí	k1gNnPc4	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
malého	malý	k2eAgNnSc2d1	malé
inckého	incký	k2eAgNnSc2d1	incké
království	království	k1gNnSc2	království
se	se	k3xPyFc4	se
znelíbila	znelíbit	k5eAaPmAgFnS	znelíbit
sousední	sousední	k2eAgFnSc1d1	sousední
velké	velký	k2eAgFnSc3d1	velká
říši	říš	k1gFnSc3	říš
Chanků	Chanek	k1gMnPc2	Chanek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
také	také	k6eAd1	také
rozšiřovali	rozšiřovat	k5eAaImAgMnP	rozšiřovat
své	svůj	k3xOyFgNnSc4	svůj
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
než	než	k8xS	než
Inkové	Ink	k1gMnPc1	Ink
<g/>
.	.	kIx.	.
</s>
<s>
Chankové	Chanek	k1gMnPc1	Chanek
začali	začít	k5eAaPmAgMnP	začít
sestavovat	sestavovat	k5eAaImF	sestavovat
armádu	armáda	k1gFnSc4	armáda
proti	proti	k7c3	proti
malému	malý	k2eAgNnSc3d1	malé
<g/>
,	,	kIx,	,
úrodnému	úrodný	k2eAgNnSc3d1	úrodné
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
že	že	k8xS	že
Inků	Ink	k1gMnPc2	Ink
bylo	být	k5eAaImAgNnS	být
málo	málo	k6eAd1	málo
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
slabí	slabit	k5eAaImIp3nP	slabit
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
rozdělení	rozdělení	k1gNnSc1	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Inkům	Ink	k1gMnPc3	Ink
panoval	panovat	k5eAaImAgMnS	panovat
starý	starý	k2eAgMnSc1d1	starý
Viracocha	Viracoch	k1gMnSc4	Viracoch
Inca	Inc	k2eAgFnSc1d1	Inca
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
boje	boj	k1gInSc2	boj
raději	rád	k6eAd2	rád
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
se	se	k3xPyFc4	se
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svého	svůj	k3xOyFgNnSc2	svůj
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Cusi	Cusi	k1gNnSc4	Cusi
Yupanqui	Yupanqu	k1gFnSc2	Yupanqu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
kmeny	kmen	k1gInPc7	kmen
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
sestavil	sestavit	k5eAaPmAgMnS	sestavit
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
odvážně	odvážně	k6eAd1	odvážně
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
proti	proti	k7c3	proti
armádě	armáda	k1gFnSc3	armáda
Chanků	Chanek	k1gInPc2	Chanek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lítém	lítý	k2eAgInSc6d1	lítý
boji	boj	k1gInSc6	boj
Inkové	Ink	k1gMnPc1	Ink
Chanky	Chanek	k1gInPc4	Chanek
porazili	porazit	k5eAaPmAgMnP	porazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1438	[number]	k4	1438
byl	být	k5eAaImAgMnS	být
princ	princ	k1gMnSc1	princ
Cusi	Cusi	k1gNnSc2	Cusi
Yupanqui	Yupanqu	k1gFnSc2	Yupanqu
korunován	korunován	k2eAgInSc4d1	korunován
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
datum	datum	k1gNnSc4	datum
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInPc3	první
historicky	historicky	k6eAd1	historicky
ověřeným	ověřený	k2eAgNnSc7d1	ověřené
datem	datum	k1gNnSc7	datum
incké	incký	k2eAgFnSc2d1	incká
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Pachacuti	Pachacuť	k1gFnSc2	Pachacuť
Yupanqui	Yupanqu	k1gFnSc2	Yupanqu
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
výborný	výborný	k2eAgMnSc1d1	výborný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proměnil	proměnit	k5eAaPmAgMnS	proměnit
malé	malý	k2eAgNnSc4d1	malé
incké	incký	k2eAgNnSc4d1	incké
království	království	k1gNnSc4	království
v	v	k7c4	v
prosperující	prosperující	k2eAgInSc4d1	prosperující
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
kmeny	kmen	k1gInPc4	kmen
vyhlazoval	vyhlazovat	k5eAaImAgInS	vyhlazovat
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnSc6d1	jiná
připojoval	připojovat	k5eAaImAgMnS	připojovat
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
politiky	politika	k1gFnSc2	politika
reciprocity	reciprocita	k1gFnSc2	reciprocita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nárokované	nárokovaný	k2eAgInPc4d1	nárokovaný
kmeny	kmen	k1gInPc4	kmen
uplatil	uplatit	k5eAaPmAgInS	uplatit
a	a	k8xC	a
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
jim	on	k3xPp3gMnPc3	on
například	například	k6eAd1	například
vzácné	vzácný	k2eAgNnSc4d1	vzácné
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
připojily	připojit	k5eAaPmAgFnP	připojit
a	a	k8xC	a
získaly	získat	k5eAaPmAgFnP	získat
tím	ten	k3xDgNnSc7	ten
různé	různý	k2eAgFnPc4d1	různá
politické	politický	k2eAgFnPc4d1	politická
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kmenů	kmen	k1gInPc2	kmen
této	tento	k3xDgFnSc2	tento
nabídky	nabídka	k1gFnSc2	nabídka
využilo	využít	k5eAaPmAgNnS	využít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
incká	incký	k2eAgFnSc1d1	incká
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
vládnoucí	vládnoucí	k2eAgFnPc1d1	vládnoucí
rodiny	rodina	k1gFnPc1	rodina
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kmenů	kmen	k1gInPc2	kmen
posílaly	posílat	k5eAaImAgFnP	posílat
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
naučili	naučit	k5eAaPmAgMnP	naučit
správnému	správný	k2eAgNnSc3d1	správné
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
mohli	moct	k5eAaImAgMnP	moct
pod	pod	k7c7	pod
inckou	incký	k2eAgFnSc7d1	incká
nadvládou	nadvláda	k1gFnSc7	nadvláda
vládnout	vládnout	k5eAaImF	vládnout
svým	svůj	k3xOyFgInPc3	svůj
kmenům	kmen	k1gInPc3	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Pachacuti	Pachacut	k1gMnPc1	Pachacut
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
soudržnost	soudržnost	k1gFnSc4	soudržnost
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
podnikal	podnikat	k5eAaImAgMnS	podnikat
takové	takový	k3xDgInPc4	takový
výboje	výboj	k1gInPc4	výboj
<g/>
,	,	kIx,	,
o	o	k7c6	o
jakých	jaký	k3yIgInPc6	jaký
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
jen	jen	k9	jen
nechat	nechat	k5eAaPmF	nechat
zdát	zdát	k5eAaImF	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Říši	říše	k1gFnSc4	říše
z	z	k7c2	z
království	království	k1gNnSc2	království
Cuzco	Cuzco	k6eAd1	Cuzco
transformoval	transformovat	k5eAaBmAgInS	transformovat
na	na	k7c4	na
Tahuantintsuyo	Tahuantintsuyo	k1gNnSc4	Tahuantintsuyo
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
čtveřice	čtveřice	k1gFnSc2	čtveřice
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
sbíhaly	sbíhat	k5eAaImAgFnP	sbíhat
v	v	k7c6	v
Cuzcu	Cuzcum	k1gNnSc6	Cuzcum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
z	z	k7c2	z
kterých	který	k3yRgFnPc2	který
byly	být	k5eAaImAgFnP	být
příslušné	příslušný	k2eAgFnPc4d1	příslušná
země	zem	k1gFnPc4	zem
spravovány	spravován	k2eAgFnPc4d1	spravována
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Pachacutiho	Pachacuti	k1gMnSc2	Pachacuti
syna	syn	k1gMnSc2	syn
Túpaca	Túpacus	k1gMnSc2	Túpacus
Yupanquiho	Yupanqui	k1gMnSc2	Yupanqui
říše	říš	k1gFnSc2	říš
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
táhla	táhnout	k5eAaImAgFnS	táhnout
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
jižní	jižní	k2eAgFnSc2d1	jižní
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
až	až	k9	až
po	po	k7c4	po
střední	střední	k2eAgNnSc4d1	střední
Chile	Chile	k1gNnSc4	Chile
a	a	k8xC	a
od	od	k7c2	od
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
až	až	k9	až
po	po	k7c4	po
amazonskou	amazonský	k2eAgFnSc4d1	Amazonská
džungli	džungle	k1gFnSc4	džungle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příchod	příchod	k1gInSc1	příchod
Španělů	Španěl	k1gMnPc2	Španěl
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
panování	panování	k1gNnSc2	panování
dalšího	další	k2eAgMnSc2d1	další
Inky	Inka	k1gMnSc2	Inka
Huayna	Huayn	k1gMnSc2	Huayn
Capaka	Capak	k1gMnSc2	Capak
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
sídlil	sídlit	k5eAaImAgInS	sídlit
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Quitu	Quito	k1gNnSc6	Quito
<g/>
,	,	kIx,	,
přistávají	přistávat	k5eAaImIp3nP	přistávat
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
první	první	k4xOgMnPc1	první
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sem	sem	k6eAd1	sem
zavlekli	zavleknout	k5eAaPmAgMnP	zavleknout
zkázonosnou	zkázonosný	k2eAgFnSc4d1	zkázonosná
epidemii	epidemie	k1gFnSc4	epidemie
pravých	pravý	k2eAgFnPc2d1	pravá
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zdecimovala	zdecimovat	k5eAaPmAgFnS	zdecimovat
indiánskou	indiánský	k2eAgFnSc4d1	indiánská
populaci	populace	k1gFnSc4	populace
a	a	k8xC	a
jíž	jenž	k3xRgFnSc3	jenž
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
Huayna	Huayn	k1gMnSc4	Huayn
Cápac	Cápac	k1gInSc1	Cápac
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Huáscar	Huáscara	k1gFnPc2	Huáscara
a	a	k8xC	a
Atahualpa	Atahualp	k1gMnSc2	Atahualp
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
několikaletého	několikaletý	k2eAgInSc2d1	několikaletý
bratrovražedného	bratrovražedný	k2eAgInSc2d1	bratrovražedný
boje	boj	k1gInSc2	boj
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Triumfující	triumfující	k2eAgFnSc1d1	triumfující
Atahualpa	Atahualpa	k1gFnSc1	Atahualpa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyvraždil	vyvraždit	k5eAaPmAgMnS	vyvraždit
s	s	k7c7	s
Huáscarem	Huáscar	k1gInSc7	Huáscar
většinu	většina	k1gFnSc4	většina
cuzcoské	cuzcoský	k2eAgFnSc2d1	cuzcoský
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
však	však	k9	však
své	svůj	k3xOyFgNnSc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
ani	ani	k8xC	ani
nemohl	moct	k5eNaImAgMnS	moct
pořádně	pořádně	k6eAd1	pořádně
vychutnat	vychutnat	k5eAaPmF	vychutnat
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1532	[number]	k4	1532
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgMnS	dostavit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
neozbrojeným	ozbrojený	k2eNgInSc7d1	neozbrojený
několikatisícovým	několikatisícový	k2eAgInSc7d1	několikatisícový
doprovodem	doprovod	k1gInSc7	doprovod
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Španělů	Španěl	k1gMnPc2	Španěl
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Cajamarce	Cajamarka	k1gFnSc6	Cajamarka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obětí	oběť	k1gFnSc7	oběť
zrádného	zrádný	k2eAgInSc2d1	zrádný
útoku	útok	k1gInSc2	útok
pouhé	pouhý	k2eAgFnSc2d1	pouhá
stovky	stovka	k1gFnSc2	stovka
conquistadorů	conquistador	k1gMnPc2	conquistador
vedených	vedený	k2eAgMnPc2d1	vedený
Franciskem	Francisko	k1gNnSc7	Francisko
Pizarrem	Pizarr	k1gMnSc7	Pizarr
<g/>
.	.	kIx.	.
</s>
<s>
Šokovaní	šokovaný	k2eAgMnPc1d1	šokovaný
Indiáni	Indián	k1gMnPc1	Indián
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nepostavili	postavit	k5eNaPmAgMnP	postavit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
pobiti	pobit	k2eAgMnPc1d1	pobit
<g/>
;	;	kIx,	;
Atahualpa	Atahualpa	k1gFnSc1	Atahualpa
byl	být	k5eAaImAgInS	být
zajat	zajat	k2eAgMnSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
zajatý	zajatý	k1gMnSc1	zajatý
Atahualpa	Atahualp	k1gMnSc2	Atahualp
splnil	splnit	k5eAaPmAgInS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
poddaní	poddaný	k1gMnPc1	poddaný
naplnili	naplnit	k5eAaPmAgMnP	naplnit
zlatem	zlato	k1gNnSc7	zlato
až	až	k9	až
ke	k	k7c3	k
stropu	strop	k1gInSc2	strop
určenou	určený	k2eAgFnSc4d1	určená
místnost	místnost	k1gFnSc4	místnost
(	(	kIx(	(
<g/>
místnost	místnost	k1gFnSc1	místnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
byl	být	k5eAaImAgMnS	být
vězněn	vězněn	k2eAgMnSc1d1	vězněn
<g/>
)	)	kIx)	)
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
ještě	ještě	k6eAd1	ještě
dvojnásobný	dvojnásobný	k2eAgInSc4d1	dvojnásobný
objem	objem	k1gInSc4	objem
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
ho	on	k3xPp3gMnSc4	on
nepropustili	propustit	k5eNaPmAgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
spiknutí	spiknutí	k1gNnSc2	spiknutí
a	a	k8xC	a
usmrcen	usmrcen	k2eAgMnSc1d1	usmrcen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc1	zánik
říše	říš	k1gFnSc2	říš
===	===	k?	===
</s>
</p>
<p>
<s>
Pizarro	Pizarro	k6eAd1	Pizarro
potom	potom	k6eAd1	potom
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
Cuzco	Cuzco	k1gNnSc4	Cuzco
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Cuzca	Cuzc	k1gInSc2	Cuzc
potkal	potkat	k5eAaPmAgInS	potkat
Pizzaro	Pizzara	k1gFnSc5	Pizzara
družinu	družina	k1gFnSc4	družina
Manco	Manco	k1gNnSc1	Manco
Inky	Inka	k1gMnSc2	Inka
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
přeživších	přeživší	k2eAgNnPc2d1	přeživší
z	z	k7c2	z
Huáscarovy	Huáscarův	k2eAgFnSc2d1	Huáscarův
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Manco	Manco	k1gMnSc1	Manco
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
Pizzarovi	Pizzar	k1gMnSc3	Pizzar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
Huáscarova	Huáscarův	k2eAgMnSc4d1	Huáscarův
soka	sok	k1gMnSc4	sok
Atahualpu	Atahualp	k1gInSc2	Atahualp
<g/>
.	.	kIx.	.
</s>
<s>
Pizzaro	Pizzara	k1gFnSc5	Pizzara
měl	mít	k5eAaImAgInS	mít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
intuici	intuice	k1gFnSc4	intuice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
předejít	předejít	k5eAaPmF	předejít
možnému	možný	k2eAgNnSc3d1	možné
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
nadvládě	nadvláda	k1gFnSc3	nadvláda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
naoko	naoko	k6eAd1	naoko
udělil	udělit	k5eAaPmAgMnS	udělit
Mancovi	Manec	k1gMnSc3	Manec
naprostou	naprostý	k2eAgFnSc4d1	naprostá
suverenitu	suverenita	k1gFnSc4	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
však	však	k9	však
dobře	dobře	k6eAd1	dobře
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
podrobení	podrobení	k1gNnSc6	podrobení
a	a	k8xC	a
vládnutí	vládnutí	k1gNnSc6	vládnutí
tak	tak	k8xS	tak
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
říši	říš	k1gFnSc6	říš
měl	mít	k5eAaImAgMnS	mít
málo	málo	k4c4	málo
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
tlačil	tlačit	k5eAaImAgMnS	tlačit
na	na	k7c4	na
Manca	Mancus	k1gMnSc4	Mancus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sestavoval	sestavovat	k5eAaImAgMnS	sestavovat
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Manco	Manco	k6eAd1	Manco
velice	velice	k6eAd1	velice
rád	rád	k6eAd1	rád
Pizzarovi	Pizzar	k1gMnSc3	Pizzar
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
pomstít	pomstít	k5eAaPmF	pomstít
generálu	generál	k1gMnSc3	generál
Quisquisovi	Quisquis	k1gMnSc3	Quisquis
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dal	dát	k5eAaPmAgMnS	dát
vyvraždit	vyvraždit	k5eAaPmF	vyvraždit
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Pizzarův	Pizzarův	k2eAgMnSc1d1	Pizzarův
společník	společník	k1gMnSc1	společník
Hernando	Hernanda	k1gFnSc5	Hernanda
de	de	k?	de
Soto	Soto	k1gMnSc1	Soto
vydal	vydat	k5eAaPmAgMnS	vydat
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
50	[number]	k4	50
španělských	španělský	k2eAgInPc2d1	španělský
jezdců	jezdec	k1gInPc2	jezdec
a	a	k8xC	a
10	[number]	k4	10
000	[number]	k4	000
inckými	incký	k2eAgFnPc7d1	incká
bojovníky	bojovník	k1gMnPc4	bojovník
na	na	k7c4	na
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
generálu	generál	k1gMnSc3	generál
Quisquisovi	Quisquis	k1gMnSc3	Quisquis
<g/>
.	.	kIx.	.
</s>
<s>
Společnými	společný	k2eAgFnPc7d1	společná
silami	síla	k1gFnPc7	síla
donutili	donutit	k5eAaPmAgMnP	donutit
generála	generál	k1gMnSc4	generál
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Cuzca	Cuzc	k1gInSc2	Cuzc
byla	být	k5eAaImAgFnS	být
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
povstání	povstání	k1gNnPc2	povstání
a	a	k8xC	a
nezávislý	závislý	k2eNgInSc1d1	nezávislý
incký	incký	k2eAgInSc1d1	incký
státeček	státeček	k1gInSc1	státeček
ve	v	k7c6	v
Vilcabambě	Vilcabamba	k1gFnSc6	Vilcabamba
(	(	kIx(	(
<g/>
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
Cuzca	Cuzc	k1gInSc2	Cuzc
a	a	k8xC	a
Machu	macha	k1gFnSc4	macha
Picchu	Picch	k1gInSc2	Picch
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
<g/>
,	,	kIx,	,
věk	věk	k1gInSc1	věk
Inků	Ink	k1gMnPc2	Ink
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vládci	vládce	k1gMnSc3	vládce
Inků	Ink	k1gMnPc2	Ink
==	==	k?	==
</s>
</p>
<p>
<s>
Inkové	Ink	k1gMnPc1	Ink
měli	mít	k5eAaImAgMnP	mít
pevně	pevně	k6eAd1	pevně
stanovená	stanovený	k2eAgNnPc4d1	stanovené
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prostého	prostý	k2eAgInSc2d1	prostý
lidu	lid	k1gInSc2	lid
–	–	k?	–
měl	mít	k5eAaImAgInS	mít
prakticky	prakticky	k6eAd1	prakticky
neomezené	omezený	k2eNgNnSc4d1	neomezené
množství	množství	k1gNnSc4	množství
manželek	manželka	k1gFnPc2	manželka
a	a	k8xC	a
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
panovník	panovník	k1gMnSc1	panovník
vzešel	vzejít	k5eAaPmAgMnS	vzejít
z	z	k7c2	z
úplně	úplně	k6eAd1	úplně
jiného	jiný	k2eAgInSc2d1	jiný
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Inka	Inka	k1gMnSc1	Inka
si	se	k3xPyFc3	se
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
bral	brát	k5eAaImAgInS	brát
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
vzešlý	vzešlý	k2eAgMnSc1d1	vzešlý
z	z	k7c2	z
takového	takový	k3xDgInSc2	takový
incestního	incestní	k2eAgInSc2d1	incestní
svazku	svazek	k1gInSc2	svazek
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
dědicem	dědic	k1gMnSc7	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
"	"	kIx"	"
<g/>
čistota	čistota	k1gFnSc1	čistota
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
vládu	vláda	k1gFnSc4	vláda
nezaručovalo	zaručovat	k5eNaImAgNnS	zaručovat
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělalo	udělat	k5eAaPmAgNnS	udělat
hlavní	hlavní	k2eAgInSc4d1	hlavní
terč	terč	k1gInSc4	terč
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
bratrovražedné	bratrovražedný	k2eAgInPc1d1	bratrovražedný
boje	boj	k1gInPc1	boj
byly	být	k5eAaImAgInP	být
nutností	nutnost	k1gFnSc7	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Inka	Inka	k1gMnSc1	Inka
tím	ten	k3xDgNnSc7	ten
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
a	a	k8xC	a
jen	jen	k9	jen
on	on	k3xPp3gMnSc1	on
může	moct	k5eAaImIp3nS	moct
vládnout	vládnout	k5eAaImF	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
neznali	znát	k5eNaImAgMnP	znát
písmo	písmo	k1gNnSc4	písmo
a	a	k8xC	a
jména	jméno	k1gNnSc2	jméno
jejich	jejich	k3xOp3gInPc2	jejich
předků	předek	k1gInPc2	předek
se	se	k3xPyFc4	se
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
po	po	k7c4	po
generace	generace	k1gFnPc4	generace
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
ústím	ústí	k1gNnSc7	ústí
podání	podání	k1gNnSc1	podání
<g/>
.	.	kIx.	.
</s>
<s>
Těla	tělo	k1gNnPc1	tělo
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
inckých	incký	k2eAgMnPc2d1	incký
panovníků	panovník	k1gMnPc2	panovník
se	se	k3xPyFc4	se
nepohřbívala	pohřbívat	k5eNaImAgFnS	pohřbívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byla	být	k5eAaImAgNnP	být
dále	daleko	k6eAd2	daleko
uctívána	uctívat	k5eAaImNgNnP	uctívat
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
živá	živý	k2eAgFnSc1d1	živá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zemědělství	zemědělství	k1gNnSc1	zemědělství
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
každá	každý	k3xTgFnSc1	každý
velká	velký	k2eAgFnSc1d1	velká
říše	říše	k1gFnSc1	říše
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
incká	incký	k2eAgFnSc1d1	incká
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Inkům	Ink	k1gMnPc3	Ink
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
pěstování	pěstování	k1gNnSc4	pěstování
plodin	plodina	k1gFnPc2	plodina
rozložení	rozložení	k1gNnSc4	rozložení
jejich	jejich	k3xOp3gFnSc2	jejich
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
vybudovat	vybudovat	k5eAaPmF	vybudovat
zavlažovací	zavlažovací	k2eAgInPc4d1	zavlažovací
kanály	kanál	k1gInPc4	kanál
a	a	k8xC	a
terasy	teras	k1gInPc4	teras
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
sekerka	sekerka	k1gFnSc1	sekerka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
plodinou	plodina	k1gFnSc7	plodina
byly	být	k5eAaImAgFnP	být
brambory	brambora	k1gFnPc4	brambora
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
plodinou	plodina	k1gFnSc7	plodina
byla	být	k5eAaImAgFnS	být
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimochodem	mimochodem	k9	mimochodem
<g/>
,	,	kIx,	,
vařilo	vařit	k5eAaImAgNnS	vařit
i	i	k9	i
pivo	pivo	k1gNnSc1	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
merlík	merlík	k1gInSc4	merlík
<g/>
,	,	kIx,	,
fazole	fazole	k1gFnPc1	fazole
<g/>
,	,	kIx,	,
tykve	tykev	k1gFnPc1	tykev
<g/>
,	,	kIx,	,
rajče	rajče	k1gNnSc1	rajče
jedlé	jedlý	k2eAgInPc1d1	jedlý
<g/>
,	,	kIx,	,
arašídy	arašíd	k1gInPc1	arašíd
<g/>
,	,	kIx,	,
papriky	paprika	k1gFnPc1	paprika
a	a	k8xC	a
melouny	meloun	k1gInPc1	meloun
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
se	se	k3xPyFc4	se
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
majetkem	majetek	k1gInSc7	majetek
Boha	bůh	k1gMnSc2	bůh
slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
=	=	kIx~	=
kněžstva	kněžstvo	k1gNnSc2	kněžstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
rolníky	rolník	k1gMnPc4	rolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenství	vojenství	k1gNnSc2	vojenství
==	==	k?	==
</s>
</p>
<p>
<s>
Incká	incký	k2eAgFnSc1d1	incká
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
armádou	armáda	k1gFnSc7	armáda
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
platila	platit	k5eAaImAgFnS	platit
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
pro	pro	k7c4	pro
poddané	poddaný	k1gMnPc4	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
nejméně	málo	k6eAd3	málo
jednou	jeden	k4xCgFnSc7	jeden
účastnit	účastnit	k5eAaImF	účastnit
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
moc	moc	k6eAd1	moc
nezajímali	zajímat	k5eNaImAgMnP	zajímat
o	o	k7c4	o
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jim	on	k3xPp3gMnPc3	on
zabíjení	zabíjení	k1gNnSc1	zabíjení
nepřátel	nepřítel	k1gMnPc2	nepřítel
nebylo	být	k5eNaImAgNnS	být
žádnou	žádný	k3yNgFnSc7	žádný
překážkou	překážka	k1gFnSc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
neměli	mít	k5eNaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
železo	železo	k1gNnSc4	železo
nebo	nebo	k8xC	nebo
ocel	ocel	k1gFnSc4	ocel
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
zbraně	zbraň	k1gFnPc1	zbraň
nebyly	být	k5eNaImAgFnP	být
o	o	k7c4	o
moc	moc	k6eAd1	moc
lepší	dobrý	k2eAgFnPc4d2	lepší
než	než	k8xS	než
zbraně	zbraň	k1gFnPc1	zbraň
jejich	jejich	k3xOp3gMnPc2	jejich
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
nepoužívali	používat	k5eNaImAgMnP	používat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
luky	luk	k1gInPc1	luk
a	a	k8xC	a
šípy	šíp	k1gInPc1	šíp
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
střelnou	střelný	k2eAgFnSc7d1	střelná
zbraní	zbraň	k1gFnSc7	zbraň
byl	být	k5eAaImAgInS	být
ruční	ruční	k2eAgInSc1d1	ruční
prak	prak	k1gInSc1	prak
<g/>
.	.	kIx.	.
</s>
<s>
Neznali	znát	k5eNaImAgMnP	znát
ani	ani	k8xC	ani
žádné	žádný	k3yNgInPc1	žádný
obléhací	obléhací	k2eAgInPc1d1	obléhací
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnPc1	pevnost
se	se	k3xPyFc4	se
dobývaly	dobývat	k5eAaImAgFnP	dobývat
ztečí	zteč	k1gFnSc7	zteč
<g/>
.	.	kIx.	.
</s>
<s>
Incká	incký	k2eAgNnPc1d1	incké
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
pouštěla	pouštět	k5eAaImAgFnS	pouštět
za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
bubnů	buben	k1gInPc2	buben
a	a	k8xC	a
keramických	keramický	k2eAgFnPc2d1	keramická
nebo	nebo	k8xC	nebo
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
trumpet	trumpeta	k1gFnPc2	trumpeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Incká	incký	k2eAgFnSc1d1	incká
zbroj	zbroj	k1gFnSc1	zbroj
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
helem	helma	k1gFnPc2	helma
vyhotovených	vyhotovený	k2eAgFnPc2d1	vyhotovená
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
kulatých	kulatý	k2eAgFnPc2d1	kulatá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obdélníkových	obdélníkový	k2eAgInPc2d1	obdélníkový
štítů	štít	k1gInPc2	štít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
látkových	látkový	k2eAgFnPc2d1	látková
tunik	tunika	k1gFnPc2	tunika
polstrovaných	polstrovaný	k2eAgFnPc2d1	polstrovaná
bavlnou	bavlna	k1gFnSc7	bavlna
a	a	k8xC	a
s	s	k7c7	s
dřevěnými	dřevěný	k2eAgNnPc7d1	dřevěné
prkénky	prkénko	k1gNnPc7	prkénko
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
<g/>
Incké	incký	k2eAgFnPc1d1	incká
zbraně	zbraň	k1gFnPc1	zbraň
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kopí	kopit	k5eAaImIp3nP	kopit
se	s	k7c7	s
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kostěnými	kostěný	k2eAgInPc7d1	kostěný
hroty	hrot	k1gInPc7	hrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
obouruční	obouruční	k2eAgInPc1d1	obouruční
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
meče	meč	k1gInPc1	meč
s	s	k7c7	s
ozubenými	ozubený	k2eAgFnPc7d1	ozubená
hranami	hrana	k1gFnPc7	hrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kyje	kyj	k1gInPc1	kyj
s	s	k7c7	s
kamennými	kamenný	k2eAgFnPc7d1	kamenná
a	a	k8xC	a
kovovými	kovový	k2eAgFnPc7d1	kovová
špičatými	špičatý	k2eAgFnPc7d1	špičatá
hlavicemi	hlavice	k1gFnPc7	hlavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
válečné	válečný	k2eAgFnPc1d1	válečná
sekery	sekera	k1gFnPc1	sekera
s	s	k7c7	s
kamennými	kamenný	k2eAgFnPc7d1	kamenná
nebo	nebo	k8xC	nebo
měděnými	měděný	k2eAgFnPc7d1	měděná
hlavicemi	hlavice	k1gFnPc7	hlavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
bola	bola	k1gFnSc1	bola
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgInPc7	který
byl	být	k5eAaImAgInS	být
navázán	navázán	k2eAgInSc4d1	navázán
provaz	provaz	k1gInSc4	provaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kožený	kožený	k2eAgInSc4d1	kožený
nebo	nebo	k8xC	nebo
látkový	látkový	k2eAgInSc4d1	látkový
prak	prak	k1gInSc4	prak
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
vrhaly	vrhat	k5eAaImAgInP	vrhat
kameny	kámen	k1gInPc1	kámen
velikosti	velikost	k1gFnSc2	velikost
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náboženství	náboženství	k1gNnSc2	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Incké	incký	k2eAgInPc1d1	incký
mýty	mýtus	k1gInPc1	mýtus
byly	být	k5eAaImAgInP	být
tradovány	tradovat	k5eAaImNgInP	tradovat
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Španělští	španělský	k2eAgMnPc1d1	španělský
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
incké	incký	k2eAgInPc1d1	incký
mýty	mýtus	k1gInPc1	mýtus
byly	být	k5eAaImAgInP	být
zaznamenávány	zaznamenávat	k5eAaImNgInP	zaznamenávat
na	na	k7c4	na
quipus	quipus	k1gInSc4	quipus
<g/>
,	,	kIx,	,
andské	andský	k2eAgInPc4d1	andský
provázky	provázek	k1gInPc4	provázek
s	s	k7c7	s
uzlíky	uzlík	k1gInPc7	uzlík
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c6	v
reinkarnaci	reinkarnace	k1gFnSc6	reinkarnace
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
přechodem	přechod	k1gInSc7	přechod
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Inků	Ink	k1gMnPc2	Ink
si	se	k3xPyFc3	se
představovala	představovat	k5eAaImAgFnS	představovat
posmrtný	posmrtný	k2eAgInSc4d1	posmrtný
svět	svět	k1gInSc4	svět
velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc1d1	podobný
euro-americké	euromerický	k2eAgFnSc3d1	euro-americká
představě	představa	k1gFnSc3	představa
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
pole	pole	k1gNnPc1	pole
pokrytá	pokrytý	k2eAgNnPc1d1	pokryté
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
zasněžené	zasněžený	k2eAgInPc4d1	zasněžený
vrcholky	vrcholek	k1gInPc4	vrcholek
hor.	hor.	k?	hor.
Kvůli	kvůli	k7c3	kvůli
základnímu	základní	k2eAgNnSc3d1	základní
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
vytratila	vytratit	k5eAaPmAgFnS	vytratit
životní	životní	k2eAgFnSc1d1	životní
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
ohrozila	ohrozit	k5eAaPmAgFnS	ohrozit
jejich	jejich	k3xOp3gInSc4	jejich
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nesměla	smět	k5eNaImAgFnS	smět
těla	tělo	k1gNnSc2	tělo
nebožtíků	nebožtík	k1gMnPc2	nebožtík
spalovat	spalovat	k5eAaImF	spalovat
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
dodržovali	dodržovat	k5eAaImAgMnP	dodržovat
incký	incký	k2eAgInSc4d1	incký
morální	morální	k2eAgInSc4d1	morální
kodex	kodex	k1gInSc4	kodex
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
posmrtném	posmrtný	k2eAgInSc6d1	posmrtný
světě	svět	k1gInSc6	svět
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
"	"	kIx"	"
<g/>
sluncem	slunce	k1gNnSc7	slunce
vyhřáté	vyhřátý	k2eAgFnSc2d1	vyhřátá
<g/>
"	"	kIx"	"
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
trávili	trávit	k5eAaImAgMnP	trávit
věčné	věčný	k2eAgFnPc4d1	věčná
dny	dna	k1gFnPc4	dna
ve	v	k7c6	v
"	"	kIx"	"
<g/>
studené	studený	k2eAgFnSc6d1	studená
<g/>
"	"	kIx"	"
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
také	také	k9	také
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
lebeční	lebeční	k2eAgFnSc4d1	lebeční
deformaci	deformace	k1gFnSc4	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Dělali	dělat	k5eAaImAgMnP	dělat
to	ten	k3xDgNnSc4	ten
pomocí	pomocí	k7c2	pomocí
obmotání	obmotání	k1gNnSc2	obmotání
a	a	k8xC	a
utažení	utažení	k1gNnSc2	utažení
tenkého	tenký	k2eAgInSc2d1	tenký
pruhu	pruh	k1gInSc2	pruh
látky	látka	k1gFnSc2	látka
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
novorozeněte	novorozeně	k1gNnSc2	novorozeně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
více	hodně	k6eAd2	hodně
kuželovitého	kuželovitý	k2eAgInSc2d1	kuželovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Lebeční	lebeční	k2eAgFnSc1d1	lebeční
deformace	deformace	k1gFnSc1	deformace
byla	být	k5eAaImAgFnS	být
prováděna	provádět	k5eAaImNgFnS	provádět
kvůli	kvůli	k7c3	kvůli
odlišení	odlišení	k1gNnSc3	odlišení
sociálních	sociální	k2eAgFnPc2d1	sociální
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
prováděla	provádět	k5eAaImAgFnS	provádět
pouze	pouze	k6eAd1	pouze
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
prováděli	provádět	k5eAaImAgMnP	provádět
lidské	lidský	k2eAgFnPc4d1	lidská
oběti	oběť	k1gFnPc4	oběť
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
výhradně	výhradně	k6eAd1	výhradně
o	o	k7c4	o
dětská	dětský	k2eAgNnPc4d1	dětské
obětování	obětování	k1gNnPc4	obětování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k6eAd1	hlavně
během	běh	k1gInSc7	běh
nebo	nebo	k8xC	nebo
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
důležitých	důležitý	k2eAgFnPc2d1	důležitá
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
korunovace	korunovace	k1gFnSc2	korunovace
nebo	nebo	k8xC	nebo
úmrtí	úmrtí	k1gNnSc2	úmrtí
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
urozených	urozený	k2eAgFnPc2d1	urozená
rodin	rodina	k1gFnPc2	rodina
byly	být	k5eAaImAgFnP	být
omámeny	omámen	k2eAgFnPc1d1	omámena
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
udušeny	udušen	k2eAgInPc4d1	udušen
a	a	k8xC	a
pohřbeny	pohřben	k2eAgInPc4d1	pohřben
na	na	k7c6	na
posvátných	posvátný	k2eAgNnPc6d1	posvátné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
incké	incký	k2eAgFnSc6d1	incká
říši	říš	k1gFnSc6	říš
bylo	být	k5eAaImAgNnS	být
ayllu	ayllat	k5eAaPmIp1nS	ayllat
-	-	kIx~	-
vesnická	vesnický	k2eAgFnSc1d1	vesnická
občina	občina	k1gFnSc1	občina
<g/>
.	.	kIx.	.
</s>
<s>
Poddaní	poddaný	k1gMnPc1	poddaný
byli	být	k5eAaImAgMnP	být
organizováni	organizovat	k5eAaBmNgMnP	organizovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
desítkového	desítkový	k2eAgInSc2d1	desítkový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
úkolem	úkol	k1gInSc7	úkol
kuraků	kurak	k1gMnPc2	kurak
-	-	kIx~	-
starostů	starosta	k1gMnPc2	starosta
či	či	k8xC	či
předáků	předák	k1gMnPc2	předák
bylo	být	k5eAaImAgNnS	být
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
poddané	poddaná	k1gFnPc4	poddaná
<g/>
,	,	kIx,	,
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
soudní	soudní	k2eAgFnPc4d1	soudní
pře	pře	k1gFnPc4	pře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ručili	ručit	k5eAaImAgMnP	ručit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nikdo	nikdo	k3yNnSc1	nikdo
nestrádal	strádat	k5eNaImAgMnS	strádat
hladem	hlad	k1gMnSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
puric	puric	k1gMnSc1	puric
-	-	kIx~	-
občan	občan	k1gMnSc1	občan
incké	incký	k2eAgFnSc2d1	incká
říše	říš	k1gFnSc2	říš
měl	mít	k5eAaImAgMnS	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
jedné	jeden	k4xCgFnSc6	jeden
topo	topo	k1gMnSc1	topo
-	-	kIx~	-
tj.	tj.	kA	tj.
asi	asi	k9	asi
půl	půl	k1xP	půl
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
rodina	rodina	k1gFnSc1	rodina
dostávala	dostávat	k5eAaImAgFnS	dostávat
další	další	k2eAgNnSc4d1	další
topo	topo	k1gNnSc4	topo
<g/>
,	,	kIx,	,
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
dcery	dcera	k1gFnSc2	dcera
polovinu	polovina	k1gFnSc4	polovina
topo	topo	k6eAd1	topo
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
výživu	výživa	k1gFnSc4	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
přerozdělovala	přerozdělovat	k5eAaImAgFnS	přerozdělovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zdravý	zdravý	k2eAgMnSc1d1	zdravý
člověk	člověk	k1gMnSc1	člověk
nechal	nechat	k5eAaPmAgMnS	nechat
pole	pole	k1gNnSc4	pole
ležet	ležet	k5eAaImF	ležet
ladem	ladem	k6eAd1	ladem
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
a	a	k8xC	a
pole	pole	k1gFnSc1	pole
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
zabaveno	zabaven	k2eAgNnSc4d1	zabaveno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
dělili	dělit	k5eAaImAgMnP	dělit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
věkových	věkový	k2eAgFnPc2d1	věková
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
k	k	k7c3	k
desáté	desátá	k1gFnSc3	desátá
třídě	třída	k1gFnSc3	třída
patřili	patřit	k5eAaImAgMnP	patřit
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
handicapem	handicap	k1gInSc7	handicap
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
postižení	postižený	k2eAgMnPc1d1	postižený
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
starci	stařec	k1gMnPc1	stařec
nad	nad	k7c4	nad
80	[number]	k4	80
let	léto	k1gNnPc2	léto
nemuseli	muset	k5eNaImAgMnP	muset
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ostatním	ostatní	k1gNnSc7	ostatní
se	se	k3xPyFc4	se
přidělovaly	přidělovat	k5eAaImAgInP	přidělovat
úkoly	úkol	k1gInPc1	úkol
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
fyzických	fyzický	k2eAgFnPc2d1	fyzická
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
60-80	[number]	k4	60-80
let	léto	k1gNnPc2	léto
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zaučovat	zaučovat	k5eAaImF	zaučovat
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dospívající	dospívající	k2eAgInPc4d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
také	také	k9	také
prováděli	provádět	k5eAaImAgMnP	provádět
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgFnPc4d1	náročná
práce	práce	k1gFnPc4	práce
jako	jako	k8xC	jako
předení	předení	k1gNnSc4	předení
<g/>
,	,	kIx,	,
sběr	sběr	k1gInSc4	sběr
klasů	klas	k1gInPc2	klas
a	a	k8xC	a
ovoce	ovoce	k1gNnSc4	ovoce
nebo	nebo	k8xC	nebo
chov	chov	k1gInSc4	chov
morčat	morče	k1gNnPc2	morče
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
třetiny	třetina	k1gFnPc4	třetina
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
patřila	patřit	k5eAaImAgFnS	patřit
vládci	vládce	k1gMnSc3	vládce
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
dvoru	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
chrámům	chrám	k1gInPc3	chrám
a	a	k8xC	a
kněžstvu	kněžstvo	k1gNnSc3	kněžstvo
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc4	třetí
část	část	k1gFnSc4	část
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rolník	rolník	k1gMnSc1	rolník
měl	mít	k5eAaImAgMnS	mít
právo	právo	k1gNnSc4	právo
si	se	k3xPyFc3	se
ponechat	ponechat	k5eAaPmF	ponechat
třetinu	třetina	k1gFnSc4	třetina
výnosů	výnos	k1gInPc2	výnos
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
třetinu	třetina	k1gFnSc4	třetina
odevzdával	odevzdávat	k5eAaImAgMnS	odevzdávat
státu	stát	k1gInSc2	stát
a	a	k8xC	a
třetí	třetí	k4xOgFnSc2	třetí
chrámům	chrám	k1gInPc3	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Naturální	naturální	k2eAgFnSc4d1	naturální
daň	daň	k1gFnSc4	daň
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
nahradit	nahradit	k5eAaPmF	nahradit
pracemi	práce	k1gFnPc7	práce
na	na	k7c6	na
státních	státní	k2eAgInPc6d1	státní
či	či	k8xC	či
chrámových	chrámový	k2eAgInPc6d1	chrámový
pozemcích	pozemek	k1gInPc6	pozemek
<g/>
,	,	kIx,	,
stavbě	stavba	k1gFnSc3	stavba
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
hradeb	hradba	k1gFnPc2	hradba
či	či	k8xC	či
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Kukuřice	kukuřice	k1gFnPc1	kukuřice
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
komodity	komodita	k1gFnPc1	komodita
z	z	k7c2	z
třetiny	třetina	k1gFnSc2	třetina
patřící	patřící	k2eAgFnSc1d1	patřící
vládci	vládce	k1gMnPc7	vládce
(	(	kIx(	(
<g/>
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
sloužily	sloužit	k5eAaImAgFnP	sloužit
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
výživě	výživa	k1gFnSc3	výživa
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
dvořanů	dvořan	k1gMnPc2	dvořan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
ukládaly	ukládat	k5eAaImAgFnP	ukládat
do	do	k7c2	do
státních	státní	k2eAgNnPc2d1	státní
skladišť	skladiště	k1gNnPc2	skladiště
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hladu	hlad	k1gInSc2	hlad
a	a	k8xC	a
neúrody	neúroda	k1gFnPc4	neúroda
zdarma	zdarma	k6eAd1	zdarma
vydávány	vydáván	k2eAgFnPc4d1	vydávána
potřebným	potřebný	k2eAgFnPc3d1	potřebná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
evidenci	evidence	k1gFnSc3	evidence
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
sčítání	sčítání	k1gNnSc4	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
předávání	předávání	k1gNnSc2	předávání
zpráv	zpráva	k1gFnPc2	zpráva
sloužil	sloužit	k5eAaImAgMnS	sloužit
systém	systém	k1gInSc4	systém
kipu	kipus	k1gInSc2	kipus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Incký	incký	k2eAgInSc1d1	incký
morální	morální	k2eAgInSc1d1	morální
kodex	kodex	k1gInSc1	kodex
zněl	znět	k5eAaImAgInS	znět
<g/>
:	:	kIx,	:
-	-	kIx~	-
ama	ama	k?	ama
suwa	suwa	k1gMnSc1	suwa
<g/>
,	,	kIx,	,
ama	ama	k?	ama
llulla	llulla	k1gMnSc1	llulla
<g/>
,	,	kIx,	,
ama	ama	k?	ama
quella	quello	k1gNnSc2	quello
(	(	kIx(	(
<g/>
nekraď	krást	k5eNaImRp2nS	krást
<g/>
,	,	kIx,	,
nelži	lhát	k5eNaImRp2nS	lhát
<g/>
,	,	kIx,	,
nebuď	budit	k5eNaImRp2nS	budit
líný	líný	k2eAgMnSc1d1	líný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krádež	krádež	k1gFnSc1	krádež
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
vražda	vražda	k1gFnSc1	vražda
a	a	k8xC	a
zrada	zrada	k1gFnSc1	zrada
<g/>
,	,	kIx,	,
trestala	trestat	k5eAaImAgFnS	trestat
většinou	většinou	k6eAd1	většinou
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopadený	dopadený	k2eAgMnSc1d1	dopadený
zloděj	zloděj	k1gMnSc1	zloděj
kradl	krást	k5eAaImAgMnS	krást
z	z	k7c2	z
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
místo	místo	k1gNnSc4	místo
něj	on	k3xPp3gMnSc4	on
popraven	popraven	k2eAgMnSc1d1	popraven
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnSc2	jehož
správy	správa	k1gFnSc2	správa
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Popravovalo	popravovat	k5eAaImAgNnS	popravovat
se	se	k3xPyFc4	se
shozením	shození	k1gNnSc7	shození
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
kamenováním	kamenování	k1gNnSc7	kamenování
nebo	nebo	k8xC	nebo
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
existovaly	existovat	k5eAaImAgFnP	existovat
i	i	k9	i
jakési	jakýsi	k3yIgFnSc2	jakýsi
věznice	věznice	k1gFnSc2	věznice
nebo	nebo	k8xC	nebo
mučírny	mučírna	k1gFnSc2	mučírna
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
žila	žít	k5eAaImAgNnP	žít
divoká	divoký	k2eAgNnPc1d1	divoké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
jedovatí	jedovatět	k5eAaImIp3nP	jedovatět
hadi	had	k1gMnPc1	had
a	a	k8xC	a
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
odsouzený	odsouzený	k1gMnSc1	odsouzený
přežil	přežít	k5eAaPmAgMnS	přežít
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
zařízení	zařízení	k1gNnSc6	zařízení
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
omilostněn	omilostnit	k5eAaPmNgMnS	omilostnit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
méně	málo	k6eAd2	málo
závažné	závažný	k2eAgInPc4d1	závažný
zločiny	zločin	k1gInPc4	zločin
se	s	k7c7	s
provinilci	provinilec	k1gMnPc7	provinilec
trestali	trestat	k5eAaImAgMnP	trestat
bitím	bití	k1gNnSc7	bití
<g/>
,	,	kIx,	,
nucenými	nucený	k2eAgFnPc7d1	nucená
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
zabavením	zabavení	k1gNnSc7	zabavení
majetku	majetek	k1gInSc2	majetek
nebo	nebo	k8xC	nebo
deportací	deportace	k1gFnPc2	deportace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Případě	případ	k1gInSc6	případ
vzpoury	vzpoura	k1gFnSc2	vzpoura
byli	být	k5eAaImAgMnP	být
deportováni	deportován	k2eAgMnPc1d1	deportován
obyvatelé	obyvatel	k1gMnPc1	obyvatel
celých	celý	k2eAgFnPc2d1	celá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pověsti	pověst	k1gFnPc4	pověst
a	a	k8xC	a
báje	báj	k1gFnPc4	báj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
===	===	k?	===
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dávno	dávno	k6eAd1	dávno
se	se	k3xPyFc4	se
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Titicaca	Titicacum	k1gNnSc2	Titicacum
objevil	objevit	k5eAaPmAgMnS	objevit
vysoký	vysoký	k2eAgMnSc1d1	vysoký
vousatý	vousatý	k2eAgMnSc1d1	vousatý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
berlou	berla	k1gFnSc7	berla
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
bůh	bůh	k1gMnSc1	bůh
Viracocha	Viracoch	k1gMnSc2	Viracoch
-	-	kIx~	-
Stvořitel	Stvořitel	k1gMnSc1	Stvořitel
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Titicaca	Titicac	k1gInSc2	Titicac
uprostřed	uprostřed	k7c2	uprostřed
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
tam	tam	k6eAd1	tam
přikázal	přikázat	k5eAaPmAgMnS	přikázat
slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
měsíci	měsíc	k1gInSc3	měsíc
a	a	k8xC	a
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyšly	vyjít	k5eAaPmAgFnP	vyjít
<g/>
,	,	kIx,	,
a	a	k8xC	a
ony	onen	k3xDgFnPc1	onen
vyšly	vyjít	k5eAaPmAgFnP	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
vymodeloval	vymodelovat	k5eAaPmAgMnS	vymodelovat
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tak	tak	k6eAd1	tak
praotce	praotec	k1gMnPc4	praotec
a	a	k8xC	a
pramatky	pramatka	k1gFnPc4	pramatka
všech	všecek	k3xTgInPc2	všecek
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
páru	pár	k1gInSc3	pár
pak	pak	k6eAd1	pak
dal	dát	k5eAaPmAgMnS	dát
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
budou	být	k5eAaImBp3nP	být
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
budou	být	k5eAaImBp3nP	být
zpívat	zpívat	k5eAaImF	zpívat
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
budou	být	k5eAaImBp3nP	být
sít	sít	k5eAaImF	sít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
budou	být	k5eAaImBp3nP	být
tančit	tančit	k5eAaImF	tančit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
vdechl	vdechnout	k5eAaPmAgMnS	vdechnout
život	život	k1gInSc4	život
a	a	k8xC	a
přikázal	přikázat	k5eAaPmAgMnS	přikázat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
a	a	k8xC	a
vyšli	vyjít	k5eAaPmAgMnP	vyjít
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
každý	každý	k3xTgMnSc1	každý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
oni	onen	k3xDgMnPc1	onen
vyšli	vyjít	k5eAaPmAgMnP	vyjít
z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
z	z	k7c2	z
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgNnPc1	onen
místa	místo	k1gNnPc1	místo
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pakarina	pakarin	k2eAgNnPc1d1	pakarin
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
místa	místo	k1gNnSc2	místo
úsvitu	úsvit	k1gInSc2	úsvit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
potopě	potopa	k1gFnSc6	potopa
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
lety	let	k1gInPc7	let
žil	žít	k5eAaImAgMnS	žít
jeden	jeden	k4xCgMnSc1	jeden
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přehlédl	přehlédnout	k5eAaPmAgMnS	přehlédnout
svá	svůj	k3xOyFgNnPc4	svůj
stáda	stádo	k1gNnPc4	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvířata	zvíře	k1gNnPc1	zvíře
ani	ani	k8xC	ani
nežerou	žrát	k5eNaImIp3nP	žrát
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nenapájí	napájet	k5eNaImIp3nS	napájet
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
jen	jen	k9	jen
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
dívají	dívat	k5eAaImIp3nP	dívat
se	se	k3xPyFc4	se
na	na	k7c4	na
oblohu	obloha	k1gFnSc4	obloha
a	a	k8xC	a
naříkají	naříkat	k5eAaBmIp3nP	naříkat
<g/>
.	.	kIx.	.
</s>
<s>
Pastýř	pastýř	k1gMnSc1	pastýř
se	se	k3xPyFc4	se
rozzlobil	rozzlobit	k5eAaPmAgMnS	rozzlobit
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
stalo	stát	k5eAaPmAgNnS	stát
<g/>
?	?	kIx.	?
</s>
<s>
Máte	mít	k5eAaImIp2nP	mít
ode	ode	k7c2	ode
mne	já	k3xPp1nSc2	já
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
pastvu	pastva	k1gFnSc4	pastva
a	a	k8xC	a
nejčistší	čistý	k2eAgFnSc4d3	nejčistší
vodu	voda	k1gFnSc4	voda
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
vy	vy	k3xPp2nPc1	vy
tu	tu	k6eAd1	tu
jen	jen	k6eAd1	jen
stojíte	stát	k5eAaImIp2nP	stát
<g/>
,	,	kIx,	,
hledíte	hledět	k5eAaImIp2nP	hledět
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
a	a	k8xC	a
naříkáte	naříkat	k5eAaBmIp2nP	naříkat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
obrátila	obrátit	k5eAaPmAgFnS	obrátit
lama	lama	k1gFnSc1	lama
a	a	k8xC	a
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Dobře	dobře	k6eAd1	dobře
mne	já	k3xPp1nSc2	já
poslouchej	poslouchat	k5eAaImRp2nS	poslouchat
a	a	k8xC	a
vyslechni	vyslechnout	k5eAaPmRp2nS	vyslechnout
co	co	k3yInSc1	co
Ti	ty	k3xPp2nSc3	ty
povím	povědět	k5eAaPmIp1nS	povědět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Pastýř	pastýř	k1gMnSc1	pastýř
zůstal	zůstat	k5eAaPmAgMnS	zůstat
stát	stát	k5eAaPmF	stát
jako	jako	k9	jako
uhranutý	uhranutý	k2eAgMnSc1d1	uhranutý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
znělo	znět	k5eAaImAgNnS	znět
úplně	úplně	k6eAd1	úplně
jako	jako	k9	jako
by	by	kYmCp3nS	by
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
mluvil	mluvit	k5eAaImAgMnS	mluvit
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
lama	lama	k1gFnSc1	lama
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Vidíš	vidět	k5eAaImIp2nS	vidět
ty	ten	k3xDgFnPc4	ten
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vycházejí	vycházet	k5eAaImIp3nP	vycházet
nad	nad	k7c7	nad
horou	hora	k1gFnSc7	hora
Jokakoto	Jokakota	k1gFnSc5	Jokakota
<g/>
?	?	kIx.	?
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
hvězdy	hvězda	k1gFnPc1	hvězda
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přesně	přesně	k6eAd1	přesně
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
zničí	zničit	k5eAaPmIp3nS	zničit
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
potopa	potopa	k1gFnSc1	potopa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pastýř	pastýř	k1gMnSc1	pastýř
jí	jíst	k5eAaImIp3nS	jíst
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
a	a	k8xC	a
vzal	vzít	k5eAaPmAgInS	vzít
celou	celý	k2eAgFnSc4d1	celá
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
začalo	začít	k5eAaPmAgNnS	začít
pršet	pršet	k5eAaImF	pršet
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pršelo	pršet	k5eAaImAgNnS	pršet
a	a	k8xC	a
pršelo	pršet	k5eAaImAgNnS	pršet
<g/>
,	,	kIx,	,
až	až	k8xS	až
jediným	jediný	k2eAgNnSc7d1	jediné
nezatopeným	zatopený	k2eNgNnSc7d1	zatopený
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
zůstal	zůstat	k5eAaPmAgMnS	zůstat
vrcholek	vrcholek	k1gInSc4	vrcholek
oné	onen	k3xDgFnSc2	onen
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
tak	tak	k9	tak
málo	málo	k4c4	málo
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
že	že	k8xS	že
liška	liška	k1gFnSc1	liška
uklouzla	uklouznout	k5eAaPmAgFnS	uklouznout
a	a	k8xC	a
namočila	namočit	k5eAaPmAgFnS	namočit
si	se	k3xPyFc3	se
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
dneska	dneska	k?	dneska
každá	každý	k3xTgFnSc1	každý
liška	liška	k1gFnSc1	liška
ocas	ocas	k1gInSc4	ocas
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odchod	odchod	k1gInSc1	odchod
Virakoči	Virakoč	k1gFnSc3	Virakoč
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
bůh	bůh	k1gMnSc1	bůh
Virakoča	Virakoča	k1gMnSc1	Virakoča
starý	starý	k2eAgMnSc1d1	starý
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
mužem	muž	k1gMnSc7	muž
jménem	jméno	k1gNnSc7	jméno
Apo	Apo	k1gMnSc5	Apo
Tambo	Tamba	k1gMnSc5	Tamba
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
Manco	Manco	k1gMnSc1	Manco
Capaca	Capaca	k1gMnSc1	Capaca
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc2	první
mytického	mytický	k2eAgMnSc2d1	mytický
vládce	vládce	k1gMnSc2	vládce
Inků	Ink	k1gMnPc2	Ink
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
připravený	připravený	k2eAgMnSc1d1	připravený
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Předal	předat	k5eAaPmAgMnS	předat
kus	kus	k1gInSc4	kus
své	svůj	k3xOyFgFnSc2	svůj
berly	berla	k1gFnSc2	berla
Manco	Manco	k6eAd1	Manco
Capacovi	Capacův	k2eAgMnPc1d1	Capacův
a	a	k8xC	a
pak	pak	k6eAd1	pak
opustil	opustit	k5eAaPmAgInS	opustit
svět	svět	k1gInSc1	svět
řekou	řeka	k1gFnSc7	řeka
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Chapa	Chap	k1gMnSc4	Chap
Maca	Mac	k2eAgFnSc1d1	Maca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
Tiahuanacu	Tiahuanacus	k1gInSc6	Tiahuanacus
I.	I.	kA	I.
===	===	k?	===
</s>
</p>
<p>
<s>
Jednou	jednou	k6eAd1	jednou
Inka	Inka	k1gFnSc1	Inka
Mayta	Mayta	k1gFnSc1	Mayta
Capac	Capac	k1gFnSc1	Capac
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
pověsti	pověst	k1gFnSc2	pověst
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
Sinchi	Sinchi	k1gNnSc4	Sinchi
Roca	Rocum	k1gNnSc2	Rocum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpočíval	odpočívat	k5eAaImAgInS	odpočívat
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
opuštěného	opuštěný	k2eAgNnSc2d1	opuštěné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
dostihl	dostihnout	k5eAaPmAgMnS	dostihnout
velice	velice	k6eAd1	velice
rychlý	rychlý	k2eAgMnSc1d1	rychlý
běžec	běžec	k1gMnSc1	běžec
a	a	k8xC	a
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
uřícený	uřícený	k2eAgMnSc1d1	uřícený
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
vládci	vládce	k1gMnSc3	vládce
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
zprávu	zpráva	k1gFnSc4	zpráva
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
říše	říš	k1gFnSc2	říš
Cuzca	Cuzc	k1gInSc2	Cuzc
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
ocenil	ocenit	k5eAaPmAgMnS	ocenit
jeho	jeho	k3xOp3gFnSc4	jeho
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
svého	svůj	k3xOyFgMnSc4	svůj
posla	posel	k1gMnSc4	posel
ke	k	k7c3	k
guanaku	guanak	k1gInSc3	guanak
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
lamy	lama	k1gFnSc2	lama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
ho	on	k3xPp3gMnSc4	on
nebývalým	bývalý	k2eNgInSc7d1	bývalý
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
vybídl	vybídnout	k5eAaPmAgInS	vybídnout
běžce	běžec	k1gMnPc4	běžec
<g/>
,	,	kIx,	,
neurozeného	urozený	k2eNgMnSc4d1	neurozený
příslušníka	příslušník	k1gMnSc4	příslušník
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
posadil	posadit	k5eAaPmAgMnS	posadit
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgInS	říct
mu	on	k3xPp3gNnSc3	on
tedy	tedy	k9	tedy
kečuánsky	kečuánsky	k6eAd1	kečuánsky
<g/>
:	:	kIx,	:
Tia	Tia	k1gMnSc1	Tia
Huanaco	Huanaco	k1gMnSc1	Huanaco
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Usedni	usednout	k5eAaPmRp2nS	usednout
<g/>
,	,	kIx,	,
běžče	běžec	k1gMnSc5	běžec
rychlý	rychlý	k2eAgInSc1d1	rychlý
jako	jako	k9	jako
guanako	guanako	k6eAd1	guanako
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
toto	tento	k3xDgNnSc4	tento
kečuánské	kečuánský	k2eAgNnSc4d1	kečuánský
jméno	jméno	k1gNnSc4	jméno
už	už	k6eAd1	už
městu	město	k1gNnSc3	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
Tiahuanacu	Tiahuanacus	k1gInSc6	Tiahuanacus
II	II	kA	II
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
věčné	věčný	k2eAgFnSc2d1	věčná
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
vod	voda	k1gFnPc2	voda
jezera	jezero	k1gNnSc2	jezero
Titicaca	Titicaca	k1gMnSc1	Titicaca
bůh	bůh	k1gMnSc1	bůh
Con	Con	k1gFnSc4	Con
Ticci	Ticce	k1gFnSc4	Ticce
Viracocha	Viracoch	k1gMnSc2	Viracoch
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Tiahuanacu	Tiahuanacus	k1gInSc6	Tiahuanacus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
zdech	zeď	k1gFnPc6	zeď
stvořil	stvořit	k5eAaPmAgInS	stvořit
Slunce	slunce	k1gNnSc4	slunce
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kráčelo	kráčet	k5eAaImAgNnS	kráčet
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
putuje	putovat	k5eAaImIp3nS	putovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tiahuanacu	Tiahuanacus	k1gInSc6	Tiahuanacus
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Viracocha	Viracoch	k1gMnSc4	Viracoch
i	i	k9	i
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
všechna	všechen	k3xTgNnPc1	všechen
ostatní	ostatní	k2eAgNnPc1d1	ostatní
nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
stvořil	stvořit	k5eAaPmAgInS	stvořit
–	–	k?	–
samozřejmě	samozřejmě	k6eAd1	samozřejmě
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
–	–	k?	–
modely	model	k1gInPc4	model
mužů	muž	k1gMnPc2	muž
i	i	k8xC	i
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
rozeslal	rozeslat	k5eAaPmAgMnS	rozeslat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
párech	pár	k1gInPc6	pár
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
určil	určit	k5eAaPmAgInS	určit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
prameni	pramen	k1gInSc6	pramen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
peruánské	peruánský	k2eAgFnSc6d1	peruánská
řece	řeka	k1gFnSc6	řeka
či	či	k8xC	či
z	z	k7c2	z
které	který	k3yIgFnSc2	který
skály	skála	k1gFnSc2	skála
mají	mít	k5eAaImIp3nP	mít
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
návštěvník	návštěvník	k1gMnSc1	návštěvník
Peru	prát	k5eAaImIp1nS	prát
může	moct	k5eAaImIp3nS	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
mnohými	mnohý	k2eAgFnPc7d1	mnohá
památkami	památka	k1gFnPc7	památka
na	na	k7c4	na
inckou	incký	k2eAgFnSc4d1	incká
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
zřícenin	zřícenina	k1gFnPc2	zřícenina
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
velkých	velký	k2eAgFnPc2d1	velká
vojenských	vojenský	k2eAgFnPc2d1	vojenská
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
akvaduktů	akvadukt	k1gInPc2	akvadukt
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
veřejných	veřejný	k2eAgFnPc2d1	veřejná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
své	svůj	k3xOyFgFnPc4	svůj
stavby	stavba	k1gFnPc4	stavba
stavěli	stavět	k5eAaImAgMnP	stavět
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
malty	malta	k1gFnSc2	malta
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgInPc1d1	kamenný
bloky	blok	k1gInPc1	blok
byly	být	k5eAaImAgInP	být
opracovány	opracovat	k5eAaPmNgInP	opracovat
a	a	k8xC	a
dopraveny	dopravit	k5eAaPmNgInP	dopravit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
určení	určení	k1gNnSc2	určení
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
skládány	skládat	k5eAaImNgInP	skládat
tak	tak	k6eAd1	tak
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
nevešla	vejít	k5eNaPmAgFnS	vejít
čepel	čepel	k1gFnSc1	čepel
nože	nůž	k1gInSc2	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Střechy	střecha	k1gFnPc1	střecha
byly	být	k5eAaImAgFnP	být
dělány	dělat	k5eAaImNgFnP	dělat
většinou	většinou	k6eAd1	většinou
ze	z	k7c2	z
slámových	slámový	k2eAgInPc2d1	slámový
došků	došek	k1gInPc2	došek
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
byly	být	k5eAaImAgInP	být
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Španělů	Španěl	k1gMnPc2	Španěl
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
pálit	pálit	k5eAaImF	pálit
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
nález	nález	k1gInSc1	nález
Hirama	Hiram	k1gMnSc2	Hiram
Binghama	Bingham	k1gMnSc2	Bingham
na	na	k7c4	na
Macchu	Maccha	k1gFnSc4	Maccha
Picchu	Picch	k1gInSc2	Picch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnPc4d3	nejznámější
incká	incký	k2eAgNnPc4d1	incké
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nS	patřit
Machu	macha	k1gFnSc4	macha
Picchu	Picch	k1gInSc2	Picch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
Machu	macha	k1gFnSc4	macha
Picchu	Picch	k1gInSc2	Picch
panovník	panovník	k1gMnSc1	panovník
Pachacuti	Pachacuť	k1gFnSc2	Pachacuť
jako	jako	k8xC	jako
své	svůj	k3xOyFgFnSc2	svůj
odpočinkové	odpočinkový	k2eAgFnSc2d1	odpočinková
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proslaveno	proslaven	k2eAgNnSc1d1	proslaveno
díky	díky	k7c3	díky
Američanovi	Američan	k1gMnSc3	Američan
Hiramu	Hiram	k1gInSc2	Hiram
Binghamovi	Bingham	k1gMnSc3	Bingham
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
původně	původně	k6eAd1	původně
hledal	hledat	k5eAaImAgMnS	hledat
rebelské	rebelský	k2eAgNnSc4d1	rebelské
město	město	k1gNnSc4	město
Vilcabamba	Vilcabamb	k1gMnSc2	Vilcabamb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silnice	silnice	k1gFnPc1	silnice
===	===	k?	===
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpozoruhodnějších	pozoruhodný	k2eAgFnPc2d3	nejpozoruhodnější
inckých	incký	k2eAgFnPc2d1	incká
staveb	stavba	k1gFnPc2	stavba
jsou	být	k5eAaImIp3nP	být
určitě	určitě	k6eAd1	určitě
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
jejich	jejich	k3xOp3gInPc1	jejich
zbytky	zbytek	k1gInPc1	zbytek
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
jejich	jejich	k3xOp3gFnSc4	jejich
dokonalost	dokonalost	k1gFnSc4	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
silnic	silnice	k1gFnPc2	silnice
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
křižovaly	křižovat	k5eAaImAgFnP	křižovat
různé	různý	k2eAgFnPc1d1	různá
končiny	končina	k1gFnPc1	končina
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
silnic	silnice	k1gFnPc2	silnice
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
silnice	silnice	k1gFnPc1	silnice
však	však	k9	však
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Quita	Quit	k1gInSc2	Quit
do	do	k7c2	do
Cuzka	Cuzek	k1gMnSc2	Cuzek
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
táhla	táhnout	k5eAaImAgFnS	táhnout
z	z	k7c2	z
Cuzca	Cuzc	k1gInSc2	Cuzc
na	na	k7c4	na
jih	jih	k1gInSc4	jih
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Chile	Chile	k1gNnSc3	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
silnice	silnice	k1gFnSc1	silnice
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
náhorní	náhorní	k2eAgFnSc4d1	náhorní
rovinu	rovina	k1gFnSc4	rovina
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
zas	zas	k6eAd1	zas
nížinou	nížina	k1gFnSc7	nížina
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
silnice	silnice	k1gFnSc2	silnice
přes	přes	k7c4	přes
náhorní	náhorní	k2eAgFnSc4d1	náhorní
rovinu	rovina	k1gFnSc4	rovina
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
rázu	ráz	k1gInSc3	ráz
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Ležel	ležet	k5eAaImAgInS	ležet
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
úseky	úsek	k1gInPc4	úsek
vytesány	vytesán	k2eAgInPc4d1	vytesán
do	do	k7c2	do
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
řeky	řeka	k1gFnPc4	řeka
vedly	vést	k5eAaImAgFnP	vést
visuté	visutý	k2eAgInPc4d1	visutý
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
do	do	k7c2	do
srázů	sráz	k1gInPc2	sráz
propastí	propast	k1gFnPc2	propast
byla	být	k5eAaImAgNnP	být
vytesána	vytesán	k2eAgNnPc1d1	vytesáno
skalní	skalní	k2eAgNnPc1d1	skalní
schodiště	schodiště	k1gNnPc1	schodiště
a	a	k8xC	a
horské	horský	k2eAgInPc1d1	horský
průrvy	průrvy	k?	průrvy
byly	být	k5eAaImAgInP	být
vyplněny	vyplnit	k5eAaPmNgInP	vyplnit
zdivem	zdivo	k1gNnSc7	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Šíře	šíře	k1gFnSc1	šíře
silnic	silnice	k1gFnPc2	silnice
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c4	mezi
půl	půl	k1xP	půl
metrem	metro	k1gNnSc7	metro
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
šesti	šest	k4xCc7	šest
metry	metr	k1gInPc7	metr
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
silnicích	silnice	k1gFnPc6	silnice
se	se	k3xPyFc4	se
jako	jako	k9	jako
poslové	posel	k1gMnPc1	posel
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
rychlí	rychlý	k2eAgMnPc1d1	rychlý
běžci	běžec	k1gMnPc1	běžec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
chasquis	chasquis	k1gFnPc7	chasquis
<g/>
.	.	kIx.	.
</s>
<s>
Běhali	běhat	k5eAaImAgMnP	běhat
podle	podle	k7c2	podle
štafetového	štafetový	k2eAgInSc2d1	štafetový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
úsek	úsek	k1gInSc4	úsek
cesty	cesta	k1gFnSc2	cesta
byli	být	k5eAaImAgMnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dva	dva	k4xCgMnPc1	dva
odpočinutí	odpočinutý	k2eAgMnPc1d1	odpočinutý
běžci	běžec	k1gMnPc1	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Předávali	předávat	k5eAaImAgMnP	předávat
zprávy	zpráva	k1gFnPc4	zpráva
na	na	k7c6	na
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
tambos	tambosa	k1gFnPc2	tambosa
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
předávat	předávat	k5eAaImF	předávat
poselství	poselství	k1gNnSc4	poselství
nebo	nebo	k8xC	nebo
zboží	zboží	k1gNnSc4	zboží
až	až	k9	až
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
400	[number]	k4	400
km	km	kA	km
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
nevyužívali	využívat	k5eNaImAgMnP	využívat
ani	ani	k8xC	ani
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gFnPc4	on
vůbec	vůbec	k9	vůbec
neznali	neznat	k5eAaImAgMnP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Používali	používat	k5eAaImAgMnP	používat
jedině	jedině	k6eAd1	jedině
lamy	lama	k1gFnPc4	lama
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
chodili	chodit	k5eAaImAgMnP	chodit
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Divy	div	k1gInPc1	div
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-242-0772-9	[number]	k4	80-242-0772-9
</s>
</p>
<p>
<s>
BOROVEC	BOROVEC	kA	BOROVEC
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Osudný	osudný	k2eAgInSc4d1	osudný
den	den	k1gInSc4	den
říše	říš	k1gFnSc2	říš
Inků	Ink	k1gMnPc2	Ink
<g/>
:	:	kIx,	:
začátek	začátek	k1gInSc4	začátek
španělské	španělský	k2eAgFnSc2d1	španělská
conquisty	conquista	k1gFnSc2	conquista
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
23	[number]	k4	23
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KŘÍŽOVÁ	Křížová	k1gFnSc1	Křížová
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
Inkové	Ink	k1gMnPc1	Ink
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Skřivan	Skřivan	k1gMnSc1	Skřivan
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86493-21-0	[number]	k4	80-86493-21-0
</s>
</p>
<p>
<s>
PRESCOTT	PRESCOTT	kA	PRESCOTT
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Hickling	Hickling	k1gInSc1	Hickling
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
dobytí	dobytí	k1gNnSc2	dobytí
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROEDL	ROEDL	kA	ROEDL
<g/>
,	,	kIx,	,
Bohumír	Bohumír	k1gMnSc1	Bohumír
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
o	o	k7c6	o
conquistě	conquista	k1gFnSc6	conquista
a	a	k8xC	a
prvních	první	k4xOgNnPc6	první
kronikářích	kronikář	k1gMnPc6	kronikář
Peru	prát	k5eAaImIp1nS	prát
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
22	[number]	k4	22
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
inckých	incký	k2eAgMnPc2d1	incký
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Incká	incký	k2eAgFnSc1d1	incká
říše	říše	k1gFnSc1	říše
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Inca	Inca	k1gFnSc1	Inca
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Inca	Inca	k1gFnSc1	Inca
Empire	empir	k1gInSc5	empir
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Inka	Inka	k1gFnSc1	Inka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Inkové	Ink	k1gMnPc1	Ink
–	–	k?	–
soubor	soubor	k1gInSc4	soubor
článků	článek	k1gInPc2	článek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mystika	mystik	k1gMnSc2	mystik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Inkové	Ink	k1gMnPc1	Ink
se	se	k3xPyFc4	se
nepřejídali	přejídat	k5eNaImAgMnP	přejídat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dost	dost	k6eAd1	dost
pili	pít	k5eAaImAgMnP	pít
<g/>
.	.	kIx.	.
iDnes	iDnes	k1gInSc4	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2001-06-12	[number]	k4	2001-06-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
silnice	silnice	k1gFnPc4	silnice
z	z	k7c2	z
říše	říš	k1gFnSc2	říš
Inků	Ink	k1gMnPc2	Ink
<g/>
.	.	kIx.	.
iDnes	iDnes	k1gMnSc1	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2001-09-07	[number]	k4	2001-09-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
