<s>
Přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
stráně	stráň	k1gFnSc2	stráň
je	být	k5eAaImIp3nS	být
přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Rejhotice	Rejhotice	k1gFnSc2	Rejhotice
obce	obec	k1gFnSc2	obec
Loučná	Loučný	k2eAgFnSc1d1	Loučná
nad	nad	k7c7	nad
Desnou	Desna	k1gFnSc7	Desna
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
CHKO	CHKO	kA	CHKO
Jeseníky	Jeseník	k1gInPc1	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvýkonnější	výkonný	k2eAgFnSc4d3	nejvýkonnější
vodní	vodní	k2eAgFnSc4d1	vodní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
její	její	k3xOp3gInSc4	její
instalovaný	instalovaný	k2eAgInSc4d1	instalovaný
výkon	výkon	k1gInSc4	výkon
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
×	×	k?	×
325	[number]	k4	325
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
elektrárny	elektrárna	k1gFnSc2	elektrárna
je	být	k5eAaImIp3nS	být
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
stabilitu	stabilita	k1gFnSc4	stabilita
elektrizační	elektrizační	k2eAgFnSc2d1	elektrizační
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
rovnováha	rovnováha	k1gFnSc1	rovnováha
mezi	mezi	k7c7	mezi
aktuální	aktuální	k2eAgFnSc7d1	aktuální
spotřebou	spotřeba	k1gFnSc7	spotřeba
a	a	k8xC	a
výkonem	výkon	k1gInSc7	výkon
dodávaným	dodávaný	k2eAgInSc7d1	dodávaný
energetickými	energetický	k2eAgInPc7d1	energetický
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
tak	tak	k6eAd1	tak
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
čerpadlového	čerpadlový	k2eAgMnSc2d1	čerpadlový
do	do	k7c2	do
turbínového	turbínový	k2eAgInSc2d1	turbínový
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
nástupu	nástup	k1gInSc2	nástup
větrných	větrný	k2eAgFnPc2d1	větrná
a	a	k8xC	a
slunečních	sluneční	k2eAgFnPc2d1	sluneční
elektráren	elektrárna	k1gFnPc2	elektrárna
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
nestabilita	nestabilita	k1gFnSc1	nestabilita
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proto	proto	k8xC	proto
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
elektrárna	elektrárna	k1gFnSc1	elektrárna
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
čerpadlového	čerpadlový	k2eAgInSc2d1	čerpadlový
režimu	režim	k1gInSc2	režim
i	i	k8xC	i
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
přecházela	přecházet	k5eAaImAgFnS	přecházet
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
režimu	režim	k1gInSc2	režim
nejčastěji	často	k6eAd3	často
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
PVE	PVE	kA	PVE
Dlouhé	Dlouhé	k2eAgFnPc4d1	Dlouhé
stráně	stráň	k1gFnPc4	stráň
disponuje	disponovat	k5eAaBmIp3nS	disponovat
nádržemi	nádrž	k1gFnPc7	nádrž
s	s	k7c7	s
výškovým	výškový	k2eAgInSc7d1	výškový
rozdílem	rozdíl	k1gInSc7	rozdíl
510,7	[number]	k4	510,7
m.	m.	k?	m.
Horní	horní	k2eAgFnSc1d1	horní
nádrž	nádrž	k1gFnSc1	nádrž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
hory	hora	k1gFnSc2	hora
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
stráně	stráň	k1gFnSc2	stráň
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1350	[number]	k4	1350
m	m	kA	m
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
provozní	provozní	k2eAgInSc4d1	provozní
objem	objem	k1gInSc4	objem
2	[number]	k4	2
580	[number]	k4	580
000	[number]	k4	000
m3	m3	k4	m3
(	(	kIx(	(
<g/>
celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
2	[number]	k4	2
719	[number]	k4	719
750	[number]	k4	750
m3	m3	k4	m3
<g/>
)	)	kIx)	)
a	a	k8xC	a
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
15,4	[number]	k4	15,4
ha	ha	kA	ha
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
hloubkou	hloubka	k1gFnSc7	hloubka
26	[number]	k4	26
m.	m.	k?	m.
Dolní	dolní	k2eAgFnSc1d1	dolní
nádrž	nádrž	k1gFnSc1	nádrž
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
říčce	říčka	k1gFnSc6	říčka
Divoká	divoký	k2eAgFnSc1d1	divoká
Desná	Desná	k1gFnSc1	Desná
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
824,7	[number]	k4	824,7
m	m	kA	m
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
3	[number]	k4	3
405	[number]	k4	405
000	[number]	k4	000
m3	m3	k4	m3
a	a	k8xC	a
plochu	plocha	k1gFnSc4	plocha
16,3	[number]	k4	16,3
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
načerpání	načerpání	k1gNnSc6	načerpání
horní	horní	k2eAgFnSc2d1	horní
nádrže	nádrž	k1gFnSc2	nádrž
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
její	její	k3xOp3gFnSc1	její
hladina	hladina	k1gFnSc1	hladina
o	o	k7c4	o
21,5	[number]	k4	21,5
m	m	kA	m
a	a	k8xC	a
hladina	hladina	k1gFnSc1	hladina
dolní	dolní	k2eAgFnSc2d1	dolní
nádrže	nádrž	k1gFnSc2	nádrž
klesne	klesnout	k5eAaPmIp3nS	klesnout
o	o	k7c4	o
22,2	[number]	k4	22,2
m.	m.	k?	m.
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
nádrž	nádrž	k1gFnSc1	nádrž
je	být	k5eAaImIp3nS	být
izolována	izolován	k2eAgFnSc1d1	izolována
18	[number]	k4	18
<g/>
cm	cm	kA	cm
vrstvou	vrstva	k1gFnSc7	vrstva
přírodního	přírodní	k2eAgInSc2d1	přírodní
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
dovezen	dovézt	k5eAaPmNgInS	dovézt
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
–	–	k?	–
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
potřeba	potřeba	k6eAd1	potřeba
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
vydrží	vydržet	k5eAaPmIp3nS	vydržet
rozsah	rozsah	k1gInSc4	rozsah
teplot	teplota	k1gFnPc2	teplota
od	od	k7c2	od
−	−	k?	−
do	do	k7c2	do
+60	+60	k4	+60
°	°	k?	°
<g/>
C.	C.	kA	C.
Vedle	vedle	k7c2	vedle
horní	horní	k2eAgFnSc2d1	horní
nádrže	nádrž	k1gFnSc2	nádrž
stojí	stát	k5eAaImIp3nS	stát
malá	malý	k2eAgFnSc1d1	malá
mohyla	mohyla	k1gFnSc1	mohyla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
původní	původní	k2eAgFnSc4d1	původní
výšku	výška	k1gFnSc4	výška
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jinak	jinak	k6eAd1	jinak
seříznuta	seříznout	k5eAaPmNgFnS	seříznout
asi	asi	k9	asi
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
areál	areál	k1gInSc1	areál
elektrárny	elektrárna	k1gFnSc2	elektrárna
nachází	nacházet	k5eAaImIp3nS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ekologických	ekologický	k2eAgInPc2d1	ekologický
důvodů	důvod	k1gInPc2	důvod
celý	celý	k2eAgInSc4d1	celý
provoz	provoz	k1gInSc4	provoz
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kaverně	kaverna	k1gFnSc6	kaverna
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
87,5	[number]	k4	87,5
×	×	k?	×
25,5	[number]	k4	25,5
×	×	k?	×
50	[number]	k4	50
m	m	kA	m
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
dvě	dva	k4xCgFnPc1	dva
24	[number]	k4	24
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgNnPc4d1	vysoké
turbosoustrojí	turbosoustrojí	k1gNnPc4	turbosoustrojí
s	s	k7c7	s
reverzními	reverzní	k2eAgFnPc7d1	reverzní
Francisovými	Francisový	k2eAgFnPc7d1	Francisová
turbínami	turbína	k1gFnPc7	turbína
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
výkon	výkon	k1gInSc4	výkon
325	[number]	k4	325
MW	MW	kA	MW
(	(	kIx(	(
<g/>
výkon	výkon	k1gInSc1	výkon
v	v	k7c6	v
turbínovém	turbínový	k2eAgInSc6d1	turbínový
režimu	režim	k1gInSc6	režim
<g/>
,	,	kIx,	,
v	v	k7c6	v
čerpadlovém	čerpadlový	k2eAgInSc6d1	čerpadlový
režimu	režim	k1gInSc6	režim
je	být	k5eAaImIp3nS	být
maximum	maximum	k1gNnSc1	maximum
312	[number]	k4	312
MW	MW	kA	MW
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnPc4d3	veliký
reverzní	reverzní	k2eAgFnPc4d1	reverzní
vodní	vodní	k2eAgFnPc4d1	vodní
turbíny	turbína	k1gFnPc4	turbína
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
nádrží	nádrž	k1gFnSc7	nádrž
je	být	k5eAaImIp3nS	být
kaverna	kaverna	k1gFnSc1	kaverna
spojena	spojit	k5eAaPmNgFnS	spojit
dvěma	dva	k4xCgInPc7	dva
přivaděči	přivaděč	k1gInPc7	přivaděč
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
3,6	[number]	k4	3,6
m	m	kA	m
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
1547	[number]	k4	1547
a	a	k8xC	a
1499	[number]	k4	1499
m	m	kA	m
<g/>
,	,	kIx,	,
s	s	k7c7	s
dolní	dolní	k2eAgFnSc7d1	dolní
nádrží	nádrž	k1gFnSc7	nádrž
dvěma	dva	k4xCgInPc7	dva
tunely	tunel	k1gInPc7	tunel
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
5,2	[number]	k4	5,2
m	m	kA	m
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
354	[number]	k4	354
a	a	k8xC	a
390	[number]	k4	390
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
mají	mít	k5eAaImIp3nP	mít
přivaděče	přivaděč	k1gInPc1	přivaděč
kulové	kulový	k2eAgFnSc2d1	kulová
uzávěry	uzávěra	k1gFnSc2	uzávěra
<g/>
;	;	kIx,	;
při	při	k7c6	při
plném	plný	k2eAgInSc6d1	plný
výkonu	výkon	k1gInSc6	výkon
protéká	protékat	k5eAaImIp3nS	protékat
každým	každý	k3xTgInSc7	každý
přivaděčem	přivaděč	k1gInSc7	přivaděč
68,5	[number]	k4	68,5
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
;	;	kIx,	;
celý	celý	k2eAgInSc1d1	celý
objem	objem	k1gInSc1	objem
nádrže	nádrž	k1gFnSc2	nádrž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
načerpat	načerpat	k5eAaPmF	načerpat
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turbínách	turbína	k1gFnPc6	turbína
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
duté	dutý	k2eAgFnSc6d1	dutá
hřídeli	hřídel	k1gFnSc6	hřídel
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
1000	[number]	k4	1000
mm	mm	kA	mm
připevněny	připevněn	k2eAgInPc4d1	připevněn
generátory	generátor	k1gInPc4	generátor
<g/>
,	,	kIx,	,
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
pak	pak	k6eAd1	pak
rozběhové	rozběhový	k2eAgInPc1d1	rozběhový
motory	motor	k1gInPc1	motor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
při	při	k7c6	při
spouštění	spouštění	k1gNnSc6	spouštění
čerpadlového	čerpadlový	k2eAgInSc2d1	čerpadlový
režimu	režim	k1gInSc2	režim
roztočí	roztočit	k5eAaPmIp3nS	roztočit
lopatky	lopatka	k1gFnPc4	lopatka
turbíny	turbína	k1gFnSc2	turbína
do	do	k7c2	do
protisměru	protisměr	k1gInSc2	protisměr
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
lopatky	lopatka	k1gFnPc4	lopatka
poháněny	poháněn	k2eAgFnPc4d1	poháněna
přímo	přímo	k6eAd1	přímo
generátory	generátor	k1gInPc4	generátor
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
motorů	motor	k1gInPc2	motor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stropě	strop	k1gInSc6	strop
turbínové	turbínový	k2eAgFnSc2d1	turbínová
kaverny	kaverna	k1gFnSc2	kaverna
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
jeřáby	jeřáb	k1gInPc4	jeřáb
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
nosnosti	nosnost	k1gFnSc6	nosnost
250	[number]	k4	250
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
společně	společně	k6eAd1	společně
schopny	schopen	k2eAgFnPc1d1	schopna
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
soustrojí	soustrojí	k1gNnSc4	soustrojí
na	na	k7c4	na
odkládací	odkládací	k2eAgFnSc4d1	odkládací
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgFnSc3d1	přístupná
opravě	oprava	k1gFnSc3	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
turbínové	turbínový	k2eAgFnSc2d1	turbínová
kaverny	kaverna	k1gFnSc2	kaverna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgFnSc1d1	další
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozměru	rozměr	k1gInSc6	rozměr
115	[number]	k4	115
×	×	k?	×
16	[number]	k4	16
×	×	k?	×
21,7	[number]	k4	21,7
m	m	kA	m
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc2	který
jsou	být	k5eAaImIp3nP	být
rozvodny	rozvodna	k1gFnPc1	rozvodna
22	[number]	k4	22
kV	kV	k?	kV
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
blokové	blokový	k2eAgInPc1d1	blokový
trojfázové	trojfázový	k2eAgInPc1d1	trojfázový
transformátory	transformátor	k1gInPc1	transformátor
(	(	kIx(	(
<g/>
chráněné	chráněný	k2eAgNnSc1d1	chráněné
automatickým	automatický	k2eAgInSc7d1	automatický
hasicím	hasicí	k2eAgInSc7d1	hasicí
systémem	systém	k1gInSc7	systém
s	s	k7c7	s
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
proud	proud	k1gInSc1	proud
o	o	k7c4	o
napětí	napětí	k1gNnSc4	napětí
400	[number]	k4	400
kV	kV	k?	kV
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
veden	vést	k5eAaImNgInS	vést
na	na	k7c4	na
zapouzdřené	zapouzdřený	k2eAgNnSc4d1	zapouzdřené
vývodové	vývodový	k2eAgNnSc4d1	vývodový
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
venkovním	venkovní	k2eAgNnSc7d1	venkovní
vedením	vedení	k1gNnSc7	vedení
52	[number]	k4	52
km	km	kA	km
do	do	k7c2	do
rozvodny	rozvodna	k1gFnSc2	rozvodna
v	v	k7c6	v
Krasíkově	Krasíkův	k2eAgNnSc6d1	Krasíkův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
vybudována	vybudován	k2eAgFnSc1d1	vybudována
celá	celý	k2eAgFnSc1d1	celá
soustava	soustava	k1gFnSc1	soustava
komunikačních	komunikační	k2eAgMnPc2d1	komunikační
<g/>
,	,	kIx,	,
větracích	větrací	k2eAgInPc2d1	větrací
a	a	k8xC	a
odvodňovacích	odvodňovací	k2eAgInPc2d1	odvodňovací
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
šachet	šachta	k1gFnPc2	šachta
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
8,5	[number]	k4	8,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
nádrž	nádrž	k1gFnSc1	nádrž
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
dostupná	dostupný	k2eAgFnSc1d1	dostupná
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
(	(	kIx(	(
<g/>
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
její	její	k3xOp3gFnSc2	její
nesjízdnosti	nesjízdnost	k1gFnSc2	nesjízdnost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
nádrž	nádrž	k1gFnSc1	nádrž
dostupná	dostupný	k2eAgFnSc1d1	dostupná
také	také	k9	také
tunelem	tunel	k1gInSc7	tunel
s	s	k7c7	s
2400	[number]	k4	2400
schody	schod	k1gInPc7	schod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
opatřen	opatřit	k5eAaPmNgInS	opatřit
plošinovým	plošinový	k2eAgInSc7d1	plošinový
výtahem	výtah	k1gInSc7	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
statické	statický	k2eAgFnSc2d1	statická
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
přeměny	přeměna	k1gFnSc2	přeměna
nadbytečné	nadbytečný	k2eAgFnSc2d1	nadbytečná
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
špičkovou	špičkový	k2eAgFnSc4d1	špičková
<g/>
)	)	kIx)	)
také	také	k6eAd1	také
elektrárna	elektrárna	k1gFnSc1	elektrárna
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
funkci	funkce	k1gFnSc4	funkce
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
–	–	k?	–
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
výkonová	výkonový	k2eAgFnSc1d1	výkonová
rezerva	rezerva	k1gFnSc1	rezerva
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
regulační	regulační	k2eAgInSc4d1	regulační
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
řízení	řízení	k1gNnSc6	řízení
frekvence	frekvence	k1gFnSc2	frekvence
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
kompenzačním	kompenzační	k2eAgInSc6d1	kompenzační
režimu	režim	k1gInSc6	režim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
regulovat	regulovat	k5eAaImF	regulovat
napětí	napětí	k1gNnSc4	napětí
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
dokáže	dokázat	k5eAaPmIp3nS	dokázat
elektrárna	elektrárna	k1gFnSc1	elektrárna
z	z	k7c2	z
klidu	klid	k1gInSc2	klid
do	do	k7c2	do
maximálního	maximální	k2eAgInSc2d1	maximální
turbínového	turbínový	k2eAgInSc2d1	turbínový
výkonu	výkon	k1gInSc2	výkon
přejít	přejít	k5eAaPmF	přejít
za	za	k7c4	za
100	[number]	k4	100
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
dodávat	dodávat	k5eAaImF	dodávat
nepřetržitě	přetržitě	k6eNd1	přetržitě
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
čerpadlového	čerpadlový	k2eAgMnSc2d1	čerpadlový
do	do	k7c2	do
turbínového	turbínový	k2eAgInSc2d1	turbínový
režimu	režim	k1gInSc2	režim
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
přejít	přejít	k5eAaPmF	přejít
za	za	k7c4	za
150	[number]	k4	150
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
klidu	klid	k1gInSc2	klid
do	do	k7c2	do
čerpadlového	čerpadlový	k2eAgNnSc2d1	čerpadlové
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
400	[number]	k4	400
s.	s.	k?	s.
Provoz	provoz	k1gInSc1	provoz
elektrárny	elektrárna	k1gFnSc2	elektrárna
je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
dálkově	dálkově	k6eAd1	dálkově
<g/>
,	,	kIx,	,
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
centrálního	centrální	k2eAgInSc2d1	centrální
dispečinku	dispečink	k1gInSc2	dispečink
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
elektrárně	elektrárna	k1gFnSc6	elektrárna
pracuje	pracovat	k5eAaImIp3nS	pracovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
zejména	zejména	k9	zejména
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
malou	malý	k2eAgFnSc7d1	malá
nízkotlakou	nízkotlaký	k2eAgFnSc7d1	nízkotlaká
vyrovnávací	vyrovnávací	k2eAgFnSc7d1	vyrovnávací
elektrárnou	elektrárna	k1gFnSc7	elektrárna
(	(	kIx(	(
<g/>
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
stráně	stráň	k1gFnSc2	stráň
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pokrytí	pokrytí	k1gNnSc3	pokrytí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
spotřeby	spotřeba	k1gFnSc2	spotřeba
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
Francisovou	Francisový	k2eAgFnSc7d1	Francisová
turbínou	turbína	k1gFnSc7	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
163	[number]	k4	163
kW	kW	kA	kW
osazenou	osazený	k2eAgFnSc4d1	osazená
na	na	k7c6	na
odtoku	odtok	k1gInSc6	odtok
z	z	k7c2	z
dolní	dolní	k2eAgFnSc2d1	dolní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
PVE	PVE	kA	PVE
je	být	k5eAaImIp3nS	být
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
i	i	k9	i
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zásobování	zásobování	k1gNnSc4	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
:	:	kIx,	:
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
úpravnu	úpravna	k1gFnSc4	úpravna
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
potůčku	potůček	k1gInSc2	potůček
Jezerní	jezerní	k2eAgFnSc1d1	jezerní
<g/>
,	,	kIx,	,
disponuje	disponovat	k5eAaBmIp3nS	disponovat
také	také	k9	také
vlastní	vlastní	k2eAgFnSc7d1	vlastní
čistírnou	čistírna	k1gFnSc7	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvážení	zvážení	k1gNnSc6	zvážení
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
lokalit	lokalita	k1gFnPc2	lokalita
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
v	v	k7c6	v
masivu	masiv	k1gInSc6	masiv
Mravenečníku	mravenečník	k1gMnSc3	mravenečník
zahájena	zahájit	k5eAaPmNgNnP	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
však	však	k9	však
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
útlumu	útlum	k1gInSc6	útlum
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
vůbec	vůbec	k9	vůbec
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
elektrárny	elektrárna	k1gFnSc2	elektrárna
modernizován	modernizován	k2eAgInSc1d1	modernizován
–	–	k?	–
namísto	namísto	k7c2	namísto
původně	původně	k6eAd1	původně
plánovaných	plánovaný	k2eAgNnPc2d1	plánované
čtyř	čtyři	k4xCgNnPc2	čtyři
soustrojí	soustrojí	k1gNnPc2	soustrojí
s	s	k7c7	s
oddělenými	oddělený	k2eAgFnPc7d1	oddělená
turbínami	turbína	k1gFnPc7	turbína
a	a	k8xC	a
čerpadly	čerpadlo	k1gNnPc7	čerpadlo
byly	být	k5eAaImAgInP	být
navrženy	navržen	k2eAgInPc1d1	navržen
dvě	dva	k4xCgFnPc1	dva
reverzní	reverzní	k2eAgFnPc1d1	reverzní
turbíny	turbína	k1gFnPc1	turbína
o	o	k7c6	o
vyšším	vysoký	k2eAgInSc6d2	vyšší
výkonu	výkon	k1gInSc6	výkon
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dokončení	dokončení	k1gNnSc6	dokončení
elektrárny	elektrárna	k1gFnSc2	elektrárna
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
ochránců	ochránce	k1gMnPc2	ochránce
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
technických	technický	k2eAgInPc6d1	technický
problémech	problém	k1gInPc6	problém
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
turbínou	turbína	k1gFnSc7	turbína
byla	být	k5eAaImAgFnS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
do	do	k7c2	do
ostrého	ostrý	k2eAgInSc2d1	ostrý
provozu	provoz	k1gInSc2	provoz
uvedena	uvést	k5eAaPmNgFnS	uvést
až	až	k6eAd1	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc4	společnost
Ingstav	Ingstav	k1gFnSc3	Ingstav
<g/>
,	,	kIx,	,
dodavatelem	dodavatel	k1gMnSc7	dodavatel
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
investorských	investorský	k2eAgFnPc2d1	investorská
činností	činnost	k1gFnPc2	činnost
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
Energotis	Energotis	k1gFnSc2	Energotis
<g/>
,	,	kIx,	,
podzemní	podzemní	k2eAgFnSc2d1	podzemní
práce	práce	k1gFnSc2	práce
prováděla	provádět	k5eAaImAgFnS	provádět
firma	firma	k1gFnSc1	firma
Subterra	Subterr	k1gMnSc2	Subterr
<g/>
,	,	kIx,	,
asfaltobetonové	asfaltobetonový	k2eAgNnSc4d1	asfaltobetonový
těsnění	těsnění	k1gNnSc4	těsnění
zajistila	zajistit	k5eAaPmAgFnS	zajistit
bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
společnost	společnost	k1gFnSc1	společnost
Slovasfalt	Slovasfalta	k1gFnPc2	Slovasfalta
<g/>
.	.	kIx.	.
</s>
<s>
Generálním	generální	k2eAgMnSc7d1	generální
projektantem	projektant	k1gMnSc7	projektant
stavby	stavba	k1gFnSc2	stavba
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
Aquatis	Aquatis	k1gFnSc1	Aquatis
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
technologie	technologie	k1gFnSc1	technologie
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
firma	firma	k1gFnSc1	firma
ČKD	ČKD	kA	ČKD
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
transformátory	transformátor	k1gMnPc4	transformátor
dodala	dodat	k5eAaPmAgFnS	dodat
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
pancíř	pancíř	k1gInSc1	pancíř
přivaděčů	přivaděč	k1gInPc2	přivaděč
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
společnost	společnost	k1gFnSc1	společnost
Hutní	hutní	k2eAgFnSc2d1	hutní
montáže	montáž	k1gFnSc2	montáž
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
technologické	technologický	k2eAgInPc1d1	technologický
celky	celek	k1gInPc1	celek
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
:	:	kIx,	:
řídicí	řídicí	k2eAgInSc1d1	řídicí
systém	systém	k1gInSc1	systém
a	a	k8xC	a
zapouzdřenou	zapouzdřený	k2eAgFnSc4d1	zapouzdřená
rozvodnu	rozvodna	k1gFnSc4	rozvodna
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
firma	firma	k1gFnSc1	firma
AEG	AEG	kA	AEG
<g/>
,	,	kIx,	,
kabelový	kabelový	k2eAgInSc4d1	kabelový
vývod	vývod	k1gInSc4	vývod
dodala	dodat	k5eAaPmAgFnS	dodat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
firma	firma	k1gFnSc1	firma
SILEC	SILEC	kA	SILEC
<g/>
,	,	kIx,	,
výkonové	výkonový	k2eAgInPc1d1	výkonový
vypínače	vypínač	k1gInPc1	vypínač
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
ABB	ABB	kA	ABB
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
činily	činit	k5eAaImAgFnP	činit
přibližně	přibližně	k6eAd1	přibližně
6,5	[number]	k4	6,5
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
elektrárna	elektrárna	k1gFnSc1	elektrárna
se	se	k3xPyFc4	se
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
elektrárna	elektrárna	k1gFnSc1	elektrárna
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
dodala	dodat	k5eAaPmAgFnS	dodat
2	[number]	k4	2
672,716	[number]	k4	672,716
GWh	GWh	k1gMnPc2	GWh
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2007	[number]	k4	2007
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
generální	generální	k2eAgFnSc1d1	generální
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
horní	horní	k2eAgFnSc2d1	horní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
–	–	k?	–
první	první	k4xOgFnPc1	první
závady	závada	k1gFnPc1	závada
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
už	už	k6eAd1	už
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
izolace	izolace	k1gFnSc1	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Opravy	oprava	k1gFnPc4	oprava
provedla	provést	k5eAaPmAgFnS	provést
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
firma	firma	k1gFnSc1	firma
Walo	Walo	k1gMnSc1	Walo
Bertschinger	Bertschinger	k1gMnSc1	Bertschinger
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výměnou	výměna	k1gFnSc7	výměna
řídicího	řídicí	k2eAgInSc2d1	řídicí
systému	systém	k1gInSc2	systém
stála	stát	k5eAaImAgFnS	stát
zhruba	zhruba	k6eAd1	zhruba
360	[number]	k4	360
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zejména	zejména	k9	zejména
zpočátku	zpočátku	k6eAd1	zpočátku
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
kritiku	kritika	k1gFnSc4	kritika
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k9	již
stavba	stavba	k1gFnSc1	stavba
výraznější	výrazný	k2eAgNnSc4d2	výraznější
množství	množství	k1gNnSc4	množství
kritiky	kritika	k1gFnSc2	kritika
nevyvolává	vyvolávat	k5eNaImIp3nS	vyvolávat
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
stráně	stráň	k1gFnPc1	stráň
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
v	v	k7c6	v
internetové	internetový	k2eAgFnSc6d1	internetová
anketě	anketa	k1gFnSc6	anketa
serveru	server	k1gInSc2	server
iDNES	iDNES	k?	iDNES
o	o	k7c4	o
"	"	kIx"	"
<g/>
div	div	k1gInSc4	div
Česka	Česko	k1gNnSc2	Česko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
kole	kolo	k1gNnSc6	kolo
získaly	získat	k5eAaPmAgFnP	získat
13	[number]	k4	13
555	[number]	k4	555
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
31	[number]	k4	31
tisíc	tisíc	k4xCgInPc2	tisíc
zaslaných	zaslaný	k2eAgInPc2d1	zaslaný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
pořádá	pořádat	k5eAaImIp3nS	pořádat
celoročně	celoročně	k6eAd1	celoročně
(	(	kIx(	(
<g/>
i	i	k9	i
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
a	a	k8xC	a
svátcích	svátek	k1gInPc6	svátek
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
telefonickém	telefonický	k2eAgNnSc6d1	telefonické
objednání	objednání	k1gNnSc6	objednání
<g/>
)	)	kIx)	)
exkurze	exkurze	k1gFnSc1	exkurze
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
návštěvy	návštěva	k1gFnSc2	návštěva
infocentra	infocentrum	k1gNnSc2	infocentrum
a	a	k8xC	a
promítání	promítání	k1gNnSc2	promítání
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
o	o	k7c6	o
elektrárně	elektrárna	k1gFnSc6	elektrárna
<g/>
,	,	kIx,	,
prohlídce	prohlídka	k1gFnSc6	prohlídka
podzemní	podzemní	k2eAgFnSc2d1	podzemní
strojovny	strojovna	k1gFnSc2	strojovna
s	s	k7c7	s
turbínami	turbína	k1gFnPc7	turbína
a	a	k8xC	a
případné	případný	k2eAgFnPc4d1	případná
obhlídky	obhlídka	k1gFnPc4	obhlídka
dolní	dolní	k2eAgFnSc2d1	dolní
a	a	k8xC	a
horní	horní	k2eAgFnSc2d1	horní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nP	navštívit
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
stráně	stráň	k1gFnSc2	stráň
asi	asi	k9	asi
šedesát	šedesát	k4xCc1	šedesát
tisíc	tisíc	k4xCgInSc1	tisíc
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
statistiky	statistika	k1gFnSc2	statistika
napočítali	napočítat	k5eAaPmAgMnP	napočítat
miliontého	miliontý	k4xOgMnSc4	miliontý
návštěvníka	návštěvník	k1gMnSc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
stráně	stráň	k1gFnSc2	stráň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
Stráně	stráň	k1gFnPc1	stráň
<g/>
,	,	kIx,	,
web	web	k1gInSc1	web
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
Podzemí	podzemí	k1gNnPc2	podzemí
PVE	PVE	kA	PVE
Dlouhé	Dlouhá	k1gFnSc2	Dlouhá
stráně	stráň	k1gFnSc2	stráň
<g/>
,	,	kIx,	,
reportáž	reportáž	k1gFnSc1	reportáž
<g/>
,	,	kIx,	,
ejeseniky	ejesenik	k1gInPc1	ejesenik
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Heřmanský	Heřmanský	k2eAgMnSc1d1	Heřmanský
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2006	[number]	k4	2006
Jeseníky	Jeseník	k1gInPc4	Jeseník
-	-	kIx~	-
letecké	letecký	k2eAgFnPc4d1	letecká
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
ejeseniky	ejesenik	k1gInPc4	ejesenik
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Heřmanský	Heřmanský	k2eAgMnSc1d1	Heřmanský
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Gangur	Gangur	k1gMnSc1	Gangur
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Kňazovčík	Kňazovčík	k1gMnSc1	Kňazovčík
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
Jan	Jan	k1gMnSc1	Jan
Hlinák	Hlinák	k1gMnSc1	Hlinák
<g/>
:	:	kIx,	:
Jeseníky	Jeseník	k1gInPc1	Jeseník
<g/>
,	,	kIx,	,
PVO	PVO	kA	PVO
(	(	kIx(	(
<g/>
přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
)	)	kIx)	)
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
stráně	stráň	k1gFnPc1	stráň
<g/>
,	,	kIx,	,
Paladix	Paladix	k1gInSc1	Paladix
foto-on-line	fotonin	k1gInSc5	foto-on-lin
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
rubrika	rubrika	k1gFnSc1	rubrika
Tipy	tip	k1gInPc1	tip
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
stráně	stráň	k1gFnSc2	stráň
-	-	kIx~	-
oprava	oprava	k1gFnSc1	oprava
horní	horní	k2eAgFnSc2d1	horní
nádrže	nádrž	k1gFnSc2	nádrž
PVE	PVE	kA	PVE
<g/>
,	,	kIx,	,
ejeseniky	ejesenik	k1gInPc1	ejesenik
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Heřmanský	Heřmanský	k2eAgMnSc1d1	Heřmanský
<g/>
,	,	kIx,	,
Tisková	tiskový	k2eAgFnSc1d1	tisková
zpráva	zpráva	k1gFnSc1	zpráva
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
Rostislav	Rostislav	k1gMnSc1	Rostislav
Jančar	jančar	k1gInSc1	jančar
<g/>
:	:	kIx,	:
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
Stráně	stráň	k1gFnPc1	stráň
skrz	skrz	k7c4	skrz
naskrz	naskrz	k6eAd1	naskrz
<g/>
.	.	kIx.	.
</s>
<s>
Reportáž	reportáž	k1gFnSc1	reportáž
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
nepodíváte	podívat	k5eNaImIp2nP	podívat
<g/>
,	,	kIx,	,
technet	technet	k1gInSc1	technet
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
reportáž	reportáž	k1gFnSc1	reportáž
z	z	k7c2	z
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
PVE	PVE	kA	PVE
</s>
