<s>
Audrey	Audrea	k1gFnPc1	Audrea
Justine	Justin	k1gMnSc5	Justin
Tautou	Tauta	k1gFnSc7	Tauta
[	[	kIx(	[
<g/>
odre	odr	k1gInSc5	odr
žüstyn	žüstyna	k1gFnPc2	žüstyna
totu	toto	k1gNnSc6	toto
<g/>
]	]	kIx)	]
narozená	narozený	k2eAgFnSc1d1	narozená
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1976	[number]	k4	1976
v	v	k7c6	v
Beaumontu	Beaumont	k1gInSc6	Beaumont
<g/>
,	,	kIx,	,
departement	departement	k1gInSc1	departement
Puy-de-Dôme	Puye-Dôm	k1gMnSc5	Puy-de-Dôm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známou	známá	k1gFnSc4	známá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
rolí	role	k1gFnPc2	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
salon	salon	k1gInSc4	salon
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
César	César	k1gMnSc1	César
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
ženská	ženský	k2eAgFnSc1d1	ženská
herecká	herecký	k2eAgFnSc1d1	herecká
naděje	naděje	k1gFnSc1	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Francie	Francie	k1gFnSc2	Francie
proslula	proslout	k5eAaPmAgNnP	proslout
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
svojí	svůj	k3xOyFgFnSc7	svůj
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rolí	role	k1gFnSc7	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Amélie	Amélie	k1gFnSc2	Amélie
z	z	k7c2	z
Montmartru	Montmartr	k1gInSc2	Montmartr
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
pak	pak	k6eAd1	pak
hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
v	v	k7c6	v
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
filmu	film	k1gInSc6	film
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
oblíbené	oblíbený	k2eAgMnPc4d1	oblíbený
spisovatele	spisovatel	k1gMnPc4	spisovatel
patří	patřit	k5eAaImIp3nS	patřit
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Auster	Auster	k1gMnSc1	Auster
<g/>
,	,	kIx,	,
a	a	k8xC	a
Timothy	Timotha	k1gFnPc1	Timotha
Zahn	Zahn	k1gInSc1	Zahn
<g/>
,	,	kIx,	,
jejími	její	k3xOp3gMnPc7	její
oblíbenými	oblíbený	k2eAgMnPc7d1	oblíbený
básníky	básník	k1gMnPc7	básník
jsou	být	k5eAaImIp3nP	být
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
a	a	k8xC	a
Tristan	Tristan	k1gInSc4	Tristan
Tzara	Tzaro	k1gNnSc2	Tzaro
<g/>
.	.	kIx.	.
</s>
<s>
Audrey	Audrey	k1gInPc4	Audrey
si	se	k3xPyFc3	se
fotografuje	fotografovat	k5eAaImIp3nS	fotografovat
všechny	všechen	k3xTgMnPc4	všechen
reportéry	reportér	k1gMnPc4	reportér
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dělají	dělat	k5eAaImIp3nP	dělat
interview	interview	k1gInSc4	interview
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
fotky	fotka	k1gFnPc4	fotka
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
v	v	k7c6	v
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
ji	on	k3xPp3gFnSc4	on
mnozí	mnohý	k2eAgMnPc1d1	mnohý
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
typickou	typický	k2eAgFnSc4d1	typická
okcitánskou	okcitánský	k2eAgFnSc4d1	okcitánská
obyvatelku	obyvatelka	k1gFnSc4	obyvatelka
Auvergnate	Auvergnat	k1gInSc5	Auvergnat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
pochybnosti	pochybnost	k1gFnSc2	pochybnost
novinářů	novinář	k1gMnPc2	novinář
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
účinkováním	účinkování	k1gNnSc7	účinkování
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
,	,	kIx,	,
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
zábavný	zábavný	k2eAgInSc4d1	zábavný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
film	film	k1gInSc1	film
náboženský	náboženský	k2eAgInSc1d1	náboženský
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepodílela	podílet	k5eNaImAgFnS	podílet
na	na	k7c6	na
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
napadal	napadat	k5eAaBmAgInS	napadat
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
jsem	být	k5eAaImIp1nS	být
vychována	vychovat	k5eAaPmNgFnS	vychovat
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
oficiálně	oficiálně	k6eAd1	oficiálně
nejsem	být	k5eNaImIp1nS	být
katolička	katolička	k1gFnSc1	katolička
<g/>
,	,	kIx,	,
a	a	k8xC	a
věřím	věřit	k5eAaImIp1nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
-	-	kIx~	-
tedy	tedy	k9	tedy
v	v	k7c4	v
mého	můj	k3xOp1gMnSc4	můj
Boha	bůh	k1gMnSc4	bůh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Začínala	začínat	k5eAaImAgFnS	začínat
jako	jako	k9	jako
televizní	televizní	k2eAgFnSc1d1	televizní
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známou	známá	k1gFnSc4	známá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
rolí	role	k1gFnPc2	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
salon	salon	k1gInSc4	salon
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgNnP	být
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
César	César	k1gMnSc1	César
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
ženská	ženský	k2eAgFnSc1d1	ženská
herecká	herecký	k2eAgFnSc1d1	herecká
naděje	naděje	k1gFnSc1	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Francie	Francie	k1gFnSc2	Francie
proslula	proslout	k5eAaPmAgNnP	proslout
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
svojí	svůj	k3xOyFgFnSc7	svůj
rolí	role	k1gFnSc7	role
Amélie	Amélie	k1gFnSc1	Amélie
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Amélie	Amélie	k1gFnSc2	Amélie
z	z	k7c2	z
Montmartru	Montmartr	k1gInSc2	Montmartr
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
mj.	mj.	kA	mj.
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
Césara	César	k1gMnSc4	César
<g/>
,	,	kIx,	,
Evropskou	evropský	k2eAgFnSc4d1	Evropská
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
či	či	k8xC	či
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
rolí	role	k1gFnSc7	role
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
role	role	k1gFnSc2	role
Matildy	Matilda	k1gFnSc2	Matilda
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Příliš	příliš	k6eAd1	příliš
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
zásnuby	zásnub	k1gInPc1	zásnub
(	(	kIx(	(
<g/>
Un	Un	k1gFnSc1	Un
long	longa	k1gFnPc2	longa
dimanche	dimanchat	k5eAaPmIp3nS	dimanchat
de	de	k?	de
fiançailles	fiançailles	k1gInSc1	fiançailles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgNnP	být
mj.	mj.	kA	mj.
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
Césara	César	k1gMnSc4	César
či	či	k8xC	či
Evropskou	evropský	k2eAgFnSc4d1	Evropská
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
filmu	film	k1gInSc6	film
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
hereckým	herecký	k2eAgMnSc7d1	herecký
partnerem	partner	k1gMnSc7	partner
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanks	k1gInSc1	Hanks
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
tématu	téma	k1gNnSc3	téma
mírně	mírně	k6eAd1	mírně
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
v	v	k7c6	v
domovské	domovský	k2eAgFnSc6d1	domovská
Francii	Francie	k1gFnSc6	Francie
chce	chtít	k5eAaImIp3nS	chtít
i	i	k9	i
nadále	nadále	k6eAd1	nadále
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
členkou	členka	k1gFnSc7	členka
americké	americký	k2eAgFnSc2d1	americká
Akademie	akademie	k1gFnSc2	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Écume	Écum	k1gInSc5	Écum
des	des	k1gNnPc6	des
jours	jours	k1gInSc4	jours
<g/>
)	)	kIx)	)
-	-	kIx~	-
Chloé	Chloé	k1gNnSc1	Chloé
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Thérè	Thérè	k1gFnSc1	Thérè
Desqueyroux	Desqueyroux	k1gInSc1	Desqueyroux
-	-	kIx~	-
Thérè	Thérè	k1gFnSc1	Thérè
Desqueyroux	Desqueyroux	k1gInSc1	Desqueyroux
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
(	(	kIx(	(
<g/>
Des	des	k1gNnSc1	des
vents	ventsa	k1gFnPc2	ventsa
contraires	contrairesa	k1gFnPc2	contrairesa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Něžnost	něžnost	k1gFnSc1	něžnost
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
délicatesse	délicatesse	k1gFnSc2	délicatesse
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
Nathalie	Nathalie	k1gFnSc1	Nathalie
Kerl	Kerl	k1gMnSc1	Kerl
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Krásné	krásný	k2eAgFnSc2d1	krásná
lži	lež	k1gFnSc2	lež
(	(	kIx(	(
<g/>
De	De	k?	De
vrais	vrais	k1gInSc1	vrais
mensonges	mensonges	k1gInSc1	mensonges
<g/>
)	)	kIx)	)
-	-	kIx~	-
Émilie	Émilie	k1gFnSc1	Émilie
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
(	(	kIx(	(
<g/>
Coco	Coco	k1gMnSc1	Coco
avant	avant	k1gMnSc1	avant
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gabrielle	Gabrielle	k1gFnSc1	Gabrielle
"	"	kIx"	"
<g/>
Coco	Coco	k1gMnSc1	Coco
<g/>
"	"	kIx"	"
Chanel	Chanel	k1gMnSc1	Chanel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Prostě	prostě	k9	prostě
spolu	spolu	k6eAd1	spolu
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Camille	Camille	k1gFnSc1	Camille
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Hors	Hors	k1gInSc1	Hors
de	de	k?	de
prix	prix	k1gInSc1	prix
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
-	-	kIx~	-
Sophie	Sophie	k1gFnSc1	Sophie
Neveu	Neveus	k1gInSc2	Neveus
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
Poupées	Poupées	k1gInSc1	Poupées
russes	russes	k1gInSc4	russes
-	-	kIx~	-
Martine	Martin	k1gMnSc5	Martin
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Příliš	příliš	k6eAd1	příliš
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
zásnuby	zásnuba	k1gFnPc1	zásnuba
(	(	kIx(	(
<g/>
Un	Un	k1gFnPc1	Un
long	long	k1gInSc4	long
dimanche	dimanch	k1gFnSc2	dimanch
de	de	k?	de
fiançailles	fiançailles	k1gMnSc1	fiançailles
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mathilde	Mathild	k1gMnSc5	Mathild
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Nowhere	Nowher	k1gInSc5	Nowher
to	ten	k3xDgNnSc4	ten
Go	Go	k1gFnSc7	Go
But	But	k1gMnSc1	But
Up	Up	k1gFnSc2	Up
-	-	kIx~	-
Val	valit	k5eAaImRp2nS	valit
Chipzik	Chipzik	k1gInSc1	Chipzik
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Pas	pas	k6eAd1	pas
sur	sur	k?	sur
la	la	k1gNnSc7	la
bouche	bouch	k1gFnSc2	bouch
-	-	kIx~	-
Huguette	Huguett	k1gInSc5	Huguett
Verberie	Verberie	k1gFnPc1	Verberie
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Les	les	k1gInSc4	les
Marins	Marinsa	k1gFnPc2	Marinsa
perdus	perdus	k1gMnSc1	perdus
-	-	kIx~	-
Lalla	Lalla	k1gMnSc1	Lalla
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Špína	špína	k1gFnSc1	špína
Londýna	Londýn	k1gInSc2	Londýn
(	(	kIx(	(
<g/>
Dirty	Dirta	k1gFnSc2	Dirta
Pretty	Pretta	k1gFnSc2	Pretta
Things	Thingsa	k1gFnPc2	Thingsa
<g/>
)	)	kIx)	)
-	-	kIx~	-
Senay	Senaa	k1gFnSc2	Senaa
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Erasmus	Erasmus	k1gInSc1	Erasmus
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Auberge	Auberge	k1gFnSc1	Auberge
espagnole	espagnole	k1gFnSc1	espagnole
<g/>
)	)	kIx)	)
-	-	kIx~	-
Martine	Martin	k1gMnSc5	Martin
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Má	mít	k5eAaImIp3nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
(	(	kIx(	(
<g/>
À	À	k?	À
la	la	k1gNnSc1	la
folie	folie	k1gFnSc1	folie
<g/>
...	...	k?	...
pas	pas	k1gInSc1	pas
du	du	k?	du
tout	tout	k1gInSc1	tout
<g/>
)	)	kIx)	)
-	-	kIx~	-
Angélique	Angélique	k1gFnSc1	Angélique
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
ne	ne	k9	ne
(	(	kIx(	(
<g/>
Dieu	Diea	k1gMnSc4	Diea
est	est	k?	est
grand	grand	k1gMnSc1	grand
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
suis	suis	k6eAd1	suis
toute	toute	k5eAaPmIp2nP	toute
petite	petit	k1gInSc5	petit
<g/>
)	)	kIx)	)
-	-	kIx~	-
Michè	Michè	k1gMnSc1	Michè
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Amélie	Amélie	k1gFnSc2	Amélie
z	z	k7c2	z
Montmartru	Montmartr	k1gInSc2	Montmartr
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Fabuleux	Fabuleux	k1gInSc1	Fabuleux
destin	destin	k1gInSc1	destin
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Amélie	Amélie	k1gFnSc1	Amélie
Poulain	Poulain	k1gMnSc1	Poulain
<g/>
)	)	kIx)	)
-	-	kIx~	-
Amélie	Amélie	k1gFnSc1	Amélie
Poulain	Poulaina	k1gFnPc2	Poulaina
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Le	Le	k1gMnSc1	Le
Battement	Battement	k1gMnSc1	Battement
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
ailes	ailes	k1gMnSc1	ailes
du	du	k?	du
papillon	papillon	k1gInSc1	papillon
-	-	kIx~	-
Irè	Irè	k1gFnSc1	Irè
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Le	Le	k1gMnSc1	Le
Libertin	libertin	k1gMnSc1	libertin
-	-	kIx~	-
Julie	Julie	k1gFnSc1	Julie
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Holbach	Holbach	k1gMnSc1	Holbach
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Voyous	Voyous	k1gMnSc1	Voyous
voyelles	voyelles	k1gMnSc1	voyelles
-	-	kIx~	-
Anne-Sophie	Anne-Sophie	k1gFnSc1	Anne-Sophie
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Vem	Vem	k?	Vem
si	se	k3xPyFc3	se
mě	já	k3xPp1nSc2	já
(	(	kIx(	(
<g/>
Épouse-moi	Épouseoi	k1gNnPc4	Épouse-moi
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
Marie-Ange	Marie-Ange	k1gInSc1	Marie-Ange
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Triste	Trist	k1gMnSc5	Trist
à	à	k?	à
mourir	mourir	k1gMnSc1	mourir
-	-	kIx~	-
Caro	Caro	k1gMnSc1	Caro
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
salon	salon	k1gInSc4	salon
krásy	krása	k1gFnSc2	krása
(	(	kIx(	(
<g/>
Vénus	Vénus	k1gInSc4	Vénus
beauté	beautý	k2eAgFnPc1d1	beautý
(	(	kIx(	(
<g/>
institut	institut	k1gInSc1	institut
<g/>
))	))	k?	))
-	-	kIx~	-
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Le	Le	k1gFnSc1	Le
Boiteux	Boiteux	k1gInSc1	Boiteux
<g/>
:	:	kIx,	:
Baby	baby	k1gNnSc1	baby
blues	blues	k1gNnSc1	blues
-	-	kIx~	-
Blandine	Blandin	k1gMnSc5	Blandin
Piancet	Piancet	k1gMnSc5	Piancet
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
Vieille	Vieille	k1gFnSc2	Vieille
barriè	barriè	k?	barriè
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Chaos	chaos	k1gInSc1	chaos
technique	techniqu	k1gInSc2	techniqu
-	-	kIx~	-
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
Radostná	radostný	k2eAgFnSc1d1	radostná
událost	událost	k1gFnSc1	událost
(	(	kIx(	(
<g/>
Bébés	Bébés	k1gInSc1	Bébés
boum	boum	k1gInSc1	boum
<g/>
)	)	kIx)	)
-	-	kIx~	-
Elsa	Elsa	k1gFnSc1	Elsa
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
La	la	k1gNnSc4	la
Vérité	Véritý	k2eAgFnSc2d1	Véritý
est	est	k?	est
un	un	k?	un
vilain	vilain	k1gMnSc1	vilain
défaut	défaut	k1gMnSc1	défaut
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
Coeur	Coeur	k1gMnSc1	Coeur
de	de	k?	de
cible	cible	k1gInSc1	cible
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
</s>
