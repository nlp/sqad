<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
Henry	Henry	k1gMnSc1	Henry
Fonda	Fonda	k1gMnSc1	Fonda
byl	být	k5eAaImAgMnS	být
obsazen	obsadit	k5eAaPmNgMnS	obsadit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgInSc3	svůj
typu	typ	k1gInSc3	typ
jako	jako	k9	jako
darebák	darebák	k1gMnSc1	darebák
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bronson	Bronson	k1gMnSc1	Bronson
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
osudový	osudový	k2eAgMnSc1d1	osudový
soupeř	soupeř	k1gMnSc1	soupeř
"	"	kIx"	"
<g/>
Harmonika	harmonika	k1gFnSc1	harmonika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Jason	Jason	k1gNnSc1	Jason
Robards	Robardsa	k1gFnPc2	Robardsa
jako	jako	k8xC	jako
sympatický	sympatický	k2eAgMnSc1d1	sympatický
bandita	bandita	k1gMnSc1	bandita
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
a	a	k8xC	a
Claudia	Claudia	k1gFnSc1	Claudia
Cardinalová	Cardinalová	k1gFnSc1	Cardinalová
jako	jako	k8xS	jako
Jill	Jill	k1gInSc1	Jill
<g/>
,	,	kIx,	,
ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
minulostí	minulost	k1gFnSc7	minulost
prostitutky	prostitutka	k1gFnPc1	prostitutka
<g/>
.	.	kIx.	.
</s>
