<p>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgInSc1d1	legendární
italský	italský	k2eAgInSc1d1	italský
western	western	k1gInSc1	western
italského	italský	k2eAgMnSc2d1	italský
režiséra	režisér	k1gMnSc2	režisér
Sergia	Sergius	k1gMnSc2	Sergius
Leoneho	Leone	k1gMnSc2	Leone
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
Henry	Henry	k1gMnSc1	Henry
Fonda	Fonda	k1gMnSc1	Fonda
byl	být	k5eAaImAgMnS	být
obsazen	obsadit	k5eAaPmNgMnS	obsadit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgInSc3	svůj
typu	typ	k1gInSc3	typ
jako	jako	k9	jako
darebák	darebák	k1gMnSc1	darebák
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bronson	Bronson	k1gMnSc1	Bronson
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
osudový	osudový	k2eAgMnSc1d1	osudový
soupeř	soupeř	k1gMnSc1	soupeř
"	"	kIx"	"
<g/>
Harmonika	harmonika	k1gFnSc1	harmonika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Jason	Jason	k1gNnSc1	Jason
Robards	Robardsa	k1gFnPc2	Robardsa
jako	jako	k8xC	jako
sympatický	sympatický	k2eAgMnSc1d1	sympatický
bandita	bandita	k1gMnSc1	bandita
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
a	a	k8xC	a
Claudia	Claudia	k1gFnSc1	Claudia
Cardinalová	Cardinalová	k1gFnSc1	Cardinalová
jako	jako	k8xC	jako
Jill	Jill	k1gInSc1	Jill
<g/>
,	,	kIx,	,
ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
minulostí	minulost	k1gFnSc7	minulost
prostitutky	prostitutka	k1gFnPc1	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc4	scénář
napsali	napsat	k5eAaBmAgMnP	napsat
Leone	Leo	k1gMnSc5	Leo
a	a	k8xC	a
Sergio	Sergio	k1gMnSc1	Sergio
Donati	Donat	k1gMnPc1	Donat
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
Leone	Leo	k1gMnSc5	Leo
<g/>
,	,	kIx,	,
Bernardo	Bernarda	k1gMnSc5	Bernarda
Bertolucci	Bertolucc	k1gMnSc5	Bertolucc
a	a	k8xC	a
Dario	Daria	k1gFnSc5	Daria
Argento	Argento	k1gNnSc1	Argento
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
kamerou	kamera	k1gFnSc7	kamera
stál	stát	k5eAaImAgMnS	stát
Tonino	Tonino	k1gNnSc4	Tonino
Delli	Dell	k1gMnSc3	Dell
Colli	Coll	k1gMnSc3	Coll
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Ennio	Ennio	k1gMnSc1	Ennio
Morricone	Morricon	k1gMnSc5	Morricon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
film	film	k1gInSc1	film
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
kasovní	kasovní	k2eAgInSc4d1	kasovní
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
po	po	k7c6	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Leoneho	Leone	k1gMnSc4	Leone
neortodoxní	ortodoxní	k2eNgInSc1d1	neortodoxní
western	western	k1gInSc1	western
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
negativní	negativní	k2eAgFnSc7d1	negativní
kritikou	kritika	k1gFnSc7	kritika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
stal	stát	k5eAaPmAgInS	stát
filmovým	filmový	k2eAgInSc7d1	filmový
propadákem	propadák	k1gInSc7	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
je	být	k5eAaImIp3nS	být
však	však	k9	však
nyní	nyní	k6eAd1	nyní
obecně	obecně	k6eAd1	obecně
uznáván	uznáván	k2eAgMnSc1d1	uznáván
jako	jako	k8xC	jako
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
westernových	westernový	k2eAgInPc2d1	westernový
filmů	film	k1gInPc2	film
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
stříbrné	stříbrný	k2eAgNnSc4d1	stříbrné
plátno	plátno	k1gNnSc4	plátno
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
eposu	epos	k1gInSc2	epos
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
filmu	film	k1gInSc2	film
Hodný	hodný	k2eAgInSc1d1	hodný
<g/>
,	,	kIx,	,
zlý	zlý	k2eAgInSc1d1	zlý
a	a	k8xC	a
ošklivý	ošklivý	k2eAgInSc1d1	ošklivý
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
Leone	Leo	k1gMnSc5	Leo
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
opustit	opustit	k5eAaPmF	opustit
žánr	žánr	k1gInSc4	žánr
westernu	western	k1gInSc2	western
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
řekl	říct	k5eAaPmAgMnS	říct
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
chtěl	chtít	k5eAaImAgMnS	chtít
říci	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
ho	on	k3xPp3gInSc4	on
román	román	k1gInSc4	román
The	The	k1gFnSc1	The
Hoods	Hoods	k1gInSc1	Hoods
<g/>
,	,	kIx,	,
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
kniha	kniha	k1gFnSc1	kniha
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
autorově	autorův	k2eAgFnSc6d1	autorova
vlastní	vlastní	k2eAgFnSc6d1	vlastní
zkušenosti	zkušenost	k1gFnSc6	zkušenost
během	během	k7c2	během
prohibice	prohibice	k1gFnSc2	prohibice
<g/>
,	,	kIx,	,
a	a	k8xC	a
plánoval	plánovat	k5eAaImAgMnS	plánovat
ji	on	k3xPp3gFnSc4	on
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
o	o	k7c4	o
sedmnáct	sedmnáct	k4xCc4	sedmnáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
Tenkrát	tenkrát	k6eAd1	tenkrát
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodská	hollywoodský	k2eAgNnPc1d1	hollywoodské
studia	studio	k1gNnPc1	studio
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nabízela	nabízet	k5eAaImAgFnS	nabízet
jen	jen	k9	jen
westerny	western	k1gInPc4	western
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
United	United	k1gMnSc1	United
Artists	Artists	k1gInSc1	Artists
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
stálo	stát	k5eAaImAgNnS	stát
i	i	k9	i
za	za	k7c7	za
dolarovou	dolarový	k2eAgFnSc7d1	dolarová
trilogií	trilogie	k1gFnSc7	trilogie
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
Leonemu	Leonema	k1gFnSc4	Leonema
film	film	k1gInSc4	film
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
Charltonem	Charlton	k1gInSc7	Charlton
Hestonem	Heston	k1gInSc7	Heston
<g/>
,	,	kIx,	,
Kirkem	Kirek	k1gMnSc7	Kirek
Douglasem	Douglas	k1gMnSc7	Douglas
a	a	k8xC	a
Rockem	rock	k1gInSc7	rock
Hudsonem	Hudson	k1gMnSc7	Hudson
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Leone	Leo	k1gMnSc5	Leo
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
Paramount	Paramount	k1gInSc1	Paramount
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
velkorysý	velkorysý	k2eAgInSc1d1	velkorysý
rozpočet	rozpočet	k1gInSc1	rozpočet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Henrym	Henry	k1gMnSc7	Henry
Fondou	Fonda	k1gMnSc7	Fonda
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
pracovat	pracovat	k5eAaImF	pracovat
vlastně	vlastně	k9	vlastně
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
Leone	Leo	k1gMnSc5	Leo
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
dva	dva	k4xCgInPc4	dva
střety	střet	k1gInPc4	střet
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
paralelně	paralelně	k6eAd1	paralelně
odehrají	odehrát	k5eAaPmIp3nP	odehrát
ve	v	k7c6	v
Flagstone	Flagston	k1gInSc5	Flagston
<g/>
,	,	kIx,	,
fiktivním	fiktivní	k2eAgNnSc6d1	fiktivní
městě	město	k1gNnSc6	město
na	na	k7c6	na
Divokém	divoký	k2eAgInSc6d1	divoký
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
boj	boj	k1gInSc1	boj
o	o	k7c4	o
pozemek	pozemek	k1gInSc4	pozemek
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
pomsty	pomsta	k1gFnSc2	pomsta
chladnokrevnému	chladnokrevný	k2eAgMnSc3d1	chladnokrevný
zabijákovi	zabiják	k1gMnSc3	zabiják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
boje	boj	k1gInSc2	boj
o	o	k7c4	o
Sweetwater	Sweetwater	k1gInSc4	Sweetwater
<g/>
,	,	kIx,	,
pozemek	pozemek	k1gInSc4	pozemek
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Flagstone	Flagston	k1gInSc5	Flagston
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
má	mít	k5eAaImIp3nS	mít
zdroj	zdroj	k1gInSc4	zdroj
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemek	k1gInPc1	pozemek
koupil	koupit	k5eAaPmAgMnS	koupit
Brett	Brett	k1gMnSc1	Brett
McBain	McBain	k1gMnSc1	McBain
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
železnice	železnice	k1gFnSc1	železnice
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
vést	vést	k5eAaImF	vést
touto	tento	k3xDgFnSc7	tento
oblastí	oblast	k1gFnSc7	oblast
kvůli	kvůli	k7c3	kvůli
zajištění	zajištění	k1gNnSc3	zajištění
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
parní	parní	k2eAgFnPc4d1	parní
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
zjistí	zjistit	k5eAaPmIp3nS	zjistit
železniční	železniční	k2eAgMnSc1d1	železniční
magnát	magnát	k1gMnSc1	magnát
Morton	Morton	k1gInSc1	Morton
(	(	kIx(	(
<g/>
Gabriele	Gabriela	k1gFnSc3	Gabriela
Ferzetti	Ferzetti	k1gNnSc2	Ferzetti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pošle	poslat	k5eAaPmIp3nS	poslat
najatého	najatý	k2eAgMnSc4d1	najatý
zabijáka	zabiják	k1gMnSc4	zabiják
Franka	Frank	k1gMnSc4	Frank
(	(	kIx(	(
<g/>
Henry	Henry	k1gMnSc1	Henry
Fonda	Fonda	k1gMnSc1	Fonda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
McBaina	McBaina	k1gMnSc1	McBaina
zastrašil	zastrašit	k5eAaPmAgMnS	zastrašit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Frank	Frank	k1gMnSc1	Frank
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
McBaina	McBaino	k1gNnSc2	McBaino
i	i	k8xC	i
jeho	jeho	k3xOp3gFnPc1	jeho
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
zastřelí	zastřelit	k5eAaPmIp3nP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
nechá	nechat	k5eAaPmIp3nS	nechat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
činu	čin	k1gInSc2	čin
podvržené	podvržený	k2eAgInPc1d1	podvržený
důkazy	důkaz	k1gInPc1	důkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
banditu	bandita	k1gMnSc4	bandita
Cheyenna	Cheyenn	k1gMnSc4	Cheyenn
(	(	kIx(	(
<g/>
Jason	Jason	k1gInSc1	Jason
Robards	Robards	k1gInSc1	Robards
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
gang	gang	k1gInSc1	gang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevuje	objevovat	k5eAaImIp3nS	objevovat
Jill	Jill	k1gInSc1	Jill
(	(	kIx(	(
<g/>
Claudia	Claudia	k1gFnSc1	Claudia
Cardinalová	Cardinalová	k1gFnSc1	Cardinalová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
McBainova	McBainův	k2eAgFnSc1d1	McBainův
novomanželka	novomanželka	k1gFnSc1	novomanželka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
z	z	k7c2	z
New	New	k1gFnSc2	New
Orleansu	Orleans	k1gInSc2	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Shledává	shledávat	k5eAaImIp3nS	shledávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
nová	nový	k2eAgFnSc1d1	nová
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
zdědila	zdědit	k5eAaPmAgFnS	zdědit
důležitý	důležitý	k2eAgInSc4d1	důležitý
pozemek	pozemek	k1gInSc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
přichází	přicházet	k5eAaImIp3nS	přicházet
záhadný	záhadný	k2eAgMnSc1d1	záhadný
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
Bronson	Bronson	k1gMnSc1	Bronson
<g/>
)	)	kIx)	)
hrající	hrající	k2eAgInSc1d1	hrající
podivný	podivný	k2eAgInSc1d1	podivný
motiv	motiv	k1gInSc1	motiv
na	na	k7c4	na
foukací	foukací	k2eAgFnSc4d1	foukací
harmoniku	harmonika	k1gFnSc4	harmonika
(	(	kIx(	(
<g/>
a	a	k8xC	a
protože	protože	k8xS	protože
nikdo	nikdo	k3yNnSc1	nikdo
nezná	znát	k5eNaImIp3nS	znát
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
mu	on	k3xPp3gMnSc3	on
začne	začít	k5eAaPmIp3nS	začít
říkat	říkat	k5eAaImF	říkat
právě	právě	k6eAd1	právě
"	"	kIx"	"
<g/>
Harmonika	harmonika	k1gFnSc1	harmonika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Mortonovi	Mortonův	k2eAgMnPc1d1	Mortonův
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
pan	pan	k1gMnSc1	pan
Vláček	vláčka	k1gFnPc2	vláčka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
pase	pást	k5eAaImIp3nS	pást
po	po	k7c6	po
Frankovi	Frank	k1gMnSc6	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Cheyenne	Cheyennout	k5eAaImIp3nS	Cheyennout
a	a	k8xC	a
Harmonika	harmonika	k1gFnSc1	harmonika
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
sblíží	sblížit	k5eAaPmIp3nS	sblížit
a	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
část	část	k1gFnSc4	část
Frankovy	Frankův	k2eAgFnSc2d1	Frankova
bandy	banda	k1gFnSc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
zradí	zradit	k5eAaPmIp3nS	zradit
a	a	k8xC	a
zajme	zajmout	k5eAaPmIp3nS	zajmout
těžce	těžce	k6eAd1	těžce
nemocného	nemocný	k2eAgMnSc4d1	nemocný
Mortona	Morton	k1gMnSc4	Morton
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
získat	získat	k5eAaPmF	získat
pozemky	pozemek	k1gInPc1	pozemek
patřící	patřící	k2eAgInPc1d1	patřící
nyní	nyní	k6eAd1	nyní
Jill	Jill	k1gInSc4	Jill
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
intimní	intimní	k2eAgInSc1d1	intimní
poměr	poměr	k1gInSc1	poměr
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
donutit	donutit	k5eAaPmF	donutit
usedlost	usedlost	k1gFnSc4	usedlost
prodat	prodat	k5eAaPmF	prodat
ve	v	k7c6	v
fingované	fingovaný	k2eAgFnSc6d1	fingovaná
dražbě	dražba	k1gFnSc6	dražba
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
zabrání	zabránit	k5eAaPmIp3nS	zabránit
Harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dražbu	dražba	k1gFnSc4	dražba
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
díky	díky	k7c3	díky
odměně	odměna	k1gFnSc3	odměna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hledaného	hledaný	k2eAgMnSc4d1	hledaný
zločince	zločinec	k1gMnSc4	zločinec
Cheyenna	Cheyenn	k1gMnSc4	Cheyenn
<g/>
.	.	kIx.	.
</s>
<s>
Harmonikovi	harmonik	k1gMnSc3	harmonik
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
nedaří	dařit	k5eNaImIp3nS	dařit
vyřídit	vyřídit	k5eAaPmF	vyřídit
si	se	k3xPyFc3	se
účty	účet	k1gInPc4	účet
s	s	k7c7	s
Frankem	frank	k1gInSc7	frank
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
mu	on	k3xPp3gMnSc3	on
zachrání	zachránit	k5eAaPmIp3nP	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
mu	on	k3xPp3gMnSc3	on
<g/>
"	"	kIx"	"
ho	on	k3xPp3gInSc4	on
nezabil	zabít	k5eNaPmAgInS	zabít
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Cheyenne	Cheyennout	k5eAaImIp3nS	Cheyennout
mezitím	mezitím	k6eAd1	mezitím
při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svých	svůj	k3xOyFgMnPc2	svůj
kumpánů	kumpánů	k?	kumpánů
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
a	a	k8xC	a
společně	společně	k6eAd1	společně
napadnou	napadnout	k5eAaPmIp3nP	napadnout
Mortonův	Mortonův	k2eAgInSc4d1	Mortonův
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Frank	Frank	k1gMnSc1	Frank
sám	sám	k3xTgMnSc1	sám
přijede	přijet	k5eAaPmIp3nS	přijet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Harmonika	harmonika	k1gFnSc1	harmonika
jde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
pistolnickém	pistolnický	k2eAgInSc6d1	pistolnický
souboji	souboj	k1gInSc6	souboj
Harmonika	harmonik	k1gMnSc2	harmonik
Franka	Frank	k1gMnSc2	Frank
smrtelně	smrtelně	k6eAd1	smrtelně
zraní	zranit	k5eAaPmIp3nS	zranit
a	a	k8xC	a
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
mu	on	k3xPp3gMnSc3	on
připomene	připomenout	k5eAaPmIp3nS	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdysi	kdysi	k6eAd1	kdysi
právě	právě	k9	právě
Frank	Frank	k1gMnSc1	Frank
popravil	popravit	k5eAaPmAgMnS	popravit
jeho	jeho	k3xOp3gMnSc4	jeho
bratra	bratr	k1gMnSc4	bratr
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
foukací	foukací	k2eAgFnSc4d1	foukací
harmoniku	harmonika	k1gFnSc4	harmonika
s	s	k7c7	s
cynickými	cynický	k2eAgNnPc7d1	cynické
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
pěkně	pěkně	k6eAd1	pěkně
bratříčkovi	bratříčkův	k2eAgMnPc1d1	bratříčkův
zahraj	zahrát	k5eAaPmRp2nS	zahrát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úplném	úplný	k2eAgInSc6d1	úplný
závěru	závěr	k1gInSc6	závěr
Harmonika	harmonik	k1gMnSc2	harmonik
a	a	k8xC	a
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
opouštějí	opouštět	k5eAaImIp3nP	opouštět
Jill	Jill	k1gInSc1	Jill
a	a	k8xC	a
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
zranění	zranění	k1gNnPc4	zranění
utrpěná	utrpěný	k2eAgNnPc4d1	utrpěné
při	při	k7c6	při
přepadení	přepadení	k1gNnSc6	přepadení
Mortonova	Mortonův	k2eAgInSc2d1	Mortonův
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
JORDÁN	Jordán	k1gMnSc1	Jordán
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
:	:	kIx,	:
český	český	k2eAgMnSc1d1	český
průvodce	průvodce	k1gMnSc1	průvodce
světovými	světový	k2eAgInPc7d1	světový
westerny	western	k1gInPc7	western
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2507	[number]	k4	2507
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
