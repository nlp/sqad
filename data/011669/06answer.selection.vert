<s>
Palcát	palcát	k1gInSc1	palcát
<g/>
,	,	kIx,	,
nářečně	nářečně	k6eAd1	nářečně
a	a	k8xC	a
zastarale	zastarale	k6eAd1	zastarale
bodzikan	bodzikan	k1gMnSc1	bodzikan
<g/>
,	,	kIx,	,
budzikan	budzikan	k1gMnSc1	budzikan
z	z	k7c2	z
tur.	tur.	k?	tur.
buzdogan	buzdogan	k1gInSc1	buzdogan
<g/>
,	,	kIx,	,
u	u	k7c2	u
kozáků	kozák	k1gMnPc2	kozák
bulava	bulava	k1gFnSc1	bulava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úderná	úderný	k2eAgFnSc1d1	úderná
ruční	ruční	k2eAgFnSc1d1	ruční
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
