<p>
<s>
Palcát	palcát	k1gInSc1	palcát
<g/>
,	,	kIx,	,
nářečně	nářečně	k6eAd1	nářečně
a	a	k8xC	a
zastarale	zastarale	k6eAd1	zastarale
bodzikan	bodzikan	k1gMnSc1	bodzikan
<g/>
,	,	kIx,	,
budzikan	budzikan	k1gMnSc1	budzikan
z	z	k7c2	z
tur.	tur.	k?	tur.
buzdogan	buzdogan	k1gInSc1	buzdogan
<g/>
,	,	kIx,	,
u	u	k7c2	u
kozáků	kozák	k1gMnPc2	kozák
bulava	bulava	k1gFnSc1	bulava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úderná	úderný	k2eAgFnSc1d1	úderná
ruční	ruční	k2eAgFnSc1d1	ruční
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Palcát	palcát	k1gInSc1	palcát
je	být	k5eAaImIp3nS	být
variantou	varianta	k1gFnSc7	varianta
kyje	kyj	k1gInSc2	kyj
a	a	k8xC	a
válečného	válečný	k2eAgNnSc2d1	válečné
kladiva	kladivo	k1gNnSc2	kladivo
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
nimž	jenž	k3xRgMnPc3	jenž
má	mít	k5eAaImIp3nS	mít
výhodu	výhoda	k1gFnSc4	výhoda
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
nutno	nutno	k6eAd1	nutno
jej	on	k3xPp3gNnSc4	on
před	před	k7c7	před
úderem	úder	k1gInSc7	úder
natáčet	natáčet	k5eAaImF	natáčet
do	do	k7c2	do
správného	správný	k2eAgInSc2d1	správný
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
drcení	drcení	k1gNnSc3	drcení
plátů	plát	k1gInPc2	plát
protivníkova	protivníkův	k2eAgNnPc4d1	protivníkovo
brnění	brnění	k1gNnPc4	brnění
<g/>
,	,	kIx,	,
při	při	k7c6	při
úderu	úder	k1gInSc6	úder
na	na	k7c4	na
protivníka	protivník	k1gMnSc4	protivník
v	v	k7c6	v
drátěné	drátěný	k2eAgFnSc6d1	drátěná
košili	košile	k1gFnSc6	košile
či	či	k8xC	či
jiné	jiný	k2eAgFnSc6d1	jiná
lehčí	lehký	k2eAgFnSc6d2	lehčí
zbroji	zbroj	k1gFnSc6	zbroj
působil	působit	k5eAaImAgMnS	působit
zlomeniny	zlomenina	k1gFnPc4	zlomenina
a	a	k8xC	a
zhmožděniny	zhmožděnina	k1gFnPc4	zhmožděnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
užíván	užívat	k5eAaImNgInS	užívat
též	též	k9	též
jako	jako	k8xS	jako
odznak	odznak	k1gInSc4	odznak
moci	moc	k1gFnSc2	moc
nebo	nebo	k8xC	nebo
vojenské	vojenský	k2eAgFnSc2d1	vojenská
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
českých	český	k2eAgMnPc2d1	český
husitů	husita	k1gMnPc2	husita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
u	u	k7c2	u
kozáků	kozák	k1gInPc2	kozák
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bulava	bulava	k1gFnSc1	bulava
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
palcátem	palcát	k1gInSc7	palcát
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
,	,	kIx,	,
mocný	mocný	k2eAgMnSc1d1	mocný
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
v	v	k7c6	v
husitských	husitský	k2eAgFnPc6d1	husitská
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kozácký	kozácký	k2eAgInSc1d1	kozácký
palcát	palcát	k1gInSc1	palcát
===	===	k?	===
</s>
</p>
<p>
<s>
Kozácký	kozácký	k2eAgInSc1d1	kozácký
palcát	palcát	k1gInSc1	palcát
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
piernacz	piernacz	k1gInSc1	piernacz
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
broń	broń	k?	broń
pierzasta	pierzasta	k1gMnSc1	pierzasta
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
obvykle	obvykle	k6eAd1	obvykle
proveden	provést	k5eAaPmNgInS	provést
ze	z	k7c2	z
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
péry	péro	k1gNnPc7	péro
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kozáků	kozák	k1gMnPc2	kozák
byl	být	k5eAaImAgMnS	být
symbolem	symbol	k1gInSc7	symbol
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
kozáckých	kozácký	k2eAgMnPc2d1	kozácký
staršinů	staršina	k1gMnPc2	staršina
na	na	k7c6	na
Záporoží	Záporoží	k1gNnSc6	Záporoží
<g/>
.	.	kIx.	.
</s>
<s>
Plnil	plnit	k5eAaImAgMnS	plnit
také	také	k9	také
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
železného	železný	k2eAgInSc2d1	železný
dopisu	dopis	k1gInSc2	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jej	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
u	u	k7c2	u
sebe	se	k3xPyFc2	se
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
volný	volný	k2eAgInSc4d1	volný
průjezd	průjezd	k1gInSc4	průjezd
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
salvus	salvus	k1gMnSc1	salvus
conductus	conductus	k1gMnSc1	conductus
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedotknutelnost	nedotknutelnost	k1gFnSc1	nedotknutelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kůsa	Kůsa	k1gFnSc1	Kůsa
(	(	kIx(	(
<g/>
válečná	válečný	k2eAgFnSc1d1	válečná
kosa	kosa	k1gFnSc1	kosa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kropáč	kropáč	k1gInSc1	kropáč
</s>
</p>
<p>
<s>
Okovaný	okovaný	k2eAgInSc1d1	okovaný
cep	cep	k1gInSc1	cep
</s>
</p>
<p>
<s>
Valaška	Valaška	k1gFnSc1	Valaška
</s>
</p>
<p>
<s>
Žezlo	žezlo	k1gNnSc1	žezlo
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
palcát	palcát	k1gInSc1	palcát
</s>
</p>
<p>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
zbraně	zbraň	k1gFnPc1	zbraň
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
palcát	palcát	k1gInSc1	palcát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Palcát	palcát	k1gInSc1	palcát
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
