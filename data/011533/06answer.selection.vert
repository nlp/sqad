<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgFnSc1d1	dopravní
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
pěší	pěší	k2eAgFnSc4d1	pěší
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgFnSc4d1	silniční
nebo	nebo	k8xC	nebo
železniční	železniční	k2eAgFnSc4d1	železniční
cestu	cesta	k1gFnSc4	cesta
případně	případně	k6eAd1	případně
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
terénní	terénní	k2eAgFnSc1d1	terénní
nerovnost	nerovnost	k1gFnSc1	nerovnost
(	(	kIx(	(
<g/>
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
rokle	rokle	k1gFnSc1	rokle
<g/>
,	,	kIx,	,
strž	strž	k1gFnSc1	strž
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
