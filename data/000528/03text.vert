<s>
Klášter	klášter	k1gInSc1	klášter
augustiniánů	augustinián	k1gMnPc2	augustinián
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
je	být	k5eAaImIp3nS	být
kompex	kompex	k1gInSc1	kompex
barokních	barokní	k2eAgFnPc2d1	barokní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Husova	Husův	k2eAgFnSc1d1	Husova
213	[number]	k4	213
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hřbitova	hřbitov	k1gInSc2	hřbitov
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
vrchlabské	vrchlabský	k2eAgNnSc1d1	vrchlabské
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
hrabětem	hrabě	k1gMnSc7	hrabě
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
Františkem	František	k1gMnSc7	František
Felixem	Felix	k1gMnSc7	Felix
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
<g/>
,	,	kIx,	,
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
něj	on	k3xPp3gMnSc4	on
povoláni	povolán	k2eAgMnPc1d1	povolán
řeholníci	řeholník	k1gMnPc1	řeholník
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
inaugurace	inaugurace	k1gFnSc1	inaugurace
augustiniánů	augustinián	k1gMnPc2	augustinián
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1714	[number]	k4	1714
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
budována	budovat	k5eAaImNgFnS	budovat
stavitelem	stavitel	k1gMnSc7	stavitel
Matyášem	Matyáš	k1gMnSc7	Matyáš
Auerem	Auer	k1gMnSc7	Auer
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
začala	začít	k5eAaPmAgFnS	začít
stavba	stavba	k1gFnSc1	stavba
konventního	konventní	k2eAgInSc2d1	konventní
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
svatému	svatý	k2eAgMnSc3d1	svatý
Augustinovi	Augustin	k1gMnSc3	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
kláštera	klášter	k1gInSc2	klášter
provozovali	provozovat	k5eAaImAgMnP	provozovat
řeholníci	řeholník	k1gMnPc1	řeholník
lékárnu	lékárna	k1gFnSc4	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
klášterní	klášterní	k2eAgFnSc1d1	klášterní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
areál	areál	k1gInSc1	areál
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
obnoven	obnoven	k2eAgMnSc1d1	obnoven
byl	být	k5eAaImAgMnS	být
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
mělo	mít	k5eAaImAgNnS	mít
celé	celý	k2eAgNnSc4d1	celé
jedno	jeden	k4xCgNnSc4	jeden
patro	patro	k1gNnSc4	patro
pronajaté	pronajatý	k2eAgNnSc4d1	pronajaté
Krkonošské	krkonošský	k2eAgNnSc4d1	Krkonošské
muzeum	muzeum	k1gNnSc4	muzeum
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
sbírky	sbírka	k1gFnPc4	sbírka
a	a	k8xC	a
expozice	expozice	k1gFnPc4	expozice
-	-	kIx~	-
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastěhování	nastěhování	k1gNnSc1	nastěhování
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
úředně	úředně	k6eAd1	úředně
posvěceného	posvěcený	k2eAgInSc2d1	posvěcený
útisku	útisk	k1gInSc2	útisk
řeholníků	řeholník	k1gMnPc2	řeholník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
páter	páter	k1gMnSc1	páter
Johann	Johann	k1gMnSc1	Johann
Fischer	Fischer	k1gMnSc1	Fischer
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
Gestapem	gestapo	k1gNnSc7	gestapo
<g/>
,	,	kIx,	,
zbaven	zbaven	k2eAgInSc1d1	zbaven
svěcení	svěcení	k1gNnSc2	svěcení
a	a	k8xC	a
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byli	být	k5eAaImAgMnP	být
augustiniáni	augustinián	k1gMnPc1	augustinián
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Akce	akce	k1gFnSc2	akce
K	k	k7c3	k
definitivně	definitivně	k6eAd1	definitivně
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
,	,	kIx,	,
připadly	připadnout	k5eAaPmAgFnP	připadnout
muzeu	muzeum	k1gNnSc6	muzeum
všechny	všechen	k3xTgFnPc4	všechen
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
muzea	muzeum	k1gNnSc2	muzeum
provedla	provést	k5eAaPmAgFnS	provést
řadu	řada	k1gFnSc4	řada
necitlivých	citlivý	k2eNgInPc2d1	necitlivý
zásahů	zásah	k1gInPc2	zásah
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ustaly	ustat	k5eAaPmAgFnP	ustat
až	až	k6eAd1	až
převodem	převod	k1gInSc7	převod
muzea	muzeum	k1gNnSc2	muzeum
pod	pod	k7c4	pod
Správu	správa	k1gFnSc4	správa
KRNAP	KRNAP	kA	KRNAP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
sklepních	sklepní	k2eAgFnPc2d1	sklepní
prostor	prostora	k1gFnPc2	prostora
využíval	využívat	k5eAaImAgInS	využívat
státní	státní	k2eAgInSc1d1	státní
statek	statek	k1gInSc1	statek
k	k	k7c3	k
uskladněné	uskladněný	k2eAgFnSc3d1	uskladněná
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
přímém	přímý	k2eAgNnSc6d1	přímé
sousedství	sousedství	k1gNnSc6	sousedství
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
postihla	postihnout	k5eAaPmAgFnS	postihnout
budovy	budova	k1gFnPc4	budova
stavební	stavební	k2eAgFnSc2d1	stavební
havárie	havárie	k1gFnSc2	havárie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
masa	masa	k1gFnSc1	masa
sněhu	sníh	k1gInSc2	sníh
sjela	sjet	k5eAaPmAgFnS	sjet
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
kostela	kostel	k1gInSc2	kostel
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
konventu	konvent	k1gInSc2	konvent
a	a	k8xC	a
významně	významně	k6eAd1	významně
ji	on	k3xPp3gFnSc4	on
poškodila	poškodit	k5eAaPmAgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
uzavřeny	uzavřen	k2eAgInPc4d1	uzavřen
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
postupná	postupný	k2eAgFnSc1d1	postupná
<g/>
,	,	kIx,	,
důkladná	důkladný	k2eAgFnSc1d1	důkladná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nijak	nijak	k6eAd1	nijak
rychlá	rychlý	k2eAgFnSc1d1	rychlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
:	:	kIx,	:
muzeum	muzeum	k1gNnSc1	muzeum
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
zčásti	zčásti	k6eAd1	zčásti
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
a	a	k8xC	a
jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
byl	být	k5eAaImAgInS	být
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
opraven	opravna	k1gFnPc2	opravna
klášterní	klášterní	k2eAgInSc4d1	klášterní
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
církevních	církevní	k2eAgFnPc2d1	církevní
restitucí	restituce	k1gFnPc2	restituce
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
vrácen	vrátit	k5eAaPmNgInS	vrátit
řádu	řád	k1gInSc2	řád
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gInSc4	on
však	však	k9	však
vzápětí	vzápětí	k6eAd1	vzápětí
prodal	prodat	k5eAaPmAgInS	prodat
dosavadnímu	dosavadní	k2eAgMnSc3d1	dosavadní
uživateli	uživatel	k1gMnSc3	uživatel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Správě	správa	k1gFnSc6	správa
KRNAP	KRNAP	kA	KRNAP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budovách	budova	k1gFnPc6	budova
kláštera	klášter	k1gInSc2	klášter
tedy	tedy	k8xC	tedy
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
muzeum	muzeum	k1gNnSc1	muzeum
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
kostele	kostel	k1gInSc6	kostel
na	na	k7c4	na
kůru	kůra	k1gFnSc4	kůra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
varhany	varhany	k1gInPc1	varhany
firmy	firma	k1gFnSc2	firma
Gebrüder	Gebrüder	k1gMnSc1	Gebrüder
Rieger	Rieger	k1gMnSc1	Rieger
Jägerndorf	Jägerndorf	k1gMnSc1	Jägerndorf
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
666	[number]	k4	666
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgInPc1d1	postavený
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
kostela	kostel	k1gInSc2	kostel
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
postaveny	postaven	k2eAgInPc4d1	postaven
zrekonstruované	zrekonstruovaný	k2eAgInPc4d1	zrekonstruovaný
dvoumanuálové	dvoumanuálový	k2eAgInPc4d1	dvoumanuálový
varhany	varhany	k1gInPc4	varhany
Amanda	Amand	k1gMnSc2	Amand
Hanische	Hanisch	k1gMnSc2	Hanisch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Varhaníkem	varhaník	k1gMnSc7	varhaník
v	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
Radek	Radek	k1gMnSc1	Radek
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
města	město	k1gNnSc2	město
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xC	jako
obřadní	obřadní	k2eAgFnSc1d1	obřadní
a	a	k8xC	a
koncertní	koncertní	k2eAgFnSc1d1	koncertní
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Konvent	konvent	k1gInSc1	konvent
vlastní	vlastní	k2eAgFnSc1d1	vlastní
Správa	správa	k1gFnSc1	správa
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
umístěno	umístit	k5eAaPmNgNnS	umístit
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
Kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
život	život	k1gInSc1	život
a	a	k8xC	a
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
kostele	kostel	k1gInSc6	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rodová	rodový	k2eAgFnSc1d1	rodová
hrobka	hrobka	k1gFnSc1	hrobka
hraběcího	hraběcí	k2eAgInSc2d1	hraběcí
rodu	rod	k1gInSc2	rod
Morzinů	Morzin	k1gInPc2	Morzin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodové	rodový	k2eAgFnSc6d1	rodová
hrobce	hrobka	k1gFnSc6	hrobka
je	být	k5eAaImIp3nS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
také	také	k9	také
veliký	veliký	k2eAgMnSc1d1	veliký
mecenáš	mecenáš	k1gMnSc1	mecenáš
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
italský	italský	k2eAgMnSc1d1	italský
skladatel	skladatel	k1gMnSc1	skladatel
Antonio	Antonio	k1gMnSc1	Antonio
Vivaldi	Vivald	k1gMnPc1	Vivald
dedikoval	dedikovat	k5eAaBmAgMnS	dedikovat
některé	některý	k3yIgFnPc4	některý
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
jeho	jeho	k3xOp3gFnSc4	jeho
Il	Il	k1gFnSc4	Il
cimento	cimento	k1gNnSc4	cimento
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
armonia	armonium	k1gNnSc2	armonium
e	e	k0	e
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
inventione	invention	k1gInSc5	invention
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
slavné	slavný	k2eAgInPc4d1	slavný
koncerty	koncert	k1gInPc4	koncert
Čtvero	čtvero	k4xRgNnSc1	čtvero
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
