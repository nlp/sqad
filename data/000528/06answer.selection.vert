<s>
Klášter	klášter	k1gInSc1	klášter
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
hrabětem	hrabě	k1gMnSc7	hrabě
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
Františkem	František	k1gMnSc7	František
Felixem	Felix	k1gMnSc7	Felix
z	z	k7c2	z
Morzinu	Morzin	k1gInSc2	Morzin
<g/>
,	,	kIx,	,
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
něj	on	k3xPp3gMnSc4	on
povoláni	povolán	k2eAgMnPc1d1	povolán
řeholníci	řeholník	k1gMnPc1	řeholník
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
inaugurace	inaugurace	k1gFnSc1	inaugurace
augustiniánů	augustinián	k1gMnPc2	augustinián
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1714	[number]	k4	1714
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
.	.	kIx.	.
</s>
