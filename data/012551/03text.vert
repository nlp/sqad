<p>
<s>
Alerion	Alerion	k1gInSc1	Alerion
je	být	k5eAaImIp3nS	být
obecná	obecný	k2eAgFnSc1d1	obecná
heraldická	heraldický	k2eAgFnSc1d1	heraldická
figura	figura	k1gFnSc1	figura
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ptáka	pták	k1gMnSc2	pták
s	s	k7c7	s
rozepjatými	rozepjatý	k2eAgNnPc7d1	rozepjaté
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
erbovníku	erbovník	k1gMnSc6	erbovník
Armorial	Armorial	k1gInSc4	Armorial
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Urfe	Urf	k1gMnSc2	Urf
(	(	kIx(	(
<g/>
1380	[number]	k4	1380
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xS	jako
orel	orel	k1gMnSc1	orel
beze	beze	k7c2	beze
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
zobáku	zobák	k1gInSc2	zobák
a	a	k8xC	a
drápů	dráp	k1gInPc2	dráp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
heraldice	heraldika	k1gFnSc6	heraldika
je	být	k5eAaImIp3nS	být
alerion	alerion	k1gInSc1	alerion
jako	jako	k8xC	jako
figura	figura	k1gFnSc1	figura
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
až	až	k9	až
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
dynastie	dynastie	k1gFnSc2	dynastie
Habsbursko-Lotrinské	habsburskootrinský	k2eAgFnSc2d1	habsbursko-lotrinská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předobraz	předobraz	k1gInSc4	předobraz
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
ptáka	pták	k1gMnSc2	pták
aleriona	alerion	k1gMnSc2	alerion
popisuje	popisovat	k5eAaImIp3nS	popisovat
evangelista	evangelista	k1gMnSc1	evangelista
Jan	Jan	k1gMnSc1	Jan
nebo	nebo	k8xC	nebo
také	také	k9	také
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Beauvais	Beauvais	k1gInSc4	Beauvais
zvaný	zvaný	k2eAgInSc4d1	zvaný
le	le	k?	le
Picard	Picard	k1gInSc4	Picard
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Bestiaire	Bestiair	k1gInSc5	Bestiair
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
prý	prý	k9	prý
žije	žít	k5eAaImIp3nS	žít
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
alerionů	alerion	k1gInPc2	alerion
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
větších	veliký	k2eAgFnPc2d2	veliký
než	než	k8xS	než
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
60	[number]	k4	60
letech	léto	k1gNnPc6	léto
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc1	samice
dvě	dva	k4xCgNnPc4	dva
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
sedí	sedit	k5eAaImIp3nS	sedit
60	[number]	k4	60
dní	den	k1gInPc2	den
a	a	k8xC	a
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
rodiče	rodič	k1gMnPc1	rodič
odletí	odletět	k5eAaPmIp3nP	odletět
nad	nad	k7c4	nad
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vrhnou	vrhnout	k5eAaPmIp3nP	vrhnout
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
utopí	utopit	k5eAaPmIp3nP	utopit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
osiřelá	osiřelý	k2eAgNnPc4d1	osiřelé
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
krmí	krmit	k5eAaImIp3nP	krmit
je	on	k3xPp3gFnPc4	on
a	a	k8xC	a
hlídají	hlídat	k5eAaImIp3nP	hlídat
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
mohou	moct	k5eAaImIp3nP	moct
mláďata	mládě	k1gNnPc1	mládě
sama	sám	k3xTgFnSc1	sám
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
užívaly	užívat	k5eAaImAgFnP	užívat
tuto	tento	k3xDgFnSc4	tento
heraldickou	heraldický	k2eAgFnSc4d1	heraldická
figuru	figura	k1gFnSc4	figura
zejména	zejména	k9	zejména
rody	rod	k1gInPc1	rod
Lotrinských	lotrinský	k2eAgInPc2d1	lotrinský
a	a	k8xC	a
Montmorency	Montmorenc	k1gMnPc4	Montmorenc
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
erb	erb	k1gInSc1	erb
rodiny	rodina	k1gFnSc2	rodina
Montmorency	Montmorenca	k1gFnSc2	Montmorenca
<g/>
,	,	kIx,	,
pyšnící	pyšnící	k2eAgFnSc2d1	pyšnící
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
aleriony	alerion	k1gInPc4	alerion
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
součástí	součást	k1gFnSc7	součást
znaků	znak	k1gInPc2	znak
francouzských	francouzský	k2eAgInPc2d1	francouzský
měst	město	k1gNnPc2	město
Bessancourt	Bessancourta	k1gFnPc2	Bessancourta
<g/>
,	,	kIx,	,
Deuil	Deuila	k1gFnPc2	Deuila
la	la	k0	la
Barre	Barr	k1gMnSc5	Barr
<g/>
,	,	kIx,	,
Eaubonne	Eaubonn	k1gMnSc5	Eaubonn
<g/>
,	,	kIx,	,
Écouen	Écouen	k1gInSc1	Écouen
<g/>
,	,	kIx,	,
Ézanville	Ézanville	k1gInSc1	Ézanville
<g/>
,	,	kIx,	,
Montsoult	Montsoult	k1gMnSc1	Montsoult
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Plessis-Bouchard	Plessis-Bouchard	k1gMnSc1	Plessis-Bouchard
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Leu	Lea	k1gFnSc4	Lea
la	la	k1gNnSc1	la
Fort	Fort	k?	Fort
<g/>
,	,	kIx,	,
Taverny	taverna	k1gFnSc2	taverna
a	a	k8xC	a
Montmorency	Montmorenca	k1gFnSc2	Montmorenca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAROCH	BAROCH	kA	BAROCH
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Nadpřirozené	nadpřirozený	k2eAgFnPc1d1	nadpřirozená
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
monstra	monstrum	k1gNnPc1	monstrum
a	a	k8xC	a
bájní	bájný	k2eAgMnPc1d1	bájný
tvorové	tvor	k1gMnPc1	tvor
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Genealogie	genealogie	k1gFnSc2	genealogie
a	a	k8xC	a
Heraldika	heraldik	k1gMnSc2	heraldik
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PALIVEC	PALIVEC	kA	PALIVEC
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
.	.	kIx.	.
</s>
<s>
Heraldická	heraldický	k2eAgFnSc1d1	heraldická
symbolika	symbolika	k1gFnSc1	symbolika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Heraldika	heraldika	k1gFnSc1	heraldika
</s>
</p>
<p>
<s>
Genealogie	genealogie	k1gFnSc1	genealogie
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alerion	Alerion	k1gInSc1	Alerion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Alerion	Alerion	k1gInSc1	Alerion
–	–	k?	–
článek	článek	k1gInSc1	článek
Co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
alerion	alerion	k1gInSc1	alerion
<g/>
?	?	kIx.	?
</s>
</p>
