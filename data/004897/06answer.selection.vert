<s>
Mezi	mezi	k7c7	mezi
sebevrahy	sebevrah	k1gMnPc7	sebevrah
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
