<p>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
KHL	KHL	kA	KHL
<g/>
,	,	kIx,	,
К	К	k?	К
х	х	k?	х
л	л	k?	л
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
profesionální	profesionální	k2eAgFnSc1d1	profesionální
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
,	,	kIx,	,
ruskou	ruský	k2eAgFnSc4d1	ruská
superligu	superliga	k1gFnSc4	superliga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
prémiovou	prémiový	k2eAgFnSc4d1	prémiová
ligu	liga	k1gFnSc4	liga
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
KHL	KHL	kA	KHL
je	být	k5eAaImIp3nS	být
složena	složen	k2eAgFnSc1d1	složena
z	z	k7c2	z
24	[number]	k4	24
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
19	[number]	k4	19
<g/>
,	,	kIx,	,
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
<g/>
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
zde	zde	k6eAd1	zde
nehraje	hrát	k5eNaImIp3nS	hrát
už	už	k6eAd1	už
slovenský	slovenský	k2eAgInSc1d1	slovenský
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Slovan	Slovan	k1gInSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zkrachoval	zkrachovat	k5eAaPmAgInS	zkrachovat
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
KHL	KHL	kA	KHL
ruští	ruský	k2eAgMnPc1d1	ruský
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
i	i	k9	i
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
hokejové	hokejový	k2eAgInPc1d1	hokejový
kluby	klub	k1gInPc1	klub
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
hokeji	hokej	k1gInSc6	hokej
jsou	být	k5eAaImIp3nP	být
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Dynamo	dynamo	k1gNnSc4	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
SKA	SKA	kA	SKA
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
premiérové	premiérový	k2eAgFnSc6d1	premiérová
sezóně	sezóna	k1gFnSc6	sezóna
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Ak	Ak	k1gMnSc1	Ak
Bars	Bars	k?	Bars
Kazaň	Kazaň	k1gFnSc4	Kazaň
nad	nad	k7c7	nad
Lokomotivem	Lokomotiv	k1gInSc7	Lokomotiv
Jaroslavl	Jaroslavl	k1gFnSc2	Jaroslavl
symbolicky	symbolicky	k6eAd1	symbolicky
v	v	k7c4	v
den	den	k1gInSc4	den
letu	let	k1gInSc2	let
Jurije	Jurije	k1gMnSc1	Jurije
Gagarina	Gagarina	k1gMnSc1	Gagarina
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zvedl	zvednout	k5eAaPmAgMnS	zvednout
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Alexej	Alexej	k1gMnSc1	Alexej
Morozov	Morozov	k1gInSc4	Morozov
Gagarinův	Gagarinův	k2eAgInSc4d1	Gagarinův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
KHL	KHL	kA	KHL
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
celkovou	celkový	k2eAgFnSc4d1	celková
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
5,32	[number]	k4	5,32
miliony	milion	k4xCgInPc1	milion
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
sezóny	sezóna	k1gFnSc2	sezóna
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
struktura	struktura	k1gFnSc1	struktura
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
kluby	klub	k1gInPc1	klub
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
podle	podle	k7c2	podle
geografické	geografický	k2eAgFnSc2d1	geografická
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc1	dva
konference	konference	k1gFnPc1	konference
–	–	k?	–
Východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
Západní	západní	k2eAgFnSc3d1	západní
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
konference	konference	k1gFnSc1	konference
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
hraje	hrát	k5eAaImIp3nS	hrát
minimálně	minimálně	k6eAd1	minimálně
dvě	dva	k4xCgFnPc1	dva
hry	hra	k1gFnPc1	hra
proti	proti	k7c3	proti
každému	každý	k3xTgInSc3	každý
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
play-off	playff	k1gInSc4	play-off
hraje	hrát	k5eAaImIp3nS	hrát
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
osm	osm	k4xCc4	osm
klubů	klub	k1gInPc2	klub
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
konference	konference	k1gFnSc2	konference
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
vítězné	vítězný	k2eAgInPc4d1	vítězný
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
vítězové	vítěz	k1gMnPc1	vítěz
hrají	hrát	k5eAaImIp3nP	hrát
o	o	k7c4	o
Gagarinův	Gagarinův	k2eAgInSc4d1	Gagarinův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráči	hráč	k1gMnPc1	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
klubech	klub	k1gInPc6	klub
KHL	KHL	kA	KHL
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
jen	jen	k9	jen
pět	pět	k4xCc4	pět
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
může	moct	k5eAaImIp3nS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
ruské	ruský	k2eAgNnSc1d1	ruské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
minulé	minulý	k2eAgFnSc2d1	minulá
sezóny	sezóna	k1gFnSc2	sezóna
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
hráči	hráč	k1gMnPc1	hráč
minimálně	minimálně	k6eAd1	minimálně
třinácti	třináct	k4xCc2	třináct
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
vede	vést	k5eAaImIp3nS	vést
KHL	KHL	kA	KHL
spor	spor	k1gInSc1	spor
s	s	k7c7	s
NHL	NHL	kA	NHL
ohledně	ohledně	k7c2	ohledně
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nedovoleně	dovoleně	k6eNd1	dovoleně
odejdou	odejít	k5eAaPmIp3nP	odejít
buď	buď	k8xC	buď
z	z	k7c2	z
KHL	KHL	kA	KHL
nebo	nebo	k8xC	nebo
z	z	k7c2	z
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
týká	týkat	k5eAaImIp3nS	týkat
je	on	k3xPp3gNnSc4	on
Alexandr	Alexandr	k1gMnSc1	Alexandr
Radulov	Radulov	k1gInSc1	Radulov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
utekl	utéct	k5eAaPmAgMnS	utéct
ze	z	k7c2	z
zámořského	zámořský	k2eAgInSc2d1	zámořský
Nashvillu	Nashvill	k1gInSc2	Nashvill
Predators	Predatorsa	k1gFnPc2	Predatorsa
do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
Ufy	Ufy	k1gFnSc2	Ufy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
ligami	liga	k1gFnPc7	liga
vypořádán	vypořádán	k2eAgMnSc1d1	vypořádán
<g/>
,	,	kIx,	,
když	když	k8xS	když
obě	dva	k4xCgFnPc1	dva
podepsali	podepsat	k5eAaPmAgMnP	podepsat
novou	nový	k2eAgFnSc4d1	nová
dohodu	dohoda	k1gFnSc4	dohoda
ke	k	k7c3	k
ctění	ctění	k1gNnSc3	ctění
jejich	jejich	k3xOp3gFnPc2	jejich
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
složení	složení	k1gNnSc2	složení
týmů	tým	k1gInPc2	tým
==	==	k?	==
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
–	–	k?	–
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
klubů	klub	k1gInPc2	klub
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
–	–	k?	–
rozdělení	rozdělení	k1gNnSc4	rozdělení
ligy	liga	k1gFnSc2	liga
na	na	k7c6	na
Východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
Západní	západní	k2eAgFnSc6d1	západní
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
Avtomobilist	Avtomobilist	k1gMnSc1	Avtomobilist
Jekatěrinburg	Jekatěrinburg	k1gMnSc1	Jekatěrinburg
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
soutěže	soutěž	k1gFnSc2	soutěž
(	(	kIx(	(
<g/>
VHL	VHL	kA	VHL
<g/>
)	)	kIx)	)
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
Chimik	Chimik	k1gMnSc1	Chimik
Voskresensk	Voskresensk	k1gInSc4	Voskresensk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
–	–	k?	–
spojením	spojení	k1gNnSc7	spojení
HK	HK	kA	HK
Dynamo	dynamo	k1gNnSc4	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
HC	HC	kA	HC
MVD	MVD	kA	MVD
Balašicha	Balašich	k1gMnSc2	Balašich
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
OHK	OHK	kA	OHK
Dynamo	dynamo	k1gNnSc1	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
HC	HC	kA	HC
Jugra	Jugra	k1gMnSc1	Jugra
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1	Chanty-Mansijsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
–	–	k?	–
připojil	připojit	k5eAaPmAgInS	připojit
se	se	k3xPyFc4	se
slovenský	slovenský	k2eAgInSc1d1	slovenský
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Poprad	Poprad	k1gInSc1	Poprad
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
sezónu	sezóna	k1gFnSc4	sezóna
vynechal	vynechat	k5eAaPmAgMnS	vynechat
Lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Jaroslavl	Jaroslavl	k1gFnSc2	Jaroslavl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
–	–	k?	–
připojil	připojit	k5eAaPmAgInS	připojit
se	se	k3xPyFc4	se
slovenský	slovenský	k2eAgInSc1d1	slovenský
HC	HC	kA	HC
Slovan	Slovan	k1gInSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
HC	HC	kA	HC
Donbass	Donbass	k1gInSc1	Donbass
Doněck	Doněck	k1gInSc1	Doněck
<g/>
,	,	kIx,	,
Lev	lev	k1gInSc1	lev
v	v	k7c6	v
Popradu	Poprad	k1gInSc6	Poprad
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
klub	klub	k1gInSc1	klub
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
–	–	k?	–
připojil	připojit	k5eAaPmAgInS	připojit
se	se	k3xPyFc4	se
chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
Medveščak	Medveščak	k1gMnSc1	Medveščak
a	a	k8xC	a
Admiral	Admiral	k1gMnSc1	Admiral
Vladivostok	Vladivostok	k1gInSc4	Vladivostok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
–	–	k?	–
připojil	připojit	k5eAaPmAgInS	připojit
se	se	k3xPyFc4	se
finský	finský	k2eAgInSc1d1	finský
Jokerit	Jokerit	k1gInSc1	Jokerit
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Lada	Lada	k1gFnSc1	Lada
Togliatti	Togliatť	k1gFnSc2	Togliatť
(	(	kIx(	(
<g/>
v	v	k7c6	v
KHL	KHL	kA	KHL
vynechala	vynechat	k5eAaPmAgFnS	vynechat
čtyři	čtyři	k4xCgFnPc4	čtyři
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
)	)	kIx)	)
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
tým	tým	k1gInSc4	tým
HK	HK	kA	HK
Soči	Soči	k1gNnSc1	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
z	z	k7c2	z
ligy	liga	k1gFnSc2	liga
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
kvůli	kvůli	k7c3	kvůli
proruským	proruský	k2eAgInPc3d1	proruský
nepokojům	nepokoj	k1gInPc3	nepokoj
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
HC	HC	kA	HC
Donbass	Donbass	k1gInSc1	Donbass
Doněck	Doněck	k1gInSc1	Doněck
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgInPc3d1	finanční
problémům	problém	k1gInPc3	problém
i	i	k9	i
HC	HC	kA	HC
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
–	–	k?	–
končí	končit	k5eAaImIp3nS	končit
Atlant	atlant	k1gInSc1	atlant
Mytišči	Mytišč	k1gFnSc3	Mytišč
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
do	do	k7c2	do
ligy	liga	k1gFnSc2	liga
vrací	vracet	k5eAaImIp3nS	vracet
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
–	–	k?	–
připojil	připojit	k5eAaPmAgInS	připojit
se	se	k3xPyFc4	se
čínský	čínský	k2eAgInSc1d1	čínský
tým	tým	k1gInSc1	tým
HC	HC	kA	HC
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Kunlun	Kunluna	k1gFnPc2	Kunluna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
–	–	k?	–
z	z	k7c2	z
ligy	liga	k1gFnSc2	liga
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
Medveščak	Medveščak	k1gInSc1	Medveščak
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
a	a	k8xC	a
Metallurg	Metallurg	k1gMnSc1	Metallurg
Novokuzněck	Novokuzněck	k1gMnSc1	Novokuzněck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
–	–	k?	–
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
byly	být	k5eAaImAgFnP	být
nejslabší	slabý	k2eAgInPc4d3	nejslabší
celky	celek	k1gInPc4	celek
HC	HC	kA	HC
Jugra	Jugra	k1gFnSc1	Jugra
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1	Chanty-Mansijsk
a	a	k8xC	a
HC	HC	kA	HC
Lada	Lada	k1gFnSc1	Lada
Togliatti	Togliatť	k1gFnSc2	Togliatť
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zápasů	zápas	k1gInPc2	zápas
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
z	z	k7c2	z
56	[number]	k4	56
na	na	k7c4	na
62	[number]	k4	62
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
•	•	k?	•
<g/>
Sezóna	sezóna	k1gFnSc1	sezóna
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
2020	[number]	k4	2020
-	-	kIx~	-
odešel	odejít	k5eAaPmAgMnS	odejít
kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgInPc3d1	finanční
problémům	problém	k1gInPc3	problém
HC	HC	kA	HC
Slovan	Slovan	k1gInSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
byla	být	k5eAaImAgFnS
předčasně	předčasně	k6eAd1
ukončena	ukončit	k5eAaPmNgFnS
po	po	k7c6
základní	základní	k2eAgFnSc6d1
části	část	k1gFnSc6
z	z	k7c2
důvodu	důvod	k1gInSc2
pandemie	pandemie	k1gFnSc2
koronaviru	koronavir	k1gInSc2
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
sezón	sezóna	k1gFnPc2	sezóna
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Trofeje	trofej	k1gInPc4	trofej
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Týmové	týmový	k2eAgFnPc1d1	týmová
trofeje	trofej	k1gFnPc1	trofej
===	===	k?	===
</s>
</p>
<p>
<s>
Gagarinův	Gagarinův	k2eAgInSc1d1	Gagarinův
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
–	–	k?	–
udělován	udělován	k2eAgInSc4d1	udělován
vítězi	vítěz	k1gMnSc6	vítěz
play-off	playff	k1gMnSc1	play-off
Kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
</s>
</p>
<p>
<s>
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
–	–	k?	–
udělován	udělován	k2eAgInSc4d1	udělován
vítězi	vítěz	k1gMnPc7	vítěz
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
KHL	KHL	kA	KHL
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
Západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
–	–	k?	–
udělován	udělovat	k5eAaImNgInS	udělovat
týmu	tým	k1gInSc3	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
play-off	playff	k1gInSc1	play-off
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
Východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
–	–	k?	–
udělován	udělovat	k5eAaImNgInS	udělovat
týmu	tým	k1gInSc3	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
play-off	playff	k1gInSc1	play-off
z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
Lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
–	–	k?	–
pohárový	pohárový	k2eAgMnSc1d1	pohárový
vítěz	vítěz	k1gMnSc1	vítěz
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
vicemistrem	vicemistr	k1gMnSc7	vicemistr
v	v	k7c4	v
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
následující	následující	k2eAgFnSc2d1	následující
sezóny	sezóna	k1gFnSc2	sezóna
</s>
</p>
<p>
<s>
Trofej	trofej	k1gFnSc1	trofej
Vsevoloda	Vsevoloda	k1gFnSc1	Vsevoloda
Bobrova	bobrův	k2eAgFnSc1d1	Bobrova
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Individuální	individuální	k2eAgFnPc1d1	individuální
trofeje	trofej	k1gFnPc1	trofej
===	===	k?	===
</s>
</p>
<p>
<s>
Nejproduktivnější	produktivní	k2eAgMnSc1d3	nejproduktivnější
hráč	hráč	k1gMnSc1	hráč
–	–	k?	–
hráč	hráč	k1gMnSc1	hráč
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
</s>
</p>
<p>
<s>
Nejužitečnější	užitečný	k2eAgMnSc1d3	nejužitečnější
hráč	hráč	k1gMnSc1	hráč
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
v	v	k7c6	v
plus	plus	k1gNnSc6	plus
<g/>
/	/	kIx~	/
<g/>
minus	minus	k1gInSc1	minus
bodování	bodování	k1gNnSc2	bodování
</s>
</p>
<p>
<s>
Obránce	obránce	k1gMnSc1	obránce
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hokejka	hokejka	k1gFnSc1	hokejka
–	–	k?	–
udělována	udělovat	k5eAaImNgFnS	udělovat
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
KHL	KHL	kA	KHL
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
play-off	playff	k1gMnSc1	play-off
–	–	k?	–
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
v	v	k7c4	v
play-off	playff	k1gInSc4	play-off
</s>
</p>
<p>
<s>
Trofej	trofej	k1gFnSc1	trofej
Alexeje	Alexej	k1gMnSc2	Alexej
Čerepanova	Čerepanův	k2eAgFnSc1d1	Čerepanova
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
nováček	nováček	k1gMnSc1	nováček
KHL	KHL	kA	KHL
</s>
</p>
<p>
<s>
Železný	železný	k2eAgMnSc1d1	železný
muž	muž	k1gMnSc1	muž
–	–	k?	–
udělována	udělovat	k5eAaImNgFnS	udělovat
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
třech	tři	k4xCgFnPc6	tři
sezonách	sezona	k1gFnPc6	sezona
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
KHL	KHL	kA	KHL
</s>
</p>
<p>
<s>
Gentleman	gentleman	k1gMnSc1	gentleman
na	na	k7c6	na
ledě	led	k1gInSc6	led
–	–	k?	–
útočník	útočník	k1gMnSc1	útočník
nebo	nebo	k8xC	nebo
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
který	který	k3yIgMnSc1	který
nejlépe	dobře	k6eAd3	dobře
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
dovednosti	dovednost	k1gFnPc4	dovednost
a	a	k8xC	a
slušné	slušný	k2eAgNnSc4d1	slušné
chování	chování	k1gNnSc4	chování
na	na	k7c6	na
ledě	led	k1gInSc6	led
</s>
</p>
<p>
<s>
Sekundová	sekundový	k2eAgFnSc1d1	sekundová
trofej	trofej	k1gFnSc1	trofej
–	–	k?	–
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
a	a	k8xC	a
nejpozdnější	pozdný	k2eAgInSc4d3	nejpozdnější
gól	gól	k1gInSc4	gól
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
</s>
</p>
<p>
<s>
Trofej	trofej	k1gInSc1	trofej
za	za	k7c4	za
věrnost	věrnost	k1gFnSc4	věrnost
hokeji	hokej	k1gInSc6	hokej
–	–	k?	–
udělovány	udělovat	k5eAaImNgFnP	udělovat
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hokejistou	hokejista	k1gMnSc7	hokejista
veteránovi	veterán	k1gMnSc6	veterán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
trojka	trojka	k1gFnSc1	trojka
–	–	k?	–
udělována	udělovat	k5eAaImNgFnS	udělovat
nejúčinnějšímu	účinný	k2eAgNnSc3d3	nejúčinnější
útoku	útok	k1gInSc3	útok
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
přilba	přilba	k1gFnSc1	přilba
–	–	k?	–
trofej	trofej	k1gInSc1	trofej
odměňující	odměňující	k2eAgInSc1d1	odměňující
"	"	kIx"	"
<g/>
All-Star	All-Star	k1gInSc1	All-Star
<g/>
"	"	kIx"	"
tým	tým	k1gInSc1	tým
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
brankáře	brankář	k1gMnSc4	brankář
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc4	dva
obránce	obránce	k1gMnPc4	obránce
a	a	k8xC	a
tři	tři	k4xCgMnPc4	tři
útočníky	útočník	k1gMnPc4	útočník
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
trenér	trenér	k1gMnSc1	trenér
–	–	k?	–
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
</s>
</p>
<p>
<s>
Trofej	trofej	k1gFnSc1	trofej
Valentina	Valentina	k1gFnSc1	Valentina
Syče	syčet	k5eAaImSgInS	syčet
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
manažer	manažer	k1gMnSc1	manažer
klubu	klub	k1gInSc2	klub
</s>
</p>
<p>
<s>
Trofej	trofej	k1gFnSc1	trofej
Andreje	Andrej	k1gMnSc2	Andrej
Starovojtova	Starovojtův	k2eAgFnSc1d1	Starovojtův
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
</s>
</p>
<p>
<s>
Trofej	trofej	k1gFnSc1	trofej
Dmitrije	Dmitrije	k1gFnSc2	Dmitrije
Ryžkova	Ryžkov	k1gInSc2	Ryžkov
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovní	sportovní	k2eAgMnSc1d1	sportovní
reportér	reportér	k1gMnSc1	reportér
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
televizní	televizní	k2eAgMnSc1d1	televizní
komentátor	komentátor	k1gMnSc1	komentátor
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hlasatel	hlasatel	k1gMnSc1	hlasatel
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgInSc1d3	nejlepší
TV	TV	kA	TV
–	–	k?	–
udělována	udělován	k2eAgFnSc1d1	udělována
stanici	stanice	k1gFnSc3	stanice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
významně	významně	k6eAd1	významně
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
pokrytí	pokrytí	k1gNnSc3	pokrytí
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
hokeje	hokej	k1gInSc2	hokej
</s>
</p>
<p>
<s>
==	==	k?	==
Kluby	klub	k1gInPc1	klub
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Západní	západní	k2eAgFnPc1d1	západní
konference	konference	k1gFnPc1	konference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Účastníci	účastník	k1gMnPc1	účastník
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
2020	[number]	k4	2020
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Televize	televize	k1gFnSc1	televize
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2013	[number]	k4	2013
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
na	na	k7c6	na
KHL	KHL	kA	KHL
výhradní	výhradní	k2eAgFnSc1d1	výhradní
práva	práv	k2eAgFnSc1d1	práva
televize	televize	k1gFnSc1	televize
Sport	sport	k1gInSc1	sport
1	[number]	k4	1
a	a	k8xC	a
Sport	sport	k1gInSc1	sport
2	[number]	k4	2
maďarské	maďarský	k2eAgFnSc2d1	maďarská
společnosti	společnost	k1gFnSc2	společnost
AMC	AMC	kA	AMC
Networks	Networks	k1gInSc1	Networks
International	International	k1gMnSc5	International
Central	Central	k1gMnSc5	Central
Europe	Europ	k1gMnSc5	Europ
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Chello	Chella	k1gMnSc5	Chella
Central	Central	k1gMnSc5	Central
Europe	Europ	k1gMnSc5	Europ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vysílala	vysílat	k5eAaImAgFnS	vysílat
vybrané	vybraný	k2eAgInPc4d1	vybraný
zápasy	zápas	k1gInPc4	zápas
i	i	k8xC	i
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
a	a	k8xC	a
Fanda	Fanda	k1gFnSc1	Fanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
jsou	být	k5eAaImIp3nP	být
zápasy	zápas	k1gInPc1	zápas
vysílány	vysílat	k5eAaImNgInP	vysílat
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
televizním	televizní	k2eAgInSc6d1	televizní
kanále	kanál	k1gInSc6	kanál
KHL-TV	KHL-TV	k1gFnSc2	KHL-TV
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
sledovat	sledovat	k5eAaImF	sledovat
také	také	k9	také
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
(	(	kIx(	(
<g/>
tv	tv	k?	tv
<g/>
.	.	kIx.	.
<g/>
khl	khl	k?	khl
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
na	na	k7c4	na
vysílání	vysílání	k1gNnSc4	vysílání
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
ruské	ruský	k2eAgFnSc2d1	ruská
televize	televize	k1gFnSc2	televize
Match	Match	k1gMnSc1	Match
TV	TV	kA	TV
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pak	pak	k6eAd1	pak
Viasat	Viasat	k1gFnSc1	Viasat
<g/>
,	,	kIx,	,
Sport	sport	k1gInSc1	sport
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
TV	TV	kA	TV
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
ESPN	ESPN	kA	ESPN
<g/>
,	,	kIx,	,
Sportitalia	Sportitalia	k1gFnSc1	Sportitalia
2	[number]	k4	2
<g/>
,	,	kIx,	,
Sportdigital	Sportdigital	k1gMnSc1	Sportdigital
<g/>
.	.	kIx.	.
<g/>
tv	tv	k?	tv
<g/>
,	,	kIx,	,
Premier	Premier	k1gInSc1	Premier
Sports	Sportsa	k1gFnPc2	Sportsa
<g/>
,	,	kIx,	,
Hockey	Hockea	k1gFnSc2	Hockea
TV	TV	kA	TV
<g/>
,	,	kIx,	,
DigiSport	DigiSport	k1gInSc1	DigiSport
<g/>
,	,	kIx,	,
Yle	Yle	k1gMnSc1	Yle
<g/>
,	,	kIx,	,
Sportklub	Sportklub	k1gMnSc1	Sportklub
a	a	k8xC	a
Eurosport-Asia	Eurosport-Asia	k1gFnSc1	Eurosport-Asia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zápas	zápas	k1gInSc1	zápas
hvězd	hvězda	k1gFnPc2	hvězda
KHL	KHL	kA	KHL
==	==	k?	==
</s>
</p>
<p>
<s>
Utkání	utkání	k1gNnSc1	utkání
hvězd	hvězda	k1gFnPc2	hvězda
KHL	KHL	kA	KHL
je	být	k5eAaImIp3nS	být
souboj	souboj	k1gInSc1	souboj
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
KHL	KHL	kA	KHL
konaný	konaný	k2eAgInSc4d1	konaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
nebo	nebo	k8xC	nebo
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
hrály	hrát	k5eAaImAgInP	hrát
tým	tým	k1gInSc1	tým
ruských	ruský	k2eAgMnPc2d1	ruský
hráčů	hráč	k1gMnPc2	hráč
versus	versus	k7c1	versus
"	"	kIx"	"
<g/>
zbytek	zbytek	k1gInSc1	zbytek
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Východní	východní	k2eAgInPc1d1	východní
versus	versus	k7c1	versus
Západní	západní	k2eAgMnPc1d1	západní
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
==	==	k?	==
</s>
</p>
<p>
<s>
Rekordní	rekordní	k2eAgFnSc1d1	rekordní
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
celé	celá	k1gFnSc2	celá
KHL	KHL	kA	KHL
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
finálový	finálový	k2eAgInSc4d1	finálový
zápas	zápas	k1gInSc4	zápas
Lva	lev	k1gInSc2	lev
Praha	Praha	k1gFnSc1	Praha
s	s	k7c7	s
Metallurg	Metallurg	k1gInSc1	Metallurg
Magnitogorsk	Magnitogorsk	k1gInSc4	Magnitogorsk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
17	[number]	k4	17
073	[number]	k4	073
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezónách	sezóna	k1gFnPc6	sezóna
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
výsledkový	výsledkový	k2eAgInSc1d1	výsledkový
a	a	k8xC	a
zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
servis	servis	k1gInSc1	servis
z	z	k7c2	z
KHL	KHL	kA	KHL
na	na	k7c6	na
Hokejportal	Hokejportal	k1gFnSc6	Hokejportal
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
KHL	KHL	kA	KHL
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.khlportal.cz	www.khlportal.cza	k1gFnPc2	www.khlportal.cza
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
ligy	liga	k1gFnSc2	liga
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
www.khl.ru	www.khl.r	k1gInSc6	www.khl.r
–	–	k?	–
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
strojový	strojový	k2eAgInSc1d1	strojový
překlad	překlad	k1gInSc1	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
)	)	kIx)	)
</s>
</p>
