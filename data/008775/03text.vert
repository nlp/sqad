<p>
<s>
Elektronvolt	elektronvolt	k1gInSc1	elektronvolt
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
eV	eV	k?	eV
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
mimo	mimo	k7c4	mimo
soustavu	soustava	k1gFnSc4	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kinetické	kinetický	k2eAgFnSc3d1	kinetická
energii	energie	k1gFnSc3	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
získá	získat	k5eAaPmIp3nS	získat
elektron	elektron	k1gInSc1	elektron
urychlený	urychlený	k2eAgInSc1d1	urychlený
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
napětím	napětí	k1gNnSc7	napětí
jednoho	jeden	k4xCgInSc2	jeden
voltu	volt	k1gInSc2	volt
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
energie	energie	k1gFnSc2	energie
zejména	zejména	k9	zejména
v	v	k7c6	v
částicové	částicový	k2eAgFnSc6d1	částicová
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
chemii	chemie	k1gFnSc6	chemie
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
energie	energie	k1gFnSc1	energie
jedné	jeden	k4xCgFnSc2	jeden
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
joulech	joule	k1gInPc6	joule
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednotka	jednotka	k1gFnSc1	jednotka
technicky	technicky	k6eAd1	technicky
výhodná	výhodný	k2eAgFnSc1d1	výhodná
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
běžným	běžný	k2eAgFnPc3d1	běžná
metodám	metoda	k1gFnPc3	metoda
měření	měření	k1gNnSc2	měření
energie	energie	k1gFnSc2	energie
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektronvolt	elektronvolt	k1gInSc4	elektronvolt
lze	lze	k6eAd1	lze
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
jednotku	jednotka	k1gFnSc4	jednotka
energie	energie	k1gFnSc2	energie
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
joule	joule	k1gInSc4	joule
podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1,602	[number]	k4	1,602
</s>
</p>
<p>
</p>
<p>
<s>
176	[number]	k4	176
</s>
</p>
<p>
</p>
<p>
<s>
6208	[number]	k4	6208
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
98	[number]	k4	98
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
19	[number]	k4	19
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
602	[number]	k4	602
<g/>
\	\	kIx~	\
<g/>
,176	,176	k4	,176
<g/>
\	\	kIx~	\
<g/>
,6208	,6208	k4	,6208
<g/>
(	(	kIx(	(
<g/>
98	[number]	k4	98
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
číselně	číselně	k6eAd1	číselně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
náboji	náboj	k1gInSc3	náboj
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
coulombech	coulomb	k1gInPc6	coulomb
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
práce	práce	k1gFnSc1	práce
vykonaná	vykonaný	k2eAgFnSc1d1	vykonaná
na	na	k7c6	na
náboji	náboj	k1gInSc6	náboj
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
silou	síla	k1gFnSc7	síla
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
součin	součin	k1gInSc1	součin
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
1	[number]	k4	1
e	e	k0	e
<g/>
)	)	kIx)	)
a	a	k8xC	a
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
1	[number]	k4	1
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
energii	energie	k1gFnSc4	energie
získá	získat	k5eAaPmIp3nS	získat
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
elektrostatickém	elektrostatický	k2eAgNnSc6d1	elektrostatické
poli	pole	k1gNnSc6	pole
i	i	k9	i
jiná	jiný	k2eAgFnSc1d1	jiná
částice	částice	k1gFnSc1	částice
se	s	k7c7	s
stejně	stejně	k6eAd1	stejně
velkým	velký	k2eAgInSc7d1	velký
nábojem	náboj	k1gInSc7	náboj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
proton	proton	k1gInSc1	proton
<g/>
,	,	kIx,	,
pozitron	pozitron	k1gInSc1	pozitron
či	či	k8xC	či
mion	mion	k1gInSc1	mion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektronvolt	elektronvolt	k1gInSc1	elektronvolt
není	být	k5eNaImIp3nS	být
mezi	mezi	k7c7	mezi
standardními	standardní	k2eAgFnPc7d1	standardní
jednotkami	jednotka	k1gFnPc7	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
určována	určovat	k5eAaImNgFnS	určovat
experimentálně	experimentálně	k6eAd1	experimentálně
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
upřesňuje	upřesňovat	k5eAaImIp3nS	upřesňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
povoluje	povolovat	k5eAaImIp3nS	povolovat
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
jednotkami	jednotka	k1gFnPc7	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jakož	jakož	k8xC	jakož
i	i	k9	i
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
experimentálně	experimentálně	k6eAd1	experimentálně
stanovené	stanovený	k2eAgFnPc4d1	stanovená
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
atomovou	atomový	k2eAgFnSc4d1	atomová
hmotnostní	hmotnostní	k2eAgFnSc4d1	hmotnostní
konstantu	konstanta	k1gFnSc4	konstanta
a	a	k8xC	a
astronomickou	astronomický	k2eAgFnSc4d1	astronomická
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektronvolt	elektronvolt	k1gInSc1	elektronvolt
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
jednotky	jednotka	k1gFnSc2	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Elektronvolt	elektronvolt	k1gInSc1	elektronvolt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
běžných	běžný	k2eAgNnPc6d1	běžné
měřítkách	měřítko	k1gNnPc6	měřítko
extrémně	extrémně	k6eAd1	extrémně
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
pohybu	pohyb	k1gInSc2	pohyb
letícího	letící	k2eAgMnSc2d1	letící
komára	komár	k1gMnSc2	komár
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
bilion	bilion	k4xCgInSc4	bilion
elektronvoltů	elektronvolt	k1gInPc2	elektronvolt
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
užitečná	užitečný	k2eAgFnSc1d1	užitečná
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
energie	energie	k1gFnPc1	energie
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnSc2d1	malá
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
eV	eV	k?	eV
často	často	k6eAd1	často
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
větší	veliký	k2eAgInPc1d2	veliký
násobky	násobek	k1gInPc1	násobek
a	a	k8xC	a
předpony	předpona	k1gFnPc1	předpona
<g/>
:	:	kIx,	:
1	[number]	k4	1
keV	keV	k?	keV
je	být	k5eAaImIp3nS	být
tisíc	tisíc	k4xCgInSc4	tisíc
eV	eV	k?	eV
<g/>
,	,	kIx,	,
1	[number]	k4	1
MeV	MeV	k1gFnPc2	MeV
je	být	k5eAaImIp3nS	být
milion	milion	k4xCgInSc4	milion
eV	eV	k?	eV
<g/>
,	,	kIx,	,
1	[number]	k4	1
GeV	GeV	k1gMnPc2	GeV
je	být	k5eAaImIp3nS	být
miliarda	miliarda	k4xCgFnSc1	miliarda
eV	eV	k?	eV
<g/>
,	,	kIx,	,
1	[number]	k4	1
TeV	TeV	k1gFnPc2	TeV
je	být	k5eAaImIp3nS	být
bilion	bilion	k4xCgInSc4	bilion
eV.	eV.	k?	eV.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
zkratka	zkratka	k1gFnSc1	zkratka
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
akronym	akronym	k1gInSc1	akronym
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
skloňováním	skloňování	k1gNnSc7	skloňování
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgInSc4d3	veliký
urychlovač	urychlovač	k1gInSc4	urychlovač
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
LHC	LHC	kA	LHC
<g/>
)	)	kIx)	)
dodá	dodat	k5eAaPmIp3nS	dodat
každému	každý	k3xTgInSc3	každý
protonu	proton	k1gInSc3	proton
energii	energie	k1gFnSc4	energie
7	[number]	k4	7
TeV	TeV	k1gFnPc2	TeV
<g/>
.	.	kIx.	.
</s>
<s>
Rozbitím	rozbití	k1gNnSc7	rozbití
jediného	jediný	k2eAgNnSc2d1	jediné
jádra	jádro	k1gNnSc2	jádro
uranu	uran	k1gInSc2	uran
235U	[number]	k4	235U
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
přibližně	přibližně	k6eAd1	přibližně
215	[number]	k4	215
MeV	MeV	k1gFnPc2	MeV
<g/>
.	.	kIx.	.
<g/>
Sloučením	sloučení	k1gNnSc7	sloučení
jednoho	jeden	k4xCgMnSc4	jeden
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc3	atom
deuteria	deuterium	k1gNnSc2	deuterium
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
tritia	tritium	k1gNnSc2	tritium
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
17,6	[number]	k4	17,6
MeV	MeV	k1gFnPc2	MeV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obrazovkách	obrazovka	k1gFnPc6	obrazovka
barevných	barevný	k2eAgInPc2d1	barevný
televizorů	televizor	k1gInPc2	televizor
jsou	být	k5eAaImIp3nP	být
elektrony	elektron	k1gInPc1	elektron
urychlovány	urychlovat	k5eAaImNgInP	urychlovat
vysokým	vysoký	k2eAgNnSc7d1	vysoké
napětím	napětí	k1gNnSc7	napětí
kolem	kolem	k7c2	kolem
32	[number]	k4	32
tisíc	tisíc	k4xCgInPc2	tisíc
voltů	volt	k1gInPc2	volt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
elektrony	elektron	k1gInPc1	elektron
získávají	získávat	k5eAaImIp3nP	získávat
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
32	[number]	k4	32
keV	keV	k?	keV
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
hodí	hodit	k5eAaImIp3nS	hodit
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
energie	energie	k1gFnSc2	energie
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
řádově	řádově	k6eAd1	řádově
jednotky	jednotka	k1gFnSc2	jednotka
či	či	k8xC	či
desítky	desítka	k1gFnPc4	desítka
eV	eV	k?	eV
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
molekulu	molekula	k1gFnSc4	molekula
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vytržení	vytržení	k1gNnSc3	vytržení
elektronu	elektron	k1gInSc2	elektron
z	z	k7c2	z
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
ionizaci	ionizace	k1gFnSc4	ionizace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
13,6	[number]	k4	13,6
eV.	eV.	k?	eV.
Řádově	řádově	k6eAd1	řádově
jednotky	jednotka	k1gFnSc2	jednotka
eV	eV	k?	eV
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
energie	energie	k1gFnSc1	energie
fotonů	foton	k1gInPc2	foton
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
elektronvolt	elektronvolt	k1gInSc4	elektronvolt
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
termodynamice	termodynamika	k1gFnSc6	termodynamika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
střední	střední	k2eAgFnSc1d1	střední
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
částic	částice	k1gFnPc2	částice
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
je	být	k5eAaImIp3nS	být
38	[number]	k4	38
meV	meV	k?	meV
(	(	kIx(	(
<g/>
milielektronvolt	milielektronvolt	k1gMnSc1	milielektronvolt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Rychlost	rychlost	k1gFnSc1	rychlost
elektronu	elektron	k1gInSc2	elektron
s	s	k7c7	s
kinetickou	kinetický	k2eAgFnSc7d1	kinetická
energií	energie	k1gFnSc7	energie
1	[number]	k4	1
eV	eV	k?	eV
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
593	[number]	k4	593
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Rychlost	rychlost	k1gFnSc1	rychlost
protonu	proton	k1gInSc2	proton
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
kinetickou	kinetický	k2eAgFnSc7d1	kinetická
energií	energie	k1gFnSc7	energie
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
jen	jen	k9	jen
13,8	[number]	k4	13,8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
elektronvoltu	elektronvolt	k1gInSc2	elektronvolt
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
SI	si	k1gNnSc2	si
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
měřením	měření	k1gNnSc7	měření
náboje	náboj	k1gInSc2	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřesnější	přesný	k2eAgMnSc1d3	nejpřesnější
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
metod	metoda	k1gFnPc2	metoda
je	být	k5eAaImIp3nS	být
měření	měření	k1gNnSc1	měření
Josephsonova	Josephsonův	k2eAgInSc2d1	Josephsonův
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nS	určit
hodnota	hodnota	k1gFnSc1	hodnota
Josephsonovy	Josephsonův	k2eAgFnSc2d1	Josephsonův
konstanty	konstanta	k1gFnSc2	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K_	K_	k1gMnPc6	K_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stanoví	stanovit	k5eAaPmIp3nS	stanovit
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
K_	K_	k1gFnSc1	K_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnPc6	R_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
von	von	k1gInSc1	von
Klitzingova	Klitzingův	k2eAgFnSc1d1	Klitzingův
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
změřena	změřit	k5eAaPmNgFnS	změřit
řádově	řádově	k6eAd1	řádově
přesněji	přesně	k6eAd2	přesně
než	než	k8xS	než
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K_	K_	k1gMnPc6	K_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
odchylka	odchylka	k1gFnSc1	odchylka
měření	měření	k1gNnSc2	měření
Josephsonovy	Josephsonův	k2eAgFnSc2d1	Josephsonův
konstanty	konstanta	k1gFnSc2	konstanta
je	být	k5eAaImIp3nS	být
2,5	[number]	k4	2,5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
8	[number]	k4	8
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
miliontiny	miliontina	k1gFnSc2	miliontina
procenta	procento	k1gNnSc2	procento
<g/>
)	)	kIx)	)
a	a	k8xC	a
právě	právě	k9	právě
takovou	takový	k3xDgFnSc4	takový
přesnost	přesnost	k1gFnSc4	přesnost
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
převod	převod	k1gInSc4	převod
elektronvoltu	elektronvolt	k1gInSc2	elektronvolt
na	na	k7c4	na
jouly	joule	k1gInPc4	joule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnPc4	užití
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
částice	částice	k1gFnPc4	částice
s	s	k7c7	s
elementárním	elementární	k2eAgInSc7d1	elementární
nábojem	náboj	k1gInSc7	náboj
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
změna	změna	k1gFnSc1	změna
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
přímo	přímo	k6eAd1	přímo
elektrickému	elektrický	k2eAgNnSc3d1	elektrické
napětí	napětí	k1gNnSc3	napětí
ve	v	k7c6	v
voltech	volt	k1gInPc6	volt
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc1	částice
urychlena	urychlen	k2eAgFnSc1d1	urychlena
(	(	kIx(	(
<g/>
či	či	k8xC	či
zbrzděna	zbrzdit	k5eAaPmNgFnS	zbrzdit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
aparatura	aparatura	k1gFnSc1	aparatura
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
vnějšího	vnější	k2eAgInSc2d1	vnější
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
brzdné	brzdný	k2eAgNnSc1d1	brzdné
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
energie	energie	k1gFnSc2	energie
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světlo	světlo	k1gNnSc1	světlo
(	(	kIx(	(
<g/>
či	či	k8xC	či
jiné	jiný	k2eAgNnSc4d1	jiné
záření	záření	k1gNnSc4	záření
<g/>
)	)	kIx)	)
prochází	procházet	k5eAaImIp3nS	procházet
okénkem	okénko	k1gNnSc7	okénko
do	do	k7c2	do
evakuované	evakuovaný	k2eAgFnSc2d1	evakuovaná
baňky	baňka	k1gFnSc2	baňka
a	a	k8xC	a
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
katodu	katoda	k1gFnSc4	katoda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
vytrhlo	vytrhnout	k5eAaPmAgNnS	vytrhnout
elektrony	elektron	k1gInPc4	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
prolétají	prolétat	k5eAaPmIp3nP	prolétat
skrze	skrze	k?	skrze
mřížku	mřížka	k1gFnSc4	mřížka
<g/>
,	,	kIx,	,
dopadají	dopadat	k5eAaImIp3nP	dopadat
na	na	k7c4	na
anodu	anoda	k1gFnSc4	anoda
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
měříme	měřit	k5eAaImIp1nP	měřit
mikroampérmetrem	mikroampérmetr	k1gInSc7	mikroampérmetr
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
stanovili	stanovit	k5eAaPmAgMnP	stanovit
energii	energie	k1gFnSc4	energie
vyletujících	vyletující	k2eAgInPc2d1	vyletující
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
nastavíme	nastavět	k5eAaBmIp1nP	nastavět
pomocí	pomocí	k7c2	pomocí
potenciometru	potenciometr	k1gInSc2	potenciometr
brzdné	brzdný	k2eAgNnSc1d1	brzdné
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c4	mezi
katodu	katoda	k1gFnSc4	katoda
a	a	k8xC	a
mřížku	mřížka	k1gFnSc4	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k6eAd1	málo
energetické	energetický	k2eAgInPc1d1	energetický
elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
tímto	tento	k3xDgNnSc7	tento
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
vráceny	vrátit	k5eAaPmNgFnP	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
katodu	katoda	k1gFnSc4	katoda
a	a	k8xC	a
neúčastní	účastnit	k5eNaImIp3nS	účastnit
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
elektron	elektron	k1gInSc4	elektron
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
brzdné	brzdný	k2eAgNnSc4d1	brzdné
pole	pole	k1gNnSc4	pole
překoná	překonat	k5eAaPmIp3nS	překonat
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
k	k	k7c3	k
anodě	anoda	k1gFnSc3	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Potřebná	potřebný	k2eAgFnSc1d1	potřebná
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
přímo	přímo	k6eAd1	přímo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
brzdnému	brzdný	k2eAgNnSc3d1	brzdné
napětí	napětí	k1gNnSc3	napětí
ve	v	k7c6	v
voltech	volt	k1gInPc6	volt
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
experimentálně	experimentálně	k6eAd1	experimentálně
zjistit	zjistit	k5eAaPmF	zjistit
krajní	krajní	k2eAgFnSc4d1	krajní
hodnotu	hodnota	k1gFnSc4	hodnota
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
katodou	katoda	k1gFnSc7	katoda
a	a	k8xC	a
mřížkou	mřížka	k1gFnSc7	mřížka
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
obvodem	obvod	k1gInSc7	obvod
ještě	ještě	k9	ještě
prochází	procházet	k5eAaImIp3nS	procházet
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
1,2	[number]	k4	1,2
voltu	volt	k1gInSc2	volt
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
dodává	dodávat	k5eAaImIp3nS	dodávat
elektronům	elektron	k1gInPc3	elektron
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
1,2	[number]	k4	1,2
elektronvoltu	elektronvolt	k1gInSc2	elektronvolt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
tedy	tedy	k9	tedy
často	často	k6eAd1	často
porovnáváme	porovnávat	k5eAaImIp1nP	porovnávat
neznámou	známý	k2eNgFnSc4d1	neznámá
hodnotu	hodnota	k1gFnSc4	hodnota
energie	energie	k1gFnSc2	energie
částice	částice	k1gFnSc2	částice
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
elektronvoltem	elektronvolt	k1gInSc7	elektronvolt
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
této	tento	k3xDgFnSc2	tento
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesnost	nepřesnost	k1gFnSc1	nepřesnost
převodního	převodní	k2eAgInSc2d1	převodní
koeficientu	koeficient	k1gInSc2	koeficient
mezi	mezi	k7c7	mezi
eV	eV	k?	eV
a	a	k8xC	a
J	J	kA	J
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zcela	zcela	k6eAd1	zcela
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chybám	chyba	k1gFnPc3	chyba
měření	měření	k1gNnSc2	měření
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
laboratorních	laboratorní	k2eAgFnPc6d1	laboratorní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
definice	definice	k1gFnSc2	definice
realizovat	realizovat	k5eAaBmF	realizovat
výrazně	výrazně	k6eAd1	výrazně
přesněji	přesně	k6eAd2	přesně
než	než	k8xS	než
joule	joule	k1gInSc4	joule
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstanty	konstanta	k1gFnSc2	konstanta
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
konstanty	konstanta	k1gFnPc1	konstanta
mají	mít	k5eAaImIp3nP	mít
rozměr	rozměr	k1gInSc4	rozměr
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
veličinami	veličina	k1gFnPc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vyjádření	vyjádření	k1gNnSc1	vyjádření
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
joulů	joule	k1gInPc2	joule
používat	používat	k5eAaImF	používat
elektronvolty	elektronvolt	k1gInPc4	elektronvolt
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
CODATA	CODATA	kA	CODATA
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c4	v
doporučení	doporučení	k1gNnSc4	doporučení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
tyto	tento	k3xDgFnPc4	tento
hodnoty	hodnota	k1gFnPc4	hodnota
konstant	konstanta	k1gFnPc2	konstanta
a	a	k8xC	a
směrodatných	směrodatný	k2eAgFnPc2d1	směrodatná
odchylek	odchylka	k1gFnPc2	odchylka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Energie	energie	k1gFnSc1	energie
fotonů	foton	k1gInPc2	foton
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
veškeré	veškerý	k3xTgNnSc4	veškerý
jiné	jiný	k2eAgNnSc4d1	jiné
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
–	–	k?	–
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
frekvenci	frekvence	k1gFnSc4	frekvence
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
hf	hf	k?	hf
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
hc	hc	k?	hc
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	on	k3xPp3gFnPc4	on
frekvence	frekvence	k1gFnPc4	frekvence
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádříme	vyjádřit	k5eAaPmIp1nP	vyjádřit
<g/>
-li	i	k?	-li
součin	součin	k1gInSc4	součin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
hc	hc	k?	hc
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
eV	eV	k?	eV
·	·	k?	·
nm	nm	k?	nm
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
užitečné	užitečný	k2eAgNnSc4d1	užitečné
vyjádření	vyjádření	k1gNnSc4	vyjádření
energie	energie	k1gFnSc2	energie
fotonu	foton	k1gInSc2	foton
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1239,841	[number]	k4	1239,841
</s>
</p>
<p>
</p>
<p>
<s>
929	[number]	k4	929
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
27	[number]	k4	27
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1240	[number]	k4	1240
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1239	[number]	k4	1239
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
841	[number]	k4	841
<g/>
\	\	kIx~	\
<g/>
,929	,929	k4	,929
<g/>
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
nm	nm	k?	nm
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
1240	[number]	k4	1240
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
i	i	k9	i
okolní	okolní	k2eAgNnSc1d1	okolní
infračervené	infračervený	k2eAgNnSc1d1	infračervené
a	a	k8xC	a
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
tvořeno	tvořit	k5eAaImNgNnS	tvořit
fotony	foton	k1gInPc4	foton
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
řádově	řádově	k6eAd1	řádově
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
elektronvoltů	elektronvolt	k1gInPc2	elektronvolt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
veličiny	veličina	k1gFnPc1	veličina
udávané	udávaný	k2eAgFnPc1d1	udávaná
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
částicové	částicový	k2eAgFnSc6d1	částicová
fyzice	fyzika	k1gFnSc6	fyzika
se	se	k3xPyFc4	se
elektronvolty	elektronvolt	k1gInPc7	elektronvolt
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
násobky	násobek	k1gInPc4	násobek
a	a	k8xC	a
mocniny	mocnina	k1gFnPc4	mocnina
běžně	běžně	k6eAd1	běžně
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
hodnot	hodnota	k1gFnPc2	hodnota
jiných	jiný	k2eAgFnPc2d1	jiná
veličin	veličina	k1gFnPc2	veličina
než	než	k8xS	než
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konvence	konvence	k1gFnSc1	konvence
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
veličiny	veličina	k1gFnPc1	veličina
k	k	k7c3	k
sobě	se	k3xPyFc3	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
základní	základní	k2eAgInSc4d1	základní
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
přímé	přímý	k2eAgFnSc2d1	přímá
úměrnosti	úměrnost	k1gFnSc2	úměrnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
energie	energie	k1gFnSc1	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
kontextu	kontext	k1gInSc6	kontext
úměrná	úměrný	k2eAgFnSc1d1	úměrná
veličině	veličina	k1gFnSc6	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
zapisujeme	zapisovat	k5eAaImIp1nP	zapisovat
to	ten	k3xDgNnSc4	ten
jako	jako	k8xS	jako
rovnici	rovnice	k1gFnSc4	rovnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
fA	fa	kA	fa
<g/>
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
úměrnosti	úměrnost	k1gFnSc2	úměrnost
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
některá	některý	k3yIgFnSc1	některý
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc4	hbar
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejich	jejich	k3xOp3gFnSc1	jejich
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Konstanta	konstanta	k1gFnSc1	konstanta
určuje	určovat	k5eAaImIp3nS	určovat
způsob	způsob	k1gInSc4	způsob
přepočtu	přepočet	k1gInSc2	přepočet
veličiny	veličina	k1gFnSc2	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
také	také	k9	také
zpět	zpět	k6eAd1	zpět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
jednotku	jednotka	k1gFnSc4	jednotka
pro	pro	k7c4	pro
veličinu	veličina	k1gFnSc4	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
použít	použít	k5eAaPmF	použít
jednotku	jednotka	k1gFnSc4	jednotka
energie	energie	k1gFnSc2	energie
vydělenou	vydělený	k2eAgFnSc7d1	vydělená
konstantou	konstanta	k1gFnSc7	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
volí	volit	k5eAaImIp3nS	volit
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jednotku	jednotka	k1gFnSc4	jednotka
zapisujeme	zapisovat	k5eAaImIp1nP	zapisovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
A	A	kA	A
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
zápis	zápis	k1gInSc1	zápis
kóduje	kódovat	k5eAaBmIp3nS	kódovat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hodnotu	hodnota	k1gFnSc4	hodnota
veličiny	veličina	k1gFnSc2	veličina
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
protonu	proton	k1gInSc2	proton
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
938	[number]	k4	938
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
938	[number]	k4	938
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
MeV	MeV	k1gFnSc1	MeV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
hodnotu	hodnota	k1gFnSc4	hodnota
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
jednotku	jednotka	k1gFnSc4	jednotka
SI	si	k1gNnSc2	si
–	–	k?	–
kilogram	kilogram	k1gInSc4	kilogram
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přepočítat	přepočítat	k5eAaPmF	přepočítat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
938	[number]	k4	938
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
938	[number]	k4	938
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
jouly	joule	k1gInPc4	joule
a	a	k8xC	a
výsledek	výsledek	k1gInSc4	výsledek
podělit	podělit	k5eAaPmF	podělit
druhou	druhý	k4xOgFnSc7	druhý
mocninou	mocnina	k1gFnSc7	mocnina
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Číselně	číselně	k6eAd1	číselně
tedy	tedy	k9	tedy
provádíme	provádět	k5eAaImIp1nP	provádět
tento	tento	k3xDgInSc4	tento
výpočet	výpočet	k1gInSc4	výpočet
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
938	[number]	k4	938
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1,602	[number]	k4	1,602
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
19	[number]	k4	19
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
67	[number]	k4	67
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
27	[number]	k4	27
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
938	[number]	k4	938
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
602	[number]	k4	602
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
67	[number]	k4	67
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
protonu	proton	k1gInSc2	proton
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
67	[number]	k4	67
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
27	[number]	k4	27
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
67	[number]	k4	67
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
zápis	zápis	k1gInSc1	zápis
je	být	k5eAaImIp3nS	být
konzistentní	konzistentní	k2eAgInSc1d1	konzistentní
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hodnoty	hodnota	k1gFnPc4	hodnota
kdykoli	kdykoli	k6eAd1	kdykoli
přepočítat	přepočítat	k5eAaPmF	přepočítat
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
jednotky	jednotka	k1gFnPc4	jednotka
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
i	i	k9	i
čtenáři	čtenář	k1gMnPc1	čtenář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nezná	znát	k5eNaImIp3nS	znát
příslušný	příslušný	k2eAgInSc1d1	příslušný
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Částicoví	částicový	k2eAgMnPc1d1	částicový
fyzikové	fyzik	k1gMnPc1	fyzik
tuto	tento	k3xDgFnSc4	tento
konvenci	konvence	k1gFnSc4	konvence
znají	znát	k5eAaImIp3nP	znát
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
některých	některý	k3yIgInPc2	některý
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
přitom	přitom	k6eAd1	přitom
ale	ale	k8xC	ale
vynechávají	vynechávat	k5eAaImIp3nP	vynechávat
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
například	například	k6eAd1	například
hmotnost	hmotnost	k1gFnSc1	hmotnost
protonu	proton	k1gInSc2	proton
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
938	[number]	k4	938
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
938	[number]	k4	938
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
MeV	MeV	k1gFnSc1	MeV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
SI	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
zápis	zápis	k1gInSc1	zápis
formálně	formálně	k6eAd1	formálně
nesprávný	správný	k2eNgInSc1d1	nesprávný
a	a	k8xC	a
pro	pro	k7c4	pro
neznalého	znalý	k2eNgMnSc4d1	neznalý
člověka	člověk	k1gMnSc4	člověk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
matoucí	matoucí	k2eAgMnSc1d1	matoucí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
způsob	způsob	k1gInSc1	způsob
převodu	převod	k1gInSc2	převod
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
užívající	užívající	k2eAgInSc1d1	užívající
tento	tento	k3xDgInSc1	tento
zápis	zápis	k1gInSc1	zápis
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
sekci	sekce	k1gFnSc6	sekce
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
výpočtech	výpočet	k1gInPc6	výpočet
používá	používat	k5eAaImIp3nS	používat
místo	místo	k6eAd1	místo
SI	se	k3xPyFc3	se
některou	některý	k3yIgFnSc4	některý
tzv.	tzv.	kA	tzv.
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
soustavu	soustava	k1gFnSc4	soustava
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
zavedeny	zavést	k5eAaPmNgFnP	zavést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
základní	základní	k2eAgFnPc1d1	základní
konstanty	konstanta	k1gFnPc1	konstanta
měly	mít	k5eAaImAgFnP	mít
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
tato	tento	k3xDgFnSc1	tento
konstanta	konstanta	k1gFnSc1	konstanta
odpadá	odpadat	k5eAaImIp3nS	odpadat
i	i	k9	i
ze	z	k7c2	z
zápisu	zápis	k1gInSc2	zápis
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
konvence	konvence	k1gFnSc2	konvence
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uvádět	uvádět	k5eAaImF	uvádět
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc6	jejich
násobcích	násobek	k1gInPc6	násobek
a	a	k8xC	a
mocninách	mocnina	k1gFnPc6	mocnina
dokonce	dokonce	k9	dokonce
všechny	všechen	k3xTgFnPc1	všechen
veličiny	veličina	k1gFnPc1	veličina
relevantní	relevantní	k2eAgFnPc1d1	relevantní
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Níže	nízce	k6eAd2	nízce
uvádíme	uvádět	k5eAaImIp1nP	uvádět
příklady	příklad	k1gInPc1	příklad
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
vyjádření	vyjádření	k1gNnSc3	vyjádření
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
elektronvolty	elektronvolt	k1gInPc1	elektronvolt
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
konstantou	konstanta	k1gFnSc7	konstanta
<g/>
,	,	kIx,	,
a	a	k8xC	a
kontext	kontext	k1gInSc1	kontext
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
převodního	převodní	k2eAgInSc2d1	převodní
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hmotnost	hmotnost	k1gFnSc4	hmotnost
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
každé	každý	k3xTgFnSc2	každý
hmotnosti	hmotnost	k1gFnSc2	hmotnost
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
mc2	mc2	k4	mc2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vztah	vztah	k1gInSc4	vztah
přímé	přímý	k2eAgFnSc2d1	přímá
úměrnosti	úměrnost	k1gFnSc2	úměrnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
měřit	měřit	k5eAaImF	měřit
hmotnost	hmotnost	k1gFnSc4	hmotnost
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
jednotkách	jednotka	k1gFnPc6	jednotka
jako	jako	k8xS	jako
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
klidovou	klidový	k2eAgFnSc4d1	klidová
hmotnost	hmotnost	k1gFnSc4	hmotnost
elektronu	elektron	k1gInSc2	elektron
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
31	[number]	k4	31
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
9	[number]	k4	9
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
11	[number]	k4	11
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
můžeme	moct	k5eAaImIp1nP	moct
vynásobit	vynásobit	k5eAaPmF	vynásobit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
klidovou	klidový	k2eAgFnSc4d1	klidová
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
joulech	joule	k1gInPc6	joule
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převodu	převod	k1gInSc6	převod
na	na	k7c4	na
elektronvolty	elektronvolt	k1gInPc4	elektronvolt
můžeme	moct	k5eAaImIp1nP	moct
psát	psát	k5eAaImF	psát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
511	[number]	k4	511
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
511	[number]	k4	511
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
zkráceně	zkráceně	k6eAd1	zkráceně
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
i	i	k9	i
jako	jako	k9	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
511	[number]	k4	511
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
511	[number]	k4	511
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
energii	energie	k1gFnSc3	energie
uvolněné	uvolněný	k2eAgFnSc3d1	uvolněná
při	při	k7c6	při
anihilaci	anihilace	k1gFnSc6	anihilace
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1,602	[number]	k4	1,602
</s>
</p>
<p>
</p>
<p>
<s>
176	[number]	k4	176
</s>
</p>
<p>
</p>
<p>
<s>
565	[number]	k4	565
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
35	[number]	k4	35
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
19	[number]	k4	19
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
299	[number]	k4	299
</s>
</p>
<p>
</p>
<p>
<s>
792	[number]	k4	792
</s>
</p>
<p>
</p>
<p>
<s>
458	[number]	k4	458
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1,782	[number]	k4	1,782
</s>
</p>
<p>
</p>
<p>
<s>
661	[number]	k4	661
</s>
</p>
<p>
</p>
<p>
<s>
845	[number]	k4	845
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
39	[number]	k4	39
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
36	[number]	k4	36
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
602	[number]	k4	602
<g/>
\	\	kIx~	\
<g/>
,176	,176	k4	,176
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,565	,565	k4	,565
<g/>
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
299	[number]	k4	299
<g/>
\	\	kIx~	\
<g/>
,792	,792	k4	,792
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
,458	,458	k4	,458
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
782	[number]	k4	782
<g/>
\	\	kIx~	\
<g/>
,661	,661	k4	,661
<g/>
\	\	kIx~	\
<g/>
,845	,845	k4	,845
<g/>
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
udává	udávat	k5eAaImIp3nS	udávat
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
MeV	MeV	k1gFnSc1	MeV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1,782	[number]	k4	1,782
</s>
</p>
<p>
</p>
<p>
<s>
662	[number]	k4	662
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
30	[number]	k4	30
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
782	[number]	k4	782
<g/>
\	\	kIx~	\
<g/>
,662	,662	k4	,662
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zhruba	zhruba	k6eAd1	zhruba
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
elektronu	elektron	k1gInSc2	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hybnost	hybnost	k1gFnSc4	hybnost
===	===	k?	===
</s>
</p>
<p>
<s>
Hybnost	hybnost	k1gFnSc1	hybnost
fotonu	foton	k1gInSc2	foton
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
jeho	jeho	k3xOp3gFnSc4	jeho
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
konstantou	konstanta	k1gFnSc7	konstanta
úměrnosti	úměrnost	k1gFnSc2	úměrnost
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
E	E	kA	E
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
c	c	k0	c
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
můžeme	moct	k5eAaImIp1nP	moct
přirozeně	přirozeně	k6eAd1	přirozeně
měřit	měřit	k5eAaImF	měřit
hybnost	hybnost	k1gFnSc4	hybnost
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1,602	[number]	k4	1,602
</s>
</p>
<p>
</p>
<p>
<s>
176	[number]	k4	176
</s>
</p>
<p>
</p>
<p>
<s>
565	[number]	k4	565
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
35	[number]	k4	35
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
19	[number]	k4	19
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
299	[number]	k4	299
</s>
</p>
<p>
</p>
<p>
<s>
792	[number]	k4	792
</s>
</p>
<p>
</p>
<p>
<s>
458	[number]	k4	458
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
5,344	[number]	k4	5,344
</s>
</p>
<p>
</p>
<p>
<s>
285	[number]	k4	285
</s>
</p>
<p>
</p>
<p>
<s>
76	[number]	k4	76
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
28	[number]	k4	28
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
602	[number]	k4	602
<g/>
\	\	kIx~	\
<g/>
,176	,176	k4	,176
<g/>
\	\	kIx~	\
<g/>
,565	,565	k4	,565
<g/>
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
299	[number]	k4	299
<g/>
\	\	kIx~	\
<g/>
,792	,792	k4	,792
<g/>
\	\	kIx~	\
<g/>
,458	,458	k4	,458
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
5	[number]	k4	5
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
344	[number]	k4	344
<g/>
\	\	kIx~	\
<g/>
,285	,285	k4	,285
<g/>
\	\	kIx~	\
<g/>
,76	,76	k4	,76
<g/>
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Hybnost	hybnost	k1gFnSc1	hybnost
fotonu	foton	k1gInSc2	foton
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
jednotkách	jednotka	k1gFnPc6	jednotka
číselně	číselně	k6eAd1	číselně
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhodnost	výhodnost	k1gFnSc4	výhodnost
těchto	tento	k3xDgFnPc2	tento
jednotek	jednotka	k1gFnPc2	jednotka
lze	lze	k6eAd1	lze
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
máme	mít	k5eAaImIp1nP	mít
určit	určit	k5eAaPmF	určit
hybnost	hybnost	k1gFnSc4	hybnost
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
urychlen	urychlit	k5eAaPmNgInS	urychlit
elektrickým	elektrický	k2eAgInSc7d1	elektrický
napětím	napětí	k1gNnSc7	napětí
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
150	[number]	k4	150
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
150	[number]	k4	150
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
kV	kV	k?	kV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
elektronvoltu	elektronvolt	k1gInSc2	elektronvolt
získal	získat	k5eAaPmAgInS	získat
elektron	elektron	k1gInSc1	elektron
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
150	[number]	k4	150
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
150	[number]	k4	150
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
511	[number]	k4	511
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
511	[number]	k4	511
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gFnSc1	jeho
energie	energie	k1gFnSc1	energie
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
511	[number]	k4	511
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc6	E_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
511	[number]	k4	511
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
urychlené	urychlený	k2eAgFnSc2d1	urychlená
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jednoduše	jednoduše	k6eAd1	jednoduše
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
661	[number]	k4	661
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
661	[number]	k4	661
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
K	k	k7c3	k
výpočtu	výpočet	k1gInSc3	výpočet
hybnosti	hybnost	k1gFnSc2	hybnost
použijeme	použít	k5eAaPmIp1nP	použít
relativistický	relativistický	k2eAgInSc4d1	relativistický
vztah	vztah	k1gInSc4	vztah
známý	známý	k2eAgInSc4d1	známý
jako	jako	k8xS	jako
Pythagorova	Pythagorův	k2eAgFnSc5d1	Pythagorova
věta	věta	k1gFnSc1	věta
o	o	k7c4	o
energii	energie	k1gFnSc4	energie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
pc	pc	k?	pc
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Odtud	odtud	k6eAd1	odtud
plyne	plynout	k5eAaImIp3nS	plynout
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
661	[number]	k4	661
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
511	[number]	k4	511
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
419	[number]	k4	419
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
pc	pc	k?	pc
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
661	[number]	k4	661
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
511	[number]	k4	511
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
419	[number]	k4	419
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Hybnost	hybnost	k1gFnSc1	hybnost
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k9	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
419	[number]	k4	419
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
419	[number]	k4	419
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
keV	keV	k?	keV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
konstanta	konstanta	k1gFnSc1	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
chová	chovat	k5eAaImIp3nS	chovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
znát	znát	k5eAaImF	znát
její	její	k3xOp3gFnSc4	její
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
číselné	číselný	k2eAgFnPc1d1	číselná
operace	operace	k1gFnPc1	operace
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teplota	teplota	k1gFnSc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
kelvinů	kelvin	k1gInPc2	kelvin
někdy	někdy	k6eAd1	někdy
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
<g/>
.	.	kIx.	.
</s>
<s>
Převod	převod	k1gInSc1	převod
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
hodnotou	hodnota	k1gFnSc7	hodnota
Boltzmannovy	Boltzmannův	k2eAgFnSc2d1	Boltzmannova
konstanty	konstanta	k1gFnSc2	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
8,617	[number]	k4	8,617
</s>
</p>
<p>
</p>
<p>
<s>
3324	[number]	k4	3324
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
78	[number]	k4	78
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
</p>
<p>
<s>
604,519	[number]	k4	604,519
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
8	[number]	k4	8
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
617	[number]	k4	617
<g/>
\	\	kIx~	\
<g/>
,3324	,3324	k4	,3324
<g/>
(	(	kIx(	(
<g/>
78	[number]	k4	78
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
/	/	kIx~	/
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
11	[number]	k4	11
<g/>
\	\	kIx~	\
<g/>
,604	,604	k4	,604
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
519	[number]	k4	519
<g/>
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
teplotu	teplota	k1gFnSc4	teplota
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
Slunce	slunce	k1gNnSc2	slunce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
15,7	[number]	k4	15,7
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
350	[number]	k4	350
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,350	,350	k4	,350
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
350	[number]	k4	350
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
kT	kT	k?	kT
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,350	,350	k4	,350
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zápise	zápis	k1gInSc6	zápis
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
neuvádí	uvádět	k5eNaImIp3nS	uvádět
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
350	[number]	k4	350
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,350	,350	k4	,350
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
použití	použití	k1gNnSc2	použití
jednotky	jednotka	k1gFnSc2	jednotka
energie	energie	k1gFnSc2	energie
'	'	kIx"	'
<g/>
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
<g/>
'	'	kIx"	'
pro	pro	k7c4	pro
udání	udání	k1gNnSc4	udání
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stačí	stačit	k5eAaBmIp3nS	stačit
hodnotu	hodnota	k1gFnSc4	hodnota
vynásobit	vynásobit	k5eAaPmF	vynásobit
faktorem	faktor	k1gInSc7	faktor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
3	[number]	k4	3
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
získáme	získat	k5eAaPmIp1nP	získat
střední	střední	k2eAgFnSc4d1	střední
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
zmiňované	zmiňovaný	k2eAgNnSc4d1	zmiňované
jádro	jádro	k1gNnSc4	jádro
slunce	slunce	k1gNnSc2	slunce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2025	[number]	k4	2025
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k2eAgInSc4d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc4	langle
E_	E_	k1gFnSc2	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
=	=	kIx~	=
<g/>
2025	[number]	k4	2025
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Časy	čas	k1gInPc4	čas
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc7d1	krátká
střední	střední	k2eAgFnSc7d1	střední
dobou	doba	k1gFnSc7	doba
života	život	k1gInSc2	život
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnPc3	tau
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
někdy	někdy	k6eAd1	někdy
udává	udávat	k5eAaImIp3nS	udávat
tzv.	tzv.	kA	tzv.
rozpadová	rozpadový	k2eAgFnSc1d1	rozpadová
šířka	šířka	k1gFnSc1	šířka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
rozměr	rozměr	k1gInSc4	rozměr
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Gamma	Gammum	k1gNnPc4	Gammum
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
mezon	mezon	k1gInSc1	mezon
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
dobu	doba	k1gFnSc4	doba
života	život	k1gInSc2	život
asi	asi	k9	asi
1,53	[number]	k4	1,53
pikosekund	pikosekunda	k1gFnPc2	pikosekunda
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rozpadová	rozpadový	k2eAgFnSc1d1	rozpadová
šířka	šířka	k1gFnSc1	šířka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
30	[number]	k4	30
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
4	[number]	k4	4
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
30	[number]	k4	30
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
časové	časový	k2eAgInPc4d1	časový
údaje	údaj	k1gInPc4	údaj
lze	lze	k6eAd1	lze
udávat	udávat	k5eAaImF	udávat
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
či	či	k8xC	či
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
6,582	[number]	k4	6,582
</s>
</p>
<p>
</p>
<p>
<s>
119	[number]	k4	119
</s>
</p>
<p>
</p>
<p>
<s>
28	[number]	k4	28
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
43	[number]	k4	43
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
16	[number]	k4	16
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
=	=	kIx~	=
<g/>
6	[number]	k4	6
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
582	[number]	k4	582
<g/>
\	\	kIx~	\
<g/>
,119	,119	k4	,119
<g/>
\	\	kIx~	\
<g/>
,28	,28	k4	,28
<g/>
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
A	a	k9	a
protože	protože	k8xS	protože
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
dává	dávat	k5eAaImIp3nS	dávat
přímý	přímý	k2eAgInSc1d1	přímý
přepočet	přepočet	k1gInSc1	přepočet
mezi	mezi	k7c7	mezi
jednotkami	jednotka	k1gFnPc7	jednotka
času	čas	k1gInSc2	čas
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
měřit	měřit	k5eAaImF	měřit
i	i	k9	i
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
či	či	k8xC	či
přesněji	přesně	k6eAd2	přesně
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1,973	[number]	k4	1,973
</s>
</p>
<p>
</p>
<p>
<s>
269	[number]	k4	269
</s>
</p>
<p>
</p>
<p>
<s>
72	[number]	k4	72
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
973	[number]	k4	973
<g/>
\	\	kIx~	\
<g/>
,269	,269	k4	,269
<g/>
\	\	kIx~	\
<g/>
,72	,72	k4	,72
<g/>
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
typickým	typický	k2eAgFnPc3d1	typická
malým	malý	k2eAgFnPc3d1	malá
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
často	často	k6eAd1	často
ve	v	k7c6	v
tvarech	tvar	k1gInPc6	tvar
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
197,326	[number]	k4	197,326
</s>
</p>
<p>
</p>
<p>
<s>
972	[number]	k4	972
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
197	[number]	k4	197
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
326	[number]	k4	326
<g/>
\	\	kIx~	\
<g/>
,972	,972	k4	,972
<g/>
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
197,326	[number]	k4	197,326
</s>
</p>
<p>
</p>
<p>
<s>
972	[number]	k4	972
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
MeV	MeV	k1gFnSc1	MeV
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
197	[number]	k4	197
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
326	[number]	k4	326
<g/>
\	\	kIx~	\
<g/>
,972	,972	k4	,972
<g/>
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
fm	fm	k?	fm
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
jednotka	jednotka	k1gFnSc1	jednotka
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
voltu	volt	k1gInSc2	volt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
použita	použit	k2eAgFnSc1d1	použita
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Philosophical	Philosophical	k1gFnSc2	Philosophical
Magazine	Magazin	k1gInSc5	Magazin
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Karla	Karel	k1gMnSc2	Karel
Taylora	Taylor	k1gMnSc2	Taylor
Comptona	Compton	k1gMnSc2	Compton
a	a	k8xC	a
Owena	Owen	k1gMnSc2	Owen
Willanse	Willans	k1gMnSc2	Willans
Richardsona	Richardson	k1gMnSc2	Richardson
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Photoelectric	Photoelectric	k1gMnSc1	Photoelectric
Effect	Effect	k1gMnSc1	Effect
<g/>
"	"	kIx"	"
o	o	k7c6	o
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
jednotka	jednotka	k1gFnSc1	jednotka
BeV	BeV	k1gFnSc2	BeV
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
bev	bev	k?	bev
či	či	k8xC	či
Bev	Bev	k1gMnSc1	Bev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
B	B	kA	B
představovalo	představovat	k5eAaImAgNnS	představovat
miliardu	miliarda	k4xCgFnSc4	miliarda
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
"	"	kIx"	"
<g/>
billion	billion	k1gInSc1	billion
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
však	však	k8xC	však
IUPAP	IUPAP	kA	IUPAP
její	její	k3xOp3gNnSc4	její
používání	používání	k1gNnSc4	používání
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
a	a	k8xC	a
pro	pro	k7c4	pro
miliardu	miliarda	k4xCgFnSc4	miliarda
elektronvoltů	elektronvolt	k1gInPc2	elektronvolt
upřednostnil	upřednostnit	k5eAaPmAgMnS	upřednostnit
použití	použití	k1gNnPc4	použití
předpony	předpona	k1gFnSc2	předpona
giga	giga	k1gFnSc1	giga
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
GeV	GeV	k1gFnSc2	GeV
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
starších	starý	k2eAgFnPc6d2	starší
publikacích	publikace	k1gFnPc6	publikace
se	se	k3xPyFc4	se
jako	jako	k9	jako
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
elektronvolt	elektronvolt	k1gInSc4	elektronvolt
uvádí	uvádět	k5eAaImIp3nS	uvádět
"	"	kIx"	"
<g/>
ev	ev	k?	ev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
jednotky	jednotka	k1gFnSc2	jednotka
BeV	BeV	k1gFnSc2	BeV
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
název	název	k1gInSc1	název
částicového	částicový	k2eAgInSc2d1	částicový
urychlovače	urychlovač	k1gInSc2	urychlovač
Bevatron	Bevatron	k1gInSc1	Bevatron
(	(	kIx(	(
<g/>
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stejného	stejný	k2eAgInSc2d1	stejný
klíče	klíč	k1gInSc2	klíč
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
urychlovač	urychlovač	k1gInSc1	urychlovač
Tevatron	Tevatron	k1gInSc1	Tevatron
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
urychloval	urychlovat	k5eAaImAgInS	urychlovat
protony	proton	k1gInPc4	proton
a	a	k8xC	a
antiprotony	antiproton	k1gInPc4	antiproton
na	na	k7c4	na
energie	energie	k1gFnPc4	energie
až	až	k9	až
1	[number]	k4	1
TeV	TeV	k1gFnPc2	TeV
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Zevatron	Zevatron	k1gInSc1	Zevatron
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
nadsázkou	nadsázka	k1gFnSc7	nadsázka
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
přírodní	přírodní	k2eAgInPc4d1	přírodní
astrofyzikální	astrofyzikální	k2eAgInPc4d1	astrofyzikální
zdroje	zdroj	k1gInPc4	zdroj
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
energiemi	energie	k1gFnPc7	energie
až	až	k9	až
1021	[number]	k4	1021
eV	eV	k?	eV
(	(	kIx(	(
<g/>
předpona	předpona	k1gFnSc1	předpona
zetta	zetta	k1gMnSc1	zetta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
energie	energie	k1gFnSc1	energie
jediné	jediný	k2eAgFnSc2d1	jediná
částice	částice	k1gFnSc2	částice
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
nikdy	nikdy	k6eAd1	nikdy
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
metrologický	metrologický	k2eAgInSc1d1	metrologický
institut	institut	k1gInSc1	institut
<g/>
:	:	kIx,	:
Povolené	povolený	k2eAgFnPc1d1	povolená
jednotky	jednotka	k1gFnPc1	jednotka
mimo	mimo	k7c4	mimo
SI	si	k1gNnSc4	si
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
,	,	kIx,	,
converter	converter	k1gMnSc1	converter
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
–	–	k?	–
převodní	převodní	k2eAgFnPc4d1	převodní
tabulky	tabulka	k1gFnPc4	tabulka
veličin	veličina	k1gFnPc2	veličina
</s>
</p>
