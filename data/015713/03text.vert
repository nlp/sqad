<s>
Letecká	letecký	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
Ruský	ruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Dmitrij	Dmitrij	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
na	na	k7c6
místě	místo	k1gNnSc6
nehodyNehoda	nehodyNehoda	k1gFnSc1
Datum	datum	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2011	#num#	k4
Čas	čas	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
moskevského	moskevský	k2eAgInSc2d1
času	čas	k1gInSc2
Fáze	fáze	k1gFnSc2
letu	let	k1gInSc2
</s>
<s>
start	start	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
příčina	příčina	k1gFnSc1
</s>
<s>
chyba	chyba	k1gFnSc1
pilota	pilota	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
</s>
<s>
Jaroslavl	Jaroslavnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
57	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
40	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Počátek	počátek	k1gInSc1
letu	let	k1gInSc2
</s>
<s>
letiště	letiště	k1gNnSc1
Tunošna	Tunošn	k1gInSc2
<g/>
,	,	kIx,
Jaroslavl	Jaroslavl	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
Cíl	Cíla	k1gFnPc2
letu	let	k1gInSc2
</s>
<s>
letiště	letiště	k1gNnSc1
Minsk-	Minsk-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Minsk	Minsk	k1gInSc1
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
Letoun	letoun	k1gInSc1
Model	model	k1gInSc1
</s>
<s>
Jakovlev	Jakovlet	k5eAaPmDgInS
Jak-	Jak-	k1gMnSc2
<g/>
42	#num#	k4
<g/>
D	D	kA
Dopravce	dopravce	k1gMnSc1
</s>
<s>
Yak-Service	Yak-Service	k1gFnSc1
Registrace	registrace	k1gFnSc1
</s>
<s>
RA-	RA-	k?
<g/>
42434	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stáří	stáří	k1gNnSc1
</s>
<s>
1993	#num#	k4
Následky	následek	k1gInPc4
Na	na	k7c6
palubě	paluba	k1gFnSc6
osob	osoba	k1gFnPc2
</s>
<s>
45	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Mrtvých	mrtvý	k2eAgInPc2d1
</s>
<s>
44	#num#	k4
potvrzených	potvrzená	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Přeživších	přeživší	k2eAgInPc2d1
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
jeden	jeden	k4xCgInSc4
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
<g/>
)	)	kIx)
Letoun	letoun	k1gInSc1
</s>
<s>
zničen	zničen	k2eAgInSc1d1
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
letounu	letoun	k1gInSc2
Jak-	Jak-	kA
<g/>
42	#num#	k4
u	u	k7c2
ruské	ruský	k2eAgFnSc2d1
Jaroslavli	Jaroslavli	k1gFnSc2
se	se	k3xPyFc4
udála	udát	k5eAaPmAgFnS
7	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc4
Jak	jak	k8xC,k8xS
<g/>
–	–	k?
<g/>
42	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
palubě	paluba	k1gFnSc6
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgInS
hokejový	hokejový	k2eAgInSc4d1
tým	tým	k1gInSc4
Lokomotivu	lokomotiva	k1gFnSc4
Jaroslavl	Jaroslavl	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
zřítil	zřítit	k5eAaPmAgInS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
do	do	k7c2
běloruského	běloruský	k2eAgInSc2d1
Minsku	Minsk	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
odstartovat	odstartovat	k5eAaPmF
novou	nový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
KHL	KHL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
palubě	paluba	k1gFnSc6
letadla	letadlo	k1gNnSc2
byli	být	k5eAaImAgMnP
i	i	k9
tři	tři	k4xCgMnPc1
čeští	český	k2eAgMnPc1d1
hokejisté	hokejista	k1gMnPc1
Josef	Josef	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Rachůnek	Rachůnek	k1gMnSc1
<g/>
,	,	kIx,
Slovák	Slovák	k1gMnSc1
Pavol	Pavol	k1gInSc4
Demitra	Demitrum	k1gNnSc2
a	a	k8xC
Němec	Němec	k1gMnSc1
Robert	Robert	k1gMnSc1
Dietrich	Dietrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Havárii	havárie	k1gFnSc4
nikdo	nikdo	k3yNnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nepřežil	přežít	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
katastrofu	katastrofa	k1gFnSc4
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
představuje	představovat	k5eAaImIp3nS
„	„	k?
<g/>
nejtemnější	temný	k2eAgInSc4d3
den	den	k1gInSc4
v	v	k7c6
dějinách	dějiny	k1gFnPc6
našeho	náš	k3xOp1gInSc2
sportu	sport	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letadlo	letadlo	k1gNnSc1
</s>
<s>
Jak-	Jak-	k?
<g/>
42	#num#	k4
<g/>
D	D	kA
RA-42434	RA-42434	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
provozován	provozovat	k5eAaImNgInS
společnosti	společnost	k1gFnSc6
Aero	aero	k1gNnSc4
Rent	renta	k1gFnPc2
</s>
<s>
Zřícený	zřícený	k2eAgInSc1d1
stroj	stroj	k1gInSc1
byl	být	k5eAaImAgInS
třímotorový	třímotorový	k2eAgInSc1d1
proudový	proudový	k2eAgInSc1d1
letoun	letoun	k1gInSc1
Jakovlev	Jakovlev	k1gMnSc2
Jak-	Jak-	k1gMnSc2
<g/>
42	#num#	k4
<g/>
D	D	kA
s	s	k7c7
registračními	registrační	k2eAgInPc7d1
znaky	znak	k1gInPc7
RA-42434	RA-42434	k1gMnSc2
ve	v	k7c6
VIP	VIP	kA
salónní	salónní	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
pro	pro	k7c4
73	#num#	k4
pasažérů	pasažér	k1gMnPc2
a	a	k8xC
posádku	posádka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
webu	web	k1gInSc2
Russianplanes	Russianplanesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
net	net	k?
mělo	mít	k5eAaImAgNnS
letadlo	letadlo	k1gNnSc1
nalétáno	nalétat	k5eAaBmNgNnS,k5eAaImNgNnS
do	do	k7c2
počátku	počátek	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
teprve	teprve	k6eAd1
6	#num#	k4
310	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
provedlo	provést	k5eAaPmAgNnS
3	#num#	k4
026	#num#	k4
cyklů	cyklus	k1gInPc2
(	(	kIx(
<g/>
startů	start	k1gInPc2
a	a	k8xC
přistání	přistání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
nehody	nehoda	k1gFnSc2
měl	mít	k5eAaImAgMnS
nalétáno	nalétat	k5eAaBmNgNnS,k5eAaImNgNnS
přibližně	přibližně	k6eAd1
6	#num#	k4
500	#num#	k4
hod	hod	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
asi	asi	k9
polovina	polovina	k1gFnSc1
z	z	k7c2
aktuálně	aktuálně	k6eAd1
povolené	povolený	k2eAgFnSc2d1
doby	doba	k1gFnSc2
12	#num#	k4
000	#num#	k4
hod	hod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
projektovaná	projektovaný	k2eAgFnSc1d1
životnost	životnost	k1gFnSc1
typu	typ	k1gInSc2
Jak-	Jak-	k1gFnSc2
<g/>
42	#num#	k4
je	být	k5eAaImIp3nS
40	#num#	k4
000	#num#	k4
hod	hod	k1gInSc1
<g/>
,	,	kIx,
18	#num#	k4
000	#num#	k4
cyklů	cyklus	k1gInPc2
a	a	k8xC
35	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stroj	stroj	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
provozu	provoz	k1gInSc6
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
v	v	k7c6
barvách	barva	k1gFnPc6
Orel	Orel	k1gMnSc1
Avia	Avia	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Bykovo	Bykovo	k1gNnSc4
Avia	Avia	k1gFnSc1
a	a	k8xC
Aero	aero	k1gNnSc1
Rent	renta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
nehody	nehoda	k1gFnSc2
byly	být	k5eAaImAgFnP
provozovatelem	provozovatel	k1gMnSc7
letadla	letadlo	k1gNnSc2
ruské	ruský	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
Yak	Yak	k1gFnSc2
Service	Service	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
Yak	Yak	k1gFnSc1
Service	Service	k1gFnSc1
založená	založený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1993	#num#	k4
provozovala	provozovat	k5eAaImAgFnS
celkem	celkem	k6eAd1
tři	tři	k4xCgNnPc1
letadla	letadlo	k1gNnPc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
havarovaného	havarovaný	k2eAgMnSc2d1
Jak-	Jak-	k1gMnSc2
<g/>
42	#num#	k4
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
také	také	k9
dvě	dva	k4xCgNnPc1
menší	malý	k2eAgNnPc1d2
letadla	letadlo	k1gNnPc1
Jak-	Jak-	k1gMnPc2
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
měl	mít	k5eAaImAgMnS
Yak	Yak	k1gFnSc4
Service	Service	k1gFnSc2
vzhledem	vzhledem	k7c3
k	k	k7c3
problémům	problém	k1gInPc3
s	s	k7c7
bezpečností	bezpečnost	k1gFnSc7
provozu	provoz	k1gInSc2
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
zakázáno	zakázán	k2eAgNnSc1d1
létat	létat	k5eAaImF
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
přišla	přijít	k5eAaPmAgFnS
o	o	k7c4
povolení	povolení	k1gNnSc4
létat	létat	k5eAaImF
do	do	k7c2
zemí	zem	k1gFnPc2
EU	EU	kA
s	s	k7c7
letadly	letadlo	k1gNnPc7
Jak-	Jak-	k1gMnSc2
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
vyšetřování	vyšetřování	k1gNnSc2
definitivně	definitivně	k6eAd1
odebraly	odebrat	k5eAaPmAgFnP
státní	státní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
společnosti	společnost	k1gFnSc2
Yak	Yak	k1gFnSc3
Service	Service	k1gFnSc2
licenci	licence	k1gFnSc4
leteckého	letecký	k2eAgMnSc2d1
dopravce	dopravce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nehoda	nehoda	k1gFnSc1
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Luxusně	luxusně	k6eAd1
vybavené	vybavený	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
se	s	k7c7
14	#num#	k4
t	t	k?
paliva	palivo	k1gNnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
havarovalo	havarovat	k5eAaPmAgNnS
za	za	k7c2
slunného	slunný	k2eAgInSc2d1
dne	den	k1gInSc2
těsně	těsně	k6eAd1
před	před	k7c7
čtvrtou	čtvrtý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
odpoledne	odpoledne	k6eAd1
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
UTC	UTC	kA
<g/>
)	)	kIx)
v	v	k7c6
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
letiště	letiště	k1gNnSc2
Tunošna	Tunošn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třímotorovému	třímotorový	k2eAgMnSc3d1
letounu	letoun	k1gMnSc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
uskutečnit	uskutečnit	k5eAaPmF
vzlet	vzlet	k1gInSc4
na	na	k7c6
délce	délka	k1gFnSc6
dráhy	dráha	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
po	po	k7c6
startu	start	k1gInSc6
z	z	k7c2
travnatého	travnatý	k2eAgNnSc2d1
předpolí	předpolí	k1gNnSc2
ranveje	ranvej	k1gInSc2
nabrat	nabrat	k5eAaPmF
výšku	výška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
se	se	k3xPyFc4
odlepil	odlepit	k5eAaPmAgMnS
od	od	k7c2
země	zem	k1gFnSc2
a	a	k8xC
po	po	k7c6
kolizi	kolize	k1gFnSc6
s	s	k7c7
anténním	anténní	k2eAgInSc7d1
systémem	systém	k1gInSc7
LLZ	LLZ	kA
se	se	k3xPyFc4
naklonil	naklonit	k5eAaPmAgInS
a	a	k8xC
dopadl	dopadnout	k5eAaPmAgInS
zpět	zpět	k6eAd1
na	na	k7c4
zem	zem	k1gFnSc4
z	z	k7c2
výšky	výška	k1gFnSc2
několika	několik	k4yIc2
metrů	metr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dopadu	dopad	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
cca	cca	kA
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozlomil	rozlomit	k5eAaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
hořet	hořet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
trosek	troska	k1gFnPc2
spadla	spadnout	k5eAaPmAgFnS
do	do	k7c2
blízké	blízký	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Volhy	Volha	k1gFnSc2
(	(	kIx(
<g/>
57	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
40	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
osmi	osm	k4xCc2
členů	člen	k1gInPc2
posádky	posádka	k1gFnSc2
a	a	k8xC
třiceti	třicet	k4xCc2
sedmi	sedm	k4xCc2
pasažérů	pasažér	k1gMnPc2
přežily	přežít	k5eAaPmAgFnP
jen	jen	k6eAd1
dvě	dva	k4xCgFnPc4
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
jednou	jednou	k9
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Galimov	Galimov	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
utrpěl	utrpět	k5eAaPmAgMnS
popáleniny	popálenina	k1gFnPc4
na	na	k7c4
90	#num#	k4
%	%	kIx~
povrchu	povrch	k1gInSc2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
druhou	druhý	k4xOgFnSc7
pak	pak	k6eAd1
pozemní	pozemní	k2eAgMnSc1d1
mechanik	mechanik	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
B.	B.	kA
Sizov	Sizov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Ohledně	ohledně	k7c2
Galimova	Galimův	k2eAgMnSc2d1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
objevovaly	objevovat	k5eAaImAgFnP
zprávy	zpráva	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
poté	poté	k6eAd1
byly	být	k5eAaImAgFnP
dementovány	dementován	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
Alexandr	Alexandr	k1gMnSc1
Galimov	Galimov	k1gInSc1
svým	svůj	k3xOyFgNnSc7
zraněním	zranění	k1gNnSc7
podlehl	podlehnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vyšetřování	vyšetřování	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
vedl	vést	k5eAaImAgMnS
standardně	standardně	k6eAd1
MAK	mako	k1gNnPc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
přizváni	přizvat	k5eAaPmNgMnP
i	i	k9
specialisté	specialista	k1gMnPc1
ze	z	k7c2
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
pocházely	pocházet	k5eAaImAgFnP
oběti	oběť	k1gFnPc4
nehody	nehoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ČR	ČR	kA
se	se	k3xPyFc4
vyšetřování	vyšetřování	k1gNnSc2
krátkodobě	krátkodobě	k6eAd1
účastnil	účastnit	k5eAaImAgMnS
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Procházka	Procházka	k1gMnSc1
z	z	k7c2
ÚZPLN	ÚZPLN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
nehodě	nehoda	k1gFnSc6
byly	být	k5eAaImAgFnP
odebrány	odebrat	k5eAaPmNgFnP
k	k	k7c3
rozboru	rozbor	k1gInSc3
vzorky	vzorek	k1gInPc7
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
letoun	letoun	k1gInSc1
dotankoval	dotankovat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
v	v	k7c6
normě	norma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
den	dna	k1gFnPc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
potápěčům	potápěč	k1gMnPc3
vyzvednout	vyzvednout	k5eAaPmF
černé	černý	k2eAgFnPc4d1
skříňky	skříňka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
předběžné	předběžný	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
dat	datum	k1gNnPc2
vyšetřovatelé	vyšetřovatel	k1gMnPc1
stanovili	stanovit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
motory	motor	k1gInPc1
pracovaly	pracovat	k5eAaImAgInP
až	až	k9
do	do	k7c2
nárazu	náraz	k1gInSc2
do	do	k7c2
překážky	překážka	k1gFnSc2
a	a	k8xC
že	že	k8xS
vztlakové	vztlakový	k2eAgFnPc1d1
klapky	klapka	k1gFnPc1
byly	být	k5eAaImAgFnP
ve	v	k7c6
správné	správný	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
pro	pro	k7c4
vzlet	vzlet	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
prokuratura	prokuratura	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
letadlo	letadlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
po	po	k7c6
řádné	řádný	k2eAgFnSc6d1
údržbě	údržba	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
byl	být	k5eAaImAgMnS
vyměněn	vyměnit	k5eAaPmNgInS
jeden	jeden	k4xCgInSc1
motor	motor	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Předsedkyně	předsedkyně	k1gFnPc1
MAK	mako	k1gNnPc2
Taťjana	Taťjana	k1gFnSc1
Anodinová	Anodinový	k2eAgFnSc1d1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
letoun	letoun	k1gInSc1
nebyl	být	k5eNaImAgInS
přetížen	přetížit	k5eAaPmNgInS
a	a	k8xC
podle	podle	k7c2
dat	datum	k1gNnPc2
z	z	k7c2
černých	černý	k2eAgFnPc2d1
skříněk	skříňka	k1gFnPc2
fungovaly	fungovat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
okamžiku	okamžik	k1gInSc2
nehody	nehoda	k1gFnSc2
všechny	všechen	k3xTgInPc4
systémy	systém	k1gInPc4
letounu	letoun	k1gInSc2
správně	správně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přepis	přepis	k1gInSc1
zapisovače	zapisovač	k1gInSc2
zvuků	zvuk	k1gInPc2
v	v	k7c6
pilotní	pilotní	k2eAgFnSc6d1
kabině	kabina	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
v	v	k7c6
tisku	tisk	k1gInSc6
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
motory	motor	k1gInPc1
byly	být	k5eAaImAgInP
záměrně	záměrně	k6eAd1
nastaveny	nastavit	k5eAaPmNgInP
na	na	k7c4
redukovaný	redukovaný	k2eAgInSc4d1
tah	tah	k1gInSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
nominální	nominální	k2eAgFnSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
77	#num#	k4
%	%	kIx~
vzletového	vzletový	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzletový	vzletový	k2eAgInSc1d1
tah	tah	k1gInSc1
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
až	až	k9
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
pilot	pilot	k1gMnSc1
nedokázal	dokázat	k5eNaPmAgMnS
navzdory	navzdory	k7c3
dostatečné	dostatečný	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
letoun	letoun	k1gInSc4
rotovat	rotovat	k5eAaImF
(	(	kIx(
<g/>
zvednout	zvednout	k5eAaPmF
příďovou	příďový	k2eAgFnSc4d1
podvozkovou	podvozkový	k2eAgFnSc4d1
nohu	noha	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
vzletu	vzlet	k1gInSc2
řešila	řešit	k5eAaImAgFnS
posádka	posádka	k1gFnSc1
také	také	k9
přenastavení	přenastavení	k1gNnSc4
horizontálního	horizontální	k2eAgInSc2d1
stabilizátoru	stabilizátor	k1gInSc2
(	(	kIx(
<g/>
vodorovná	vodorovný	k2eAgFnSc1d1
ocasní	ocasní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
,	,	kIx,
fungující	fungující	k2eAgFnSc1d1
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
výškové	výškový	k2eAgNnSc1d1
kormidlo	kormidlo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
oficiálního	oficiální	k2eAgNnSc2d1
vyjádření	vyjádření	k1gNnSc2
MAK	mako	k1gNnPc2
zahájila	zahájit	k5eAaPmAgFnS
posádka	posádka	k1gFnSc1
vzlet	vzlet	k1gInSc1
řádně	řádně	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
redukovaným	redukovaný	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
<g/>
,	,	kIx,
se	s	k7c7
správně	správně	k6eAd1
vyváženým	vyvážený	k2eAgNnSc7d1
i	i	k8xC
nastaveným	nastavený	k2eAgNnSc7d1
letadlem	letadlo	k1gNnSc7
<g/>
,	,	kIx,
z	z	k7c2
místa	místo	k1gNnSc2
dráhy	dráha	k1gFnSc2
23	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
do	do	k7c2
ní	on	k3xPp3gFnSc2
ústí	ústit	k5eAaImIp3nS
pojížděcí	pojížděcí	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
5	#num#	k4
a	a	k8xC
měli	mít	k5eAaImAgMnP
před	před	k7c7
sebou	se	k3xPyFc7
délku	délka	k1gFnSc4
ještě	ještě	k9
2700	#num#	k4
m	m	kA
(	(	kIx(
<g/>
potřebná	potřebný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
v	v	k7c6
daném	daný	k2eAgInSc6d1
případě	případ	k1gInSc6
byla	být	k5eAaImAgFnS
cca	cca	kA
1	#num#	k4
200	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
cca	cca	kA
165	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
letadlo	letadlo	k1gNnSc1
zrychlovalo	zrychlovat	k5eAaImAgNnS
normálně	normálně	k6eAd1
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
zrychlování	zrychlování	k1gNnSc1
zpomalilo	zpomalit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
rotaci	rotace	k1gFnSc4
při	při	k7c6
chybně	chybně	k6eAd1
stanovené	stanovený	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc3
185	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
správně	správně	k6eAd1
210	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letoun	letoun	k1gInSc1
však	však	k9
na	na	k7c4
změnu	změna	k1gFnSc4
výškovky	výškovka	k1gFnSc2
nereagoval	reagovat	k5eNaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
se	se	k3xPyFc4
domnívali	domnívat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
překročili	překročit	k5eAaPmAgMnP
již	již	k6eAd1
(	(	kIx(
<g/>
chybně	chybně	k6eAd1
stanovenou	stanovený	k2eAgFnSc4d1
<g/>
)	)	kIx)
rychlost	rychlost	k1gFnSc4
V	v	k7c4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
je	být	k5eAaImIp3nS
ještě	ještě	k9
možno	možno	k6eAd1
bezpečně	bezpečně	k6eAd1
přerušit	přerušit	k5eAaPmF
vzlet	vzlet	k1gInSc4
a	a	k8xC
zabrzdit	zabrzdit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
dal	dát	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
instrukci	instrukce	k1gFnSc3
k	k	k7c3
plnému	plný	k2eAgInSc3d1
vzletovému	vzletový	k2eAgInSc3d1
tahu	tah	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
nastavený	nastavený	k2eAgInSc4d1
vzletový	vzletový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
akceleroval	akcelerovat	k5eAaImAgInS
letoun	letoun	k1gInSc1
jen	jen	k9
zvolna	zvolna	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
dosáhl	dosáhnout	k5eAaPmAgInS
(	(	kIx(
<g/>
dostatečné	dostatečný	k2eAgFnPc4d1
<g/>
)	)	kIx)
nejvyšší	vysoký	k2eAgFnPc4d3
rychlosti	rychlost	k1gFnPc4
230	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Důvod	důvod	k1gInSc4
nízké	nízký	k2eAgFnSc2d1
akcelerace	akcelerace	k1gFnSc2
a	a	k8xC
neschopnosti	neschopnost	k1gFnSc2
zvednout	zvednout	k5eAaPmF
letadlo	letadlo	k1gNnSc4
spatřují	spatřovat	k5eAaImIp3nP
vyšetřovatelé	vyšetřovatel	k1gMnPc1
v	v	k7c6
objevení	objevení	k1gNnSc6
se	se	k3xPyFc4
brzdné	brzdný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc4
původ	původ	k1gInSc4
předpokládají	předpokládat	k5eAaImIp3nP
v	v	k7c6
nevědomém	vědomý	k2eNgNnSc6d1
přibrzďování	přibrzďování	k1gNnSc6
druhého	druhý	k4xOgMnSc2
pilota	pilot	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
letoun	letoun	k1gInSc1
přejel	přejet	k5eAaPmAgInS
konec	konec	k1gInSc4
dráhy	dráha	k1gFnSc2
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgInS
ještě	ještě	k9
asi	asi	k9
450	#num#	k4
m	m	kA
po	po	k7c6
trávě	tráva	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
začali	začít	k5eAaPmAgMnP
přestavovat	přestavovat	k5eAaImF
horizontální	horizontální	k2eAgInSc4d1
stabilizátor	stabilizátor	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
posádce	posádka	k1gFnSc3
nakonec	nakonec	k6eAd1
stroj	stroj	k1gInSc4
podařilo	podařit	k5eAaPmAgNnS
zvednout	zvednout	k5eAaPmF
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
<g/>
,	,	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kolizi	kolize	k1gFnSc3
s	s	k7c7
anténami	anténa	k1gFnPc7
a	a	k8xC
kontejnerem	kontejner	k1gInSc7
LLZ	LLZ	kA
<g/>
,	,	kIx,
narostl	narůst	k5eAaPmAgMnS
po	po	k7c6
odlepení	odlepení	k1gNnSc6
prudce	prudko	k6eAd1
<g/>
,	,	kIx,
během	běh	k1gInSc7
asi	asi	k9
dvou	dva	k4xCgFnPc2
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
úhel	úhel	k1gInSc4
náběhu	náběh	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
letadlo	letadlo	k1gNnSc1
přešlo	přejít	k5eAaPmAgNnS
samovolně	samovolně	k6eAd1
do	do	k7c2
pádu	pád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
dosažená	dosažený	k2eAgFnSc1d1
výška	výška	k1gFnSc1
byla	být	k5eAaImAgFnS
asi	asi	k9
5	#num#	k4
m.	m.	k?
I	i	k8xC
toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vysvětlit	vysvětlit	k5eAaPmF
vymizením	vymizení	k1gNnSc7
klopného	klopný	k2eAgInSc2d1
momentu	moment	k1gInSc2
brzdícího	brzdící	k2eAgInSc2d1
hlavního	hlavní	k2eAgInSc2d1
podvozku	podvozek	k1gInSc2
po	po	k7c6
ztrátě	ztráta	k1gFnSc6
kontaktu	kontakt	k1gInSc2
se	s	k7c7
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
náklonu	náklon	k1gInSc6
narazil	narazit	k5eAaPmAgMnS
do	do	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
destrukci	destrukce	k1gFnSc3
křídla	křídlo	k1gNnSc2
a	a	k8xC
vznícení	vznícení	k1gNnSc2
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
po	po	k7c4
přetočení	přetočení	k1gNnSc4
na	na	k7c4
záda	záda	k1gNnPc4
k	k	k7c3
destrukci	destrukce	k1gFnSc3
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zahynula	zahynout	k5eAaPmAgFnS
většina	většina	k1gFnSc1
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
ustanovení	ustanovení	k1gNnSc3
přesných	přesný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
a	a	k8xC
příčin	příčina	k1gFnPc2
nehody	nehoda	k1gFnSc2
se	se	k3xPyFc4
vyšetřovatelé	vyšetřovatel	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
provést	provést	k5eAaPmF
na	na	k7c6
základě	základ	k1gInSc6
dostupných	dostupný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
počítačové	počítačový	k2eAgFnSc2d1
simulace	simulace	k1gFnSc2
a	a	k8xC
pokusné	pokusný	k2eAgInPc4d1
lety	let	k1gInPc4
identického	identický	k2eAgInSc2d1
letounu	letoun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experimenty	experiment	k1gInPc7
se	se	k3xPyFc4
prováděly	provádět	k5eAaImAgFnP
<g/>
,	,	kIx,
od	od	k7c2
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
s	s	k7c7
letounem	letoun	k1gInSc7
společnosti	společnost	k1gFnSc2
Yak	Yak	k1gFnSc2
Service	Service	k1gFnSc2
na	na	k7c4
5,4	5,4	k4
km	km	kA
dlouhé	dlouhý	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
letiště	letiště	k1gNnSc2
Ramenskoje	Ramenskoj	k1gInSc2
(	(	kIx(
<g/>
55	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
38	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zachráněný	zachráněný	k2eAgMnSc1d1
mechanik	mechanik	k1gMnSc1
vypověděl	vypovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
není	být	k5eNaImIp3nS
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
letoun	letoun	k1gInSc1
vykazoval	vykazovat	k5eAaImAgInS
nějaké	nějaký	k3yIgFnPc4
závady	závada	k1gFnPc4
před	před	k7c7
<g/>
,	,	kIx,
ani	ani	k8xC
během	během	k7c2
nehody	nehoda	k1gFnSc2
a	a	k8xC
upřesnil	upřesnit	k5eAaPmAgMnS
hmotnosti	hmotnost	k1gFnSc3
osob	osoba	k1gFnPc2
a	a	k8xC
nákladu	náklad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
pro	pro	k7c4
média	médium	k1gNnPc4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
seděl	sedět	k5eAaImAgMnS
nepřipoutaný	připoutaný	k2eNgMnSc1d1
v	v	k7c6
kabině	kabina	k1gFnSc6
pro	pro	k7c4
cestující	cestující	k1gMnPc4
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
letoun	letoun	k1gInSc1
dlouho	dlouho	k6eAd1
nezvedal	zvedat	k5eNaImAgInS
do	do	k7c2
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
zavládla	zavládnout	k5eAaPmAgFnS
mezi	mezi	k7c7
pasažéry	pasažér	k1gMnPc7
nervozita	nervozita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
členy	člen	k1gMnPc4
posádky	posádka	k1gFnSc2
měly	mít	k5eAaImAgFnP
existovat	existovat	k5eAaImF
velmi	velmi	k6eAd1
přátelské	přátelský	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
a	a	k8xC
sám	sám	k3xTgInSc1
prý	prý	k9
měl	mít	k5eAaImAgInS
předtuchu	předtucha	k1gFnSc4
blížící	blížící	k2eAgFnSc2d1
se	se	k3xPyFc4
katastrofy	katastrofa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
podrobná	podrobný	k2eAgFnSc1d1
a	a	k8xC
obsáhlá	obsáhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zveřejněna	zveřejněn	k2eAgFnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vlastním	vlastní	k2eAgInSc6d1
letounu	letoun	k1gInSc6
<g/>
,	,	kIx,
palivu	palivo	k1gNnSc6
<g/>
,	,	kIx,
stavu	stav	k1gInSc6
letiště	letiště	k1gNnSc2
nebo	nebo	k8xC
povětrnostních	povětrnostní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
neshledali	shledat	k5eNaPmAgMnP
vyšetřovatelé	vyšetřovatel	k1gMnPc1
žádné	žádný	k3yNgFnSc2
mimořádné	mimořádný	k2eAgFnSc2d1
okolnosti	okolnost	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
vést	vést	k5eAaImF
ke	k	k7c3
vzniku	vznik	k1gInSc3
nehody	nehoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zpráva	zpráva	k1gFnSc1
však	však	k9
uvádí	uvádět	k5eAaImIp3nS
významné	významný	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
v	v	k7c6
práci	práce	k1gFnSc6
vedení	vedení	k1gNnSc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
vedoucích	vedoucí	k2eAgFnPc6d1
funkcích	funkce	k1gFnPc6
nebyl	být	k5eNaImAgInS
nikdo	nikdo	k3yNnSc1
odborně	odborně	k6eAd1
kvalifikovaný	kvalifikovaný	k2eAgMnSc1d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
letectví	letectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
shledány	shledán	k2eAgInPc1d1
závažné	závažný	k2eAgInPc1d1
nedostatky	nedostatek	k1gInPc1
ve	v	k7c6
vedení	vedení	k1gNnSc6
výcviku	výcvik	k1gInSc3
posádek	posádka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mimo	mimo	k6eAd1
jiné	jiný	k2eAgNnSc1d1
způsobilo	způsobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
přechod	přechod	k1gInSc1
obou	dva	k4xCgMnPc2
pilotů	pilot	k1gMnPc2
z	z	k7c2
typu	typ	k1gInSc2
Jak-	Jak-	k1gFnSc2
<g/>
40	#num#	k4
na	na	k7c4
Jak-	Jak-	k1gFnSc4
<g/>
42	#num#	k4
neproběhl	proběhnout	k5eNaPmAgInS
uspokojivým	uspokojivý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
a	a	k8xC
posádky	posádka	k1gFnSc2
si	se	k3xPyFc3
na	na	k7c4
nový	nový	k2eAgInSc4d1
typ	typ	k1gInSc4
mohly	moct	k5eAaImAgInP
přenést	přenést	k5eAaPmF
návyky	návyk	k1gInPc4
z	z	k7c2
předchozího	předchozí	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
zejména	zejména	k9
položení	položení	k1gNnSc1
nohou	noha	k1gFnPc2
při	při	k7c6
vzletu	vzlet	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
u	u	k7c2
Jak-	Jak-	k1gFnSc2
<g/>
40	#num#	k4
bývají	bývat	k5eAaImIp3nP
u	u	k7c2
pedálů	pedál	k1gInPc2
zvláštní	zvláštní	k2eAgFnSc2d1
opěrky	opěrka	k1gFnSc2
na	na	k7c4
paty	pata	k1gFnPc4
a	a	k8xC
nohy	noha	k1gFnPc4
proto	proto	k8xC
zůstávají	zůstávat	k5eAaImIp3nP
po	po	k7c4
dobu	doba	k1gFnSc4
vzletu	vzlet	k1gInSc2
na	na	k7c6
pedálech	pedál	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak-	Jak-	k1gFnSc1
<g/>
42	#num#	k4
má	mít	k5eAaImIp3nS
pedály	pedál	k1gInPc4
bez	bez	k7c2
opěrek	opěrka	k1gFnPc2
a	a	k8xC
nohy	noha	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
pokládat	pokládat	k5eAaImF
na	na	k7c4
podlahu	podlaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Jak-	Jak-	k1gMnSc1
<g/>
40	#num#	k4
byl	být	k5eAaImAgMnS
první	první	k4xOgMnSc1
dopravní	dopravní	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Jakovlevovy	Jakovlevův	k2eAgFnSc2d1
konstrukční	konstrukční	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
konstruovali	konstruovat	k5eAaImAgMnP
především	především	k6eAd1
vojenské	vojenský	k2eAgInPc4d1
a	a	k8xC
cvičné	cvičný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
uvedené	uvedený	k2eAgFnSc2d1
opěrky	opěrka	k1gFnSc2
někdy	někdy	k6eAd1
s	s	k7c7
oblibou	obliba	k1gFnSc7
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dopravních	dopravní	k2eAgInPc6d1
letadlech	letadlo	k1gNnPc6
ale	ale	k8xC
nejsou	být	k5eNaImIp3nP
obvyklé	obvyklý	k2eAgFnPc1d1
a	a	k8xC
u	u	k7c2
modelu	model	k1gInSc2
Jak-	Jak-	k1gFnSc2
<g/>
42	#num#	k4
se	se	k3xPyFc4
už	už	k6eAd1
montovaly	montovat	k5eAaImAgInP
standardní	standardní	k2eAgInPc1d1
pedály	pedál	k1gInPc1
(	(	kIx(
<g/>
na	na	k7c6
zemi	zem	k1gFnSc6
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
brždění	bržděný	k2eAgMnPc1d1
jednotlivých	jednotlivý	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
a	a	k8xC
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
kormidla	kormidlo	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Dalším	další	k2eAgInSc7d1
aspektem	aspekt	k1gInSc7
absence	absence	k1gFnSc2
odborného	odborný	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
bylo	být	k5eAaImAgNnS
rutinní	rutinní	k2eAgNnSc4d1
zanedbávání	zanedbávání	k1gNnSc4
a	a	k8xC
porušování	porušování	k1gNnSc4
předepsaných	předepsaný	k2eAgInPc2d1
letových	letový	k2eAgInPc2d1
postupů	postup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
osudového	osudový	k2eAgInSc2d1
letu	let	k1gInSc2
například	například	k6eAd1
nebyla	být	k5eNaImAgFnS
řádně	řádně	k6eAd1
spočítána	spočítán	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
stroje	stroj	k1gInSc2
ani	ani	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
vyvážení	vyvážení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
vyplynulo	vyplynout	k5eAaPmAgNnS
chybné	chybný	k2eAgNnSc4d1
stanovení	stanovení	k1gNnSc4
mezní	mezní	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
pro	pro	k7c4
přerušení	přerušení	k1gNnSc4
vzletu	vzlet	k1gInSc2
i	i	k8xC
rotaci	rotace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
okolnost	okolnost	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
mohla	moct	k5eAaImAgFnS
projevit	projevit	k5eAaPmF
na	na	k7c6
vzniku	vznik	k1gInSc6
nehody	nehoda	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
kapitán	kapitán	k1gMnSc1
se	s	k7c7
přitažením	přitažení	k1gNnSc7
řídícího	řídící	k2eAgInSc2d1
sloupku	sloupek	k1gInSc2
snažil	snažit	k5eAaImAgInS
letoun	letoun	k1gInSc1
rotovat	rotovat	k5eAaImF
příliš	příliš	k6eAd1
brzy	brzy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
toho	ten	k3xDgMnSc4
dosáhl	dosáhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
přitahoval	přitahovat	k5eAaImAgMnS
patrně	patrně	k6eAd1
s	s	k7c7
druhým	druhý	k4xOgMnSc7
pilotem	pilot	k1gMnSc7
sloupek	sloupek	k1gInSc1
větší	veliký	k2eAgFnSc4d2
a	a	k8xC
větší	veliký	k2eAgFnSc4d2
silou	síla	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
opřením	opření	k1gNnSc7
se	se	k3xPyFc4
o	o	k7c6
pedály	pedál	k1gInPc7
dobrzďoval	dobrzďovat	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
vyvolalo	vyvolat	k5eAaPmAgNnS
naklápění	naklápění	k1gNnSc1
stroje	stroj	k1gInSc2
na	na	k7c4
nos	nos	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
při	při	k7c6
dostatečné	dostatečný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
už	už	k6eAd1
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
odlepit	odlepit	k5eAaPmF
přední	přední	k2eAgFnPc4d1
podvozkovou	podvozkový	k2eAgFnSc4d1
nohu	noha	k1gFnSc4
od	od	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiným	jiný	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
absence	absence	k1gFnSc2
řádné	řádný	k2eAgFnSc2d1
předletové	předletový	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
nepřerušili	přerušit	k5eNaPmAgMnP
vzlet	vzlet	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
domnívali	domnívat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
překročili	překročit	k5eAaPmAgMnP
kritickou	kritický	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
tomu	ten	k3xDgNnSc3
tak	tak	k9
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chyb	chyba	k1gFnPc2
a	a	k8xC
zanedbání	zanedbání	k1gNnSc1
jmenují	jmenovat	k5eAaImIp3nP,k5eAaBmIp3nP
vyšetřovatelé	vyšetřovatel	k1gMnPc1
více	hodně	k6eAd2
<g/>
,	,	kIx,
kromě	kromě	k7c2
uvedených	uvedený	k2eAgNnPc2d1
však	však	k9
na	na	k7c4
vznik	vznik	k1gInSc4
nehody	nehoda	k1gFnSc2
mohla	moct	k5eAaImAgFnS
mít	mít	k5eAaImF
vliv	vliv	k1gInSc4
nesecvičenost	nesecvičenost	k1gFnSc4
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
projevující	projevující	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
závěrečné	závěrečný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
vzletu	vzlet	k1gInSc2
(	(	kIx(
<g/>
krátkodobé	krátkodobý	k2eAgNnSc1d1
stažení	stažení	k1gNnSc1
motorů	motor	k1gInPc2
na	na	k7c4
volnoběh	volnoběh	k1gInSc4
při	při	k7c6
jízdě	jízda	k1gFnSc6
po	po	k7c6
trávě	tráva	k1gFnSc6
zřejmě	zřejmě	k6eAd1
mechanikem	mechanik	k1gMnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
piloti	pilot	k1gMnPc1
se	se	k3xPyFc4
zoufale	zoufale	k6eAd1
snažili	snažit	k5eAaImAgMnP
zvednout	zvednout	k5eAaPmF
stroj	stroj	k1gInSc4
přitahováním	přitahování	k1gNnSc7
sloupku	sloupek	k1gInSc2
řízení	řízení	k1gNnSc2
silou	síla	k1gFnSc7
až	až	k9
700	#num#	k4
N	N	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Závažným	závažný	k2eAgInSc7d1
problémem	problém	k1gInSc7
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
být	být	k5eAaImF
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
druhého	druhý	k4xOgMnSc2
pilota	pilot	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gFnPc6
tkáních	tkáň	k1gFnPc6
byly	být	k5eAaImAgFnP
objeveny	objeven	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
pro	pro	k7c4
piloty	pilota	k1gFnPc4
zakázaného	zakázaný	k2eAgInSc2d1
fenobarbitalu	fenobarbital	k1gMnSc3
<g/>
,	,	kIx,
látky	látka	k1gFnPc1
s	s	k7c7
tlumivými	tlumivý	k2eAgInPc7d1
účinky	účinek	k1gInPc7
na	na	k7c4
nervovou	nervový	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
pilot	pilot	k1gMnSc1
se	se	k3xPyFc4
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
léčil	léčit	k5eAaImAgMnS
na	na	k7c6
neurologii	neurologie	k1gFnSc6
s	s	k7c7
poruchou	porucha	k1gFnSc7
zhoršujícího	zhoršující	k2eAgMnSc2d1
se	se	k3xPyFc4
přenosu	přenos	k1gInSc2
nervových	nervový	k2eAgInPc2d1
signálů	signál	k1gInPc2
periferního	periferní	k2eAgNnSc2d1
nervstva	nervstvo	k1gNnSc2
<g/>
,	,	kIx,
neuropatií	neuropatie	k1gFnPc2
<g/>
,	,	kIx,
projevující	projevující	k2eAgInPc4d1
se	se	k3xPyFc4
postupující	postupující	k2eAgFnSc7d1
ztrátou	ztráta	k1gFnSc7
citlivosti	citlivost	k1gFnSc2
a	a	k8xC
zhoršenou	zhoršený	k2eAgFnSc7d1
koordinací	koordinace	k1gFnSc7
pohybů	pohyb	k1gInPc2
dolních	dolní	k2eAgFnPc2d1
končetin	končetina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
si	se	k3xPyFc3
nemusel	muset	k5eNaImAgMnS
být	být	k5eAaImF
druhý	druhý	k4xOgInSc1
pilot	pilot	k1gInSc1
vědom	vědom	k2eAgMnSc1d1
skutečné	skutečný	k2eAgInPc4d1
polohy	poloh	k1gInPc4
nohou	noha	k1gFnPc2
a	a	k8xC
tlaku	tlak	k1gInSc2
na	na	k7c4
brzdové	brzdový	k2eAgInPc4d1
pedály	pedál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
porucha	porucha	k1gFnSc1
je	být	k5eAaImIp3nS
jednoznačně	jednoznačně	k6eAd1
neslučitelná	slučitelný	k2eNgFnSc1d1
s	s	k7c7
výkonem	výkon	k1gInSc7
povolání	povolání	k1gNnSc2
pilota	pilota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
závěr	závěr	k1gInSc4
obsahuje	obsahovat	k5eAaImIp3nS
zpráva	zpráva	k1gFnSc1
několik	několik	k4yIc4
stran	strana	k1gFnPc2
doporučení	doporučení	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
zabránit	zabránit	k5eAaPmF
zjištěným	zjištěný	k2eAgFnPc3d1
chybám	chyba	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osoby	osoba	k1gFnPc1
na	na	k7c6
palubě	paluba	k1gFnSc6
</s>
<s>
Posádka	posádka	k1gFnSc1
</s>
<s>
JménoZeměPozicePoznámka	JménoZeměPozicePoznámka	k1gFnSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Anatolijevič	Anatolijevič	k1gMnSc1
Solomencev	Solomencva	k1gFnPc2
Ruskokapitán	Ruskokapitán	k1gMnSc1
letadlazemřel	letadlazemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Igor	Igor	k1gMnSc1
Konstantinovič	Konstantinovič	k1gMnSc1
Ževelov	Ževelov	k1gInSc4
Ruskodruhý	Ruskodruhý	k2eAgInSc4d1
pilotzemřel	pilotzemřet	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Sergej	Sergej	k1gMnSc1
Valerjevič	Valerjevič	k1gMnSc1
Žuravljov	Žuravljov	k1gInSc4
Ruskomechanikzemřel	Ruskomechanikzemřel	k1gMnSc3
</s>
<s>
Jelena	Jelena	k1gFnSc1
Sarmatovová	Sarmatovová	k1gFnSc1
Ruskoletuškazemřela	Ruskoletuškazemřela	k1gFnSc1
</s>
<s>
Naděžda	Naděžda	k1gFnSc1
Muzafarovna	Muzafarovna	k1gFnSc1
Maksumovová	Maksumovová	k1gFnSc1
Ruskoletuškazemřela	Ruskoletuškazemřela	k1gFnSc1
</s>
<s>
Jelena	Jelena	k1gFnSc1
Šavinová	Šavinová	k1gFnSc1
Ruskoletuškazemřela	Ruskoletuškazemřela	k1gFnSc1
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Maťuškin	Maťuškin	k1gMnSc1
Ruskoletový	ruskoletový	k2eAgMnSc1d1
inženýrzemřel	inženýrzemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Borisovič	Borisovič	k1gMnSc1
Sizov	Sizov	k1gInSc4
Ruskoletový	ruskoletový	k2eAgInSc4d1
inženýrpřežil	inženýrpřežit	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Hokejový	hokejový	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
HráčVěkZeměPozicePoznámka	HráčVěkZeměPozicePoznámka	k1gFnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Vjuchin	Vjuchin	k1gMnSc1
<g/>
38	#num#	k4
letUkrajinabrankářzemřel	letUkrajinabrankářzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Stefan	Stefan	k1gMnSc1
Liv	Liv	k1gMnSc1
<g/>
30	#num#	k4
letŠvédskobrankářzemřel	letŠvédskobrankářzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Pavel	Pavel	k1gMnSc1
Trachanov	Trachanov	k1gInSc4
<g/>
33	#num#	k4
letRuskoobráncezemřel	letRuskoobráncezemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Uryčev	Uryčev	k1gMnSc5
<g/>
20	#num#	k4
letRuskoobráncezemřel	letRuskoobráncezemřet	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Michail	Michail	k1gMnSc1
Balandin	Balandin	k2eAgMnSc1d1
<g/>
31	#num#	k4
letRuskoobráncezemřel	letRuskoobráncezemřet	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Robert	Robert	k1gMnSc1
Dietrich	Dietrich	k1gMnSc1
<g/>
25	#num#	k4
letNěmeckoobráncezemřel	letNěmeckoobráncezemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Karel	Karel	k1gMnSc1
Rachůnek	Rachůnek	k1gMnSc1
<g/>
32	#num#	k4
letČeskoobráncezemřel	letČeskoobráncezemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Ruslan	Ruslan	k1gInSc1
Salej	Salej	k1gInSc1
<g/>
36	#num#	k4
letBěloruskoobráncezemřel	letBěloruskoobráncezemřet	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Maxim	Maxim	k1gMnSc1
Šuvalov	Šuvalov	k1gInSc4
<g/>
18	#num#	k4
letRuskoobráncezemřel	letRuskoobráncezemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Marat	Marat	k1gInSc1
Kalimulin	Kalimulin	k2eAgInSc1d1
<g/>
23	#num#	k4
letRuskoobráncezemřel	letRuskoobráncezemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Kā	Kā	k1gInSc1
Skrastiņ	Skrastiņ	k1gInSc1
<g/>
37	#num#	k4
letLotyšskoobráncezemřel	letLotyšskoobráncezemřet	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Sergej	Sergej	k1gMnSc1
Ostapčuk	Ostapčuk	k1gMnSc1
<g/>
21	#num#	k4
letBěloruskolevé	letBěloruskolevá	k1gFnSc2
křídlozemřel	křídlozemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Ivan	Ivan	k1gMnSc1
Tkačenko	Tkačenka	k1gFnSc5
<g/>
31	#num#	k4
letRuskolevé	letRuskolevá	k1gFnSc6
křídlozemřel	křídlozemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Vasjunov	Vasjunov	k1gInSc4
<g/>
23	#num#	k4
letRuskolevé	letRuskolevá	k1gFnSc2
křídlozemřel	křídlozemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Arťom	Arťom	k1gInSc1
Jarčuk	Jarčuk	k1gInSc1
<g/>
21	#num#	k4
letRuskolevé	letRuskolevá	k1gFnSc2
křídlozemřel	křídlozemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Vitalij	Vitalít	k5eAaPmRp2nS
Anikejenko	Anikejenka	k1gFnSc5
<g/>
24	#num#	k4
letRuskostřední	letRuskostřednit	k5eAaPmIp3nS
útočníkzemřel	útočníkzemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Gennadij	Gennadít	k5eAaPmRp2nS
Čurilov	Čurilov	k1gInSc4
<g/>
24	#num#	k4
letRuskostřední	letRuskostřednit	k5eAaPmIp3nS
útočníkzemřel	útočníkzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Pavol	Pavol	k1gInSc1
Demitra	Demitra	k1gFnSc1
<g/>
36	#num#	k4
letSlovenskostřední	letSlovenskostřednit	k5eAaPmIp3nS
útočníkzemřel	útočníkzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Nikita	Nikita	k1gFnSc1
Kljukin	Kljukin	k1gInSc1
<g/>
21	#num#	k4
letRuskostřední	letRuskostřednit	k5eAaPmIp3nS
útočníkzemřel	útočníkzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Jan	Jan	k1gMnSc1
Marek	Marek	k1gMnSc1
<g/>
31	#num#	k4
letČeskostřední	letČeskostřednit	k5eAaPmIp3nS
útočníkzemřel	útočníkzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
/	/	kIx~
Daniil	Daniil	k1gMnSc1
Sobčenko	Sobčenka	k1gFnSc5
<g/>
20	#num#	k4
letRusko	letRusko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Ukrajinastřední	Ukrajinastřední	k2eAgMnSc1d1
útočníkzemřel	útočníkzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Josef	Josef	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
<g/>
30	#num#	k4
letČeskostřední	letČeskostřednit	k5eAaPmIp3nS
útočníkzemřel	útočníkzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Galimov	Galimov	k1gInSc4
<g/>
26	#num#	k4
letRuskopravé	letRuskopravý	k2eAgInPc1d1
křídlozemřel	křídlozemřet	k5eAaPmAgInS,k5eAaImAgInS
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Kaljanin	Kaljanin	k2eAgMnSc1d1
<g/>
23	#num#	k4
letRuskopravé	letRuskopravý	k2eAgInPc4d1
křídlozemřel	křídlozemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Andrej	Andrej	k1gMnSc1
Kirjuchin	Kirjuchin	k1gInSc4
<g/>
24	#num#	k4
letRuskopravé	letRuskopravý	k2eAgInPc1d1
křídlozemřel	křídlozemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Pavel	Pavel	k1gMnSc1
Snurnicyn	Snurnicyn	k1gMnSc1
<g/>
19	#num#	k4
letRuskoútočníkzemřel	letRuskoútočníkzemřet	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Realizační	realizační	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
JménoVěkZeměPozicePoznámka	JménoVěkZeměPozicePoznámka	k1gFnSc1
</s>
<s>
Brad	brada	k1gFnPc2
McCrimmon	McCrimmon	k1gNnSc1
<g/>
52	#num#	k4
let	léto	k1gNnPc2
Kanadahlavní	Kanadahlavní	k2eAgFnSc2d1
trenérzemřel	trenérzemřet	k5eAaPmAgInS,k5eAaImAgInS
</s>
<s>
Igor	Igor	k1gMnSc1
Koroljov	Koroljov	k1gInSc4
<g/>
41	#num#	k4
let	léto	k1gNnPc2
Ruskotrenérzemřel	Ruskotrenérzemřel	k1gMnPc2
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
<g/>
41	#num#	k4
let	léto	k1gNnPc2
Ruskotrenérzemřel	Ruskotrenérzemřel	k1gMnPc2
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Krivonosov	Krivonosov	k1gInSc4
<g/>
31	#num#	k4
let	léto	k1gNnPc2
Běloruskokondiční	běloruskokondiční	k2eAgFnSc2d1
trenérzemřel	trenérzemřet	k5eAaPmAgInS,k5eAaImAgInS
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Bachvalov	Bachvalov	k1gInSc4
Ruskovideooperátorzemřel	Ruskovideooperátorzemřel	k1gMnSc2
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Beljajev	Beljajev	k1gFnSc2
Ruskokustod	Ruskokustod	k1gInSc1
<g/>
/	/	kIx~
<g/>
masérzemřel	masérzemřet	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Kunnov	Kunnov	k1gInSc4
Ruskomasérzemřel	Ruskomasérzemřel	k1gMnSc2
</s>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1
Kuzněcov	Kuzněcov	k1gInSc4
Ruskomasérzemřel	Ruskomasérzemřel	k1gFnSc2
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Piskunov	Piskunov	k1gInSc4
Ruskoadministrátorzemřel	Ruskoadministrátorzemřel	k1gFnSc2
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Sidorov	Sidorov	k1gInSc1
Ruskoanalytický	ruskoanalytický	k2eAgInSc4d1
trenérzemřel	trenérzemřet	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Andrej	Andrej	k1gMnSc1
Zimin	Zimin	k1gMnSc1
Ruskolékařzemřel	Ruskolékařzemřel	k1gMnSc1
</s>
<s>
Sportovní	sportovní	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
</s>
<s>
Lokomotiv	lokomotiva	k1gFnPc2
Jaroslavl	Jaroslavl	k1gMnSc1
přišel	přijít	k5eAaPmAgMnS
prakticky	prakticky	k6eAd1
o	o	k7c4
celý	celý	k2eAgInSc4d1
hokejový	hokejový	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvažovaly	zvažovat	k5eAaImAgInP
se	se	k3xPyFc4
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jej	on	k3xPp3gMnSc4
nahradit	nahradit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
uvažovanými	uvažovaný	k2eAgFnPc7d1
variantami	varianta	k1gFnPc7
bylo	být	k5eAaImAgNnS
složení	složení	k1gNnSc1
nového	nový	k2eAgInSc2d1
týmu	tým	k1gInSc2
z	z	k7c2
hráčů	hráč	k1gMnPc2
z	z	k7c2
ostatních	ostatní	k2eAgInPc2d1
klubů	klub	k1gInPc2
KHL	KHL	kA
<g/>
,	,	kIx,
mimořádný	mimořádný	k2eAgInSc4d1
draft	draft	k1gInSc4
či	či	k8xC
tým	tým	k1gInSc4
poskládat	poskládat	k5eAaPmF
z	z	k7c2
hráčů	hráč	k1gMnPc2
hrající	hrající	k2eAgMnSc1d1
za	za	k7c7
Jaroslavl	Jaroslavl	k1gFnSc7
v	v	k7c4
juniorské	juniorský	k2eAgNnSc4d1
MHL	MHL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Lokomotiv	lokomotiva	k1gFnPc2
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
nezúčastnit	zúčastnit	k5eNaPmF
se	se	k3xPyFc4
probíhajícího	probíhající	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
při	při	k7c6
nehodě	nehoda	k1gFnSc6
o	o	k7c4
tři	tři	k4xCgInPc4
stabilní	stabilní	k2eAgInPc4d1
členy	člen	k1gInPc4
širšího	široký	k2eAgInSc2d2
kádru	kádr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
byli	být	k5eAaImAgMnP
držiteli	držitel	k1gMnPc7
titulu	titul	k1gInSc2
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
Josef	Josef	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
2005	#num#	k4
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Rachůnek	Rachůnek	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Marek	Marek	k1gMnSc1
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Český	český	k2eAgInSc1d1
hokejový	hokejový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c4
vyřazení	vyřazení	k1gNnSc4
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
Rachůnek	Rachůnek	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
15	#num#	k4
<g/>
,	,	kIx,
Vašíček	Vašíček	k1gMnSc1
63	#num#	k4
<g/>
)	)	kIx)
zemřelých	zemřelý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
z	z	k7c2
reprezentace	reprezentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
celou	celý	k2eAgFnSc4d1
hokejovou	hokejový	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
bude	být	k5eAaImBp3nS
reprezentační	reprezentační	k2eAgInSc1d1
tým	tým	k1gInSc1
nosit	nosit	k5eAaImF
na	na	k7c6
dresech	dres	k1gInPc6
nášivky	nášivka	k1gFnSc2
s	s	k7c7
jejich	jejich	k3xOp3gNnPc7
čísly	číslo	k1gNnPc7
dresů	dres	k1gInPc2
a	a	k8xC
motivem	motiv	k1gInSc7
srdce	srdce	k1gNnSc2
a	a	k8xC
hokejky	hokejka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
přišlo	přijít	k5eAaPmAgNnS
v	v	k7c6
osobě	osoba	k1gFnSc6
Pavola	Pavola	k1gFnSc1
Demitry	Demitr	k1gInPc4
o	o	k7c4
svého	svůj	k3xOyFgMnSc4
kapitána	kapitán	k1gMnSc4
<g/>
,	,	kIx,
bronzového	bronzový	k2eAgMnSc4d1
medailistu	medailista	k1gMnSc4
z	z	k7c2
MS	MS	kA
2003	#num#	k4
a	a	k8xC
komplexního	komplexní	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
MS	MS	kA
2011	#num#	k4
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
se	se	k3xPyFc4
rozloučil	rozloučit	k5eAaPmAgMnS
s	s	k7c7
reprezentačním	reprezentační	k2eAgInSc7d1
dresem	dres	k1gInSc7
<g/>
;	;	kIx,
sezónu	sezóna	k1gFnSc4
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
chtěl	chtít	k5eAaImAgMnS
odehrát	odehrát	k5eAaPmF
v	v	k7c6
KHL	KHL	kA
a	a	k8xC
od	od	k7c2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
pouze	pouze	k6eAd1
věnovat	věnovat	k5eAaImF,k5eAaPmF
mládežnickému	mládežnický	k2eAgInSc3d1
hokeji	hokej	k1gInSc3
v	v	k7c6
Dukle	Dukla	k1gFnSc6
Trenčín	Trenčín	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
jeho	jeho	k3xOp3gNnSc4
číslo	číslo	k1gNnSc4
38	#num#	k4
bylo	být	k5eAaImAgNnS
vyřazeno	vyřadit	k5eAaPmNgNnS
ze	z	k7c2
slovenské	slovenský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
palubě	paluba	k1gFnSc6
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
další	další	k2eAgMnSc1d1
držitel	držitel	k1gMnSc1
titulu	titul	k1gInSc2
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
švédský	švédský	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
Stefan	Stefan	k1gMnSc1
Liv	Liv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
olympijským	olympijský	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
havárie	havárie	k1gFnSc2
probíhal	probíhat	k5eAaImAgInS
zápas	zápas	k1gInSc1
mezi	mezi	k7c7
týmy	tým	k1gInPc7
Salavat	Salavat	k1gMnSc2
Julajev	Julajev	k1gMnSc2
Ufa	Ufa	k1gMnSc2
a	a	k8xC
Atlant	atlant	k1gInSc1
Mytišči	Mytišč	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
přerušen	přerušit	k5eAaPmNgInS
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
první	první	k4xOgFnSc2
třetiny	třetina	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
nehodě	nehoda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
se	se	k3xPyFc4
poté	poté	k6eAd1
rozhodli	rozhodnout	k5eAaPmAgMnP
v	v	k7c6
zápase	zápas	k1gInSc6
nepokračovat	pokračovat	k5eNaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Program	program	k1gInSc1
KHL	KHL	kA
byl	být	k5eAaImAgInS
přerušen	přerušit	k5eAaPmNgInS
a	a	k8xC
začátek	začátek	k1gInSc1
soutěže	soutěž	k1gFnSc2
byl	být	k5eAaImAgInS
posunut	posunout	k5eAaPmNgInS
na	na	k7c6
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátek	začátek	k1gInSc1
české	český	k2eAgFnSc2d1
Extraligy	extraliga	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
důsledku	důsledek	k1gInSc6
pohřbu	pohřeb	k1gInSc2
Jana	Jan	k1gMnSc2
Marka	Marek	k1gMnSc2
posunut	posunout	k5eAaPmNgInS
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tryzny	tryzna	k1gFnPc1
a	a	k8xC
pohřby	pohřeb	k1gInPc1
obětí	oběť	k1gFnPc2
</s>
<s>
Lidé	člověk	k1gMnPc1
zapalují	zapalovat	k5eAaImIp3nP
svíčky	svíčka	k1gFnPc4
u	u	k7c2
Jaroslavlské	Jaroslavlský	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
arény	aréna	k1gFnSc2
k	k	k7c3
uctění	uctění	k1gNnSc3
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
Zápas	zápas	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
v	v	k7c6
Minsku	Minsk	k1gInSc6
odehrát	odehrát	k5eAaPmF
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
po	po	k7c6
neštěstí	neštěstí	k1gNnSc6
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgInS
symbolickým	symbolický	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
Jaroslavle	Jaroslavle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hale	hala	k1gFnSc6
byla	být	k5eAaImAgFnS
při	při	k7c6
smuteční	smuteční	k2eAgFnSc6d1
ceremonii	ceremonie	k1gFnSc6
hráči	hráč	k1gMnPc1
Dynama	dynamo	k1gNnSc2
uctěna	uctěn	k2eAgFnSc1d1
památka	památka	k1gFnSc1
všech	všecek	k3xTgMnPc2
zesnulých	zesnulý	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
portréty	portrét	k1gInPc1
byly	být	k5eAaImAgInP
umístěny	umístit	k5eAaPmNgInP
na	na	k7c6
ledě	led	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k1gFnPc4
si	se	k3xPyFc3
vstřelili	vstřelit	k5eAaPmAgMnP
do	do	k7c2
své	svůj	k3xOyFgFnSc2
branky	branka	k1gFnSc2
za	za	k7c2
každého	každý	k3xTgMnSc2
zesnulého	zesnulý	k2eAgMnSc2d1
jeden	jeden	k4xCgInSc4
gól	gól	k1gInSc4
a	a	k8xC
květiny	květina	k1gFnPc4
k	k	k7c3
brance	branka	k1gFnSc3
soupeře	soupeř	k1gMnSc2
přivezl	přivézt	k5eAaPmAgInS
i	i	k9
běloruský	běloruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Lukašenko	Lukašenka	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tryzna	tryzna	k1gFnSc1
za	za	k7c4
hokejisty	hokejista	k1gMnPc4
na	na	k7c6
Staroměstském	staroměstský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
středu	střed	k1gInSc6
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
V	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
proběhl	proběhnout	k5eAaPmAgInS
hlavní	hlavní	k2eAgInSc1d1
smuteční	smuteční	k2eAgInSc1d1
obřad	obřad	k1gInSc1
v	v	k7c4
sobotu	sobota	k1gFnSc4
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
dopoledne	dopoledne	k6eAd1
v	v	k7c6
hokejové	hokejový	k2eAgFnSc6d1
hale	hala	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
Minsku	Minsk	k1gInSc6
byly	být	k5eAaImAgInP
na	na	k7c6
ledě	led	k1gInSc6
vystaveny	vystaven	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
a	a	k8xC
u	u	k7c2
sedmi	sedm	k4xCc2
hráčů	hráč	k1gMnPc2
a	a	k8xC
sedmi	sedm	k4xCc2
členů	člen	k1gInPc2
realizačního	realizační	k2eAgInSc2d1
týmu	tým	k1gInSc2
pochovaných	pochovaný	k2eAgFnPc2d1
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
byly	být	k5eAaImAgFnP
před	před	k7c7
pohřbem	pohřeb	k1gInSc7
vystaveny	vystavit	k5eAaPmNgFnP
také	také	k9
jejich	jejich	k3xOp3gFnPc1
rakve	rakev	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obřadu	obřad	k1gInSc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
na	na	k7c4
100	#num#	k4
tis	tis	k1gInSc4
<g/>
.	.	kIx.
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
také	také	k9
ruský	ruský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
René	René	k1gMnSc1
Fasel	Fasel	k1gMnSc1
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
Ruské	ruský	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
Vladislav	Vladislav	k1gMnSc1
Treťjak	Treťjak	k1gMnSc1
a	a	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
významných	významný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pietní	pietní	k2eAgNnSc1d1
místo	místo	k1gNnSc1
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
</s>
<s>
Těla	tělo	k1gNnPc4
tří	tři	k4xCgMnPc2
českých	český	k2eAgMnPc2d1
reprezentantů	reprezentant	k1gMnPc2
byla	být	k5eAaImAgFnS
přepravena	přepravit	k5eAaPmNgFnS
v	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c4
sobotu	sobota	k1gFnSc4
leteckým	letecký	k2eAgInSc7d1
speciálem	speciál	k1gInSc7
české	český	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
naplánoval	naplánovat	k5eAaBmAgMnS
pietní	pietní	k2eAgNnSc4d1
shromáždění	shromáždění	k1gNnSc4
na	na	k7c4
pražské	pražský	k2eAgNnSc4d1
Staroměstské	staroměstský	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
již	již	k6eAd1
po	po	k7c4
oznámení	oznámení	k1gNnSc4
nehody	nehoda	k1gFnSc2
spontánně	spontánně	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
pietní	pietní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c4
neděli	neděle	k1gFnSc4
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
v	v	k7c4
15	#num#	k4
hodin	hodina	k1gFnPc2
odpoledne	odpoledne	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
budou	být	k5eAaImBp3nP
vystaveny	vystavit	k5eAaPmNgFnP
i	i	k9
rakve	rakev	k1gFnPc1
s	s	k7c7
ostatky	ostatek	k1gInPc7
hokejistů	hokejista	k1gMnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
však	však	k9
veřejnost	veřejnost	k1gFnSc1
nepovažovala	považovat	k5eNaImAgFnS
za	za	k7c4
vhodné	vhodný	k2eAgNnSc4d1
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
tohoto	tento	k3xDgInSc2
záměru	záměr	k1gInSc2
bylo	být	k5eAaImAgNnS
upuštěno	upustit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderátorem	moderátor	k1gInSc7
akce	akce	k1gFnSc2
na	na	k7c6
Staroměstském	staroměstský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
byl	být	k5eAaImAgMnS
Robert	Robert	k1gMnSc1
Záruba	Záruba	k1gMnSc1
<g/>
,	,	kIx,
k	k	k7c3
uctění	uctění	k1gNnSc3
památky	památka	k1gFnSc2
byla	být	k5eAaImAgFnS
držena	držen	k2eAgFnSc1d1
minuta	minuta	k1gFnSc1
ticha	ticho	k1gNnSc2
a	a	k8xC
promítány	promítán	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
památné	památný	k2eAgInPc4d1
momenty	moment	k1gInPc4
z	z	k7c2
kariéry	kariéra	k1gFnSc2
těchto	tento	k3xDgMnPc2
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátký	krátký	k2eAgInSc1d1
proslov	proslov	k1gInSc1
pronesl	pronést	k5eAaPmAgMnS
předseda	předseda	k1gMnSc1
ČSLH	ČSLH	kA
Tomáš	Tomáš	k1gMnSc1
Král	Král	k1gMnSc1
a	a	k8xC
na	na	k7c4
závěr	závěr	k1gInSc4
promluvil	promluvit	k5eAaPmAgMnS
pražský	pražský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
Dominik	Dominik	k1gMnSc1
Duka	Duka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akce	akce	k1gFnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
několik	několik	k4yIc1
tisíc	tisíc	k4xCgInPc2
hokejových	hokejový	k2eAgMnPc2d1
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
také	také	k9
řada	řada	k1gFnSc1
hokejistů	hokejista	k1gMnPc2
(	(	kIx(
<g/>
např.	např.	kA
Jaromír	Jaromír	k1gMnSc1
Jágr	Jágr	k1gMnSc1
<g/>
,	,	kIx,
Patrik	Patrik	k1gMnSc1
Eliáš	Eliáš	k1gMnSc1
<g/>
,	,	kIx,
Dominik	Dominik	k1gMnSc1
Hašek	Hašek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pietní	pietní	k2eAgFnSc6d1
akci	akce	k1gFnSc6
navazovala	navazovat	k5eAaImAgFnS
zádušní	zádušní	k2eAgFnSc1d1
mše	mše	k1gFnSc1
v	v	k7c6
Týnském	týnský	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
sloužil	sloužit	k5eAaImAgMnS
Tomáš	Tomáš	k1gMnSc1
Holub	Holub	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
ČSLH	ČSLH	kA
navždy	navždy	k6eAd1
vyřadil	vyřadit	k5eAaPmAgInS
z	z	k7c2
reprezentace	reprezentace	k1gFnSc2
čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
hráči	hráč	k1gMnPc1
oblékali	oblékat	k5eAaImAgMnP
(	(	kIx(
<g/>
č.	č.	k?
4	#num#	k4
Karla	Karel	k1gMnSc2
Rachůnka	Rachůnek	k1gMnSc2
<g/>
,	,	kIx,
č.	č.	k?
15	#num#	k4
Jana	Jan	k1gMnSc2
Marka	Marek	k1gMnSc2
a	a	k8xC
č.	č.	k?
63	#num#	k4
Josefa	Josef	k1gMnSc2
Vašíčka	Vašíček	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
</s>
<s>
V	v	k7c6
historii	historie	k1gFnSc6
letectví	letectví	k1gNnSc2
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
první	první	k4xOgInSc4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
při	při	k7c6
nehodě	nehoda	k1gFnSc6
letadla	letadlo	k1gNnSc2
zahynul	zahynout	k5eAaPmAgInS
celý	celý	k2eAgInSc1d1
nebo	nebo	k8xC
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
sportovního	sportovní	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
nehoda	nehoda	k1gFnSc1
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
do	do	k7c2
počtu	počet	k1gInSc2
obětí	oběť	k1gFnPc2
z	z	k7c2
řad	řada	k1gFnPc2
sportovců	sportovec	k1gMnPc2
největší	veliký	k2eAgInSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
byly	být	k5eAaImAgFnP
podobně	podobně	k6eAd1
postiženy	postihnout	k5eAaPmNgFnP
československý	československý	k2eAgInSc1d1
hokejový	hokejový	k2eAgInSc1d1
tým	tým	k1gInSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
AC	AC	kA
Turín	Turín	k1gInSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krasobruslařská	krasobruslařský	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
USA	USA	kA
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
,	,	kIx,
72	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
sportovců	sportovec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uruguayský	uruguayský	k2eAgInSc1d1
ragbyový	ragbyový	k2eAgInSc1d1
tým	tým	k1gInSc1
Old	Olda	k1gFnPc2
Christians	Christiansa	k1gFnPc2
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
obětí	oběť	k1gFnPc2
při	při	k7c6
nehodě	nehoda	k1gFnSc6
<g/>
,	,	kIx,
dalších	další	k2eAgMnPc2d1
17	#num#	k4
nepřežilo	přežít	k5eNaPmAgNnS
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
v	v	k7c6
horách	hora	k1gFnPc6
na	na	k7c6
sněhu	sníh	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
Zambie	Zambie	k1gFnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
30	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
А	А	k?
п	п	k?
Я	Я	k?
7	#num#	k4
с	с	k?
2011	#num#	k4
г	г	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
2011	#num#	k4
Lokomotiv	lokomotiva	k1gFnPc2
Yaroslavl	Yaroslavl	k1gFnSc1
plane	planout	k5eAaImIp3nS
crash	crash	k1gInSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Probe	Prob	k1gMnSc5
Finds	Findsa	k1gFnPc2
Pilot	pilot	k1gMnSc1
Error	Error	k1gMnSc1
In	In	k1gMnSc1
Russian	Russian	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Team	team	k1gInSc1
Plane	planout	k5eAaImIp3nS
Crash	Crash	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
Free	Freus	k1gMnSc5
Europe	Europ	k1gMnSc5
<g/>
.	.	kIx.
2	#num#	k4
November	November	k1gInSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
November	November	k1gInSc1
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-SEP-	-SEP-	k?
<g/>
2011	#num#	k4
<g/>
Jakovlev	Jakovlev	k1gFnSc1
42	#num#	k4
<g/>
d.	d.	k?
aviation-safety	aviation-safeta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
К	К	k?
с	с	k?
Я	Я	k?
<g/>
42	#num#	k4
в	в	k?
Я	Я	k?
о	о	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
pro	pro	k7c4
mimořádné	mimořádný	k2eAgFnPc4d1
události	událost	k1gFnPc4
(	(	kIx(
<g/>
EMERCOM	EMERCOM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Full	Full	k1gInSc1
list	list	k1gInSc1
of	of	k?
people	people	k6eAd1
on	on	k3xPp3gMnSc1
board	board	k1gMnSc1
of	of	k?
crashed	crashed	k1gInSc1
Yak-	Yak-	k1gFnSc1
<g/>
42	#num#	k4
<g/>
,	,	kIx,
Russia	Russia	k1gFnSc1
Today	Todaa	k1gFnSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
↑	↑	k?
Rozlosování	rozlosování	k1gNnSc1
KHL	KHL	kA
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
hokej	hokej	k1gInSc4
<g/>
.	.	kIx.
<g/>
sport	sport	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Tragédie	tragédie	k1gFnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
havárii	havárie	k1gFnSc6
letadla	letadlo	k1gNnSc2
zahynuli	zahynout	k5eAaPmAgMnP
tři	tři	k4xCgMnPc1
čeští	český	k2eAgMnPc1d1
hokejoví	hokejový	k2eAgMnPc1d1
reprezentanti	reprezentant	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Yaroslavl	Yaroslavl	k1gFnSc1
plane	planout	k5eAaImIp3nS
tragedy	traged	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
Kontinentální	kontinentální	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
,	,	kIx,
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ruština	ruština	k1gFnSc1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
vyšetřovatele	vyšetřovatel	k1gMnSc2
<g/>
.	.	kIx.
www.mak.ru	www.mak.ra	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Havarovaný	havarovaný	k2eAgMnSc1d1
Jak-	Jak-	k1gMnSc1
<g/>
42	#num#	k4
<g/>
D	D	kA
nebyl	být	k5eNaImAgMnS
příliš	příliš	k6eAd1
starý	starý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
aerolinky	aerolinka	k1gFnPc1
měly	mít	k5eAaImAgFnP
problémy	problém	k1gInPc4
s	s	k7c7
bezpečností	bezpečnost	k1gFnSc7
<g/>
,	,	kIx,
technet	technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Top	topit	k5eAaImRp2nS
KHL	KHL	kA
squad	squad	k1gInSc1
killed	killed	k1gInSc1
in	in	k?
passenger	passenger	k1gInSc1
plane	planout	k5eAaImIp3nS
crash	crash	k1gInSc1
in	in	k?
Russia	Russia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rt	rt	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Н	Н	k?
п	п	k?
к	к	k?
Я	Я	k?
<g/>
42	#num#	k4
в	в	k?
Я	Я	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lenta	lenta	k1gFnSc1
<g/>
,	,	kIx,
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ruština	ruština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Jediný	jediný	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
přežil	přežít	k5eAaPmAgMnS
pád	pád	k1gInSc1
Jaku	jak	k1gMnSc3
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-09-11	2011-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Piloti	pilot	k1gMnPc1
nebyli	být	k5eNaImAgMnP
žádní	žádný	k3yNgMnPc1
zajíci	zajíc	k1gMnPc1
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
expert	expert	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
<g/>
↑	↑	k?
HRADECKY	hradecky	k6eAd1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crash	Crash	k1gMnSc1
<g/>
:	:	kIx,
Yak	Yak	k1gMnSc1
Service	Service	k1gFnSc2
YK42	YK42	k1gMnSc1
at	at	k?
Yaroslavl	Yaroslavl	k1gMnSc1
on	on	k3xPp3gMnSc1
Sep	Sep	k1gFnSc7
7	#num#	k4
<g/>
th	th	k?
2011	#num#	k4
<g/>
,	,	kIx,
failed	failed	k1gInSc1
to	ten	k3xDgNnSc1
climb	climb	k1gInSc4
on	on	k3xPp3gMnSc1
takeoff	takeoff	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnPc2
Aviation	Aviation	k1gInSc4
Herald	Heralda	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-09-08	2011-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Spadlo	spadnout	k5eAaPmAgNnS
letadlo	letadlo	k1gNnSc1
s	s	k7c7
hokejisty	hokejista	k1gMnPc7
Jaroslavle	Jaroslavle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahynuli	zahynout	k5eAaPmAgMnP
i	i	k9
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
Rachůnek	Rachůnek	k1gMnSc1
a	a	k8xC
Vašíček	Vašíček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
П	П	k?
<g/>
:	:	kIx,
р	р	k?
Я	Я	k?
<g/>
42	#num#	k4
б	б	k?
и	и	k?
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
All	All	k1gFnSc1
systems	systemsa	k1gFnPc2
of	of	k?
Yak-	Yak-	k1gFnPc2
<g/>
42	#num#	k4
plane	planout	k5eAaImIp3nS
work	work	k1gMnSc1
well	well	k1gMnSc1
before	befor	k1gInSc5
crash	crash	k1gInSc1
<g/>
:	:	kIx,
investigator	investigator	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
С	С	k?
и	и	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
п	п	k?
<g/>
,	,	kIx,
п	п	k?
р	р	k?
э	э	k?
Я	Я	k?
<g/>
421	#num#	k4
2	#num#	k4
3	#num#	k4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
www.mak.ru	www.mak.r	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Jediný	jediný	k2eAgMnSc1d1
svědek	svědek	k1gMnSc1
nehody	nehoda	k1gFnSc2
pohovořil	pohovořit	k5eAaPmAgMnS
o	o	k7c6
posledních	poslední	k2eAgFnPc6d1
minutách	minuta	k1gFnPc6
před	před	k7c7
katastrofou	katastrofa	k1gFnSc7
<g/>
↑	↑	k?
KHL	KHL	kA
odložila	odložit	k5eAaPmAgFnS
start	start	k1gInSc4
na	na	k7c4
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslavl	Jaroslavl	k1gFnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
lize	liga	k1gFnSc6
pokračovat	pokračovat	k5eAaImF
<g/>
,	,	kIx,
jiné	jiný	k2eAgInPc1d1
kluby	klub	k1gInPc1
jí	on	k3xPp3gFnSc2
pomohou	pomoct	k5eAaPmIp3nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hokej	hokej	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-09-08	2011-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hokejisté	hokejista	k1gMnPc1
Jaroslavle	Jaroslavle	k1gNnSc2
nebudou	být	k5eNaImBp3nP
hrát	hrát	k5eAaImF
letošní	letošní	k2eAgInSc4d1
ročník	ročník	k1gInSc4
KHL	KHL	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-9-10	2011-9-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TOMAS	TOMAS	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odešli	odejít	k5eAaPmAgMnP
mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
opory	opora	k1gFnSc2
reprezentace	reprezentace	k1gFnSc2
a	a	k8xC
střelci	střelec	k1gMnPc7
důležitých	důležitý	k2eAgInPc2d1
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Reprezentace	reprezentace	k1gFnSc1
nezapomíná	zapomínat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srdce	srdce	k1gNnPc1
a	a	k8xC
čísla	číslo	k1gNnSc2
4	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
a	a	k8xC
63	#num#	k4
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
hráči	hráč	k1gMnPc1
vyšitá	vyšitý	k2eAgFnSc1d1
na	na	k7c6
dresech	dres	k1gInPc6
<g/>
.	.	kIx.
hokej	hokej	k1gInSc1
<g/>
.	.	kIx.
<g/>
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-03	2011-11-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovenský	slovenský	k2eAgInSc1d1
tisk	tisk	k1gInSc1
o	o	k7c6
tragédii	tragédie	k1gFnSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
:	:	kIx,
Zemřel	zemřít	k5eAaPmAgMnS
hokejový	hokejový	k2eAgMnSc1d1
bůh	bůh	k1gMnSc1
Demitra	Demitrum	k1gNnSc2
<g/>
.	.	kIx.
sportovninoviny	sportovninovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-09-08	2011-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
LANGR	Langr	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Start	start	k1gInSc1
KHL	KHL	kA
se	se	k3xPyFc4
posouvá	posouvat	k5eAaImIp3nS
na	na	k7c4
pondělí	pondělí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kluby	klub	k1gInPc7
chtějí	chtít	k5eAaImIp3nP
Jaroslavli	Jaroslavli	k1gFnSc3
půjčit	půjčit	k5eAaPmF
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-08	2011-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JÁCHIM	JÁCHIM	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
!	!	kIx.
</s>
<s desamb="1">
Tipsport	Tipsport	k1gInSc1
extraliga	extraliga	k1gFnSc1
odstartuje	odstartovat	k5eAaPmIp3nS
až	až	k9
v	v	k7c6
neděli	neděle	k1gFnSc6
<g/>
,	,	kIx,
páteční	páteční	k2eAgNnSc4d1
první	první	k4xOgNnSc4
kolo	kolo	k1gNnSc4
bylo	být	k5eAaImAgNnS
odloženo	odložit	k5eAaPmNgNnS
<g/>
.	.	kIx.
hokej	hokej	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-09-12	2011-09-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Symbolické	symbolický	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Jaroslavle	Jaroslavle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-08	2011-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TOMAS	TOMAS	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Při	při	k7c6
tryzně	tryzna	k1gFnSc6
v	v	k7c6
Minsku	Minsk	k1gInSc6
si	se	k3xPyFc3
místní	místní	k2eAgMnPc1d1
hokejisté	hokejista	k1gMnPc1
dávali	dávat	k5eAaImAgMnP
vlastní	vlastní	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-09	2011-09-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ledový	ledový	k2eAgInSc4d1
palác	palác	k1gInSc4
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
zahalil	zahalit	k5eAaPmAgMnS
smutek	smutek	k1gInSc4
<g/>
,	,	kIx,
loučil	loučit	k5eAaImAgMnS
se	se	k3xPyFc4
s	s	k7c7
tragicky	tragicky	k6eAd1
zemřelými	zemřelý	k2eAgMnPc7d1
hokejisty	hokejista	k1gMnPc7
<g/>
.	.	kIx.
hokej	hokej	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-10	2011-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BEREŇ	BEREŇ	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hokejový	hokejový	k2eAgInSc1d1
svět	svět	k1gInSc1
se	se	k3xPyFc4
loučil	loučit	k5eAaImAgInS
s	s	k7c7
týmem	tým	k1gInSc7
Jaroslavle	Jaroslavle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tryzna	tryzna	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
na	na	k7c6
ledě	led	k1gInSc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-10	2011-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-10	2011-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MIB	MIB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tragicky	tragicky	k6eAd1
zesnulí	zesnulý	k2eAgMnPc1d1
hokejisté	hokejista	k1gMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
přivezl	přivézt	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
armádní	armádní	k2eAgInSc4d1
speciál	speciál	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-10	2011-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ROJ	roj	k1gInSc1
<g/>
;	;	kIx,
LIK	LIK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národ	národ	k1gInSc1
se	se	k3xPyFc4
loučil	loučit	k5eAaImAgInS
se	s	k7c7
zesnulými	zesnulý	k2eAgMnPc7d1
hokejisty	hokejista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaplněný	zaplněný	k2eAgInSc1d1
Staromák	Staromák	k1gInSc1
vzpomínal	vzpomínat	k5eAaImAgInS
na	na	k7c4
zlaté	zlatý	k2eAgMnPc4d1
hrdiny	hrdina	k1gMnPc4
<g/>
.	.	kIx.
hokej	hokej	k1gInSc1
<g/>
.	.	kIx.
<g/>
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-11	2011-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tisíce	tisíc	k4xCgInPc1
lidí	člověk	k1gMnPc2
si	se	k3xPyFc3
na	na	k7c6
Staroměstském	staroměstský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
připomněly	připomnět	k5eAaPmAgInP
památku	památka	k1gFnSc4
tří	tři	k4xCgMnPc2
hokejistů	hokejista	k1gMnPc2
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-11	2011-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LANGR	Langr	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
;	;	kIx,
BEREŇ	BEREŇ	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíce	tisíc	k4xCgInPc1
fanoušků	fanoušek	k1gMnPc2
uctily	uctít	k5eAaPmAgInP
památku	památka	k1gFnSc4
hokejistů	hokejista	k1gMnPc2
<g/>
,	,	kIx,
dorazili	dorazit	k5eAaPmAgMnP
i	i	k9
Jágr	Jágr	k1gMnSc1
či	či	k8xC
Růžička	Růžička	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-11	2011-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MIKULKA	Mikulka	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnPc1d3
letecké	letecký	k2eAgFnPc1d1
tragédie	tragédie	k1gFnPc1
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
umírali	umírat	k5eAaImAgMnP
sportovci	sportovec	k1gMnPc1
<g/>
.	.	kIx.
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-07	2011-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
-	-	kIx~
7693	#num#	k4
1213	#num#	k4
-	-	kIx~
7693	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Let	let	k1gInSc1
Spanair	Spanair	k1gInSc1
5022	#num#	k4
-	-	kIx~
Letecká	letecký	k2eAgFnSc1d1
katastrofa	katastrofa	k1gFnSc1
s	s	k7c7
velmi	velmi	k6eAd1
podobným	podobný	k2eAgInSc7d1
průběhem	průběh	k1gInSc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Letecká	letecký	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Zpráva	zpráva	k1gFnSc1
U	u	k7c2
Jaroslavle	Jaroslavle	k1gNnSc2
se	se	k3xPyFc4
zřítilo	zřítit	k5eAaPmAgNnS
letadlo	letadlo	k1gNnSc1
s	s	k7c7
hokejisty	hokejista	k1gMnPc7
<g/>
,	,	kIx,
zahynuli	zahynout	k5eAaPmAgMnP
i	i	k9
tři	tři	k4xCgMnPc1
Češi	Čech	k1gMnPc1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
V	v	k7c6
Rusku	Rusko	k1gNnSc6
spadlo	spadnout	k5eAaPmAgNnS
letadlo	letadlo	k1gNnSc1
s	s	k7c7
hokejisty	hokejista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahynuli	zahynout	k5eAaPmAgMnP
i	i	k9
Rachůnek	Rachůnek	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
a	a	k8xC
Vašíček	Vašíček	k1gMnSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
