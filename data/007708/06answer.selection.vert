<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Pravidel	pravidlo	k1gNnPc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
psáno	psát	k5eAaImNgNnS	psát
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
s	s	k7c7	s
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Česká	český	k2eAgFnSc1d1	Česká
astronomická	astronomický	k2eAgFnSc1d1	astronomická
společnost	společnost	k1gFnSc1	společnost
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
psaní	psaní	k1gNnSc4	psaní
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
S	s	k7c7	s
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
planetární	planetární	k2eAgInSc1d1	planetární
systém	systém	k1gInSc1	systém
hvězdy	hvězda	k1gFnSc2	hvězda
známé	známý	k2eAgFnPc4d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
