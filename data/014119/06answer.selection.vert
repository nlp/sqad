<s desamb="1">
Zimy	zima	k1gFnPc1
jsou	být	k5eAaImIp3nP
mírné	mírný	k2eAgFnPc1d1
a	a	k8xC
teplota	teplota	k1gFnSc1
málokdy	málokdy	k6eAd1
spadne	spadnout	k5eAaPmIp3nS
pod	pod	k7c4
0	#num#	k4
°	°	kA
<g/>
C.	C.	kA
Léta	léto	k1gNnPc1
jsou	být	k5eAaImIp3nP
horká	horký	k2eAgFnSc1d1
a	a	k8xC
teplota	teplota	k1gFnSc1
příležitostně	příležitostně	k6eAd1
překračuje	překračovat	k5eAaImIp3nS
40	#num#	k4
°	°	kA
<g/>
C.	C.	kA
</s>