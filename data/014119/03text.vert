<s>
Mérida	Mérida	k1gFnSc1
(	(	kIx(
<g/>
Španělsko	Španělsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Mérida	Mérida	k1gFnSc1
Mérida	Mérida	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
38	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
57	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
217	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Autonomního	autonomní	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
</s>
<s>
Extremadura	Extremadura	k1gFnSc1
Provincie	provincie	k1gFnSc2
</s>
<s>
Badajoz	Badajoz	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Tierra	Tierra	k1gFnSc1
de	de	k?
Mérida	Mérida	k1gFnSc1
-	-	kIx~
Vegas	Vegas	k1gMnSc1
Bajas	Bajas	k1gMnSc1
Soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Mérida	Mérida	k1gFnSc1
</s>
<s>
Mérida	Mérida	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
865,6	865,6	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
59	#num#	k4
548	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
68,8	68,8	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Antonio	Antonio	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
Osuna	Osuna	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
2015	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.merida.es	www.merida.es	k1gMnSc1
PSČ	PSČ	kA
</s>
<s>
06800	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
EX	ex	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mérida	Mérida	k1gFnSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
25	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pozdějším	pozdní	k2eAgMnSc7d2
římským	římský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Augustem	August	k1gMnSc7
jako	jako	k8xS,k8xC
Emerita	Emerita	k1gFnSc1
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
španělského	španělský	k2eAgNnSc2d1
autonomního	autonomní	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
Extremadura	Extremadura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
provincii	provincie	k1gFnSc6
Badajoz	Badajoza	k1gFnPc2
a	a	k8xC
podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
57	#num#	k4
127	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1
archeologická	archeologický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
zařazena	zařazen	k2eAgFnSc1d1
ke	k	k7c3
světovému	světový	k2eAgNnSc3d1
dědictví	dědictví	k1gNnSc3
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
zdrojů	zdroj	k1gInPc2
(	(	kIx(
<g/>
Oxfordský	oxfordský	k2eAgInSc1d1
slovník	slovník	k1gInSc1
svatých	svatá	k1gFnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
ostatky	ostatek	k1gInPc1
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
Ježíšových	Ježíšův	k2eAgMnPc2d1
učedníků	učedník	k1gMnPc2
–	–	k?
Jakuba	Jakub	k1gMnSc4
Staršího	starší	k1gMnSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I	i	k9
když	když	k8xS
některými	některý	k3yIgInPc7
prameny	pramen	k1gInPc7
je	být	k5eAaImIp3nS
za	za	k7c4
toto	tento	k3xDgNnSc4
místo	místo	k6eAd1
považována	považován	k2eAgFnSc1d1
mnohem	mnohem	k6eAd1
slavnější	slavný	k2eAgFnSc1d2
lokalita	lokalita	k1gFnSc1
–	–	k?
poutní	poutní	k2eAgNnSc4d1
město	město	k1gNnSc4
Santiago	Santiago	k1gNnSc1
de	de	k?
Compostela	Compostel	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Mérida	Mérida	k1gFnSc1
má	mít	k5eAaImIp3nS
středozemní	středozemní	k2eAgNnSc1d1
kontinentální	kontinentální	k2eAgNnSc1d1
klima	klima	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
ovlivňováno	ovlivňovat	k5eAaImNgNnS
atlantským	atlantský	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
omývajícím	omývající	k2eAgInSc7d1
portugalské	portugalský	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimy	zima	k1gFnPc1
jsou	být	k5eAaImIp3nP
mírné	mírný	k2eAgFnPc1d1
a	a	k8xC
teplota	teplota	k1gFnSc1
málokdy	málokdy	k6eAd1
spadne	spadnout	k5eAaPmIp3nS
pod	pod	k7c4
0	#num#	k4
°	°	kA
<g/>
C.	C.	kA
Léta	léto	k1gNnPc1
jsou	být	k5eAaImIp3nP
horká	horký	k2eAgFnSc1d1
a	a	k8xC
teplota	teplota	k1gFnSc1
příležitostně	příležitostně	k6eAd1
překračuje	překračovat	k5eAaImIp3nS
40	#num#	k4
°	°	kA
<g/>
C.	C.	kA
</s>
<s>
Roční	roční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
okolo	okolo	k7c2
450	#num#	k4
až	až	k9
500	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
počet	počet	k1gInSc1
srážek	srážka	k1gFnPc2
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
listopad	listopad	k1gInSc4
a	a	k8xC
prosinec	prosinec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léta	léto	k1gNnPc1
jsou	být	k5eAaImIp3nP
suchá	suchý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
sucha	sucho	k1gNnSc2
se	se	k3xPyFc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
celém	celý	k2eAgNnSc6d1
jižním	jižní	k2eAgNnSc6d1
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
vrací	vracet	k5eAaImIp3nS
v	v	k7c6
cyklech	cyklus	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
trvají	trvat	k5eAaImIp3nP
od	od	k7c2
2	#num#	k4
do	do	k7c2
5	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
je	být	k5eAaImIp3nS
počasí	počasí	k1gNnSc1
velmi	velmi	k6eAd1
nestabilní	stabilní	k2eNgMnSc1d1
<g/>
,	,	kIx,
neustále	neustále	k6eAd1
se	se	k3xPyFc4
střídají	střídat	k5eAaImIp3nP
období	období	k1gNnSc4
sucha	sucho	k1gNnSc2
se	s	k7c7
silnými	silný	k2eAgFnPc7d1
bouřkami	bouřka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Populace	populace	k1gFnSc1
Méridy	Mérida	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
1842	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1842	#num#	k4
</s>
<s>
1857	#num#	k4
</s>
<s>
1860	#num#	k4
</s>
<s>
1877	#num#	k4
</s>
<s>
1887	#num#	k4
</s>
<s>
1897	#num#	k4
</s>
<s>
1900	#num#	k4
</s>
<s>
1910	#num#	k4
</s>
<s>
1920	#num#	k4
</s>
<s>
3	#num#	k4
780	#num#	k4
</s>
<s>
5	#num#	k4
505	#num#	k4
</s>
<s>
5	#num#	k4
975	#num#	k4
</s>
<s>
7	#num#	k4
390	#num#	k4
</s>
<s>
10	#num#	k4
063	#num#	k4
</s>
<s>
10	#num#	k4
886	#num#	k4
</s>
<s>
11	#num#	k4
168	#num#	k4
</s>
<s>
14	#num#	k4
633	#num#	k4
</s>
<s>
15	#num#	k4
502	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1940	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
1981	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
19	#num#	k4
354	#num#	k4
</s>
<s>
25	#num#	k4
501	#num#	k4
</s>
<s>
23	#num#	k4
835	#num#	k4
</s>
<s>
34	#num#	k4
297	#num#	k4
</s>
<s>
40	#num#	k4
059	#num#	k4
</s>
<s>
41	#num#	k4
783	#num#	k4
</s>
<s>
51	#num#	k4
135	#num#	k4
</s>
<s>
50	#num#	k4
271	#num#	k4
</s>
<s>
57	#num#	k4
127	#num#	k4
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Mérida	Mérida	k1gFnSc1
<g/>
,	,	kIx,
Filipíny	Filipíny	k1gFnPc1
</s>
<s>
Mérida	Mérida	k1gFnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Mérida	Mérida	k1gFnSc1
<g/>
,	,	kIx,
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Panoramatický	panoramatický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Plaza	plaz	k1gMnSc4
de	de	k?
Españ	Españ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Národní	národní	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
:	:	kIx,
Padrón	Padrón	k1gInSc1
municipal	municipat	k5eAaPmAgInS,k5eAaImAgInS
de	de	k?
Españ	Españ	k1gInSc2
de	de	k?
2020	#num#	k4
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mérida	Mérid	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Španělské	španělský	k2eAgFnPc1d1
památky	památka	k1gFnPc1
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Alcalá	Alcalý	k2eAgFnSc1d1
de	de	k?
Henares	Henares	k1gInSc1
(	(	kIx(
<g/>
univerzita	univerzita	k1gFnSc1
a	a	k8xC
historická	historický	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Almadén	Almadén	k1gInSc1
(	(	kIx(
<g/>
hornické	hornický	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Kulturní	kulturní	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
Aranjuez	Aranjuez	k1gInSc1
•	•	k?
jeskyně	jeskyně	k1gFnSc2
Altamira	Altamiro	k1gNnSc2
•	•	k?
Dolmeny	dolmen	k1gInPc1
v	v	k7c6
Antequeře	Antequera	k1gFnSc6
•	•	k?
Atapuerca	Atapuerc	k2eAgFnSc1d1
•	•	k?
Ávila	Ávila	k1gFnSc1
•	•	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
Palau	Palaus	k1gInSc2
de	de	k?
la	la	k1gNnSc2
Música	Músicus	k1gMnSc2
Catalana	Catalan	k1gMnSc2
a	a	k8xC
nemocnice	nemocnice	k1gFnSc2
Sant	Sant	k1gMnSc1
Pau	Pau	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Biskajský	biskajský	k2eAgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
most	most	k1gInSc1
•	•	k?
Burgos	Burgos	k1gInSc1
(	(	kIx(
<g/>
katedrála	katedrála	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Cáceres	Cáceres	k1gInSc1
•	•	k?
Córdoba	Córdoba	k1gFnSc1
•	•	k?
Cuenca	Cuenc	k2eAgFnSc1d1
•	•	k?
Doñ	Doñ	k1gFnSc1
(	(	kIx(
<g/>
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
El	Ela	k1gFnPc2
Escorial	Escorial	k1gMnSc1
•	•	k?
Gaudího	Gaudí	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
•	•	k?
Garajonay	Garajonaa	k1gMnSc2
(	(	kIx(
<g/>
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Granada	Granada	k1gFnSc1
(	(	kIx(
<g/>
Alhambra	Alhambra	k1gFnSc1
<g/>
,	,	kIx,
Generalife	Generalif	k1gInSc5
a	a	k8xC
Albaicín	Albaicína	k1gFnPc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Herkulova	Herkulův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
•	•	k?
Ibiza	Ibiza	k1gFnSc1
(	(	kIx(
<g/>
biodiverzita	biodiverzita	k1gFnSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
San	San	k1gMnSc1
Millán	Millán	k2eAgMnSc1d1
de	de	k?
Yuso	Yuso	k1gMnSc1
a	a	k8xC
Suso	Suso	k1gMnSc1
(	(	kIx(
<g/>
kláštery	klášter	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Médulas	Médulas	k1gInSc1
•	•	k?
Llotja	Llotja	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Seda	Seda	k1gMnSc1
•	•	k?
Lugo	Lugo	k1gMnSc1
(	(	kIx(
<g/>
římské	římský	k2eAgFnSc2d1
hradby	hradba	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Mérida	Mérida	k1gFnSc1
(	(	kIx(
<g/>
archeologická	archeologický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Monte	Mont	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
Perdido	Perdida	k1gFnSc5
•	•	k?
Mudéjarská	Mudéjarský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Aragonii	Aragonie	k1gFnSc6
•	•	k?
Salamanca	Salamanc	k1gInSc2
•	•	k?
Palmový	palmový	k2eAgInSc1d1
háj	háj	k1gInSc1
v	v	k7c6
Elche	Elche	k1gNnSc6
•	•	k?
Santa	Santa	k1gMnSc1
María	María	k1gMnSc1
de	de	k?
Guadalupe	Guadalup	k1gInSc5
(	(	kIx(
<g/>
klášter	klášter	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
památky	památka	k1gFnPc1
Ovieda	Ovied	k1gMnSc2
a	a	k8xC
Asturského	Asturský	k2eAgNnSc2d1
království	království	k1gNnSc2
•	•	k?
Poblet	Poblet	k1gInSc1
(	(	kIx(
<g/>
klášter	klášter	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
San	San	k1gFnSc1
Cristóbal	Cristóbal	k1gInSc1
de	de	k?
La	la	k1gNnSc2
Laguna	laguna	k1gFnSc1
•	•	k?
Santiago	Santiago	k1gNnSc1
de	de	k?
Compostela	Compostel	k1gMnSc2
•	•	k?
Segovia	Segovius	k1gMnSc2
a	a	k8xC
její	její	k3xOp3gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
akvadukt	akvadukt	k1gInSc1
•	•	k?
kulturní	kulturní	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
Serra	Serra	k1gFnSc1
de	de	k?
Tramuntana	Tramuntana	k1gFnSc1
•	•	k?
Sevilla	Sevilla	k1gFnSc1
(	(	kIx(
<g/>
katedrála	katedrála	k1gFnSc1
<g/>
,	,	kIx,
alcázar	alcázar	k1gInSc1
a	a	k8xC
indický	indický	k2eAgInSc1d1
archiv	archiv	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Skalní	skalní	k2eAgNnSc1d1
umění	umění	k1gNnSc1
ve	v	k7c6
Středomořské	středomořský	k2eAgFnSc6d1
pánvi	pánev	k1gFnSc6
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
•	•	k?
Svatojakubská	svatojakubský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
•	•	k?
Tarragona	Tarragona	k1gFnSc1
(	(	kIx(
<g/>
archeologické	archeologický	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Teide	Teid	k1gInSc5
(	(	kIx(
<g/>
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Toledo	Toledo	k1gNnSc4
•	•	k?
Údolí	údolí	k1gNnSc2
Côa	Côa	k1gMnSc2
a	a	k8xC
Siega	Sieg	k1gMnSc2
Verde	Verd	k1gInSc5
•	•	k?
Úbeda	Úbed	k1gMnSc2
a	a	k8xC
Baeza	Baez	k1gMnSc2
(	(	kIx(
<g/>
renesanční	renesanční	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Vall	Vall	k1gInSc1
de	de	k?
Boí	boa	k1gFnPc2
(	(	kIx(
<g/>
románské	románský	k2eAgInPc1d1
kostely	kostel	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Původní	původní	k2eAgInPc4d1
bukové	bukový	k2eAgInPc4d1
lesy	les	k1gInPc4
Karpat	Karpaty	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Evropy	Evropa	k1gFnSc2
•	•	k?
Medina	Medina	k1gFnSc1
Azahara	Azahara	k1gFnSc1
•	•	k?
Risco	Risco	k6eAd1
Caído	Caído	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
–	–	k?
Españ	Españ	k1gInSc2
–	–	k?
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgMnSc1d1
společenstvía	společenstvía	k1gMnSc1
jejich	jejich	k3xOp3gNnPc4
správní	správní	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
(	(	kIx(
<g/>
Sevilla	Sevilla	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Aragonie	Aragonie	k1gFnSc1
(	(	kIx(
<g/>
Zaragoza	Zaragoza	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Asturie	Asturie	k1gFnSc1
(	(	kIx(
<g/>
Oviedo	Oviedo	k1gNnSc1
–	–	k?
Uviéu	Uviéus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Baleáry	Baleáry	k1gFnPc1
(	(	kIx(
<g/>
Palma	palma	k1gFnSc1
de	de	k?
Mallorca	Mallorca	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Baskicko	Baskicko	k1gNnSc1
(	(	kIx(
<g/>
Vitoria-Gasteiz	Vitoria-Gasteiz	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Extremadura	Extremadura	k1gFnSc1
(	(	kIx(
<g/>
Mérida	Mérida	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Galicie	Galicie	k1gFnSc1
(	(	kIx(
<g/>
Santiago	Santiago	k1gNnSc1
de	de	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Compostela	Compostela	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kanárské	kanárský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
(	(	kIx(
<g/>
Las	laso	k1gNnPc2
Palmas	Palmasa	k1gFnPc2
de	de	k?
Gran	Gran	k1gNnSc1
Canaria	Canarium	k1gNnSc2
a	a	k8xC
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
de	de	k?
Tenerife	Tenerif	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Kantábrie	Kantábrie	k1gFnSc1
(	(	kIx(
<g/>
Santander	Santander	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kastilie	Kastilie	k1gFnSc1
a	a	k8xC
León	León	k1gInSc1
(	(	kIx(
<g/>
Valladolid	Valladolid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kastilie	Kastilie	k1gFnSc1
–	–	k?
La	la	k1gNnSc1
Mancha	Mancha	k1gFnSc1
(	(	kIx(
<g/>
Toledo	Toledo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Katalánsko	Katalánsko	k1gNnSc1
(	(	kIx(
<g/>
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
La	la	k1gNnSc1
Rioja	Rioja	k1gMnSc1
(	(	kIx(
<g/>
Logroñ	Logroñ	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Murcie	Murcie	k1gFnSc1
(	(	kIx(
<g/>
Murcia	Murcia	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Navarra	Navarra	k1gFnSc1
(	(	kIx(
<g/>
Pamplona	Pamplona	k1gFnSc1
–	–	k?
Iruñ	Iruñ	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Valencie	Valencie	k1gFnSc1
(	(	kIx(
<g/>
Valencie	Valencie	k1gFnSc1
–	–	k?
Valè	Valè	k1gNnPc1
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Ceuta	Ceuta	k1gFnSc1
•	•	k?
Melilla	Melilla	k1gFnSc1
Přímá	přímý	k2eAgFnSc1d1
správa	správa	k1gFnSc1
ústřední	ústřední	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Španělské	španělský	k2eAgFnPc1d1
severoafrické	severoafrický	k2eAgFnPc1d1
državy	država	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4038714-8	4038714-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80113609	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
152444620	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80113609	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
