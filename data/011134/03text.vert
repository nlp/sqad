<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Pivoňková	Pivoňková	k1gFnSc1	Pivoňková
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
ve	v	k7c6	v
Vlašimi	Vlašim	k1gFnSc6	Vlašim
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
česká	český	k2eAgFnSc1d1	Česká
plavkyně	plavkyně	k1gFnSc1	plavkyně
<g/>
,	,	kIx,	,
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
znak	znak	k1gInSc1	znak
a	a	k8xC	a
vicemistryně	vicemistryně	k1gFnSc1	vicemistryně
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
znak	znak	k1gInSc1	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závodila	závodit	k5eAaImAgFnS	závodit
za	za	k7c4	za
klub	klub	k1gInSc4	klub
USK	USK	kA	USK
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
2004	[number]	k4	2004
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Sportovci	sportovec	k1gMnPc1	sportovec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
sports-reference	sportseferenka	k1gFnSc6	sports-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
swimrankings	swimrankings	k1gInSc4	swimrankings
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
