<s>
Od	od	k7c2	od
citronu	citron	k1gInSc2	citron
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
)	)	kIx)	)
a	a	k8xC	a
mírou	míra	k1gFnSc7	míra
kyselosti	kyselost	k1gFnSc2	kyselost
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
