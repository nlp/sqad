<s>
Limeta	Limeta	k1gFnSc1	Limeta
(	(	kIx(	(
<g/>
též	též	k9	též
limetka	limetka	k1gFnSc1	limetka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
citrusový	citrusový	k2eAgInSc4d1	citrusový
plod	plod	k1gInSc4	plod
stromu	strom	k1gInSc2	strom
Citrus	citrus	k1gInSc1	citrus
limetta	limetta	k1gFnSc1	limetta
<g/>
,	,	kIx,	,
řazený	řazený	k2eAgInSc1d1	řazený
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
kyseloplodých	kyseloplodý	k2eAgInPc2d1	kyseloplodý
citrusů	citrus	k1gInPc2	citrus
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
citron	citron	k1gInSc4	citron
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
citronu	citron	k1gInSc2	citron
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
)	)	kIx)	)
a	a	k8xC	a
mírou	míra	k1gFnSc7	míra
kyselosti	kyselost	k1gFnSc2	kyselost
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sklízí	sklízet	k5eAaImIp3nP	sklízet
se	se	k3xPyFc4	se
nezralá	zralý	k2eNgNnPc1d1	nezralé
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
jí	on	k3xPp3gFnSc7	on
neškodí	škodit	k5eNaImIp3nS	škodit
na	na	k7c6	na
šťavnatosti	šťavnatost	k1gFnSc6	šťavnatost
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
se	s	k7c7	s
žluto-zelenou	žlutoelený	k2eAgFnSc7d1	žluto-zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
limetky	limetka	k1gFnSc2	limetka
se	se	k3xPyFc4	se
od	od	k7c2	od
citronu	citron	k1gInSc2	citron
liší	lišit	k5eAaImIp3nP	lišit
opět	opět	k6eAd1	opět
pouze	pouze	k6eAd1	pouze
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Limeta	Limeta	k1gFnSc1	Limeta
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
v	v	k7c6	v
takřka	takřka	k6eAd1	takřka
stejné	stejný	k2eAgFnSc6d1	stejná
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Limety	Limeta	k1gFnPc1	Limeta
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
:	:	kIx,	:
Limeta	Limet	k1gMnSc2	Limet
mexická	mexický	k2eAgFnSc1d1	mexická
–	–	k?	–
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
krásně	krásně	k6eAd1	krásně
zelené	zelený	k2eAgNnSc4d1	zelené
zbarvení	zbarvení	k1gNnSc4	zbarvení
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hojně	hojně	k6eAd1	hojně
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Limeta	Limeta	k1gFnSc1	Limeta
perská	perský	k2eAgFnSc1d1	perská
–	–	k?	–
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
zelenožluté	zelenožlutý	k2eAgNnSc1d1	zelenožluté
zbarvení	zbarvení	k1gNnSc1	zbarvení
(	(	kIx(	(
<g/>
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
časté	častý	k2eAgInPc1d1	častý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Limety	Limeta	k1gFnPc1	Limeta
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
citrony	citron	k1gInPc1	citron
vitamíny	vitamín	k1gInPc4	vitamín
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Limety	Limeta	k1gFnPc1	Limeta
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
páse	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Limeta	Limeta	k1gFnSc1	Limeta
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
používaná	používaný	k2eAgFnSc1d1	používaná
do	do	k7c2	do
koktejlů	koktejl	k1gInPc2	koktejl
(	(	kIx(	(
<g/>
jako	jako	k9	jako
např.	např.	kA	např.
Cuba	Cuba	k1gFnSc1	Cuba
Libre	Libr	k1gInSc5	Libr
nebo	nebo	k8xC	nebo
mojito	mojít	k5eAaPmNgNnS	mojít
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
salátové	salátový	k2eAgInPc4d1	salátový
zálivky	zálivek	k1gInPc4	zálivek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
šťávou	šťáva	k1gFnSc7	šťáva
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
marinovat	marinovat	k5eAaBmF	marinovat
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
maso	maso	k1gNnSc4	maso
solit	solit	k5eAaImF	solit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
nakládají	nakládat	k5eAaImIp3nP	nakládat
limetky	limetek	k1gInPc1	limetek
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
okurky	okurka	k1gFnSc2	okurka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kůra	kůra	k1gFnSc1	kůra
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
citronová	citronový	k2eAgFnSc1d1	citronová
(	(	kIx(	(
<g/>
nastrouhaná	nastrouhaný	k2eAgFnSc1d1	nastrouhaná
<g/>
)	)	kIx)	)
na	na	k7c4	na
sorbety	sorbet	k1gInPc4	sorbet
a	a	k8xC	a
moučníky	moučník	k1gInPc4	moučník
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
kyselá	kyselý	k2eAgFnSc1d1	kyselá
<g/>
.	.	kIx.	.
</s>
<s>
Výtažky	výtažek	k1gInPc1	výtažek
a	a	k8xC	a
kousky	kousek	k1gInPc1	kousek
limetky	limetka	k1gFnSc2	limetka
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
pro	pro	k7c4	pro
údajné	údajný	k2eAgNnSc4d1	údajné
příznivé	příznivý	k2eAgNnSc4d1	příznivé
působení	působení	k1gNnSc4	působení
na	na	k7c4	na
pleť	pleť	k1gFnSc4	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
limetka	limetka	k1gFnSc1	limetka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
limetka	limetka	k1gFnSc1	limetka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
