<s>
Lučavka	lučavka	k1gFnSc1	lučavka
královská	královský	k2eAgFnSc1d1	královská
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
aqua	aqu	k2eAgFnSc1d1	aqua
regia	regia	k1gFnSc1	regia
neboli	neboli	k8xC	neboli
královská	královský	k2eAgFnSc1d1	královská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
také	také	k9	také
acidum	acidum	k1gInSc1	acidum
chloronitrosum	chloronitrosum	k1gInSc1	chloronitrosum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dýmavá	dýmavý	k2eAgFnSc1d1	dýmavá
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
kapalina	kapalina	k1gFnSc1	kapalina
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
rozpouštění	rozpouštění	k1gNnSc4	rozpouštění
obtížně	obtížně	k6eAd1	obtížně
rozpustných	rozpustný	k2eAgInPc2d1	rozpustný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vzácných	vzácný	k2eAgInPc2d1	vzácný
(	(	kIx(	(
<g/>
královských	královský	k2eAgInPc2d1	královský
<g/>
)	)	kIx)	)
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
