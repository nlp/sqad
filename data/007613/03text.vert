<s>
David	David	k1gMnSc1	David
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgNnSc4d1	renesanční
sochařské	sochařský	k2eAgNnSc4d1	sochařské
dílo	dílo	k1gNnSc4	dílo
Michelangela	Michelangel	k1gMnSc2	Michelangel
Buonarrotiho	Buonarroti	k1gMnSc2	Buonarroti
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1501	[number]	k4	1501
<g/>
–	–	k?	–
<g/>
1504	[number]	k4	1504
<g/>
.	.	kIx.	.
</s>
<s>
Znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
biblického	biblický	k2eAgMnSc4d1	biblický
Davida	David	k1gMnSc4	David
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
soubojem	souboj	k1gInSc7	souboj
s	s	k7c7	s
Goliášem	goliáš	k1gInSc7	goliáš
<g/>
;	;	kIx,	;
Michelangelo	Michelangela	k1gFnSc5	Michelangela
dokonale	dokonale	k6eAd1	dokonale
znázornil	znázornit	k5eAaPmAgMnS	znázornit
proporce	proporce	k1gFnPc4	proporce
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
označováno	označován	k2eAgNnSc1d1	označováno
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
sochařských	sochařský	k2eAgNnPc2d1	sochařské
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
mužského	mužský	k2eAgNnSc2d1	mužské
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
vyhotovena	vyhotovit	k5eAaPmNgFnS	vyhotovit
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
objednáno	objednat	k5eAaPmNgNnS	objednat
florentskou	florentský	k2eAgFnSc7d1	florentská
Signorií	signoria	k1gFnSc7	signoria
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1501	[number]	k4	1501
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
umístěna	umístit	k5eAaPmNgFnS	umístit
nejprve	nejprve	k6eAd1	nejprve
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
Palazza	Palazz	k1gMnSc2	Palazz
Vecchio	Vecchio	k1gNnSc4	Vecchio
<g/>
,	,	kIx,	,
florentské	florentský	k2eAgFnPc4d1	florentská
radnice	radnice	k1gFnPc4	radnice
<g/>
;	;	kIx,	;
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
originál	originál	k1gInSc1	originál
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
Akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
místa	místo	k1gNnSc2	místo
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1504	[number]	k4	1504
svolána	svolán	k2eAgFnSc1d1	svolána
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
členy	člen	k1gInPc4	člen
byli	být	k5eAaImAgMnP	být
také	také	k9	také
například	například	k6eAd1	například
Andrea	Andrea	k1gFnSc1	Andrea
della	della	k1gFnSc1	della
Robbia	Robbia	k1gFnSc1	Robbia
<g/>
,	,	kIx,	,
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticell	k1gMnPc7	Botticell
nebo	nebo	k8xC	nebo
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Michelangelo	Michelanget	k5eAaPmAgNnS	Michelanget
za	za	k7c4	za
sochu	socha	k1gFnSc4	socha
Davida	David	k1gMnSc2	David
údajně	údajně	k6eAd1	údajně
dostal	dostat	k5eAaPmAgInS	dostat
900	[number]	k4	900
zlatých	zlatý	k2eAgInPc2d1	zlatý
dukátů	dukát	k1gInPc2	dukát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
prý	prý	k9	prý
bylo	být	k5eAaImAgNnS	být
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
vydělal	vydělat	k5eAaPmAgInS	vydělat
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
jeho	jeho	k3xOp3gMnSc1	jeho
současník	současník	k1gMnSc1	současník
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc7	Vinca
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
tvoří	tvořit	k5eAaImIp3nS	tvořit
příběh	příběh	k1gInSc1	příběh
tvorby	tvorba	k1gFnSc2	tvorba
Davida	David	k1gMnSc2	David
část	část	k1gFnSc4	část
románu	román	k1gInSc2	román
Karla	Karel	k1gMnSc2	Karel
Schulze	Schulz	k1gMnSc2	Schulz
Kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
bolest	bolest	k1gFnSc1	bolest
<g/>
.	.	kIx.	.
</s>
