<s>
Zloději	zloděj	k1gMnPc1	zloděj
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
Time	Time	k1gFnSc1	Time
Bandits	Banditsa	k1gFnPc2	Banditsa
<g/>
)	)	kIx)	)
je	on	k3xPp3gInPc4	on
fantasy	fantas	k1gInPc4	fantas
film	film	k1gInSc1	film
Terryho	Terry	k1gMnSc2	Terry
Gilliama	Gilliam	k1gMnSc2	Gilliam
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
produkoval	produkovat	k5eAaImAgInS	produkovat
Terry	Terra	k1gFnSc2	Terra
Giliam	Giliam	k1gInSc1	Giliam
a	a	k8xC	a
George	George	k1gFnSc1	George
Harisson	Harissona	k1gFnPc2	Harissona
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
originálního	originální	k2eAgInSc2d1	originální
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
odehrávajícího	odehrávající	k2eAgMnSc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
časových	časový	k2eAgNnPc6d1	časové
obdobích	období	k1gNnPc6	období
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
obsazením	obsazení	k1gNnSc7	obsazení
liliputánů	lilipután	k1gMnPc2	lilipután
do	do	k7c2	do
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
jedenáctiletý	jedenáctiletý	k2eAgMnSc1d1	jedenáctiletý
chlapec	chlapec	k1gMnSc1	chlapec
Kevin	Kevin	k1gMnSc1	Kevin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
za	za	k7c2	za
okázalého	okázalý	k2eAgInSc2d1	okázalý
nezájmu	nezájem	k1gInSc2	nezájem
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
nic	nic	k6eAd1	nic
objeví	objevit	k5eAaPmIp3nS	objevit
skupina	skupina	k1gFnSc1	skupina
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
tunelem	tunel	k1gInSc7	tunel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
projdou	projít	k5eAaPmIp3nP	projít
mnoha	mnoho	k4c2	mnoho
údobími	údobí	k1gNnPc7	údobí
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
setkají	setkat	k5eAaPmIp3nP	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Napoleonem	napoleon	k1gInSc7	napoleon
<g/>
,	,	kIx,	,
projedou	projet	k5eAaPmIp3nP	projet
se	se	k3xPyFc4	se
v	v	k7c6	v
Titanicu	Titanicus	k1gInSc6	Titanicus
<g/>
,	,	kIx,	,
navštíví	navštívit	k5eAaPmIp3nS	navštívit
dvůr	dvůr	k1gInSc1	dvůr
krále	král	k1gMnSc2	král
Agamemnóna	Agamemnón	k1gMnSc2	Agamemnón
<g/>
)	)	kIx)	)
i	i	k9	i
fantastickými	fantastický	k2eAgFnPc7d1	fantastická
krajinami	krajina	k1gFnPc7	krajina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
setkali	setkat	k5eAaPmAgMnP	setkat
a	a	k8xC	a
bojovali	bojovat	k5eAaImAgMnP	bojovat
se	s	k7c7	s
ztělesněnou	ztělesněný	k2eAgFnSc7d1	ztělesněná
Zlobou	zloba	k1gFnSc7	zloba
<g/>
.	.	kIx.	.
</s>
