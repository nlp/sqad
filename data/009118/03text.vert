<p>
<s>
Bone	bon	k1gInSc5	bon
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgNnPc1d1	krátké
<g/>
,	,	kIx,	,
asi	asi	k9	asi
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tok	tok	k1gInSc4	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
jen	jen	k9	jen
kousek	kousek	k1gInSc1	kousek
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
severního	severní	k2eAgNnSc2d1	severní
ramene	rameno	k1gNnSc2	rameno
řeky	řeka	k1gFnSc2	řeka
Palix	Palix	k1gInSc1	Palix
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
směr	směr	k1gInSc1	směr
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
západní	západní	k2eAgFnSc1d1	západní
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
řeka	řeka	k1gFnSc1	řeka
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Willapského	Willapský	k2eAgInSc2d1	Willapský
zálivu	záliv	k1gInSc2	záliv
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Bay	Bay	k1gMnPc2	Bay
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
kousek	kousek	k6eAd1	kousek
severně	severně	k6eAd1	severně
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Niawiakum	Niawiakum	k1gInSc1	Niawiakum
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
řeky	řeka	k1gFnSc2	řeka
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
navíc	navíc	k6eAd1	navíc
představují	představovat	k5eAaImIp3nP	představovat
močály	močál	k1gInPc1	močál
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
řeka	řeka	k1gFnSc1	řeka
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
estuáru	estuár	k1gInSc2	estuár
a	a	k8xC	a
wattového	wattový	k2eAgNnSc2d1	wattové
pobřeží	pobřeží	k1gNnSc2	pobřeží
Willapského	Willapský	k2eAgInSc2d1	Willapský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
ústí	ústí	k1gNnSc2	ústí
řeku	řeka	k1gFnSc4	řeka
překračuje	překračovat	k5eAaImIp3nS	překračovat
silnice	silnice	k1gFnSc1	silnice
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Route	Rout	k1gInSc5	Rout
101	[number]	k4	101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
případů	případ	k1gInPc2	případ
epidemie	epidemie	k1gFnSc2	epidemie
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
severozápad	severozápad	k1gInSc4	severozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
právě	právě	k6eAd1	právě
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Bone	bon	k1gInSc5	bon
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnSc1d1	zdejší
osadník	osadník	k1gMnSc1	osadník
James	James	k1gMnSc1	James
Swan	Swan	k1gMnSc1	Swan
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
šíří	šířit	k5eAaImIp3nS	šířit
mezi	mezi	k7c4	mezi
indiány	indián	k1gMnPc4	indián
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Činuků	Činuk	k1gMnPc2	Činuk
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
několik	několik	k4yIc1	několik
lodí	loď	k1gFnPc2	loď
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
při	při	k7c6	při
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Columbie	Columbia	k1gFnSc2	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
Činuků	Činuk	k1gMnPc2	Činuk
utrpěl	utrpět	k5eAaPmAgInS	utrpět
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
–	–	k?	–
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
roku	rok	k1gInSc2	rok
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
svých	svůj	k3xOyFgMnPc2	svůj
členů	člen	k1gMnPc2	člen
–	–	k?	–
a	a	k8xC	a
epidemie	epidemie	k1gFnSc1	epidemie
se	se	k3xPyFc4	se
šířila	šířit	k5eAaImAgFnS	šířit
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
Columbie	Columbia	k1gFnSc2	Columbia
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Washingtonu	Washington	k1gInSc2	Washington
a	a	k8xC	a
Oregonu	Oregon	k1gInSc2	Oregon
<g/>
,	,	kIx,	,
na	na	k7c4	na
Vancouverův	Vancouverův	k2eAgInSc4d1	Vancouverův
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
březích	břeh	k1gInPc6	břeh
řek	řek	k1gMnSc1	řek
Skagit	Skagit	k1gMnSc1	Skagit
a	a	k8xC	a
Nooksack	Nooksack	k1gMnSc1	Nooksack
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
historie	historie	k1gFnPc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k9	jako
Národní	národní	k2eAgFnSc1d1	národní
územní	územní	k2eAgFnSc1d1	územní
rezervace	rezervace	k1gFnSc1	rezervace
Bone	bon	k1gInSc5	bon
River	Rivra	k1gFnPc2	Rivra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
038	[number]	k4	038
hektarů	hektar	k1gInPc2	hektar
chrání	chránit	k5eAaImIp3nS	chránit
to	ten	k3xDgNnSc1	ten
nejčistší	čistý	k2eAgNnSc1d3	nejčistší
zbývající	zbývající	k2eAgNnSc1d1	zbývající
slanisko	slanisko	k1gNnSc1	slanisko
Willapského	Willapský	k2eAgInSc2d1	Willapský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
zdejší	zdejší	k2eAgNnSc4d1	zdejší
vodní	vodní	k2eAgNnSc4d1	vodní
ptactvo	ptactvo	k1gNnSc4	ptactvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
projekty	projekt	k1gInPc1	projekt
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
povodím	povodí	k1gNnSc7	povodí
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
řeky	řeka	k1gFnSc2	řeka
Niawiakum	Niawiakum	k1gNnSc1	Niawiakum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bone	bon	k1gInSc5	bon
River	Rivero	k1gNnPc2	Rivero
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
