<s>
Antonio	Antonio	k1gMnSc1
Lucio	Lucio	k6eAd1
Vivaldi	Vivald	k1gMnPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1678	#num#	k4
Benátky	Benátky	k1gFnPc4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1741	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přezdíván	přezdíván	k2eAgInSc1d1
jako	jako	k8xC,k8xS
il	il	k?
Prete	Pret	k1gInSc5
Rosso	Rossa	k1gFnSc5
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
zrzavý	zrzavý	k2eAgMnSc1d1
n.	n.	k?
rudý	rudý	k1gMnSc1
kněz	kněz	k1gMnSc1
<g/>
)	)	kIx)
díky	díky	k7c3
zrzavé	zrzavý	k2eAgFnSc3d1
barvě	barva	k1gFnSc3
svých	svůj	k3xOyFgInPc2
vlasů	vlas	k1gInPc2
(	(	kIx(
<g/>
pod	pod	k7c7
parukou	paruka	k1gFnSc7
nejsou	být	k5eNaImIp3nP
vidět	vidět	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
barokní	barokní	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
houslový	houslový	k2eAgMnSc1d1
virtuos	virtuos	k1gMnSc1
<g/>
.	.	kIx.
</s>