<s>
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
</s>
<s>
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
Rodné	rodný	k2eAgInPc5d1
jméno	jméno	k1gNnSc4
</s>
<s>
Antonio	Antonio	k1gMnSc1
Lucio	Lucio	k6eAd1
Vivaldi	Vivald	k1gMnPc1
Narození	narození	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1678	#num#	k4
Benátky	Benátky	k1gFnPc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1741	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Vídeň	Vídeň	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
operní	operní	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
klavírista	klavírista	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
Catholic	Catholice	k1gFnPc2
priest	priest	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
varhaník	varhaník	k1gMnSc1
<g/>
,	,	kIx,
impresário	impresário	k1gMnSc1
a	a	k8xC
houslista	houslista	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Giovanni	Giovann	k1gMnPc1
Battista	Battista	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
a	a	k8xC
Camilla	Camillo	k1gNnPc1
Calicchio	Calicchio	k6eAd1
Významná	významný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Čtvero	čtvero	k4xRgNnSc4
ročních	roční	k2eAgFnPc2d1
dobOrlando	dobOrlando	k6eAd1
furioso	furioso	k6eAd1
(	(	kIx(
<g/>
Vivaldi	Vivald	k1gMnPc1
<g/>
,	,	kIx,
1727	#num#	k4
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antonio	Antonio	k1gMnSc1
Lucio	Lucio	k6eAd1
Vivaldi	Vivald	k1gMnPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1678	#num#	k4
Benátky	Benátky	k1gFnPc4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1741	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přezdíván	přezdíván	k2eAgInSc1d1
jako	jako	k8xC,k8xS
il	il	k?
Prete	Pret	k1gInSc5
Rosso	Rossa	k1gFnSc5
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
zrzavý	zrzavý	k2eAgMnSc1d1
n.	n.	k?
rudý	rudý	k1gMnSc1
kněz	kněz	k1gMnSc1
<g/>
)	)	kIx)
díky	díky	k7c3
zrzavé	zrzavý	k2eAgFnSc3d1
barvě	barva	k1gFnSc3
svých	svůj	k3xOyFgInPc2
vlasů	vlas	k1gInPc2
(	(	kIx(
<g/>
pod	pod	k7c7
parukou	paruka	k1gFnSc7
nejsou	být	k5eNaImIp3nP
vidět	vidět	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
barokní	barokní	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
houslový	houslový	k2eAgMnSc1d1
virtuos	virtuos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nejznámějším	známý	k2eAgInSc7d3
dílem	díl	k1gInSc7
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc4
čtyř	čtyři	k4xCgInPc2
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
nazvaný	nazvaný	k2eAgInSc1d1
Čtvero	čtvero	k4xRgNnSc1
ročních	roční	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejznámějším	známý	k2eAgFnPc3d3
a	a	k8xC
nejhranějším	hraný	k2eAgFnPc3d3
skladbám	skladba	k1gFnPc3
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
Vivaldiho	Vivaldi	k1gMnSc2
</s>
<s>
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
podob	podoba	k1gFnPc2
Antonia	Antonio	k1gMnSc2
Vivaldiho	Vivaldi	k1gMnSc2
<g/>
,	,	kIx,
mědiryt	mědiryt	k1gInSc4
od	od	k7c2
Françoise	Françoise	k1gFnSc2
Morellona	Morellon	k1gMnSc2
la	la	k1gNnSc2
Cave	Cav	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1725	#num#	k4
pro	pro	k7c4
vydání	vydání	k1gNnSc4
sbírky	sbírka	k1gFnSc2
koncertů	koncert	k1gInPc2
op	op	k1gMnSc1
<g/>
.	.	kIx.
8	#num#	k4
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1678	#num#	k4
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžitě	okamžitě	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
byl	být	k5eAaImAgInS
pokřtěn	pokřtít	k5eAaPmNgInS
porodní	porodní	k2eAgFnSc7d1
asistentkou	asistentka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
bylo	být	k5eAaImAgNnS
bezprostřední	bezprostřední	k2eAgNnSc1d1
ohrožení	ohrožení	k1gNnSc1
života	život	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
dnes	dnes	k6eAd1
již	již	k6eAd1
není	být	k5eNaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
špatný	špatný	k2eAgInSc4d1
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
novorozence	novorozenec	k1gMnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
kvůli	kvůli	k7c3
zemětřesení	zemětřesení	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
ten	ten	k3xDgInSc4
den	den	k1gInSc4
Benátky	Benátky	k1gFnPc1
postihlo	postihnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
se	se	k3xPyFc4
živil	živit	k5eAaImAgMnS
jako	jako	k9
holič	holič	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
též	též	k9
zřejmě	zřejmě	k6eAd1
znamenitý	znamenitý	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
dost	dost	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1688	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
uvedena	uveden	k2eAgFnSc1d1
opera	opera	k1gFnSc1
La	la	k1gNnSc1
Fedeltà	Fedeltà	k1gMnSc2
sfortunata	sfortunat	k1gMnSc2
skladatele	skladatel	k1gMnSc2
jménem	jméno	k1gNnSc7
Giovanni	Giovanň	k1gMnSc3
Battista	Battista	k1gMnSc1
Rossi	Ross	k1gMnSc3
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
je	být	k5eAaImIp3nS
totožné	totožný	k2eAgNnSc1d1
se	s	k7c7
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
pod	pod	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
Antoniův	Antoniův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
vystupoval	vystupovat	k5eAaImAgMnS
jako	jako	k9
hudebník	hudebník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
naráželo	narážet	k5eAaPmAgNnS,k5eAaImAgNnS
na	na	k7c4
barvu	barva	k1gFnSc4
vlasů	vlas	k1gInPc2
obvyklou	obvyklý	k2eAgFnSc4d1
v	v	k7c6
rodině	rodina	k1gFnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Rosso	Rossa	k1gFnSc5
=	=	kIx~
zrzavý	zrzavý	k2eAgInSc4d1
<g/>
,	,	kIx,
rudý	rudý	k2eAgInSc4d1
–	–	k?
ani	ani	k8xC
Antonio	Antonio	k1gMnSc1
se	se	k3xPyFc4
tomuto	tento	k3xDgNnSc3
označení	označení	k1gNnSc3
nevyhnul	vyhnout	k5eNaPmAgInS
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostalo	dostat	k5eAaPmAgNnS
přezdívky	přezdívka	k1gFnSc2
„	„	k?
<g/>
zrzavý	zrzavý	k2eAgMnSc1d1
páter	páter	k1gMnSc1
<g/>
“	“	k?
–	–	k?
Il	Il	k1gMnSc5
Prete	Pret	k1gMnSc5
Rosso	Rossa	k1gFnSc5
<g/>
.	.	kIx.
<g/>
)	)	kIx)
V	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
jako	jako	k9
hudebník	hudebník	k1gMnSc1
byl	být	k5eAaImAgMnS
otec	otec	k1gMnSc1
zřejmě	zřejmě	k6eAd1
dobrý	dobrý	k2eAgMnSc1d1
a	a	k8xC
uznávaný	uznávaný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
sdružení	sdružení	k1gNnPc2
Sovvegno	Sovvegno	k6eAd1
dei	dei	k?
musicisti	musicistit	k5eAaPmRp2nS
di	di	k?
Santa	Sant	k1gMnSc4
Cecilia	Cecilius	k1gMnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
něco	něco	k6eAd1
mezi	mezi	k7c7
hudebním	hudební	k2eAgInSc7d1
spolkem	spolek	k1gInSc7
a	a	k8xC
odbory	odbor	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
pravidelně	pravidelně	k6eAd1
hrával	hrávat	k5eAaImAgMnS
v	v	k7c6
orchestru	orchestr	k1gInSc6
baziliky	bazilika	k1gFnSc2
sv.	sv.	kA
Marka	Marek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Tak	tak	k9
Antonio	Antonio	k1gMnSc1
základy	základ	k1gInPc4
hry	hra	k1gFnSc2
na	na	k7c4
housle	housle	k1gFnPc4
získal	získat	k5eAaPmAgMnS
u	u	k7c2
otce	otec	k1gMnSc2
a	a	k8xC
první	první	k4xOgFnSc1
lekce	lekce	k1gFnSc1
kompozice	kompozice	k1gFnSc2
mu	on	k3xPp3gMnSc3
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
poskytl	poskytnout	k5eAaPmAgInS
Giovanni	Giovaneň	k1gFnSc3
Legrenzi	Legrenze	k1gFnSc4
<g/>
,	,	kIx,
znamenitý	znamenitý	k2eAgMnSc1d1
barokní	barokní	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
době	doba	k1gFnSc6
v	v	k7c6
chrámu	chrám	k1gInSc6
sv.	sv.	kA
Marka	Marek	k1gMnSc2
kapelníkem	kapelník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
roku	rok	k1gInSc2
1691	#num#	k4
<g/>
,	,	kIx,
tj.	tj.	kA
z	z	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
skladateli	skladatel	k1gMnSc3
bylo	být	k5eAaImAgNnS
13	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
chrámová	chrámový	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
Laetatus	Laetatus	k1gInSc1
sum	suma	k1gFnPc2
(	(	kIx(
<g/>
RV	RV	kA
Anh	Anh	k1gFnSc1
31	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nese	nést	k5eAaImIp3nS
zřetelné	zřetelný	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
Legrenziho	Legrenzi	k1gMnSc2
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
intenzivního	intenzivní	k2eAgNnSc2d1
hudebního	hudební	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
studoval	studovat	k5eAaImAgMnS
teologii	teologie	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1703	#num#	k4
byl	být	k5eAaImAgInS
vysvěcen	vysvětit	k5eAaPmNgInS
na	na	k7c4
kněze	kněz	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vivaldi	Vivald	k1gMnPc1
však	však	k9
od	od	k7c2
dětství	dětství	k1gNnSc2
trpěl	trpět	k5eAaImAgInS
vrozeným	vrozený	k2eAgNnSc7d1
zúžením	zúžení	k1gNnSc7
hrtanu	hrtan	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
vyvinulo	vyvinout	k5eAaPmAgNnS
vážné	vážný	k2eAgNnSc4d1
astma	astma	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
tohoto	tento	k3xDgNnSc2
onemocnění	onemocnění	k1gNnSc2
byl	být	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
osvobozen	osvobodit	k5eAaPmNgMnS
od	od	k7c2
povinnosti	povinnost	k1gFnSc2
sloužit	sloužit	k5eAaImF
mše	mše	k1gFnPc4
(	(	kIx(
<g/>
1704	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1706	#num#	k4
přestal	přestat	k5eAaPmAgInS
být	být	k5eAaImF
aktivním	aktivní	k2eAgMnSc7d1
knězem	kněz	k1gMnSc7
zcela	zcela	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Ospedale	Ospedale	k6eAd1
della	delnout	k5eAaPmAgFnS,k5eAaImAgFnS,k5eAaBmAgFnS
Pietà	Pietà	k1gFnSc1
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
</s>
<s>
Gabriele	Gabriela	k1gFnSc3
Bella	Bella	k1gMnSc1
<g/>
:	:	kIx,
Vystoupení	vystoupení	k1gNnSc1
Ospedalského	Ospedalský	k2eAgInSc2d1
sboru	sbor	k1gInSc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
cantata	cantat	k1gMnSc2
delle	dell	k1gMnSc2
putte	putte	k5eAaPmIp2nP
delli	dell	k1gMnPc1
Ospitali	Ospitali	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolem	kolem	k7c2
roku	rok	k1gInSc2
1720	#num#	k4
<g/>
,	,	kIx,
Benátky	Benátky	k1gFnPc1
<g/>
,	,	kIx,
Palazzo	Palazza	k1gFnSc5
Querini	Querin	k1gMnPc1
Stampalia	Stampalius	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1703	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
houslovým	houslový	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
v	v	k7c6
sirotčinci	sirotčinec	k1gInSc6
Pio	Pio	k1gMnPc2
Ospedale	Ospedala	k1gFnSc3
della	della	k6eAd1
Pietà	Pietà	k1gMnPc1
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
takovýchto	takovýto	k3xDgFnPc2
institucí	instituce	k1gFnPc2
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
několik	několik	k4yIc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
poskytnout	poskytnout	k5eAaPmF
ochranu	ochrana	k1gFnSc4
a	a	k8xC
vzdělání	vzdělání	k1gNnSc4
opuštěným	opuštěný	k2eAgMnPc3d1
sirotkům	sirotek	k1gMnPc3
a	a	k8xC
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
rodiče	rodič	k1gMnPc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
nemohli	moct	k5eNaImAgMnP
postarat	postarat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provoz	provoz	k1gInSc1
byl	být	k5eAaImAgInS
financován	financovat	k5eAaBmNgInS
z	z	k7c2
prostředků	prostředek	k1gInPc2
Benátské	benátský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlapci	chlapec	k1gMnPc1
se	se	k3xPyFc4
vyučili	vyučit	k5eAaPmAgMnP
řemeslům	řemeslo	k1gNnPc3
a	a	k8xC
opouštěli	opouštět	k5eAaImAgMnP
sirotčinec	sirotčinec	k1gInSc4
v	v	k7c6
15	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívky	dívka	k1gFnSc2
byly	být	k5eAaImAgInP
vedeny	vést	k5eAaImNgInP
k	k	k7c3
domácím	domácí	k2eAgFnPc3d1
pracím	práce	k1gFnPc3
<g/>
,	,	kIx,
získávaly	získávat	k5eAaImAgFnP
hudební	hudební	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
a	a	k8xC
ty	ten	k3xDgFnPc1
nejtalentovanější	talentovaný	k2eAgFnPc1d3
se	se	k3xPyFc4
stávaly	stávat	k5eAaImAgFnP
členkami	členka	k1gFnPc7
Ospedalského	Ospedalský	k2eAgInSc2d1
sboru	sbor	k1gInSc2
a	a	k8xC
orchestru	orchestr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinností	povinnost	k1gFnPc2
sboru	sbor	k1gInSc2
a	a	k8xC
orchestru	orchestr	k1gInSc2
byla	být	k5eAaImAgFnS
účast	účast	k1gFnSc1
na	na	k7c6
mších	mše	k1gFnPc6
a	a	k8xC
slavnostech	slavnost	k1gFnPc6
církevních	církevní	k2eAgFnPc2d1
i	i	k8xC
světských	světský	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
toto	tento	k3xDgNnSc4
těleso	těleso	k1gNnSc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
Vivaldi	Vivald	k1gMnPc1
mnoho	mnoho	k4c4
koncertů	koncert	k1gInPc2
<g/>
,	,	kIx,
kantát	kantáta	k1gFnPc2
a	a	k8xC
duchovních	duchovní	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
mistrů	mistr	k1gMnPc2
učitelů	učitel	k1gMnPc2
v	v	k7c6
sirotčinci	sirotčinec	k1gInSc6
nebyla	být	k5eNaImAgFnS
zaměstnáním	zaměstnání	k1gNnSc7
na	na	k7c4
dobu	doba	k1gFnSc4
neurčitou	určitý	k2eNgFnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
byli	být	k5eAaImAgMnP
mistři	mistr	k1gMnPc1
znovu	znovu	k6eAd1
voleni	volit	k5eAaImNgMnP
jakousi	jakýsi	k3yIgFnSc7
správní	správní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
sirotčince	sirotčinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1709	#num#	k4
Vivaldi	Vivald	k1gMnPc1
zvolen	zvolit	k5eAaPmNgMnS
nebyl	být	k5eNaImAgMnS
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
rok	rok	k1gInSc1
byl	být	k5eAaImAgInS
prakticky	prakticky	k6eAd1
bez	bez	k7c2
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1711	#num#	k4
se	se	k3xPyFc4
však	však	k9
do	do	k7c2
funkce	funkce	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
a	a	k8xC
roku	rok	k1gInSc2
1716	#num#	k4
byl	být	k5eAaImAgMnS
ustanoven	ustanovit	k5eAaPmNgMnS
koncertním	koncertní	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
těchto	tento	k3xDgNnPc2
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
Vivaldi	Vivald	k1gMnPc1
neobyčejně	obyčejně	k6eNd1
plodný	plodný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkomponoval	zkomponovat	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
oper	opera	k1gFnPc2
a	a	k8xC
mnoho	mnoho	k6eAd1
hudby	hudba	k1gFnSc2
instrumentální	instrumentální	k2eAgNnPc1d1
i	i	k8xC
vokální	vokální	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1705	#num#	k4
vydal	vydat	k5eAaPmAgMnS
tiskem	tisk	k1gInSc7
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
sbírku	sbírka	k1gFnSc4
dvanácti	dvanáct	k4xCc2
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
dvoje	dvoje	k4xRgFnPc4
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
Raccolta	Raccolta	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1708	#num#	k4
následoval	následovat	k5eAaImAgInS
další	další	k2eAgInSc1d1
svazek	svazek	k1gInSc1
12	#num#	k4
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
housle	housle	k1gFnPc4
sólo	sólo	k1gNnSc1
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
sbírky	sbírka	k1gFnPc1
byly	být	k5eAaImAgFnP
psány	psát	k5eAaImNgFnP
ještě	ještě	k9
v	v	k7c6
konvenčním	konvenční	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečným	skutečný	k2eAgInSc7d1
přelomem	přelom	k1gInSc7
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
tvorbě	tvorba	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
opus	opus	k1gInSc1
3	#num#	k4
<g/>
,	,	kIx,
Dvanáct	dvanáct	k4xCc1
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
jedny	jeden	k4xCgFnPc4
<g/>
,	,	kIx,
dvoje	dvoje	k4xRgFnPc1
a	a	k8xC
čtvery	čtvero	k4xRgFnPc1
housle	housle	k1gFnPc1
a	a	k8xC
smyčcový	smyčcový	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
L	L	kA
<g/>
’	’	k?
<g/>
Estro	Estro	k1gNnSc1
Armonico	Armonico	k1gMnSc1
<g/>
,	,	kIx,
věnovaný	věnovaný	k2eAgMnSc1d1
Ferdinandu	Ferdinand	k1gMnSc3
Medicejskému	Medicejský	k2eAgInSc3d1
a	a	k8xC
vydaný	vydaný	k2eAgInSc1d1
v	v	k7c6
Amsterodamu	Amsterodam	k1gInSc6
roku	rok	k1gInSc2
1711	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyklus	cyklus	k1gInSc1
měl	mít	k5eAaImAgInS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
obrovský	obrovský	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
ještě	ještě	k6eAd1
znásobený	znásobený	k2eAgInSc1d1
následujícím	následující	k2eAgInSc7d1
cyklem	cyklus	k1gInSc7
koncertů	koncert	k1gInPc2
pro	pro	k7c4
housle	housle	k1gFnPc4
a	a	k8xC
orchestr	orchestr	k1gInSc1
La	la	k1gNnSc2
Stravaganza	Stravaganza	k1gFnSc1
<g/>
,	,	kIx,
vydaným	vydaný	k2eAgNnSc7d1
v	v	k7c6
roce	rok	k1gInSc6
1714	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
úspěchu	úspěch	k1gInSc6
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Vivaldi	Vivald	k1gMnPc1
žádaným	žádaný	k2eAgMnSc7d1
hudebníkem	hudebník	k1gMnSc7
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
četným	četný	k2eAgFnPc3d1
koncertním	koncertní	k2eAgFnPc3d1
cestám	cesta	k1gFnPc3
poctivě	poctivě	k6eAd1
plnil	plnit	k5eAaImAgInS
smlouvu	smlouva	k1gFnSc4
se	s	k7c7
sirotčincem	sirotčinec	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zněla	znět	k5eAaImAgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
nové	nový	k2eAgInPc4d1
koncerty	koncert	k1gInPc4
měsíčně	měsíčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
účetních	účetní	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
zkomponoval	zkomponovat	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1723	#num#	k4
až	až	k9
1729	#num#	k4
na	na	k7c4
140	#num#	k4
koncertů	koncert	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vivaldi	Vivald	k1gMnPc1
a	a	k8xC
opera	opera	k1gFnSc1
</s>
<s>
Karikatura	karikatura	k1gFnSc1
od	od	k7c2
Piera	Pier	k1gInSc2
Leone	Leo	k1gMnSc5
Ghezzi	Ghezze	k1gFnSc4
</s>
<s>
V	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
opera	opera	k1gFnSc1
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
nejpopulárnější	populární	k2eAgFnSc7d3
formou	forma	k1gFnSc7
zábavy	zábava	k1gFnSc2
a	a	k8xC
nejvýnosnější	výnosný	k2eAgNnSc1d3
pro	pro	k7c4
skladatele	skladatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
divadelních	divadelní	k2eAgFnPc2d1
scén	scéna	k1gFnPc2
soutěžilo	soutěžit	k5eAaImAgNnS
o	o	k7c4
přízeň	přízeň	k1gFnSc4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vivaldi	Vivald	k1gMnPc1
svou	svůj	k3xOyFgFnSc4
operní	operní	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
mimo	mimo	k7c4
Benátky	Benátky	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
opera	opera	k1gFnSc1
Ottone	Otton	k1gInSc5
in	in	k?
villa	villa	k1gMnSc1
(	(	kIx(
<g/>
RV	RV	kA
729	#num#	k4
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
ve	v	k7c6
Vicenze	Vicenza	k1gFnSc6
roku	rok	k1gInSc2
1713	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
se	se	k3xPyFc4
však	však	k9
stal	stát	k5eAaPmAgMnS
impresáriem	impresário	k1gMnSc7
divadla	divadlo	k1gNnSc2
Sant	Sant	k1gInSc4
<g/>
'	'	kIx"
<g/>
Angelo	Angela	k1gFnSc5
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
opera	opera	k1gFnSc1
Orlando	Orlanda	k1gFnSc5
finto	finta	k1gFnSc5
pazzo	pazza	k1gFnSc5
(	(	kIx(
<g/>
RV	RV	kA
727	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opera	opera	k1gFnSc1
však	však	k9
neměla	mít	k5eNaImAgFnS
velký	velký	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
úspěch	úspěch	k1gInSc4
měla	mít	k5eAaImAgFnS
spoluúčast	spoluúčast	k1gFnSc1
na	na	k7c6
společném	společný	k2eAgInSc6d1
projektu	projekt	k1gInSc6
sedmi	sedm	k4xCc2
skladatelů	skladatel	k1gMnPc2
Nerone	Nero	k1gMnSc5
fatto	fatta	k1gMnSc5
Cesare	Cesar	k1gMnSc5
(	(	kIx(
<g/>
RV	RV	kA
274	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
11	#num#	k4
árií	árie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partitura	partitura	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
ztratila	ztratit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Arsilda	Arsild	k1gMnSc2
regina	regin	k1gMnSc2
di	di	k?
Ponto	Ponto	k1gNnSc1
(	(	kIx(
<g/>
RV	RV	kA
700	#num#	k4
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
zase	zase	k9
problémy	problém	k1gInPc4
s	s	k7c7
censurou	censura	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
vadil	vadit	k5eAaImAgMnS
trochu	trochu	k6eAd1
lesbický	lesbický	k2eAgInSc4d1
motiv	motiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrdinka	hrdinka	k1gFnSc1
Arsilda	Arsilda	k1gFnSc1
se	se	k3xPyFc4
totiž	totiž	k9
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
jiné	jiný	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
níž	jenž	k3xRgFnSc6
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgFnS
opera	opera	k1gFnSc1
censurou	censura	k1gFnSc7
konečně	konečně	k6eAd1
povolena	povolen	k2eAgFnSc1d1
<g/>
,	,	kIx,
sklidila	sklidit	k5eAaPmAgFnS
u	u	k7c2
diváků	divák	k1gMnPc2
bouřlivý	bouřlivý	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
u	u	k7c2
všech	všecek	k3xTgNnPc2
děl	dělo	k1gNnPc2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
a	a	k8xC
historie	historie	k1gFnSc2
není	být	k5eNaImIp3nS
ovšem	ovšem	k9
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
kvůli	kvůli	k7c3
hudebním	hudební	k2eAgFnPc3d1
kvalitám	kvalita	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
jeho	jeho	k3xOp3gFnPc7
dalšími	další	k2eAgFnPc7d1
operami	opera	k1gFnPc7
zasluhuje	zasluhovat	k5eAaImIp3nS
pozornosti	pozornost	k1gFnSc3
zejména	zejména	k9
opera	opera	k1gFnSc1
La	la	k1gNnSc2
costanza	costanza	k1gFnSc1
trionfante	trionfant	k1gMnSc5
degli	degl	k1gMnSc5
amori	amor	k1gMnSc5
e	e	k0
degli	deglit	k5eAaPmRp2nS
odi	odi	k?
(	(	kIx(
<g/>
RV	RV	kA
706	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1716	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
přepracované	přepracovaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1732	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
Artabano	Artabana	k1gFnSc5
re	re	k9
dei	dei	k?
Parti	Parti	k1gNnPc1
hrána	hrát	k5eAaImNgNnP
také	také	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ironií	ironie	k1gFnPc2
osudu	osud	k1gInSc2
se	se	k3xPyFc4
právě	právě	k9
tato	tento	k3xDgFnSc1
pražská	pražský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
nedochovala	dochovat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
Pio	Pio	k1gMnSc4
Ospedale	Ospedala	k1gFnSc6
komponoval	komponovat	k5eAaImAgMnS
také	také	k9
řadu	řada	k1gFnSc4
liturgických	liturgický	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
nejdůležitější	důležitý	k2eAgMnSc1d3
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
oratoria	oratorium	k1gNnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
první	první	k4xOgInSc1
Moyses	Moyses	k1gInSc1
Deus	Deus	k1gInSc1
Pharaonis	Pharaonis	k1gFnSc1
(	(	kIx(
<g/>
RV	RV	kA
643	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
nedochovalo	dochovat	k5eNaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
druhé	druhý	k4xOgNnSc1
<g/>
,	,	kIx,
Juditha	Juditha	k1gMnSc1
triumphans	triumphans	k1gInSc1
(	(	kIx(
<g/>
RV	RV	kA
644	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
uměleckých	umělecký	k2eAgInPc2d1
vrcholů	vrchol	k1gInPc2
Vivaldiho	Vivaldi	k1gMnSc2
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
objednáno	objednat	k5eAaPmNgNnS
k	k	k7c3
oslavě	oslava	k1gFnSc3
vítězství	vítězství	k1gNnSc2
nad	nad	k7c4
Turky	Turek	k1gMnPc4
a	a	k8xC
znovudobytí	znovudobytí	k1gNnSc4
ostrova	ostrov	k1gInSc2
Korfu	Korfu	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zajímavost	zajímavost	k1gFnSc4
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
dodat	dodat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgFnPc1
role	role	k1gFnPc1
i	i	k8xC
orchestr	orchestr	k1gInSc1
byly	být	k5eAaImAgFnP
obsazeny	obsadit	k5eAaPmNgInP
chovankami	chovanka	k1gFnPc7
sirotčince	sirotčinec	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ukazuje	ukazovat	k5eAaImIp3nS
vynikající	vynikající	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
pedagogické	pedagogický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
učitelů	učitel	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
interpretačních	interpretační	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
dívek	dívka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vivaldiho	Vivaldize	k6eAd1
úspěchy	úspěch	k1gInPc1
a	a	k8xC
moderní	moderní	k2eAgInSc1d1
styl	styl	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
oper	opera	k1gFnPc2
vyvolaly	vyvolat	k5eAaPmAgFnP
bouřlivou	bouřlivý	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
u	u	k7c2
jeho	jeho	k3xOp3gMnPc2
konzervativnějších	konzervativní	k2eAgMnPc2d2
kolegů	kolega	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benedetto	Benedetto	k1gNnSc1
Marcello	Marcello	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
rodina	rodina	k1gFnSc1
byla	být	k5eAaImAgFnS
ironií	ironie	k1gFnSc7
osudu	osud	k1gInSc2
vlastníkem	vlastník	k1gMnSc7
divadla	divadlo	k1gNnSc2
Sant	Sant	k1gInSc4
<g/>
´	´	k?
Angelo	Angela	k1gFnSc5
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
dokonce	dokonce	k9
posměšný	posměšný	k2eAgInSc1d1
a	a	k8xC
pomlouvačný	pomlouvačný	k2eAgInSc1d1
pamflet	pamflet	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
poškodit	poškodit	k5eAaPmF
umělcovu	umělcův	k2eAgFnSc4d1
pověst	pověst	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Léta	léto	k1gNnPc1
mistrovská	mistrovský	k2eAgNnPc1d1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1718	#num#	k4
byl	být	k5eAaImAgInS
Vivaldi	Vivald	k1gMnPc1
po	po	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
kapelníkem	kapelník	k1gMnSc7
na	na	k7c6
dvoře	dvůr	k1gInSc6
Filipa	Filip	k1gMnSc2
Hesenského	hesenský	k2eAgMnSc2d1
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
Mantovského	mantovský	k2eAgMnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
komponoval	komponovat	k5eAaImAgMnS
opery	opera	k1gFnPc4
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátké	krátký	k2eAgFnSc6d1
zastávce	zastávka	k1gFnSc6
v	v	k7c6
Miláně	Milán	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
pastorální	pastorální	k2eAgNnSc4d1
drama	drama	k1gNnSc4
La	la	k1gNnSc2
Silvia	Silvia	k1gFnSc1
(	(	kIx(
<g/>
RV	RV	kA
734	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
oratorium	oratorium	k1gNnSc1
L	L	kA
<g/>
’	’	k?
<g/>
adorazione	adorazion	k1gInSc5
delli	dell	k1gMnPc7
tre	tre	k?
re	re	k9
magi	magi	k1gNnSc1
al	ala	k1gFnPc2
bambino	bambino	k1gNnSc1
Gesù	Gesù	k1gMnSc1
(	(	kIx(
<g/>
RV	RV	kA
645	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
novým	nový	k2eAgMnSc7d1
papežem	papež	k1gMnSc7
Benediktem	Benedikt	k1gMnSc7
XIII	XIII	kA
<g/>
.	.	kIx.
pozván	pozvat	k5eAaPmNgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
několik	několik	k4yIc4
oper	opera	k1gFnPc2
a	a	k8xC
koncertů	koncert	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1725	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
Benátek	Benátky	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
roce	rok	k1gInSc6
uvedl	uvést	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
opery	opera	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
zkomponoval	zkomponovat	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
dnes	dnes	k6eAd1
snad	snad	k9
nejpopulárnější	populární	k2eAgNnSc1d3
dílo	dílo	k1gNnSc1
Čtvero	čtvero	k4xRgNnSc4
ročních	roční	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
houslové	houslový	k2eAgInPc4d1
koncerty	koncert	k1gInPc4
zobrazující	zobrazující	k2eAgFnSc1d1
roční	roční	k2eAgNnSc4d1
období	období	k1gNnSc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspirací	inspirace	k1gFnPc2
bylo	být	k5eAaImAgNnS
patrně	patrně	k6eAd1
okolí	okolí	k1gNnSc1
Mantovy	Mantova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
dílo	dílo	k1gNnSc1
znamenalo	znamenat	k5eAaImAgNnS
doslova	doslova	k6eAd1
revoluci	revoluce	k1gFnSc4
v	v	k7c6
hudbě	hudba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vivaldimu	Vivaldim	k1gInSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
hudebně	hudebně	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
specifické	specifický	k2eAgInPc4d1
zvuky	zvuk	k1gInPc4
venkova	venkov	k1gInSc2
<g/>
:	:	kIx,
zpěv	zpěv	k1gInSc1
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
štěkání	štěkání	k1gNnSc4
psů	pes	k1gMnPc2
<g/>
,	,	kIx,
bzučení	bzučení	k1gNnSc1
much	moucha	k1gFnPc2
<g/>
,	,	kIx,
déšť	déšť	k1gInSc4
<g/>
,	,	kIx,
bouři	bouře	k1gFnSc4
<g/>
,	,	kIx,
výkřiky	výkřik	k1gInPc1
opilců	opilec	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ticho	ticho	k1gNnSc1
nad	nad	k7c7
zasněženou	zasněžený	k2eAgFnSc7d1
krajinou	krajina	k1gFnSc7
a	a	k8xC
praskání	praskání	k1gNnPc4
ohně	oheň	k1gInSc2
v	v	k7c6
krbu	krb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
každému	každý	k3xTgInSc3
koncertu	koncert	k1gInSc3
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
báseň	báseň	k1gFnSc4
popisující	popisující	k2eAgFnPc4d1
scény	scéna	k1gFnPc4
ilustrované	ilustrovaný	k2eAgFnPc4d1
hudbou	hudba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
publikovány	publikován	k2eAgInPc1d1
jako	jako	k8xC,k8xS
první	první	k4xOgInPc4
čtyři	čtyři	k4xCgInPc4
koncerty	koncert	k1gInPc4
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
dvanácti	dvanáct	k4xCc2
koncertů	koncert	k1gInPc2
nazvané	nazvaný	k2eAgFnSc2d1
Il	Il	k1gFnSc2
cimento	cimento	k1gNnSc1
dell	dell	k1gInSc1
<g/>
’	’	k?
<g/>
Armonia	Armonium	k1gNnSc2
e	e	k0
dell	dell	k1gInSc1
<g/>
’	’	k?
<g/>
Inventione	Invention	k1gInSc5
(	(	kIx(
<g/>
op	op	k1gMnSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vydány	vydat	k5eAaPmNgInP
v	v	k7c6
Amsterodamu	Amsterodam	k1gInSc6
roku	rok	k1gInSc2
1725	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílo	dílo	k1gNnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
hraběti	hrabě	k1gMnSc3
Václavovi	Václav	k1gMnSc3
z	z	k7c2
Morzinu	Morzin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgMnS
knězem	kněz	k1gMnSc7
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
náznaky	náznak	k1gInPc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
podlehl	podlehnout	k5eAaPmAgMnS
kouzlu	kouzlo	k1gNnSc3
benátské	benátský	k2eAgFnSc2d1
zpěvačky	zpěvačka	k1gFnSc2
Anny	Anna	k1gFnSc2
Giraudové	Giraudová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přinejmenším	přinejmenším	k6eAd1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
jeho	jeho	k3xOp3gFnPc4
hudba	hudba	k1gFnSc1
má	mít	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
romantický	romantický	k2eAgInSc1d1
nádech	nádech	k1gInSc1
a	a	k8xC
je	být	k5eAaImIp3nS
prokazatelné	prokazatelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tehdy	tehdy	k6eAd1
své	svůj	k3xOyFgFnPc4
árie	árie	k1gFnPc4
upravoval	upravovat	k5eAaImAgMnS
tak	tak	k9
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vynikly	vyniknout	k5eAaPmAgFnP
hlasové	hlasový	k2eAgFnPc4d1
přednosti	přednost	k1gFnPc4
zpěvačky	zpěvačka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
dostával	dostávat	k5eAaImAgInS
Vivaldi	Vivald	k1gMnPc1
objednávky	objednávka	k1gFnPc4
ze	z	k7c2
šlechtických	šlechtický	k2eAgInPc2d1
a	a	k8xC
královských	královský	k2eAgInPc2d1
dvorů	dvůr	k1gInPc2
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
svatební	svatební	k2eAgFnSc1d1
kantáta	kantáta	k1gFnSc1
Gloria	Gloria	k1gFnSc1
e	e	k0
Imeneo	Imeneo	k1gMnSc1
(	(	kIx(
<g/>
RV	RV	kA
687	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
psána	psát	k5eAaImNgFnS
ke	k	k7c3
svatbě	svatba	k1gFnSc3
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XV	XV	kA
<g/>
.	.	kIx.
a	a	k8xC
cyklus	cyklus	k1gInSc1
koncertů	koncert	k1gInPc2
La	la	k1gNnSc2
Cetra	Cetr	k1gInSc2
(	(	kIx(
<g/>
op	op	k1gMnSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
věnován	věnovat	k5eAaPmNgInS,k5eAaImNgInS
Karlovi	Karel	k1gMnSc3
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburskému	habsburský	k2eAgInSc3d1
<g/>
,	,	kIx,
císaři	císař	k1gMnSc3
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
jej	on	k3xPp3gMnSc4
vůbec	vůbec	k9
velmi	velmi	k6eAd1
obdivoval	obdivovat	k5eAaImAgMnS
<g/>
,	,	kIx,
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
ho	on	k3xPp3gInSc4
rytířem	rytíř	k1gMnSc7
a	a	k8xC
odměnil	odměnit	k5eAaPmAgInS
zlatou	zlatá	k1gFnSc4
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1730	#num#	k4
Vivaldi	Vivald	k1gMnPc1
na	na	k7c4
oplátku	oplátka	k1gFnSc4
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
a	a	k8xC
do	do	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
uvedl	uvést	k5eAaPmAgMnS
zde	zde	k6eAd1
svou	svůj	k3xOyFgFnSc4
operu	opera	k1gFnSc4
Farnace	Farnace	k1gFnSc2
(	(	kIx(
<g/>
RV	RV	kA
711	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
přednosti	přednost	k1gFnPc4
Vivaldiho	Vivaldi	k1gMnSc2
patřilo	patřit	k5eAaImAgNnS
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgInS
smysl	smysl	k1gInSc4
i	i	k9
pro	pro	k7c4
literární	literární	k2eAgFnPc4d1
kvality	kvalita	k1gFnPc4
libret	libreto	k1gNnPc2
a	a	k8xC
z	z	k7c2
nadprodukce	nadprodukce	k1gFnSc2
plytkých	plytký	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
dodávaných	dodávaný	k2eAgInPc2d1
libretisty-řemeslníky	libretisty-řemeslník	k1gInPc4
si	se	k3xPyFc3
dovedl	dovést	k5eAaPmAgMnS
vybrat	vybrat	k5eAaPmF
kvalitu	kvalita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
poslední	poslední	k2eAgFnSc2d1
opery	opera	k1gFnSc2
zdobí	zdobit	k5eAaImIp3nS
jména	jméno	k1gNnPc4
vrcholných	vrcholný	k2eAgMnPc2d1
básníků	básník	k1gMnPc2
a	a	k8xC
dramatiků	dramatik	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
:	:	kIx,
Pietra	Pietr	k1gMnSc2
Mestastasia	Mestastasius	k1gMnSc2
a	a	k8xC
Carla	Carl	k1gMnSc2
Goldoniho	Goldoni	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Ústup	ústup	k1gInSc1
ze	z	k7c2
slávy	sláva	k1gFnSc2
a	a	k8xC
smrt	smrt	k1gFnSc4
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
v	v	k7c6
místě	místo	k1gNnSc6
úmrtí	úmrtí	k1gNnSc2
Antonia	Antonio	k1gMnSc2
Vivaldiho	Vivaldi	k1gMnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
Jako	jako	k9
většina	většina	k1gFnSc1
skladatelů	skladatel	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
sklonku	sklonek	k1gInSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
dostal	dostat	k5eAaPmAgMnS
Vivaldi	Vivald	k1gMnPc1
do	do	k7c2
finančních	finanční	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měnící	měnící	k2eAgMnSc1d1
se	se	k3xPyFc4
vkus	vkus	k1gInSc1
publika	publikum	k1gNnSc2
začal	začít	k5eAaPmAgInS
považovat	považovat	k5eAaImF
Vivaldiho	Vivaldi	k1gMnSc4
za	za	k7c4
staromódního	staromódní	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vivaldi	Vivald	k1gMnPc1
rozprodal	rozprodat	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
partitury	partitura	k1gFnPc4
za	za	k7c4
minimální	minimální	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
a	a	k8xC
přesunul	přesunout	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
doufal	doufat	k5eAaImAgMnS
ve	v	k7c4
větší	veliký	k2eAgNnSc4d2
uznání	uznání	k1gNnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc7
velkým	velký	k2eAgMnSc7d1
příznivcem	příznivec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečně	skutečně	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
císařovým	císařův	k2eAgMnSc7d1
dvorním	dvorní	k2eAgMnSc7d1
skladatelem	skladatel	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
krátce	krátce	k6eAd1
po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
císař	císař	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
rána	ráno	k1gNnSc2
osudu	osud	k1gInSc6
zanechala	zanechat	k5eAaPmAgFnS
skladatele	skladatel	k1gMnSc4
bez	bez	k7c2
ochránce	ochránce	k1gMnSc2
a	a	k8xC
bez	bez	k7c2
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodával	prodávat	k5eAaImAgInS
další	další	k2eAgInPc4d1
rukopisy	rukopis	k1gInPc4
a	a	k8xC
zanedlouho	zanedlouho	k6eAd1
po	po	k7c6
císaři	císař	k1gMnSc6
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1741	#num#	k4
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pochován	pochován	k2eAgMnSc1d1
v	v	k7c6
prostém	prostý	k2eAgInSc6d1
hrobě	hrob	k1gInSc6
na	na	k7c6
nemocničním	nemocniční	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
uváděná	uváděný	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
mladičký	mladičký	k2eAgMnSc1d1
Josef	Josef	k1gMnSc1
Haydn	Haydn	k1gMnSc1
zkomponoval	zkomponovat	k5eAaPmAgMnS
pro	pro	k7c4
Vivaldiho	Vivaldi	k1gMnSc4
pohřební	pohřební	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
být	být	k5eAaImF
mylná	mylný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
téměř	téměř	k6eAd1
200	#num#	k4
let	léto	k1gNnPc2
upadla	upadnout	k5eAaPmAgFnS
hudba	hudba	k1gFnSc1
Antonia	Antonio	k1gMnSc2
Vivaldiho	Vivaldi	k1gMnSc2
v	v	k7c6
zapomnění	zapomnění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Hrob	hrob	k1gInSc1
Antonia	Antonio	k1gMnSc2
Vivaldiho	Vivaldi	k1gMnSc2
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
přenesen	přenést	k5eAaPmNgInS
ke	k	k7c3
chrámu	chrám	k1gInSc3
Karlskirche	Karlskirch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
místě	místo	k1gNnSc6
posledního	poslední	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
skladatele	skladatel	k1gMnSc2
stojí	stát	k5eAaImIp3nS
dnes	dnes	k6eAd1
hotel	hotel	k1gInSc4
Sacher	Sachra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
a	a	k8xC
Čechy	Čechy	k1gFnPc1
</s>
<s>
Vivaldiho	Vivaldize	k6eAd1
hudba	hudba	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Čechách	Čechy	k1gFnPc6
už	už	k6eAd1
za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
nesmírně	smírně	k6eNd1
populární	populární	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benátský	benátský	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
impresário	impresário	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Denzio	Denzio	k1gMnSc1
do	do	k7c2
Prahy	Praha	k1gFnSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
1726	#num#	k4
až	až	k9
1736	#num#	k4
přivezl	přivézt	k5eAaPmAgInS
šest	šest	k4xCc1
Vivaldiho	Vivaldiha	k1gFnSc5
operních	operní	k2eAgFnPc2d1
představení	představení	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
dvě	dva	k4xCgFnPc1
premiéry	premiéra	k1gFnPc1
byly	být	k5eAaImAgFnP
zkomponovány	zkomponován	k2eAgFnPc1d1
přímo	přímo	k6eAd1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
<g/>
:	:	kIx,
Argippo	Argippa	k1gFnSc5
(	(	kIx(
<g/>
RV	RV	kA
697	#num#	k4
<g/>
)	)	kIx)
uvedena	uvést	k5eAaPmNgFnS
na	na	k7c4
podzim	podzim	k1gInSc4
1730	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
Pasticcio	Pasticcio	k1gMnSc1
Alvilda	Alvilda	k1gMnSc1
<g/>
,	,	kIx,
Regina	Regina	k1gFnSc1
dei	dei	k?
Goti	Goti	k1gNnSc1
<g/>
,	,	kIx,
nastudovaná	nastudovaný	k2eAgFnSc1d1
na	na	k7c6
jaře	jaro	k1gNnSc6
1731	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
představení	představení	k1gNnSc4
zřejmě	zřejmě	k6eAd1
řídil	řídit	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
autor	autor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opery	opera	k1gFnPc1
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgFnP
ve	v	k7c6
šlechtickém	šlechtický	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
hraběte	hrabě	k1gMnSc2
Františka	František	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Šporka	Šporek	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
paláci	palác	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
Na	na	k7c6
Poříčí	Poříčí	k1gNnSc6
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
dvě	dva	k4xCgFnPc1
premiéry	premiéra	k1gFnPc1
(	(	kIx(
<g/>
Argippo	Argippa	k1gFnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
už	už	k6eAd1
Vivaldi	Vivald	k1gMnPc1
evropsky	evropsky	k6eAd1
proslulým	proslulý	k2eAgMnSc7d1
skladatelem	skladatel	k1gMnSc7
a	a	k8xC
cestoval	cestovat	k5eAaImAgMnS
zvláště	zvláště	k6eAd1
na	na	k7c4
nová	nový	k2eAgNnPc4d1
uvedení	uvedení	k1gNnSc3
svých	svůj	k3xOyFgNnPc2
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
žil	žít	k5eAaImAgMnS
zřejmě	zřejmě	k6eAd1
tehdy	tehdy	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opery	opera	k1gFnSc2
uvedl	uvést	k5eAaPmAgMnS
na	na	k7c4
českou	český	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
impresário	impresário	k1gMnSc1
Šporkova	Šporkův	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
Antonio	Antonio	k1gMnSc1
Denzio	Denzio	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
i	i	k9
v	v	k7c6
Itálii	Itálie	k1gFnSc6
znám	znát	k5eAaImIp1nS
jako	jako	k9
libretista	libretista	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
vedl	vést	k5eAaImAgMnS
operní	operní	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
Vivaldi	Vivald	k1gMnPc1
najímal	najímat	k5eAaImAgMnS
zpěváky	zpěvák	k1gMnPc4
a	a	k8xC
obstarával	obstarávat	k5eAaImAgMnS
operní	operní	k2eAgFnPc4d1
novinky	novinka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sbírky	sbírka	k1gFnSc2
koncertů	koncert	k1gInPc2
Il	Il	k1gFnSc2
cimento	cimento	k1gNnSc1
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
armonia	armonium	k1gNnSc2
e	e	k0
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
inventione	invention	k1gInSc5
s	s	k7c7
věnováním	věnování	k1gNnSc7
hraběti	hrabě	k1gMnSc6
Václavovi	Václav	k1gMnSc6
z	z	k7c2
Morzinu	Morzin	k1gInSc2
</s>
<s>
V	v	k7c6
českých	český	k2eAgInPc6d1
archivech	archiv	k1gInPc6
je	být	k5eAaImIp3nS
dochováno	dochován	k2eAgNnSc1d1
mnoho	mnoho	k4c1
unikátních	unikátní	k2eAgInPc2d1
zápisů	zápis	k1gInPc2
jeho	jeho	k3xOp3gInPc2
děl	dít	k5eAaImAgMnS,k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
byly	být	k5eAaImAgInP
také	také	k6eAd1
napsány	napsat	k5eAaPmNgInP,k5eAaBmNgInP
přímo	přímo	k6eAd1
na	na	k7c4
objednávku	objednávka	k1gFnSc4
českých	český	k2eAgMnPc2d1
šlechticů	šlechtic	k1gMnPc2
<g/>
:	:	kIx,
např.	např.	kA
loutnové	loutnový	k2eAgFnPc4d1
skladby	skladba	k1gFnPc4
pro	pro	k7c4
hraběte	hrabě	k1gMnSc4
z	z	k7c2
Vrtby	vrtba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraběti	hrabě	k1gMnSc6
Václavovi	Václav	k1gMnSc6
z	z	k7c2
Morzinu	Morzin	k1gInSc2
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
dedikováno	dedikován	k2eAgNnSc1d1
Vivaldiho	Vivaldi	k1gMnSc2
nejslavnější	slavný	k2eAgNnSc4d3
dílo	dílo	k1gNnSc4
Il	Il	k1gFnSc2
cimento	cimento	k1gNnSc1
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
armonia	armonium	k1gNnSc2
e	e	k0
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
inventione	invention	k1gInSc5
(	(	kIx(
<g/>
Souboj	souboj	k1gInSc1
harmonie	harmonie	k1gFnSc2
s	s	k7c7
invencí	invence	k1gFnSc7
–	–	k?
op	op	k1gMnSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
slavné	slavný	k2eAgInPc4d1
koncerty	koncert	k1gInPc4
Čtvero	čtvero	k4xRgNnSc1
ročních	roční	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
sám	sám	k3xTgMnSc1
řídil	řídit	k5eAaImAgMnS
Morzinovu	Morzinův	k2eAgFnSc4d1
kapelu	kapela	k1gFnSc4
při	při	k7c6
koncertech	koncert	k1gInPc6
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palác	palác	k1gInSc1
rodu	rod	k1gInSc2
Morzinů	Morzin	k1gInPc2
stojí	stát	k5eAaImIp3nS
v	v	k7c6
Nerudově	Nerudův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
č.	č.	k?
256	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
poblíž	poblíž	k6eAd1
horní	horní	k2eAgFnPc4d1
části	část	k1gFnPc4
Malostranského	malostranský	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
měl	mít	k5eAaImAgInS
Vivaldi	Vivald	k1gMnPc1
i	i	k9
na	na	k7c4
tvorbu	tvorba	k1gFnSc4
českých	český	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
na	na	k7c4
Antonína	Antonín	k1gMnSc4
Reichenauera	Reichenauer	k1gMnSc4
či	či	k8xC
Bohuslava	Bohuslav	k1gMnSc4
Matěje	Matěj	k1gMnSc4
Černohorského	Černohorský	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
a	a	k8xC
vliv	vliv	k1gInSc1
</s>
<s>
Většina	většina	k1gFnSc1
Vivaldiho	Vivaldi	k1gMnSc2
děl	dělo	k1gNnPc2
byla	být	k5eAaImAgFnS
znovuobjevena	znovuobjevit	k5eAaPmNgFnS
až	až	k9
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
Turíně	Turín	k1gInSc6
a	a	k8xC
v	v	k7c6
Janově	Janov	k1gInSc6
a	a	k8xC
byla	být	k5eAaImAgFnS
publikována	publikovat	k5eAaBmNgFnS
v	v	k7c6
průběhu	průběh	k1gInSc6
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vivaldiho	Vivaldi	k1gMnSc2
hudba	hudba	k1gFnSc1
je	být	k5eAaImIp3nS
formálně	formálně	k6eAd1
pevně	pevně	k6eAd1
zakotvena	zakotvit	k5eAaPmNgFnS
v	v	k7c6
barokní	barokní	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
velmi	velmi	k6eAd1
vynalézavá	vynalézavý	k2eAgFnSc1d1
v	v	k7c6
harmonických	harmonický	k2eAgInPc6d1
kontrastech	kontrast	k1gInPc6
<g/>
,	,	kIx,
strhujících	strhující	k2eAgInPc6d1
rytmech	rytmus	k1gInPc6
a	a	k8xC
neopakovatelné	opakovatelný	k2eNgFnSc3d1
melodice	melodika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadto	nadto	k6eAd1
se	se	k3xPyFc4
Vivaldi	Vivald	k1gMnPc1
nesnažil	snažit	k5eNaImAgMnS
jen	jen	k6eAd1
vyhovět	vyhovět	k5eAaPmF
akademickým	akademický	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
doby	doba	k1gFnSc2
a	a	k8xC
skládat	skládat	k5eAaImF
pro	pro	k7c4
hrstku	hrstka	k1gFnSc4
intelektuálů	intelektuál	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
svou	svůj	k3xOyFgFnSc4
hudbu	hudba	k1gFnSc4
komponoval	komponovat	k5eAaImAgMnS
cílevědomě	cílevědomě	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
blízká	blízký	k2eAgFnSc1d1
nejširším	široký	k2eAgFnPc3d3
vrstvám	vrstva	k1gFnPc3
posluchačů	posluchač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
hudby	hudba	k1gFnSc2
čiší	čišet	k5eAaImIp3nS
potěšení	potěšení	k1gNnSc4
z	z	k7c2
komponování	komponování	k1gNnSc2
a	a	k8xC
radost	radost	k1gFnSc4
ze	z	k7c2
života	život	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
přenáší	přenášet	k5eAaImIp3nS
na	na	k7c4
posluchače	posluchač	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgNnSc6
tkví	tkvět	k5eAaImIp3nS
i	i	k9
dnešní	dnešní	k2eAgFnSc1d1
popularita	popularita	k1gFnSc1
Vivaldiho	Vivaldi	k1gMnSc4
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vivaldi	Vivald	k1gMnPc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
tvůrců	tvůrce	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
stáli	stát	k5eAaImAgMnP
u	u	k7c2
přerodu	přerod	k1gInSc2
barokní	barokní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
v	v	k7c4
klasický	klasický	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
hluboce	hluboko	k6eAd1
byl	být	k5eAaImAgInS
Vivaldiho	Vivaldi	k1gMnSc4
koncerty	koncert	k1gInPc7
a	a	k8xC
áriemi	árie	k1gFnPc7
ovlivněn	ovlivněn	k2eAgInSc4d1
Johann	Johann	k1gInSc4
Sebastian	Sebastian	k1gMnSc1
Bach	Bach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřetelné	zřetelný	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
nacházíme	nacházet	k5eAaImIp1nP
v	v	k7c6
Janových	Janových	k2eAgFnPc6d1
pašijích	pašije	k1gFnPc6
<g/>
,	,	kIx,
Matoušových	Matoušových	k2eAgFnPc6d1
pašijích	pašije	k1gFnPc6
a	a	k8xC
četných	četný	k2eAgFnPc6d1
kantátách	kantáta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bach	Bach	k1gMnSc1
upravil	upravit	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
Vivaldiho	Vivaldi	k1gMnSc2
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
pro	pro	k7c4
cembalo	cembalo	k1gNnSc4
a	a	k8xC
orchestr	orchestr	k1gInSc4
včetně	včetně	k7c2
nejznámějšího	známý	k2eAgInSc2d3
Koncertu	koncert	k1gInSc2
pro	pro	k7c4
čtyři	čtyři	k4xCgFnPc4
housle	housle	k1gFnPc4
a	a	k8xC
violoncello	violoncello	k1gNnSc4
(	(	kIx(
<g/>
RV	RV	kA
580	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
poznamenat	poznamenat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
době	doba	k1gFnSc6
autorství	autorství	k1gNnSc2
nebylo	být	k5eNaImAgNnS
chápáno	chápat	k5eAaImNgNnS
tak	tak	k6eAd1
jako	jako	k8xC,k8xS
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílo	dílo	k1gNnSc1
se	s	k7c7
prakticky	prakticky	k6eAd1
ihned	ihned	k6eAd1
po	po	k7c6
prvním	první	k4xOgNnSc6
provedení	provedení	k1gNnSc6
stávalo	stávat	k5eAaImAgNnS
veřejným	veřejný	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
a	a	k8xC
často	často	k6eAd1
bývalo	bývat	k5eAaImAgNnS
hráno	hrát	k5eAaImNgNnS
v	v	k7c6
nejroztodivnějších	roztodivný	k2eAgNnPc6d3
nástrojových	nástrojový	k2eAgNnPc6d1
obsazeních	obsazení	k1gNnPc6
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
měl	mít	k5eAaImAgMnS
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
)	)	kIx)
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
původní	původní	k2eAgInSc4d1
záměr	záměr	k1gInSc4
autora	autor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Citování	citování	k1gNnSc3
skladby	skladba	k1gFnSc2
jiného	jiný	k2eAgMnSc2d1
autora	autor	k1gMnSc2
bylo	být	k5eAaImAgNnS
naopak	naopak	k6eAd1
chápáno	chápat	k5eAaImNgNnS
dokonce	dokonce	k9
jako	jako	k9
projev	projev	k1gInSc4
úcty	úcta	k1gFnSc2
skladatele	skladatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
renesanci	renesance	k1gFnSc6
Vivaldiho	Vivaldi	k1gMnSc2
díla	dílo	k1gNnSc2
má	mít	k5eAaImIp3nS
významnou	významný	k2eAgFnSc4d1
zásluhu	zásluha	k1gFnSc4
proslulý	proslulý	k2eAgMnSc1d1
houslista	houslista	k1gMnSc1
Fritz	Fritz	k1gMnSc1
Kreisler	Kreisler	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
slavil	slavit	k5eAaImAgInS
úspěchy	úspěch	k1gInPc4
s	s	k7c7
Vivaldiho	Vivaldiha	k1gFnSc5
houslovým	houslový	k2eAgInSc7d1
koncertem	koncert	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ironií	ironie	k1gFnPc2
osudu	osud	k1gInSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
tento	tento	k3xDgInSc1
koncert	koncert	k1gInSc1
nebyl	být	k5eNaImAgInS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
Vivaldiho	Vivaldi	k1gMnSc2
dílem	dílem	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
šlo	jít	k5eAaImAgNnS
o	o	k7c4
dobový	dobový	k2eAgInSc4d1
plagiát	plagiát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
jeho	jeho	k3xOp3gNnSc1
provedení	provedení	k1gNnSc1
probudilo	probudit	k5eAaPmAgNnS
zájem	zájem	k1gInSc4
o	o	k7c4
další	další	k1gNnSc4
Vivaldiho	Vivaldi	k1gMnSc2
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynikající	vynikající	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c6
tomto	tento	k3xDgNnSc6
poli	pole	k1gNnSc6
odvedl	odvést	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
Marc	Marc	k1gFnSc1
Pincherle	Pincherl	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
započal	započnout	k5eAaPmAgInS
se	s	k7c7
systematickým	systematický	k2eAgMnSc7d1
zpracování	zpracování	k1gNnSc2
Vivaldiho	Vivaldi	k1gMnSc4
pozůstalosti	pozůstalost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfredo	Alfredo	k1gNnSc1
Casella	Casella	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
zorganizoval	zorganizovat	k5eAaPmAgMnS
historický	historický	k2eAgMnSc1d1
Vivaldiho	Vivaldi	k1gMnSc4
týden	týden	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
zazněla	zaznět	k5eAaImAgFnS,k5eAaPmAgFnS
reprezentativní	reprezentativní	k2eAgFnSc1d1
přehlídka	přehlídka	k1gFnSc1
objevených	objevený	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
založen	založit	k5eAaPmNgInS
Vivaldiho	Vivaldi	k1gMnSc2
ústav	ústava	k1gFnPc2
(	(	kIx(
<g/>
Istituto	Istitut	k2eAgNnSc1d1
Italiano	Italiana	k1gFnSc5
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
uměleckým	umělecký	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
skladatel	skladatel	k1gMnSc1
Gian	Gian	k1gMnSc1
Francesco	Francesco	k1gMnSc1
Malipiero	Malipiero	k1gNnSc4
a	a	k8xC
hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
objevování	objevování	k1gNnSc2
<g/>
,	,	kIx,
publikování	publikování	k1gNnSc3
a	a	k8xC
provádění	provádění	k1gNnSc3
Vivaldiho	Vivaldi	k1gMnSc2
děl	dít	k5eAaImAgMnS,k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Vlna	vlna	k1gFnSc1
zájmu	zájem	k1gInSc2
o	o	k7c4
skladatele	skladatel	k1gMnPc4
postihla	postihnout	k5eAaPmAgFnS
i	i	k8xC
kinematografii	kinematografie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
byl	být	k5eAaImAgInS
do	do	k7c2
kin	kino	k1gNnPc2
uveden	uvést	k5eAaPmNgInS
italsko-francouzský	italsko-francouzský	k2eAgInSc1d1
film	film	k1gInSc1
Vivaldi	Vivald	k1gMnPc1
–	–	k?
princ	princ	k1gMnSc1
benátský	benátský	k2eAgMnSc1d1
režiséra	režisér	k1gMnSc4
Jeana-Louise	Jeana-Louise	k1gFnSc2
Guillerma	Guillerma	k1gFnSc1
se	s	k7c7
Stefanem	Stefan	k1gMnSc7
Dionisim	Dionisima	k1gFnPc2
v	v	k7c6
titulní	titulní	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
motivy	motiv	k1gInPc4
Vivaldiho	Vivaldi	k1gMnSc2
života	život	k1gInSc2
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
údajně	údajně	k6eAd1
další	další	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
má	mít	k5eAaImIp3nS
ztvárnit	ztvárnit	k5eAaPmF
Joseph	Joseph	k1gInSc4
Fiennes	Fiennesa	k1gFnPc2
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1
rolích	role	k1gFnPc6
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
objevit	objevit	k5eAaPmF
Malcolm	Malcolm	k1gInSc4
McDowell	McDowella	k1gFnPc2
<g/>
,	,	kIx,
Jacqueline	Jacquelin	k1gInSc5
Bisset	Bisset	k1gMnSc1
či	či	k8xC
Gérard	Gérard	k1gMnSc1
Depardieu	Depardieus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Chiesa	Chiesa	k1gFnSc1
della	delnout	k5eAaPmAgFnS,k5eAaImAgFnS,k5eAaBmAgFnS
Pietà	Pietà	k1gFnSc4
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
Antonia	Antonio	k1gMnSc4
Vivaldiho	Vivaldi	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc4
souhrnný	souhrnný	k2eAgInSc4d1
katalog	katalog	k1gInSc4
děl	dělo	k1gNnPc2
Antonia	Antonio	k1gMnSc2
Vivaldiho	Vivaldi	k1gMnSc2
vydal	vydat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
dánský	dánský	k2eAgMnSc1d1
muzikolog	muzikolog	k1gMnSc1
Peter	Peter	k1gMnSc1
Ryom	Ryom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
Köchelův	Köchelův	k2eAgInSc1d1
seznam	seznam	k1gInSc1
děl	dít	k5eAaImAgInS,k5eAaBmAgInS
Mozartových	Mozartových	k2eAgInSc1d1
přešel	přejít	k5eAaPmAgInS
Ryomův	Ryomův	k2eAgInSc1d1
seznam	seznam	k1gInSc1
do	do	k7c2
obecného	obecný	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
skladby	skladba	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
RV	RV	kA
325	#num#	k4
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
325	#num#	k4
<g/>
.	.	kIx.
položku	položka	k1gFnSc4
v	v	k7c6
Ryomově	Ryomův	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opusovými	opusový	k2eAgFnPc7d1
čísly	číslo	k1gNnPc7
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
pouze	pouze	k6eAd1
skladby	skladba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
vyšly	vyjít	k5eAaPmAgFnP
tiskem	tisk	k1gInSc7
za	za	k7c4
Vivaldiho	Vivaldi	k1gMnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
790	#num#	k4
položek	položka	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
autorství	autorství	k1gNnSc1
doloženo	doložen	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
dalších	další	k2eAgFnPc2d1
63	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
prozatím	prozatím	k6eAd1
pochybné	pochybný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
čísla	číslo	k1gNnSc2
RV	RV	kA
1	#num#	k4
až	až	k8xS
RV	RV	kA
585	#num#	k4
jsou	být	k5eAaImIp3nP
skladby	skladba	k1gFnPc1
instrumentální	instrumentální	k2eAgFnSc1d1
<g/>
,	,	kIx,
RV	RV	kA
586	#num#	k4
až	až	k8xS
RV	RV	kA
642	#num#	k4
skladby	skladba	k1gFnPc1
vokální	vokální	k2eAgFnSc1d1
náboženského	náboženský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
a	a	k8xC
RV	RV	kA
643	#num#	k4
–	–	k?
RV	RV	kA
740	#num#	k4
skladby	skladba	k1gFnPc4
vokální	vokální	k2eAgFnSc2d1
světské	světský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položky	položka	k1gFnSc2
RV	RV	kA
741	#num#	k4
až	až	k8xS
RV	RV	kA
790	#num#	k4
představují	představovat	k5eAaImIp3nP
dodatky	dodatek	k1gInPc1
<g/>
,	,	kIx,
tj.	tj.	kA
díla	dílo	k1gNnSc2
objevená	objevený	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
od	od	k7c2
prvního	první	k4xOgNnSc2
vydání	vydání	k1gNnSc2
katalogu	katalog	k1gInSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
průzkum	průzkum	k1gInSc1
hudebních	hudební	k2eAgInPc2d1
archivů	archiv	k1gInPc2
dosud	dosud	k6eAd1
neskončil	skončit	k5eNaPmAgInS
a	a	k8xC
že	že	k8xS
se	se	k3xPyFc4
dočkáme	dočkat	k5eAaPmIp1nP
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
i	i	k8xC
číslo	číslo	k1gNnSc1
790	#num#	k4
bude	být	k5eAaImBp3nS
vbrzku	vbrzku	k6eAd1
vysoce	vysoce	k6eAd1
překročeno	překročit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
jiného	jiný	k2eAgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
katalog	katalog	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
instrumentálních	instrumentální	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
pro	pro	k7c4
nejrůznější	různý	k2eAgInPc4d3
nástroje	nástroj	k1gInPc4
(	(	kIx(
<g/>
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
violoncello	violoncello	k1gNnSc4
<g/>
,	,	kIx,
lesní	lesní	k2eAgInSc4d1
roh	roh	k1gInSc4
<g/>
,	,	kIx,
hoboj	hoboj	k1gFnSc1
<g/>
,	,	kIx,
flétna	flétna	k1gFnSc1
<g/>
,	,	kIx,
viola	viola	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
amore	amor	k1gMnSc5
<g/>
,	,	kIx,
cembalo	cembalo	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
loutna	loutna	k1gFnSc1
a	a	k8xC
mandolína	mandolína	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
cca	cca	kA
230	#num#	k4
koncertů	koncert	k1gInPc2
pro	pro	k7c4
housle	housle	k1gFnPc4
<g/>
;	;	kIx,
</s>
<s>
50	#num#	k4
oper	opera	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
86	#num#	k4
sonát	sonáta	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
řadu	řada	k1gFnSc4
symfonií	symfonie	k1gFnPc2
(	(	kIx(
<g/>
i	i	k9
když	když	k8xS
tady	tady	k6eAd1
je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
rozlišení	rozlišení	k1gNnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
Vivaldi	Vivald	k1gMnPc1
četná	četný	k2eAgNnPc4d1
svá	svůj	k3xOyFgNnPc4
koncerta	koncert	k1gMnSc4
označoval	označovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
symfonie	symfonie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
na	na	k7c4
60	#num#	k4
mší	mše	k1gFnPc2
<g/>
,	,	kIx,
oratorií	oratorium	k1gNnPc2
a	a	k8xC
příležitostných	příležitostný	k2eAgInPc2d1
hymnů	hymnus	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
na	na	k7c4
50	#num#	k4
kantát	kantáta	k1gFnPc2
<g/>
,	,	kIx,
koncertních	koncertní	k2eAgFnPc2d1
árií	árie	k1gFnPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
vokálních	vokální	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
stále	stále	k6eAd1
se	se	k3xPyFc4
rozrůstající	rozrůstající	k2eAgInSc1d1
počet	počet	k1gInSc1
drobných	drobný	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
komorního	komorní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Díla	dílo	k1gNnPc1
vyšlá	vyšlý	k2eAgNnPc1d1
tiskem	tisk	k1gInSc7
za	za	k7c4
Vivaldiho	Vivaldi	k1gMnSc4
života	život	k1gInSc2
</s>
<s>
Opus	opus	k1gInSc1
1	#num#	k4
–	–	k?
Raccolta	Raccolta	k1gMnSc1
–	–	k?
Dvanáct	dvanáct	k4xCc1
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
dvoje	dvoje	k4xRgFnPc4
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
2	#num#	k4
–	–	k?
12	#num#	k4
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1708	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
3	#num#	k4
–	–	k?
L	L	kA
<g/>
’	’	k?
<g/>
estro	estro	k6eAd1
Armonico	Armonico	k1gNnSc1
–	–	k?
Dvanáct	dvanáct	k4xCc4
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
kombinace	kombinace	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Opus	opus	k1gInSc1
4	#num#	k4
–	–	k?
La	la	k1gNnSc2
stravaganza	stravaganz	k1gMnSc2
–	–	k?
Dvanáct	dvanáct	k4xCc1
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
5	#num#	k4
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
Opusu	opus	k1gInSc2
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Čtyři	čtyři	k4xCgFnPc1
sonáty	sonáta	k1gFnPc1
pro	pro	k7c4
housle	housle	k1gFnPc4
a	a	k8xC
dvě	dva	k4xCgFnPc4
sonáty	sonáta	k1gFnPc4
pro	pro	k7c4
dvoje	dvoje	k4xRgFnPc4
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1716	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Opus	opus	k1gInSc1
6	#num#	k4
–	–	k?
Šest	šest	k4xCc1
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
(	(	kIx(
<g/>
1716	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
7	#num#	k4
–	–	k?
Dva	dva	k4xCgInPc4
koncerty	koncert	k1gInPc4
pro	pro	k7c4
hoboj	hoboj	k1gFnSc4
a	a	k8xC
deset	deset	k4xCc4
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
(	(	kIx(
<g/>
1716	#num#	k4
<g/>
–	–	k?
<g/>
1721	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
8	#num#	k4
–	–	k?
Il	Il	k1gMnSc1
cimento	cimento	k1gNnSc1
dell	dell	k1gMnSc1
<g/>
’	’	k?
<g/>
armonia	armonium	k1gNnSc2
e	e	k0
dell	dell	k1gInSc1
<g/>
’	’	k?
<g/>
inventione	invention	k1gInSc5
(	(	kIx(
<g/>
Souboj	souboj	k1gInSc1
harmonie	harmonie	k1gFnSc2
s	s	k7c7
invencí	invence	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dvanáct	dvanáct	k4xCc1
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Opus	opus	k1gInSc1
9	#num#	k4
–	–	k?
La	la	k1gNnSc1
cetra	cetra	k1gFnSc1
(	(	kIx(
<g/>
Lyra	lyra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dvanáct	dvanáct	k4xCc1
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
<g/>
.	.	kIx.
a	a	k8xC
jeden	jeden	k4xCgMnSc1
pro	pro	k7c4
dvoje	dvoje	k4xRgFnPc4
housle	housle	k1gFnPc4
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
10	#num#	k4
–	–	k?
Šest	šest	k4xCc1
flétnových	flétnový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
(	(	kIx(
<g/>
1728	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
11	#num#	k4
–	–	k?
Šest	šest	k4xCc1
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
a	a	k8xC
jeden	jeden	k4xCgInSc1
hobojový	hobojový	k2eAgInSc1d1
(	(	kIx(
<g/>
druhý	druhý	k4xOgInSc1
z	z	k7c2
těchto	tento	k3xDgInPc2
koncertů	koncert	k1gInPc2
e-moll	e-molla	k1gFnPc2
(	(	kIx(
<g/>
RV	RV	kA
277	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Il	Il	k1gMnSc1
favorito	favorita	k1gFnSc5
(	(	kIx(
<g/>
1729	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
12	#num#	k4
–	–	k?
Pět	pět	k4xCc1
houslových	houslový	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
bez	bez	k7c2
sóla	sólo	k1gNnSc2
(	(	kIx(
<g/>
1729	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opus	opus	k1gInSc1
13	#num#	k4
–	–	k?
Il	Il	k1gMnSc1
pastor	pastor	k1gMnSc1
fido	fido	k1gMnSc1
–	–	k?
Šest	šest	k4xCc1
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
musette	musette	k5eAaPmIp2nP
<g/>
,	,	kIx,
violu	viola	k1gFnSc4
<g/>
,	,	kIx,
cembalo	cembalo	k1gNnSc4
<g/>
,	,	kIx,
flétnu	flétna	k1gFnSc4
<g/>
,	,	kIx,
hoboj	hoboj	k1gFnSc4
<g/>
,	,	kIx,
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1737	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bez	bez	k7c2
čísla	číslo	k1gNnSc2
–	–	k?
Šest	šest	k4xCc1
sonát	sonáta	k1gFnPc2
pro	pro	k7c4
violoncello	violoncello	k1gNnSc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k1gMnSc1
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Opery	opera	k1gFnPc1
</s>
<s>
Ottone	Otton	k1gMnSc5
in	in	k?
Villa	Villo	k1gNnPc1
(	(	kIx(
<g/>
1713	#num#	k4
Vicenza	Vicenza	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Orlando	Orlanda	k1gFnSc5
finto	finta	k1gFnSc5
pazzo	pazza	k1gFnSc5
(	(	kIx(
<g/>
1714	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Nerone	Nero	k1gMnSc5
fatto	fatta	k1gMnSc5
Cesare	Cesar	k1gMnSc5
(	(	kIx(
<g/>
1715	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
costanza	costanz	k1gMnSc2
trionfante	trionfant	k1gMnSc5
degl	degnout	k5eAaPmAgInS
<g/>
’	’	k?
<g/>
amori	amori	k6eAd1
e	e	k0
de	de	k?
gl	gl	k?
<g/>
’	’	k?
<g/>
odii	odi	k1gFnSc2
(	(	kIx(
<g/>
1716	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Arsilda	Arsilda	k1gMnSc1
<g/>
,	,	kIx,
regina	regina	k1gMnSc1
di	di	k?
Ponto	Ponto	k1gNnSc4
(	(	kIx(
<g/>
1716	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
incoronazione	incoronazion	k1gInSc5
di	di	k?
Dario	Daria	k1gFnSc5
(	(	kIx(
<g/>
1717	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Tieteberga	Tieteberga	k1gFnSc1
(	(	kIx(
<g/>
1717	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Scanderbeg	Scanderbeg	k1gInSc1
(	(	kIx(
<g/>
1718	#num#	k4
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Armida	Armida	k1gFnSc1
al	ala	k1gFnPc2
campo	campa	k1gFnSc5
d	d	k?
<g/>
’	’	k?
<g/>
Egitto	Egitto	k1gNnSc4
(	(	kIx(
<g/>
1718	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Teuzzone	Teuzzon	k1gMnSc5
(	(	kIx(
<g/>
1719	#num#	k4
Mantova	Mantova	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Die	Die	k?
über	über	k1gInSc1
Hass	Hass	k1gInSc1
und	und	k?
Liebe	Lieb	k1gMnSc5
siegende	siegend	k1gMnSc5
Beständigkeit	Beständigkeit	k1gMnSc1
(	(	kIx(
<g/>
1719	#num#	k4
Hamburg	Hamburg	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tito	tento	k3xDgMnPc1
Manlio	Manlio	k6eAd1
(	(	kIx(
<g/>
1720	#num#	k4
Mantova	Mantova	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
Candace	Candace	k1gFnSc2
o	o	k7c4
siano	siano	k1gNnSc4
Li	li	k8xS
veri	vere	k1gFnSc4
amici	amice	k1gFnSc4
(	(	kIx(
<g/>
1720	#num#	k4
Mantova	Mantova	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc4
verità	verità	k?
iargn	iargn	k1gInSc1
cimento	cimento	k1gNnSc1
(	(	kIx(
<g/>
1720	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Tito	tento	k3xDgMnPc1
Manlio	Manlio	k6eAd1
(	(	kIx(
<g/>
1720	#num#	k4
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Filippo	Filippa	k1gFnSc5
Re	re	k9
di	di	k?
Macedonia	Macedonium	k1gNnPc1
(	(	kIx(
<g/>
1721	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
Silvia	Silvia	k1gFnSc1
(	(	kIx(
<g/>
1721	#num#	k4
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ercole	Ercole	k1gFnSc1
su	su	k?
<g/>
’	’	k?
<g/>
l	l	kA
Termodonte	Termodont	k1gInSc5
(	(	kIx(
<g/>
1723	#num#	k4
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Giustino	Giustin	k2eAgNnSc1d1
(	(	kIx(
<g/>
1724	#num#	k4
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
virtù	virtù	k?
trionfante	trionfant	k1gMnSc5
dell	dell	k1gInSc1
<g/>
’	’	k?
<g/>
amore	amor	k1gMnSc5
e	e	k0
dell	dell	k1gMnSc1
<g/>
’	’	k?
<g/>
odio	odio	k1gMnSc1
overo	overo	k1gNnSc1
Il	Il	k1gMnSc1
Tigrane	Tigran	k1gInSc5
(	(	kIx(
<g/>
1724	#num#	k4
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
inganno	inganno	k6eAd1
trionfante	trionfant	k1gMnSc5
in	in	k?
amore	amor	k1gMnSc5
(	(	kIx(
<g/>
1725	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Cunegonda	Cunegonda	k1gFnSc1
(	(	kIx(
<g/>
1726	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
Fede	Fed	k1gInSc2
tradita	tradita	k1gFnSc1
e	e	k0
vendicata	vendice	k1gNnPc1
(	(	kIx(
<g/>
1726	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
Tirannia	Tirannium	k1gNnSc2
gastigata	gastigat	k1gMnSc2
(	(	kIx(
<g/>
1726	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dorilla	Dorilla	k1gFnSc1
in	in	k?
Tempe	Temp	k1gInSc5
(	(	kIx(
<g/>
1726	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Ipermestra	Ipermestra	k1gFnSc1
(	(	kIx(
<g/>
1727	#num#	k4
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Siroe	Siroe	k6eAd1
<g/>
,	,	kIx,
Re	re	k9
di	di	k?
Persia	Persia	k1gFnSc1
(	(	kIx(
<g/>
1727	#num#	k4
Reggio	Reggio	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Farnace	Farnace	k1gFnSc1
(	(	kIx(
<g/>
1727	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Orlando	Orlando	k6eAd1
(	(	kIx(
<g/>
1727	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Rosilena	Rosilena	k1gFnSc1
ed	ed	k?
Oronte	Oront	k1gInSc5
(	(	kIx(
<g/>
1728	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
Atenaide	Atenaid	k1gInSc5
o	o	k7c4
sia	sia	k?
Gli	Gli	k1gFnSc4
affetti	affett	k5eAaPmF,k5eAaBmF,k5eAaImF
generosi	generose	k1gFnSc4
(	(	kIx(
<g/>
1728	#num#	k4
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Argippo	Argippa	k1gFnSc5
(	(	kIx(
<g/>
1730	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Alvilda	Alvilda	k1gFnSc1
<g/>
,	,	kIx,
Regina	Regina	k1gFnSc1
de	de	k?
<g/>
’	’	k?
Goti	Got	k1gFnSc2
(	(	kIx(
<g/>
1730	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
odio	odio	k1gMnSc1
vinto	vinto	k1gNnSc1
dalla	dalla	k1gMnSc1
costanza	costanz	k1gMnSc2
(	(	kIx(
<g/>
1731	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc1
fida	fidus	k1gMnSc2
ninfa	ninf	k1gMnSc2
(	(	kIx(
<g/>
1732	#num#	k4
Verona	Verona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Semiramide	Semiramid	k1gMnSc5
(	(	kIx(
<g/>
1732	#num#	k4
Mantova	Mantova	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Motezuma	Motezuma	k1gFnSc1
(	(	kIx(
<g/>
1733	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
Olimpiade	Olimpiad	k1gInSc5
(	(	kIx(
<g/>
1734	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
Adelaide	Adelaid	k1gInSc5
(	(	kIx(
<g/>
1735	#num#	k4
Verona	Verona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Griselda	Griselda	k1gFnSc1
(	(	kIx(
<g/>
1735	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Aristide	Aristid	k1gMnSc5
(	(	kIx(
<g/>
1735	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Bajazet	Bajazet	k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
1735	#num#	k4
Verona	Verona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ginevra	Ginevra	k1gFnSc1
<g/>
,	,	kIx,
Principessa	Principessa	k1gFnSc1
di	di	k?
Scozia	Scozia	k1gFnSc1
(	(	kIx(
<g/>
1736	#num#	k4
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Didone	Didon	k1gMnSc5
(	(	kIx(
<g/>
1737	#num#	k4
London	London	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Catone	Caton	k1gMnSc5
in	in	k?
Utica	Uticum	k1gNnPc1
(	(	kIx(
<g/>
1737	#num#	k4
Verona	Verona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Il	Il	k?
giorno	giorno	k1gNnSc1
felice	felice	k1gFnSc1
(	(	kIx(
<g/>
1737	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rosmira	Rosmira	k1gFnSc1
(	(	kIx(
<g/>
1738	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
’	’	k?
<g/>
oracolo	oracola	k1gFnSc5
in	in	k?
Messenia	Messenium	k1gNnPc1
(	(	kIx(
<g/>
1738	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Feraspe	Feraspat	k5eAaPmIp3nS
(	(	kIx(
<g/>
1739	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Ernelinda	Ernelinda	k1gFnSc1
(	(	kIx(
<g/>
1740	#num#	k4
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ryom	Ryom	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
:	:	kIx,
Verzeichis	Verzeichis	k1gFnSc1
der	drát	k5eAaImRp2nS
Werke	Werke	k1gInSc1
Antonio	Antonio	k1gMnSc1
Vivaldis	Vivaldis	k1gFnPc2
<g/>
,	,	kIx,
Leipzig	Leipziga	k1gFnPc2
<g/>
,	,	kIx,
VEB	Veba	k1gFnPc2
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
212	#num#	k4
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
Répertoire	Répertoir	k1gMnSc5
des	des	k1gNnSc2
oeuvres	oeuvres	k1gMnSc1
instrumentales	instrumentales	k1gMnSc1
d	d	k?
<g/>
’	’	k?
<g/>
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
<g/>
,	,	kIx,
Copenhague	Copenhagu	k1gFnPc1
<g/>
,	,	kIx,
Engstrom	Engstrom	k1gInSc1
&	&	k?
Sodring	Sodring	k1gInSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
726	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ISBN	ISBN	kA
87-87091-19-4	87-87091-19-4	k4
</s>
<s>
Talbot	Talbot	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
:	:	kIx,
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2014	#num#	k4
<g/>
,	,	kIx,
272	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
recenze	recenze	k1gFnSc1
na	na	k7c4
knihu	kniha	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Johann	Johann	k1gMnSc1
Sebastian	Sebastian	k1gMnSc1
Bach	Bach	k1gMnSc1
</s>
<s>
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Händel	Händlo	k1gNnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
</s>
<s>
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
na	na	k7c6
projektu	projekt	k1gInSc6
Musopen	Musopen	k2eAgMnSc1d1
</s>
<s>
Volně	volně	k6eAd1
přístupné	přístupný	k2eAgFnPc4d1
partitury	partitura	k1gFnPc4
děl	dít	k5eAaBmAgMnS,k5eAaImAgMnS
od	od	k7c2
A.	A.	kA
Vivaldiho	Vivaldi	k1gMnSc2
v	v	k7c6
projektu	projekt	k1gInSc6
IMSLP	IMSLP	kA
</s>
<s>
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
<g/>
:	:	kIx,
Koncert	koncert	k1gInSc1
B	B	kA
dur	dur	k1gNnSc7
<g/>
,	,	kIx,
d	d	k?
moll	moll	k1gNnSc1
<g/>
,	,	kIx,
g	g	kA
moll	moll	k1gNnSc1
</s>
<s>
Úplný	úplný	k2eAgInSc1d1
soupis	soupis	k1gInSc1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Four	Four	k1gInSc1
Seasons	Seasonsa	k1gFnPc2
MP3	MP3	k1gFnSc2
Creative	Creativ	k1gInSc5
Commons	Commons	k1gInSc1
Recording	Recording	k1gInSc4
</s>
<s>
Vivaldiho	Vivaldize	k6eAd1
díla	dílo	k1gNnSc2
na	na	k7c6
Choral	Choral	k1gFnSc6
Public	publicum	k1gNnPc2
Domain	Domain	k1gMnSc1
Library	Librara	k1gFnSc2
(	(	kIx(
<g/>
ChoralWiki	ChoralWik	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Vivaldiho	Vivaldize	k6eAd1
díla	dílo	k1gNnSc2
na	na	k7c4
WIMA	WIMA	kA
<g/>
:	:	kIx,
Werner	Werner	k1gMnSc1
Icking	Icking	k1gInSc4
Music	Musice	k1gFnPc2
Archive	archiv	k1gInSc5
</s>
<s>
Gloria	Gloria	k1gFnSc1
RV	RV	kA
589	#num#	k4
–	–	k?
Paritura	Paritura	k1gFnSc1
a	a	k8xC
party	party	k1gFnSc1
na	na	k7c4
Kantoreiarchiv	Kantoreiarchivo	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Barokní	barokní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Hudební	hudební	k2eAgFnSc2d1
formy	forma	k1gFnSc2
</s>
<s>
vokální	vokální	k2eAgFnSc1d1
</s>
<s>
barokní	barokní	k2eAgFnSc1d1
opera	opera	k1gFnSc1
•	•	k?
oratorium	oratorium	k1gNnSc1
•	•	k?
kantáta	kantáta	k1gFnSc1
•	•	k?
introduzione	introduzion	k1gInSc5
intrumentální	intrumentální	k2eAgNnSc1d1
</s>
<s>
concerto	concerta	k1gFnSc5
grosso	grossa	k1gFnSc5
•	•	k?
fuga	fuga	k1gFnSc1
•	•	k?
suita	suita	k1gFnSc1
•	•	k?
sonáta	sonáta	k1gFnSc1
</s>
<s>
Skladatelé	skladatel	k1gMnPc1
</s>
<s>
čeští	český	k2eAgMnPc1d1
</s>
<s>
Adam	Adam	k1gMnSc1
Michna	Michna	k1gMnSc1
z	z	k7c2
Otradovic	Otradovice	k1gFnPc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Josef	Josef	k1gMnSc1
Vejvanovský	Vejvanovský	k2eAgMnSc1d1
•	•	k?
Jan	Jan	k1gMnSc1
Dismas	Dismas	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
Matěj	Matěj	k1gMnSc1
Černohorský	Černohorský	k1gMnSc1
•	•	k?
Šimon	Šimon	k1gMnSc1
Brixi	Brixe	k1gFnSc3
•	•	k?
Heinrich	Heinrich	k1gMnSc1
Iganz	Iganz	k1gMnSc1
Franz	Franz	k1gMnSc1
Biber	Biber	k1gMnSc1
zahraniční	zahraniční	k2eAgMnSc1d1
</s>
<s>
Johann	Johann	k1gMnSc1
Sebastian	Sebastian	k1gMnSc1
Bach	Bach	k1gMnSc1
•	•	k?
Antonio	Antonio	k1gMnSc1
Vivaldi	Vivald	k1gMnPc1
•	•	k?
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Händel	Händlo	k1gNnPc2
•	•	k?
Henry	Henry	k1gMnSc1
Purcell	Purcell	k1gMnSc1
•	•	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc1
•	•	k?
Jean-Philippe	Jean-Philipp	k1gInSc5
Rameau	Ramea	k1gMnSc3
•	•	k?
Antonio	Antonio	k1gMnSc1
Caldara	Caldar	k1gMnSc2
•	•	k?
Alessandro	Alessandra	k1gFnSc5
Scarlatti	Scarlatti	k1gNnPc7
•	•	k?
Johann	Johann	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Fux	Fux	k1gMnSc1
•	•	k?
Johann	Johann	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Hasse	Hasse	k1gFnSc2
•	•	k?
Marc-Antoine	Marc-Antoin	k1gInSc5
Charpentier	Charpentira	k1gFnPc2
</s>
<s>
Interpreti	interpret	k1gMnPc1
</s>
<s>
Ars	Ars	k?
rediviva	rediviva	k1gFnSc1
•	•	k?
Hof-Musici	Hof-Musice	k1gFnSc3
•	•	k?
Collegium	Collegium	k1gNnSc1
1704	#num#	k4
•	•	k?
Collegium	Collegium	k1gNnSc4
Marianum	Marianum	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990008800	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118627287	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2129	#num#	k4
9169	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79021280	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500336474	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
42027007	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79021280	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
