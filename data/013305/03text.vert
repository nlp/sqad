<p>
<s>
Pangram	Pangram	k1gInSc1	Pangram
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
pan	pan	k1gMnSc1	pan
gramma	gramma	k1gFnSc1	gramma
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
každé	každý	k3xTgNnSc4	každý
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věta	věta	k1gFnSc1	věta
či	či	k8xC	či
úsek	úsek	k1gInSc1	úsek
textu	text	k1gInSc2	text
obsahující	obsahující	k2eAgInSc1d1	obsahující
všechna	všechen	k3xTgNnPc4	všechen
písmena	písmeno	k1gNnPc4	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
slovní	slovní	k2eAgFnSc4d1	slovní
hříčku	hříčka	k1gFnSc4	hříčka
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
vytvořit	vytvořit	k5eAaPmF	vytvořit
co	co	k3yQnSc4	co
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
vtipný	vtipný	k2eAgMnSc1d1	vtipný
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
text	text	k1gInSc1	text
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
vlastností	vlastnost	k1gFnSc7	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
pangram	pangram	k1gInSc1	pangram
využívá	využívat	k5eAaImIp3nS	využívat
např.	např.	kA	např.
jako	jako	k8xC	jako
způsob	způsob	k1gInSc1	způsob
náhledu	náhled	k1gInSc2	náhled
na	na	k7c4	na
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c4	na
test	test	k1gInSc4	test
počítačových	počítačový	k2eAgFnPc2d1	počítačová
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
psacích	psací	k2eAgInPc2d1	psací
strojů	stroj	k1gInPc2	stroj
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
věty	věta	k1gFnSc2	věta
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
říkanka	říkanka	k1gFnSc1	říkanka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
The	The	k?	The
quick	quick	k1gInSc1	quick
brown	brown	k1gNnSc1	brown
fox	fox	k1gInSc1	fox
jumps	jumps	k6eAd1	jumps
over	over	k1gInSc1	over
a	a	k8xC	a
lazy	lazy	k?	lazy
dog	doga	k1gFnPc2	doga
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
hnědá	hnědý	k2eAgFnSc1d1	hnědá
liška	liška	k1gFnSc1	liška
skáče	skákat	k5eAaImIp3nS	skákat
přes	přes	k7c4	přes
líného	líný	k2eAgMnSc4d1	líný
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jackdaws	Jackdaws	k6eAd1	Jackdaws
love	lov	k1gInSc5	lov
my	my	k3xPp1nPc1	my
big	big	k?	big
sphinx	sphinx	k1gInSc1	sphinx
of	of	k?	of
quartz	quartz	k1gInSc1	quartz
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kavky	kavka	k1gFnPc1	kavka
milují	milovat	k5eAaImIp3nP	milovat
mou	můj	k3xOp1gFnSc4	můj
velkou	velký	k2eAgFnSc4d1	velká
křemennou	křemenný	k2eAgFnSc4d1	křemenná
sfingu	sfinga	k1gFnSc4	sfinga
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
pangram	pangram	k1gInSc1	pangram
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Portez	Portez	k1gInSc1	Portez
ce	ce	k?	ce
vieux	vieux	k1gInSc1	vieux
whisky	whisky	k1gFnSc2	whisky
au	au	k0	au
juge	jugat	k5eAaPmIp3nS	jugat
blond	blond	k2eAgNnPc2d1	blond
qui	qui	k?	qui
fume	fume	k1gNnPc2	fume
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Doneste	donést	k5eAaPmRp2nP	donést
tu	tu	k6eAd1	tu
starou	stará	k1gFnSc4	stará
whisky	whisky	k1gFnSc2	whisky
blonďatému	blonďatý	k2eAgMnSc3d1	blonďatý
soudci	soudce	k1gMnSc3	soudce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kouří	kouřit	k5eAaImIp3nS	kouřit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
pangram	pangram	k1gInSc1	pangram
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vypätá	Vypätý	k2eAgFnSc1d1	Vypätý
dcéra	dcéra	k1gFnSc1	dcéra
grófa	gróf	k1gMnSc2	gróf
Maxwella	Maxwell	k1gMnSc2	Maxwell
s	s	k7c7	s
IQ	iq	kA	iq
nižším	nízký	k2eAgNnSc7d2	nižší
ako	ako	k?	ako
kôň	kôň	k?	kôň
núti	núti	k6eAd1	núti
čeľaď	čeľadit	k5eAaPmRp2nS	čeľadit
hrýzť	hrýztit	k5eAaPmRp2nS	hrýztit
hŕbu	hŕbu	k5eAaPmIp1nS	hŕbu
jabĺk	jabĺk	k6eAd1	jabĺk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vypjatá	vypjatý	k2eAgFnSc1d1	vypjatá
dcera	dcera	k1gFnSc1	dcera
hraběte	hrabě	k1gMnSc2	hrabě
Maxwella	Maxwell	k1gMnSc2	Maxwell
s	s	k7c7	s
IQ	iq	kA	iq
nižším	nízký	k2eAgMnSc7d2	nižší
než	než	k8xS	než
kůň	kůň	k1gMnSc1	kůň
nutí	nutit	k5eAaImIp3nS	nutit
čeleď	čeleď	k1gFnSc4	čeleď
kousat	kousat	k5eAaImF	kousat
spousty	spousta	k1gFnPc4	spousta
jablek	jablko	k1gNnPc2	jablko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kŕdeľ	Kŕdeľ	k?	Kŕdeľ
šťastných	šťastný	k2eAgNnPc2d1	šťastné
ďatľov	ďatľovo	k1gNnPc2	ďatľovo
učí	učit	k5eAaImIp3nS	učit
pri	pri	k?	pri
ústí	ústí	k1gNnSc1	ústí
Váhu	Váh	k1gInSc2	Váh
mĺkveho	mĺkveze	k6eAd1	mĺkveze
koňa	koňa	k1gFnSc1	koňa
obhrýzať	obhrýzatit	k5eAaPmRp2nS	obhrýzatit
kôru	kôru	k5eAaPmIp1nS	kôru
a	a	k8xC	a
žrať	žrať	k1gFnSc4	žrať
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
mäso	mäso	k1gNnSc1	mäso
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hejno	hejno	k1gNnSc1	hejno
šťastných	šťastný	k2eAgMnPc2d1	šťastný
datlů	datel	k1gMnPc2	datel
učí	učit	k5eAaImIp3nP	učit
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
Váhu	Váh	k1gInSc2	Váh
zamlklého	zamlklý	k2eAgMnSc2d1	zamlklý
koně	kůň	k1gMnSc2	kůň
okusovat	okusovat	k5eAaImF	okusovat
kůru	kůra	k1gFnSc4	kůra
a	a	k8xC	a
žrát	žrát	k5eAaImF	žrát
čerstvé	čerstvý	k2eAgNnSc4d1	čerstvé
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
pangramu	pangram	k1gInSc6	pangram
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
každé	každý	k3xTgNnSc1	každý
písmeno	písmeno	k1gNnSc1	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
právě	právě	k9	právě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
pangram	pangram	k1gInSc4	pangram
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
anglického	anglický	k2eAgInSc2d1	anglický
dokonalého	dokonalý	k2eAgInSc2d1	dokonalý
pangramu	pangram	k1gInSc2	pangram
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
J.Q.	J.Q.	k?	J.Q.
Vandz	Vandz	k1gMnSc1	Vandz
struck	struck	k1gMnSc1	struck
my	my	k3xPp1nPc1	my
big	big	k?	big
fox	fox	k1gInSc1	fox
whelp	whelp	k1gInSc1	whelp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pangramy	Pangram	k1gInPc1	Pangram
pro	pro	k7c4	pro
speciální	speciální	k2eAgNnPc4d1	speciální
česká	český	k2eAgNnPc4d1	české
písmena	písmeno	k1gNnPc4	písmeno
==	==	k?	==
</s>
</p>
<p>
<s>
Pangram	Pangram	k1gInSc1	Pangram
pro	pro	k7c4	pro
úplnou	úplný	k2eAgFnSc4d1	úplná
sadu	sada	k1gFnSc4	sada
znaků	znak	k1gInPc2	znak
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nechť	nechť	k9	nechť
již	již	k6eAd1	již
hříšné	hříšný	k2eAgInPc1d1	hříšný
saxofony	saxofon	k1gInPc1	saxofon
ďáblů	ďábel	k1gMnPc2	ďábel
rozezvučí	rozezvučet	k5eAaPmIp3nP	rozezvučet
síň	síň	k1gFnSc4	síň
úděsnými	úděsný	k2eAgInPc7d1	úděsný
tóny	tón	k1gInPc7	tón
waltzu	waltz	k1gInSc2	waltz
<g/>
,	,	kIx,	,
tanga	tango	k1gNnSc2	tango
a	a	k8xC	a
quickstepu	quickstep	k1gInSc2	quickstep
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
delší	dlouhý	k2eAgInSc1d2	delší
příklad	příklad	k1gInSc1	příklad
může	moct	k5eAaImIp3nS	moct
posloužit	posloužit	k5eAaPmF	posloužit
následující	následující	k2eAgFnSc1d1	následující
báseň	báseň	k1gFnSc1	báseň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
přílivu	příliv	k1gInSc6	příliv
žluťoučkých	žluťoučký	k2eAgInPc2d1	žluťoučký
květů	květ	k1gInPc2	květ
včelky	včelka	k1gFnSc2	včelka
se	se	k3xPyFc4	se
vznášejí	vznášet	k5eAaImIp3nP	vznášet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hleď	hledět	k5eAaImRp2nS	hledět
<g/>
,	,	kIx,	,
toť	toť	k?	toť
čarovný	čarovný	k2eAgInSc1d1	čarovný
je	být	k5eAaImIp3nS	být
loužek	loužek	k1gInSc1	loužek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hedvábné	hedvábný	k2eAgNnSc1d1	hedvábné
štěstíčka	štěstíčko	k1gNnPc1	štěstíčko
září	zářit	k5eAaImIp3nP	zářit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
žíňky	žíňka	k1gFnPc1	žíňka
běží	běžet	k5eAaImIp3nP	běžet
kolem	kolem	k7c2	kolem
lesní	lesní	k2eAgFnSc2d1	lesní
tůně	tůně	k1gFnSc2	tůně
a	a	k8xC	a
kadeřemi	kadeř	k1gFnPc7	kadeř
svými	svůj	k3xOyFgMnPc7	svůj
čeří	čeřit	k5eAaImIp3nS	čeřit
stříbrosvit	stříbrosvit	k1gInSc1	stříbrosvit
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Qvído	Qvído	k1gNnSc1	Qvído
<g/>
,	,	kIx,	,
kouzelníkův	kouzelníkův	k2eAgMnSc1d1	kouzelníkův
učeň	učeň	k1gMnSc1	učeň
s	s	k7c7	s
ďolíčky	ďolíček	k1gInPc7	ďolíček
utírá	utírat	k5eAaImIp3nS	utírat
prach	prach	k1gInSc1	prach
z	z	k7c2	z
vílích	vílí	k2eAgNnPc2d1	vílí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
náhlý	náhlý	k2eAgInSc4d1	náhlý
úsvit	úsvit	k1gInSc4	úsvit
oblažil	oblažit	k5eAaPmAgMnS	oblažit
zemětvář	zemětvář	k1gMnSc1	zemětvář
prolínajícím	prolínající	k2eAgInSc7d1	prolínající
hřejivým	hřejivý	k2eAgInSc7d1	hřejivý
dotekem	dotek	k1gInSc7	dotek
svým	svůj	k3xOyFgNnSc7	svůj
<g/>
.	.	kIx.	.
<g/>
Variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
pangram	pangram	k1gInSc1	pangram
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
ohled	ohled	k1gInSc1	ohled
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
všechna	všechen	k3xTgNnPc4	všechen
písmena	písmeno	k1gNnPc4	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
k	k	k7c3	k
takovému	takový	k3xDgInSc3	takový
účelu	účel	k1gInSc3	účel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
podpory	podpora	k1gFnSc2	podpora
znaků	znak	k1gInPc2	znak
národních	národní	k2eAgFnPc2d1	národní
abeced	abeceda	k1gFnPc2	abeceda
u	u	k7c2	u
počítačů	počítač	k1gMnPc2	počítač
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Příliš	příliš	k6eAd1	příliš
žluťoučký	žluťoučký	k2eAgMnSc1d1	žluťoučký
kůň	kůň	k1gMnSc1	kůň
úpěl	úpět	k5eAaImAgMnS	úpět
ďábelské	ďábelský	k2eAgFnPc4d1	ďábelská
ódy	óda	k1gFnPc4	óda
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
účelům	účel	k1gInPc3	účel
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
též	též	k9	též
například	například	k6eAd1	například
věty	věta	k1gFnSc2	věta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
zákeřný	zákeřný	k2eAgMnSc1d1	zákeřný
učeň	učeň	k1gMnSc1	učeň
s	s	k7c7	s
ďolíčky	ďolíček	k1gInPc7	ďolíček
běží	běžet	k5eAaImIp3nS	běžet
podél	podél	k7c2	podél
zóny	zóna	k1gFnSc2	zóna
úlů	úl	k1gInPc2	úl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyciď	vycídit	k5eAaPmRp2nS	vycídit
křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
nůž	nůž	k1gInSc4	nůž
<g/>
,	,	kIx,	,
ó	ó	k0	ó
učiň	učinit	k5eAaPmRp2nS	učinit
úděsné	úděsný	k2eAgFnPc1d1	úděsná
líbivým	líbivý	k2eAgInSc7d1	líbivý
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
čeří	čeřit	k5eAaImIp3nS	čeřit
kýlem	kýl	k1gInSc7	kýl
tůň	tůň	k1gFnSc1	tůň
obzvlášť	obzvlášť	k6eAd1	obzvlášť
v	v	k7c6	v
Grónské	grónský	k2eAgFnSc6d1	grónská
úžině	úžina	k1gFnSc6	úžina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
náhlý	náhlý	k2eAgInSc1d1	náhlý
déšť	déšť	k1gInSc1	déšť
již	již	k6eAd1	již
zvířil	zvířit	k5eAaPmAgInS	zvířit
prach	prach	k1gInSc1	prach
a	a	k8xC	a
čilá	čilý	k2eAgFnSc1d1	čilá
laň	laň	k1gFnSc1	laň
teď	teď	k6eAd1	teď
běží	běžet	k5eAaImIp3nS	běžet
s	s	k7c7	s
houfcem	houfec	k1gInSc7	houfec
gazel	gazel	k1gInSc1	gazel
k	k	k7c3	k
úkrytům	úkryt	k1gInPc3	úkryt
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
předposlední	předposlední	k2eAgNnSc4d1	předposlední
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
větou	věta	k1gFnSc7	věta
příliš	příliš	k6eAd1	příliš
žluťoučký	žluťoučký	k2eAgMnSc1d1	žluťoučký
kůň	kůň	k1gMnSc1	kůň
úpěl	úpět	k5eAaImAgMnS	úpět
ďábelské	ďábelský	k2eAgFnPc4d1	ďábelská
ódy	óda	k1gFnPc4	óda
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
písmeno	písmeno	k1gNnSc1	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
právě	právě	k9	právě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Nahradíme	nahradit	k5eAaPmIp1nP	nahradit
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
příliš	příliš	k6eAd1	příliš
za	za	k7c4	za
příšerně	příšerně	k6eAd1	příšerně
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
Ě	Ě	kA	Ě
se	se	k3xPyFc4	se
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
úpěl	úpět	k5eAaImAgInS	úpět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
účely	účel	k1gInPc4	účel
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lorem	Lorem	k6eAd1	Lorem
ipsum	ipsum	k1gInSc1	ipsum
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pangram	Pangram	k1gInSc1	Pangram
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
