Jak	jak	k6eAd1
se	se	k3xPyFc4
Hellerův	Hellerův	k2eAgInSc1d1
román	román	k1gInSc1
postupem	postupem	k7c2
času	čas	k1gInSc2
stal	stát	k5eAaPmAgInS
známý	známý	k2eAgInSc1d1
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
fráze	fráze	k1gFnSc1
Hlava	Hlava	k1gMnSc1
XXII	XXII	kA
význam	význam	k1gInSc1
označení	označení	k1gNnSc1
pro	pro	k7c4
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
výběr	výběr	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
žádné	žádný	k3yNgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
nevede	vést	k5eNaImIp3nS
k	k	k7c3
úspěchu	úspěch	k1gInSc3
<g/>
.	.	kIx.
