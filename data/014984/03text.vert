<s>
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
</s>
<s>
Hlava	Hlava	k1gMnSc1
XXII	XXII	kA
Autor	autor	k1gMnSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Heller	Heller	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Catch-	Catch-	k?
<g/>
22	#num#	k4
Překladatel	překladatel	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
JindraDušan	JindraDušan	k1gMnSc1
JanákJaroslav	JanákJaroslava	k1gFnPc2
Chuchvalec	chuchvalec	k1gInSc1
Ilustrátor	ilustrátor	k1gMnSc1
</s>
<s>
Bohumil	Bohumil	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Země	zem	k1gFnSc2
</s>
<s>
USA	USA	kA
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
román	román	k1gInSc1
<g/>
,	,	kIx,
satira	satira	k1gFnSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
1961	#num#	k4
Česky	česky	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
</s>
<s>
1964	#num#	k4
Počet	počet	k1gInSc1
stran	strana	k1gFnPc2
</s>
<s>
cca	cca	kA
520	#num#	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
—	—	k?
</s>
<s>
Něco	něco	k3yInSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Catch-	Catch-	k1gFnPc1
<g/>
22	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
první	první	k4xOgInSc4
a	a	k8xC
nejznámější	známý	k2eAgInSc4d3
román	román	k1gInSc4
amerického	americký	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Josepha	Joseph	k1gMnSc2
Hellera	Heller	k1gMnSc2
<g/>
,	,	kIx,
vydaný	vydaný	k2eAgInSc1d1
roku	rok	k1gInSc2
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritici	kritik	k1gMnPc1
ji	on	k3xPp3gFnSc4
přirovnávají	přirovnávat	k5eAaImIp3nP
k	k	k7c3
Haškovým	Haškův	k2eAgInPc3d1
Osudům	osud	k1gInPc3
dobrého	dobré	k1gNnSc2
vojáka	voják	k1gMnSc4
Švejka	Švejk	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
románu	román	k1gInSc6
</s>
<s>
Kniha	kniha	k1gFnSc1
nemá	mít	k5eNaImIp3nS
výraznější	výrazný	k2eAgFnSc4d2
dějovou	dějový	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sledem	sled	k1gInSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
příhod	příhoda	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
členěna	členit	k5eAaImNgFnS
do	do	k7c2
kapitol	kapitola	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
věnovány	věnovat	k5eAaImNgFnP,k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
románu	román	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
absurdit	absurdita	k1gFnPc2
<g/>
,	,	kIx,
satiry	satira	k1gFnSc2
a	a	k8xC
černého	černý	k2eAgInSc2d1
humoru	humor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazykově	jazykově	k6eAd1
kniha	kniha	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
vojenský	vojenský	k2eAgInSc4d1
slang	slang	k1gInSc4
a	a	k8xC
často	často	k6eAd1
i	i	k9
vulgarismy	vulgarismus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Román	román	k1gInSc1
<g/>
,	,	kIx,
inspirovaný	inspirovaný	k2eAgInSc1d1
autorovými	autorův	k2eAgInPc7d1
vlastními	vlastní	k2eAgInPc7d1
zážitky	zážitek	k1gInPc7
z	z	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
na	na	k7c4
děj	děj	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
postavy	postava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostrůvku	ostrůvek	k1gInSc6
Pianosa	Pianosa	k1gFnSc1
u	u	k7c2
pobřeží	pobřeží	k1gNnSc2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
děj	děj	k1gInSc1
odehrává	odehrávat	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
nesmírné	smírný	k2eNgNnSc1d1
množství	množství	k1gNnSc1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paradoxní	paradoxní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
uprostřed	uprostřed	k7c2
války	válka	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
knize	kniha	k1gFnSc6
nevyskytují	vyskytovat	k5eNaImIp3nP
žádní	žádný	k3yNgMnPc1
němečtí	německý	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgMnSc1d1
nepřítel	nepřítel	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
postupem	postup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
tak	tak	k9
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
více	hodně	k6eAd2
než	než	k8xS
Němců	Němec	k1gMnPc2
začíná	začínat	k5eAaImIp3nS
obávat	obávat	k5eAaImF
vlastní	vlastní	k2eAgFnSc2d1
americké	americký	k2eAgFnSc2d1
byrokratické	byrokratický	k2eAgFnSc2d1
mašinérie	mašinérie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
americká	americký	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zesměšňuje	zesměšňovat	k5eAaImIp3nS
americké	americký	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nevídané	vídaný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodej	prodej	k1gInSc1
nejvíce	hodně	k6eAd3,k6eAd1
rozpoutala	rozpoutat	k5eAaPmAgFnS
Vietnamská	vietnamský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
společností	společnost	k1gFnSc7
přijímaná	přijímaný	k2eAgFnSc1d1
přesně	přesně	k6eAd1
jako	jako	k8xC,k8xS
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
je	být	k5eAaImIp3nS
kapitán	kapitán	k1gMnSc1
Yossarian	Yossarian	k1gMnSc1
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
syrských	syrský	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
spíše	spíše	k9
Arménů	Armén	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
uniknout	uniknout	k5eAaPmF
letecké	letecký	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
uvědomuje	uvědomovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
riskuje	riskovat	k5eAaBmIp3nS
život	život	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
znamená	znamenat	k5eAaImIp3nS
víc	hodně	k6eAd2
než	než	k8xS
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děj	děj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c6
ostrově	ostrov	k1gInSc6
Pianosa	Pianosa	k1gFnSc1
ve	v	k7c6
Středozemním	středozemní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
Yossarian	Yossarian	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
bombometčík	bombometčík	k1gMnSc1
na	na	k7c6
bombardérech	bombardér	k1gInPc6
B-25	B-25	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
cestě	cesta	k1gFnSc6
domů	dům	k1gInPc2
brání	bránit	k5eAaImIp3nP
Yossarianovi	Yossarianův	k2eAgMnPc1d1
především	především	k9
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
vojenského	vojenský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
letecké	letecký	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zbaven	zbavit	k5eAaPmNgMnS
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
uznán	uznat	k5eAaPmNgInS
za	za	k7c4
psychicky	psychicky	k6eAd1
narušeného	narušený	k2eAgMnSc4d1
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
požádá	požádat	k5eAaPmIp3nS
o	o	k7c6
uvolnění	uvolnění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
uvolnění	uvolnění	k1gNnSc6
požádá	požádat	k5eAaPmIp3nS
<g/>
,	,	kIx,
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
psychickém	psychický	k2eAgNnSc6d1
zdraví	zdraví	k1gNnSc6
<g/>
,	,	kIx,
takže	takže	k8xS
být	být	k5eAaImF
uvolněn	uvolněn	k2eAgMnSc1d1
nemůže	moct	k5eNaImIp3nS
<g/>
:	:	kIx,
to	ten	k3xDgNnSc1
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
celý	celý	k2eAgInSc4d1
román	román	k1gInSc4
do	do	k7c2
absurdní	absurdní	k2eAgFnSc2d1
roviny	rovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraz	výraz	k1gInSc1
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jako	jako	k8xS,k8xC
označení	označení	k1gNnSc1
takové	takový	k3xDgFnSc3
absurdní	absurdní	k2eAgFnSc1d1
situace	situace	k1gFnSc1
součástí	součást	k1gFnSc7
běžného	běžný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Absurdnost	absurdnost	k1gFnSc1
celé	celý	k2eAgFnSc2d1
situace	situace	k1gFnSc2
vysvítá	vysvítat	k5eAaImIp3nS
rovněž	rovněž	k9
z	z	k7c2
přístupu	přístup	k1gInSc2
důstojníků	důstojník	k1gMnPc2
k	k	k7c3
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
chování	chování	k1gNnSc1
je	být	k5eAaImIp3nS
promyšlené	promyšlený	k2eAgNnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
buď	buď	k8xC
bezúčelné	bezúčelný	k2eAgNnSc1d1
plnění	plnění	k1gNnSc1
rozkazů	rozkaz	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
posílení	posílení	k1gNnSc1
popularity	popularita	k1gFnSc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
bojový	bojový	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrým	dobrý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
například	například	k6eAd1
snaha	snaha	k1gFnSc1
plukovníka	plukovník	k1gMnSc2
Cathcarta	Cathcart	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pumy	puma	k1gFnPc1
při	při	k7c6
náletech	nálet	k1gInPc6
padaly	padat	k5eAaImAgFnP
co	co	k9
nejblíž	blízce	k6eAd3
od	od	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
protože	protože	k8xS
letecké	letecký	k2eAgInPc4d1
snímky	snímek	k1gInPc4
pak	pak	k6eAd1
vypadají	vypadat	k5eAaPmIp3nP,k5eAaImIp3nP
nejlépe	dobře	k6eAd3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
úspěšnost	úspěšnost	k1gFnSc4
bojové	bojový	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Yossarian	Yossarian	k1gMnSc1
vidí	vidět	k5eAaImIp3nS
každý	každý	k3xTgInSc4
den	den	k1gInSc4
umírat	umírat	k5eAaImF
své	svůj	k3xOyFgMnPc4
přátele	přítel	k1gMnPc4
i	i	k8xC
další	další	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
nesmyslné	smyslný	k2eNgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
denně	denně	k6eAd1
bojuje	bojovat	k5eAaImIp3nS
s	s	k7c7
vojenskou	vojenský	k2eAgFnSc7d1
byrokracií	byrokracie	k1gFnSc7
a	a	k8xC
s	s	k7c7
nelogickými	logický	k2eNgInPc7d1
předpisy	předpis	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
je	být	k5eAaImIp3nS
popsána	popsat	k5eAaPmNgFnS
jako	jako	k9
absurdnost	absurdnost	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
není	být	k5eNaImIp3nS
lidský	lidský	k2eAgInSc1d1
život	život	k1gInSc1
důležitý	důležitý	k2eAgInSc1d1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
jen	jen	k9
o	o	k7c4
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
rozkazy	rozkaz	k1gInPc4
<g/>
,	,	kIx,
dobyté	dobytý	k2eAgFnPc4d1
kóty	kóta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yossarian	Yossarian	k1gMnSc1
se	se	k3xPyFc4
záměrně	záměrně	k6eAd1
chová	chovat	k5eAaImIp3nS
jako	jako	k9
blázen	blázen	k1gMnSc1
<g/>
,	,	kIx,
občas	občas	k6eAd1
se	se	k3xPyFc4
pokouší	pokoušet	k5eAaImIp3nS
o	o	k7c4
recesi	recese	k1gFnSc4
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
každou	každý	k3xTgFnSc4
cenu	cena	k1gFnSc4
dostat	dostat	k5eAaPmF
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Často	často	k6eAd1
pobývá	pobývat	k5eAaImIp3nS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
se	s	k7c7
simulovanými	simulovaný	k2eAgFnPc7d1
nemocemi	nemoc	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnul	vyhnout	k5eAaPmAgInS
výkonu	výkon	k1gInSc3
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
nemocnice	nemocnice	k1gFnSc2
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
plukovník	plukovník	k1gMnSc1
Cathcart	Cathcarta	k1gFnPc2
opět	opět	k6eAd1
zvýšil	zvýšit	k5eAaPmAgInS
počet	počet	k1gInSc1
povinných	povinný	k2eAgInPc2d1
letů	let	k1gInPc2
(	(	kIx(
<g/>
misí	mise	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
uvědomí	uvědomit	k5eAaPmIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
schováváním	schovávání	k1gNnSc7
svůj	svůj	k3xOyFgInSc4
pobyt	pobyt	k1gInSc4
v	v	k7c6
armádě	armáda	k1gFnSc6
jen	jen	k9
prodlužuje	prodlužovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podléhá	podléhat	k5eAaImIp3nS
panickému	panický	k2eAgInSc3d1
strachu	strach	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
válku	válka	k1gFnSc4
nepřežije	přežít	k5eNaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
při	při	k7c6
náletech	nálet	k1gInPc6
se	se	k3xPyFc4
jen	jen	k9
snaží	snažit	k5eAaImIp3nS
vyhnout	vyhnout	k5eAaPmF
protiletecké	protiletecký	k2eAgFnSc3d1
palbě	palba	k1gFnSc3
nepřítele	nepřítel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
zbabělá	zbabělý	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
mu	on	k3xPp3gMnSc3
paradoxně	paradoxně	k6eAd1
přináší	přinášet	k5eAaImIp3nS
nečekané	čekaný	k2eNgInPc4d1
úspěchy	úspěch	k1gInPc4
a	a	k8xC
dokonce	dokonce	k9
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
vyznamenán	vyznamenat	k5eAaPmNgInS
válečným	válečný	k2eAgInSc7d1
křížem	kříž	k1gInSc7
<g/>
;	;	kIx,
poctu	pocta	k1gFnSc4
si	se	k3xPyFc3
však	však	k9
pokazí	pokazit	k5eAaPmIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
dekorování	dekorování	k1gNnSc3
se	se	k3xPyFc4
dostaví	dostavit	k5eAaPmIp3nS
nahý	nahý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
je	být	k5eAaImIp3nS
počet	počet	k1gInSc1
letů	let	k1gInPc2
opět	opět	k6eAd1
zvýšen	zvýšit	k5eAaPmNgInS
<g/>
,	,	kIx,
odmítne	odmítnout	k5eAaPmIp3nS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
dál	daleko	k6eAd2
zúčastňovat	zúčastňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadřízení	nadřízení	k1gNnSc1
si	se	k3xPyFc3
s	s	k7c7
ním	on	k3xPp3gInSc7
už	už	k6eAd1
nevědí	vědět	k5eNaImIp3nP
rady	rada	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
mu	on	k3xPp3gMnSc3
nabídnou	nabídnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
propustí	propustit	k5eAaPmIp3nS
z	z	k7c2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
armádu	armáda	k1gFnSc4
všude	všude	k6eAd1
chválit	chválit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yossarian	Yossariany	k1gInPc2
nejdřív	dříve	k6eAd3
na	na	k7c4
tuto	tento	k3xDgFnSc4
dohodu	dohoda	k1gFnSc4
přistoupí	přistoupit	k5eAaPmIp3nP
<g/>
,	,	kIx,
později	pozdě	k6eAd2
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
ale	ale	k9
rozmyslí	rozmyslet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
vidí	vidět	k5eAaImIp3nS
v	v	k7c6
útěku	útěk	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
za	za	k7c2
pomoci	pomoc	k1gFnSc2
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
důstojníků	důstojník	k1gMnPc2
a	a	k8xC
vojenského	vojenský	k2eAgMnSc4d1
kaplana	kaplan	k1gMnSc4
pokusí	pokusit	k5eAaPmIp3nS
doplout	doplout	k5eAaPmF
na	na	k7c6
člunu	člun	k1gInSc6
do	do	k7c2
neutrálního	neutrální	k2eAgNnSc2d1
Švédska	Švédsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyšší	vysoký	k2eAgMnPc1d2
důstojníci	důstojník	k1gMnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
drtivé	drtivý	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
ukazováni	ukazován	k2eAgMnPc1d1
jako	jako	k9
blázni	blázen	k1gMnPc1
či	či	k8xC
úplní	úplný	k2eAgMnPc1d1
idioti	idiot	k1gMnPc1
–	–	k?
například	například	k6eAd1
generál	generál	k1gMnSc1
Scheisskopf	Scheisskopf	k1gMnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Sráč	sráč	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vůbec	vůbec	k9
nezajímá	zajímat	k5eNaImIp3nS
o	o	k7c4
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
a	a	k8xC
jedině	jedině	k6eAd1
o	o	k7c4
přehlídky	přehlídka	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
spí	spát	k5eAaImIp3nS
s	s	k7c7
nižšími	nízký	k2eAgMnPc7d2
důstojníky	důstojník	k1gMnPc7
<g/>
;	;	kIx,
Major	major	k1gMnSc1
Major	major	k1gMnSc1
(	(	kIx(
<g/>
jméno	jméno	k1gNnSc1
dostal	dostat	k5eAaPmAgInS
od	od	k7c2
otce	otec	k1gMnSc2
bez	bez	k7c2
vědomí	vědomí	k1gNnSc2
matky	matka	k1gFnSc2
a	a	k8xC
shodou	shoda	k1gFnSc7
okolností	okolnost	k1gFnPc2
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc2
majora	major	k1gMnSc2
<g/>
)	)	kIx)
trpí	trpět	k5eAaImIp3nS
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgNnSc3
jménu	jméno	k1gNnSc6
těžkým	těžký	k2eAgInSc7d1
strachem	strach	k1gInSc7
z	z	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
k	k	k7c3
němu	on	k3xPp3gInSc3
smějí	smát	k5eAaImIp3nP
vojáci	voják	k1gMnPc1
chodit	chodit	k5eAaImF
<g/>
,	,	kIx,
jen	jen	k9
když	když	k8xS
není	být	k5eNaImIp3nS
přítomen	přítomen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
McWatt	McWatt	k1gMnSc1
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
Yossarianova	Yossarianův	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rád	rád	k6eAd1
předvádí	předvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
jednou	jednou	k6eAd1
při	při	k7c6
nízkém	nízký	k2eAgInSc6d1
přeletu	přelet	k1gInSc6
nad	nad	k7c7
pláží	pláž	k1gFnSc7
zabije	zabít	k5eAaPmIp3nS
kamaráda	kamarád	k1gMnSc4
a	a	k8xC
vzápětí	vzápětí	k6eAd1
nalétne	nalétnout	k5eAaPmIp3nS
střemhlav	střemhlav	k6eAd1
na	na	k7c4
útes	útes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
tom	ten	k3xDgNnSc6
je	být	k5eAaImIp3nS
dílo	dílo	k1gNnSc1
trochu	trochu	k6eAd1
podobné	podobný	k2eAgNnSc1d1
Švejkovi	Švejkův	k2eAgMnPc1d1
–	–	k?
i	i	k8xC
v	v	k7c6
něm	on	k3xPp3gInSc6
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
důstojníků	důstojník	k1gMnPc2
(	(	kIx(
<g/>
snad	snad	k9
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
nadporučíka	nadporučík	k1gMnSc2
Lukáše	Lukáš	k1gMnSc2
<g/>
)	)	kIx)
vylíčena	vylíčen	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
naprostí	naprostý	k2eAgMnPc1d1
hlupáci	hlupák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
a	a	k8xC
slovenské	slovenský	k2eAgInPc1d1
překlady	překlad	k1gInPc1
</s>
<s>
Do	do	k7c2
češtiny	čeština	k1gFnSc2
román	román	k1gInSc1
přeložil	přeložit	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Chuchvalec	chuchvalec	k1gInSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
překlad	překlad	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
a	a	k8xC
znovu	znovu	k6eAd1
o	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
:	:	kIx,
</s>
<s>
HELLER	HELLER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Chuchvalec	chuchvalec	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
451	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
422478	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nový	nový	k2eAgInSc4d1
překlad	překlad	k1gInSc4
pořídil	pořídit	k5eAaPmAgMnS
Miroslav	Miroslav	k1gMnSc1
Jindra	Jindra	k1gMnSc1
(	(	kIx(
<g/>
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
překladu	překlad	k1gInSc6
je	být	k5eAaImIp3nS
román	román	k1gInSc1
opětovně	opětovně	k6eAd1
vydáván	vydávat	k5eAaImNgInS,k5eAaPmNgInS
<g/>
:	:	kIx,
</s>
<s>
HELLER	HELLER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Miroslav	Miroslav	k1gMnSc1
Jindra	Jindra	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmé	osmý	k4xOgNnSc1
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
Plusu	plus	k1gInSc6
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
revidované	revidovaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
522	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
259	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
527	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Překlad	překlad	k1gInSc1
do	do	k7c2
slovenštiny	slovenština	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
HELLER	HELLER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Dušan	Dušan	k1gMnSc1
Janák	Janák	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
520	#num#	k4
s.	s.	k?
</s>
<s>
Přenesený	přenesený	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
No-win	No-win	k2eAgInSc4d1
situation	situation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
Hellerův	Hellerův	k2eAgInSc1d1
román	román	k1gInSc1
postupem	postupem	k7c2
času	čas	k1gInSc2
stal	stát	k5eAaPmAgInS
známý	známý	k2eAgInSc1d1
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
fráze	fráze	k1gFnSc1
Hlava	Hlava	k1gMnSc1
XXII	XXII	kA
význam	význam	k1gInSc1
označení	označení	k1gNnSc1
pro	pro	k7c4
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
výběr	výběr	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
žádné	žádný	k3yNgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
nevede	vést	k5eNaImIp3nS
k	k	k7c3
úspěchu	úspěch	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Pokračování	pokračování	k1gNnSc1
</s>
<s>
K	k	k7c3
zestárlým	zestárlý	k2eAgFnPc3d1
postavám	postava	k1gFnPc3
Hlavy	hlava	k1gFnSc2
XXII	XXII	kA
se	se	k3xPyFc4
Heller	Heller	k1gMnSc1
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c6
sklonku	sklonek	k1gInSc6
života	život	k1gInSc2
v	v	k7c6
románu	román	k1gInSc6
Zavíráme	zavírat	k5eAaImIp1nP
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
angl.	angl.	k?
Closing	Closing	k1gInSc1
Time	Time	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Filmové	filmový	k2eAgInPc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
</s>
<s>
Hlava	hlava	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
Catch-	Catch-	k1gFnSc1
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
americký	americký	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Mike	Mike	k1gNnSc1
Nichols	Nicholsa	k1gFnPc2
<g/>
,	,	kIx,
hrají	hrát	k5eAaImIp3nP
<g/>
:	:	kIx,
Alan	Alan	k1gMnSc1
Arkin	Arkin	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Balsam	Balsam	k1gInSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
<g/>
,	,	kIx,
Art	Art	k1gMnSc1
Garfunkel	Garfunkel	k1gMnSc1
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Gilford	Gilford	k1gMnSc1
<g/>
,	,	kIx,
Buck	Buck	k1gMnSc1
Henry	Henry	k1gMnSc1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Newhart	Newharta	k1gFnPc2
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnPc1
Perkins	Perkins	k1gInSc1
<g/>
,	,	kIx,
Paula	Paula	k1gFnSc1
Prentiss	Prentissa	k1gFnPc2
</s>
<s>
Hlava	hlava	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
Catch-	Catch-	k1gFnSc1
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
americký	americký	k2eAgInSc1d1
seriál	seriál	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
autory	autor	k1gMnPc7
jsou	být	k5eAaImIp3nP
Luke	Luke	k1gNnSc1
Davies	Daviesa	k1gFnPc2
a	a	k8xC
David	David	k1gMnSc1
Michôd	Michôd	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
se	se	k3xPyFc4
ujali	ujmout	k5eAaPmAgMnP
George	George	k1gNnSc2
Clooney	Cloonea	k1gFnSc2
<g/>
,	,	kIx,
Grant	grant	k1gInSc4
Heslov	Heslovo	k1gNnPc2
a	a	k8xC
Ellen	Ellen	k2eAgMnSc1d1
Kuras	Kuras	k1gMnSc1
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
po	po	k7c6
dvou	dva	k4xCgFnPc6
epizodách	epizoda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrají	hrát	k5eAaImIp3nP
<g/>
:	:	kIx,
Christopher	Christophra	k1gFnPc2
Abbott	Abbott	k1gInSc1
<g/>
,	,	kIx,
Kyle	Kyle	k1gFnSc1
Chandler	Chandler	k1gMnSc1
<g/>
,	,	kIx,
Hugh	Hugh	k1gMnSc1
Laurie	Laurie	k1gFnSc2
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
Clooney	Cloonea	k1gMnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MACURA	Macura	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
světových	světový	k2eAgNnPc2d1
literárních	literární	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
svazek	svazek	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
<g/>
–	–	k?
<g/>
L.	L.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
475	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
345	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MACURA	Macura	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
světových	světový	k2eAgNnPc2d1
literárních	literární	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
svazek	svazek	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
<g/>
–	–	k?
<g/>
L.	L.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
475	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Stať	stať	k1gFnSc1
„	„	k?
<g/>
HLAVA	hlava	k1gFnSc1
XXII	XXII	kA
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
344	#num#	k4
<g/>
–	–	k?
<g/>
345	#num#	k4
<g/>
;	;	kIx,
autorka	autorka	k1gFnSc1
Alena	Alena	k1gFnSc1
Macurová	Macurová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Téma	téma	k1gNnSc1
Dílo	dílo	k1gNnSc1
<g/>
:	:	kIx,
<g/>
Hlava	hlava	k1gFnSc1
XXII	XXII	kA
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4229273-6	4229273-6	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
176599686	#num#	k4
</s>
