<p>
<s>
Umělá	umělý	k2eAgFnSc1d1	umělá
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
výpočetních	výpočetní	k2eAgInPc2d1	výpočetní
modelů	model	k1gInPc2	model
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
umělé	umělý	k2eAgFnSc6d1	umělá
inteligenci	inteligence	k1gFnSc6	inteligence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gInSc7	její
vzorem	vzor	k1gInSc7	vzor
je	být	k5eAaImIp3nS	být
chování	chování	k1gNnSc1	chování
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
biologických	biologický	k2eAgFnPc2d1	biologická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1	umělá
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
distribuované	distribuovaný	k2eAgNnSc4d1	distribuované
paralelní	paralelní	k2eAgNnSc4d1	paralelní
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
umělých	umělý	k2eAgFnPc2d1	umělá
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
formálních	formální	k2eAgInPc2d1	formální
<g/>
)	)	kIx)	)
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
předobrazem	předobraz	k1gInSc7	předobraz
je	být	k5eAaImIp3nS	být
biologický	biologický	k2eAgInSc1d1	biologický
neuron	neuron	k1gInSc1	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
propojeny	propojit	k5eAaPmNgInP	propojit
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
si	se	k3xPyFc3	se
předávají	předávat	k5eAaImIp3nP	předávat
signály	signál	k1gInPc4	signál
a	a	k8xC	a
transformují	transformovat	k5eAaBmIp3nP	transformovat
je	on	k3xPp3gFnPc4	on
pomocí	pomocí	k7c2	pomocí
určitých	určitý	k2eAgFnPc2d1	určitá
přenosových	přenosový	k2eAgFnPc2d1	přenosová
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Neuron	neuron	k1gInSc1	neuron
má	mít	k5eAaImIp3nS	mít
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
vstupů	vstup	k1gInPc2	vstup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neuronové	neuronový	k2eAgFnPc1d1	neuronová
sítě	síť	k1gFnPc1	síť
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
pro	pro	k7c4	pro
rozpoznávání	rozpoznávání	k1gNnSc4	rozpoznávání
a	a	k8xC	a
kompresi	komprese	k1gFnSc4	komprese
obrazů	obraz	k1gInPc2	obraz
nebo	nebo	k8xC	nebo
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
předvídání	předvídání	k1gNnSc1	předvídání
vývoje	vývoj	k1gInSc2	vývoj
časových	časový	k2eAgFnPc2d1	časová
řad	řada	k1gFnPc2	řada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
burzovních	burzovní	k2eAgInPc2d1	burzovní
indexů	index	k1gInPc2	index
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
filtrování	filtrování	k1gNnSc3	filtrování
spamu	spam	k1gInSc2	spam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
prohlubování	prohlubování	k1gNnSc3	prohlubování
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
nervových	nervový	k2eAgFnPc2d1	nervová
soustav	soustava	k1gFnPc2	soustava
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
perceptronová	perceptronový	k2eAgFnSc1d1	perceptronový
síť	síť	k1gFnSc1	síť
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
simulace	simulace	k1gFnSc1	simulace
fyziologického	fyziologický	k2eAgInSc2d1	fyziologický
modelu	model	k1gInSc2	model
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
vzorů	vzor	k1gInPc2	vzor
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Model	model	k1gInSc1	model
umělého	umělý	k2eAgInSc2d1	umělý
neuronu	neuron	k1gInSc2	neuron
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
modelů	model	k1gInPc2	model
neuronů	neuron	k1gInPc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
velmi	velmi	k6eAd1	velmi
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
používající	používající	k2eAgFnPc1d1	používající
nespojité	spojitý	k2eNgFnPc4d1	nespojitá
přenosové	přenosový	k2eAgFnPc4d1	přenosová
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
popisující	popisující	k2eAgNnSc1d1	popisující
každý	každý	k3xTgInSc4	každý
detail	detail	k1gInSc4	detail
chování	chování	k1gNnSc4	chování
neuronu	neuron	k1gInSc2	neuron
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpoužívanějších	používaný	k2eAgMnPc2d3	nejpoužívanější
je	být	k5eAaImIp3nS	být
model	model	k1gInSc4	model
popsaný	popsaný	k2eAgInSc4d1	popsaný
McCullochem	McCulloch	k1gInSc7	McCulloch
a	a	k8xC	a
Pittsem	Pittso	k1gNnSc7	Pittso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Y	Y	kA	Y
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Θ	Θ	k?	Θ
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
=	=	kIx~	=
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
vstupy	vstup	k1gInPc1	vstup
neuronu	neuron	k1gInSc2	neuron
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
synaptické	synaptický	k2eAgFnPc4d1	synaptická
váhy	váha	k1gFnPc4	váha
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Θ	Θ	k?	Θ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Theta	Theto	k1gNnSc2	Theto
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
práh	práh	k1gInSc1	práh
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
přenosová	přenosový	k2eAgFnSc1d1	přenosová
funkce	funkce	k1gFnSc1	funkce
neuronu	neuron	k1gInSc2	neuron
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
aktivační	aktivační	k2eAgFnPc1d1	aktivační
funkce	funkce	k1gFnPc1	funkce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Y	Y	kA	Y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
výstup	výstup	k1gInSc4	výstup
neuronuVelikost	neuronuVelikost	k1gFnSc1	neuronuVelikost
vah	váha	k1gFnPc2	váha
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
uložení	uložení	k1gNnSc4	uložení
zkušeností	zkušenost	k1gFnPc2	zkušenost
do	do	k7c2	do
neuronu	neuron	k1gInSc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
vstup	vstup	k1gInSc1	vstup
důležitější	důležitý	k2eAgInSc1d2	Důležitější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biologickém	biologický	k2eAgInSc6d1	biologický
neuronu	neuron	k1gInSc6	neuron
práh	práh	k1gInSc1	práh
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Θ	Θ	k?	Θ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Theta	Theto	k1gNnSc2	Theto
}	}	kIx)	}
</s>
</p>
<p>
<s>
označuje	označovat	k5eAaImIp3nS	označovat
prahovou	prahový	k2eAgFnSc4d1	prahová
hodnotu	hodnota	k1gFnSc4	hodnota
aktivace	aktivace	k1gFnSc2	aktivace
neuronu	neuron	k1gInSc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Tzn.	tzn.	kA	tzn.
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
neuron	neuron	k1gInSc1	neuron
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pasivním	pasivní	k2eAgInSc6d1	pasivní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
povahy	povaha	k1gFnSc2	povaha
vstupních	vstupní	k2eAgInPc2d1	vstupní
(	(	kIx(	(
<g/>
a	a	k8xC	a
výstupních	výstupní	k2eAgInPc2d1	výstupní
<g/>
)	)	kIx)	)
dat	datum	k1gNnPc2	datum
můžeme	moct	k5eAaImIp1nP	moct
neurony	neuron	k1gInPc1	neuron
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
binární	binární	k2eAgNnSc4d1	binární
a	a	k8xC	a
spojité	spojitý	k2eAgNnSc4d1	spojité
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
neuronu	neuron	k1gInSc2	neuron
a	a	k8xC	a
typu	typ	k1gInSc2	typ
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
vhodná	vhodný	k2eAgFnSc1d1	vhodná
přenosová	přenosový	k2eAgFnSc1d1	přenosová
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenosové	přenosový	k2eAgFnPc4d1	přenosová
funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Skoková	skokový	k2eAgFnSc1d1	skoková
přenosová	přenosový	k2eAgFnSc1d1	přenosová
funkce	funkce	k1gFnSc1	funkce
vrací	vracet	k5eAaImIp3nS	vracet
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
daná	daný	k2eAgFnSc1d1	daná
mez	mez	k1gFnSc1	mez
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
větší	veliký	k2eAgNnSc4d2	veliký
vrací	vracet	k5eAaImIp3nS	vracet
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
Θ	Θ	k?	Θ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
Θ	Θ	k?	Θ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
\	\	kIx~	\
<g/>
Theta	Theta	k1gMnSc1	Theta
}	}	kIx)	}
</s>
</p>
<p>
<s>
Sigmoidální	Sigmoidální	k2eAgFnSc1d1	Sigmoidální
přenosová	přenosový	k2eAgFnSc1d1	přenosová
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-kx	x	k?	-kx
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnPc1	její
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nP	blížit
v	v	k7c6	v
minus	minus	k1gNnSc6	minus
nekonečnu	nekonečno	k1gNnSc6	nekonečno
nule	nula	k1gFnSc3	nula
a	a	k8xC	a
v	v	k7c6	v
nekonečnu	nekonečno	k1gNnSc6	nekonečno
jedničce	jednička	k1gFnSc3	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nulu	nula	k1gFnSc4	nula
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
0,5	[number]	k4	0,5
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
sigmoidální	sigmoidální	k2eAgFnSc2d1	sigmoidální
přenosové	přenosový	k2eAgFnSc2d1	přenosová
funkce	funkce	k1gFnSc2	funkce
oproti	oproti	k7c3	oproti
skokové	skokový	k2eAgFnSc3d1	skoková
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
spojité	spojitý	k2eAgFnPc4d1	spojitá
první	první	k4xOgFnPc4	první
derivace	derivace	k1gFnPc4	derivace
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přenosová	přenosový	k2eAgFnSc1d1	přenosová
funkce	funkce	k1gFnSc1	funkce
hyperbolické	hyperbolický	k2eAgFnSc2d1	hyperbolická
tangenty	tangenta	k1gFnSc2	tangenta
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-kx	x	k?	-kx
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnPc1	její
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
-1	-1	k4	-1
v	v	k7c4	v
minus	minus	k6eAd1	minus
nekonečnu	nekonečno	k1gNnSc3	nekonečno
a	a	k8xC	a
jedničce	jednička	k1gFnSc3	jednička
v	v	k7c6	v
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nulu	nula	k1gFnSc4	nula
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
Přenosová	přenosový	k2eAgFnSc1d1	přenosová
funkce	funkce	k1gFnSc1	funkce
radiální	radiální	k2eAgFnSc2d1	radiální
báze	báze	k1gFnSc2	báze
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-kx	x	k?	-kx
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnPc1	její
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nP	blížit
nule	nula	k1gFnSc3	nula
v	v	k7c4	v
minus	minus	k6eAd1	minus
nekonečnu	nekonečno	k1gNnSc3	nekonečno
a	a	k8xC	a
nule	nula	k1gFnSc3	nula
v	v	k7c6	v
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nulu	nula	k4xCgFnSc4	nula
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
hodnota	hodnota	k1gFnSc1	hodnota
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektury	architektura	k1gFnSc2	architektura
sítě	síť	k1gFnSc2	síť
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
způsobů	způsob	k1gInPc2	způsob
propojení	propojení	k1gNnSc2	propojení
neuronů	neuron	k1gInPc2	neuron
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
přenosové	přenosový	k2eAgFnSc2d1	přenosová
funkce	funkce	k1gFnSc2	funkce
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
různých	různý	k2eAgFnPc2d1	různá
architektur	architektura	k1gFnPc2	architektura
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Perceptron	Perceptron	k1gInSc1	Perceptron
</s>
</p>
<p>
<s>
Vrstevnatá	vrstevnatý	k2eAgFnSc1d1	vrstevnatá
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Rekurentní	rekurentní	k2eAgFnSc1d1	rekurentní
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Kohonenovy	Kohonenův	k2eAgFnPc1d1	Kohonenova
mapy	mapa	k1gFnPc1	mapa
</s>
</p>
<p>
<s>
Modulární	modulární	k2eAgFnPc1d1	modulární
neuronové	neuronový	k2eAgFnPc1d1	neuronová
sítě	síť	k1gFnPc1	síť
</s>
</p>
<p>
<s>
==	==	k?	==
Učení	učení	k1gNnSc3	učení
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
==	==	k?	==
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
učení	učení	k1gNnSc2	učení
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
nastavit	nastavit	k5eAaPmF	nastavit
síť	síť	k1gFnSc4	síť
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dávala	dávat	k5eAaImAgFnS	dávat
co	co	k9	co
nejpřesnější	přesný	k2eAgInPc4d3	nejpřesnější
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biologických	biologický	k2eAgFnPc6d1	biologická
sítích	síť	k1gFnPc6	síť
jsou	být	k5eAaImIp3nP	být
zkušenosti	zkušenost	k1gFnPc1	zkušenost
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
dendritech	dendrit	k1gInPc6	dendrit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
umělých	umělý	k2eAgFnPc6d1	umělá
neuronových	neuronový	k2eAgFnPc6d1	neuronová
sítích	síť	k1gFnPc6	síť
jsou	být	k5eAaImIp3nP	být
zkušenosti	zkušenost	k1gFnPc1	zkušenost
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
matematickém	matematický	k2eAgInSc6d1	matematický
ekvivalentu	ekvivalent	k1gInSc6	ekvivalent
-	-	kIx~	-
váhách	váha	k1gFnPc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
Učení	učení	k1gNnSc4	učení
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
na	na	k7c6	na
učení	učení	k1gNnSc6	učení
s	s	k7c7	s
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
učení	učení	k1gNnSc4	učení
bez	bez	k7c2	bez
učitele	učitel	k1gMnSc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Fáze	fáze	k1gFnSc1	fáze
učení	učení	k1gNnSc2	učení
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
adaptivní	adaptivní	k2eAgFnSc7d1	adaptivní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
naučení	naučení	k1gNnSc6	naučení
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc1	síť
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
vybavování	vybavování	k1gNnSc2	vybavování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Učení	učení	k1gNnSc4	učení
s	s	k7c7	s
učitelem	učitel	k1gMnSc7	učitel
===	===	k?	===
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
biologických	biologický	k2eAgFnPc6d1	biologická
sítích	síť	k1gFnPc6	síť
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
využita	využít	k5eAaPmNgFnS	využít
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Neuronové	neuronový	k2eAgFnSc3d1	neuronová
síti	síť	k1gFnSc3	síť
je	být	k5eAaImIp3nS	být
předložen	předložen	k2eAgInSc1d1	předložen
vzor	vzor	k1gInSc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
nastavení	nastavení	k1gNnSc2	nastavení
je	být	k5eAaImIp3nS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
aktuální	aktuální	k2eAgInSc1d1	aktuální
výsledek	výsledek	k1gInSc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
porovnáme	porovnat	k5eAaPmIp1nP	porovnat
s	s	k7c7	s
vyžadovaným	vyžadovaný	k2eAgInSc7d1	vyžadovaný
výsledkem	výsledek	k1gInSc7	výsledek
a	a	k8xC	a
určíme	určit	k5eAaPmIp1nP	určit
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
spočítáme	spočítat	k5eAaPmIp1nP	spočítat
nutnou	nutný	k2eAgFnSc4d1	nutná
korekci	korekce	k1gFnSc4	korekce
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
typu	typ	k1gInSc2	typ
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
a	a	k8xC	a
upravíme	upravit	k5eAaPmIp1nP	upravit
hodnoty	hodnota	k1gFnPc4	hodnota
vah	váha	k1gFnPc2	váha
či	či	k8xC	či
prahů	práh	k1gInPc2	práh
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
snížili	snížit	k5eAaPmAgMnP	snížit
hodnotu	hodnota	k1gFnSc4	hodnota
chyby	chyba	k1gFnSc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
opakujeme	opakovat	k5eAaImIp1nP	opakovat
až	až	k9	až
do	do	k7c2	do
dosažení	dosažení	k1gNnSc2	dosažení
námi	my	k3xPp1nPc7	my
stanovené	stanovený	k2eAgFnPc4d1	stanovená
minimální	minimální	k2eAgFnSc4d1	minimální
chyby	chyba	k1gFnPc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc1	síť
adaptována	adaptován	k2eAgFnSc1d1	adaptována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Učení	učení	k1gNnPc4	učení
bez	bez	k7c2	bez
učitele	učitel	k1gMnSc2	učitel
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
učení	učení	k1gNnSc6	učení
bez	bez	k7c2	bez
učitele	učitel	k1gMnSc2	učitel
nevyhodnocujeme	vyhodnocovat	k5eNaImIp1nP	vyhodnocovat
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
učení	učení	k1gNnSc6	učení
nám	my	k3xPp1nPc3	my
výstup	výstup	k1gInSc1	výstup
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
sadu	sad	k1gInSc2	sad
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
sama	sám	k3xTgMnSc4	sám
třídí	třídit	k5eAaImIp3nS	třídit
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
si	se	k3xPyFc3	se
vzory	vzor	k1gInPc1	vzor
třídí	třídit	k5eAaImIp3nP	třídit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
typického	typický	k2eAgMnSc4d1	typický
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
topologii	topologie	k1gFnSc4	topologie
vlastnostem	vlastnost	k1gFnPc3	vlastnost
vstupu	vstup	k1gInSc2	vstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Modulární	modulární	k2eAgFnSc1d1	modulární
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
==	==	k?	==
</s>
</p>
<p>
<s>
Biologické	biologický	k2eAgFnPc1d1	biologická
studie	studie	k1gFnPc1	studie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
nepracuje	pracovat	k5eNaImIp3nS	pracovat
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
masivní	masivní	k2eAgFnSc1d1	masivní
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
soubor	soubor	k1gInSc1	soubor
malých	malý	k2eAgFnPc2d1	malá
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
dal	dát	k5eAaPmAgInS	dát
zrod	zrod	k1gInSc4	zrod
konceptu	koncept	k1gInSc2	koncept
modulárních	modulární	k2eAgFnPc2d1	modulární
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc2	který
několik	několik	k4yIc1	několik
malých	malý	k2eAgFnPc2d1	malá
sítí	síť	k1gFnPc2	síť
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
nebo	nebo	k8xC	nebo
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyřešily	vyřešit	k5eAaPmAgFnP	vyřešit
daný	daný	k2eAgInSc4d1	daný
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výbor	výbor	k1gInSc1	výbor
strojů	stroj	k1gInPc2	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
výbor	výbor	k1gInSc1	výbor
<g/>
)	)	kIx)	)
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
Committee	Committee	k1gInSc1	Committee
of	of	k?	of
Machines	Machines	k1gInSc1	Machines
<g/>
;	;	kIx,	;
CoM	CoM	k1gFnSc1	CoM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
různých	různý	k2eAgFnPc2d1	různá
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
dohromady	dohromady	k6eAd1	dohromady
"	"	kIx"	"
<g/>
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
příklad	příklad	k1gInSc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
dává	dávat	k5eAaImIp3nS	dávat
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgInPc4d2	lepší
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
srovnaní	srovnaný	k2eAgMnPc1d1	srovnaný
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
modely	model	k1gInPc7	model
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
CoM	CoM	k?	CoM
směřuje	směřovat	k5eAaImIp3nS	směřovat
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
CoM	CoM	k?	CoM
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
obecné	obecný	k2eAgFnSc3d1	obecná
bagging	bagging	k1gInSc1	bagging
metodě	metoda	k1gFnSc3	metoda
strojového	strojový	k2eAgNnSc2d1	strojové
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nutná	nutný	k2eAgFnSc1d1	nutná
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
Komisi	komise	k1gFnSc6	komise
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
trénováním	trénování	k1gNnSc7	trénování
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
náhodně	náhodně	k6eAd1	náhodně
vyhraných	vyhraný	k2eAgFnPc2d1	vyhraná
startovních	startovní	k2eAgFnPc2d1	startovní
vah	váha	k1gFnPc2	váha
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
trénování	trénování	k1gNnSc3	trénování
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
náhodně	náhodně	k6eAd1	náhodně
vybraných	vybraný	k2eAgFnPc6d1	vybraná
podmnožinách	podmnožina	k1gFnPc6	podmnožina
trénovacích	trénovací	k2eAgFnPc2d1	trénovací
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Asociativní	asociativní	k2eAgFnSc2d1	asociativní
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
ASNN	ASNN	kA	ASNN
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
ASNN	ASNN	kA	ASNN
je	být	k5eAaImIp3nS	být
rozšíření	rozšíření	k1gNnSc4	rozšíření
CoM	CoM	k1gFnSc2	CoM
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jde	jít	k5eAaImIp3nS	jít
až	až	k9	až
za	za	k7c4	za
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
<g/>
/	/	kIx~	/
<g/>
vážené	vážený	k2eAgInPc4d1	vážený
průměry	průměr	k1gInPc4	průměr
různých	různý	k2eAgInPc2d1	různý
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
ASNN	ASNN	kA	ASNN
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
kombinaci	kombinace	k1gFnSc4	kombinace
souboru	soubor	k1gInSc2	soubor
dopředných	dopředný	k2eAgFnPc2d1	dopředná
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
k-nejbližšího	kejbližý	k2eAgMnSc2d2	k-nejbližý
souseda	soused	k1gMnSc2	soused
(	(	kIx(	(
<g/>
kNN	kNN	k?	kNN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
korelaci	korelace	k1gFnSc4	korelace
mezi	mezi	k7c7	mezi
souborem	soubor	k1gInSc7	soubor
odpovědí	odpověď	k1gFnPc2	odpověď
jako	jako	k8xS	jako
měřítko	měřítko	k1gNnSc4	měřítko
"	"	kIx"	"
<g/>
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
analyzovanými	analyzovaný	k2eAgInPc7d1	analyzovaný
případy	případ	k1gInPc7	případ
pro	pro	k7c4	pro
kNN	kNN	k?	kNN
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
opravuje	opravovat	k5eAaImIp3nS	opravovat
systematickou	systematický	k2eAgFnSc4d1	systematická
chybu	chyba	k1gFnSc4	chyba
souboru	soubor	k1gInSc2	soubor
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Asociativní	asociativní	k2eAgInSc1d1	asociativní
NS	NS	kA	NS
má	mít	k5eAaImIp3nS	mít
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
s	s	k7c7	s
trénovacími	trénovací	k2eAgNnPc7d1	trénovací
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
data	datum	k1gNnPc1	datum
stanou	stanout	k5eAaPmIp3nP	stanout
dostupnými	dostupný	k2eAgNnPc7d1	dostupné
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc1	síť
okamžitě	okamžitě	k6eAd1	okamžitě
vylepší	vylepšit	k5eAaPmIp3nS	vylepšit
svou	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
předpovědi	předpověď	k1gFnSc2	předpověď
a	a	k8xC	a
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
aproximaci	aproximace	k1gFnSc4	aproximace
dat	datum	k1gNnPc2	datum
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
znovu	znovu	k6eAd1	znovu
přetrénovat	přetrénovat	k5eAaPmF	přetrénovat
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
ASNN	ASNN	kA	ASNN
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
interpretovat	interpretovat	k5eAaBmF	interpretovat
výsledky	výsledek	k1gInPc4	výsledek
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
analýzou	analýza	k1gFnSc7	analýza
korelací	korelace	k1gFnSc7	korelace
mezi	mezi	k7c4	mezi
příklady	příklad	k1gInPc4	příklad
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
demonstrována	demonstrovat	k5eAaBmNgFnS	demonstrovat
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.vcclab.org	www.vcclab.org	k1gInSc1	www.vcclab.org
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
umělá	umělý	k2eAgFnSc1d1	umělá
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Předpovídání	předpovídání	k1gNnSc1	předpovídání
pomocí	pomocí	k7c2	pomocí
neuronové	neuronový	k2eAgFnSc2d1	neuronová
sítě	síť	k1gFnSc2	síť
-	-	kIx~	-
včetně	včetně	k7c2	včetně
Java	Jav	k1gInSc2	Jav
appletu	applést	k5eAaPmIp1nS	applést
pro	pro	k7c4	pro
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
předpovídáním	předpovídání	k1gNnSc7	předpovídání
funkce	funkce	k1gFnSc2	funkce
</s>
</p>
<p>
<s>
ČVUT	ČVUT	kA	ČVUT
Gerstner	Gerstner	k1gMnSc1	Gerstner
-	-	kIx~	-
prezentace	prezentace	k1gFnSc1	prezentace
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
</s>
</p>
