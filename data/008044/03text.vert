<s>
Curych	Curych	k1gInSc1	Curych
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Zürich	Zürich	k1gInSc1	Zürich
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
kantonu	kanton	k1gInSc2	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
předhůří	předhůří	k1gNnSc6	předhůří
Alp	Alpy	k1gFnPc2	Alpy
u	u	k7c2	u
severního	severní	k2eAgInSc2d1	severní
konce	konec	k1gInSc2	konec
Curyšského	curyšský	k2eAgNnSc2d1	Curyšské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Limmat	Limma	k1gNnPc2	Limma
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
zde	zde	k6eAd1	zde
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
vytéká	vytékat	k5eAaImIp3nS	vytékat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
lesnatými	lesnatý	k2eAgInPc7d1	lesnatý
kopci	kopec	k1gInPc7	kopec
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
Uetliberg	Uetliberg	k1gInSc1	Uetliberg
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
869	[number]	k4	869
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
12	[number]	k4	12
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
Bezirk	Bezirk	k1gInSc1	Bezirk
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
404	[number]	k4	404
783	[number]	k4	783
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
aglomeraci	aglomerace	k1gFnSc6	aglomerace
přes	přes	k7c4	přes
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
už	už	k9	už
v	v	k7c6	v
neolitu	neolit	k1gInSc6	neolit
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Turicum	Turicum	k1gNnSc4	Turicum
založili	založit	k5eAaPmAgMnP	založit
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
svobodným	svobodný	k2eAgNnSc7d1	svobodné
říšským	říšský	k2eAgNnSc7d1	říšské
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
členem	člen	k1gMnSc7	člen
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
reformace	reformace	k1gFnSc1	reformace
Ulricha	Ulrich	k1gMnSc2	Ulrich
Zwingliho	Zwingli	k1gMnSc2	Zwingli
<g/>
.	.	kIx.	.
</s>
<s>
Curych	Curych	k1gInSc1	Curych
je	být	k5eAaImIp3nS	být
bankovní	bankovní	k2eAgNnSc4d1	bankovní
středisko	středisko	k1gNnSc4	středisko
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgNnPc2d3	nejbohatší
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dopravní	dopravní	k2eAgInSc1d1	dopravní
uzel	uzel	k1gInSc1	uzel
celoevropského	celoevropský	k2eAgInSc2d1	celoevropský
významu	význam	k1gInSc2	význam
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc6d1	železniční
i	i	k8xC	i
dálniční	dálniční	k2eAgFnSc6d1	dálniční
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
hospodářským	hospodářský	k2eAgInPc3d1	hospodářský
a	a	k8xC	a
kulturním	kulturní	k2eAgInPc3d1	kulturní
centrům	centr	k1gInPc3	centr
země	zem	k1gFnSc2	zem
a	a	k8xC	a
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
průzkumů	průzkum	k1gInPc2	průzkum
mezi	mezi	k7c4	mezi
nejdražší	drahý	k2eAgNnPc4d3	nejdražší
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgNnPc2d1	další
švýcarských	švýcarský	k2eAgNnPc2d1	švýcarské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úrodném	úrodný	k2eAgNnSc6d1	úrodné
místě	místo	k1gNnSc6	místo
hustého	hustý	k2eAgNnSc2d1	husté
neolitického	neolitický	k2eAgNnSc2d1	neolitické
osídlení	osídlení	k1gNnSc2	osídlení
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
Helvétové	Helvét	k1gMnPc1	Helvét
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgNnSc4d1	kamenné
město	město	k1gNnSc4	město
založili	založit	k5eAaPmAgMnP	založit
Římané	Říman	k1gMnPc1	Říman
roku	rok	k1gInSc2	rok
15	[number]	k4	15
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
zda	zda	k8xS	zda
celní	celní	k2eAgFnSc4d1	celní
stanici	stanice	k1gFnSc4	stanice
a	a	k8xC	a
město	město	k1gNnSc4	město
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Turicum	Turicum	k1gNnSc4	Turicum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	on	k3xPp3gMnPc4	on
osídlili	osídlit	k5eAaPmAgMnP	osídlit
germánští	germánský	k2eAgMnPc1d1	germánský
Alamani	Alaman	k1gMnPc1	Alaman
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
sem	sem	k6eAd1	sem
pronikali	pronikat	k5eAaImAgMnP	pronikat
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
misionáři	misionář	k1gMnPc1	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
835	[number]	k4	835
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
připomíná	připomínat	k5eAaImIp3nS	připomínat
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
římské	římský	k2eAgFnSc2d1	římská
pevnosti	pevnost	k1gFnSc2	pevnost
císařem	císař	k1gMnSc7	císař
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Němcem	Němec	k1gMnSc7	Němec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
853	[number]	k4	853
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgInSc4	první
ženský	ženský	k2eAgInSc4d1	ženský
benediktinský	benediktinský	k2eAgInSc4d1	benediktinský
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Fraumünster	Fraumünster	k1gInSc1	Fraumünster
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1218	[number]	k4	1218
byl	být	k5eAaImAgInS	být
Curych	Curych	k1gInSc1	Curych
císařským	císařský	k2eAgNnSc7d1	císařské
městem	město	k1gNnSc7	město
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
abatyše	abatyše	k1gFnSc2	abatyše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
38	[number]	k4	38
hektarů	hektar	k1gInPc2	hektar
vybudovalo	vybudovat	k5eAaPmAgNnS	vybudovat
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
dvě	dva	k4xCgFnPc1	dva
brány	brána	k1gFnPc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Kodex	kodex	k1gInSc1	kodex
Manesse	Manesse	k1gFnSc2	Manesse
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgInPc2d3	nejkrásnější
iluminovaných	iluminovaný	k2eAgInPc2d1	iluminovaný
rukopisů	rukopis	k1gInPc2	rukopis
kurtoazní	kurtoazní	k2eAgFnSc2d1	kurtoazní
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc4d1	hlavní
pramen	pramen	k1gInSc4	pramen
středověké	středověký	k2eAgFnSc2d1	středověká
německé	německý	k2eAgFnSc2d1	německá
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
heraldiky	heraldika	k1gFnSc2	heraldika
<g/>
.	.	kIx.	.
</s>
<s>
Feudální	feudální	k2eAgFnSc4d1	feudální
vládu	vláda	k1gFnSc4	vláda
abatyše	abatyše	k1gFnSc2	abatyše
nahradila	nahradit	k5eAaPmAgFnS	nahradit
roku	rok	k1gInSc2	rok
1336	[number]	k4	1336
cechovní	cechovní	k2eAgFnSc1d1	cechovní
samospráva	samospráva	k1gFnSc1	samospráva
(	(	kIx(	(
<g/>
Zunftordnung	Zunftordnung	k1gInSc1	Zunftordnung
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
se	se	k3xPyFc4	se
měšťané	měšťan	k1gMnPc1	měšťan
přísahou	přísaha	k1gFnSc7	přísaha
připojili	připojit	k5eAaPmAgMnP	připojit
jako	jako	k8xC	jako
pátý	pátý	k4xOgInSc1	pátý
člen	člen	k1gInSc1	člen
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
konfederace	konfederace	k1gFnSc2	konfederace
(	(	kIx(	(
<g/>
Confederatio	Confederatio	k6eAd1	Confederatio
helvetica	helvetica	k6eAd1	helvetica
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Eidgenossenschaft	Eidgenossenschaft	k2eAgMnSc1d1	Eidgenossenschaft
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
Curych	Curych	k1gInSc1	Curych
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
kazatelem	kazatel	k1gMnSc7	kazatel
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
Ulrich	Ulrich	k1gMnSc1	Ulrich
Zwingli	Zwingl	k1gMnSc3	Zwingl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zahájil	zahájit	k5eAaPmAgMnS	zahájit
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
reformaci	reformace	k1gFnSc4	reformace
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1531	[number]	k4	1531
zde	zde	k6eAd1	zde
vyšel	vyjít	k5eAaPmAgInS	vyjít
jeho	jeho	k3xOp3gInSc1	jeho
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
Zürcher	Zürchra	k1gFnPc2	Zürchra
Bibel	Bibel	k1gInSc1	Bibel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konfliktů	konflikt	k1gInPc2	konflikt
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
katolickými	katolický	k2eAgInPc7d1	katolický
kantony	kanton	k1gInPc7	kanton
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
Kappelské	Kappelský	k2eAgInPc4d1	Kappelský
války	válek	k1gInPc4	válek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
si	se	k3xPyFc3	se
Curych	Curych	k1gInSc1	Curych
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
nákladné	nákladný	k2eAgNnSc4d1	nákladné
opevnění	opevnění	k1gNnSc4	opevnění
a	a	k8xC	a
vymáhání	vymáhání	k1gNnSc1	vymáhání
daní	daň	k1gFnPc2	daň
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
selské	selský	k2eAgFnPc4d1	selská
vzpoury	vzpoura	k1gFnPc4	vzpoura
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
město	město	k1gNnSc1	město
tvrdě	tvrdě	k6eAd1	tvrdě
potlačilo	potlačit	k5eAaPmAgNnS	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
patricijskou	patricijský	k2eAgFnSc7d1	patricijská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Helvetská	helvetský	k2eAgFnSc1d1	helvetská
revoluce	revoluce	k1gFnSc1	revoluce
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
pozvala	pozvat	k5eAaPmAgFnS	pozvat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
francouzské	francouzský	k2eAgNnSc1d1	francouzské
republikánské	republikánský	k2eAgNnSc1d1	republikánské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
zemi	zem	k1gFnSc3	zem
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
pod	pod	k7c7	pod
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
patronací	patronace	k1gFnSc7	patronace
sice	sice	k8xC	sice
brzy	brzy	k6eAd1	brzy
padla	padnout	k5eAaPmAgFnS	padnout
<g/>
,	,	kIx,	,
zrušila	zrušit	k5eAaPmAgFnS	zrušit
však	však	k9	však
feudální	feudální	k2eAgFnSc4d1	feudální
vládu	vláda	k1gFnSc4	vláda
města	město	k1gNnSc2	město
nad	nad	k7c7	nad
kantonem	kanton	k1gInSc7	kanton
a	a	k8xC	a
připravila	připravit	k5eAaPmAgFnS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
moderní	moderní	k2eAgNnSc4d1	moderní
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Curyšská	curyšský	k2eAgFnSc1d1	curyšská
smlouva	smlouva	k1gFnSc1	smlouva
1859	[number]	k4	1859
ukončila	ukončit	k5eAaPmAgFnS	ukončit
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
uznala	uznat	k5eAaPmAgFnS	uznat
samostatnost	samostatnost	k1gFnSc4	samostatnost
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
železnice	železnice	k1gFnSc1	železnice
Curych	Curych	k1gInSc1	Curych
–	–	k?	–
Baden	Baden	k1gInSc1	Baden
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
současného	současný	k2eAgNnSc2d1	současné
města	město	k1gNnSc2	město
Bahnhofstrasse	Bahnhofstrasse	k1gFnSc2	Bahnhofstrasse
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
od	od	k7c2	od
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
budova	budova	k1gFnSc1	budova
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
curyšská	curyšský	k2eAgFnSc1d1	curyšská
burza	burza	k1gFnSc1	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
a	a	k8xC	a
1874	[number]	k4	1874
přijal	přijmout	k5eAaPmAgInS	přijmout
Curych	Curych	k1gInSc1	Curych
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zbořil	zbořit	k5eAaPmAgMnS	zbořit
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
cílem	cíl	k1gInSc7	cíl
velké	velký	k2eAgFnSc2d1	velká
imigrace	imigrace	k1gFnSc2	imigrace
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
se	se	k3xPyFc4	se
však	však	k9	však
obtížně	obtížně	k6eAd1	obtížně
domáhali	domáhat	k5eAaImAgMnP	domáhat
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
začalo	začít	k5eAaPmAgNnS	začít
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	let	k1gInPc6	let
pobytu	pobyt	k1gInSc2	pobyt
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
stává	stávat	k5eAaImIp3nS	stávat
občanem	občan	k1gMnSc7	občan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
městu	město	k1gNnSc3	město
přičleněny	přičleněn	k2eAgFnPc1d1	přičleněna
okolní	okolní	k2eAgFnPc1d1	okolní
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikal	vznikat	k5eAaImAgInS	vznikat
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
drží	držet	k5eAaImIp3nS	držet
devítičlenná	devítičlenný	k2eAgFnSc1d1	devítičlenná
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Stadtrat	Stadtrat	k1gFnSc1	Stadtrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k6eAd1	moc
125	[number]	k4	125
<g/>
členný	členný	k2eAgInSc1d1	členný
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Gemeinderat	Gemeinderat	k1gInSc1	Gemeinderat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgMnPc1	obojí
přímo	přímo	k6eAd1	přímo
volené	volená	k1gFnPc4	volená
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
tvoří	tvořit	k5eAaImIp3nP	tvořit
profesionální	profesionální	k2eAgMnPc1d1	profesionální
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgInSc1	jeden
odbor	odbor	k1gInSc1	odbor
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
představený	představený	k1gMnSc1	představený
výkonného	výkonný	k2eAgInSc2d1	výkonný
odboru	odbor	k1gInSc2	odbor
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
primátor	primátor	k1gMnSc1	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc4d1	současná
koalici	koalice	k1gFnSc4	koalice
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
liberálů	liberál	k1gMnPc2	liberál
a	a	k8xC	a
zelených	zelená	k1gFnPc2	zelená
vede	vést	k5eAaImIp3nS	vést
Corine	Corin	k1gInSc5	Corin
Mauch	Mauch	k1gMnSc1	Mauch
(	(	kIx(	(
<g/>
SD	SD	kA	SD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
jednou	jeden	k4xCgFnSc7	jeden
týdně	týdně	k6eAd1	týdně
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
placeni	platit	k5eAaImNgMnP	platit
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
zasedáních	zasedání	k1gNnPc6	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
radnici	radnice	k1gFnSc4	radnice
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
volené	volený	k2eAgInPc1d1	volený
orgány	orgán	k1gInPc1	orgán
kantonu	kanton	k1gInSc2	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
a	a	k8xC	a
převažuje	převažovat	k5eAaImIp3nS	převažovat
nad	nad	k7c4	nad
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
ji	on	k3xPp3gFnSc4	on
jednotná	jednotný	k2eAgFnSc1d1	jednotná
síť	síť	k1gFnSc1	síť
předměstských	předměstský	k2eAgFnPc2d1	předměstská
železnic	železnice	k1gFnPc2	železnice
(	(	kIx(	(
<g/>
S-Bahn	S-Bahn	k1gInSc1	S-Bahn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
13	[number]	k4	13
linek	linka	k1gFnPc2	linka
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
18	[number]	k4	18
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
6	[number]	k4	6
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
spojů	spoj	k1gInPc2	spoj
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
linkách	linka	k1gFnPc6	linka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
10	[number]	k4	10
spojů	spoj	k1gInPc2	spoj
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
spojuje	spojovat	k5eAaImIp3nS	spojovat
tramvaj	tramvaj	k1gFnSc1	tramvaj
letiště	letiště	k1gNnSc2	letiště
s	s	k7c7	s
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc4	síť
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
lodní	lodní	k2eAgInPc1d1	lodní
spoje	spoj	k1gInPc1	spoj
po	po	k7c6	po
jezeře	jezero	k1gNnSc6	jezero
i	i	k8xC	i
řece	řeka	k1gFnSc3	řeka
a	a	k8xC	a
několik	několik	k4yIc1	několik
lanovek	lanovka	k1gFnPc2	lanovka
do	do	k7c2	do
okolních	okolní	k2eAgInPc2d1	okolní
kopců	kopec	k1gInPc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Zürich	Züricha	k1gFnPc2	Züricha
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Kloten	Kloten	k2eAgMnSc1d1	Kloten
<g/>
,	,	kIx,	,
asi	asi	k9	asi
13	[number]	k4	13
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
24	[number]	k4	24
miliony	milion	k4xCgInPc1	milion
cestujících	cestující	k1gMnPc2	cestující
za	za	k7c4	za
rok	rok	k1gInSc4	rok
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
kontinenty	kontinent	k1gInPc7	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnPc1	letiště
intenzivně	intenzivně	k6eAd1	intenzivně
využívají	využívat	k5eAaPmIp3nP	využívat
společnosti	společnost	k1gFnPc1	společnost
Swiss	Swissa	k1gFnPc2	Swissa
(	(	kIx(	(
<g/>
Swiss	Swissa	k1gFnPc2	Swissa
International	International	k1gMnPc2	International
Air	Air	k1gMnPc2	Air
Lines	Linesa	k1gFnPc2	Linesa
a	a	k8xC	a
Swiss	Swissa	k1gFnPc2	Swissa
European	European	k1gMnSc1	European
Air	Air	k1gMnSc1	Air
Lines	Lines	k1gMnSc1	Lines
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alitalia	Alitalia	k1gFnSc1	Alitalia
a	a	k8xC	a
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
(	(	kIx(	(
<g/>
nazývá	nazývat	k5eAaImIp3nS	nazývat
ho	on	k3xPp3gMnSc4	on
třetím	třetí	k4xOgMnSc7	třetí
hubem	hub	k1gMnSc7	hub
<g/>
)	)	kIx)	)
a	a	k8xC	a
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgNnSc7d1	domovské
letištěm	letiště	k1gNnSc7	letiště
společností	společnost	k1gFnPc2	společnost
Belair	Belair	k1gInSc4	Belair
<g/>
,	,	kIx,	,
Edelweiss	Edelweiss	k1gInSc4	Edelweiss
Air	Air	k1gFnPc2	Air
a	a	k8xC	a
Helvetic	Helvetice	k1gFnPc2	Helvetice
Airways	Airwaysa	k1gFnPc2	Airwaysa
i	i	k9	i
základnou	základna	k1gFnSc7	základna
záchranné	záchranný	k2eAgFnSc2d1	záchranná
letecké	letecký	k2eAgFnSc2d1	letecká
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2900	[number]	k4	2900
vlakových	vlakový	k2eAgInPc2d1	vlakový
pohybů	pohyb	k1gInPc2	pohyb
a	a	k8xC	a
350-500	[number]	k4	350-500
tisíci	tisíc	k4xCgInPc7	tisíc
pasažéry	pasažér	k1gMnPc7	pasažér
denně	denně	k6eAd1	denně
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrušnější	rušný	k2eAgInSc4d3	nejrušnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
dalších	další	k2eAgNnPc2d1	další
16	[number]	k4	16
nádraží	nádraží	k1gNnPc2	nádraží
a	a	k8xC	a
10	[number]	k4	10
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Železnici	železnice	k1gFnSc4	železnice
provozují	provozovat	k5eAaImIp3nP	provozovat
Švýcarské	švýcarský	k2eAgFnPc1d1	švýcarská
železnice	železnice	k1gFnPc1	železnice
(	(	kIx(	(
<g/>
SBB	SBB	kA	SBB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
i	i	k9	i
přímé	přímý	k2eAgNnSc4d1	přímé
dálkové	dálkový	k2eAgNnSc4d1	dálkové
spojení	spojení	k1gNnSc4	spojení
vlaky	vlak	k1gInPc1	vlak
TGV	TGV	kA	TGV
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
ICE	ICE	kA	ICE
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
je	být	k5eAaImIp3nS	být
udržováno	udržovat	k5eAaImNgNnS	udržovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nočního	noční	k2eAgNnSc2d1	noční
spojení	spojení	k1gNnSc2	spojení
.	.	kIx.	.
</s>
<s>
Vtěsném	Vtěsný	k2eAgNnSc6d1	Vtěsný
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
dálnice	dálnice	k1gFnPc4	dálnice
A	a	k9	a
1	[number]	k4	1
(	(	kIx(	(
<g/>
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
a	a	k8xC	a
St.	st.	kA	st.
Gallen	Gallen	k1gInSc1	Gallen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A	a	k9	a
3	[number]	k4	3
(	(	kIx(	(
<g/>
Basilej	Basilej	k1gFnSc1	Basilej
a	a	k8xC	a
Sargans	Sargans	k1gInSc1	Sargans
<g/>
)	)	kIx)	)
a	a	k8xC	a
A	a	k9	a
4	[number]	k4	4
(	(	kIx(	(
<g/>
Schaffhausen	Schaffhausen	k1gInSc1	Schaffhausen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
se	se	k3xPyFc4	se
buduje	budovat	k5eAaImIp3nS	budovat
obchvat	obchvat	k1gInSc1	obchvat
kolem	kolem	k7c2	kolem
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Limmat	Limma	k1gNnPc2	Limma
protéká	protékat	k5eAaImIp3nS	protékat
starým	starý	k2eAgInSc7d1	starý
centrem	centr	k1gInSc7	centr
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
bývalého	bývalý	k2eAgNnSc2d1	bývalé
opevnění	opevnění	k1gNnSc2	opevnění
a	a	k8xC	a
vodní	vodní	k2eAgInSc4d1	vodní
příkop	příkop	k1gInSc4	příkop
Schanzengraben	Schanzengrabna	k1gFnPc2	Schanzengrabna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
centra	centrum	k1gNnSc2	centrum
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
Sihl	Sihla	k1gFnPc2	Sihla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Limmatu	Limmat	k1gInSc2	Limmat
leží	ležet	k5eAaImIp3nS	ležet
Niederdorf	Niederdorf	k1gInSc1	Niederdorf
<g/>
,	,	kIx,	,
živější	živý	k2eAgFnSc1d2	živější
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
restauracemi	restaurace	k1gFnPc7	restaurace
<g/>
,	,	kIx,	,
kiny	kino	k1gNnPc7	kino
a	a	k8xC	a
hudebními	hudební	k2eAgInPc7d1	hudební
kluby	klub	k1gInPc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
Živá	živý	k2eAgFnSc1d1	živá
je	být	k5eAaImIp3nS	být
také	také	k9	také
4	[number]	k4	4
<g/>
.	.	kIx.	.
čtvrť	čtvrť	k1gFnSc1	čtvrť
kolem	kolem	k7c2	kolem
Langstrasse	Langstrasse	k1gFnSc2	Langstrasse
<g/>
,	,	kIx,	,
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
národnostmi	národnost	k1gFnPc7	národnost
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
kulturami	kultura	k1gFnPc7	kultura
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prostituce	prostituce	k1gFnSc2	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Altstadt	Altstadt	k1gInSc1	Altstadt
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
s	s	k7c7	s
cechovními	cechovní	k2eAgInPc7d1	cechovní
domy	dům	k1gInPc7	dům
leží	ležet	k5eAaImIp3nP	ležet
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
Limmatu	Limmat	k1gInSc2	Limmat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
Lindenhof	Lindenhof	k1gInSc1	Lindenhof
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
doloženě	doloženě	k6eAd1	doloženě
osídlení	osídlení	k1gNnSc4	osídlení
nad	nad	k7c7	nad
Limmatem	Limma	k1gNnSc7	Limma
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
parkovou	parkový	k2eAgFnSc7d1	parková
úpravou	úprava	k1gFnSc7	úprava
na	na	k7c6	na
zbytcích	zbytek	k1gInPc6	zbytek
románského	románský	k2eAgInSc2d1	románský
hradu	hrad	k1gInSc2	hrad
s	s	k7c7	s
památníkem	památník	k1gInSc7	památník
<g/>
,	,	kIx,	,
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
obranu	obrana	k1gFnSc4	obrana
Curychu	Curych	k1gInSc6	Curych
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Lindenhofu	Lindenhof	k1gInSc2	Lindenhof
probíhá	probíhat	k5eAaImIp3nS	probíhat
luxusní	luxusní	k2eAgFnSc1d1	luxusní
Bahnhofstrasse	Bahnhofstrasse	k1gFnSc1	Bahnhofstrasse
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kostelů	kostel	k1gInPc2	kostel
je	být	k5eAaImIp3nS	být
protestantská	protestantský	k2eAgFnSc1d1	protestantská
<g/>
.	.	kIx.	.
</s>
<s>
Grossmünster	Grossmünster	k1gInSc1	Grossmünster
je	být	k5eAaImIp3nS	být
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
románská	románský	k2eAgFnSc1d1	románská
bazilika	bazilika	k1gFnSc1	bazilika
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1100	[number]	k4	1100
až	až	k6eAd1	až
1220	[number]	k4	1220
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
raně	raně	k6eAd1	raně
křesťanských	křesťanský	k2eAgInPc6d1	křesťanský
základech	základ	k1gInPc6	základ
s	s	k7c7	s
relikviemi	relikvie	k1gFnPc7	relikvie
svaých	svaých	k2eAgMnSc2d1	svaých
Felixe	Felix	k1gMnSc2	Felix
a	a	k8xC	a
Reguly	Regula	k1gFnSc2	Regula
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
řádu	řád	k1gInSc2	řád
augustiniánů	augustinián	k1gMnPc2	augustinián
-kanovníků	anovník	k1gInPc2	-kanovník
<g/>
.	.	kIx.	.
</s>
<s>
Vyniká	vynikat	k5eAaImIp3nS	vynikat
dvěma	dva	k4xCgInPc3	dva
pozdně	pozdně	k6eAd1	pozdně
gotickými	gotický	k2eAgFnPc7d1	gotická
věžemi	věž	k1gFnPc7	věž
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
,	,	kIx,	,
kupole	kupole	k1gFnSc1	kupole
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výklenku	výklenek	k1gInSc6	výklenek
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
2	[number]	k4	2
<g/>
.	.	kIx.	.
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
kopie	kopie	k1gFnSc1	kopie
sochy	socha	k1gFnSc2	socha
trůnícího	trůnící	k2eAgMnSc2d1	trůnící
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
chrám	chrám	k1gInSc1	chrám
založit	založit	k5eAaPmF	založit
<g/>
,	,	kIx,	,
když	když	k8xS	když
sem	sem	k6eAd1	sem
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
jelena	jelen	k1gMnSc4	jelen
a	a	k8xC	a
padl	padnout	k5eAaImAgMnS	padnout
zde	zde	k6eAd1	zde
jeho	jeho	k3xOp3gMnSc1	jeho
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
reformace	reformace	k1gFnSc2	reformace
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
si	se	k3xPyFc3	se
Uldrich	Uldrich	k1gMnSc1	Uldrich
Zwingli	Zwingle	k1gFnSc6	Zwingle
tento	tento	k3xDgInSc4	tento
kostel	kostel	k1gInSc4	kostel
vybral	vybrat	k5eAaPmAgMnS	vybrat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
odstranit	odstranit	k5eAaPmF	odstranit
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
i	i	k9	i
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Kirchenstrasse	Kirchenstrass	k1gInSc6	Kirchenstrass
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
chóru	chór	k1gInSc6	chór
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
gotické	gotický	k2eAgFnPc1d1	gotická
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
popravených	popravený	k2eAgMnPc2d1	popravený
svatých	svatý	k1gMnPc2	svatý
Felixe	Felix	k1gMnSc2	Felix
a	a	k8xC	a
Reguly	Regula	k1gFnSc2	Regula
<g/>
,	,	kIx,	,
držících	držící	k2eAgFnPc2d1	držící
své	svůj	k3xOyFgFnPc4	svůj
uťaté	uťatý	k2eAgFnPc4d1	uťatá
hlavy	hlava	k1gFnPc4	hlava
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
fragmenty	fragment	k1gInPc1	fragment
maleb	malba	k1gFnPc2	malba
zdobí	zdobit	k5eAaImIp3nP	zdobit
stěny	stěna	k1gFnPc1	stěna
halové	halový	k2eAgFnSc2d1	halová
krypty	krypta	k1gFnSc2	krypta
<g/>
.	.	kIx.	.
</s>
<s>
Rokem	rok	k1gInSc7	rok
1586	[number]	k4	1586
je	být	k5eAaImIp3nS	být
datována	datován	k2eAgFnSc1d1	datována
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
<g/>
.	.	kIx.	.
</s>
<s>
Románská	románský	k2eAgFnSc1d1	románská
křížová	křížový	k2eAgFnSc1d1	křížová
chodba	chodba	k1gFnSc1	chodba
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
zásadně	zásadně	k6eAd1	zásadně
dostavěna	dostavět	k5eAaPmNgFnS	dostavět
<g/>
.	.	kIx.	.
</s>
<s>
Fraumünster	Fraumünster	k1gInSc1	Fraumünster
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
gotická	gotický	k2eAgFnSc1d1	gotická
bazilika	bazilika	k1gFnSc1	bazilika
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
klášterní	klášterní	k2eAgInSc4d1	klášterní
kostel	kostel	k1gInSc4	kostel
benediktinek	benediktinka	k1gFnPc2	benediktinka
<g/>
,	,	kIx,	,
písemně	písemně	k6eAd1	písemně
doložený	doložený	k2eAgInSc1d1	doložený
roku	rok	k1gInSc3	rok
853	[number]	k4	853
jako	jako	k9	jako
fundace	fundace	k1gFnPc1	fundace
císaře	císař	k1gMnSc2	císař
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Němce	Němec	k1gMnSc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
abatyšemi	abatyše	k1gFnPc7	abatyše
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gFnPc4	jeho
dcery	dcera	k1gFnPc4	dcera
Hildegarda	Hildegarda	k1gFnSc1	Hildegarda
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
856	[number]	k4	856
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bertha	Bertha	k1gFnSc1	Bertha
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
877	[number]	k4	877
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
měl	mít	k5eAaImAgInS	mít
různá	různý	k2eAgNnPc4d1	různé
privilegia	privilegium	k1gNnPc4	privilegium
a	a	k8xC	a
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc6	století
mj.	mj.	kA	mj.
mincovní	mincovní	k2eAgNnSc1d1	mincovní
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
zbytky	zbytek	k1gInPc1	zbytek
gotické	gotický	k2eAgFnSc2d1	gotická
křížové	křížový	k2eAgFnSc2d1	křížová
chodby	chodba	k1gFnSc2	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
jsou	být	k5eAaImIp3nP	být
varhany	varhany	k1gFnPc1	varhany
na	na	k7c6	na
empoře	empora	k1gFnSc6	empora
<g/>
,	,	kIx,	,
s	s	k7c7	s
5793	[number]	k4	5793
píšťalami	píšťala	k1gFnPc7	píšťala
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgMnPc1d3	veliký
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
kantonu	kanton	k1gInSc6	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
zdobí	zdobit	k5eAaImIp3nS	zdobit
závěr	závěr	k1gInSc4	závěr
kostela	kostel	k1gInSc2	kostel
barevná	barevný	k2eAgNnPc1d1	barevné
okna	okno	k1gNnPc1	okno
<g/>
:	:	kIx,	:
tři	tři	k4xCgInPc1	tři
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Marca	Marcus	k1gMnSc2	Marcus
Chagalla	Chagall	k1gMnSc2	Chagall
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
podle	podle	k7c2	podle
Alberta	Albert	k1gMnSc2	Albert
Giacomettiho	Giacometti	k1gMnSc2	Giacometti
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgFnPc1d1	klášterní
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
strženy	strhnout	k5eAaPmNgFnP	strhnout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Münsterplatz	Münsterplatza	k1gFnPc2	Münsterplatza
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
chodníku	chodník	k1gInSc6	chodník
zasazena	zasazen	k2eAgFnSc1d1	zasazena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
uprchlíci	uprchlík	k1gMnPc1	uprchlík
(	(	kIx(	(
<g/>
exulanti	exulant	k1gMnPc1	exulant
<g/>
)	)	kIx)	)
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
děkují	děkovat	k5eAaImIp3nP	děkovat
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
domov	domov	k1gInSc4	domov
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupaci	okupace	k1gFnSc6	okupace
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
Peterskirche	Peterskirche	k1gInSc1	Peterskirche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
800	[number]	k4	800
<g/>
,	,	kIx,	,
písemně	písemně	k6eAd1	písemně
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
k	k	k7c3	k
roku	rok	k1gInSc3	rok
857	[number]	k4	857
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgInSc1d1	gotický
chór	chór	k1gInSc1	chór
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
z	z	k7c2	z
gotické	gotický	k2eAgFnSc2d1	gotická
etapy	etapa	k1gFnSc2	etapa
stavby	stavba	k1gFnSc2	stavba
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc4	kostel
byl	být	k5eAaImAgMnS	být
radikálně	radikálně	k6eAd1	radikálně
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1705	[number]	k4	1705
-	-	kIx~	-
1709	[number]	k4	1709
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věži	věž	k1gFnSc6	věž
jsou	být	k5eAaImIp3nP	být
renesanční	renesanční	k2eAgFnPc1d1	renesanční
hodiny	hodina	k1gFnPc1	hodina
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
a	a	k8xC	a
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
ciferníkem	ciferník	k1gInSc7	ciferník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
8,7	[number]	k4	8,7
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wasserkirche	Wasserkirche	k6eAd1	Wasserkirche
<g/>
,	,	kIx,	,
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
původně	původně	k6eAd1	původně
na	na	k7c6	na
samostatném	samostatný	k2eAgInSc6d1	samostatný
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
popravy	poprava	k1gFnSc2	poprava
sv.	sv.	kA	sv.
Felixe	Felix	k1gMnSc4	Felix
a	a	k8xC	a
Reguly	Regul	k1gMnPc4	Regul
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1634	[number]	k4	1634
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Predigerkirche	Predigerkirche	k1gInSc1	Predigerkirche
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgInSc1d1	bývalý
kostel	kostel	k1gInSc1	kostel
dominikánů	dominikán	k1gMnPc2	dominikán
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Schweizerisches	Schweizerisches	k1gInSc1	Schweizerisches
Landesmuseum	Landesmuseum	k1gInSc1	Landesmuseum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
romantická	romantický	k2eAgFnSc1d1	romantická
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
skrývá	skrývat	k5eAaImIp3nS	skrývat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
sbírek	sbírka	k1gFnPc2	sbírka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
Musée	Musé	k1gFnSc2	Musé
Suisse	Suisse	k1gFnSc2	Suisse
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
parku	park	k1gInSc2	park
Platzspitz	Platzspitza	k1gFnPc2	Platzspitza
naproti	naproti	k7c3	naproti
Hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
nádraží	nádraží	k1gNnSc3	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
umělecké	umělecký	k2eAgInPc4d1	umělecký
a	a	k8xC	a
historické	historický	k2eAgInPc4d1	historický
předměty	předmět	k1gInPc4	předmět
ze	z	k7c2	z
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgNnSc4d1	stříbrné
liturgické	liturgický	k2eAgNnSc4d1	liturgické
a	a	k8xC	a
votivní	votivní	k2eAgNnSc4d1	votivní
nádobí	nádobí	k1gNnSc4	nádobí
<g/>
,	,	kIx,	,
šperky	šperk	k1gInPc4	šperk
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
sbírka	sbírka	k1gFnSc1	sbírka
prstenů	prsten	k1gInPc2	prsten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
keramiku	keramika	k1gFnSc4	keramika
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
kamnových	kamnový	k2eAgInPc2d1	kamnový
kachlů	kachel	k1gInPc2	kachel
obdibí	obdibí	k1gNnSc2	obdibí
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nábytek	nábytek	k1gInSc1	nábytek
<g/>
,	,	kIx,	,
hodiny	hodina	k1gFnPc1	hodina
či	či	k8xC	či
hodinky	hodinka	k1gFnPc1	hodinka
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
gotické	gotický	k2eAgFnPc1d1	gotická
dřevořezby	dřevořezba	k1gFnPc1	dřevořezba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gotickou	gotický	k2eAgFnSc4d1	gotická
deskovou	deskový	k2eAgFnSc4d1	desková
malbu	malba	k1gFnSc4	malba
a	a	k8xC	a
grafiku	grafika	k1gFnSc4	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
je	být	k5eAaImIp3nS	být
sbíka	sbíka	k1gFnSc1	sbíka
oděvů	oděv	k1gInPc2	oděv
a	a	k8xC	a
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
také	také	k9	také
chrámová	chrámový	k2eAgNnPc4d1	chrámové
paramenta	paramenta	k1gNnPc4	paramenta
a	a	k8xC	a
gobelíny	gobelín	k1gInPc4	gobelín
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Kunsthaus	Kunsthaus	k1gInSc1	Kunsthaus
<g/>
)	)	kIx)	)
na	na	k7c4	na
Pfauenplatz	Pfauenplatz	k1gInSc4	Pfauenplatz
vyniká	vynikat	k5eAaImIp3nS	vynikat
bohatou	bohatý	k2eAgFnSc7d1	bohatá
sbírkou	sbírka	k1gFnSc7	sbírka
malby	malba	k1gFnSc2	malba
a	a	k8xC	a
plastiky	plastika	k1gFnSc2	plastika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
středověku	středověk	k1gInSc2	středověk
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
sbírka	sbírka	k1gFnSc1	sbírka
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
malíře	malíř	k1gMnSc2	malíř
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Hodlera	Hodler	k1gMnSc2	Hodler
a	a	k8xC	a
kolekce	kolekce	k1gFnSc2	kolekce
moderního	moderní	k2eAgNnSc2d1	moderní
francouzského	francouzský	k2eAgNnSc2d1	francouzské
malířství	malířství	k1gNnSc2	malířství
(	(	kIx(	(
<g/>
Cézanne	Cézann	k1gMnSc5	Cézann
<g/>
,	,	kIx,	,
Toulouse-Lautrec	Toulouse-Lautrec	k1gMnSc1	Toulouse-Lautrec
<g/>
,	,	kIx,	,
Renoir	Renoir	k1gMnSc1	Renoir
<g/>
,	,	kIx,	,
Degas	Degas	k1gMnSc1	Degas
<g/>
,	,	kIx,	,
Legér	Legér	k1gMnSc1	Legér
<g/>
,	,	kIx,	,
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
pro	pro	k7c4	pro
moderní	moderní	k2eAgNnSc4d1	moderní
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
designu	design	k1gInSc2	design
(	(	kIx(	(
<g/>
Museum	museum	k1gNnSc4	museum
für	für	k?	für
Gestaltung	Gestaltung	k1gInSc1	Gestaltung
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
budovách	budova	k1gFnPc6	budova
poblíž	poblíž	k6eAd1	poblíž
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
design	design	k1gInSc4	design
a	a	k8xC	a
užité	užitý	k2eAgNnSc4d1	užité
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
muzea	muzeum	k1gNnPc1	muzeum
Curyšské	curyšský	k2eAgFnSc2d1	curyšská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
muzea	muzeum	k1gNnPc4	muzeum
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
,	,	kIx,	,
zoologii	zoologie	k1gFnSc4	zoologie
a	a	k8xC	a
paleontologii	paleontologie	k1gFnSc4	paleontologie
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
volný	volný	k2eAgInSc4d1	volný
vstup	vstup	k1gInSc4	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
hodin	hodina	k1gFnPc2	hodina
-	-	kIx~	-
Uhrenmuseum	Uhrenmuseum	k1gNnSc1	Uhrenmuseum
Bayer	Bayer	k1gMnSc1	Bayer
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bahnhofstrasse	Bahnhofstrass	k1gInSc6	Bahnhofstrass
31	[number]	k4	31
<g/>
;	;	kIx,	;
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
soukromých	soukromý	k2eAgFnPc2d1	soukromá
sbírek	sbírka	k1gFnPc2	sbírka
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Centre	centr	k1gInSc5	centr
Le	Le	k1gFnPc3	Le
Corbusier	Corbusier	k1gInSc4	Corbusier
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
připomíná	připomínat	k5eAaImIp3nS	připomínat
dílo	dílo	k1gNnSc4	dílo
slavného	slavný	k2eAgMnSc2d1	slavný
architekta	architekt	k1gMnSc2	architekt
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
postavil	postavit	k5eAaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Rietbergovo	Rietbergův	k2eAgNnSc1d1	Rietbergův
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Gablerstrasse	Gablerstrass	k1gInSc6	Gablerstrass
15	[number]	k4	15
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
stavba	stavba	k1gFnSc1	stavba
původně	původně	k6eAd1	původně
předměstské	předměstský	k2eAgFnSc2d1	předměstská
vily	vila	k1gFnSc2	vila
Wessendock	Wessendocka	k1gFnPc2	Wessendocka
<g/>
,	,	kIx,	,
situovaná	situovaný	k2eAgFnSc1d1	situovaná
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
parku	park	k1gInSc6	park
<g/>
;	;	kIx,	;
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
sbírky	sbírka	k1gFnPc4	sbírka
mimoevropského	mimoevropský	k2eAgNnSc2d1	mimoevropské
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
barona	baron	k1gMnSc2	baron
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Heydta	Heydta	k1gFnSc1	Heydta
<g/>
;	;	kIx,	;
pořádá	pořádat	k5eAaImIp3nS	pořádat
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Stiftung	Stiftung	k1gMnSc1	Stiftung
Sammlung	Sammlung	k1gMnSc1	Sammlung
Emil	Emil	k1gMnSc1	Emil
Georg	Georg	k1gMnSc1	Georg
Bührle	Bührle	k1gFnSc1	Bührle
-	-	kIx~	-
soukromá	soukromý	k2eAgFnSc1d1	soukromá
sbírka	sbírka	k1gFnSc1	sbírka
umění	umění	k1gNnSc1	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
zdejší	zdejší	k2eAgMnSc1d1	zdejší
průmyslník	průmyslník	k1gMnSc1	průmyslník
Muzeum	muzeum	k1gNnSc1	muzeum
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
fotbalu	fotbal	k1gInSc2	fotbal
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
muzeí	muzeum	k1gNnPc2	muzeum
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
je	být	k5eAaImIp3nS	být
také	také	k9	také
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
Schauspielhaus	Schauspielhaus	k1gInSc1	Schauspielhaus
<g/>
)	)	kIx)	)
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
scénou	scéna	k1gFnSc7	scéna
(	(	kIx(	(
<g/>
Schiffbauhalle	Schiffbauhalle	k1gInSc1	Schiffbauhalle
<g/>
)	)	kIx)	)
v	v	k7c4	v
Zürich-West	Zürich-West	k1gFnSc4	Zürich-West
<g/>
,	,	kIx,	,
Operní	operní	k2eAgNnSc4d1	operní
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
Opernhaus	Opernhaus	k1gInSc1	Opernhaus
<g/>
)	)	kIx)	)
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
impozantní	impozantní	k2eAgFnSc1d1	impozantní
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
historismu	historismus	k1gInSc2	historismus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
památná	památný	k2eAgFnSc1d1	památná
vystoupeními	vystoupení	k1gNnPc7	vystoupení
Emy	Ema	k1gFnSc2	Ema
Destinnové	Destinnová	k1gFnSc2	Destinnová
a	a	k8xC	a
Gabriely	Gabriela	k1gFnSc2	Gabriela
Beňačkové	Beňačková	k1gFnSc2	Beňačková
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgInSc1d1	koncertní
sál	sál	k1gInSc1	sál
(	(	kIx(	(
<g/>
Tonhalle	Tonhalle	k1gInSc1	Tonhalle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnSc2d1	malá
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
kina	kino	k1gNnSc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
sídlí	sídlet	k5eAaImIp3nS	sídlet
FIFA	FIFA	kA	FIFA
(	(	kIx(	(
<g/>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
Football	Football	k1gInSc1	Football
Associations	Associations	k1gInSc1	Associations
<g/>
)	)	kIx)	)
a	a	k8xC	a
IIHF	IIHF	kA	IIHF
(	(	kIx(	(
<g/>
International	International	k1gMnSc2	International
Ice	Ice	k1gFnSc7	Ice
Hockey	Hockea	k1gFnSc2	Hockea
Federation	Federation	k1gInSc1	Federation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pořádá	pořádat	k5eAaImIp3nS	pořádat
tradiční	tradiční	k2eAgInSc1d1	tradiční
velký	velký	k2eAgInSc1d1	velký
atletický	atletický	k2eAgInSc1d1	atletický
mítink	mítink	k1gInSc1	mítink
Weltklasse	Weltklass	k1gMnSc2	Weltklass
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
součástí	součást	k1gFnPc2	součást
Diamantové	diamantový	k2eAgFnSc2d1	Diamantová
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
hostil	hostit	k5eAaImAgInS	hostit
Curych	Curych	k1gInSc1	Curych
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
stadiony	stadion	k1gInPc1	stadion
jsou	být	k5eAaImIp3nP	být
Hardturm	Hardturm	k1gInSc1	Hardturm
a	a	k8xC	a
Letzigrund	Letzigrund	k1gInSc1	Letzigrund
(	(	kIx(	(
<g/>
právě	právě	k6eAd1	právě
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
nový	nový	k2eAgInSc1d1	nový
Letzigrund	Letzigrund	k1gInSc1	Letzigrund
<g/>
)	)	kIx)	)
odvěkých	odvěký	k2eAgMnPc2d1	odvěký
rivalů	rival	k1gMnPc2	rival
FCZ	FCZ	kA	FCZ
a	a	k8xC	a
GCZ	GCZ	kA	GCZ
<g/>
.	.	kIx.	.
</s>
<s>
Hokejové	hokejový	k2eAgInPc4d1	hokejový
stadiony	stadion	k1gInPc4	stadion
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc2d1	sportovní
haly	hala	k1gFnSc2	hala
mj.	mj.	kA	mj.
klubu	klub	k1gInSc2	klub
ZSC	ZSC	kA	ZSC
Lions	Lions	k1gInSc1	Lions
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
je	být	k5eAaImIp3nS	být
polytechnika	polytechnika	k1gFnSc1	polytechnika
ETH	ETH	kA	ETH
<g/>
,	,	kIx,	,
špičková	špičkový	k2eAgFnSc1d1	špičková
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímž	jejíž	k3xOyRp3gFnPc3	jejíž
budovám	budova	k1gFnPc3	budova
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
vede	vést	k5eAaImIp3nS	vést
lanovka	lanovka	k1gFnSc1	lanovka
podobná	podobný	k2eAgFnSc1d1	podobná
petřínské	petřínský	k2eAgFnSc3d1	Petřínská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedenáctém	jedenáctý	k4xOgInSc6	jedenáctý
poschodí	poschodí	k1gNnSc6	poschodí
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
ETH	ETH	kA	ETH
je	být	k5eAaImIp3nS	být
upravena	upraven	k2eAgFnSc1d1	upravena
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
plošina	plošina	k1gFnSc1	plošina
s	s	k7c7	s
krásným	krásný	k2eAgInSc7d1	krásný
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
staré	starý	k2eAgNnSc4d1	staré
město	město	k1gNnSc4	město
až	až	k9	až
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Uni	Uni	k?	Uni
(	(	kIx(	(
<g/>
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
ústavy	ústav	k1gInPc4	ústav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působili	působit	k5eAaImAgMnP	působit
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
mnozí	mnohý	k2eAgMnPc1d1	mnohý
nositelé	nositel	k1gMnPc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1915	[number]	k4	1915
tam	tam	k6eAd1	tam
přednášel	přednášet	k5eAaImAgMnS	přednášet
i	i	k9	i
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Kchun-ming	Kchuning	k1gInSc1	Kchun-ming
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
