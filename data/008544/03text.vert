<p>
<s>
Hniezdne	Hniezdnout	k5eAaPmIp3nS	Hniezdnout
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Gnézda	Gnézda	k1gFnSc1	Gnézda
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Kniesen	Kniesen	k2eAgMnSc1d1	Kniesen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Stará	starý	k2eAgFnSc1d1	stará
Ľubovňa	Ľubovňa	k1gFnSc1	Ľubovňa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
427	[number]	k4	427
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1286	[number]	k4	1286
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Popradu	Poprad	k1gInSc2	Poprad
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ľubovňanské	Ľubovňanský	k2eAgFnSc6d1	Ľubovňanský
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malé	malý	k2eAgNnSc1d1	malé
rekreační	rekreační	k2eAgNnSc1d1	rekreační
středisko	středisko	k1gNnSc1	středisko
Údolie	Údolie	k1gFnSc2	Údolie
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hniezdne	Hniezdne	k1gFnSc2	Hniezdne
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hniezdne	Hniezdne	k1gFnSc2	Hniezdne
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
obce	obec	k1gFnSc2	obec
</s>
</p>
