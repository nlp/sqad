<s>
Největší	veliký	k2eAgFnSc7d3	veliký
želvou	želva	k1gFnSc7	želva
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
kožatka	kožatka	k1gFnSc1	kožatka
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
želva	želva	k1gFnSc1	želva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
váží	vážit	k5eAaImIp3nS	vážit
i	i	k9	i
přes	přes	k7c4	přes
900	[number]	k4	900
kg	kg	kA	kg
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
krunýř	krunýř	k1gInSc1	krunýř
je	být	k5eAaImIp3nS	být
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
