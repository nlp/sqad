<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Poodří	Poodří	k1gNnSc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
PoodříIUCN	PoodříIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Rybník	rybník	k1gInSc1
Kotvice	kotvice	k1gFnSc2
v	v	k7c6
CHKO	CHKO	kA
PoodříZákladní	PoodříZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
212	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
81,5	81,5	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc6
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
5,8	5,8	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Poodří	Poodří	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
85	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.poodri.ochranaprirody.cz	www.poodri.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Poodří	Poodří	k1gNnSc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
novému	nový	k2eAgNnSc3d1
vyhlášení	vyhlášení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
Nařízením	nařízení	k1gNnSc7
vlády	vláda	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
č.	č.	k?
51	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Poodří	Poodří	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
byl	být	k5eAaImAgInS
zároveň	zároveň	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
původní	původní	k2eAgInSc1d1
vyhlašovací	vyhlašovací	k2eAgInSc1d1
předpis	předpis	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
81,5	81,5	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
harmonicky	harmonicky	k6eAd1
utvářená	utvářený	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
nivy	niva	k1gFnSc2
řeky	řeka	k1gFnSc2
Odry	Odra	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gInPc2
přítoků	přítok	k1gInPc2
se	s	k7c7
zachovanými	zachovaný	k2eAgInPc7d1
přírodními	přírodní	k2eAgInPc7d1
procesy	proces	k1gInPc7
přirozeného	přirozený	k2eAgInSc2d1
nivního	nivní	k2eAgInSc2d1
ekosystému	ekosystém	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
typickým	typický	k2eAgInSc7d1
krajinným	krajinný	k2eAgInSc7d1
rázem	ráz	k1gInSc7
tvořeným	tvořený	k2eAgInSc7d1
mozaikou	mozaika	k1gFnSc7
enkláv	enkláva	k1gFnPc2
lučních	luční	k2eAgInPc2d1
aluviálních	aluviální	k2eAgInPc2d1
porostů	porost	k1gInPc2
<g/>
,	,	kIx,
porostů	porost	k1gInPc2
lužního	lužní	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
značným	značný	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
dřevin	dřevina	k1gFnPc2
rostoucích	rostoucí	k2eAgFnPc2d1
mimo	mimo	k7c4
les	les	k1gInSc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
se	s	k7c7
starými	starý	k2eAgNnPc7d1
rameny	rameno	k1gNnPc7
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
trvalými	trvalý	k2eAgFnPc7d1
a	a	k8xC
periodickými	periodický	k2eAgFnPc7d1
tůněmi	tůně	k1gFnPc7
<g/>
,	,	kIx,
prameništi	prameniště	k1gNnPc7
ve	v	k7c6
svazích	svah	k1gInPc6
říčních	říční	k2eAgFnPc2d1
teras	terasa	k1gFnPc2
a	a	k8xC
rybníky	rybník	k1gInPc4
s	s	k7c7
druhově	druhově	k6eAd1
pestrou	pestrý	k2eAgFnSc7d1
florou	flora	k1gFnSc7
a	a	k8xC
faunou	fauna	k1gFnSc7
s	s	k7c7
funkcí	funkce	k1gFnSc7
významné	významný	k2eAgFnSc2d1
tahové	tahový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
vodních	vodní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
a	a	k8xC
s	s	k7c7
přírodními	přírodní	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
krajiny	krajina	k1gFnSc2
spočívajícími	spočívající	k2eAgInPc7d1
v	v	k7c6
zachovalé	zachovalý	k2eAgFnSc6d1
dynamice	dynamika	k1gFnSc6
přirozených	přirozený	k2eAgInPc2d1
říčních	říční	k2eAgInPc2d1
procesů	proces	k1gInPc2
meandrujících	meandrující	k2eAgInPc2d1
toků	tok	k1gInPc2
a	a	k8xC
režimu	režim	k1gInSc2
povrchových	povrchový	k2eAgInPc2d1
rozlivů	rozliv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
také	také	k9
mokřadní	mokřadní	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
a	a	k8xC
na	na	k7c4
ně	on	k3xPp3gFnPc4
vázané	vázaný	k2eAgFnPc4d1
vzácné	vzácný	k2eAgFnPc4d1
a	a	k8xC
zvláště	zvláště	k6eAd1
chráněné	chráněný	k2eAgInPc4d1
druhy	druh	k1gInPc4
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
rozmístění	rozmístění	k1gNnSc1
a	a	k8xC
urbanistická	urbanistický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
dochovaných	dochovaný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
historického	historický	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
předměty	předmět	k1gInPc1
ochrany	ochrana	k1gFnSc2
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
Poodří	Poodří	k1gNnSc2
a	a	k8xC
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
Cihelna	cihelna	k1gFnSc1
Kunín	Kunín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
Studénce	studénka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgInPc1d1
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Moravskoslezském	moravskoslezský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
a	a	k8xC
zasahuje	zasahovat	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
a	a	k8xC
Ostrava-město	Ostrava-města	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
podél	podél	k7c2
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Odry	Odra	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Jeseník	Jeseník	k1gInSc1
nad	nad	k7c7
Odrou	Odra	k1gFnSc7
a	a	k8xC
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
její	její	k3xOp3gFnSc3
údolní	údolní	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
v	v	k7c6
ní	on	k3xPp3gFnSc6
nejsou	být	k5eNaImIp3nP
velké	velký	k2eAgInPc4d1
výškové	výškový	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Polanském	Polanský	k1gMnSc6
lese	les	k1gInSc6
–	–	k?
214,1	214,1	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
západně	západně	k6eAd1
od	od	k7c2
Kunína	Kunín	k1gInSc2
–	–	k?
293,5	293,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Klimatické	klimatický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
mírně	mírně	k6eAd1
teplé	teplý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
asi	asi	k9
7	#num#	k4
<g/>
–	–	k?
<g/>
8,5	8,5	k4
°	°	k?
<g/>
C.	C.	kA
Za	za	k7c4
rok	rok	k1gInSc4
zde	zde	k6eAd1
spadne	spadnout	k5eAaPmIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
okolo	okolo	k7c2
700	#num#	k4
mm	mm	kA
srážek	srážka	k1gFnPc2
a	a	k8xC
okolo	okolo	k7c2
1	#num#	k4
m	m	kA
sněhu	sníh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Rostlinstvo	rostlinstvo	k1gNnSc1
</s>
<s>
Žebratka	žebratka	k1gFnSc1
bahenní	bahenní	k2eAgFnSc2d1
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c6
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
jak	jak	k6eAd1
vodních	vodní	k2eAgNnPc2d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
lučních	luční	k2eAgFnPc2d1
a	a	k8xC
lesních	lesní	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
rybníků	rybník	k1gInPc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
porosty	porost	k1gInPc1
ostřice	ostřice	k1gFnSc2
a	a	k8xC
rákosu	rákos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
v	v	k7c6
rybnících	rybník	k1gInPc6
rostou	růst	k5eAaImIp3nP
vzácné	vzácný	k2eAgFnPc1d1
vodní	vodní	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
kotvice	kotvice	k1gFnSc2
plovoucí	plovoucí	k2eAgFnPc1d1
<g/>
,	,	kIx,
nepukalka	nepukalka	k1gFnSc1
plovoucí	plovoucí	k2eAgFnSc2d1
a	a	k8xC
vodní	vodní	k2eAgFnSc2d1
masožravé	masožravý	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
bublinatky	bublinatka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
slepých	slepý	k2eAgNnPc2d1
říčních	říční	k2eAgNnPc2d1
ramen	rameno	k1gNnPc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
například	například	k6eAd1
ohrožená	ohrožený	k2eAgFnSc1d1
žebratka	žebratka	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
a	a	k8xC
kvete	kvést	k5eAaImIp3nS
zde	zde	k6eAd1
stulík	stulík	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
typické	typický	k2eAgInPc1d1
také	také	k9
periodicky	periodicky	k6eAd1
zaplavované	zaplavovaný	k2eAgFnPc4d1
nivní	nivní	k2eAgFnPc4d1
louky	louka	k1gFnPc4
s	s	k7c7
charakteristickými	charakteristický	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesy	les	k1gInPc7
zabírají	zabírat	k5eAaImIp3nP
asi	asi	k9
10	#num#	k4
%	%	kIx~
území	území	k1gNnSc2
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tvořeny	tvořen	k2eAgFnPc1d1
převážně	převážně	k6eAd1
dřevinami	dřevina	k1gFnPc7
jako	jako	k8xC,k8xS
habr	habr	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
či	či	k8xC
jasan	jasan	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bylinném	bylinný	k2eAgNnSc6d1
patře	patro	k1gNnSc6
lesů	les	k1gInPc2
roste	růst	k5eAaImIp3nS
například	například	k6eAd1
sněženka	sněženka	k1gFnSc1
podsněžník	podsněžník	k1gInSc1
<g/>
,	,	kIx,
sasanka	sasanka	k1gFnSc1
hajní	hajní	k2eAgFnSc1d1
<g/>
,	,	kIx,
orsej	orsej	k1gInSc1
jarní	jarní	k2eAgInSc1d1
<g/>
,	,	kIx,
česnek	česnek	k1gInSc1
medvědí	medvědí	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Města	město	k1gNnSc2
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc1
alespoň	alespoň	k9
z	z	k7c2
části	část	k1gFnSc2
leží	ležet	k5eAaImIp3nS
města	město	k1gNnSc2
Studénka	studénka	k1gFnSc1
a	a	k8xC
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mykoflóra	Mykoflóra	k6eAd1
</s>
<s>
Oblast	oblast	k1gFnSc1
Poodří	Poodří	k1gNnSc2
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
výskytem	výskyt	k1gInSc7
řady	řada	k1gFnSc2
vzácných	vzácný	k2eAgFnPc2d1
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
teplomilných	teplomilný	k2eAgInPc2d1
druhů	druh	k1gInPc2
hub	houba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dřevokazných	dřevokazný	k2eAgInPc2d1
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
například	například	k6eAd1
trsnatec	trsnatec	k1gMnSc1
lupenitý	lupenitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Grifola	Grifola	k1gFnSc1
frondosa	frondosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
choroš	choroš	k1gInSc1
oříš	oříš	k1gInSc1
(	(	kIx(
<g/>
Polyporus	Polyporus	k1gMnSc1
umbellatus	umbellatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kotrč	kotrč	k1gInSc4
Němcův	Němcův	k2eAgInSc4d1
(	(	kIx(
<g/>
Sparassis	Sparassis	k1gInSc4
nemecii	nemecie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ohňovec	ohňovec	k1gInSc4
Hartigův	Hartigův	k2eAgInSc4d1
(	(	kIx(
<g/>
Phellinus	Phellinus	k1gInSc4
hartigii	hartigie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rezavec	rezavec	k1gInSc1
dubový	dubový	k2eAgInSc1d1
(	(	kIx(
<g/>
Inonotus	Inonotus	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
dryadeus	dryadeus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rezavec	rezavec	k1gMnSc1
dubomilný	dubomilný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Inonotus	Inonotus	k1gMnSc1
dryophilus	dryophilus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lesklokorka	lesklokorka	k1gFnSc1
pryskyřičnatá	pryskyřičnatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ganoderma	Ganoderma	k1gFnSc1
resinaceum	resinaceum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lesklokorka	lesklokorka	k1gFnSc1
lesklá	lesklý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ganoderma	Ganoderma	k1gFnSc1
lucidum	lucidum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
troudnatec	troudnatec	k1gMnSc1
jasanový	jasanový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Perenniporia	Perenniporium	k1gNnSc2
fraxinea	fraxine	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
korálovec	korálovec	k1gMnSc1
bukový	bukový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Hericium	Hericium	k1gNnSc1
coralloides	coralloidesa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
korálovec	korálovec	k1gMnSc1
ježatý	ježatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Hericium	Hericium	k1gNnSc1
erinaceus	erinaceus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
korálovec	korálovec	k1gMnSc1
jedlový	jedlový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Hericium	Hericium	k1gNnSc1
flagellum	flagellum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
hrotnatka	hrotnatka	k1gFnSc1
zápašná	zápašný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sarcodontia	Sarcodontia	k1gFnSc1
crocea	crocea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Živočišstvo	živočišstvo	k1gNnSc1
</s>
<s>
V	v	k7c6
rybnících	rybník	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
řece	řeka	k1gFnSc6
Odře	odřít	k5eAaPmIp3nS
i	i	k9
v	v	k7c6
tůních	tůně	k1gFnPc6
žije	žít	k5eAaImIp3nS
mnoho	mnoho	k4c1
druhů	druh	k1gInPc2
vodních	vodní	k2eAgMnPc2d1
měkkýšů	měkkýš	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
ohrožený	ohrožený	k2eAgMnSc1d1
velevrub	velevrub	k1gMnSc1
tupý	tupý	k2eAgMnSc1d1
či	či	k8xC
svinutec	svinutec	k1gInSc1
tenký	tenký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poodří	Poodří	k1gNnSc1
hostí	hostit	k5eAaImIp3nP
také	také	k9
dva	dva	k4xCgInPc1
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
korýšů	korýš	k1gMnPc2
-	-	kIx~
raka	rak	k1gMnSc2
říčního	říční	k2eAgMnSc2d1
a	a	k8xC
žábronožku	žábronožka	k1gFnSc4
sněžní	sněžní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohatá	bohatý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
hmyzí	hmyzí	k2eAgFnSc1d1
fauna	fauna	k1gFnSc1
<g/>
,	,	kIx,
nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
mnohé	mnohý	k2eAgInPc1d1
lokálnější	lokální	k2eAgInPc1d2
druhy	druh	k1gInPc1
jako	jako	k8xC,k8xS
modrásek	modrásek	k1gMnSc1
bahenní	bahenní	k2eAgMnSc1d1
<g/>
,	,	kIx,
ohniváček	ohniváček	k1gMnSc1
černočárný	černočárný	k2eAgMnSc1d1
<g/>
,	,	kIx,
roháč	roháč	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
páchník	páchník	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
či	či	k8xC
saranče	saranče	k1gFnSc1
Stetophyma	Stetophyma	k1gFnSc1
grossum	grossum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
řece	řeka	k1gFnSc6
Odře	Odra	k1gFnSc6
žijí	žít	k5eAaImIp3nP
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
ryb	ryba	k1gFnPc2
ouklejka	ouklejka	k1gFnSc1
pruhovaná	pruhovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
hořavka	hořavka	k1gFnSc1
hořká	hořká	k1gFnSc1
či	či	k8xC
střevle	střevle	k1gFnSc1
potoční	potoční	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
tůních	tůně	k1gFnPc6
pak	pak	k6eAd1
piskoř	piskoř	k1gMnSc1
pruhovaný	pruhovaný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc6
žije	žít	k5eAaImIp3nS
mnoho	mnoho	k4c1
druhů	druh	k1gInPc2
obojživelníků	obojživelník	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
hlediska	hledisko	k1gNnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
patří	patřit	k5eAaImIp3nS
čolek	čolek	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
blatnice	blatnice	k1gFnSc1
skvrnitá	skvrnitý	k2eAgFnSc1d1
a	a	k8xC
skokan	skokan	k1gMnSc1
ostronosý	ostronosý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
rybníky	rybník	k1gInPc7
a	a	k8xC
mokřady	mokřad	k1gInPc7
oblíbeným	oblíbený	k2eAgNnSc7d1
útočištěm	útočiště	k1gNnSc7
vodních	vodní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
hnízdí	hnízdit	k5eAaImIp3nS
zde	zde	k6eAd1
například	například	k6eAd1
volavka	volavka	k1gFnSc1
popelavá	popelavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
kormorán	kormorán	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
potápka	potápka	k1gFnSc1
rudokrká	rudokrkat	k5eAaPmIp3nS
<g/>
,	,	kIx,
zrzohlávka	zrzohlávka	k1gFnSc1
rudozobá	rudozobý	k2eAgFnSc1d1
<g/>
,	,	kIx,
morčák	morčák	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
kopřivka	kopřivka	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
husa	husa	k1gFnSc1
velká	velká	k1gFnSc1
<g/>
,	,	kIx,
bukač	bukač	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
chřástal	chřástal	k1gMnSc1
polní	polní	k2eAgMnSc1d1
či	či	k8xC
moták	moták	k1gMnSc1
pochop	pochop	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
k	k	k7c3
vidění	vidění	k1gNnSc3
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zde	zde	k6eAd1
sice	sice	k8xC
nehnízdí	hnízdit	k5eNaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
nějakou	nějaký	k3yIgFnSc4
část	část	k1gFnSc4
roku	rok	k1gInSc2
zde	zde	k6eAd1
pobývají	pobývat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
chráněných	chráněný	k2eAgInPc2d1
druhů	druh	k1gInPc2
savců	savec	k1gMnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
například	například	k6eAd1
bobr	bobr	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
a	a	k8xC
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenán	k2eAgNnSc1d1
16	#num#	k4
druhů	druh	k1gInPc2
netopýrů	netopýr	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
76	#num#	k4
<g/>
%	%	kIx~
netopýří	netopýří	k2eAgFnSc2d1
fauny	fauna	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Památná	památný	k2eAgNnPc1d1
místa	místo	k1gNnPc1
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
Albrechtice	Albrechtice	k1gFnPc1
(	(	kIx(
<g/>
Velké	velký	k2eAgFnPc1d1
Albrechtice	Albrechtice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
NPR	NPR	kA
</s>
<s>
Polanská	Polanská	k1gFnSc1
niva	niva	k1gFnSc1
</s>
<s>
PR	pr	k0
</s>
<s>
Bartošovický	Bartošovický	k2eAgInSc1d1
luh	luh	k1gInSc1
</s>
<s>
Bařiny	bařina	k1gFnPc1
</s>
<s>
Bažantula	Bažantula	k1gFnSc1
</s>
<s>
Koryta	koryto	k1gNnPc1
</s>
<s>
Kotvice	kotvice	k1gFnSc1
</s>
<s>
Polanský	Polanský	k1gMnSc1
les	les	k1gInSc4
</s>
<s>
Rákosina	rákosina	k1gFnSc1
</s>
<s>
Rezavka	Rezavka	k1gFnSc1
</s>
<s>
PP	PP	kA
</s>
<s>
Meandry	meandr	k1gInPc1
staré	starý	k2eAgFnSc2d1
Odry	Odra	k1gFnSc2
</s>
<s>
Pusté	pustý	k2eAgFnPc1d1
nivy	niva	k1gFnPc1
(	(	kIx(
<g/>
zrušena	zrušen	k2eAgFnSc1d1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nařízení	nařízení	k1gNnSc1
vlády	vláda	k1gFnSc2
č.	č.	k?
51	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
,	,	kIx,
O	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Poodří	Poodří	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DECKEROVÁ	DECKEROVÁ	kA
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřevokazné	dřevokazný	k2eAgFnPc4d1
houby	houba	k1gFnPc4
Poodří	Poodří	k1gNnSc2
–	–	k?
část	část	k1gFnSc1
I.	I.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
JAROŠEK	JAROŠEK	kA
<g/>
,	,	kIx,
Radim	Radim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poodří	Poodří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Společnost	společnost	k1gFnSc1
přátel	přítel	k1gMnPc2
Poodří	Poodří	k1gNnSc2
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
2338	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Poodří	Poodří	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Poodří	Poodří	k1gNnSc2
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
lesu	les	k1gInSc3
v	v	k7c6
Poodří	Poodří	k1gNnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Beskydy	Beskydy	k1gFnPc1
•	•	k?
Poodří	Poodří	k1gNnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Radhošť	Radhošť	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Šipka	šipka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bartošovický	Bartošovický	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Bařiny	Bařina	k1gMnSc2
•	•	k?
Bažantula	Bažantul	k1gMnSc2
•	•	k?
Huštýn	Huštýn	k1gInSc1
•	•	k?
Koryta	koryto	k1gNnSc2
•	•	k?
Kotvice	kotvice	k1gFnSc1
•	•	k?
Královec	Královec	k1gInSc1
•	•	k?
Noříčí	Noříčí	k2eAgInSc1d1
•	•	k?
Rákosina	rákosina	k1gFnSc1
•	•	k?
Rybníky	rybník	k1gInPc1
v	v	k7c6
Trnávce	Trnávka	k1gFnSc6
•	•	k?
Suchá	suchý	k2eAgFnSc1d1
Dora	Dora	k1gFnSc1
•	•	k?
Svinec	svinec	k1gInSc1
•	•	k?
Trojačka	Trojačka	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Domorazské	Domorazský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Kamenárka	Kamenárka	k1gFnSc1
•	•	k?
Meandry	meandr	k1gInPc1
Staré	Staré	k2eAgInPc1d1
Odry	odr	k1gInPc1
•	•	k?
Na	na	k7c6
Čermence	Čermenka	k1gFnSc6
•	•	k?
Pikritové	Pikritový	k2eAgFnSc6d1
mandlovce	mandlovka	k1gFnSc6
u	u	k7c2
Kojetína	Kojetín	k1gInSc2
•	•	k?
Polštářové	polštářový	k2eAgFnSc2d1
lávy	láva	k1gFnSc2
ve	v	k7c6
Straníku	straník	k1gMnSc6
•	•	k?
Prameny	pramen	k1gInPc1
Zrzávky	Zrzávka	k1gFnSc2
•	•	k?
Pusté	pustý	k2eAgFnSc2d1
nivy	niva	k1gFnSc2
•	•	k?
Sedlnické	Sedlnický	k2eAgFnSc2d1
sněženky	sněženka	k1gFnSc2
•	•	k?
Stříbrné	stříbrný	k2eAgNnSc4d1
jezírko	jezírko	k1gNnSc4
•	•	k?
Travertinová	travertinový	k2eAgFnSc1d1
kaskáda	kaskáda	k1gFnSc1
•	•	k?
Váňův	Váňův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Vrásový	vrásový	k2eAgInSc1d1
soubor	soubor	k1gInSc1
v	v	k7c4
Klokočůvku	Klokočůvka	k1gFnSc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Ostrava-město	Ostrava-města	k1gMnSc5
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Poodří	Poodří	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Polanská	Polanská	k1gFnSc1
niva	niva	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Landek	Landek	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Polanský	Polanský	k1gMnSc1
les	les	k1gInSc1
•	•	k?
Přemyšov	Přemyšov	k1gInSc1
•	•	k?
Rezavka	Rezavka	k1gFnSc1
•	•	k?
Štěpán	Štěpán	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
památky	památka	k1gFnPc4
</s>
<s>
Heřmanický	Heřmanický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Kunčický	kunčický	k2eAgInSc1d1
bludný	bludný	k2eAgInSc1d1
balvan	balvan	k1gInSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
u	u	k7c2
rondelu	rondel	k1gInSc2
•	•	k?
Porubský	Porubský	k1gMnSc1
bludný	bludný	k2eAgInSc4d1
balvan	balvan	k1gInSc4
•	•	k?
Rovninské	Rovninský	k2eAgInPc4d1
balvany	balvan	k1gInPc4
•	•	k?
Turkov	Turkov	k1gInSc1
•	•	k?
Václavovice	Václavovice	k1gFnSc1
-	-	kIx~
pískovna	pískovna	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
|	|	kIx~
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
