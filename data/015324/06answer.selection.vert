<s desamb="1">
K	k	k7c3
novému	nový	k2eAgNnSc3d1
vyhlášení	vyhlášení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
Nařízením	nařízení	k1gNnSc7
vlády	vláda	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
č.	č.	k?
51	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Poodří	Poodří	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
byl	být	k5eAaImAgInS
zároveň	zároveň	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
původní	původní	k2eAgInSc1d1
vyhlašovací	vyhlašovací	k2eAgInSc1d1
předpis	předpis	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
81,5	81,5	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>