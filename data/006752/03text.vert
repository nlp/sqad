<s>
Brambůrky	brambůrka	k1gFnPc1	brambůrka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bramborové	bramborový	k2eAgInPc1d1	bramborový
lupínky	lupínek	k1gInPc1	lupínek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgFnPc1d1	drobná
pochutiny	pochutina	k1gFnPc1	pochutina
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
z	z	k7c2	z
plátků	plátek	k1gInPc2	plátek
brambor	brambora	k1gFnPc2	brambora
smažené	smažený	k2eAgFnSc2d1	smažená
a	a	k8xC	a
dochucené	dochucený	k2eAgFnSc2d1	dochucená
solí	sůl	k1gFnSc7	sůl
nebo	nebo	k8xC	nebo
dalšími	další	k2eAgFnPc7d1	další
jinými	jiný	k2eAgFnPc7d1	jiná
ingrediencemi	ingredience	k1gFnPc7	ingredience
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
brambůrky	brambůrek	k1gInPc4	brambůrek
prý	prý	k9	prý
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1853	[number]	k4	1853
indiánský	indiánský	k2eAgInSc1d1	indiánský
kuchař	kuchař	k1gMnSc1	kuchař
George	George	k1gNnPc2	George
Crum	Cru	k1gNnSc7	Cru
v	v	k7c4	v
Moon	Moon	k1gInSc4	Moon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lake	Lake	k1gNnSc7	Lake
House	house	k1gNnSc4	house
poblíž	poblíž	k7c2	poblíž
Saratoga	Saratog	k1gMnSc2	Saratog
Springs	Springsa	k1gFnPc2	Springsa
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Kuchař	Kuchař	k1gMnSc1	Kuchař
byl	být	k5eAaImAgMnS	být
rozezlen	rozezlít	k5eAaPmNgMnS	rozezlít
zákazníkem	zákazník	k1gMnSc7	zákazník
(	(	kIx(	(
<g/>
populární	populární	k2eAgInSc1d1	populární
mýtus	mýtus	k1gInSc1	mýtus
chybně	chybně	k6eAd1	chybně
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
tohoto	tento	k3xDgMnSc4	tento
zákazníka	zákazník	k1gMnSc4	zákazník
Cornelia	Cornelius	k1gMnSc4	Cornelius
Vanderbilta	Vanderbilt	k1gMnSc4	Vanderbilt
nebo	nebo	k8xC	nebo
Neila	Neil	k1gMnSc4	Neil
Vanderbilta	Vanderbilt	k1gMnSc4	Vanderbilt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
objednal	objednat	k5eAaPmAgMnS	objednat
francouzské	francouzský	k2eAgInPc4d1	francouzský
brambory	brambor	k1gInPc4	brambor
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
vrátit	vrátit	k5eAaPmF	vrátit
kuchaři	kuchař	k1gMnPc1	kuchař
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
plátky	plátek	k1gInPc1	plátek
brambor	brambora	k1gFnPc2	brambora
moc	moc	k6eAd1	moc
silné	silný	k2eAgFnPc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Crum	Crum	k1gInSc1	Crum
nakrájel	nakrájet	k5eAaPmAgInS	nakrájet
plátky	plátek	k1gInPc4	plátek
brambor	brambora	k1gFnPc2	brambora
tak	tak	k8xS	tak
tenké	tenký	k2eAgFnSc2d1	tenká
<g/>
,	,	kIx,	,
že	že	k8xS	že
nešly	jít	k5eNaImAgFnP	jít
uvařit	uvařit	k5eAaPmF	uvařit
v	v	k7c6	v
rozpáleném	rozpálený	k2eAgInSc6d1	rozpálený
oleji	olej	k1gInSc6	olej
v	v	k7c6	v
kotlíku	kotlík	k1gInSc6	kotlík
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
usmažil	usmažit	k5eAaPmAgMnS	usmažit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
je	být	k5eAaImIp3nS	být
naschvál	naschvál	k6eAd1	naschvál
přesolil	přesolit	k5eAaPmAgMnS	přesolit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Crum	Crum	k1gMnSc1	Crum
očekával	očekávat	k5eAaImAgMnS	očekávat
kritiku	kritika	k1gFnSc4	kritika
<g/>
,	,	kIx,	,
hosté	host	k1gMnPc1	host
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pochutiny	pochutina	k1gFnSc2	pochutina
<g/>
.	.	kIx.	.
</s>
<s>
Chipsy	chips	k1gInPc1	chips
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
staly	stát	k5eAaPmAgInP	stát
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
chodem	chod	k1gInSc7	chod
na	na	k7c6	na
menu	menu	k1gNnSc6	menu
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Saratoga	Saratoga	k1gFnSc1	Saratoga
Chips	chips	k1gInSc1	chips
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgInSc2	tento
příběhu	příběh	k1gInSc2	příběh
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
objev	objev	k1gInSc1	objev
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kelloggovi	Kelloggův	k2eAgMnPc1d1	Kelloggův
(	(	kIx(	(
<g/>
bratru	bratru	k9	bratru
zakladatele	zakladatel	k1gMnSc2	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kellogg	Kellogg	k1gMnSc1	Kellogg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Brambůrky	brambůrek	k1gInPc7	brambůrek
jsou	být	k5eAaImIp3nP	být
nezdravá	zdravý	k2eNgFnSc1d1	nezdravá
strava	strava	k1gFnSc1	strava
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
ke	k	k7c3	k
špatné	špatný	k2eAgFnSc3d1	špatná
životosprávě	životospráva	k1gFnSc3	životospráva
<g/>
,	,	kIx,	,
obezitě	obezita	k1gFnSc3	obezita
a	a	k8xC	a
celulitidě	celulitida	k1gFnSc3	celulitida
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Technické	technický	k2eAgFnSc2d1	technická
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
poprvé	poprvé	k6eAd1	poprvé
prokázali	prokázat	k5eAaPmAgMnP	prokázat
přítomnost	přítomnost	k1gFnSc4	přítomnost
rakovinotvorné	rakovinotvorný	k2eAgFnSc2d1	rakovinotvorná
látky	látka	k1gFnSc2	látka
–	–	k?	–
glycidamidu	glycidamid	k1gInSc2	glycidamid
–	–	k?	–
ve	v	k7c6	v
smažených	smažený	k2eAgInPc6d1	smažený
hranolcích	hranolec	k1gInPc6	hranolec
a	a	k8xC	a
chipsech	chips	k1gInPc6	chips
<g/>
.	.	kIx.	.
</s>
<s>
Glycidamid	Glycidamid	k1gInSc1	Glycidamid
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
brambor	brambora	k1gFnPc2	brambora
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
nebezpečnější	bezpečný	k2eNgMnSc1d2	nebezpečnější
než	než	k8xS	než
v	v	k7c6	v
hranolcích	hranolec	k1gInPc6	hranolec
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
objevený	objevený	k2eAgInSc4d1	objevený
akrylamid	akrylamid	k1gInSc4	akrylamid
<g/>
.	.	kIx.	.
</s>
<s>
Glycidamid	Glycidamid	k1gInSc1	Glycidamid
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výrobcích	výrobek	k1gInPc6	výrobek
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
od	od	k7c2	od
0,3	[number]	k4	0,3
do	do	k7c2	do
1,5	[number]	k4	1,5
mikrogramu	mikrogram	k1gInSc2	mikrogram
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
v	v	k7c6	v
deseti	deset	k4xCc6	deset
vzorcích	vzorec	k1gInPc6	vzorec
bramborových	bramborový	k2eAgInPc2d1	bramborový
lupínků	lupínek	k1gInPc2	lupínek
a	a	k8xC	a
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
druzích	druh	k1gInPc6	druh
hranolků	hranolek	k1gInPc2	hranolek
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
výroby	výroba	k1gFnSc2	výroba
domácích	domácí	k2eAgFnPc2d1	domácí
brambůrek	brambůrka	k1gFnPc2	brambůrka
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Potato	Potat	k2eAgNnSc1d1	Potato
chip	chip	k1gMnSc1	chip
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Bohemia	bohemia	k1gFnSc1	bohemia
Chips	chips	k1gInSc4	chips
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brambůrky	brambůrka	k1gFnSc2	brambůrka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Chipsy	chips	k1gInPc1	chips
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
rizikové	rizikový	k2eAgFnPc1d1	riziková
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
až	až	k9	až
40	[number]	k4	40
procent	procento	k1gNnPc2	procento
tuku	tuk	k1gInSc2	tuk
Test	test	k1gInSc1	test
brambůrků	brambůrek	k1gInPc2	brambůrek
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
