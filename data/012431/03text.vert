<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
povídková	povídkový	k2eAgFnSc1d1	povídková
kniha	kniha	k1gFnSc1	kniha
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Listen	listen	k1gInSc1	listen
z	z	k7c2	z
edice	edice	k1gFnSc2	edice
Česká	český	k2eAgFnSc1d1	Česká
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
svazek	svazek	k1gInSc1	svazek
17	[number]	k4	17
nebo	nebo	k8xC	nebo
18	[number]	k4	18
-	-	kIx~	-
v	v	k7c6	v
ediční	ediční	k2eAgFnSc6d1	ediční
poznámce	poznámka	k1gFnSc6	poznámka
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
11	[number]	k4	11
povídek	povídka	k1gFnPc2	povídka
různých	různý	k2eAgMnPc2d1	různý
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
na	na	k7c4	na
zadané	zadaný	k2eAgNnSc4d1	zadané
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
na	na	k7c6	na
titulu	titul	k1gInSc6	titul
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
Luďka	Luděk	k1gMnSc2	Luděk
Bárty	Bárta	k1gMnSc2	Bárta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povídky	povídka	k1gFnPc1	povídka
==	==	k?	==
</s>
</p>
<p>
<s>
Halina	Halina	k1gFnSc1	Halina
Pawlowská	Pawlowská	k1gFnSc1	Pawlowská
–	–	k?	–
Jenom	jenom	k9	jenom
kousek	kousek	k1gInSc4	kousek
k	k	k7c3	k
lásce	láska	k1gFnSc3	láska
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
–	–	k?	–
Smilstvo	smilstvo	k1gNnSc4	smilstvo
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Šabach	Šabacha	k1gFnPc2	Šabacha
–	–	k?	–
Království	království	k1gNnSc1	království
za	za	k7c4	za
story	story	k1gFnSc4	story
</s>
</p>
<p>
<s>
Iva	Iva	k1gFnSc1	Iva
Pekárková	Pekárková	k1gFnSc1	Pekárková
–	–	k?	–
Špendlík	špendlík	k1gInSc1	špendlík
na	na	k7c4	na
krokodýla	krokodýl	k1gMnSc4	krokodýl
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Kantůrková	Kantůrková	k1gFnSc1	Kantůrková
–	–	k?	–
U	u	k7c2	u
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Hauserová	Hauserová	k1gFnSc1	Hauserová
–	–	k?	–
Deníček	deníček	k1gInSc4	deníček
sebevražedkyně	sebevražedkyně	k1gFnSc2	sebevražedkyně
</s>
</p>
<p>
<s>
Daniela	Daniela	k1gFnSc1	Daniela
Fischerová	Fischerová	k1gFnSc1	Fischerová
–	–	k?	–
Jednooká	jednooký	k2eAgFnSc1d1	jednooká
<g/>
,	,	kIx,	,
jednoruká	jednoruký	k2eAgFnSc1d1	jednoruká
a	a	k8xC	a
já	já	k3xPp1nSc1	já
</s>
</p>
<p>
<s>
Irena	Irena	k1gFnSc1	Irena
Dousková	Dousková	k1gFnSc1	Dousková
–	–	k?	–
Potřetí	potřetí	k4xO	potřetí
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnPc4	Libuše
Koubská	Koubský	k2eAgFnSc5d1	Koubská
–	–	k?	–
Ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Olga	Olga	k1gFnSc1	Olga
Walló	Walló	k1gFnSc2	Walló
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Nosková	Nosková	k1gFnSc1	Nosková
–	–	k?	–
Falešný	falešný	k2eAgInSc1d1	falešný
Mičurin	Mičurin	k1gInSc1	Mičurin
</s>
</p>
<p>
<s>
==	==	k?	==
Nakladatelské	nakladatelský	k2eAgInPc1d1	nakladatelský
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
Listen	listen	k1gInSc1	listen
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
ISBN	ISBN	kA	ISBN
978-80-86526-30-0	[number]	k4	978-80-86526-30-0
</s>
</p>
