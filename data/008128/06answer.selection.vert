<s>
Mlýn	mlýn	k1gInSc1	mlýn
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
technologické	technologický	k2eAgNnSc1d1	Technologické
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
mletí	mletí	k1gNnSc3	mletí
obilí	obilí	k1gNnSc2	obilí
na	na	k7c4	na
mouku	mouka	k1gFnSc4	mouka
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
produkty	produkt	k1gInPc4	produkt
nebo	nebo	k8xC	nebo
k	k	k7c3	k
mletí	mletí	k1gNnSc3	mletí
rudy	ruda	k1gFnSc2	ruda
(	(	kIx(	(
<g/>
kulový	kulový	k2eAgInSc1d1	kulový
mlýn	mlýn	k1gInSc1	mlýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jiných	jiný	k2eAgFnPc2d1	jiná
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
či	či	k8xC	či
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
