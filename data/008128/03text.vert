<s>
Mlýn	mlýn	k1gInSc1	mlýn
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
technologické	technologický	k2eAgNnSc1d1	Technologické
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
mletí	mletí	k1gNnSc3	mletí
obilí	obilí	k1gNnSc2	obilí
na	na	k7c4	na
mouku	mouka	k1gFnSc4	mouka
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
produkty	produkt	k1gInPc4	produkt
nebo	nebo	k8xC	nebo
k	k	k7c3	k
mletí	mletí	k1gNnSc3	mletí
rudy	ruda	k1gFnSc2	ruda
(	(	kIx(	(
<g/>
kulový	kulový	k2eAgInSc1d1	kulový
mlýn	mlýn	k1gInSc1	mlýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jiných	jiný	k2eAgFnPc2d1	jiná
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
či	či	k8xC	či
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
této	tento	k3xDgFnSc2	tento
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
mlecí	mlecí	k2eAgInSc1d1	mlecí
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
válcové	válcový	k2eAgFnPc1d1	válcová
stolice	stolice	k1gFnPc1	stolice
nebo	nebo	k8xC	nebo
mlýnské	mlýnský	k2eAgInPc1d1	mlýnský
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
typu	typ	k1gInSc2	typ
výsledných	výsledný	k2eAgInPc2d1	výsledný
produktů	produkt	k1gInPc2	produkt
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
kromě	kromě	k7c2	kromě
pohonného	pohonný	k2eAgNnSc2d1	pohonné
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
rozvodů	rozvod	k1gInPc2	rozvod
také	také	k9	také
ostatní	ostatní	k2eAgInPc4d1	ostatní
mlýnské	mlýnský	k2eAgInPc4d1	mlýnský
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stroje	stroj	k1gInPc4	stroj
na	na	k7c4	na
čištění	čištění	k1gNnSc4	čištění
zrna	zrno	k1gNnSc2	zrno
(	(	kIx(	(
<g/>
aspiratér	aspiratér	k1gMnSc1	aspiratér
<g/>
,	,	kIx,	,
tarár	tarár	k1gInSc1	tarár
<g/>
,	,	kIx,	,
žejbro	žejbro	k1gNnSc1	žejbro
<g/>
,	,	kIx,	,
koukolník	koukolník	k1gInSc1	koukolník
-	-	kIx~	-
trieur	trieur	k1gMnSc1	trieur
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
na	na	k7c6	na
loupání	loupání	k1gNnSc6	loupání
zrna	zrno	k1gNnSc2	zrno
-	-	kIx~	-
loupačky	loupačka	k1gFnPc1	loupačka
<g/>
,	,	kIx,	,
třídění	třídění	k1gNnSc1	třídění
rozemletých	rozemletý	k2eAgInPc2d1	rozemletý
produktů	produkt	k1gInPc2	produkt
-	-	kIx~	-
vysévače	vysévač	k1gInPc1	vysévač
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
čištění	čištění	k1gNnSc2	čištění
a	a	k8xC	a
třídění	třídění	k1gNnSc2	třídění
krupice	krupice	k1gFnSc2	krupice
-	-	kIx~	-
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
slouží	sloužit	k5eAaImIp3nS	sloužit
kapsové	kapsový	k2eAgInPc4d1	kapsový
a	a	k8xC	a
šnekové	šnekový	k2eAgInPc4d1	šnekový
dopravníky	dopravník	k1gInPc4	dopravník
nebo	nebo	k8xC	nebo
pneumatické	pneumatický	k2eAgNnSc4d1	pneumatické
-	-	kIx~	-
vzduchové	vzduchový	k2eAgNnSc4d1	vzduchové
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Mlecí	mlecí	k2eAgInSc1d1	mlecí
proces	proces	k1gInSc1	proces
ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
lze	lze	k6eAd1	lze
velmi	velmi	k6eAd1	velmi
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
okruhů	okruh	k1gInPc2	okruh
<g/>
:	:	kIx,	:
čištění	čištění	k1gNnSc2	čištění
zrna	zrno	k1gNnSc2	zrno
<g/>
,	,	kIx,	,
loupání	loupání	k1gNnSc2	loupání
zrna	zrno	k1gNnSc2	zrno
<g/>
,	,	kIx,	,
mletí	mletí	k1gNnSc1	mletí
(	(	kIx(	(
<g/>
rozemílání	rozemílání	k1gNnSc1	rozemílání
<g/>
,	,	kIx,	,
šrotování	šrotování	k1gNnSc1	šrotování
<g/>
,	,	kIx,	,
vymílání	vymílání	k1gNnSc1	vymílání
<g/>
,	,	kIx,	,
domílka	domílka	k1gFnSc1	domílka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třídění	třídění	k1gNnSc1	třídění
(	(	kIx(	(
<g/>
vysévání	vysévání	k1gNnSc1	vysévání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc4	čištění
krupice	krupice	k1gFnSc2	krupice
a	a	k8xC	a
popřípadě	popřípadě	k6eAd1	popřípadě
ještě	ještě	k6eAd1	ještě
dodatečné	dodatečný	k2eAgFnSc2d1	dodatečná
úpravy	úprava	k1gFnSc2	úprava
např.	např.	kA	např.
míchání	míchání	k1gNnSc2	míchání
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
několik	několik	k4yIc4	několik
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
členěny	členit	k5eAaImNgInP	členit
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gInPc4	on
pohání	pohánět	k5eAaImIp3nS	pohánět
<g/>
:	:	kIx,	:
vodní	vodní	k2eAgInSc1d1	vodní
mlýn	mlýn	k1gInSc1	mlýn
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
mlýna	mlýn	k1gInSc2	mlýn
je	být	k5eAaImIp3nS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
vodním	vodní	k2eAgInSc7d1	vodní
tokem	tok	k1gInSc7	tok
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
rozlišován	rozlišovat	k5eAaImNgInS	rozlišovat
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
náhonu	náhon	k1gInSc2	náhon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
žene	hnát	k5eAaImIp3nS	hnát
mlýnské	mlýnský	k2eAgNnSc4d1	mlýnské
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
pohonem	pohon	k1gInSc7	pohon
vodních	vodní	k2eAgInPc2d1	vodní
mlýnů	mlýn	k1gInPc2	mlýn
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
horní	horní	k2eAgInSc1d1	horní
náhon	náhon	k1gInSc1	náhon
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgFnSc1d1	vrchní
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
-	-	kIx~	-
voda	voda	k1gFnSc1	voda
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
lopatky	lopatka	k1gFnPc4	lopatka
mlýnského	mlýnský	k2eAgNnSc2d1	mlýnské
kola	kolo	k1gNnSc2	kolo
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
dolní	dolní	k2eAgInSc1d1	dolní
náhon	náhon	k1gInSc1	náhon
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnSc1d1	spodní
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
-	-	kIx~	-
voda	voda	k1gFnSc1	voda
teče	téct	k5eAaImIp3nS	téct
pod	pod	k7c7	pod
kolem	kolo	k1gNnSc7	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
lopatky	lopatka	k1gFnPc4	lopatka
<g/>
.	.	kIx.	.
střední	střední	k2eAgFnSc1d1	střední
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
vodní	vodní	k2eAgFnSc1d1	vodní
kola	kola	k1gFnSc1	kola
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
nátokem	nátok	k1gInSc7	nátok
větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
do	do	k7c2	do
lopatek	lopatka	k1gFnPc2	lopatka
větrného	větrný	k2eAgInSc2d1	větrný
mlýna	mlýn	k1gInSc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
krajem	kraj	k1gInSc7	kraj
větrných	větrný	k2eAgInPc2d1	větrný
mlýnů	mlýn	k1gInPc2	mlýn
je	být	k5eAaImIp3nS	být
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
poněkud	poněkud	k6eAd1	poněkud
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
těchto	tento	k3xDgInPc2	tento
mlýnů	mlýn	k1gInPc2	mlýn
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
větrem	vítr	k1gInSc7	vítr
hnaná	hnaný	k2eAgNnPc4d1	hnané
vodní	vodní	k2eAgNnPc4d1	vodní
čerpadla	čerpadlo	k1gNnPc4	čerpadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hamr	hamr	k1gInSc1	hamr
Sovovy	Sovův	k2eAgInPc1d1	Sovův
mlýny	mlýn	k1gInPc1	mlýn
Winternitzovy	Winternitzův	k2eAgInPc1d1	Winternitzův
mlýny	mlýn	k1gInPc1	mlýn
Molinologie	Molinologie	k1gFnSc2	Molinologie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mlýn	mlýn	k1gInSc1	mlýn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mlýn	mlýn	k1gInSc1	mlýn
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Vodní	vodní	k2eAgInPc1d1	vodní
mlýny	mlýn	k1gInPc1	mlýn
Těšitelová	Těšitelová	k1gFnSc1	Těšitelová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
starých	starý	k2eAgInPc2d1	starý
českých	český	k2eAgInPc2d1	český
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
46	[number]	k4	46
<g/>
,	,	kIx,	,
s.	s.	k?	s.
185	[number]	k4	185
<g/>
-	-	kIx~	-
<g/>
193	[number]	k4	193
<g/>
.	.	kIx.	.
</s>
