<p>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
početnější	početní	k2eAgNnSc4d2	početnější
hudební	hudební	k2eAgNnSc4d1	hudební
instrumentální	instrumentální	k2eAgNnSc4d1	instrumentální
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
alespoň	alespoň	k9	alespoň
hlavní	hlavní	k2eAgInPc1d1	hlavní
nástroje	nástroj	k1gInPc1	nástroj
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgMnSc7	jeden
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
menších	malý	k2eAgNnPc2d2	menší
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
duo	duo	k1gNnSc1	duo
<g/>
,	,	kIx,	,
kvarteto	kvarteto	k1gNnSc1	kvarteto
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
orchestr	orchestr	k1gInSc1	orchestr
většinou	většinou	k6eAd1	většinou
řídí	řídit	k5eAaImIp3nS	řídit
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
a	a	k8xC	a
původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
orchestr	orchestr	k1gInSc1	orchestr
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
(	(	kIx(	(
<g/>
Johann	Johann	k1gMnSc1	Johann
Mattheson	Mattheson	k1gMnSc1	Mattheson
<g/>
:	:	kIx,	:
Das	Das	k1gMnSc1	Das
neu-eröffnete	uröffnout	k5eNaPmIp2nP	u-eröffnout
Orchestre	orchestr	k1gInSc5	orchestr
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
1713	[number]	k4	1713
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ὀ	ὀ	k?	ὀ
(	(	kIx(	(
<g/>
orchéstra	orchéstra	k1gFnSc1	orchéstra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
divadle	divadlo	k1gNnSc6	divadlo
půlkruhový	půlkruhový	k2eAgInSc4d1	půlkruhový
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
předscénou	předscéna	k1gFnSc7	předscéna
(	(	kIx(	(
<g/>
proscéniem	proscénium	k1gNnSc7	proscénium
<g/>
)	)	kIx)	)
a	a	k8xC	a
hledištěm	hlediště	k1gNnSc7	hlediště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
divadelních	divadelní	k2eAgNnPc6d1	divadelní
představeních	představení	k1gNnPc6	představení
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
chór	chór	k1gInSc1	chór
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
evropské	evropský	k2eAgNnSc1d1	Evropské
divadlo	divadlo	k1gNnSc1	divadlo
obnovovalo	obnovovat	k5eAaImAgNnS	obnovovat
<g/>
,	,	kIx,	,
umístili	umístit	k5eAaPmAgMnP	umístit
sem	sem	k6eAd1	sem
soubor	soubor	k1gInSc4	soubor
doprovázejících	doprovázející	k2eAgMnPc2d1	doprovázející
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
orchestr	orchestr	k1gInSc1	orchestr
<g/>
"	"	kIx"	"
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
stylových	stylový	k2eAgNnPc6d1	stylové
obdobích	období	k1gNnPc6	období
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
renesanci	renesance	k1gFnSc3	renesance
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
slovo	slovo	k1gNnSc1	slovo
capella	capella	k1gFnSc1	capella
(	(	kIx(	(
<g/>
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vokalisty	vokalista	k1gMnPc7	vokalista
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
instrumentálním	instrumentální	k2eAgInSc7d1	instrumentální
doprovodem	doprovod	k1gInSc7	doprovod
čili	čili	k8xC	čili
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
doprovázejících	doprovázející	k2eAgFnPc2d1	doprovázející
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
výraz	výraz	k1gInSc1	výraz
přenesen	přenést	k5eAaPmNgInS	přenést
na	na	k7c4	na
samostatný	samostatný	k2eAgInSc4d1	samostatný
instrumentální	instrumentální	k2eAgInSc4d1	instrumentální
ansámbl	ansámbl	k1gInSc4	ansámbl
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
"	"	kIx"	"
<g/>
a	a	k8xC	a
capella	capella	k6eAd1	capella
<g/>
"	"	kIx"	"
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgInSc4d1	barokní
orchestr	orchestr	k1gInSc4	orchestr
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
fundamentu	fundament	k1gInSc2	fundament
(	(	kIx(	(
<g/>
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
,	,	kIx,	,
fagot	fagot	k1gInSc1	fagot
<g/>
,	,	kIx,	,
loutna	loutna	k1gFnSc1	loutna
<g/>
,	,	kIx,	,
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrchních	vrchní	k2eAgInPc2d1	vrchní
melodických	melodický	k2eAgInPc2d1	melodický
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
hoboj	hoboj	k1gInSc1	hoboj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapelník	kapelník	k1gMnSc1	kapelník
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
cembala	cembalo	k1gNnSc2	cembalo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
provedení	provedení	k1gNnSc4	provedení
skladby	skladba	k1gFnSc2	skladba
také	také	k9	také
například	například	k6eAd1	například
jako	jako	k8xS	jako
houslista	houslista	k1gMnSc1	houslista
(	(	kIx(	(
<g/>
Antonio	Antonio	k1gMnSc1	Antonio
Vivaldi	Vivald	k1gMnPc5	Vivald
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
používal	používat	k5eAaImAgMnS	používat
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
úzu	úzus	k1gInSc2	úzus
dirigentské	dirigentský	k2eAgNnSc1d1	dirigentské
hole	hole	k1gNnSc1	hole
(	(	kIx(	(
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gMnPc4	Lull
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orchestry	orchestra	k1gFnPc1	orchestra
mívaly	mívat	k5eAaImAgFnP	mívat
kolem	kolem	k7c2	kolem
dvaceti	dvacet	k4xCc2	dvacet
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
již	již	k9	již
také	také	k9	také
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
příležitostném	příležitostný	k2eAgNnSc6d1	příležitostné
vystoupení	vystoupení	k1gNnSc6	vystoupení
mnohem	mnohem	k6eAd1	mnohem
početnějších	početní	k2eAgNnPc2d2	početnější
uskupení	uskupení	k1gNnPc2	uskupení
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
sta	sto	k4xCgNnSc2	sto
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
orchestru	orchestr	k1gInSc2	orchestr
např.	např.	kA	např.
Berlín	Berlín	k1gInSc1	Berlín
a	a	k8xC	a
především	především	k9	především
Mannheim	Mannheim	k1gInSc4	Mannheim
a	a	k8xC	a
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
uplatňovali	uplatňovat	k5eAaImAgMnP	uplatňovat
čeští	český	k2eAgMnPc1d1	český
hudebníci	hudebník	k1gMnPc1	hudebník
(	(	kIx(	(
<g/>
bratři	bratr	k1gMnPc1	bratr
František	František	k1gMnSc1	František
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Bendové	Benda	k1gMnPc1	Benda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Stamic	Stamic	k1gMnSc1	Stamic
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Stamic	Stamic	k1gMnSc1	Stamic
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Richter	Richter	k1gMnSc1	Richter
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřhlasé	čtyřhlasý	k2eAgNnSc1d1	čtyřhlasé
obsazení	obsazení	k1gNnSc1	obsazení
smyčců	smyčec	k1gInPc2	smyčec
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violy	viola	k1gFnPc4	viola
a	a	k8xC	a
cella	cello	k1gNnSc2	cello
s	s	k7c7	s
kontrabasy	kontrabas	k1gInPc7	kontrabas
<g/>
)	)	kIx)	)
a	a	k8xC	a
zdvojení	zdvojení	k1gNnSc1	zdvojení
dřev	dřevo	k1gNnPc2	dřevo
(	(	kIx(	(
2	[number]	k4	2
flétny	flétna	k1gFnSc2	flétna
<g/>
,	,	kIx,	,
2	[number]	k4	2
hoboje	hoboj	k1gInPc1	hoboj
<g/>
,	,	kIx,	,
2	[number]	k4	2
klarinety	klarinet	k1gInPc1	klarinet
<g/>
,	,	kIx,	,
2	[number]	k4	2
fagoty	fagot	k1gInPc7	fagot
<g/>
;	;	kIx,	;
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
klasicismu	klasicismus	k1gInSc6	klasicismus
2	[number]	k4	2
hoboje	hoboj	k1gFnSc2	hoboj
<g/>
,	,	kIx,	,
2	[number]	k4	2
horny	horna	k1gFnPc1	horna
<g/>
)	)	kIx)	)
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
obohacována	obohacovat	k5eAaImNgFnS	obohacovat
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
v	v	k7c6	v
období	období	k1gNnSc6	období
romantismu	romantismus	k1gInSc2	romantismus
zejména	zejména	k9	zejména
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
žesťů	žestě	k1gInPc2	žestě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
především	především	k9	především
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
bicích	bicí	k2eAgFnPc2d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
aktuální	aktuální	k2eAgFnSc1d1	aktuální
nástrojové	nástrojový	k2eAgNnSc4d1	nástrojové
složení	složení	k1gNnSc4	složení
orchestru	orchestr	k1gInSc2	orchestr
zcela	zcela	k6eAd1	zcela
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
dané	daný	k2eAgFnSc3d1	daná
partituře	partitura	k1gFnSc3	partitura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
předepisovat	předepisovat	k5eAaImF	předepisovat
například	například	k6eAd1	například
elektrofonické	elektrofonický	k2eAgInPc4d1	elektrofonický
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
digitálně	digitálně	k6eAd1	digitálně
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
zvuk	zvuk	k1gInSc1	zvuk
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc1	klasifikace
orchestrů	orchestr	k1gInPc2	orchestr
==	==	k?	==
</s>
</p>
<p>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
orchestru	orchestra	k1gFnSc4	orchestra
vycházejí	vycházet	k5eAaImIp3nP	vycházet
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakých	jaký	k3yIgInPc2	jaký
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
skládá	skládat	k5eAaImIp3nS	skládat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
orchestr	orchestr	k1gInSc1	orchestr
nebo	nebo	k8xC	nebo
dechový	dechový	k2eAgInSc1d1	dechový
orchestr	orchestr	k1gInSc1	orchestr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
jsou	být	k5eAaImIp3nP	být
nástroje	nástroj	k1gInPc1	nástroj
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
(	(	kIx(	(
<g/>
komorní	komorní	k2eAgInSc1d1	komorní
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jakému	jaký	k3yQgInSc3	jaký
druhu	druh	k1gInSc3	druh
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
věnuje	věnovat	k5eAaImIp3nS	věnovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jazzový	jazzový	k2eAgMnSc1d1	jazzový
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgInSc1d1	taneční
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
<g/>
,	,	kIx,	,
mozartovský	mozartovský	k2eAgMnSc1d1	mozartovský
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
,	,	kIx,	,
scénický	scénický	k2eAgInSc1d1	scénický
orchestr	orchestr	k1gInSc1	orchestr
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
základní	základní	k2eAgNnPc1d1	základní
kritéria	kritérion	k1gNnPc1	kritérion
se	se	k3xPyFc4	se
často	často	k6eAd1	často
prolínají	prolínat	k5eAaImIp3nP	prolínat
a	a	k8xC	a
při	při	k7c6	při
specifikaci	specifikace	k1gFnSc6	specifikace
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
hudebních	hudební	k2eAgNnPc2d1	hudební
těles	těleso	k1gNnPc2	těleso
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
další	další	k2eAgNnPc4d1	další
hlediska	hledisko	k1gNnPc4	hledisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symfonický	symfonický	k2eAgInSc4d1	symfonický
(	(	kIx(	(
<g/>
filharmonický	filharmonický	k2eAgInSc4d1	filharmonický
<g/>
)	)	kIx)	)
orchestr	orchestr	k1gInSc4	orchestr
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
<g/>
)	)	kIx)	)
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
často	často	k6eAd1	často
také	také	k9	také
jako	jako	k9	jako
filharmonický	filharmonický	k2eAgInSc4d1	filharmonický
(	(	kIx(	(
<g/>
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
vždy	vždy	k6eAd1	vždy
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgMnPc6d1	tvořící
nástrojové	nástrojový	k2eAgFnPc4d1	nástrojová
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
sekce	sekce	k1gFnSc1	sekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsazení	obsazení	k1gNnSc1	obsazení
orchestru	orchestr	k1gInSc2	orchestr
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
ustálené	ustálený	k2eAgFnSc6d1	ustálená
posloupnosti	posloupnost	k1gFnSc6	posloupnost
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
pořadí	pořadí	k1gNnSc4	pořadí
instrumentálních	instrumentální	k2eAgInPc2d1	instrumentální
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dechové	dechový	k2eAgInPc1d1	dechový
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
flétny	flétna	k1gFnPc1	flétna
a	a	k8xC	a
pikola	pikola	k1gFnSc1	pikola
<g/>
,	,	kIx,	,
hoboje	hoboj	k1gFnPc1	hoboj
a	a	k8xC	a
anglický	anglický	k2eAgInSc1d1	anglický
roh	roh	k1gInSc1	roh
<g/>
,	,	kIx,	,
klarinety	klarinet	k1gInPc1	klarinet
<g/>
,	,	kIx,	,
fagoty	fagot	k1gInPc1	fagot
a	a	k8xC	a
kontrafagot	kontrafagot	k1gInSc1	kontrafagot
</s>
</p>
<p>
<s>
Dechové	dechový	k2eAgInPc4d1	dechový
žesťové	žesťový	k2eAgInPc4d1	žesťový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
:	:	kIx,	:
lesní	lesní	k2eAgInPc4d1	lesní
rohy	roh	k1gInPc4	roh
(	(	kIx(	(
<g/>
horny	horna	k1gFnPc4	horna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trubky	trubka	k1gFnPc4	trubka
<g/>
,	,	kIx,	,
pozouny	pozouna	k1gFnPc4	pozouna
<g/>
,	,	kIx,	,
tuba	tuba	k1gFnSc1	tuba
</s>
</p>
<p>
<s>
Bicí	bicí	k2eAgInPc4d1	bicí
<g/>
:	:	kIx,	:
tympány	tympán	k1gInPc4	tympán
(	(	kIx(	(
<g/>
kotle	kotel	k1gInPc4	kotel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc1d1	malý
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
činely	činel	k1gInPc1	činel
<g/>
,	,	kIx,	,
triangl	triangl	k1gInSc1	triangl
</s>
</p>
<p>
<s>
Harfy	harfa	k1gFnPc1	harfa
</s>
</p>
<p>
<s>
Smyčce	smyčec	k1gInPc1	smyčec
<g/>
:	:	kIx,	:
první	první	k4xOgFnPc1	první
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnPc4	druhý
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violy	viola	k1gFnPc4	viola
<g/>
,	,	kIx,	,
violoncella	violoncello	k1gNnPc4	violoncello
<g/>
,	,	kIx,	,
kontrabasyPodle	kontrabasyPodle	k6eAd1	kontrabasyPodle
potřeb	potřeba	k1gFnPc2	potřeba
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
partitury	partitura	k1gFnSc2	partitura
mohou	moct	k5eAaImIp3nP	moct
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
nástrojům	nástroj	k1gInPc3	nástroj
přistupovat	přistupovat	k5eAaImF	přistupovat
i	i	k9	i
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
např.	např.	kA	např.
drnkací	drnkací	k2eAgFnPc1d1	drnkací
nebo	nebo	k8xC	nebo
klávesové	klávesový	k2eAgFnPc1d1	klávesová
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
<g/>
,	,	kIx,	,
celesta	celesta	k1gFnSc1	celesta
<g/>
,	,	kIx,	,
xylofon	xylofon	k1gInSc1	xylofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzové	jazzový	k2eAgInPc1d1	jazzový
(	(	kIx(	(
<g/>
saxofon	saxofon	k1gInSc1	saxofon
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Také	také	k9	také
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
nástrojové	nástrojový	k2eAgFnPc1d1	nástrojová
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
orchestru	orchestr	k1gInSc6	orchestr
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
hierarchii	hierarchie	k1gFnSc4	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
první	první	k4xOgMnPc1	první
<g/>
)	)	kIx)	)
hráč	hráč	k1gMnSc1	hráč
každé	každý	k3xTgFnSc2	každý
skupiny	skupina	k1gFnSc2	skupina
hraje	hrát	k5eAaImIp3nS	hrát
sóla	sólo	k1gNnPc1	sólo
předepsaná	předepsaný	k2eAgFnSc1d1	předepsaná
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
nástroj	nástroj	k1gInSc4	nástroj
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
vedoucím	vedoucí	k1gMnPc3	vedoucí
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
nástrojové	nástrojový	k2eAgFnPc1d1	nástrojová
sekce	sekce	k1gFnPc1	sekce
<g/>
)	)	kIx)	)
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
vést	vést	k5eAaImF	vést
dílčí	dílčí	k2eAgFnPc4d1	dílčí
zkoušky	zkouška	k1gFnPc4	zkouška
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
slaďovat	slaďovat	k5eAaImF	slaďovat
její	její	k3xOp3gInSc4	její
způsob	způsob	k1gInSc4	způsob
interpretace	interpretace	k1gFnSc2	interpretace
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
dirigenta	dirigent	k1gMnSc2	dirigent
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
techniky	technika	k1gFnSc2	technika
hry	hra	k1gFnPc4	hra
typické	typický	k2eAgFnPc4d1	typická
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
smyčců	smyčec	k1gInPc2	smyčec
tak	tak	k6eAd1	tak
zvané	zvaný	k2eAgNnSc4d1	zvané
smykování	smykování	k1gNnSc4	smykování
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
sóloinstrumentalista	sóloinstrumentalista	k1gMnSc1	sóloinstrumentalista
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sólohobojista	sólohobojista	k1gMnSc1	sólohobojista
<g/>
,	,	kIx,	,
sólovioloncellista	sólovioloncellista	k1gMnSc1	sólovioloncellista
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
konkurzu	konkurz	k1gInSc6	konkurz
<g/>
,	,	kIx,	,
vypsaném	vypsaný	k2eAgInSc6d1	vypsaný
na	na	k7c6	na
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Konkurzní	konkurzní	k2eAgFnSc4d1	konkurzní
komisi	komise	k1gFnSc4	komise
tvoří	tvořit	k5eAaImIp3nS	tvořit
obvykle	obvykle	k6eAd1	obvykle
šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
a	a	k8xC	a
první	první	k4xOgMnPc1	první
hráči	hráč	k1gMnPc1	hráč
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
nástrojových	nástrojový	k2eAgFnPc2d1	nástrojová
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Sóloví	sólový	k2eAgMnPc1d1	sólový
hráči	hráč	k1gMnPc1	hráč
smyčcových	smyčcový	k2eAgFnPc2d1	smyčcová
sekcí	sekce	k1gFnPc2	sekce
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
koncertní	koncertní	k2eAgMnPc1d1	koncertní
mistři	mistr	k1gMnPc1	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
má	mít	k5eAaImIp3nS	mít
koncertní	koncertní	k2eAgMnSc1d1	koncertní
mistr	mistr	k1gMnSc1	mistr
skupiny	skupina	k1gFnSc2	skupina
prvních	první	k4xOgFnPc2	první
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
podřízen	podřídit	k5eAaPmNgInS	podřídit
přímo	přímo	k6eAd1	přímo
dirigentovi	dirigentův	k2eAgMnPc1d1	dirigentův
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
skladby	skladba	k1gFnSc2	skladba
spojovací	spojovací	k2eAgInSc1d1	spojovací
článek	článek	k1gInSc1	článek
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
renomovaných	renomovaný	k2eAgInPc6d1	renomovaný
orchestrech	orchestr	k1gInPc6	orchestr
procházejí	procházet	k5eAaImIp3nP	procházet
instrumentalisté	instrumentalista	k1gMnPc1	instrumentalista
konkurzem	konkurz	k1gInSc7	konkurz
často	často	k6eAd1	často
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ucházejí	ucházet	k5eAaImIp3nP	ucházet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
do	do	k7c2	do
orchestru	orchestr	k1gInSc2	orchestr
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
tutti	tutti	k2eAgMnPc1d1	tutti
hráči	hráč	k1gMnPc1	hráč
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
ucházejí	ucházet	k5eAaImIp3nP	ucházet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
hráče	hráč	k1gMnSc2	hráč
nástrojové	nástrojový	k2eAgFnSc2d1	nástrojová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
pracovní	pracovní	k2eAgFnSc1d1	pracovní
povinnost	povinnost	k1gFnSc1	povinnost
orchestrálního	orchestrální	k2eAgMnSc2d1	orchestrální
hráče	hráč	k1gMnSc2	hráč
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
smlouva	smlouva	k1gFnSc1	smlouva
obvykle	obvykle	k6eAd1	obvykle
počtem	počet	k1gInSc7	počet
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgInPc2d1	zvaný
výkonů	výkon	k1gInPc2	výkon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
250	[number]	k4	250
čtyř-	čtyř-	k?	čtyř-
až	až	k8xS	až
pětihodinových	pětihodinový	k2eAgInPc2d1	pětihodinový
výkonů	výkon	k1gInPc2	výkon
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uměleckým	umělecký	k2eAgNnSc7d1	umělecké
vedením	vedení	k1gNnSc7	vedení
orchestru	orchestr	k1gInSc2	orchestr
je	být	k5eAaImIp3nS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
jeho	jeho	k3xOp3gMnSc1	jeho
šéfdirigent	šéfdirigent	k1gMnSc1	šéfdirigent
<g/>
.	.	kIx.	.
</s>
<s>
Způsobem	způsob	k1gInSc7	způsob
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
významně	významně	k6eAd1	významně
spoluutváří	spoluutvářet	k5eAaImIp3nS	spoluutvářet
úroveň	úroveň	k1gFnSc4	úroveň
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
formuje	formovat	k5eAaImIp3nS	formovat
jeho	on	k3xPp3gInSc4	on
repertoár	repertoár	k1gInSc4	repertoár
(	(	kIx(	(
<g/>
členy	člen	k1gInPc4	člen
umělecké	umělecký	k2eAgFnSc2d1	umělecká
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolurozhoduje	spolurozhodovat	k5eAaImIp3nS	spolurozhodovat
o	o	k7c6	o
dramaturgii	dramaturgie	k1gFnSc6	dramaturgie
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
krom	krom	k7c2	krom
něj	on	k3xPp3gMnSc2	on
také	také	k9	také
první	první	k4xOgMnPc1	první
hráči	hráč	k1gMnPc1	hráč
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
nástrojových	nástrojový	k2eAgFnPc2d1	nástrojová
sekcí	sekce	k1gFnPc2	sekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
šéfdirigenta	šéfdirigent	k1gMnSc2	šéfdirigent
mají	mít	k5eAaImIp3nP	mít
velké	velká	k1gFnPc1	velká
symfonické	symfonický	k2eAgFnSc2d1	symfonická
orchestry	orchestra	k1gFnSc2	orchestra
také	také	k9	také
své	svůj	k3xOyFgMnPc4	svůj
stálé	stálý	k2eAgMnPc4d1	stálý
a	a	k8xC	a
hostující	hostující	k2eAgMnPc4d1	hostující
dirigenty	dirigent	k1gMnPc4	dirigent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnost	osobnost	k1gFnSc1	osobnost
dirigenta	dirigent	k1gMnSc2	dirigent
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
nastudování	nastudování	k1gNnSc6	nastudování
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
tatáž	týž	k3xTgFnSc1	týž
partitura	partitura	k1gFnSc1	partitura
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
interpretována	interpretovat	k5eAaBmNgFnS	interpretovat
různě	různě	k6eAd1	různě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
-	-	kIx~	-
při	při	k7c6	při
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
spolupráci	spolupráce	k1gFnSc6	spolupráce
-	-	kIx~	-
i	i	k9	i
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
práce	práce	k1gFnSc2	práce
samotného	samotný	k2eAgInSc2d1	samotný
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
specifickém	specifický	k2eAgInSc6d1	specifický
způsobu	způsob	k1gInSc6	způsob
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Gracian	Gracian	k1gMnSc1	Gracian
Černušák	Černušák	k1gMnSc1	Černušák
<g/>
:	:	kIx,	:
Dějepis	dějepis	k1gInSc1	dějepis
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1930	[number]	k4	1930
</s>
</p>
<p>
<s>
Mirko	Mirko	k1gMnSc1	Mirko
Očadlík	Očadlík	k1gMnSc1	Očadlík
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
Panton	Panton	k1gInSc1	Panton
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
18	[number]	k4	18
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
859	[number]	k4	859
<g/>
Bernard	Bernard	k1gMnSc1	Bernard
Lehmann	Lehmann	k1gMnSc1	Lehmann
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
orchestre	orchestr	k1gInSc5	orchestr
dans	dansit	k5eAaPmRp2nS	dansit
tous	tous	k1gInSc1	tous
ses	ses	k?	ses
éclats	éclats	k1gInSc1	éclats
<g/>
.	.	kIx.	.
</s>
<s>
Ethnographie	Ethnographie	k1gFnSc1	Ethnographie
des	des	k1gNnSc2	des
formations	formations	k6eAd1	formations
symphoniques	symphoniquesa	k1gFnPc2	symphoniquesa
<g/>
,	,	kIx,	,
La	la	k0	la
Découverte	Découvert	k1gMnSc5	Découvert
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
2-7071-4610-2	[number]	k4	2-7071-4610-2
</s>
</p>
<p>
<s>
Konzertbuch	Konzertbuch	k1gMnSc1	Konzertbuch
Orchestermusik	Orchestermusik	k1gMnSc1	Orchestermusik
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
Malte	málit	k5eAaImRp2nP	málit
Korff	Korff	k1gInSc4	Korff
<g/>
,	,	kIx,	,
Breitkopf	Breitkopf	k1gInSc4	Breitkopf
und	und	k?	und
Härtel	Härtel	k1gInSc1	Härtel
<g/>
,	,	kIx,	,
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-423-03023-2	[number]	k4	3-423-03023-2
(	(	kIx(	(
<g/>
dtv	dtv	k?	dtv
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
ISBN	ISBN	kA	ISBN
3-7618-3023-8	[number]	k4	3-7618-3023-8
(	(	kIx(	(
<g/>
Bärenreiter	Bärenreiter	k1gInSc1	Bärenreiter
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
Michels	Michels	k1gInSc1	Michels
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
zu	zu	k?	zu
Musik	musika	k1gFnPc2	musika
<g/>
,	,	kIx,	,
DTV	DTV	kA	DTV
<g/>
,	,	kIx,	,
Bärenreiter	Bärenreiter	k1gInSc1	Bärenreiter
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Mattheson	Mattheson	k1gMnSc1	Mattheson
<g/>
:	:	kIx,	:
Das	Das	k1gFnPc1	Das
neueröffnete	ueröffnout	k5eNaPmIp2nP	ueröffnout
Orchester	orchestra	k1gFnPc2	orchestra
<g/>
,	,	kIx,	,
Hamburg	Hamburg	k1gInSc1	Hamburg
1713	[number]	k4	1713
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
soubor	soubor	k1gInSc1	soubor
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
orchestr	orchestr	k1gInSc1	orchestr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
orchestr	orchestr	k1gInSc1	orchestr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
