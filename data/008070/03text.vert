<s>
Rýma	rýma	k1gFnSc1	rýma
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
rhinitis	rhinitis	k1gFnSc1	rhinitis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
sliznice	sliznice	k1gFnSc2	sliznice
nosní	nosní	k2eAgFnSc2d1	nosní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Rýma	rýma	k1gFnSc1	rýma
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zduřením	zduření	k1gNnSc7	zduření
nosní	nosní	k2eAgFnSc2d1	nosní
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
omezuje	omezovat	k5eAaImIp3nS	omezovat
průchodnost	průchodnost	k1gFnSc1	průchodnost
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
a	a	k8xC	a
výtokem	výtok	k1gInSc7	výtok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
hlenovitý	hlenovitý	k2eAgInSc1d1	hlenovitý
<g/>
,	,	kIx,	,
hlenohnisavý	hlenohnisavý	k2eAgInSc1d1	hlenohnisavý
až	až	k6eAd1	až
hnisavý	hnisavý	k2eAgInSc1d1	hnisavý
<g/>
.	.	kIx.	.
</s>
<s>
Polykáním	polykání	k1gNnSc7	polykání
výtoku	výtok	k1gInSc2	výtok
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dráždivému	dráždivý	k2eAgInSc3d1	dráždivý
kašli	kašel	k1gInSc3	kašel
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bývá	bývat	k5eAaImIp3nS	bývat
způsobená	způsobený	k2eAgFnSc1d1	způsobená
viry	vira	k1gFnPc1	vira
-	-	kIx~	-
rhinoviry	rhinovira	k1gFnPc1	rhinovira
<g/>
,	,	kIx,	,
coronaviry	coronavira	k1gFnPc1	coronavira
<g/>
,	,	kIx,	,
virem	vir	k1gInSc7	vir
influenzae	influenza	k1gFnSc2	influenza
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
<g/>
,	,	kIx,	,
parainfluenzae	parainfluenzae	k1gNnSc2	parainfluenzae
či	či	k8xC	či
RSV	RSV	kA	RSV
(	(	kIx(	(
<g/>
respiratory	respirator	k1gMnPc4	respirator
syncytial	syncytiat	k5eAaPmAgInS	syncytiat
virus	virus	k1gInSc1	virus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
a	a	k8xC	a
batolat	batole	k1gNnPc2	batole
adenoviry	adenovira	k1gMnSc2	adenovira
<g/>
.	.	kIx.	.
</s>
<s>
Sliznici	sliznice	k1gFnSc4	sliznice
oslabenou	oslabený	k2eAgFnSc7d1	oslabená
virovou	virový	k2eAgFnSc7d1	virová
infekcí	infekce	k1gFnSc7	infekce
pak	pak	k6eAd1	pak
snáze	snadno	k6eAd2	snadno
napadnou	napadnout	k5eAaPmIp3nP	napadnout
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenzae	k1gNnSc2	influenzae
<g/>
,	,	kIx,	,
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
,	,	kIx,	,
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pyogenes	pyogenes	k1gMnSc1	pyogenes
a	a	k8xC	a
Branhamella	Branhamella	k1gMnSc1	Branhamella
catharalis	catharalis	k1gFnSc2	catharalis
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
nosní	nosní	k2eAgFnSc2d1	nosní
sliznice	sliznice	k1gFnSc2	sliznice
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
snižuje	snižovat	k5eAaImIp3nS	snižovat
nachlazení	nachlazení	k1gNnSc1	nachlazení
<g/>
,	,	kIx,	,
výkyvy	výkyv	k1gInPc1	výkyv
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
civilizační	civilizační	k2eAgInPc1d1	civilizační
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
vegetativní	vegetativní	k2eAgFnSc1d1	vegetativní
labilita	labilita	k1gFnSc1	labilita
a	a	k8xC	a
stres	stres	k1gInSc1	stres
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
opakovaných	opakovaný	k2eAgFnPc2d1	opakovaná
rým	rýma	k1gFnPc2	rýma
adenoidní	adenoidní	k2eAgFnSc2d1	adenoidní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
jednak	jednak	k8xC	jednak
mechanicky	mechanicky	k6eAd1	mechanicky
ucpáním	ucpání	k1gNnSc7	ucpání
nosních	nosní	k2eAgFnPc2d1	nosní
choan	choana	k1gFnPc2	choana
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vlastním	vlastní	k2eAgNnSc7d1	vlastní
infekčním	infekční	k2eAgNnSc7d1	infekční
ložiskem	ložisko	k1gNnSc7	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průběhu	průběh	k1gInSc2	průběh
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
rýmu	rýma	k1gFnSc4	rýma
akutní	akutní	k2eAgFnSc4d1	akutní
a	a	k8xC	a
chronickou	chronický	k2eAgFnSc4d1	chronická
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
příznaky	příznak	k1gInPc4	příznak
akutní	akutní	k2eAgFnSc2d1	akutní
rýmy	rýma	k1gFnSc2	rýma
(	(	kIx(	(
<g/>
rhinitis	rhinitis	k1gFnSc1	rhinitis
acuta	acuta	k1gFnSc1	acuta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
svědění	svědění	k1gNnSc2	svědění
v	v	k7c6	v
nose	nos	k1gInSc6	nos
<g/>
,	,	kIx,	,
nechutenství	nechutenství	k1gNnSc4	nechutenství
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
hojná	hojný	k2eAgFnSc1d1	hojná
vodnatá	vodnatý	k2eAgFnSc1d1	vodnatá
sekrece	sekrece	k1gFnSc1	sekrece
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
hlenovitou	hlenovitý	k2eAgFnSc4d1	hlenovitá
<g/>
,	,	kIx,	,
hlenohnisavou	hlenohnisavý	k2eAgFnSc4d1	hlenohnisavá
až	až	k8xS	až
hnisavou	hnisavý	k2eAgFnSc4d1	hnisavá
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnSc1d1	nosní
sliznice	sliznice	k1gFnSc1	sliznice
je	být	k5eAaImIp3nS	být
překrvená	překrvený	k2eAgFnSc1d1	překrvená
<g/>
,	,	kIx,	,
nosní	nosní	k2eAgFnPc1d1	nosní
skořepy	skořepa	k1gFnPc1	skořepa
jsou	být	k5eAaImIp3nP	být
prosáklé	prosáklý	k2eAgFnPc1d1	prosáklá
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
hlenem	hlen	k1gInSc7	hlen
<g/>
.	.	kIx.	.
</s>
<s>
Zduřelá	zduřelý	k2eAgFnSc1d1	zduřelá
sliznice	sliznice	k1gFnSc1	sliznice
ucpává	ucpávat	k5eAaImIp3nS	ucpávat
průchod	průchod	k1gInSc4	průchod
nosem	nos	k1gInSc7	nos
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc6	dýchání
nosem	nos	k1gInSc7	nos
je	být	k5eAaImIp3nS	být
ztížené	ztížený	k2eAgNnSc1d1	ztížené
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
noční	noční	k2eAgNnSc1d1	noční
chrápání	chrápání	k1gNnSc1	chrápání
a	a	k8xC	a
kašel	kašel	k1gInSc1	kašel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
zvětšené	zvětšený	k2eAgFnPc4d1	zvětšená
spádové	spádový	k2eAgFnPc4d1	spádová
uzliny	uzlina	k1gFnPc4	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
infekci	infekce	k1gFnSc6	infekce
adenoviry	adenovira	k1gFnSc2	adenovira
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
zarudlé	zarudlý	k2eAgFnSc2d1	zarudlá
spojivky	spojivka	k1gFnSc2	spojivka
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
produkci	produkce	k1gFnSc4	produkce
slz	slza	k1gFnPc2	slza
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
obvykle	obvykle	k6eAd1	obvykle
spontánně	spontánně	k6eAd1	spontánně
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
do	do	k7c2	do
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
infekce	infekce	k1gFnPc1	infekce
rozšíří	rozšířit	k5eAaPmIp3nP	rozšířit
i	i	k9	i
do	do	k7c2	do
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
dutin	dutina	k1gFnPc2	dutina
nosních	nosní	k2eAgFnPc2d1	nosní
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
rhinosinusitidě	rhinosinusitida	k1gFnSc6	rhinosinusitida
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
ji	on	k3xPp3gFnSc4	on
provází	provázet	k5eAaImIp3nS	provázet
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířením	rozšíření	k1gNnSc7	rozšíření
infekce	infekce	k1gFnSc2	infekce
do	do	k7c2	do
středoušní	středoušní	k2eAgFnSc2d1	středoušní
dutiny	dutina	k1gFnSc2	dutina
vzniká	vznikat	k5eAaImIp3nS	vznikat
zánět	zánět	k1gInSc1	zánět
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provázen	provázen	k2eAgMnSc1d1	provázen
i	i	k9	i
přechodnou	přechodný	k2eAgFnSc7d1	přechodná
nedoslýchavostí	nedoslýchavost	k1gFnSc7	nedoslýchavost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
infekce	infekce	k1gFnSc2	infekce
vzniká	vznikat	k5eAaImIp3nS	vznikat
laryngotracheobronchitida	laryngotracheobronchitida	k1gFnSc1	laryngotracheobronchitida
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
symptomatická	symptomatický	k2eAgFnSc1d1	symptomatická
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
antipyretika	antipyretikum	k1gNnSc2	antipyretikum
<g/>
,	,	kIx,	,
dostatek	dostatek	k1gInSc4	dostatek
tekutin	tekutina	k1gFnPc2	tekutina
a	a	k8xC	a
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
přívod	přívod	k1gInSc4	přívod
vitaminů	vitamin	k1gInPc2	vitamin
<g/>
.	.	kIx.	.
</s>
<s>
Dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neumějí	umět	k5eNaImIp3nP	umět
samy	sám	k3xTgInPc4	sám
smrkat	smrkat	k5eAaImF	smrkat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nosní	nosní	k2eAgNnSc1d1	nosní
sekret	sekret	k1gInSc4	sekret
i	i	k8xC	i
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
odsát	odsát	k5eAaPmF	odsát
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnPc1d1	nosní
kapky	kapka	k1gFnPc1	kapka
(	(	kIx(	(
<g/>
dekongestanty	dekongestant	k1gMnPc7	dekongestant
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nemají	mít	k5eNaImIp3nP	mít
používat	používat	k5eAaImF	používat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
riziko	riziko	k1gNnSc1	riziko
návyku	návyk	k1gInSc2	návyk
(	(	kIx(	(
<g/>
sanorinismus	sanorinismus	k1gInSc1	sanorinismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
použití	použití	k1gNnSc6	použití
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zbytek	zbytek	k1gInSc4	zbytek
nosních	nosní	k2eAgFnPc2d1	nosní
kapek	kapka	k1gFnPc2	kapka
či	či	k8xC	či
sprejů	sprej	k1gInPc2	sprej
uchovávat	uchovávat	k5eAaImF	uchovávat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
infekce	infekce	k1gFnPc1	infekce
držet	držet	k5eAaImF	držet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
nosní	nosní	k2eAgFnSc4d1	nosní
sprchu	sprcha	k1gFnSc4	sprcha
nebo	nebo	k8xC	nebo
výplach	výplach	k1gInSc4	výplach
nosu	nos	k1gInSc2	nos
některým	některý	k3yIgNnPc3	některý
z	z	k7c2	z
běžně	běžně	k6eAd1	běžně
dostupných	dostupný	k2eAgInPc2d1	dostupný
sprejů	sprej	k1gInPc2	sprej
se	s	k7c7	s
solným	solný	k2eAgInSc7d1	solný
roztokem	roztok	k1gInSc7	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Hlen	hlen	k1gInSc1	hlen
(	(	kIx(	(
<g/>
mukus	mukus	k1gInSc1	mukus
<g/>
,	,	kIx,	,
též	též	k9	též
sliz	sliz	k1gInSc1	sliz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
tělní	tělní	k2eAgFnSc1d1	tělní
tekutina	tekutina	k1gFnSc1	tekutina
vylučovaná	vylučovaný	k2eAgFnSc1d1	vylučovaná
sliznicemi	sliznice	k1gFnPc7	sliznice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mukózní	mukózní	k2eAgFnSc7d1	mukózní
membránou	membrána	k1gFnSc7	membrána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
zvlhčovat	zvlhčovat	k5eAaImF	zvlhčovat
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
určité	určitý	k2eAgFnPc4d1	určitá
partie	partie	k1gFnPc4	partie
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
trávicí	trávicí	k2eAgFnSc1d1	trávicí
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgFnSc1d1	dýchací
a	a	k8xC	a
rozmnožovací	rozmnožovací	k2eAgFnSc1d1	rozmnožovací
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
otužování	otužování	k1gNnSc1	otužování
<g/>
,	,	kIx,	,
vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
výživa	výživa	k1gFnSc1	výživa
s	s	k7c7	s
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
přívodem	přívod	k1gInSc7	přívod
vitaminů	vitamin	k1gInPc2	vitamin
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
infekčních	infekční	k2eAgFnPc2d1	infekční
rým	rýma	k1gFnPc2	rýma
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
rhinoviry	rhinovira	k1gFnSc2	rhinovira
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
přenos	přenos	k1gInSc4	přenos
je	být	k5eAaImIp3nS	být
kontaminovanými	kontaminovaný	k2eAgInPc7d1	kontaminovaný
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
častá	častý	k2eAgFnSc1d1	častá
výměna	výměna	k1gFnSc1	výměna
kapesníků	kapesník	k1gInPc2	kapesník
a	a	k8xC	a
mytí	mytí	k1gNnSc2	mytí
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nákaz	nákaza	k1gFnPc2	nákaza
přenášených	přenášený	k2eAgFnPc2d1	přenášená
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
cestou	cesta	k1gFnSc7	cesta
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
úzkému	úzký	k2eAgInSc3d1	úzký
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
nemocnými	nemocný	k1gMnPc7	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
infekční	infekční	k2eAgInSc1d1	infekční
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1	chronická
rýma	rýma	k1gFnSc1	rýma
(	(	kIx(	(
<g/>
rhinitis	rhinitis	k1gFnSc1	rhinitis
chronica	chronica	k1gFnSc1	chronica
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
choroby	choroba	k1gFnPc4	choroba
dětského	dětský	k2eAgInSc2d1	dětský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1	chronická
rýma	rýma	k1gFnSc1	rýma
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
katarální	katarální	k2eAgInSc4d1	katarální
(	(	kIx(	(
<g/>
catarrh	catarrh	k1gInSc4	catarrh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hypertrofickou	hypertrofický	k2eAgFnSc4d1	hypertrofická
<g/>
,	,	kIx,	,
atrofickou	atrofický	k2eAgFnSc4d1	atrofická
<g/>
,	,	kIx,	,
a	a	k8xC	a
alergickou	alergický	k2eAgFnSc4d1	alergická
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gFnPc4	jejich
příčiny	příčina	k1gFnPc4	příčina
vzniku	vznik	k1gInSc2	vznik
patří	patřit	k5eAaImIp3nP	patřit
anatomické	anatomický	k2eAgFnPc4d1	anatomická
úchylky	úchylka	k1gFnPc4	úchylka
v	v	k7c6	v
nose	nos	k1gInSc6	nos
–	–	k?	–
deviace	deviace	k1gFnSc2	deviace
nosního	nosní	k2eAgNnSc2d1	nosní
septa	septum	k1gNnSc2	septum
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgFnSc1d1	akutní
rýma	rýma	k1gFnSc1	rýma
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
chronickou	chronický	k2eAgFnSc4d1	chronická
<g/>
,	,	kIx,	,
zvětšené	zvětšený	k2eAgFnSc2d1	zvětšená
adenoidní	adenoidní	k2eAgFnSc2d1	adenoidní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
srdečně-cévní	srdečněévní	k2eAgFnSc2d1	srdečně-cévní
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
exogenní	exogenní	k2eAgInPc4d1	exogenní
faktory	faktor	k1gInPc4	faktor
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
plyny	plyn	k1gInPc1	plyn
a	a	k8xC	a
kolísavé	kolísavý	k2eAgFnPc1d1	kolísavá
teploty	teplota	k1gFnPc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rýma	rýma	k1gFnSc1	rýma
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
