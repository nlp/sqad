<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
matematická	matematický	k2eAgFnSc1d1	matematická
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
opakované	opakovaný	k2eAgNnSc4d1	opakované
násobení	násobení	k1gNnSc4	násobení
<g/>
?	?	kIx.	?
</s>
