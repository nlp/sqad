<s>
Polypeptid	polypeptid	k1gInSc1	polypeptid
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
peptidový	peptidový	k2eAgInSc4d1	peptidový
biopolymer	biopolymer	k1gInSc4	biopolymer
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
polypeptidy	polypeptid	k1gInPc7	polypeptid
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
bílkoviny	bílkovina	k1gFnPc1	bílkovina
(	(	kIx(	(
<g/>
proteiny	protein	k1gInPc1	protein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
řetězec	řetězec	k1gInSc1	řetězec
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc4d2	delší
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
termíny	termín	k1gInPc1	termín
polypeptid	polypeptid	k1gInSc1	polypeptid
a	a	k8xC	a
protein	protein	k1gInSc1	protein
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
však	však	k9	však
polypeptid	polypeptid	k1gInSc1	polypeptid
definuje	definovat	k5eAaBmIp3nS	definovat
například	například	k6eAd1	například
jako	jako	k8xS	jako
polymer	polymer	k1gInSc1	polymer
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
50	[number]	k4	50
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Polypeptid	polypeptid	k1gInSc1	polypeptid
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
procesem	proces	k1gInSc7	proces
translace	translace	k1gFnSc2	translace
na	na	k7c6	na
ribozomu	ribozom	k1gInSc6	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
polypeptidy	polypeptid	k1gInPc1	polypeptid
jsou	být	k5eAaImIp3nP	být
oligopeptidy	oligopeptida	k1gFnPc4	oligopeptida
<g/>
.	.	kIx.	.
</s>
