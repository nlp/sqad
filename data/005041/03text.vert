<s>
Většina	většina	k1gFnSc1	většina
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
roztříděna	roztřídit	k5eAaPmNgFnS	roztřídit
do	do	k7c2	do
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
fylogenetickou	fylogenetický	k2eAgFnSc7d1	fylogenetická
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsažené	obsažený	k2eAgFnSc6d1	obsažená
mají	mít	k5eAaImIp3nP	mít
společného	společný	k2eAgMnSc4d1	společný
předchůdce	předchůdce	k1gMnSc4	předchůdce
(	(	kIx(	(
<g/>
prajazyk	prajazyk	k1gInSc1	prajazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
předchůdce	předchůdce	k1gMnSc1	předchůdce
nám	my	k3xPp1nPc3	my
většinou	většinou	k6eAd1	většinou
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
většina	většina	k1gFnSc1	většina
jazyků	jazyk	k1gInPc2	jazyk
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
zaznamenanou	zaznamenaný	k2eAgFnSc4d1	zaznamenaná
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
mnoho	mnoho	k4c4	mnoho
znaků	znak	k1gInPc2	znak
tohoto	tento	k3xDgMnSc4	tento
společného	společný	k2eAgMnSc4d1	společný
předchůdce	předchůdce	k1gMnSc4	předchůdce
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
(	(	kIx(	(
<g/>
komparativní	komparativní	k2eAgFnSc2d1	komparativní
<g/>
)	)	kIx)	)
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rekonstruktivní	rekonstruktivní	k2eAgInSc4d1	rekonstruktivní
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
základy	základ	k1gInPc4	základ
položil	položit	k5eAaPmAgMnS	položit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
August	August	k1gMnSc1	August
Schleicher	Schleichra	k1gFnPc2	Schleichra
<g/>
.	.	kIx.	.
</s>
<s>
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
rodiny	rodina	k1gFnPc1	rodina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
větve	větev	k1gFnPc1	větev
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
historie	historie	k1gFnSc1	historie
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
pomocí	pomocí	k7c2	pomocí
stromového	stromový	k2eAgInSc2d1	stromový
diagramu	diagram	k1gInSc2	diagram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgMnSc1d1	společný
předchůdce	předchůdce	k1gMnSc1	předchůdce
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
větve	větev	k1gFnSc2	větev
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prajazyk	prajazyk	k1gInSc1	prajazyk
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
protojazyk	protojazyk	k6eAd1	protojazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rekonstruovatelný	rekonstruovatelný	k2eAgInSc1d1	rekonstruovatelný
prajazyk	prajazyk	k1gInSc1	prajazyk
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
praindoevropština	praindoevropština	k1gFnSc1	praindoevropština
(	(	kIx(	(
<g/>
neexistují	existovat	k5eNaImIp3nP	existovat
ale	ale	k8xC	ale
žádné	žádný	k3yNgInPc1	žádný
písemné	písemný	k2eAgInPc1d1	písemný
záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
mluvilo	mluvit	k5eAaImAgNnS	mluvit
před	před	k7c7	před
vynálezem	vynález	k1gInSc7	vynález
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prajazyk	prajazyk	k1gInSc1	prajazyk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
občas	občas	k6eAd1	občas
ztotožněn	ztotožnit	k5eAaPmNgInS	ztotožnit
s	s	k7c7	s
historicky	historicky	k6eAd1	historicky
známým	známý	k2eAgInSc7d1	známý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
provinciální	provinciální	k2eAgInPc1d1	provinciální
dialekty	dialekt	k1gInPc1	dialekt
latiny	latina	k1gFnSc2	latina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vulgární	vulgární	k2eAgFnSc1d1	vulgární
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
lidová	lidový	k2eAgFnSc1d1	lidová
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
daly	dát	k5eAaPmAgFnP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
moderním	moderní	k2eAgInPc3d1	moderní
románským	románský	k2eAgInPc3d1	románský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
prarománský	prarománský	k2eAgInSc1d1	prarománský
jazyk	jazyk	k1gInSc1	jazyk
víceméně	víceméně	k9	víceméně
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
latinou	latina	k1gFnSc7	latina
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
klasickou	klasický	k2eAgFnSc7d1	klasická
(	(	kIx(	(
<g/>
spisovnou	spisovný	k2eAgFnSc7d1	spisovná
<g/>
)	)	kIx)	)
verzí	verze	k1gFnSc7	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
141	[number]	k4	141
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
indiánským	indiánský	k2eAgInPc3d1	indiánský
jazykům	jazyk	k1gInPc3	jazyk
a	a	k8xC	a
jazykům	jazyk	k1gInPc3	jazyk
domorodců	domorodec	k1gMnPc2	domorodec
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
ostatních	ostatní	k2eAgFnPc2d1	ostatní
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
izolovaných	izolovaný	k2eAgInPc2d1	izolovaný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
vlastní	vlastní	k2eAgFnPc4d1	vlastní
jazykové	jazykový	k2eAgFnPc4d1	jazyková
rodiny	rodina	k1gFnPc4	rodina
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
na	na	k7c4	na
88	[number]	k4	88
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
jazyků	jazyk	k1gMnPc2	jazyk
amerických	americký	k2eAgMnPc2d1	americký
<g/>
,	,	kIx,	,
australských	australský	k2eAgMnPc2d1	australský
a	a	k8xC	a
papuánských	papuánský	k2eAgMnPc2d1	papuánský
domorodců	domorodec	k1gMnPc2	domorodec
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
14	[number]	k4	14
izolovaných	izolovaný	k2eAgInPc2d1	izolovaný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
Seznam	seznam	k1gInSc1	seznam
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
</s>
