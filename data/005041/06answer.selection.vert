<s>
Správně	správně	k6eAd1	správně
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
fylogenetickou	fylogenetický	k2eAgFnSc7d1	fylogenetická
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsažené	obsažený	k2eAgFnSc6d1	obsažená
mají	mít	k5eAaImIp3nP	mít
společného	společný	k2eAgMnSc4d1	společný
předchůdce	předchůdce	k1gMnSc4	předchůdce
(	(	kIx(	(
<g/>
prajazyk	prajazyk	k1gInSc1	prajazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
