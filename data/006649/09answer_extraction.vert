dáno	dán	k2eAgNnSc1d1	dáno
jeho	jeho	k3xOp3gNnSc4	jeho
geneticky	geneticky	k6eAd1	geneticky
danými	daný	k2eAgFnPc7d1	daná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
temperament	temperament	k1gInSc4	temperament
a	a	k8xC	a
instinkty	instinkt	k1gInPc4	instinkt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
modifikovány	modifikovat	k5eAaBmNgFnP	modifikovat
během	během	k7c2	během
socializace	socializace	k1gFnSc2	socializace
a	a	k8xC	a
učením	učení	k1gNnSc7	učení
