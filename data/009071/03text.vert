<p>
<s>
Dubnium	Dubnium	k1gNnSc1	Dubnium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Db	db	kA	db
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Dubnium	Dubnium	k1gNnSc4	Dubnium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
nebo	nebo	k8xC	nebo
urychlovači	urychlovač	k1gInSc6	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dubnium	Dubnium	k1gNnSc1	Dubnium
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
konstanty	konstanta	k1gFnPc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
poloze	poloha	k1gFnSc6	poloha
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
by	by	k9	by
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
mělo	mít	k5eAaImAgNnS	mít
připomínat	připomínat	k5eAaImF	připomínat
tantal	tantal	k1gInSc4	tantal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
příprava	příprava	k1gFnSc1	příprava
prvku	prvek	k1gInSc2	prvek
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
105	[number]	k4	105
byla	být	k5eAaImAgFnS	být
ohlášena	ohlášen	k2eAgFnSc1d1	ohlášena
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
Ústavu	ústav	k1gInSc2	ústav
jaderného	jaderný	k2eAgInSc2d1	jaderný
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
Dubně	Dubna	k1gFnSc6	Dubna
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
pomocí	pomocí	k7c2	pomocí
urychlovače	urychlovač	k1gInSc2	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přitom	přitom	k6eAd1	přitom
bombardován	bombardován	k2eAgInSc1d1	bombardován
terč	terč	k1gInSc1	terč
z	z	k7c2	z
americia	americium	k1gNnSc2	americium
243	[number]	k4	243
<g/>
Am	Am	k1gFnSc2	Am
urychlenými	urychlený	k2eAgInPc7d1	urychlený
ionty	ion	k1gInPc7	ion
neonu	neon	k1gInSc2	neon
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ohlášen	ohlásit	k5eAaPmNgInS	ohlásit
vznik	vznik	k1gInSc1	vznik
dvou	dva	k4xCgInPc2	dva
izotopů	izotop	k1gInPc2	izotop
nového	nový	k2eAgInSc2d1	nový
prvku	prvek	k1gInSc2	prvek
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
105	[number]	k4	105
a	a	k8xC	a
relativní	relativní	k2eAgFnSc7d1	relativní
atomovou	atomový	k2eAgFnSc7d1	atomová
hmotností	hmotnost	k1gFnSc7	hmotnost
260	[number]	k4	260
a	a	k8xC	a
261	[number]	k4	261
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
místě	místo	k1gNnSc6	místo
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
prvek	prvek	k1gInSc4	prvek
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
dubnium	dubnium	k1gNnSc4	dubnium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24395	[number]	k4	24395
Am	Am	k1gFnSc1	Am
+	+	kIx~	+
2210	[number]	k4	2210
Ne	Ne	kA	Ne
→	→	k?	→
260105	[number]	k4	260105
Db	db	kA	db
+	+	kIx~	+
5	[number]	k4	5
10	[number]	k4	10
n	n	k0	n
24395	[number]	k4	24395
Am	Am	k1gFnPc1	Am
+	+	kIx~	+
2210	[number]	k4	2210
Ne	Ne	kA	Ne
→	→	k?	→
261105	[number]	k4	261105
Db	db	kA	db
+	+	kIx~	+
4	[number]	k4	4
10	[number]	k4	10
nVědci	nVědce	k1gMnPc7	nVědce
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
university	universita	k1gFnSc2	universita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
lineárního	lineární	k2eAgInSc2d1	lineární
urychlovače	urychlovač	k1gInSc2	urychlovač
částic	částice	k1gFnPc2	částice
bombardováním	bombardování	k1gNnSc7	bombardování
terče	terč	k1gInSc2	terč
z	z	k7c2	z
izotopů	izotop	k1gInPc2	izotop
kalifornia	kalifornium	k1gNnSc2	kalifornium
249	[number]	k4	249
<g/>
Cf	Cf	k1gFnSc2	Cf
jádry	jádro	k1gNnPc7	jádro
dusíku	dusík	k1gInSc2	dusík
14N	[number]	k4	14N
získali	získat	k5eAaPmAgMnP	získat
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
105	[number]	k4	105
a	a	k8xC	a
relativní	relativní	k2eAgFnSc7d1	relativní
atomovou	atomový	k2eAgFnSc7d1	atomová
hmotností	hmotnost	k1gFnSc7	hmotnost
260	[number]	k4	260
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
jej	on	k3xPp3gNnSc4	on
hahnium	hahnium	k1gNnSc4	hahnium
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc1d1	chemický
symbol	symbol	k1gInSc1	symbol
Ha	ha	kA	ha
<g/>
)	)	kIx)	)
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
německého	německý	k2eAgMnSc2d1	německý
jaderného	jaderný	k2eAgMnSc2d1	jaderný
fyzika	fyzik	k1gMnSc2	fyzik
Otto	Otto	k1gMnSc1	Otto
Hahna	Hahn	k1gMnSc2	Hahn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24998	[number]	k4	24998
Cf	Cf	k1gFnSc1	Cf
+	+	kIx~	+
157	[number]	k4	157
N	N	kA	N
→	→	k?	→
260105	[number]	k4	260105
Db	db	kA	db
+	+	kIx~	+
4	[number]	k4	4
10	[number]	k4	10
nV	nV	k?	nV
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
dubnium	dubnium	k1gNnSc1	dubnium
označovalo	označovat	k5eAaImAgNnS	označovat
názvem	název	k1gInSc7	název
nielsbohrium	nielsbohrium	k1gNnSc1	nielsbohrium
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc1d1	chemický
symbol	symbol	k1gInSc1	symbol
Ns	Ns	k1gFnSc2	Ns
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
IUPAC	IUPAC	kA	IUPAC
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
nakonec	nakonec	k6eAd1	nakonec
definitivně	definitivně	k6eAd1	definitivně
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
prvku	prvek	k1gInSc2	prvek
dubnium	dubnium	k1gNnSc4	dubnium
s	s	k7c7	s
chemickou	chemický	k2eAgFnSc7d1	chemická
značkou	značka	k1gFnSc7	značka
Db	db	kA	db
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Izotopy	izotop	k1gInPc4	izotop
==	==	k?	==
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
izotopů	izotop	k1gInPc2	izotop
dubnia	dubnium	k1gNnSc2	dubnium
<g/>
,	,	kIx,	,
nejstabilnější	stabilní	k2eAgInSc1d3	nejstabilnější
izotop	izotop	k1gInSc1	izotop
268	[number]	k4	268
<g/>
Db	db	kA	db
má	mít	k5eAaImIp3nS	mít
poločas	poločas	k1gInSc1	poločas
přeměny	přeměna	k1gFnSc2	přeměna
32	[number]	k4	32
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
skupina	skupina	k1gFnSc1	skupina
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dubnium	dubnium	k1gNnSc4	dubnium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dubnium	dubnium	k1gNnSc4	dubnium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
