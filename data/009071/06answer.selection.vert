<s>
Dubnium	Dubnium	k1gNnSc1	Dubnium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Db	db	kA	db
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Dubnium	Dubnium	k1gNnSc4	Dubnium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
nebo	nebo	k8xC	nebo
urychlovači	urychlovač	k1gInSc6	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
