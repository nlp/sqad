<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kýhos	Kýhos	k1gMnSc1	Kýhos
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1956	[number]	k4	1956
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
týmu	tým	k1gInSc2	tým
BK	BK	kA	BK
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Litvínova	Litvínov	k1gInSc2	Litvínov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
hokejové	hokejový	k2eAgFnSc6d1	hokejová
soutěži	soutěž	k1gFnSc6	soutěž
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Litvínova	Litvínov	k1gInSc2	Litvínov
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1977	[number]	k4	1977
<g/>
/	/	kIx~	/
<g/>
1978	[number]	k4	1978
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1981	[number]	k4	1981
<g/>
/	/	kIx~	/
<g/>
1982	[number]	k4	1982
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1983	[number]	k4	1983
<g/>
/	/	kIx~	/
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
útoku	útok	k1gInSc6	útok
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Růžičkou	Růžička	k1gMnSc7	Růžička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1984	[number]	k4	1984
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Československo	Československo	k1gNnSc4	Československo
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
zamířil	zamířit	k5eAaPmAgMnS	zamířit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trenérská	trenérský	k2eAgFnSc1d1	trenérská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Roli	role	k1gFnSc4	role
trenéra	trenér	k1gMnSc2	trenér
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Litvínově	Litvínov	k1gInSc6	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
tvořil	tvořit	k5eAaImAgInS	tvořit
dvojici	dvojice	k1gFnSc4	dvojice
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Beránkem	Beránek	k1gMnSc7	Beránek
a	a	k8xC	a
celek	celek	k1gInSc4	celek
dovedli	dovést	k5eAaPmAgMnP	dovést
ke	k	k7c3	k
stříbrným	stříbrný	k2eAgFnPc3d1	stříbrná
medailím	medaile	k1gFnPc3	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
trenérská	trenérský	k2eAgFnSc1d1	trenérská
dráha	dráha	k1gFnSc1	dráha
byla	být	k5eAaImAgFnS	být
poté	poté	k6eAd1	poté
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
chomutovským	chomutovský	k2eAgInSc7d1	chomutovský
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
vedl	vést	k5eAaImAgMnS	vést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
do	do	k7c2	do
konce	konec	k1gInSc2	konec
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
sezóny	sezóna	k1gFnSc2	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
Kometu	kometa	k1gFnSc4	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
v	v	k7c6	v
extralize	extraliga	k1gFnSc6	extraliga
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
a	a	k8xC	a
HC	HC	kA	HC
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
střídačku	střídačka	k1gFnSc4	střídačka
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
extraligového	extraligový	k2eAgInSc2d1	extraligový
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
týmem	tým	k1gInSc7	tým
se	se	k3xPyFc4	se
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
probojoval	probojovat	k5eAaPmAgMnS	probojovat
přes	přes	k7c4	přes
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
mistra	mistr	k1gMnSc4	mistr
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
vítěze	vítěz	k1gMnSc2	vítěz
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
Spartu	Sparta	k1gFnSc4	Sparta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Kometa	kometa	k1gFnSc1	kometa
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Zlínu	Zlín	k1gInSc3	Zlín
a	a	k8xC	a
tým	tým	k1gInSc1	tým
vedený	vedený	k2eAgInSc1d1	vedený
trenérem	trenér	k1gMnSc7	trenér
Kýhosem	Kýhos	k1gMnSc7	Kýhos
získal	získat	k5eAaPmAgMnS	získat
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
i	i	k8xC	i
na	na	k7c4	na
sezónu	sezóna	k1gFnSc4	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
získal	získat	k5eAaPmAgMnS	získat
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
ročníku	ročník	k1gInSc2	ročník
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
postu	post	k1gInSc6	post
trenéra	trenér	k1gMnSc2	trenér
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Alois	Alois	k1gMnSc1	Alois
Hadamczik	Hadamczik	k1gMnSc1	Hadamczik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
trenérem	trenér	k1gMnSc7	trenér
královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
klubu	klub	k1gInSc2	klub
Mountfield	Mountfielda	k1gFnPc2	Mountfielda
HK	HK	kA	HK
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
následující	následující	k2eAgFnSc2d1	následující
sezóny	sezóna	k1gFnSc2	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
týmu	tým	k1gInSc2	tým
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
trénuje	trénovat	k5eAaImIp3nS	trénovat
klub	klub	k1gInSc1	klub
BK	BK	kA	BK
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
