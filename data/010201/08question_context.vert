<s>
Joseph	Joseph	k1gMnSc1	Joseph
Effner	Effner	k1gMnSc1	Effner
(	(	kIx(	(
<g/>
křtěn	křtěn	k2eAgMnSc1d1	křtěn
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
Dachau	Dachaus	k1gInSc2	Dachaus
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1745	[number]	k4	1745
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1706	[number]	k4	1706
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Gabriela	Gabriel	k1gMnSc2	Gabriel
Germaina	Germain	k2eAgFnSc1d1	Germain
Boffranda	Boffranda	k1gFnSc1	Boffranda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1715	[number]	k4	1715
-	-	kIx~	-
1726	[number]	k4	1726
byl	být	k5eAaImAgMnS	být
architektem	architekt	k1gMnSc7	architekt
bavorského	bavorský	k2eAgMnSc2d1	bavorský
kurfiřta	kurfiřt	k1gMnSc4	kurfiřt
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
II	II	kA	II
<g/>
.	.	kIx.	.
Emanuela	Emanuela	k1gFnSc1	Emanuela
<g/>
.	.	kIx.	.
</s>
