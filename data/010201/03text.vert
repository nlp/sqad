<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Effner	Effner	k1gMnSc1	Effner
(	(	kIx(	(
<g/>
křtěn	křtěn	k2eAgMnSc1d1	křtěn
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
Dachau	Dachaus	k1gInSc2	Dachaus
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1745	[number]	k4	1745
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1706	[number]	k4	1706
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Gabriela	Gabriel	k1gMnSc2	Gabriel
Germaina	Germain	k2eAgFnSc1d1	Germain
Boffranda	Boffranda	k1gFnSc1	Boffranda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1715	[number]	k4	1715
-	-	kIx~	-
1726	[number]	k4	1726
byl	být	k5eAaImAgMnS	být
architektem	architekt	k1gMnSc7	architekt
bavorského	bavorský	k2eAgMnSc2d1	bavorský
kurfiřta	kurfiřt	k1gMnSc4	kurfiřt
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
II	II	kA	II
<g/>
.	.	kIx.	.
Emanuela	Emanuela	k1gFnSc1	Emanuela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
1715	[number]	k4	1715
<g/>
/	/	kIx~	/
<g/>
1717	[number]	k4	1717
<g/>
-	-	kIx~	-
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Dachau	Dachaus	k1gInSc6	Dachaus
</s>
</p>
<p>
<s>
1715	[number]	k4	1715
<g/>
/	/	kIx~	/
<g/>
1717	[number]	k4	1717
<g/>
-	-	kIx~	-
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámek	zámek	k1gInSc4	zámek
Fürstenried	Fürstenried	k1gMnSc1	Fürstenried
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Mnichova	Mnichov	k1gInSc2	Mnichov
</s>
</p>
<p>
<s>
1718	[number]	k4	1718
<g/>
/	/	kIx~	/
<g/>
1721	[number]	k4	1721
<g/>
-	-	kIx~	-
rozšíření	rozšíření	k1gNnSc4	rozšíření
parku	park	k1gInSc2	park
a	a	k8xC	a
zámku	zámek	k1gInSc2	zámek
Nymphenburg	Nymphenburg	k1gMnSc1	Nymphenburg
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
</s>
</p>
<p>
<s>
1716	[number]	k4	1716
<g/>
/	/	kIx~	/
<g/>
1719	[number]	k4	1719
<g/>
-	-	kIx~	-
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Pagodenburgu	Pagodenburg	k1gInSc6	Pagodenburg
a	a	k8xC	a
Badenburgu	Badenburg	k1gInSc6	Badenburg
</s>
</p>
<p>
<s>
1719	[number]	k4	1719
<g/>
/	/	kIx~	/
<g/>
1726	[number]	k4	1726
<g/>
-	-	kIx~	-
rozšíření	rozšíření	k1gNnSc2	rozšíření
zámku	zámek	k1gInSc2	zámek
Schleissheim	Schleissheima	k1gFnPc2	Schleissheima
</s>
</p>
<p>
<s>
1723	[number]	k4	1723
<g/>
/	/	kIx~	/
<g/>
1729	[number]	k4	1729
<g/>
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Preysing	Preysing	k1gInSc1	Preysing
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
</s>
</p>
<p>
<s>
stavba	stavba	k1gFnSc1	stavba
Reiche	Reich	k1gFnSc2	Reich
Zimmer	Zimmra	k1gFnPc2	Zimmra
v	v	k7c6	v
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
rezidenci	rezidence	k1gFnSc6	rezidence
bavorských	bavorský	k2eAgMnPc2d1	bavorský
králů	král	k1gMnPc2	král
a	a	k8xC	a
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Joseph	Joseph	k1gMnSc1	Joseph
Effner	Effner	k1gMnSc1	Effner
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Joseph	Josepha	k1gFnPc2	Josepha
Effner	Effnero	k1gNnPc2	Effnero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
