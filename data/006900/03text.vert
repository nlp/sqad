<s>
Obec	obec	k1gFnSc1	obec
Třebosice	Třebosice	k1gFnSc2	Třebosice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Pardubický	pardubický	k2eAgInSc1d1	pardubický
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
185	[number]	k4	185
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1319	[number]	k4	1319
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
archeologickou	archeologický	k2eAgFnSc7d1	archeologická
lokalitou	lokalita	k1gFnSc7	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Archeoložka	archeoložka	k1gFnSc1	archeoložka
Kristýna	Kristýna	k1gFnSc1	Kristýna
Bulvová	Bulvová	k1gFnSc1	Bulvová
její	její	k3xOp3gInSc4	její
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
poznání	poznání	k1gNnSc4	poznání
neolitické	neolitický	k2eAgFnSc2d1	neolitická
kultury	kultura	k1gFnSc2	kultura
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
významu	význam	k1gInSc3	význam
Věstonic	Věstonice	k1gFnPc2	Věstonice
pro	pro	k7c4	pro
poznání	poznání	k1gNnSc4	poznání
moravského	moravský	k2eAgInSc2d1	moravský
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc4	obec
proslavil	proslavit	k5eAaPmAgMnS	proslavit
unikátní	unikátní	k2eAgInSc4d1	unikátní
nález	nález	k1gInSc4	nález
miniaturní	miniaturní	k2eAgFnSc2d1	miniaturní
plastiky	plastika	k1gFnSc2	plastika
kozla	kozel	k1gMnSc2	kozel
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
6	[number]	k4	6
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
lengyelské	lengyelský	k2eAgFnSc2d1	lengyelská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
zastupitelstvo	zastupitelstvo	k1gNnSc4	zastupitelstvo
obce	obec	k1gFnSc2	obec
schválilo	schválit	k5eAaPmAgNnS	schválit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlava	hlava	k1gFnSc1	hlava
kozla	kozel	k1gMnSc2	kozel
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
symbolem	symbol	k1gInSc7	symbol
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
znaku	znak	k1gInSc6	znak
a	a	k8xC	a
praporu	prapor	k1gInSc6	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
dvoutřídní	dvoutřídní	k2eAgFnSc4d1	dvoutřídní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
staré	starý	k2eAgFnSc6d1	stará
faře	fara	k1gFnSc6	fara
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
<g/>
1879	[number]	k4	1879
nahradila	nahradit	k5eAaPmAgFnS	nahradit
nově	nově	k6eAd1	nově
postavená	postavený	k2eAgFnSc1d1	postavená
čtyřtřídní	čtyřtřídní	k2eAgFnSc1d1	čtyřtřídní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
řídícím	řídící	k2eAgMnSc7d1	řídící
učitelem	učitel	k1gMnSc7	učitel
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Vosáhlo	Vosáhlo	k1gMnSc1	Vosáhlo
<g/>
.	.	kIx.	.
</s>
<s>
Učilo	učít	k5eAaPmAgNnS	učít
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
od	od	k7c2	od
první	první	k4xOgFnSc2	první
do	do	k7c2	do
páté	pátý	k4xOgFnSc2	pátý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc4	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
barokní	barokní	k2eAgFnSc1d1	barokní
zvonice	zvonice	k1gFnSc1	zvonice
-	-	kIx~	-
osmiúhelníkový	osmiúhelníkový	k2eAgInSc1d1	osmiúhelníkový
půdorys	půdorys	k1gInSc1	půdorys
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
zvon	zvon	k1gInSc1	zvon
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1464	[number]	k4	1464
(	(	kIx(	(
<g/>
po	po	k7c4	po
starší	starý	k2eAgFnSc4d2	starší
předchůdkyni	předchůdkyně	k1gFnSc4	předchůdkyně
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Třebosice	Třebosice	k1gFnSc2	Třebosice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
