<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Schweizerische	Schweizerische	k1gInSc1	Schweizerische
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
román	román	k1gInSc4	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
německy	německy	k6eAd1	německy
píšícího	píšící	k2eAgMnSc2d1	píšící
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
protestantského	protestantský	k2eAgMnSc2d1	protestantský
pastora	pastor	k1gMnSc2	pastor
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Johanna	Johann	k1gMnSc4	Johann
Davida	David	k1gMnSc2	David
Wysse	Wyss	k1gMnSc2	Wyss
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
napsal	napsat	k5eAaPmAgMnS	napsat
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
známým	známý	k2eAgInSc7d1	známý
románem	román	k1gInSc7	román
Daniela	Daniel	k1gMnSc2	Daniel
Defoea	Defoeus	k1gMnSc2	Defoeus
Robinson	Robinson	k1gMnSc1	Robinson
Crusoe	Cruso	k1gFnPc4	Cruso
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1794	[number]	k4	1794
<g/>
–	–	k?	–
<g/>
1798	[number]	k4	1798
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
budoucím	budoucí	k2eAgNnSc6d1	budoucí
knižním	knižní	k2eAgNnSc6d1	knižní
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Johann	Johann	k1gMnSc1	Johann
Rudolf	Rudolf	k1gMnSc1	Rudolf
Wyss	Wyss	k1gInSc1	Wyss
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
filozofie	filozofie	k1gFnSc2	filozofie
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
národní	národní	k2eAgFnSc2d1	národní
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
dílo	dílo	k1gNnSc4	dílo
upravit	upravit	k5eAaPmF	upravit
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
tiskem	tisk	k1gInSc7	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
líčí	líčit	k5eAaImIp3nS	líčit
osudy	osud	k1gInPc4	osud
chudé	chudý	k2eAgFnSc2d1	chudá
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
synové	syn	k1gMnPc1	syn
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
osmi	osm	k4xCc2	osm
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
příjmení	příjmení	k1gNnSc1	příjmení
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
originálním	originální	k2eAgInSc6d1	originální
textu	text	k1gInSc6	text
knihy	kniha	k1gFnSc2	kniha
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
v	v	k7c6	v
převyprávěné	převyprávěný	k2eAgFnSc6d1	převyprávěná
verzi	verze	k1gFnSc6	verze
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rodinu	rodina	k1gFnSc4	rodina
Braunových	Braunová	k1gFnPc2	Braunová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
opustit	opustit	k5eAaPmF	opustit
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
si	se	k3xPyFc3	se
nový	nový	k2eAgInSc4d1	nový
domov	domov	k1gInSc4	domov
v	v	k7c6	v
zámořských	zámořský	k2eAgFnPc6d1	zámořská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
lodi	loď	k1gFnSc2	loď
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
zachrání	zachránit	k5eAaPmIp3nS	zachránit
na	na	k7c6	na
neobydleném	obydlený	k2eNgInSc6d1	neobydlený
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nazvou	nazvat	k5eAaPmIp3nP	nazvat
Nové	Nové	k2eAgNnSc1d1	Nové
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
prožijí	prožít	k5eAaPmIp3nP	prožít
zde	zde	k6eAd1	zde
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
vzkvétající	vzkvétající	k2eAgFnSc1d1	vzkvétající
kolonie	kolonie	k1gFnSc1	kolonie
objevena	objevit	k5eAaPmNgFnS	objevit
anglickou	anglický	k2eAgFnSc7d1	anglická
korvetou	korveta	k1gFnSc7	korveta
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
vítězně	vítězně	k6eAd1	vítězně
utkají	utkat	k5eAaPmIp3nP	utkat
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
nereálnou	reálný	k2eNgFnSc7d1	nereálná
faunou	fauna	k1gFnSc7	fauna
a	a	k8xC	a
flórou	flóra	k1gFnSc7	flóra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sem	sem	k6eAd1	sem
autor	autor	k1gMnSc1	autor
umístil	umístit	k5eAaPmAgMnS	umístit
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
co	co	k9	co
nejpestřejší	pestrý	k2eAgInSc4d3	nejpestřejší
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
příběh	příběh	k1gInSc4	příběh
a	a	k8xC	a
seznámit	seznámit	k5eAaPmF	seznámit
dětského	dětský	k2eAgMnSc4d1	dětský
čtenáře	čtenář	k1gMnSc4	čtenář
s	s	k7c7	s
co	co	k9	co
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
různých	různý	k2eAgNnPc2d1	různé
exotických	exotický	k2eAgNnPc2d1	exotické
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
tak	tak	k9	tak
musí	muset	k5eAaImIp3nS	muset
bojovat	bojovat	k5eAaImF	bojovat
se	s	k7c7	s
lvy	lev	k1gMnPc7	lev
<g/>
,	,	kIx,	,
tygry	tygr	k1gMnPc7	tygr
<g/>
,	,	kIx,	,
medvědy	medvěd	k1gMnPc7	medvěd
<g/>
,	,	kIx,	,
slony	slon	k1gMnPc7	slon
<g/>
,	,	kIx,	,
hrochy	hroch	k1gMnPc7	hroch
<g/>
,	,	kIx,	,
šakaly	šakal	k1gMnPc7	šakal
<g/>
,	,	kIx,	,
hyenami	hyena	k1gFnPc7	hyena
a	a	k8xC	a
hroznýši	hroznýš	k1gMnPc7	hroznýš
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
lovit	lovit	k5eAaImF	lovit
antilopy	antilopa	k1gFnPc4	antilopa
<g/>
,	,	kIx,	,
plameňáky	plameňák	k1gMnPc4	plameňák
<g/>
,	,	kIx,	,
velryby	velryba	k1gFnPc4	velryba
<g/>
,	,	kIx,	,
lachtany	lachtan	k1gMnPc4	lachtan
<g/>
,	,	kIx,	,
mrože	mrož	k1gMnPc4	mrož
<g/>
,	,	kIx,	,
tuleně	tuleň	k1gMnPc4	tuleň
<g/>
,	,	kIx,	,
žraloky	žralok	k1gMnPc4	žralok
<g/>
,	,	kIx,	,
leguány	leguán	k1gMnPc4	leguán
<g/>
,	,	kIx,	,
holuby	holub	k1gMnPc4	holub
a	a	k8xC	a
vlaštovky	vlaštovka	k1gFnPc4	vlaštovka
<g/>
,	,	kIx,	,
chovat	chovat	k5eAaImF	chovat
psy	pes	k1gMnPc4	pes
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc4	kráva
<g/>
,	,	kIx,	,
buvoly	buvol	k1gMnPc4	buvol
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc4	koza
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnPc4	ovce
<g/>
,	,	kIx,	,
osly	osel	k1gMnPc4	osel
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
slepice	slepice	k1gFnPc4	slepice
<g/>
,	,	kIx,	,
husy	husa	k1gFnPc4	husa
i	i	k8xC	i
kachny	kachna	k1gFnPc4	kachna
a	a	k8xC	a
ochočit	ochočit	k5eAaPmF	ochočit
si	se	k3xPyFc3	se
orla	orel	k1gMnSc4	orel
a	a	k8xC	a
opici	opice	k1gFnSc4	opice
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
nalezištěm	naleziště	k1gNnSc7	naleziště
všech	všecek	k3xTgFnPc2	všecek
možných	možný	k2eAgFnPc2d1	možná
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Fritz	Fritz	k1gMnSc1	Fritz
také	také	k9	také
zachrání	zachránit	k5eAaPmIp3nS	zachránit
mladou	mladý	k2eAgFnSc4d1	mladá
Angličanku	Angličanka	k1gFnSc4	Angličanka
Jenny	Jenna	k1gFnSc2	Jenna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
na	na	k7c6	na
nedalekém	daleký	k2eNgInSc6d1	nedaleký
sopečném	sopečný	k2eAgInSc6d1	sopečný
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
(	(	kIx(	(
<g/>
zprávu	zpráva	k1gFnSc4	zpráva
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
přinese	přinést	k5eAaPmIp3nS	přinést
kormorán	kormorán	k1gMnSc1	kormorán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
kolonie	kolonie	k1gFnSc1	kolonie
objevena	objeven	k2eAgFnSc1d1	objevena
<g/>
,	,	kIx,	,
nikomu	nikdo	k3yNnSc3	nikdo
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pryč	pryč	k6eAd1	pryč
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
odjedou	odjet	k5eAaPmIp3nP	odjet
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
jen	jen	k9	jen
Fritz	Fritz	k1gMnSc1	Fritz
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jenny	Jenna	k1gFnSc2	Jenna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
zde	zde	k6eAd1	zde
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
prostřední	prostřední	k2eAgMnSc1d1	prostřední
syn	syn	k1gMnSc1	syn
Jakob	Jakob	k1gMnSc1	Jakob
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nakoupil	nakoupit	k5eAaPmAgInS	nakoupit
pro	pro	k7c4	pro
kolonii	kolonie	k1gFnSc4	kolonie
některé	některý	k3yIgFnPc4	některý
potřebné	potřebný	k2eAgFnPc4d1	potřebná
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
přibude	přibýt	k5eAaPmIp3nS	přibýt
také	také	k9	také
anglická	anglický	k2eAgFnSc1d1	anglická
rodina	rodina	k1gFnSc1	rodina
Wolstonových	Wolstonový	k2eAgInPc2d1	Wolstonový
s	s	k7c7	s
dcerami	dcera	k1gFnPc7	dcera
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
postupně	postupně	k6eAd1	postupně
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
mnohé	mnohý	k2eAgInPc4d1	mnohý
nedostatky	nedostatek	k1gInPc4	nedostatek
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dnes	dnes	k6eAd1	dnes
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
spíše	spíše	k9	spíše
úsměv	úsměv	k1gInSc4	úsměv
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Wyssův	Wyssův	k2eAgMnSc1d1	Wyssův
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
ke	k	k7c3	k
klasickým	klasický	k2eAgInPc3d1	klasický
dílům	díl	k1gInPc3	díl
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
důležitost	důležitost	k1gFnSc4	důležitost
lidské	lidský	k2eAgFnSc2d1	lidská
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
také	také	k9	také
rodinné	rodinný	k2eAgFnSc2d1	rodinná
i	i	k8xC	i
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
soudržnosti	soudržnost	k1gFnSc2	soudržnost
<g/>
,	,	kIx,	,
obětavosti	obětavost	k1gFnSc2	obětavost
a	a	k8xC	a
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Propaguje	propagovat	k5eAaImIp3nS	propagovat
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
únik	únik	k1gInSc1	únik
od	od	k7c2	od
evropské	evropský	k2eAgFnSc2d1	Evropská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
mínění	mínění	k1gNnSc2	mínění
dostává	dostávat	k5eAaImIp3nS	dostávat
svými	svůj	k3xOyFgFnPc7	svůj
válkami	válka	k1gFnPc7	válka
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
do	do	k7c2	do
slepé	slepý	k2eAgFnSc2d1	slepá
uličky	ulička	k1gFnSc2	ulička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pokračování	pokračování	k1gNnSc1	pokračování
od	od	k7c2	od
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
==	==	k?	==
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
oblíbené	oblíbený	k2eAgFnSc3d1	oblíbená
dětské	dětský	k2eAgFnSc3d1	dětská
četbě	četba	k1gFnSc3	četba
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
ho	on	k3xPp3gNnSc4	on
natolik	natolik	k6eAd1	natolik
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
roku	rok	k1gInSc3	rok
1900	[number]	k4	1900
napsal	napsat	k5eAaPmAgInS	napsat
pokračování	pokračování	k1gNnSc4	pokračování
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Druhá	druhý	k4xOgFnSc1	druhý
vlast	vlast	k1gFnSc1	vlast
(	(	kIx(	(
<g/>
Seconde	Second	k1gInSc5	Second
patrie	patrie	k1gFnSc2	patrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
Vernových	Vernův	k2eAgFnPc6d1	Vernova
knihách	kniha	k1gFnPc6	kniha
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
motivy	motiv	k1gInPc7	motiv
ze	z	k7c2	z
Švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
Robinsona	Robinson	k1gMnSc2	Robinson
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Obydlí	obydlet	k5eAaPmIp3nS	obydlet
v	v	k7c6	v
obrovském	obrovský	k2eAgInSc6d1	obrovský
vykotlaném	vykotlaný	k2eAgInSc6d1	vykotlaný
stromě	strom	k1gInSc6	strom
(	(	kIx(	(
<g/>
Škola	škola	k1gFnSc1	škola
robinsonů	robinson	k1gMnPc2	robinson
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
école	école	k1gInSc1	école
des	des	k1gNnSc1	des
robinsons	robinsons	k1gInSc1	robinsons
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ochočení	ochočení	k1gNnSc1	ochočení
pštrosa	pštros	k1gMnSc2	pštros
(	(	kIx(	(
<g/>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Perils	Perils	k1gInSc1	Perils
of	of	k?	of
the	the	k?	the
Wild	Wild	k1gInSc1	Wild
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
divočina	divočina	k1gFnSc1	divočina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Francis	Francis	k1gFnSc1	Francis
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ztracený	ztracený	k2eAgInSc4d1	ztracený
němý	němý	k2eAgInSc4d1	němý
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Swiss	Swiss	k1gInSc1	Swiss
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Edward	Edward	k1gMnSc1	Edward
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Swiss	Swiss	k1gInSc1	Swiss
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
William	William	k1gInSc1	William
A.	A.	kA	A.
Graham	graham	k1gInSc1	graham
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Swiss	Swiss	k1gInSc1	Swiss
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ken	Ken	k1gMnSc1	Ken
Annakin	Annakin	k1gMnSc1	Annakin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Swiss	Swiss	k1gInSc1	Swiss
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Harry	Harra	k1gFnSc2	Harra
Harris	Harris	k1gFnSc1	Harris
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Kazoku	Kazok	k1gInSc3	Kazok
Robinson	Robinson	k1gMnSc1	Robinson
hyôryû	hyôryû	k?	hyôryû
fushigina	fushigina	k1gFnSc1	fushigina
shima	shima	k1gFnSc1	shima
no	no	k9	no
furône	furônout	k5eAaImIp3nS	furônout
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Yoshihiro	Yoshihiro	k1gNnSc1	Yoshihiro
Kuroda	Kurod	k1gMnSc2	Kurod
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Beverly	Beverla	k1gFnPc1	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
z	z	k7c2	z
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hills	k1gInSc1	Hills
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Troy	Troa	k1gFnSc2	Troa
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
New	New	k1gFnPc1	New
Swiss	Swissa	k1gFnPc2	Swissa
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
rodina	rodina	k1gFnSc1	rodina
Robinsonů	Robinson	k1gMnPc2	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Stewart	Stewart	k1gMnSc1	Stewart
Raffill	Raffill	k1gMnSc1	Raffill
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Swiss	Swiss	k1gInSc1	Swiss
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
:	:	kIx,	:
Lost	Lost	k1gMnSc1	Lost
in	in	k?	in
the	the	k?	the
Jungle	Jungle	k1gInSc1	Jungle
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
:	:	kIx,	:
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Edgar	Edgar	k1gMnSc1	Edgar
G.	G.	kA	G.
Ulmer	Ulmer	k1gInSc1	Ulmer
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stranded	Stranded	k1gInSc1	Stranded
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Ztroskotaní	ztroskotaný	k2eAgMnPc1d1	ztroskotaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Charles	Charles	k1gMnSc1	Charles
Beeson	Beeson	k1gInSc1	Beeson
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lukasík	Lukasík	k1gMnSc1	Lukasík
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaromír	Jaromír	k1gMnSc1	Jaromír
Červenka	Červenka	k1gMnSc1	Červenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinzon	Robinzon	k1gMnSc1	Robinzon
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
Bohumír	Bohumír	k1gMnSc1	Bohumír
Polách	Polách	k1gMnSc1	Polách
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinzon	Robinzon	k1gMnSc1	Robinzon
<g/>
,	,	kIx,	,
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1973	[number]	k4	1973
a	a	k8xC	a
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
Bohumír	Bohumír	k1gMnSc1	Bohumír
Polách	Polách	k1gMnSc1	Polách
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinzon	Robinzon	k1gMnSc1	Robinzon
<g/>
,	,	kIx,	,
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
Bohumír	Bohumír	k1gMnSc1	Bohumír
Polách	Polách	k1gMnSc1	Polách
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Medek	Medek	k1gMnSc1	Medek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Robinsonů	Robinson	k1gMnPc2	Robinson
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
anglickou	anglický	k2eAgFnSc4d1	anglická
adaptaci	adaptace	k1gFnSc4	adaptace
Swiss	Swissa	k1gFnPc2	Swissa
family	famila	k1gFnSc2	famila
Robinson	Robinson	k1gMnSc1	Robinson
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Šimon	Šimon	k1gMnSc1	Šimon
Jimel	Jimel	k1gMnSc1	Jimel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Robinzonáda	robinzonáda	k1gFnSc1	robinzonáda
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Swiss	Swiss	k1gInSc1	Swiss
Family	Famila	k1gFnSc2	Famila
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
;	;	kIx,	;
or	or	k?	or
Adventures	Adventures	k1gInSc1	Adventures
in	in	k?	in
a	a	k8xC	a
Desert	desert	k1gInSc1	desert
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
Schweizerische	Schweizerische	k1gInSc1	Schweizerische
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Zusammenfassung	Zusammenfassung	k1gInSc1	Zusammenfassung
und	und	k?	und
Beobachtungen	Beobachtungen	k1gInSc1	Beobachtungen
zur	zur	k?	zur
Rezeption	Rezeption	k1gInSc1	Rezeption
bei	bei	k?	bei
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.kodovky.cz/kniha/101	[url]	k4	http://www.kodovky.cz/kniha/101
</s>
</p>
