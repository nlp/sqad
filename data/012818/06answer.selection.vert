<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Schweizerische	Schweizerische	k1gInSc1	Schweizerische
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
román	román	k1gInSc4	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
německy	německy	k6eAd1	německy
píšícího	píšící	k2eAgMnSc2d1	píšící
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
protestantského	protestantský	k2eAgMnSc2d1	protestantský
pastora	pastor	k1gMnSc2	pastor
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Johanna	Johann	k1gMnSc4	Johann
Davida	David	k1gMnSc2	David
Wysse	Wyss	k1gMnSc2	Wyss
<g/>
.	.	kIx.	.
</s>
