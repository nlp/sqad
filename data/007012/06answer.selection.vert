<s>
První	první	k4xOgFnSc1
známá	známý	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1822	[number]	k4
francouzským	francouzský	k2eAgMnSc7d1
vynálezcem	vynálezce	k1gMnSc7
Nicéphorem	Nicéphor	k1gMnSc7
Niépcem	Niépec	k1gMnSc7
<g/>
,	,	kIx,
při	při	k7c6
pokusech	pokus	k1gInPc6
o	o	k7c4
vyhotovení	vyhotovení	k1gNnSc4
tisku	tisk	k1gInSc2
však	však	k9
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>