<p>
<s>
Šiloašský	Šiloašský	k2eAgInSc1d1	Šiloašský
nápis	nápis	k1gInSc1	nápis
nebo	nebo	k8xC	nebo
Silvánský	Silvánský	k2eAgInSc1d1	Silvánský
nápis	nápis	k1gInSc1	nápis
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jeruzalémské	jeruzalémský	k2eAgFnSc2d1	Jeruzalémská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Silván	Silvána	k1gFnPc2	Silvána
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
textu	text	k1gInSc2	text
původně	původně	k6eAd1	původně
vyrytého	vyrytý	k2eAgInSc2d1	vyrytý
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
Chizkijášova	Chizkijášův	k2eAgInSc2d1	Chizkijášův
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
Gichonského	Gichonský	k2eAgInSc2d1	Gichonský
pramene	pramen	k1gInSc2	pramen
do	do	k7c2	do
Šiloašského	Šiloašský	k2eAgInSc2d1	Šiloašský
rybníka	rybník	k1gInSc2	rybník
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
americkým	americký	k2eAgMnSc7d1	americký
biblistou	biblista	k1gMnSc7	biblista
Edwardem	Edward	k1gMnSc7	Edward
Robinsonem	Robinson	k1gMnSc7	Robinson
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
je	být	k5eAaImIp3nS	být
záznamem	záznam	k1gInSc7	záznam
o	o	k7c4	o
dokončení	dokončení	k1gNnSc4	dokončení
ražby	ražba	k1gFnSc2	ražba
tunelu	tunel	k1gInSc2	tunel
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
známým	známý	k2eAgInPc3d1	známý
nápisům	nápis	k1gInPc3	nápis
tohoto	tento	k3xDgMnSc4	tento
druhu	druh	k1gInSc2	druh
psaným	psaný	k2eAgMnSc7d1	psaný
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
paleo-hebrejským	paleoebrejský	k2eAgNnSc7d1	paleo-hebrejský
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
ze	z	k7c2	z
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
pamětní	pamětní	k2eAgInSc1d1	pamětní
nápis	nápis	k1gInSc1	nápis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
také	také	k9	také
připisován	připisován	k2eAgInSc4d1	připisován
votivní	votivní	k2eAgInSc4d1	votivní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
objevu	objev	k1gInSc2	objev
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
Chizkijášův	Chizkijášův	k2eAgInSc1d1	Chizkijášův
tunel	tunel	k1gInSc1	tunel
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
důkladně	důkladně	k6eAd1	důkladně
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
archeologové	archeolog	k1gMnPc1	archeolog
jako	jako	k8xC	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edward	Edward	k1gMnSc1	Edward
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Wilson	Wilson	k1gMnSc1	Wilson
a	a	k8xC	a
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Warren	Warrna	k1gFnPc2	Warrna
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
nevšiml	všimnout	k5eNaPmAgInS	všimnout
nápisu	nápis	k1gInSc2	nápis
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
kvůli	kvůli	k7c3	kvůli
minerálním	minerální	k2eAgFnPc3d1	minerální
usazeninám	usazenina	k1gFnPc3	usazenina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gInSc4	on
téměř	téměř	k6eAd1	téměř
zakryly	zakrýt	k5eAaPmAgInP	zakrýt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Easton	Easton	k1gInSc1	Easton
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bible	bible	k1gFnSc1	bible
Dictionary	Dictionar	k1gInPc1	Dictionar
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
nápis	nápis	k1gInSc1	nápis
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
asi	asi	k9	asi
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
tunelu	tunel	k1gInSc2	tunel
objevil	objevit	k5eAaPmAgMnS	objevit
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
brodil	brodit	k5eAaImAgInS	brodit
tunelem	tunel	k1gInSc7	tunel
od	od	k7c2	od
Šiloašského	Šiloašský	k2eAgInSc2d1	Šiloašský
rybníku	rybník	k1gInSc3	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šiloašský	Šiloašský	k2eAgInSc1d1	Šiloašský
nápis	nápis	k1gInSc1	nápis
byl	být	k5eAaImAgInS	být
tajně	tajně	k6eAd1	tajně
vysekán	vysekat	k5eAaPmNgInS	vysekat
ze	z	k7c2	z
stěny	stěna	k1gFnSc2	stěna
tunelu	tunel	k1gInSc2	tunel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
a	a	k8xC	a
rozbit	rozbit	k2eAgInSc1d1	rozbit
na	na	k7c4	na
kusy	kus	k1gInPc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
díky	díky	k7c3	díky
úsilí	úsilí	k1gNnSc3	úsilí
britského	britský	k2eAgMnSc2d1	britský
konzula	konzul	k1gMnSc2	konzul
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
pak	pak	k6eAd1	pak
umístěny	umístit	k5eAaPmNgFnP	umístit
do	do	k7c2	do
Istanbulského	istanbulský	k2eAgNnSc2d1	istanbulské
archeologického	archeologický	k2eAgNnSc2d1	Archeologické
musea	museum	k1gNnSc2	museum
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgMnSc1d1	jeruzalémský
starosta	starosta	k1gMnSc1	starosta
Uri	Uri	k1gFnSc2	Uri
Lupolianski	Lupoliansk	k1gFnSc2	Lupoliansk
při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
tureckým	turecký	k2eAgMnSc7d1	turecký
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
Namikem	Namik	k1gInSc7	Namik
Tanem	Tanem	k?	Tanem
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
požádal	požádat	k5eAaPmAgInS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tabulka	tabulka	k1gFnSc1	tabulka
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
gesto	gesto	k1gNnSc1	gesto
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
spojenci	spojenec	k1gMnPc7	spojenec
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
žádost	žádost	k1gFnSc4	žádost
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Šiloašský	Šiloašský	k2eAgInSc1d1	Šiloašský
nápis	nápis	k1gInSc1	nápis
byl	být	k5eAaImAgInS	být
majetkem	majetek	k1gInSc7	majetek
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
tedy	tedy	k9	tedy
ke	k	k7c3	k
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
dědictví	dědictví	k1gNnSc3	dědictví
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jako	jako	k9	jako
projev	projev	k1gInSc4	projev
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Turecka	Turecko	k1gNnSc2	Turecko
prezident	prezident	k1gMnSc1	prezident
Abdullah	Abdullah	k1gMnSc1	Abdullah
Gul	gula	k1gFnPc2	gula
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
nápis	nápis	k1gInSc4	nápis
krátkodobě	krátkodobě	k6eAd1	krátkodobě
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
vystavení	vystavení	k1gNnSc4	vystavení
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tunel	tunel	k1gInSc1	tunel
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
jiný	jiný	k2eAgInSc1d1	jiný
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
vedoucí	vedoucí	k1gMnPc1	vedoucí
od	od	k7c2	od
Gichonského	Gichonský	k2eAgInSc2d1	Gichonský
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
Šiloašskému	Šiloašský	k2eAgInSc3d1	Šiloašský
rybníku	rybník	k1gInSc3	rybník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poněkud	poněkud	k6eAd1	poněkud
přímější	přímý	k2eAgFnSc7d2	přímější
trasou	trasa	k1gFnSc7	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
Kanál	kanál	k1gInSc1	kanál
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
Reicha	Reich	k1gMnSc2	Reich
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1800	[number]	k4	1800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc6d1	bronzová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
m	m	kA	m
hluboká	hluboký	k2eAgFnSc1d1	hluboká
strouha	strouha	k1gFnSc1	strouha
zakrytá	zakrytý	k2eAgFnSc1d1	zakrytá
seshora	seshora	k6eAd1	seshora
kamennými	kamenný	k2eAgFnPc7d1	kamenná
deskami	deska	k1gFnPc7	deska
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
zamaskovány	zamaskovat	k5eAaPmNgInP	zamaskovat
zelení	zeleň	k1gFnSc7	zeleň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
užší	úzký	k2eAgNnSc1d2	užší
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
průchozí	průchozí	k2eAgFnSc1d1	průchozí
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vyústění	vyústění	k1gNnSc2	vyústění
u	u	k7c2	u
Šiloašského	Šiloašský	k2eAgInSc2d1	Šiloašský
rybníka	rybník	k1gInSc2	rybník
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
menších	malý	k2eAgFnPc2d2	menší
výpustí	výpust	k1gFnPc2	výpust
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
byly	být	k5eAaImAgFnP	být
zavlažovány	zavlažovat	k5eAaImNgFnP	zavlažovat
zahrady	zahrada	k1gFnPc1	zahrada
v	v	k7c6	v
Kidrónském	Kidrónský	k2eAgNnSc6d1	Kidrónský
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Pravěký	pravěký	k2eAgInSc1d1	pravěký
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
horském	horský	k2eAgInSc6d1	horský
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dobře	dobře	k6eAd1	dobře
chráněný	chráněný	k2eAgInSc1d1	chráněný
téměř	téměř	k6eAd1	téměř
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
jednu	jeden	k4xCgFnSc4	jeden
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgInSc1d1	hlavní
zdroj	zdroj	k1gInSc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
Gichonský	Gichonský	k2eAgInSc4d1	Gichonský
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
Kidrónským	Kidrónský	k2eAgNnSc7d1	Kidrónský
údolím	údolí	k1gNnSc7	údolí
vně	vně	k6eAd1	vně
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
případného	případný	k2eAgNnSc2d1	případné
obléhání	obléhání	k1gNnSc2	obléhání
by	by	kYmCp3nS	by
město	město	k1gNnSc1	město
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
zdroj	zdroj	k1gInSc4	zdroj
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bible	bible	k1gFnSc1	bible
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Chizkijáš	Chizkijáš	k1gMnSc1	Chizkijáš
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Albrighta	Albright	k1gInSc2	Albright
715	[number]	k4	715
<g/>
–	–	k?	–
<g/>
687	[number]	k4	687
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
asyrským	asyrský	k2eAgNnSc7d1	asyrské
obležením	obležení	k1gNnSc7	obležení
nechal	nechat	k5eAaPmAgMnS	nechat
vyústění	vyústění	k1gNnSc3	vyústění
pramene	pramen	k1gInSc2	pramen
zasypat	zasypat	k5eAaPmF	zasypat
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
odvedl	odvést	k5eAaPmAgMnS	odvést
tunelem	tunel	k1gInSc7	tunel
do	do	k7c2	do
Šiloašského	Šiloašský	k2eAgInSc2d1	Šiloašský
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
Královská	královský	k2eAgFnSc1d1	královská
20,20	[number]	k4	20,20
(	(	kIx(	(
<g/>
Ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
překlad	překlad	k1gInSc1	překlad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
O	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
příbězích	příběh	k1gInPc6	příběh
Chizkijášových	Chizkijášová	k1gFnPc2	Chizkijášová
<g/>
,	,	kIx,	,
o	o	k7c6	o
všech	všecek	k3xTgInPc6	všecek
jeho	jeho	k3xOp3gInPc6	jeho
bohatýrských	bohatýrský	k2eAgInPc6d1	bohatýrský
činech	čin	k1gInPc6	čin
a	a	k8xC	a
jak	jak	k6eAd1	jak
udělal	udělat	k5eAaPmAgInS	udělat
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
vodovod	vodovod	k1gInSc1	vodovod
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
přivedl	přivést	k5eAaPmAgMnS	přivést
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
letopisů	letopis	k1gInPc2	letopis
králů	král	k1gMnPc2	král
judských	judský	k2eAgMnPc2d1	judský
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2	[number]	k4	2
Paralipomenon	paralipomenon	k1gNnSc1	paralipomenon
32,2	[number]	k4	32,2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
Ekum	Ekum	k1gInSc1	Ekum
<g/>
.	.	kIx.	.
překlad	překlad	k1gInSc1	překlad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
uviděl	uvidět	k5eAaPmAgMnS	uvidět
Chizkijáš	Chizkijáš	k1gFnSc4	Chizkijáš
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sancherib	Sancherib	k1gMnSc1	Sancherib
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
Jeruzalému	Jeruzalém	k1gInSc3	Jeruzalém
<g/>
,	,	kIx,	,
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
velmoži	velmož	k1gMnPc7	velmož
a	a	k8xC	a
bohatýry	bohatýr	k1gMnPc7	bohatýr
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasypou	zasypat	k5eAaPmIp3nP	zasypat
vodní	vodní	k2eAgInPc1d1	vodní
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
vně	vně	k6eAd1	vně
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
zasypali	zasypat	k5eAaPmAgMnP	zasypat
všechny	všechen	k3xTgInPc4	všechen
prameny	pramen	k1gInPc4	pramen
i	i	k8xC	i
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
protékal	protékat	k5eAaImAgInS	protékat
středem	střed	k1gInSc7	střed
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Řekli	říct	k5eAaPmAgMnP	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
mají	mít	k5eAaImIp3nP	mít
asyrští	asyrský	k2eAgMnPc1d1	asyrský
králové	král	k1gMnPc1	král
najít	najít	k5eAaPmF	najít
tolik	tolik	k6eAd1	tolik
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
přitáhnou	přitáhnout	k5eAaPmIp3nP	přitáhnout
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Překlad	překlad	k1gInSc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
kvůli	kvůli	k7c3	kvůli
usazeninám	usazenina	k1gFnPc3	usazenina
téměř	téměř	k6eAd1	téměř
nečitelný	čitelný	k2eNgInSc4d1	nečitelný
nápis	nápis	k1gInSc4	nápis
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
přečíst	přečíst	k5eAaPmF	přečíst
profesor	profesor	k1gMnSc1	profesor
A.H.	A.H.	k1gMnSc1	A.H.
Sayce	Sayce	k1gMnSc1	Sayce
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
očištěn	očistit	k5eAaPmNgInS	očistit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
zředěné	zředěný	k2eAgFnSc2d1	zředěná
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
zřetelněji	zřetelně	k6eAd2	zřetelně
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
řádek	řádka	k1gFnPc2	řádka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
oddělována	oddělovat	k5eAaImNgNnP	oddělovat
tečkami	tečka	k1gFnPc7	tečka
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
slovo	slovo	k1gNnSc1	slovo
zada	zadum	k1gNnSc2	zadum
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
řádku	řádek	k1gInSc6	řádek
je	být	k5eAaImIp3nS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
kvůli	kvůli	k7c3	kvůli
trhlině	trhlina	k1gFnSc3	trhlina
v	v	k7c6	v
kameni	kámen	k1gInSc6	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Překlad	překlad	k1gInSc1	překlad
nápisu	nápis	k1gInSc2	nápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
...	...	k?	...
proražení	proražený	k2eAgMnPc1d1	proražený
...	...	k?	...
a	a	k8xC	a
taková	takový	k3xDgFnSc1	takový
byla	být	k5eAaImAgFnS	být
událost	událost	k1gFnSc4	událost
proražení	proražení	k1gNnSc2	proražení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ještě	ještě	k6eAd1	ještě
...	...	k?	...
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
kopáči	kopáč	k1gMnPc1	kopáč
zvedali	zvedat	k5eAaImAgMnP	zvedat
<g/>
)	)	kIx)	)
motyku	motyka	k1gFnSc4	motyka
jeden	jeden	k4xCgMnSc1	jeden
proti	proti	k7c3	proti
druhému	druhý	k4xOgMnSc3	druhý
a	a	k8xC	a
zbývalo	zbývat	k5eAaImAgNnS	zbývat
prorazit	prorazit	k5eAaPmF	prorazit
ještě	ještě	k9	ještě
tři	tři	k4xCgInPc4	tři
lokty	loket	k1gInPc4	loket
...	...	k?	...
bylo	být	k5eAaImAgNnS	být
slyšet	slyšet	k5eAaImF	slyšet
hlas	hlas	k1gInSc4	hlas
...	...	k?	...
</s>
</p>
<p>
<s>
jak	jak	k6eAd1	jak
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
volali	volat	k5eAaImAgMnP	volat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
<g/>
)	)	kIx)	)
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
byla	být	k5eAaImAgFnS	být
ZADA	ZADA	kA	ZADA
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
...	...	k?	...
a	a	k8xC	a
v	v	k7c4	v
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
proražení	proražený	k2eAgMnPc1d1	proražený
raziči	razič	k1gMnPc1	razič
kopali	kopat	k5eAaImAgMnP	kopat
jeden	jeden	k4xCgMnSc1	jeden
proti	proti	k7c3	proti
druhému	druhý	k4xOgMnSc3	druhý
<g/>
,	,	kIx,	,
motyka	motyka	k1gFnSc1	motyka
proti	proti	k7c3	proti
motyce	motyka	k1gFnSc3	motyka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tekla	téct	k5eAaImAgNnP	téct
</s>
</p>
<p>
<s>
voda	voda	k1gFnSc1	voda
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
rybníku	rybník	k1gInSc3	rybník
<g/>
,	,	kIx,	,
1200	[number]	k4	1200
loktů	loket	k1gInPc2	loket
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
100	[number]	k4	100
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
loktů	loket	k1gInPc2	loket
byla	být	k5eAaImAgFnS	být
výška	výška	k1gFnSc1	výška
skály	skála	k1gFnSc2	skála
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
kopáčů	kopáč	k1gInPc2	kopáč
...	...	k?	...
<g/>
Nápis	nápis	k1gInSc1	nápis
tedy	tedy	k9	tedy
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
budování	budování	k1gNnSc1	budování
tunelu	tunel	k1gInSc2	tunel
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
textu	text	k1gInSc2	text
dvě	dva	k4xCgFnPc4	dva
čety	četa	k1gFnPc1	četa
kopáčů	kopáč	k1gMnPc2	kopáč
postupovaly	postupovat	k5eAaImAgFnP	postupovat
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
konců	konec	k1gInPc2	konec
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nebyl	být	k5eNaImAgInS	být
průběh	průběh	k1gInSc1	průběh
tak	tak	k6eAd1	tak
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
z	z	k7c2	z
nápisu	nápis	k1gInSc2	nápis
<g/>
;	;	kIx,	;
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
setkání	setkání	k1gNnSc2	setkání
tunel	tunel	k1gInSc4	tunel
náhle	náhle	k6eAd1	náhle
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
a	a	k8xC	a
osy	osa	k1gFnPc1	osa
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
současných	současný	k2eAgMnPc2d1	současný
archeologů	archeolog	k1gMnPc2	archeolog
byl	být	k5eAaImAgInS	být
směr	směr	k1gInSc1	směr
kopání	kopání	k1gNnSc2	kopání
určován	určován	k2eAgInSc1d1	určován
akusticky	akusticky	k6eAd1	akusticky
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
i	i	k9	i
popis	popis	k1gInSc4	popis
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
Šiloašském	Šiloašský	k2eAgInSc6d1	Šiloašský
nápisu	nápis	k1gInSc6	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
opomíjená	opomíjený	k2eAgFnSc1d1	opomíjená
poslední	poslední	k2eAgFnSc1d1	poslední
věta	věta	k1gFnSc1	věta
totiž	totiž	k9	totiž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavitelé	stavitel	k1gMnPc1	stavitel
znali	znát	k5eAaImAgMnP	znát
hloubku	hloubka	k1gFnSc4	hloubka
tunelu	tunel	k1gInSc2	tunel
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
jeho	jeho	k3xOp3gInSc2	jeho
průběhu	průběh	k1gInSc2	průběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Siloam	Siloam	k1gInSc1	Siloam
inscription	inscription	k1gInSc1	inscription
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chizkijášův	Chizkijášův	k2eAgInSc1d1	Chizkijášův
tunel	tunel	k1gInSc1	tunel
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šiloašský	Šiloašský	k2eAgInSc4d1	Šiloašský
nápis	nápis	k1gInSc4	nápis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zřetelnější	zřetelný	k2eAgInSc1d2	zřetelnější
snímek	snímek	k1gInSc1	snímek
nápisu	nápis	k1gInSc2	nápis
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Šiloašský	Šiloašský	k2eAgInSc1d1	Šiloašský
nápis	nápis	k1gInSc1	nápis
-	-	kIx~	-
transkripvce	transkripvce	k1gMnSc1	transkripvce
a	a	k8xC	a
vokalizace	vokalizace	k1gFnSc1	vokalizace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ligatura	ligatura	k1gFnSc1	ligatura
v	v	k7c6	v
nápisu	nápis	k1gInSc6	nápis
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jewish	Jewish	k1gInSc1	Jewish
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
<g/>
:	:	kIx,	:
Šiloašský	Šiloašský	k2eAgInSc1d1	Šiloašský
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
:	:	kIx,	:
Siloe	Siloe	k1gFnSc1	Siloe
</s>
</p>
