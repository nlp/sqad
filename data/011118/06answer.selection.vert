<s>
Šiloašský	Šiloašský	k2eAgInSc1d1	Šiloašský
nápis	nápis	k1gInSc1	nápis
nebo	nebo	k8xC	nebo
Silvánský	Silvánský	k2eAgInSc1d1	Silvánský
nápis	nápis	k1gInSc1	nápis
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jeruzalémské	jeruzalémský	k2eAgFnSc2d1	Jeruzalémská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Silván	Silvána	k1gFnPc2	Silvána
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
textu	text	k1gInSc2	text
původně	původně	k6eAd1	původně
vyrytého	vyrytý	k2eAgInSc2d1	vyrytý
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
Chizkijášova	Chizkijášův	k2eAgInSc2d1	Chizkijášův
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
Gichonského	Gichonský	k2eAgInSc2d1	Gichonský
pramene	pramen	k1gInSc2	pramen
do	do	k7c2	do
Šiloašského	Šiloašský	k2eAgInSc2d1	Šiloašský
rybníka	rybník	k1gInSc2	rybník
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
