<p>
<s>
Lefka	Lefka	k1gMnSc1	Lefka
Ori	Ori	k1gMnSc1	Ori
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Λ	Λ	k?	Λ
Ό	Ό	k?	Ό
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc4	pohoří
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
Lefka	Lefka	k1gFnSc1	Lefka
Ori	Ori	k1gFnSc2	Ori
"	"	kIx"	"
<g/>
Bílé	bílý	k2eAgFnPc1d1	bílá
hory	hora	k1gFnPc1	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vápencový	vápencový	k2eAgInSc1d1	vápencový
masiv	masiv	k1gInSc1	masiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
viditelný	viditelný	k2eAgInSc1d1	viditelný
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
bělostné	bělostný	k2eAgFnSc3d1	bělostná
barvě	barva	k1gFnSc3	barva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vrcholu	vrchol	k1gInSc2	vrchol
Pachnes	Pachnes	k1gInSc4	Pachnes
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
2	[number]	k4	2
453	[number]	k4	453
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrcholy	vrchol	k1gInPc4	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
Pachnes	Pachnes	k1gInSc1	Pachnes
(	(	kIx(	(
<g/>
2	[number]	k4	2
453	[number]	k4	453
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trocharis	Trocharis	k1gFnSc1	Trocharis
(	(	kIx(	(
<g/>
2	[number]	k4	2
410	[number]	k4	410
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grias	Grias	k1gMnSc1	Grias
Sorsos	Sorsos	k1gMnSc1	Sorsos
(	(	kIx(	(
<g/>
2	[number]	k4	2
331	[number]	k4	331
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kastro	Kastro	k1gNnSc1	Kastro
(	(	kIx(	(
<g/>
2	[number]	k4	2
218	[number]	k4	218
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kakovoli	Kakovoli	k6eAd1	Kakovoli
(	(	kIx(	(
<g/>
2	[number]	k4	2
214	[number]	k4	214
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zaranokefala	Zaranokefat	k5eAaPmAgFnS	Zaranokefat
(	(	kIx(	(
<g/>
2	[number]	k4	2
140	[number]	k4	140
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Volakias	Volakias	k1gInSc1	Volakias
(	(	kIx(	(
<g/>
2	[number]	k4	2
116	[number]	k4	116
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gingilos	Gingilos	k1gInSc1	Gingilos
(	(	kIx(	(
<g/>
2	[number]	k4	2
080	[number]	k4	080
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Dostupnost	dostupnost	k1gFnSc1	dostupnost
a	a	k8xC	a
turismus	turismus	k1gInSc1	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
nedostupnost	nedostupnost	k1gFnSc1	nedostupnost
pohoří	pohoří	k1gNnSc2	pohoří
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
při	při	k7c6	při
výstupech	výstup	k1gInPc6	výstup
na	na	k7c4	na
mnohé	mnohý	k2eAgInPc4d1	mnohý
vrcholy	vrchol	k1gInPc4	vrchol
pohoří	pohořet	k5eAaPmIp3nS	pohořet
překonávat	překonávat	k5eAaImF	překonávat
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k9	třeba
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Páchnes	Páchnesa	k1gFnPc2	Páchnesa
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
snadný	snadný	k2eAgInSc1d1	snadný
-	-	kIx~	-
od	od	k7c2	od
konce	konec	k1gInSc2	konec
kamenité	kamenitý	k2eAgFnSc2d1	kamenitá
silnice	silnice	k1gFnSc2	silnice
z	z	k7c2	z
planiny	planina	k1gFnSc2	planina
Anopoli	Anopole	k1gFnSc4	Anopole
se	se	k3xPyFc4	se
po	po	k7c6	po
asi	asi	k9	asi
max	max	kA	max
<g/>
.	.	kIx.	.
45	[number]	k4	45
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
ne	ne	k9	ne
<g />
.	.	kIx.	.
</s>
<s>
příliš	příliš	k6eAd1	příliš
strmého	strmý	k2eAgInSc2d1	strmý
výstupu	výstup	k1gInSc2	výstup
po	po	k7c6	po
dobře	dobře	k6eAd1	dobře
vybudovaném	vybudovaný	k2eAgInSc6d1	vybudovaný
chodníku	chodník	k1gInSc6	chodník
dojde	dojít	k5eAaPmIp3nS	dojít
do	do	k7c2	do
sedla	sedlo	k1gNnSc2	sedlo
Rousiés	Rousiésa	k1gFnPc2	Rousiésa
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2130	[number]	k4	2130
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vede	vést	k5eAaImIp3nS	vést
další	další	k2eAgFnSc1d1	další
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
často	často	k6eAd1	často
chozená	chozený	k2eAgFnSc1d1	chozená
pěšina	pěšina	k1gFnSc1	pěšina
<g/>
,	,	kIx,	,
značená	značený	k2eAgFnSc1d1	značená
jak	jak	k8xC	jak
kamennými	kamenný	k2eAgInPc7d1	kamenný
mužíky	mužík	k1gInPc7	mužík
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
<g/>
)	)	kIx)	)
červenobílými	červenobílý	k2eAgFnPc7d1	červenobílá
značkami	značka	k1gFnPc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sedla	sedlo	k1gNnSc2	sedlo
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Páchnes	Páchnes	k1gInSc1	Páchnes
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
obtížnost	obtížnost	k1gFnSc4	obtížnost
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
přes	přes	k7c4	přes
letní	letní	k2eAgInPc4d1	letní
měsíce	měsíc	k1gInPc4	měsíc
mají	mít	k5eAaImIp3nP	mít
prakticky	prakticky	k6eAd1	prakticky
status	status	k1gInSc4	status
horské	horský	k2eAgFnSc2d1	horská
pouště	poušť	k1gFnSc2	poušť
<g/>
)	)	kIx)	)
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
velká	velký	k2eAgNnPc1d1	velké
vedra	vedro	k1gNnPc1	vedro
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
Rousiés	Rousiésa	k1gFnPc2	Rousiésa
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
celoročně	celoročně	k6eAd1	celoročně
fungující	fungující	k2eAgInSc4d1	fungující
pramen	pramen	k1gInSc4	pramen
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
celoročně	celoročně	k6eAd1	celoročně
fungujících	fungující	k2eAgMnPc2d1	fungující
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jen	jen	k6eAd1	jen
několik	několik	k4yIc1	několik
a	a	k8xC	a
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
jejich	jejich	k3xOp3gFnSc4	jejich
polohu	poloha	k1gFnSc4	poloha
znát	znát	k5eAaImF	znát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pohoří	pohoří	k1gNnSc4	pohoří
lze	lze	k6eAd1	lze
k	k	k7c3	k
nocování	nocování	k1gNnSc3	nocování
využít	využít	k5eAaPmF	využít
přístavku	přístavek	k1gInSc2	přístavek
horské	horský	k2eAgFnSc2d1	horská
chaty	chata	k1gFnSc2	chata
Volika	Volika	k1gFnSc1	Volika
nad	nad	k7c7	nad
vesnicí	vesnice	k1gFnSc7	vesnice
Kámbi	Kámb	k1gFnSc2	Kámb
(	(	kIx(	(
<g/>
chata	chata	k1gFnSc1	chata
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
však	však	k9	však
uzamčená	uzamčený	k2eAgFnSc1d1	uzamčená
<g/>
)	)	kIx)	)
či	či	k8xC	či
bivak	bivak	k1gInSc4	bivak
na	na	k7c6	na
horské	horský	k2eAgFnSc6d1	horská
planině	planina	k1gFnSc6	planina
Katsiveli	Katsivel	k1gInSc6	Katsivel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kamenné	kamenný	k2eAgInPc1d1	kamenný
přístřešky	přístřešek	k1gInPc1	přístřešek
místních	místní	k2eAgMnPc2d1	místní
pastevců	pastevec	k1gMnPc2	pastevec
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
mitato	mitato	k6eAd1	mitato
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
několik	několik	k4yIc4	několik
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
turisticky	turisticky	k6eAd1	turisticky
nejoblíbenějším	oblíbený	k2eAgNnPc3d3	nejoblíbenější
místům	místo	k1gNnPc3	místo
patří	patřit	k5eAaImIp3nS	patřit
kaňon	kaňon	k1gInSc1	kaňon
Samaria	samarium	k1gNnSc2	samarium
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Samaria	samarium	k1gNnSc2	samarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
západní	západní	k2eAgFnPc1d1	západní
stěny	stěna	k1gFnPc1	stěna
tvoří	tvořit	k5eAaImIp3nP	tvořit
prakticky	prakticky	k6eAd1	prakticky
kolmé	kolmý	k2eAgFnPc1d1	kolmá
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgInPc4d1	vysoký
svahy	svah	k1gInPc4	svah
hřebene	hřeben	k1gInSc2	hřeben
s	s	k7c7	s
vrcholy	vrchol	k1gInPc7	vrchol
Volakias	Volakiasa	k1gFnPc2	Volakiasa
a	a	k8xC	a
Gigilos	Gigilosa	k1gFnPc2	Gigilosa
(	(	kIx(	(
<g/>
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
vrchol	vrchol	k1gInSc4	vrchol
vede	vést	k5eAaImIp3nS	vést
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
značená	značený	k2eAgFnSc1d1	značená
a	a	k8xC	a
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
s	s	k7c7	s
běžným	běžný	k2eAgInSc7d1	běžný
fyzickým	fyzický	k2eAgInSc7d1	fyzický
fondem	fond	k1gInSc7	fond
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
náročná	náročný	k2eAgFnSc1d1	náročná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
asfaltové	asfaltový	k2eAgFnPc1d1	asfaltová
silnice	silnice	k1gFnPc1	silnice
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
rokle	rokle	k1gFnSc2	rokle
Samária	Samárium	k1gNnSc2	Samárium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samaria	samarium	k1gNnSc2	samarium
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
soutěska	soutěska	k1gFnSc1	soutěska
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celé	celý	k2eAgNnSc1d1	celé
pohoří	pohoří	k1gNnSc1	pohoří
protíná	protínat	k5eAaImIp3nS	protínat
od	od	k7c2	od
západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
z	z	k7c2	z
planiny	planina	k1gFnSc2	planina
Omalos	Omalos	k1gInSc1	Omalos
<g/>
)	)	kIx)	)
na	na	k7c4	na
východ	východ	k1gInSc4	východ
(	(	kIx(	(
<g/>
na	na	k7c4	na
planinu	planina	k1gFnSc4	planina
Niátos	Niátosa	k1gFnPc2	Niátosa
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
níže	nízce	k6eAd2	nízce
položenou	položený	k2eAgFnSc4d1	položená
planinu	planina	k1gFnSc4	planina
Askifou	Askifá	k1gFnSc4	Askifá
<g/>
)	)	kIx)	)
značená	značený	k2eAgFnSc1d1	značená
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
černo-žluté	černo-žlutý	k2eAgFnSc2d1	černo-žlutá
tyče	tyč	k1gFnSc2	tyč
či	či	k8xC	či
kosodélníkové	kosodélníkový	k2eAgFnSc2d1	kosodélníkový
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
černo-žluté	černo-žlutý	k2eAgInPc4d1	černo-žlutý
pruhy	pruh	k1gInPc4	pruh
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
a	a	k8xC	a
kamenech	kámen	k1gInPc6	kámen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
jihoevropské	jihoevropský	k2eAgFnSc2d1	jihoevropská
trasy	trasa	k1gFnSc2	trasa
E	E	kA	E
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trasa	trasa	k1gFnSc1	trasa
prochází	procházet	k5eAaImIp3nS	procházet
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc7d1	celá
Krétou	Kréta	k1gFnSc7	Kréta
a	a	k8xC	a
u	u	k7c2	u
západního	západní	k2eAgInSc2d1	západní
okraje	okraj	k1gInSc2	okraj
Lefka	Lefka	k1gMnSc1	Lefka
Ori	Ori	k1gMnSc1	Ori
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
(	(	kIx(	(
<g/>
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
horská	horský	k2eAgFnSc1d1	horská
větev	větev	k1gFnSc1	větev
překoná	překonat	k5eAaPmIp3nS	překonat
celé	celý	k2eAgNnSc4d1	celé
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
horská	horský	k2eAgFnSc1d1	horská
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
již	již	k6eAd1	již
jen	jen	k9	jen
pro	pro	k7c4	pro
zdatné	zdatný	k2eAgMnPc4d1	zdatný
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
dobrým	dobrý	k2eAgInSc7d1	dobrý
orientačním	orientační	k2eAgInSc7d1	orientační
smyslem	smysl	k1gInSc7	smysl
a	a	k8xC	a
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
v	v	k7c6	v
náročném	náročný	k2eAgInSc6d1	náročný
kamenitém	kamenitý	k2eAgInSc6d1	kamenitý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
začátky	začátek	k1gInPc4	začátek
tras	trasa	k1gFnPc2	trasa
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
využít	využít	k5eAaPmF	využít
některé	některý	k3yIgFnPc4	některý
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
podhůří	podhůří	k1gNnSc6	podhůří
Levka	Levek	k1gMnSc2	Levek
Ori	Ori	k1gMnSc2	Ori
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
místními	místní	k2eAgInPc7d1	místní
autobusovými	autobusový	k2eAgInPc7d1	autobusový
spoji	spoj	k1gInPc7	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
výchozími	výchozí	k2eAgInPc7d1	výchozí
body	bod	k1gInPc7	bod
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
vesnice	vesnice	k1gFnSc1	vesnice
Omalos	Omalosa	k1gFnPc2	Omalosa
(	(	kIx(	(
<g/>
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
planině	planina	k1gFnSc6	planina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lakki	Lakk	k1gFnSc5	Lakk
<g/>
,	,	kIx,	,
Therisso	Therissa	k1gFnSc5	Therissa
<g/>
,	,	kIx,	,
Kámbi	Kámb	k1gMnPc7	Kámb
či	či	k8xC	či
Ammoudari	Ammoudar	k1gMnPc7	Ammoudar
na	na	k7c6	na
planině	planina	k1gFnSc6	planina
Askifou	Askifá	k1gFnSc4	Askifá
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
dostat	dostat	k5eAaPmF	dostat
samozřejmě	samozřejmě	k6eAd1	samozřejmě
od	od	k7c2	od
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
překonávat	překonávat	k5eAaImF	překonávat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
trasy	trasa	k1gFnPc4	trasa
do	do	k7c2	do
Lefka	Lefek	k1gInSc2	Lefek
Ori	Ori	k1gFnSc2	Ori
existují	existovat	k5eAaImIp3nP	existovat
3	[number]	k4	3
mapy	mapa	k1gFnPc1	mapa
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
ANAVASI	ANAVASI	kA	ANAVASI
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
které	který	k3yQgInPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
jak	jak	k8xS	jak
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
obchodech	obchod	k1gInPc6	obchod
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Samaria	samarium	k1gNnSc2	samarium
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
výskytem	výskyt	k1gInSc7	výskyt
endemického	endemický	k2eAgInSc2d1	endemický
druhu	druh	k1gInSc2	druh
kozy	koza	k1gFnSc2	koza
bezoárové	bezoárový	k2eAgFnSc2d1	bezoárová
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
Kri-Kri	Kri-Kr	k1gFnPc4	Kri-Kr
<g/>
)	)	kIx)	)
a	a	k8xC	a
zalesněným	zalesněný	k2eAgNnSc7d1	zalesněné
údolím	údolí	k1gNnSc7	údolí
s	s	k7c7	s
bohatými	bohatý	k2eAgInPc7d1	bohatý
porosty	porost	k1gInPc7	porost
cypřišů	cypřiš	k1gInPc2	cypřiš
<g/>
,	,	kIx,	,
oleandrů	oleandr	k1gInPc2	oleandr
a	a	k8xC	a
piniovými	piniový	k2eAgInPc7d1	piniový
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
kaňonu	kaňon	k1gInSc2	kaňon
teče	téct	k5eAaImIp3nS	téct
travertinový	travertinový	k2eAgInSc1d1	travertinový
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
po	po	k7c6	po
několika	několik	k4yIc6	několik
stovkách	stovka	k1gFnPc6	stovka
metrů	metr	k1gInPc2	metr
postupně	postupně	k6eAd1	postupně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
v	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
korytě	koryto	k1gNnSc6	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
tak	tak	k6eAd1	tak
do	do	k7c2	do
června	červen	k1gInSc2	červen
jím	jíst	k5eAaImIp1nS	jíst
voda	voda	k1gFnSc1	voda
teče	teč	k1gFnSc2	teč
až	až	k9	až
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Rokle	rokle	k1gFnSc1	rokle
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Libyjského	libyjský	k2eAgNnSc2d1	Libyjské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
také	také	k9	také
roste	růst	k5eAaImIp3nS	růst
značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
endemických	endemický	k2eAgInPc2d1	endemický
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
geologického	geologický	k2eAgInSc2d1	geologický
vývoje	vývoj	k1gInSc2	vývoj
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
a	a	k8xC	a
nejatraktivnějších	atraktivní	k2eAgInPc2d3	nejatraktivnější
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
pilát	pilát	k1gInSc1	pilát
Anchusa	Anchus	k1gMnSc2	Anchus
cespitosa	cespitosa	k1gFnSc1	cespitosa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
endemické	endemický	k2eAgInPc1d1	endemický
druhy	druh	k1gInPc1	druh
šafránů	šafrán	k1gInPc2	šafrán
či	či	k8xC	či
tulipánů	tulipán	k1gInPc2	tulipán
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
atraktivním	atraktivní	k2eAgInPc3d1	atraktivní
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lefka	Lefek	k1gInSc2	Lefek
Ori	Ori	k1gMnPc2	Ori
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Horolezecký	horolezecký	k2eAgInSc1d1	horolezecký
kluc	kluc	k1gInSc1	kluc
Chania	Chanium	k1gNnSc2	Chanium
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
