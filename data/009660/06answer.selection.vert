<s>
Pojištění	pojištěný	k1gMnPc1	pojištěný
proti	proti	k7c3	proti
úpadku	úpadek	k1gInSc3	úpadek
chrání	chránit	k5eAaImIp3nS	chránit
klienty	klient	k1gMnPc7	klient
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kancelář	kancelář	k1gFnSc1	kancelář
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
svého	svůj	k3xOyFgInSc2	svůj
úpadku	úpadek	k1gInSc2	úpadek
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
dostát	dostát	k5eAaPmF	dostát
svým	svůj	k3xOyFgInSc7	svůj
závazkům	závazek	k1gInPc3	závazek
plynoucím	plynoucí	k2eAgInSc6d1	plynoucí
z	z	k7c2	z
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
cestovní	cestovní	k2eAgFnSc2d1	cestovní
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
klientem	klient	k1gMnSc7	klient
<g/>
.	.	kIx.	.
</s>
