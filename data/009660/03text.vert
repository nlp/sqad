<p>
<s>
Pojištění	pojištěný	k1gMnPc1	pojištěný
proti	proti	k7c3	proti
úpadku	úpadek	k1gInSc3	úpadek
chrání	chránit	k5eAaImIp3nS	chránit
klienty	klient	k1gMnPc7	klient
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kancelář	kancelář	k1gFnSc1	kancelář
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
svého	svůj	k3xOyFgInSc2	svůj
úpadku	úpadek	k1gInSc2	úpadek
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
dostát	dostát	k5eAaPmF	dostát
svým	svůj	k3xOyFgInSc7	svůj
závazkům	závazek	k1gInPc3	závazek
plynoucím	plynoucí	k2eAgInSc6d1	plynoucí
z	z	k7c2	z
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
cestovní	cestovní	k2eAgFnSc2d1	cestovní
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
klientem	klient	k1gMnSc7	klient
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
cestovní	cestovní	k2eAgFnPc4d1	cestovní
kanceláře	kancelář	k1gFnPc4	kancelář
je	být	k5eAaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
doložení	doložení	k1gNnSc1	doložení
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
řízení	řízení	k1gNnSc2	řízení
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
příslušné	příslušný	k2eAgFnSc2d1	příslušná
koncese	koncese	k1gFnSc2	koncese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
postaráno	postarán	k2eAgNnSc1d1	postaráno
o	o	k7c4	o
klienty	klient	k1gMnPc4	klient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
zájezdu	zájezd	k1gInSc6	zájezd
a	a	k8xC	a
čerpají	čerpat	k5eAaImIp3nP	čerpat
služby	služba	k1gFnPc4	služba
cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zejména	zejména	k9	zejména
repatriaci	repatriace	k1gFnSc4	repatriace
těchto	tento	k3xDgMnPc2	tento
klientů	klient	k1gMnPc2	klient
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
odškodnění	odškodnění	k1gNnSc4	odškodnění
klientů	klient	k1gMnPc2	klient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
zálohu	záloha	k1gFnSc4	záloha
na	na	k7c4	na
zájezd	zájezd	k1gInSc4	zájezd
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc4	jeho
plnou	plný	k2eAgFnSc4d1	plná
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
neodcestovali	odcestovat	k5eNaPmAgMnP	odcestovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tito	tento	k3xDgMnPc1	tento
klienti	klient	k1gMnPc1	klient
uplatní	uplatnit	k5eAaPmIp3nP	uplatnit
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
u	u	k7c2	u
příslušné	příslušný	k2eAgFnSc2d1	příslušná
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výše	výše	k1gFnSc1	výše
odškodnění	odškodnění	k1gNnSc2	odškodnění
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
výši	výše	k1gFnSc6	výše
pojištění	pojištění	k1gNnSc2	pojištění
dané	daný	k2eAgFnSc2d1	daná
cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
pojišťovnou	pojišťovna	k1gFnSc7	pojišťovna
a	a	k8xC	a
cestovní	cestovní	k2eAgFnSc7d1	cestovní
kanceláří	kancelář	k1gFnSc7	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přihlášení	přihlášení	k1gNnSc4	přihlášení
nároků	nárok	k1gInPc2	nárok
mají	mít	k5eAaImIp3nP	mít
klienti	klient	k1gMnPc1	klient
lhůtu	lhůta	k1gFnSc4	lhůta
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
výplata	výplata	k1gFnSc1	výplata
náhrad	náhrada	k1gFnPc2	náhrada
následuje	následovat	k5eAaImIp3nS	následovat
proto	proto	k8xC	proto
až	až	k9	až
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
lhůtě	lhůta	k1gFnSc6	lhůta
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
většinou	většinou	k6eAd1	většinou
neobdrží	obdržet	k5eNaPmIp3nS	obdržet
zpět	zpět	k6eAd1	zpět
plnou	plný	k2eAgFnSc4d1	plná
cenu	cena	k1gFnSc4	cena
zájezdu	zájezd	k1gInSc2	zájezd
nebo	nebo	k8xC	nebo
části	část	k1gFnSc2	část
za	za	k7c4	za
nevyčerpané	vyčerpaný	k2eNgFnPc4d1	nevyčerpaná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
podpisem	podpis	k1gInSc7	podpis
cestovní	cestovní	k2eAgFnSc2d1	cestovní
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
klientem	klient	k1gMnSc7	klient
a	a	k8xC	a
cestovní	cestovní	k2eAgFnSc7d1	cestovní
kanceláří	kancelář	k1gFnSc7	kancelář
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
klient	klient	k1gMnSc1	klient
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
má	mít	k5eAaImIp3nS	mít
sjednáno	sjednán	k2eAgNnSc1d1	sjednáno
toto	tento	k3xDgNnSc4	tento
povinné	povinný	k2eAgNnSc4d1	povinné
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
od	od	k7c2	od
příslušné	příslušný	k2eAgFnSc2d1	příslušná
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
obdrží	obdržet	k5eAaPmIp3nS	obdržet
certifikát	certifikát	k1gInSc4	certifikát
o	o	k7c6	o
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
pojištění	pojištění	k1gNnSc6	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Příslušné	příslušný	k2eAgFnPc1d1	příslušná
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
seznamy	seznam	k1gInPc1	seznam
pojištěných	pojištěný	k2eAgFnPc2d1	pojištěná
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
se	se	k3xPyFc4	se
klient	klient	k1gMnSc1	klient
může	moct	k5eAaImIp3nS	moct
informovat	informovat	k5eAaBmF	informovat
u	u	k7c2	u
Asociace	asociace	k1gFnSc2	asociace
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
klient	klient	k1gMnSc1	klient
kupuje	kupovat	k5eAaImIp3nS	kupovat
zájezd	zájezd	k1gInSc4	zájezd
od	od	k7c2	od
cestovní	cestovní	k2eAgFnSc2d1	cestovní
agentury	agentura	k1gFnSc2	agentura
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zřetelně	zřetelně	k6eAd1	zřetelně
označeno	označit	k5eAaPmNgNnS	označit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
tento	tento	k3xDgInSc4	tento
zájezd	zájezd	k1gInSc4	zájezd
organizuje	organizovat	k5eAaBmIp3nS	organizovat
a	a	k8xC	a
klient	klient	k1gMnSc1	klient
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
se	se	k3xPyFc4	se
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
povinné	povinný	k2eAgNnSc4d1	povinné
pojištění	pojištění	k1gNnSc4	pojištění
proti	proti	k7c3	proti
úpadku	úpadek	k1gInSc3	úpadek
je	být	k5eAaImIp3nS	být
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
zájezd	zájezd	k1gInSc4	zájezd
</s>
</p>
<p>
<s>
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
</s>
</p>
<p>
<s>
cestovní	cestovní	k2eAgFnSc1d1	cestovní
agentura	agentura	k1gFnSc1	agentura
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Pojištěné	pojištěný	k2eAgFnPc1d1	pojištěná
cestovní	cestovní	k2eAgFnPc1d1	cestovní
kanceláře	kancelář	k1gFnPc1	kancelář
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
:	:	kIx,	:
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
159	[number]	k4	159
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
podmínkách	podmínka	k1gFnPc6	podmínka
podnikání	podnikání	k1gNnSc2	podnikání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
</s>
</p>
