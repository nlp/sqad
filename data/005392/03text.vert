<s>
Apollo	Apollo	k1gNnSc1	Apollo
17	[number]	k4	17
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
ještě	ještě	k9	ještě
zůstane	zůstat	k5eAaPmIp3nS	zůstat
posledním	poslední	k2eAgInSc7d1	poslední
letem	let	k1gInSc7	let
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
43	[number]	k4	43
<g/>
.	.	kIx.	.
lodí	loď	k1gFnPc2	loď
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
(	(	kIx(	(
<g/>
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
)	)	kIx)	)
z	z	k7c2	z
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
označen	označit	k5eAaPmNgMnS	označit
dle	dle	k7c2	dle
COSPAR	COSPAR	kA	COSPAR
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
96	[number]	k4	96
<g/>
A.	A.	kA	A.
Eugene	Eugen	k1gInSc5	Eugen
A.	A.	kA	A.
Cernan	Cernany	k1gInPc2	Cernany
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
letu	let	k1gInSc2	let
Ronald	Ronald	k1gMnSc1	Ronald
Evans	Evansa	k1gFnPc2	Evansa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Harrison	Harrison	k1gMnSc1	Harrison
Schmitt	Schmitt	k1gMnSc1	Schmitt
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
LM	LM	kA	LM
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc7d1	původní
profesí	profes	k1gFnSc7	profes
geolog	geolog	k1gMnSc1	geolog
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
včetně	včetně	k7c2	včetně
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Gene	gen	k1gInSc5	gen
Cernan	Cernan	k1gMnSc1	Cernan
<g/>
,	,	kIx,	,
Ron	Ron	k1gMnSc1	Ron
Evans	Evans	k1gInSc1	Evans
a	a	k8xC	a
Joe	Joe	k1gMnPc1	Joe
Engle	Engle	k1gFnSc2	Engle
tvořili	tvořit	k5eAaImAgMnP	tvořit
záložní	záložní	k2eAgFnSc4d1	záložní
posádku	posádka	k1gFnSc4	posádka
letu	let	k1gInSc2	let
Apollo	Apollo	k1gNnSc1	Apollo
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Engle	Engle	k1gInSc1	Engle
létal	létat	k5eAaImAgInS	létat
před	před	k7c7	před
programem	program	k1gInSc7	program
Apollo	Apollo	k1gNnSc1	Apollo
v	v	k7c6	v
experimentálním	experimentální	k2eAgInSc6d1	experimentální
letounu	letoun	k1gInSc6	letoun
X-	X-	k1gFnPc2	X-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
nepřesáhl	přesáhnout	k5eNaPmAgMnS	přesáhnout
hranici	hranice	k1gFnSc4	hranice
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zvyklostí	zvyklost	k1gFnPc2	zvyklost
v	v	k7c6	v
programu	program	k1gInSc6	program
Apollo	Apollo	k1gMnSc1	Apollo
se	se	k3xPyFc4	se
záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
stala	stát	k5eAaPmAgFnS	stát
základní	základní	k2eAgFnSc1d1	základní
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
lety	let	k1gInPc4	let
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Harrison	Harrison	k1gMnSc1	Harrison
Schmitt	Schmitt	k1gMnSc1	Schmitt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
v	v	k7c6	v
záložní	záložní	k2eAgFnSc6d1	záložní
posádce	posádka	k1gFnSc6	posádka
letu	let	k1gInSc2	let
Apollo	Apollo	k1gNnSc1	Apollo
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
cyklu	cyklus	k1gInSc2	cyklus
měl	mít	k5eAaImAgInS	mít
tedy	tedy	k9	tedy
letět	letět	k5eAaImF	letět
při	při	k7c6	při
misi	mise	k1gFnSc6	mise
Apollo	Apollo	k1gNnSc1	Apollo
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
misí	mise	k1gFnPc2	mise
Apollo	Apollo	k1gNnSc1	Apollo
18	[number]	k4	18
-	-	kIx~	-
20	[number]	k4	20
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schmitt	Schmitt	k1gInSc1	Schmitt
nepoletí	letět	k5eNaImIp3nS	letět
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
vědci	vědec	k1gMnPc1	vědec
v	v	k7c6	v
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
lobbovali	lobbovat	k5eAaImAgMnP	lobbovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c4	na
Měsíční	měsíční	k2eAgInSc4d1	měsíční
povrch	povrch	k1gInSc4	povrch
podíval	podívat	k5eAaPmAgMnS	podívat
i	i	k9	i
profesní	profesní	k2eAgMnSc1d1	profesní
geolog	geolog	k1gMnSc1	geolog
<g/>
.	.	kIx.	.
</s>
<s>
Deke	Dekat	k5eAaPmIp3nS	Dekat
Slayton	Slayton	k1gInSc1	Slayton
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
o	o	k7c4	o
obsazení	obsazení	k1gNnSc4	obsazení
posádek	posádka	k1gFnPc2	posádka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
možnostmi	možnost	k1gFnPc7	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
se	se	k3xPyFc4	se
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
pouze	pouze	k6eAd1	pouze
Schmitt	Schmitt	k2eAgMnSc1d1	Schmitt
a	a	k8xC	a
poletí	letět	k5eAaImIp3nS	letět
záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
14	[number]	k4	14
(	(	kIx(	(
<g/>
Cernan	Cernany	k1gInPc2	Cernany
a	a	k8xC	a
Evans	Evansa	k1gFnPc2	Evansa
<g/>
)	)	kIx)	)
se	s	k7c7	s
Schmittem	Schmitt	k1gInSc7	Schmitt
namísto	namísto	k7c2	namísto
Engleho	Engle	k1gMnSc2	Engle
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
celá	celý	k2eAgFnSc1d1	celá
posádka	posádka	k1gFnSc1	posádka
a	a	k8xC	a
poletí	poletět	k5eAaPmIp3nS	poletět
záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
Apollo	Apollo	k1gNnSc1	Apollo
15	[number]	k4	15
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
Schmitt	Schmitt	k1gMnSc1	Schmitt
trénoval	trénovat	k5eAaImAgMnS	trénovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
letět	letět	k5eAaImF	letět
až	až	k6eAd1	až
jako	jako	k8xS	jako
Apollo	Apollo	k1gMnSc1	Apollo
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
první	první	k4xOgFnSc4	první
možnost	možnost	k1gFnSc4	možnost
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přeřazení	přeřazení	k1gNnSc3	přeřazení
Harrisona	Harrison	k1gMnSc2	Harrison
Schmitta	Schmitt	k1gMnSc2	Schmitt
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Josepha	Joseph	k1gMnSc2	Joseph
Engla	Engl	k1gMnSc2	Engl
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Scott	Scott	k1gMnSc1	Scott
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
Alfred	Alfred	k1gMnSc1	Alfred
Worden	Wordna	k1gFnPc2	Wordna
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
James	James	k1gMnSc1	James
Irwin	Irwin	k1gMnSc1	Irwin
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Young	Young	k1gMnSc1	Young
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
Stuart	Stuart	k1gInSc4	Stuart
Roosa	Roos	k1gMnSc4	Roos
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Charles	Charles	k1gMnSc1	Charles
Duke	Duk	k1gMnSc2	Duk
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Záložní	záložní	k2eAgFnSc7d1	záložní
posádkou	posádka	k1gFnSc7	posádka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základní	základní	k2eAgFnSc1d1	základní
posádka	posádka	k1gFnSc1	posádka
letu	let	k1gInSc2	let
Apollo	Apollo	k1gNnSc1	Apollo
15	[number]	k4	15
až	až	k8xS	až
když	když	k8xS	když
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Apollo	Apollo	k1gMnSc1	Apollo
17	[number]	k4	17
bude	být	k5eAaImBp3nS	být
poslední	poslední	k2eAgFnSc6d1	poslední
misí	mise	k1gFnSc7	mise
na	na	k7c4	na
Měsíční	měsíční	k2eAgInSc4d1	měsíční
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
skandálu	skandál	k1gInSc6	skandál
se	s	k7c7	s
známkami	známka	k1gFnPc7	známka
při	při	k7c6	při
letu	let	k1gInSc6	let
Apollo	Apollo	k1gNnSc1	Apollo
15	[number]	k4	15
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Scott	Scotta	k1gFnPc2	Scotta
<g/>
,	,	kIx,	,
Worden	Wordna	k1gFnPc2	Wordna
a	a	k8xC	a
Irwin	Irwina	k1gFnPc2	Irwina
měli	mít	k5eAaImAgMnP	mít
prodat	prodat	k5eAaPmF	prodat
dopisní	dopisní	k2eAgFnPc4d1	dopisní
obálky	obálka	k1gFnPc4	obálka
s	s	k7c7	s
upomínkou	upomínka	k1gFnSc7	upomínka
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgInP	být
výslovně	výslovně	k6eAd1	výslovně
schváleny	schválit	k5eAaPmNgInP	schválit
<g/>
,	,	kIx,	,
německému	německý	k2eAgMnSc3d1	německý
obchodníku	obchodník	k1gMnSc3	obchodník
s	s	k7c7	s
poštovními	poštovní	k2eAgFnPc7d1	poštovní
známkami	známka	k1gFnPc7	známka
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
svěřeneckého	svěřenecký	k2eAgInSc2d1	svěřenecký
fondu	fond	k1gInSc2	fond
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
posádky	posádka	k1gFnSc2	posádka
Apolla	Apollo	k1gNnSc2	Apollo
15	[number]	k4	15
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nahrazeni	nahradit	k5eAaPmNgMnP	nahradit
velitelem	velitel	k1gMnSc7	velitel
a	a	k8xC	a
pilotem	pilot	k1gMnSc7	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Apolla	Apollo	k1gNnSc2	Apollo
16	[number]	k4	16
Youngem	Young	k1gInSc7	Young
a	a	k8xC	a
Dukem	Duk	k1gInSc7	Duk
a	a	k8xC	a
Roosou	Roosa	k1gFnSc7	Roosa
z	z	k7c2	z
Apolla	Apollo	k1gNnSc2	Apollo
14	[number]	k4	14
jakožto	jakožto	k8xS	jakožto
pilotem	pilot	k1gMnSc7	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Apollo	Apollo	k1gNnSc1	Apollo
17	[number]	k4	17
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
z	z	k7c2	z
mysu	mys	k1gInSc2	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
America	Americus	k1gMnSc2	Americus
s	s	k7c7	s
Ronem	Ron	k1gMnSc7	Ron
Evansem	Evans	k1gMnSc7	Evans
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
obletech	oblet	k1gInPc6	oblet
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
75	[number]	k4	75
obletů	oblet	k1gInPc2	oblet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
Challenger	Challenger	k1gInSc1	Challenger
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
Eugene	Eugen	k1gInSc5	Eugen
A.	A.	kA	A.
Cernan	Cernan	k1gMnSc1	Cernan
a	a	k8xC	a
Harrison	Harrison	k1gMnSc1	Harrison
Schmitt	Schmitt	k1gMnSc1	Schmitt
přistál	přistát	k5eAaPmAgMnS	přistát
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Taurus-Littrow	Taurus-Littrow	k1gFnSc2	Taurus-Littrow
v	v	k7c6	v
měsíčním	měsíční	k2eAgNnSc6d1	měsíční
Mare	Mare	k1gNnSc6	Mare
Serenitatis	Serenitatis	k1gFnSc2	Serenitatis
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
asi	asi	k9	asi
350	[number]	k4	350
km	km	kA	km
daleko	daleko	k6eAd1	daleko
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
poslední	poslední	k2eAgInSc1d1	poslední
stupeň	stupeň	k1gInSc1	stupeň
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
strávila	strávit	k5eAaPmAgFnS	strávit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
celkem	celkem	k6eAd1	celkem
75	[number]	k4	75
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
3	[number]	k4	3
výstupy	výstup	k1gInPc1	výstup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
trvání	trvání	k1gNnSc6	trvání
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
4	[number]	k4	4
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
lunárního	lunární	k2eAgNnSc2d1	lunární
vozítka	vozítko	k1gNnSc2	vozítko
Lunar	Lunar	k1gMnSc1	Lunar
Rover	rover	k1gMnSc1	rover
celkem	celek	k1gInSc7	celek
33,8	[number]	k4	33,8
km	km	kA	km
údolí	údolí	k1gNnSc2	údolí
Taurus-Littrow	Taurus-Littrow	k1gFnPc1	Taurus-Littrow
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
vycházky	vycházka	k1gFnSc2	vycházka
nalezla	naleznout	k5eAaPmAgFnS	naleznout
posádka	posádka	k1gFnSc1	posádka
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
horninu	hornina	k1gFnSc4	hornina
s	s	k7c7	s
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
dopravila	dopravit	k5eAaPmAgFnS	dopravit
celkem	celkem	k6eAd1	celkem
110,5	[number]	k4	110,5
kg	kg	kA	kg
vzorků	vzorek	k1gInPc2	vzorek
měsíční	měsíční	k2eAgFnSc2d1	měsíční
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
pilot	pilot	k1gInSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Ronald	Ronald	k1gMnSc1	Ronald
Evans	Evansa	k1gFnPc2	Evansa
výstup	výstup	k1gInSc1	výstup
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
EVA	Eva	k1gFnSc1	Eva
<g/>
)	)	kIx)	)
do	do	k7c2	do
otevřeného	otevřený	k2eAgInSc2d1	otevřený
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
6	[number]	k4	6
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
vyjmul	vyjmout	k5eAaPmAgMnS	vyjmout
z	z	k7c2	z
kamery	kamera	k1gFnSc2	kamera
umístěné	umístěný	k2eAgFnSc2d1	umístěná
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
plášti	plášť	k1gInSc6	plášť
Apolla	Apollo	k1gNnSc2	Apollo
17	[number]	k4	17
tři	tři	k4xCgFnPc4	tři
kazety	kazeta	k1gFnPc4	kazeta
s	s	k7c7	s
naexponovaným	naexponovaný	k2eAgInSc7d1	naexponovaný
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Velitelský	velitelský	k2eAgInSc4d1	velitelský
modul	modul	k1gInSc4	modul
Apolla	Apollo	k1gNnSc2	Apollo
17	[number]	k4	17
přistál	přistát	k5eAaPmAgMnS	přistát
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
poblíž	poblíž	k7c2	poblíž
souostroví	souostroví	k1gNnSc2	souostroví
Samoa	Samoa	k1gFnSc1	Samoa
přibližně	přibližně	k6eAd1	přibližně
640	[number]	k4	640
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
bodu	bod	k1gInSc2	bod
přistání	přistání	k1gNnSc2	přistání
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
6,5	[number]	k4	6,5
km	km	kA	km
od	od	k7c2	od
záchranné	záchranný	k2eAgFnSc2d1	záchranná
lodi	loď	k1gFnSc2	loď
USS	USS	kA	USS
Ticonderoga	Ticonderog	k1gMnSc2	Ticonderog
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
Apolla	Apollo	k1gNnSc2	Apollo
17	[number]	k4	17
Eugene	Eugen	k1gInSc5	Eugen
A.	A.	kA	A.
Cernan	Cernan	k1gInSc1	Cernan
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
jako	jako	k8xS	jako
poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zanechal	zanechat	k5eAaPmAgMnS	zanechat
své	svůj	k3xOyFgInPc4	svůj
stopy	stop	k1gInPc4	stop
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svých	svůj	k3xOyFgFnPc2	svůj
procházek	procházka	k1gFnPc2	procházka
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
československou	československý	k2eAgFnSc4d1	Československá
vlajku	vlajka	k1gFnSc4	vlajka
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
svého	svůj	k3xOyFgInSc2	svůj
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
předkům	předek	k1gMnPc3	předek
<g/>
,	,	kIx,	,
pocházejícím	pocházející	k2eAgInSc7d1	pocházející
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
věnoval	věnovat	k5eAaPmAgInS	věnovat
Astronomickému	astronomický	k2eAgInSc3d1	astronomický
ústavu	ústav	k1gInSc2	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgNnSc6d1	Ondřejovo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
pořídila	pořídit	k5eAaPmAgFnS	pořídit
posádka	posádka	k1gFnSc1	posádka
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
45	[number]	k4	45
000	[number]	k4	000
km	km	kA	km
fotografii	fotografia	k1gFnSc4	fotografia
plně	plně	k6eAd1	plně
osvětlené	osvětlený	k2eAgFnSc2d1	osvětlená
polokoule	polokoule	k1gFnSc2	polokoule
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
Modrá	modrý	k2eAgFnSc1d1	modrá
skleněnka	skleněnka	k1gFnSc1	skleněnka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
takovou	takový	k3xDgFnSc4	takový
fotografii	fotografia	k1gFnSc4	fotografia
pořízenou	pořízený	k2eAgFnSc4d1	pořízená
během	během	k7c2	během
pilotovaných	pilotovaný	k2eAgInPc2d1	pilotovaný
letů	let	k1gInPc2	let
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
17	[number]	k4	17
jsou	být	k5eAaImIp3nP	být
jediní	jediný	k2eAgMnPc1d1	jediný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
takto	takto	k6eAd1	takto
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
Zemi	zem	k1gFnSc3	zem
spatřili	spatřit	k5eAaPmAgMnP	spatřit
<g/>
.	.	kIx.	.
</s>
