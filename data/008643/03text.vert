<p>
<s>
Brionne	Brionnout	k5eAaImIp3nS	Brionnout
je	on	k3xPp3gFnPc4	on
francouzská	francouzský	k2eAgFnSc1d1	francouzská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Eure	Eur	k1gFnSc2	Eur
v	v	k7c6	v
regionu	region	k1gInSc6	region
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
4	[number]	k4	4
297	[number]	k4	297
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
kantonu	kanton	k1gInSc2	kanton
Brionne	Brionn	k1gMnSc5	Brionn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Eure	Eur	k1gFnSc2	Eur
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
