<s>
Iľja	Iľja	k1gMnSc1	Iľja
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Frank	Frank	k1gMnSc1	Frank
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
М	М	k?	М
Ф	Ф	k?	Ф
<g/>
;	;	kIx,	;
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
sovětským	sovětský	k2eAgMnSc7d1	sovětský
fyzikem	fyzik	k1gMnSc7	fyzik
Igorem	Igor	k1gMnSc7	Igor
Jevgeněvičem	Jevgeněvič	k1gMnSc7	Jevgeněvič
Tammem	Tamm	k1gMnSc7	Tamm
na	na	k7c6	na
základě	základ	k1gInSc6	základ
klasické	klasický	k2eAgFnSc2d1	klasická
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
přesnou	přesný	k2eAgFnSc4d1	přesná
teorii	teorie	k1gFnSc4	teorie
vzniku	vznik	k1gInSc2	vznik
Čerenkovova	Čerenkovův	k2eAgNnSc2d1	Čerenkovovo
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Alexejevičem	Alexejevič	k1gMnSc7	Alexejevič
Čerenkovem	Čerenkov	k1gInSc7	Čerenkov
a	a	k8xC	a
Tammem	Tamm	k1gInSc7	Tamm
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
