<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
י	י	k?	י
jóchanán	jóchanán	k2eAgMnSc1d1	jóchanán
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Hospodin	Hospodin	k1gMnSc1	Hospodin
je	být	k5eAaImIp3nS	být
milostivý	milostivý	k2eAgMnSc1d1	milostivý
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
milostivý	milostivý	k2eAgInSc1d1	milostivý
dar	dar	k1gInSc1	dar
Hospodinův	Hospodinův	k2eAgInSc1d1	Hospodinův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
