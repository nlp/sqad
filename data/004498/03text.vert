<s>
Jana	Jana	k1gFnSc1	Jana
je	být	k5eAaImIp3nS	být
ženská	ženský	k2eAgFnSc1d1	ženská
forma	forma	k1gFnSc1	forma
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jména	jméno	k1gNnSc2	jméno
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
י	י	k?	י
jóchanán	jóchanán	k2eAgMnSc1d1	jóchanán
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Hospodin	Hospodin	k1gMnSc1	Hospodin
je	být	k5eAaImIp3nS	být
milostivý	milostivý	k2eAgMnSc1d1	milostivý
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
milostivý	milostivý	k2eAgInSc1d1	milostivý
dar	dar	k1gInSc1	dar
Hospodinův	Hospodinův	k2eAgInSc1d1	Hospodinův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
někdy	někdy	k6eAd1	někdy
samostatná	samostatný	k2eAgFnSc1d1	samostatná
forma	forma	k1gFnSc1	forma
Johana	Johana	k1gFnSc1	Johana
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
slavít	slavít	k5eAaPmF	slavít
buď	buď	k8xC	buď
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
na	na	k7c6	na
Janu	Jan	k1gMnSc6	Jan
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
nebo	nebo	k8xC	nebo
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
na	na	k7c4	na
Giannu	Gianen	k2eAgFnSc4d1	Gianna
Mollu	Molla	k1gFnSc4	Molla
či	či	k8xC	či
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
na	na	k7c4	na
Janu	Jana	k1gFnSc4	Jana
de	de	k?	de
Chantal	Chantal	k1gMnSc1	Chantal
<g/>
.	.	kIx.	.
</s>
<s>
Janička	Janička	k1gFnSc1	Janička
<g/>
,	,	kIx,	,
Janinka	Janinka	k1gFnSc1	Janinka
<g/>
,	,	kIx,	,
Janka	Janka	k1gFnSc1	Janka
<g/>
,	,	kIx,	,
Jáňa	Jáňa	k1gMnSc1	Jáňa
<g/>
,	,	kIx,	,
Joanka	Joanka	k1gMnSc1	Joanka
<g/>
,	,	kIx,	,
Johanka	Johanka	k1gFnSc1	Johanka
<g/>
,	,	kIx,	,
Jája	Jája	k1gFnSc1	Jája
<g/>
,	,	kIx,	,
Janča	Janča	k1gMnSc1	Janča
<g/>
,	,	kIx,	,
Janet	Janet	k1gMnSc1	Janet
<g/>
,	,	kIx,	,
Jenny	Jenn	k1gMnPc4	Jenn
<g/>
,	,	kIx,	,
Jennee	Jenne	k1gMnPc4	Jenne
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
Janika	Janika	k1gFnSc1	Janika
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
srovnání	srovnání	k1gNnSc2	srovnání
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c2	za
sledovaných	sledovaný	k2eAgInPc2d1	sledovaný
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-0,3	-0,3	k4	-0,3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Albánsky	albánsky	k6eAd1	albánsky
<g/>
:	:	kIx,	:
Joana	Joana	k1gFnSc1	Joana
<g/>
,	,	kIx,	,
Johana	Johana	k1gFnSc1	Johana
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
Joan	Joan	k1gMnSc1	Joan
<g/>
,	,	kIx,	,
Joann	Joann	k1gMnSc1	Joann
<g/>
,	,	kIx,	,
Joanne	Joann	k1gMnSc5	Joann
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
Johanne	Johann	k1gMnSc5	Johann
<g/>
,	,	kIx,	,
Johanna	Johann	k1gMnSc4	Johann
<g/>
,	,	kIx,	,
Joanna	Joann	k1gMnSc4	Joann
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnPc4	zdrobnělina
<g/>
:	:	kIx,	:
Janet	Janet	k1gMnSc1	Janet
<g/>
,	,	kIx,	,
Janes	Janes	k1gMnSc1	Janes
<g/>
,	,	kIx,	,
Janice	Janice	k1gFnSc1	Janice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ي	ي	k?	ي
<g/>
ّ	ّ	k?	ّ
<g/>
ا	ا	k?	ا
(	(	kIx(	(
<g/>
Yuwannā	Yuwannā	k1gMnSc1	Yuwannā
<g/>
)	)	kIx)	)
Bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
:	:	kIx,	:
Я	Я	k?	Я
(	(	kIx(	(
<g/>
Jana	Jan	k1gMnSc2	Jan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Я	Я	k?	Я
(	(	kIx(	(
<g/>
Janica	Janica	k1gMnSc1	Janica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
И	И	k?	И
(	(	kIx(	(
<g/>
Ivana	Ivan	k1gMnSc2	Ivan
<g/>
)	)	kIx)	)
Česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnPc1	zdrobnělina
<g/>
:	:	kIx,	:
Janka	Janka	k1gFnSc1	Janka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Janinka	Janinka	k1gFnSc1	Janinka
<g/>
,	,	kIx,	,
Janička	Janička	k1gFnSc1	Janička
<g/>
,	,	kIx,	,
Jani	Jani	k?	Jani
<g/>
,	,	kIx,	,
Janča	Janča	k1gMnSc1	Janča
<g/>
,	,	kIx,	,
Janina	Janin	k2eAgFnSc1d1	Janina
<g/>
,	,	kIx,	,
Jája	Jája	k1gFnSc1	Jája
<g/>
,	,	kIx,	,
Januška	Januška	k1gFnSc1	Januška
<g/>
)	)	kIx)	)
Čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
乔	乔	k?	乔
(	(	kIx(	(
<g/>
Qiáo	Qiáo	k1gMnSc1	Qiáo
wǎ	wǎ	k?	wǎ
nà	nà	k?	nà
<g/>
)	)	kIx)	)
Dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
Johanne	Johann	k1gInSc5	Johann
Hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
י	י	k?	י
(	(	kIx(	(
<g/>
Yoḥ	Yoḥ	k1gMnSc1	Yoḥ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
י	י	k?	י
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ָ	ָ	k?	ָ
<g/>
ד	ד	k?	ד
(	(	kIx(	(
<g/>
Yocheved	Yocheved	k1gMnSc1	Yocheved
<g/>
)	)	kIx)	)
Finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Johanna	Johanna	k1gFnSc1	Johanna
Francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Jeanne	Jeann	k1gMnSc5	Jeann
<g/>
,	,	kIx,	,
Yanna	Yanno	k1gNnPc4	Yanno
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnPc4	zdrobnělina
<g/>
:	:	kIx,	:
Jeannette	Jeannett	k1gMnSc5	Jeannett
<g/>
,	,	kIx,	,
Jeannine	Jeannin	k1gInSc5	Jeannin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jehanne	Jehann	k1gInSc5	Jehann
Irsky	Irsko	k1gNnPc7	Irsko
<g/>
:	:	kIx,	:
Siobhán	Siobhán	k2eAgInSc4d1	Siobhán
<g/>
,	,	kIx,	,
Sinéad	Sinéad	k1gInSc4	Sinéad
Italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Giovanna	Giovanna	k1gFnSc1	Giovanna
<g/>
,	,	kIx,	,
Gianna	Gianna	k1gFnSc1	Gianna
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ioana	Ioan	k1gMnSc4	Ioan
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc4	Ivan
<g/>
,	,	kIx,	,
Nina	Nina	k1gFnSc1	Nina
<g/>
,	,	kIx,	,
Zanna	Zanna	k1gFnSc1	Zanna
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnPc1	zdrobnělina
<g/>
:	:	kIx,	:
Giovannina	Giovannina	k1gFnSc1	Giovannina
<g/>
,	,	kIx,	,
Giannina	Giannina	k1gFnSc1	Giannina
<g/>
)	)	kIx)	)
Islandsky	islandsky	k6eAd1	islandsky
<g/>
:	:	kIx,	:
Jóhanna	Jóhanna	k1gFnSc1	Jóhanna
<g/>
,	,	kIx,	,
Jensína	Jensína	k1gFnSc1	Jensína
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnPc4	zdrobnělina
<g/>
:	:	kIx,	:
Jóna	Jón	k1gMnSc2	Jón
<g/>
,	,	kIx,	,
Hansína	Hansína	k1gFnSc1	Hansína
<g/>
)	)	kIx)	)
Katalánsky	katalánsky	k6eAd1	katalánsky
<g/>
:	:	kIx,	:
Joana	Joana	k1gFnSc1	Joana
Maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Janika	Janika	k1gFnSc1	Janika
<g />
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Johanna	Johanna	k1gFnSc1	Johanna
<g/>
,	,	kIx,	,
Joanna	Joanna	k1gFnSc1	Joanna
<g/>
,	,	kIx,	,
Johanne	Johann	k1gInSc5	Johann
Polsky	Polska	k1gFnSc2	Polska
<g/>
:	:	kIx,	:
Joanna	Joann	k1gInSc2	Joann
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnSc2	zdrobnělina
<g/>
:	:	kIx,	:
Asia	Asium	k1gNnSc2	Asium
<g/>
,	,	kIx,	,
Joaśka	Joaśko	k1gNnSc2	Joaśko
<g/>
,	,	kIx,	,
Aśka	Aśkum	k1gNnSc2	Aśkum
<g/>
,	,	kIx,	,
Asiunia	Asiunium	k1gNnSc2	Asiunium
<g/>
,	,	kIx,	,
Asieńka	Asieńko	k1gNnSc2	Asieńko
<g/>
)	)	kIx)	)
Portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
Joana	Joana	k1gFnSc1	Joana
Rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
:	:	kIx,	:
Ioana	Ioana	k1gFnSc1	Ioana
<g/>
,	,	kIx,	,
Oana	Oana	k1gFnSc1	Oana
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
:	:	kIx,	:
Я	Я	k?	Я
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Jana	Jana	k1gFnSc1	Jana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Я	Я	k?	Я
(	(	kIx(	(
<g/>
Janka	Janka	k1gFnSc1	Janka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
И	И	k?	И
(	(	kIx(	(
<g/>
Ivana	Ivan	k1gMnSc2	Ivan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
(	(	kIx(	(
<g/>
Žana	Žana	k1gMnSc1	Žana
<g/>
)	)	kIx)	)
Řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
Γ	Γ	k?	Γ
(	(	kIx(	(
<g/>
Yanna	Yanna	k1gFnSc1	Yanna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gianna	Gianna	k1gFnSc1	Gianna
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnPc1	zdrobnělina
<g/>
:	:	kIx,	:
Γ	Γ	k?	Γ
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Yannoula	Yannoula	k1gFnSc1	Yannoula
<g/>
))	))	k?	))
Skotsky	skotsky	k6eAd1	skotsky
<g/>
:	:	kIx,	:
Sinéad	Sinéad	k1gInSc1	Sinéad
Slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
Srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
Jovana	Jovana	k1gFnSc1	Jovana
Španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Juana	Juan	k1gMnSc4	Juan
(	(	kIx(	(
<g/>
zdrobněliny	zdrobnělina	k1gFnSc2	zdrobnělina
<g/>
:	:	kIx,	:
Juanita	Juanita	k1gFnSc1	Juanita
<g/>
)	)	kIx)	)
Švédsky	švédsky	k6eAd1	švédsky
<g/>
:	:	kIx,	:
Johanna	Johanna	k1gFnSc1	Johanna
Ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
І	І	k?	І
(	(	kIx(	(
<g/>
Ivanna	Ivann	k1gMnSc2	Ivann
<g/>
)	)	kIx)	)
Welšsky	Welšsky	k1gMnSc1	Welšsky
<g/>
:	:	kIx,	:
Siân	Siân	k1gMnSc1	Siân
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1412	[number]	k4	1412
<g/>
-	-	kIx~	-
<g/>
1431	[number]	k4	1431
<g/>
)	)	kIx)	)
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
de	de	k?	de
Chantal	Chantal	k1gMnSc1	Chantal
(	(	kIx(	(
<g/>
1572	[number]	k4	1572
<g/>
-	-	kIx~	-
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
sv.	sv.	kA	sv.
Gianna	Giann	k1gMnSc2	Giann
Beretta	Berett	k1gMnSc2	Berett
Molla	Moll	k1gMnSc2	Moll
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
viz	vidět	k5eAaImRp2nS	vidět
Johana	Johana	k1gFnSc1	Johana
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Jana	Jana	k1gFnSc1	Jana
I.	I.	kA	I.
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Šílená	šílený	k2eAgFnSc1d1	šílená
(	(	kIx(	(
<g/>
1479	[number]	k4	1479
<g/>
-	-	kIx~	-
<g/>
1555	[number]	k4	1555
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kastilská	kastilský	k2eAgFnSc1d1	Kastilská
královna	královna	k1gFnSc1	královna
Jana	Jana	k1gFnSc1	Jana
I.	I.	kA	I.
Navarrská	navarrský	k2eAgFnSc1d1	Navarrská
(	(	kIx(	(
<g/>
1272	[number]	k4	1272
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
francouzská	francouzský	k2eAgFnSc1d1	francouzská
a	a	k8xC	a
navarrská	navarrský	k2eAgFnSc1d1	Navarrská
Jana	Jana	k1gFnSc1	Jana
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Navarrská	navarrský	k2eAgFnSc1d1	Navarrská
(	(	kIx(	(
<g/>
1311	[number]	k4	1311
<g/>
–	–	k?	–
<g/>
1349	[number]	k4	1349
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
navarrská	navarrský	k2eAgFnSc1d1	Navarrská
Jana	Jana	k1gFnSc1	Jana
Andresíková	Andresíkový	k2eAgFnSc1d1	Andresíkový
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jane	Jan	k1gMnSc5	Jan
Austenová	Austenový	k2eAgFnSc1d1	Austenová
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Jane	Jan	k1gMnSc5	Jan
Birkin	Birkin	k1gInSc1	Birkin
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jana	Jana	k1gFnSc1	Jana
Bobošíková	Bobošíková	k1gFnSc1	Bobošíková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
Jana	Jana	k1gFnSc1	Jana
Boušková	Boušková	k1gFnSc1	Boušková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
harfenistka	harfenistka	k1gFnSc1	harfenistka
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
Brejchová	Brejchová	k1gFnSc1	Brejchová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Dítětová	Dítětová	k1gFnSc1	Dítětová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Janka	Janka	k1gFnSc1	Janka
Djagilevová	Djagilevová	k1gFnSc1	Djagilevová
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
skladatelka	skladatelka	k1gFnSc1	skladatelka
Jane	Jan	k1gMnSc5	Jan
Fondová	fondový	k2eAgFnSc1d1	Fondová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g />
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
Husáková	Husáková	k1gFnSc1	Husáková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
šansoniérka	šansoniérka	k1gFnSc1	šansoniérka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
(	(	kIx(	(
<g/>
Ta	ten	k3xDgFnSc1	ten
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
ohrady	ohrada	k1gFnSc2	ohrada
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
Janatová	Janatová	k1gFnSc1	Janatová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
dětská	dětský	k2eAgFnSc1d1	dětská
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
baletka	baletka	k1gFnSc1	baletka
Jana	Jana	k1gFnSc1	Jana
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jana	Jana	k1gFnSc1	Jana
D.	D.	kA	D.
Karlíčková	Karlíčková	k1gFnSc1	Karlíčková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Jana	Jana	k1gFnSc1	Jana
Kirschner	Kirschner	k1gInSc1	Kirschner
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Janica	Janic	k1gInSc2	Janic
<g />
.	.	kIx.	.
</s>
<s>
Kostelić	Kostelić	k?	Kostelić
<g/>
,	,	kIx,	,
srbochorvatská	srbochorvatský	k2eAgFnSc1d1	srbochorvatský
lyžařka	lyžařka	k1gFnSc1	lyžařka
Jana	Jana	k1gFnSc1	Jana
Koubková	Koubková	k1gFnSc1	Koubková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jana	Jana	k1gFnSc1	Jana
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jana	Jana	k1gFnSc1	Jana
Malknechtová	Malknechtová	k1gFnSc1	Malknechtová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jana	Jana	k1gFnSc1	Jana
Mařasová	Mařasová	k1gFnSc1	Mařasová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jan	k1gMnSc2	Jan
Petrů	Petrů	k1gMnSc2	Petrů
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
české	český	k2eAgFnPc1d1	Česká
zpěvačky	zpěvačka	k1gFnPc1	zpěvačka
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
(	(	kIx(	(
<g/>
známější	známý	k2eAgNnSc1d2	známější
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Petra	Petra	k1gFnSc1	Petra
Janů	Jan	k1gMnPc2	Jan
<g/>
)	)	kIx)	)
Jana	Jan	k1gMnSc2	Jan
<g />
.	.	kIx.	.
</s>
<s>
Preissová	Preissová	k1gFnSc1	Preissová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Rybářová	Rybářová	k1gFnSc1	Rybářová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Štefánková	Štefánková	k1gFnSc1	Štefánková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
Jana	Jana	k1gFnSc1	Jana
Štěpánková	Štěpánková	k1gFnSc1	Štěpánková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Šulcová	Šulcová	k1gFnSc1	Šulcová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Švandová	Švandová	k1gFnSc1	Švandová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Vébrová	Vébrová	k1gFnSc1	Vébrová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
písničkářka	písničkářka	k1gFnSc1	písničkářka
Jana	Jana	k1gFnSc1	Jana
Werichová	Werichová	k1gFnSc1	Werichová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
papežka	papežka	k1gFnSc1	papežka
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Jana	Jan	k1gMnSc2	Jan
Majerová	Majerová	k1gFnSc1	Majerová
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Bastardi	bastard	k1gMnPc1	bastard
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
ji	on	k3xPp3gFnSc4	on
Kristýna	Kristýna	k1gFnSc1	Kristýna
Leichtová	Leichtová	k1gFnSc1	Leichtová
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
Turková	Turková	k1gFnSc1	Turková
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
filmu	film	k1gInSc2	film
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
ji	on	k3xPp3gFnSc4	on
Libuše	Libuše	k1gFnSc1	Libuše
Šafránková	Šafránková	k1gFnSc1	Šafránková
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
kalendář	kalendář	k1gInSc1	kalendář
<g/>
:	:	kIx,	:
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Chúzova	Chúzův	k2eAgFnSc1d1	Chúzův
<g/>
,	,	kIx,	,
NZ	NZ	kA	NZ
<g/>
)	)	kIx)	)
Slovenský	slovenský	k2eAgInSc1d1	slovenský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Římskokatolický	římskokatolický	k2eAgInSc4d1	římskokatolický
kalendář	kalendář	k1gInSc4	kalendář
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Františka	František	k1gMnSc2	František
de	de	k?	de
Chantal	Chantal	k1gMnSc1	Chantal
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
–	–	k?	–
singl	singl	k1gInSc4	singl
kapely	kapela	k1gFnSc2	kapela
Killing	Killing	k1gInSc4	Killing
Joke	Jok	k1gFnSc2	Jok
</s>
