<s>
Původní	původní	k2eAgFnSc1d1
osmibarevná	osmibarevný	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
Gilbertem	Gilbert	k1gMnSc7
Bakerem	Baker	k1gMnSc7
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
LGBT	LGBT	kA
hnutí	hnutí	k1gNnSc2
</s>
<s>
Pro	pro	k7c4
hromadnou	hromadný	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
a	a	k8xC
prodej	prodej	k1gInSc4
své	svůj	k3xOyFgFnSc2
vlajky	vlajka	k1gFnSc2
se	se	k3xPyFc4
Gilbert	Gilbert	k1gMnSc1
Baker	Baker	k1gMnSc1
obrátil	obrátit	k5eAaPmAgMnS
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Francisca	k1gFnSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
Paramount	Paramounta	k1gFnPc2
Flag	flaga	k1gFnPc2
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>