<s desamb="1">
Protože	protože	k8xS
však	však	k9
růžovou	růžový	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
autor	autor	k1gMnSc1
na	na	k7c6
vlajce	vlajka	k1gFnSc6
použil	použít	k5eAaPmAgInS
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
možno	možno	k6eAd1
tehdy	tehdy	k6eAd1
průmyslově	průmyslově	k6eAd1
vyrobit	vyrobit	k5eAaPmF
<g/>
,	,	kIx,
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
vlajka	vlajka	k1gFnSc1
zredukována	zredukován	k2eAgFnSc1d1
na	na	k7c4
sedm	sedm	k4xCc4
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>