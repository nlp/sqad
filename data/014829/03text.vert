<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
je	být	k5eAaImIp3nS
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
vlajkovém	vlajkový	k2eAgInSc6d1
listu	list	k1gInSc6
jsou	být	k5eAaImIp3nP
použity	použít	k5eAaPmNgInP
barevné	barevný	k2eAgInPc1d1
pruhy	pruh	k1gInPc1
připomínající	připomínající	k2eAgFnSc4d1
duhu	duha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
barevná	barevný	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
má	mít	k5eAaImIp3nS
dlouhou	dlouhý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
a	a	k8xC
používala	používat	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
odlišných	odlišný	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
typů	typ	k1gInPc2
duhových	duhový	k2eAgFnPc2d1
vlajek	vlajka	k1gFnPc2
lišících	lišící	k2eAgFnPc2d1
se	se	k3xPyFc4
počtem	počet	k1gInSc7
pruhů	pruh	k1gInPc2
(	(	kIx(
<g/>
barev	barva	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
řazením	řazení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
a	a	k8xC
pacifismus	pacifismus	k1gInSc1
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
s	s	k7c7
italským	italský	k2eAgInSc7d1
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
PACE	Paka	k1gFnSc6
<g/>
“	“	k?
–	–	k?
mír	mír	k1gInSc1
</s>
<s>
Jednu	jeden	k4xCgFnSc4
z	z	k7c2
forem	forma	k1gFnPc2
duhové	duhový	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
mezinárodní	mezinárodní	k2eAgNnSc1d1
mírové	mírový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
Bandiera	Bandiera	k1gFnSc1
della	della	k6eAd1
Pace	Paka	k1gFnSc3
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
vlajka	vlajka	k1gFnSc1
míru	mír	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
stala	stát	k5eAaPmAgFnS
symbolem	symbol	k1gInSc7
italského	italský	k2eAgNnSc2d1
pacifistického	pacifistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
mírový	mírový	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
Aldo	Aldo	k1gMnSc1
Capitini	Capitin	k2eAgMnPc1d1
a	a	k8xC
poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
při	při	k7c6
pochodu	pochod	k1gInSc6
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byla	být	k5eAaImAgFnS
hojně	hojně	k6eAd1
použita	použít	k5eAaPmNgFnS
při	při	k7c6
protestu	protest	k1gInSc6
proti	proti	k7c3
válce	válka	k1gFnSc3
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
Itálii	Itálie	k1gFnSc6
vyvěšována	vyvěšován	k2eAgNnPc1d1
z	z	k7c2
oken	okno	k1gNnPc2
při	při	k7c6
akci	akce	k1gFnSc6
Pace	Paka	k1gFnSc6
da	da	k?
tutti	tutti	k2eAgMnPc1d1
i	i	k8xC
balconi	balcon	k1gMnPc1
(	(	kIx(
<g/>
Mír	mír	k1gInSc1
ze	z	k7c2
všech	všecek	k3xTgInPc2
balkonů	balkon	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
s	s	k7c7
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
PACE	Paka	k1gFnSc6
<g/>
“	“	k?
jako	jako	k8xC,k8xS
protiválečný	protiválečný	k2eAgInSc4d1
symbol	symbol	k1gInSc4
rozšířila	rozšířit	k5eAaPmAgFnS
také	také	k9
do	do	k7c2
dalších	další	k2eAgInPc2d1
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
vlajkou	vlajka	k1gFnSc7
užívanou	užívaný	k2eAgFnSc7d1
jak	jak	k8xS,k8xC
odpůrci	odpůrce	k1gMnPc7
války	válka	k1gFnSc2
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
odpůrci	odpůrce	k1gMnPc1
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zasedání	zasedání	k1gNnSc6
NATO	NATO	kA
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
dokonce	dokonce	k9
tato	tento	k3xDgFnSc1
duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
zakázána	zakázán	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
protestům	protest	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
mírových	mírový	k2eAgMnPc2d1
aktivistů	aktivista	k1gMnPc2
se	se	k3xPyFc4
od	od	k7c2
duhové	duhový	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
gay	gay	k1gMnSc1
a	a	k8xC
lesbického	lesbický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
liší	lišit	k5eAaImIp3nP
ve	v	k7c6
třech	tři	k4xCgInPc6
podstatných	podstatný	k2eAgInPc6d1
bodech	bod	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
Barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgFnP
v	v	k7c6
opačném	opačný	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
odstíny	odstín	k1gInPc1
modré	modrý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
nahoře	nahoře	k6eAd1
<g/>
,	,	kIx,
červené	červený	k2eAgFnPc1d1
dole	dole	k6eAd1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
pacifistů	pacifista	k1gMnPc2
má	mít	k5eAaImIp3nS
sedm	sedm	k4xCc1
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
tmavě	tmavě	k6eAd1
modrý	modrý	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
navíc	navíc	k6eAd1
i	i	k9
světle	světle	k6eAd1
modrý	modrý	k2eAgInSc1d1
pruh	pruh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řazení	řazení	k1gNnSc3
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
modrých	modrý	k2eAgInPc2d1
odstínů	odstín	k1gInPc2
bývají	bývat	k5eAaImIp3nP
odlišná	odlišný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nich	on	k3xPp3gNnPc6
následuje	následovat	k5eAaImIp3nS
fialový	fialový	k2eAgInSc1d1
pruh	pruh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
nahrazen	nahradit	k5eAaPmNgInS
třetím	třetí	k4xOgInSc7
odstínem	odstín	k1gInSc7
modré	modrý	k2eAgNnSc1d1
podle	podle	k7c2
toho	ten	k3xDgInSc2
<g/>
,	,	kIx,
jaké	jaký	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
barvy	barva	k1gFnSc2
zvolil	zvolit	k5eAaPmAgMnS
konkrétní	konkrétní	k2eAgMnPc4d1
výrobce	výrobce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
regulérní	regulérní	k2eAgNnSc4d1
řazení	řazení	k1gNnSc4
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
–	–	k?
tmavomodrá	tmavomodrý	k2eAgFnSc1d1
–	–	k?
fialová	fialový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
či	či	k8xC
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
vlajky	vlajka	k1gFnSc2
je	být	k5eAaImIp3nS
nápis	nápis	k1gInSc1
„	„	k?
<g/>
PACE	Paka	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
nahrazován	nahrazovat	k5eAaImNgInS
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
PEACE	PEACE	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
PAIX	PAIX	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
SHALOM	SHALOM	kA
<g/>
“	“	k?
případně	případně	k6eAd1
jiným	jiný	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
znamenající	znamenající	k2eAgInSc4d1
mír	mír	k1gInSc4
v	v	k7c6
dalších	další	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
LGBT	LGBT	kA
hnutí	hnutí	k1gNnSc2
</s>
<s>
Původní	původní	k2eAgFnSc1d1
osmibarevná	osmibarevný	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
Gilbertem	Gilbert	k1gMnSc7
Bakerem	Baker	k1gMnSc7
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
bez	bez	k7c2
růžového	růžový	k2eAgInSc2d1
pruhu	pruh	k1gInSc2
kvůli	kvůli	k7c3
nedostupnosti	nedostupnost	k1gFnSc3
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šestibarevná	šestibarevný	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
bez	bez	k7c2
tyrkysového	tyrkysový	k2eAgInSc2d1
pruhu	pruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indigový	indigový	k2eAgInSc1d1
pruh	pruh	k1gInSc1
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
na	na	k7c4
královskou	královský	k2eAgFnSc4d1
modř	modř	k1gFnSc4
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
rovněž	rovněž	k9
univerzálním	univerzální	k2eAgFnPc3d1
gay	gay	k1gMnSc1
a	a	k8xC
lesbickým	lesbický	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
a	a	k8xC
postupem	postup	k1gInSc7
doby	doba	k1gFnSc2
vytlačila	vytlačit	k5eAaPmAgFnS
z	z	k7c2
této	tento	k3xDgFnSc2
pozice	pozice	k1gFnSc2
jiné	jiný	k2eAgInPc4d1
symboly	symbol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmi	ten	k3xDgInPc7
byly	být	k5eAaImAgFnP
růžový	růžový	k2eAgInSc4d1
trojúhelník	trojúhelník	k1gInSc4
<g/>
,	,	kIx,
původně	původně	k6eAd1
určený	určený	k2eAgMnSc1d1
pro	pro	k7c4
označení	označení	k1gNnPc4
homosexuálů	homosexuál	k1gMnPc2
v	v	k7c6
době	doba	k1gFnSc6
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
stal	stát	k5eAaPmAgMnS
především	především	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
symbolem	symbol	k1gInSc7
emancipace	emancipace	k1gFnSc2
gayů	gay	k1gMnPc2
a	a	k8xC
leseb	lesba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
také	také	k9
symbol	symbol	k1gInSc1
lambdy	lambda	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
rozšířený	rozšířený	k2eAgMnSc1d1
spíše	spíše	k9
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
i	i	k8xC
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
úkor	úkor	k1gInSc4
těchto	tento	k3xDgNnPc2
znamení	znamení	k1gNnPc2
etablovala	etablovat	k5eAaBmAgFnS
jako	jako	k9
všeobecně	všeobecně	k6eAd1
známý	známý	k2eAgInSc1d1
a	a	k8xC
přijímaný	přijímaný	k2eAgInSc1d1
symbol	symbol	k1gInSc1
LGBT	LGBT	kA
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
pohřbu	pohřeb	k1gInSc6
herečky	herečka	k1gFnSc2
Judy	judo	k1gNnPc7
Garlandové	Garlandový	k2eAgMnPc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
u	u	k7c2
gayů	gay	k1gMnPc2
velmi	velmi	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
<g/>
,	,	kIx,
objevilo	objevit	k5eAaPmAgNnS
několik	několik	k4yIc1
duhových	duhový	k2eAgFnPc2d1
vlajek	vlajka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
narážka	narážka	k1gFnSc1
na	na	k7c4
její	její	k3xOp3gFnSc4
nejslavnější	slavný	k2eAgFnSc4d3
píseň	píseň	k1gFnSc4
Over	Overa	k1gFnPc2
the	the	k?
Rainbow	Rainbow	k1gFnPc2
z	z	k7c2
filmu	film	k1gInSc2
Čaroděj	čaroděj	k1gMnSc1
ze	z	k7c2
země	zem	k1gFnSc2
Oz	Oz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ovšem	ovšem	k9
sporné	sporný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zdali	zdali	k8xS
tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
měla	mít	k5eAaImAgFnS
vliv	vliv	k1gInSc4
na	na	k7c4
pozdější	pozdní	k2eAgNnSc4d2
přijetí	přijetí	k1gNnSc4
duhové	duhový	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
pro	pro	k7c4
gay	gay	k1gMnSc1
a	a	k8xC
lesbické	lesbický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Duhovou	duhový	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
určenou	určený	k2eAgFnSc4d1
pro	pro	k7c4
toto	tento	k3xDgNnSc4
hnutí	hnutí	k1gNnSc4
navrhl	navrhnout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
americký	americký	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
Gilbert	Gilbert	k1gMnSc1
Baker	Baker	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
symbolizovat	symbolizovat	k5eAaImF
gay	gay	k1gMnSc1
a	a	k8xC
lesbickou	lesbický	k2eAgFnSc4d1
hrdost	hrdost	k1gFnSc4
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
současně	současně	k6eAd1
rozmanitost	rozmanitost	k1gFnSc1
LGBT	LGBT	kA
komunit	komunita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původních	původní	k2eAgFnPc2d1
osm	osm	k4xCc1
barev	barva	k1gFnPc2
mělo	mít	k5eAaImAgNnS
vyjadřovat	vyjadřovat	k5eAaImF
tyto	tento	k3xDgFnPc4
vlastnosti	vlastnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
růžová	růžový	k2eAgFnSc1d1
–	–	k?
sexualita	sexualita	k1gFnSc1
</s>
<s>
červená	červená	k1gFnSc1
–	–	k?
život	život	k1gInSc4
</s>
<s>
oranžová	oranžový	k2eAgFnSc1d1
–	–	k?
zdraví	zdravit	k5eAaImIp3nS
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
–	–	k?
sluneční	sluneční	k2eAgInSc1d1
svit	svit	k1gInSc1
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
–	–	k?
příroda	příroda	k1gFnSc1
</s>
<s>
tyrkysová	tyrkysový	k2eAgFnSc1d1
–	–	k?
umění	umění	k1gNnSc2
</s>
<s>
indigo	indigo	k1gNnSc1
–	–	k?
harmonie	harmonie	k1gFnSc2
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
–	–	k?
duch	duch	k1gMnSc1
</s>
<s>
Pro	pro	k7c4
hromadnou	hromadný	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
a	a	k8xC
prodej	prodej	k1gInSc4
své	svůj	k3xOyFgFnSc2
vlajky	vlajka	k1gFnSc2
se	se	k3xPyFc4
Gilbert	Gilbert	k1gMnSc1
Baker	Baker	k1gMnSc1
obrátil	obrátit	k5eAaPmAgMnS
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Francisca	k1gFnSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
Paramount	Paramounta	k1gFnPc2
Flag	flaga	k1gFnPc2
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
však	však	k9
růžovou	růžový	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
autor	autor	k1gMnSc1
na	na	k7c6
vlajce	vlajka	k1gFnSc6
použil	použít	k5eAaPmAgInS
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
možno	možno	k6eAd1
tehdy	tehdy	k6eAd1
průmyslově	průmyslově	k6eAd1
vyrobit	vyrobit	k5eAaPmF
<g/>
,	,	kIx,
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
vlajka	vlajka	k1gFnSc1
zredukována	zredukován	k2eAgFnSc1d1
na	na	k7c4
sedm	sedm	k4xCc4
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1978	#num#	k4
byl	být	k5eAaImAgInS
zavražděn	zavraždit	k5eAaPmNgMnS
Harvey	Harve	k1gMnPc7
Milk	Milk	k1gInSc4
<g/>
,	,	kIx,
veřejně	veřejně	k6eAd1
činný	činný	k2eAgMnSc1d1
gay	gay	k1gMnSc1
a	a	k8xC
člen	člen	k1gMnSc1
městské	městský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
San	San	k1gMnSc1
Francisca	Francisca	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
první	první	k4xOgNnPc4
výročí	výročí	k1gNnPc2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgMnS
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
a	a	k8xC
jako	jako	k9
znamení	znamení	k1gNnSc2
solidarity	solidarita	k1gFnSc2
zorganizován	zorganizován	k2eAgInSc4d1
vzpomínkový	vzpomínkový	k2eAgInSc4d1
průvod	průvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizátoři	organizátor	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
listopadu	listopad	k1gInSc6
1979	#num#	k4
rozhodli	rozhodnout	k5eAaPmAgMnP
v	v	k7c6
průvodu	průvod	k1gInSc6
použít	použít	k5eAaPmF
Bakerovu	Bakerův	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
jako	jako	k8xC,k8xS
ústřední	ústřední	k2eAgInSc1d1
symbol	symbol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
barvy	barva	k1gFnPc1
rovnoměrně	rovnoměrně	k6eAd1
rozděleny	rozdělit	k5eAaPmNgFnP
podél	podél	k7c2
cesty	cesta	k1gFnSc2
průvodu	průvod	k1gInSc2
–	–	k?
tři	tři	k4xCgFnPc4
barvy	barva	k1gFnPc4
po	po	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
ulice	ulice	k1gFnSc2
–	–	k?
odstranil	odstranit	k5eAaPmAgMnS
organizační	organizační	k2eAgInSc4d1
výbor	výbor	k1gInSc4
z	z	k7c2
vlajky	vlajka	k1gFnSc2
další	další	k2eAgInSc4d1
pruh	pruh	k1gInSc4
(	(	kIx(
<g/>
tyrkysový	tyrkysový	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrat	návrat	k1gInSc1
k	k	k7c3
sedmi	sedm	k4xCc3
pruhům	pruh	k1gInPc3
již	již	k6eAd1
neproběhl	proběhnout	k5eNaPmAgInS
a	a	k8xC
na	na	k7c6
vlajce	vlajka	k1gFnSc6
zůstalo	zůstat	k5eAaPmAgNnS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
jen	jen	k9
šest	šest	k4xCc1
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Symbol	symbol	k1gInSc1
duhy	duha	k1gFnSc2
u	u	k7c2
ekologických	ekologický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
</s>
<s>
Loď	loď	k1gFnSc1
Arctic	Arctice	k1gFnPc2
Sunrise	Sunrise	k1gFnSc1
organizace	organizace	k1gFnSc1
Greenpeace	Greenpeace	k1gFnSc1
</s>
<s>
Také	také	k9
organizace	organizace	k1gFnSc1
Greenpeace	Greenpeace	k1gFnSc2
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
ochranou	ochrana	k1gFnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
používá	používat	k5eAaImIp3nS
symbol	symbol	k1gInSc1
duhy	duha	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
pětibarevného	pětibarevný	k2eAgInSc2d1
duhového	duhový	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
na	na	k7c4
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
<g/>
)	)	kIx)
bílém	bílý	k2eAgInSc6d1
podkladu	podklad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
její	její	k3xOp3gFnPc1
lodě	loď	k1gFnPc1
nesou	nést	k5eAaImIp3nP
tento	tento	k3xDgInSc4
symbol	symbol	k1gInSc4
<g/>
,	,	kIx,
loď	loď	k1gFnSc1
Rainbow	Rainbow	k1gMnSc1
Warrior	Warrior	k1gMnSc1
má	mít	k5eAaImIp3nS
duhu	duha	k1gFnSc4
i	i	k9
v	v	k7c6
názvu	název	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Naopak	naopak	k6eAd1
česká	český	k2eAgFnSc1d1
nevládní	vládní	k2eNgFnSc1d1
ekologická	ekologický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Hnutí	hnutí	k1gNnSc2
DUHA	duha	k1gFnSc1
od	od	k7c2
symbolu	symbol	k1gInSc2
duhy	duha	k1gFnSc2
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
logu	logo	k1gNnSc6
upustila	upustit	k5eAaPmAgFnS
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
používá	používat	k5eAaImIp3nS
zelený	zelený	k2eAgInSc1d1
kruh	kruh	k1gInSc1
po	po	k7c6
vzoru	vzor	k1gInSc6
světové	světový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
ekologických	ekologický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
Přátelé	přítel	k1gMnPc1
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
užití	užití	k1gNnSc1
duhové	duhový	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Německá	německý	k2eAgFnSc1d1
selská	selský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
již	již	k6eAd1
během	během	k7c2
Německé	německý	k2eAgFnSc2d1
selské	selský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1524	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gNnSc1
užití	užití	k1gNnSc1
je	být	k5eAaImIp3nS
spojováno	spojovat	k5eAaImNgNnS
s	s	k7c7
reformátorem	reformátor	k1gMnSc7
a	a	k8xC
vůdcem	vůdce	k1gMnSc7
sedláků	sedlák	k1gMnPc2
Thomasem	Thomas	k1gMnSc7
Müntzerem	Müntzer	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
proto	proto	k8xC
často	často	k6eAd1
zobrazován	zobrazován	k2eAgMnSc1d1
<g/>
,	,	kIx,
tak	tak	k9
jako	jako	k9
na	na	k7c6
pomníku	pomník	k1gInSc6
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
rodném	rodný	k2eAgInSc6d1
Stolbergu	Stolberg	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
duhovou	duhový	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
v	v	k7c6
ruce	ruka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vlajka	vlajka	k1gFnSc1
města	město	k1gNnSc2
Cuzco	Cuzco	k6eAd1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Mauricia	Mauricia	k1gFnSc1
</s>
<s>
Říše	říše	k1gFnSc1
Inků	Ink	k1gMnPc2
a	a	k8xC
město	město	k1gNnSc1
Cuzco	Cuzco	k6eAd1
</s>
<s>
Barvy	barva	k1gFnPc1
duhy	duha	k1gFnSc2
se	se	k3xPyFc4
vyskytovaly	vyskytovat	k5eAaImAgInP
rovněž	rovněž	k9
na	na	k7c6
vlajce	vlajka	k1gFnSc6
Incké	incký	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
duhovou	duhový	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
používá	používat	k5eAaImIp3nS
region	region	k1gInSc4
Inka	Inka	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Cuzco	Cuzco	k6eAd1
v	v	k7c6
Peru	Peru	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajka	vlajka	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
pruhů	pruh	k1gInPc2
(	(	kIx(
<g/>
barev	barva	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
počtu	počet	k1gInSc2
jako	jako	k8xS,k8xC
má	mít	k5eAaImIp3nS
vlajka	vlajka	k1gFnSc1
mírového	mírový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
řazení	řazení	k1gNnSc1
barev	barva	k1gFnPc2
je	být	k5eAaImIp3nS
odlišné	odlišný	k2eAgNnSc1d1
a	a	k8xC
podobá	podobat	k5eAaImIp3nS
se	se	k3xPyFc4
naopak	naopak	k6eAd1
vlajce	vlajka	k1gFnSc3
LGBT	LGBT	kA
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
ale	ale	k9
pouze	pouze	k6eAd1
šest	šest	k4xCc4
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vlajka	vlajka	k1gFnSc1
židovské	židovský	k2eAgFnSc2d1
autonomní	autonomní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Ruska	Rusko	k1gNnSc2
poblíž	poblíž	k7c2
čínských	čínský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
používá	používat	k5eAaImIp3nS
jako	jako	k9
vlajku	vlajka	k1gFnSc4
též	též	k9
upravenou	upravený	k2eAgFnSc4d1
formu	forma	k1gFnSc4
duhové	duhový	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
sedm	sedm	k4xCc4
úzkých	úzký	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
umístěných	umístěný	k2eAgInPc2d1
uprostřed	uprostřed	k7c2
vlajkového	vlajkový	k2eAgInSc2d1
listu	list	k1gInSc2
na	na	k7c6
bílém	bílý	k2eAgInSc6d1
podkladě	podklad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řazení	řazení	k1gNnSc1
barev	barva	k1gFnPc2
je	být	k5eAaImIp3nS
obdobné	obdobný	k2eAgFnSc3d1
jako	jako	k8xC,k8xS
u	u	k7c2
vlajky	vlajka	k1gFnSc2
LGBT	LGBT	kA
hnutí	hnutí	k1gNnPc4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlajka	vlajka	k1gFnSc1
svazu	svaz	k1gInSc2
ICA	ICA	kA
z	z	k7c2
roku	rok	k1gInSc2
1925	#num#	k4
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
hospodářských	hospodářský	k2eAgNnPc2d1
družstev	družstvo	k1gNnPc2
</s>
<s>
Totožný	totožný	k2eAgInSc1d1
počet	počet	k1gInSc1
i	i	k8xC
řazení	řazení	k1gNnSc1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
přes	přes	k7c4
celý	celý	k2eAgInSc4d1
list	list	k1gInSc4
vlajky	vlajka	k1gFnSc2
si	se	k3xPyFc3
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
vlajku	vlajka	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
zvolila	zvolit	k5eAaPmAgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
International	International	k1gFnSc2
Co-operative	Co-operativ	k1gInSc5
Alliance	Allianec	k1gMnSc4
(	(	kIx(
<g/>
ICA	ICA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
sdružuje	sdružovat	k5eAaImIp3nS
hospodářská	hospodářský	k2eAgNnPc4d1
družstva	družstvo	k1gNnPc4
v	v	k7c6
mnoha	mnoho	k4c6
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaz	svaz	k1gInSc4
si	se	k3xPyFc3
vybral	vybrat	k5eAaPmAgMnS
a	a	k8xC
používá	používat	k5eAaImIp3nS
duhu	duha	k1gFnSc4
jako	jako	k8xC,k8xS
symbol	symbol	k1gInSc4
spolupráce	spolupráce	k1gFnSc2
a	a	k8xC
jednoty	jednota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	se	k3xPyFc4
ICA	ICA	kA
rozhodla	rozhodnout	k5eAaPmAgFnS
změnit	změnit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
vlajku	vlajka	k1gFnSc4
pro	pro	k7c4
snadnou	snadný	k2eAgFnSc4d1
záměnu	záměna	k1gFnSc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
duhovými	duhový	k2eAgFnPc7d1
vlajkami	vlajka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
používá	používat	k5eAaImIp3nS
na	na	k7c6
bílé	bílý	k2eAgFnSc6d1
vlajce	vlajka	k1gFnSc6
stylizovanou	stylizovaný	k2eAgFnSc4d1
holubici	holubice	k1gFnSc4
míru	mír	k1gInSc2
v	v	k7c6
barvě	barva	k1gFnSc6
duhy	duha	k1gFnSc2
doplněnou	doplněný	k2eAgFnSc7d1
zkratkou	zkratka	k1gFnSc7
svého	svůj	k3xOyFgInSc2
názvu	název	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Regenbogenfahne	Regenbogenfahne	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Rainbow	Rainbow	k1gFnSc6
flag	flag	k1gInSc1
(	(	kIx(
<g/>
LGBT	LGBT	kA
movement	movement	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Článek	článek	k1gInSc1
deníku	deník	k1gInSc2
Neue	Neu	k1gFnSc2
Zürcher	Zürchra	k1gFnPc2
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
↑	↑	k?
Některé	některý	k3yIgNnSc1
gay	gay	k1gMnSc1
a	a	k8xC
lesbické	lesbický	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
použily	použít	k5eAaPmAgFnP
slovo	slovo	k1gNnSc4
Lambda	lambda	k1gNnSc2
do	do	k7c2
svého	svůj	k3xOyFgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
Jihočeská	jihočeský	k2eAgFnSc1d1
lambda	lambda	k1gNnSc7
nebo	nebo	k8xC
M	M	kA
Klub	klub	k1gInSc1
Lambda	lambda	k1gNnSc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
ICA	ICA	kA
Archivováno	archivován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bisexuální	bisexuální	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
hrdosti	hrdost	k1gFnSc2
</s>
<s>
Intersexuální	intersexuální	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
hrdosti	hrdost	k1gFnSc2
</s>
<s>
Pansexuální	Pansexuální	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
hrdosti	hrdost	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Ukázky	ukázka	k1gFnPc4
užití	užití	k1gNnSc2
pacifistické	pacifistický	k2eAgFnPc4d1
duhové	duhový	k2eAgFnPc4d1
vlajky	vlajka	k1gFnPc4
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sexualita	sexualita	k1gFnSc1
</s>
