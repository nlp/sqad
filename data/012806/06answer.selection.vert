<s>
Za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
zakladatele	zakladatel	k1gMnSc4	zakladatel
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
přednesl	přednést	k5eAaPmAgMnS	přednést
světu	svět	k1gInSc3	svět
obecně	obecně	k6eAd1	obecně
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
verzi	verze	k1gFnSc4	verze
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
