<p>
<s>
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
biologickou	biologický	k2eAgFnSc7d1	biologická
evolucí	evoluce	k1gFnSc7	evoluce
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
zakladatele	zakladatel	k1gMnSc4	zakladatel
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
přednesl	přednést	k5eAaPmAgMnS	přednést
světu	svět	k1gInSc3	svět
obecně	obecně	k6eAd1	obecně
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
verzi	verze	k1gFnSc4	verze
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
obor	obor	k1gInSc4	obor
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
evoluční	evoluční	k2eAgMnPc1d1	evoluční
biologové	biolog	k1gMnPc1	biolog
či	či	k8xC	či
evolucionisté	evolucionista	k1gMnPc1	evolucionista
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
příznivců	příznivec	k1gMnPc2	příznivec
evolucionismu	evolucionismus	k1gInSc2	evolucionismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
je	být	k5eAaImIp3nS	být
interdisciplinární	interdisciplinární	k2eAgInSc4d1	interdisciplinární
obor	obor	k1gInSc4	obor
zasahující	zasahující	k2eAgInSc4d1	zasahující
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
terénních	terénní	k2eAgMnPc2d1	terénní
i	i	k8xC	i
laboratorních	laboratorní	k2eAgMnPc2d1	laboratorní
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
tak	tak	k6eAd1	tak
využívá	využívat	k5eAaImIp3nS	využívat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
studují	studovat	k5eAaImIp3nP	studovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
organizmy	organizmus	k1gInPc1	organizmus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mammalogie	mammalogie	k1gFnSc2	mammalogie
<g/>
,	,	kIx,	,
ornitologie	ornitologie	k1gFnSc2	ornitologie
nebo	nebo	k8xC	nebo
herpetologie	herpetologie	k1gFnSc2	herpetologie
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
speciální	speciální	k2eAgInPc4d1	speciální
případy	případ	k1gInPc4	případ
pro	pro	k7c4	pro
hledání	hledání	k1gNnSc4	hledání
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c4	na
obecnější	obecní	k2eAgFnPc4d2	obecní
otázky	otázka	k1gFnPc4	otázka
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
využívá	využívat	k5eAaPmIp3nS	využívat
evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
poznatků	poznatek	k1gInPc2	poznatek
paleontologie	paleontologie	k1gFnSc2	paleontologie
a	a	k8xC	a
geologie	geologie	k1gFnSc2	geologie
o	o	k7c6	o
fosiliích	fosilie	k1gFnPc6	fosilie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
hledat	hledat	k5eAaImF	hledat
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
po	po	k7c6	po
tempu	tempo	k1gNnSc6	tempo
a	a	k8xC	a
způsobu	způsob	k1gInSc6	způsob
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
i	i	k9	i
poznatků	poznatek	k1gInPc2	poznatek
teoretických	teoretický	k2eAgInPc2d1	teoretický
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
populační	populační	k2eAgFnSc1d1	populační
genetika	genetika	k1gFnSc1	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
teorie	teorie	k1gFnPc1	teorie
lze	lze	k6eAd1	lze
experimentálně	experimentálně	k6eAd1	experimentálně
ověřovat	ověřovat	k5eAaImF	ověřovat
např.	např.	kA	např.
na	na	k7c6	na
modelu	model	k1gInSc6	model
Drosophila	Drosophil	k1gMnSc2	Drosophil
-	-	kIx~	-
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
realizována	realizován	k2eAgFnSc1d1	realizována
i	i	k8xC	i
experimentální	experimentální	k2eAgFnSc1d1	experimentální
evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obor	obor	k1gInSc1	obor
zájmu	zájem	k1gInSc2	zájem
==	==	k?	==
</s>
</p>
<p>
<s>
Evoluční	evoluční	k2eAgMnPc1d1	evoluční
biologové	biolog	k1gMnPc1	biolog
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
především	především	k9	především
na	na	k7c4	na
mikroevolucionisty	mikroevolucionista	k1gMnPc4	mikroevolucionista
a	a	k8xC	a
makroevolucionisty	makroevolucionista	k1gMnPc4	makroevolucionista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
se	se	k3xPyFc4	se
ale	ale	k9	ale
nezabývá	zabývat	k5eNaImIp3nS	zabývat
jen	jen	k9	jen
samotným	samotný	k2eAgInSc7d1	samotný
vývojem	vývoj	k1gInSc7	vývoj
-	-	kIx~	-
tedy	tedy	k9	tedy
adaptací	adaptace	k1gFnSc7	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
témata	téma	k1gNnPc1	téma
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
netýkají	týkat	k5eNaImIp3nP	týkat
přímo	přímo	k6eAd1	přímo
metod	metoda	k1gFnPc2	metoda
přizpůsobování	přizpůsobování	k1gNnPc2	přizpůsobování
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
evoluce	evoluce	k1gFnSc1	evoluce
adaptace	adaptace	k1gFnSc2	adaptace
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
</s>
</p>
<p>
<s>
evoluce	evoluce	k1gFnSc1	evoluce
variací	variace	k1gFnPc2	variace
(	(	kIx(	(
<g/>
speciace	speciace	k1gFnSc1	speciace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
evoluce	evoluce	k1gFnSc1	evoluce
specializací	specializace	k1gFnPc2	specializace
(	(	kIx(	(
<g/>
mutace	mutace	k1gFnSc1	mutace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
evoluce	evoluce	k1gFnSc1	evoluce
komplexity	komplexita	k1gFnSc2	komplexita
(	(	kIx(	(
<g/>
třídění	třídění	k1gNnSc1	třídění
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
stability	stabilita	k1gFnSc2	stabilita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sociální	sociální	k2eAgFnSc1d1	sociální
evoluce	evoluce	k1gFnSc1	evoluce
</s>
</p>
<p>
<s>
genetika	genetika	k1gFnSc1	genetika
a	a	k8xC	a
dědičnost	dědičnost	k1gFnSc1	dědičnost
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
evoluční	evoluční	k2eAgMnSc1d1	evoluční
biologové	biolog	k1gMnPc1	biolog
==	==	k?	==
</s>
</p>
<p>
<s>
Othenio	Othenio	k1gMnSc1	Othenio
Abel	Abel	k1gMnSc1	Abel
</s>
</p>
<p>
<s>
Francisco	Francisco	k1gMnSc1	Francisco
J.	J.	kA	J.
Ayala	Ayala	k1gMnSc1	Ayala
</s>
</p>
<p>
<s>
Deborah	Deborah	k1gMnSc1	Deborah
Charlesworth	Charlesworth	k1gMnSc1	Charlesworth
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
F.	F.	kA	F.
Crow	Crow	k1gMnSc1	Crow
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
</s>
</p>
<p>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Darwin	Darwin	k1gMnSc1	Darwin
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Dennett	Dennett	k1gMnSc1	Dennett
</s>
</p>
<p>
<s>
Theodosius	Theodosius	k1gMnSc1	Theodosius
Dobzhansky	Dobzhansky	k1gMnSc1	Dobzhansky
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Theodor	Theodor	k1gMnSc1	Theodor
Doflein	Doflein	k1gMnSc1	Doflein
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Anton	Anton	k1gMnSc1	Anton
Dohrn	Dohrn	k1gMnSc1	Dohrn
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
W.	W.	kA	W.
Ewald	Ewald	k1gMnSc1	Ewald
</s>
</p>
<p>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Fisher	Fishra	k1gFnPc2	Fishra
</s>
</p>
<p>
<s>
Douglas	Douglas	k1gInSc1	Douglas
Futuyma	Futuymum	k1gNnSc2	Futuymum
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Gegenbaur	Gegenbaur	k1gMnSc1	Gegenbaur
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Jay	Jay	k1gMnSc1	Jay
Gould	Gould	k1gMnSc1	Gould
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
</s>
</p>
<p>
<s>
J.B.	J.B.	k?	J.B.
<g/>
S.	S.	kA	S.
Haldane	Haldan	k1gInSc5	Haldan
</s>
</p>
<p>
<s>
W.	W.	kA	W.
D.	D.	kA	D.
Hamilton	Hamilton	k1gInSc4	Hamilton
</s>
</p>
<p>
<s>
Willi	Will	k1gMnPc1	Will
Hennig	Henniga	k1gFnPc2	Henniga
</s>
</p>
<p>
<s>
Bert	Berta	k1gFnPc2	Berta
Hölldobler	Hölldobler	k1gMnSc1	Hölldobler
</s>
</p>
<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Jones	Jones	k1gMnSc1	Jones
</s>
</p>
<p>
<s>
Olivia	Olivia	k1gFnSc1	Olivia
Judson	Judsona	k1gFnPc2	Judsona
</s>
</p>
<p>
<s>
Motoo	Motoo	k6eAd1	Motoo
Kimura	Kimura	k1gFnSc1	Kimura
</s>
</p>
<p>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
Kutschera	Kutschero	k1gNnSc2	Kutschero
</s>
</p>
<p>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lamarck	Lamarck	k1gMnSc1	Lamarck
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Lewontin	Lewontin	k1gMnSc1	Lewontin
</s>
</p>
<p>
<s>
Lynn	Lynn	k1gInSc1	Lynn
Margulis	Margulis	k1gFnSc2	Margulis
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Louis	Louis	k1gMnSc1	Louis
Maupertuis	Maupertuis	k1gInSc1	Maupertuis
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Mayr	Mayr	k1gMnSc1	Mayr
</s>
</p>
<p>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Meisenheimer	Meisenheimer	k1gMnSc1	Meisenheimer
</s>
</p>
<p>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
Müller	Müller	k1gMnSc1	Müller
</s>
</p>
<p>
<s>
Tomoko	Tomoko	k6eAd1	Tomoko
Ohta	Oht	k2eAgFnSc1d1	Oht
</s>
</p>
<p>
<s>
Steven	Steven	k2eAgInSc1d1	Steven
Pinker	Pinker	k1gInSc1	Pinker
</s>
</p>
<p>
<s>
Bernhard	Bernhard	k1gMnSc1	Bernhard
Rensch	Rensch	k1gMnSc1	Rensch
</s>
</p>
<p>
<s>
Matt	Matt	k1gMnSc1	Matt
Ridley	Ridlea	k1gFnSc2	Ridlea
</s>
</p>
<p>
<s>
Beth	Beth	k1gInSc1	Beth
Shapiro	Shapiro	k1gNnSc1	Shapiro
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Smith	Smith	k1gMnSc1	Smith
</s>
</p>
<p>
<s>
Sewall	Sewall	k1gMnSc1	Sewall
Wright	Wright	k1gMnSc1	Wright
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gMnSc1	Russel
Wallace	Wallace	k1gFnSc1	Wallace
</s>
</p>
<p>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Weinberg	Weinberg	k1gMnSc1	Weinberg
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
Weismann	Weismann	k1gMnSc1	Weismann
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Wickler	Wickler	k1gMnSc1	Wickler
</s>
</p>
<p>
<s>
Allan	Allan	k1gMnSc1	Allan
Wilson	Wilson	k1gMnSc1	Wilson
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
Osborne	Osborn	k1gInSc5	Osborn
Wilson	Wilson	k1gMnSc1	Wilson
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Zimmermann	Zimmermann	k1gMnSc1	Zimmermann
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgFnSc3d1	Česká
osobnosti	osobnost	k1gFnSc3	osobnost
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
(	(	kIx(	(
<g/>
biolog	biolog	k1gMnSc1	biolog
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Frynta	Frynta	k1gMnSc1	Frynta
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Flegr	Flegr	k1gMnSc1	Flegr
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Rozcestník	rozcestník	k1gInSc1	rozcestník
Evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
na	na	k7c4	na
Scienceword	Scienceword	k1gInSc4	Scienceword
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
