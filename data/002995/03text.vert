<s>
Štír	štír	k1gMnSc1	štír
je	být	k5eAaImIp3nS	být
osmé	osmý	k4xOgNnSc1	osmý
astrologické	astrologický	k2eAgNnSc1d1	astrologické
znamení	znamení	k1gNnSc1	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
mající	mající	k2eAgInSc4d1	mající
původ	původ	k1gInSc4	původ
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Štíra	štír	k1gMnSc2	štír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Štír	štír	k1gMnSc1	štír
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
negativní	negativní	k2eAgNnSc4d1	negativní
(	(	kIx(	(
<g/>
introvertní	introvertní	k2eAgNnSc4d1	introvertní
<g/>
)	)	kIx)	)
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vodní	vodní	k2eAgNnSc4d1	vodní
znamení	znamení	k1gNnSc4	znamení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
pevných	pevný	k2eAgNnPc2d1	pevné
znamení	znamení	k1gNnPc2	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Štír	štír	k1gMnSc1	štír
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
ovládán	ovládat	k5eAaImNgInS	ovládat
planetou	planeta	k1gFnSc7	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
objevení	objevení	k1gNnSc1	objevení
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
vládce	vládce	k1gMnSc4	vládce
tohoto	tento	k3xDgNnSc2	tento
znamení	znamení	k1gNnSc2	znamení
považováno	považován	k2eAgNnSc1d1	považováno
ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Šipka	šipka	k1gFnSc1	šipka
na	na	k7c6	na
spodu	spod	k1gInSc6	spod
znaku	znak	k1gInSc2	znak
představuje	představovat	k5eAaImIp3nS	představovat
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
ocas	ocas	k1gInSc4	ocas
Štíra	štír	k1gMnSc2	štír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
konstelaci	konstelace	k1gFnSc6	konstelace
Štíra	štír	k1gMnSc2	štír
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sideralistické	sideralistický	k2eAgFnSc6d1	sideralistický
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Scorpio	Scorpio	k6eAd1	Scorpio
(	(	kIx(	(
<g/>
astrology	astrolog	k1gMnPc7	astrolog
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
