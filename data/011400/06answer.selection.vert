<s>
Specifické	specifický	k2eAgFnPc1d1	specifická
černé	černý	k2eAgFnPc1d1	černá
tečky	tečka	k1gFnPc1	tečka
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
Humboldtova	Humboldtův	k2eAgMnSc4d1	Humboldtův
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
magellanského	magellanský	k2eAgMnSc4d1	magellanský
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
v	v	k7c6	v
nepatrné	nepatrný	k2eAgFnSc6d1	nepatrná
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
