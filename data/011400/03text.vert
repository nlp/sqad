<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
humboldti	humboldť	k1gFnSc2	humboldť
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nelétavý	létavý	k2eNgMnSc1d1	nelétavý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tučňákovitých	tučňákovitý	k2eAgFnPc2d1	tučňákovitý
(	(	kIx(	(
<g/>
Spheniscidae	Spheniscidae	k1gFnPc2	Spheniscidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
přírodovědci	přírodovědec	k1gMnSc3	přírodovědec
Alexandru	Alexandr	k1gMnSc3	Alexandr
von	von	k1gInSc4	von
Humboldtovi	Humboldt	k1gMnSc3	Humboldt
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c6	na
pobřežích	pobřeží	k1gNnPc6	pobřeží
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
příbuzným	příbuzný	k1gMnSc7	příbuzný
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
<g/>
,	,	kIx,	,
tučňák	tučňák	k1gMnSc1	tučňák
magellanský	magellanský	k2eAgMnSc1d1	magellanský
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
tučňák	tučňák	k1gMnSc1	tučňák
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
váhy	váha	k1gFnPc1	váha
3,6	[number]	k4	3,6
<g/>
–	–	k?	–
<g/>
5,9	[number]	k4	5,9
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
kolem	kolem	k7c2	kolem
zobáku	zobák	k1gInSc2	zobák
má	mít	k5eAaImIp3nS	mít
holou	holý	k2eAgFnSc4d1	holá
<g/>
,	,	kIx,	,
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
žlázy	žláza	k1gFnPc4	žláza
obklopené	obklopený	k2eAgFnPc1d1	obklopená
cévami	céva	k1gFnPc7	céva
plnící	plnící	k2eAgFnSc4d1	plnící
termoregulační	termoregulační	k2eAgFnSc4d1	termoregulační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
tělní	tělní	k2eAgFnSc1d1	tělní
teplota	teplota	k1gFnSc1	teplota
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
žlázách	žláza	k1gFnPc6	žláza
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
ochlazeno	ochladit	k5eAaPmNgNnS	ochladit
okolním	okolní	k2eAgInSc7d1	okolní
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
probíhá	probíhat	k5eAaImIp3nS	probíhat
celoročně	celoročně	k6eAd1	celoročně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
mezi	mezi	k7c7	mezi
březnem	březen	k1gInSc7	březen
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
snáší	snášet	k5eAaImIp3nP	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc4	dva
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
35	[number]	k4	35
dnech	den	k1gInPc6	den
inkubace	inkubace	k1gFnSc2	inkubace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
potomstvo	potomstvo	k1gNnSc4	potomstvo
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
samice	samice	k1gFnSc1	samice
i	i	k8xC	i
samec	samec	k1gInSc1	samec
skoro	skoro	k6eAd1	skoro
rovným	rovný	k2eAgInSc7d1	rovný
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
asi	asi	k9	asi
100	[number]	k4	100
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
osamostatní	osamostatnit	k5eAaPmIp3nP	osamostatnit
a	a	k8xC	a
zamíří	zamířit	k5eAaPmIp3nP	zamířit
na	na	k7c4	na
otevřené	otevřený	k2eAgNnSc4d1	otevřené
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
asi	asi	k9	asi
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
procento	procento	k1gNnSc1	procento
z	z	k7c2	z
chovaných	chovaný	k2eAgInPc2d1	chovaný
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nS	množit
a	a	k8xC	a
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
si	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c4	na
evropské	evropský	k2eAgFnPc4d1	Evropská
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xS	jako
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
150	[number]	k4	150
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnSc6	jeho
populaci	populace	k1gFnSc6	populace
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
několik	několik	k4yIc1	několik
především	především	k6eAd1	především
lidských	lidský	k2eAgInPc2d1	lidský
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
podél	podél	k7c2	podél
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obývá	obývat	k5eAaImIp3nS	obývat
chladný	chladný	k2eAgInSc1d1	chladný
a	a	k8xC	a
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
bohatý	bohatý	k2eAgInSc4d1	bohatý
Humboldtův	Humboldtův	k2eAgInSc4d1	Humboldtův
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgFnSc4d3	nejsevernější
kolonii	kolonie	k1gFnSc4	kolonie
můžeme	moct	k5eAaImIp1nP	moct
nejspíš	nejspíš	k9	nejspíš
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
v	v	k7c6	v
mysu	mys	k1gInSc6	mys
Parinas	Parinasa	k1gFnPc2	Parinasa
<g/>
,	,	kIx,	,
na	na	k7c6	na
veřejné	veřejný	k2eAgFnSc6d1	veřejná
pláži	pláž	k1gFnSc6	pláž
Punta	punto	k1gNnSc2	punto
Balcones	Balconesa	k1gFnPc2	Balconesa
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
kolonii	kolonie	k1gFnSc4	kolonie
pak	pak	k6eAd1	pak
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c4	v
Punta	punto	k1gNnPc4	punto
San	San	k1gMnSc1	San
Juan	Juan	k1gMnSc1	Juan
(	(	kIx(	(
<g/>
Punta	punto	k1gNnPc1	punto
Arenas	Arenasa	k1gFnPc2	Arenasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
v	v	k7c4	v
Pachachamas	Pachachamas	k1gInSc4	Pachachamas
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
kolonie	kolonie	k1gFnSc1	kolonie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
např.	např.	kA	např.
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Puñ	Puñ	k1gFnSc2	Puñ
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
teplotách	teplota	k1gFnPc6	teplota
vzduchu	vzduch	k1gInSc2	vzduch
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
de	de	k?	de
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
a	a	k8xC	a
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
mívá	mívat	k5eAaImIp3nS	mívat
až	až	k9	až
28	[number]	k4	28
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
Kvůli	kvůli	k7c3	kvůli
úživným	úživný	k2eAgInPc3d1	úživný
oceánským	oceánský	k2eAgInPc3d1	oceánský
proudům	proud	k1gInPc3	proud
obvykle	obvykle	k6eAd1	obvykle
nemigruje	migrovat	k5eNaImIp3nS	migrovat
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
nejdále	daleko	k6eAd3	daleko
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
km	km	kA	km
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
i	i	k9	i
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Udeří	udeřit	k5eAaPmIp3nP	udeřit
<g/>
-li	i	k?	-li
klimatický	klimatický	k2eAgInSc1d1	klimatický
jev	jev	k1gInSc1	jev
ENSO	ENSO	kA	ENSO
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gFnSc2	Niñ
Southern	Southern	k1gMnSc1	Southern
Oscillation	Oscillation	k1gInSc1	Oscillation
<g/>
)	)	kIx)	)
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
každých	každý	k3xTgFnPc2	každý
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
ztrátu	ztráta	k1gFnSc4	ztráta
potravní	potravní	k2eAgFnSc2d1	potravní
hojnosti	hojnost	k1gFnSc2	hojnost
zapříčiněnou	zapříčiněný	k2eAgFnSc4d1	zapříčiněná
oteplováním	oteplování	k1gNnSc7	oteplování
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
stahuje	stahovat	k5eAaImIp3nS	stahovat
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
až	až	k9	až
900	[number]	k4	900
km	km	kA	km
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc4d1	velký
druhy	druh	k1gInPc4	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnSc2	výška
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
3,6	[number]	k4	3,6
<g/>
–	–	k?	–
<g/>
5,9	[number]	k4	5,9
kg	kg	kA	kg
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výkyvy	výkyv	k1gInPc1	výkyv
v	v	k7c6	v
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
a	a	k8xC	a
období	období	k1gNnSc6	období
pelichání	pelichání	k1gNnSc2	pelichání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
vypadá	vypadat	k5eAaImIp3nS	vypadat
velmi	velmi	k6eAd1	velmi
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
příbuzný	příbuzný	k1gMnSc1	příbuzný
tučňák	tučňák	k1gMnSc1	tučňák
magellanský	magellanský	k2eAgMnSc1d1	magellanský
<g/>
.	.	kIx.	.
</s>
<s>
Odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
však	však	k9	však
svým	svůj	k3xOyFgInSc7	svůj
jediným	jediný	k2eAgInSc7d1	jediný
černým	černý	k2eAgInSc7d1	černý
pruhem	pruh	k1gInSc7	pruh
vedoucím	vedoucí	k1gMnSc7	vedoucí
přes	přes	k7c4	přes
hruď	hruď	k1gFnSc4	hruď
(	(	kIx(	(
<g/>
tučňák	tučňák	k1gMnSc1	tučňák
magellanský	magellanský	k2eAgMnSc1d1	magellanský
má	mít	k5eAaImIp3nS	mít
pruhy	pruh	k1gInPc4	pruh
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgFnPc1d1	specifická
černé	černý	k2eAgFnPc1d1	černá
tečky	tečka	k1gFnPc1	tečka
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
Humboldtova	Humboldtův	k2eAgMnSc4d1	Humboldtův
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
tučňáka	tučňák	k1gMnSc2	tučňák
magellanského	magellanský	k2eAgMnSc4d1	magellanský
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
v	v	k7c6	v
nepatrné	nepatrný	k2eAgFnSc6d1	nepatrná
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Rozvrh	rozvrh	k1gInSc1	rozvrh
těchto	tento	k3xDgFnPc2	tento
nepravidelných	pravidelný	k2eNgFnPc2d1	nepravidelná
skvrn	skvrna	k1gFnPc2	skvrna
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
lidské	lidský	k2eAgInPc4d1	lidský
otisky	otisk	k1gInPc4	otisk
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
a	a	k8xC	a
neexistují	existovat	k5eNaImIp3nP	existovat
tedy	tedy	k9	tedy
dva	dva	k4xCgMnPc1	dva
naprosto	naprosto	k6eAd1	naprosto
stejní	stejný	k2eAgMnPc1d1	stejný
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
tučňáci	tučňák	k1gMnPc1	tučňák
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
rodu	rod	k1gInSc2	rod
má	mít	k5eAaImIp3nS	mít
nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
kolem	kolem	k7c2	kolem
zobáku	zobák	k1gInSc2	zobák
neopeřenou	opeřený	k2eNgFnSc4d1	neopeřená
kůži	kůže	k1gFnSc4	kůže
narůžověle	narůžověle	k6eAd1	narůžověle
až	až	k6eAd1	až
červené	červený	k2eAgFnPc4d1	červená
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
holá	holý	k2eAgFnSc1d1	holá
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
pokryta	pokryt	k2eAgFnSc1d1	pokryta
cévami	céva	k1gFnPc7	céva
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
přebytečné	přebytečný	k2eAgNnSc4d1	přebytečné
teplo	teplo	k1gNnSc4	teplo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přehřátí	přehřátí	k1gNnSc2	přehřátí
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
tučňáku	tučňák	k1gMnSc3	tučňák
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
36	[number]	k4	36
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Mláďata	mládě	k1gNnPc1	mládě
mají	mít	k5eAaImIp3nP	mít
první	první	k4xOgInPc4	první
týdny	týden	k1gInPc4	týden
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgNnSc4d1	šedé
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zbarvuje	zbarvovat	k5eAaImIp3nS	zbarvovat
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jasnější	jasný	k2eAgFnPc1d2	jasnější
skvrny	skvrna	k1gFnPc1	skvrna
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
asi	asi	k9	asi
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
kolem	kolem	k6eAd1	kolem
oči	oko	k1gNnPc4	oko
peří	peří	k1gNnSc2	peří
bělá	bělat	k5eAaImIp3nS	bělat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
šat	šat	k1gInSc1	šat
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
dospívání	dospívání	k1gNnSc6	dospívání
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
inkoustově	inkoustově	k6eAd1	inkoustově
černý	černý	k2eAgInSc1d1	černý
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
břichem	břicho	k1gNnSc7	břicho
a	a	k8xC	a
charakterními	charakterní	k2eAgFnPc7d1	charakterní
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
plně	plně	k6eAd1	plně
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
v	v	k7c6	v
porovnáním	porovnání	k1gNnSc7	porovnání
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
postrádají	postrádat	k5eAaImIp3nP	postrádat
především	především	k6eAd1	především
černý	černý	k2eAgInSc4d1	černý
pás	pás	k1gInSc4	pás
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
skýtajícími	skýtající	k2eAgFnPc7d1	skýtající
nástrahami	nástraha	k1gFnPc7	nástraha
a	a	k8xC	a
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dožije	dožít	k5eAaPmIp3nS	dožít
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
zralý	zralý	k2eAgInSc1d1	zralý
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
zpravidla	zpravidla	k6eAd1	zpravidla
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
==	==	k?	==
</s>
</p>
<p>
<s>
Studený	studený	k2eAgInSc1d1	studený
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
proud	proud	k1gInSc1	proud
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
tichomoří	tichomoří	k1gNnSc2	tichomoří
plno	plno	k6eAd1	plno
druhů	druh	k1gInPc2	druh
hejnových	hejnův	k2eAgFnPc2d1	hejnův
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
jimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
převážně	převážně	k6eAd1	převážně
živí	živit	k5eAaImIp3nP	živit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
ančovičky	ančovička	k1gFnPc4	ančovička
a	a	k8xC	a
sardinky	sardinka	k1gFnPc4	sardinka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
sardel	sardel	k1gFnSc4	sardel
peruánskou	peruánský	k2eAgFnSc4d1	peruánská
(	(	kIx(	(
<g/>
Engraulis	Engraulis	k1gFnSc4	Engraulis
ringens	ringensa	k1gFnPc2	ringensa
<g/>
)	)	kIx)	)
či	či	k8xC	či
sardinku	sardinka	k1gFnSc4	sardinka
tečkovanou	tečkovaný	k2eAgFnSc4d1	tečkovaná
(	(	kIx(	(
<g/>
Sardinops	Sardinopsa	k1gFnPc2	Sardinopsa
sagax	sagax	k1gInSc1	sagax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnou	možný	k2eAgFnSc7d1	možná
kořistí	kořist	k1gFnSc7	kořist
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
sépií	sépie	k1gFnPc2	sépie
(	(	kIx(	(
<g/>
Sepia	Sepia	k1gFnSc1	Sepia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikosti	velikost	k1gFnSc2	velikost
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
cm	cm	kA	cm
a	a	k8xC	a
pohlcena	pohlcen	k2eAgFnSc1d1	pohlcena
bývá	bývat	k5eAaImIp3nS	bývat
ihned	ihned	k6eAd1	ihned
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
zpravidla	zpravidla	k6eAd1	zpravidla
časně	časně	k6eAd1	časně
zrána	zrána	k6eAd1	zrána
a	a	k8xC	a
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
až	až	k9	až
do	do	k7c2	do
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
mláďatům	mládě	k1gNnPc3	mládě
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
se	se	k3xPyFc4	se
tak	tak	k9	tak
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
asi	asi	k9	asi
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
km	km	kA	km
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
tah	tah	k1gInSc4	tah
může	moct	k5eAaImIp3nS	moct
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
nalovit	nalovit	k5eAaBmF	nalovit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
gramů	gram	k1gInPc2	gram
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc4	podmínka
vyhovující	vyhovující	k2eAgFnPc4d1	vyhovující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
hloubkách	hloubka	k1gFnPc6	hloubka
nepřesahujících	přesahující	k2eNgInPc2d1	nepřesahující
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ve	v	k7c6	v
výjimečném	výjimečný	k2eAgInSc6d1	výjimečný
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Potápění	potápění	k1gNnSc1	potápění
trvá	trvat	k5eAaImIp3nS	trvat
nanejvýš	nanejvýš	k6eAd1	nanejvýš
minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
7,5	[number]	k4	7,5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
==	==	k?	==
</s>
</p>
<p>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
bývají	bývat	k5eAaImIp3nP	bývat
velkým	velký	k2eAgNnSc7d1	velké
lákadlem	lákadlo	k1gNnSc7	lákadlo
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
lišky	liška	k1gFnPc4	liška
<g/>
,	,	kIx,	,
zdivočelé	zdivočelý	k2eAgMnPc4d1	zdivočelý
psy	pes	k1gMnPc4	pes
a	a	k8xC	a
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
invazivní	invazivní	k2eAgInPc4d1	invazivní
druhy	druh	k1gInPc4	druh
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nemocné	nemocný	k2eAgMnPc4d1	nemocný
dospělé	dospělí	k1gMnPc4	dospělí
uloví	ulovit	k5eAaPmIp3nS	ulovit
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
představují	představovat	k5eAaImIp3nP	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
mořští	mořský	k2eAgMnPc1d1	mořský
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
jsou	být	k5eAaImIp3nP	být
Racci	racek	k1gMnPc1	racek
pruhoocasí	pruhoocasý	k2eAgMnPc1d1	pruhoocasý
(	(	kIx(	(
<g/>
Larus	Larus	k1gMnSc1	Larus
belcheri	belcher	k1gFnSc2	belcher
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Racci	racek	k1gMnPc1	racek
jižní	jižní	k2eAgMnPc1d1	jižní
(	(	kIx(	(
<g/>
Larus	Larus	k1gMnSc1	Larus
dominicanus	dominicanus	k1gMnSc1	dominicanus
<g/>
)	)	kIx)	)
či	či	k8xC	či
chaluhovití	chaluhovitý	k2eAgMnPc1d1	chaluhovitý
(	(	kIx(	(
<g/>
Stercorariidae	Stercorariidae	k1gNnSc7	Stercorariidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
predátoři	predátor	k1gMnPc1	predátor
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
především	především	k9	především
o	o	k7c4	o
vejce	vejce	k1gNnPc4	vejce
nebo	nebo	k8xC	nebo
menší	malý	k2eAgNnPc4d2	menší
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
ukryta	ukryt	k2eAgFnSc1d1	ukryta
v	v	k7c6	v
hnízdních	hnízdní	k2eAgFnPc6d1	hnízdní
jámách	jáma	k1gFnPc6	jáma
či	či	k8xC	či
puklinách	puklina	k1gFnPc6	puklina
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
krátká	krátký	k2eAgFnSc1d1	krátká
chvíle	chvíle	k1gFnSc1	chvíle
nepozornosti	nepozornost	k1gFnSc2	nepozornost
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
přiživí	přiživit	k5eAaPmIp3nP	přiživit
hadi	had	k1gMnPc1	had
(	(	kIx(	(
<g/>
Serpentes	Serpentes	k1gMnSc1	Serpentes
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
krysy	krysa	k1gFnSc2	krysa
(	(	kIx(	(
<g/>
Rodentia	Rodentia	k1gFnSc1	Rodentia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
napadají	napadat	k5eAaPmIp3nP	napadat
tuleni	tuleň	k1gMnPc1	tuleň
(	(	kIx(	(
<g/>
Phocidae	Phocidae	k1gInSc1	Phocidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žraloci	žralok	k1gMnPc1	žralok
(	(	kIx(	(
<g/>
Carcharhiniformes	Carcharhiniformes	k1gMnSc1	Carcharhiniformes
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lachtani	lachtan	k1gMnPc1	lachtan
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Otaria	Otarium	k1gNnSc2	Otarium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
celoročně	celoročně	k6eAd1	celoročně
nejčastěji	často	k6eAd3	často
mezi	mezi	k7c7	mezi
březnem	březen	k1gInSc7	březen
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
přívětivým	přívětivý	k2eAgFnPc3d1	přívětivá
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
ročnímu	roční	k2eAgInSc3d1	roční
cyklu	cyklus	k1gInSc3	cyklus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
trvajícího	trvající	k2eAgNnSc2d1	trvající
přepeřovacího	přepeřovací	k2eAgNnSc2d1	přepeřovací
období	období	k1gNnSc2	období
a	a	k8xC	a
následně	následně	k6eAd1	následně
dvou	dva	k4xCgNnPc2	dva
pětiměsíčních	pětiměsíční	k2eAgNnPc2d1	pětiměsíční
období	období	k1gNnPc2	období
chovných	chovný	k2eAgInPc2d1	chovný
cyklů	cyklus	k1gInPc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tučňáci	tučňák	k1gMnPc1	tučňák
viděni	vidět	k5eAaImNgMnP	vidět
kolem	kolem	k7c2	kolem
jejich	jejich	k3xOp3gNnPc2	jejich
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
tuto	tento	k3xDgFnSc4	tento
harmonii	harmonie	k1gFnSc4	harmonie
nenaruší	narušit	k5eNaPmIp3nS	narušit
ekologický	ekologický	k2eAgInSc1d1	ekologický
jev	jev	k1gInSc1	jev
ENSO	ENSO	kA	ENSO
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
Niňo	Niňo	k6eAd1	Niňo
Southern	Southern	k1gInSc1	Southern
Oscillation	Oscillation	k1gInSc1	Oscillation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
oblastí	oblast	k1gFnPc2	oblast
přinesl	přinést	k5eAaPmAgInS	přinést
moře	moře	k1gNnSc4	moře
chudé	chudý	k2eAgNnSc4d1	chudé
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
malé	malý	k2eAgFnSc2d1	malá
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
monogamní	monogamní	k2eAgMnPc1d1	monogamní
<g/>
,	,	kIx,	,
dospělí	dospělí	k1gMnPc1	dospělí
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
věrnost	věrnost	k1gFnSc4	věrnost
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
spolu	spolu	k6eAd1	spolu
většina	většina	k1gFnSc1	většina
dvojic	dvojice	k1gFnPc2	dvojice
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
nezahyne	zahynout	k5eNaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
si	se	k3xPyFc3	se
nachází	nacházet	k5eAaImIp3nS	nacházet
samice	samice	k1gFnSc2	samice
nového	nový	k2eAgMnSc4d1	nový
partnera	partner	k1gMnSc4	partner
častěji	často	k6eAd2	často
než	než	k8xS	než
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
volba	volba	k1gFnSc1	volba
partnera	partner	k1gMnSc2	partner
spočívá	spočívat	k5eAaImIp3nS	spočívat
z	z	k7c2	z
pravidla	pravidlo	k1gNnSc2	pravidlo
na	na	k7c4	na
samici	samice	k1gFnSc4	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
dny	den	k1gInPc4	den
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
===	===	k?	===
</s>
</p>
<p>
<s>
Rozmnožovací	rozmnožovací	k2eAgFnPc1d1	rozmnožovací
aktivity	aktivita	k1gFnPc1	aktivita
nastávají	nastávat	k5eAaImIp3nP	nastávat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
dubnem	duben	k1gInSc7	duben
a	a	k8xC	a
květnem	květen	k1gInSc7	květen
či	či	k8xC	či
zářím	září	k1gNnSc7	září
až	až	k8xS	až
říjnem	říjen	k1gInSc7	říjen
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
dostaví	dostavit	k5eAaPmIp3nP	dostavit
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
vybojuje	vybojovat	k5eAaPmIp3nS	vybojovat
nejvhodnější	vhodný	k2eAgNnSc4d3	nejvhodnější
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
vyhrabání	vyhrabání	k1gNnSc4	vyhrabání
nové	nový	k2eAgFnSc2d1	nová
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jen	jen	k9	jen
vyčistí	vyčistit	k5eAaPmIp3nS	vyčistit
starou	starý	k2eAgFnSc4d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
stín	stín	k1gInSc4	stín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
na	na	k7c4	na
noru	nora	k1gFnSc4	nora
doléhá	doléhat	k5eAaImIp3nS	doléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
nežádoucímu	žádoucí	k2eNgInSc3d1	nežádoucí
žáru	žár	k1gInSc3	žár
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nory	Nora	k1gFnSc2	Nora
vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
hluboko	hluboko	k6eAd1	hluboko
–	–	k?	–
průměrná	průměrný	k2eAgFnSc1d1	průměrná
počáteční	počáteční	k2eAgFnSc1d1	počáteční
nora	nora	k1gFnSc1	nora
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vstupní	vstupní	k2eAgInSc1d1	vstupní
otvor	otvor	k1gInSc1	otvor
široký	široký	k2eAgInSc1d1	široký
50	[number]	k4	50
cm	cm	kA	cm
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
asi	asi	k9	asi
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
chodba	chodba	k1gFnSc1	chodba
vysoká	vysoká	k1gFnSc1	vysoká
20	[number]	k4	20
cm	cm	kA	cm
končící	končící	k2eAgFnSc7d1	končící
hnízdící	hnízdící	k2eAgFnSc7d1	hnízdící
komůrkou	komůrka	k1gFnSc7	komůrka
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
45	[number]	k4	45
cm	cm	kA	cm
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
hluboko	hluboko	k6eAd1	hluboko
asi	asi	k9	asi
40	[number]	k4	40
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Postupným	postupný	k2eAgNnSc7d1	postupné
opravováním	opravování	k1gNnSc7	opravování
její	její	k3xOp3gFnSc2	její
rozměry	rozměra	k1gFnSc2	rozměra
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
je	on	k3xPp3gMnPc4	on
vystýlají	vystýlat	k5eAaImIp3nP	vystýlat
malými	malý	k2eAgFnPc7d1	malá
větvičkami	větvička	k1gFnPc7	větvička
<g/>
,	,	kIx,	,
klacíky	klacík	k1gInPc7	klacík
<g/>
,	,	kIx,	,
kamínky	kamínek	k1gInPc7	kamínek
<g/>
,	,	kIx,	,
šupinami	šupina	k1gFnPc7	šupina
z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
nebo	nebo	k8xC	nebo
i	i	k8xC	i
kostmi	kost	k1gFnPc7	kost
z	z	k7c2	z
uhynulých	uhynulý	k2eAgMnPc2d1	uhynulý
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prakticky	prakticky	k6eAd1	prakticky
veškerým	veškerý	k3xTgInSc7	veškerý
dostupným	dostupný	k2eAgInSc7d1	dostupný
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nehnízdí	hnízdit	k5eNaImIp3nS	hnízdit
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
je	on	k3xPp3gNnSc4	on
i	i	k9	i
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
děrách	děra	k1gFnPc6	děra
nebo	nebo	k8xC	nebo
puklinách	puklina	k1gFnPc6	puklina
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
balvany	balvan	k1gInPc4	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Využívat	využívat	k5eAaPmF	využívat
můžou	můžou	k?	můžou
i	i	k8xC	i
hnízd	hnízdo	k1gNnPc2	hnízdo
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Námluvy	námluva	k1gFnSc2	námluva
===	===	k?	===
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiní	jiný	k2eAgMnPc1d1	jiný
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
ozývá	ozývat	k5eAaImIp3nS	ozývat
se	se	k3xPyFc4	se
i	i	k9	i
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
hlubokými	hluboký	k2eAgInPc7d1	hluboký
hrdelními	hrdelní	k2eAgInPc7d1	hrdelní
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
sociální	sociální	k2eAgInSc4d1	sociální
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
uvědomovací	uvědomovací	k2eAgInSc1d1	uvědomovací
hlas	hlas	k1gInSc1	hlas
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
udržovaní	udržovaný	k2eAgMnPc1d1	udržovaný
kontaktu	kontakt	k1gInSc2	kontakt
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
ptáky	pták	k1gMnPc7	pták
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtější	typický	k2eAgFnSc7d3	nejtypičtější
fází	fáze	k1gFnSc7	fáze
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
troubení	troubení	k1gNnSc1	troubení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
samec	samec	k1gMnSc1	samec
před	před	k7c7	před
samicí	samice	k1gFnSc7	samice
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
nahoru	nahoru	k6eAd1	nahoru
nataženým	natažený	k2eAgInSc7d1	natažený
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
párů	pár	k1gInPc2	pár
troubí	troubit	k5eAaImIp3nS	troubit
samec	samec	k1gMnSc1	samec
i	i	k8xC	i
samice	samice	k1gFnSc1	samice
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úvodu	úvod	k1gInSc6	úvod
se	se	k3xPyFc4	se
partner	partner	k1gMnSc1	partner
projevuje	projevovat	k5eAaImIp3nS	projevovat
chvějivými	chvějivý	k2eAgInPc7d1	chvějivý
pohyby	pohyb	k1gInPc7	pohyb
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
zobákem	zobák	k1gInSc7	zobák
něžně	něžně	k6eAd1	něžně
probírá	probírat	k5eAaImIp3nS	probírat
peří	peří	k1gNnSc1	peří
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
své	svůj	k3xOyFgFnSc3	svůj
partnerce	partnerka	k1gFnSc3	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
přikrčí	přikrčit	k5eAaPmIp3nP	přikrčit
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
úplně	úplně	k6eAd1	úplně
lehne	lehnout	k5eAaPmIp3nS	lehnout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
samec	samec	k1gMnSc1	samec
mermomocí	mermomocí	k6eAd1	mermomocí
snaží	snažit	k5eAaImIp3nS	snažit
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
na	na	k7c4	na
její	její	k3xOp3gNnPc4	její
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
udržet	udržet	k5eAaPmF	udržet
rovnováhu	rovnováha	k1gFnSc4	rovnováha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
přitisknout	přitisknout	k5eAaPmF	přitisknout
svou	svůj	k3xOyFgFnSc4	svůj
kloaku	kloaka	k1gFnSc4	kloaka
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Páření	páření	k1gNnSc2	páření
===	===	k?	===
</s>
</p>
<p>
<s>
Poloha	poloha	k1gFnSc1	poloha
při	při	k7c6	při
kopulaci	kopulace	k1gFnSc6	kopulace
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
a	a	k8xC	a
samec	samec	k1gInSc1	samec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zmůže	zmoct	k5eAaPmIp3nS	zmoct
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
minuty	minuta	k1gFnSc2	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
probíhá	probíhat	k5eAaImIp3nS	probíhat
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
naskočí	naskočit	k5eAaPmIp3nS	naskočit
na	na	k7c4	na
samičí	samičí	k2eAgNnPc4d1	samičí
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
vyzdvihne	vyzdvihnout	k5eAaPmIp3nS	vyzdvihnout
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
třít	třít	k5eAaImF	třít
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
samec	samec	k1gMnSc1	samec
nemá	mít	k5eNaImIp3nS	mít
penis	penis	k1gInSc4	penis
<g/>
,	,	kIx,	,
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
kloaka	kloaka	k1gFnSc1	kloaka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
část	část	k1gFnSc1	část
konečníku	konečník	k1gInSc2	konečník
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
vyústěna	vyústěn	k2eAgFnSc1d1	vyústěna
trávicí	trávicí	k2eAgFnSc3d1	trávicí
<g/>
,	,	kIx,	,
vylučovací	vylučovací	k2eAgFnSc3d1	vylučovací
a	a	k8xC	a
právě	právě	k9	právě
rozmnožovací	rozmnožovací	k2eAgFnSc1d1	rozmnožovací
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
kloaku	kloaka	k1gFnSc4	kloaka
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
obě	dva	k4xCgFnPc4	dva
varlata	varle	k1gNnPc1	varle
umístěna	umístěn	k2eAgNnPc1d1	umístěno
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Otíráním	otírání	k1gNnSc7	otírání
o	o	k7c4	o
kloaku	kloaka	k1gFnSc4	kloaka
jeho	jeho	k3xOp3gFnSc2	jeho
družky	družka	k1gFnSc2	družka
předá	předat	k5eAaPmIp3nS	předat
své	svůj	k3xOyFgNnSc4	svůj
sperma	sperma	k1gNnSc4	sperma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
doputuje	doputovat	k5eAaPmIp3nS	doputovat
do	do	k7c2	do
skladovacích	skladovací	k2eAgInPc2d1	skladovací
kanálků	kanálek	k1gInPc2	kanálek
<g/>
.	.	kIx.	.
</s>
<s>
Spermie	spermie	k1gFnSc1	spermie
oplodní	oplodnit	k5eAaPmIp3nS	oplodnit
vajíčka	vajíčko	k1gNnPc4	vajíčko
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
následně	následně	k6eAd1	následně
v	v	k7c6	v
samičím	samičí	k2eAgInSc6d1	samičí
vejcovodu	vejcovod	k1gInSc6	vejcovod
ztvrdnou	ztvrdnout	k5eAaPmIp3nP	ztvrdnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Snesení	snesení	k1gNnSc2	snesení
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
chov	chov	k1gInSc4	chov
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
páření	páření	k1gNnSc6	páření
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc1	samice
jedno	jeden	k4xCgNnSc4	jeden
až	až	k8xS	až
výjimečně	výjimečně	k6eAd1	výjimečně
tři	tři	k4xCgNnPc1	tři
vejce	vejce	k1gNnPc1	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
přibližně	přibližně	k6eAd1	přibližně
72,8	[number]	k4	72,8
x	x	k?	x
55,8	[number]	k4	55,8
mm	mm	kA	mm
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
103	[number]	k4	103
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
mezi	mezi	k7c7	mezi
snesením	snesení	k1gNnSc7	snesení
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
dnům	den	k1gInPc3	den
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zkušenosti	zkušenost	k1gFnPc4	zkušenost
páru	pár	k1gInSc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
synchronizovaně	synchronizovaně	k6eAd1	synchronizovaně
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
vylíhlé	vylíhlý	k2eAgNnSc1d1	vylíhlé
mládě	mládě	k1gNnSc1	mládě
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
hustý	hustý	k2eAgInSc4d1	hustý
prachově	prachově	k6eAd1	prachově
šedý	šedý	k2eAgInSc4d1	šedý
šat	šat	k1gInSc4	šat
<g/>
,	,	kIx,	,
srostlá	srostlý	k2eAgNnPc1d1	srostlé
oční	oční	k2eAgNnPc1d1	oční
víčka	víčko	k1gNnPc1	víčko
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
kolem	kolem	k7c2	kolem
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
krmeno	krmen	k2eAgNnSc1d1	krmeno
natrávenou	natrávený	k2eAgFnSc7d1	natrávená
kašovitou	kašovitý	k2eAgFnSc7d1	kašovitá
stravou	strava	k1gFnSc7	strava
z	z	k7c2	z
volete	vole	k1gNnSc2	vole
(	(	kIx(	(
<g/>
jícnu	jícen	k1gInSc2	jícen
<g/>
)	)	kIx)	)
od	od	k7c2	od
obou	dva	k4xCgMnPc2	dva
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Vidět	vidět	k5eAaImF	vidět
začne	začít	k5eAaPmIp3nS	začít
druhý	druhý	k4xOgInSc4	druhý
týden	týden	k1gInSc4	týden
asi	asi	k9	asi
po	po	k7c6	po
10	[number]	k4	10
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
péči	péče	k1gFnSc4	péče
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
podělí	podělit	k5eAaPmIp3nP	podělit
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
hlídá	hlídat	k5eAaImIp3nS	hlídat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
sedí	sedit	k5eAaImIp3nS	sedit
samice	samice	k1gFnSc1	samice
a	a	k8xC	a
samec	samec	k1gMnSc1	samec
loví	lovit	k5eAaImIp3nS	lovit
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
si	se	k3xPyFc3	se
pár	pár	k1gInSc1	pár
úlohu	úloha	k1gFnSc4	úloha
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
i	i	k9	i
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
mláděte	mládě	k1gNnSc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
od	od	k7c2	od
přírody	příroda	k1gFnSc2	příroda
zdatnějšímu	zdatný	k2eAgInSc3d2	zdatnější
samci	samec	k1gInSc3	samec
vysedává	vysedávat	k5eAaImIp3nS	vysedávat
samička	samička	k1gFnSc1	samička
patrně	patrně	k6eAd1	patrně
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
a	a	k8xC	a
samec	samec	k1gMnSc1	samec
tedy	tedy	k9	tedy
častěji	často	k6eAd2	často
loví	lovit	k5eAaImIp3nP	lovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
větší	veliký	k2eAgMnPc1d2	veliký
a	a	k8xC	a
dokážou	dokázat	k5eAaPmIp3nP	dokázat
si	se	k3xPyFc3	se
regulovat	regulovat	k5eAaImF	regulovat
teplotu	teplota	k1gFnSc4	teplota
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ponechají	ponechat	k5eAaPmIp3nP	ponechat
mláďata	mládě	k1gNnPc1	mládě
v	v	k7c6	v
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
vrátí	vrátit	k5eAaPmIp3nP	vrátit
s	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgInPc1d2	vyšší
nároky	nárok	k1gInPc1	nárok
a	a	k8xC	a
tak	tak	k6eAd1	tak
loví	lovit	k5eAaImIp3nP	lovit
partneři	partner	k1gMnPc1	partner
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
utvoří	utvořit	k5eAaPmIp3nP	utvořit
malé	malý	k2eAgFnPc1d1	malá
školky	školka	k1gFnPc1	školka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
čítají	čítat	k5eAaImIp3nP	čítat
maximálně	maximálně	k6eAd1	maximálně
asi	asi	k9	asi
pět	pět	k4xCc4	pět
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
patřičně	patřičně	k6eAd1	patřičně
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
nezkušené	zkušený	k2eNgInPc4d1	nezkušený
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
nejsou	být	k5eNaImIp3nP	být
natolik	natolik	k6eAd1	natolik
vědomy	vědom	k2eAgFnPc1d1	vědoma
rizika	riziko	k1gNnSc2	riziko
spojeného	spojený	k2eAgNnSc2d1	spojené
s	s	k7c7	s
nevhodným	vhodný	k2eNgInSc7d1	nevhodný
způsobem	způsob	k1gInSc7	způsob
zahnízdění	zahnízdění	k1gNnSc4	zahnízdění
–	–	k?	–
především	především	k6eAd1	především
nevyhloubení	nevyhloubení	k1gNnSc1	nevyhloubení
nory	nora	k1gFnSc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
až	až	k8xS	až
sedmém	sedmý	k4xOgInSc6	sedmý
týdnu	týden	k1gInSc6	týden
(	(	kIx(	(
<g/>
asi	asi	k9	asi
51	[number]	k4	51
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mláďata	mládě	k1gNnPc4	mládě
váhy	váha	k1gFnSc2	váha
kolem	kolem	k7c2	kolem
3	[number]	k4	3
kg	kg	kA	kg
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
doba	doba	k1gFnSc1	doba
pelichání	pelichání	k1gNnSc2	pelichání
do	do	k7c2	do
definitivního	definitivní	k2eAgInSc2d1	definitivní
šatu	šat	k1gInSc2	šat
–	–	k?	–
nejprve	nejprve	k6eAd1	nejprve
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
prachové	prachový	k2eAgNnSc1d1	prachové
peří	peří	k1gNnSc1	peří
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejdříve	dříve	k6eAd3	dříve
definitivní	definitivní	k2eAgNnSc1d1	definitivní
pera	pero	k1gNnPc1	pero
narůstají	narůstat	k5eAaImIp3nP	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
62	[number]	k4	62
dnech	den	k1gInPc6	den
je	být	k5eAaImIp3nS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
pelichání	pelichání	k1gNnSc1	pelichání
břicha	břicho	k1gNnSc2	břicho
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
definitivní	definitivní	k2eAgFnPc4d1	definitivní
váhy	váha	k1gFnPc4	váha
4	[number]	k4	4
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
69	[number]	k4	69
dni	den	k1gInSc6	den
jdou	jít	k5eAaImIp3nP	jít
definitivním	definitivní	k2eAgInSc7d1	definitivní
peřím	peřit	k5eAaImIp1nS	peřit
porostlá	porostlý	k2eAgNnPc4d1	porostlé
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
mezi	mezi	k7c4	mezi
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
dnem	den	k1gInSc7	den
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
porostlá	porostlý	k2eAgNnPc1d1	porostlé
definitivním	definitivní	k2eAgInSc7d1	definitivní
peřím	peřit	k5eAaImIp1nS	peřit
celá	celý	k2eAgFnSc1d1	celá
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
připravena	připraven	k2eAgNnPc1d1	připraveno
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sociálních	sociální	k2eAgInPc2d1	sociální
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
od	od	k7c2	od
dospělých	dospělí	k1gMnPc2	dospělí
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Můžou	Můžou	k?	Můžou
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
okolní	okolní	k2eAgMnPc1d1	okolní
tučňáci	tučňák	k1gMnPc1	tučňák
napadali	napadat	k5eAaPmAgMnP	napadat
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
dospělý	dospělý	k2eAgInSc1d1	dospělý
šat	šat	k1gInSc1	šat
se	se	k3xPyFc4	se
takovým	takový	k3xDgMnPc3	takový
mládencům	mládenec	k1gMnPc3	mládenec
dostaví	dostavit	k5eAaPmIp3nP	dostavit
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
zhubnou	zhubnout	k5eAaPmIp3nP	zhubnout
než	než	k8xS	než
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
dobře	dobře	k6eAd1	dobře
lovit	lovit	k5eAaImF	lovit
–	–	k?	–
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
až	až	k9	až
jeden	jeden	k4xCgInSc1	jeden
kilogram	kilogram	k1gInSc1	kilogram
váhy	váha	k1gFnSc2	váha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
35	[number]	k4	35
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
stav	stav	k1gInSc1	stav
znormalizuje	znormalizovat	k5eAaPmIp3nS	znormalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
zralý	zralý	k2eAgInSc1d1	zralý
jsou	být	k5eAaImIp3nP	být
samice	samice	k1gFnPc1	samice
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
a	a	k8xC	a
samci	samec	k1gMnPc1	samec
asi	asi	k9	asi
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
asi	asi	k9	asi
15	[number]	k4	15
000	[number]	k4	000
těchto	tento	k3xDgMnPc2	tento
tučňáků	tučňák	k1gMnPc2	tučňák
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgFnSc1d1	žijící
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
velmi	velmi	k6eAd1	velmi
ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
údajů	údaj	k1gInPc2	údaj
a	a	k8xC	a
odhadů	odhad	k1gInPc2	odhad
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
000	[number]	k4	000
až	až	k9	až
32	[number]	k4	32
000	[number]	k4	000
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
zranitelných	zranitelný	k2eAgInPc2d1	zranitelný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
lety	léto	k1gNnPc7	léto
představovala	představovat	k5eAaImAgFnS	představovat
enormní	enormní	k2eAgFnSc4d1	enormní
hrozbu	hrozba	k1gFnSc4	hrozba
masivní	masivní	k2eAgFnSc1d1	masivní
těžba	těžba	k1gFnSc1	těžba
guána	guáno	k1gNnSc2	guáno
<g/>
,	,	kIx,	,
nahromaděného	nahromaděný	k2eAgInSc2d1	nahromaděný
trusu	trus	k1gInSc2	trus
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
hnízdili	hnízdit	k5eAaImAgMnP	hnízdit
<g/>
,	,	kIx,	,
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
budovali	budovat	k5eAaImAgMnP	budovat
svá	svůj	k3xOyFgNnPc4	svůj
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
následky	následek	k1gInPc4	následek
těžebních	těžební	k2eAgInPc2d1	těžební
zásahů	zásah	k1gInPc2	zásah
uhynulo	uhynout	k5eAaPmAgNnS	uhynout
nespočet	nespočet	k1gInSc4	nespočet
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
byly	být	k5eAaImAgFnP	být
zlikvidovány	zlikvidovat	k5eAaPmNgFnP	zlikvidovat
i	i	k9	i
jejich	jejich	k3xOp3gFnPc1	jejich
nakladené	nakladený	k2eAgFnPc1d1	nakladená
snůšky	snůška	k1gFnPc1	snůška
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
začala	začít	k5eAaPmAgFnS	začít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
plně	plně	k6eAd1	plně
využívat	využívat	k5eAaPmF	využívat
ropná	ropný	k2eAgFnSc1d1	ropná
surovina	surovina	k1gFnSc1	surovina
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
tuk	tuk	k1gInSc4	tuk
a	a	k8xC	a
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
oleje	olej	k1gInSc2	olej
masivně	masivně	k6eAd1	masivně
vybíjeni	vybíjet	k5eAaImNgMnP	vybíjet
po	po	k7c6	po
tisících	tisíc	k4xCgInPc6	tisíc
kusech	kus	k1gInPc6	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
hrozbu	hrozba	k1gFnSc4	hrozba
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
možné	možný	k2eAgInPc4d1	možný
negativní	negativní	k2eAgInPc4d1	negativní
důsledky	důsledek	k1gInPc4	důsledek
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nepřiměřená	přiměřený	k2eNgFnSc1d1	nepřiměřená
ztráta	ztráta	k1gFnSc1	ztráta
potravního	potravní	k2eAgInSc2d1	potravní
zdroje	zdroj	k1gInSc2	zdroj
velkým	velký	k2eAgNnPc3d1	velké
nebezpečím	nebezpečí	k1gNnPc3	nebezpečí
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
celého	celý	k2eAgInSc2d1	celý
ekosystému	ekosystém	k1gInSc2	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnPc1d1	zdejší
mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tučňáků	tučňák	k1gMnPc2	tučňák
Humboldtových	Humboldtových	k2eAgInSc1d1	Humboldtových
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
velmi	velmi	k6eAd1	velmi
závislý	závislý	k2eAgInSc1d1	závislý
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Nezřídka	nezřídka	k6eAd1	nezřídka
tučňáci	tučňák	k1gMnPc1	tučňák
uvíznou	uvíznout	k5eAaPmIp3nP	uvíznout
v	v	k7c4	v
samotných	samotný	k2eAgInPc2d1	samotný
v	v	k7c6	v
rybářských	rybářský	k2eAgFnPc6d1	rybářská
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zabírají	zabírat	k5eAaImIp3nP	zabírat
velkou	velký	k2eAgFnSc4d1	velká
vodní	vodní	k2eAgFnSc4d1	vodní
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
prakticky	prakticky	k6eAd1	prakticky
nelze	lze	k6eNd1	lze
vymanit	vymanit	k5eAaPmF	vymanit
<g/>
,	,	kIx,	,
zachytí	zachytit	k5eAaPmIp3nS	zachytit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
umírají	umírat	k5eAaImIp3nP	umírat
ptáci	pták	k1gMnPc1	pták
na	na	k7c4	na
následky	následek	k1gInPc4	následek
utonutí	utonutí	k1gNnSc2	utonutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nemalé	malý	k2eNgNnSc1d1	nemalé
riziko	riziko	k1gNnSc1	riziko
představuje	představovat	k5eAaImIp3nS	představovat
taktéž	taktéž	k?	taktéž
výskyt	výskyt	k1gInSc1	výskyt
nepůvodních	původní	k2eNgInPc2d1	nepůvodní
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
krysa	krysa	k1gFnSc1	krysa
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
rattus	rattus	k1gMnSc1	rattus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
potkan	potkan	k1gMnSc1	potkan
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
norvegicus	norvegicus	k1gMnSc1	norvegicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
hlodavci	hlodavec	k1gMnPc1	hlodavec
soustředí	soustředit	k5eAaPmIp3nP	soustředit
obzvláště	obzvláště	k6eAd1	obzvláště
na	na	k7c4	na
neoplodněná	oplodněný	k2eNgNnPc4d1	neoplodněné
vajíčka	vajíčko	k1gNnPc4	vajíčko
ležící	ležící	k2eAgNnPc1d1	ležící
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
zranit	zranit	k5eAaPmF	zranit
nebo	nebo	k8xC	nebo
i	i	k9	i
zabít	zabít	k5eAaPmF	zabít
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
hrozbu	hrozba	k1gFnSc4	hrozba
však	však	k9	však
představují	představovat	k5eAaImIp3nP	představovat
zatoulaní	zatoulaný	k2eAgMnPc1d1	zatoulaný
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
f.	f.	k?	f.
familiaris	familiaris	k1gInSc1	familiaris
<g/>
)	)	kIx)	)
a	a	k8xC	a
divoké	divoký	k2eAgFnSc2d1	divoká
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc2	Felis
silvestris	silvestris	k1gFnSc2	silvestris
f.	f.	k?	f.
catus	catus	k1gInSc1	catus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
znatelnou	znatelný	k2eAgFnSc7d1	znatelná
hrozbou	hrozba	k1gFnSc7	hrozba
je	být	k5eAaImIp3nS	být
rozmach	rozmach	k1gInSc1	rozmach
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
se	se	k3xPyFc4	se
nechovají	chovat	k5eNaImIp3nP	chovat
zrovna	zrovna	k6eAd1	zrovna
ohleduplně	ohleduplně	k6eAd1	ohleduplně
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
nedobře	dobře	k6eNd1	dobře
zajištěných	zajištěný	k2eAgNnPc6d1	zajištěné
místech	místo	k1gNnPc6	místo
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
pošlapání	pošlapání	k1gNnSc3	pošlapání
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
extrémně	extrémně	k6eAd1	extrémně
citlivý	citlivý	k2eAgInSc4d1	citlivý
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
znatelný	znatelný	k2eAgInSc1d1	znatelný
neúspěch	neúspěch	k1gInSc1	neúspěch
v	v	k7c6	v
reprodukci	reprodukce	k1gFnSc6	reprodukce
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
častou	častý	k2eAgFnSc7d1	častá
a	a	k8xC	a
početnější	početní	k2eAgFnSc7d2	početnější
přítomností	přítomnost	k1gFnSc7	přítomnost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
tyto	tento	k3xDgMnPc4	tento
ptáky	pták	k1gMnPc4	pták
rozvíjející	rozvíjející	k2eAgMnPc4d1	rozvíjející
se	se	k3xPyFc4	se
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojené	spojený	k2eAgFnPc1d1	spojená
výstavby	výstavba	k1gFnPc1	výstavba
narušující	narušující	k2eAgFnPc1d1	narušující
jejich	jejich	k3xOp3gInSc4	jejich
biotop	biotop	k1gInSc4	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
masové	masový	k2eAgInPc4d1	masový
protesty	protest	k1gInPc4	protest
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zakládájících	zakládájící	k2eAgMnPc2d1	zakládájící
se	se	k3xPyFc4	se
na	na	k7c6	na
podepsaných	podepsaný	k2eAgFnPc6d1	podepsaná
peticích	petice	k1gFnPc6	petice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
víru	vír	k1gInSc6	vír
demokracie	demokracie	k1gFnSc2	demokracie
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
zabrání	zabránit	k5eAaPmIp3nS	zabránit
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
oddálí	oddálit	k5eAaPmIp3nS	oddálit
plány	plán	k1gInPc4	plán
mocných	mocný	k2eAgMnPc2d1	mocný
investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
příliš	příliš	k6eAd1	příliš
neřeší	řešit	k5eNaImIp3nP	řešit
možné	možný	k2eAgFnPc4d1	možná
hrozby	hrozba	k1gFnPc4	hrozba
pro	pro	k7c4	pro
tuzemskou	tuzemský	k2eAgFnSc4d1	tuzemská
faunu	fauna	k1gFnSc4	fauna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
příčinou	příčina	k1gFnSc7	příčina
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
nešetrný	šetrný	k2eNgInSc1d1	nešetrný
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
početné	početný	k2eAgFnPc4d1	početná
a	a	k8xC	a
reálné	reálný	k2eAgFnPc4d1	reálná
hrozby	hrozba	k1gFnPc4	hrozba
jsou	být	k5eAaImIp3nP	být
ochranná	ochranný	k2eAgNnPc1d1	ochranné
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
vytváření	vytváření	k1gNnSc1	vytváření
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
na	na	k7c6	na
významných	významný	k2eAgFnPc6d1	významná
nebo	nebo	k8xC	nebo
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgNnPc6d1	ohrožené
hnízdištích	hnízdiště	k1gNnPc6	hnízdiště
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
monitorování	monitorování	k1gNnSc1	monitorování
chovných	chovný	k2eAgFnPc2d1	chovná
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
případná	případný	k2eAgFnSc1d1	případná
eradikace	eradikace	k1gFnSc1	eradikace
invazivních	invazivní	k2eAgInPc2d1	invazivní
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
záchranných	záchranný	k2eAgInPc2d1	záchranný
projektů	projekt	k1gInPc2	projekt
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
i	i	k9	i
Chilská	chilský	k2eAgFnSc1d1	chilská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zachraňovat	zachraňovat	k5eAaImF	zachraňovat
oplodněná	oplodněný	k2eAgFnSc1d1	oplodněná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
opuštěná	opuštěný	k2eAgNnPc1d1	opuštěné
vejce	vejce	k1gNnPc1	vejce
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
museu	museum	k1gNnSc6	museum
Shimonoseki	Shimonosek	k1gFnSc2	Shimonosek
Marine	Marin	k1gInSc5	Marin
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
samice	samice	k1gFnSc2	samice
uměle	uměle	k6eAd1	uměle
oplodnit	oplodnit	k5eAaPmF	oplodnit
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
rozmrazených	rozmrazený	k2eAgInPc2d1	rozmrazený
vzorků	vzorek	k1gInPc2	vzorek
samčího	samčí	k2eAgNnSc2d1	samčí
spermatu	sperma	k1gNnSc2	sperma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
140	[number]	k4	140
evropských	evropský	k2eAgFnPc2d1	Evropská
zoo	zoo	k1gFnPc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
chován	chovat	k5eAaImNgMnS	chovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
programu	program	k1gInSc2	program
EEP	EEP	kA	EEP
–	–	k?	–
Evropský	evropský	k2eAgInSc4d1	evropský
záchovný	záchovný	k2eAgInSc4d1	záchovný
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
společného	společný	k2eAgInSc2d1	společný
projektu	projekt	k1gInSc2	projekt
evropských	evropský	k2eAgFnPc2d1	Evropská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
spolupráce	spolupráce	k1gFnSc1	spolupráce
při	při	k7c6	při
chovu	chov	k1gInSc6	chov
některých	některý	k3yIgMnPc2	některý
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
asociace	asociace	k1gFnSc1	asociace
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chov	chov	k1gInSc1	chov
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tučňáků	tučňák	k1gMnPc2	tučňák
Humboldtových	Humboldtová	k1gFnPc2	Humboldtová
<g/>
,	,	kIx,	,
plánovala	plánovat	k5eAaImAgFnS	plánovat
také	také	k9	také
ostravská	ostravský	k2eAgFnSc1d1	Ostravská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
chovu	chov	k1gInSc2	chov
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
odchov	odchov	k1gInSc1	odchov
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vylíhlo	vylíhnout	k5eAaPmAgNnS	vylíhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
140	[number]	k4	140
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
evropských	evropský	k2eAgInPc2d1	evropský
chovů	chov	k1gInPc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
aktuálně	aktuálně	k6eAd1	aktuálně
chovanou	chovaný	k2eAgFnSc4d1	chovaná
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
již	již	k6eAd1	již
zejména	zejména	k9	zejména
jedinci	jedinec	k1gMnPc1	jedinec
právě	právě	k6eAd1	právě
z	z	k7c2	z
pražských	pražský	k2eAgInPc2d1	pražský
odchovů	odchov	k1gInPc2	odchov
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
jsou	být	k5eAaImIp3nP	být
krmena	krmit	k5eAaImNgNnP	krmit
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
mořskými	mořský	k2eAgFnPc7d1	mořská
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
doplňovány	doplňovat	k5eAaImNgInP	doplňovat
dalšími	další	k2eAgFnPc7d1	další
látkami	látka	k1gFnPc7	látka
potřebnými	potřebný	k2eAgFnPc7d1	potřebná
pro	pro	k7c4	pro
život	život	k1gInSc4	život
těchto	tento	k3xDgMnPc2	tento
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
chovu	chov	k1gInSc2	chov
byli	být	k5eAaImAgMnP	být
dokonce	dokonce	k9	dokonce
představeni	představen	k2eAgMnPc1d1	představen
veřejnosti	veřejnost	k1gFnSc3	veřejnost
i	i	k9	i
podrobněji	podrobně	k6eAd2	podrobně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
včetně	včetně	k7c2	včetně
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
povahy	povaha	k1gFnSc2	povaha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaní	kampaň	k1gFnSc7	kampaň
Poznejme	poznat	k5eAaPmRp1nP	poznat
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
či	či	k8xC	či
Seznamte	seznámit	k5eAaPmRp2nP	seznámit
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pipin	pipina	k1gFnPc2	pipina
–	–	k?	–
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
partner	partner	k1gMnSc1	partner
Pinďi	Pinď	k1gFnSc2	Pinď
<g/>
,	,	kIx,	,
135	[number]	k4	135
<g/>
.	.	kIx.	.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
narozený	narozený	k2eAgMnSc1d1	narozený
tučňák	tučňák	k1gMnSc1	tučňák
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pinďa	Pinď	k2eAgFnSc1d1	Pinďa
–	–	k?	–
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
partnerka	partnerka	k1gFnSc1	partnerka
Pipina	pipina	k1gFnSc1	pipina
<g/>
,	,	kIx,	,
144	[number]	k4	144
<g/>
.	.	kIx.	.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
narozený	narozený	k2eAgMnSc1d1	narozený
tučňák	tučňák	k1gMnSc1	tučňák
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otylka	Otylka	k1gFnSc1	Otylka
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
samice	samice	k1gFnSc1	samice
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
narozena	narozen	k2eAgFnSc1d1	narozena
2002	[number]	k4	2002
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
chováno	chovat	k5eAaImNgNnS	chovat
29	[number]	k4	29
tučňáků	tučňák	k1gMnPc2	tučňák
Humboldtových	Humboldtův	k2eAgMnPc2d1	Humboldtův
(	(	kIx(	(
<g/>
14	[number]	k4	14
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
15	[number]	k4	15
samic	samice	k1gFnPc2	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pavilon	pavilon	k1gInSc1	pavilon
tučňáků	tučňák	k1gMnPc2	tučňák
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
zoo	zoo	k1gFnSc2	zoo
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
a	a	k8xC	a
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
234	[number]	k4	234
m	m	kA	m
<g/>
2	[number]	k4	2
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
letech	let	k1gInPc6	let
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
(	(	kIx(	(
<g/>
otevřen	otevřít	k5eAaPmNgInS	otevřít
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
byla	být	k5eAaImAgFnS	být
přestavena	přestavit	k5eAaPmNgFnS	přestavit
po	po	k7c6	po
povodni	povodeň	k1gFnSc6	povodeň
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
znovuotevření	znovuotevření	k1gNnSc3	znovuotevření
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnicky	návštěvnicky	k6eAd1	návštěvnicky
přístupný	přístupný	k2eAgInSc1d1	přístupný
pavilon	pavilon	k1gInSc1	pavilon
disponuje	disponovat	k5eAaBmIp3nS	disponovat
umělými	umělý	k2eAgFnPc7d1	umělá
skalami	skála	k1gFnPc7	skála
s	s	k7c7	s
hnízdními	hnízdní	k2eAgInPc7d1	hnízdní
výklenky	výklenek	k1gInPc7	výklenek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
navazuje	navazovat	k5eAaImIp3nS	navazovat
výběh	výběh	k1gInSc1	výběh
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
635	[number]	k4	635
m	m	kA	m
<g/>
2	[number]	k4	2
s	s	k7c7	s
bazénem	bazén	k1gInSc7	bazén
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
90	[number]	k4	90
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
až	až	k9	až
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
humboldti	humboldť	k1gFnSc2	humboldť
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
na	na	k7c4	na
animaldiversity	animaldiversit	k1gInPc4	animaldiversit
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
