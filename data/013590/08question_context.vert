<s>
Věstonická	věstonický	k2eAgFnSc1d1
venuše	venuše	k1gFnSc1
je	být	k5eAaImIp3nS
keramická	keramický	k2eAgFnSc1d1
soška	soška	k1gFnSc1
nahé	nahý	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
vyrobená	vyrobený	k2eAgFnSc1d1
z	z	k7c2
pálené	pálený	k2eAgFnSc2d1
hlíny	hlína	k1gFnSc2
pocházející	pocházející	k2eAgFnSc6d1
z	z	k7c2
mladého	mladý	k2eAgInSc2d1
paleolitu	paleolit	k1gInSc2
a	a	k8xC
datovaná	datovaný	k2eAgFnSc1d1
do	do	k7c2
období	období	k1gNnSc2
29	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	kA
l.	l.	kA
Tato	tento	k3xDgFnSc1
figurka	figurka	k1gFnSc1
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
několika	několik	k4yIc7
dalšími	další	k2eAgMnPc7d1
z	z	k7c2
okolních	okolní	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
známá	známý	k2eAgFnSc1d1
keramická	keramický	k2eAgFnSc1d1
soška	soška	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gInSc1
objev	objev	k1gInSc1
vyvrátil	vyvrátit	k5eAaPmAgInS
dosavadní	dosavadní	k2eAgFnSc4d1
domněnku	domněnka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
keramika	keramika	k1gFnSc1
nebyla	být	k5eNaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
paleolitu	paleolit	k1gInSc2
ještě	ještě	k6eAd1
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>