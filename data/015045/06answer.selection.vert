<s>
Moscovium	Moscovium	k1gNnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
název	název	k1gInSc1
moskovium	moskovium	k1gNnSc1
nevychází	vycházet	k5eNaImIp3nS
z	z	k7c2
odborných	odborný	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ze	z	k7c2
zprávy	zpráva	k1gFnSc2
ČTK	ČTK	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nelze	lze	k6eNd1
ho	on	k3xPp3gInSc4
zatím	zatím	k6eAd1
brát	brát	k5eAaImF
jako	jako	k8xS,k8xC
konečný	konečný	k2eAgInSc1d1
<g/>
;	;	kIx,
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Mc	Mc	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc1
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
115	#num#	k4
<g/>
.	.	kIx.
</s>