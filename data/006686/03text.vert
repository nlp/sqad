<s>
Mensa	mensa	k1gFnSc1	mensa
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
nevýdělečná	výdělečný	k2eNgFnSc1d1	nevýdělečná
organizace	organizace	k1gFnSc1	organizace
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
IQ	iq	kA	iq
mezi	mezi	k7c7	mezi
horními	horní	k2eAgNnPc7d1	horní
2	[number]	k4	2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
minimální	minimální	k2eAgMnSc1d1	minimální
IQ	iq	kA	iq
dle	dle	k7c2	dle
české	český	k2eAgFnSc2d1	Česká
stupnice	stupnice	k1gFnSc2	stupnice
130	[number]	k4	130
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Stanford-Binetovy	Stanford-Binetův	k2eAgFnSc2d1	Stanford-Binetův
132	[number]	k4	132
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Cattellovy	Cattellův	k2eAgFnSc2d1	Cattellův
148	[number]	k4	148
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tohoto	tento	k3xDgInSc2	tento
výsledku	výsledek	k1gInSc2	výsledek
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
v	v	k7c6	v
testu	test	k1gInSc6	test
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
schváleném	schválený	k2eAgInSc6d1	schválený
mezinárodním	mezinárodní	k2eAgMnSc7d1	mezinárodní
dozorčím	dozorčí	k2eAgMnSc7d1	dozorčí
psychologem	psycholog	k1gMnSc7	psycholog
Mensy	mensa	k1gFnSc2	mensa
International	International	k1gFnSc2	International
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
zemích	zem	k1gFnPc6	zem
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
členů	člen	k1gInPc2	člen
–	–	k?	–
formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
národních	národní	k2eAgFnPc2d1	národní
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Mensa	mensa	k1gFnSc1	mensa
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
a	a	k8xC	a
zastřešující	zastřešující	k2eAgFnSc2d1	zastřešující
Mensy	mensa	k1gFnSc2	mensa
International	International	k1gFnSc2	International
<g/>
.	.	kIx.	.
</s>
