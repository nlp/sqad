<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Stargate	Stargat	k1gMnSc5	Stargat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sci-fi	scii	k1gFnSc1	sci-fi
film	film	k1gInSc1	film
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
USA	USA	kA	USA
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
Roland	Rolando	k1gNnPc2	Rolando
Emmerich	Emmericha	k1gFnPc2	Emmericha
<g/>
.	.	kIx.	.
</s>
