<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Stargate	Stargat	k1gMnSc5	Stargat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sci-fi	scii	k1gFnSc1	sci-fi
film	film	k1gInSc1	film
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
USA	USA	kA	USA
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
Roland	Rolando	k1gNnPc2	Rolando
Emmerich	Emmericha	k1gFnPc2	Emmericha
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
scénáře	scénář	k1gInSc2	scénář
jsou	být	k5eAaImIp3nP	být
Roland	Roland	k1gInSc4	Roland
Emmerich	Emmericha	k1gFnPc2	Emmericha
a	a	k8xC	a
Dean	Deana	k1gFnPc2	Deana
Devlin	Devlina	k1gFnPc2	Devlina
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
kinech	kino	k1gNnPc6	kino
16	[number]	k4	16
651	[number]	k4	651
018	[number]	k4	018
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k8xC	i
navazující	navazující	k2eAgInSc1d1	navazující
seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
do	do	k7c2	do
multimediální	multimediální	k2eAgFnSc2d1	multimediální
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Stopáž	stopáž	k1gFnSc1	stopáž
filmu	film	k1gInSc2	film
činí	činit	k5eAaImIp3nS	činit
121	[number]	k4	121
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
profesor	profesor	k1gMnSc1	profesor
Langford	Langford	k1gMnSc1	Langford
objevuje	objevovat	k5eAaImIp3nS	objevovat
obrovský	obrovský	k2eAgInSc4d1	obrovský
kamenný	kamenný	k2eAgInSc4d1	kamenný
náhrobek	náhrobek	k1gInSc4	náhrobek
v	v	k7c6	v
písčinách	písčina	k1gFnPc6	písčina
Gízy	Gíza	k1gFnSc2	Gíza
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
neznámými	známý	k2eNgInPc7d1	neznámý
symboly	symbol	k1gInPc7	symbol
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
ukryto	ukryt	k2eAgNnSc1d1	ukryto
neznámé	známý	k2eNgNnSc1d1	neznámé
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Artefakty	artefakt	k1gInPc1	artefakt
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
se	se	k3xPyFc4	se
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vědců	vědec	k1gMnPc2	vědec
snaží	snažit	k5eAaImIp3nS	snažit
rozluštit	rozluštit	k5eAaPmF	rozluštit
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
Langfordova	Langfordův	k2eAgFnSc1d1	Langfordův
dcera	dcera	k1gFnSc1	dcera
Catherine	Catherin	k1gInSc5	Catherin
nabízí	nabízet	k5eAaImIp3nS	nabízet
egyptologovi	egyptolog	k1gMnSc3	egyptolog
a	a	k8xC	a
lingvistovi	lingvista	k1gMnSc3	lingvista
Danielu	Daniel	k1gMnSc3	Daniel
Jacksonovi	Jackson	k1gMnSc3	Jackson
<g/>
,	,	kIx,	,
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přeložení	přeložení	k1gNnSc4	přeložení
starověkých	starověký	k2eAgInPc2d1	starověký
egyptských	egyptský	k2eAgInPc2d1	egyptský
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
prokázat	prokázat	k5eAaPmF	prokázat
jeho	jeho	k3xOp3gFnSc4	jeho
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramidy	pyramida	k1gFnPc1	pyramida
jsou	být	k5eAaImIp3nP	být
přistávací	přistávací	k2eAgFnPc1d1	přistávací
plochy	plocha	k1gFnPc1	plocha
pro	pro	k7c4	pro
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
úkol	úkol	k1gInSc4	úkol
přijímá	přijímat	k5eAaImIp3nS	přijímat
a	a	k8xC	a
cestuje	cestovat	k5eAaImIp3nS	cestovat
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
uvnitř	uvnitř	k7c2	uvnitř
Creek	Creky	k1gFnPc2	Creky
Mountain	Mountaina	k1gFnPc2	Mountaina
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
překládá	překládat	k5eAaImIp3nS	překládat
hieroglyfy	hieroglyf	k1gInPc4	hieroglyf
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
na	na	k7c6	na
kamenném	kamenný	k2eAgInSc6d1	kamenný
náhrobku	náhrobek	k1gInSc6	náhrobek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
čte	číst	k5eAaImIp3nS	číst
<g/>
:	:	kIx,	:
Milion	milion	k4xCgInSc1	milion
let	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
zapečetěn	zapečetit	k5eAaPmNgMnS	zapečetit
a	a	k8xC	a
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c4	na
věčné	věčný	k2eAgInPc4d1	věčný
časy	čas	k1gInPc4	čas
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Jack	Jack	k1gMnSc1	Jack
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
<g/>
,	,	kIx,	,
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
převzít	převzít	k5eAaPmF	převzít
velení	velení	k1gNnSc4	velení
nad	nad	k7c7	nad
projektem	projekt	k1gInSc7	projekt
a	a	k8xC	a
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
tajný	tajný	k2eAgInSc1d1	tajný
<g/>
.	.	kIx.	.
</s>
<s>
Jacksona	Jacksona	k1gFnSc1	Jacksona
napadne	napadnout	k5eAaPmIp3nS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
symboly	symbol	k1gInPc1	symbol
jsou	být	k5eAaImIp3nP	být
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
pro	pro	k7c4	pro
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
se	se	k3xPyFc4	se
vloží	vložit	k5eAaPmIp3nS	vložit
do	do	k7c2	do
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
červí	červí	k2eAgFnSc4d1	červí
díru	díra	k1gFnSc4	díra
do	do	k7c2	do
určeného	určený	k2eAgNnSc2d1	určené
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Langfordová	Langfordová	k1gFnSc1	Langfordová
dává	dávat	k5eAaImIp3nS	dávat
Jacksonovi	Jackson	k1gMnSc3	Jackson
medailon	medailon	k1gInSc1	medailon
"	"	kIx"	"
<g/>
oko	oko	k1gNnSc1	oko
Ra	ra	k0	ra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
původně	původně	k6eAd1	původně
našla	najít	k5eAaPmAgFnS	najít
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
při	při	k7c6	při
objevení	objevení	k1gNnSc6	objevení
brány	brána	k1gFnSc2	brána
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gInSc4	Neil
vede	vést	k5eAaImIp3nS	vést
tým	tým	k1gInSc1	tým
skrze	skrze	k?	skrze
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
na	na	k7c6	na
cizí	cizí	k2eAgFnSc6d1	cizí
planetě	planeta	k1gFnSc6	planeta
uvnitř	uvnitř	k7c2	uvnitř
pyramidy	pyramida	k1gFnSc2	pyramida
uprostřed	uprostřed	k7c2	uprostřed
obrovských	obrovský	k2eAgFnPc2d1	obrovská
dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
vytočit	vytočit	k5eAaPmF	vytočit
adresu	adresa	k1gFnSc4	adresa
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
souřadnice	souřadnice	k1gFnSc1	souřadnice
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
zde	zde	k6eAd1	zde
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
týmu	tým	k1gInSc2	tým
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
v	v	k7c6	v
pyramidě	pyramida	k1gFnSc6	pyramida
zatímco	zatímco	k8xS	zatímco
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
jdou	jít	k5eAaImIp3nP	jít
ven	ven	k6eAd1	ven
a	a	k8xC	a
objeví	objevit	k5eAaPmIp3nS	objevit
vesnici	vesnice	k1gFnSc4	vesnice
obývanou	obývaný	k2eAgFnSc7d1	obývaná
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
když	když	k8xS	když
uvidí	uvidět	k5eAaPmIp3nP	uvidět
Jacksonův	Jacksonův	k2eAgInSc4d1	Jacksonův
medailon	medailon	k1gInSc4	medailon
<g/>
,	,	kIx,	,
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
bohové	bůh	k1gMnPc1	bůh
posláni	poslat	k5eAaPmNgMnP	poslat
Raem	Rae	k1gNnSc7	Rae
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
mluví	mluvit	k5eAaImIp3nP	mluvit
starověkým	starověký	k2eAgInSc7d1	starověký
egyptským	egyptský	k2eAgInSc7d1	egyptský
dialektem	dialekt	k1gInSc7	dialekt
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
psaní	psaní	k1gNnSc1	psaní
je	být	k5eAaImIp3nS	být
těmto	tento	k3xDgMnPc3	tento
lidem	člověk	k1gMnPc3	člověk
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
navazuje	navazovat	k5eAaImIp3nS	navazovat
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
:	:	kIx,	:
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
se	s	k7c7	s
Skaarou	Skaara	k1gFnSc7	Skaara
<g/>
,	,	kIx,	,
Kawalsky	Kawalsky	k1gFnSc1	Kawalsky
a	a	k8xC	a
Brown	Brown	k1gNnSc1	Brown
se	s	k7c7	s
Skaarovými	Skaarův	k2eAgMnPc7d1	Skaarův
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
Jackson	Jackson	k1gMnSc1	Jackson
neúmyslně	úmyslně	k6eNd1	úmyslně
začíná	začínat	k5eAaImIp3nS	začínat
nadějný	nadějný	k2eAgInSc1d1	nadějný
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
Sha	Sha	k1gFnSc7	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sha	Sha	k1gFnSc1	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
má	mít	k5eAaImIp3nS	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
zkušenost	zkušenost	k1gFnSc4	zkušenost
se	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
vidět	vidět	k5eAaImF	vidět
více	hodně	k6eAd2	hodně
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
opouští	opouštět	k5eAaImIp3nS	opouštět
město	město	k1gNnSc1	město
a	a	k8xC	a
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
skrytých	skrytý	k2eAgFnPc2d1	skrytá
katakomb	katakomby	k1gFnPc2	katakomby
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
napsaných	napsaný	k2eAgInPc2d1	napsaný
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
egyptský	egyptský	k2eAgMnSc1d1	egyptský
bůh	bůh	k1gMnSc1	bůh
Ra	ra	k0	ra
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nějakou	nějaký	k3yIgFnSc7	nějaký
cizí	cizí	k2eAgFnSc7d1	cizí
životní	životní	k2eAgFnSc7d1	životní
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opustila	opustit	k5eAaPmAgFnS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
umírající	umírající	k2eAgInSc4d1	umírající
svět	svět	k1gInSc4	svět
hledat	hledat	k5eAaImF	hledat
lék	lék	k1gInSc4	lék
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
smrtelnost	smrtelnost	k1gFnSc4	smrtelnost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
dospěla	dochvít	k5eAaPmAgFnS	dochvít
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
<g/>
"	"	kIx"	"
těla	tělo	k1gNnSc2	tělo
jednoho	jeden	k4xCgMnSc2	jeden
lidského	lidský	k2eAgMnSc2d1	lidský
mladíka	mladík	k1gMnSc2	mladík
jako	jako	k9	jako
parazit	parazit	k1gMnSc1	parazit
a	a	k8xC	a
zotročila	zotročit	k5eAaPmAgFnS	zotročit
lidi	člověk	k1gMnPc4	člověk
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
pokročilou	pokročilý	k2eAgFnSc7d1	pokročilá
technologií	technologie	k1gFnSc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
a	a	k8xC	a
pohřbili	pohřbít	k5eAaPmAgMnP	pohřbít
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
tisíce	tisíc	k4xCgInPc1	tisíc
ostatních	ostatní	k2eAgMnPc2d1	ostatní
byli	být	k5eAaImAgMnP	být
odvedeni	odvést	k5eAaPmNgMnP	odvést
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
planety	planeta	k1gFnPc4	planeta
skrze	skrze	k?	skrze
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
využívání	využívání	k1gNnSc4	využívání
jako	jako	k8xC	jako
otroci	otrok	k1gMnPc1	otrok
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
minerálu	minerál	k1gInSc2	minerál
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
veškerá	veškerý	k3xTgFnSc1	veškerý
Raova	Raova	k1gFnSc1	Raova
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
dalšího	další	k2eAgNnSc2d1	další
povstání	povstání	k1gNnSc2	povstání
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
a	a	k8xC	a
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
lidem	člověk	k1gMnPc3	člověk
čtení	čtení	k1gNnSc2	čtení
a	a	k8xC	a
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
nalézá	nalézat	k5eAaImIp3nS	nalézat
Jackson	Jackson	k1gMnSc1	Jackson
kartuši	kartuše	k1gFnSc4	kartuše
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
symboly	symbol	k1gInPc4	symbol
potřebné	potřebný	k2eAgInPc4d1	potřebný
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sedmý	sedmý	k4xOgInSc1	sedmý
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
nečitelný	čitelný	k2eNgInSc1d1	nečitelný
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
týmu	tým	k1gInSc2	tým
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
nad	nad	k7c7	nad
pyramidu	pyramid	k1gInSc6	pyramid
obrovské	obrovský	k2eAgNnSc4d1	obrovské
kosmické	kosmický	k2eAgNnSc4d1	kosmické
plavidlo	plavidlo	k1gNnSc4	plavidlo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
pyramidě	pyramida	k1gFnSc6	pyramida
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
zabiti	zabít	k5eAaPmNgMnP	zabít
nebo	nebo	k8xC	nebo
transportováni	transportovat	k5eAaBmNgMnP	transportovat
do	do	k7c2	do
pyramidového	pyramidový	k2eAgNnSc2d1	pyramidové
plavidla	plavidlo	k1gNnSc2	plavidlo
transportními	transportní	k2eAgInPc7d1	transportní
kruhy	kruh	k1gInPc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
a	a	k8xC	a
Jackson	Jackson	k1gMnSc1	Jackson
jsou	být	k5eAaImIp3nP	být
přivedeni	přiveden	k2eAgMnPc1d1	přiveden
strážemi	stráž	k1gFnPc7	stráž
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
Raem	Rae	k1gNnSc7	Rae
<g/>
.	.	kIx.	.
</s>
<s>
Raovi	Raoev	k1gFnSc3	Raoev
stráže	stráž	k1gFnSc2	stráž
nosí	nosit	k5eAaImIp3nS	nosit
hrůzostrašné	hrůzostrašný	k2eAgNnSc1d1	hrůzostrašné
brnění	brnění	k1gNnSc1	brnění
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Anubise	Anubise	k1gFnSc2	Anubise
a	a	k8xC	a
Horuse	Horuse	k1gFnSc2	Horuse
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
stráže	stráž	k1gFnSc2	stráž
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
strážný	strážný	k1gMnSc1	strážný
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zápěstí	zápěstí	k1gNnSc6	zápěstí
modré	modrý	k2eAgNnSc1d1	modré
ozdobené	ozdobený	k2eAgNnSc1d1	ozdobené
tlačítko	tlačítko	k1gNnSc1	tlačítko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
kovové	kovový	k2eAgFnPc4d1	kovová
boží	boží	k2eAgFnPc4d1	boží
masky	maska	k1gFnPc4	maska
do	do	k7c2	do
límců	límec	k1gInPc2	límec
jejich	jejich	k3xOp3gNnSc4	jejich
brnění	brnění	k1gNnSc4	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
deaktivuje	deaktivovat	k5eAaImIp3nS	deaktivovat
svou	svůj	k3xOyFgFnSc4	svůj
masku	maska	k1gFnSc4	maska
a	a	k8xC	a
ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc4d1	lidský
mladík	mladík	k1gInSc4	mladík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
bílé	bílé	k1gNnSc1	bílé
očí	oko	k1gNnPc2	oko
často	často	k6eAd1	často
žhnou	žhnout	k5eAaImIp3nP	žhnout
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
odhalí	odhalit	k5eAaPmIp3nS	odhalit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
poslat	poslat	k5eAaPmF	poslat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
přinesenou	přinesený	k2eAgFnSc4d1	přinesená
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neilem	Neil	k1gInSc7	Neil
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
bude	být	k5eAaImBp3nS	být
objevena	objeven	k2eAgFnSc1d1	objevena
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Destruktivní	destruktivní	k2eAgFnSc1d1	destruktivní
síla	síla	k1gFnSc1	síla
bomby	bomba	k1gFnSc2	bomba
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
x	x	k?	x
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Ra	ra	k0	ra
k	k	k7c3	k
bombě	bomba	k1gFnSc3	bomba
přidal	přidat	k5eAaPmAgInS	přidat
minerál	minerál	k1gInSc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
zneškodnění	zneškodnění	k1gNnSc4	zneškodnění
stráží	stráž	k1gFnPc2	stráž
a	a	k8xC	a
zabití	zabití	k1gNnSc4	zabití
Raa	Raa	k1gFnSc2	Raa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slituje	slitovat	k5eAaPmIp3nS	slitovat
se	se	k3xPyFc4	se
když	když	k9	když
Ra	ra	k0	ra
použije	použít	k5eAaPmIp3nS	použít
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
"	"	kIx"	"
<g/>
dvořany	dvořan	k1gMnPc4	dvořan
<g/>
"	"	kIx"	"
jako	jako	k9	jako
lidské	lidský	k2eAgInPc4d1	lidský
štíty	štít	k1gInPc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gInSc4	Neil
je	být	k5eAaImIp3nS	být
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
zajatými	zajatý	k2eAgInPc7d1	zajatý
členy	člen	k1gInPc7	člen
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Jackson	Jackson	k1gMnSc1	Jackson
je	být	k5eAaImIp3nS	být
regenerován	regenerovat	k5eAaBmNgMnS	regenerovat
v	v	k7c6	v
zařízení	zařízení	k1gNnSc6	zařízení
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
sarkofágu	sarkofág	k1gInSc2	sarkofág
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabije	zabít	k5eAaPmIp3nS	zabít
Jacksona	Jackson	k1gMnSc4	Jackson
a	a	k8xC	a
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ho	on	k3xPp3gMnSc4	on
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Jackson	Jackson	k1gMnSc1	Jackson
nezabije	zabít	k5eNaPmIp3nS	zabít
zbytek	zbytek	k1gInSc4	zbytek
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukázal	ukázat	k5eAaPmAgMnS	ukázat
vesničanům	vesničan	k1gMnPc3	vesničan
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ra	ra	k0	ra
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
jediný	jediný	k2eAgMnSc1d1	jediný
opravdový	opravdový	k2eAgMnSc1d1	opravdový
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
Ra	ra	k0	ra
shromáždí	shromáždět	k5eAaImIp3nS	shromáždět
místní	místní	k2eAgMnPc4d1	místní
lidi	člověk	k1gMnPc4	člověk
před	před	k7c7	před
pyramidou	pyramida	k1gFnSc7	pyramida
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
mladých	mladý	k2eAgMnPc2d1	mladý
vesničanů	vesničan	k1gMnPc2	vesničan
dává	dávat	k5eAaImIp3nS	dávat
znamení	znamení	k1gNnSc4	znamení
Jacksonovi	Jacksonův	k2eAgMnPc1d1	Jacksonův
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
pozemské	pozemský	k2eAgFnPc1d1	pozemská
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
je	být	k5eAaImIp3nS	být
předávána	předáván	k2eAgFnSc1d1	předávána
tyčová	tyčový	k2eAgFnSc1d1	tyčová
zbraň	zbraň	k1gFnSc1	zbraň
na	na	k7c4	na
popravu	poprava	k1gFnSc4	poprava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
otočí	otočit	k5eAaPmIp3nS	otočit
a	a	k8xC	a
střílí	střílet	k5eAaImIp3nS	střílet
na	na	k7c4	na
Raa	Raa	k1gFnSc4	Raa
<g/>
.	.	kIx.	.
</s>
<s>
Vesničané	vesničan	k1gMnPc1	vesničan
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
a	a	k8xC	a
osvobodí	osvobodit	k5eAaPmIp3nP	osvobodit
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neila	Neil	k1gMnSc4	Neil
<g/>
,	,	kIx,	,
Jacksona	Jackson	k1gMnSc4	Jackson
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
když	když	k8xS	když
Skaara	Skaara	k1gFnSc1	Skaara
kreslí	kreslit	k5eAaImIp3nS	kreslit
obraz	obraz	k1gInSc1	obraz
lidového	lidový	k2eAgNnSc2d1	lidové
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c4	nad
Raem	Raem	k1gInSc4	Raem
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Jackson	Jackson	k1gMnSc1	Jackson
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
kresby	kresba	k1gFnSc2	kresba
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
sedmý	sedmý	k4xOgInSc4	sedmý
symbol	symbol	k1gInSc4	symbol
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
aktivaci	aktivace	k1gFnSc3	aktivace
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
nad	nad	k7c7	nad
pyramidou	pyramida	k1gFnSc7	pyramida
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
a	a	k8xC	a
vesničtí	vesnický	k2eAgMnPc1d1	vesnický
mladíci	mladík	k1gMnPc1	mladík
přestrojení	přestrojený	k2eAgMnPc1d1	přestrojený
za	za	k7c4	za
otroky	otrok	k1gMnPc4	otrok
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
přemohou	přemoct	k5eAaPmIp3nP	přemoct
a	a	k8xC	a
zabijí	zabít	k5eAaPmIp3nP	zabít
jejich	jejich	k3xOp3gMnPc4	jejich
dozorce	dozorce	k1gMnPc4	dozorce
a	a	k8xC	a
stáhnou	stáhnout	k5eAaPmIp3nP	stáhnout
mu	on	k3xPp3gMnSc3	on
boží	boží	k2eAgFnSc4d1	boží
masku	maska	k1gFnSc4	maska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
"	"	kIx"	"
<g/>
bohové	bůh	k1gMnPc1	bůh
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
smrtelníci	smrtelník	k1gMnPc1	smrtelník
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
<g/>
,	,	kIx,	,
Jackson	Jackson	k1gMnSc1	Jackson
a	a	k8xC	a
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
týmu	tým	k1gInSc2	tým
vrátí	vrátit	k5eAaPmIp3nP	vrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
bráně	brána	k1gFnSc3	brána
doufajíce	doufat	k5eAaImSgFnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
deaktivuje	deaktivovat	k5eAaImIp3nS	deaktivovat
bombu	bomba	k1gFnSc4	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
mezitím	mezitím	k6eAd1	mezitím
popravuje	popravovat	k5eAaImIp3nS	popravovat
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
stráží	stráž	k1gFnPc2	stráž
za	za	k7c4	za
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zahájí	zahájit	k5eAaPmIp3nS	zahájit
otevřené	otevřený	k2eAgNnSc4d1	otevřené
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
Raovi	Raoev	k1gFnSc3	Raoev
<g/>
,	,	kIx,	,
Ra	ra	k0	ra
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
svou	svůj	k3xOyFgFnSc4	svůj
loď	loď	k1gFnSc4	loď
ke	k	k7c3	k
startu	start	k1gInSc3	start
<g/>
.	.	kIx.	.
</s>
<s>
Sha	Sha	k?	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
je	být	k5eAaImIp3nS	být
zabita	zabít	k5eAaPmNgFnS	zabít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jackson	Jackson	k1gMnSc1	Jackson
použije	použít	k5eAaPmIp3nS	použít
mechanismus	mechanismus	k1gInSc4	mechanismus
na	na	k7c4	na
zápěstí	zápěstí	k1gNnSc4	zápěstí
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
zabitých	zabitý	k2eAgMnPc2d1	zabitý
strážných	strážný	k1gMnPc2	strážný
<g/>
,	,	kIx,	,
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
transportér	transportér	k1gInSc1	transportér
a	a	k8xC	a
křísí	křísit	k5eAaImIp3nS	křísit
Sha	Sha	k1gFnSc1	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
v	v	k7c6	v
Raově	Raova	k1gFnSc6	Raova
sarkofágu	sarkofág	k1gInSc2	sarkofág
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
mezitím	mezitím	k6eAd1	mezitím
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
poslat	poslat	k5eAaPmF	poslat
bombu	bomba	k1gFnSc4	bomba
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
hlavní	hlavní	k2eAgMnSc1d1	hlavní
strážný	strážný	k1gMnSc1	strážný
se	se	k3xPyFc4	se
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
udělá	udělat	k5eAaPmIp3nS	udělat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
transportní	transportní	k2eAgInPc4d1	transportní
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
k	k	k7c3	k
Hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
bráně	brána	k1gFnSc3	brána
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c6	na
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neila	Neila	k1gMnSc1	Neila
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Jackson	Jacksona	k1gFnPc2	Jacksona
bere	brát	k5eAaImIp3nS	brát
Sha	Sha	k1gFnSc1	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
<g/>
,	,	kIx,	,
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
s	s	k7c7	s
transportními	transportní	k2eAgInPc7d1	transportní
kruhy	kruh	k1gInPc7	kruh
a	a	k8xC	a
tak	tak	k9	tak
tak	tak	k9	tak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
popravě	poprava	k1gFnSc3	poprava
od	od	k7c2	od
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
když	když	k8xS	když
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
přemůže	přemoct	k5eAaPmIp3nS	přemoct
hlavního	hlavní	k2eAgMnSc4d1	hlavní
strážného	strážný	k1gMnSc4	strážný
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
drží	držet	k5eAaImIp3nP	držet
u	u	k7c2	u
země	zem	k1gFnSc2	zem
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
transportních	transportní	k2eAgInPc2d1	transportní
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
ovládací	ovládací	k2eAgInSc1d1	ovládací
mechanismus	mechanismus	k1gInSc1	mechanismus
strážného	strážný	k1gMnSc2	strážný
a	a	k8xC	a
transportuje	transportovat	k5eAaBmIp3nS	transportovat
tak	tak	k9	tak
hlavu	hlava	k1gFnSc4	hlava
strážného	strážný	k1gMnSc2	strážný
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
Jacksona	Jacksona	k1gFnSc1	Jacksona
se	s	k7c7	s
Sha	Sha	k1gFnSc7	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
k	k	k7c3	k
Hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
bráně	brána	k1gFnSc3	brána
<g/>
.	.	kIx.	.
</s>
<s>
Raovo	Raovo	k6eAd1	Raovo
plavidlo	plavidlo	k1gNnSc1	plavidlo
stoupá	stoupat	k5eAaImIp3nS	stoupat
nad	nad	k7c4	nad
pyramidu	pyramida	k1gFnSc4	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
deaktivovat	deaktivovat	k5eAaImF	deaktivovat
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jí	on	k3xPp3gFnSc3	on
dopraví	dopravit	k5eAaPmIp3nS	dopravit
po	po	k7c4	po
mocí	moc	k1gFnSc7	moc
transportních	transportní	k2eAgInPc2d1	transportní
kruhů	kruh	k1gInPc2	kruh
na	na	k7c4	na
Raovu	Raova	k1gFnSc4	Raova
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Bomba	bomba	k1gFnSc1	bomba
exploduje	explodovat	k5eAaBmIp3nS	explodovat
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
Raa	Raa	k1gFnSc1	Raa
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
se	s	k7c7	s
Sha	Sha	k1gFnSc7	Sha
<g/>
'	'	kIx"	'
<g/>
uri	uri	k?	uri
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
se	se	k3xPyFc4	se
zbytkem	zbytek	k1gInSc7	zbytek
týmu	tým	k1gInSc2	tým
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
Hvězdní	hvězdný	k2eAgMnPc1d1	hvězdný
brány	brána	k1gFnPc1	brána
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
