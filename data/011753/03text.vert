<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Falkenburk	Falkenburk	k1gInSc1	Falkenburk
je	být	k5eAaImIp3nS	být
pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
majitel	majitel	k1gMnSc1	majitel
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
panství	panství	k1gNnSc2	panství
Jindřich	Jindřich	k1gMnSc1	Jindřich
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
městečka	městečko	k1gNnSc2	městečko
Jablonné	Jablonné	k1gNnSc4	Jablonné
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
veřejnosti	veřejnost	k1gFnSc3	veřejnost
přístupný	přístupný	k2eAgInSc4d1	přístupný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
stávala	stávat	k5eAaImAgFnS	stávat
zřejmě	zřejmě	k6eAd1	zřejmě
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1450	[number]	k4	1450
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Vlastník	vlastník	k1gMnSc1	vlastník
panství	panství	k1gNnSc2	panství
Jindřich	Jindřich	k1gMnSc1	Jindřich
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1562	[number]	k4	1562
až	až	k9	až
1572	[number]	k4	1572
postavit	postavit	k5eAaPmF	postavit
u	u	k7c2	u
rybníka	rybník	k1gInSc2	rybník
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
rybník	rybník	k1gInSc1	rybník
<g/>
)	)	kIx)	)
kamenný	kamenný	k2eAgInSc1d1	kamenný
zámeček	zámeček	k1gInSc1	zámeček
s	s	k7c7	s
věžičkou	věžička	k1gFnSc7	věžička
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1668	[number]	k4	1668
získal	získat	k5eAaPmAgMnS	získat
panství	panství	k1gNnSc4	panství
Jan	Jan	k1gMnSc1	Jan
Pachta	Pachta	k1gMnSc1	Pachta
z	z	k7c2	z
Rájova	Rájův	k2eAgMnSc2d1	Rájův
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
potomci	potomek	k1gMnPc1	potomek
zámeček	zámeček	k1gInSc4	zámeček
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přestavbu	přestavba	k1gFnSc4	přestavba
prováděl	provádět	k5eAaImAgMnS	provádět
Filip	Filip	k1gMnSc1	Filip
Heger	Heger	k1gMnSc1	Heger
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1759	[number]	k4	1759
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zámek	zámek	k1gInSc1	zámek
patřil	patřit	k5eAaImAgInS	patřit
Janu	Jan	k1gMnSc3	Jan
Jáchymovi	Jáchym	k1gMnSc3	Jáchym
Pachtovi	Pacht	k1gMnSc3	Pacht
z	z	k7c2	z
Rájova	Rájův	k2eAgMnSc2d1	Rájův
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
Hegerem	Heger	k1gMnSc7	Heger
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
oranžérie	oranžérie	k1gFnSc1	oranžérie
zvlněného	zvlněný	k2eAgInSc2d1	zvlněný
půdorysu	půdorys	k1gInSc2	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
opozdilý	opozdilý	k2eAgInSc4d1	opozdilý
projev	projev	k1gInSc4	projev
dynamického	dynamický	k2eAgNnSc2d1	dynamické
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vojenský	vojenský	k2eAgInSc1d1	vojenský
lazaret	lazaret	k1gInSc1	lazaret
a	a	k8xC	a
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
zámek	zámek	k1gInSc1	zámek
zpustl	zpustnout	k5eAaPmAgInS	zpustnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
stavbu	stavba	k1gFnSc4	stavba
koupil	koupit	k5eAaPmAgMnS	koupit
Mořic	Mořic	k1gMnSc1	Mořic
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Liebig	Liebig	k1gMnSc1	Liebig
<g/>
,	,	kIx,	,
liberecký	liberecký	k2eAgMnSc1d1	liberecký
továrník	továrník	k1gMnSc1	továrník
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
přestavěl	přestavět	k5eAaPmAgInS	přestavět
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Přestavby	přestavba	k1gFnPc1	přestavba
jím	on	k3xPp3gNnSc7	on
provedené	provedený	k2eAgInPc1d1	provedený
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
necitlivé	citlivý	k2eNgInPc1d1	necitlivý
a	a	k8xC	a
pseudorokokové	pseudorokokový	k2eAgInPc1d1	pseudorokokový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
i	i	k9	i
s	s	k7c7	s
barokním	barokní	k2eAgInSc7d1	barokní
parkem	park	k1gInSc7	park
veřejnosti	veřejnost	k1gFnSc2	veřejnost
nepřístupný	přístupný	k2eNgInSc1d1	nepřístupný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xS	jako
dětský	dětský	k2eAgInSc1d1	dětský
domov	domov	k1gInSc1	domov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
renovace	renovace	k1gFnSc1	renovace
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístupnost	přístupnost	k1gFnSc4	přístupnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
nepřístupný	přístupný	k2eNgInSc1d1	nepřístupný
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dojít	dojít	k5eAaPmF	dojít
ulicemi	ulice	k1gFnPc7	ulice
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
Jablonného	Jablonné	k1gNnSc2	Jablonné
a	a	k8xC	a
nedaleko	nedaleko	k7c2	nedaleko
silnice	silnice	k1gFnSc2	silnice
od	od	k7c2	od
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
do	do	k7c2	do
Jablonného	Jablonné	k1gNnSc2	Jablonné
a	a	k8xC	a
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k6eAd1	kolem
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
zdejším	zdejší	k2eAgInSc7d1	zdejší
mokřadem	mokřad	k1gInSc7	mokřad
vedena	vést	k5eAaImNgFnS	vést
žlutě	žlutě	k6eAd1	žlutě
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
z	z	k7c2	z
Jablonného	Jablonné	k1gNnSc2	Jablonné
přes	přes	k7c4	přes
Jezevčí	jezevčí	k2eAgInSc4d1	jezevčí
vrch	vrch	k1gInSc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
necelý	celý	k2eNgInSc4d1	necelý
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Jablonné	Jablonná	k1gFnSc2	Jablonná
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
086	[number]	k4	086
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nový	nový	k2eAgInSc4d1	nový
Falkenburk	Falkenburk	k1gInSc4	Falkenburk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Web	web	k1gInSc4	web
Toulky	toulka	k1gFnSc2	toulka
Českem	Česko	k1gNnSc7	Česko
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
Dětského	dětský	k2eAgInSc2d1	dětský
domova	domov	k1gInSc2	domov
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
</s>
</p>
