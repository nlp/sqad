<s>
Nový	nový	k2eAgInSc1d1	nový
Falkenburk	Falkenburk	k1gInSc1	Falkenburk
je	být	k5eAaImIp3nS	být
pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
majitel	majitel	k1gMnSc1	majitel
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
panství	panství	k1gNnSc2	panství
Jindřich	Jindřich	k1gMnSc1	Jindřich
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
