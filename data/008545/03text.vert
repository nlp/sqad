<p>
<s>
Nico	Nico	k6eAd1	Nico
je	být	k5eAaImIp3nS	být
balet	balet	k1gInSc1	balet
velšského	velšský	k2eAgMnSc2d1	velšský
hudebníka	hudebník	k1gMnSc2	hudebník
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
choreografie	choreografie	k1gFnSc2	choreografie
je	být	k5eAaImIp3nS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
choreograf	choreograf	k1gMnSc1	choreograf
Ed	Ed	k1gMnSc1	Ed
Wubbe	Wubb	k1gMnSc5	Wubb
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
předveden	předveden	k2eAgInSc1d1	předveden
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1997	[number]	k4	1997
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Schouwburg	Schouwburg	k1gInSc1	Schouwburg
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
baletním	baletní	k2eAgInSc6d1	baletní
souborem	soubor	k1gInSc7	soubor
Scapino	Scapina	k1gFnSc5	Scapina
Ballet	Ballet	k1gInSc4	Ballet
<g/>
.	.	kIx.	.
</s>
<s>
Cale	Cale	k6eAd1	Cale
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
audiozáznam	audiozáznam	k1gInSc1	audiozáznam
představení	představení	k1gNnPc2	představení
na	na	k7c4	na
albu	alba	k1gFnSc4	alba
Dance	Danka	k1gFnSc3	Danka
Music	Musice	k1gFnPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
Balet	balet	k1gInSc1	balet
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
životem	život	k1gInSc7	život
německé	německý	k2eAgFnSc2d1	německá
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
a	a	k8xC	a
herečky	herečka	k1gFnSc2	herečka
Nico	Nico	k6eAd1	Nico
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
skladatel	skladatel	k1gMnSc1	skladatel
Cale	Cale	k1gNnSc4	Cale
během	během	k7c2	během
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
často	často	k6eAd1	často
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
představení	představení	k1gNnPc2	představení
je	být	k5eAaImIp3nS	být
tanečnice	tanečnice	k1gFnSc1	tanečnice
zabalena	zabalen	k2eAgFnSc1d1	zabalena
do	do	k7c2	do
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
fólie	fólie	k1gFnSc2	fólie
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
svými	svůj	k3xOyFgInPc7	svůj
pohyby	pohyb	k1gInPc7	pohyb
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
objevuje	objevovat	k5eAaImIp3nS	objevovat
více	hodně	k6eAd2	hodně
tanečníků	tanečník	k1gMnPc2	tanečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
