<s>
Pilaf	pilaf	k1gInSc1
</s>
<s>
Pilaf	pilaf	k1gInSc1
Složení	složení	k1gNnSc2
</s>
<s>
rýže	rýže	k1gFnPc1
a	a	k8xC
obilniny	obilnina	k1gFnPc1
<g/>
,	,	kIx,
rostlinný	rostlinný	k2eAgInSc1d1
olej	olej	k1gInSc1
a	a	k8xC
animal	animal	k1gMnSc1
fat	fatum	k1gNnPc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pilaf	pilaf	k1gInSc1
s	s	k7c7
kuřecím	kuřecí	k2eAgNnSc7d1
masem	maso	k1gNnSc7
</s>
<s>
Pilaf	pilaf	k1gInSc1
nebo	nebo	k8xC
také	také	k9
pilav	pilav	k1gInSc4
je	být	k5eAaImIp3nS
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
základ	základ	k1gInSc4
rýže	rýže	k1gFnSc2
nebo	nebo	k8xC
pšenice	pšenice	k1gFnSc2
(	(	kIx(
<g/>
bulgur	bulgur	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
osmaží	osmažit	k5eAaPmIp3nS
na	na	k7c6
oleji	olej	k1gInSc6
a	a	k8xC
potom	potom	k6eAd1
vaří	vařit	k5eAaImIp3nS
v	v	k7c6
kořeněném	kořeněný	k2eAgInSc6d1
bujónu	bujón	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
mnoho	mnoho	k4c4
druhů	druh	k1gInPc2
pilafů	pilaf	k1gInPc2
<g/>
,	,	kIx,
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
v	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
vaří	vařit	k5eAaImIp3nS
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
různé	různý	k2eAgFnSc3d1
příměsi	příměs	k1gFnSc3
–	–	k?
zeleninu	zelenina	k1gFnSc4
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnPc4
<g/>
,	,	kIx,
různé	různý	k2eAgInPc4d1
druhy	druh	k1gInPc4
masa	maso	k1gNnSc2
a	a	k8xC
nesmí	smět	k5eNaImIp3nS
chybět	chybět	k5eAaImF
koření	koření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pilaf	pilaf	k1gInSc1
a	a	k8xC
podobná	podobný	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
obvyklá	obvyklý	k2eAgNnPc4d1
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Středním	střední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
jihoasijské	jihoasijský	k2eAgFnSc6d1
kuchyni	kuchyně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Ázerbájdžánu	Ázerbájdžán	k1gInSc6
je	být	k5eAaImIp3nS
pilaf	pilaf	k1gInSc1
národním	národní	k2eAgNnSc7d1
jídlem	jídlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
často	často	k6eAd1
se	se	k3xPyFc4
vaří	vařit	k5eAaImIp3nS
např.	např.	kA
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
,	,	kIx,
zemích	zem	k1gFnPc6
bývalého	bývalý	k2eAgInSc2d1
SSSR	SSSR	kA
<g/>
,	,	kIx,
na	na	k7c6
Balkáně	Balkán	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
Íránu	Írán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jména	jméno	k1gNnPc1
</s>
<s>
V	v	k7c6
různých	různý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
má	mít	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
názvy	název	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Albánsky	albánsky	k6eAd1
pilaf	pilaf	k1gInSc1
</s>
<s>
Arménsky	arménsky	k6eAd1
փ	փ	k?
</s>
<s>
Ázerbájdžánsky	ázerbájdžánsky	k6eAd1
plov	plovat	k5eAaImRp2nS
</s>
<s>
Bosensky	bosensky	k6eAd1
pilav	pilav	k1gInSc1
</s>
<s>
Řecky	řecky	k6eAd1
π	π	k?
</s>
<s>
Hindsky	hindsky	k6eAd1
प	प	k?
<g/>
ु	ु	k?
<g/>
ल	ल	k?
<g/>
ा	ा	k?
<g/>
व	व	k?
</s>
<s>
Kazašsky	kazašsky	k6eAd1
п	п	k?
</s>
<s>
Makedonsky	makedonsky	k6eAd1
П	П	k?
(	(	kIx(
<g/>
Pilav	pilav	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
palaw	palaw	k?
<g/>
)	)	kIx)
</s>
<s>
Mandarinsky	Mandarinsky	k6eAd1
抓	抓	k?
(	(	kIx(
<g/>
zhua	zhua	k1gFnSc1
fan	fana	k1gFnPc2
<g/>
)	)	kIx)
<g/>
'	'	kIx"
</s>
<s>
Urdsky	urdsky	k6eAd1
pulao	pulao	k6eAd1
</s>
<s>
Persky	persky	k6eAd1
polow	polow	k?
</s>
<s>
Rumunsky	rumunsky	k6eAd1
pilaf	pilaf	k1gInSc1
</s>
<s>
Rusky	Ruska	k1gFnPc1
"	"	kIx"
<g/>
п	п	k?
<g/>
"	"	kIx"
plov	plovat	k5eAaImRp2nS
</s>
<s>
Srbsky	srbsky	k6eAd1
pilav	pilav	k1gInSc1
</s>
<s>
Turecky	turecky	k6eAd1
pilav	pilav	k1gInSc1
</s>
<s>
Tádžicky	tádžicky	k6eAd1
п	п	k?
-	-	kIx~
palav	palav	k1gInSc1
[	[	kIx(
<g/>
palau	palau	k5eAaPmIp1nS
<g/>
]	]	kIx)
<g/>
'	'	kIx"
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
pelau	pelau	k5eAaPmIp1nS
</s>
<s>
Uzbecky	uzbecky	k6eAd1
п	п	k?
palov	palov	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Persky	persky	k6eAd1
پ	پ	k?
-	-	kIx~
polō	polō	k?
</s>
<s>
Turkmensky	turkmensky	k6eAd1
palov	palov	k1gInSc1
</s>
<s>
Turkicky	turkicky	k6eAd1
pilav	pilav	k1gInSc1
</s>
<s>
Krymskotatarsky	krymskotatarsky	k6eAd1
pilâv	pilâv	k1gInSc1
</s>
<s>
Tatarsky	tatarsky	k6eAd1
pı	pı	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pilaf	pilaf	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Recepty	recept	k1gInPc1
na	na	k7c4
ázerbájdžánský	ázerbájdžánský	k2eAgInSc4d1
pilaf	pilaf	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Recepty	recept	k1gInPc1
na	na	k7c4
turecký	turecký	k2eAgInSc4d1
pilaf	pilaf	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
