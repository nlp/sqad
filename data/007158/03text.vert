<s>
Právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
multidimenzionální	multidimenzionální	k2eAgInSc1d1	multidimenzionální
fenomén	fenomén	k1gInSc1	fenomén
a	a	k8xC	a
vícevýznamový	vícevýznamový	k2eAgInSc1d1	vícevýznamový
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
definovat	definovat	k5eAaBmF	definovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
jeho	jeho	k3xOp3gFnPc6	jeho
rovinách	rovina	k1gFnPc6	rovina
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
aspektech	aspekt	k1gInPc6	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jeho	jeho	k3xOp3gFnSc4	jeho
definici	definice	k1gFnSc4	definice
tedy	tedy	k8xC	tedy
zúžit	zúžit	k5eAaPmF	zúžit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
normativní	normativní	k2eAgInSc4d1	normativní
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
především	především	k9	především
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rovině	rovina	k1gFnSc6	rovina
je	být	k5eAaImIp3nS	být
chápán	chápat	k5eAaImNgInS	chápat
nejčastěji	často	k6eAd3	často
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
soustavy	soustava	k1gFnSc2	soustava
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pravidel	pravidlo	k1gNnPc2	pravidlo
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
příkazů	příkaz	k1gInPc2	příkaz
<g/>
,	,	kIx,	,
zákazů	zákaz	k1gInPc2	zákaz
nebo	nebo	k8xC	nebo
dovolení	dovolení	k1gNnPc2	dovolení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
lidské	lidský	k2eAgNnSc1d1	lidské
spolužití	spolužití	k1gNnSc1	spolužití
a	a	k8xC	a
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uznávané	uznávaný	k2eAgFnPc1d1	uznávaná
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
stanovené	stanovený	k2eAgInPc1d1	stanovený
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
oblasti	oblast	k1gFnSc3	oblast
nejlépe	dobře	k6eAd3	dobře
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pojem	pojem	k1gInSc1	pojem
objektivního	objektivní	k2eAgNnSc2d1	objektivní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
rovinu	rovina	k1gFnSc4	rovina
normativní	normativní	k2eAgFnSc4d1	normativní
působí	působit	k5eAaImIp3nP	působit
právo	právo	k1gNnSc4	právo
též	též	k9	též
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
sociální	sociální	k2eAgFnSc2d1	sociální
<g/>
,	,	kIx,	,
axiologické	axiologický	k2eAgFnSc2d1	axiologická
<g/>
,	,	kIx,	,
mocenské	mocenský	k2eAgFnSc2d1	mocenská
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
<g/>
.	.	kIx.	.
</s>
<s>
Objektivní	objektivní	k2eAgNnSc1d1	objektivní
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
univerzálním	univerzální	k2eAgInSc7d1	univerzální
normativním	normativní	k2eAgInSc7d1	normativní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
mohou	moct	k5eAaImIp3nP	moct
stávat	stávat	k5eAaImF	stávat
i	i	k9	i
normy	norma	k1gFnPc4	norma
pocházející	pocházející	k2eAgFnPc4d1	pocházející
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
normativních	normativní	k2eAgInPc2d1	normativní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
systémy	systém	k1gInPc4	systém
s	s	k7c7	s
hodnotovým	hodnotový	k2eAgInSc7d1	hodnotový
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
technického	technický	k2eAgInSc2d1	technický
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nedodržování	nedodržování	k1gNnSc2	nedodržování
práva	právo	k1gNnPc4	právo
ho	on	k3xPp3gMnSc4	on
stát	stát	k1gInSc1	stát
mocensky	mocensky	k6eAd1	mocensky
vynucuje	vynucovat	k5eAaImIp3nS	vynucovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
souhrnu	souhrn	k1gInSc6	souhrn
všechny	všechen	k3xTgFnPc1	všechen
platné	platný	k2eAgFnPc1d1	platná
právní	právní	k2eAgFnPc1d1	právní
normy	norma	k1gFnPc1	norma
tvoří	tvořit	k5eAaImIp3nP	tvořit
právní	právní	k2eAgInSc4d1	právní
řád	řád	k1gInSc4	řád
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
slovo	slovo	k1gNnSc1	slovo
právo	právo	k1gNnSc4	právo
má	mít	k5eAaImIp3nS	mít
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
<g/>
)	)	kIx)	)
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
významy	význam	k1gInPc4	význam
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
právníci	právník	k1gMnPc1	právník
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
také	také	k9	také
pro	pro	k7c4	pro
subjektivní	subjektivní	k2eAgNnSc4d1	subjektivní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
určitého	určitý	k2eAgInSc2d1	určitý
subjektu	subjekt	k1gInSc2	subjekt
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c6	o
objektivním	objektivní	k2eAgMnSc6d1	objektivní
právem	právem	k6eAd1	právem
zaručenou	zaručený	k2eAgFnSc4d1	zaručená
možnost	možnost	k1gFnSc4	možnost
chovat	chovat	k5eAaImF	chovat
se	se	k3xPyFc4	se
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
právní	právní	k2eAgFnSc1d1	právní
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
znalosti	znalost	k1gFnPc1	znalost
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
představy	představ	k1gInPc1	představ
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
platnosti	platnost	k1gFnSc6	platnost
a	a	k8xC	a
oprávněnosti	oprávněnost	k1gFnSc6	oprávněnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
právní	právní	k2eAgNnSc4d1	právní
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
práva	právo	k1gNnSc2	právo
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
vývoje	vývoj	k1gInSc2	vývoj
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
právo	právo	k1gNnSc1	právo
bylo	být	k5eAaImAgNnS	být
právo	právo	k1gNnSc4	právo
zvykové	zvykový	k2eAgFnSc2d1	zvyková
(	(	kIx(	(
<g/>
obyčejové	obyčejový	k2eAgFnSc2d1	obyčejová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existovalo	existovat	k5eAaImAgNnS	existovat
ve	v	k7c6	v
vědomí	vědomí	k1gNnSc6	vědomí
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nepsané	psaný	k2eNgNnSc4d1	nepsané
právo	právo	k1gNnSc4	právo
předávané	předávaný	k2eAgNnSc4d1	předávané
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
písemných	písemný	k2eAgInPc2d1	písemný
předpisů	předpis	k1gInPc2	předpis
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgNnSc1d1	pozitivní
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
platné	platný	k2eAgNnSc1d1	platné
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gInSc4	on
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
platných	platný	k2eAgFnPc2d1	platná
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
vyjádřených	vyjádřený	k2eAgFnPc2d1	vyjádřená
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nějakého	nějaký	k3yIgInSc2	nějaký
právního	právní	k2eAgInSc2d1	právní
předpisu	předpis	k1gInSc2	předpis
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
přirozené	přirozený	k2eAgNnSc1d1	přirozené
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
odvozováno	odvozovat	k5eAaImNgNnS	odvozovat
z	z	k7c2	z
mravního	mravní	k2eAgInSc2d1	mravní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
vysněné	vysněný	k2eAgNnSc4d1	vysněné
<g/>
,	,	kIx,	,
ideální	ideální	k2eAgNnSc4d1	ideální
a	a	k8xC	a
nejdokonalejší	dokonalý	k2eAgNnSc4d3	nejdokonalejší
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Chammurapiho	Chammurapize	k6eAd1	Chammurapize
zákoník	zákoník	k1gInSc1	zákoník
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
právních	právní	k2eAgInPc2d1	právní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cca	cca	kA	cca
1800	[number]	k4	1800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
kamenná	kamenný	k2eAgFnSc1d1	kamenná
deska	deska	k1gFnSc1	deska
vystavená	vystavený	k2eAgFnSc1d1	vystavená
na	na	k7c6	na
veřejném	veřejný	k2eAgNnSc6d1	veřejné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
rodinné	rodinný	k2eAgMnPc4d1	rodinný
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgMnPc4d1	obchodní
a	a	k8xC	a
především	především	k6eAd1	především
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
známý	známý	k2eAgInSc1d1	známý
historický	historický	k2eAgInSc1d1	historický
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
římský	římský	k2eAgInSc1d1	římský
Zákon	zákon	k1gInSc1	zákon
dvanácti	dvanáct	k4xCc2	dvanáct
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
souhrn	souhrn	k1gInSc1	souhrn
práva	právo	k1gNnSc2	právo
soukromého	soukromý	k2eAgNnSc2d1	soukromé
i	i	k8xC	i
trestního	trestní	k2eAgNnSc2d1	trestní
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bronzových	bronzový	k2eAgFnPc2d1	bronzová
desek	deska	k1gFnPc2	deska
vystavených	vystavený	k2eAgFnPc2d1	vystavená
také	také	k9	také
na	na	k7c6	na
veřejném	veřejný	k2eAgNnSc6d1	veřejné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
sudnyj	sudnyj	k1gInSc1	sudnyj
ljudem	ljud	k1gInSc7	ljud
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
laiky	laik	k1gMnPc4	laik
<g/>
,	,	kIx,	,
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Konstantinem	Konstantin	k1gMnSc7	Konstantin
a	a	k8xC	a
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nechal	nechat	k5eAaPmAgMnS	nechat
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
sepsat	sepsat	k5eAaPmF	sepsat
Horní	horní	k2eAgInSc4d1	horní
zákoník	zákoník	k1gInSc4	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Magna	Magen	k2eAgFnSc1d1	Magna
charta	charta	k1gFnSc1	charta
libertatum	libertatum	k1gNnSc4	libertatum
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
anglický	anglický	k2eAgInSc1d1	anglický
právní	právní	k2eAgInSc1d1	právní
dokument	dokument	k1gInSc1	dokument
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1215	[number]	k4	1215
<g/>
,	,	kIx,	,
omezoval	omezovat	k5eAaImAgMnS	omezovat
některé	některý	k3yIgFnPc4	některý
panovnické	panovnický	k2eAgFnPc4d1	panovnická
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgNnSc1d1	pozitivní
právo	právo	k1gNnSc1	právo
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
především	především	k9	především
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
<g/>
,	,	kIx,	,
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
právo	právo	k1gNnSc1	právo
jako	jako	k8xS	jako
takové	takový	k3xDgNnSc1	takový
a	a	k8xC	a
morálka	morálka	k1gFnSc1	morálka
jako	jako	k8xS	jako
mimoprávní	mimoprávní	k2eAgInSc1d1	mimoprávní
normativní	normativní	k2eAgInSc1d1	normativní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
u	u	k7c2	u
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
práva	právo	k1gNnSc2	právo
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
původci	původce	k1gMnPc7	původce
práva	právo	k1gNnSc2	právo
Bůh	bůh	k1gMnSc1	bůh
nebo	nebo	k8xC	nebo
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
může	moct	k5eAaImIp3nS	moct
uznávat	uznávat	k5eAaImF	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xS	jako
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
objektivního	objektivní	k2eAgNnSc2d1	objektivní
práva	právo	k1gNnSc2	právo
pochází	pocházet	k5eAaImIp3nS	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
práva	právo	k1gNnSc2	právo
římského	římský	k2eAgNnSc2d1	římské
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odlišovalo	odlišovat	k5eAaImAgNnS	odlišovat
především	především	k9	především
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
zájem	zájem	k1gInSc4	zájem
chrání	chránit	k5eAaImIp3nS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zájem	zájem	k1gInSc4	zájem
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
právo	právo	k1gNnSc1	právo
veřejné	veřejný	k2eAgNnSc1d1	veřejné
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
právo	právo	k1gNnSc4	právo
soukromé	soukromý	k2eAgNnSc4d1	soukromé
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k9	ale
více	hodně	k6eAd2	hodně
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
právo	právo	k1gNnSc4	právo
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
jeho	jeho	k3xOp3gMnPc2	jeho
adresátů	adresát	k1gMnPc2	adresát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovni	roveň	k1gMnPc1	roveň
a	a	k8xC	a
vzájemná	vzájemný	k2eAgNnPc1d1	vzájemné
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc1	povinnost
si	se	k3xPyFc3	se
stanoví	stanovit	k5eAaPmIp3nP	stanovit
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
soukromé	soukromý	k2eAgNnSc4d1	soukromé
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
nadřízeném	nadřízený	k2eAgInSc6d1	nadřízený
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnSc2	povinnost
může	moct	k5eAaImIp3nS	moct
stanovit	stanovit	k5eAaPmF	stanovit
ostatním	ostatní	k1gNnSc7	ostatní
i	i	k8xC	i
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
veřejné	veřejný	k2eAgNnSc4d1	veřejné
<g/>
.	.	kIx.	.
</s>
<s>
Objektivní	objektivní	k2eAgNnSc1d1	objektivní
právo	právo	k1gNnSc1	právo
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
územní	územní	k2eAgFnSc2d1	územní
platnosti	platnost	k1gFnSc2	platnost
na	na	k7c4	na
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
na	na	k7c4	na
hmotné	hmotný	k2eAgNnSc4d1	hmotné
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
procesní	procesní	k2eAgNnSc4d1	procesní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
někým	někdo	k3yInSc7	někdo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
zvnějšku	zvnějšku	k6eAd1	zvnějšku
(	(	kIx(	(
<g/>
právo	právo	k1gNnSc4	právo
heteronomní	heteronomní	k2eAgFnSc2d1	heteronomní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
účastníci	účastník	k1gMnPc1	účastník
právního	právní	k2eAgInSc2d1	právní
vztahu	vztah	k1gInSc2	vztah
stanoví	stanovit	k5eAaPmIp3nP	stanovit
sami	sám	k3xTgMnPc1	sám
(	(	kIx(	(
<g/>
právo	právo	k1gNnSc4	právo
autonomní	autonomní	k2eAgFnSc2d1	autonomní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Právní	právní	k2eAgNnSc1d1	právní
odvětví	odvětví	k1gNnSc1	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
platných	platný	k2eAgFnPc2d1	platná
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
teoreticky	teoreticky	k6eAd1	teoreticky
rozdělit	rozdělit	k5eAaPmF	rozdělit
i	i	k9	i
na	na	k7c4	na
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
právní	právní	k2eAgNnPc4d1	právní
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
splňují	splňovat	k5eAaImIp3nP	splňovat
tzv.	tzv.	kA	tzv.
odvětvotvorná	odvětvotvorný	k2eAgNnPc1d1	odvětvotvorný
kritéria	kritérion	k1gNnPc1	kritérion
(	(	kIx(	(
<g/>
existence	existence	k1gFnSc2	existence
společných	společný	k2eAgInPc2d1	společný
institutů	institut	k1gInPc2	institut
a	a	k8xC	a
právních	právní	k2eAgInPc2d1	právní
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
systémová	systémový	k2eAgFnSc1d1	systémová
soudržnost	soudržnost	k1gFnSc1	soudržnost
a	a	k8xC	a
akceptovatelnost	akceptovatelnost	k1gFnSc1	akceptovatelnost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
odborné	odborný	k2eAgFnSc2d1	odborná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
právu	právo	k1gNnSc6	právo
jsou	být	k5eAaImIp3nP	být
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
právními	právní	k2eAgMnPc7d1	právní
odvětvími	odvětví	k1gNnPc7	odvětví
občanské	občanský	k2eAgNnSc1d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
rodinné	rodinný	k2eAgNnSc4d1	rodinné
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc4d1	obchodní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
lze	lze	k6eAd1	lze
sem	sem	k6eAd1	sem
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
pracovní	pracovní	k2eAgNnSc4d1	pracovní
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
veřejného	veřejný	k2eAgNnSc2d1	veřejné
práva	právo	k1gNnSc2	právo
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc4d1	správní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
finanční	finanční	k2eAgNnSc4d1	finanční
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Právní	právní	k2eAgFnSc1d1	právní
norma	norma	k1gFnSc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
závazná	závazný	k2eAgNnPc1d1	závazné
pravidla	pravidlo	k1gNnPc1	pravidlo
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
dodržování	dodržování	k1gNnSc1	dodržování
je	být	k5eAaImIp3nS	být
vynutitelné	vynutitelný	k2eAgNnSc1d1	vynutitelné
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnPc4d1	právní
normy	norma	k1gFnPc4	norma
přijímá	přijímat	k5eAaImIp3nS	přijímat
normotvůrce	normotvůrec	k1gInPc4	normotvůrec
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
parlament	parlament	k1gInSc1	parlament
jako	jako	k8xS	jako
subjekt	subjekt	k1gInSc1	subjekt
nadaný	nadaný	k2eAgInSc1d1	nadaný
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mj.	mj.	kA	mj.
na	na	k7c4	na
<g/>
:	:	kIx,	:
zavazující	zavazující	k2eAgNnSc4d1	zavazující
přikazující	přikazující	k2eAgNnSc4d1	přikazující
–	–	k?	–
stanoví	stanovit	k5eAaPmIp3nS	stanovit
povinnost	povinnost	k1gFnSc4	povinnost
nějak	nějak	k6eAd1	nějak
jednat	jednat	k5eAaImF	jednat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
platit	platit	k5eAaImF	platit
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
zakazující	zakazující	k2eAgInSc4d1	zakazující
–	–	k?	–
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
činit	činit	k5eAaImF	činit
určité	určitý	k2eAgInPc1d1	určitý
kroky	krok	k1gInPc1	krok
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uzavírat	uzavírat	k5eAaPmF	uzavírat
kartelové	kartelový	k2eAgFnPc4d1	kartelová
dohody	dohoda	k1gFnPc4	dohoda
<g/>
.	.	kIx.	.
opravňující	opravňující	k2eAgInSc4d1	opravňující
–	–	k?	–
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
zachovat	zachovat	k5eAaPmF	zachovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uzavřít	uzavřít	k5eAaPmF	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
upravují	upravovat	k5eAaImIp3nP	upravovat
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
společenských	společenský	k2eAgInPc2d1	společenský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
právní	právní	k2eAgInSc1d1	právní
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pramen	pramen	k1gInSc1	pramen
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnPc1d1	právní
normy	norma	k1gFnPc1	norma
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
druzích	druh	k1gInPc6	druh
pramenů	pramen	k1gInPc2	pramen
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
pramenů	pramen	k1gInPc2	pramen
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
materiální	materiální	k2eAgFnSc6d1	materiální
a	a	k8xC	a
formální	formální	k2eAgFnSc6d1	formální
<g/>
.	.	kIx.	.
</s>
<s>
Materiálními	materiální	k2eAgInPc7d1	materiální
prameny	pramen	k1gInPc7	pramen
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
reálné	reálný	k2eAgFnPc1d1	reálná
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
společenské	společenský	k2eAgFnSc2d1	společenská
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
poměry	poměra	k1gFnSc2	poměra
dané	daný	k2eAgFnSc2d1	daná
společnosti	společnost	k1gFnSc2	společnost
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc2	její
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
určité	určitý	k2eAgFnSc2d1	určitá
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Formálními	formální	k2eAgInPc7d1	formální
prameny	pramen	k1gInPc7	pramen
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
ty	ten	k3xDgInPc1	ten
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
právo	právo	k1gNnSc4	právo
přímo	přímo	k6eAd1	přímo
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
nepsané	nepsaný	k2eAgNnSc1d1	nepsaný
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
ius	ius	k?	ius
non	non	k?	non
scriptum	scriptum	k1gNnSc4	scriptum
<g/>
)	)	kIx)	)
–	–	k?	–
právní	právní	k2eAgInPc4d1	právní
obyčeje	obyčej	k1gInPc4	obyčej
a	a	k8xC	a
právní	právní	k2eAgInPc4d1	právní
principy	princip	k1gInPc4	princip
<g/>
.	.	kIx.	.
psané	psaný	k2eAgNnSc4d1	psané
právo	právo	k1gNnSc4	právo
(	(	kIx(	(
<g/>
ius	ius	k?	ius
scriptum	scriptum	k1gNnSc1	scriptum
<g/>
)	)	kIx)	)
–	–	k?	–
především	především	k9	především
normativní	normativní	k2eAgInPc4d1	normativní
právní	právní	k2eAgInPc4d1	právní
akty	akt	k1gInPc4	akt
čili	čili	k8xC	čili
právní	právní	k2eAgInPc1d1	právní
předpisy	předpis	k1gInPc1	předpis
(	(	kIx(	(
<g/>
publikované	publikovaný	k2eAgNnSc1d1	publikované
zejména	zejména	k9	zejména
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
normativní	normativní	k2eAgFnPc4d1	normativní
smlouvy	smlouva	k1gFnPc4	smlouva
či	či	k8xC	či
soudní	soudní	k2eAgInPc4d1	soudní
precedenty	precedent	k1gInPc4	precedent
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejčastějšího	častý	k2eAgInSc2d3	nejčastější
pramene	pramen	k1gInSc2	pramen
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
právní	právní	k2eAgInSc1d1	právní
předpis	předpis	k1gInSc1	předpis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gFnSc4	jeho
platnost	platnost	k1gFnSc4	platnost
–	–	k?	–
předpis	předpis	k1gInSc1	předpis
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
legisvakanční	legisvakanční	k2eAgFnSc1d1	legisvakanční
lhůta	lhůta	k1gFnSc1	lhůta
–	–	k?	–
období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
platností	platnost	k1gFnSc7	platnost
a	a	k8xC	a
účinností	účinnost	k1gFnSc7	účinnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
účinnost	účinnost	k1gFnSc1	účinnost
–	–	k?	–
nastává	nastávat	k5eAaImIp3nS	nastávat
povinnost	povinnost	k1gFnSc4	povinnost
se	s	k7c7	s
předpisem	předpis	k1gInSc7	předpis
řídit	řídit	k5eAaImF	řídit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
působnost	působnost	k1gFnSc1	působnost
–	–	k?	–
územní	územní	k2eAgFnSc1d1	územní
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
<g/>
,	,	kIx,	,
časová	časový	k2eAgFnSc1d1	časová
i	i	k8xC	i
věcná	věcný	k2eAgFnSc1d1	věcná
<g/>
.	.	kIx.	.
</s>
