<p>
<s>
Úrodnost	úrodnost	k1gFnSc1	úrodnost
je	být	k5eAaImIp3nS	být
vlastnost	vlastnost	k1gFnSc4	vlastnost
půdy	půda	k1gFnSc2	půda
nebo	nebo	k8xC	nebo
daného	daný	k2eAgNnSc2d1	dané
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
půdy	půda	k1gFnSc2	půda
zajistit	zajistit	k5eAaPmF	zajistit
rostlinám	rostlina	k1gFnPc3	rostlina
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
množství	množství	k1gNnSc1	množství
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
vláhy	vláha	k1gFnSc2	vláha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úrodná	úrodný	k2eAgFnSc1d1	úrodná
půda	půda	k1gFnSc1	půda
==	==	k?	==
</s>
</p>
<p>
<s>
Úrodnost	úrodnost	k1gFnSc4	úrodnost
půdy	půda	k1gFnSc2	půda
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
zejména	zejména	k9	zejména
tyto	tento	k3xDgInPc1	tento
ukazatele	ukazatel	k1gInPc1	ukazatel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
množství	množství	k1gNnSc1	množství
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
stopových	stopový	k2eAgInPc2d1	stopový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
kobalt	kobalt	k1gInSc1	kobalt
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dostatek	dostatek	k1gInSc1	dostatek
humusu	humus	k1gInSc2	humus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
strukturu	struktura	k1gFnSc4	struktura
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
zadržet	zadržet	k5eAaPmF	zadržet
vláhu	vláha	k1gFnSc4	vláha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pH	ph	kA	ph
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
6,0	[number]	k4	6,0
do	do	k7c2	do
6,8	[number]	k4	6,8
</s>
</p>
<p>
<s>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
struktura	struktura	k1gFnSc1	struktura
půdy	půda	k1gFnSc2	půda
–	–	k?	–
provzdušnění	provzdušnění	k1gNnSc1	provzdušnění
</s>
</p>
<p>
<s>
Půdní	půdní	k2eAgInPc1d1	půdní
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
růst	růst	k1gInSc4	růst
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
symbiotičtí	symbiotický	k2eAgMnPc1d1	symbiotický
vazači	vazač	k1gMnPc1	vazač
dusíku	dusík	k1gInSc2	dusík
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Fertility	fertilita	k1gFnSc2	fertilita
(	(	kIx(	(
<g/>
soil	soil	k1gMnSc1	soil
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hnojení	hnojení	k1gNnSc1	hnojení
</s>
</p>
<p>
<s>
Úrodný	úrodný	k2eAgInSc1d1	úrodný
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
</s>
</p>
