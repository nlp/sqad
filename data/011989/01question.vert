<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vlastnost	vlastnost	k1gFnSc1	vlastnost
půdy	půda	k1gFnSc2	půda
nebo	nebo	k8xC	nebo
daného	daný	k2eAgNnSc2d1	dané
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
?	?	kIx.	?
</s>
