<s>
Biuretová	Biuretový	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
je	být	k5eAaImIp3nS
reakce	reakce	k1gFnPc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
dokazuje	dokazovat	k5eAaImIp3nS
bílkovina	bílkovina	k1gFnSc1
pomocí	pomocí	k7c2
směsi	směs	k1gFnSc2
roztoků	roztok	k1gInPc2
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgMnSc2d1
NaOH	NaOH	k1gMnSc2
a	a	k8xC
síranu	síran	k1gInSc2
měďnatého	měďnatý	k2eAgInSc2d1
CuSO	CuSO	k1gFnPc7
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>