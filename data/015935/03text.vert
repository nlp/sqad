<s>
Biuretová	Biuretový	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
</s>
<s>
Biuretová	Biuretový	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
je	být	k5eAaImIp3nS
reakce	reakce	k1gFnPc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
dokazuje	dokazovat	k5eAaImIp3nS
bílkovina	bílkovina	k1gFnSc1
pomocí	pomocí	k7c2
směsi	směs	k1gFnSc2
roztoků	roztok	k1gInPc2
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgMnSc2d1
NaOH	NaOH	k1gMnSc2
a	a	k8xC
síranu	síran	k1gInSc2
měďnatého	měďnatý	k2eAgInSc2d1
CuSO	CuSO	k1gFnPc7
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílkovina	bílkovina	k1gFnSc1
se	se	k3xPyFc4
při	při	k7c6
důkazu	důkaz	k1gInSc6
zbarví	zbarvit	k5eAaPmIp3nS
modrofialově	modrofialově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Biuretovou	Biuretový	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
dokážeme	dokázat	k5eAaPmIp1nP
peptidové	peptidový	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
<g/>
,	,	kIx,
kterými	který	k3yIgFnPc7,k3yRgFnPc7,k3yQgFnPc7
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
váží	vážit	k5eAaImIp3nP
aminokyseliny	aminokyselina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
tvoří	tvořit	k5eAaImIp3nP
v	v	k7c6
alkalickém	alkalický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
se	s	k7c7
solemi	sůl	k1gFnPc7
mědi	měď	k1gFnSc2
charakteristicky	charakteristicky	k6eAd1
barevný	barevný	k2eAgInSc1d1
komplex	komplex	k1gInSc1
(	(	kIx(
<g/>
navzdory	navzdory	k7c3
názvu	název	k1gInSc3
však	však	k9
při	při	k7c6
reakci	reakce	k1gFnSc6
sloučenina	sloučenina	k1gFnSc1
biuret	biuret	k1gInSc4
<g/>
,	,	kIx,
HN	HN	kA
<g/>
(	(	kIx(
<g/>
CONH	CONH	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
nevzniká	vznikat	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
zkumavky	zkumavka	k1gFnSc2
dáme	dát	k5eAaPmIp1nP
cca	cca	kA
2	#num#	k4
ml	ml	kA
odfiltrovaného	odfiltrovaný	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
<g/>
,	,	kIx,
přidáme	přidat	k5eAaPmIp1nP
cca	cca	kA
10	#num#	k4
<g/>
%	%	kIx~
roztok	roztok	k1gInSc1
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
<g/>
,	,	kIx,
min	min	kA
1	#num#	k4
ml	ml	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
směs	směs	k1gFnSc1
silně	silně	k6eAd1
zalkalizovala	zalkalizovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
přidáváme	přidávat	k5eAaImIp1nP
po	po	k7c6
kapkách	kapka	k1gFnPc6
roztok	roztoka	k1gFnPc2
síranu	síran	k1gInSc2
měďnatého	měďnatý	k2eAgInSc2d1
a	a	k8xC
lehce	lehko	k6eAd1
promícháme	promíchat	k5eAaPmIp1nP
skleněnou	skleněný	k2eAgFnSc7d1
tyčinkou	tyčinka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
vložíme	vložit	k5eAaPmIp1nP
do	do	k7c2
zkumavky	zkumavka	k1gFnSc2
a	a	k8xC
začneme	začít	k5eAaPmIp1nP
pozvolna	pozvolna	k6eAd1
zahřívat	zahřívat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozorované	pozorovaný	k2eAgFnPc4d1
změny	změna	k1gFnPc4
zapíšeme	zapsat	k5eAaPmIp1nP
<g/>
,	,	kIx,
reakcí	reakce	k1gFnSc7
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
vznikat	vznikat	k5eAaImF
biuret	biuret	k5eAaImF,k5eAaBmF,k5eAaPmF
odlišné	odlišný	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
xantoproteinová	xantoproteinový	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kniha	kniha	k1gFnSc1
Chemické	chemický	k2eAgInPc1d1
pokusy	pokus	k1gInPc7
<g/>
/	/	kIx~
<g/>
Důkaz	důkaz	k1gInSc1
bílkovin	bílkovina	k1gFnPc2
ve	v	k7c6
Wikiknihách	Wikiknih	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
