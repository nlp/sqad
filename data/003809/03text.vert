<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Příbor	Příbor	k1gInSc1	Příbor
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Sigismund	Sigismunda	k1gFnPc2	Sigismunda
Šlomo	Šloma	k1gFnSc5	Šloma
Freud	Freud	k1gInSc4	Freud
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
lékař-neurolog	lékařeurolog	k1gMnSc1	lékař-neurolog
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
moravském	moravský	k2eAgInSc6d1	moravský
Příboře	Příbor	k1gInSc6	Příbor
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgFnSc6d1	mluvící
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
pocházející	pocházející	k2eAgFnSc6d1	pocházející
z	z	k7c2	z
Haliče	Halič	k1gFnSc2	Halič
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
takřka	takřka	k6eAd1	takřka
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
uchýlil	uchýlit	k5eAaPmAgInS	uchýlit
před	před	k7c7	před
nastupujícím	nastupující	k2eAgInSc7d1	nastupující
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
psychoterapeutickou	psychoterapeutický	k2eAgFnSc4d1	psychoterapeutická
metodu	metoda	k1gFnSc4	metoda
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
volných	volný	k2eAgFnPc6d1	volná
asociacích	asociace	k1gFnPc6	asociace
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc1	vytvoření
přenosového	přenosový	k2eAgInSc2d1	přenosový
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
na	na	k7c4	na
interpretaci	interpretace	k1gFnSc4	interpretace
jeho	jeho	k3xOp3gFnPc2	jeho
promluv	promluva	k1gFnPc2	promluva
<g/>
,	,	kIx,	,
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
přenosových	přenosový	k2eAgFnPc2d1	přenosová
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
odporu	odpor	k1gInSc2	odpor
během	během	k7c2	během
terapie	terapie	k1gFnSc2	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
této	tento	k3xDgFnSc2	tento
terapeutické	terapeutický	k2eAgFnSc2d1	terapeutická
techniky	technika	k1gFnSc2	technika
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
teoretický	teoretický	k2eAgInSc1d1	teoretický
systém	systém	k1gInSc1	systém
popisující	popisující	k2eAgFnSc2d1	popisující
člověka	člověk	k1gMnSc4	člověk
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psychologického	psychologický	k2eAgNnSc2d1	psychologické
<g/>
,	,	kIx,	,
filozofického	filozofický	k2eAgNnSc2d1	filozofické
i	i	k8xC	i
antropologického	antropologický	k2eAgNnSc2d1	antropologické
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
pudová	pudový	k2eAgFnSc1d1	pudová
teorie	teorie	k1gFnSc1	teorie
(	(	kIx(	(
<g/>
libido	libido	k1gNnSc1	libido
<g/>
,	,	kIx,	,
vývojová	vývojový	k2eAgNnPc4d1	vývojové
stadia	stadion	k1gNnPc4	stadion
libida	libido	k1gNnSc2	libido
<g/>
,	,	kIx,	,
narcismus	narcismus	k1gInSc1	narcismus
<g/>
,	,	kIx,	,
pud	pud	k1gInSc1	pud
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc1	první
topika	topika	k1gFnSc1	topika
(	(	kIx(	(
<g/>
přání	přání	k1gNnSc1	přání
<g/>
,	,	kIx,	,
výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
,	,	kIx,	,
předvědomí	předvědomý	k2eAgMnPc1d1	předvědomý
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
druhá	druhý	k4xOgFnSc1	druhý
topika	topika	k1gFnSc1	topika
(	(	kIx(	(
<g/>
ego	ego	k1gNnSc1	ego
<g/>
,	,	kIx,	,
superego	superego	k1gMnSc1	superego
a	a	k8xC	a
id	idy	k1gFnPc2	idy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
věnoval	věnovat	k5eAaImAgMnS	věnovat
též	též	k6eAd1	též
kultuře	kultura	k1gFnSc3	kultura
a	a	k8xC	a
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejcitovanějším	citovaný	k2eAgMnSc7d3	nejcitovanější
psychologem	psycholog	k1gMnSc7	psycholog
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
předci	předek	k1gMnPc1	předek
žili	žít	k5eAaImAgMnP	žít
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
utekli	utéct	k5eAaPmAgMnP	utéct
před	před	k7c7	před
pogromem	pogrom	k1gInSc7	pogrom
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
Litvy	Litva	k1gFnSc2	Litva
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
do	do	k7c2	do
Haliče	Halič	k1gFnSc2	Halič
a	a	k8xC	a
poté	poté	k6eAd1	poté
zakotvili	zakotvit	k5eAaPmAgMnP	zakotvit
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
původní	původní	k2eAgNnSc4d1	původní
židovské	židovský	k2eAgNnSc4d1	Židovské
jméno	jméno	k1gNnSc4	jméno
znělo	znět	k5eAaImAgNnS	znět
Šlomo	Šloma	k1gFnSc5	Šloma
Simcha	Simcha	k1gMnSc1	Simcha
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
je	být	k5eAaImIp3nS	být
německým	německý	k2eAgInSc7d1	německý
překladem	překlad	k1gInSc7	překlad
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
Simcha	Simch	k1gMnSc2	Simch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značí	značit	k5eAaImIp3nS	značit
"	"	kIx"	"
<g/>
radost	radost	k1gFnSc4	radost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Šlomo	Šloma	k1gFnSc5	Šloma
(	(	kIx(	(
<g/>
či	či	k8xC	či
Solomon	Solomon	k1gMnSc1	Solomon
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgMnS	obdržet
po	po	k7c6	po
dědovi	děd	k1gMnSc6	děd
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
narozením	narození	k1gNnSc7	narození
<g/>
.	.	kIx.	.
</s>
<s>
Šlomo	Šlomo	k6eAd1	Šlomo
Simcha	Simcha	k1gFnSc1	Simcha
pak	pak	k6eAd1	pak
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
moudrý	moudrý	k2eAgMnSc1d1	moudrý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
z	z	k7c2	z
učení	učení	k1gNnSc2	učení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
burgundském	burgundský	k2eAgMnSc6d1	burgundský
králi	král	k1gMnSc6	král
svatém	svatý	k1gMnSc6	svatý
Zikmundovi	Zikmund	k1gMnSc6	Zikmund
<g/>
,	,	kIx,	,
českém	český	k2eAgInSc6d1	český
národním	národní	k2eAgInSc6d1	národní
patronu	patron	k1gInSc6	patron
(	(	kIx(	(
<g/>
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnPc3d1	lucemburský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Motivací	motivace	k1gFnSc7	motivace
volby	volba	k1gFnSc2	volba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
snaha	snaha	k1gFnSc1	snaha
po	po	k7c6	po
konformitě	konformita	k1gFnSc6	konformita
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
katolickým	katolický	k2eAgMnSc7d1	katolický
<g/>
)	)	kIx)	)
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
z	z	k7c2	z
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
manželství	manželství	k1gNnSc2	manželství
židovského	židovský	k2eAgMnSc2d1	židovský
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
látkami	látka	k1gFnPc7	látka
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
akova	akov	k1gMnSc2	akov
Freuda	Freud	k1gMnSc2	Freud
s	s	k7c7	s
Amálii	Amálie	k1gFnSc6	Amálie
Nathanovou	Nathanův	k2eAgFnSc7d1	Nathanova
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
společenství	společenství	k1gNnSc2	společenství
asimilovaných	asimilovaný	k2eAgMnPc2d1	asimilovaný
židů	žid	k1gMnPc2	žid
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
do	do	k7c2	do
Sigmundových	Sigmundových	k2eAgNnPc2d1	Sigmundových
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
kvůli	kvůli	k7c3	kvůli
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
bankrotu	bankrot	k1gInSc3	bankrot
otcova	otcův	k2eAgInSc2d1	otcův
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
–	–	k?	–
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
–	–	k?	–
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
usadila	usadit	k5eAaPmAgFnS	usadit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
jako	jako	k8xC	jako
malý	malý	k1gMnSc1	malý
nechodil	chodit	k5eNaImAgMnS	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc4	vzdělání
získal	získat	k5eAaPmAgInS	získat
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
Leopoldstädter	Leopoldstädter	k1gInSc4	Leopoldstädter
Kommunal-Realgymnasium	Kommunal-Realgymnasium	k1gNnSc4	Kommunal-Realgymnasium
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
Sperlovo	Sperlův	k2eAgNnSc1d1	Sperlův
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
zde	zde	k6eAd1	zde
k	k	k7c3	k
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
žákům	žák	k1gMnPc3	žák
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
primusem	primus	k1gMnSc7	primus
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgInS	chtít
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
přečtení	přečtení	k1gNnSc6	přečtení
spisu	spis	k1gInSc2	spis
O	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
připisován	připisovat	k5eAaImNgInS	připisovat
Goethemu	Goethem	k1gInSc3	Goethem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
ji	on	k3xPp3gFnSc4	on
studovat	studovat	k5eAaImF	studovat
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zkoušky	zkouška	k1gFnPc4	zkouška
složil	složit	k5eAaPmAgMnS	složit
za	za	k7c4	za
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
jistým	jistý	k2eAgNnSc7d1	jisté
zpožděním	zpoždění	k1gNnSc7	zpoždění
–	–	k?	–
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
dvěma	dva	k4xCgInPc3	dva
studijním	studijní	k2eAgInPc3d1	studijní
pobytům	pobyt	k1gInPc3	pobyt
v	v	k7c6	v
experimentální	experimentální	k2eAgFnSc6d1	experimentální
zoologické	zoologický	k2eAgFnSc6d1	zoologická
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
Terstu	Terst	k1gInSc6	Terst
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
jeho	jeho	k3xOp3gFnSc1	jeho
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
ho	on	k3xPp3gInSc4	on
také	také	k9	také
silně	silně	k6eAd1	silně
vzrušila	vzrušit	k5eAaPmAgFnS	vzrušit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gInSc4	Schliemann
objevil	objevit	k5eAaPmAgMnS	objevit
starořecké	starořecký	k2eAgNnSc1d1	starořecké
město	město	k1gNnSc1	město
Trója	Trója	k1gFnSc1	Trója
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vykopal	vykopat	k5eAaPmAgInS	vykopat
zpod	zpod	k7c2	zpod
nánosů	nános	k1gInPc2	nános
času	čas	k1gInSc2	čas
jen	jen	k6eAd1	jen
díky	díky	k7c3	díky
důvěře	důvěra	k1gFnSc3	důvěra
v	v	k7c4	v
Homérův	Homérův	k2eAgInSc4d1	Homérův
básnický	básnický	k2eAgInSc4d1	básnický
popis	popis	k1gInSc4	popis
<g/>
.	.	kIx.	.
</s>
<s>
Archeologie	archeologie	k1gFnSc1	archeologie
ho	on	k3xPp3gMnSc4	on
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
fascinovala	fascinovat	k5eAaBmAgFnS	fascinovat
a	a	k8xC	a
metafora	metafora	k1gFnSc1	metafora
vykopávky	vykopávka	k1gFnSc2	vykopávka
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
jeho	jeho	k3xOp3gNnSc4	jeho
pozdější	pozdní	k2eAgNnSc4d2	pozdější
pojetí	pojetí	k1gNnSc4	pojetí
nevědomí	nevědomí	k1gNnSc2	nevědomí
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
obsahům	obsah	k1gInPc3	obsah
dobíral	dobírat	k5eAaImAgMnS	dobírat
<g/>
.	.	kIx.	.
</s>
<s>
Promoval	promovat	k5eAaBmAgMnS	promovat
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
jako	jako	k8xC	jako
doktor	doktor	k1gMnSc1	doktor
veškerého	veškerý	k3xTgNnSc2	veškerý
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
prostředí	prostředí	k1gNnSc1	prostředí
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
ho	on	k3xPp3gInSc4	on
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
v	v	k7c6	v
několika	několik	k4yIc6	několik
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
antisemitismem	antisemitismus	k1gInSc7	antisemitismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
posílilo	posílit	k5eAaPmAgNnS	posílit
jeho	jeho	k3xOp3gFnSc4	jeho
opozičnictví	opozičnictví	k1gNnSc1	opozičnictví
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
odradilo	odradit	k5eAaPmAgNnS	odradit
od	od	k7c2	od
sympatií	sympatie	k1gFnPc2	sympatie
k	k	k7c3	k
německému	německý	k2eAgNnSc3d1	německé
nacionálnímu	nacionální	k2eAgNnSc3d1	nacionální
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vždy	vždy	k6eAd1	vždy
buď	buď	k8xC	buď
Rakušanem	Rakušan	k1gMnSc7	Rakušan
nebo	nebo	k8xC	nebo
Židem	Žid	k1gMnSc7	Žid
(	(	kIx(	(
<g/>
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
náboženském	náboženský	k2eAgInSc6d1	náboženský
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaujaly	zaujmout	k5eAaPmAgFnP	zaujmout
ho	on	k3xPp3gInSc4	on
přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
Theodora	Theodor	k1gMnSc2	Theodor
Meynerta	Meynert	k1gMnSc2	Meynert
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nepochyboval	pochybovat	k5eNaImAgMnS	pochybovat
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
plně	plně	k6eAd1	plně
neurologickém	urologický	k2eNgInSc6d1	urologický
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
psýše	psýcha	k1gFnSc3	psýcha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
T.G.	T.G.	k1gFnSc7	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
i	i	k8xC	i
přednášky	přednáška	k1gFnPc1	přednáška
filozofa	filozof	k1gMnSc2	filozof
Franze	Franze	k1gFnSc1	Franze
Brentana	Brentana	k1gFnSc1	Brentana
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
odbíhal	odbíhat	k5eAaImAgInS	odbíhat
od	od	k7c2	od
témat	téma	k1gNnPc2	téma
lékařských	lékařský	k2eAgNnPc2d1	lékařské
ke	k	k7c3	k
kulturním	kulturní	k2eAgFnPc3d1	kulturní
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgInS	přeložit
například	například	k6eAd1	například
čtyři	čtyři	k4xCgFnPc1	čtyři
eseje	esej	k1gFnPc1	esej
Johna	John	k1gMnSc2	John
Stuarta	Stuart	k1gMnSc2	Stuart
Milla	Mill	k1gMnSc2	Mill
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
o	o	k7c6	o
ženské	ženský	k2eAgFnSc6d1	ženská
emancipaci	emancipace	k1gFnSc6	emancipace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohlcen	pohltit	k5eAaPmNgInS	pohltit
byl	být	k5eAaImAgInS	být
též	též	k9	též
četbou	četba	k1gFnSc7	četba
knihy	kniha	k1gFnSc2	kniha
brněnského	brněnský	k2eAgMnSc2d1	brněnský
rodáka	rodák	k1gMnSc2	rodák
Theodora	Theodor	k1gMnSc2	Theodor
Gomperze	Gomperze	k1gFnSc2	Gomperze
Myšlení	myšlení	k1gNnSc1	myšlení
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
studie	studie	k1gFnSc1	studie
Dějiny	dějiny	k1gFnPc1	dějiny
řecké	řecký	k2eAgFnSc2d1	řecká
civilizace	civilizace	k1gFnSc2	civilizace
Jacoba	Jacoba	k1gFnSc1	Jacoba
Burckhardta	Burckhardta	k1gFnSc1	Burckhardta
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
později	pozdě	k6eAd2	pozdě
řadu	řada	k1gFnSc4	řada
svých	svůj	k3xOyFgInPc2	svůj
objevů	objev	k1gInPc2	objev
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
mytologií	mytologie	k1gFnSc7	mytologie
(	(	kIx(	(
<g/>
Oidipus	Oidipus	k1gMnSc1	Oidipus
<g/>
,	,	kIx,	,
Élektra	Élektra	k1gFnSc1	Élektra
<g/>
,	,	kIx,	,
Erós	Erós	k1gMnSc1	Erós
<g/>
,	,	kIx,	,
Thanatos	Thanatos	k1gMnSc1	Thanatos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gInSc4	on
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
dílo	dílo	k1gNnSc1	dílo
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
a	a	k8xC	a
intenzivně	intenzivně	k6eAd1	intenzivně
ho	on	k3xPp3gMnSc4	on
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vzdálil	vzdálit	k5eAaPmAgMnS	vzdálit
biologii	biologie	k1gFnSc4	biologie
<g/>
,	,	kIx,	,
Darwina	Darwin	k1gMnSc4	Darwin
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
vážil	vážit	k5eAaImAgMnS	vážit
a	a	k8xC	a
podle	podle	k7c2	podle
Élisabeth	Élisabetha	k1gFnPc2	Élisabetha
Roudinescové	Roudinescová	k1gFnSc2	Roudinescová
byl	být	k5eAaImAgInS	být
darwinismus	darwinismus	k1gInSc1	darwinismus
vždy	vždy	k6eAd1	vždy
základním	základní	k2eAgInSc7d1	základní
skrytým	skrytý	k2eAgInSc7d1	skrytý
modelem	model	k1gInSc7	model
celého	celý	k2eAgNnSc2d1	celé
Freudova	Freudův	k2eAgNnSc2d1	Freudovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Sulloway	Sullowaa	k1gFnSc2	Sullowaa
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
analogicky	analogicky	k6eAd1	analogicky
<g/>
,	,	kIx,	,
Freud	Freud	k1gInSc1	Freud
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
"	"	kIx"	"
<g/>
kryptobiologický	kryptobiologický	k2eAgInSc4d1	kryptobiologický
<g/>
"	"	kIx"	"
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
čistě	čistě	k6eAd1	čistě
psychologického	psychologický	k2eAgInSc2d1	psychologický
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
psýchu	psýcha	k1gFnSc4	psýcha
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
laboratoře	laboratoř	k1gFnSc2	laboratoř
Ernsta	Ernst	k1gMnSc2	Ernst
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Rittera	Ritter	k1gMnSc2	Ritter
von	von	k1gInSc1	von
Brückeho	Brücke	k1gMnSc2	Brücke
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
i	i	k9	i
publikovat	publikovat	k5eAaBmF	publikovat
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
tisku	tisk	k1gInSc6	tisk
–	–	k?	–
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
příspěvek	příspěvek	k1gInSc1	příspěvek
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
pohlavním	pohlavní	k2eAgFnPc3d1	pohlavní
žlázám	žláza	k1gFnPc3	žláza
úhořů	úhoř	k1gMnPc2	úhoř
<g/>
,	,	kIx,	,
úspěch	úspěch	k1gInSc4	úspěch
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
práce	práce	k1gFnSc1	práce
o	o	k7c6	o
centrální	centrální	k2eAgFnSc6d1	centrální
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
mihule	mihule	k1gFnSc2	mihule
říční	říční	k2eAgFnSc2d1	říční
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
neurologických	urologický	k2eNgNnPc6d1	neurologické
bádáních	bádání	k1gNnPc6	bádání
nebyl	být	k5eNaImAgMnS	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
definování	definování	k1gNnSc2	definování
neuronu	neuron	k1gInSc2	neuron
(	(	kIx(	(
<g/>
definován	definovat	k5eAaBmNgMnS	definovat
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
až	až	k9	až
Waldeyerem	Waldeyer	k1gInSc7	Waldeyer
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nebyl	být	k5eNaImAgMnS	být
Freud	Freud	k1gMnSc1	Freud
ochoten	ochoten	k2eAgMnSc1d1	ochoten
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
zcela	zcela	k6eAd1	zcela
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
osudu	osud	k1gInSc2	osud
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
láska	láska	k1gFnSc1	láska
–	–	k?	–
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
poznal	poznat	k5eAaPmAgMnS	poznat
Martu	Marta	k1gFnSc4	Marta
Bernaysovou	Bernaysová	k1gFnSc4	Bernaysová
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
hmotně	hmotně	k6eAd1	hmotně
zabezpečen	zabezpečit	k5eAaPmNgInS	zabezpečit
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
však	však	k9	však
byly	být	k5eAaImAgFnP	být
nuzné	nuzný	k2eAgFnPc1d1	nuzná
a	a	k8xC	a
Brücke	Brück	k1gFnPc1	Brück
Freudovi	Freuda	k1gMnSc3	Freuda
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
lépe	dobře	k6eAd2	dobře
placenou	placený	k2eAgFnSc4d1	placená
pozici	pozice	k1gFnSc4	pozice
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
čekat	čekat	k5eAaImF	čekat
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Opustil	opustit	k5eAaPmAgMnS	opustit
proto	proto	k8xC	proto
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Meynertovým	Meynertův	k2eAgMnSc7d1	Meynertův
asistentem	asistent	k1gMnSc7	asistent
(	(	kIx(	(
<g/>
sekundararzt	sekundararzt	k1gMnSc1	sekundararzt
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ještě	ještě	k9	ještě
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
inklinoval	inklinovat	k5eAaImAgMnS	inklinovat
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
například	například	k6eAd1	například
metodu	metoda	k1gFnSc4	metoda
barvení	barvení	k1gNnSc2	barvení
neurologických	urologický	k2eNgInPc2d1	urologický
řezů	řez	k1gInPc2	řez
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
reputaci	reputace	k1gFnSc4	reputace
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
rovněž	rovněž	k9	rovněž
monografii	monografie	k1gFnSc4	monografie
o	o	k7c6	o
rostlině	rostlina	k1gFnSc6	rostlina
kokainovník	kokainovník	k1gInSc4	kokainovník
pravý	pravý	k2eAgInSc4d1	pravý
<g/>
,	,	kIx,	,
experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
kokainem	kokain	k1gInSc7	kokain
však	však	k9	však
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
lékařských	lékařský	k2eAgInPc6d1	lékařský
a	a	k8xC	a
vědeckých	vědecký	k2eAgInPc6d1	vědecký
kruzích	kruh	k1gInPc6	kruh
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
–	–	k?	–
především	především	k6eAd1	především
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
kokainem	kokain	k1gInSc7	kokain
ulevit	ulevit	k5eAaPmF	ulevit
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Ernstu	Ernst	k1gMnSc3	Ernst
von	von	k1gInSc1	von
Fleischlovi	Fleischl	k1gMnSc3	Fleischl
<g/>
,	,	kIx,	,
morfinistovi	morfinista	k1gMnSc3	morfinista
<g/>
.	.	kIx.	.
</s>
<s>
Fleischl	Fleischnout	k5eAaPmAgMnS	Fleischnout
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
závislým	závislý	k2eAgMnSc7d1	závislý
jak	jak	k8xS	jak
na	na	k7c6	na
morfiu	morfium	k1gNnSc6	morfium
tak	tak	k6eAd1	tak
na	na	k7c6	na
kokainu	kokain	k1gInSc6	kokain
(	(	kIx(	(
<g/>
kterýžto	kterýžto	k?	kterýžto
možný	možný	k2eAgInSc4d1	možný
účinek	účinek	k1gInSc4	účinek
Freud	Freud	k1gMnSc1	Freud
neznal	znát	k5eNaImAgMnS	znát
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
byla	být	k5eAaImAgFnS	být
dosud	dosud	k6eAd1	dosud
neprozkoumána	prozkoumán	k2eNgFnSc1d1	neprozkoumána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
kritice	kritika	k1gFnSc3	kritika
Freuda	Freudo	k1gNnSc2	Freudo
jeho	on	k3xPp3gMnSc2	on
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
soukromým	soukromý	k2eAgMnSc7d1	soukromý
docentem	docent	k1gMnSc7	docent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
příliš	příliš	k6eAd1	příliš
nelepšila	lepšit	k5eNaImAgFnS	lepšit
<g/>
.	.	kIx.	.
</s>
<s>
Podnikl	podniknout	k5eAaPmAgMnS	podniknout
tudíž	tudíž	k8xC	tudíž
dva	dva	k4xCgInPc4	dva
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
staly	stát	k5eAaPmAgFnP	stát
zásadními	zásadní	k2eAgFnPc7d1	zásadní
<g/>
:	:	kIx,	:
přijal	přijmout	k5eAaPmAgInS	přijmout
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
klinice	klinika	k1gFnSc6	klinika
v	v	k7c6	v
Oberdöblingu	Oberdöbling	k1gInSc6	Oberdöbling
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
o	o	k7c6	o
hypnóze	hypnóza	k1gFnSc6	hypnóza
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
roční	roční	k2eAgInSc4d1	roční
stipendijní	stipendijní	k2eAgInSc4d1	stipendijní
pobyt	pobyt	k1gInSc4	pobyt
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
k	k	k7c3	k
profesoru	profesor	k1gMnSc3	profesor
Jean-Martinu	Jean-Martin	k1gInSc2	Jean-Martin
Charcotovi	Charcot	k1gMnSc3	Charcot
<g/>
.	.	kIx.	.
</s>
<s>
Charcot	Charcot	k1gMnSc1	Charcot
používal	používat	k5eAaImAgMnS	používat
hypnózu	hypnóza	k1gFnSc4	hypnóza
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
přednáškách	přednáška	k1gFnPc6	přednáška
<g/>
,	,	kIx,	,
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
její	její	k3xOp3gInPc4	její
účinky	účinek	k1gInPc4	účinek
na	na	k7c6	na
dobrovolnících	dobrovolník	k1gMnPc6	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
byl	být	k5eAaImAgMnS	být
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
jeho	jeho	k3xOp3gFnSc2	jeho
pozornosti	pozornost	k1gFnSc2	pozornost
se	se	k3xPyFc4	se
dostávala	dostávat	k5eAaImAgFnS	dostávat
i	i	k9	i
hysterie	hysterie	k1gFnSc1	hysterie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
Charcot	Charcot	k1gInSc1	Charcot
také	také	k9	také
zabýval	zabývat	k5eAaImAgInS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přednášek	přednáška	k1gFnPc2	přednáška
u	u	k7c2	u
Charcota	Charcot	k1gMnSc2	Charcot
byl	být	k5eAaImAgMnS	být
Freud	Freud	k1gMnSc1	Freud
upozorněn	upozornit	k5eAaPmNgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hypnózu	hypnóza	k1gFnSc4	hypnóza
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
Hyppolyte	Hyppolyt	k1gInSc5	Hyppolyt
Bernheim	Bernheim	k1gInPc3	Bernheim
v	v	k7c6	v
Nancy	Nancy	k1gFnSc6	Nancy
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mezi	mezi	k7c7	mezi
Charcotem	Charcot	k1gInSc7	Charcot
a	a	k8xC	a
Bernheimem	Bernheim	k1gInSc7	Bernheim
panovala	panovat	k5eAaImAgFnS	panovat
ohromná	ohromný	k2eAgFnSc1d1	ohromná
rivalita	rivalita	k1gFnSc1	rivalita
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
si	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
oba	dva	k4xCgMnPc1	dva
získal	získat	k5eAaPmAgMnS	získat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přeložil	přeložit	k5eAaPmAgMnS	přeložit
jejich	jejich	k3xOp3gNnPc4	jejich
díla	dílo	k1gNnPc4	dílo
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
:	:	kIx,	:
Charcotovy	Charcotův	k2eAgFnSc2d1	Charcotova
přednášky	přednáška	k1gFnSc2	přednáška
Leçons	Leçons	k1gInSc1	Leçons
sur	sur	k?	sur
les	les	k1gInSc1	les
maladies	maladies	k1gInSc1	maladies
du	du	k?	du
systè	systè	k?	systè
nerveux	nerveux	k1gInSc4	nerveux
a	a	k8xC	a
Bernheimovu	Bernheimův	k2eAgFnSc4d1	Bernheimův
práci	práce	k1gFnSc4	práce
De	De	k?	De
la	la	k1gNnPc2	la
suggestion	suggestion	k1gInSc1	suggestion
et	et	k?	et
de	de	k?	de
ses	ses	k?	ses
applications	applications	k1gInSc1	applications
thérapeutiques	thérapeutiques	k1gInSc1	thérapeutiques
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Charcota	Charcot	k1gMnSc2	Charcot
pochopil	pochopit	k5eAaPmAgInS	pochopit
především	především	k9	především
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgInSc4d2	pozdější
koncept	koncept	k1gInSc4	koncept
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
Bernaysovou	Bernaysová	k1gFnSc7	Bernaysová
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgInS	otevřít
si	se	k3xPyFc3	se
soukromou	soukromý	k2eAgFnSc4d1	soukromá
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
léčením	léčení	k1gNnSc7	léčení
neuróz	neuróza	k1gFnPc2	neuróza
a	a	k8xC	a
hysterie	hysterie	k1gFnSc2	hysterie
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
užíval	užívat	k5eAaImAgInS	užívat
elektroléčbu	elektroléčba	k1gFnSc4	elektroléčba
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Heinricha	Heinrich	k1gMnSc2	Heinrich
Erba	Erbus	k1gMnSc2	Erbus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
nulové	nulový	k2eAgInPc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
způsoby	způsob	k1gInPc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
pomoci	pomoct	k5eAaPmF	pomoct
hysterickým	hysterický	k2eAgMnPc3d1	hysterický
pacientům	pacient	k1gMnPc3	pacient
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nějak	nějak	k6eAd1	nějak
aplikovat	aplikovat	k5eAaBmF	aplikovat
hypnózu	hypnóza	k1gFnSc4	hypnóza
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
propracoval	propracovat	k5eAaPmAgInS	propracovat
k	k	k7c3	k
vlastní	vlastní	k2eAgFnSc3d1	vlastní
metodě	metoda	k1gFnSc3	metoda
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc3d1	vlastní
teorii	teorie	k1gFnSc3	teorie
lidské	lidský	k2eAgFnSc3d1	lidská
osobnosti	osobnost	k1gFnSc3	osobnost
–	–	k?	–
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
objevu	objev	k1gInSc6	objev
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
sehrál	sehrát	k5eAaPmAgInS	sehrát
případ	případ	k1gInSc1	případ
"	"	kIx"	"
<g/>
Anny	Anna	k1gFnSc2	Anna
O.	O.	kA	O.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
nazývána	nazýván	k2eAgFnSc1d1	nazývána
ve	v	k7c6	v
Freudových	Freudový	k2eAgInPc6d1	Freudový
spisech	spis	k1gInPc6	spis
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
pacientka	pacientka	k1gFnSc1	pacientka
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Bertha	Berth	k1gMnSc4	Berth
Pappenheimová	Pappenheimová	k1gFnSc1	Pappenheimová
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
slavnou	slavný	k2eAgFnSc7d1	slavná
feministkou	feministka	k1gFnSc7	feministka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
případu	případ	k1gInSc6	případ
Freud	Freud	k1gMnSc1	Freud
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Josefa	Josef	k1gMnSc2	Josef
Breuera	Breuer	k1gMnSc2	Breuer
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
o	o	k7c6	o
Bertě	Berta	k1gFnSc6	Berta
dozvídal	dozvídat	k5eAaImAgMnS	dozvídat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgFnS	být
několikanásobně	několikanásobně	k6eAd1	několikanásobně
spřízněna	spřízněn	k2eAgFnSc1d1	spřízněna
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
jednadvacetileté	jednadvacetiletý	k2eAgFnSc3d1	jednadvacetiletá
dívce	dívka	k1gFnSc3	dívka
poprvé	poprvé	k6eAd1	poprvé
povolán	povolán	k2eAgInSc4d1	povolán
Breuer	Breuer	k1gInSc4	Breuer
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostikoval	diagnostikovat	k5eAaBmAgInS	diagnostikovat
těžkou	těžký	k2eAgFnSc4d1	těžká
hysterii	hysterie	k1gFnSc4	hysterie
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
ženu	žena	k1gFnSc4	žena
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Trpěla	trpět	k5eAaImAgFnS	trpět
fyzickými	fyzický	k2eAgFnPc7d1	fyzická
poruchami	porucha	k1gFnPc7	porucha
(	(	kIx(	(
<g/>
ztráta	ztráta	k1gFnSc1	ztráta
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
ochrnutí	ochrnutí	k1gNnSc2	ochrnutí
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
řeči	řeč	k1gFnSc2	řeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
psychickém	psychický	k2eAgNnSc6d1	psychické
vypětí	vypětí	k1gNnSc6	vypětí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
pramenilo	pramenit	k5eAaImAgNnS	pramenit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
nocích	nok	k1gInPc6	nok
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
nemocného	nemocný	k2eAgMnSc4d1	nemocný
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Breuer	Breuer	k1gInSc4	Breuer
léčil	léčit	k5eAaImAgMnS	léčit
klidem	klid	k1gInSc7	klid
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgInP	vrátit
i	i	k9	i
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stres	stres	k1gInSc1	stres
nebyl	být	k5eNaImAgInS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
příčinou	příčina	k1gFnSc7	příčina
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Breuer	Breuer	k1gMnSc1	Breuer
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
hypnózu	hypnóza	k1gFnSc4	hypnóza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Bernheima	Bernheimum	k1gNnSc2	Bernheimum
v	v	k7c6	v
Nancy	Nancy	k1gFnSc6	Nancy
se	se	k3xPyFc4	se
nesnažil	snažit	k5eNaImAgMnS	snažit
pacientku	pacientek	k1gMnSc3	pacientek
sugestivně	sugestivně	k6eAd1	sugestivně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nechal	nechat	k5eAaPmAgMnS	nechat
promlouvat	promlouvat	k5eAaImF	promlouvat
ji	on	k3xPp3gFnSc4	on
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
,	,	kIx,	,
dotazoval	dotazovat	k5eAaImAgMnS	dotazovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
na	na	k7c4	na
původ	původ	k1gInSc4	původ
jejích	její	k3xOp3gInPc2	její
symptomů	symptom	k1gInPc2	symptom
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
pacientce	pacientka	k1gFnSc3	pacientka
se	se	k3xPyFc4	se
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
z	z	k7c2	z
hypnózy	hypnóza	k1gFnSc2	hypnóza
skutečně	skutečně	k6eAd1	skutečně
ulevovalo	ulevovat	k5eAaImAgNnS	ulevovat
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Bertha	Bertha	k1gFnSc1	Bertha
tomu	ten	k3xDgMnSc3	ten
říkala	říkat	k5eAaImAgFnS	říkat
"	"	kIx"	"
<g/>
léčba	léčba	k1gFnSc1	léčba
mluvením	mluvení	k1gNnSc7	mluvení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
talking	talking	k1gInSc1	talking
cure	curat	k5eAaPmIp3nS	curat
<g/>
)	)	kIx)	)
či	či	k8xC	či
"	"	kIx"	"
<g/>
vymetání	vymetání	k1gNnSc1	vymetání
komína	komín	k1gInSc2	komín
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
chimney	chimney	k1gInPc4	chimney
sweeping	sweeping	k1gInSc1	sweeping
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
léčba	léčba	k1gFnSc1	léčba
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Breuer	Breuer	k1gInSc1	Breuer
nepředpokládal	předpokládat	k5eNaImAgInS	předpokládat
<g/>
:	:	kIx,	:
Bertha	Bertha	k1gFnSc1	Bertha
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zjevně	zjevně	k6eAd1	zjevně
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
,	,	kIx,	,
fixovala	fixovat	k5eAaImAgFnS	fixovat
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Breuer	Breuer	k1gInSc4	Breuer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
léčbu	léčba	k1gFnSc4	léčba
náhle	náhle	k6eAd1	náhle
přerušil	přerušit	k5eAaPmAgMnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Berta	Berta	k1gFnSc1	Berta
si	se	k3xPyFc3	se
vzápětí	vzápětí	k6eAd1	vzápětí
vsugerovala	vsugerovat	k5eAaPmAgFnS	vsugerovat
porodní	porodní	k2eAgFnPc4d1	porodní
bolesti	bolest	k1gFnPc4	bolest
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
ten	ten	k3xDgInSc4	ten
večer	večer	k1gInSc4	večer
se	se	k3xPyFc4	se
zmítala	zmítat	k5eAaImAgFnS	zmítat
v	v	k7c6	v
křečích	křeč	k1gFnPc6	křeč
<g/>
,	,	kIx,	,
křičíc	křičet	k5eAaImSgFnS	křičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Teď	teď	k6eAd1	teď
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
dítě	dítě	k1gNnSc1	dítě
doktora	doktor	k1gMnSc2	doktor
Breuera	Breuer	k1gMnSc2	Breuer
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
přivolán	přivolán	k2eAgMnSc1d1	přivolán
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
ji	on	k3xPp3gFnSc4	on
naposled	naposled	k6eAd1	naposled
do	do	k7c2	do
hypnózy	hypnóza	k1gFnSc2	hypnóza
a	a	k8xC	a
odeslal	odeslat	k5eAaPmAgMnS	odeslat
do	do	k7c2	do
sanatoria	sanatorium	k1gNnSc2	sanatorium
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
sám	sám	k3xTgMnSc1	sám
odjel	odjet	k5eAaPmAgMnS	odjet
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
na	na	k7c4	na
jakési	jakýsi	k3yIgFnPc4	jakýsi
druhé	druhý	k4xOgFnPc4	druhý
líbánky	líbánky	k1gFnPc4	líbánky
<g/>
.	.	kIx.	.
</s>
<s>
Bertha	Bertha	k1gFnSc1	Bertha
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Breuerovi	Breuer	k1gMnSc3	Breuer
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
přenosu	přenos	k1gInSc2	přenos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
léčby	léčba	k1gFnSc2	léčba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vyděsilo	vyděsit	k5eAaPmAgNnS	vyděsit
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
roku	rok	k1gInSc6	rok
1932	[number]	k4	1932
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Stefanu	Stefan	k1gMnSc3	Stefan
Zweigovi	Zweig	k1gMnSc3	Zweig
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
On	on	k3xPp3gMnSc1	on
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
zahodil	zahodit	k5eAaPmAgMnS	zahodit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
všech	všecek	k3xTgInPc6	všecek
velkých	velký	k2eAgInPc6d1	velký
duševních	duševní	k2eAgInPc6d1	duševní
darech	dar	k1gInPc6	dar
v	v	k7c6	v
sobě	se	k3xPyFc3	se
neměl	mít	k5eNaImAgMnS	mít
nic	nic	k3yNnSc1	nic
faustovského	faustovský	k2eAgNnSc2d1	Faustovské
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
rychle	rychle	k6eAd1	rychle
zjišťoval	zjišťovat	k5eAaImAgMnS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektroléčba	elektroléčba	k1gFnSc1	elektroléčba
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgInS	chopit
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
Breuerova	Breuerův	k2eAgInSc2d1	Breuerův
případu	případ	k1gInSc2	případ
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
–	–	k?	–
pacientce	pacientka	k1gFnSc3	pacientka
se	se	k3xPyFc4	se
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mohla	moct	k5eAaImAgFnS	moct
bez	bez	k7c2	bez
zábran	zábrana	k1gFnPc2	zábrana
<g/>
,	,	kIx,	,
v	v	k7c6	v
hypnóze	hypnóza	k1gFnSc6	hypnóza
<g/>
,	,	kIx,	,
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ji	on	k3xPp3gFnSc4	on
trápí	trápit	k5eAaImIp3nP	trápit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
také	také	k9	také
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
uvádět	uvádět	k5eAaImF	uvádět
pacienty	pacient	k1gMnPc4	pacient
do	do	k7c2	do
hypnózy	hypnóza	k1gFnSc2	hypnóza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
příliš	příliš	k6eAd1	příliš
vlohy	vloha	k1gFnPc4	vloha
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
proto	proto	k8xC	proto
novou	nový	k2eAgFnSc4d1	nová
techniku	technika	k1gFnSc4	technika
–	–	k?	–
když	když	k8xS	když
si	se	k3xPyFc3	se
pacient	pacient	k1gMnSc1	pacient
nemohl	moct	k5eNaImAgMnS	moct
na	na	k7c4	na
něco	něco	k3yInSc4	něco
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
<g/>
,	,	kIx,	,
Freud	Freud	k1gMnSc1	Freud
mu	on	k3xPp3gMnSc3	on
vsugeroval	vsugerovat	k5eAaPmAgMnS	vsugerovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
vybaví	vybavit	k5eAaPmIp3nP	vybavit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
mu	on	k3xPp3gMnSc3	on
sáhne	sáhnout	k5eAaPmIp3nS	sáhnout
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
měla	mít	k5eAaImAgFnS	mít
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
dospěl	dochvít	k5eAaPmAgInS	dochvít
ke	k	k7c3	k
klíčovému	klíčový	k2eAgInSc3d1	klíčový
objevu	objev	k1gInSc2	objev
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hysterici	hysterik	k1gMnPc1	hysterik
trpí	trpět	k5eAaImIp3nP	trpět
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	Nechtěl	k1gMnSc1	Nechtěl
si	se	k3xPyFc3	se
však	však	k9	však
uzurpovat	uzurpovat	k5eAaBmF	uzurpovat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
náležel	náležet	k5eAaImAgMnS	náležet
Breuerovi	Breuer	k1gMnSc3	Breuer
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Freud	Freud	k1gInSc4	Freud
Breuera	Breuera	k1gFnSc1	Breuera
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
od	od	k7c2	od
případu	případ	k1gInSc2	případ
Anny	Anna	k1gFnSc2	Anna
O.	O.	kA	O.
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
jejího	její	k3xOp3gInSc2	její
chorobopisu	chorobopis	k1gInSc2	chorobopis
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
přidal	přidat	k5eAaPmAgMnS	přidat
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
chorobopisy	chorobopis	k1gInPc4	chorobopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nashromáždil	nashromáždit	k5eAaPmAgMnS	nashromáždit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
publikovali	publikovat	k5eAaBmAgMnP	publikovat
Studie	studie	k1gFnSc1	studie
o	o	k7c4	o
hysterii	hysterie	k1gFnSc4	hysterie
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
bez	bez	k7c2	bez
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
neshod	neshoda	k1gFnPc2	neshoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Terapii	terapie	k1gFnSc4	terapie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
takto	takto	k6eAd1	takto
společně	společně	k6eAd1	společně
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
<g/>
,	,	kIx,	,
nazvali	nazvat	k5eAaBmAgMnP	nazvat
katartická	katartický	k2eAgFnSc1d1	katartický
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgNnP	vyvinout
<g/>
,	,	kIx,	,
již	již	k9	již
bez	bez	k7c2	bez
Breuerovy	Breuerův	k2eAgFnSc2d1	Breuerova
účasti	účast	k1gFnSc2	účast
<g/>
,	,	kIx,	,
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ovšem	ovšem	k9	ovšem
Freud	Freud	k1gInSc1	Freud
musel	muset	k5eAaImAgInS	muset
ještě	ještě	k6eAd1	ještě
rozvinout	rozvinout	k5eAaPmF	rozvinout
metodu	metoda	k1gFnSc4	metoda
volné	volný	k2eAgFnSc2d1	volná
asociace	asociace	k1gFnSc2	asociace
–	–	k?	–
dialog	dialog	k1gInSc1	dialog
mezi	mezi	k7c7	mezi
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
pacientem	pacient	k1gMnSc7	pacient
postupně	postupně	k6eAd1	postupně
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
pacientův	pacientův	k2eAgInSc4d1	pacientův
monolog	monolog	k1gInSc4	monolog
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
také	také	k9	také
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Studiích	studie	k1gFnPc6	studie
o	o	k7c6	o
hysterii	hysterie	k1gFnSc6	hysterie
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
jeho	jeho	k3xOp3gFnSc1	jeho
pacientka	pacientka	k1gFnSc1	pacientka
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tam	tam	k6eAd1	tam
nazývá	nazývat	k5eAaImIp3nS	nazývat
Ema	Ema	k1gFnSc1	Ema
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tu	tu	k6eAd1	tu
řekla	říct	k5eAaPmAgFnS	říct
hodně	hodně	k6eAd1	hodně
rozmrzele	rozmrzele	k6eAd1	rozmrzele
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nemám	mít	k5eNaImIp1nS	mít
stále	stále	k6eAd1	stále
vyptávat	vyptávat	k5eAaImF	vyptávat
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
ono	onen	k3xDgNnSc1	onen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechat	nechat	k5eAaPmF	nechat
jí	on	k3xPp3gFnSc7	on
vypravovat	vypravovat	k5eAaImF	vypravovat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mi	já	k3xPp1nSc3	já
má	mít	k5eAaImIp3nS	mít
říci	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
aby	aby	kYmCp3nS	aby
ještě	ještě	k6eAd1	ještě
méně	málo	k6eAd2	málo
rozptyloval	rozptylovat	k5eAaImAgMnS	rozptylovat
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
i	i	k9	i
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
prostředí	prostředí	k1gNnSc4	prostředí
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgNnSc1d1	známé
především	především	k6eAd1	především
jako	jako	k8xC	jako
Freudova	Freudův	k2eAgFnSc1d1	Freudova
pohovka	pohovka	k1gFnSc1	pohovka
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
metoda	metoda	k1gFnSc1	metoda
Freuda	Freud	k1gMnSc2	Freud
utvrdila	utvrdit	k5eAaPmAgFnS	utvrdit
v	v	k7c6	v
několika	několik	k4yIc6	několik
věcech	věc	k1gFnPc6	věc
<g/>
:	:	kIx,	:
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
nevědomá	vědomý	k2eNgFnSc1d1	nevědomá
část	část	k1gFnSc1	část
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
je	být	k5eAaImIp3nS	být
noční	noční	k2eAgInSc4d1	noční
sen	sen	k1gInSc4	sen
a	a	k8xC	a
že	že	k8xS	že
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
v	v	k7c6	v
neurotické	neurotický	k2eAgFnSc6d1	neurotická
poruše	porucha	k1gFnSc6	porucha
hraje	hrát	k5eAaImIp3nS	hrát
sexualita	sexualita	k1gFnSc1	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
poslední	poslední	k2eAgInSc1d1	poslední
objev	objev	k1gInSc1	objev
znamenal	znamenat	k5eAaImAgInS	znamenat
definitivní	definitivní	k2eAgInSc1d1	definitivní
rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
Breuerem	Breuer	k1gInSc7	Breuer
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
sexuality	sexualita	k1gFnSc2	sexualita
však	však	k8xC	však
Freuda	Freuda	k1gFnSc1	Freuda
dlouho	dlouho	k6eAd1	dlouho
mátla	mást	k5eAaImAgFnS	mást
<g/>
,	,	kIx,	,
domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
<g/>
,	,	kIx,	,
že	že	k8xS	že
neurotici	neurotik	k1gMnPc1	neurotik
byli	být	k5eAaImAgMnP	být
v	v	k7c4	v
dětství	dětství	k1gNnSc4	dětství
sexuálně	sexuálně	k6eAd1	sexuálně
svedeni	sveden	k2eAgMnPc1d1	sveden
dospělými	dospělí	k1gMnPc7	dospělí
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
takřka	takřka	k6eAd1	takřka
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
svědčili	svědčit	k5eAaImAgMnP	svědčit
jeho	jeho	k3xOp3gMnPc1	jeho
pacienti	pacient	k1gMnPc1	pacient
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
tzv.	tzv.	kA	tzv.
teorii	teorie	k1gFnSc4	teorie
traumatu	trauma	k1gNnSc3	trauma
zavrhl	zavrhnout	k5eAaPmAgInS	zavrhnout
<g/>
,	,	kIx,	,
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
scény	scéna	k1gFnPc1	scéna
svedení	svedení	k1gNnSc2	svedení
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
byly	být	k5eAaImAgFnP	být
dětskými	dětský	k2eAgFnPc7d1	dětská
sexuálními	sexuální	k2eAgFnPc7d1	sexuální
fantaziemi	fantazie	k1gFnPc7	fantazie
a	a	k8xC	a
že	že	k8xS	že
neurotik	neurotik	k1gMnSc1	neurotik
je	být	k5eAaImIp3nS	být
neurotikem	neurotik	k1gMnSc7	neurotik
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
fantazii	fantazie	k1gFnSc3	fantazie
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
jako	jako	k9	jako
k	k	k7c3	k
realitě	realita	k1gFnSc3	realita
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
autoři	autor	k1gMnPc1	autor
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
Jeffrey	Jeffrey	k1gInPc1	Jeffrey
Masson	Masson	k1gNnSc1	Masson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgNnSc1d1	původní
stanovisko	stanovisko	k1gNnSc1	stanovisko
bylo	být	k5eAaImAgNnS	být
správné	správný	k2eAgNnSc1d1	správné
a	a	k8xC	a
Freud	Freud	k1gMnSc1	Freud
jen	jen	k9	jen
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
snaze	snaha	k1gFnSc3	snaha
ochránit	ochránit	k5eAaPmF	ochránit
pověst	pověst	k1gFnSc4	pověst
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
však	však	k9	však
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
neobjevil	objevit	k5eNaPmAgInS	objevit
jen	jen	k6eAd1	jen
studiem	studio	k1gNnSc7	studio
neurotiků	neurotik	k1gMnPc2	neurotik
a	a	k8xC	a
uplatněním	uplatnění	k1gNnSc7	uplatnění
své	svůj	k3xOyFgFnSc2	svůj
nové	nový	k2eAgFnSc2d1	nová
terapeutické	terapeutický	k2eAgFnSc2d1	terapeutická
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
analýza	analýza	k1gFnSc1	analýza
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
autoanalýza	autoanalýza	k1gFnSc1	autoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
začal	začít	k5eAaPmAgMnS	začít
analyzovat	analyzovat	k5eAaImF	analyzovat
své	svůj	k3xOyFgInPc4	svůj
sny	sen	k1gInPc4	sen
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
první	první	k4xOgMnSc1	první
rozebral	rozebrat	k5eAaPmAgMnS	rozebrat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Bellevue	bellevue	k1gFnSc2	bellevue
nedaleko	nedaleko	k7c2	nedaleko
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
sen	sen	k1gInSc1	sen
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Irmině	Irmin	k2eAgFnSc3d1	Irmina
injekci	injekce	k1gFnSc3	injekce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
také	také	k9	také
později	pozdě	k6eAd2	pozdě
otevřel	otevřít	k5eAaPmAgInS	otevřít
knihu	kniha	k1gFnSc4	kniha
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Autoanalýza	Autoanalýza	k1gFnSc1	Autoanalýza
však	však	k9	však
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgNnPc4	dva
hlavní	hlavní	k2eAgNnPc4d1	hlavní
témata	téma	k1gNnPc4	téma
<g/>
:	:	kIx,	:
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Freudovým	Freudový	k2eAgMnSc7d1	Freudový
otcem	otec	k1gMnSc7	otec
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
Freud	Freud	k1gInSc4	Freud
začal	začít	k5eAaPmAgInS	začít
pozorovat	pozorovat	k5eAaImF	pozorovat
neurotické	neurotický	k2eAgInPc4d1	neurotický
rysy	rys	k1gInPc4	rys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Fliessem	Fliess	k1gMnSc7	Fliess
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
Freud	Freud	k1gMnSc1	Freud
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
latentně	latentně	k6eAd1	latentně
homosexuální	homosexuální	k2eAgInSc1d1	homosexuální
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
sám	sám	k3xTgMnSc1	sám
Freud	Freud	k1gMnSc1	Freud
prožil	prožít	k5eAaPmAgMnS	prožít
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jeho	jeho	k3xOp3gMnPc1	jeho
pacienti	pacient	k1gMnPc1	pacient
prožívají	prožívat	k5eAaImIp3nP	prožívat
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
–	–	k?	–
"	"	kIx"	"
<g/>
hysterickou	hysterický	k2eAgFnSc4d1	hysterická
lásku	láska	k1gFnSc4	láska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
stav	stav	k1gInSc1	stav
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
přenosu	přenos	k1gInSc2	přenos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
Freud	Freud	k1gMnSc1	Freud
objevil	objevit	k5eAaPmAgMnS	objevit
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
zmínil	zmínit	k5eAaPmAgMnS	zmínit
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
dopise	dopis	k1gInSc6	dopis
Fliessovi	Fliessův	k2eAgMnPc1d1	Fliessův
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1897	[number]	k4	1897
(	(	kIx(	(
<g/>
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
ročním	roční	k2eAgNnSc7d1	roční
výročím	výročí	k1gNnSc7	výročí
smrti	smrt	k1gFnSc2	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
Freud	Freud	k1gInSc1	Freud
své	svůj	k3xOyFgInPc4	svůj
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
až	až	k9	až
1905	[number]	k4	1905
v	v	k7c6	v
několika	několik	k4yIc6	několik
zakladatelských	zakladatelský	k2eAgInPc6d1	zakladatelský
spisech	spis	k1gInPc6	spis
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
Výkladu	výklad	k1gInSc6	výklad
snů	sen	k1gInPc2	sen
definoval	definovat	k5eAaBmAgInS	definovat
nevědomí	nevědomí	k1gNnPc4	nevědomí
a	a	k8xC	a
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Třech	tři	k4xCgNnPc6	tři
pojednáních	pojednání	k1gNnPc6	pojednání
o	o	k7c6	o
sexuální	sexuální	k2eAgFnSc6d1	sexuální
teorii	teorie	k1gFnSc6	teorie
definoval	definovat	k5eAaBmAgInS	definovat
libido	libido	k1gNnSc4	libido
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
orální	orální	k2eAgInSc4d1	orální
a	a	k8xC	a
anální	anální	k2eAgNnSc4d1	anální
stadium	stadium	k1gNnSc4	stadium
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
dětskou	dětský	k2eAgFnSc4d1	dětská
sexualitu	sexualita	k1gFnSc4	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
sexuality	sexualita	k1gFnSc2	sexualita
v	v	k7c6	v
neuróze	neuróza	k1gFnSc6	neuróza
popsal	popsat	k5eAaPmAgMnS	popsat
především	především	k9	především
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
případu	případ	k1gInSc2	případ
Dory	Dora	k1gFnSc2	Dora
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zlomek	zlomek	k1gInSc1	zlomek
analysy	analysa	k1gFnSc2	analysa
případu	případ	k1gInSc2	případ
hysterie	hysterie	k1gFnSc2	hysterie
<g/>
)	)	kIx)	)
Základní	základní	k2eAgInSc1d1	základní
model	model	k1gInSc1	model
z	z	k7c2	z
Výkladu	výklad	k1gInSc2	výklad
snů	sen	k1gInPc2	sen
pak	pak	k9	pak
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
populárních	populární	k2eAgFnPc6d1	populární
pracích	práce	k1gFnPc6	práce
Psychopatologie	psychopatologie	k1gFnSc2	psychopatologie
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
freudovské	freudovský	k2eAgNnSc1d1	freudovské
přeřeknutí	přeřeknutí	k1gNnSc1	přeřeknutí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vtip	vtip	k1gInSc4	vtip
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
nevědomí	nevědomí	k1gNnSc3	nevědomí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
využil	využít	k5eAaPmAgMnS	využít
svou	svůj	k3xOyFgFnSc4	svůj
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sbírku	sbírka	k1gFnSc4	sbírka
anekdot	anekdota	k1gFnPc2	anekdota
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
židovských	židovská	k1gFnPc2	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
budily	budit	k5eAaImAgInP	budit
jeho	jeho	k3xOp3gInPc1	jeho
objevy	objev	k1gInPc1	objev
pouze	pouze	k6eAd1	pouze
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
Freud	Freud	k1gInSc4	Freud
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
však	však	k9	však
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
profesorem	profesor	k1gMnSc7	profesor
(	(	kIx(	(
<g/>
außerordentlicher	außerordentlichra	k1gFnPc2	außerordentlichra
Professor	Professor	k1gInSc1	Professor
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
začal	začít	k5eAaPmAgInS	začít
získávat	získávat	k5eAaImF	získávat
první	první	k4xOgMnPc4	první
příznivce	příznivec	k1gMnPc4	příznivec
mezi	mezi	k7c7	mezi
vídeňskými	vídeňský	k2eAgMnPc7d1	vídeňský
lékaři	lékař	k1gMnPc7	lékař
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
ustavil	ustavit	k5eAaPmAgMnS	ustavit
tradici	tradice	k1gFnSc4	tradice
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
středečních	středeční	k2eAgNnPc2d1	středeční
setkání	setkání	k1gNnPc2	setkání
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
bytě	byt	k1gInSc6	byt
na	na	k7c6	na
Berggasse	Berggass	k1gInSc6	Berggass
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
schůzku	schůzka	k1gFnSc4	schůzka
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
dostavili	dostavit	k5eAaPmAgMnP	dostavit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Reitler	Reitler	k1gMnSc1	Reitler
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Kahane	kahan	k1gInSc5	kahan
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Stekel	Stekel	k1gMnSc1	Stekel
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Adler	Adler	k1gMnSc1	Adler
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
účastníky	účastník	k1gMnPc7	účastník
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
stali	stát	k5eAaPmAgMnP	stát
muzikolog	muzikolog	k1gMnSc1	muzikolog
Max	Max	k1gMnSc1	Max
Graf	graf	k1gInSc1	graf
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
Hugo	Hugo	k1gMnSc1	Hugo
Heller	Heller	k1gMnSc1	Heller
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
Paul	Paul	k1gMnSc1	Paul
Federn	Federn	k1gMnSc1	Federn
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Meisl	Meisl	k1gInSc1	Meisl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
přibyli	přibýt	k5eAaPmAgMnP	přibýt
Eduard	Eduard	k1gMnSc1	Eduard
Hitschmann	Hitschmann	k1gMnSc1	Hitschmann
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Deutsch	Deutsch	k1gMnSc1	Deutsch
a	a	k8xC	a
Philipp	Philipp	k1gMnSc1	Philipp
Frey	Frea	k1gFnSc2	Frea
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
Isidor	Isidor	k1gMnSc1	Isidor
Sadger	Sadger	k1gMnSc1	Sadger
a	a	k8xC	a
Otto	Otto	k1gMnSc1	Otto
Rank	rank	k1gInSc1	rank
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
třeba	třeba	k9	třeba
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Sándor	Sándor	k1gInSc1	Sándor
Ferenczi	Ferencze	k1gFnSc4	Ferencze
či	či	k8xC	či
Hans	Hans	k1gMnSc1	Hans
Sachs	Sachsa	k1gFnPc2	Sachsa
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
momentem	moment	k1gInSc7	moment
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
navázání	navázání	k1gNnSc1	navázání
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
curyšskou	curyšský	k2eAgFnSc7d1	curyšská
psychologickou	psychologický	k2eAgFnSc7d1	psychologická
školou	škola	k1gFnSc7	škola
(	(	kIx(	(
<g/>
Max	Max	k1gMnSc1	Max
Eitingon	Eitingon	k1gMnSc1	Eitingon
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Bleuler	Bleuler	k1gMnSc1	Bleuler
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Abraham	Abraham	k1gMnSc1	Abraham
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lokální	lokální	k2eAgInSc1d1	lokální
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
směr	směr	k1gInSc1	směr
proměnil	proměnit	k5eAaPmAgInS	proměnit
v	v	k7c4	v
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgInS	být
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
první	první	k4xOgInSc1	první
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
psychoanalytický	psychoanalytický	k2eAgInSc1d1	psychoanalytický
kongres	kongres	k1gInSc1	kongres
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Freud	Freud	k1gMnSc1	Freud
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Ernestem	Ernest	k1gMnSc7	Ernest
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
posléze	posléze	k6eAd1	posléze
šířil	šířit	k5eAaImAgMnS	šířit
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
sepsal	sepsat	k5eAaPmAgMnS	sepsat
také	také	k9	také
dosud	dosud	k6eAd1	dosud
nejpodrobnější	podrobný	k2eAgInSc4d3	nejpodrobnější
<g/>
,	,	kIx,	,
třídílný	třídílný	k2eAgInSc4d1	třídílný
Freudův	Freudův	k2eAgInSc4d1	Freudův
životopis	životopis	k1gInSc4	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
USA	USA	kA	USA
přijel	přijet	k5eAaPmAgMnS	přijet
Abraham	Abraham	k1gMnSc1	Abraham
Arden	Ardeny	k1gFnPc2	Ardeny
Brill	Brill	k1gMnSc1	Brill
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sehrál	sehrát	k5eAaPmAgMnS	sehrát
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
podobnou	podobný	k2eAgFnSc4d1	podobná
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
Jones	Jones	k1gInSc4	Jones
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
vlivu	vliv	k1gInSc3	vliv
byli	být	k5eAaImAgMnP	být
Freud	Freud	k1gInSc4	Freud
a	a	k8xC	a
Jung	Jung	k1gMnSc1	Jung
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
pozváni	pozvat	k5eAaPmNgMnP	pozvat
k	k	k7c3	k
přednáškám	přednáška	k1gFnPc3	přednáška
na	na	k7c4	na
Clarkovu	Clarkův	k2eAgFnSc4d1	Clarkova
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgInS	vést
Granville	Granville	k1gInSc1	Granville
Stanley	Stanlea	k1gFnSc2	Stanlea
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
salzburského	salzburský	k2eAgInSc2d1	salzburský
kongresu	kongres	k1gInSc2	kongres
začaly	začít	k5eAaPmAgFnP	začít
však	však	k9	však
hnutím	hnutí	k1gNnPc3	hnutí
zmítat	zmítat	k5eAaImF	zmítat
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
vlakem	vlak	k1gInSc7	vlak
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
skupina	skupina	k1gFnSc1	skupina
nespokojenců	nespokojenec	k1gMnPc2	nespokojenec
demonstrativně	demonstrativně	k6eAd1	demonstrativně
nepustila	pustit	k5eNaPmAgFnS	pustit
Freuda	Freuda	k1gFnSc1	Freuda
v	v	k7c6	v
kupé	kupé	k1gNnSc6	kupé
sednout	sednout	k5eAaPmF	sednout
(	(	kIx(	(
<g/>
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
Stekel	Stekel	k1gMnSc1	Stekel
<g/>
,	,	kIx,	,
Sadger	Sadger	k1gMnSc1	Sadger
<g/>
,	,	kIx,	,
Reitler	Reitler	k1gMnSc1	Reitler
<g/>
,	,	kIx,	,
Federn	Federn	k1gMnSc1	Federn
<g/>
,	,	kIx,	,
Wittels	Wittels	k1gInSc1	Wittels
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyčítala	vyčítat	k5eAaImAgFnS	vyčítat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
opomíjel	opomíjet	k5eAaImAgInS	opomíjet
své	svůj	k3xOyFgMnPc4	svůj
první	první	k4xOgMnPc4	první
spolubojovníky	spolubojovník	k1gMnPc4	spolubojovník
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
delegáty	delegát	k1gMnPc4	delegát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Curyšany	Curyšana	k1gFnSc2	Curyšana
<g/>
.	.	kIx.	.
</s>
<s>
Rozbuškou	rozbuška	k1gFnSc7	rozbuška
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zejména	zejména	k9	zejména
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Freud	Freud	k1gInSc1	Freud
nepozval	pozvat	k5eNaPmAgInS	pozvat
nikoho	nikdo	k3yNnSc4	nikdo
z	z	k7c2	z
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
psychoanalytické	psychoanalytický	k2eAgFnSc6d1	psychoanalytická
ročence	ročenka	k1gFnSc6	ročenka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
Bleulerově	Bleulerův	k2eAgInSc6d1	Bleulerův
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
redaktorem	redaktor	k1gMnSc7	redaktor
ročenky	ročenka	k1gFnSc2	ročenka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jung	Jung	k1gMnSc1	Jung
a	a	k8xC	a
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
redaktory	redaktor	k1gMnPc7	redaktor
Bleuler	Bleuler	k1gInSc4	Bleuler
a	a	k8xC	a
Freud	Freud	k1gInSc4	Freud
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Curychu	Curych	k1gInSc2	Curych
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaskočený	zaskočený	k2eAgInSc1d1	zaskočený
Freud	Freud	k1gInSc1	Freud
se	se	k3xPyFc4	se
bránil	bránit	k5eAaImAgInS	bránit
argumentem	argument	k1gInSc7	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
seznámení	seznámení	k1gNnSc2	seznámení
se	s	k7c7	s
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
hosty	host	k1gMnPc7	host
byla	být	k5eAaImAgFnS	být
společenská	společenský	k2eAgFnSc1d1	společenská
nezbytnost	nezbytnost	k1gFnSc1	nezbytnost
<g/>
,	,	kIx,	,
Curyšané	Curyšan	k1gMnPc1	Curyšan
mají	mít	k5eAaImIp3nP	mít
oporu	opora	k1gFnSc4	opora
na	na	k7c6	na
Curyšské	curyšský	k2eAgFnSc6d1	curyšská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kliniku	klinika	k1gFnSc4	klinika
(	(	kIx(	(
<g/>
Burghölzli	Burghölzli	k1gFnSc4	Burghölzli
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadní	usnadnit	k5eAaPmIp3nS	usnadnit
průnik	průnik	k1gInSc1	průnik
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
do	do	k7c2	do
vědeckého	vědecký	k2eAgInSc2d1	vědecký
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
nevyhlášeným	vyhlášený	k2eNgMnSc7d1	nevyhlášený
vůdcem	vůdce	k1gMnSc7	vůdce
nespokojenců	nespokojenec	k1gMnPc2	nespokojenec
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
a	a	k8xC	a
krize	krize	k1gFnSc1	krize
byla	být	k5eAaImAgFnS	být
načas	načas	k6eAd1	načas
zažehnána	zažehnán	k2eAgFnSc1d1	zažehnána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
však	však	k9	však
znovu	znovu	k6eAd1	znovu
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
předsedající	předsedající	k1gMnSc1	předsedající
kongresu	kongres	k1gInSc2	kongres
Ferenczi	Ferencze	k1gFnSc4	Ferencze
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
(	(	kIx(	(
<g/>
na	na	k7c4	na
Freudovo	Freudův	k2eAgNnSc4d1	Freudovo
přání	přání	k1gNnSc4	přání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
doživotním	doživotní	k2eAgMnSc7d1	doživotní
prezidentem	prezident	k1gMnSc7	prezident
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc2d1	vznikající
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
společnosti	společnost	k1gFnSc2	společnost
stal	stát	k5eAaPmAgMnS	stát
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
Adlera	Adler	k1gMnSc2	Adler
a	a	k8xC	a
Stekela	Stekel	k1gMnSc2	Stekel
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
bouři	bouře	k1gFnSc4	bouře
nevole	nevole	k1gFnSc2	nevole
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
musel	muset	k5eAaImAgInS	muset
Ferenczi	Ferencze	k1gFnSc4	Ferencze
jednání	jednání	k1gNnSc2	jednání
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenci	nespokojenec	k1gMnPc1	nespokojenec
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
ve	v	k7c6	v
Stekelově	Stekelův	k2eAgInSc6d1	Stekelův
pokoji	pokoj	k1gInSc6	pokoj
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
demonstrativním	demonstrativní	k2eAgInSc6d1	demonstrativní
odjezdu	odjezd	k1gInSc6	odjezd
z	z	k7c2	z
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
pokoj	pokoj	k1gInSc4	pokoj
a	a	k8xC	a
přednesl	přednést	k5eAaPmAgMnS	přednést
dramatický	dramatický	k2eAgInSc4d1	dramatický
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nebude	být	k5eNaImBp3nS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
psychoanalytického	psychoanalytický	k2eAgNnSc2d1	psychoanalytické
hnutí	hnutí	k1gNnSc2	hnutí
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
sežehne	sežehnout	k5eAaPmIp3nS	sežehnout
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňané	Vídeňan	k1gMnPc1	Vídeňan
si	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
vynutili	vynutit	k5eAaPmAgMnP	vynutit
kompromis	kompromis	k1gInSc4	kompromis
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jung	Jung	k1gMnSc1	Jung
bude	být	k5eAaImBp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jen	jen	k9	jen
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
dva	dva	k4xCgInPc4	dva
taktické	taktický	k2eAgInPc4d1	taktický
kroky	krok	k1gInPc4	krok
<g/>
:	:	kIx,	:
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
jako	jako	k8xS	jako
předsedu	předseda	k1gMnSc4	předseda
Adlera	Adler	k1gMnSc4	Adler
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vznik	vznik	k1gInSc4	vznik
psychoanalytického	psychoanalytický	k2eAgInSc2d1	psychoanalytický
měsíčníku	měsíčník	k1gInSc2	měsíčník
Zentralblatt	Zentralblatt	k1gMnSc1	Zentralblatt
für	für	k?	für
Psychoanalyse	psychoanalysa	k1gFnSc6	psychoanalysa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nP	by
vedli	vést	k5eAaImAgMnP	vést
Stekel	Stekel	k1gMnSc1	Stekel
a	a	k8xC	a
Adler	Adler	k1gMnSc1	Adler
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
nadšení	nadšení	k1gNnSc1	nadšení
a	a	k8xC	a
schizma	schizma	k1gNnSc1	schizma
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
podařilo	podařit	k5eAaPmAgNnS	podařit
odvrátit	odvrátit	k5eAaPmF	odvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ne	ne	k9	ne
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
byl	být	k5eAaImAgInS	být
Freud	Freud	k1gInSc4	Freud
ochoten	ochoten	k2eAgMnSc1d1	ochoten
Adlerovi	Adler	k1gMnSc3	Adler
ustupovat	ustupovat	k5eAaImF	ustupovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
politických	politický	k2eAgFnPc6d1	politická
<g/>
"	"	kIx"	"
tahanicích	tahanice	k1gFnPc6	tahanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
teoretických	teoretický	k2eAgFnPc6d1	teoretická
otázkách	otázka	k1gFnPc6	otázka
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
norimberském	norimberský	k2eAgInSc6d1	norimberský
kongresu	kongres	k1gInSc6	kongres
Adler	Adler	k1gMnSc1	Adler
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Lékařského	lékařský	k2eAgNnSc2d1	lékařské
kolegia	kolegium	k1gNnSc2	kolegium
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
přemístil	přemístit	k5eAaPmAgInS	přemístit
setkání	setkání	k1gNnSc4	setkání
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
přednášku	přednáška	k1gFnSc4	přednáška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zcela	zcela	k6eAd1	zcela
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Freudovu	Freudův	k2eAgFnSc4d1	Freudova
sexuální	sexuální	k2eAgFnSc4d1	sexuální
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
již	již	k6eAd1	již
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
proti	proti	k7c3	proti
Adlerovi	Adler	k1gMnSc3	Adler
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Překvapený	překvapený	k2eAgMnSc1d1	překvapený
Adler	Adler	k1gMnSc1	Adler
opustil	opustit	k5eAaPmAgMnS	opustit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
přednáškovou	přednáškový	k2eAgFnSc4d1	přednášková
místnost	místnost	k1gFnSc4	místnost
a	a	k8xC	a
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
odešel	odejít	k5eAaPmAgMnS	odejít
i	i	k9	i
Stekel	Stekel	k1gMnSc1	Stekel
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
osm	osm	k4xCc1	osm
jeho	jeho	k3xOp3gMnPc2	jeho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
nejprve	nejprve	k6eAd1	nejprve
Společnost	společnost	k1gFnSc4	společnost
pro	pro	k7c4	pro
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Adler	Adler	k1gMnSc1	Adler
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pojem	pojem	k1gInSc4	pojem
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
vůbec	vůbec	k9	vůbec
nepoužívat	používat	k5eNaImF	používat
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
školu	škola	k1gFnSc4	škola
zvanou	zvaný	k2eAgFnSc4d1	zvaná
individuální	individuální	k2eAgFnSc4d1	individuální
psychologie	psychologie	k1gFnPc4	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
následoval	následovat	k5eAaImAgInS	následovat
i	i	k9	i
rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
Curyšany	Curyšan	k1gInPc7	Curyšan
<g/>
.	.	kIx.	.
</s>
<s>
Freuda	Freud	k1gMnSc4	Freud
nejprve	nejprve	k6eAd1	nejprve
znepokojilo	znepokojit	k5eAaPmAgNnS	znepokojit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jung	Jung	k1gMnSc1	Jung
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
Bleulerem	Bleuler	k1gMnSc7	Bleuler
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
Freud	Freud	k1gMnSc1	Freud
přál	přát	k5eAaImAgMnS	přát
vždy	vždy	k6eAd1	vždy
mít	mít	k5eAaImF	mít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
názory	názor	k1gInPc1	názor
lišily	lišit	k5eAaImAgInP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
Jung	Jung	k1gMnSc1	Jung
začal	začít	k5eAaPmAgMnS	začít
vzdalovat	vzdalovat	k5eAaImF	vzdalovat
i	i	k9	i
ideově	ideově	k6eAd1	ideově
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
práci	práce	k1gFnSc4	práce
Proměny	proměna	k1gFnSc2	proměna
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
libida	libido	k1gNnSc2	libido
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Freudovu	Freudův	k2eAgFnSc4d1	Freudova
sexuální	sexuální	k2eAgFnSc4d1	sexuální
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
neměla	mít	k5eNaImAgFnS	mít
proti	proti	k7c3	proti
náboženství	náboženství	k1gNnSc3	náboženství
bojovat	bojovat	k5eAaImF	bojovat
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
náboženstvím	náboženství	k1gNnSc7	náboženství
novým	nový	k2eAgNnSc7d1	nové
<g/>
,	,	kIx,	,
Freud	Freudo	k1gNnPc2	Freudo
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
přerušil	přerušit	k5eAaPmAgMnS	přerušit
styky	styk	k1gInPc7	styk
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
pak	pak	k6eAd1	pak
založil	založit	k5eAaPmAgMnS	založit
analytickou	analytický	k2eAgFnSc4d1	analytická
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
Jungem	Jung	k1gMnSc7	Jung
a	a	k8xC	a
Adlerem	Adler	k1gMnSc7	Adler
nicméně	nicméně	k8xC	nicméně
Freuda	Freuda	k1gMnSc1	Freuda
přiměl	přimět	k5eAaPmAgMnS	přimět
k	k	k7c3	k
obohacení	obohacení	k1gNnSc3	obohacení
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
<g/>
:	:	kIx,	:
Jungův	Jungův	k2eAgInSc1d1	Jungův
koncept	koncept	k1gInSc1	koncept
"	"	kIx"	"
<g/>
introverze	introverze	k1gFnSc1	introverze
<g/>
/	/	kIx~	/
<g/>
extroverze	extroverze	k1gFnSc1	extroverze
libida	libido	k1gNnSc2	libido
<g/>
"	"	kIx"	"
a	a	k8xC	a
Adlerův	Adlerův	k2eAgInSc1d1	Adlerův
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
egoistické	egoistický	k2eAgInPc4d1	egoistický
pudy	pud	k1gInPc4	pud
ho	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgInS	přivést
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
teorie	teorie	k1gFnSc2	teorie
narcismu	narcismus	k1gInSc2	narcismus
(	(	kIx(	(
<g/>
definice	definice	k1gFnSc1	definice
v	v	k7c6	v
článku	článek	k1gInSc6	článek
K	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
narcismu	narcismus	k1gInSc2	narcismus
<g/>
,	,	kIx,	,
rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
teorie	teorie	k1gFnSc2	teorie
v	v	k7c6	v
textech	text	k1gInPc6	text
Pudy	pud	k1gInPc4	pud
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
osudy	osud	k1gInPc1	osud
a	a	k8xC	a
Smutek	smutek	k1gInSc1	smutek
a	a	k8xC	a
melancholie	melancholie	k1gFnSc1	melancholie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jungova	Jungův	k2eAgFnSc1d1	Jungova
teorie	teorie	k1gFnSc1	teorie
archetypu	archetyp	k1gInSc2	archetyp
také	také	k9	také
Freuda	Freuda	k1gFnSc1	Freuda
vyburcovala	vyburcovat	k5eAaPmAgFnS	vyburcovat
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
Totemu	totem	k1gInSc2	totem
a	a	k8xC	a
tabu	tabu	k1gNnSc1	tabu
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
originální	originální	k2eAgFnSc4d1	originální
spekulaci	spekulace	k1gFnSc4	spekulace
o	o	k7c6	o
"	"	kIx"	"
<g/>
vraždě	vražda	k1gFnSc6	vražda
primordiálního	primordiální	k2eAgMnSc2d1	primordiální
otce	otec	k1gMnSc2	otec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
popudem	popud	k1gInSc7	popud
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
díky	díky	k7c3	díky
významných	významný	k2eAgNnPc2d1	významné
úspěchům	úspěch	k1gInPc3	úspěch
psychoanalytiků	psychoanalytik	k1gMnPc2	psychoanalytik
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
válečných	válečný	k2eAgFnPc2d1	válečná
neuróz	neuróza	k1gFnPc2	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Zážitek	zážitek	k1gInSc4	zážitek
války	válka	k1gFnSc2	válka
také	také	k9	také
Freuda	Freuda	k1gMnSc1	Freuda
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
k	k	k7c3	k
definování	definování	k1gNnSc3	definování
tzv.	tzv.	kA	tzv.
pudu	pud	k1gInSc2	pud
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spise	spis	k1gInSc6	spis
Mimo	mimo	k7c4	mimo
princip	princip	k1gInSc4	princip
slasti	slast	k1gFnSc2	slast
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jeho	jeho	k3xOp3gMnSc3	jeho
pudová	pudový	k2eAgFnSc1d1	pudová
teorie	teorie	k1gFnSc1	teorie
dostala	dostat	k5eAaPmAgFnS	dostat
třetí	třetí	k4xOgNnSc1	třetí
epicentrum	epicentrum	k1gNnSc1	epicentrum
(	(	kIx(	(
<g/>
po	po	k7c6	po
libidu	libido	k1gNnSc6	libido
a	a	k8xC	a
narcismu	narcismus	k1gInSc6	narcismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Freudově	Freudův	k2eAgInSc6d1	Freudův
životě	život	k1gInSc6	život
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
klíčový	klíčový	k2eAgInSc1d1	klíčový
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
vydal	vydat	k5eAaPmAgMnS	vydat
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
spisů	spis	k1gInPc2	spis
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
Ono	onen	k3xDgNnSc1	onen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
tzv.	tzv.	kA	tzv.
strukturální	strukturální	k2eAgInSc4d1	strukturální
model	model	k1gInSc4	model
psýchy	psýcha	k1gFnSc2	psýcha
(	(	kIx(	(
<g/>
ego	ego	k1gNnSc1	ego
<g/>
,	,	kIx,	,
superego	superego	k1gMnSc1	superego
a	a	k8xC	a
id	idy	k1gFnPc2	idy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
posléze	posléze	k6eAd1	posléze
zdaleka	zdaleka	k6eAd1	zdaleka
nejvlivnějším	vlivný	k2eAgInSc7d3	nejvlivnější
jeho	jeho	k3xOp3gInSc7	jeho
konceptem	koncept	k1gInSc7	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ovšem	ovšem	k9	ovšem
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
u	u	k7c2	u
něho	on	k3xPp3gInSc2	on
propukla	propuknout	k5eAaPmAgFnS	propuknout
rakovina	rakovina	k1gFnSc1	rakovina
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
a	a	k8xC	a
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
se	se	k3xPyFc4	se
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
si	se	k3xPyFc3	se
předsevzal	předsevzít	k5eAaPmAgMnS	předsevzít
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
zaobírat	zaobírat	k5eAaImF	zaobírat
již	již	k6eAd1	již
jen	jen	k9	jen
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
baví	bavit	k5eAaImIp3nS	bavit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byly	být	k5eAaImAgFnP	být
kulturně-psychologické	kulturněsychologický	k2eAgFnPc4d1	kulturně-psychologický
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
skutečně	skutečně	k6eAd1	skutečně
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
poslední	poslední	k2eAgFnSc4d1	poslední
fázi	fáze	k1gFnSc4	fáze
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
(	(	kIx(	(
<g/>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
jedné	jeden	k4xCgFnSc2	jeden
iluze	iluze	k1gFnSc2	iluze
<g/>
,	,	kIx,	,
Nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
a	a	k8xC	a
monoteistické	monoteistický	k2eAgNnSc1d1	monoteistické
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
neochabla	ochabnout	k5eNaPmAgFnS	ochabnout
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
byla	být	k5eAaImAgFnS	být
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
vystavena	vystavit	k5eAaPmNgFnS	vystavit
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Freud	Freud	k1gMnSc1	Freud
i	i	k8xC	i
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
:	:	kIx,	:
roku	rok	k1gInSc6	rok
1930	[number]	k4	1930
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
Goethovu	Goethův	k2eAgFnSc4d1	Goethova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nikoli	nikoli	k9	nikoli
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
objevy	objev	k1gInPc4	objev
psychologické	psychologický	k2eAgInPc4d1	psychologický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
sloh	sloh	k1gInSc4	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
přebírala	přebírat	k5eAaImAgFnS	přebírat
dcera	dcera	k1gFnSc1	dcera
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
přečetla	přečíst	k5eAaPmAgFnS	přečíst
i	i	k9	i
děkovnou	děkovný	k2eAgFnSc4d1	děkovná
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Freud	Freud	k1gInSc1	Freud
po	po	k7c6	po
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
amputaci	amputace	k1gFnSc6	amputace
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
nemohl	moct	k5eNaImAgInS	moct
téměř	téměř	k6eAd1	téměř
mluvit	mluvit	k5eAaImF	mluvit
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
léčil	léčit	k5eAaImAgMnS	léčit
pacienty	pacient	k1gMnPc4	pacient
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
četla	číst	k5eAaImAgFnS	číst
Anna	Anna	k1gFnSc1	Anna
děkovnou	děkovný	k2eAgFnSc4d1	děkovná
řeč	řeč	k1gFnSc4	řeč
i	i	k9	i
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1931	[number]	k4	1931
při	při	k7c6	při
odhalování	odhalování	k1gNnSc6	odhalování
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
na	na	k7c6	na
Freudově	Freudův	k2eAgInSc6d1	Freudův
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
rodišti	rodiště	k1gNnSc6	rodiště
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
Přestože	přestože	k8xS	přestože
institucionálních	institucionální	k2eAgFnPc2d1	institucionální
poct	pocta	k1gFnPc2	pocta
nebylo	být	k5eNaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
Freud	Freud	k1gMnSc1	Freud
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
úctu	úcta	k1gFnSc4	úcta
mezi	mezi	k7c7	mezi
mnohými	mnohý	k2eAgInPc7d1	mnohý
intelektuálními	intelektuální	k2eAgInPc7d1	intelektuální
a	a	k8xC	a
uměleckými	umělecký	k2eAgInPc7d1	umělecký
velikány	velikán	k1gMnPc4	velikán
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Freudových	Freudový	k2eAgInPc2d1	Freudový
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
sepsali	sepsat	k5eAaPmAgMnP	sepsat
zdravici	zdravice	k1gFnSc4	zdravice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
položil	položit	k5eAaPmAgMnS	položit
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neztratí	ztratit	k5eNaPmIp3nP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
vědění	vědění	k1gNnSc3	vědění
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
natrvalo	natrvalo	k6eAd1	natrvalo
zapírat	zapírat	k5eAaImF	zapírat
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
zůstane	zůstat	k5eAaPmIp3nS	zůstat
nějaký	nějaký	k3yIgInSc4	nějaký
čin	čin	k1gInSc4	čin
naší	náš	k3xOp1gFnSc2	náš
rasy	rasa	k1gFnSc2	rasa
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
průnik	průnik	k1gInSc1	průnik
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
lidské	lidský	k2eAgFnSc2d1	lidská
mysli	mysl	k1gFnSc2	mysl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc4	dopis
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
200	[number]	k4	200
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
<g/>
,	,	kIx,	,
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
či	či	k8xC	či
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
osobním	osobní	k2eAgInSc7d1	osobní
dopisem	dopis	k1gInSc7	dopis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
šťasten	šťasten	k2eAgMnSc1d1	šťasten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vám	vy	k3xPp2nPc3	vy
naše	náš	k3xOp1gFnSc1	náš
generace	generace	k1gFnSc1	generace
může	moct	k5eAaImIp3nS	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
úctu	úcta	k1gFnSc4	úcta
jako	jako	k8xC	jako
jednomu	jeden	k4xCgMnSc3	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
největších	veliký	k2eAgMnPc2d3	veliký
učitelů	učitel	k1gMnPc2	učitel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Freudovi	Freuda	k1gMnSc3	Freuda
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
dopřáno	dopřát	k5eAaPmNgNnS	dopřát
dožít	dožít	k5eAaPmF	dožít
ověnčen	ověnčen	k2eAgInSc1d1	ověnčen
poctami	pocta	k1gFnPc7	pocta
a	a	k8xC	a
obklopen	obklopen	k2eAgInSc4d1	obklopen
úctou	úcta	k1gFnSc7	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
politická	politický	k2eAgFnSc1d1	politická
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
:	:	kIx,	:
nacismus	nacismus	k1gInSc1	nacismus
–	–	k?	–
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1933	[number]	k4	1933
nacisté	nacista	k1gMnPc1	nacista
pálili	pálit	k5eAaImAgMnP	pálit
Freudovy	Freudův	k2eAgFnPc4d1	Freudova
knihy	kniha	k1gFnPc4	kniha
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
to	ten	k3xDgNnSc4	ten
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jaké	jaký	k3yRgFnPc1	jaký
děláme	dělat	k5eAaImIp1nP	dělat
pokroky	pokrok	k1gInPc4	pokrok
<g/>
!	!	kIx.	!
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
by	by	kYmCp3nP	by
mne	já	k3xPp1nSc4	já
byli	být	k5eAaImAgMnP	být
upálili	upálit	k5eAaPmAgMnP	upálit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
spokojují	spokojovat	k5eAaImIp3nP	spokojovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pálí	pálit	k5eAaImIp3nP	pálit
moje	můj	k3xOp1gFnPc1	můj
knihy	kniha	k1gFnPc1	kniha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
jeho	jeho	k3xOp3gFnSc4	jeho
psychologii	psychologie	k1gFnSc4	psychologie
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
zvrhlou	zvrhlý	k2eAgFnSc4d1	zvrhlá
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
nacismus	nacismus	k1gInSc1	nacismus
nemůže	moct	k5eNaImIp3nS	moct
uchytit	uchytit	k5eAaPmF	uchytit
a	a	k8xC	a
že	že	k8xS	že
si	se	k3xPyFc3	se
Hitler	Hitler	k1gMnSc1	Hitler
netroufne	troufnout	k5eNaPmIp3nS	troufnout
Rakousko	Rakousko	k1gNnSc4	Rakousko
okupovat	okupovat	k5eAaBmF	okupovat
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
bránil	bránit	k5eAaImAgMnS	bránit
emigraci	emigrace	k1gFnSc4	emigrace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zvolila	zvolit	k5eAaPmAgFnS	zvolit
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgMnPc2d1	jiný
analytiků	analytik	k1gMnPc2	analytik
(	(	kIx(	(
<g/>
prchali	prchat	k5eAaImAgMnP	prchat
zejména	zejména	k9	zejména
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
novými	nový	k2eAgInPc7d1	nový
centry	centrum	k1gNnPc7	centrum
psychoanalytického	psychoanalytický	k2eAgNnSc2d1	psychoanalytické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
anšlusu	anšlus	k1gInSc6	anšlus
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
utéct	utéct	k5eAaPmF	utéct
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
nacisté	nacista	k1gMnPc1	nacista
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
již	již	k6eAd1	již
nechtěli	chtít	k5eNaImAgMnP	chtít
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Pomohla	pomoct	k5eAaPmAgFnS	pomoct
analytička	analytička	k1gFnSc1	analytička
a	a	k8xC	a
řecká	řecký	k2eAgFnSc1d1	řecká
princezna	princezna	k1gFnSc1	princezna
Marie	Marie	k1gFnSc1	Marie
Bonapartová	Bonapartová	k1gFnSc1	Bonapartová
<g/>
,	,	kIx,	,
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
kroky	krok	k1gInPc4	krok
USA	USA	kA	USA
a	a	k8xC	a
přímluva	přímluva	k1gFnSc1	přímluva
italského	italský	k2eAgMnSc2d1	italský
fašistického	fašistický	k2eAgMnSc2d1	fašistický
vůdce	vůdce	k1gMnSc2	vůdce
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
si	se	k3xPyFc3	se
Freuda	Freuda	k1gMnSc1	Freuda
vážil	vážit	k5eAaImAgMnS	vážit
–	–	k?	–
Freud	Freud	k1gMnSc1	Freud
mohl	moct	k5eAaImAgMnS	moct
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
odjet	odjet	k5eAaPmF	odjet
i	i	k9	i
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Annou	Anna	k1gFnSc7	Anna
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
významnou	významný	k2eAgFnSc7d1	významná
analytičkou	analytička	k1gFnSc7	analytička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
čtyři	čtyři	k4xCgFnPc1	čtyři
Freudovy	Freudův	k2eAgFnPc1d1	Freudova
sestry	sestra	k1gFnPc1	sestra
však	však	k9	však
zahynuly	zahynout	k5eAaPmAgFnP	zahynout
během	během	k7c2	během
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
odmítal	odmítat	k5eAaImAgInS	odmítat
bolest	bolest	k1gFnSc4	bolest
utišující	utišující	k2eAgInPc4d1	utišující
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
jasně	jasně	k6eAd1	jasně
myslet	myslet	k5eAaImF	myslet
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
staly	stát	k5eAaPmAgFnP	stát
bolesti	bolest	k1gFnPc1	bolest
neúnosné	únosný	k2eNgFnPc1d1	neúnosná
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
Freud	Freud	k1gInSc4	Freud
svého	svůj	k3xOyFgMnSc2	svůj
ošetřujícího	ošetřující	k2eAgMnSc2d1	ošetřující
lékaře	lékař	k1gMnSc2	lékař
Maxe	Max	k1gMnSc2	Max
Schura	Schur	k1gMnSc2	Schur
o	o	k7c4	o
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
dávku	dávka	k1gFnSc4	dávka
morfinu	morfin	k1gInSc2	morfin
<g/>
.	.	kIx.	.
</s>
<s>
Schur	Schur	k1gMnSc1	Schur
posléze	posléze	k6eAd1	posléze
napsal	napsat	k5eAaBmAgMnS	napsat
Freudovu	Freudův	k2eAgFnSc4d1	Freudova
biografii	biografie	k1gFnSc4	biografie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gInPc4	jeho
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
poslední	poslední	k2eAgFnPc4d1	poslední
chvíle	chvíle	k1gFnPc4	chvíle
přesně	přesně	k6eAd1	přesně
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Schura	Schur	k1gMnSc2	Schur
Freud	Freuda	k1gFnPc2	Freuda
o	o	k7c6	o
eutanazii	eutanazie	k1gFnSc6	eutanazie
požádal	požádat	k5eAaPmAgMnS	požádat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dočetl	dočíst	k5eAaPmAgMnS	dočíst
Balzacovu	Balzacův	k2eAgFnSc4d1	Balzacova
novelu	novela	k1gFnSc4	novela
Šagrénová	šagrénový	k2eAgFnSc1d1	Šagrénová
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
přání	přání	k1gNnSc2	přání
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
Freudovým	Freudový	k2eAgNnSc7d1	Freudový
velkým	velký	k2eAgNnSc7d1	velké
tématem	téma	k1gNnSc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
židovskému	židovský	k2eAgInSc3d1	židovský
zvyku	zvyk	k1gInSc3	zvyk
byl	být	k5eAaImAgMnS	být
Freud	Freud	k1gMnSc1	Freud
zpopelněn	zpopelnit	k5eAaPmNgMnS	zpopelnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
smutečním	smuteční	k2eAgInSc6d1	smuteční
obřadu	obřad	k1gInSc6	obřad
promluvili	promluvit	k5eAaPmAgMnP	promluvit
Ernest	Ernest	k1gMnSc1	Ernest
Jones	Jones	k1gMnSc1	Jones
a	a	k8xC	a
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýraznějším	výrazný	k2eAgMnPc3d3	nejvýraznější
představitelům	představitel	k1gMnPc3	představitel
ateismu	ateismus	k1gInSc2	ateismus
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
vydal	vydat	k5eAaPmAgInS	vydat
článek	článek	k1gInSc4	článek
Nutkavá	nutkavý	k2eAgNnPc4d1	nutkavé
jednání	jednání	k1gNnPc4	jednání
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
úkony	úkon	k1gInPc4	úkon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
náboženský	náboženský	k2eAgInSc4d1	náboženský
ritus	ritus	k1gInSc4	ritus
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
k	k	k7c3	k
rituálům	rituál	k1gInPc3	rituál
nutkavého	nutkavý	k2eAgMnSc4d1	nutkavý
neurotika	neurotik	k1gMnSc4	neurotik
a	a	k8xC	a
náboženství	náboženství	k1gNnSc4	náboženství
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
nutkavou	nutkavý	k2eAgFnSc4d1	nutkavá
neurózu	neuróza	k1gFnSc4	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
útok	útok	k1gInSc1	útok
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
znamenal	znamenat	k5eAaImAgInS	znamenat
spis	spis	k1gInSc1	spis
Totem	totem	k1gInSc1	totem
a	a	k8xC	a
tabu	tabu	k1gNnSc1	tabu
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Freud	Freud	k1gMnSc1	Freud
popisuje	popisovat	k5eAaImIp3nS	popisovat
vznik	vznik	k1gInSc4	vznik
primárního	primární	k2eAgNnSc2d1	primární
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
totemismu	totemismus	k1gInSc2	totemismus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
odčinění	odčinění	k1gNnSc1	odčinění
prvotní	prvotní	k2eAgFnSc2d1	prvotní
vraždy	vražda	k1gFnSc2	vražda
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
lidských	lidský	k2eAgFnPc6d1	lidská
tlupách	tlupa	k1gFnPc6	tlupa
musela	muset	k5eAaImAgFnS	muset
odehrávat	odehrávat	k5eAaImF	odehrávat
jako	jako	k9	jako
jakýsi	jakýsi	k3yIgInSc4	jakýsi
zakladatelský	zakladatelský	k2eAgInSc4d1	zakladatelský
kulturní	kulturní	k2eAgInSc4d1	kulturní
akt	akt	k1gInSc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
monoteismu	monoteismus	k1gInSc2	monoteismus
však	však	k9	však
dle	dle	k7c2	dle
Freuda	Freud	k1gMnSc2	Freud
není	být	k5eNaImIp3nS	být
ničím	ničí	k3xOyNgMnPc3	ničí
jiným	jiný	k2eAgMnPc3d1	jiný
než	než	k8xS	než
zastřenějším	zastřený	k2eAgMnPc3d2	zastřenější
a	a	k8xC	a
abstraktnějším	abstraktní	k2eAgMnSc7d2	abstraktnější
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
zakódován	zakódovat	k5eAaPmNgInS	zakódovat
i	i	k9	i
v	v	k7c6	v
primitivních	primitivní	k2eAgInPc6d1	primitivní
totemech	totem	k1gInPc6	totem
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
se	se	k3xPyFc4	se
Freud	Freud	k1gInSc1	Freud
vypořádává	vypořádávat	k5eAaImIp3nS	vypořádávat
i	i	k9	i
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
pozdních	pozdní	k2eAgFnPc6d1	pozdní
pracích	práce	k1gFnPc6	práce
–	–	k?	–
Nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
Budoucnost	budoucnost	k1gFnSc1	budoucnost
jedné	jeden	k4xCgFnSc2	jeden
iluze	iluze	k1gFnSc2	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Freud	Freud	k1gInSc1	Freud
náboženství	náboženství	k1gNnSc2	náboženství
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
iluzi	iluze	k1gFnSc4	iluze
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Iluze	iluze	k1gFnSc1	iluze
není	být	k5eNaImIp3nS	být
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
omyl	omyl	k1gInSc1	omyl
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
také	také	k9	také
nutně	nutně	k6eAd1	nutně
být	být	k5eAaImF	být
omylem	omyl	k1gInSc7	omyl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
splnění	splnění	k1gNnSc4	splnění
nejstarších	starý	k2eAgInPc2d3	nejstarší
<g/>
,	,	kIx,	,
nejsilnějších	silný	k2eAgNnPc2d3	nejsilnější
a	a	k8xC	a
nejnaléhavějších	naléhavý	k2eAgNnPc2d3	nejnaléhavější
přání	přání	k1gNnPc2	přání
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
míní	mínit	k5eAaImIp3nS	mínit
přání	přání	k1gNnSc4	přání
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Budoucnosti	budoucnost	k1gFnSc2	budoucnost
jedné	jeden	k4xCgFnSc2	jeden
iluze	iluze	k1gFnSc2	iluze
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
přesto	přesto	k8xC	přesto
snad	snad	k9	snad
lidstvo	lidstvo	k1gNnSc1	lidstvo
jednou	jednou	k6eAd1	jednou
překoná	překonat	k5eAaPmIp3nS	překonat
onu	onen	k3xDgFnSc4	onen
"	"	kIx"	"
<g/>
dětskou	dětský	k2eAgFnSc4d1	dětská
neurózu	neuróza	k1gFnSc4	neuróza
<g/>
"	"	kIx"	"
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
převládne	převládnout	k5eAaPmIp3nS	převládnout
vědecký	vědecký	k2eAgInSc4d1	vědecký
realismus	realismus	k1gInSc4	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
však	však	k9	však
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
Freudovou	Freudový	k2eAgFnSc7d1	Freudový
protináboženskostí	protináboženskost	k1gFnSc7	protináboženskost
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mladších	mladý	k2eAgNnPc6d2	mladší
letech	léto	k1gNnPc6	léto
měl	mít	k5eAaImAgInS	mít
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
pověrám	pověra	k1gFnPc3	pověra
numerologického	numerologický	k2eAgInSc2d1	numerologický
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
i	i	k9	i
jeho	on	k3xPp3gInSc2	on
přenosového	přenosový	k2eAgInSc2d1	přenosový
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
Fliessovi	Fliess	k1gMnSc3	Fliess
–	–	k?	–
ten	ten	k3xDgInSc1	ten
měl	mít	k5eAaImAgInS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
periodických	periodický	k2eAgInPc6d1	periodický
cyklech	cyklus	k1gInPc6	cyklus
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
Freudovi	Freuda	k1gMnSc3	Freuda
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
51	[number]	k4	51
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
tomu	ten	k3xDgNnSc3	ten
dlouho	dlouho	k6eAd1	dlouho
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Jungovi	Jung	k1gMnSc3	Jung
to	ten	k3xDgNnSc1	ten
Freud	Freud	k1gInSc4	Freud
<g/>
,	,	kIx,	,
s	s	k7c7	s
ironií	ironie	k1gFnSc7	ironie
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
specificky	specificky	k6eAd1	specificky
mystickou	mystický	k2eAgFnSc7d1	mystická
povahou	povaha	k1gFnSc7	povaha
svého	svůj	k3xOyFgInSc2	svůj
mysticismu	mysticismus	k1gInSc2	mysticismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Octava	Octava	k1gFnSc1	Octava
Mannoniho	Mannoni	k1gMnSc2	Mannoni
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
analýza	analýza	k1gFnSc1	analýza
pověrečnosti	pověrečnost	k1gFnPc4	pověrečnost
v	v	k7c6	v
Psychopatologii	psychopatologie	k1gFnSc6	psychopatologie
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
analýzou	analýza	k1gFnSc7	analýza
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
pověrečnosti	pověrečnost	k1gFnSc2	pověrečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
řadě	řada	k1gFnSc6	řada
přednášek	přednáška	k1gFnPc2	přednáška
k	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
navíc	navíc	k6eAd1	navíc
Freud	Freud	k1gMnSc1	Freud
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
telepatický	telepatický	k2eAgInSc1d1	telepatický
přenos	přenos	k1gInSc1	přenos
myšlenek	myšlenka	k1gFnPc2	myšlenka
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Bakan	Bakan	k1gMnSc1	Bakan
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kontroverzní	kontroverzní	k2eAgFnSc6d1	kontroverzní
knize	kniha	k1gFnSc6	kniha
dokonce	dokonce	k9	dokonce
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Freud	Freud	k1gMnSc1	Freud
celou	celý	k2eAgFnSc4d1	celá
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studia	studio	k1gNnSc2	studio
tajné	tajný	k2eAgFnSc2d1	tajná
židovské	židovský	k2eAgFnSc2d1	židovská
mystické	mystický	k2eAgFnSc2d1	mystická
nauky	nauka	k1gFnSc2	nauka
–	–	k?	–
kabaly	kabala	k1gFnSc2	kabala
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
většina	většina	k1gFnSc1	většina
životopisců	životopisec	k1gMnPc2	životopisec
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nevěrohodné	věrohodný	k2eNgNnSc4d1	nevěrohodné
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
utajovaném	utajovaný	k2eAgNnSc6d1	utajované
ovlivnění	ovlivnění	k1gNnSc6	ovlivnění
okultismem	okultismus	k1gInSc7	okultismus
někdy	někdy	k6eAd1	někdy
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Freud	Freud	k1gInSc4	Freud
byl	být	k5eAaImAgMnS	být
člen	člen	k1gMnSc1	člen
židovské	židovský	k2eAgFnSc2d1	židovská
odnože	odnož	k1gFnSc2	odnož
zednářského	zednářský	k2eAgNnSc2d1	zednářské
hnutí	hnutí	k1gNnSc2	hnutí
–	–	k?	–
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
naj	naj	k?	naj
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
rit	rit	k1gInSc1	rit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
členství	členství	k1gNnSc1	členství
mělo	mít	k5eAaImAgNnS	mít
však	však	k9	však
spíše	spíše	k9	spíše
význam	význam	k1gInSc4	význam
identifikace	identifikace	k1gFnSc2	identifikace
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
národností	národnost	k1gFnSc7	národnost
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
spolek	spolek	k1gInSc1	spolek
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
naj	naj	k?	naj
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
rit	rit	k1gInSc1	rit
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
kdy	kdy	k6eAd1	kdy
pozval	pozvat	k5eAaPmAgMnS	pozvat
k	k	k7c3	k
přednášce	přednáška	k1gFnSc3	přednáška
o	o	k7c6	o
psychoanalytických	psychoanalytický	k2eAgFnPc6d1	psychoanalytická
teoriích	teorie	k1gFnPc6	teorie
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
všude	všude	k6eAd1	všude
jinde	jinde	k6eAd1	jinde
nevítaným	vítaný	k2eNgMnSc7d1	nevítaný
hostem	host	k1gMnSc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
velmi	velmi	k6eAd1	velmi
cenil	cenit	k5eAaImAgMnS	cenit
<g/>
.	.	kIx.	.
</s>
<s>
Freudova	Freudův	k2eAgFnSc1d1	Freudova
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
obviňována	obviňován	k2eAgFnSc1d1	obviňována
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
novým	nový	k2eAgNnSc7d1	nové
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Muzikolog	muzikolog	k1gMnSc1	muzikolog
Max	Max	k1gMnSc1	Max
Graf	graf	k1gInSc1	graf
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
Freudových	Freudový	k2eAgMnPc2d1	Freudový
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
vzniku	vznik	k1gInSc2	vznik
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
skutečně	skutečně	k6eAd1	skutečně
připomínala	připomínat	k5eAaImAgFnS	připomínat
"	"	kIx"	"
<g/>
založení	založení	k1gNnSc1	založení
nového	nový	k2eAgNnSc2d1	nové
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
jsem	být	k5eAaImIp1nS	být
prožil	prožít	k5eAaPmAgMnS	prožít
všechny	všechen	k3xTgFnPc4	všechen
etapy	etapa	k1gFnPc4	etapa
dějin	dějiny	k1gFnPc2	dějiny
církve	církev	k1gFnSc2	církev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Graf	graf	k1gInSc4	graf
a	a	k8xC	a
mínil	mínit	k5eAaImAgMnS	mínit
především	především	k6eAd1	především
sérii	série	k1gFnSc4	série
exkomunikací	exkomunikace	k1gFnPc2	exkomunikace
z	z	k7c2	z
Freudova	Freudův	k2eAgInSc2d1	Freudův
nejužšího	úzký	k2eAgInSc2d3	nejužší
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Připomínán	připomínán	k2eAgMnSc1d1	připomínán
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
také	také	k9	také
Freudův	Freudův	k2eAgInSc4d1	Freudův
dogmatismus	dogmatismus	k1gInSc4	dogmatismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Jonese	Jonese	k1gFnSc2	Jonese
jednou	jednou	k6eAd1	jednou
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vlastníme	vlastnit	k5eAaImIp1nP	vlastnit
pravdu	pravda	k1gFnSc4	pravda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
označoval	označovat	k5eAaImAgMnS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
politickou	politický	k2eAgFnSc4d1	politická
nulu	nula	k1gFnSc4	nula
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řekl	říct	k5eAaPmAgMnS	říct
to	ten	k3xDgNnSc4	ten
například	například	k6eAd1	například
Maxi	maxi	k6eAd1	maxi
Eastmanovi	Eastmanův	k2eAgMnPc1d1	Eastmanův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
příliš	příliš	k6eAd1	příliš
neorientuje	orientovat	k5eNaBmIp3nS	orientovat
a	a	k8xC	a
málokdy	málokdy	k6eAd1	málokdy
odhadne	odhadnout	k5eAaPmIp3nS	odhadnout
vývoj	vývoj	k1gInSc4	vývoj
událostí	událost	k1gFnPc2	událost
–	–	k?	–
zmýlil	zmýlit	k5eAaPmAgMnS	zmýlit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
pravděpodobný	pravděpodobný	k2eAgInSc4d1	pravděpodobný
vznik	vznik	k1gInSc4	vznik
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zmýlil	zmýlit	k5eAaPmAgMnS	zmýlit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
poté	poté	k6eAd1	poté
očekával	očekávat	k5eAaImAgInS	očekávat
snadné	snadný	k2eAgNnSc4d1	snadné
vítězství	vítězství	k1gNnSc4	vítězství
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
atp.	atp.	kA	atp.
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
podporoval	podporovat	k5eAaImAgMnS	podporovat
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
sociální	sociální	k2eAgFnSc4d1	sociální
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
vůdce	vůdce	k1gMnSc1	vůdce
Viktor	Viktor	k1gMnSc1	Viktor
Adler	Adler	k1gMnSc1	Adler
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
osobním	osobní	k2eAgMnSc7d1	osobní
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
koupil	koupit	k5eAaPmAgInS	koupit
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
dokonce	dokonce	k9	dokonce
byt	byt	k1gInSc4	byt
na	na	k7c6	na
Berggasse	Berggass	k1gInSc6	Berggass
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebýt	být	k5eNaImF	být
Adlerova	Adlerův	k2eAgFnSc1d1	Adlerova
předčasného	předčasný	k2eAgInSc2d1	předčasný
skonu	skon	k1gInSc2	skon
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
Rakousko	Rakousko	k1gNnSc4	Rakousko
zachránit	zachránit	k5eAaPmF	zachránit
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jejich	jejich	k3xOp3gNnSc1	jejich
přátelství	přátelství	k1gNnSc1	přátelství
začalo	začít	k5eAaPmAgNnS	začít
prudkým	prudký	k2eAgInSc7d1	prudký
sporem	spor	k1gInSc7	spor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
málem	málem	k6eAd1	málem
skončil	skončit	k5eAaPmAgInS	skončit
soubojem	souboj	k1gInSc7	souboj
–	–	k?	–
Freud	Freud	k1gInSc1	Freud
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
při	při	k7c6	při
studentské	studentský	k2eAgFnSc6d1	studentská
hádce	hádka	k1gFnSc6	hádka
o	o	k7c4	o
materialismus	materialismus	k1gInSc4	materialismus
(	(	kIx(	(
<g/>
jejž	jenž	k3xRgMnSc4	jenž
už	už	k6eAd1	už
jako	jako	k9	jako
mladík	mladík	k1gMnSc1	mladík
vyznával	vyznávat	k5eAaImAgMnS	vyznávat
<g/>
)	)	kIx)	)
Adlera	Adler	k1gMnSc2	Adler
urazil	urazit	k5eAaPmAgInS	urazit
poznámkou	poznámka	k1gFnSc7	poznámka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdysi	kdysi	k6eAd1	kdysi
pásl	pásnout	k5eAaImAgMnS	pásnout
vepře	vepř	k1gMnSc4	vepř
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Adler	Adler	k1gMnSc1	Adler
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Freuda	Freuda	k1gMnSc1	Freuda
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
zachoval	zachovat	k5eAaPmAgInS	zachovat
chladnou	chladný	k2eAgFnSc4d1	chladná
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velmi	velmi	k6eAd1	velmi
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Heinrich	Heinrich	k1gMnSc1	Heinrich
Braun	Braun	k1gMnSc1	Braun
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
sociálnědemokratického	sociálnědemokratický	k2eAgInSc2d1	sociálnědemokratický
deníku	deník	k1gInSc2	deník
Neue	Neu	k1gFnSc2	Neu
Zeit	Zeita	k1gFnPc2	Zeita
<g/>
,	,	kIx,	,
znali	znát	k5eAaImAgMnP	znát
se	se	k3xPyFc4	se
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
ho	on	k3xPp3gNnSc2	on
původně	původně	k6eAd1	původně
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
kteréhož	kteréhož	k?	kteréhož
záměru	záměr	k1gInSc2	záměr
však	však	k9	však
Freud	Freud	k1gMnSc1	Freud
nakonec	nakonec	k6eAd1	nakonec
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
Freud	Freud	k1gMnSc1	Freud
podepsal	podepsat	k5eAaPmAgMnS	podepsat
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
výzvu	výzev	k1gInSc2	výzev
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
osobnostmi	osobnost	k1gFnPc7	osobnost
rakouské	rakouský	k2eAgFnSc2d1	rakouská
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Franz	Franz	k1gMnSc1	Franz
Werfel	Werfel	k1gMnSc1	Werfel
či	či	k8xC	či
Robert	Robert	k1gMnSc1	Robert
Musil	Musil	k1gMnSc1	Musil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
udělili	udělit	k5eAaPmAgMnP	udělit
Freudovi	Freuda	k1gMnSc3	Freuda
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
starosta	starosta	k1gMnSc1	starosta
Karl	Karl	k1gMnSc1	Karl
Seitz	Seitz	k1gMnSc1	Seitz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
vypracování	vypracování	k1gNnSc4	vypracování
reformy	reforma	k1gFnSc2	reforma
školských	školský	k2eAgNnPc2d1	školské
zařízení	zařízení	k1gNnPc2	zařízení
Alfreda	Alfred	k1gMnSc2	Alfred
Adlera	Adler	k1gMnSc2	Adler
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
odpadlých	odpadlý	k2eAgMnPc2d1	odpadlý
Sigmundových	Sigmundových	k2eAgMnPc2d1	Sigmundových
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
Freuda	Freudo	k1gNnSc2	Freudo
rozčílilo	rozčílit	k5eAaPmAgNnS	rozčílit
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
sociálním	sociální	k2eAgMnPc3d1	sociální
demokratům	demokrat	k1gMnPc3	demokrat
tak	tak	k6eAd1	tak
ochladl	ochladnout	k5eAaPmAgMnS	ochladnout
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
si	se	k3xPyFc3	se
vážil	vážit	k5eAaImAgMnS	vážit
také	také	k9	také
císaře	císař	k1gMnSc4	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
jmenování	jmenování	k1gNnSc4	jmenování
antisemity	antisemita	k1gMnSc2	antisemita
Karla	Karel	k1gMnSc2	Karel
Luegera	Luegera	k1gFnSc1	Luegera
vídeňským	vídeňský	k2eAgMnSc7d1	vídeňský
starostou	starosta	k1gMnSc7	starosta
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Freud	Freud	k1gInSc4	Freud
vkládal	vkládat	k5eAaImAgInS	vkládat
určité	určitý	k2eAgFnPc4d1	určitá
naděje	naděje	k1gFnPc4	naděje
do	do	k7c2	do
křesťansko-sociálního	křesťanskoociální	k2eAgNnSc2d1	křesťansko-sociální
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
autoritářského	autoritářský	k2eAgMnSc2d1	autoritářský
kancléře	kancléř	k1gMnSc2	kancléř
Engelberta	Engelbert	k1gMnSc2	Engelbert
Dollfusse	Dollfuss	k1gMnSc2	Dollfuss
–	–	k?	–
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
hráz	hráz	k1gFnSc1	hráz
proti	proti	k7c3	proti
pronikání	pronikání	k1gNnSc3	pronikání
nacismu	nacismus	k1gInSc2	nacismus
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
politických	politický	k2eAgFnPc2d1	politická
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
zmýlil	zmýlit	k5eAaPmAgMnS	zmýlit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
byl	být	k5eAaImAgInS	být
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Benito	Benita	k1gMnSc5	Benita
Mussolinimu	Mussolinimo	k1gNnSc3	Mussolinimo
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nechal	nechat	k5eAaPmAgInS	nechat
provést	provést	k5eAaPmF	provést
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
(	(	kIx(	(
<g/>
archeologie	archeologie	k1gFnSc1	archeologie
Freuda	Freuda	k1gFnSc1	Freuda
fascinovala	fascinovat	k5eAaBmAgFnS	fascinovat
od	od	k7c2	od
školních	školní	k2eAgNnPc2d1	školní
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
mu	on	k3xPp3gMnSc3	on
poslat	poslat	k5eAaPmF	poslat
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
přes	přes	k7c4	přes
jednu	jeden	k4xCgFnSc4	jeden
italskou	italský	k2eAgFnSc4d1	italská
pacientku	pacientka	k1gFnSc4	pacientka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Mussoliniho	Mussolini	k1gMnSc4	Mussolini
osobně	osobně	k6eAd1	osobně
znala	znát	k5eAaImAgFnS	znát
–	–	k?	–
napsal	napsat	k5eAaBmAgMnS	napsat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
věnování	věnování	k1gNnPc1	věnování
"	"	kIx"	"
<g/>
velkému	velký	k2eAgMnSc3d1	velký
kulturnímu	kulturní	k2eAgMnSc3d1	kulturní
hrdinovi	hrdina	k1gMnSc3	hrdina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k9	což
ješitného	ješitný	k2eAgMnSc2d1	ješitný
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
ohromilo	ohromit	k5eAaPmAgNnS	ohromit
a	a	k8xC	a
považoval	považovat	k5eAaImAgInS	považovat
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
nutné	nutný	k2eAgNnSc4d1	nutné
apelovat	apelovat	k5eAaImF	apelovat
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nacisté	nacista	k1gMnPc1	nacista
umožnili	umožnit	k5eAaPmAgMnP	umožnit
Freudovi	Freuda	k1gMnSc3	Freuda
klidný	klidný	k2eAgInSc4d1	klidný
odjezd	odjezd	k1gInSc4	odjezd
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Věnování	věnování	k1gNnSc1	věnování
do	do	k7c2	do
knihy	kniha	k1gFnSc2	kniha
Mussolinimu	Mussolinim	k1gInSc2	Mussolinim
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
filozofa	filozof	k1gMnSc2	filozof
Michela	Michel	k1gMnSc2	Michel
Onfraye	Onfray	k1gMnSc2	Onfray
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyčetl	vyčíst	k5eAaPmAgInS	vyčíst
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
"	"	kIx"	"
<g/>
fašistickému	fašistický	k2eAgInSc3d1	fašistický
césarismu	césarismus	k1gInSc3	césarismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
inspiraci	inspirace	k1gFnSc4	inspirace
dílem	dílo	k1gNnSc7	dílo
Friedricha	Friedrich	k1gMnSc2	Friedrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
je	být	k5eAaImIp3nS	být
však	však	k9	však
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
Junga	Jung	k1gMnSc2	Jung
Freud	Freud	k1gInSc1	Freud
nikdy	nikdy	k6eAd1	nikdy
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
nečetl	číst	k5eNaImAgInS	číst
a	a	k8xC	a
Freudovo	Freudův	k2eAgNnSc1d1	Freudovo
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
dle	dle	k7c2	dle
Junga	Jung	k1gMnSc2	Jung
"	"	kIx"	"
<g/>
šachový	šachový	k2eAgInSc1d1	šachový
tah	tah	k1gInSc1	tah
dějin	dějiny	k1gFnPc2	dějiny
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kompenzoval	kompenzovat	k5eAaBmAgMnS	kompenzovat
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
zbožštění	zbožštění	k1gNnSc2	zbožštění
principu	princip	k1gInSc2	princip
moci	moc	k1gFnSc2	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
byl	být	k5eAaImAgInS	být
též	též	k9	též
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
pacifista	pacifista	k1gMnSc1	pacifista
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
mírový	mírový	k2eAgInSc4d1	mírový
aktivismus	aktivismus	k1gInSc4	aktivismus
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
o	o	k7c4	o
což	což	k3yRnSc4	což
ho	on	k3xPp3gMnSc4	on
Einstein	Einstein	k1gMnSc1	Einstein
osobně	osobně	k6eAd1	osobně
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaImAgMnS	učinit
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nedělal	dělat	k5eNaImAgMnS	dělat
iluze	iluze	k1gFnPc4	iluze
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
zkrocení	zkrocení	k1gNnPc2	zkrocení
lidské	lidský	k2eAgFnSc2d1	lidská
destruktivity	destruktivita	k1gFnSc2	destruktivita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
jeho	jeho	k3xOp3gInSc1	jeho
text	text	k1gInSc4	text
Proč	proč	k6eAd1	proč
válka	válka	k1gFnSc1	válka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
odpovědí	odpověď	k1gFnSc7	odpověď
právě	právě	k9	právě
na	na	k7c4	na
Einsteinovu	Einsteinův	k2eAgFnSc4d1	Einsteinova
výzvu	výzva	k1gFnSc4	výzva
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nakrátko	nakrátko	k6eAd1	nakrátko
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
nacionalistickým	nacionalistický	k2eAgFnPc3d1	nacionalistická
vášním	vášeň	k1gFnPc3	vášeň
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
styděl	stydět	k5eAaImAgInS	stydět
(	(	kIx(	(
<g/>
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
studie	studie	k1gFnSc1	studie
Masová	masový	k2eAgFnSc1d1	masová
psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
analýza	analýza	k1gFnSc1	analýza
Já	já	k3xPp1nSc1	já
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
antiklerikálem	antiklerikál	k1gMnSc7	antiklerikál
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
kritizován	kritizovat	k5eAaImNgInS	kritizovat
konzervativními	konzervativní	k2eAgInPc7d1	konzervativní
kruhy	kruh	k1gInPc7	kruh
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
ho	on	k3xPp3gMnSc4	on
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
bolševismem	bolševismus	k1gInSc7	bolševismus
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
obvinění	obvinění	k1gNnPc1	obvinění
měla	mít	k5eAaImAgNnP	mít
někdy	někdy	k6eAd1	někdy
až	až	k6eAd1	až
kuriózní	kuriózní	k2eAgFnSc4d1	kuriózní
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
vlivný	vlivný	k2eAgMnSc1d1	vlivný
páter	páter	k1gMnSc1	páter
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Schmidt	Schmidt	k1gMnSc1	Schmidt
například	například	k6eAd1	například
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bolševici	bolševik	k1gMnPc1	bolševik
objevili	objevit	k5eAaPmAgMnP	objevit
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
a	a	k8xC	a
zrušili	zrušit	k5eAaPmAgMnP	zrušit
rodinné	rodinný	k2eAgFnPc4d1	rodinná
vazby	vazba	k1gFnPc4	vazba
a	a	k8xC	a
jakákoli	jakýkoli	k3yIgNnPc4	jakýkoli
omezení	omezení	k1gNnPc4	omezení
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
také	také	k9	také
mezi	mezi	k7c7	mezi
sourozenci	sourozenec	k1gMnPc7	sourozenec
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vztah	vztah	k1gInSc1	vztah
Freuda	Freud	k1gMnSc2	Freud
k	k	k7c3	k
marxismu	marxismus	k1gInSc3	marxismus
byl	být	k5eAaImAgMnS	být
však	však	k9	však
poněkud	poněkud	k6eAd1	poněkud
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
shrnul	shrnout	k5eAaPmAgMnS	shrnout
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Nové	Nové	k2eAgFnSc2d1	Nové
řady	řada	k1gFnSc2	řada
přednášek	přednáška	k1gFnPc2	přednáška
k	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
zde	zde	k6eAd1	zde
marxismu	marxismus	k1gInSc2	marxismus
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
realistický	realistický	k2eAgInSc1d1	realistický
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
plodí	plodit	k5eAaImIp3nP	plodit
stejné	stejný	k2eAgFnPc4d1	stejná
iluze	iluze	k1gFnPc4	iluze
jako	jako	k8xC	jako
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vyčítal	vyčítat	k5eAaImAgInS	vyčítat
mu	on	k3xPp3gNnSc3	on
především	především	k9	především
iluzivní	iluzivní	k2eAgNnSc4d1	iluzivní
hodnocení	hodnocení	k1gNnSc4	hodnocení
lidské	lidský	k2eAgFnSc2d1	lidská
povahy	povaha	k1gFnSc2	povaha
jako	jako	k8xC	jako
z	z	k7c2	z
podstaty	podstata	k1gFnSc2	podstata
dobré	dobrý	k2eAgFnSc2d1	dobrá
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
Pocit	pocit	k1gInSc1	pocit
stísněnosti	stísněnost	k1gFnSc2	stísněnost
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
překládáno	překládat	k5eAaImNgNnS	překládat
jako	jako	k8xS	jako
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
)	)	kIx)	)
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Komunisté	komunista	k1gMnPc1	komunista
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nalezli	naleznout	k5eAaPmAgMnP	naleznout
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vysvobození	vysvobození	k1gNnSc3	vysvobození
od	od	k7c2	od
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
nich	on	k3xPp3gMnPc2	on
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dobrý	dobrý	k2eAgMnSc1d1	dobrý
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
ale	ale	k9	ale
zavedení	zavedení	k1gNnSc6	zavedení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
zkazilo	zkazit	k5eAaPmAgNnS	zkazit
jeho	jeho	k3xOp3gFnSc4	jeho
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Nechci	chtít	k5eNaImIp1nS	chtít
kritizovat	kritizovat	k5eAaImF	kritizovat
komunistický	komunistický	k2eAgInSc1d1	komunistický
systém	systém	k1gInSc1	systém
z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
nemohu	moct	k5eNaImIp1nS	moct
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
zrušení	zrušení	k1gNnSc4	zrušení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
účelné	účelný	k2eAgNnSc1d1	účelné
a	a	k8xC	a
prospěšné	prospěšný	k2eAgNnSc1d1	prospěšné
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
dokáži	dokázat	k5eAaPmIp1nS	dokázat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
psychologický	psychologický	k2eAgInSc1d1	psychologický
předpoklad	předpoklad	k1gInSc1	předpoklad
je	být	k5eAaImIp3nS	být
neudržitelnou	udržitelný	k2eNgFnSc7d1	neudržitelná
iluzí	iluze	k1gFnSc7	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Zrušením	zrušení	k1gNnSc7	zrušení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
odebíráme	odebírat	k5eAaImIp1nP	odebírat
lidské	lidský	k2eAgFnPc1d1	lidská
chuti	chuť	k1gFnPc1	chuť
k	k	k7c3	k
agresi	agrese	k1gFnSc3	agrese
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
jistě	jistě	k6eAd1	jistě
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jistě	jistě	k9	jistě
ne	ne	k9	ne
ten	ten	k3xDgInSc1	ten
nejsilnější	silný	k2eAgInSc1d3	nejsilnější
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
I	i	k9	i
mě	já	k3xPp1nSc4	já
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
nepochybné	pochybný	k2eNgNnSc1d1	nepochybné
<g/>
,	,	kIx,	,
že	že	k8xS	že
reálná	reálný	k2eAgFnSc1d1	reálná
změna	změna	k1gFnSc1	změna
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
pomůže	pomoct	k5eAaPmIp3nS	pomoct
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
etický	etický	k2eAgInSc4d1	etický
příkaz	příkaz	k1gInSc4	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
pochopení	pochopení	k1gNnSc1	pochopení
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
socialistů	socialist	k1gMnPc2	socialist
pokaženo	pokazit	k5eAaPmNgNnS	pokazit
a	a	k8xC	a
znehodnoceno	znehodnotit	k5eAaPmNgNnS	znehodnotit
chybným	chybný	k2eAgNnSc7d1	chybné
posouzením	posouzení	k1gNnSc7	posouzení
lidské	lidský	k2eAgFnSc2d1	lidská
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Freud	Freud	k1gMnSc1	Freud
měl	mít	k5eAaImAgMnS	mít
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
ústy	ústa	k1gNnPc7	ústa
André	André	k1gMnSc2	André
Bretona	Breton	k1gMnSc2	Breton
přímo	přímo	k6eAd1	přímo
dovolával	dovolávat	k5eAaImAgInS	dovolávat
–	–	k?	–
Breton	breton	k1gInSc1	breton
koncept	koncept	k1gInSc1	koncept
"	"	kIx"	"
<g/>
automatického	automatický	k2eAgNnSc2d1	automatické
psaní	psaní	k1gNnSc2	psaní
<g/>
"	"	kIx"	"
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studia	studio	k1gNnSc2	studio
Freudových	Freudový	k2eAgMnPc2d1	Freudový
textů	text	k1gInPc2	text
o	o	k7c4	o
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
započalo	započnout	k5eAaPmAgNnS	započnout
roku	rok	k1gInSc3	rok
1916	[number]	k4	1916
a	a	k8xC	a
v	v	k7c6	v
Surrealistickém	surrealistický	k2eAgInSc6d1	surrealistický
manifestu	manifest	k1gInSc6	manifest
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
vděk	vděk	k1gInSc1	vděk
Freudovi	Freuda	k1gMnSc3	Freuda
za	za	k7c4	za
navrácení	navrácení	k1gNnSc4	navrácení
práv	právo	k1gNnPc2	právo
imaginaci	imaginace	k1gFnSc3	imaginace
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
Freuda	Freud	k1gMnSc2	Freud
k	k	k7c3	k
modernímu	moderní	k2eAgNnSc3d1	moderní
umění	umění	k1gNnSc3	umění
byl	být	k5eAaImAgInS	být
však	však	k9	však
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
,	,	kIx,	,
především	především	k9	především
k	k	k7c3	k
modernímu	moderní	k2eAgNnSc3d1	moderní
malířství	malířství	k1gNnSc3	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Oskaru	Oskar	k1gMnSc3	Oskar
Pfisterovi	Pfister	k1gMnSc3	Pfister
při	při	k7c6	při
diskusi	diskuse	k1gFnSc6	diskuse
o	o	k7c6	o
expresionistickém	expresionistický	k2eAgNnSc6d1	expresionistické
malířství	malířství	k1gNnSc6	malířství
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgMnS	mít
byste	by	kYmCp2nP	by
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
jsem	být	k5eAaImIp1nS	být
vůči	vůči	k7c3	vůči
bláznům	blázen	k1gMnPc3	blázen
strašlivě	strašlivě	k6eAd1	strašlivě
netolerantní	tolerantní	k2eNgMnSc1d1	netolerantní
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
těchto	tento	k3xDgMnPc2	tento
'	'	kIx"	'
<g/>
kumštýřů	kumštýř	k1gMnPc2	kumštýř
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
přímo	přímo	k6eAd1	přímo
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
oněch	onen	k3xDgNnPc2	onen
jimi	on	k3xPp3gInPc7	on
hned	hned	k6eAd1	hned
ocejchovaných	ocejchovaný	k2eAgMnPc2d1	ocejchovaný
filistrů	filistr	k1gMnPc2	filistr
a	a	k8xC	a
šosáků	šosák	k1gMnPc2	šosák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Karlu	Karel	k1gMnSc3	Karel
Abrahamovi	Abraham	k1gMnSc3	Abraham
zase	zase	k9	zase
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
moderní	moderní	k2eAgMnPc1d1	moderní
malíři	malíř	k1gMnPc1	malíř
zjevně	zjevně	k6eAd1	zjevně
trpí	trpět	k5eAaImIp3nP	trpět
"	"	kIx"	"
<g/>
vrozenými	vrozený	k2eAgFnPc7d1	vrozená
poruchami	porucha	k1gFnPc7	porucha
zraku	zrak	k1gInSc2	zrak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Breton	Breton	k1gMnSc1	Breton
Freuda	Freuda	k1gMnSc1	Freuda
navštívil	navštívit	k5eAaPmAgMnS	navštívit
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
ze	z	k7c2	z
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
magické	magický	k2eAgFnSc2d1	magická
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
očekával	očekávat	k5eAaImAgInS	očekávat
<g/>
,	,	kIx,	,
potkal	potkat	k5eAaPmAgInS	potkat
prý	prý	k9	prý
"	"	kIx"	"
<g/>
šedou	šedý	k2eAgFnSc4d1	šedá
myš	myš	k1gFnSc4	myš
v	v	k7c6	v
převleku	převlek	k1gInSc6	převlek
za	za	k7c4	za
venkovského	venkovský	k2eAgMnSc4d1	venkovský
lékaře	lékař	k1gMnSc4	lékař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
surrealistického	surrealistický	k2eAgNnSc2d1	surrealistické
hnutí	hnutí	k1gNnSc2	hnutí
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
Breton	breton	k1gInSc4	breton
přesto	přesto	k8xC	přesto
Freuda	Freuda	k1gMnSc1	Freuda
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
patronem	patron	k1gMnSc7	patron
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
spolu	spolu	k6eAd1	spolu
znovu	znovu	k6eAd1	znovu
intenzivně	intenzivně	k6eAd1	intenzivně
korespondovali	korespondovat	k5eAaImAgMnP	korespondovat
o	o	k7c6	o
Bretonově	Bretonův	k2eAgFnSc6d1	Bretonova
nové	nový	k2eAgFnSc6d1	nová
koncepci	koncepce	k1gFnSc6	koncepce
výkladu	výklad	k1gInSc2	výklad
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
Breton	Breton	k1gMnSc1	Breton
Freuda	Freuda	k1gMnSc1	Freuda
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
příspěvek	příspěvek	k1gInSc4	příspěvek
do	do	k7c2	do
surrealistického	surrealistický	k2eAgInSc2d1	surrealistický
sborníku	sborník	k1gInSc2	sborník
o	o	k7c6	o
snech	sen	k1gInPc6	sen
Trajectoire	Trajectoir	k1gInSc5	Trajectoir
du	du	k?	du
rê	rê	k?	rê
<g/>
,	,	kIx,	,
Freud	Freud	k1gMnSc1	Freud
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vykládat	vykládat	k5eAaImF	vykládat
sny	sen	k1gInPc4	sen
bez	bez	k7c2	bez
asociací	asociace	k1gFnPc2	asociace
snícího	snící	k2eAgNnSc2d1	snící
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
aktuální	aktuální	k2eAgFnSc2d1	aktuální
situace	situace	k1gFnSc2	situace
snícího	snící	k2eAgNnSc2d1	snící
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nesmysl	nesmysl	k1gInSc1	nesmysl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Freudově	Freudův	k2eAgInSc6d1	Freudův
odjezdu	odjezd	k1gInSc6	odjezd
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
ho	on	k3xPp3gInSc4	on
tam	tam	k6eAd1	tam
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
navštívila	navštívit	k5eAaPmAgFnS	navštívit
druhá	druhý	k4xOgFnSc1	druhý
velká	velký	k2eAgFnSc1d1	velká
osobnost	osobnost	k1gFnSc1	osobnost
surrealismu	surrealismus	k1gInSc2	surrealismus
-	-	kIx~	-
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Porozuměl	porozumět	k5eAaPmAgMnS	porozumět
si	se	k3xPyFc3	se
s	s	k7c7	s
Freudem	Freud	k1gInSc7	Freud
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
Breton	breton	k1gInSc4	breton
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgMnS	představit
mu	on	k3xPp3gInSc3	on
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
překonávala	překonávat	k5eAaImAgFnS	překonávat
Bretonovo	Bretonův	k2eAgNnSc4d1	Bretonův
"	"	kIx"	"
<g/>
automatické	automatický	k2eAgNnSc4d1	automatické
psaní	psaní	k1gNnSc4	psaní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc1	využití
mechanismů	mechanismus	k1gInPc2	mechanismus
paranoii	paranoia	k1gFnSc4	paranoia
v	v	k7c6	v
kreativitě	kreativita	k1gFnSc6	kreativita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
také	také	k9	také
známý	známý	k2eAgInSc1d1	známý
Freudův	Freudův	k2eAgInSc1d1	Freudův
portrét	portrét	k1gInSc1	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
surrealismus	surrealismus	k1gInSc1	surrealismus
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
udržoval	udržovat	k5eAaImAgInS	udržovat
vztahy	vztah	k1gInPc7	vztah
i	i	k8xC	i
s	s	k7c7	s
umělci	umělec	k1gMnPc7	umělec
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
s	s	k7c7	s
osobnostmi	osobnost	k1gFnPc7	osobnost
proslulého	proslulý	k2eAgMnSc2d1	proslulý
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
"	"	kIx"	"
<g/>
fin	fin	k?	fin
du	du	k?	du
siè	siè	k?	siè
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
vztah	vztah	k1gInSc1	vztah
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
Arthurem	Arthur	k1gMnSc7	Arthur
Schnitzlerem	Schnitzler	k1gMnSc7	Schnitzler
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gMnSc4	on
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
,	,	kIx,	,
dopisovali	dopisovat	k5eAaImAgMnP	dopisovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Schnitzler	Schnitzler	k1gInSc1	Schnitzler
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
setkání	setkání	k1gNnSc4	setkání
<g/>
,	,	kIx,	,
Freud	Freud	k1gMnSc1	Freud
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
nejasným	jasný	k2eNgNnSc7d1	nejasné
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nS	aby
nepotkal	potkat	k5eNaPmAgMnS	potkat
svého	svůj	k3xOyFgMnSc4	svůj
dvojníka	dvojník	k1gMnSc4	dvojník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
dvojnictví	dvojnictví	k1gNnSc2	dvojnictví
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Doppelgänger	Doppelgänger	k1gInSc1	Doppelgänger
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
mělo	mít	k5eAaImAgNnS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
i	i	k8xC	i
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Fliessem	Fliess	k1gInSc7	Fliess
a	a	k8xC	a
Freud	Freud	k1gInSc1	Freud
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
textu	text	k1gInSc6	text
Cosi	cosi	k3yInSc4	cosi
tísnivého	tísnivý	k2eAgMnSc4d1	tísnivý
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Freud	Freud	k1gMnSc1	Freud
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k2eAgMnSc7d1	známý
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgMnS	navázat
kontakt	kontakt	k1gInSc4	kontakt
i	i	k9	i
s	s	k7c7	s
umělci	umělec	k1gMnPc7	umělec
mimo	mimo	k7c4	mimo
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
zejména	zejména	k9	zejména
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
života	život	k1gInSc2	život
si	se	k3xPyFc3	se
hojně	hojně	k6eAd1	hojně
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
s	s	k7c7	s
Arnoldem	Arnold	k1gMnSc7	Arnold
Zweigem	Zweig	k1gMnSc7	Zweig
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
ho	on	k3xPp3gInSc4	on
navštívil	navštívit	k5eAaPmAgMnS	navštívit
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wellsa	k1gFnPc2	Wellsa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
si	se	k3xPyFc3	se
posléze	posléze	k6eAd1	posléze
korespondovali	korespondovat	k5eAaImAgMnP	korespondovat
<g/>
.	.	kIx.	.
</s>
<s>
Rolland	Rolland	k1gMnSc1	Rolland
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
Freuda	Freuda	k1gMnSc1	Freuda
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboženství	náboženství	k1gNnSc1	náboženství
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
legitimitu	legitimita	k1gFnSc4	legitimita
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc7	jeho
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
oceánický	oceánický	k2eAgInSc4d1	oceánický
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
jednoty	jednota	k1gFnSc2	jednota
se	s	k7c7	s
všehomírem	všehomír	k1gInSc7	všehomír
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
pocit	pocit	k1gInSc1	pocit
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
obnovením	obnovení	k1gNnSc7	obnovení
pocitů	pocit	k1gInPc2	pocit
dítěte	dítě	k1gNnSc2	dítě
u	u	k7c2	u
mateřského	mateřský	k2eAgInSc2d1	mateřský
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgNnSc7d1	klasické
uměním	umění	k1gNnSc7	umění
byl	být	k5eAaImAgMnS	být
Freud	Freud	k1gMnSc1	Freud
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
četl	číst	k5eAaImAgMnS	číst
Homéra	Homér	k1gMnSc2	Homér
<g/>
,	,	kIx,	,
Goetheho	Goethe	k1gMnSc2	Goethe
i	i	k8xC	i
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
objev	objev	k1gInSc1	objev
Oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
provázán	provázat	k5eAaPmNgInS	provázat
s	s	k7c7	s
úvahami	úvaha	k1gFnPc7	úvaha
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
dramatu	drama	k1gNnSc6	drama
Sofoklově	Sofoklův	k2eAgNnSc6d1	Sofoklovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c6	o
Shakespearově	Shakespearův	k2eAgMnSc6d1	Shakespearův
Hamletovi	Hamlet	k1gMnSc6	Hamlet
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jak	jak	k6eAd1	jak
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tentýž	týž	k3xTgMnSc1	týž
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bez	bez	k7c2	bez
rozpaků	rozpak	k1gInPc2	rozpak
posílá	posílat	k5eAaImIp3nS	posílat
své	svůj	k3xOyFgMnPc4	svůj
dvořany	dvořan	k1gMnPc4	dvořan
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
ukvapeně	ukvapeně	k6eAd1	ukvapeně
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
Laerta	Laerta	k1gFnSc1	Laerta
<g/>
,	,	kIx,	,
váhá	váhat	k5eAaImIp3nS	váhat
<g/>
,	,	kIx,	,
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
vraždou	vražda	k1gFnSc7	vražda
strýce	strýc	k1gMnSc2	strýc
pomstít	pomstít	k5eAaPmF	pomstít
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
?	?	kIx.	?
</s>
<s>
Jak	jak	k8xC	jak
lépe	dobře	k6eAd2	dobře
to	ten	k3xDgNnSc4	ten
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
než	než	k8xS	než
trýzní	trýznit	k5eAaImIp3nP	trýznit
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
působí	působit	k5eAaImIp3nS	působit
temná	temný	k2eAgFnSc1d1	temná
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obíral	obírat	k5eAaImAgMnS	obírat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stejného	stejný	k2eAgInSc2d1	stejný
činu	čin	k1gInSc2	čin
dopustí	dopustit	k5eAaPmIp3nS	dopustit
i	i	k9	i
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
píše	psát	k5eAaImIp3nS	psát
Freud	Freud	k1gMnSc1	Freud
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Fliessovi	Fliess	k1gMnSc3	Fliess
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
definuje	definovat	k5eAaBmIp3nS	definovat
oidipovský	oidipovský	k2eAgInSc1d1	oidipovský
komplex	komplex	k1gInSc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Analýzy	analýza	k1gFnPc1	analýza
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
neodřekl	odřeknout	k5eNaPmAgMnS	odřeknout
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
analyzoval	analyzovat	k5eAaImAgInS	analyzovat
druhořadý	druhořadý	k2eAgInSc1d1	druhořadý
román	román	k1gInSc1	román
Gradiva	Gradiv	k1gMnSc2	Gradiv
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Jensense	Jensens	k1gMnSc2	Jensens
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
poslal	poslat	k5eAaPmAgMnS	poslat
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
aby	aby	kYmCp3nS	aby
posílil	posílit	k5eAaPmAgInS	posílit
s	s	k7c7	s
Jungem	Jung	k1gMnSc7	Jung
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
napsal	napsat	k5eAaBmAgInS	napsat
článek	článek	k1gInSc1	článek
Básník	básník	k1gMnSc1	básník
a	a	k8xC	a
lidské	lidský	k2eAgFnPc1d1	lidská
fantazie	fantazie	k1gFnPc1	fantazie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
tvorbu	tvorba	k1gFnSc4	tvorba
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
dennímu	denní	k2eAgNnSc3d1	denní
snění	snění	k1gNnSc3	snění
a	a	k8xC	a
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
jí	on	k3xPp3gFnSc3	on
i	i	k9	i
stejnou	stejný	k2eAgFnSc4d1	stejná
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
anonymně	anonymně	k6eAd1	anonymně
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
Michalengelův	Michalengelův	k2eAgMnSc1d1	Michalengelův
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
doznal	doznat	k5eAaPmAgMnS	doznat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
silnému	silný	k2eAgInSc3d1	silný
vztahu	vztah	k1gInSc3	vztah
k	k	k7c3	k
umění	umění	k1gNnSc3	umění
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
účinků	účinek	k1gInPc2	účinek
umění	umění	k1gNnSc6	umění
mi	já	k3xPp1nSc3	já
chybí	chybit	k5eAaPmIp3nS	chybit
správné	správný	k2eAgNnSc1d1	správné
porozumění	porozumění	k1gNnSc1	porozumění
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nicméně	nicméně	k8xC	nicméně
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
působí	působit	k5eAaImIp3nS	působit
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
silně	silně	k6eAd1	silně
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
díla	dílo	k1gNnPc4	dílo
básnická	básnický	k2eAgNnPc4d1	básnické
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
sochařské	sochařský	k2eAgFnSc2d1	sochařská
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pak	pak	k6eAd1	pak
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
tak	tak	k9	tak
pohnut	pohnut	k2eAgInSc1d1	pohnut
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
při	při	k7c6	při
jistých	jistý	k2eAgFnPc6d1	jistá
příležitostech	příležitost	k1gFnPc6	příležitost
dlouho	dlouho	k6eAd1	dlouho
prodléval	prodlévat	k5eAaImAgMnS	prodlévat
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
sochu	socha	k1gFnSc4	socha
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterou	který	k3yQgFnSc7	který
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
stál	stát	k5eAaImAgInS	stát
fascinován	fascinován	k2eAgInSc1d1	fascinován
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
Michelangelovu	Michelangelův	k2eAgFnSc4d1	Michelangelova
sochu	socha	k1gFnSc4	socha
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
řetězech	řetěz	k1gInPc6	řetěz
(	(	kIx(	(
<g/>
San	San	k1gMnSc1	San
Pietro	Pietro	k1gNnSc1	Pietro
in	in	k?	in
Vincoli	Vincole	k1gFnSc4	Vincole
<g/>
)	)	kIx)	)
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
pozice	pozice	k1gFnSc1	pozice
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vykládána	vykládat	k5eAaImNgFnS	vykládat
jako	jako	k8xC	jako
chvíle	chvíle	k1gFnSc1	chvíle
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
prorok	prorok	k1gMnSc1	prorok
vymrští	vymrštit	k5eAaPmIp3nS	vymrštit
a	a	k8xC	a
zlostně	zlostně	k6eAd1	zlostně
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
na	na	k7c4	na
dav	dav	k1gInSc4	dav
tančící	tančící	k2eAgInPc4d1	tančící
kolem	kolem	k7c2	kolem
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
telete	tele	k1gNnSc2	tele
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
však	však	k9	však
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
socha	socha	k1gFnSc1	socha
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
zadržel	zadržet	k5eAaPmAgMnS	zadržet
svůj	svůj	k3xOyFgInSc4	svůj
hněv	hněv	k1gInSc4	hněv
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
usedl	usednout	k5eAaPmAgMnS	usednout
a	a	k8xC	a
instinktivně	instinktivně	k6eAd1	instinktivně
skryl	skrýt	k5eAaPmAgMnS	skrýt
před	před	k7c7	před
zlotřilci	zlotřilec	k1gMnPc7	zlotřilec
tabulky	tabulka	k1gFnSc2	tabulka
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
přinesl	přinést	k5eAaPmAgInS	přinést
ze	z	k7c2	z
Sinaje	Sinaj	k1gInSc2	Sinaj
<g/>
.	.	kIx.	.
</s>
<s>
Mnohokrát	mnohokrát	k6eAd1	mnohokrát
bylo	být	k5eAaImAgNnS	být
upozorněno	upozornit	k5eAaPmNgNnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Freud	Freud	k1gMnSc1	Freud
touto	tento	k3xDgFnSc7	tento
analýzou	analýza	k1gFnSc7	analýza
patrně	patrně	k6eAd1	patrně
popisoval	popisovat	k5eAaImAgMnS	popisovat
spíše	spíše	k9	spíše
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
psychoanalytickém	psychoanalytický	k2eAgNnSc6d1	psychoanalytické
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
vyšla	vyjít	k5eAaPmAgFnS	vyjít
právě	právě	k6eAd1	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Adlerova	Adlerův	k2eAgNnSc2d1	Adlerovo
a	a	k8xC	a
Jungova	Jungův	k2eAgNnSc2d1	Jungovo
odpadnutí	odpadnutí	k1gNnSc2	odpadnutí
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
esej	esej	k1gInSc1	esej
věnoval	věnovat	k5eAaImAgInS	věnovat
i	i	k9	i
Fjodoru	Fjodor	k1gMnSc3	Fjodor
Michajlovičovi	Michajlovič	k1gMnSc3	Michajlovič
Dostojevskému	Dostojevský	k2eAgMnSc3d1	Dostojevský
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaBmAgInS	nazvat
ho	on	k3xPp3gMnSc4	on
Dostojevskij	Dostojevskij	k1gMnSc4	Dostojevskij
a	a	k8xC	a
otcovražda	otcovražda	k1gFnSc1	otcovražda
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc4	román
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamaz	k1gMnSc3	Karamaz
zde	zde	k6eAd1	zde
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
román	román	k1gInSc4	román
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
a	a	k8xC	a
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
Dostojevského	Dostojevský	k2eAgMnSc4d1	Dostojevský
epilepsii	epilepsie	k1gFnSc6	epilepsie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
psychogenní	psychogenní	k2eAgInSc4d1	psychogenní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
eseji	esej	k1gFnSc6	esej
Cosi	cosi	k3yInSc1	cosi
tísnivého	tísnivý	k2eAgNnSc2d1	tísnivé
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
bizarním	bizarní	k2eAgFnPc3d1	bizarní
povídkám	povídka	k1gFnPc3	povídka
E.	E.	kA	E.
T.	T.	kA	T.
A.	A.	kA	A.
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
text	text	k1gInSc4	text
věnoval	věnovat	k5eAaPmAgMnS	věnovat
rovněž	rovněž	k9	rovněž
analýze	analýza	k1gFnSc3	analýza
povídky	povídka	k1gFnSc2	povídka
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Stefana	Stefan	k1gMnSc2	Stefan
Zweiga	Zweig	k1gMnSc2	Zweig
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
Motiv	motiv	k1gInSc1	motiv
volby	volba	k1gFnSc2	volba
skříněk	skříňka	k1gFnPc2	skříňka
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
mj.	mj.	kA	mj.
Shakespearovu	Shakespearův	k2eAgMnSc3d1	Shakespearův
Králi	Král	k1gMnSc3	Král
Learovi	Lear	k1gMnSc3	Lear
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
Jedna	jeden	k4xCgFnSc1	jeden
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
z	z	k7c2	z
Dichtung	Dichtunga	k1gFnPc2	Dichtunga
und	und	k?	und
Wahrheit	Wahrheit	k1gMnSc1	Wahrheit
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
Goethemu	Goethem	k1gInSc2	Goethem
<g/>
,	,	kIx,	,
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
jednu	jeden	k4xCgFnSc4	jeden
jeho	jeho	k3xOp3gFnSc4	jeho
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
(	(	kIx(	(
<g/>
vyhazování	vyhazování	k1gNnSc1	vyhazování
hrnců	hrnec	k1gInPc2	hrnec
oknem	okno	k1gNnSc7	okno
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Goethe	Goethe	k1gInSc1	Goethe
trpěl	trpět	k5eAaImAgInS	trpět
pocity	pocit	k1gInPc4	pocit
rivality	rivalita	k1gFnSc2	rivalita
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgMnSc3	svůj
mladšímu	mladý	k2eAgMnSc3d2	mladší
sourozenci	sourozenec	k1gMnSc3	sourozenec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sám	sám	k3xTgInSc1	sám
Freud	Freud	k1gInSc1	Freud
tyto	tento	k3xDgInPc4	tento
pocity	pocit	k1gInPc4	pocit
vůči	vůči	k7c3	vůči
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
bratrovi	bratr	k1gMnSc3	bratr
objevil	objevit	k5eAaPmAgInS	objevit
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
autoanalýzy	autoanalýza	k1gFnSc2	autoanalýza
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
cítil	cítit	k5eAaImAgMnS	cítit
dlouho	dlouho	k6eAd1	dlouho
pocit	pocit	k1gInSc4	pocit
viny	vina	k1gFnSc2	vina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
i	i	k9	i
Freudův	Freudův	k2eAgInSc1d1	Freudův
asi	asi	k9	asi
nejznámější	známý	k2eAgInSc1d3	nejznámější
kulturologický	kulturologický	k2eAgInSc1d1	kulturologický
text	text	k1gInSc1	text
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Freud	Freud	k1gInSc1	Freud
snaží	snažit	k5eAaImIp3nS	snažit
dobrat	dobrat	k5eAaPmF	dobrat
původu	původ	k1gInSc3	původ
da	da	k?	da
Vinciho	Vinci	k1gMnSc4	Vinci
geniality	genialita	k1gFnSc2	genialita
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
jeho	jeho	k3xOp3gFnSc7	jeho
homosexualitou	homosexualita	k1gFnSc7	homosexualita
–	–	k?	–
dle	dle	k7c2	dle
Freuda	Freudo	k1gNnSc2	Freudo
sublimovanou	sublimovaný	k2eAgFnSc4d1	sublimovaná
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
v	v	k7c6	v
textu	text	k1gInSc6	text
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
i	i	k9	i
slavný	slavný	k2eAgInSc1d1	slavný
obraz	obraz	k1gInSc1	obraz
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
"	"	kIx"	"
<g/>
záhadný	záhadný	k2eAgInSc4d1	záhadný
<g/>
"	"	kIx"	"
úsměv	úsměv	k1gInSc4	úsměv
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
úsměvem	úsměv	k1gInSc7	úsměv
kojící	kojící	k2eAgFnSc2d1	kojící
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
Freud	Freud	k1gMnSc1	Freud
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takřka	takřka	k6eAd1	takřka
neposlouchá	poslouchat	k5eNaImIp3nS	poslouchat
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vychutnat	vychutnat	k5eAaPmF	vychutnat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
neumí	umět	k5eNaImIp3nS	umět
zanalyzovat	zanalyzovat	k5eAaPmF	zanalyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Octave	Octavat	k5eAaPmIp3nS	Octavat
Mannoni	Mannoň	k1gFnSc3	Mannoň
však	však	k8xC	však
zjistil	zjistit	k5eAaPmAgMnS	zjistit
od	od	k7c2	od
Freudova	Freudův	k2eAgMnSc2d1	Freudův
syna	syn	k1gMnSc2	syn
Ernsta	Ernst	k1gMnSc2	Ernst
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nebyla	být	k5eNaImAgFnS	být
tak	tak	k9	tak
docela	docela	k6eAd1	docela
pravda	pravda	k1gFnSc1	pravda
–	–	k?	–
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Freud	Freud	k1gMnSc1	Freud
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
broukával	broukávat	k5eAaImAgMnS	broukávat
si	se	k3xPyFc3	se
prý	prý	k9	prý
Mozartovy	Mozartův	k2eAgFnPc4d1	Mozartova
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
film	film	k1gInSc1	film
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
centru	centr	k1gInSc6	centr
jeho	jeho	k3xOp3gInSc2	jeho
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
USA	USA	kA	USA
se	se	k3xPyFc4	se
byl	být	k5eAaImAgInS	být
podívat	podívat	k5eAaImF	podívat
na	na	k7c4	na
první	první	k4xOgInPc4	první
grotesky	grotesk	k1gInPc4	grotesk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vkus	vkus	k1gInSc1	vkus
byl	být	k5eAaImAgInS	být
klasický	klasický	k2eAgInSc1d1	klasický
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc1d1	populární
kultura	kultura	k1gFnSc1	kultura
ho	on	k3xPp3gInSc4	on
nezajímala	zajímat	k5eNaImAgFnS	zajímat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
emigraci	emigrace	k1gFnSc6	emigrace
překvapil	překvapit	k5eAaPmAgInS	překvapit
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgInS	začít
hltat	hltat	k5eAaImF	hltat
detektivní	detektivní	k2eAgInPc4d1	detektivní
romány	román	k1gInPc4	román
Agathy	Agatha	k1gFnSc2	Agatha
Christie	Christie	k1gFnSc2	Christie
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kapitolou	kapitola	k1gFnSc7	kapitola
jsou	být	k5eAaImIp3nP	být
Freudovy	Freudův	k2eAgFnPc1d1	Freudova
vlastní	vlastní	k2eAgFnPc1d1	vlastní
umělecké	umělecký	k2eAgFnPc1d1	umělecká
ambice	ambice	k1gFnPc1	ambice
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
Studiích	studie	k1gFnPc6	studie
o	o	k7c6	o
hysterii	hysterie	k1gFnSc6	hysterie
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
nemile	mile	k6eNd1	mile
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
chorobopisy	chorobopis	k1gInPc1	chorobopis
připomínají	připomínat	k5eAaImIp3nP	připomínat
novely	novela	k1gFnPc4	novela
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
punc	punc	k1gInSc4	punc
vědeckosti	vědeckost	k1gFnSc2	vědeckost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Stevena	Steven	k2eAgFnSc1d1	Stevena
Marcuse	Marcuse	k1gFnSc1	Marcuse
Freud	Freuda	k1gFnPc2	Freuda
nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
méně	málo	k6eAd2	málo
důvěryhodný	důvěryhodný	k2eAgMnSc1d1	důvěryhodný
<g/>
,	,	kIx,	,
než	než	k8xS	než
při	při	k7c6	při
takovýchto	takovýto	k3xDgFnPc6	takovýto
poznámkách	poznámka	k1gFnPc6	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
účinku	účinek	k1gInSc6	účinek
na	na	k7c4	na
nevědeckého	vědecký	k2eNgMnSc4d1	nevědecký
čtenáře	čtenář	k1gMnSc4	čtenář
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
záleželo	záležet	k5eAaImAgNnS	záležet
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgInS	věnovat
stylistice	stylistika	k1gFnSc3	stylistika
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Johnson	Johnson	k1gMnSc1	Johnson
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
stylu	styl	k1gInSc6	styl
stejnou	stejný	k2eAgFnSc4d1	stejná
pozornost	pozornost	k1gFnSc4	pozornost
jako	jako	k8xC	jako
jiní	jiný	k2eAgMnPc1d1	jiný
lékaři	lékař	k1gMnPc1	lékař
–	–	k?	–
autoři	autor	k1gMnPc1	autor
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Arthur	Arthur	k1gMnSc1	Arthur
Conan	Conan	k1gMnSc1	Conan
Doyle	Doyle	k1gFnSc1	Doyle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
postupoval	postupovat	k5eAaImAgInS	postupovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
První	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
Královské	královský	k2eAgFnSc2d1	královská
–	–	k?	–
byl	být	k5eAaImAgMnS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
pravdě	pravda	k1gFnSc6	pravda
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
vznešený	vznešený	k2eAgInSc1d1	vznešený
styl	styl	k1gInSc1	styl
a	a	k8xC	a
základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnSc2	jeho
prózy	próza	k1gFnSc2	próza
byla	být	k5eAaImAgFnS	být
víra	víra	k1gFnSc1	víra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xC	jako
maturant	maturant	k1gMnSc1	maturant
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
příteli	přítel	k1gMnSc3	přítel
Emilu	Emil	k1gMnSc3	Emil
Flussovi	Fluss	k1gMnSc3	Fluss
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gMnSc1	můj
profesor	profesor	k1gMnSc1	profesor
mi	já	k3xPp1nSc3	já
také	také	k6eAd1	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc4	ten
Herder	Herder	k1gInSc1	Herder
pěkně	pěkně	k6eAd1	pěkně
nazývá	nazývat	k5eAaImIp3nS	nazývat
<g/>
,	,	kIx,	,
idiomatický	idiomatický	k2eAgInSc1d1	idiomatický
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
bezchybný	bezchybný	k2eAgInSc4d1	bezchybný
i	i	k8xC	i
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
fakt	fakt	k1gInSc1	fakt
mě	já	k3xPp1nSc4	já
náležitě	náležitě	k6eAd1	náležitě
udivil	udivit	k5eAaPmAgMnS	udivit
<g/>
,	,	kIx,	,
nemarnil	marnit	k5eNaImAgMnS	marnit
jsem	být	k5eAaImIp1nS	být
čas	čas	k1gInSc4	čas
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
událost	událost	k1gFnSc4	událost
jsem	být	k5eAaImIp1nS	být
co	co	k9	co
nejvíc	hodně	k6eAd3	hodně
rozhlásil	rozhlásit	k5eAaPmAgMnS	rozhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jste	být	k5eAaImIp2nP	být
si	se	k3xPyFc3	se
dosud	dosud	k6eAd1	dosud
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
nepovšiml	povšimnout	k5eNaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
dopisujete	dopisovat	k5eAaImIp2nP	dopisovat
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
stylistou	stylista	k1gMnSc7	stylista
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
Vám	vy	k3xPp2nPc3	vy
radím	radit	k5eAaImIp1nS	radit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
zájmu	zájem	k1gInSc2	zájem
–	–	k?	–
tyto	tento	k3xDgInPc4	tento
listy	list	k1gInPc4	list
uchovejte	uchovat	k5eAaPmRp2nP	uchovat
<g/>
,	,	kIx,	,
svažte	svázat	k5eAaPmRp2nP	svázat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
je	on	k3xPp3gInPc4	on
střežte	střežit	k5eAaImRp2nP	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
nikdy	nikdy	k6eAd1	nikdy
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
ironii	ironie	k1gFnSc3	ironie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
možná	možná	k9	možná
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaPmF	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
vydal	vydat	k5eAaPmAgMnS	vydat
čtyři	čtyři	k4xCgInPc4	čtyři
zásadní	zásadní	k2eAgInPc4d1	zásadní
chorobopisy	chorobopis	k1gInPc4	chorobopis
svých	svůj	k3xOyFgMnPc2	svůj
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
užíval	užívat	k5eAaImAgInS	užívat
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
pacienty	pacient	k1gMnPc4	pacient
přezdívky	přezdívka	k1gFnPc4	přezdívka
–	–	k?	–
Dora	Dora	k1gFnSc1	Dora
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
Hans	Hans	k1gMnSc1	Hans
<g/>
,	,	kIx,	,
Krysí	krysí	k2eAgMnSc1d1	krysí
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
Vlčí	vlčí	k2eAgMnSc1d1	vlčí
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Dora	Dora	k1gFnSc1	Dora
byla	být	k5eAaImAgFnS	být
krásnou	krásný	k2eAgFnSc7d1	krásná
osmnáctiletou	osmnáctiletý	k2eAgFnSc7d1	osmnáctiletá
dívkou	dívka	k1gFnSc7	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Ida	Ida	k1gFnSc1	Ida
Bauerová	Bauerová	k1gFnSc1	Bauerová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
analýza	analýza	k1gFnSc1	analýza
byla	být	k5eAaImAgFnS	být
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
ji	on	k3xPp3gFnSc4	on
přerušila	přerušit	k5eAaPmAgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Freud	Freud	k1gMnSc1	Freud
zvolil	zvolit	k5eAaPmAgMnS	zvolit
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
ukázkového	ukázkový	k2eAgInSc2d1	ukázkový
chorobopisu	chorobopis	k1gInSc2	chorobopis
(	(	kIx(	(
<g/>
nazval	nazvat	k5eAaBmAgMnS	nazvat
ho	on	k3xPp3gInSc4	on
Zlomek	zlomek	k1gInSc4	zlomek
analysy	analysa	k1gFnSc2	analysa
případu	případ	k1gInSc2	případ
hysterie	hysterie	k1gFnSc2	hysterie
<g/>
)	)	kIx)	)
právě	právě	k9	právě
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
nové	nový	k2eAgInPc4d1	nový
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
sexuality	sexualita	k1gFnSc2	sexualita
v	v	k7c6	v
neuróze	neuróza	k1gFnSc6	neuróza
(	(	kIx(	(
<g/>
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
byla	být	k5eAaImAgFnS	být
masturbace	masturbace	k1gFnSc1	masturbace
i	i	k8xC	i
milostná	milostný	k2eAgFnSc1d1	milostná
rivalita	rivalita	k1gFnSc1	rivalita
v	v	k7c6	v
rodinných	rodinný	k2eAgInPc6d1	rodinný
vztazích	vztah	k1gInPc6	vztah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
výkladu	výklad	k1gInSc2	výklad
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
jádrem	jádro	k1gNnSc7	jádro
analýzy	analýza	k1gFnSc2	analýza
byla	být	k5eAaImAgFnS	být
interpretace	interpretace	k1gFnSc1	interpretace
dvou	dva	k4xCgInPc2	dva
Dořiných	Dořin	k2eAgMnPc2d1	Dořin
snů	sen	k1gInPc2	sen
<g/>
)	)	kIx)	)
a	a	k8xC	a
nově	nově	k6eAd1	nově
objevenou	objevený	k2eAgFnSc4d1	objevená
roli	role	k1gFnSc4	role
přenosu	přenos	k1gInSc2	přenos
–	–	k?	–
tedy	tedy	k9	tedy
vtažení	vtažení	k1gNnSc1	vtažení
analytika	analytik	k1gMnSc2	analytik
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Doře	Dora	k1gFnSc3	Dora
zdálo	zdát	k5eAaImAgNnS	zdát
o	o	k7c6	o
kouři	kouř	k1gInSc6	kouř
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Freud	Freud	k1gInSc1	Freud
vyložil	vyložit	k5eAaPmAgInS	vyložit
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
–	–	k?	–
"	"	kIx"	"
<g/>
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kouř	kouř	k1gInSc4	kouř
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
oheň	oheň	k1gInSc1	oheň
<g/>
"	"	kIx"	"
–	–	k?	–
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
celoživotně	celoživotně	k6eAd1	celoživotně
silným	silný	k2eAgInSc7d1	silný
kuřákem	kuřák	k1gInSc7	kuřák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
Hans	Hans	k1gMnSc1	Hans
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Herbert	Herbert	k1gInSc1	Herbert
Graf	graf	k1gInSc1	graf
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
hudebního	hudební	k2eAgMnSc2d1	hudební
vědce	vědec	k1gMnSc2	vědec
Maxe	Max	k1gMnSc2	Max
Grafa	Graf	k1gMnSc2	Graf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
Hans	hansa	k1gFnPc2	hansa
začal	začít	k5eAaPmAgInS	začít
odmítat	odmítat	k5eAaImF	odmítat
vycházet	vycházet	k5eAaImF	vycházet
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
bál	bát	k5eAaImAgMnS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
kousne	kousnout	k5eAaPmIp3nS	kousnout
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
pověřil	pověřit	k5eAaPmAgInS	pověřit
vedením	vedení	k1gNnSc7	vedení
analýzy	analýza	k1gFnSc2	analýza
Hansova	Hansův	k2eAgMnSc2d1	Hansův
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Hansovu	Hansův	k2eAgFnSc4d1	Hansova
fobii	fobie	k1gFnSc4	fobie
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
komplexu	komplex	k1gInSc2	komplex
–	–	k?	–
strach	strach	k1gInSc1	strach
z	z	k7c2	z
otce	otec	k1gMnSc2	otec
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
Hans	Hans	k1gMnSc1	Hans
přenesl	přenést	k5eAaPmAgMnS	přenést
na	na	k7c4	na
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Fobie	fobie	k1gFnSc1	fobie
skutečně	skutečně	k6eAd1	skutečně
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
Hans	Hans	k1gMnSc1	Hans
Freuda	Freuda	k1gMnSc1	Freuda
znovu	znovu	k6eAd1	znovu
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
byl	být	k5eAaImAgMnS	být
potěšen	potěšit	k5eAaPmNgMnS	potěšit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
psychicky	psychicky	k6eAd1	psychicky
zcela	zcela	k6eAd1	zcela
normální	normální	k2eAgMnSc1d1	normální
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
čelila	čelit	k5eAaImAgFnS	čelit
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
že	že	k8xS	že
neuróza	neuróza	k1gFnSc1	neuróza
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
a	a	k8xC	a
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
terapie	terapie	k1gFnSc1	terapie
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nemožná	možný	k2eNgFnSc1d1	nemožná
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
však	však	k9	však
Freuda	Freuda	k1gMnSc1	Freuda
překvapil	překvapit	k5eAaPmAgMnS	překvapit
sdělením	sdělení	k1gNnSc7	sdělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
dětské	dětský	k2eAgFnSc2d1	dětská
analýzy	analýza	k1gFnSc2	analýza
nic	nic	k3yNnSc1	nic
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odporovalo	odporovat	k5eAaImAgNnS	odporovat
základní	základní	k2eAgFnSc4d1	základní
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
podmínkou	podmínka	k1gFnSc7	podmínka
vyléčení	vyléčení	k1gNnSc1	vyléčení
je	být	k5eAaImIp3nS	být
uvedení	uvedení	k1gNnSc4	uvedení
nevědomého	vědomý	k2eNgInSc2d1	nevědomý
obsahu	obsah	k1gInSc2	obsah
do	do	k7c2	do
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
proto	proto	k8xC	proto
přidal	přidat	k5eAaPmAgMnS	přidat
do	do	k7c2	do
systému	systém	k1gInSc2	systém
kategorii	kategorie	k1gFnSc3	kategorie
předvědomí	předvědomý	k2eAgMnPc1d1	předvědomý
<g/>
.	.	kIx.	.
</s>
<s>
Hansův	Hansův	k2eAgInSc1d1	Hansův
chorobopis	chorobopis	k1gInSc1	chorobopis
vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Analýza	analýza	k1gFnSc1	analýza
fobie	fobie	k1gFnSc1	fobie
pětiletého	pětiletý	k2eAgMnSc2d1	pětiletý
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Krysí	krysí	k2eAgMnSc1d1	krysí
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
Rattenmann	Rattenmann	k1gMnSc1	Rattenmann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Ernst	Ernst	k1gMnSc1	Ernst
Lanzer	Lanzer	k1gMnSc1	Lanzer
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vzdělaným	vzdělaný	k2eAgMnSc7d1	vzdělaný
mladým	mladý	k1gMnSc7	mladý
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
trpěl	trpět	k5eAaImAgMnS	trpět
podivnou	podivný	k2eAgFnSc7d1	podivná
nutkavou	nutkavý	k2eAgFnSc7d1	nutkavá
neurózou	neuróza	k1gFnSc7	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vojenském	vojenský	k2eAgNnSc6d1	vojenské
cvičení	cvičení	k1gNnSc6	cvičení
ztratil	ztratit	k5eAaPmAgInS	ztratit
skřipec	skřipec	k1gInSc4	skřipec
a	a	k8xC	a
slyšel	slyšet	k5eAaImAgMnS	slyšet
o	o	k7c6	o
orientálním	orientální	k2eAgInSc6d1	orientální
způsobu	způsob	k1gInSc6	způsob
mučení	mučení	k1gNnSc1	mučení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oběti	oběť	k1gFnSc3	oběť
nechá	nechat	k5eAaPmIp3nS	nechat
vlézt	vlézt	k5eAaPmF	vlézt
krysa	krysa	k1gFnSc1	krysa
do	do	k7c2	do
řitního	řitní	k2eAgInSc2d1	řitní
otvoru	otvor	k1gInSc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
začal	začít	k5eAaPmAgInS	začít
mít	mít	k5eAaImF	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
mučením	mučení	k1gNnSc7	mučení
jsou	být	k5eAaImIp3nP	být
ohroženi	ohrožen	k2eAgMnPc1d1	ohrožen
jeho	jeho	k3xOp3gInPc4	jeho
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
snoubenka	snoubenka	k1gFnSc1	snoubenka
<g/>
.	.	kIx.	.
</s>
<s>
Zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
si	se	k3xPyFc3	se
nutkavou	nutkavý	k2eAgFnSc4d1	nutkavá
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
této	tento	k3xDgFnSc3	tento
katastrofě	katastrofa	k1gFnSc3	katastrofa
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uhradí	uhradit	k5eAaPmIp3nS	uhradit
poštovné	poštovné	k1gNnSc4	poštovné
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
skřipec	skřipec	k1gInSc4	skřipec
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
při	při	k7c6	při
analýze	analýza	k1gFnSc6	analýza
velmi	velmi	k6eAd1	velmi
terapeuticky	terapeuticky	k6eAd1	terapeuticky
uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
obsese	obsese	k1gFnSc2	obsese
(	(	kIx(	(
<g/>
nutkavé	nutkavý	k2eAgFnSc2d1	nutkavá
neurózy	neuróza	k1gFnSc2	neuróza
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
předpoklady	předpoklad	k1gInPc1	předpoklad
(	(	kIx(	(
<g/>
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
hysterie	hysterie	k1gFnSc2	hysterie
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
depresí	deprese	k1gFnPc2	deprese
<g/>
,	,	kIx,	,
u	u	k7c2	u
psychotických	psychotický	k2eAgFnPc2d1	psychotická
potíží	potíž	k1gFnPc2	potíž
nemůže	moct	k5eNaImIp3nS	moct
dle	dle	k7c2	dle
Freuda	Freud	k1gMnSc2	Freud
uspět	uspět	k5eAaPmF	uspět
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chorobopis	chorobopis	k1gInSc1	chorobopis
Krysího	krysí	k2eAgMnSc2d1	krysí
muže	muž	k1gMnSc2	muž
Freud	Freud	k1gMnSc1	Freud
vydal	vydat	k5eAaPmAgMnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
případu	případ	k1gInSc3	případ
nutkavé	nutkavý	k2eAgFnSc2d1	nutkavá
neurózy	neuróza	k1gFnSc2	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Vlčí	vlčí	k2eAgMnSc1d1	vlčí
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
Wolfsmann	Wolfsmann	k1gMnSc1	Wolfsmann
<g/>
)	)	kIx)	)
trpěl	trpět	k5eAaImAgInS	trpět
řadou	řada	k1gFnSc7	řada
psychických	psychický	k2eAgFnPc2d1	psychická
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
psychózy	psychóza	k1gFnSc2	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
opravdu	opravdu	k6eAd1	opravdu
vyléčit	vyléčit	k5eAaPmF	vyléčit
(	(	kIx(	(
<g/>
v	v	k7c6	v
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
byl	být	k5eAaImAgInS	být
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napsal	napsat	k5eAaBmAgMnS	napsat
chorobopis	chorobopis	k1gInSc4	chorobopis
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
neuróze	neuróza	k1gFnSc6	neuróza
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Vlčí	vlčí	k2eAgMnSc1d1	vlčí
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Sergej	Sergej	k1gMnSc1	Sergej
Pankejev	Pankejev	k1gMnSc1	Pankejev
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
ruský	ruský	k2eAgMnSc1d1	ruský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
přišel	přijít	k5eAaPmAgMnS	přijít
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívka	přezdívka	k1gFnSc1	přezdívka
Vlčí	vlčet	k5eAaImIp3nS	vlčet
muž	muž	k1gMnSc1	muž
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
podle	podle	k7c2	podle
klíčového	klíčový	k2eAgInSc2d1	klíčový
snu	sen	k1gInSc2	sen
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
si	se	k3xPyFc3	se
pacient	pacient	k1gMnSc1	pacient
vybavil	vybavit	k5eAaPmAgMnS	vybavit
<g/>
:	:	kIx,	:
na	na	k7c6	na
ořešáku	ořešák	k1gInSc6	ořešák
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
oknem	okno	k1gNnSc7	okno
sedělo	sedět	k5eAaImAgNnS	sedět
několik	několik	k4yIc1	několik
bílých	bílý	k2eAgMnPc2d1	bílý
vlků	vlk	k1gMnPc2	vlk
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
tlustými	tlustý	k2eAgInPc7d1	tlustý
ocasy	ocas	k1gInPc7	ocas
a	a	k8xC	a
upřeně	upřeně	k6eAd1	upřeně
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dívalo	dívat	k5eAaImAgNnS	dívat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
snu	sen	k1gInSc6	sen
propukla	propuknout	k5eAaPmAgFnS	propuknout
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
dětská	dětský	k2eAgFnSc1d1	dětská
neuróza	neuróza	k1gFnSc1	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
sen	sen	k1gInSc4	sen
vyložil	vyložit	k5eAaPmAgMnS	vyložit
jako	jako	k9	jako
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
prascénu	prascéna	k1gFnSc4	prascéna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pozorování	pozorování	k1gNnSc1	pozorování
soulože	soulož	k1gFnSc2	soulož
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Freuda	Freudo	k1gNnPc4	Freudo
byl	být	k5eAaImAgMnS	být
Vlčí	vlčí	k2eAgMnSc1d1	vlčí
muž	muž	k1gMnSc1	muž
cenný	cenný	k2eAgInSc4d1	cenný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	jíst	k5eAaImIp1nS	jíst
mohl	moct	k5eAaImAgMnS	moct
oponovat	oponovat	k5eAaImF	oponovat
Jungovi	Jungův	k2eAgMnPc1d1	Jungův
–	–	k?	–
byl	být	k5eAaImAgInS	být
Rus	Rus	k1gFnSc4	Rus
(	(	kIx(	(
<g/>
a	a	k8xC	a
Jung	Jung	k1gMnSc1	Jung
začal	začít	k5eAaPmAgMnS	začít
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
freudovská	freudovský	k2eAgFnSc1d1	freudovská
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
popisuje	popisovat	k5eAaImIp3nS	popisovat
jen	jen	k9	jen
židovskou	židovský	k2eAgFnSc4d1	židovská
mysl	mysl	k1gFnSc4	mysl
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
přetékaly	přetékat	k5eAaImAgFnP	přetékat
sexualitou	sexualita	k1gFnSc7	sexualita
(	(	kIx(	(
<g/>
a	a	k8xC	a
Jung	Jung	k1gMnSc1	Jung
začal	začít	k5eAaPmAgMnS	začít
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
nevinné	vinný	k2eNgNnSc1d1	nevinné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ho	on	k3xPp3gInSc4	on
Freud	Freud	k1gInSc4	Freud
vybral	vybrat	k5eAaPmAgMnS	vybrat
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
chorobopisu	chorobopis	k1gInSc2	chorobopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nazval	nazvat	k5eAaPmAgInS	nazvat
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
případu	případ	k1gInSc2	případ
dětské	dětský	k2eAgFnSc2d1	dětská
neurózy	neuróza	k1gFnSc2	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rodný	rodný	k2eAgInSc4d1	rodný
Příbor	Příbor	k1gInSc4	Příbor
vzpomínal	vzpomínat	k5eAaImAgInS	vzpomínat
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
sedmnáctiletý	sedmnáctiletý	k2eAgMnSc1d1	sedmnáctiletý
gymnazista	gymnazista	k1gMnSc1	gymnazista
na	na	k7c6	na
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Prožil	prožít	k5eAaPmAgMnS	prožít
zde	zde	k6eAd1	zde
tehdy	tehdy	k6eAd1	tehdy
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
zamilovanost	zamilovanost	k1gFnSc1	zamilovanost
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
ho	on	k3xPp3gNnSc4	on
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
dcera	dcera	k1gFnSc1	dcera
jeho	jeho	k3xOp3gMnPc2	jeho
hostitelů	hostitel	k1gMnPc2	hostitel
<g/>
,	,	kIx,	,
Gisela	Gisela	k1gFnSc1	Gisela
Flussová	Flussová	k1gFnSc1	Flussová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
O	o	k7c6	o
krycích	krycí	k2eAgFnPc6d1	krycí
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
přiznal	přiznat	k5eAaPmAgMnS	přiznat
sílu	síla	k1gFnSc4	síla
svého	svůj	k3xOyFgNnSc2	svůj
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
poblouznění	poblouznění	k1gNnSc2	poblouznění
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nucený	nucený	k2eAgInSc1d1	nucený
odchod	odchod	k1gInSc1	odchod
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
dlouho	dlouho	k6eAd1	dlouho
vnímal	vnímat	k5eAaImAgInS	vnímat
bolestně	bolestně	k6eAd1	bolestně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
mé	můj	k3xOp1gNnSc1	můj
první	první	k4xOgNnSc1	první
poblouznění	poblouznění	k1gNnSc1	poblouznění
<g/>
,	,	kIx,	,
dost	dost	k6eAd1	dost
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
udržované	udržovaný	k2eAgFnPc4d1	udržovaná
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
do	do	k7c2	do
výchovného	výchovný	k2eAgInSc2d1	výchovný
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
rozchod	rozchod	k1gInSc4	rozchod
po	po	k7c6	po
tak	tak	k6eAd1	tak
krátké	krátký	k2eAgFnSc6d1	krátká
známosti	známost	k1gFnSc6	známost
mou	můj	k3xOp1gFnSc4	můj
touhu	touha	k1gFnSc4	touha
ještě	ještě	k9	ještě
jaksepatří	jaksepatří	k6eAd1	jaksepatří
zesílil	zesílit	k5eAaPmAgMnS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
hodiny	hodina	k1gFnSc2	hodina
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
osaměle	osaměle	k6eAd1	osaměle
procházel	procházet	k5eAaImAgInS	procházet
opět	opět	k6eAd1	opět
nalezenými	nalezený	k2eAgInPc7d1	nalezený
nádhernými	nádherný	k2eAgInPc7d1	nádherný
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
budováním	budování	k1gNnSc7	budování
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
zámků	zámek	k1gInPc2	zámek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kupodivu	kupodivu	k6eAd1	kupodivu
nesměřovaly	směřovat	k5eNaImAgInP	směřovat
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
zlepšit	zlepšit	k5eAaPmF	zlepšit
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
úpadku	úpadek	k1gInSc3	úpadek
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
zesílil	zesílit	k5eAaPmAgMnS	zesílit
jako	jako	k8xC	jako
mladí	mladý	k2eAgMnPc1d1	mladý
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
onom	onen	k3xDgInSc6	onen
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
mé	můj	k3xOp1gMnPc4	můj
milované	milovaný	k1gMnPc4	milovaný
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgMnS	být
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c4	v
povolání	povolání	k1gNnSc4	povolání
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bych	by	kYmCp1nS	by
za	za	k7c4	za
všechna	všechen	k3xTgNnPc4	všechen
ta	ten	k3xDgNnPc4	ten
léta	léto	k1gNnPc4	léto
jistě	jistě	k6eAd1	jistě
dobře	dobře	k6eAd1	dobře
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Příbor	Příbor	k1gInSc1	Příbor
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nenavštívil	navštívit	k5eNaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
na	na	k7c6	na
manévrech	manévr	k1gInPc6	manévr
jako	jako	k8xS	jako
lékař	lékař	k1gMnSc1	lékař
c.	c.	k?	c.
k.	k.	k?	k.
armády	armáda	k1gFnSc2	armáda
během	během	k7c2	během
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
blízko	blízko	k1gNnSc1	blízko
rodišti	rodiště	k1gNnSc3	rodiště
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
jeho	jeho	k3xOp3gFnSc1	jeho
korespondence	korespondence	k1gFnSc1	korespondence
s	s	k7c7	s
Breuerem	Breuero	k1gNnSc7	Breuero
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stále	stále	k6eAd1	stále
si	se	k3xPyFc3	se
hrajeme	hrát	k5eAaImIp1nP	hrát
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
snesitelné	snesitelný	k2eAgFnSc6d1	snesitelná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velkoměstská	velkoměstský	k2eAgFnSc1d1	velkoměstská
kavárna	kavárna	k1gFnSc1	kavárna
se	s	k7c7	s
zmrzlinou	zmrzlina	k1gFnSc7	zmrzlina
<g/>
,	,	kIx,	,
novinami	novina	k1gFnPc7	novina
a	a	k8xC	a
dobrým	dobrý	k2eAgNnSc7d1	dobré
pečivem	pečivo	k1gNnSc7	pečivo
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
tehdy	tehdy	k6eAd1	tehdy
příteli	přítel	k1gMnSc3	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
vlasti	vlast	k1gFnSc6	vlast
–	–	k?	–
v	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
k	k	k7c3	k
českému	český	k2eAgNnSc3d1	české
vydání	vydání	k1gNnSc3	vydání
svých	svůj	k3xOyFgFnPc2	svůj
přednášek	přednáška	k1gFnPc2	přednáška
Úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
psychoanalysy	psychoanalysa	k1gFnSc2	psychoanalysa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
překladu	překlad	k1gInSc6	překlad
mých	můj	k3xOp1gFnPc2	můj
přednášek	přednáška	k1gFnPc2	přednáška
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
podaří	podařit	k5eAaPmIp3nS	podařit
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
nově	nově	k6eAd1	nově
rozkvétající	rozkvétající	k2eAgFnSc3d1	rozkvétající
zemi	zem	k1gFnSc3	zem
stoupence	stoupenka	k1gFnSc3	stoupenka
pro	pro	k7c4	pro
mladou	mladý	k2eAgFnSc4d1	mladá
vědu	věda	k1gFnSc4	věda
psychoanalytickou	psychoanalytický	k2eAgFnSc7d1	psychoanalytická
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
starého	starý	k2eAgMnSc2d1	starý
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
zadostiučiněním	zadostiučinění	k1gNnSc7	zadostiučinění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
znění	znění	k1gNnSc4	znění
přísloví	přísloví	k1gNnSc2	přísloví
přece	přece	k9	přece
jen	jen	k9	jen
poněkud	poněkud	k6eAd1	poněkud
uplatním	uplatnit	k5eAaPmIp1nS	uplatnit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vlasti	vlast	k1gFnSc6	vlast
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čímž	což	k3yQnSc7	což
narážel	narážet	k5eAaPmAgInS	narážet
na	na	k7c4	na
biblické	biblický	k2eAgNnSc4d1	biblické
přísloví	přísloví	k1gNnSc4	přísloví
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vlasti	vlast	k1gFnSc6	vlast
není	být	k5eNaImIp3nS	být
nikdo	nikdo	k3yNnSc1	nikdo
prorokem	prorok	k1gMnSc7	prorok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Rodiště	rodiště	k1gNnSc1	rodiště
Příbor	Příbor	k1gInSc1	Příbor
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zmiňováno	zmiňován	k2eAgNnSc1d1	zmiňováno
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
článku	článek	k1gInSc6	článek
O	o	k7c6	o
krycích	krycí	k2eAgFnPc6d1	krycí
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
si	se	k3xPyFc3	se
vybavuje	vybavovat	k5eAaImIp3nS	vybavovat
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
dětskou	dětský	k2eAgFnSc4d1	dětská
hru	hra	k1gFnSc4	hra
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
za	za	k7c7	za
městem	město	k1gNnSc7	město
a	a	k8xC	a
o	o	k7c6	o
rodišti	rodiště	k1gNnSc6	rodiště
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
necítil	cítit	k5eNaImAgMnS	cítit
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
mě	já	k3xPp1nSc4	já
nikdy	nikdy	k6eAd1	nikdy
neopustila	opustit	k5eNaPmAgFnS	opustit
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
krásných	krásný	k2eAgInPc6d1	krásný
lesích	les	k1gInPc6	les
rodného	rodný	k2eAgInSc2d1	rodný
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
jsem	být	k5eAaImIp1nS	být
otci	otec	k1gMnSc3	otec
utíkal	utíkat	k5eAaImAgMnS	utíkat
<g/>
,	,	kIx,	,
sotva	sotva	k8xS	sotva
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
chodit	chodit	k5eAaImF	chodit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Výkladu	výklad	k1gInSc6	výklad
snů	sen	k1gInPc2	sen
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
historku	historka	k1gFnSc4	historka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
ve	v	k7c6	v
Příboru	příbor	k1gInSc6	příbor
čelil	čelit	k5eAaImAgInS	čelit
antisemitskému	antisemitský	k2eAgInSc3d1	antisemitský
útoku	útok	k1gInSc3	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
rodiště	rodiště	k1gNnSc1	rodiště
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
jeho	on	k3xPp3gInSc2	on
snu	sen	k1gInSc2	sen
o	o	k7c6	o
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Výkladu	výklad	k1gInSc6	výklad
snů	sen	k1gInPc2	sen
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
i	i	k9	i
návštěvu	návštěva	k1gFnSc4	návštěva
Prahy	Praha	k1gFnSc2	Praha
ze	z	k7c2	z
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
místní	místní	k2eAgNnSc1d1	místní
slovanské	slovanský	k2eAgNnSc1d1	slovanské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
jezdil	jezdit	k5eAaImAgMnS	jezdit
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
známého	známý	k1gMnSc2	známý
iluzionisty	iluzionista	k1gMnSc2	iluzionista
Erika	Erik	k1gMnSc2	Erik
Jana	Jan	k1gMnSc2	Jan
Hanussena	Hanussen	k2eAgFnSc1d1	Hanussena
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Freuda	Freuda	k1gMnSc1	Freuda
potkal	potkat	k5eAaPmAgMnS	potkat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Flóře	Flóra	k1gFnSc6	Flóra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
deníku	deník	k1gInSc2	deník
si	se	k3xPyFc3	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
lékař	lékař	k1gMnSc1	lékař
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
Freud	Freud	k1gMnSc1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
partii	partie	k1gFnSc4	partie
biliáru	biliár	k1gInSc2	biliár
a	a	k8xC	a
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
čáře	čára	k1gFnSc6	čára
jsem	být	k5eAaImIp1nS	být
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
jsem	být	k5eAaImIp1nS	být
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
experimentuji	experimentovat	k5eAaImIp1nS	experimentovat
s	s	k7c7	s
hypnózou	hypnóza	k1gFnSc7	hypnóza
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
doktor	doktor	k1gMnSc1	doktor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
dělá	dělat	k5eAaImIp3nS	dělat
také	také	k9	také
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
z	z	k7c2	z
terapeutických	terapeutický	k2eAgInPc2d1	terapeutický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pouhé	pouhý	k2eAgNnSc1d1	pouhé
šarlatánství	šarlatánství	k1gNnSc1	šarlatánství
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
velké	velký	k2eAgFnSc2d1	velká
prohry	prohra	k1gFnSc2	prohra
špatnou	špatný	k2eAgFnSc4d1	špatná
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
nenechal	nechat	k5eNaPmAgMnS	nechat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
a	a	k8xC	a
pozval	pozvat	k5eAaPmAgMnS	pozvat
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
moka	moka	k1gNnSc4	moka
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
mi	já	k3xPp1nSc3	já
udělal	udělat	k5eAaPmAgInS	udělat
nesrozumitelnou	srozumitelný	k2eNgFnSc4d1	nesrozumitelná
přednášku	přednáška	k1gFnSc4	přednáška
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
objevu	objev	k1gInSc6	objev
podvědomí	podvědomí	k1gNnSc2	podvědomí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
!	!	kIx.	!
</s>
<s>
Jak	jak	k6eAd1	jak
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
objevit	objevit	k5eAaPmF	objevit
podvědomí	podvědomí	k1gNnSc2	podvědomí
<g/>
?	?	kIx.	?
</s>
<s>
Protože	protože	k8xS	protože
mě	já	k3xPp1nSc4	já
nudil	nudit	k5eAaImAgMnS	nudit
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
partii	partie	k1gFnSc3	partie
biliáru	biliár	k1gInSc2	biliár
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jsem	být	k5eAaImIp1nS	být
rovněž	rovněž	k9	rovněž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
autoanalýze	autoanalýza	k1gFnSc6	autoanalýza
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
údajně	údajně	k6eAd1	údajně
částečně	částečně	k6eAd1	částečně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
znalost	znalost	k1gFnSc1	znalost
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Důvěřoval	důvěřovat	k5eAaImAgMnS	důvěřovat
jen	jen	k9	jen
českému	český	k2eAgNnSc3d1	české
služebnictvu	služebnictvo	k1gNnSc3	služebnictvo
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
milovníkem	milovník	k1gMnSc7	milovník
pravých	pravý	k2eAgInPc2d1	pravý
olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Freudova	Freudův	k2eAgInSc2d1	Freudův
osudu	osud	k1gInSc2	osud
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
i	i	k9	i
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zpočátku	zpočátku	k6eAd1	zpočátku
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
trávil	trávit	k5eAaImAgInS	trávit
léto	léto	k1gNnSc4	léto
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Sandor	Sandor	k1gMnSc1	Sandor
Ferenczi	Ferencze	k1gFnSc4	Ferencze
tam	tam	k6eAd1	tam
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
poslal	poslat	k5eAaPmAgMnS	poslat
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
bohatého	bohatý	k2eAgMnSc2d1	bohatý
pivovarníka	pivovarník	k1gMnSc2	pivovarník
Antona	Anton	k1gMnSc2	Anton
von	von	k1gInSc1	von
Freunda	Freund	k1gMnSc4	Freund
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
nechtěl	chtít	k5eNaImAgMnS	chtít
sám	sám	k3xTgMnSc1	sám
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
příliš	příliš	k6eAd1	příliš
blízcí	blízký	k2eAgMnPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Freund	Freunda	k1gFnPc2	Freunda
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
amputaci	amputace	k1gFnSc6	amputace
jednoho	jeden	k4xCgNnSc2	jeden
varlete	varle	k1gNnSc2	varle
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
karcinomem	karcinom	k1gInSc7	karcinom
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
nebyl	být	k5eNaImAgInS	být
fyziologický	fyziologický	k2eAgInSc4d1	fyziologický
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
impotentním	impotentní	k2eAgNnSc7d1	impotentní
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
analýzu	analýza	k1gFnSc4	analýza
během	během	k7c2	během
procházek	procházka	k1gFnPc2	procházka
po	po	k7c6	po
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
operace	operace	k1gFnSc1	operace
pivovarníkovi	pivovarník	k1gMnSc3	pivovarník
znovuzpřítomnila	znovuzpřítomnit	k5eAaImAgFnS	znovuzpřítomnit
kastrační	kastrační	k2eAgFnPc4d1	kastrační
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Freundovi	Freund	k1gMnSc6	Freund
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
a	a	k8xC	a
v	v	k7c4	v
nadšení	nadšení	k1gNnSc4	nadšení
okamžitě	okamžitě	k6eAd1	okamžitě
Freudovi	Freuda	k1gMnSc3	Freuda
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
dar	dar	k1gInSc1	dar
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
brzy	brzy	k6eAd1	brzy
skutečně	skutečně	k6eAd1	skutečně
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
nadace	nadace	k1gFnSc2	nadace
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc1	jeho
hnutí	hnutí	k1gNnSc1	hnutí
trpělo	trpět	k5eAaImAgNnS	trpět
neustále	neustále	k6eAd1	neustále
finanční	finanční	k2eAgFnSc7d1	finanční
nouzí	nouze	k1gFnSc7	nouze
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
válečných	válečný	k2eAgInPc6d1	válečný
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
mohl	moct	k5eAaImAgMnS	moct
s	s	k7c7	s
penězi	peníze	k1gInPc7	peníze
volně	volně	k6eAd1	volně
disponovat	disponovat	k5eAaBmF	disponovat
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vybudovat	vybudovat	k5eAaPmF	vybudovat
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
psychoanalytické	psychoanalytický	k2eAgNnSc1d1	psychoanalytické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
obav	obava	k1gFnPc2	obava
z	z	k7c2	z
účtu	účet	k1gInSc2	účet
utratil	utratit	k5eAaPmAgMnS	utratit
50	[number]	k4	50
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
tiskařských	tiskařský	k2eAgInPc2d1	tiskařský
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
podepsal	podepsat	k5eAaPmAgMnS	podepsat
řadu	řada	k1gFnSc4	řada
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
pro	pro	k7c4	pro
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
plynuly	plynout	k5eAaImAgInP	plynout
finanční	finanční	k2eAgInPc1d1	finanční
závazky	závazek	k1gInPc1	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
přišel	přijít	k5eAaPmAgMnS	přijít
rychlý	rychlý	k2eAgInSc4d1	rychlý
zvrat	zvrat	k1gInSc4	zvrat
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
padla	padnout	k5eAaPmAgFnS	padnout
revoluční	revoluční	k2eAgFnSc1d1	revoluční
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
Bély	Béla	k1gMnSc2	Béla
Kuna	kuna	k1gFnSc1	kuna
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
ostatně	ostatně	k6eAd1	ostatně
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
velmi	velmi	k6eAd1	velmi
přála	přát	k5eAaImAgFnS	přát
<g/>
,	,	kIx,	,
Ferenczi	Ferench	k1gMnPc1	Ferench
se	se	k3xPyFc4	se
na	na	k7c6	na
revoluci	revoluce	k1gFnSc6	revoluce
přímo	přímo	k6eAd1	přímo
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
)	)	kIx)	)
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
autoritativní	autoritativní	k2eAgMnSc1d1	autoritativní
diktátor	diktátor	k1gMnSc1	diktátor
Miklós	Miklósa	k1gFnPc2	Miklósa
Horthy	Hortha	k1gFnSc2	Hortha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zakázal	zakázat	k5eAaPmAgInS	zakázat
převod	převod	k1gInSc1	převod
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
maďarských	maďarský	k2eAgInPc2d1	maďarský
účtů	účet	k1gInPc2	účet
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
už	už	k6eAd1	už
se	se	k3xPyFc4	se
k	k	k7c3	k
von	von	k1gInSc1	von
Freundovým	Freundový	k2eAgInPc3d1	Freundový
penězům	peníze	k1gInPc3	peníze
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
a	a	k8xC	a
psychoanalytické	psychoanalytický	k2eAgNnSc1d1	psychoanalytické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
(	(	kIx(	(
<g/>
zpočátku	zpočátku	k6eAd1	zpočátku
ho	on	k3xPp3gMnSc4	on
vedl	vést	k5eAaImAgMnS	vést
Otto	Otto	k1gMnSc1	Otto
Rank	rank	k1gInSc4	rank
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Freudův	Freudův	k2eAgMnSc1d1	Freudův
syn	syn	k1gMnSc1	syn
Martin	Martin	k1gMnSc1	Martin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
existovalo	existovat	k5eAaImAgNnS	existovat
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
napořád	napořád	k6eAd1	napořád
už	už	k6eAd1	už
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
topilo	topit	k5eAaImAgNnS	topit
v	v	k7c6	v
dluzích	dluh	k1gInPc6	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
cítil	cítit	k5eAaImAgMnS	cítit
Židem	Žid	k1gMnSc7	Žid
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životopisné	životopisný	k2eAgFnSc6d1	životopisná
črtě	črta	k1gFnSc6	črta
O	o	k7c6	o
sobě	se	k3xPyFc3	se
a	a	k8xC	a
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bolestně	bolestně	k6eAd1	bolestně
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dotkla	dotknout	k5eAaPmAgFnS	dotknout
vlna	vlna	k1gFnSc1	vlna
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
–	–	k?	–
utvrdila	utvrdit	k5eAaPmAgFnS	utvrdit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
židovství	židovství	k1gNnSc6	židovství
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
nepochopil	pochopit	k5eNaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
–	–	k?	–
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
–	–	k?	–
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
rasu	rasa	k1gFnSc4	rasa
stydět	stydět	k5eAaImF	stydět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
judaismu	judaismus	k1gInSc3	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
řediteli	ředitel	k1gMnSc3	ředitel
židovského	židovský	k2eAgNnSc2d1	Židovské
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Židovské	židovský	k2eAgNnSc1d1	Židovské
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
stejně	stejně	k6eAd1	stejně
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
jako	jako	k8xC	jako
všechna	všechen	k3xTgNnPc1	všechen
ostatní	ostatní	k2eAgNnPc1d1	ostatní
náboženství	náboženství	k1gNnPc1	náboženství
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
citově	citově	k6eAd1	citově
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nejsem	být	k5eNaImIp1nS	být
účasten	účasten	k2eAgInSc1d1	účasten
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
vždy	vždy	k6eAd1	vždy
silný	silný	k2eAgInSc4d1	silný
cit	cit	k1gInSc4	cit
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
národem	národ	k1gInSc7	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
jsem	být	k5eAaImIp1nS	být
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
u	u	k7c2	u
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsme	být	k5eAaImIp1nP	být
zůstali	zůstat	k5eAaPmAgMnP	zůstat
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Freud	Freud	k1gInSc1	Freud
skutečně	skutečně	k6eAd1	skutečně
judaismus	judaismus	k1gInSc4	judaismus
nijak	nijak	k6eAd1	nijak
nešetřil	šetřit	k5eNaImAgMnS	šetřit
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgNnPc7d1	jiné
náboženstvími	náboženství	k1gNnPc7	náboženství
–	–	k?	–
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Muž	muž	k1gMnSc1	muž
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
a	a	k8xC	a
monoteistické	monoteistický	k2eAgNnSc4d1	monoteistické
náboženství	náboženství	k1gNnSc4	náboženství
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
byl	být	k5eAaImAgMnS	být
původem	původ	k1gInSc7	původ
Egypťan	Egypťan	k1gMnSc1	Egypťan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
založením	založení	k1gNnSc7	založení
judaismu	judaismus	k1gInSc2	judaismus
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zachránit	zachránit	k5eAaPmF	zachránit
monoteistický	monoteistický	k2eAgInSc4d1	monoteistický
Atonův	Atonův	k2eAgInSc4d1	Atonův
kult	kult	k1gInSc4	kult
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
židovským	židovský	k2eAgInSc7d1	židovský
lidem	lid	k1gInSc7	lid
zabit	zabit	k2eAgInSc1d1	zabit
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
"	"	kIx"	"
<g/>
židovský	židovský	k2eAgInSc4d1	židovský
pocit	pocit	k1gInSc4	pocit
viny	vina	k1gFnSc2	vina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
židovské	židovský	k2eAgFnSc2d1	židovská
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Freuda	Freud	k1gMnSc2	Freud
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pocit	pocit	k1gInSc4	pocit
viny	vina	k1gFnSc2	vina
základem	základ	k1gInSc7	základ
kultury	kultura	k1gFnSc2	kultura
vždy	vždy	k6eAd1	vždy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
filozofa	filozof	k1gMnSc2	filozof
Michela	Michel	k1gMnSc2	Michel
Onfraye	Onfraye	k1gFnSc4	Onfraye
bylo	být	k5eAaImAgNnS	být
krajně	krajně	k6eAd1	krajně
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
teze	teze	k1gFnPc4	teze
Freud	Freud	k1gInSc1	Freud
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
silného	silný	k2eAgNnSc2d1	silné
pronásledování	pronásledování	k1gNnSc2	pronásledování
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
je	být	k5eAaImIp3nS	být
však	však	k9	však
neobhajitelné	obhajitelný	k2eNgNnSc1d1	neobhajitelné
<g/>
,	,	kIx,	,
Freud	Freud	k1gMnSc1	Freud
si	se	k3xPyFc3	se
židovské	židovský	k2eAgFnPc1d1	židovská
kultury	kultura	k1gFnPc1	kultura
vždy	vždy	k6eAd1	vždy
vážil	vážit	k5eAaImAgInS	vážit
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nebral	brát	k5eNaImAgMnS	brát
honoráře	honorář	k1gInPc4	honorář
za	za	k7c4	za
překlad	překlad	k1gInSc4	překlad
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
do	do	k7c2	do
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Johnson	Johnson	k1gMnSc1	Johnson
ho	on	k3xPp3gNnSc4	on
dokonce	dokonce	k9	dokonce
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejvýraznějšího	výrazný	k2eAgMnSc2d3	nejvýraznější
představitele	představitel	k1gMnSc2	představitel
židovství	židovství	k1gNnSc2	židovství
<g/>
"	"	kIx"	"
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
židovství	židovství	k1gNnSc6	židovství
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Maxi	maxi	k6eAd1	maxi
Grafovi	Grafův	k2eAgMnPc1d1	Grafův
jednou	jednou	k6eAd1	jednou
prý	prý	k9	prý
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
neumožníš	umožnit	k5eNaPmIp2nS	umožnit
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
jako	jako	k9	jako
Žid	Žid	k1gMnSc1	Žid
<g/>
,	,	kIx,	,
zbavíš	zbavit	k5eAaPmIp2nS	zbavit
ho	on	k3xPp3gInSc4	on
tím	ten	k3xDgNnSc7	ten
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nedají	dát	k5eNaPmIp3nP	dát
ničím	ničí	k3xOyNgMnPc3	ničí
jiným	jiný	k1gMnPc3	jiný
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Není	být	k5eNaImIp3nS	být
však	však	k9	však
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k8xS	co
tím	ten	k3xDgNnSc7	ten
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
výchovu	výchova	k1gFnSc4	výchova
vždy	vždy	k6eAd1	vždy
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
ani	ani	k8xC	ani
jemu	on	k3xPp3gNnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
nedostalo	dostat	k5eNaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
zřejmě	zřejmě	k6eAd1	zřejmě
neměl	mít	k5eNaImAgMnS	mít
zcela	zcela	k6eAd1	zcela
jasno	jasno	k6eAd1	jasno
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
vyjádření	vyjádření	k1gNnSc1	vyjádření
v	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
k	k	k7c3	k
hebrejskému	hebrejský	k2eAgNnSc3d1	hebrejské
vydání	vydání	k1gNnSc3	vydání
Totemu	totem	k1gInSc2	totem
a	a	k8xC	a
tabu	tabu	k1gNnSc1	tabu
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Žádný	žádný	k3yNgMnSc1	žádný
ze	z	k7c2	z
čtenářů	čtenář	k1gMnPc2	čtenář
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
vmyslet	vmyslet	k5eAaPmF	vmyslet
do	do	k7c2	do
citového	citový	k2eAgNnSc2d1	citové
rozpoložení	rozpoložení	k1gNnSc2	rozpoložení
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nerozumí	rozumět	k5eNaImIp3nS	rozumět
svatému	svatý	k2eAgInSc3d1	svatý
jazyku	jazyk	k1gInSc3	jazyk
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc3	náboženství
otců	otec	k1gMnPc2	otec
–	–	k?	–
jako	jako	k8xC	jako
každému	každý	k3xTgMnSc3	každý
jinému	jiný	k1gMnSc3	jiný
–	–	k?	–
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
odcizen	odcizen	k2eAgInSc4d1	odcizen
<g/>
,	,	kIx,	,
nacionalistické	nacionalistický	k2eAgInPc4d1	nacionalistický
ideály	ideál	k1gInPc4	ideál
nemůže	moct	k5eNaImIp3nS	moct
sdílet	sdílet	k5eAaImF	sdílet
<g/>
,	,	kIx,	,
a	a	k8xC	a
přece	přece	k9	přece
nikdy	nikdy	k6eAd1	nikdy
nezapíral	zapírat	k5eNaImAgMnS	zapírat
příslušnost	příslušnost	k1gFnSc4	příslušnost
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
svéráz	svéráz	k1gInSc4	svéráz
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
jako	jako	k9	jako
židovský	židovský	k2eAgMnSc1d1	židovský
a	a	k8xC	a
nepřeje	přát	k5eNaImIp3nS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tomu	ten	k3xDgMnSc3	ten
bylo	být	k5eAaImAgNnS	být
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Zeptali	zeptat	k5eAaPmAgMnP	zeptat
<g/>
-li	i	k?	-li
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
<g/>
:	:	kIx,	:
co	co	k9	co
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tobě	ty	k3xPp2nSc6	ty
židovského	židovský	k2eAgMnSc2d1	židovský
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsi	být	k5eAaImIp2nS	být
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
jsi	být	k5eAaImIp2nS	být
měl	mít	k5eAaImAgMnS	mít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
soukmenovci	soukmenovec	k1gMnPc7	soukmenovec
společné	společný	k2eAgNnSc1d1	společné
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
potom	potom	k6eAd1	potom
by	by	kYmCp3nS	by
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
ještě	ještě	k9	ještě
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
to	ten	k3xDgNnSc4	ten
hlavní	hlavní	k2eAgNnSc4d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc4	tento
podstatné	podstatný	k2eAgNnSc4d1	podstatné
by	by	kYmCp3nS	by
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jasnými	jasný	k2eAgNnPc7d1	jasné
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Otec	otec	k1gMnSc1	otec
i	i	k8xC	i
matka	matka	k1gFnSc1	matka
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Haliče	Halič	k1gFnSc2	Halič
<g/>
,	,	kIx,	,
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
z	z	k7c2	z
města	město	k1gNnSc2	město
Brody	Brod	k1gInPc4	Brod
<g/>
,	,	kIx,	,
centra	centrum	k1gNnPc4	centrum
chasidské	chasidský	k2eAgFnSc2d1	chasidská
kultury	kultura	k1gFnSc2	kultura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
data	datum	k1gNnSc2	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
panují	panovat	k5eAaImIp3nP	panovat
nejasnosti	nejasnost	k1gFnPc4	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Určitě	určitě	k6eAd1	určitě
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
nějakého	nějaký	k3yIgInSc2	nějaký
šestého	šestý	k4xOgInSc2	šestý
dne	den	k1gInSc2	den
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
všichni	všechen	k3xTgMnPc1	všechen
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nesporné	sporný	k2eNgNnSc4d1	nesporné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tehdy	tehdy	k6eAd1	tehdy
radní	radní	k1gMnPc1	radní
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
chtěli	chtít	k5eAaImAgMnP	chtít
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
rodáka	rodák	k1gMnSc2	rodák
vytvořit	vytvořit	k5eAaPmF	vytvořit
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
po	po	k7c4	po
nahlédnutí	nahlédnutí	k1gNnSc4	nahlédnutí
do	do	k7c2	do
obecní	obecní	k2eAgFnSc2d1	obecní
matriky	matrika	k1gFnSc2	matrika
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
písař	písař	k1gMnSc1	písař
jasně	jasně	k6eAd1	jasně
poznačil	poznačit	k5eAaPmAgMnS	poznačit
6	[number]	k4	6
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
bibli	bible	k1gFnSc6	bible
otec	otec	k1gMnSc1	otec
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
narození	narození	k1gNnSc4	narození
na	na	k7c4	na
úterý	úterý	k1gNnSc4	úterý
měsíce	měsíc	k1gInSc2	měsíc
roš	roš	k?	roš
chodeš	chodat	k5eAaBmIp2nS	chodat
roku	rok	k1gInSc2	rok
5616	[number]	k4	5616
(	(	kIx(	(
<g/>
po	po	k7c6	po
nesmírně	smírně	k6eNd1	smírně
složitém	složitý	k2eAgInSc6d1	složitý
výpočtu	výpočet	k1gInSc6	výpočet
z	z	k7c2	z
židovského	židovský	k2eAgInSc2d1	židovský
letopočtu	letopočet	k1gInSc2	letopočet
se	se	k3xPyFc4	se
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
možná	možná	k9	možná
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mezi	mezi	k7c7	mezi
svatbou	svatba	k1gFnSc7	svatba
a	a	k8xC	a
narozením	narození	k1gNnSc7	narození
prvního	první	k4xOgNnSc2	první
dítěte	dítě	k1gNnSc2	dítě
bylo	být	k5eAaImAgNnS	být
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
Sigmund	Sigmund	k1gMnSc1	Sigmund
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sigismund	Sigismund	k1gInSc1	Sigismund
<g/>
.	.	kIx.	.
</s>
<s>
Změnil	změnit	k5eAaPmAgMnS	změnit
si	se	k3xPyFc3	se
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
studiích	studio	k1gNnPc6	studio
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgMnSc3	svůj
příbuznému	příbuzný	k1gMnSc3	příbuzný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
rovněž	rovněž	k9	rovněž
Sigismund	Sigismund	k1gMnSc1	Sigismund
<g/>
.	.	kIx.	.
</s>
<s>
Octave	Octavat	k5eAaPmIp3nS	Octavat
Mannoni	Mannoň	k1gFnSc6	Mannoň
však	však	k9	však
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
změna	změna	k1gFnSc1	změna
jména	jméno	k1gNnSc2	jméno
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
hlubší	hluboký	k2eAgInPc4d2	hlubší
psychologické	psychologický	k2eAgInPc4d1	psychologický
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
že	že	k8xS	že
podobně	podobně	k6eAd1	podobně
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
slovo	slovo	k1gNnSc4	slovo
narcismus	narcismus	k1gInSc1	narcismus
namísto	namísto	k7c2	namísto
správného	správný	k2eAgMnSc2d1	správný
narcissismus	narcissismus	k1gInSc1	narcissismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rodinné	rodinný	k2eAgFnSc2d1	rodinná
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Freud	Freud	k1gMnSc1	Freud
připomíná	připomínat	k5eAaImIp3nS	připomínat
ve	v	k7c6	v
Výkladu	výklad	k1gInSc6	výklad
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
při	při	k7c6	při
analýze	analýza	k1gFnSc6	analýza
snu	sen	k1gInSc2	sen
o	o	k7c6	o
strýci	strýc	k1gMnSc6	strýc
Josefovi	Josef	k1gMnSc6	Josef
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakási	jakýsi	k3yIgFnSc1	jakýsi
selka	selka	k1gFnSc1	selka
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
prorokovala	prorokovat	k5eAaImAgFnS	prorokovat
jeho	jeho	k3xOp3gFnSc3	jeho
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
právě	právě	k6eAd1	právě
darovala	darovat	k5eAaPmAgFnS	darovat
světu	svět	k1gInSc3	svět
velkého	velký	k2eAgMnSc2d1	velký
muže	muž	k1gMnSc2	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Freudova	Freudův	k2eAgFnSc1d1	Freudova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
manželky	manželka	k1gFnPc1	manželka
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
záhy	záhy	k6eAd1	záhy
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Sigismund	Sigismund	k1gMnSc1	Sigismund
Freud	Freud	k1gMnSc1	Freud
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
už	už	k6eAd1	už
dva	dva	k4xCgMnPc4	dva
dospělé	dospělí	k1gMnPc4	dospělí
nevlastní	vlastní	k2eNgMnPc4d1	nevlastní
bratry	bratr	k1gMnPc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
prý	prý	k9	prý
původně	původně	k6eAd1	původně
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
dědečka	dědeček	k1gMnSc4	dědeček
a	a	k8xC	a
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
bratra	bratr	k1gMnSc4	bratr
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
mámin	mámin	k2eAgMnSc1d1	mámin
mazlíček	mazlíček	k1gMnSc1	mazlíček
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
blíž	blízce	k6eAd2	blízce
než	než	k8xS	než
k	k	k7c3	k
sourozencům	sourozenec	k1gMnPc3	sourozenec
měl	mít	k5eAaImAgMnS	mít
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
stejně	stejně	k6eAd1	stejně
starému	starý	k2eAgMnSc3d1	starý
synovci	synovec	k1gMnSc3	synovec
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
nerozluční	rozluční	k2eNgMnPc1d1	nerozluční
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
synovci	synovec	k1gMnSc3	synovec
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
klíčové	klíčový	k2eAgNnSc4d1	klíčové
téma	téma	k1gNnSc4	téma
své	svůj	k3xOyFgFnSc2	svůj
autoanalýzy	autoanalýza	k1gFnSc2	autoanalýza
a	a	k8xC	a
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
skrze	skrze	k?	skrze
něj	on	k3xPp3gMnSc2	on
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
některým	některý	k3yIgMnPc3	některý
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
žákům	žák	k1gMnPc3	žák
–	–	k?	–
potřebou	potřeba	k1gFnSc7	potřeba
blízkého	blízký	k2eAgMnSc2d1	blízký
přítele	přítel	k1gMnSc2	přítel
si	se	k3xPyFc3	se
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
ho	on	k3xPp3gInSc4	on
brzy	brzy	k6eAd1	brzy
zapudit	zapudit	k5eAaPmF	zapudit
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
vždy	vždy	k6eAd1	vždy
vykresloval	vykreslovat	k5eAaImAgInS	vykreslovat
jako	jako	k9	jako
obětavou	obětavý	k2eAgFnSc4d1	obětavá
a	a	k8xC	a
milující	milující	k2eAgFnSc4d1	milující
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
Freudových	Freudový	k2eAgNnPc2d1	Freudový
vnoučat	vnouče	k1gNnPc2	vnouče
však	však	k9	však
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dominantní	dominantní	k2eAgFnSc4d1	dominantní
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nikdy	nikdy	k6eAd1	nikdy
neopouštěla	opouštět	k5eNaImAgFnS	opouštět
štiplavá	štiplavý	k2eAgFnSc1d1	štiplavá
ironie	ironie	k1gFnSc1	ironie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
měla	mít	k5eAaImAgFnS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
frau	frau	k6eAd1	frau
tornado	tornada	k1gFnSc5	tornada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
třídě	třída	k1gFnSc6	třída
(	(	kIx(	(
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
chování	chování	k1gNnSc2	chování
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
(	(	kIx(	(
<g/>
čtyřku	čtyřka	k1gFnSc4	čtyřka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
věděl	vědět	k5eAaImAgMnS	vědět
totiž	totiž	k9	totiž
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
spolužáci	spolužák	k1gMnPc1	spolužák
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
vykřičené	vykřičený	k2eAgInPc4d1	vykřičený
domy	dům	k1gInPc4	dům
a	a	k8xC	a
lokály	lokál	k1gInPc4	lokál
a	a	k8xC	a
nikomu	nikdo	k3yNnSc3	nikdo
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Známka	známka	k1gFnSc1	známka
snížená	snížený	k2eAgFnSc1d1	snížená
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvou	dva	k4xCgNnPc6	dva
pololetích	pololetí	k1gNnPc6	pololetí
opět	opět	k6eAd1	opět
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
výbornou	výborná	k1gFnSc4	výborná
a	a	k8xC	a
tam	tam	k6eAd1	tam
už	už	k6eAd1	už
zůstala	zůstat	k5eAaPmAgFnS	zůstat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
Freud	Freud	k1gInSc1	Freud
často	často	k6eAd1	často
mluvčím	mluvčit	k5eAaImIp1nS	mluvčit
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
chlapců	chlapec	k1gMnPc2	chlapec
-	-	kIx~	-
učitelé	učitel	k1gMnPc1	učitel
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
chování	chování	k1gNnSc4	chování
u	u	k7c2	u
jinak	jinak	k6eAd1	jinak
pilného	pilný	k2eAgMnSc2d1	pilný
žáka	žák	k1gMnSc2	žák
se	s	k7c7	s
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
povinnost	povinnost	k1gFnSc4	povinnost
tolerovali	tolerovat	k5eAaImAgMnP	tolerovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
snoubenkou	snoubenka	k1gFnSc7	snoubenka
čekal	čekat	k5eAaImAgInS	čekat
Freud	Freud	k1gInSc1	Freud
přes	přes	k7c4	přes
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
matka	matka	k1gFnSc1	matka
snoubenky	snoubenka	k1gFnSc2	snoubenka
nechtěla	chtít	k5eNaImAgFnS	chtít
dát	dát	k5eAaPmF	dát
dceru	dcera	k1gFnSc4	dcera
nezajištěnému	zajištěný	k2eNgMnSc3d1	nezajištěný
mladíkovi	mladík	k1gMnSc3	mladík
bez	bez	k7c2	bez
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgFnSc3	svůj
snoubence	snoubenka	k1gFnSc3	snoubenka
zhruba	zhruba	k6eAd1	zhruba
1500	[number]	k4	1500
mnohostránkových	mnohostránkový	k2eAgInPc2d1	mnohostránkový
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
dosud	dosud	k6eAd1	dosud
jen	jen	k9	jen
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
životopisců	životopisec	k1gMnPc2	životopisec
nebyl	být	k5eNaImAgMnS	být
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
nikdy	nikdy	k6eAd1	nikdy
nevěrný	věrný	k2eNgInSc1d1	nevěrný
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
spekulace	spekulace	k1gFnSc2	spekulace
(	(	kIx(	(
<g/>
kterou	který	k3yIgFnSc4	který
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
Jung	Jung	k1gMnSc1	Jung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
poměr	poměr	k1gInSc1	poměr
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
švagrovou	švagrův	k2eAgFnSc7d1	švagrova
Minnou	Minný	k2eAgFnSc7d1	Minný
Bernaysovou	Bernaysová	k1gFnSc7	Bernaysová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jeden	jeden	k4xCgInSc4	jeden
čas	čas	k1gInSc4	čas
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
Freudovými	Freudový	k2eAgFnPc7d1	Freudový
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
spekulaci	spekulace	k1gFnSc4	spekulace
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
kupříkladu	kupříkladu	k6eAd1	kupříkladu
antifreudovsky	antifreudovsky	k6eAd1	antifreudovsky
naladěný	naladěný	k2eAgMnSc1d1	naladěný
Hans	Hans	k1gMnSc1	Hans
Eysenck	Eysenck	k1gMnSc1	Eysenck
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
měl	mít	k5eAaImAgInS	mít
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
Mathilde	Mathild	k1gInSc5	Mathild
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
Jean-Martin	Jean-Martin	k1gMnSc1	Jean-Martin
(	(	kIx(	(
<g/>
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
Charcota	Charcot	k1gMnSc2	Charcot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
Olivier	Olivier	k1gMnSc1	Olivier
(	(	kIx(	(
<g/>
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
Olivera	Oliver	k1gMnSc2	Oliver
Cromwella	Cromwell	k1gMnSc2	Cromwell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
Ernst	Ernst	k1gMnSc1	Ernst
(	(	kIx(	(
<g/>
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
Brückeho	Brücke	k1gMnSc2	Brücke
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
Sophie	Sophie	k1gFnSc2	Sophie
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
významnou	významný	k2eAgFnSc7d1	významná
psychoanalytičkou	psychoanalytička	k1gFnSc7	psychoanalytička
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
byl	být	k5eAaImAgInS	být
architektem	architekt	k1gMnSc7	architekt
a	a	k8xC	a
angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
sionistickém	sionistický	k2eAgNnSc6d1	sionistické
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Lucian	Lucian	k1gMnSc1	Lucian
Freud	Freud	k1gMnSc1	Freud
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
významným	významný	k2eAgMnSc7d1	významný
malířem	malíř	k1gMnSc7	malíř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Clement	Clement	k1gMnSc1	Clement
Freud	Freud	k1gMnSc1	Freud
spisovatelem	spisovatel	k1gMnSc7	spisovatel
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Grimble	Grimble	k1gFnSc2	Grimble
<g/>
)	)	kIx)	)
a	a	k8xC	a
politikem	politik	k1gMnSc7	politik
(	(	kIx(	(
<g/>
poslanec	poslanec	k1gMnSc1	poslanec
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
Liberální	liberální	k2eAgFnSc4d1	liberální
stranu	strana	k1gFnSc4	strana
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lucianova	Lucianův	k2eAgFnSc1d1	Lucianova
dcera	dcera	k1gFnSc1	dcera
Bella	Bello	k1gNnSc2	Bello
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
módní	módní	k2eAgFnSc7d1	módní
návrhářkou	návrhářka	k1gFnSc7	návrhářka
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
Esther	Esthra	k1gFnPc2	Esthra
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
(	(	kIx(	(
<g/>
Její	její	k3xOp3gInSc4	její
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
román	román	k1gInSc4	román
Hideous	Hideous	k1gInSc1	Hideous
Kinky	Kinka	k1gFnSc2	Kinka
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
i	i	k9	i
filmového	filmový	k2eAgNnSc2d1	filmové
zpracování	zpracování	k1gNnSc2	zpracování
s	s	k7c7	s
Kate	kat	k1gInSc5	kat
Winsletovou	Winsletový	k2eAgFnSc7d1	Winsletová
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Clementova	Clementův	k2eAgFnSc1d1	Clementův
dcera	dcera	k1gFnSc1	dcera
Emma	Emma	k1gFnSc1	Emma
je	být	k5eAaImIp3nS	být
manželkou	manželka	k1gFnSc7	manželka
scenáristy	scenárista	k1gMnSc2	scenárista
Richarda	Richard	k1gMnSc2	Richard
Curtise	Curtise	k1gFnSc2	Curtise
(	(	kIx(	(
<g/>
autora	autor	k1gMnSc2	autor
komedií	komedie	k1gFnSc7	komedie
Notting	Notting	k1gInSc4	Notting
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgFnPc1	čtyři
svatby	svatba	k1gFnPc1	svatba
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
Bridget	Bridgeta	k1gFnPc2	Bridgeta
Jonesové	Jonesový	k2eAgNnSc1d1	Jonesové
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
či	či	k8xC	či
seriálu	seriál	k1gInSc2	seriál
Černá	Černá	k1gFnSc1	Černá
zmije	zmije	k1gFnSc1	zmije
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Mathew	Mathew	k1gMnSc1	Mathew
se	se	k3xPyFc4	se
zase	zase	k9	zase
přiženil	přiženit	k5eAaPmAgMnS	přiženit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
mediálního	mediální	k2eAgMnSc4d1	mediální
magnáta	magnát	k1gMnSc4	magnát
Ruperta	Rupert	k1gMnSc4	Rupert
Murdocha	Murdoch	k1gMnSc4	Murdoch
<g/>
.	.	kIx.	.
</s>
<s>
Freudův	Freudův	k2eAgMnSc1d1	Freudův
synovec	synovec	k1gMnSc1	synovec
Edward	Edward	k1gMnSc1	Edward
L.	L.	kA	L.
Bernays	Bernays	k1gInSc1	Bernays
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderních	moderní	k2eAgNnPc2d1	moderní
public	publicum	k1gNnPc2	publicum
relations	relations	k6eAd1	relations
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
rád	rád	k6eAd1	rád
hrál	hrát	k5eAaImAgMnS	hrát
taroky	tarok	k1gInPc4	tarok
a	a	k8xC	a
hru	hra	k1gFnSc4	hra
Go	Go	k1gFnSc2	Go
<g/>
.	.	kIx.	.
</s>
<s>
Choval	chovat	k5eAaImAgMnS	chovat
psy	pes	k1gMnPc4	pes
čau-čau	čau-čau	k6eAd1	čau-čau
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
kokain	kokain	k1gInSc4	kokain
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
droga	droga	k1gFnSc1	droga
<g/>
,	,	kIx,	,
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
neškodnou	škodný	k2eNgFnSc7d1	neškodná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
povzbuzující	povzbuzující	k2eAgInPc4d1	povzbuzující
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
dost	dost	k6eAd1	dost
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Posílal	posílat	k5eAaImAgMnS	posílat
jej	on	k3xPp3gMnSc4	on
snoubence	snoubenec	k1gMnSc4	snoubenec
i	i	k9	i
přátelům	přítel	k1gMnPc3	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
přišel	přijít	k5eAaPmAgInS	přijít
i	i	k9	i
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
znecitlivující	znecitlivující	k2eAgInPc4d1	znecitlivující
účinky	účinek	k1gInPc4	účinek
(	(	kIx(	(
<g/>
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
aplikaci	aplikace	k1gFnSc6	aplikace
mu	on	k3xPp3gMnSc3	on
zdřevěněl	zdřevěnět	k5eAaPmAgInS	zdřevěnět
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
ověřil	ověřit	k5eAaPmAgMnS	ověřit
i	i	k9	i
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
když	když	k9	když
chtěl	chtít	k5eAaImAgMnS	chtít
pomoci	pomoct	k5eAaPmF	pomoct
asistentovi	asistentův	k2eAgMnPc1d1	asistentův
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
na	na	k7c4	na
děsivé	děsivý	k2eAgFnPc4d1	děsivá
bolesti	bolest	k1gFnPc4	bolest
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Freud	Freud	k1gInSc1	Freud
kokain	kokain	k1gInSc1	kokain
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
následoval	následovat	k5eAaImAgInS	následovat
houf	houf	k1gInSc4	houf
zvědavých	zvědavý	k2eAgMnPc2d1	zvědavý
kolegů	kolega	k1gMnPc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
droze	droga	k1gFnSc3	droga
asistenta	asistent	k1gMnSc2	asistent
bolesti	bolest	k1gFnSc2	bolest
sužovat	sužovat	k5eAaImF	sužovat
přestaly	přestat	k5eAaPmAgFnP	přestat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
všechny	všechen	k3xTgMnPc4	všechen
udivilo	udivit	k5eAaPmAgNnS	udivit
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
poté	poté	k6eAd1	poté
odjel	odjet	k5eAaPmAgMnS	odjet
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
snoubenkou	snoubenka	k1gFnSc7	snoubenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dva	dva	k4xCgMnPc1	dva
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
úspěchu	úspěch	k1gInSc3	úspěch
koky	kokus	k1gMnPc7	kokus
přihlíželi	přihlížet	k5eAaImAgMnP	přihlížet
–	–	k?	–
Carl	Carl	k1gMnSc1	Carl
Koller	Koller	k1gMnSc1	Koller
a	a	k8xC	a
Leopold	Leopold	k1gMnSc1	Leopold
Königstein	Königstein	k1gMnSc1	Königstein
–	–	k?	–
ji	on	k3xPp3gFnSc4	on
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
pilně	pilně	k6eAd1	pilně
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Freud	Freud	k1gMnSc1	Freud
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
dověděl	dovědět	k5eAaPmAgMnS	dovědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
doktor	doktor	k1gMnSc1	doktor
Koller	Koller	k1gMnSc1	Koller
splnil	splnit	k5eAaPmAgMnS	splnit
sen	sen	k1gInSc4	sen
každého	každý	k3xTgMnSc2	každý
anesteziologa	anesteziolog	k1gMnSc2	anesteziolog
–	–	k?	–
založil	založit	k5eAaPmAgMnS	založit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
kokainu	kokain	k1gInSc2	kokain
lokální	lokální	k2eAgFnSc3d1	lokální
anesteziologii	anesteziologie	k1gFnSc3	anesteziologie
<g/>
.	.	kIx.	.
</s>
<s>
Operovat	operovat	k5eAaImF	operovat
například	například	k6eAd1	například
oči	oko	k1gNnPc4	oko
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
možné	možný	k2eAgFnPc4d1	možná
bezbolestně	bezbolestně	k6eAd1	bezbolestně
a	a	k8xC	a
při	při	k7c6	při
vědomí	vědomí	k1gNnSc6	vědomí
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
tak	tak	k6eAd1	tak
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
proslavit	proslavit	k5eAaPmF	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
Kollerovi	Kollerův	k2eAgMnPc1d1	Kollerův
nic	nic	k3yNnSc1	nic
nezazlíval	zazlívat	k5eNaImAgMnS	zazlívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
řady	řada	k1gFnSc2	řada
životopisců	životopisec	k1gMnPc2	životopisec
cítil	cítit	k5eAaImAgMnS	cítit
určitou	určitý	k2eAgFnSc4d1	určitá
křivdu	křivda	k1gFnSc4	křivda
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
Koller	Koller	k1gMnSc1	Koller
(	(	kIx(	(
<g/>
rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
Sušice	Sušice	k1gFnSc2	Sušice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Königstein	Königstein	k1gInSc1	Königstein
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
objev	objev	k1gInSc1	objev
aplikoval	aplikovat	k5eAaBmAgInS	aplikovat
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezmínili	zmínit	k5eNaPmAgMnP	zmínit
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
prvenství	prvenství	k1gNnSc4	prvenství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
spálil	spálit	k5eAaPmAgInS	spálit
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
vědecké	vědecký	k2eAgFnPc4d1	vědecká
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
výpisky	výpisek	k1gInPc4	výpisek
a	a	k8xC	a
dopisy	dopis	k1gInPc4	dopis
(	(	kIx(	(
<g/>
krom	krom	k7c2	krom
těch	ten	k3xDgInPc2	ten
od	od	k7c2	od
snoubenky	snoubenka	k1gFnSc2	snoubenka
Marty	Marta	k1gFnSc2	Marta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Martě	Marta	k1gFnSc3	Marta
to	ten	k3xDgNnSc4	ten
následně	následně	k6eAd1	následně
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Životopisci	životopisec	k1gMnSc3	životopisec
ať	ať	k8xC	ať
se	se	k3xPyFc4	se
moří	mořit	k5eAaImIp3nS	mořit
<g/>
,	,	kIx,	,
nebudeme	být	k5eNaImBp1nP	být
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc4	ten
ulehčovat	ulehčovat	k5eAaImF	ulehčovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
se	se	k3xPyFc4	se
těším	těšit	k5eAaImIp1nS	těšit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
mýlit	mýlit	k5eAaImF	mýlit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Disponoval	disponovat	k5eAaBmAgMnS	disponovat
tzv.	tzv.	kA	tzv.
fonografickou	fonografický	k2eAgFnSc7d1	Fonografická
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
po	po	k7c6	po
vyslechnutí	vyslechnutí	k1gNnSc6	vyslechnutí
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
promluv	promluva	k1gFnPc2	promluva
takřka	takřka	k6eAd1	takřka
přesně	přesně	k6eAd1	přesně
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
odstupu	odstup	k1gInSc2	odstup
písemně	písemně	k6eAd1	písemně
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
–	–	k?	–
takto	takto	k6eAd1	takto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
jeho	jeho	k3xOp3gFnPc1	jeho
spis	spis	k1gInSc4	spis
Přednášky	přednáška	k1gFnSc2	přednáška
k	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
psychoanalysy	psychoanalysa	k1gFnSc2	psychoanalysa
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc4	přednáška
pronášel	pronášet	k5eAaImAgInS	pronášet
zpatra	zpatrum	k1gNnPc4	zpatrum
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
večer	večer	k6eAd1	večer
je	být	k5eAaImIp3nS	být
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
svědků	svědek	k1gMnPc2	svědek
takřka	takřka	k6eAd1	takřka
v	v	k7c6	v
doslovném	doslovný	k2eAgNnSc6d1	doslovné
znění	znění	k1gNnSc6	znění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
podmiňovala	podmiňovat	k5eAaImAgFnS	podmiňovat
jeho	jeho	k3xOp3gNnSc4	jeho
snadné	snadný	k2eAgNnSc1d1	snadné
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
cizím	cizit	k5eAaImIp1nS	cizit
jazykům	jazyk	k1gMnPc3	jazyk
i	i	k8xC	i
práci	práce	k1gFnSc3	práce
s	s	k7c7	s
pacienty	pacient	k1gMnPc7	pacient
–	–	k?	–
Freud	Freud	k1gInSc1	Freud
si	se	k3xPyFc3	se
při	při	k7c6	při
psychoanalytickém	psychoanalytický	k2eAgNnSc6d1	psychoanalytické
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc1	nic
nezapisoval	zapisovat	k5eNaImAgMnS	zapisovat
<g/>
,	,	kIx,	,
domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
pacienty	pacient	k1gMnPc4	pacient
rušilo	rušit	k5eAaImAgNnS	rušit
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc1	poznámka
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
během	během	k7c2	během
dne	den	k1gInSc2	den
provedených	provedený	k2eAgInPc2d1	provedený
analýzách	analýza	k1gFnPc6	analýza
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
udělal	udělat	k5eAaPmAgMnS	udělat
až	až	k9	až
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
trpěl	trpět	k5eAaImAgMnS	trpět
"	"	kIx"	"
<g/>
cestovní	cestovní	k2eAgFnSc7d1	cestovní
neurózou	neuróza	k1gFnSc7	neuróza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jízda	jízda	k1gFnSc1	jízda
vlakem	vlak	k1gInSc7	vlak
ho	on	k3xPp3gNnSc2	on
zneklidňovala	zneklidňovat	k5eAaImAgFnS	zneklidňovat
<g/>
.	.	kIx.	.
</s>
<s>
Autoanalýza	Autoanalýza	k1gFnSc1	Autoanalýza
mu	on	k3xPp3gMnSc3	on
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
železnici	železnice	k1gFnSc3	železnice
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
kolem	kolem	k7c2	kolem
třetího	třetí	k4xOgInSc2	třetí
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
když	když	k8xS	když
rodina	rodina	k1gFnSc1	rodina
opouštěla	opouštět	k5eAaImAgFnS	opouštět
Příbor	Příbor	k1gInSc4	Příbor
a	a	k8xC	a
mířila	mířit	k5eAaImAgFnS	mířit
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
spatřil	spatřit	k5eAaPmAgMnS	spatřit
prvně	prvně	k?	prvně
v	v	k7c6	v
životě	život	k1gInSc6	život
plynové	plynový	k2eAgNnSc4d1	plynové
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gMnSc4	on
poděsilo	poděsit	k5eAaPmAgNnS	poděsit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
připomnělo	připomnět	k5eAaPmAgNnS	připomnět
historky	historka	k1gFnPc4	historka
o	o	k7c6	o
plamenech	plamen	k1gInPc6	plamen
pekelných	pekelný	k2eAgFnPc2d1	pekelná
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgMnPc6	jenž
mu	on	k3xPp3gMnSc3	on
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
jeho	jeho	k3xOp3gFnSc1	jeho
katolická	katolický	k2eAgFnSc1d1	katolická
chůva	chůva	k1gFnSc1	chůva
Monika	Monika	k1gFnSc1	Monika
Zajícová	Zajícová	k1gFnSc1	Zajícová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
významnou	významný	k2eAgFnSc7d1	významná
figurou	figura	k1gFnSc7	figura
jeho	jeho	k3xOp3gNnSc2	jeho
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
Freud	Freud	k1gInSc1	Freud
ji	on	k3xPp3gFnSc4	on
velmi	velmi	k6eAd1	velmi
miloval	milovat	k5eAaImAgInS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
autoanalýze	autoanalýza	k1gFnSc6	autoanalýza
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
vynořil	vynořit	k5eAaPmAgInS	vynořit
její	její	k3xOp3gInSc1	její
obraz	obraz	k1gInSc1	obraz
jako	jako	k8xS	jako
svůdkyně	svůdkyně	k1gFnSc1	svůdkyně
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
vychovatelky	vychovatelka	k1gFnPc4	vychovatelka
<g/>
,	,	kIx,	,
nechávala	nechávat	k5eAaImAgFnS	nechávat
ho	on	k3xPp3gInSc4	on
například	například	k6eAd1	například
koupat	koupat	k5eAaImF	koupat
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
znečištěna	znečistit	k5eAaPmNgFnS	znečistit
její	její	k3xOp3gFnSc1	její
menstruační	menstruační	k2eAgFnSc1d1	menstruační
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
brávala	brávat	k5eAaImAgFnS	brávat
ho	on	k3xPp3gMnSc4	on
sebou	se	k3xPyFc7	se
často	často	k6eAd1	často
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
moderního	moderní	k2eAgInSc2d1	moderní
managementu	management	k1gInSc2	management
Peter	Peter	k1gMnSc1	Peter
Drucker	Drucker	k1gMnSc1	Drucker
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
chlapce	chlapec	k1gMnSc4	chlapec
jednou	jednou	k9	jednou
rodiče	rodič	k1gMnPc4	rodič
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
kavárny	kavárna	k1gFnSc2	kavárna
upozornili	upozornit	k5eAaPmAgMnP	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k6eAd1	právě
"	"	kIx"	"
<g/>
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Drucker	Drucker	k1gMnSc1	Drucker
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zeptal	zeptat	k5eAaPmAgMnS	zeptat
"	"	kIx"	"
<g/>
Důležitější	důležitý	k2eAgMnSc1d2	důležitější
než	než	k8xS	než
císař	císař	k1gMnSc1	císař
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
prý	prý	k9	prý
odpověděli	odpovědět	k5eAaPmAgMnP	odpovědět
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
důležitější	důležitý	k2eAgNnPc1d2	důležitější
než	než	k8xS	než
císař	císař	k1gMnSc1	císař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
mužem	muž	k1gMnSc7	muž
byl	být	k5eAaImAgMnS	být
Freud	Freud	k1gMnSc1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Drucker	Drucker	k1gMnSc1	Drucker
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
nepatřili	patřit	k5eNaImAgMnP	patřit
k	k	k7c3	k
úzkému	úzký	k2eAgInSc3d1	úzký
kruhu	kruh	k1gInSc3	kruh
Freudových	Freudový	k2eAgMnPc2d1	Freudový
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Freud	Freud	k1gMnSc1	Freud
nebyl	být	k5eNaImAgMnS	být
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
zdaleka	zdaleka	k6eAd1	zdaleka
takový	takový	k3xDgMnSc1	takový
vyděděnec	vyděděnec	k1gMnSc1	vyděděnec
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
praví	pravit	k5eAaImIp3nS	pravit
freudovská	freudovský	k2eAgFnSc1d1	freudovská
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
uznáván	uznáván	k2eAgInSc1d1	uznáván
jako	jako	k8xC	jako
velká	velký	k2eAgFnSc1d1	velká
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
Freudovi	Freuda	k1gMnSc3	Freuda
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
USA	USA	kA	USA
zřejmě	zřejmě	k6eAd1	zřejmě
vůbec	vůbec	k9	vůbec
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
přijetí	přijetí	k1gNnSc2	přijetí
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
pronikla	proniknout	k5eAaPmAgFnS	proniknout
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
nedůvěřoval	důvěřovat	k5eNaImAgMnS	důvěřovat
<g/>
,	,	kIx,	,
obával	obávat	k5eAaImAgMnS	obávat
se	se	k3xPyFc4	se
povrchního	povrchní	k2eAgInSc2d1	povrchní
amerického	americký	k2eAgInSc2d1	americký
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
věci	věc	k1gFnSc3	věc
<g/>
.	.	kIx.	.
</s>
<s>
Podílelo	podílet	k5eAaImAgNnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
několik	několik	k4yIc1	několik
kuriózních	kuriózní	k2eAgFnPc2d1	kuriózní
příhod	příhoda	k1gFnPc2	příhoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
Freud	Freud	k1gInSc1	Freud
nebyl	být	k5eNaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
:	:	kIx,	:
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
mu	on	k3xPp3gMnSc3	on
slavný	slavný	k2eAgMnSc1d1	slavný
filmový	filmový	k2eAgMnSc1d1	filmový
magnát	magnát	k1gMnSc1	magnát
Samuel	Samuel	k1gMnSc1	Samuel
Goldwyn	Goldwyn	k1gMnSc1	Goldwyn
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
když	když	k8xS	když
přijede	přijet	k5eAaPmIp3nS	přijet
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
k	k	k7c3	k
sérii	série	k1gFnSc3	série
filmů	film	k1gInPc2	film
o	o	k7c6	o
milostných	milostný	k2eAgFnPc6d1	milostná
romancích	romance	k1gFnPc6	romance
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
mezi	mezi	k7c7	mezi
Kleopatrou	Kleopatra	k1gFnSc7	Kleopatra
a	a	k8xC	a
Marcem	Marce	k1gMnSc7	Marce
Antoniem	Antonio	k1gMnSc7	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
nabídky	nabídka	k1gFnPc1	nabídka
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
sesypaly	sesypat	k5eAaPmAgInP	sesypat
z	z	k7c2	z
USA	USA	kA	USA
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
případu	případ	k1gInSc2	případ
Loeb-Leopold	Loeb-Leopolda	k1gFnPc2	Loeb-Leopolda
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
dva	dva	k4xCgMnPc1	dva
chicagští	chicagský	k2eAgMnPc1d1	chicagský
chlapci	chlapec	k1gMnPc1	chlapec
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
svou	svůj	k3xOyFgFnSc4	svůj
čtrnáctiletou	čtrnáctiletý	k2eAgFnSc4d1	čtrnáctiletá
příbuznou	příbuzná	k1gFnSc4	příbuzná
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
spáchali	spáchat	k5eAaPmAgMnP	spáchat
"	"	kIx"	"
<g/>
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
zločin	zločin	k1gInSc1	zločin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mediální	mediální	k2eAgMnSc1d1	mediální
magnát	magnát	k1gMnSc1	magnát
William	William	k1gInSc4	William
Randolph	Randolph	k1gMnSc1	Randolph
Hearst	Hearst	k1gMnSc1	Hearst
Freudovi	Freuda	k1gMnSc3	Freuda
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vypraví	vypravit	k5eAaPmIp3nS	vypravit
soukromou	soukromý	k2eAgFnSc4d1	soukromá
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
přijede	přijet	k5eAaPmIp3nS	přijet
svědčit	svědčit	k5eAaImF	svědčit
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
chicagské	chicagský	k2eAgFnPc1d1	Chicagská
noviny	novina	k1gFnPc1	novina
Tribune	tribun	k1gMnSc5	tribun
zase	zase	k9	zase
pět	pět	k4xCc4	pět
tisíc	tisíc	k4xCgInSc4	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
když	když	k8xS	když
podrobí	podrobit	k5eAaPmIp3nS	podrobit
oba	dva	k4xCgMnPc4	dva
vrahy	vrah	k1gMnPc4	vrah
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Novinář	novinář	k1gMnSc1	novinář
Billy	Bill	k1gMnPc4	Bill
Wilder	Wilder	k1gInSc4	Wilder
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
slavný	slavný	k2eAgMnSc1d1	slavný
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zase	zase	k9	zase
nechal	nechat	k5eAaPmAgMnS	nechat
k	k	k7c3	k
Freudovi	Freuda	k1gMnSc3	Freuda
objednat	objednat	k5eAaPmF	objednat
jako	jako	k9	jako
pacient	pacient	k1gMnSc1	pacient
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vymámil	vymámit	k5eAaPmAgInS	vymámit
interview	interview	k1gNnSc4	interview
–	–	k?	–
Freud	Freud	k1gMnSc1	Freud
ho	on	k3xPp3gMnSc4	on
vykázal	vykázat	k5eAaPmAgMnS	vykázat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pracovny	pracovna	k1gFnSc2	pracovna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volné	volný	k2eAgFnSc2d1	volná
asociace	asociace	k1gFnSc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
volných	volný	k2eAgFnPc2d1	volná
asociací	asociace	k1gFnPc2	asociace
je	být	k5eAaImIp3nS	být
jádrem	jádro	k1gNnSc7	jádro
Freudovy	Freudův	k2eAgFnSc2d1	Freudova
terapeutické	terapeutický	k2eAgFnSc2d1	terapeutická
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
nechal	nechat	k5eAaPmAgMnS	nechat
pacienty	pacient	k1gMnPc4	pacient
ulehnout	ulehnout	k5eAaPmF	ulehnout
na	na	k7c4	na
lehátko	lehátko	k1gNnSc4	lehátko
a	a	k8xC	a
přiměl	přimět	k5eAaPmAgMnS	přimět
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bez	bez	k7c2	bez
sebemenší	sebemenší	k2eAgFnSc2d1	sebemenší
kontroly	kontrola	k1gFnSc2	kontrola
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
svědomí	svědomí	k1gNnSc2	svědomí
či	či	k8xC	či
vkusu	vkus	k1gInSc2	vkus
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
základní	základní	k2eAgNnSc1d1	základní
psychoanalytické	psychoanalytický	k2eAgNnSc1d1	psychoanalytické
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
)	)	kIx)	)
říkaly	říkat	k5eAaImAgFnP	říkat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jim	on	k3xPp3gMnPc3	on
právě	právě	k6eAd1	právě
běží	běžet	k5eAaImIp3nS	běžet
hlavou	hlava	k1gFnSc7	hlava
–	–	k?	–
jaké	jaký	k3yIgInPc4	jaký
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
fantazie	fantazie	k1gFnSc1	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
touto	tento	k3xDgFnSc7	tento
technikou	technika	k1gFnSc7	technika
dobrat	dobrat	k5eAaPmF	dobrat
k	k	k7c3	k
nevědomým	vědomý	k2eNgInPc3d1	nevědomý
obsahům	obsah	k1gInPc3	obsah
úplně	úplně	k6eAd1	úplně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
hypnóze	hypnóza	k1gFnSc6	hypnóza
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzdory	navzdory	k7c3	navzdory
přijetí	přijetí	k1gNnSc3	přijetí
základního	základní	k2eAgNnSc2d1	základní
psychoanalytického	psychoanalytický	k2eAgNnSc2d1	psychoanalytické
pravidla	pravidlo	k1gNnSc2	pravidlo
pacienti	pacient	k1gMnPc1	pacient
při	při	k7c6	při
volném	volný	k2eAgNnSc6d1	volné
asociování	asociování	k1gNnSc6	asociování
některé	některý	k3yIgFnSc2	některý
věci	věc	k1gFnSc2	věc
nevysloví	vyslovit	k5eNaPmIp3nS	vyslovit
<g/>
,	,	kIx,	,
v	v	k7c6	v
toku	tok	k1gInSc6	tok
jejich	jejich	k3xOp3gFnSc2	jejich
řeči	řeč	k1gFnSc2	řeč
se	se	k3xPyFc4	se
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
proluky	proluka	k1gFnPc1	proluka
či	či	k8xC	či
úplný	úplný	k2eAgInSc1d1	úplný
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
říkal	říkat	k5eAaImAgMnS	říkat
Freud	Freud	k1gMnSc1	Freud
odpor	odpor	k1gInSc1	odpor
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
psychoanalytika	psychoanalytik	k1gMnSc4	psychoanalytik
hlavním	hlavní	k2eAgNnSc7d1	hlavní
vodítkem	vodítko	k1gNnSc7	vodítko
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ego	ego	k1gNnSc1	ego
(	(	kIx(	(
<g/>
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
Freudově	Freudův	k2eAgFnSc6d1	Freudova
topice	topika	k1gFnSc6	topika
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
přijetí	přijetí	k1gNnSc6	přijetí
čehosi	cosi	k3yInSc2	cosi
nevědomého	vědomý	k2eNgNnSc2d1	nevědomé
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
Freud	Freud	k1gInSc1	Freud
usuzoval	usuzovat	k5eAaImAgInS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
střet	střet	k1gInSc4	střet
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
nevědomým	nevědomé	k1gNnSc7	nevědomé
je	být	k5eAaImIp3nS	být
také	také	k9	také
jádrem	jádro	k1gNnSc7	jádro
pacientovy	pacientův	k2eAgFnSc2d1	pacientova
neurózy	neuróza	k1gFnSc2	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgFnPc4d1	volná
asociace	asociace	k1gFnPc4	asociace
pacienta	pacient	k1gMnSc2	pacient
přimějí	přimět	k5eAaPmIp3nP	přimět
též	též	k9	též
k	k	k7c3	k
regresivní	regresivní	k2eAgFnSc3d1	regresivní
formě	forma	k1gFnSc3	forma
myšlení	myšlení	k1gNnSc2	myšlení
(	(	kIx(	(
<g/>
primárně-procesuálnímu	primárněrocesuální	k2eAgNnSc3d1	primárně-procesuální
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
stav	stav	k1gInSc4	stav
tzv.	tzv.	kA	tzv.
přenosu	přenos	k1gInSc2	přenos
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
pacient	pacient	k1gMnSc1	pacient
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
vztahovat	vztahovat	k5eAaImF	vztahovat
k	k	k7c3	k
analytikovi	analytik	k1gMnSc3	analytik
jako	jako	k8xC	jako
k	k	k7c3	k
nevědomě	vědomě	k6eNd1	vědomě
významným	významný	k2eAgFnPc3d1	významná
figurám	figura	k1gFnPc3	figura
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
minulosti	minulost	k1gFnSc2	minulost
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
matka	matka	k1gFnSc1	matka
či	či	k8xC	či
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
třetího	třetí	k4xOgNnSc2	třetí
až	až	k6eAd1	až
osmého	osmý	k4xOgInSc2	osmý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
dítě	dítě	k1gNnSc4	dítě
dle	dle	k7c2	dle
Freuda	Freud	k1gMnSc2	Freud
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
,	,	kIx,	,
že	že	k8xS	že
otec	otec	k1gMnSc1	otec
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
mají	mít	k5eAaImIp3nP	mít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
cosi	cosi	k3yInSc4	cosi
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dítě	dítě	k1gNnSc1	dítě
rovněž	rovněž	k6eAd1	rovněž
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
a	a	k8xC	a
čehož	což	k3yRnSc2	což
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
odstraněním	odstranění	k1gNnSc7	odstranění
konkurenta	konkurent	k1gMnSc2	konkurent
<g/>
,	,	kIx,	,
rivala	rival	k1gMnSc2	rival
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
rodiče	rodič	k1gMnPc1	rodič
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
cenného	cenný	k2eAgMnSc2d1	cenný
rodiče	rodič	k1gMnSc2	rodič
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
uchovat	uchovat	k5eAaPmF	uchovat
při	při	k7c6	při
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
zažívá	zažívat	k5eAaImIp3nS	zažívat
tedy	tedy	k9	tedy
význačné	význačný	k2eAgInPc4d1	význačný
prožitky	prožitek	k1gInPc4	prožitek
ambivalence	ambivalence	k1gFnSc2	ambivalence
<g/>
,	,	kIx,	,
chlapec	chlapec	k1gMnSc1	chlapec
vůči	vůči	k7c3	vůči
otci	otec	k1gMnSc3	otec
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
vůči	vůči	k7c3	vůči
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ve	v	k7c6	v
zdařilém	zdařilý	k2eAgInSc6d1	zdařilý
případě	případ	k1gInSc6	případ
řeší	řešit	k5eAaImIp3nS	řešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
oidipovské	oidipovský	k2eAgFnSc2d1	oidipovská
volby	volba	k1gFnSc2	volba
na	na	k7c6	na
vědomé	vědomý	k2eAgFnSc6d1	vědomá
rovině	rovina	k1gFnSc6	rovina
vzdá	vzdát	k5eAaPmIp3nS	vzdát
<g/>
,	,	kIx,	,
vražedná	vražedný	k2eAgNnPc4d1	vražedné
přání	přání	k1gNnPc4	přání
vytěsní	vytěsnit	k5eAaPmIp3nS	vytěsnit
a	a	k8xC	a
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
komplex	komplex	k1gInSc4	komplex
kastrační	kastrační	k2eAgInSc1d1	kastrační
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
vývojovým	vývojový	k2eAgInSc7d1	vývojový
momentem	moment	k1gInSc7	moment
dle	dle	k7c2	dle
Freuda	Freud	k1gMnSc2	Freud
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
děti	dítě	k1gFnPc1	dítě
poznají	poznat	k5eAaPmIp3nP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chlapci	chlapec	k1gMnPc1	chlapec
penis	penis	k1gInSc1	penis
mají	mít	k5eAaImIp3nP	mít
a	a	k8xC	a
dívky	dívka	k1gFnPc1	dívka
ho	on	k3xPp3gNnSc4	on
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Chlapci	chlapec	k1gMnPc1	chlapec
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
penis	penis	k1gInSc1	penis
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odebrán	odebrat	k5eAaPmNgInS	odebrat
za	za	k7c4	za
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
a	a	k8xC	a
počnou	počnout	k5eAaPmIp3nP	počnout
se	se	k3xPyFc4	se
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
<g/>
-li	i	k?	-li
masturbovat	masturbovat	k5eAaImF	masturbovat
a	a	k8xC	a
chtít	chtít	k5eAaImF	chtít
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
o	o	k7c4	o
penis	penis	k1gInSc4	penis
přijdou	přijít	k5eAaPmIp3nP	přijít
(	(	kIx(	(
<g/>
kastrační	kastrační	k2eAgInSc4d1	kastrační
strach	strach	k1gInSc4	strach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
počnou	počnout	k5eAaPmIp3nP	počnout
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gInPc3	on
byl	být	k5eAaImAgInS	být
penis	penis	k1gInSc1	penis
odebrán	odebrat	k5eAaPmNgInS	odebrat
za	za	k7c4	za
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dívka	dívka	k1gFnSc1	dívka
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
matka	matka	k1gFnSc1	matka
nemá	mít	k5eNaImIp3nS	mít
penis	penis	k1gInSc4	penis
<g/>
,	,	kIx,	,
počne	počnout	k5eAaPmIp3nS	počnout
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc3	on
méně	málo	k6eAd2	málo
vážit	vážit	k5eAaImF	vážit
<g/>
,	,	kIx,	,
ztrátu	ztráta	k1gFnSc4	ztráta
penisu	penis	k1gInSc6	penis
ji	on	k3xPp3gFnSc4	on
začne	začít	k5eAaPmIp3nS	začít
vyčítat	vyčítat	k5eAaImF	vyčítat
a	a	k8xC	a
přesune	přesunout	k5eAaPmIp3nS	přesunout
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
ji	on	k3xPp3gFnSc4	on
pak	pak	k8xC	pak
má	mít	k5eAaImIp3nS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
za	za	k7c4	za
penis	penis	k1gInSc4	penis
náhradu	náhrada	k1gFnSc4	náhrada
–	–	k?	–
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Libido	libido	k1gNnSc1	libido
<g/>
.	.	kIx.	.
</s>
<s>
Libido	libido	k1gNnSc1	libido
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
někdy	někdy	k6eAd1	někdy
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
pud	pud	k1gInSc4	pud
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
psychoanalytiky	psychoanalytik	k1gMnPc4	psychoanalytik
však	však	k9	však
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
spíše	spíše	k9	spíše
o	o	k7c4	o
"	"	kIx"	"
<g/>
metaforu	metafora	k1gFnSc4	metafora
energie	energie	k1gFnSc2	energie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zdroj	zdroj	k1gInSc4	zdroj
buď	buď	k8xC	buď
v	v	k7c6	v
nevědomí	nevědomí	k1gNnSc6	nevědomí
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
považuje	považovat	k5eAaImIp3nS	považovat
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
libidem	libido	k1gNnSc7	libido
myšlena	myšlen	k2eAgMnSc4d1	myšlen
spíše	spíše	k9	spíše
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
jsou	být	k5eAaImIp3nP	být
obsazeny	obsazen	k2eAgInPc1d1	obsazen
výboje	výboj	k1gInPc1	výboj
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
pudu	pud	k1gInSc2	pud
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
považuje	považovat	k5eAaImIp3nS	považovat
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
,	,	kIx,	,
vyniká	vynikat	k5eAaImIp3nS	vynikat
"	"	kIx"	"
<g/>
metaforická	metaforický	k2eAgFnSc1d1	metaforická
povaha	povaha	k1gFnSc1	povaha
<g/>
"	"	kIx"	"
pojmu	pojem	k1gInSc2	pojem
libido	libido	k1gNnSc1	libido
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
libido	libido	k1gNnSc1	libido
Freud	Freuda	k1gFnPc2	Freuda
převzal	převzít	k5eAaPmAgInS	převzít
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
původně	původně	k6eAd1	původně
značil	značit	k5eAaImAgMnS	značit
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
touhu	touha	k1gFnSc4	touha
či	či	k8xC	či
nutkání	nutkání	k1gNnSc4	nutkání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
akcentuje	akcentovat	k5eAaImIp3nS	akcentovat
pudový	pudový	k2eAgInSc1d1	pudový
charakter	charakter	k1gInSc1	charakter
této	tento	k3xDgFnSc2	tento
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
Freud	Freud	k1gMnSc1	Freud
však	však	k9	však
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
akcentoval	akcentovat	k5eAaImAgInS	akcentovat
psychickou	psychický	k2eAgFnSc4d1	psychická
povahu	povaha	k1gFnSc4	povaha
této	tento	k3xDgFnSc2	tento
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c6	o
libidu	libido	k1gNnSc6	libido
hovořil	hovořit	k5eAaImAgMnS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
palivu	palivo	k1gNnSc6	palivo
psychického	psychický	k2eAgInSc2d1	psychický
systému	systém	k1gInSc2	systém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
představy	představa	k1gFnPc1	představa
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
zakotveny	zakotven	k2eAgFnPc1d1	zakotvena
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Libido	libido	k1gNnSc1	libido
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
prochází	procházet	k5eAaImIp3nS	procházet
různými	různý	k2eAgNnPc7d1	různé
stádii	stádium	k1gNnPc7	stádium
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
zdroje	zdroj	k1gInPc1	zdroj
a	a	k8xC	a
formy	forma	k1gFnPc1	forma
sexuální	sexuální	k2eAgFnPc1d1	sexuální
slasti	slast	k1gFnPc1	slast
<g/>
:	:	kIx,	:
Orální	orální	k2eAgNnSc1d1	orální
období	období	k1gNnSc1	období
<g/>
:	:	kIx,	:
Erotogenní	Erotogenní	k2eAgFnSc7d1	Erotogenní
zónou	zóna	k1gFnSc7	zóna
ústa	ústa	k1gNnPc1	ústa
<g/>
,	,	kIx,	,
libost	libost	k1gFnSc1	libost
získávána	získáván	k2eAgFnSc1d1	získávána
sáním	sání	k1gNnSc7	sání
<g/>
,	,	kIx,	,
kousáním	kousání	k1gNnSc7	kousání
a	a	k8xC	a
žvýkáním	žvýkání	k1gNnSc7	žvýkání
Anální	anální	k2eAgNnPc1d1	anální
období	období	k1gNnPc1	období
<g/>
:	:	kIx,	:
Slast	slast	k1gFnSc1	slast
získávána	získáván	k2eAgFnSc1d1	získávána
zadržováním	zadržování	k1gNnSc7	zadržování
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
vypouštěním	vypouštění	k1gNnSc7	vypouštění
stolice	stolice	k1gFnSc2	stolice
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nácviku	nácvik	k1gInSc3	nácvik
čistoty	čistota	k1gFnSc2	čistota
se	se	k3xPyFc4	se
slast	slast	k1gFnSc1	slast
ze	z	k7c2	z
zadržování	zadržování	k1gNnSc2	zadržování
a	a	k8xC	a
vypouštění	vypouštění	k1gNnSc2	vypouštění
stává	stávat	k5eAaImIp3nS	stávat
socializovanou	socializovaný	k2eAgFnSc4d1	socializovaná
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
hrdosti	hrdost	k1gFnSc2	hrdost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zdrojem	zdroj	k1gInSc7	zdroj
pokoření	pokoření	k1gNnSc2	pokoření
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
vzdoru	vzdor	k1gInSc2	vzdor
nebo	nebo	k8xC	nebo
poslušnosti	poslušnost	k1gFnSc2	poslušnost
Falické	falický	k2eAgNnSc4d1	falické
období	období	k1gNnSc4	období
<g/>
:	:	kIx,	:
Významné	významný	k2eAgFnPc1d1	významná
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
genitálie	genitálie	k1gFnPc1	genitálie
<g/>
,	,	kIx,	,
souběžně	souběžně	k6eAd1	souběžně
probíhá	probíhat	k5eAaImIp3nS	probíhat
oidipovský	oidipovský	k2eAgInSc1d1	oidipovský
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
kastrační	kastrační	k2eAgInSc1d1	kastrační
komplex	komplex	k1gInSc1	komplex
Latence	latence	k1gFnSc2	latence
<g/>
:	:	kIx,	:
Díky	díky	k7c3	díky
vývoji	vývoj	k1gInSc3	vývoj
oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
a	a	k8xC	a
kastračního	kastrační	k2eAgInSc2d1	kastrační
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
identifikaci	identifikace	k1gFnSc4	identifikace
s	s	k7c7	s
rodičem	rodič	k1gMnSc7	rodič
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
vytěsnění	vytěsnění	k1gNnSc2	vytěsnění
agrese	agrese	k1gFnSc2	agrese
a	a	k8xC	a
sexuálních	sexuální	k2eAgNnPc2d1	sexuální
přání	přání	k1gNnPc2	přání
pudy	pud	k1gInPc7	pud
jakoby	jakoby	k8xS	jakoby
kanalizovány	kanalizován	k2eAgFnPc1d1	kanalizována
a	a	k8xC	a
dítě	dítě	k1gNnSc1	dítě
zdánlivě	zdánlivě	k6eAd1	zdánlivě
desexualizováno	desexualizován	k2eAgNnSc1d1	desexualizován
Genitální	genitální	k2eAgNnSc1d1	genitální
období	období	k1gNnSc1	období
<g/>
:	:	kIx,	:
Období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
ustavení	ustavení	k1gNnSc1	ustavení
pevné	pevný	k2eAgFnSc2d1	pevná
identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
identity	identita	k1gFnSc2	identita
sexuální	sexuální	k2eAgFnSc2d1	sexuální
<g/>
.	.	kIx.	.
</s>
<s>
Závěrem	závěr	k1gInSc7	závěr
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
navázání	navázání	k1gNnSc4	navázání
intimního	intimní	k2eAgInSc2d1	intimní
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nová	nový	k2eAgFnSc1d1	nová
objektní	objektní	k2eAgFnSc1d1	objektní
volba	volba	k1gFnSc1	volba
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
původní	původní	k2eAgFnSc2d1	původní
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
pozdější	pozdní	k2eAgInPc4d2	pozdější
libidinosní	libidinosní	k2eAgInPc4d1	libidinosní
pohyby	pohyb	k1gInPc4	pohyb
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
než	než	k8xS	než
ke	k	k7c3	k
genitalitě	genitalita	k1gFnSc3	genitalita
jsou	být	k5eAaImIp3nP	být
projevem	projev	k1gInSc7	projev
fixace	fixace	k1gFnSc2	fixace
či	či	k8xC	či
regrese	regrese	k1gFnSc2	regrese
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
průchod	průchod	k1gInSc4	průchod
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
fází	fáze	k1gFnSc7	fáze
nezdaří	zdařit	k5eNaPmIp3nS	zdařit
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
fixaci	fixace	k1gFnSc3	fixace
(	(	kIx(	(
<g/>
na	na	k7c4	na
fázi	fáze	k1gFnSc4	fáze
libida	libido	k1gNnSc2	libido
<g/>
,	,	kIx,	,
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
komplikovaném	komplikovaný	k2eAgInSc6d1	komplikovaný
průchodu	průchod	k1gInSc6	průchod
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
k	k	k7c3	k
regresi	regrese	k1gFnSc3	regrese
<g/>
.	.	kIx.	.
</s>
<s>
Fixovaná	fixovaný	k2eAgFnSc1d1	fixovaná
nebo	nebo	k8xC	nebo
regredující	regredující	k2eAgFnSc1d1	regredující
osoba	osoba	k1gFnSc1	osoba
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
infantilní	infantilní	k2eAgInPc4d1	infantilní
způsoby	způsob	k1gInPc4	způsob
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
pod	pod	k7c7	pod
vnějším	vnější	k2eAgInSc7d1	vnější
tlakem	tlak	k1gInSc7	tlak
(	(	kIx(	(
<g/>
eventuálně	eventuálně	k6eAd1	eventuálně
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
erotické	erotický	k2eAgInPc4d1	erotický
objekty	objekt	k1gInPc4	objekt
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
s	s	k7c7	s
fixovaným	fixovaný	k2eAgInSc7d1	fixovaný
objektem	objekt	k1gInSc7	objekt
–	–	k?	–
např.	např.	kA	např.
velká	velký	k2eAgFnSc1d1	velká
ňadra	ňadro	k1gNnSc2	ňadro
–	–	k?	–
případně	případně	k6eAd1	případně
lze	lze	k6eAd1	lze
indikovat	indikovat	k5eAaBmF	indikovat
ztrátu	ztráta	k1gFnSc4	ztráta
energie	energie	k1gFnSc2	energie
uvázané	uvázaný	k2eAgFnSc2d1	uvázaná
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
fixace	fixace	k1gFnSc1	fixace
či	či	k8xC	či
regrese	regrese	k1gFnSc1	regrese
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
projevuje	projevovat	k5eAaImIp3nS	projevovat
buď	buď	k8xC	buď
psychickou	psychický	k2eAgFnSc7d1	psychická
poruchou	porucha	k1gFnSc7	porucha
anebo	anebo	k8xC	anebo
sexuální	sexuální	k2eAgFnSc7d1	sexuální
perverzí	perverze	k1gFnSc7	perverze
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1	sexuální
perverze	perverze	k1gFnSc1	perverze
otevřeně	otevřeně	k6eAd1	otevřeně
manifestuje	manifestovat	k5eAaBmIp3nS	manifestovat
původní	původní	k2eAgFnSc4d1	původní
sexuální	sexuální	k2eAgFnSc4d1	sexuální
tematiku	tematika	k1gFnSc4	tematika
<g/>
,	,	kIx,	,
původní	původní	k2eAgInPc4d1	původní
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
také	také	k9	také
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
erogenním	erogenní	k2eAgFnPc3d1	erogenní
zónám	zóna	k1gFnPc3	zóna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
neuróz	neuróza	k1gFnPc2	neuróza
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
poruch	porucha	k1gFnPc2	porucha
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
indikuje	indikovat	k5eAaBmIp3nS	indikovat
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc4	objekt
i	i	k8xC	i
drážděné	drážděný	k2eAgFnSc2d1	drážděná
zóny	zóna	k1gFnSc2	zóna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vyměšováním	vyměšování	k1gNnSc7	vyměšování
<g/>
)	)	kIx)	)
analogicky	analogicky	k6eAd1	analogicky
jako	jako	k8xC	jako
u	u	k7c2	u
perverzí	perverze	k1gFnPc2	perverze
<g/>
.	.	kIx.	.
</s>
<s>
Perverze	perverze	k1gFnPc1	perverze
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Freuda	Freud	k1gMnSc4	Freud
svědky	svědek	k1gMnPc7	svědek
dějin	dějiny	k1gFnPc2	dějiny
libida	libido	k1gNnSc2	libido
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
neurózy	neuróza	k1gFnSc2	neuróza
<g/>
.	.	kIx.	.
</s>
<s>
Perverze	perverze	k1gFnPc1	perverze
jsou	být	k5eAaImIp3nP	být
negativy	negativ	k1gInPc4	negativ
neuróz	neuróza	k1gFnPc2	neuróza
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
neurotik	neurotik	k1gMnSc1	neurotik
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
deviant	deviant	k1gMnSc1	deviant
<g/>
"	"	kIx"	"
přehrává	přehrávat	k5eAaImIp3nS	přehrávat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
úkolem	úkol	k1gInSc7	úkol
perverze	perverze	k1gFnSc2	perverze
je	být	k5eAaImIp3nS	být
však	však	k9	však
bránit	bránit	k5eAaImF	bránit
Ego	ego	k1gNnSc4	ego
před	před	k7c7	před
úzkostí	úzkost	k1gFnSc7	úzkost
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Freuda	Freud	k1gMnSc4	Freud
"	"	kIx"	"
<g/>
polymorfně	polymorfně	k6eAd1	polymorfně
perverzní	perverzní	k2eAgInSc1d1	perverzní
<g/>
"	"	kIx"	"
a	a	k8xC	a
sexuální	sexuální	k2eAgFnPc1d1	sexuální
perverze	perverze	k1gFnPc1	perverze
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
představuje	představovat	k5eAaImIp3nS	představovat
regresi	regrese	k1gFnSc4	regrese
ke	k	k7c3	k
sklonům	sklon	k1gInPc3	sklon
infantilní	infantilní	k2eAgFnSc2d1	infantilní
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
psychika	psychika	k1gFnSc1	psychika
dle	dle	k7c2	dle
Freuda	Freud	k1gMnSc2	Freud
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
vědomí	vědomí	k1gNnSc1	vědomí
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
jedinec	jedinec	k1gMnSc1	jedinec
plně	plně	k6eAd1	plně
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
.	.	kIx.	.
předvědomí	předvědomý	k2eAgMnPc1d1	předvědomý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zapomenuté	zapomenutý	k2eAgNnSc1d1	zapomenuté
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vybavitelné	vybavitelný	k2eAgFnPc4d1	vybavitelná
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
zážitky	zážitek	k1gInPc4	zážitek
<g/>
,	,	kIx,	,
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
atd.	atd.	kA	atd.
Obsahy	obsah	k1gInPc1	obsah
předvědomí	předvědomý	k2eAgMnPc1d1	předvědomý
nečelí	čelit	k5eNaImIp3nP	čelit
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
do	do	k7c2	do
vědomí	vědomí	k1gNnSc2	vědomí
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
nevědomí	nevědomí	k1gNnSc1	nevědomí
je	být	k5eAaImIp3nS	být
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
a	a	k8xC	a
vědomí	vědomí	k1gNnSc6	vědomí
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
část	část	k1gFnSc1	část
psychiky	psychika	k1gFnSc2	psychika
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
změť	změť	k1gFnSc1	změť
neorganizovaných	organizovaný	k2eNgFnPc2d1	neorganizovaná
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
přání	přání	k1gNnPc2	přání
<g/>
,	,	kIx,	,
obav	obava	k1gFnPc2	obava
<g/>
,	,	kIx,	,
zkreslených	zkreslený	k2eAgInPc2d1	zkreslený
obrazů	obraz	k1gInPc2	obraz
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nevědomí	nevědomí	k1gNnSc2	nevědomí
jsou	být	k5eAaImIp3nP	být
vytěsněny	vytěsněn	k2eAgFnPc1d1	vytěsněna
myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
city	cit	k1gInPc1	cit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
pro	pro	k7c4	pro
jedince	jedinec	k1gMnSc2	jedinec
byly	být	k5eAaImAgInP	být
při	při	k7c6	při
uvědomění	uvědomění	k1gNnSc6	uvědomění
příliš	příliš	k6eAd1	příliš
zraňující	zraňující	k2eAgFnSc2d1	zraňující
<g/>
,	,	kIx,	,
ponižující	ponižující	k2eAgFnSc2d1	ponižující
<g/>
,	,	kIx,	,
budily	budit	k5eAaImAgInP	budit
pocity	pocit	k1gInPc4	pocit
úzkosti	úzkost	k1gFnSc2	úzkost
nebo	nebo	k8xC	nebo
viny	vina	k1gFnSc2	vina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
co	co	k9	co
je	být	k5eAaImIp3nS	být
nevědomé	vědomý	k2eNgNnSc1d1	nevědomé
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
stálou	stálý	k2eAgFnSc4d1	stálá
tendenci	tendence	k1gFnSc4	tendence
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
vědomým	vědomý	k2eAgMnSc7d1	vědomý
a	a	k8xC	a
psychika	psychika	k1gFnSc1	psychika
musí	muset	k5eAaImIp3nS	muset
vynakládat	vynakládat	k5eAaImF	vynakládat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
například	například	k6eAd1	například
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
toho	ten	k3xDgMnSc4	ten
moc	moc	k6eAd1	moc
vytěsnili	vytěsnit	k5eAaPmAgMnP	vytěsnit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
cítit	cítit	k5eAaImF	cítit
únavu	únava	k1gFnSc4	únava
bez	bez	k7c2	bez
zjevné	zjevný	k2eAgFnSc2d1	zjevná
příčiny	příčina	k1gFnSc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ego	ego	k1gNnSc2	ego
<g/>
,	,	kIx,	,
superego	superego	k1gMnSc1	superego
a	a	k8xC	a
id	idy	k1gFnPc2	idy
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
tři	tři	k4xCgInPc4	tři
poměrně	poměrně	k6eAd1	poměrně
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
složky	složka	k1gFnSc2	složka
(	(	kIx(	(
<g/>
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
ego	ego	k1gNnSc4	ego
a	a	k8xC	a
superego	superego	k1gNnSc4	superego
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
rozpornými	rozporný	k2eAgInPc7d1	rozporný
principy	princip	k1gInPc7	princip
a	a	k8xC	a
cíli	cíl	k1gInPc7	cíl
<g/>
:	:	kIx,	:
Id	ido	k1gNnPc2	ido
(	(	kIx(	(
<g/>
Ono	onen	k3xDgNnSc1	onen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
pudová	pudový	k2eAgFnSc1d1	pudová
duševní	duševní	k2eAgFnSc1d1	duševní
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
libido	libido	k1gNnSc1	libido
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
ego	ego	k1gNnSc4	ego
i	i	k8xC	i
superego	superego	k1gNnSc4	superego
<g/>
.	.	kIx.	.
</s>
<s>
Id	Ida	k1gFnPc2	Ida
je	být	k5eAaImIp3nS	být
iracionální	iracionální	k2eAgMnSc1d1	iracionální
<g/>
,	,	kIx,	,
nebere	brát	k5eNaImIp3nS	brát
ohled	ohled	k1gInSc1	ohled
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
uspokojení	uspokojení	k1gNnSc4	uspokojení
svých	svůj	k3xOyFgNnPc2	svůj
přání	přání	k1gNnPc2	přání
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
principem	princip	k1gInSc7	princip
slasti	slast	k1gFnSc2	slast
<g/>
.	.	kIx.	.
</s>
<s>
Id	Ida	k1gFnPc2	Ida
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nevědomé	vědomý	k2eNgNnSc1d1	nevědomé
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Freuda	Freud	k1gMnSc2	Freud
představují	představovat	k5eAaImIp3nP	představovat
sny	sen	k1gInPc1	sen
splněná	splněný	k2eAgNnPc4d1	splněné
přání	přání	k1gNnPc4	přání
id	idy	k1gFnPc2	idy
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
bránou	brána	k1gFnSc7	brána
do	do	k7c2	do
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
ryzího	ryzí	k2eAgNnSc2d1	ryzí
id	idy	k1gFnPc2	idy
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
u	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
<g/>
.	.	kIx.	.
</s>
<s>
Ego	ego	k1gNnSc1	ego
(	(	kIx(	(
<g/>
Já	já	k3xPp1nSc1	já
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
principem	princip	k1gInSc7	princip
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
sebezáchovnou	sebezáchovný	k2eAgFnSc4d1	sebezáchovná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
části	část	k1gFnSc2	část
id	idy	k1gFnPc2	idy
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
dítěte	dítě	k1gNnSc2	dítě
vlivem	vlivem	k7c2	vlivem
jeho	jeho	k3xOp3gFnPc2	jeho
interakcí	interakce	k1gFnPc2	interakce
(	(	kIx(	(
<g/>
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
<g/>
)	)	kIx)	)
s	s	k7c7	s
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
racionální	racionální	k2eAgNnSc1d1	racionální
<g/>
,	,	kIx,	,
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
činy	čin	k1gInPc4	čin
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Přání	přání	k1gNnSc1	přání
id	idy	k1gFnPc2	idy
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
jim	on	k3xPp3gMnPc3	on
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
uspokojuje	uspokojovat	k5eAaImIp3nS	uspokojovat
je	on	k3xPp3gMnPc4	on
ve	v	k7c4	v
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
chvíli	chvíle	k1gFnSc4	chvíle
a	a	k8xC	a
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Ego	ego	k1gNnSc1	ego
je	být	k5eAaImIp3nS	být
vědomé	vědomý	k2eAgNnSc1d1	vědomé
<g/>
,	,	kIx,	,
předvědomé	předvědomý	k2eAgNnSc1d1	předvědomé
i	i	k8xC	i
nevědomé	vědomý	k2eNgNnSc1d1	nevědomé
<g/>
.	.	kIx.	.
</s>
<s>
Nevědomá	vědomý	k2eNgFnSc1d1	nevědomá
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
obranných	obranný	k2eAgInPc2d1	obranný
mechanismů	mechanismus	k1gInPc2	mechanismus
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
vypořádává	vypořádávat	k5eAaImIp3nS	vypořádávat
jak	jak	k8xS	jak
s	s	k7c7	s
nepřijatelnými	přijatelný	k2eNgNnPc7d1	nepřijatelné
přáními	přání	k1gNnPc7	přání
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
extrémními	extrémní	k2eAgInPc7d1	extrémní
zákazy	zákaz	k1gInPc7	zákaz
superega	supereg	k1gMnSc2	supereg
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc1d1	silné
ego	ego	k1gNnSc1	ego
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
zdravě	zdravě	k6eAd1	zdravě
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Superego	Superego	k1gNnSc1	Superego
(	(	kIx(	(
<g/>
Nadjá	Nadjá	k1gFnSc1	Nadjá
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
socializace	socializace	k1gFnSc2	socializace
z	z	k7c2	z
konfliktů	konflikt	k1gInPc2	konflikt
dítě	dítě	k1gNnSc1	dítě
versus	versus	k7c1	versus
autorita	autorita	k1gFnSc1	autorita
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ventil	ventil	k1gInSc4	ventil
vlastním	vlastní	k2eAgInPc3d1	vlastní
agresivním	agresivní	k2eAgInPc3d1	agresivní
impulsům	impuls	k1gInPc3	impuls
jedince	jedinko	k6eAd1	jedinko
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
projevenou	projevený	k2eAgFnSc4d1	projevená
agresivitu	agresivita	k1gFnSc4	agresivita
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vědomě	vědomě	k6eAd1	vědomě
či	či	k8xC	či
nevědomě	vědomě	k6eNd1	vědomě
internalizované	internalizovaný	k2eAgNnSc1d1	internalizovaný
(	(	kIx(	(
<g/>
zvnitřněné	zvnitřněný	k2eAgNnSc1d1	zvnitřněné
<g/>
)	)	kIx)	)
omezení	omezení	k1gNnSc1	omezení
<g/>
,	,	kIx,	,
zákazy	zákaz	k1gInPc1	zákaz
a	a	k8xC	a
příkazy	příkaz	k1gInPc1	příkaz
ukládané	ukládaný	k2eAgInPc1d1	ukládaný
dítěti	dítě	k1gNnSc3	dítě
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc1	dítě
významnými	významný	k2eAgFnPc7d1	významná
autoritami	autorita	k1gFnPc7	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
nevědomého	vědomý	k2eNgInSc2d1	nevědomý
pocitu	pocit	k1gInSc2	pocit
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
tísně	tíseň	k1gFnSc2	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
lze	lze	k6eAd1	lze
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
(	(	kIx(	(
<g/>
jsi	být	k5eAaImIp2nS	být
hříšník	hříšník	k1gMnSc1	hříšník
<g/>
)	)	kIx)	)
i	i	k9	i
řešení	řešení	k1gNnSc2	řešení
(	(	kIx(	(
<g/>
věříš	věřit	k5eAaImIp2nS	věřit
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
budeš	být	k5eAaImBp2nS	být
spasen	spasit	k5eAaPmNgInS	spasit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Superego	Superego	k6eAd1	Superego
je	být	k5eAaImIp3nS	být
moralizující	moralizující	k2eAgFnSc7d1	moralizující
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
se	s	k7c7	s
"	"	kIx"	"
<g/>
principem	princip	k1gInSc7	princip
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
je	být	k5eAaImIp3nS	být
svědomí	svědomí	k1gNnSc1	svědomí
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
částí	část	k1gFnPc2	část
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgNnSc4d1	ideální
ego	ego	k1gNnSc4	ego
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
dokonalost	dokonalost	k1gFnSc4	dokonalost
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
byly	být	k5eAaImAgFnP	být
činnosti	činnost	k1gFnPc4	činnost
a	a	k8xC	a
projevy	projev	k1gInPc4	projev
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
rodiče	rodič	k1gMnPc1	rodič
chválili	chválit	k5eAaImAgMnP	chválit
<g/>
.	.	kIx.	.
</s>
<s>
Superego	Superego	k6eAd1	Superego
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
současnými	současný	k2eAgFnPc7d1	současná
hodnotami	hodnota	k1gFnPc7	hodnota
jedince	jedinko	k6eAd1	jedinko
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
superego	superego	k1gNnSc4	superego
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
perfekcionistů	perfekcionista	k1gMnPc2	perfekcionista
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
rozvinuté	rozvinutý	k2eAgFnPc1d1	rozvinutá
u	u	k7c2	u
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychoanalytické	psychoanalytický	k2eAgInPc4d1	psychoanalytický
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
Freudovu	Freudův	k2eAgFnSc4d1	Freudova
následovnici	následovnice	k1gFnSc4	následovnice
bývá	bývat	k5eAaImIp3nS	bývat
považována	považován	k2eAgFnSc1d1	považována
Melanie	Melanie	k1gFnSc1	Melanie
Kleinová	Kleinová	k1gFnSc1	Kleinová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obrátila	obrátit	k5eAaPmAgFnS	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
problému	problém	k1gInSc3	problém
psychózy	psychóza	k1gFnSc2	psychóza
a	a	k8xC	a
nejranějšího	raný	k2eAgInSc2d3	nejranější
vývoje	vývoj	k1gInSc2	vývoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
<g/>
,	,	kIx,	,
svedla	svést	k5eAaPmAgFnS	svést
mnoho	mnoho	k4c4	mnoho
teoretických	teoretický	k2eAgFnPc2d1	teoretická
půtek	půtka	k1gFnPc2	půtka
s	s	k7c7	s
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
zejména	zejména	k9	zejména
Freudova	Freudův	k2eAgFnSc1d1	Freudova
dcera	dcera	k1gFnSc1	dcera
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
Heinz	Heinz	k1gMnSc1	Heinz
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
Mahlerová	Mahlerový	k2eAgFnSc1d1	Mahlerový
či	či	k8xC	či
Edith	Edith	k1gInSc4	Edith
Jacobsonová	Jacobsonový	k2eAgFnSc1d1	Jacobsonový
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
strukturálního	strukturální	k2eAgInSc2d1	strukturální
modelu	model	k1gInSc2	model
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
obran	obrana	k1gFnPc2	obrana
Ega	ego	k1gNnSc2	ego
<g/>
.	.	kIx.	.
</s>
<s>
Hartmann	Hartmann	k1gMnSc1	Hartmann
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
teorii	teorie	k1gFnSc4	teorie
tzv.	tzv.	kA	tzv.
autonomních	autonomní	k2eAgFnPc2d1	autonomní
funkcí	funkce	k1gFnPc2	funkce
Ega	ego	k1gNnSc2	ego
<g/>
,	,	kIx,	,
Mahlerová	Mahlerová	k1gFnSc1	Mahlerová
a	a	k8xC	a
Jacobsonová	Jacobsonová	k1gFnSc1	Jacobsonová
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
model	model	k1gInSc4	model
nejranějšího	raný	k2eAgInSc2d3	nejranější
vývoje	vývoj	k1gInSc2	vývoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
terminologií	terminologie	k1gFnSc7	terminologie
popisoval	popisovat	k5eAaImAgMnS	popisovat
mnohé	mnohé	k1gNnSc4	mnohé
z	z	k7c2	z
kleiniánských	kleiniánský	k2eAgFnPc2d1	kleiniánský
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
škole	škola	k1gFnSc3	škola
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
egopsychologům	egopsycholog	k1gMnPc3	egopsycholog
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
Erik	Erik	k1gMnSc1	Erik
Erikson	Erikson	k1gMnSc1	Erikson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
zejména	zejména	k9	zejména
vztahem	vztah	k1gInSc7	vztah
psychického	psychický	k2eAgInSc2d1	psychický
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
Dětství	dětství	k1gNnSc1	dětství
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kleiniánskou	kleiniánský	k2eAgFnSc4d1	kleiniánský
a	a	k8xC	a
egopsychologickou	egopsychologický	k2eAgFnSc4d1	egopsychologický
skupinu	skupina	k1gFnSc4	skupina
se	se	k3xPyFc4	se
vklínila	vklínit	k5eAaPmAgFnS	vklínit
tzv.	tzv.	kA	tzv.
skupina	skupina	k1gFnSc1	skupina
středu	střed	k1gInSc2	střed
tvořená	tvořený	k2eAgFnSc1d1	tvořená
rodilými	rodilý	k2eAgMnPc7d1	rodilý
britskými	britský	k2eAgMnPc7d1	britský
analytiky	analytik	k1gMnPc7	analytik
(	(	kIx(	(
<g/>
Donald	Donald	k1gMnSc1	Donald
Woods	Woodsa	k1gFnPc2	Woodsa
Winnicott	Winnicott	k1gMnSc1	Winnicott
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
Fairbairn	Fairbairn	k1gMnSc1	Fairbairn
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc1	Harra
Guntrip	Guntrip	k1gInSc1	Guntrip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
středu	střed	k1gInSc2	střed
vzešly	vzejít	k5eAaPmAgFnP	vzejít
i	i	k9	i
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
více	hodně	k6eAd2	hodně
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Bowlby	Bowlba	k1gFnSc2	Bowlba
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vlivnou	vlivný	k2eAgFnSc7d1	vlivná
teorií	teorie	k1gFnSc7	teorie
přimknutí	přimknutí	k1gNnSc4	přimknutí
(	(	kIx(	(
<g/>
attachment	attachment	k1gInSc1	attachment
<g/>
)	)	kIx)	)
či	či	k8xC	či
Michael	Michael	k1gMnSc1	Michael
Balint	Balint	k1gMnSc1	Balint
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
specificky	specificky	k6eAd1	specificky
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
francouzská	francouzský	k2eAgFnSc1d1	francouzská
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
centrální	centrální	k2eAgFnSc7d1	centrální
osobností	osobnost	k1gFnSc7	osobnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jacques	Jacques	k1gMnSc1	Jacques
Lacan	Lacan	k1gMnSc1	Lacan
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
reagovaly	reagovat	k5eAaBmAgFnP	reagovat
osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k8xS	jako
Didier	Didier	k1gInSc1	Didier
Anzieu	Anzieus	k1gInSc2	Anzieus
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Laplanche	Laplanch	k1gFnSc2	Laplanch
či	či	k8xC	či
André	André	k1gMnSc4	André
Green	Green	k1gInSc4	Green
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
škola	škola	k1gFnSc1	škola
tzv.	tzv.	kA	tzv.
kulturní	kulturní	k2eAgFnSc2d1	kulturní
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
(	(	kIx(	(
<g/>
Karen	Karen	k1gInSc1	Karen
Horneyová	Horneyová	k1gFnSc1	Horneyová
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
<g/>
,	,	kIx,	,
Harry	Harr	k1gMnPc4	Harr
Stack	Stack	k1gMnSc1	Stack
Sullivan	Sullivan	k1gMnSc1	Sullivan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
freudismu	freudismus	k1gInSc2	freudismus
s	s	k7c7	s
marxismem	marxismus	k1gInSc7	marxismus
usilovali	usilovat	k5eAaImAgMnP	usilovat
Herbert	Herbert	k1gInSc4	Herbert
Marcuse	Marcuse	k1gFnSc2	Marcuse
či	či	k8xC	či
Louis	Louis	k1gMnSc1	Louis
Althusser	Althusser	k1gMnSc1	Althusser
<g/>
.	.	kIx.	.
</s>
<s>
Interpersonální	interpersonální	k2eAgFnSc4d1	interpersonální
školu	škola	k1gFnSc4	škola
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
zejména	zejména	k9	zejména
Stephen	Stephen	k2eAgInSc1d1	Stephen
Mitchell	Mitchell	k1gInSc1	Mitchell
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
selfpsychologii	selfpsychologie	k1gFnSc4	selfpsychologie
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
Heinz	Heinz	k1gMnSc1	Heinz
Kohut	Kohut	k1gMnSc1	Kohut
<g/>
.	.	kIx.	.
</s>
<s>
Géza	géz	k1gMnSc4	géz
Róheim	Róheim	k1gInSc1	Róheim
využil	využít	k5eAaPmAgInS	využít
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
antropologická	antropologický	k2eAgNnPc4d1	antropologické
zkoumání	zkoumání	k1gNnPc4	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
vychází	vycházet	k5eAaImIp3nS	vycházet
také	také	k9	také
analytická	analytický	k2eAgFnSc1d1	analytická
teorie	teorie	k1gFnSc1	teorie
osobnosti	osobnost	k1gFnSc2	osobnost
Carla	Carl	k1gMnSc2	Carl
Gustava	Gustav	k1gMnSc2	Gustav
Junga	Jung	k1gMnSc2	Jung
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
čelil	čelit	k5eAaImAgMnS	čelit
vždy	vždy	k6eAd1	vždy
velkému	velký	k2eAgInSc3d1	velký
přívalu	příval	k1gInSc3	příval
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
on	on	k3xPp3gMnSc1	on
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
zákonitý	zákonitý	k2eAgInSc4d1	zákonitý
projev	projev	k1gInSc4	projev
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
nevědomému	nevědomé	k1gNnSc3	nevědomé
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvlivnějším	vlivný	k2eAgMnPc3d3	nejvlivnější
kritikům	kritik	k1gMnPc3	kritik
freudismu	freudismus	k1gInSc2	freudismus
patří	patřit	k5eAaImIp3nS	patřit
Karl	Karl	k1gMnSc1	Karl
Popper	Popper	k1gMnSc1	Popper
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
freudismus	freudismus	k1gInSc4	freudismus
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pseudovědu	pseudověda	k1gFnSc4	pseudověda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInPc1	jeho
postuláty	postulát	k1gInPc1	postulát
nejsou	být	k5eNaImIp3nP	být
falsifikovatelné	falsifikovatelný	k2eAgInPc1d1	falsifikovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
kritika	kritika	k1gFnSc1	kritika
zaznívá	zaznívat	k5eAaImIp3nS	zaznívat
také	také	k9	také
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
biologicky	biologicky	k6eAd1	biologicky
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
pozic	pozice	k1gFnPc2	pozice
behaviorismu	behaviorismus	k1gInSc2	behaviorismus
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
zejména	zejména	k9	zejména
Hans	Hans	k1gMnSc1	Hans
Eysenck	Eysenck	k1gMnSc1	Eysenck
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgNnSc2	jenž
"	"	kIx"	"
<g/>
Freud	Freud	k1gMnSc1	Freud
vrátil	vrátit	k5eAaPmAgMnS	vrátit
psychologii	psychologie	k1gFnSc4	psychologie
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
zpátky	zpátky	k6eAd1	zpátky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
je	být	k5eAaImIp3nS	být
kritika	kritika	k1gFnSc1	kritika
feministická	feministický	k2eAgFnSc1d1	feministická
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
slavné	slavný	k2eAgFnSc6d1	slavná
knize	kniha	k1gFnSc6	kniha
Druhé	druhý	k4xOgNnSc4	druhý
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
levicovými	levicový	k2eAgMnPc7d1	levicový
intelektuály	intelektuál	k1gMnPc7	intelektuál
vlivná	vlivný	k2eAgFnSc1d1	vlivná
též	též	k9	též
dekonstruktivistická	dekonstruktivistický	k2eAgFnSc1d1	dekonstruktivistická
kritika	kritika	k1gFnSc1	kritika
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Gillese	Gillese	k1gFnSc2	Gillese
Deleuzeho	Deleuze	k1gMnSc2	Deleuze
a	a	k8xC	a
Felixe	Felix	k1gMnSc2	Felix
Guattariho	Guattari	k1gMnSc2	Guattari
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozic	pozice	k1gFnPc2	pozice
strukturalismu	strukturalismus	k1gInSc2	strukturalismus
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Freudovy	Freudův	k2eAgInPc4d1	Freudův
názory	názor	k1gInPc4	názor
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
antropologické	antropologický	k2eAgFnPc4d1	antropologická
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
původu	původ	k1gInSc6	původ
incestu	incest	k1gInSc2	incest
<g/>
)	)	kIx)	)
Claude	Claud	k1gInSc5	Claud
Lévi-Strauss	Lévi-Strauss	k1gInSc4	Lévi-Strauss
<g/>
.	.	kIx.	.
</s>
<s>
Marxisticko-pavlovovskou	marxistickoavlovovský	k2eAgFnSc4d1	marxisticko-pavlovovský
kritiku	kritika	k1gFnSc4	kritika
Freuda	Freud	k1gMnSc2	Freud
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
například	například	k6eAd1	například
americký	americký	k2eAgMnSc1d1	americký
marxista	marxista	k1gMnSc1	marxista
Harry	Harra	k1gFnSc2	Harra
Kohlsaat	Kohlsaat	k1gInSc1	Kohlsaat
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kniha	kniha	k1gFnSc1	kniha
Pavlov	Pavlovo	k1gNnPc2	Pavlovo
a	a	k8xC	a
Freud	Freuda	k1gFnPc2	Freuda
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
i	i	k8xC	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
řadu	řada	k1gFnSc4	řada
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
k	k	k7c3	k
freudovské	freudovský	k2eAgFnSc3d1	freudovská
inspiraci	inspirace	k1gFnSc3	inspirace
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
třeba	třeba	k9	třeba
Luis	Luisa	k1gFnPc2	Luisa
Buñ	Buñ	k1gMnSc1	Buñ
či	či	k8xC	či
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
typicky	typicky	k6eAd1	typicky
freudovský	freudovský	k2eAgMnSc1d1	freudovský
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Hitchcockův	Hitchcockův	k2eAgInSc1d1	Hitchcockův
snímek	snímek	k1gInSc1	snímek
Vertigo	Vertigo	k6eAd1	Vertigo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
filozof	filozof	k1gMnSc1	filozof
Slavoj	Slavoj	k1gMnSc1	Slavoj
Žižek	Žižek	k1gMnSc1	Žižek
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
dům	dům	k1gInSc4	dům
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
v	v	k7c4	v
Psychu	psycha	k1gFnSc4	psycha
je	být	k5eAaImIp3nS	být
strukturován	strukturován	k2eAgInSc1d1	strukturován
dle	dle	k7c2	dle
freudovského	freudovský	k2eAgInSc2d1	freudovský
modelu	model	k1gInSc2	model
<g/>
:	:	kIx,	:
sklep	sklep	k1gInSc1	sklep
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
Id	ido	k1gNnPc2	ido
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
patro	patro	k1gNnSc4	patro
Ego	ego	k1gNnSc2	ego
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
Supergo	Supergo	k6eAd1	Supergo
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
objevil	objevit	k5eAaPmAgMnS	objevit
mnohokrát	mnohokrát	k6eAd1	mnohokrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životopisném	životopisný	k2eAgInSc6d1	životopisný
filmu	film	k1gInSc6	film
Freud	Freud	k1gInSc1	Freud
<g/>
:	:	kIx,	:
Tajná	tajný	k2eAgFnSc1d1	tajná
vášeň	vášeň	k1gFnSc1	vášeň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
představoval	představovat	k5eAaImAgMnS	představovat
zakladatele	zakladatel	k1gMnPc4	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Montgomery	Montgomera	k1gFnSc2	Montgomera
Clift	Clifta	k1gFnPc2	Clifta
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gMnSc1	Freud
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
popkulturní	popkulturní	k2eAgFnSc7d1	popkulturní
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
kriminální	kriminální	k2eAgFnSc1d1	kriminální
komedie	komedie	k1gFnSc1	komedie
The	The	k1gMnSc1	The
Seven-Per-Cent	Seven-Per-Cent	k1gMnSc1	Seven-Per-Cent
Solution	Solution	k1gInSc4	Solution
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
slavný	slavný	k2eAgMnSc1d1	slavný
detektiv	detektiv	k1gMnSc1	detektiv
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
u	u	k7c2	u
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
vyléčit	vyléčit	k5eAaPmF	vyléčit
ze	z	k7c2	z
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
kokainu	kokain	k1gInSc6	kokain
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
Conana	Conan	k1gMnSc2	Conan
Doyla	Doyl	k1gMnSc2	Doyl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
Oscary	Oscar	k1gInPc4	Oscar
a	a	k8xC	a
Freuda	Freud	k1gMnSc4	Freud
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Alan	Alan	k1gMnSc1	Alan
Arkin	Arkin	k1gMnSc1	Arkin
<g/>
.	.	kIx.	.
</s>
<s>
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
a	a	k8xC	a
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
se	se	k3xPyFc4	se
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
potkali	potkat	k5eAaPmAgMnP	potkat
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
a	a	k8xC	a
pekelný	pekelný	k2eAgInSc1d1	pekelný
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Sherlock	Sherlock	k1gMnSc1	Sherlock
Holmes	Holmes	k1gMnSc1	Holmes
and	and	k?	and
the	the	k?	the
Leading	Leading	k1gInSc1	Leading
Lady	lady	k1gFnSc1	lady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Freud	Freud	k1gInSc4	Freud
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
hypnotizuje	hypnotizovat	k5eAaBmIp3nS	hypnotizovat
divu	div	k1gInSc2	div
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
operety	opereta	k1gFnSc2	opereta
Irene	Iren	k1gInSc5	Iren
Adlerovou	Adlerův	k2eAgFnSc7d1	Adlerova
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
společně	společně	k6eAd1	společně
s	s	k7c7	s
Holmesem	Holmes	k1gInSc7	Holmes
zabránil	zabránit	k5eAaPmAgInS	zabránit
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
Roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
komedie	komedie	k1gFnSc1	komedie
Tajný	tajný	k1gMnSc1	tajný
deník	deník	k1gInSc1	deník
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Secret	Secret	k1gMnSc1	Secret
Diary	Diara	k1gMnSc2	Diara
of	of	k?	of
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Freuda	Freuda	k1gMnSc1	Freuda
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgMnS	hrát
Bud	bouda	k1gFnPc2	bouda
Cort	Cort	k2eAgMnSc1d1	Cort
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
natočila	natočit	k5eAaBmAgFnS	natočit
BBC	BBC	kA	BBC
životopisný	životopisný	k2eAgInSc1d1	životopisný
seriál	seriál	k1gInSc1	seriál
Freud	Freud	k1gInSc1	Freud
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Suchetem	Suchet	k1gMnSc7	Suchet
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k8xS	jako
Hercule	Hercule	k1gFnSc1	Hercule
Poirot	Poirota	k1gFnPc2	Poirota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
natočila	natočit	k5eAaBmAgFnS	natočit
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Bludiště	bludiště	k1gNnSc2	bludiště
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
případu	případ	k1gInSc6	případ
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
František	František	k1gMnSc1	František
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc3	jeho
pacientku	pacientka	k1gFnSc4	pacientka
zápolící	zápolící	k2eAgFnSc4d1	zápolící
s	s	k7c7	s
tajnou	tajný	k2eAgFnSc7d1	tajná
masturbací	masturbace	k1gFnSc7	masturbace
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
despotickým	despotický	k2eAgMnSc7d1	despotický
otcem	otec	k1gMnSc7	otec
(	(	kIx(	(
<g/>
Luděk	Luděk	k1gMnSc1	Luděk
Munzar	Munzar	k1gMnSc1	Munzar
<g/>
)	)	kIx)	)
hrála	hrát	k5eAaImAgFnS	hrát
Eva	Eva	k1gFnSc1	Eva
Vejmělková	Vejmělková	k1gFnSc1	Vejmělková
<g/>
.	.	kIx.	.
</s>
<s>
Režii	režie	k1gFnSc4	režie
měl	mít	k5eAaImAgMnS	mít
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
PBS	PBS	kA	PBS
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
drama	drama	k1gFnSc1	drama
The	The	k1gFnSc2	The
Question	Question	k1gInSc1	Question
of	of	k?	of
God	God	k1gFnSc1	God
<g/>
:	:	kIx,	:
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
&	&	k?	&
C.	C.	kA	C.
<g/>
S.	S.	kA	S.
Lewis	Lewis	k1gFnPc2	Lewis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Freuda	Freuda	k1gFnSc1	Freuda
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
hrál	hrát	k5eAaImAgInS	hrát
Declan	Declan	k1gInSc1	Declan
Conlon	Conlon	k1gInSc1	Conlon
a	a	k8xC	a
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
Peter	Petra	k1gFnPc2	Petra
Eyre	Eyre	k1gNnPc2	Eyre
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
hráli	hrát	k5eAaImAgMnP	hrát
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
Dáša	Dáša	k1gFnSc1	Dáša
Bláhová	Bláhová	k1gFnSc1	Bláhová
představovala	představovat	k5eAaImAgFnS	představovat
Freudovu	Freudův	k2eAgFnSc4d1	Freudova
matku	matka	k1gFnSc4	matka
Amalii	Amalie	k1gFnSc4	Amalie
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Benedikt	benedikt	k1gInSc4	benedikt
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
Jacoba	Jacoba	k1gFnSc1	Jacoba
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Bartoš	Bartoš	k1gMnSc1	Bartoš
Jean-Martina	Jean-Martin	k1gMnSc4	Jean-Martin
Charcota	Charcot	k1gMnSc4	Charcot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
natočil	natočit	k5eAaBmAgMnS	natočit
kanadský	kanadský	k2eAgMnSc1d1	kanadský
režisér	režisér	k1gMnSc1	režisér
David	David	k1gMnSc1	David
Cronenberg	Cronenberg	k1gMnSc1	Cronenberg
film	film	k1gInSc4	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
Dangerous	Dangerous	k1gInSc1	Dangerous
Method	Method	k1gInSc1	Method
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
Junga	Jung	k1gMnSc2	Jung
a	a	k8xC	a
Sabiny	Sabina	k1gMnSc2	Sabina
Spielreinové	Spielreinová	k1gFnSc2	Spielreinová
<g/>
.	.	kIx.	.
</s>
<s>
Freuda	Freuda	k1gMnSc1	Freuda
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Viggo	Viggo	k6eAd1	Viggo
Mortensen	Mortensen	k2eAgMnSc1d1	Mortensen
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
je	být	k5eAaImIp3nS	být
předobrazem	předobraz	k1gInSc7	předobraz
lékaře	lékař	k1gMnSc2	lékař
z	z	k7c2	z
kreslených	kreslený	k2eAgInPc2d1	kreslený
vtipů	vtip	k1gInPc2	vtip
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Jiránka	Jiránek	k1gMnSc2	Jiránek
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
o	o	k7c4	o
hysterii	hysterie	k1gFnSc4	hysterie
–	–	k?	–
vytěsnění	vytěsnění	k1gNnSc2	vytěsnění
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
–	–	k?	–
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
,	,	kIx,	,
předvědomí	předvědomý	k2eAgMnPc1d1	předvědomý
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
Psychopatologie	psychopatologie	k1gFnSc2	psychopatologie
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
–	–	k?	–
freudovské	freudovský	k2eAgNnSc1d1	freudovské
přeřeknutí	přeřeknutí	k1gNnSc1	přeřeknutí
<g/>
,	,	kIx,	,
předeterminovanost	předeterminovanost	k1gFnSc1	předeterminovanost
Vtip	vtip	k1gInSc1	vtip
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
nevědomí	nevědomí	k1gNnSc6	nevědomí
Tři	tři	k4xCgFnPc1	tři
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
sexuální	sexuální	k2eAgFnSc6d1	sexuální
teorii	teorie	k1gFnSc6	teorie
–	–	k?	–
libido	libido	k1gNnSc1	libido
<g/>
,	,	kIx,	,
oralita	oralita	k1gFnSc1	oralita
<g/>
,	,	kIx,	,
analita	analita	k1gFnSc1	analita
<g/>
,	,	kIx,	,
perverze	perverze	k1gFnSc1	perverze
<g />
.	.	kIx.	.
</s>
<s>
Zlomek	zlomek	k1gInSc1	zlomek
analysy	analysa	k1gFnSc2	analysa
případu	případ	k1gInSc2	případ
hysterie	hysterie	k1gFnSc2	hysterie
–	–	k?	–
chorobopis	chorobopis	k1gInSc1	chorobopis
Dory	Dora	k1gFnSc2	Dora
Analýza	analýza	k1gFnSc1	analýza
fobie	fobie	k1gFnSc1	fobie
pětiletého	pětiletý	k2eAgMnSc2d1	pětiletý
chlapce	chlapec	k1gMnSc2	chlapec
–	–	k?	–
chorobopis	chorobopis	k1gInSc1	chorobopis
Malého	Malý	k1gMnSc2	Malý
Hanse	Hans	k1gMnSc2	Hans
Poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
případu	případ	k1gInSc3	případ
nutkavé	nutkavý	k2eAgFnSc2d1	nutkavá
neurózy	neuróza	k1gFnSc2	neuróza
–	–	k?	–
chorobopis	chorobopis	k1gInSc1	chorobopis
Krysího	krysí	k2eAgMnSc2d1	krysí
muže	muž	k1gMnSc2	muž
Psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
autobiograficky	autobiograficky	k6eAd1	autobiograficky
popsanému	popsaný	k2eAgInSc3d1	popsaný
případu	případ	k1gInSc3	případ
paranoie	paranoia	k1gFnSc2	paranoia
(	(	kIx(	(
<g/>
dementia	dementia	k1gFnSc1	dementia
paranoides	paranoides	k1gMnSc1	paranoides
<g/>
)	)	kIx)	)
–	–	k?	–
chorobopis	chorobopis	k1gInSc1	chorobopis
prezidenta	prezident	k1gMnSc2	prezident
Schrebera	Schreber	k1gMnSc2	Schreber
Totem	totem	k1gInSc1	totem
a	a	k8xC	a
tabu	tabu	k1gNnSc1	tabu
–	–	k?	–
vražda	vražda	k1gFnSc1	vražda
prvotního	prvotní	k2eAgMnSc2d1	prvotní
otce	otec	k1gMnSc2	otec
K	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
<g />
.	.	kIx.	.
</s>
<s>
narcismu	narcismus	k1gInSc2	narcismus
–	–	k?	–
narcismus	narcismus	k1gInSc1	narcismus
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
případu	případ	k1gInSc2	případ
dětské	dětský	k2eAgFnSc2d1	dětská
neurózy	neuróza	k1gFnSc2	neuróza
–	–	k?	–
chorobopis	chorobopis	k1gInSc1	chorobopis
Vlčího	vlčí	k2eAgMnSc2d1	vlčí
muže	muž	k1gMnSc2	muž
Mimo	mimo	k7c4	mimo
princip	princip	k1gInSc4	princip
slasti	slast	k1gFnSc2	slast
–	–	k?	–
pud	pud	k1gInSc1	pud
života	život	k1gInSc2	život
a	a	k8xC	a
pud	pud	k1gInSc1	pud
smrti	smrt	k1gFnSc2	smrt
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
Ono	onen	k3xDgNnSc1	onen
–	–	k?	–
Ego	ego	k1gNnPc2	ego
<g/>
,	,	kIx,	,
Id	Ida	k1gFnPc2	Ida
<g/>
,	,	kIx,	,
Superego	Superego	k6eAd1	Superego
Svazek	svazek	k1gInSc1	svazek
I.	I.	kA	I.
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Jeden	jeden	k4xCgInSc1	jeden
případ	případ	k1gInSc1	případ
hypnotického	hypnotický	k2eAgInSc2d1	hypnotický
<g />
.	.	kIx.	.
</s>
<s>
uzdravení	uzdravení	k1gNnSc1	uzdravení
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
hysterických	hysterický	k2eAgInPc2d1	hysterický
symptomů	symptom	k1gInPc2	symptom
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
"	"	kIx"	"
<g/>
protivůle	protivůle	k1gFnSc1	protivůle
<g/>
"	"	kIx"	"
Charcot	Charcot	k1gInSc1	Charcot
Několik	několik	k4yIc1	několik
úvah	úvaha	k1gFnPc2	úvaha
ke	k	k7c3	k
srovnávacímu	srovnávací	k2eAgNnSc3d1	srovnávací
studiu	studio	k1gNnSc3	studio
organických	organický	k2eAgFnPc2d1	organická
a	a	k8xC	a
hysterických	hysterický	k2eAgNnPc2d1	hysterické
motorických	motorický	k2eAgNnPc2d1	motorické
ochrnutí	ochrnutí	k1gNnPc2	ochrnutí
Obranné	obranný	k2eAgFnSc2d1	obranná
neuropsychózy	neuropsychóza	k1gFnSc2	neuropsychóza
O	o	k7c6	o
psychickém	psychický	k2eAgInSc6d1	psychický
mechanismu	mechanismus	k1gInSc6	mechanismus
hysterických	hysterický	k2eAgInPc2d1	hysterický
jevů	jev	k1gInPc2	jev
Chorobopisy	chorobopis	k1gInPc4	chorobopis
K	k	k7c3	k
psychoterapii	psychoterapie	k1gFnSc3	psychoterapie
hysterie	hysterie	k1gFnSc2	hysterie
O	o	k7c6	o
oprávněnosti	oprávněnost	k1gFnSc6	oprávněnost
oddělení	oddělení	k1gNnSc2	oddělení
určitého	určitý	k2eAgInSc2d1	určitý
komplexu	komplex	k1gInSc2	komplex
symptomů	symptom	k1gInPc2	symptom
od	od	k7c2	od
neurastenie	neurastenie	k1gFnSc2	neurastenie
jakožto	jakožto	k8xS	jakožto
"	"	kIx"	"
<g/>
úzkostné	úzkostný	k2eAgFnPc1d1	úzkostná
neurózy	neuróza	k1gFnPc1	neuróza
<g/>
"	"	kIx"	"
Obsese	obsese	k1gFnSc1	obsese
a	a	k8xC	a
fobie	fobie	k1gFnSc1	fobie
Ke	k	k7c3	k
kritice	kritika	k1gFnSc3	kritika
"	"	kIx"	"
<g/>
úzkostné	úzkostný	k2eAgFnPc1d1	úzkostná
neurózy	neuróza	k1gFnPc1	neuróza
<g/>
"	"	kIx"	"
Další	další	k2eAgFnPc1d1	další
poznámky	poznámka	k1gFnPc1	poznámka
o	o	k7c6	o
obranných	obranný	k2eAgFnPc6d1	obranná
neuropsychózách	neuropsychóza	k1gFnPc6	neuropsychóza
Dědičnost	dědičnost	k1gFnSc1	dědičnost
a	a	k8xC	a
etiologie	etiologie	k1gFnSc1	etiologie
neuróz	neuróza	k1gFnPc2	neuróza
K	k	k7c3	k
etiologii	etiologie	k1gFnSc3	etiologie
hysterie	hysterie	k1gFnSc2	hysterie
Údaje	údaj	k1gInSc2	údaj
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
soukromého	soukromý	k2eAgMnSc2d1	soukromý
docenta	docent	k1gMnSc2	docent
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sigm	Sigm	k1gMnSc1	Sigm
<g/>
.	.	kIx.	.
</s>
<s>
Freuda	Freuda	k1gFnSc1	Freuda
1877-1897	[number]	k4	1877-1897
Sexualita	sexualita	k1gFnSc1	sexualita
v	v	k7c6	v
etiologii	etiologie	k1gFnSc6	etiologie
neuróz	neuróza	k1gFnPc2	neuróza
K	k	k7c3	k
psychickému	psychický	k2eAgInSc3d1	psychický
mechanismu	mechanismus	k1gInSc3	mechanismus
zapomnětlivosti	zapomnětlivost	k1gFnSc2	zapomnětlivost
O	o	k7c6	o
krycích	krycí	k2eAgFnPc6d1	krycí
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
Svazek	svazek	k1gInSc4	svazek
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
snu	sen	k1gInSc6	sen
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Psychopatologie	psychopatologie	k1gFnSc1	psychopatologie
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
V.	V.	kA	V.
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Freudovská	freudovský	k2eAgFnSc1d1	freudovská
psychoanalytická	psychoanalytický	k2eAgFnSc1d1	psychoanalytická
metoda	metoda	k1gFnSc1	metoda
O	o	k7c6	o
psychoterapii	psychoterapie	k1gFnSc6	psychoterapie
Tři	tři	k4xCgMnPc1	tři
pojednání	pojednání	k1gNnSc1	pojednání
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
sexuality	sexualita	k1gFnSc2	sexualita
Moje	můj	k3xOp1gInPc4	můj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
roli	role	k1gFnSc4	role
sexuality	sexualita	k1gFnSc2	sexualita
v	v	k7c6	v
etiologii	etiologie	k1gFnSc6	etiologie
neuróz	neuróza	k1gFnPc2	neuróza
Zlomek	zlomek	k1gInSc1	zlomek
analysy	analysa	k1gFnSc2	analysa
případu	případ	k1gInSc2	případ
hysterie	hysterie	k1gFnSc2	hysterie
Psychická	psychický	k2eAgFnSc1d1	psychická
léčba	léčba	k1gFnSc1	léčba
(	(	kIx(	(
<g/>
léčba	léčba	k1gFnSc1	léčba
duše	duše	k1gFnSc1	duše
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Vtip	vtip	k1gInSc1	vtip
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
nevědomí	nevědomí	k1gNnSc3	nevědomí
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Diagnostika	diagnostika	k1gFnSc1	diagnostika
skutkové	skutkový	k2eAgFnSc2d1	skutková
podstaty	podstata	k1gFnSc2	podstata
a	a	k8xC	a
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
K	k	k7c3	k
sexuální	sexuální	k2eAgFnSc3d1	sexuální
osvětě	osvěta	k1gFnSc3	osvěta
dětí	dítě	k1gFnPc2	dítě
Bludné	bludný	k2eAgInPc1d1	bludný
představy	představ	k1gInPc1	představ
a	a	k8xC	a
sny	sen	k1gInPc1	sen
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
W.	W.	kA	W.
Jensense	Jensens	k1gMnSc2	Jensens
"	"	kIx"	"
<g/>
Gradiva	Gradivo	k1gNnSc2	Gradivo
<g/>
"	"	kIx"	"
Nutkavá	nutkavý	k2eAgNnPc1d1	nutkavé
jednání	jednání	k1gNnPc1	jednání
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc1d1	náboženský
úkony	úkon	k1gInPc1	úkon
"	"	kIx"	"
<g/>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
<g/>
"	"	kIx"	"
sexuální	sexuální	k2eAgFnSc1d1	sexuální
morálka	morálka	k1gFnSc1	morálka
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
nervozita	nervozita	k1gFnSc1	nervozita
O	o	k7c6	o
dětských	dětský	k2eAgFnPc6d1	dětská
sexuálních	sexuální	k2eAgFnPc6d1	sexuální
teoriích	teorie	k1gFnPc6	teorie
Hysterické	hysterický	k2eAgFnSc2d1	hysterická
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
bisexualitě	bisexualita	k1gFnSc3	bisexualita
Charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
anální	anální	k2eAgFnSc1d1	anální
erotika	erotika	k1gFnSc1	erotika
Básník	básník	k1gMnSc1	básník
a	a	k8xC	a
vytváření	vytváření	k1gNnSc4	vytváření
fantazie	fantazie	k1gFnSc2	fantazie
Rodinný	rodinný	k2eAgInSc1d1	rodinný
román	román	k1gInSc1	román
neurotiků	neurotik	k1gMnPc2	neurotik
Obecně	obecně	k6eAd1	obecně
o	o	k7c6	o
hysterickém	hysterický	k2eAgInSc6d1	hysterický
záchvatu	záchvat	k1gInSc6	záchvat
Analýza	analýza	k1gFnSc1	analýza
fobie	fobie	k1gFnSc1	fobie
pětiletého	pětiletý	k2eAgMnSc2d1	pětiletý
chlapce	chlapec	k1gMnSc2	chlapec
Poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
případu	případ	k1gInSc3	případ
nutkavé	nutkavý	k2eAgFnSc2d1	nutkavá
neurózy	neuróza	k1gFnSc2	neuróza
Svazek	svazek	k1gInSc1	svazek
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
O	o	k7c6	o
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
K	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
diskuse	diskuse	k1gFnSc2	diskuse
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgNnSc4d1	závěrečné
slovo	slovo	k1gNnSc4	slovo
Příspěvky	příspěvek	k1gInPc1	příspěvek
k	k	k7c3	k
psychologii	psychologie	k1gFnSc3	psychologie
milostného	milostný	k2eAgInSc2d1	milostný
života	život	k1gInSc2	život
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Psychogenní	psychogenní	k2eAgFnSc1d1	psychogenní
porucha	porucha	k1gFnSc1	porucha
zraku	zrak	k1gInSc2	zrak
v	v	k7c6	v
psychoanalytickém	psychoanalytický	k2eAgNnSc6d1	psychoanalytické
pojetí	pojetí	k1gNnSc6	pojetí
Budoucí	budoucí	k2eAgFnSc2d1	budoucí
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
terapie	terapie	k1gFnSc2	terapie
O	o	k7c6	o
"	"	kIx"	"
<g/>
divoké	divoký	k2eAgFnSc6d1	divoká
<g/>
"	"	kIx"	"
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
O	o	k7c6	o
opačném	opačný	k2eAgInSc6d1	opačný
významu	význam	k1gInSc6	význam
prvotních	prvotní	k2eAgNnPc2d1	prvotní
slov	slovo	k1gNnPc2	slovo
Dopis	dopis	k1gInSc1	dopis
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Friedrichu	Friedrich	k1gMnSc3	Friedrich
S.	S.	kA	S.
Kraussovi	Krauss	k1gMnSc3	Krauss
o	o	k7c4	o
"	"	kIx"	"
<g/>
Antropophytei	Antropophytei	k1gNnSc4	Antropophytei
<g/>
"	"	kIx"	"
Příklady	příklad	k1gInPc1	příklad
prozrazení	prozrazení	k1gNnPc2	prozrazení
patogenních	patogenní	k2eAgFnPc2d1	patogenní
fantazií	fantazie	k1gFnPc2	fantazie
u	u	k7c2	u
neurotiků	neurotik	k1gMnPc2	neurotik
Formulování	formulování	k1gNnSc2	formulování
dvou	dva	k4xCgInPc2	dva
principů	princip	k1gInPc2	princip
psychického	psychický	k2eAgNnSc2d1	psychické
dění	dění	k1gNnSc2	dění
Psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
autobiograficky	autobiograficky	k6eAd1	autobiograficky
popsanému	popsaný	k2eAgInSc3d1	popsaný
případu	případ	k1gInSc3	případ
paranoie	paranoia	k1gFnSc2	paranoia
(	(	kIx(	(
<g/>
dementia	dementia	k1gFnSc1	dementia
paranoides	paranoides	k1gMnSc1	paranoides
<g/>
)	)	kIx)	)
O	o	k7c6	o
typech	typ	k1gInPc6	typ
neurotických	neurotický	k2eAgFnPc2d1	neurotická
onemocnění	onemocnění	k1gNnPc4	onemocnění
K	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
diskuse	diskuse	k1gFnSc2	diskuse
o	o	k7c4	o
onanii	onanie	k1gFnSc4	onanie
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
slovo	slovo	k1gNnSc1	slovo
Význam	význam	k1gInSc4	význam
pořadí	pořadí	k1gNnSc3	pořadí
samohlásek	samohláska	k1gFnPc2	samohláska
Zacházení	zacházení	k1gNnPc2	zacházení
s	s	k7c7	s
výkladem	výklad	k1gInSc7	výklad
snů	sen	k1gInPc2	sen
v	v	k7c6	v
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
je	být	k5eAaImIp3nS	být
Diana	Diana	k1gFnSc1	Diana
Efezských	Efezský	k2eAgMnPc2d1	Efezský
<g/>
"	"	kIx"	"
K	k	k7c3	k
dynamice	dynamika	k1gFnSc3	dynamika
přenosu	přenos	k1gInSc2	přenos
Rady	rada	k1gFnSc2	rada
lékaři	lékař	k1gMnPc1	lékař
při	při	k7c6	při
psychoanalytické	psychoanalytický	k2eAgFnSc6d1	psychoanalytická
léčbě	léčba	k1gFnSc6	léčba
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
Dvě	dva	k4xCgFnPc1	dva
dětské	dětský	k2eAgFnSc2d1	dětská
lži	lež	k1gFnSc2	lež
Několik	několik	k4yIc4	několik
poznámek	poznámka	k1gFnPc2	poznámka
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
nevědomí	nevědomí	k1gNnSc4	nevědomí
v	v	k7c6	v
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
Náchylnost	náchylnost	k1gFnSc1	náchylnost
k	k	k7c3	k
nutkavé	nutkavý	k2eAgFnSc3d1	nutkavá
neuróze	neuróza	k1gFnSc3	neuróza
K	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
léčby	léčba	k1gFnSc2	léčba
Svazek	svazek	k1gInSc1	svazek
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Totem	totem	k1gInSc1	totem
a	a	k8xC	a
tabu	tabu	k1gNnSc1	tabu
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
X.	X.	kA	X.
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
materiál	materiál	k1gInSc1	materiál
ve	v	k7c6	v
snech	sen	k1gInPc6	sen
Sen	sena	k1gFnPc2	sena
jako	jako	k8xC	jako
důkazní	důkazní	k2eAgInSc1d1	důkazní
prostředek	prostředek	k1gInSc1	prostředek
Motiv	motiv	k1gInSc1	motiv
volby	volba	k1gFnSc2	volba
krabičky	krabička	k1gFnSc2	krabička
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
a	a	k8xC	a
příklady	příklad	k1gInPc1	příklad
z	z	k7c2	z
analytické	analytický	k2eAgFnSc2d1	analytická
praxe	praxe	k1gFnSc2	praxe
K	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
psychoanalytického	psychoanalytický	k2eAgNnSc2d1	psychoanalytické
hnutí	hnutí	k1gNnSc2	hnutí
O	o	k7c6	o
"	"	kIx"	"
<g/>
fausse	faussa	k1gFnSc6	faussa
reconnaissance	reconnaissanec	k1gInSc2	reconnaissanec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
déja	déja	k6eAd1	déja
raconté	racontý	k2eAgNnSc1d1	racontý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
během	během	k7c2	během
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
práce	práce	k1gFnSc2	práce
Vybavování	vybavování	k1gNnSc2	vybavování
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc4	opakování
a	a	k8xC	a
propracovávání	propracovávání	k1gNnSc4	propracovávání
K	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
narcismu	narcismus	k1gInSc2	narcismus
Michelangelův	Michelangelův	k2eAgMnSc1d1	Michelangelův
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
K	k	k7c3	k
psychologii	psychologie	k1gFnSc3	psychologie
gymnasisty	gymnasista	k1gMnSc2	gymnasista
Pudy	pud	k1gInPc7	pud
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
osudy	osud	k1gInPc4	osud
Sdělení	sdělení	k1gNnSc2	sdělení
o	o	k7c6	o
případu	případ	k1gInSc6	případ
paranoi	paranoi	k6eAd1	paranoi
odporujícím	odporující	k2eAgMnSc7d1	odporující
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
teorii	teorie	k1gFnSc4	teorie
Vytěsnění	vytěsnění	k1gNnSc2	vytěsnění
Nevědomí	nevědomí	k1gNnSc2	nevědomí
Poznámky	poznámka	k1gFnSc2	poznámka
o	o	k7c6	o
přenosové	přenosový	k2eAgFnSc6d1	přenosová
lásce	láska	k1gFnSc6	láska
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
poznámky	poznámka	k1gFnPc1	poznámka
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
Pomíjejícnost	pomíjejícnost	k1gFnSc1	pomíjejícnost
Některé	některý	k3yIgInPc4	některý
charakterové	charakterový	k2eAgInPc4d1	charakterový
typy	typ	k1gInPc4	typ
z	z	k7c2	z
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
práce	práce	k1gFnSc2	práce
Vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
symbolem	symbol	k1gInSc7	symbol
a	a	k8xC	a
symptomem	symptom	k1gInSc7	symptom
Mytologická	mytologický	k2eAgFnSc1d1	mytologická
paralela	paralela	k1gFnSc1	paralela
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
plastické	plastický	k2eAgFnSc3d1	plastická
nutkavé	nutkavý	k2eAgFnSc3d1	nutkavá
představě	představa	k1gFnSc3	představa
O	o	k7c6	o
přeměnách	přeměna	k1gFnPc6	přeměna
pudů	pud	k1gInPc2	pud
zvláště	zvláště	k6eAd1	zvláště
anální	anální	k2eAgFnSc2d1	anální
erotiky	erotika	k1gFnSc2	erotika
Metapsychologické	metapsychologický	k2eAgNnSc4d1	metapsychologický
doplnění	doplnění	k1gNnSc4	doplnění
učení	učení	k1gNnSc2	učení
o	o	k7c6	o
snech	sen	k1gInPc6	sen
Smutek	smutek	k1gInSc1	smutek
a	a	k8xC	a
melancholie	melancholie	k1gFnSc1	melancholie
Svazek	svazek	k1gInSc1	svazek
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
k	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Jedna	jeden	k4xCgFnSc1	jeden
</s>
<s>
potíž	potíž	k1gFnSc1	potíž
s	s	k7c7	s
psychoanalýzou	psychoanalýza	k1gFnSc7	psychoanalýza
Jedna	jeden	k4xCgFnSc1	jeden
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
z	z	k7c2	z
"	"	kIx"	"
<g/>
Dichtung	Dichtunga	k1gFnPc2	Dichtunga
und	und	k?	und
Wahrheit	Wahrheita	k1gFnPc2	Wahrheita
<g/>
"	"	kIx"	"
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
případu	případ	k1gInSc2	případ
dětské	dětský	k2eAgFnSc2d1	dětská
neurózy	neuróza	k1gFnSc2	neuróza
Příspěvky	příspěvek	k1gInPc1	příspěvek
k	k	k7c3	k
psychologii	psychologie	k1gFnSc3	psychologie
milostného	milostný	k2eAgInSc2d1	milostný
života	život	k1gInSc2	život
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
terapie	terapie	k1gFnSc2	terapie
"	"	kIx"	"
<g/>
Dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
bito	bit	k2eAgNnSc1d1	bito
<g/>
"	"	kIx"	"
Něco	něco	k3yInSc1	něco
tísnivého	tísnivý	k2eAgNnSc2d1	tísnivé
O	o	k7c6	o
psychogenezi	psychogeneze	k1gFnSc6	psychogeneze
jednoho	jeden	k4xCgInSc2	jeden
případu	případ	k1gInSc2	případ
ženské	ženský	k2eAgFnSc2d1	ženská
homosexuality	homosexualita	k1gFnSc2	homosexualita
Myšlenková	myšlenkový	k2eAgFnSc1d1	myšlenková
asociace	asociace	k1gFnSc1	asociace
jednoho	jeden	k4xCgNnSc2	jeden
čtyřletého	čtyřletý	k2eAgNnSc2d1	čtyřleté
dítěte	dítě	k1gNnSc2	dítě
O	o	k7c6	o
předhistorii	předhistorie	k1gFnSc6	předhistorie
analytické	analytický	k2eAgFnSc2d1	analytická
techniky	technika	k1gFnSc2	technika
Svazek	svazek	k1gInSc1	svazek
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
princip	princip	k1gInSc4	princip
slasti	slast	k1gFnSc2	slast
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
práce	práce	k1gFnSc2	práce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Mimo	mimo	k7c4	mimo
princip	princip	k1gInSc4	princip
slasti	slast	k1gFnSc2	slast
Davová	davový	k2eAgFnSc1d1	davová
psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
analýza	analýza	k1gFnSc1	analýza
Já	já	k3xPp1nSc1	já
Sen	sen	k1gInSc1	sen
a	a	k8xC	a
telepatie	telepatie	k1gFnSc1	telepatie
O	o	k7c6	o
některých	některý	k3yIgInPc6	některý
neurotických	neurotický	k2eAgInPc6d1	neurotický
mechanizmech	mechanizmus	k1gInPc6	mechanizmus
u	u	k7c2	u
žárlivosti	žárlivost	k1gFnSc2	žárlivost
<g/>
,	,	kIx,	,
paranoi	parano	k1gFnSc2	parano
a	a	k8xC	a
homosexuality	homosexualita	k1gFnSc2	homosexualita
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
libida	libido	k1gNnSc2	libido
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
Ono	onen	k3xDgNnSc1	onen
Genitální	genitální	k2eAgFnPc1d1	genitální
organizace	organizace	k1gFnPc1	organizace
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
Poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
a	a	k8xC	a
praxi	praxe	k1gFnSc3	praxe
výkladu	výklad	k1gInSc2	výklad
snů	sen	k1gInPc2	sen
Ďábelská	ďábelský	k2eAgFnSc1d1	ďábelská
neuróza	neuróza	k1gFnSc1	neuróza
v	v	k7c6	v
sedmnáctém	sedmnáctý	k4xOgInSc6	sedmnáctý
století	století	k1gNnSc6	století
Josef	Josef	k1gMnSc1	Josef
Popper-Lynkeus	Popper-Lynkeus	k1gMnSc1	Popper-Lynkeus
a	a	k8xC	a
teorie	teorie	k1gFnPc1	teorie
snů	sen	k1gInPc2	sen
Ztráta	ztráta	k1gFnSc1	ztráta
smyslu	smysl	k1gInSc6	smysl
pro	pro	k7c4	pro
realitu	realita	k1gFnSc4	realita
při	při	k7c6	při
neuróze	neuróza	k1gFnSc6	neuróza
a	a	k8xC	a
psychóze	psychóza	k1gFnSc6	psychóza
Ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
problém	problém	k1gInSc4	problém
masochizmu	masochizmus	k1gInSc2	masochizmus
Neuróza	neuróza	k1gFnSc1	neuróza
a	a	k8xC	a
psychóza	psychóza	k1gFnSc1	psychóza
Zánik	zánik	k1gInSc1	zánik
oidipského	oidipský	k2eAgInSc2d1	oidipský
komplexu	komplex	k1gInSc2	komplex
Stručný	stručný	k2eAgInSc4d1	stručný
nástin	nástin	k1gInSc4	nástin
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Dodatek	dodatek	k1gInSc1	dodatek
k	k	k7c3	k
analýze	analýza	k1gFnSc3	analýza
malého	malý	k2eAgMnSc2d1	malý
Hanse	Hans	k1gMnSc2	Hans
Svazek	svazek	k1gInSc4	svazek
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Zázračný	zázračný	k2eAgInSc1d1	zázračný
blok	blok	k1gInSc1	blok
<g/>
"	"	kIx"	"
Popření	popření	k1gNnSc1	popření
Některé	některý	k3yIgInPc1	některý
psychické	psychický	k2eAgInPc4d1	psychický
důsledky	důsledek	k1gInPc4	důsledek
anatomického	anatomický	k2eAgInSc2d1	anatomický
rozdílu	rozdíl	k1gInSc2	rozdíl
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
O	o	k7c6	o
sobě	se	k3xPyFc3	se
a	a	k8xC	a
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
Odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
Zabrždění	zabržděný	k2eAgMnPc1d1	zabržděný
<g/>
,	,	kIx,	,
symptom	symptom	k1gInSc1	symptom
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
Otázka	otázka	k1gFnSc1	otázka
analýzy	analýza	k1gFnSc2	analýza
prováděné	prováděný	k2eAgMnPc4d1	prováděný
laiky	laik	k1gMnPc4	laik
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
Fetišismus	fetišismus	k1gInSc1	fetišismus
Budoucnost	budoucnost	k1gFnSc1	budoucnost
jedné	jeden	k4xCgFnSc2	jeden
iluze	iluze	k1gFnSc2	iluze
<g />
.	.	kIx.	.
</s>
<s>
Humor	humor	k1gInSc1	humor
Náboženský	náboženský	k2eAgInSc1d1	náboženský
prožitek	prožitek	k1gInSc1	prožitek
Dostojevský	Dostojevský	k2eAgInSc1d1	Dostojevský
a	a	k8xC	a
otcovražda	otcovražda	k1gFnSc1	otcovražda
Pocit	pocit	k1gInSc1	pocit
stísněnosti	stísněnost	k1gFnSc2	stísněnost
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
O	o	k7c6	o
libidinózních	libidinózní	k2eAgInPc6d1	libidinózní
typech	typ	k1gInPc6	typ
O	o	k7c6	o
ženské	ženský	k2eAgFnSc6d1	ženská
sexualitě	sexualita	k1gFnSc6	sexualita
Goethova	Goethův	k2eAgFnSc1d1	Goethova
cena	cena	k1gFnSc1	cena
1930	[number]	k4	1930
Dopis	dopis	k1gInSc1	dopis
Romainu	Romain	k1gMnSc3	Romain
Rollandovi	Rolland	k1gMnSc3	Rolland
Ernestu	Ernest	k1gMnSc3	Ernest
Jonesovi	Jones	k1gMnSc3	Jones
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
Dopis	dopis	k1gInSc4	dopis
vydavateli	vydavatel	k1gMnSc5	vydavatel
Jüdische	Jüdischus	k1gMnSc5	Jüdischus
Presszentrale	Presszentral	k1gMnSc5	Presszentral
Zürich	Züricha	k1gFnPc2	Züricha
K	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
university	universita	k1gFnSc2	universita
Dopis	dopis	k1gInSc1	dopis
Maximu	Maxim	k1gMnSc3	Maxim
Leroyovi	Leroya	k1gMnSc3	Leroya
o	o	k7c6	o
jednom	jeden	k4xCgNnSc6	jeden
Descartovu	Descartův	k2eAgFnSc4d1	Descartova
snu	sen	k1gInSc2	sen
Dopis	dopis	k1gInSc4	dopis
starostovi	starosta	k1gMnSc3	starosta
města	město	k1gNnSc2	město
Příbor	Příbor	k1gInSc1	Příbor
Svazek	svazek	k1gInSc1	svazek
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
řada	řada	k1gFnSc1	řada
přednášek	přednáška	k1gFnPc2	přednáška
k	k	k7c3	k
úvodu	úvod	k1gInSc3	úvod
do	do	k7c2	do
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Svazek	svazek	k1gInSc1	svazek
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
K	k	k7c3	k
získání	získání	k1gNnSc3	získání
ohně	oheň	k1gInSc2	oheň
Proč	proč	k6eAd1	proč
válka	válka	k1gFnSc1	válka
<g/>
?	?	kIx.	?
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
k	k	k7c3	k
mé	můj	k3xOp1gFnSc3	můj
autobiografii	autobiografie	k1gFnSc3	autobiografie
Jemnost	jemnost	k1gFnSc4	jemnost
jednoho	jeden	k4xCgInSc2	jeden
chybného	chybný	k2eAgInSc2d1	chybný
úkonu	úkon	k1gInSc2	úkon
Konstrukce	konstrukce	k1gFnSc2	konstrukce
v	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
Konečná	konečný	k2eAgFnSc1d1	konečná
a	a	k8xC	a
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
analýza	analýza	k1gFnSc1	analýza
Muž	muž	k1gMnSc1	muž
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
a	a	k8xC	a
monoteistické	monoteistický	k2eAgNnSc1d1	monoteistické
náboženství	náboženství	k1gNnSc1	náboženství
Svazek	svazek	k1gInSc1	svazek
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Příspěvky	příspěvek	k1gInPc1	příspěvek
ke	k	k7c3	k
"	"	kIx"	"
<g/>
Studiím	studie	k1gFnPc3	studie
o	o	k7c6	o
hysterii	hysterie	k1gFnSc6	hysterie
<g/>
"	"	kIx"	"
Vyplněné	vyplněný	k2eAgNnSc1d1	vyplněné
tušení	tušení	k1gNnSc1	tušení
ze	z	k7c2	z
snu	sen	k1gInSc2	sen
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
a	a	k8xC	a
telepatie	telepatie	k1gFnSc1	telepatie
Hlava	hlava	k1gFnSc1	hlava
medúzy	medúza	k1gFnSc2	medúza
Proslov	proslov	k1gInSc1	proslov
ke	k	k7c3	k
členům	člen	k1gMnPc3	člen
spolku	spolek	k1gInSc2	spolek
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
nai	nai	k?	nai
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
rith	rith	k1gMnSc1	rith
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Rozštěpení	rozštěpení	k1gNnSc2	rozštěpení
Já	já	k3xPp1nSc1	já
při	při	k7c6	při
obranném	obranný	k2eAgInSc6d1	obranný
postupu	postup	k1gInSc6	postup
Nástin	nástin	k1gInSc1	nástin
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Some	Som	k1gFnSc2	Som
Elementary	Elementara	k1gFnSc2	Elementara
Lessons	Lessons	k1gInSc1	Lessons
in	in	k?	in
Psycho-Analysis	Psycho-Analysis	k1gFnSc1	Psycho-Analysis
Výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc4	problém
BABIN	babin	k2eAgInSc1d1	babin
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
-	-	kIx~	-
tragédie	tragédie	k1gFnSc1	tragédie
nepochopení	nepochopení	k1gNnSc2	nepochopení
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
144	[number]	k4	144
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7145	[number]	k4	7145
<g/>
-	-	kIx~	-
<g/>
121	[number]	k4	121
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
CVEKL	CVEKL	kA	CVEKL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
244	[number]	k4	244
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
447	[number]	k4	447
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
ČERNOUŠEK	černoušek	k1gMnSc1	černoušek
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
-	-	kIx~	-
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
181	[number]	k4	181
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
126	[number]	k4	126
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
MANNONI	MANNONI	kA	MANNONI
<g/>
,	,	kIx,	,
Octave	Octav	k1gInSc5	Octav
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
188	[number]	k4	188
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
366	[number]	k4	366
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
MARKUS	MARKUS	kA	MARKUS
<g/>
,	,	kIx,	,
Georg	Georg	k1gInSc1	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
264	[number]	k4	264
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
447	[number]	k4	447
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SIMÉON	SIMÉON	kA	SIMÉON
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
<g/>
;	;	kIx,	;
ARIEL	ARIEL	kA	ARIEL
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
<g/>
:	:	kIx,	:
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
56	[number]	k4	56
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7407	[number]	k4	7407
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
STONE	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
Irving	Irving	k1gInSc1	Irving
<g/>
.	.	kIx.	.
</s>
<s>
Vášně	vášeň	k1gFnPc1	vášeň
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
1008	[number]	k4	1008
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
792	[number]	k4	792
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
STORR	STORR	kA	STORR
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc7	Anthon
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
162	[number]	k4	162
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85794	[number]	k4	85794
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ADAMEC	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
život	život	k1gInSc1	život
psychoanalýsy	psychoanalýsa	k1gFnSc2	psychoanalýsa
<g/>
:	:	kIx,	:
z	z	k7c2	z
dopisů	dopis	k1gInPc2	dopis
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
a	a	k8xC	a
Sándora	Sándor	k1gMnSc2	Sándor
Ferencziho	Ferenczi	k1gMnSc2	Ferenczi
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Filosofický	filosofický	k2eAgInSc1d1	filosofický
seminář	seminář	k1gInSc1	seminář
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
65	[number]	k4	65
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87234	[number]	k4	87234
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
on-line	onin	k1gInSc5	on-lin
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
ADAMEC	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalýsa	Psychoanalýsa	k1gFnSc1	Psychoanalýsa
<g/>
:	:	kIx,	:
přehled	přehled	k1gInSc1	přehled
souborného	souborný	k2eAgInSc2d1	souborný
síla	síla	k1gFnSc1	síla
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Filosofický	filosofický	k2eAgInSc1d1	filosofický
seminář	seminář	k1gInSc1	seminář
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
74	[number]	k4	74
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
1319	[number]	k4	1319
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
on-line	onin	k1gInSc5	on-lin
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
ALBERTI	ALBERTI	kA	ALBERTI
<g/>
,	,	kIx,	,
Christiane	Christian	k1gMnSc5	Christian
<g/>
;	;	kIx,	;
SAURET	SAURET	kA	SAURET
<g/>
,	,	kIx,	,
Marie-Jean	Marie-Jean	k1gInSc1	Marie-Jean
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
65	[number]	k4	65
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
BURIÁNEK	Buriánek	k1gMnSc1	Buriánek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Fakta	faktum	k1gNnPc1	faktum
a	a	k8xC	a
dojmy	dojem	k1gInPc1	dojem
<g/>
:	:	kIx,	:
psychoanalytikův	psychoanalytikův	k2eAgMnSc1d1	psychoanalytikův
malý	malý	k2eAgMnSc1d1	malý
průvodce	průvodce	k1gMnSc1	průvodce
rodným	rodný	k2eAgInSc7d1	rodný
krajem	kraj	k1gInSc7	kraj
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
930	[number]	k4	930
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
CHATEAU	CHATEAU	kA	CHATEAU
<g/>
,	,	kIx,	,
Ladislava	Ladislav	k1gMnSc2	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
pro	pro	k7c4	pro
Lou	Lou	k1gFnSc4	Lou
<g/>
,	,	kIx,	,
Sabinu	Sabina	k1gFnSc4	Sabina
a	a	k8xC	a
Marii	Maria	k1gFnSc4	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Psychoanalytické	psychoanalytický	k2eAgNnSc1d1	psychoanalytické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
190	[number]	k4	190
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86123	[number]	k4	86123
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
KERR	KERR	kA	KERR
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
metoda	metoda	k1gFnSc1	metoda
<g/>
:	:	kIx,	:
příběh	příběh	k1gInSc1	příběh
Junga	Jung	k1gMnSc2	Jung
<g/>
,	,	kIx,	,
Freuda	Freud	k1gMnSc2	Freud
a	a	k8xC	a
Sabiny	Sabina	k1gMnSc2	Sabina
Spielreinové	Spielreinová	k1gFnSc2	Spielreinová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
553	[number]	k4	553
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
MAHLER	MAHLER	kA	MAHLER
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85844	[number]	k4	85844
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
MASSON	MASSON	kA	MASSON
<g/>
,	,	kIx,	,
Jeffrey	Jeffre	k2eAgInPc4d1	Jeffre
Moussaieff	Moussaieff	k1gInSc4	Moussaieff
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
na	na	k7c4	na
pravdu	pravda	k1gFnSc4	pravda
<g/>
:	:	kIx,	:
Freudovo	Freudův	k2eAgNnSc1d1	Freudovo
potlačení	potlačení	k1gNnSc1	potlačení
teorie	teorie	k1gFnSc2	teorie
svádění	svádění	k1gNnSc2	svádění
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1640	[number]	k4	1640
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RICHEBÄCHER	RICHEBÄCHER	kA	RICHEBÄCHER
<g/>
,	,	kIx,	,
Sabine	Sabin	k1gMnSc5	Sabin
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
mezi	mezi	k7c7	mezi
Jungem	Jung	k1gMnSc7	Jung
a	a	k8xC	a
Freudem	Freud	k1gInSc7	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SAYERSOVÁ	SAYERSOVÁ	kA	SAYERSOVÁ
<g/>
,	,	kIx,	,
Janet	Janet	k1gInSc1	Janet
<g/>
.	.	kIx.	.
</s>
<s>
Matky	matka	k1gFnPc1	matka
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
258	[number]	k4	258
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SCHWARTZ	SCHWARTZ	kA	SCHWARTZ
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
393	[number]	k4	393
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
SLIPP	SLIPP	kA	SLIPP
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
<g/>
.	.	kIx.	.
</s>
<s>
Freudovská	freudovský	k2eAgFnSc1d1	freudovská
mystika	mystika	k1gFnSc1	mystika
<g/>
:	:	kIx,	:
Freud	Freud	k1gInSc1	Freud
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
feminismus	feminismus	k1gInSc1	feminismus
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
266	[number]	k4	266
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
891	[number]	k4	891
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
WEISSWEILEROVÁ	WEISSWEILEROVÁ	kA	WEISSWEILEROVÁ
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Freudové	Freudová	k1gFnPc1	Freudová
<g/>
:	:	kIx,	:
biografie	biografie	k1gFnPc1	biografie
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
:	:	kIx,	:
H	H	kA	H
a	a	k8xC	a
H	H	kA	H
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
358	[number]	k4	358
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7319	[number]	k4	7319
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
WOLFF	WOLFF	kA	WOLFF
<g/>
,	,	kIx,	,
Larry	Larr	k1gInPc7	Larr
<g/>
.	.	kIx.	.
</s>
<s>
Týrání	týrání	k1gNnSc1	týrání
a	a	k8xC	a
zneužívání	zneužívání	k1gNnSc1	zneužívání
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Freuda	Freudo	k1gNnSc2	Freudo
(	(	kIx(	(
<g/>
korespondenční	korespondenční	k2eAgInPc4d1	korespondenční
lístky	lístek	k1gInPc4	lístek
z	z	k7c2	z
konce	konec	k1gInSc2	konec
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
869	[number]	k4	869
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
YERUSHALMI	YERUSHALMI	kA	YERUSHALMI
<g/>
,	,	kIx,	,
Yosef	Yosef	k1gMnSc1	Yosef
Hayim	Hayim	k1gMnSc1	Hayim
<g/>
.	.	kIx.	.
</s>
<s>
Freudův	Freudův	k2eAgMnSc1d1	Freudův
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
:	:	kIx,	:
Judaismus	judaismus	k1gInSc1	judaismus
konečný	konečný	k2eAgInSc1d1	konečný
a	a	k8xC	a
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
234	[number]	k4	234
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2501	[number]	k4	2501
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
FLEM	FLEM	kA	FLEM
<g/>
,	,	kIx,	,
Lydia	Lydia	k1gFnSc1	Lydia
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gInSc5	Homm
Freud	Freuda	k1gFnPc2	Freuda
<g/>
:	:	kIx,	:
une	une	k?	une
biographie	biographie	k1gFnSc1	biographie
intellectuelle	intellectuelle	k1gFnSc1	intellectuelle
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Seuil	Seuil	k1gMnSc1	Seuil
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
GAY	gay	k1gMnSc1	gay
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
:	:	kIx,	:
Freud	Freud	k1gMnSc1	Freud
<g/>
:	:	kIx,	:
A	a	k8xC	a
Life	Life	k1gFnSc1	Life
for	forum	k1gNnPc2	forum
Our	Our	k1gFnSc2	Our
Time	Time	k1gFnSc1	Time
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Papermac	Papermac	k1gFnSc1	Papermac
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
JONES	JONES	kA	JONES
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
<g/>
:	:	kIx,	:
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
:	:	kIx,	:
Life	Life	k1gFnSc1	Life
and	and	k?	and
Work	Work	k1gMnSc1	Work
I-III	I-III	k1gMnSc1	I-III
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Hogarth	Hogarth	k1gMnSc1	Hogarth
Press	Press	k1gInSc4	Press
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
MAJOR	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
<g/>
,	,	kIx,	,
TALAGRAND	TALAGRAND	kA	TALAGRAND
<g/>
,	,	kIx,	,
Chantal	Chantal	k1gMnSc1	Chantal
<g/>
:	:	kIx,	:
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Gallimard	Gallimard	k1gMnSc1	Gallimard
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ELLENBERGER	ELLENBERGER	kA	ELLENBERGER
<g/>
,	,	kIx,	,
Henri	Henr	k1gInPc7	Henr
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
la	la	k0	la
découverte	découvrat	k5eAaPmRp2nP	découvrat
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
inconscient	inconscient	k1gMnSc1	inconscient
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Fayard	Fayard	k1gMnSc1	Fayard
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ANZIEU	ANZIEU	kA	ANZIEU
<g/>
,	,	kIx,	,
Didier	Didier	k1gMnSc1	Didier
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
auto-analyse	autonalysa	k1gFnSc3	auto-analysa
de	de	k?	de
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Press	Press	k1gInSc1	Press
Iniversitaires	Iniversitaires	k1gInSc1	Iniversitaires
de	de	k?	de
France	Franc	k1gMnSc2	Franc
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
APPIGNANESI	APPIGNANESI	kA	APPIGNANESI
<g/>
,	,	kIx,	,
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
FORRESTER	FORRESTER	kA	FORRESTER
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
:	:	kIx,	:
Freud	Freud	k1gMnSc1	Freud
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Women	Women	k1gInSc1	Women
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Phoenix	Phoenix	k1gInSc1	Phoenix
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
BRIDGE	BRIDGE	kA	BRIDGE
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Way	Way	k1gMnSc2	Way
Home	Hom	k1gMnSc2	Hom
<g/>
:	:	kIx,	:
Conversations	Conversations	k1gInSc1	Conversations
Between	Between	k2eAgInSc1d1	Between
Writers	Writers	k1gInSc1	Writers
and	and	k?	and
Psychoanalysts	Psychoanalysts	k1gInSc1	Psychoanalysts
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Karnac	Karnac	k1gFnSc1	Karnac
Books	Books	k1gInSc1	Books
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
GARDINER	GARDINER	kA	GARDINER
<g/>
,	,	kIx,	,
Muriel	Muriel	k1gInSc1	Muriel
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Wolf-Man	Wolf-Man	k1gMnSc1	Wolf-Man
and	and	k?	and
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Basic	Basic	kA	Basic
Books	Books	k1gInSc4	Books
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
KANZER	KANZER	kA	KANZER
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
<g/>
,	,	kIx,	,
GLENN	GLENN	kA	GLENN
<g/>
,	,	kIx,	,
Jules	Jules	k1gInSc1	Jules
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Freud	Freud	k1gInSc1	Freud
and	and	k?	and
his	his	k1gNnSc2	his
Patients	Patientsa	k1gFnPc2	Patientsa
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Yason	Yason	k1gInSc1	Yason
Aronson	Aronson	k1gNnSc1	Aronson
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
ROGAL	rogalo	k1gNnPc2	rogalo
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
:	:	kIx,	:
Freuds	Freuds	k1gInSc1	Freuds
Psychoanalyse	psychoanalysa	k1gFnSc3	psychoanalysa
<g/>
:	:	kIx,	:
am	am	k?	am
Beispiel	Beispiel	k1gInSc1	Beispiel
von	von	k1gInSc1	von
Arthur	Arthura	k1gFnPc2	Arthura
Schnitzlers	Schnitzlersa	k1gFnPc2	Schnitzlersa
Novelle	Novelle	k1gInSc1	Novelle
Leutnant	Leutnant	k1gMnSc1	Leutnant
Gustl	Gustl	k1gMnSc1	Gustl
<g/>
,	,	kIx,	,
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
:	:	kIx,	:
Militzke	Militzke	k1gInSc1	Militzke
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
SAYERS	SAYERS	kA	SAYERS
<g/>
,	,	kIx,	,
Janet	Janet	k1gMnSc1	Janet
<g/>
:	:	kIx,	:
Freud	Freud	k1gMnSc1	Freud
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Art	Art	k1gFnSc7	Art
<g/>
:	:	kIx,	:
Psychoanalysis	Psychoanalysis	k1gFnSc1	Psychoanalysis
Retold	Retold	k1gMnSc1	Retold
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
WESTERINK	WESTERINK	kA	WESTERINK
<g/>
,	,	kIx,	,
Herman	Herman	k1gMnSc1	Herman
<g/>
:	:	kIx,	:
Controversy	Controvers	k1gInPc1	Controvers
and	and	k?	and
challenge	challeng	k1gInSc2	challeng
<g/>
:	:	kIx,	:
the	the	k?	the
reception	reception	k1gInSc1	reception
of	of	k?	of
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
psychoanalysis	psychoanalysis	k1gFnSc7	psychoanalysis
in	in	k?	in
German	German	k1gMnSc1	German
and	and	k?	and
Dutch	Dutch	k1gInSc1	Dutch
speaking	speaking	k1gInSc1	speaking
theology	theolog	k1gMnPc4	theolog
and	and	k?	and
religious	religious	k1gMnSc1	religious
studies	studies	k1gMnSc1	studies
<g/>
,	,	kIx,	,
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
LIT	lit	k1gInSc1	lit
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
WEIGEL	WEIGEL	kA	WEIGEL
<g/>
,	,	kIx,	,
Sigrid	Sigrid	k1gInSc4	Sigrid
<g/>
:	:	kIx,	:
Heine	Hein	k1gMnSc5	Hein
und	und	k?	und
Freud	Freudo	k1gNnPc2	Freudo
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Kulturverlag	Kulturverlag	k1gInSc1	Kulturverlag
Kadmos	Kadmos	k1gInSc1	Kadmos
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
FONAGY	FONAGY	kA	FONAGY
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
;	;	kIx,	;
TARGETOVÁ	TARGETOVÁ	kA	TARGETOVÁ
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalytické	psychoanalytický	k2eAgFnPc1d1	psychoanalytická
teorie	teorie	k1gFnPc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7178	[number]	k4	7178
<g/>
-	-	kIx~	-
<g/>
993	[number]	k4	993
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
MITCHELL	MITCHELL	kA	MITCHELL
<g/>
,	,	kIx,	,
Stephen	Stephen	k1gInSc1	Stephen
<g/>
;	;	kIx,	;
BLACKOVÁ	BLACKOVÁ	kA	BLACKOVÁ
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k6eAd1	Freud
a	a	k8xC	a
po	po	k7c6	po
Freudovi	Freuda	k1gMnSc6	Freuda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
314	[number]	k4	314
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RYCROFT	RYCROFT	kA	RYCROFT
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Kritický	kritický	k2eAgInSc1d1	kritický
slovník	slovník	k1gInSc1	slovník
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Psychoanalytické	psychoanalytický	k2eAgNnSc1d1	psychoanalytické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
156	[number]	k4	156
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901601	[number]	k4	901601
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalytické	psychoanalytický	k2eAgInPc1d1	psychoanalytický
směry	směr	k1gInPc1	směr
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
Edward	Edward	k1gMnSc1	Edward
L.	L.	kA	L.
Bernays	Bernays	k1gInSc1	Bernays
Anna	Anna	k1gFnSc1	Anna
Freudová	Freudová	k1gFnSc1	Freudová
Lucian	Lucian	k1gInSc4	Lucian
Freud	Freud	k1gInSc1	Freud
Melanie	Melanie	k1gFnSc1	Melanie
Kleinová	Kleinová	k1gFnSc1	Kleinová
Cena	cena	k1gFnSc1	cena
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
za	za	k7c4	za
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
prózu	próza	k1gFnSc4	próza
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freuda	k1gFnPc2	Freuda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Město	město	k1gNnSc1	město
Příbor	Příbor	k1gInSc1	Příbor
-	-	kIx~	-
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Freud	Freud	k1gInSc1	Freud
na	na	k7c4	na
Answers	Answers	k1gInSc4	Answers
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Videos	Videos	k1gMnSc1	Videos
of	of	k?	of
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
na	na	k7c6	na
webu	web	k1gInSc6	web
psychoanalytika	psychoanalytik	k1gMnSc4	psychoanalytik
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Brouka	brouk	k1gMnSc4	brouk
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Brouk	brouk	k1gMnSc1	brouk
<g/>
:	:	kIx,	:
Zikmund	Zikmund	k1gMnSc1	Zikmund
Freud	Freud	k1gMnSc1	Freud
–	–	k?	–
K	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
jeho	jeho	k3xOp3gFnSc2	jeho
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Rozpravy	rozprava	k1gFnPc1	rozprava
Aventina	Aventin	k1gMnSc2	Aventin
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
r.	r.	kA	r.
VI	VI	kA	VI
<g/>
,	,	kIx,	,
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
21	[number]	k4	21
</s>
