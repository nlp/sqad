<p>
<s>
Bakteriologie	bakteriologie	k1gFnSc1	bakteriologie
(	(	kIx(	(
<g/>
ze	z	k7c2	z
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
β	β	k?	β
bakterion	bakterion	k1gInSc1	bakterion
a	a	k8xC	a
λ	λ	k?	λ
logos	logos	k1gInSc1	logos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
předmětem	předmět	k1gInSc7	předmět
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
zájmu	zájem	k1gInSc2	zájem
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
a	a	k8xC	a
identifikace	identifikace	k1gFnSc1	identifikace
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dílčí	dílčí	k2eAgNnSc1d1	dílčí
disciplínou	disciplína	k1gFnSc7	disciplína
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
se	se	k3xPyFc4	se
také	také	k9	také
zabývá	zabývat	k5eAaImIp3nS	zabývat
patogenními	patogenní	k2eAgFnPc7d1	patogenní
bakteriemi	bakterie	k1gFnPc7	bakterie
a	a	k8xC	a
tak	tak	k6eAd1	tak
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
důležité	důležitý	k2eAgFnPc4d1	důležitá
informace	informace	k1gFnPc4	informace
pro	pro	k7c4	pro
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základy	základ	k1gInPc1	základ
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
položil	položit	k5eAaPmAgMnS	položit
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Antoni	Anton	k1gMnPc1	Anton
van	vana	k1gFnPc2	vana
Leeuwenhoek	Leeuwenhoek	k6eAd1	Leeuwenhoek
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
pozorováních	pozorování	k1gNnPc6	pozorování
mikroskopem	mikroskop	k1gInSc7	mikroskop
popsal	popsat	k5eAaPmAgMnS	popsat
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
vědci	vědec	k1gMnPc7	vědec
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
byli	být	k5eAaImAgMnP	být
Lazzaro	Lazzara	k1gFnSc5	Lazzara
Spallanzani	Spallanzaň	k1gFnSc5	Spallanzaň
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Cohn	Cohn	k1gMnSc1	Cohn
<g/>
,	,	kIx,	,
Martinus	Martinus	k1gMnSc1	Martinus
Willem	Will	k1gInSc7	Will
Beijerinck	Beijerinck	k1gMnSc1	Beijerinck
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Vinogradskij	Vinogradskij	k1gMnSc1	Vinogradskij
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Kluyver	Kluyver	k1gMnSc1	Kluyver
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníky	průkopník	k1gMnPc7	průkopník
lékařské	lékařský	k2eAgFnSc2d1	lékařská
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
byli	být	k5eAaImAgMnP	být
Joseph	Joseph	k1gMnSc1	Joseph
Lister	Lister	k1gMnSc1	Lister
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
boj	boj	k1gInSc4	boj
s	s	k7c7	s
nemocemi	nemoc	k1gFnPc7	nemoc
pomocí	pomocí	k7c2	pomocí
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
a	a	k8xC	a
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc1	první
popisy	popis	k1gInPc1	popis
bakterií	bakterie	k1gFnPc2	bakterie
===	===	k?	===
</s>
</p>
<p>
<s>
1873	[number]	k4	1873
<g/>
:	:	kIx,	:
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
leprae	lepra	k1gInSc2	lepra
–	–	k?	–
Gerhard	Gerhard	k1gMnSc1	Gerhard
Armauer	Armauer	k1gMnSc1	Armauer
Hansen	Hansen	k1gInSc4	Hansen
</s>
</p>
<p>
<s>
1875	[number]	k4	1875
<g/>
:	:	kIx,	:
Clostridium	Clostridium	k1gNnSc4	Clostridium
chauvoei	chauvoe	k1gFnSc2	chauvoe
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
feseri	feseri	k6eAd1	feseri
<g/>
)	)	kIx)	)
–	–	k?	–
Johann	Johann	k1gMnSc1	Johann
Feser	Feser	k1gMnSc1	Feser
</s>
</p>
<p>
<s>
1876	[number]	k4	1876
<g/>
:	:	kIx,	:
Bacillus	Bacillus	k1gInSc1	Bacillus
anthracis	anthracis	k1gFnSc2	anthracis
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
</s>
</p>
<p>
<s>
1877	[number]	k4	1877
<g/>
:	:	kIx,	:
Clostridium	Clostridium	k1gNnSc1	Clostridium
septicum	septicum	k1gInSc1	septicum
–	–	k?	–
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
Jules	Jules	k1gMnSc1	Jules
Joubert	Joubert	k1gMnSc1	Joubert
</s>
</p>
<p>
<s>
1879	[number]	k4	1879
<g/>
:	:	kIx,	:
Neisseria	Neisserium	k1gNnSc2	Neisserium
gonorrhoeae	gonorrhoea	k1gInSc2	gonorrhoea
–	–	k?	–
Albert	Albert	k1gMnSc1	Albert
Neisser	Neisser	k1gMnSc1	Neisser
</s>
</p>
<p>
<s>
1880	[number]	k4	1880
<g/>
:	:	kIx,	:
Salmonella	Salmonella	k1gFnSc1	Salmonella
typhi	typh	k1gFnSc2	typh
–	–	k?	–
Karl	Karl	k1gMnSc1	Karl
Joseph	Joseph	k1gMnSc1	Joseph
Eberth	Eberth	k1gMnSc1	Eberth
<g/>
,	,	kIx,	,
Erysipelothrix	Erysipelothrix	k1gInSc1	Erysipelothrix
muriseptica	muriseptica	k1gMnSc1	muriseptica
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
</s>
</p>
<p>
<s>
1882	[number]	k4	1882
<g/>
:	:	kIx,	:
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
<g/>
;	;	kIx,	;
Burkholderia	Burkholderium	k1gNnPc4	Burkholderium
mallei	malle	k1gFnSc2	malle
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Malleomyces	Malleomyces	k1gInSc1	Malleomyces
m.	m.	k?	m.
<g/>
)	)	kIx)	)
–	–	k?	–
Friedrich	Friedrich	k1gMnSc1	Friedrich
Loeffler	Loeffler	k1gMnSc1	Loeffler
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Schütz	Schütz	k1gMnSc1	Schütz
<g/>
;	;	kIx,	;
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pyogenes	pyogenes	k1gMnSc1	pyogenes
–	–	k?	–
Friedrich	Friedrich	k1gMnSc1	Friedrich
Fehleisen	Fehleisen	k2eAgMnSc1d1	Fehleisen
</s>
</p>
<p>
<s>
1883	[number]	k4	1883
<g/>
:	:	kIx,	:
Vibrio	vibrio	k1gNnSc1	vibrio
cholerae	cholera	k1gInSc2	cholera
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
<g/>
;	;	kIx,	;
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
xerosis	xerosis	k1gFnSc2	xerosis
–	–	k?	–
Albert	Albert	k1gMnSc1	Albert
Neisser	Neisser	k1gMnSc1	Neisser
a	a	k8xC	a
S.	S.	kA	S.
Kuschbert	Kuschbert	k1gInSc1	Kuschbert
</s>
</p>
<p>
<s>
1884	[number]	k4	1884
<g/>
:	:	kIx,	:
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
diphtheriae	diphtheria	k1gFnSc2	diphtheria
–	–	k?	–
F.	F.	kA	F.
Loeffler	Loeffler	k1gInSc4	Loeffler
<g/>
;	;	kIx,	;
Clostridium	Clostridium	k1gNnSc4	Clostridium
tetani	tetaň	k1gFnSc3	tetaň
–	–	k?	–
Arthur	Arthur	k1gMnSc1	Arthur
Nicolaier	Nicolaier	k1gMnSc1	Nicolaier
</s>
</p>
<p>
<s>
1885	[number]	k4	1885
<g/>
:	:	kIx,	:
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
smegmatis	smegmatis	k1gFnSc2	smegmatis
–	–	k?	–
E.	E.	kA	E.
Alvarez	Alvarez	k1gMnSc1	Alvarez
a	a	k8xC	a
Ernst	Ernst	k1gMnSc1	Ernst
Tavel	Tavel	k1gMnSc1	Tavel
<g/>
;	;	kIx,	;
Salmonella	Salmonella	k1gMnSc1	Salmonella
choleraesuis	choleraesuis	k1gFnSc2	choleraesuis
–	–	k?	–
Daniel	Daniel	k1gMnSc1	Daniel
Elmer	Elmer	k1gMnSc1	Elmer
Salmon	Salmon	k1gMnSc1	Salmon
<g/>
;	;	kIx,	;
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
pseudotuberculosis	pseudotuberculosis	k1gFnSc2	pseudotuberculosis
–	–	k?	–
Edmond	Edmond	k1gMnSc1	Edmond
Nocard	Nocard	k1gMnSc1	Nocard
</s>
</p>
<p>
<s>
1886	[number]	k4	1886
<g/>
:	:	kIx,	:
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Diplococcus	Diplococcus	k1gInSc1	Diplococcus
p.	p.	k?	p.
<g/>
)	)	kIx)	)
–	–	k?	–
Albert	Albert	k1gMnSc1	Albert
Fraenkel	Fraenkel	k1gMnSc1	Fraenkel
a	a	k8xC	a
Anton	Anton	k1gMnSc1	Anton
Weichselbaum	Weichselbaum	k1gInSc1	Weichselbaum
<g/>
;	;	kIx,	;
Erysipelothrix	Erysipelothrix	k1gInSc1	Erysipelothrix
rhusiopathiae	rhusiopathia	k1gFnSc2	rhusiopathia
–	–	k?	–
F.	F.	kA	F.
Loeffler	Loeffler	k1gMnSc1	Loeffler
</s>
</p>
<p>
<s>
1887	[number]	k4	1887
<g/>
:	:	kIx,	:
Neisseria	Neisserium	k1gNnSc2	Neisserium
meningitidis	meningitidis	k1gFnSc2	meningitidis
–	–	k?	–
Anton	anton	k1gInSc1	anton
Weichselbaum	Weichselbaum	k1gInSc1	Weichselbaum
<g/>
;	;	kIx,	;
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
pseudodiphthericum	pseudodiphthericum	k1gInSc1	pseudodiphthericum
–	–	k?	–
Franz	Franz	k1gMnSc1	Franz
Adolf	Adolf	k1gMnSc1	Adolf
Hofmann	Hofmann	k1gMnSc1	Hofmann
<g/>
;	;	kIx,	;
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
agalactiae	agalactia	k1gFnSc2	agalactia
–	–	k?	–
E.	E.	kA	E.
Nocard	Nocard	k1gInSc1	Nocard
a	a	k8xC	a
Hyacinthe	Hyacinthe	k1gInSc1	Hyacinthe
Mollereau	Mollereaus	k1gInSc2	Mollereaus
</s>
</p>
<p>
<s>
1888	[number]	k4	1888
<g/>
:	:	kIx,	:
Salmonella	Salmonella	k1gFnSc1	Salmonella
enteritidis	enteritidis	k1gFnSc2	enteritidis
–	–	k?	–
August	August	k1gMnSc1	August
Gärtner	Gärtner	k1gMnSc1	Gärtner
</s>
</p>
<p>
<s>
1891	[number]	k4	1891
<g/>
:	:	kIx,	:
Salmonella	Salmonella	k1gFnSc1	Salmonella
typhimurium	typhimurium	k1gNnSc1	typhimurium
–	–	k?	–
Friedrich	Friedrich	k1gMnSc1	Friedrich
Loeffler	Loeffler	k1gMnSc1	Loeffler
</s>
</p>
<p>
<s>
1892	[number]	k4	1892
<g/>
:	:	kIx,	:
Micrococcus	Micrococcus	k1gInSc1	Micrococcus
epidermidis	epidermidis	k1gFnSc2	epidermidis
–	–	k?	–
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Welch	Welch	k1gMnSc1	Welch
<g/>
;	;	kIx,	;
Moraxella	Moraxella	k1gMnSc1	Moraxella
catarrhalis	catarrhalis	k1gFnSc2	catarrhalis
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Branhamella	Branhamella	k1gFnSc1	Branhamella
catarrhalis	catarrhalis	k1gFnSc2	catarrhalis
<g/>
,	,	kIx,	,
Neisseria	Neisserium	k1gNnSc2	Neisserium
c.	c.	k?	c.
<g/>
)	)	kIx)	)
–	–	k?	–
Seifert	Seifert	k1gMnSc1	Seifert
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Pfeiffe	Pfeiff	k1gInSc5	Pfeiff
<g/>
;	;	kIx,	;
Clostridium	Clostridium	k1gNnSc4	Clostridium
perfringens	perfringens	k6eAd1	perfringens
–	–	k?	–
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Welch	Welch	k1gInSc1	Welch
a	a	k8xC	a
George	George	k1gNnSc1	George
Nuttal	Nuttal	k1gMnSc1	Nuttal
</s>
</p>
<p>
<s>
1893	[number]	k4	1893
<g/>
:	:	kIx,	:
Arcanobacterium	Arcanobacterium	k1gNnSc1	Arcanobacterium
pyogenes	pyogenesa	k1gFnPc2	pyogenesa
–	–	k?	–
Adrien	Adriena	k1gFnPc2	Adriena
Lucet	Lucet	k1gMnSc1	Lucet
</s>
</p>
<p>
<s>
1894	[number]	k4	1894
<g/>
:	:	kIx,	:
Clostridium	Clostridium	k1gNnSc1	Clostridium
novyi	novyi	k1gNnSc2	novyi
–	–	k?	–
Frederick	Fredericka	k1gFnPc2	Fredericka
G.	G.	kA	G.
Novy	nova	k1gFnSc2	nova
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
<g/>
:	:	kIx,	:
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
bovis	bovis	k1gFnSc2	bovis
–	–	k?	–
Theobald	Theobald	k1gMnSc1	Theobald
Smith	Smith	k1gMnSc1	Smith
<g/>
;	;	kIx,	;
Clostridium	Clostridium	k1gNnSc1	Clostridium
botulinum	botulinum	k1gInSc1	botulinum
–	–	k?	–
Emile	Emil	k1gMnSc5	Emil
van	vana	k1gFnPc2	vana
Ermengem	Ermeng	k1gInSc7	Ermeng
</s>
</p>
<p>
<s>
1897	[number]	k4	1897
<g/>
:	:	kIx,	:
Propionibacterium	Propionibacterium	k1gNnSc1	Propionibacterium
acnes	acnesa	k1gFnPc2	acnesa
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Corynebacterium	Corynebacterium	k1gNnSc1	Corynebacterium
acnes	acnesa	k1gFnPc2	acnesa
<g/>
)	)	kIx)	)
–	–	k?	–
Raymond	Raymond	k1gMnSc1	Raymond
Sabouraud	Sabouraud	k1gMnSc1	Sabouraud
</s>
</p>
<p>
<s>
1898	[number]	k4	1898
<g/>
:	:	kIx,	:
Shigella	Shigello	k1gNnSc2	Shigello
dysenteriae	dysenteria	k1gInSc2	dysenteria
–	–	k?	–
Kiyoshi	Kiyoshi	k1gNnSc1	Kiyoshi
Shiga	Shiga	k1gFnSc1	Shiga
<g/>
;	;	kIx,	;
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
–	–	k?	–
E.	E.	kA	E.
Nocard	Nocard	k1gInSc1	Nocard
<g/>
,	,	kIx,	,
Émile	Émile	k1gInSc1	Émile
Roux	Roux	k1gInSc1	Roux
</s>
</p>
<p>
<s>
1900	[number]	k4	1900
<g/>
:	:	kIx,	:
Salmonella	Salmonello	k1gNnSc2	Salmonello
paratyphi	paratyph	k1gFnSc2	paratyph
B	B	kA	B
–	–	k?	–
Hugo	Hugo	k1gMnSc1	Hugo
Schottmüller	Schottmüller	k1gMnSc1	Schottmüller
<g/>
;	;	kIx,	;
Shigella	Shigella	k1gMnSc1	Shigella
flexneri	flexner	k1gFnSc2	flexner
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
paradysenteriae	paradysenteriae	k1gNnPc2	paradysenteriae
B	B	kA	B
<g/>
)	)	kIx)	)
–	–	k?	–
Simon	Simon	k1gMnSc1	Simon
Flexner	Flexner	k1gMnSc1	Flexner
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Pearson	Pearson	k1gMnSc1	Pearson
Strong	Strong	k1gMnSc1	Strong
<g/>
;	;	kIx,	;
Shigella	Shigella	k1gMnSc1	Shigella
boydii	boydie	k1gFnSc4	boydie
–	–	k?	–
Mark	Mark	k1gMnSc1	Mark
Frederick	Frederick	k1gMnSc1	Frederick
Boyd	Boyd	k1gMnSc1	Boyd
</s>
</p>
<p>
<s>
1903	[number]	k4	1903
<g/>
:	:	kIx,	:
Enterococcus	Enterococcus	k1gInSc1	Enterococcus
faecalis	faecalis	k1gFnSc2	faecalis
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
f.	f.	k?	f.
<g/>
)	)	kIx)	)
–	–	k?	–
Theodor	Theodor	k1gMnSc1	Theodor
Escherich	Escherich	k1gMnSc1	Escherich
</s>
</p>
<p>
<s>
1905	[number]	k4	1905
<g/>
:	:	kIx,	:
Treponema	Treponema	k1gFnSc1	Treponema
pallidum	pallidum	k1gNnSc1	pallidum
–	–	k?	–
Fritz	Fritz	k1gMnSc1	Fritz
Schaudinn	Schaudinn	k1gMnSc1	Schaudinn
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
;	;	kIx,	;
Treponema	Treponema	k1gFnSc1	Treponema
pertenue	pertenu	k1gFnSc2	pertenu
–	–	k?	–
Aldo	Aldo	k1gMnSc1	Aldo
Castellani	Castellaň	k1gFnSc3	Castellaň
</s>
</p>
<p>
<s>
1907	[number]	k4	1907
<g/>
:	:	kIx,	:
Shigella	Shigello	k1gNnSc2	Shigello
sonnei	sonne	k1gFnSc2	sonne
D	D	kA	D
–	–	k?	–
Walter	Walter	k1gMnSc1	Walter
Kruse	Kruse	k1gFnSc2	Kruse
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Olaf	Olaf	k1gMnSc1	Olaf
Sonne	Sonn	k1gInSc5	Sonn
<g/>
;	;	kIx,	;
Chlamydia	Chlamydium	k1gNnSc2	Chlamydium
trachomatis	trachomatis	k1gFnSc2	trachomatis
–	–	k?	–
Ludwig	Ludwig	k1gMnSc1	Ludwig
Halberstaedter	Halberstaedter	k1gMnSc1	Halberstaedter
a	a	k8xC	a
Stanislaus	Stanislaus	k1gInSc1	Stanislaus
von	von	k1gInSc1	von
Prowazek	Prowazek	k1gInSc4	Prowazek
</s>
</p>
<p>
<s>
1910	[number]	k4	1910
<g/>
:	:	kIx,	:
Micrococcus	Micrococcus	k1gMnSc1	Micrococcus
denitrificans	denitrificansa	k1gFnPc2	denitrificansa
–	–	k?	–
Martinus	Martinus	k1gMnSc1	Martinus
Beijerinck	Beijerinck	k1gMnSc1	Beijerinck
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
Helicobacter	Helicobacter	k1gInSc1	Helicobacter
pylori	pylor	k1gFnSc2	pylor
–	–	k?	–
Barry	Barra	k1gFnSc2	Barra
Marshall	Marshall	k1gMnSc1	Marshall
a	a	k8xC	a
John	John	k1gMnSc1	John
Robin	Robina	k1gFnPc2	Robina
Warren	Warrna	k1gFnPc2	Warrna
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Bakteriologia	Bakteriologium	k1gNnSc2	Bakteriologium
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Bakteriologie	bakteriologie	k1gFnSc1	bakteriologie
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
</s>
</p>
<p>
<s>
Antibiotikum	antibiotikum	k1gNnSc1	antibiotikum
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Bacteriology	Bacteriolog	k1gMnPc7	Bacteriolog
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
http://www.bacteriamuseum.org/niches/wabacteria/bacteriology.shtml	[url]	k1gInSc1	http://www.bacteriamuseum.org/niches/wabacteria/bacteriology.shtml
</s>
</p>
