<s>
Stokesova	Stokesův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
</s>
<s>
Stokesova	Stokesův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
je	být	k5eAaImIp3nS
věta	věta	k1gFnSc1
diferenciální	diferenciální	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dává	dávat	k5eAaImIp3nS
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
křivkový	křivkový	k2eAgInSc1d1
integrál	integrál	k1gInSc1
vektorového	vektorový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
přes	přes	k7c4
jednoduchou	jednoduchý	k2eAgFnSc4d1
uzavřenou	uzavřený	k2eAgFnSc4d1
křivku	křivka	k1gFnSc4
a	a	k8xC
plošný	plošný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
z	z	k7c2
rotace	rotace	k1gFnSc2
daného	daný	k2eAgNnSc2d1
vektorového	vektorový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
přes	přes	k7c4
plochu	plocha	k1gFnSc4
křivkou	křivka	k1gFnSc7
uzavřenou	uzavřený	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
věta	věta	k1gFnSc1
je	být	k5eAaImIp3nS
speciálním	speciální	k2eAgInSc7d1
případem	případ	k1gInSc7
tzv.	tzv.	kA
zobecněné	zobecněný	k2eAgFnSc2d1
Stokesovy	Stokesův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
speciálním	speciální	k2eAgInSc7d1
případem	případ	k1gInSc7
Stokesovy	Stokesův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
v	v	k7c6
rovině	rovina	k1gFnSc6
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Greenova	Greenův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
Stokesovy	Stokesův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
je	být	k5eAaImIp3nS
irský	irský	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
Georg	Georg	k1gMnSc1
Stokes	Stokes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Znění	znění	k1gNnSc1
věty	věta	k1gFnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
A	A	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
hladké	hladký	k2eAgNnSc1d1
vektorové	vektorový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
Σ	Σ	k?
libovolná	libovolný	k2eAgFnSc1d1
jednoduše	jednoduše	k6eAd1
souvislá	souvislý	k2eAgFnSc1d1
hladká	hladký	k2eAgFnSc1d1
neprotínající	protínající	k2eNgFnSc1d1
se	se	k3xPyFc4
plocha	plocha	k1gFnSc1
a	a	k8xC
γ	γ	k?
jednoduchá	jednoduchý	k2eAgFnSc1d1
uzavřená	uzavřený	k2eAgFnSc1d1
hladká	hladký	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
ohraničující	ohraničující	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
Σ	Σ	k?
(	(	kIx(
<g/>
tedy	tedy	k9
γ	γ	k?
=	=	kIx~
∂	∂	k?
<g/>
Σ	Σ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pak	pak	k6eAd1
platí	platit	k5eAaImIp3nS
</s>
<s>
∮	∮	k?
</s>
<s>
γ	γ	k?
</s>
<s>
A	a	k9
</s>
<s>
⋅	⋅	k?
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
Σ	Σ	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∇	∇	k?
</s>
<s>
×	×	k?
</s>
<s>
A	a	k9
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
d	d	k?
</s>
<s>
S	s	k7c7
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
oint	oint	k5eAaPmF
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
=	=	kIx~
<g/>
\	\	kIx~
<g/>
int	int	k?
_	_	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
\	\	kIx~
<g/>
Sigma	sigma	k1gNnSc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
nabla	nabla	k1gMnSc1
\	\	kIx~
<g/>
times	times	k1gMnSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}	}	kIx)
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
∇	∇	k?
×	×	k?
A	a	k9
je	být	k5eAaImIp3nS
rotace	rotace	k1gFnSc1
vektorového	vektorový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
A	A	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
vyjádřená	vyjádřený	k2eAgFnSc1d1
pomocí	pomoc	k1gFnSc7
operátoru	operátor	k1gInSc2
nabla	nabla	k1gMnSc1
∇	∇	k?
a	a	k8xC
křivka	křivka	k1gFnSc1
γ	γ	k?
je	být	k5eAaImIp3nS
orientována	orientovat	k5eAaBmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
obíhání	obíhání	k1gNnSc6
po	po	k7c6
této	tento	k3xDgFnSc6
křivce	křivka	k1gFnSc6
v	v	k7c6
kladném	kladný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
plocha	plocha	k1gFnSc1
Σ	Σ	k?
po	po	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Integrální	integrální	k2eAgFnSc2d1
věty	věta	k1gFnSc2
vektorového	vektorový	k2eAgInSc2d1
počtu	počet	k1gInSc2
</s>
<s>
Greenova	Greenův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
Gaussova	Gaussův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
Stokesova	Stokesův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
Zobecněná	zobecněný	k2eAgFnSc1d1
Stokesova	Stokesův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
</s>
