<s>
Stokesova	Stokesův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
je	být	k5eAaImIp3nS
věta	věta	k1gFnSc1
diferenciální	diferenciální	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dává	dávat	k5eAaImIp3nS
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
křivkový	křivkový	k2eAgInSc1d1
integrál	integrál	k1gInSc1
vektorového	vektorový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
přes	přes	k7c4
jednoduchou	jednoduchý	k2eAgFnSc4d1
uzavřenou	uzavřený	k2eAgFnSc4d1
křivku	křivka	k1gFnSc4
a	a	k8xC
plošný	plošný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
z	z	k7c2
rotace	rotace	k1gFnSc2
daného	daný	k2eAgNnSc2d1
vektorového	vektorový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
přes	přes	k7c4
plochu	plocha	k1gFnSc4
křivkou	křivka	k1gFnSc7
uzavřenou	uzavřený	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>