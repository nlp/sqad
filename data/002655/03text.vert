<s>
Konzul	konzul	k1gMnSc1	konzul
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
cos	cos	kA	cos
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
consul	consul	k1gInSc1	consul
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
consules	consules	k1gInSc1	consules
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
consulere	consuler	k1gInSc5	consuler
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
radit	radit	k5eAaImF	radit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
rozvažovat	rozvažovat	k5eAaImF	rozvažovat
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
civilní	civilní	k2eAgMnSc1d1	civilní
a	a	k8xC	a
vojenský	vojenský	k2eAgMnSc1d1	vojenský
úředník	úředník	k1gMnSc1	úředník
(	(	kIx(	(
<g/>
magistratus	magistratus	k1gMnSc1	magistratus
<g/>
)	)	kIx)	)
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Konzulát	konzulát	k1gInSc1	konzulát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
lze	lze	k6eAd1	lze
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
důležitosti	důležitost	k1gFnSc2	důležitost
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
funkci	funkce	k1gFnSc3	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zastávali	zastávat	k5eAaImAgMnP	zastávat
dva	dva	k4xCgMnPc1	dva
politici	politik	k1gMnPc1	politik
každoročně	každoročně	k6eAd1	každoročně
volení	volený	k2eAgMnPc1d1	volený
římským	římský	k2eAgMnSc7d1	římský
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nastolení	nastolení	k1gNnSc6	nastolení
císařství	císařství	k1gNnSc2	císařství
však	však	k9	však
pozbyl	pozbýt	k5eAaPmAgMnS	pozbýt
své	svůj	k3xOyFgFnPc4	svůj
kompetence	kompetence	k1gFnPc4	kompetence
a	a	k8xC	a
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
stala	stát	k5eAaPmAgFnS	stát
pouze	pouze	k6eAd1	pouze
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
navozující	navozující	k2eAgInSc1d1	navozující
klamný	klamný	k2eAgInSc1d1	klamný
dojem	dojem	k1gInSc1	dojem
pokračující	pokračující	k2eAgFnSc2d1	pokračující
existence	existence	k1gFnSc2	existence
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
republikánského	republikánský	k2eAgNnSc2d1	republikánské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
legendárním	legendární	k2eAgMnSc6d1	legendární
svržení	svržení	k1gNnSc3	svržení
posledního	poslední	k2eAgMnSc2d1	poslední
etruského	etruský	k2eAgMnSc2d1	etruský
krále	král	k1gMnSc2	král
Tarquinia	Tarquinium	k1gNnSc2	Tarquinium
Superba	Superb	k1gMnSc2	Superb
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
římského	římský	k2eAgNnSc2d1	římské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
všechna	všechen	k3xTgFnSc1	všechen
zákonná	zákonný	k2eAgFnSc1d1	zákonná
moc	moc	k1gFnSc1	moc
krále	král	k1gMnSc2	král
svěřena	svěřit	k5eAaPmNgFnS	svěřit
nově	nově	k6eAd1	nově
vytvořenému	vytvořený	k2eAgInSc3d1	vytvořený
úřadu	úřad	k1gInSc3	úřad
-	-	kIx~	-
konzulátu	konzulát	k1gInSc2	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
konzulové	konzul	k1gMnPc1	konzul
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
nejprve	nejprve	k6eAd1	nejprve
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
praetoři	praetor	k1gMnPc1	praetor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
kráčí	kráčet	k5eAaImIp3nS	kráčet
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
povinnost	povinnost	k1gFnSc4	povinnost
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
velitelé	velitel	k1gMnPc1	velitel
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označený	k2eAgMnPc1d1	označený
konzulové	konzul	k1gMnPc1	konzul
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
zřejmě	zřejmě	k6eAd1	zřejmě
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Římané	Říman	k1gMnPc1	Říman
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznik	vznik	k1gInSc1	vznik
konzulátu	konzulát	k1gInSc2	konzulát
se	se	k3xPyFc4	se
datoval	datovat	k5eAaImAgMnS	datovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
založení	založení	k1gNnSc2	založení
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
509	[number]	k4	509
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
posloupnost	posloupnost	k1gFnSc4	posloupnost
konzulů	konzul	k1gMnPc2	konzul
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
jisté	jistý	k2eAgFnPc4d1	jistá
mezery	mezera	k1gFnPc4	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Konzulům	konzul	k1gMnPc3	konzul
náležely	náležet	k5eAaImAgInP	náležet
široké	široký	k2eAgFnPc1d1	široká
pravomoci	pravomoc	k1gFnPc4	pravomoc
v	v	k7c6	v
časech	čas	k1gInPc6	čas
míru	míra	k1gFnSc4	míra
(	(	kIx(	(
<g/>
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
exekutivy	exekutiva	k1gFnSc2	exekutiva
<g/>
,	,	kIx,	,
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
a	a	k8xC	a
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uchazeči	uchazeč	k1gMnPc1	uchazeč
o	o	k7c4	o
úřad	úřad	k1gInSc4	úřad
museli	muset	k5eAaImAgMnP	muset
splnit	splnit	k5eAaPmF	splnit
věkový	věkový	k2eAgInSc4d1	věkový
limit	limit	k1gInSc4	limit
pro	pro	k7c4	pro
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
činil	činit	k5eAaImAgInS	činit
41	[number]	k4	41
let	léto	k1gNnPc2	léto
u	u	k7c2	u
patricijů	patricij	k1gMnPc2	patricij
a	a	k8xC	a
42	[number]	k4	42
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
plebeje	plebej	k1gMnPc4	plebej
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
(	(	kIx(	(
<g/>
princip	princip	k1gInSc1	princip
annuity	annuita	k1gFnSc2	annuita
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
voleni	volit	k5eAaImNgMnP	volit
dva	dva	k4xCgMnPc1	dva
konzulové	konzul	k1gMnPc1	konzul
(	(	kIx(	(
<g/>
princip	princip	k1gInSc1	princip
kolegiality	kolegialita	k1gFnSc2	kolegialita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
vůči	vůči	k7c3	vůči
jakémukoliv	jakýkoliv	k3yIgNnSc3	jakýkoliv
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
svého	svůj	k3xOyFgMnSc2	svůj
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
oprávnění	oprávnění	k1gNnSc1	oprávnění
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
využíváno	využívat	k5eAaPmNgNnS	využívat
jen	jen	k6eAd1	jen
velice	velice	k6eAd1	velice
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
dokladů	doklad	k1gInPc2	doklad
svědčících	svědčící	k2eAgInPc2d1	svědčící
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
konzulové	konzul	k1gMnPc1	konzul
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
stanoveného	stanovený	k2eAgInSc2d1	stanovený
minimálního	minimální	k2eAgInSc2d1	minimální
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
konzulů	konzul	k1gMnPc2	konzul
příslušela	příslušet	k5eAaImAgFnS	příslušet
setninovému	setninový	k2eAgNnSc3d1	setninový
shromáždění	shromáždění	k1gNnSc3	shromáždění
(	(	kIx(	(
<g/>
comitia	comitia	k1gFnSc1	comitia
centuriata	centuriata	k1gFnSc1	centuriata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
struktura	struktura	k1gFnSc1	struktura
ale	ale	k9	ale
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
aristokratům	aristokrat	k1gMnPc3	aristokrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potvrzení	potvrzení	k1gNnSc6	potvrzení
svého	svůj	k3xOyFgMnSc4	svůj
zvolení	zvolený	k2eAgMnPc1d1	zvolený
konzulové	konzul	k1gMnPc1	konzul
formálně	formálně	k6eAd1	formálně
převzali	převzít	k5eAaPmAgMnP	převzít
moc	moc	k6eAd1	moc
od	od	k7c2	od
staršího	starý	k2eAgNnSc2d2	starší
kurijního	kurijní	k2eAgNnSc2d1	kurijní
shromáždění	shromáždění	k1gNnSc2	shromáždění
(	(	kIx(	(
<g/>
comitia	comitia	k1gFnSc1	comitia
curiata	curiata	k1gFnSc1	curiata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
udělovalo	udělovat	k5eAaImAgNnS	udělovat
konzulům	konzul	k1gMnPc3	konzul
jejich	jejich	k3xOp3gNnPc4	jejich
imperium	imperium	k1gNnSc4	imperium
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dělo	dít	k5eAaImAgNnS	dít
přijetím	přijetí	k1gNnSc7	přijetí
zákona	zákon	k1gInSc2	zákon
označovaného	označovaný	k2eAgInSc2d1	označovaný
"	"	kIx"	"
<g/>
lex	lex	k?	lex
curiata	curiata	k1gFnSc1	curiata
de	de	k?	de
imperio	imperio	k1gNnSc1	imperio
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
během	během	k7c2	během
svého	své	k1gNnSc2	své
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
nebylo	být	k5eNaImAgNnS	být
nijak	nijak	k6eAd1	nijak
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
konzulové	konzul	k1gMnPc1	konzul
leckdy	leckdy	k6eAd1	leckdy
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
předních	přední	k2eAgFnPc6d1	přední
řadách	řada	k1gFnPc6	řada
vojska	vojsko	k1gNnSc2	vojsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
náhradník	náhradník	k1gMnSc1	náhradník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
consul	consout	k5eAaPmAgMnS	consout
suffectus	suffectus	k1gMnSc1	suffectus
<g/>
.	.	kIx.	.
</s>
<s>
Řádný	řádný	k2eAgMnSc1d1	řádný
konzul	konzul	k1gMnSc1	konzul
(	(	kIx(	(
<g/>
consul	consout	k5eAaPmAgMnS	consout
ordinarius	ordinarius	k1gMnSc1	ordinarius
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
153	[number]	k4	153
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ujímal	ujímat	k5eAaImAgMnS	ujímat
úřadu	úřad	k1gInSc2	úřad
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
náležela	náležet	k5eAaImAgFnS	náležet
mu	on	k3xPp3gMnSc3	on
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
jeho	on	k3xPp3gMnSc2	on
kolegy	kolega	k1gMnSc2	kolega
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
pojmenování	pojmenování	k1gNnSc6	pojmenování
příslušného	příslušný	k2eAgInSc2d1	příslušný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Konzulát	konzulát	k1gInSc1	konzulát
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
patricijům	patricij	k1gMnPc3	patricij
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
367	[number]	k4	367
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
získali	získat	k5eAaPmAgMnP	získat
taktéž	taktéž	k?	taktéž
plebejové	plebej	k1gMnPc1	plebej
právo	právo	k1gNnSc4	právo
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
úřad	úřad	k1gInSc4	úřad
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
zákonem	zákon	k1gInSc7	zákon
Lex	Lex	k1gFnSc2	Lex
Licinia	Licinium	k1gNnSc2	Licinium
Sextia	Sextius	k1gMnSc2	Sextius
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
plebej	plebej	k1gMnSc1	plebej
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
plebejským	plebejský	k2eAgMnSc7d1	plebejský
konzulem	konzul	k1gMnSc7	konzul
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
již	již	k6eAd1	již
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
Lucius	Lucius	k1gMnSc1	Lucius
Sextius	Sextius	k1gMnSc1	Sextius
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
moderní	moderní	k2eAgMnPc1d1	moderní
historikové	historik	k1gMnPc1	historik
však	však	k9	však
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
líčení	líčení	k1gNnSc4	líčení
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
boje	boj	k1gInSc2	boj
za	za	k7c4	za
zrovnoprávnění	zrovnoprávnění	k1gNnSc4	zrovnoprávnění
plebejů	plebej	k1gMnPc2	plebej
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
rané	raný	k2eAgFnSc2d1	raná
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
zřejmě	zřejmě	k6eAd1	zřejmě
okolo	okolo	k7c2	okolo
třiceti	třicet	k4xCc2	třicet
procent	procento	k1gNnPc2	procento
konzulů	konzul	k1gMnPc2	konzul
před	před	k7c7	před
Sextiem	Sextium	k1gNnSc7	Sextium
mělo	mít	k5eAaImAgNnS	mít
plebejská	plebejský	k2eAgNnPc4d1	plebejské
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
patricijská	patricijský	k2eAgNnPc4d1	patricijské
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
zkreslené	zkreslený	k2eAgFnPc4d1	zkreslená
chronologie	chronologie	k1gFnPc4	chronologie
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
ale	ale	k9	ale
naskýtá	naskýtat	k5eAaImIp3nS	naskýtat
ještě	ještě	k6eAd1	ještě
jiné	jiný	k2eAgNnSc4d1	jiné
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
si	se	k3xPyFc3	se
patricijské	patricijský	k2eAgFnPc4d1	patricijská
elity	elita	k1gFnPc4	elita
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
úřadu	úřad	k1gInSc3	úřad
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
společenských	společenský	k2eAgInPc2d1	společenský
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
postupně	postupně	k6eAd1	postupně
uzurpovaly	uzurpovat	k5eAaBmAgFnP	uzurpovat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
vojenského	vojenský	k2eAgNnSc2d1	vojenské
ohrožení	ohrožení	k1gNnSc2	ohrožení
byla	být	k5eAaImAgFnS	být
primárním	primární	k2eAgNnSc7d1	primární
kritériem	kritérion	k1gNnSc7	kritérion
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
konzulů	konzul	k1gMnPc2	konzul
především	především	k6eAd1	především
vojenská	vojenský	k2eAgFnSc1d1	vojenská
reputace	reputace	k1gFnSc1	reputace
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc1	zkušenost
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
volba	volba	k1gFnSc1	volba
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
předmětem	předmět	k1gInSc7	předmět
značného	značný	k2eAgInSc2d1	značný
politického	politický	k2eAgInSc2d1	politický
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Konzulát	konzulát	k1gInSc1	konzulát
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgInS	stát
vrcholem	vrchol	k1gInSc7	vrchol
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
(	(	kIx(	(
<g/>
cursus	cursus	k1gInSc1	cursus
honorum	honorum	k1gNnSc1	honorum
<g/>
)	)	kIx)	)
ambiciózních	ambiciózní	k2eAgMnPc2d1	ambiciózní
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nebyl	být	k5eNaImAgMnS	být
nijak	nijak	k6eAd1	nijak
odměňován	odměňovat	k5eAaImNgMnS	odměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nositel	nositel	k1gMnSc1	nositel
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc6	úřad
sám	sám	k3xTgMnSc1	sám
přispěje	přispět	k5eAaPmIp3nS	přispět
částí	část	k1gFnSc7	část
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
státní	státní	k2eAgFnSc3d1	státní
pokladně	pokladna	k1gFnSc3	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgMnPc1d1	někdejší
konzulové	konzul	k1gMnPc1	konzul
<g/>
,	,	kIx,	,
consulares	consulares	k1gMnSc1	consulares
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
skupinu	skupina	k1gFnSc4	skupina
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
členů	člen	k1gMnPc2	člen
římského	římský	k2eAgInSc2d1	římský
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
pozdní	pozdní	k2eAgFnSc2d1	pozdní
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
zastávání	zastávání	k1gNnSc1	zastávání
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
rovnalo	rovnat	k5eAaImAgNnS	rovnat
přijetí	přijetí	k1gNnSc1	přijetí
mezi	mezi	k7c4	mezi
římskou	římský	k2eAgFnSc4d1	římská
nobilitu	nobilita	k1gFnSc4	nobilita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vypuzení	vypuzení	k1gNnSc4	vypuzení
králů	král	k1gMnPc2	král
přešla	přejít	k5eAaPmAgFnS	přejít
funkce	funkce	k1gFnSc1	funkce
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
náboženského	náboženský	k2eAgMnSc2d1	náboženský
činitele	činitel	k1gMnSc2	činitel
státu	stát	k1gInSc2	stát
na	na	k7c4	na
úředníka	úředník	k1gMnSc4	úředník
zvaného	zvaný	k2eAgInSc2d1	zvaný
rex	rex	k?	rex
sacrorum	sacrorum	k1gNnSc1	sacrorum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
posvátných	posvátný	k2eAgMnPc2d1	posvátný
obřadů	obřad	k1gInPc2	obřad
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechna	všechen	k3xTgNnPc1	všechen
králova	králův	k2eAgNnPc1d1	královo
moc	moc	k6eAd1	moc
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
civilní	civilní	k2eAgFnSc6d1	civilní
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc6d1	vojenská
(	(	kIx(	(
<g/>
imperium	imperium	k1gNnSc4	imperium
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
na	na	k7c4	na
konzulát	konzulát	k1gInSc4	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
královládě	královláda	k1gFnSc3	královláda
či	či	k8xC	či
jinému	jiný	k2eAgNnSc3d1	jiné
zneužití	zneužití	k1gNnSc3	zneužití
této	tento	k3xDgFnSc2	tento
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
imperium	imperium	k1gNnSc1	imperium
sdíleno	sdílet	k5eAaImNgNnS	sdílet
dvěma	dva	k4xCgMnPc7	dva
konzuly	konzul	k1gMnPc7	konzul
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
vetovat	vetovat	k5eAaBmF	vetovat
svá	svůj	k3xOyFgNnPc4	svůj
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
byli	být	k5eAaImAgMnP	být
nadáni	nadat	k5eAaPmNgMnP	nadat
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
exekutivní	exekutivní	k2eAgFnSc7d1	exekutivní
mocí	moc	k1gFnSc7	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoci	pravomoc	k1gFnPc1	pravomoc
konzulů	konzul	k1gMnPc2	konzul
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pozvolného	pozvolný	k2eAgInSc2d1	pozvolný
vývoje	vývoj	k1gInSc2	vývoj
římského	římský	k2eAgInSc2d1	římský
republikánského	republikánský	k2eAgInSc2d1	republikánský
systému	systém	k1gInSc2	systém
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
důležitých	důležitý	k2eAgFnPc2d1	důležitá
činností	činnost	k1gFnPc2	činnost
vyjmuta	vyjmut	k2eAgFnSc1d1	vyjmuta
z	z	k7c2	z
kompetence	kompetence	k1gFnSc2	kompetence
konzulů	konzul	k1gMnPc2	konzul
a	a	k8xC	a
svěřena	svěřen	k2eAgFnSc1d1	svěřena
nově	nově	k6eAd1	nově
vzniklým	vzniklý	k2eAgMnPc3d1	vzniklý
úřadům	úřad	k1gInPc3	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
443	[number]	k4	443
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
provádění	provádění	k1gNnSc1	provádění
censu	census	k1gInSc2	census
přeneseno	přenesen	k2eAgNnSc1d1	přeneseno
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
censora	censor	k1gMnSc2	censor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
366	[number]	k4	366
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgFnP	být
konzulům	konzul	k1gMnPc3	konzul
odňaty	odnít	k5eAaPmNgFnP	odnít
jejich	jejich	k3xOp3gFnPc1	jejich
kompetence	kompetence	k1gFnPc1	kompetence
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
soudců	soudce	k1gMnPc2	soudce
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
zaujímali	zaujímat	k5eAaImAgMnP	zaujímat
praetoři	praetor	k1gMnPc1	praetor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
momentu	moment	k1gInSc2	moment
mohli	moct	k5eAaImAgMnP	moct
konzulové	konzul	k1gMnPc1	konzul
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
soudci	soudce	k1gMnPc1	soudce
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mimořádně	mimořádně	k6eAd1	mimořádně
závažných	závažný	k2eAgInPc6d1	závažný
případech	případ	k1gInPc6	případ
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byli	být	k5eAaImAgMnP	být
pověřeni	pověřit	k5eAaPmNgMnP	pověřit
usnesením	usnesení	k1gNnSc7	usnesení
senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
senatus	senatus	k1gInSc1	senatus
consultum	consultum	k1gNnSc1	consultum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
konzulové	konzul	k1gMnPc1	konzul
nacházeli	nacházet	k5eAaImAgMnP	nacházet
vně	vně	k7c2	vně
posvátných	posvátný	k2eAgFnPc2d1	posvátná
hranic	hranice	k1gFnPc2	hranice
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
pomerium	pomerium	k1gNnSc1	pomerium
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
jim	on	k3xPp3gMnPc3	on
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
magistratury	magistratura	k1gFnPc1	magistratura
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
tribunů	tribun	k1gMnPc2	tribun
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
fungování	fungování	k1gNnSc1	fungování
republiky	republika	k1gFnSc2	republika
podléhalo	podléhat	k5eAaImAgNnS	podléhat
dozoru	dozor	k1gInSc2	dozor
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
exekutivy	exekutiva	k1gFnSc2	exekutiva
byli	být	k5eAaImAgMnP	být
odpovědní	odpovědný	k2eAgMnPc1d1	odpovědný
za	za	k7c4	za
realizaci	realizace	k1gFnSc4	realizace
usnesení	usnesení	k1gNnSc2	usnesení
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
zákonů	zákon	k1gInPc2	zákon
schválených	schválený	k2eAgInPc2d1	schválený
v	v	k7c6	v
lidových	lidový	k2eAgNnPc6d1	lidové
shromážděních	shromáždění	k1gNnPc6	shromáždění
(	(	kIx(	(
<g/>
comitia	comitia	k1gFnSc1	comitia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
okolností	okolnost	k1gFnPc2	okolnost
směli	smět	k5eAaImAgMnP	smět
činit	činit	k5eAaImF	činit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
sami	sám	k3xTgMnPc1	sám
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
působili	působit	k5eAaImAgMnP	působit
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnPc1d1	hlavní
diplomaté	diplomat	k1gMnPc1	diplomat
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
vystoupením	vystoupení	k1gNnSc7	vystoupení
v	v	k7c6	v
senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
setkávali	setkávat	k5eAaImAgMnP	setkávat
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
vyslanci	vyslanec	k1gMnPc1	vyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
mohli	moct	k5eAaImAgMnP	moct
vyslance	vyslanec	k1gMnPc4	vyslanec
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
anebo	anebo	k8xC	anebo
v	v	k7c6	v
roli	role	k1gFnSc6	role
zprostředkovatele	zprostředkovatel	k1gMnSc2	zprostředkovatel
řídit	řídit	k5eAaImF	řídit
jednání	jednání	k1gNnSc4	jednání
mezi	mezi	k7c7	mezi
senátem	senát	k1gInSc7	senát
a	a	k8xC	a
cizím	cizí	k2eAgInSc7d1	cizí
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
svolávali	svolávat	k5eAaImAgMnP	svolávat
senát	senát	k1gInSc4	senát
a	a	k8xC	a
předsedali	předsedat	k5eAaImAgMnP	předsedat
schůzím	schůze	k1gFnPc3	schůze
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgFnSc2d1	stejná
povinnosti	povinnost	k1gFnSc2	povinnost
jim	on	k3xPp3gMnPc3	on
příslušely	příslušet	k5eAaImAgInP	příslušet
i	i	k9	i
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
ke	k	k7c3	k
comitia	comitia	k1gFnSc1	comitia
centuriata	centuriat	k1gMnSc2	centuriat
a	a	k8xC	a
comitia	comitius	k1gMnSc2	comitius
curiata	curiat	k1gMnSc2	curiat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidových	lidový	k2eAgNnPc6d1	lidové
shromážděních	shromáždění	k1gNnPc6	shromáždění
měli	mít	k5eAaImAgMnP	mít
konzulové	konzul	k1gMnPc1	konzul
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
řízení	řízení	k1gNnSc2	řízení
hlasování	hlasování	k1gNnSc2	hlasování
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
zde	zde	k6eAd1	zde
předkládali	předkládat	k5eAaImAgMnP	předkládat
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
konzulové	konzul	k1gMnPc1	konzul
pobývali	pobývat	k5eAaImAgMnP	pobývat
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
úkoly	úkol	k1gInPc4	úkol
plnil	plnit	k5eAaImAgInS	plnit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
nich	on	k3xPp3gInPc2	on
praetor	praetor	k1gInSc1	praetor
<g/>
.	.	kIx.	.
</s>
<s>
Konzulům	konzul	k1gMnPc3	konzul
náleželo	náležet	k5eAaImAgNnS	náležet
také	také	k9	také
právo	práv	k2eAgNnSc1d1	právo
předvolat	předvolat	k5eAaPmF	předvolat
občany	občan	k1gMnPc4	občan
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
a	a	k8xC	a
vsadit	vsadit	k5eAaPmF	vsadit
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
jejich	jejich	k3xOp3gNnSc3	jejich
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
dále	daleko	k6eAd2	daleko
prováděli	provádět	k5eAaImAgMnP	provádět
různé	různý	k2eAgInPc4d1	různý
náboženské	náboženský	k2eAgInPc4d1	náboženský
úkony	úkon	k1gInPc4	úkon
(	(	kIx(	(
<g/>
zasvěcení	zasvěcení	k1gNnSc3	zasvěcení
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
konání	konání	k1gNnSc2	konání
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
určitých	určitý	k2eAgInPc2d1	určitý
obřadů	obřad	k1gInPc2	obřad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
formálního	formální	k2eAgNnSc2d1	formální
stvrzení	stvrzení	k1gNnSc2	stvrzení
uskutečněny	uskutečnit	k5eAaPmNgFnP	uskutečnit
pouze	pouze	k6eAd1	pouze
nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
představiteli	představitel	k1gMnPc7	představitel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
auspicia	auspicius	k1gMnSc4	auspicius
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byly	být	k5eAaImAgFnP	být
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
ceremonií	ceremonie	k1gFnSc7	ceremonie
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
vojsko	vojsko	k1gNnSc1	vojsko
vytáhlo	vytáhnout	k5eAaPmAgNnS	vytáhnout
do	do	k7c2	do
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vnější	vnější	k2eAgInPc4d1	vnější
odznaky	odznak	k1gInPc4	odznak
konzulů	konzul	k1gMnPc2	konzul
patřilo	patřit	k5eAaImAgNnS	patřit
právo	právo	k1gNnSc4	právo
sedět	sedět	k5eAaImF	sedět
na	na	k7c6	na
kurulském	kurulský	k2eAgNnSc6d1	kurulský
křesle	křeslo	k1gNnSc6	křeslo
a	a	k8xC	a
toga	tog	k2eAgNnPc4d1	tog
praetexta	praetexto	k1gNnPc4	praetexto
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
konzul	konzul	k1gMnSc1	konzul
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
dvanácti	dvanáct	k4xCc2	dvanáct
liktory	liktor	k1gMnPc4	liktor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ztělesňovali	ztělesňovat	k5eAaImAgMnP	ztělesňovat
důstojnost	důstojnost	k1gFnSc4	důstojnost
jeho	on	k3xPp3gInSc2	on
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
působili	působit	k5eAaImAgMnP	působit
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
tělesní	tělesní	k2eAgMnPc1d1	tělesní
strážci	strážce	k1gMnPc1	strážce
<g/>
.	.	kIx.	.
</s>
<s>
Liktoři	liktor	k1gMnPc1	liktor
nesli	nést	k5eAaImAgMnP	nést
svazky	svazek	k1gInPc4	svazek
prutů	prut	k1gInPc2	prut
(	(	kIx(	(
<g/>
fasces	fasces	k1gInSc1	fasces
<g/>
)	)	kIx)	)
se	s	k7c7	s
sekerami	sekera	k1gFnPc7	sekera
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Pruty	prut	k1gInPc1	prut
symbolizovaly	symbolizovat	k5eAaImAgInP	symbolizovat
oprávnění	oprávnění	k1gNnSc4	oprávnění
trestat	trestat	k5eAaImF	trestat
a	a	k8xC	a
sekery	sekera	k1gFnPc1	sekera
pravomoc	pravomoc	k1gFnSc4	pravomoc
vynášet	vynášet	k5eAaImF	vynášet
rozsudek	rozsudek	k1gInSc4	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
pomeria	pomerium	k1gNnSc2	pomerium
liktoři	liktor	k1gMnPc1	liktor
ovšem	ovšem	k9	ovšem
nesměli	smět	k5eNaImAgMnP	smět
nosit	nosit	k5eAaImF	nosit
sekery	sekera	k1gFnPc4	sekera
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poukazovalo	poukazovat	k5eAaImAgNnS	poukazovat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
římský	římský	k2eAgMnSc1d1	římský
občan	občan	k1gMnSc1	občan
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
popraven	popravit	k5eAaPmNgMnS	popravit
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
lidového	lidový	k2eAgNnSc2d1	lidové
shromáždění	shromáždění	k1gNnSc2	shromáždění
museli	muset	k5eAaImAgMnP	muset
liktoři	liktor	k1gMnPc1	liktor
odložit	odložit	k5eAaPmF	odložit
i	i	k9	i
pruty	prut	k1gInPc4	prut
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
dávalo	dávat	k5eAaImAgNnS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
moc	moc	k1gFnSc1	moc
konzulů	konzul	k1gMnPc2	konzul
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
populus	populus	k1gMnSc1	populus
romanus	romanus	k1gMnSc1	romanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
hradby	hradba	k1gFnPc4	hradba
Říma	Řím	k1gInSc2	Řím
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
konzulové	konzul	k1gMnPc1	konzul
především	především	k6eAd1	především
v	v	k7c6	v
roli	role	k1gFnSc6	role
velitele	velitel	k1gMnSc2	velitel
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
shromáždění	shromáždění	k1gNnSc3	shromáždění
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
usnesení	usnesení	k1gNnSc1	usnesení
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInPc1d1	vlastní
odvody	odvod	k1gInPc1	odvod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
na	na	k7c6	na
Martově	Martův	k2eAgNnSc6d1	Martovo
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
Campus	Campus	k1gMnSc1	Campus
Martius	Martius	k1gMnSc1	Martius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prováděli	provádět	k5eAaImAgMnP	provádět
konzulové	konzul	k1gMnPc1	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
vojska	vojsko	k1gNnSc2	vojsko
museli	muset	k5eAaImAgMnP	muset
všichni	všechen	k3xTgMnPc1	všechen
legionáři	legionář	k1gMnPc1	legionář
složit	složit	k5eAaPmF	složit
konzulům	konzul	k1gMnPc3	konzul
přísahu	přísaha	k1gFnSc4	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
také	také	k9	také
dohlíželi	dohlížet	k5eAaImAgMnP	dohlížet
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
římskými	římský	k2eAgMnPc7d1	římský
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vojenského	vojenský	k2eAgNnSc2d1	vojenské
tažení	tažení	k1gNnSc2	tažení
mohl	moct	k5eAaImAgInS	moct
konzul	konzul	k1gMnSc1	konzul
uložit	uložit	k5eAaPmF	uložit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
trest	trest	k1gInSc4	trest
kterémukoliv	kterýkoliv	k3yIgMnSc3	kterýkoliv
vojákovi	voják	k1gMnSc3	voják
či	či	k8xC	či
důstojníkovi	důstojník	k1gMnSc3	důstojník
<g/>
,	,	kIx,	,
občanovi	občan	k1gMnSc3	občan
i	i	k9	i
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
konzul	konzul	k1gMnSc1	konzul
velel	velet	k5eAaImAgMnS	velet
jednomu	jeden	k4xCgNnSc3	jeden
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
nápomocni	nápomocen	k2eAgMnPc1d1	nápomocen
vojenští	vojenský	k2eAgMnPc1d1	vojenský
tribunové	tribun	k1gMnPc1	tribun
a	a	k8xC	a
quaestor	quaestor	k1gMnSc1	quaestor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spravoval	spravovat	k5eAaImAgMnS	spravovat
pokladnu	pokladna	k1gFnSc4	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Konzulské	konzulský	k2eAgNnSc1d1	konzulské
vojsko	vojsko	k1gNnSc1	vojsko
čítalo	čítat	k5eAaImAgNnS	čítat
obvykle	obvykle	k6eAd1	obvykle
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
dvěma	dva	k4xCgFnPc7	dva
legiemi	legie	k1gFnPc7	legie
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
početnými	početný	k2eAgInPc7d1	početný
sbory	sbor	k1gInPc7	sbor
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgInPc6d1	vyskytující
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
konzulové	konzul	k1gMnPc1	konzul
táhli	táhnout	k5eAaImAgMnP	táhnout
společně	společně	k6eAd1	společně
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
velení	velení	k1gNnSc6	velení
střídali	střídat	k5eAaImAgMnP	střídat
obden	obden	k6eAd1	obden
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
konzulovi	konzul	k1gMnSc3	konzul
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
drtivého	drtivý	k2eAgNnSc2d1	drtivé
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
svými	svůj	k3xOyFgInPc7	svůj
vojáky	voják	k1gMnPc4	voják
pozdraven	pozdraven	k2eAgInSc4d1	pozdraven
jako	jako	k8xC	jako
imperátor	imperátor	k1gMnSc1	imperátor
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
přiznání	přiznání	k1gNnSc4	přiznání
triumfu	triumf	k1gInSc2	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
disponoval	disponovat	k5eAaBmAgMnS	disponovat
konzul	konzul	k1gMnSc1	konzul
neomezenými	omezený	k2eNgFnPc7d1	neomezená
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
svého	svůj	k3xOyFgNnSc2	svůj
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
případné	případný	k2eAgInPc4d1	případný
přečiny	přečin	k1gInPc4	přečin
předvolán	předvolán	k2eAgInSc4d1	předvolán
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
konzulové	konzul	k1gMnPc1	konzul
měli	mít	k5eAaImAgMnP	mít
právo	právo	k1gNnSc4	právo
vetovat	vetovat	k5eAaBmF	vetovat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
svého	svůj	k3xOyFgMnSc2	svůj
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
společného	společný	k2eAgNnSc2d1	společné
velení	velení	k1gNnSc2	velení
pak	pak	k6eAd1	pak
museli	muset	k5eAaImAgMnP	muset
jednat	jednat	k5eAaImF	jednat
jednomyslně	jednomyslně	k6eAd1	jednomyslně
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
rozsudku	rozsudek	k1gInSc3	rozsudek
vyneseném	vynesený	k2eAgInSc6d1	vynesený
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
odvolat	odvolat	k5eAaPmF	odvolat
k	k	k7c3	k
jeho	jeho	k3xOp3gMnSc3	jeho
kolegovi	kolega	k1gMnSc3	kolega
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mohl	moct	k5eAaImAgMnS	moct
inkriminovaný	inkriminovaný	k2eAgInSc4d1	inkriminovaný
rozsudek	rozsudek	k1gInSc4	rozsudek
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
zbytečným	zbytečný	k2eAgInPc3d1	zbytečný
konfliktům	konflikt	k1gInPc3	konflikt
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
měsíc	měsíc	k1gInSc4	měsíc
směl	smět	k5eAaImAgInS	smět
plnit	plnit	k5eAaImF	plnit
své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
neměl	mít	k5eNaImAgMnS	mít
žádnou	žádný	k3yNgFnSc4	žádný
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přímo	přímo	k6eAd1	přímo
nezasahoval	zasahovat	k5eNaImAgMnS	zasahovat
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
svého	svůj	k3xOyFgMnSc2	svůj
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
konzulové	konzul	k1gMnPc1	konzul
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rolích	role	k1gFnPc6	role
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
střídání	střídání	k1gNnSc1	střídání
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
uplynutí	uplynutí	k1gNnSc2	uplynutí
jejich	jejich	k3xOp3gInSc2	jejich
volebního	volební	k2eAgInSc2d1	volební
termínu	termín	k1gInSc2	termín
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
omezením	omezení	k1gNnSc7	omezení
moci	moc	k1gFnSc2	moc
konzulů	konzul	k1gMnPc2	konzul
bylo	být	k5eAaImAgNnS	být
roční	roční	k2eAgNnSc4d1	roční
trvání	trvání	k1gNnSc4	trvání
jejich	jejich	k3xOp3gInSc2	jejich
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
úkoly	úkol	k1gInPc1	úkol
byly	být	k5eAaImAgInP	být
navíc	navíc	k6eAd1	navíc
předběžně	předběžně	k6eAd1	předběžně
projednávány	projednáván	k2eAgInPc1d1	projednáván
senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
konzulové	konzul	k1gMnPc1	konzul
nesměli	smět	k5eNaImAgMnP	smět
opětně	opětně	k6eAd1	opětně
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
konzulát	konzulát	k1gInSc4	konzulát
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
svého	svůj	k3xOyFgNnSc2	svůj
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konzulátu	konzulát	k1gInSc2	konzulát
musela	muset	k5eAaImAgFnS	muset
obyčejně	obyčejně	k6eAd1	obyčejně
následovat	následovat	k5eAaImF	následovat
desetiletá	desetiletý	k2eAgFnSc1d1	desetiletá
prodleva	prodleva	k1gFnSc1	prodleva
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
kandidaturou	kandidatura	k1gFnSc7	kandidatura
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
byl	být	k5eAaImAgInS	být
římský	římský	k2eAgInSc1d1	římský
stát	stát	k1gInSc1	stát
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
nějakým	nějaký	k3yIgNnSc7	nějaký
bezprostředním	bezprostřední	k2eAgNnSc7d1	bezprostřední
vnějším	vnější	k2eAgNnSc7d1	vnější
či	či	k8xC	či
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
konzulové	konzul	k1gMnPc1	konzul
jmenovat	jmenovat	k5eAaBmF	jmenovat
diktátora	diktátor	k1gMnSc4	diktátor
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
diktátor	diktátor	k1gMnSc1	diktátor
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
imperium	imperium	k1gNnSc1	imperium
konzulů	konzul	k1gMnPc2	konzul
pozastaveno	pozastaven	k2eAgNnSc1d1	pozastaveno
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
konzulům	konzul	k1gMnPc3	konzul
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
jejich	jejich	k3xOp3gNnSc2	jejich
ročního	roční	k2eAgNnSc2d1	roční
úřadování	úřadování	k1gNnSc2	úřadování
přidělována	přidělován	k2eAgFnSc1d1	přidělována
provincie	provincie	k1gFnSc1	provincie
<g/>
,	,	kIx,	,
již	jenž	k3xRgFnSc4	jenž
spravovali	spravovat	k5eAaImAgMnP	spravovat
jménem	jméno	k1gNnSc7	jméno
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
lidu	lid	k1gInSc2	lid
římského	římský	k2eAgMnSc2d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Příslušnou	příslušný	k2eAgFnSc4d1	příslušná
provincii	provincie	k1gFnSc4	provincie
si	se	k3xPyFc3	se
konzulové	konzul	k1gMnPc1	konzul
vybírali	vybírat	k5eAaImAgMnP	vybírat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
skončením	skončení	k1gNnSc7	skončení
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
konzulské	konzulský	k2eAgNnSc1d1	konzulské
imperium	imperium	k1gNnSc1	imperium
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
prokonzulské	prokonzulský	k2eAgFnPc4d1	prokonzulský
a	a	k8xC	a
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stali	stát	k5eAaPmAgMnP	stát
prokonzulové	prokonzul	k1gMnPc1	prokonzul
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
Říma	Řím	k1gInSc2	Řím
spravovali	spravovat	k5eAaImAgMnP	spravovat
jednu	jeden	k4xCgFnSc4	jeden
(	(	kIx(	(
<g/>
či	či	k8xC	či
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňování	uplatňování	k1gNnSc1	uplatňování
prokonzulského	prokonzulský	k2eAgNnSc2d1	prokonzulský
imperia	imperium	k1gNnSc2	imperium
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
provincii	provincie	k1gFnSc6	provincie
než	než	k8xS	než
ve	v	k7c4	v
svěřené	svěřený	k2eAgNnSc4d1	svěřené
bylo	být	k5eAaImAgNnS	být
nezákonné	zákonný	k2eNgNnSc1d1	nezákonné
<g/>
.	.	kIx.	.
</s>
<s>
Prokonzul	prokonzul	k1gMnSc1	prokonzul
měl	mít	k5eAaImAgMnS	mít
navíc	navíc	k6eAd1	navíc
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
opustit	opustit	k5eAaPmF	opustit
svoji	svůj	k3xOyFgFnSc4	svůj
provincii	provincie	k1gFnSc4	provincie
před	před	k7c7	před
skončením	skončení	k1gNnSc7	skončení
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
nebo	nebo	k8xC	nebo
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
svého	svůj	k3xOyFgMnSc2	svůj
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
mohl	moct	k5eAaImAgInS	moct
senát	senát	k1gInSc1	senát
udělit	udělit	k5eAaPmF	udělit
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
povolení	povolení	k1gNnPc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Místodržitelé	místodržitel	k1gMnPc1	místodržitel
setrvávali	setrvávat	k5eAaImAgMnP	setrvávat
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
většinou	většinou	k6eAd1	většinou
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nastolení	nastolení	k1gNnSc6	nastolení
principátu	principát	k1gInSc2	principát
Augustus	Augustus	k1gInSc1	Augustus
zcela	zcela	k6eAd1	zcela
zásadně	zásadně	k6eAd1	zásadně
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
charakter	charakter	k1gInSc4	charakter
konzulátu	konzulát	k1gInSc2	konzulát
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
zbavil	zbavit	k5eAaPmAgMnS	zbavit
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gFnPc2	jeho
kompetencí	kompetence	k1gFnPc2	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Konzulové	konzul	k1gMnPc1	konzul
už	už	k6eAd1	už
nebyli	být	k5eNaImAgMnP	být
voleni	volit	k5eAaImNgMnP	volit
lidovým	lidový	k2eAgNnSc7d1	lidové
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
i	i	k9	i
nadále	nadále	k6eAd1	nadále
bylo	být	k5eAaImAgNnS	být
vykonávání	vykonávání	k1gNnSc1	vykonávání
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
značnou	značný	k2eAgFnSc7d1	značná
poctou	pocta	k1gFnSc7	pocta
-	-	kIx~	-
čistě	čistě	k6eAd1	čistě
formálně	formálně	k6eAd1	formálně
představovali	představovat	k5eAaImAgMnP	představovat
konzulové	konzul	k1gMnPc1	konzul
stále	stále	k6eAd1	stále
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
úředníky	úředník	k1gMnPc4	úředník
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
mocí	moc	k1gFnSc7	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
-	-	kIx~	-
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
konzulát	konzulát	k1gInSc4	konzulát
pouhým	pouhý	k2eAgInSc7d1	pouhý
symbolem	symbol	k1gInSc7	symbol
zdánlivě	zdánlivě	k6eAd1	zdánlivě
přetrvávající	přetrvávající	k2eAgFnSc2d1	přetrvávající
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Konzulům	konzul	k1gMnPc3	konzul
náleželo	náležet	k5eAaImAgNnS	náležet
právo	právo	k1gNnSc4	právo
předsedat	předsedat	k5eAaImF	předsedat
schůzím	schůze	k1gFnPc3	schůze
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tuto	tento	k3xDgFnSc4	tento
svoji	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
směli	smět	k5eAaImAgMnP	smět
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Circu	Circum	k1gNnSc6	Circum
Maximu	maxim	k1gInSc2	maxim
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
veřejných	veřejný	k2eAgFnPc2d1	veřejná
slavností	slavnost	k1gFnPc2	slavnost
konaných	konaný	k2eAgFnPc2d1	konaná
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
ovšem	ovšem	k9	ovšem
museli	muset	k5eAaImAgMnP	muset
uhradit	uhradit	k5eAaPmF	uhradit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
mimořádných	mimořádný	k2eAgInPc6d1	mimořádný
případech	případ	k1gInPc6	případ
vedli	vést	k5eAaImAgMnP	vést
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
konzulů	konzul	k1gMnPc2	konzul
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
přinuceno	přinutit	k5eAaPmNgNnS	přinutit
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
umožnili	umožnit	k5eAaPmAgMnP	umožnit
jiným	jiné	k1gNnSc7	jiné
nastoupit	nastoupit	k5eAaPmF	nastoupit
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
jako	jako	k9	jako
suffecti	suffect	k5eAaBmF	suffect
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
museli	muset	k5eAaImAgMnP	muset
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
odstoupit	odstoupit	k5eAaPmF	odstoupit
i	i	k8xC	i
suffecti	suffect	k5eAaBmF	suffect
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
byli	být	k5eAaImAgMnP	být
ustaveni	ustaven	k2eAgMnPc1d1	ustaven
další	další	k2eAgMnPc1d1	další
náhradníci	náhradník	k1gMnPc1	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
190	[number]	k4	190
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Commoda	Commod	k1gMnSc2	Commod
se	se	k3xPyFc4	se
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
rok	rok	k1gInSc4	rok
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Císařové	Císař	k1gMnPc1	Císař
mnohdy	mnohdy	k6eAd1	mnohdy
určili	určit	k5eAaPmAgMnP	určit
za	za	k7c4	za
konzuly	konzul	k1gMnPc4	konzul
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgMnPc4	svůj
chráněnce	chráněnec	k1gMnPc4	chráněnec
anebo	anebo	k8xC	anebo
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
věková	věkový	k2eAgNnPc1d1	věkové
omezení	omezení	k1gNnPc1	omezení
byla	být	k5eAaImAgNnP	být
běžně	běžně	k6eAd1	běžně
ignorována	ignorovat	k5eAaImNgNnP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
císař	císař	k1gMnSc1	císař
Honorius	Honorius	k1gMnSc1	Honorius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
konzulem	konzul	k1gMnSc7	konzul
již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vládci	vládce	k1gMnPc1	vládce
se	se	k3xPyFc4	se
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
neomezovali	omezovat	k5eNaImAgMnP	omezovat
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Caligula	Caligul	k1gMnSc4	Caligul
prý	prý	k9	prý
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
učinit	učinit	k5eAaImF	učinit
konzulem	konzul	k1gMnSc7	konzul
svého	svůj	k3xOyFgMnSc4	svůj
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
definitivním	definitivní	k2eAgNnSc6d1	definitivní
rozdělení	rozdělení	k1gNnSc6	rozdělení
římské	římský	k2eAgFnSc2d1	římská
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
náleželo	náležet	k5eAaImAgNnS	náležet
císařům	císař	k1gMnPc3	císař
obou	dva	k4xCgFnPc2	dva
polovin	polovina	k1gFnPc2	polovina
říše	říš	k1gFnSc2	říš
právo	právo	k1gNnSc1	právo
ustavit	ustavit	k5eAaPmF	ustavit
jednoho	jeden	k4xCgMnSc4	jeden
konzula	konzul	k1gMnSc4	konzul
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
uplatňovalo	uplatňovat	k5eAaImAgNnS	uplatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
konzul	konzul	k1gMnSc1	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Západě	západ	k1gInSc6	západ
zastával	zastávat	k5eAaImAgMnS	zastávat
poslední	poslední	k2eAgMnSc1d1	poslední
konzul	konzul	k1gMnSc1	konzul
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
za	za	k7c2	za
nadvlády	nadvláda	k1gFnSc2	nadvláda
Ostrogótů	Ostrogót	k1gMnPc2	Ostrogót
v	v	k7c6	v
roce	rok	k1gInSc6	rok
534	[number]	k4	534
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východořímské	východořímský	k2eAgFnSc6d1	Východořímská
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
upustilo	upustit	k5eAaPmAgNnS	upustit
od	od	k7c2	od
ustavování	ustavování	k1gNnSc2	ustavování
konzulů	konzul	k1gMnPc2	konzul
po	po	k7c6	po
roce	rok	k1gInSc6	rok
541	[number]	k4	541
<g/>
.	.	kIx.	.
</s>
