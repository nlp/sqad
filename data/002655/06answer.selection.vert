<s>
Konzulát	konzulát	k1gInSc1	konzulát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
lze	lze	k6eAd1	lze
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
důležitosti	důležitost	k1gFnSc2	důležitost
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
funkci	funkce	k1gFnSc3	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zastávali	zastávat	k5eAaImAgMnP	zastávat
dva	dva	k4xCgMnPc1	dva
politici	politik	k1gMnPc1	politik
každoročně	každoročně	k6eAd1	každoročně
volení	volený	k2eAgMnPc1d1	volený
římským	římský	k2eAgMnSc7d1	římský
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
