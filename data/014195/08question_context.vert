<s>
Vlajka	vlajka	k1gFnSc1
Nepálu	Nepál	k1gInSc2
je	být	k5eAaImIp3nS
tvarem	tvar	k1gInSc7
naprosto	naprosto	k6eAd1
výjimečná	výjimečný	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
nestejně	stejně	k6eNd1
vysokých	vysoký	k2eAgInPc2d1
karmínových	karmínový	k2eAgInPc2d1
<g/>
,	,	kIx,
modře	modro	k6eAd1
lemovaných	lemovaný	k2eAgInPc2d1
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
položených	položená	k1gFnPc2
nad	nad	k7c7
sebou	se	k3xPyFc7
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
přes	přes	k7c4
sebe	sebe	k3xPyFc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>