<s>
Skořápka	skořápka	k1gFnSc1	skořápka
je	být	k5eAaImIp3nS	být
vápenitý	vápenitý	k2eAgInSc4d1	vápenitý
obal	obal	k1gInSc4	obal
ptačího	ptačí	k2eAgNnSc2d1	ptačí
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
skořápky	skořápka	k1gFnSc2	skořápka
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
přizpůsobena	přizpůsoben	k2eAgNnPc4d1	přizpůsobeno
prostředí	prostředí	k1gNnPc4	prostředí
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vejce	vejce	k1gNnSc4	vejce
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
maskována	maskován	k2eAgFnSc1d1	maskována
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgNnPc1d1	bílé
vejce	vejce	k1gNnPc1	vejce
proto	proto	k8xC	proto
najdeme	najít	k5eAaPmIp1nP	najít
u	u	k7c2	u
těch	ten	k3xDgInPc2	ten
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
předkové	předek	k1gMnPc1	předek
žili	žít	k5eAaImAgMnP	žít
<g/>
)	)	kIx)	)
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
norách	nora	k1gFnPc6	nora
apod.	apod.	kA	apod.
-	-	kIx~	-
např.	např.	kA	např.
datli	datel	k1gMnPc1	datel
<g/>
,	,	kIx,	,
vlhy	vlha	k1gFnPc1	vlha
<g/>
,	,	kIx,	,
papoušci	papoušek	k1gMnPc1	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
nabývají	nabývat	k5eAaImIp3nP	nabývat
rozličných	rozličný	k2eAgFnPc2d1	rozličná
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
-	-	kIx~	-
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
,	,	kIx,	,
čárky	čárka	k1gFnSc2	čárka
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Například	například	k6eAd1	například
vejce	vejce	k1gNnSc1	vejce
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
tinam	tinama	k1gFnPc2	tinama
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
skořápky	skořápka	k1gFnSc2	skořápka
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
velikostí	velikost	k1gFnSc7	velikost
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skořápka	skořápka	k1gFnSc1	skořápka
byla	být	k5eAaImAgFnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
unesla	unést	k5eAaPmAgFnS	unést
ptáka	pták	k1gMnSc4	pták
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
sedí	sedit	k5eAaImIp3nP	sedit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
dost	dost	k6eAd1	dost
tenká	tenký	k2eAgFnSc1d1	tenká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
líhnoucí	líhnoucí	k2eAgNnSc1d1	líhnoucí
ptáče	ptáče	k1gNnSc1	ptáče
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
z	z	k7c2	z
vejce	vejce	k1gNnSc2	vejce
proklovat	proklovat	k5eAaPmF	proklovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejtlustší	tlustý	k2eAgFnSc4d3	nejtlustší
skořápku	skořápka	k1gFnSc4	skořápka
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
vejce	vejce	k1gNnSc1	vejce
(	(	kIx(	(
<g/>
2	[number]	k4	2
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejtenčí	tenký	k2eAgMnSc1d3	nejtenčí
kolibřík	kolibřík	k1gMnSc1	kolibřík
0,04	[number]	k4	0,04
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snesené	snesený	k2eAgNnSc1d1	snesené
vejce	vejce	k1gNnSc1	vejce
má	mít	k5eAaImIp3nS	mít
ztenčenou	ztenčený	k2eAgFnSc4d1	ztenčená
skořápku	skořápka	k1gFnSc4	skořápka
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yRnSc2	což
hnízdící	hnízdící	k2eAgMnSc1d1	hnízdící
pták	pták	k1gMnSc1	pták
vejce	vejce	k1gNnSc4	vejce
poškodí	poškodit	k5eAaPmIp3nS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
například	například	k6eAd1	například
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Skořápka	skořápka	k1gFnSc1	skořápka
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
zárodek	zárodek	k1gInSc4	zárodek
důležitý	důležitý	k2eAgInSc4d1	důležitý
zdroj	zdroj	k1gInSc4	zdroj
vápníku	vápník	k1gInSc2	vápník
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
zárodku	zárodek	k1gInSc2	zárodek
se	se	k3xPyFc4	se
skořápka	skořápka	k1gFnSc1	skořápka
postupně	postupně	k6eAd1	postupně
ztenčuje	ztenčovat	k5eAaImIp3nS	ztenčovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
líhnutí	líhnutí	k1gNnSc4	líhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
tloušťku	tloušťka	k1gFnSc4	tloušťka
je	být	k5eAaImIp3nS	být
skořápka	skořápka	k1gFnSc1	skořápka
pórovitá	pórovitý	k2eAgFnSc1d1	pórovitá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zárodku	zárodek	k1gInSc6	zárodek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dýchat	dýchat	k5eAaImF	dýchat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zásoba	zásoba	k1gFnSc1	zásoba
vzduchu	vzduch	k1gInSc2	vzduch
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
vzduchová	vzduchový	k2eAgFnSc1d1	vzduchová
komůrka	komůrka	k1gFnSc1	komůrka
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
tupém	tupý	k2eAgInSc6d1	tupý
konci	konec	k1gInSc6	konec
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
objem	objem	k1gInSc1	objem
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
zárodku	zárodek	k1gInSc2	zárodek
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgFnPc7d1	základní
strukturálními	strukturální	k2eAgFnPc7d1	strukturální
součástmi	součást	k1gFnPc7	součást
skořápky	skořápka	k1gFnPc4	skořápka
jsou	být	k5eAaImIp3nP	být
podskořápečné	podskořápečný	k2eAgFnPc4d1	podskořápečný
blány	blána	k1gFnPc4	blána
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
skořápka	skořápka	k1gFnSc1	skořápka
a	a	k8xC	a
kutikula	kutikula	k1gFnSc1	kutikula
<g/>
.	.	kIx.	.
</s>
<s>
Uváděné	uváděný	k2eAgFnPc1d1	uváděná
informace	informace	k1gFnPc1	informace
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
převážně	převážně	k6eAd1	převážně
slepičích	slepičí	k2eAgNnPc2d1	slepičí
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgFnPc6	který
existuje	existovat	k5eAaImIp3nS	existovat
nejvíce	nejvíce	k6eAd1	nejvíce
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Vaječné	vaječný	k2eAgFnPc1d1	vaječná
podskořápečné	podskořápečný	k2eAgFnPc1d1	podskořápečný
blány	blána	k1gFnPc1	blána
obalují	obalovat	k5eAaImIp3nP	obalovat
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nP	chránit
celý	celý	k2eAgInSc4d1	celý
vaječný	vaječný	k2eAgInSc4d1	vaječný
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	s	k7c7	s
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
bílková	bílkový	k2eAgFnSc1d1	Bílková
blána	blána	k1gFnSc1	blána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
k	k	k7c3	k
bílku	bílek	k1gInSc2	bílek
a	a	k8xC	a
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
,	,	kIx,	,
podskořápečná	podskořápečný	k2eAgFnSc1d1	podskořápečný
blána	blána	k1gFnSc1	blána
přiléhající	přiléhající	k2eAgFnSc1d1	přiléhající
ke	k	k7c3	k
skořápce	skořápka	k1gFnSc3	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
blan	blána	k1gFnPc2	blána
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
mucinózní	mucinózní	k2eAgFnSc7d1	mucinózní
a	a	k8xC	a
keratinovou	keratinový	k2eAgFnSc7d1	keratinová
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
blány	blána	k1gFnSc2	blána
je	být	k5eAaImIp3nS	být
20-22	[number]	k4	20-22
μ	μ	k?	μ
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgFnPc1d1	vnější
blány	blána	k1gFnPc1	blána
50	[number]	k4	50
μ	μ	k?	μ
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
70	[number]	k4	70
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
vejce	vejce	k1gNnSc2	vejce
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
nižší	nízký	k2eAgFnSc2d2	nižší
teploty	teplota	k1gFnSc2	teplota
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
vaječný	vaječný	k2eAgInSc4d1	vaječný
obsah	obsah	k1gInSc4	obsah
ochlazením	ochlazení	k1gNnSc7	ochlazení
smrští	smršť	k1gFnPc2	smršť
a	a	k8xC	a
blány	blána	k1gFnSc2	blána
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělí	oddělit	k5eAaPmIp3nP	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
tupém	tupý	k2eAgInSc6d1	tupý
konci	konec	k1gInSc6	konec
vejce	vejce	k1gNnSc1	vejce
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
vzduchová	vzduchový	k2eAgFnSc1d1	vzduchová
komůrka	komůrka	k1gFnSc1	komůrka
<g/>
,	,	kIx,	,
bublina	bublina	k1gFnSc1	bublina
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
výšce	výška	k1gFnSc6	výška
1-2	[number]	k4	1-2
mm	mm	kA	mm
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
13-15	[number]	k4	13-15
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
vzduchové	vzduchový	k2eAgFnSc2d1	vzduchová
komůrky	komůrka	k1gFnSc2	komůrka
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
propustnosti	propustnost	k1gFnSc6	propustnost
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
,	,	kIx,	,
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc6	vlhkost
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uplynula	uplynout	k5eAaPmAgFnS	uplynout
od	od	k7c2	od
snesení	snesení	k1gNnSc2	snesení
vejce	vejce	k1gNnSc2	vejce
(	(	kIx(	(
<g/>
indikuje	indikovat	k5eAaBmIp3nS	indikovat
stáří	stáří	k1gNnSc1	stáří
vejce	vejce	k1gNnSc2	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
skořápka	skořápka	k1gFnSc1	skořápka
představuje	představovat	k5eAaImIp3nS	představovat
pevný	pevný	k2eAgInSc4d1	pevný
ochranný	ochranný	k2eAgInSc4d1	ochranný
obal	obal	k1gInSc4	obal
vaječného	vaječný	k2eAgInSc2d1	vaječný
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
určuje	určovat	k5eAaImIp3nS	určovat
tvar	tvar	k1gInSc4	tvar
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skořápce	skořápka	k1gFnSc6	skořápka
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dvě	dva	k4xCgFnPc1	dva
vrstvy	vrstva	k1gFnPc1	vrstva
-	-	kIx~	-
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
mamilární	mamilární	k2eAgFnSc1d1	mamilární
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
síle	síla	k1gFnSc6	síla
0,11	[number]	k4	0,11
mm	mm	kA	mm
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc1d1	představující
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
tloušťky	tloušťka	k1gFnSc2	tloušťka
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
,	,	kIx,	,
a	a	k8xC	a
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
,	,	kIx,	,
spongiózní	spongiózní	k2eAgFnSc1d1	spongiózní
<g/>
,	,	kIx,	,
houbovitá	houbovitý	k2eAgFnSc1d1	houbovitá
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
síle	síla	k1gFnSc6	síla
0,23	[number]	k4	0,23
mm	mm	kA	mm
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc1d1	představující
asi	asi	k9	asi
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
tloušťky	tloušťka	k1gFnSc2	tloušťka
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vnější	vnější	k2eAgFnSc2d1	vnější
vaječné	vaječný	k2eAgFnSc2d1	vaječná
blány	blána	k1gFnSc2	blána
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
četnými	četný	k2eAgFnPc7d1	četná
kónickými	kónický	k2eAgFnPc7d1	kónická
vypouklinami	vypouklina	k1gFnPc7	vypouklina
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgNnPc2	jenž
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
vlákna	vlákno	k1gNnPc1	vlákno
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Mamily	mamila	k1gFnPc1	mamila
mají	mít	k5eAaImIp3nP	mít
kruhový	kruhový	k2eAgInSc4d1	kruhový
nebo	nebo	k8xC	nebo
oválný	oválný	k2eAgInSc4d1	oválný
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vnější	vnější	k2eAgFnSc3d1	vnější
vrstvě	vrstva	k1gFnSc3	vrstva
skořápky	skořápka	k1gFnSc2	skořápka
se	se	k3xPyFc4	se
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
volné	volný	k2eAgInPc1d1	volný
prostory	prostor	k1gInPc7	prostor
(	(	kIx(	(
<g/>
kanálky	kanálek	k1gInPc7	kanálek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
výměnu	výměna	k1gFnSc4	výměna
plynů	plyn	k1gInPc2	plyn
mezi	mezi	k7c7	mezi
obsahem	obsah	k1gInSc7	obsah
vejce	vejce	k1gNnSc2	vejce
a	a	k8xC	a
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
<g/>
,	,	kIx,	,
jádro	jádro	k1gNnSc1	jádro
mamily	mamila	k1gFnSc2	mamila
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
organické	organický	k2eAgFnPc4d1	organická
povahy	povaha	k1gFnPc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
pevnost	pevnost	k1gFnSc1	pevnost
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
skořápky	skořápka	k1gFnSc2	skořápka
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
krystaly	krystal	k1gInPc1	krystal
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
skořápky	skořápka	k1gFnSc2	skořápka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
hodnot	hodnota	k1gFnPc2	hodnota
0,27	[number]	k4	0,27
<g/>
-	-	kIx~	-
<g/>
0,37	[number]	k4	0,37
mm	mm	kA	mm
(	(	kIx(	(
<g/>
u	u	k7c2	u
perliček	perlička	k1gFnPc2	perlička
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
0,5	[number]	k4	0,5
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
druhovou	druhový	k2eAgFnSc7d1	druhová
a	a	k8xC	a
plemenou	plemený	k2eAgFnSc7d1	plemená
příslušností	příslušnost	k1gFnSc7	příslušnost
<g/>
,	,	kIx,	,
výživou	výživa	k1gFnSc7	výživa
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
minerálními	minerální	k2eAgFnPc7d1	minerální
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
vitamíny	vitamín	k1gInPc7	vitamín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
snášky	snáška	k1gFnSc2	snáška
<g/>
,	,	kIx,	,
délkou	délka	k1gFnSc7	délka
snáškového	snáškový	k2eAgInSc2d1	snáškový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
věkem	věk	k1gInSc7	věk
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1	měrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
skořápky	skořápka	k1gFnSc2	skořápka
je	být	k5eAaImIp3nS	být
1,45	[number]	k4	1,45
<g/>
-	-	kIx~	-
<g/>
2,0	[number]	k4	2,0
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
5-9	[number]	k4	5-9
g.	g.	k?	g.
Propustnost	propustnost	k1gFnSc1	propustnost
skořápky	skořápka	k1gFnSc2	skořápka
je	být	k5eAaImIp3nS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
kanálků	kanálek	k1gInPc2	kanálek
(	(	kIx(	(
<g/>
pórů	pór	k1gInPc2	pór
<g/>
,	,	kIx,	,
průduchů	průduch	k1gInPc2	průduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
procházejí	procházet	k5eAaImIp3nP	procházet
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
houbovitou	houbovitý	k2eAgFnSc4d1	houbovitá
vrstvou	vrstva	k1gFnSc7	vrstva
a	a	k8xC	a
spojují	spojovat	k5eAaImIp3nP	spojovat
volné	volný	k2eAgFnPc4d1	volná
prostory	prostora	k1gFnPc4	prostora
(	(	kIx(	(
<g/>
komůrky	komůrka	k1gFnPc4	komůrka
<g/>
)	)	kIx)	)
mamilární	mamilární	k2eAgFnPc4d1	mamilární
vrstvy	vrstva	k1gFnPc4	vrstva
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Jejichž	jejichž	k3xOyRp3gInSc1	jejichž
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
17.000	[number]	k4	17.000
pórů	pór	k1gInPc2	pór
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
skořápky	skořápka	k1gFnSc2	skořápka
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
6-23	[number]	k4	6-23
μ	μ	k?	μ
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
pak	pak	k6eAd1	pak
15-65	[number]	k4	15-65
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Póry	pór	k1gInPc1	pór
nejsou	být	k5eNaImIp3nP	být
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
pórů	pór	k1gInPc2	pór
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tupém	tupý	k2eAgInSc6d1	tupý
konci	konec	k1gInSc6	konec
vejce	vejce	k1gNnSc1	vejce
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadnější	snadný	k2eAgNnSc4d2	snazší
dýchání	dýchání	k1gNnSc4	dýchání
mláďat	mládě	k1gNnPc2	mládě
před	před	k7c7	před
líhnutím	líhnutí	k1gNnSc7	líhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Sušina	sušina	k1gFnSc1	sušina
skořápky	skořápka	k1gFnSc2	skořápka
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
98-99	[number]	k4	98-99
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kolem	kolem	k7c2	kolem
2-5	[number]	k4	2-5
%	%	kIx~	%
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
glykoproteinový	glykoproteinový	k2eAgInSc1d1	glykoproteinový
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
množství	množství	k1gNnSc1	množství
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
skořápky	skořápka	k1gFnSc2	skořápka
postupně	postupně	k6eAd1	postupně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
95-98	[number]	k4	95-98
%	%	kIx~	%
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
89	[number]	k4	89
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
%	%	kIx~	%
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
,	,	kIx,	,
až	až	k9	až
2	[number]	k4	2
%	%	kIx~	%
uhličitanu	uhličitan	k1gInSc2	uhličitan
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
,	,	kIx,	,
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
a	a	k8xC	a
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvá	čerstvý	k2eAgNnPc1d1	čerstvé
vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
potažena	potáhnout	k5eAaPmNgNnP	potáhnout
solemi	sůl	k1gFnPc7	sůl
draslíku	draslík	k1gInSc2	draslík
a	a	k8xC	a
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
stáří	stář	k1gFnPc2	stář
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
převažuje	převažovat	k5eAaImIp3nS	převažovat
ve	v	k7c6	v
skořápce	skořápka	k1gFnSc6	skořápka
především	především	k9	především
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
množství	množství	k1gNnSc1	množství
ve	v	k7c6	v
skořápce	skořápka	k1gFnSc6	skořápka
slepičího	slepičí	k2eAgNnSc2d1	slepičí
vejce	vejce	k1gNnSc2	vejce
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
2,4	[number]	k4	2,4
g	g	kA	g
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
slepice	slepice	k1gFnSc2	slepice
<g/>
.	.	kIx.	.
</s>
<s>
Skořápka	skořápka	k1gFnSc1	skořápka
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
fosfor	fosfor	k1gInSc1	fosfor
(	(	kIx(	(
<g/>
0,02	[number]	k4	0,02
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
(	(	kIx(	(
<g/>
0,02	[number]	k4	0,02
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
makro-	makro-	k?	makro-
amikroprvků	amikroprvek	k1gInPc2	amikroprvek
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
vaječné	vaječný	k2eAgFnSc2d1	vaječná
skořápky	skořápka	k1gFnSc2	skořápka
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
pigmenty	pigment	k1gInPc7	pigment
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
ovoporfyrinů	ovoporfyrin	k1gInPc2	ovoporfyrin
(	(	kIx(	(
<g/>
blízké	blízký	k2eAgFnPc1d1	blízká
krevnímu	krevní	k2eAgInSc3d1	krevní
hemoglobinu	hemoglobin	k1gInSc3	hemoglobin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pigmenty	pigment	k1gInPc1	pigment
se	se	k3xPyFc4	se
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
z	z	k7c2	z
delta-amino-levulinové	deltaminoevulinový	k2eAgFnSc2d1	delta-amino-levulinový
kyseliny	kyselina	k1gFnSc2	kyselina
ve	v	k7c6	v
sliznici	sliznice	k1gFnSc6	sliznice
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
skořápky	skořápka	k1gFnSc2	skořápka
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
nutriční	nutriční	k2eAgFnSc3d1	nutriční
hodnotě	hodnota	k1gFnSc3	hodnota
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vaječné	vaječný	k2eAgFnSc2d1	vaječná
skořápky	skořápka	k1gFnSc2	skořápka
je	být	k5eAaImIp3nS	být
hlenovitý	hlenovitý	k2eAgInSc4d1	hlenovitý
obal	obal	k1gInSc4	obal
(	(	kIx(	(
<g/>
kutikula	kutikula	k1gFnSc1	kutikula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
kluzkostí	kluzkost	k1gFnSc7	kluzkost
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
snesení	snesení	k1gNnSc1	snesení
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čerstvě	čerstvě	k6eAd1	čerstvě
sneseného	snesený	k2eAgNnSc2d1	snesené
vejce	vejce	k1gNnSc2	vejce
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
kutikula	kutikula	k1gFnSc1	kutikula
setřít	setřít	k5eAaPmF	setřít
<g/>
;	;	kIx,	;
jinak	jinak	k6eAd1	jinak
záhy	záhy	k6eAd1	záhy
zasychá	zasychat	k5eAaImIp3nS	zasychat
a	a	k8xC	a
ucpává	ucpávat	k5eAaImIp3nS	ucpávat
póry	pór	k1gInPc4	pór
ve	v	k7c6	v
vaječné	vaječný	k2eAgFnSc6d1	vaječná
skořápce	skořápka	k1gFnSc6	skořápka
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vysychání	vysychání	k1gNnSc4	vysychání
vaječného	vaječný	k2eAgInSc2d1	vaječný
obsahu	obsah	k1gInSc2	obsah
i	i	k8xC	i
mikrobiální	mikrobiální	k2eAgFnSc4d1	mikrobiální
kontaminaci	kontaminace	k1gFnSc4	kontaminace
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plyny	plyn	k1gInPc4	plyn
je	být	k5eAaImIp3nS	být
propustná	propustný	k2eAgFnSc1d1	propustná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
10	[number]	k4	10
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
peptidy	peptid	k1gInPc1	peptid
s	s	k7c7	s
galaktózou	galaktóza	k1gFnSc7	galaktóza
<g/>
,	,	kIx,	,
manózou	manóza	k1gFnSc7	manóza
a	a	k8xC	a
hexózoaminem	hexózoamin	k1gInSc7	hexózoamin
<g/>
.	.	kIx.	.
</s>
<s>
Kutikula	kutikula	k1gFnSc1	kutikula
přispívá	přispívat	k5eAaImIp3nS	přispívat
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
k	k	k7c3	k
pevnosti	pevnost	k1gFnSc3	pevnost
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
.	.	kIx.	.
</s>
