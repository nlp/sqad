<s>
Pelyněk	pelyněk	k1gInSc1	pelyněk
pravý	pravý	k2eAgInSc1d1	pravý
(	(	kIx(	(
<g/>
Artemisia	Artemisia	k1gFnSc1	Artemisia
absinthium	absinthium	k1gNnSc4	absinthium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
dřevnatějící	dřevnatějící	k2eAgFnSc1d1	dřevnatějící
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
listnatými	listnatý	k2eAgFnPc7d1	listnatá
lodyhami	lodyha	k1gFnPc7	lodyha
<g/>
,	,	kIx,	,
zakončenými	zakončený	k2eAgFnPc7d1	zakončená
volnými	volný	k2eAgFnPc7d1	volná
latami	lata	k1gFnPc7	lata
žlutých	žlutý	k2eAgInPc2d1	žlutý
úborů	úbor	k1gInPc2	úbor
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgNnSc1d1	náležející
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovité	hvězdnicovitý	k2eAgFnSc2d1	hvězdnicovitý
(	(	kIx(	(
<g/>
Asteraceae	Asteracea	k1gFnSc2	Asteracea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mnohačetný	mnohačetný	k2eAgInSc1d1	mnohačetný
oddenek	oddenek	k1gInSc1	oddenek
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
lodyhy	lodyha	k1gFnSc2	lodyha
dřevnatí	dřevnatět	k5eAaImIp3nS	dřevnatět
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgFnPc1d1	světlá
lodyhy	lodyha	k1gFnPc1	lodyha
tvoří	tvořit	k5eAaImIp3nP	tvořit
trsy	trs	k1gInPc4	trs
<g/>
.	.	kIx.	.
</s>
<s>
Peřenodílné	peřenodílný	k2eAgInPc1d1	peřenodílný
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
stříbřito-šedě	stříbřito-šedě	k6eAd1	stříbřito-šedě
plstnaté	plstnatý	k2eAgFnPc1d1	plstnatá
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgInPc1d1	přízemní
listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
řapík	řapík	k1gInSc4	řapík
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
třikrát	třikrát	k6eAd1	třikrát
peřenodílné	peřenodílný	k2eAgFnPc1d1	peřenodílný
<g/>
,	,	kIx,	,
s	s	k7c7	s
úzce	úzko	k6eAd1	úzko
kopinatými	kopinatý	k2eAgInPc7d1	kopinatý
úkrojky	úkrojek	k1gInPc7	úkrojek
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInPc1d1	horní
lodyžní	lodyžní	k2eAgInPc1d1	lodyžní
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
jednoduše	jednoduše	k6eAd1	jednoduše
nebo	nebo	k8xC	nebo
dvakrát	dvakrát	k6eAd1	dvakrát
peřenodílné	peřenodílný	k2eAgNnSc1d1	peřenodílné
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholičnaté	vrcholičnatý	k2eAgInPc1d1	vrcholičnatý
laty	lat	k1gInPc1	lat
tvoří	tvořit	k5eAaImIp3nP	tvořit
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
květní	květní	k2eAgInPc1d1	květní
úbory	úbor	k1gInPc1	úbor
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
trubkovitých	trubkovitý	k2eAgInPc2d1	trubkovitý
<g/>
,	,	kIx,	,
žlutých	žlutý	k2eAgInPc2d1	žlutý
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
vejčité	vejčitý	k2eAgFnPc4d1	vejčitá
nažky	nažka	k1gFnPc4	nažka
<g/>
.	.	kIx.	.
</s>
<s>
Pelyněk	pelyněk	k1gInSc1	pelyněk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zplaňuje	zplaňovat	k5eAaImIp3nS	zplaňovat
a	a	k8xC	a
nacházíme	nacházet	k5eAaImIp1nP	nacházet
ho	on	k3xPp3gInSc4	on
pak	pak	k6eAd1	pak
na	na	k7c6	na
neobhospodařených	obhospodařený	k2eNgFnPc6d1	obhospodařený
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
na	na	k7c6	na
rumištích	rumiště	k1gNnPc6	rumiště
<g/>
,	,	kIx,	,
náspech	násep	k1gInPc6	násep
<g/>
,	,	kIx,	,
smetištích	smetiště	k1gNnPc6	smetiště
a	a	k8xC	a
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
pěstovat	pěstovat	k5eAaImF	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Sběr	sběr	k1gInSc1	sběr
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
nať	nať	k1gFnSc1	nať
<g/>
.	.	kIx.	.
</s>
<s>
Nezdřevnatělá	zdřevnatělý	k2eNgFnSc1d1	nezdřevnatělá
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
seřezává	seřezávat	k5eAaImIp3nS	seřezávat
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
do	do	k7c2	do
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Suší	sušit	k5eAaImIp3nS	sušit
se	se	k3xPyFc4	se
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
nebo	nebo	k8xC	nebo
za	za	k7c2	za
umělého	umělý	k2eAgNnSc2d1	umělé
sušení	sušení	k1gNnSc2	sušení
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
do	do	k7c2	do
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejkvalitnější	kvalitní	k2eAgInSc1d3	nejkvalitnější
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
a	a	k8xC	a
květ	květ	k1gInSc1	květ
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
je	být	k5eAaImIp3nS	být
kořenitého	kořenitý	k2eAgInSc2d1	kořenitý
pachu	pach	k1gInSc2	pach
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hořkou	hořký	k2eAgFnSc4d1	hořká
až	až	k8xS	až
odpornou	odporný	k2eAgFnSc4d1	odporná
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
látkou	látka	k1gFnSc7	látka
v	v	k7c6	v
pelyňku	pelyněk	k1gInSc6	pelyněk
je	být	k5eAaImIp3nS	být
silice	silice	k1gFnSc1	silice
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
jsou	být	k5eAaImIp3nP	být
chamazulény	chamazuléna	k1gFnPc1	chamazuléna
<g/>
,	,	kIx,	,
thujol	thujol	k1gInSc1	thujol
a	a	k8xC	a
thujon	thujon	k1gInSc1	thujon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
dávkách	dávka	k1gFnPc6	dávka
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
,	,	kIx,	,
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
opojné	opojný	k2eAgInPc4d1	opojný
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnPc4	závrať
a	a	k8xC	a
křeče	křeč	k1gFnPc4	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kandiden	kandidna	k1gFnPc2	kandidna
<g/>
,	,	kIx,	,
felandren	felandrna	k1gFnPc2	felandrna
<g/>
,	,	kIx,	,
seskviterpenové	seskviterpenový	k2eAgInPc1d1	seskviterpenový
laktony	lakton	k1gInPc1	lakton
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc1	flavonoid
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
<g/>
,	,	kIx,	,
absinthin	absinthin	k1gInSc1	absinthin
a	a	k8xC	a
anabsinthin	anabsinthin	k1gInSc1	anabsinthin
a	a	k8xC	a
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
obsah	obsah	k1gInSc1	obsah
silice	silice	k1gFnSc2	silice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zralosti	zralost	k1gFnSc2	zralost
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
formou	forma	k1gFnSc7	forma
nálevu	nálev	k1gInSc2	nálev
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
koření	koření	k1gNnSc1	koření
slouží	sloužit	k5eAaImIp3nS	sloužit
čerstvé	čerstvý	k2eAgInPc4d1	čerstvý
nebo	nebo	k8xC	nebo
sušené	sušený	k2eAgInPc4d1	sušený
mladé	mladý	k2eAgInPc4d1	mladý
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
dosud	dosud	k6eAd1	dosud
nezdřevnatělé	zdřevnatělý	k2eNgFnSc6d1	nezdřevnatělá
části	část	k1gFnSc6	část
natě	nať	k1gFnSc2	nať
<g/>
.	.	kIx.	.
</s>
<s>
Pelyněk	pelyněk	k1gInSc1	pelyněk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
mnoha	mnoho	k4c2	mnoho
hořkých	hořký	k2eAgMnPc2d1	hořký
či	či	k8xC	či
nahořklých	nahořklý	k2eAgMnPc2d1	nahořklý
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
(	(	kIx(	(
<g/>
destilát	destilát	k1gInSc1	destilát
Absint	absint	k1gInSc1	absint
a	a	k8xC	a
víno	víno	k1gNnSc1	víno
Vermut	vermut	k1gInSc1	vermut
<g/>
)	)	kIx)	)
i	i	k8xC	i
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
(	(	kIx(	(
<g/>
tonic	tonice	k1gInPc2	tonice
<g/>
)	)	kIx)	)
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pokrmů	pokrm	k1gInPc2	pokrm
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
k	k	k7c3	k
tučným	tučný	k2eAgNnPc3d1	tučné
vařeným	vařený	k2eAgNnPc3d1	vařené
nebo	nebo	k8xC	nebo
pečeným	pečený	k2eAgNnPc3d1	pečené
masům	maso	k1gNnPc3	maso
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
chuť	chuť	k1gFnSc1	chuť
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
zpříjemní	zpříjemnit	k5eAaPmIp3nS	zpříjemnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napomůže	napomoct	k5eAaPmIp3nS	napomoct
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc3	jejich
lepšímu	dobrý	k2eAgNnSc3d2	lepší
strávení	strávení	k1gNnSc3	strávení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
své	svůj	k3xOyFgInPc4	svůj
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
je	být	k5eAaImIp3nS	být
pelyněk	pelyněk	k1gInSc1	pelyněk
užíván	užíván	k2eAgInSc1d1	užíván
také	také	k9	také
ve	v	k7c6	v
farmacii	farmacie	k1gFnSc6	farmacie
jako	jako	k8xS	jako
stomachicum	stomachicum	k1gNnSc4	stomachicum
a	a	k8xC	a
digestivum	digestivum	k1gNnSc4	digestivum
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
znám	znát	k5eAaImIp1nS	znát
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dovídáme	dovídat	k5eAaImIp1nP	dovídat
ze	z	k7c2	z
spisů	spis	k1gInPc2	spis
Dioscorida	Dioscorid	k1gMnSc2	Dioscorid
i	i	k8xC	i
Plinia	Plinium	k1gNnPc1	Plinium
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
při	při	k7c6	při
nechutenství	nechutenství	k1gNnSc6	nechutenství
<g/>
,	,	kIx,	,
střevních	střevní	k2eAgFnPc6d1	střevní
a	a	k8xC	a
žlučníkových	žlučníkový	k2eAgFnPc6d1	žlučníková
kolikách	kolika	k1gFnPc6	kolika
<g/>
,	,	kIx,	,
menstruačních	menstruační	k2eAgFnPc6d1	menstruační
bolestech	bolest	k1gFnPc6	bolest
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
hlístům	hlíst	k1gMnPc3	hlíst
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sekreci	sekrece	k1gFnSc4	sekrece
zažívacích	zažívací	k2eAgFnPc2d1	zažívací
šťáv	šťáva	k1gFnPc2	šťáva
a	a	k8xC	a
tlumí	tlumit	k5eAaImIp3nS	tlumit
křeče	křeč	k1gFnPc4	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Musíme	muset	k5eAaImIp1nP	muset
se	se	k3xPyFc4	se
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
podávání	podávání	k1gNnSc3	podávání
nadměrných	nadměrný	k2eAgFnPc2d1	nadměrná
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Předávkování	předávkování	k1gNnSc1	předávkování
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
poruchami	porucha	k1gFnPc7	porucha
čití	čití	k1gNnSc2	čití
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
bolestí	bolest	k1gFnPc2	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
nutkáním	nutkání	k1gNnSc7	nutkání
na	na	k7c6	na
dávení	dávení	k1gNnSc6	dávení
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
nepříjemnými	příjemný	k2eNgInPc7d1	nepříjemný
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Pelyněk	pelyněk	k1gInSc4	pelyněk
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
užívat	užívat	k5eAaImF	užívat
těhotné	těhotný	k2eAgFnPc1d1	těhotná
a	a	k8xC	a
kojící	kojící	k2eAgFnPc1d1	kojící
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
