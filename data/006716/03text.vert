<s>
Richard	Richard	k1gMnSc1	Richard
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
odvozované	odvozovaný	k2eAgNnSc1d1	odvozované
ze	z	k7c2	z
staroněmeckého	staroněmecký	k2eAgMnSc2d1	staroněmecký
Ríchart	Ríchart	k1gInSc4	Ríchart
(	(	kIx(	(
<g/>
rík	rík	k?	rík
<g/>
/	/	kIx~	/
<g/>
rîh	rîh	k?	rîh
<g/>
=	=	kIx~	=
<g/>
vladař	vladař	k1gMnSc1	vladař
<g/>
/	/	kIx~	/
<g/>
král	král	k1gMnSc1	král
a	a	k8xC	a
hart	hart	k2eAgMnSc1d1	hart
<g/>
=	=	kIx~	=
<g/>
silný	silný	k2eAgInSc1d1	silný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
jmeniny	jmeniny	k1gFnPc4	jmeniny
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
podoba	podoba	k1gFnSc1	podoba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Ríša	Ríša	k1gMnSc1	Ríša
<g/>
,	,	kIx,	,
Riky	Riky	k1gInPc1	Riky
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+2,1	+2,1	k4	+2,1
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ricardo	Ricardo	k1gNnSc1	Ricardo
(	(	kIx(	(
<g/>
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
)	)	kIx)	)
Rikard	Rikard	k1gInSc1	Rikard
(	(	kIx(	(
<g/>
švédština	švédština	k1gFnSc1	švédština
<g/>
,	,	kIx,	,
srbochorvatština	srbochorvatština	k1gFnSc1	srbochorvatština
<g/>
)	)	kIx)	)
Riccardo	Riccardo	k1gNnSc1	Riccardo
(	(	kIx(	(
<g/>
italština	italština	k1gFnSc1	italština
<g/>
)	)	kIx)	)
Ryszard	Ryszard	k1gInSc1	Ryszard
(	(	kIx(	(
<g/>
polština	polština	k1gFnSc1	polština
<g/>
)	)	kIx)	)
Ričard	Ričard	k1gInSc1	Ričard
(	(	kIx(	(
<g/>
ruština	ruština	k1gFnSc1	ruština
<g/>
)	)	kIx)	)
Richárd	Richárd	k1gInSc1	Richárd
(	(	kIx(	(
<g/>
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
)	)	kIx)	)
Ricardus	Ricardus	k1gInSc1	Ricardus
(	(	kIx(	(
<g/>
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
Xing	Xing	k1gMnSc1	Xing
Jian	Jian	k1gMnSc1	Jian
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
čínština	čínština	k1gFnSc1	čínština
<g/>
)	)	kIx)	)
Rik	Rik	k1gMnSc1	Rik
<g/>
,	,	kIx,	,
Rick	Rick	k1gMnSc1	Rick
<g/>
,	,	kIx,	,
Rich	Rich	k1gMnSc1	Rich
<g/>
,	,	kIx,	,
Richie	Richie	k1gFnSc1	Richie
<g/>
,	,	kIx,	,
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
Ric	Ric	k1gMnSc1	Ric
<g/>
,	,	kIx,	,
Chard	Chard	k1gMnSc1	Chard
<g/>
,	,	kIx,	,
Hecka	Hecka	k1gMnSc1	Hecka
<g/>
,	,	kIx,	,
Ringo	Ringo	k1gMnSc1	Ringo
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
Ríša	Ríša	k1gMnSc1	Ríša
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
Rišo	Rišo	k1gNnSc1	Rišo
<g/>
,	,	kIx,	,
Riško	Riško	k1gNnSc1	Riško
<g/>
,	,	kIx,	,
Riki	Riki	k1gNnSc1	Riki
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
)	)	kIx)	)
Rysio	Rysio	k1gNnSc1	Rysio
(	(	kIx(	(
<g/>
polština	polština	k1gFnSc1	polština
<g/>
)	)	kIx)	)
Riku	Rikus	k1gInSc2	Rikus
(	(	kIx(	(
<g/>
finština	finština	k1gFnSc1	finština
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
Anglický	anglický	k2eAgMnSc1d1	anglický
Richard	Richard	k1gMnSc1	Richard
z	z	k7c2	z
Chichesteru	Chichester	k1gInSc2	Chichester
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
biskup	biskup	k1gMnSc1	biskup
Richard	Richard	k1gMnSc1	Richard
z	z	k7c2	z
Pampuri	Pampur	k1gFnSc2	Pampur
Richard	Richard	k1gMnSc1	Richard
I.	I.	kA	I.
Lví	lví	k2eAgNnSc1d1	lví
Srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
z	z	k7c2	z
Cornwallu	Cornwall	k1gInSc2	Cornwall
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
Normandský	normandský	k2eAgMnSc1d1	normandský
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Viléma	Vilém	k1gMnSc4	Vilém
I.	I.	kA	I.
Dobyvatele	dobyvatel	k1gMnSc4	dobyvatel
a	a	k8xC	a
dědic	dědic	k1gMnSc1	dědic
anglického	anglický	k2eAgInSc2d1	anglický
trůnu	trůn	k1gInSc2	trůn
Richard	Richard	k1gMnSc1	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Normandský	normandský	k2eAgMnSc1d1	normandský
<g/>
,	,	kIx,	,
normandský	normandský	k2eAgMnSc1d1	normandský
vévoda	vévoda	k1gMnSc1	vévoda
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Normandský	normandský	k2eAgMnSc1d1	normandský
<g/>
,	,	kIx,	,
normandský	normandský	k2eAgMnSc1d1	normandský
vévoda	vévoda	k1gMnSc1	vévoda
Richard	Richard	k1gMnSc1	Richard
Brautigan	Brautigan	k1gMnSc1	Brautigan
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Richard	Richard	k1gMnSc1	Richard
Burton	Burton	k1gInSc4	Burton
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc4	Dawkins
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
zoolog	zoolog	k1gMnSc1	zoolog
a	a	k8xC	a
evoluční	evoluční	k2eAgMnSc1d1	evoluční
biolog	biolog	k1gMnSc1	biolog
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
Richard	Richard	k1gMnSc1	Richard
Halliburton	Halliburton	k1gInSc4	Halliburton
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
reportér	reportér	k1gMnSc1	reportér
Richard	Richard	k1gMnSc1	Richard
Harris	Harris	k1gFnSc1	Harris
<g/>
,	,	kIx,	,
britsko-irský	britskorský	k2eAgMnSc1d1	britsko-irský
herec	herec	k1gMnSc1	herec
Ryszard	Ryszard	k1gMnSc1	Ryszard
Kukliński	Kuklińske	k1gFnSc4	Kuklińske
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
polský	polský	k2eAgMnSc1d1	polský
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
špion	špion	k1gMnSc1	špion
Richard	Richard	k1gMnSc1	Richard
Smalley	Smallea	k1gFnSc2	Smallea
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
Richard	Richard	k1gMnSc1	Richard
Stallman	Stallman	k1gMnSc1	Stallman
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
programátor	programátor	k1gMnSc1	programátor
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Richard	Richard	k1gMnSc1	Richard
Weiner	Weiner	k1gMnSc1	Weiner
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Richard	Richard	k1gMnSc1	Richard
Genzer	Genzer	k1gMnSc1	Genzer
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tanečník	tanečník	k1gMnSc1	tanečník
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Richard	Richard	k1gMnSc1	Richard
Kruspe	Krusp	k1gInSc5	Krusp
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
skupiny	skupina	k1gFnSc2	skupina
Rammstein	Rammstein	k1gMnSc1	Rammstein
Richard	Richard	k1gMnSc1	Richard
Krajčo	Krajčo	k1gMnSc1	Krajčo
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Kryštof	Kryštof	k1gMnSc1	Kryštof
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
příjmení	příjmení	k1gNnSc4	příjmení
Richard	Richarda	k1gFnPc2	Richarda
v	v	k7c6	v
četnosti	četnost	k1gFnSc6	četnost
výskytu	výskyt	k1gInSc2	výskyt
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
Richard	Richard	k1gMnSc1	Richard
(	(	kIx(	(
<g/>
příjmení	příjmení	k1gNnSc1	příjmení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
Odhalení	odhalení	k1gNnSc2	odhalení
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
továrna	továrna	k1gFnSc1	továrna
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
památka	památka	k1gFnSc1	památka
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
u	u	k7c2	u
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
pod	pod	k7c7	pod
vrchem	vrch	k1gInSc7	vrch
Radobýl	Radobýl	k1gMnSc1	Radobýl
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Richard	Richarda	k1gFnPc2	Richarda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Richard	Richard	k1gMnSc1	Richard
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
