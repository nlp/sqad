<s>
Kotešová	Kotešová	k1gFnSc1
</s>
<s>
Kotešová	Kotešová	k1gFnSc1
Renesanční	renesanční	k2eAgFnSc1d1
kostel	kostel	k1gInSc4
nad	nad	k7c4
obcíPoloha	obcíPoloh	k1gMnSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
57	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
330	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Žilinský	žilinský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Bytča	Bytča	k6eAd1
Tradiční	tradiční	k2eAgInSc1d1
region	region	k1gInSc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Pováží	povážit	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Kotešová	Kotešová	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
20,333	20,333	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
985	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
97,6	97,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Mozolík	Mozolík	k1gMnSc1
(	(	kIx(
<g/>
NEKA	NEKA	kA
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
1234	#num#	k4
<g/>
/	/	kIx~
<g/>
1243	#num#	k4
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.obeckotesova.sk	www.obeckotesova.sk	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
041	#num#	k4
PSČ	PSČ	kA
</s>
<s>
013	#num#	k4
61	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
BY	by	k9
NUTS	NUTS	kA
</s>
<s>
517691	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kotešová	Kotešová	k1gFnSc1
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
v	v	k7c6
okrese	okres	k1gInSc6
Bytča	Bytč	k1gInSc2
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Váh	Váh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1234	#num#	k4
<g/>
/	/	kIx~
<g/>
1243	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
je	být	k5eAaImIp3nS
renesanční	renesanční	k2eAgInSc1d1
kaštel	kaštel	k1gInSc1
a	a	k8xC
renesanční	renesanční	k2eAgInSc1d1
římskokatolický	římskokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Narození	narození	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
330	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
20	#num#	k4
333	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
2015	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
obci	obec	k1gFnSc6
1985	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kotešová	Kotešová	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Registre	registr	k1gInSc5
obnovenej	obnovenej	k?
evidencie	evidencie	k1gFnSc1
pozemkov	pozemkov	k1gInSc1
[	[	kIx(
<g/>
PDF	PDF	kA
835	#num#	k4
kB	kb	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Úrad	úrada	k1gFnPc2
geodézie	geodézie	k1gFnSc2
<g/>
,	,	kIx,
kartografie	kartografie	k1gFnSc2
a	a	k8xC
katastra	katastr	k1gMnSc2
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2014-08-19	2014-08-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kotešová	Kotešová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Kaštel	kaštel	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
slovenskehrady	slovenskehrada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Bytča	Bytčum	k1gNnSc2
</s>
<s>
Bytča	Bytča	k1gFnSc1
•	•	k?
Hlboké	Hlboký	k2eAgFnPc4d1
nad	nad	k7c4
Váhom	Váhom	k1gInSc4
•	•	k?
Hvozdnica	Hvozdnic	k1gInSc2
•	•	k?
Jablonové	Jablonová	k1gFnPc4
•	•	k?
Kolárovice	Kolárovice	k1gFnPc4
•	•	k?
Kotešová	Kotešová	k1gFnSc1
•	•	k?
Maršová-Rašov	Maršová-Rašov	k1gInSc1
•	•	k?
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Predmier	Predmier	k1gMnSc1
•	•	k?
Súľov-Hradná	Súľov-Hradný	k2eAgFnSc1d1
•	•	k?
Štiavnik	Štiavnik	k1gInSc4
•	•	k?
Veľké	Veľký	k2eAgInPc4d1
Rovné	rovný	k2eAgInPc4d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
