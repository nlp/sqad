<s>
Termín	termín	k1gInSc1	termín
hedvábí	hedvábí	k1gNnSc2	hedvábí
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
používal	používat	k5eAaImAgMnS	používat
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
"	"	kIx"	"
<g/>
nekonečně	konečně	k6eNd1	konečně
<g/>
"	"	kIx"	"
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
textilní	textilní	k2eAgFnSc1d1	textilní
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgInPc3	který
patří	patřit	k5eAaImIp3nS	patřit
jak	jak	k8xS	jak
přírodní	přírodní	k2eAgNnSc4d1	přírodní
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
umělé	umělý	k2eAgInPc4d1	umělý
výrobky	výrobek	k1gInPc4	výrobek
podobného	podobný	k2eAgInSc2d1	podobný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
přírodní	přírodní	k2eAgNnSc4d1	přírodní
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1	přírodní
neboli	neboli	k8xC	neboli
pravé	pravý	k2eAgNnSc1d1	pravé
hedvábí	hedvábí	k1gNnSc1	hedvábí
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
výměšky	výměšek	k1gInPc1	výměšek
žláz	žláza	k1gFnPc2	žláza
motýla	motýl	k1gMnSc2	motýl
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
(	(	kIx(	(
<g/>
Bombyx	Bombyx	k1gInSc1	Bombyx
mori	mor	k1gFnSc2	mor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výměšky	výměšek	k1gInPc1	výměšek
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
rychle	rychle	k6eAd1	rychle
tuhnou	tuhnout	k5eAaImIp3nP	tuhnout
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dvojice	dvojice	k1gFnSc1	dvojice
tenkých	tenký	k2eAgFnPc2d1	tenká
vláken	vlákna	k1gFnPc2	vlákna
spojených	spojený	k2eAgFnPc2d1	spojená
sericinem	sericino	k1gNnSc7	sericino
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
klihu	klih	k1gInSc2	klih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
motýl	motýl	k1gMnSc1	motýl
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kuklu	kukla	k1gFnSc4	kukla
<g/>
.	.	kIx.	.
</s>
<s>
Hedvábné	hedvábný	k2eAgNnSc1d1	hedvábné
vlákno	vlákno	k1gNnSc1	vlákno
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
následným	následný	k2eAgNnSc7d1	následné
rozvinutím	rozvinutí	k1gNnSc7	rozvinutí
této	tento	k3xDgFnSc2	tento
kukly	kukla	k1gFnSc2	kukla
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
300	[number]	k4	300
až	až	k9	až
900	[number]	k4	900
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1	přírodní
hedvábí	hedvábí	k1gNnSc1	hedvábí
bylo	být	k5eAaImAgNnS	být
vytlačováno	vytlačovat	k5eAaImNgNnS	vytlačovat
syntetickým	syntetický	k2eAgNnSc7d1	syntetické
hedvábím	hedvábí	k1gNnSc7	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
si	se	k3xPyFc3	se
však	však	k9	však
udržuje	udržovat	k5eAaImIp3nS	udržovat
nezastupitelné	zastupitelný	k2eNgNnSc4d1	nezastupitelné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
oděvním	oděvní	k2eAgInSc6d1	oděvní
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
užitné	užitný	k2eAgFnPc4d1	užitná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
–	–	k?	–
saje	sát	k5eAaImIp3nS	sát
pot	pot	k1gInSc4	pot
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
příjemně	příjemně	k6eAd1	příjemně
chladí	chladit	k5eAaImIp3nP	chladit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
hřeje	hřát	k5eAaImIp3nS	hřát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
hedvábí	hedvábí	k1gNnSc2	hedvábí
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
úzce	úzko	k6eAd1	úzko
spjato	spjat	k2eAgNnSc4d1	spjato
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
dekorování	dekorování	k1gNnSc4	dekorování
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgFnPc2d1	tradiční
dekoračních	dekorační	k2eAgFnPc2d1	dekorační
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
akvarel	akvarel	k1gInSc4	akvarel
a	a	k8xC	a
batika	batika	k1gFnSc1	batika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
tzv.	tzv.	kA	tzv.
Serti	Serti	k1gNnSc4	Serti
technika	technikum	k1gNnSc2	technikum
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnSc4d1	využívající
malbu	malba	k1gFnSc4	malba
na	na	k7c6	na
hedvábí	hedvábí	k1gNnSc6	hedvábí
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
gutty	gutta	k1gFnSc2	gutta
a	a	k8xC	a
kontur	kontura	k1gFnPc2	kontura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
speciální	speciální	k2eAgFnPc4d1	speciální
barvy	barva	k1gFnPc4	barva
na	na	k7c4	na
hedvábí	hedvábí	k1gNnSc4	hedvábí
fixované	fixovaný	k2eAgFnSc2d1	fixovaná
teplem	teplo	k1gNnSc7	teplo
(	(	kIx(	(
<g/>
Super	super	k2eAgInSc1d1	super
Tinfix	Tinfix	k1gInSc1	Tinfix
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
umělá	umělý	k2eAgNnPc1d1	umělé
textilní	textilní	k2eAgNnPc1d1	textilní
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
také	také	k9	také
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
materiály	materiál	k1gInPc1	materiál
(	(	kIx(	(
<g/>
syntetické	syntetický	k2eAgFnPc1d1	syntetická
a	a	k8xC	a
přírodní	přírodní	k2eAgInPc1d1	přírodní
polymery	polymer	k1gInPc1	polymer
<g/>
,	,	kIx,	,
sklo	sklo	k1gNnSc1	sklo
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
nahradily	nahradit	k5eAaPmAgFnP	nahradit
přírodní	přírodní	k2eAgNnSc4d1	přírodní
hedvábí	hedvábí	k1gNnSc4	hedvábí
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
předčí	předčít	k5eAaPmIp3nP	předčít
určitými	určitý	k2eAgFnPc7d1	určitá
specifickými	specifický	k2eAgFnPc7d1	specifická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
izolační	izolační	k2eAgFnSc7d1	izolační
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
specifickou	specifický	k2eAgFnSc7d1	specifická
hmotností	hmotnost	k1gFnSc7	hmotnost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1	přírodní
hedvábí	hedvábí	k1gNnSc1	hedvábí
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
významnou	významný	k2eAgFnSc7d1	významná
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
oděvní	oděvní	k2eAgInPc4d1	oděvní
výrobky	výrobek	k1gInPc4	výrobek
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
exkluzivním	exkluzivní	k2eAgInSc7d1	exkluzivní
omakem	omak	k1gInSc7	omak
<g/>
,	,	kIx,	,
leskem	lesk	k1gInSc7	lesk
a	a	k8xC	a
celkovým	celkový	k2eAgInSc7d1	celkový
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hedvábí	hedvábí	k1gNnSc2	hedvábí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hedvábí	hedvábí	k1gNnSc2	hedvábí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Genom	genom	k1gInSc1	genom
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
</s>
