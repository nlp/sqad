<s>
Slayer	Slayer	k1gInSc1	Slayer
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
thrashmetalová	thrashmetalový	k2eAgFnSc1d1	thrashmetalová
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
působící	působící	k2eAgFnSc1d1	působící
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Kerry	Kerr	k1gMnPc7	Kerr
Kingem	King	k1gMnSc7	King
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
druhého	druhý	k4xOgMnSc2	druhý
kytaristy	kytarista	k1gMnSc2	kytarista
našel	najít	k5eAaPmAgMnS	najít
Jeffa	Jeff	k1gMnSc4	Jeff
Hannemana	Hanneman	k1gMnSc4	Hanneman
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
nalezl	nalézt	k5eAaBmAgInS	nalézt
na	na	k7c4	na
post	post	k1gInSc4	post
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Toma	Tom	k1gMnSc2	Tom
Arayu	Arayus	k1gInSc2	Arayus
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
během	během	k7c2	během
chvíle	chvíle	k1gFnSc2	chvíle
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
v	v	k7c6	v
undergroundové	undergroundový	k2eAgFnSc6d1	undergroundová
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
jako	jako	k8xS	jako
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
,	,	kIx,	,
Anthrax	Anthrax	k1gInSc4	Anthrax
a	a	k8xC	a
Megadeth	Megadeth	k1gInSc4	Megadeth
založila	založit	k5eAaPmAgFnS	založit
thrashmetalovou	thrashmetalový	k2eAgFnSc4d1	thrashmetalová
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
Reign	Reigna	k1gFnPc2	Reigna
in	in	k?	in
Blood	Blooda	k1gFnPc2	Blooda
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
ovšem	ovšem	k9	ovšem
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
určitého	určitý	k2eAgInSc2d1	určitý
zenitu	zenit	k1gInSc2	zenit
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
negativní	negativní	k2eAgFnSc1d1	negativní
reklama	reklama	k1gFnSc1	reklama
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
reklama	reklama	k1gFnSc1	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgFnSc7d1	úvodní
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Death	Death	k1gMnSc1	Death
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anděl	anděl	k1gMnSc1	anděl
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
spor	spor	k1gInSc4	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
nejsou	být	k5eNaImIp3nP	být
náhodou	náhodou	k6eAd1	náhodou
"	"	kIx"	"
<g/>
nazi	naz	k1gInPc7	naz
<g/>
"	"	kIx"	"
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
song	song	k1gInSc1	song
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
praktikách	praktika	k1gFnPc6	praktika
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josefa	Josef	k1gMnSc2	Josef
Mengeleho	Mengele	k1gMnSc2	Mengele
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Jeff	Jeff	k1gInSc1	Jeff
má	mít	k5eAaImIp3nS	mít
doma	doma	k6eAd1	doma
sbírku	sbírka	k1gFnSc4	sbírka
fašistických	fašistický	k2eAgNnPc2d1	fašistické
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
a	a	k8xC	a
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
překážky	překážka	k1gFnPc4	překážka
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
brzy	brzy	k6eAd1	brzy
album	album	k1gNnSc1	album
Reign	Reign	k1gInSc1	Reign
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
500	[number]	k4	500
000	[number]	k4	000
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
první	první	k4xOgFnSc7	první
zlatou	zlatý	k2eAgFnSc7d1	zlatá
deskou	deska	k1gFnSc7	deska
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
kapelu	kapela	k1gFnSc4	kapela
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
opustil	opustit	k5eAaPmAgMnS	opustit
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
bubnoval	bubnovat	k5eAaImAgMnS	bubnovat
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
Toni	Toni	k1gFnSc1	Toni
Scaglione	Scaglion	k1gInSc5	Scaglion
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Whiplash	Whiplasha	k1gFnPc2	Whiplasha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
přišlo	přijít	k5eAaPmAgNnS	přijít
zklamání	zklamání	k1gNnSc1	zklamání
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
desky	deska	k1gFnPc1	deska
South	Southa	k1gFnPc2	Southa
of	of	k?	of
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
,	,	kIx,	,
fanoušky	fanoušek	k1gMnPc4	fanoušek
bylo	být	k5eAaImAgNnS	být
(	(	kIx(	(
<g/>
zpočátku	zpočátku	k6eAd1	zpočátku
<g/>
)	)	kIx)	)
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
pro	pro	k7c4	pro
pomalost	pomalost	k1gFnSc4	pomalost
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
odklon	odklon	k1gInSc1	odklon
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Seasons	Seasonsa	k1gFnPc2	Seasonsa
in	in	k?	in
the	the	k?	the
Abyss	Abyss	k1gInSc1	Abyss
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
stalo	stát	k5eAaPmAgNnS	stát
platinovým	platinový	k2eAgMnSc7d1	platinový
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
megaturné	megaturný	k2eAgFnSc2d1	megaturný
Clash	Clash	k1gInSc4	Clash
of	of	k?	of
Titans	Titans	k1gInSc1	Titans
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
a	a	k8xC	a
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
bubeníka	bubeník	k1gMnSc2	bubeník
přišel	přijít	k5eAaPmAgMnS	přijít
Paul	Paul	k1gMnSc1	Paul
Bostaph	Bostaph	k1gMnSc1	Bostaph
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
nacismu	nacismus	k1gInSc2	nacismus
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
Divine	Divin	k1gInSc5	Divin
Intervention	Intervention	k1gInSc4	Intervention
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
ho	on	k3xPp3gNnSc4	on
Max	Max	k1gMnSc1	Max
Cavalera	Cavaler	k1gMnSc2	Cavaler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Sepultura	Sepultura	k1gFnSc1	Sepultura
<g/>
.	.	kIx.	.
</s>
<s>
Slayer	Slayer	k1gMnSc1	Slayer
to	ten	k3xDgNnSc4	ten
oplatili	oplatit	k5eAaPmAgMnP	oplatit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
televizi	televize	k1gFnSc6	televize
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
rozhovorů	rozhovor	k1gInPc2	rozhovor
prohlasili	prohlasit	k5eAaPmAgMnP	prohlasit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sepultura	Sepultura	k1gFnSc1	Sepultura
je	být	k5eAaImIp3nS	být
banda	banda	k1gFnSc1	banda
brazilských	brazilský	k2eAgMnPc2d1	brazilský
debilů	debil	k1gMnPc2	debil
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
a	a	k8xC	a
bunch	bunch	k1gInSc1	bunch
of	of	k?	of
lowlife	lowlif	k1gInSc5	lowlif
cocksuckers	cocksuckersit	k5eAaPmRp2nS	cocksuckersit
from	from	k1gMnSc1	from
Brazil	Brazil	k1gMnSc1	Brazil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Maxe	Max	k1gMnSc2	Max
Cavalery	Cavaler	k1gInPc7	Cavaler
ze	z	k7c2	z
Sepultury	Sepultura	k1gFnSc2	Sepultura
se	se	k3xPyFc4	se
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
kapelami	kapela	k1gFnPc7	kapela
spravil	spravit	k5eAaPmAgMnS	spravit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
za	za	k7c4	za
bubny	buben	k1gInPc4	buben
skupiny	skupina	k1gFnSc2	skupina
posadil	posadit	k5eAaPmAgMnS	posadit
Lombardo	Lombardo	k1gMnSc1	Lombardo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
album	album	k1gNnSc1	album
God	God	k1gFnSc2	God
Hates	Hatesa	k1gFnPc2	Hatesa
Us	Us	k1gMnSc1	Us
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
Live	Liv	k1gMnPc4	Liv
Performance	performance	k1gFnSc2	performance
songu	song	k1gInSc2	song
Disciple	Disciple	k1gFnPc3	Disciple
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
nezískalo	získat	k5eNaPmAgNnS	získat
však	však	k9	však
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
vyšla	vyjít	k5eAaPmAgFnS	vyjít
skupině	skupina	k1gFnSc6	skupina
2	[number]	k4	2
DVD	DVD	kA	DVD
-	-	kIx~	-
War	War	k1gMnSc1	War
at	at	k?	at
Warfield	Warfield	k1gMnSc1	Warfield
a	a	k8xC	a
Still	Still	k1gMnSc1	Still
Reigning	Reigning	k1gInSc1	Reigning
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
Christ	Christ	k1gInSc1	Christ
Illusion	Illusion	k1gInSc1	Illusion
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
skladbu	skladba	k1gFnSc4	skladba
Eyes	Eyesa	k1gFnPc2	Eyesa
of	of	k?	of
the	the	k?	the
Insane	Insan	k1gMnSc5	Insan
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
obdržela	obdržet	k5eAaPmAgFnS	obdržet
kapela	kapela	k1gFnSc1	kapela
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
recesivní	recesivní	k2eAgNnSc1d1	recesivní
album	album	k1gNnSc1	album
World	World	k1gInSc1	World
Painted	Painted	k1gMnSc1	Painted
Blood	Blood	k1gInSc1	Blood
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
stylu	styl	k1gInSc3	styl
z	z	k7c2	z
již	již	k6eAd1	již
klasického	klasický	k2eAgNnSc2d1	klasické
alba	album	k1gNnSc2	album
Reign	Reigna	k1gFnPc2	Reigna
In	In	k1gFnSc7	In
Blood	Blooda	k1gFnPc2	Blooda
<g/>
.	.	kIx.	.
</s>
<s>
Slayer	Slayer	k1gMnSc1	Slayer
se	se	k3xPyFc4	se
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
turné	turné	k1gNnSc4	turné
The	The	k1gMnSc2	The
Big	Big	k1gFnSc2	Big
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Hanneman	Hanneman	k1gMnSc1	Hanneman
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
nejlegendárnějších	legendární	k2eAgFnPc2d3	nejlegendárnější
skladeb	skladba	k1gFnPc2	skladba
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
kousnutí	kousnutí	k1gNnSc4	kousnutí
jedovatým	jedovatý	k2eAgMnSc7d1	jedovatý
pavoukem	pavouk	k1gMnSc7	pavouk
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
nekrotizující	krotizující	k2eNgFnSc7d1	nekrotizující
fascilitidou	fascilitida	k1gFnSc7	fascilitida
a	a	k8xC	a
selhala	selhat	k5eAaPmAgFnS	selhat
mu	on	k3xPp3gMnSc3	on
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
byla	být	k5eAaImAgNnP	být
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
poškozena	poškodit	k5eAaPmNgFnS	poškodit
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Gary	Gara	k1gFnSc2	Gara
Holt	Holt	k?	Holt
(	(	kIx(	(
<g/>
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Exodus	Exodus	k1gInSc1	Exodus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
Slayer	Slayer	k1gMnSc1	Slayer
nahrál	nahrát	k5eAaBmAgMnS	nahrát
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Repentless	Repentlessa	k1gFnPc2	Repentlessa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvanácté	dvanáctý	k4xOgInPc1	dvanáctý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některé	některý	k3yIgFnSc2	některý
kritiky	kritika	k1gFnSc2	kritika
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
po	po	k7c6	po
6	[number]	k4	6
<g/>
leté	letý	k2eAgFnSc6d1	letá
odmlce	odmlka	k1gFnSc6	odmlka
vydařilo	vydařit	k5eAaPmAgNnS	vydařit
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
Jeffa	Jeff	k1gMnSc2	Jeff
Hannemana	Hanneman	k1gMnSc2	Hanneman
<g/>
.	.	kIx.	.
</s>
<s>
Davea	Dave	k2eAgMnSc4d1	Dave
Lombarda	Lombard	k1gMnSc4	Lombard
opět	opět	k6eAd1	opět
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Paul	Paul	k1gMnSc1	Paul
Bostaph	Bostaph	k1gMnSc1	Bostaph
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slayer	Slayer	k1gInSc1	Slayer
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
Metallicy	Metallica	k1gFnSc2	Metallica
nejcoverovanější	coverovaný	k2eAgNnSc1d3	coverovaný
(	(	kIx(	(
<g/>
nejpřehrávanější	přehrávaný	k2eAgFnSc7d3	přehrávaný
<g/>
)	)	kIx)	)
metalovou	metalový	k2eAgFnSc7d1	metalová
kapelou	kapela	k1gFnSc7	kapela
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
pověst	pověst	k1gFnSc1	pověst
technického	technický	k2eAgNnSc2d1	technické
mistrovství	mistrovství	k1gNnSc2	mistrovství
díky	díky	k7c3	díky
některým	některý	k3yIgFnPc3	některý
písním	píseň	k1gFnPc3	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
složil	složit	k5eAaPmAgMnS	složit
Jeff	Jeff	k1gMnSc1	Jeff
Hanneman	Hanneman	k1gMnSc1	Hanneman
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
poněkud	poněkud	k6eAd1	poněkud
drsnější	drsný	k2eAgFnSc7d2	drsnější
a	a	k8xC	a
rychlejší	rychlý	k2eAgFnSc7d2	rychlejší
hudbou	hudba	k1gFnSc7	hudba
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
temnějším	temný	k2eAgInSc6d2	temnější
EP	EP	kA	EP
Haunting	Haunting	k1gInSc1	Haunting
the	the	k?	the
Chapel	Chapel	k1gInSc4	Chapel
a	a	k8xC	a
alby	alba	k1gFnPc4	alba
jako	jako	k8xC	jako
Hell	Hell	k1gInSc4	Hell
Awaits	Awaitsa	k1gFnPc2	Awaitsa
a	a	k8xC	a
především	především	k6eAd1	především
Reign	Reign	k1gNnSc1	Reign
In	In	k1gFnSc2	In
Blood	Blooda	k1gFnPc2	Blooda
<g/>
)	)	kIx)	)
i	i	k9	i
vývoj	vývoj	k1gInSc1	vývoj
extrémních	extrémní	k2eAgFnPc2d1	extrémní
odnoží	odnož	k1gFnPc2	odnož
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
především	především	k9	především
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
black	black	k1gInSc1	black
metalu	metal	k1gInSc2	metal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Slipknot	Slipknota	k1gFnPc2	Slipknota
nebo	nebo	k8xC	nebo
Vader	Vadra	k1gFnPc2	Vadra
<g/>
,	,	kIx,	,
přiznávají	přiznávat	k5eAaImIp3nP	přiznávat
Slayer	Slayer	k1gInSc1	Slayer
jako	jako	k9	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavním	hlavní	k2eAgMnSc7d1	hlavní
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Slayer	Slayer	k1gMnSc1	Slayer
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
punkem	punk	k1gMnSc7	punk
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
Hanneman	Hanneman	k1gMnSc1	Hanneman
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapelami	kapela	k1gFnPc7	kapela
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
<g/>
,	,	kIx,	,
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
Motörhead	Motörhead	k1gInSc1	Motörhead
a	a	k8xC	a
Venom	Venom	k1gInSc1	Venom
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
jako	jako	k8xC	jako
otec	otec	k1gMnSc1	otec
dvoukopáků	dvoukopák	k1gMnPc2	dvoukopák
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgInSc1d1	bicí
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
pedálů	pedál	k1gInPc2	pedál
u	u	k7c2	u
velkého	velký	k2eAgInSc2d1	velký
<g/>
/	/	kIx~	/
<g/>
basového	basový	k2eAgInSc2d1	basový
bubnu	buben	k1gInSc2	buben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
je	on	k3xPp3gMnPc4	on
možné	možný	k2eAgMnPc4d1	možný
dvoukopáky	dvoukopák	k1gMnPc4	dvoukopák
slyšet	slyšet	k5eAaImF	slyšet
v	v	k7c6	v
albu	album	k1gNnSc6	album
Hell	Hellum	k1gNnPc2	Hellum
Awaits	Awaitsa	k1gFnPc2	Awaitsa
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bubeník	Bubeník	k1gMnSc1	Bubeník
Gene	gen	k1gInSc5	gen
Hoglan	Hoglan	k1gInSc1	Hoglan
(	(	kIx(	(
<g/>
Strapping	Strapping	k1gInSc1	Strapping
Young	Young	k1gMnSc1	Young
Lad	lad	k1gInSc1	lad
<g/>
,	,	kIx,	,
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
Fear	Fear	k1gInSc1	Fear
Factory	Factor	k1gInPc1	Factor
<g/>
)	)	kIx)	)
začínal	začínat	k5eAaImAgInS	začínat
jako	jako	k9	jako
bedňák	bedňák	k?	bedňák
Slayer	Slayer	k1gInSc1	Slayer
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
měl	mít	k5eAaImAgMnS	mít
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
post	post	k1gInSc4	post
drumtechnika	drumtechnik	k1gMnSc2	drumtechnik
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Hoglan	Hoglan	k1gMnSc1	Hoglan
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Kerry	Kerra	k1gMnSc2	Kerra
King	King	k1gMnSc1	King
nahrál	nahrát	k5eAaBmAgMnS	nahrát
kytarové	kytarový	k2eAgNnSc4d1	kytarové
sólo	sólo	k1gNnSc4	sólo
pro	pro	k7c4	pro
song	song	k1gInSc4	song
"	"	kIx"	"
<g/>
No	no	k9	no
Sleep	Sleep	k1gInSc1	Sleep
Til	til	k1gInSc1	til
<g/>
'	'	kIx"	'
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
"	"	kIx"	"
od	od	k7c2	od
populární	populární	k2eAgFnSc2d1	populární
hip-hop	hipop	k1gInSc4	hip-hop
<g/>
/	/	kIx~	/
<g/>
rockové	rockový	k2eAgFnPc1d1	rocková
kapely	kapela	k1gFnPc1	kapela
Beastie	Beastie	k1gFnSc2	Beastie
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
mnozí	mnohý	k2eAgMnPc1d1	mnohý
fanoušci	fanoušek	k1gMnPc1	fanoušek
spekulují	spekulovat	k5eAaImIp3nP	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
původně	původně	k6eAd1	původně
začínala	začínat	k5eAaImAgFnS	začínat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dragonslayer	Dragonslayer	k1gMnSc1	Dragonslayer
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
film	film	k1gInSc1	film
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
ale	ale	k8xC	ale
rozporuje	rozporovat	k5eAaPmIp3nS	rozporovat
samotný	samotný	k2eAgMnSc1d1	samotný
Kerry	Kerra	k1gFnPc4	Kerra
King	King	k1gInSc4	King
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
několika	několik	k4yIc6	několik
rozhovorech	rozhovor	k1gInPc6	rozhovor
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pouhý	pouhý	k2eAgInSc4d1	pouhý
mýtus	mýtus	k1gInSc4	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
hrála	hrát	k5eAaImAgFnS	hrát
kapela	kapela	k1gFnSc1	kapela
namalovaná	namalovaný	k2eAgFnSc1d1	namalovaná
silnou	silný	k2eAgFnSc7d1	silná
vrstvou	vrstva	k1gFnSc7	vrstva
makeupu	makeup	k1gInSc2	makeup
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
společně	společně	k6eAd1	společně
s	s	k7c7	s
Exodus	Exodus	k1gInSc1	Exodus
<g/>
,	,	kIx,	,
Laaz	Laaz	k1gInSc1	Laaz
Rocket	Rocketa	k1gFnPc2	Rocketa
a	a	k8xC	a
partou	parta	k1gFnSc7	parta
Savage	Savag	k1gMnSc2	Savag
Grace	Grace	k1gMnSc2	Grace
<g/>
,	,	kIx,	,
sklidili	sklidit	k5eAaPmAgMnP	sklidit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
uvítání	uvítání	k1gNnSc4	uvítání
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
fanoušci	fanoušek	k1gMnPc1	fanoušek
trochu	trochu	k6eAd1	trochu
smáli	smát	k5eAaImAgMnP	smát
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInSc3	jejich
makeupu	makeup	k1gInSc3	makeup
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
Araya	Araya	k1gMnSc1	Araya
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
setřel	setřít	k5eAaPmAgMnS	setřít
si	se	k3xPyFc3	se
líčidlo	líčidlo	k1gNnSc4	líčidlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nenamalovali	namalovat	k5eNaPmAgMnP	namalovat
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Show	show	k1gFnSc2	show
No	no	k9	no
Mercy	Merca	k1gFnPc1	Merca
bylo	být	k5eAaImAgNnS	být
příznačně	příznačně	k6eAd1	příznačně
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
však	však	k9	však
o	o	k7c4	o
žádný	žádný	k3yNgInSc4	žádný
výstřelek	výstřelek	k1gInSc4	výstřelek
kapely	kapela	k1gFnSc2	kapela
-	-	kIx~	-
Slayer	Slayer	k1gInSc4	Slayer
chtěli	chtít	k5eAaImAgMnP	chtít
jednak	jednak	k8xC	jednak
ušetřit	ušetřit	k5eAaPmF	ušetřit
za	za	k7c4	za
pronájem	pronájem	k1gInSc4	pronájem
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
druhak	druhak	k6eAd1	druhak
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
den	den	k1gInSc4	den
bubeník	bubeník	k1gMnSc1	bubeník
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
věnoval	věnovat	k5eAaImAgMnS	věnovat
snaze	snaha	k1gFnSc3	snaha
dokončit	dokončit	k5eAaPmF	dokončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc4	logo
kapely	kapela	k1gFnSc2	kapela
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
původní	původní	k2eAgFnSc2d1	původní
sestavy	sestava	k1gFnSc2	sestava
Slayer	Slayra	k1gFnPc2	Slayra
nebyla	být	k5eNaImAgFnS	být
narozena	narodit	k5eAaPmNgFnS	narodit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
frontman	frontman	k1gMnSc1	frontman
Tom	Tom	k1gMnSc1	Tom
Araya	Araya	k1gMnSc1	Araya
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
Araya	Araya	k1gMnSc1	Araya
byl	být	k5eAaImAgMnS	být
přemluven	přemluvit	k5eAaPmNgMnS	přemluvit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
paradoxně	paradoxně	k6eAd1	paradoxně
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
o	o	k7c4	o
metal	metal	k1gInSc4	metal
nezajímal	zajímat	k5eNaImAgMnS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
nedal	dát	k5eNaPmAgMnS	dát
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
muzikanta	muzikant	k1gMnSc2	muzikant
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
by	by	kYmCp3nS	by
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
roli	role	k1gFnSc4	role
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
fanoušek	fanoušek	k1gMnSc1	fanoušek
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
vyřezat	vyřezat	k5eAaPmF	vyřezat
na	na	k7c4	na
spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
zápěstí	zápěstí	k1gNnSc2	zápěstí
logo	logo	k1gNnSc4	logo
Slayer	Slayer	k1gInSc1	Slayer
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
fotografii	fotografia	k1gFnSc4	fotografia
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c4	na
booklet	booklet	k1gInSc4	booklet
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Album	album	k1gNnSc1	album
<g/>
:	:	kIx,	:
Divine	Divin	k1gMnSc5	Divin
Intervention	Intervention	k1gInSc1	Intervention
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
album	album	k1gNnSc1	album
God	God	k1gFnSc2	God
Hates	Hatesa	k1gFnPc2	Hatesa
Us	Us	k1gMnSc1	Us
All	All	k1gMnSc1	All
<g/>
,	,	kIx,	,
náhodou	náhodou	k6eAd1	náhodou
na	na	k7c4	na
den	den	k1gInSc4	den
přesně	přesně	k6eAd1	přesně
s	s	k7c7	s
teroristickým	teroristický	k2eAgInSc7d1	teroristický
<g />
.	.	kIx.	.
</s>
<s>
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
věže	věž	k1gFnPc4	věž
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Současní	současný	k2eAgMnPc1d1	současný
Tom	Tom	k1gMnSc1	Tom
Araya	Araya	k1gMnSc1	Araya
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Kerry	Kerra	k1gFnPc1	Kerra
King	King	k1gMnSc1	King
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Gary	Gar	k2eAgInPc1d1	Gar
Holt	Holt	k?	Holt
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
současně	současně	k6eAd1	současně
stále	stále	k6eAd1	stále
členem	člen	k1gMnSc7	člen
Exodus	Exodus	k1gInSc4	Exodus
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
kapely	kapela	k1gFnSc2	kapela
Paul	Paul	k1gMnSc1	Paul
Bostaph	Bostaph	k1gMnSc1	Bostaph
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
Jeff	Jeff	k1gMnSc1	Jeff
Hanneman	Hanneman	k1gMnSc1	Hanneman
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Jon	Jon	k1gMnSc5	Jon
Dette	Dett	k1gMnSc5	Dett
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Sezónní	sezónní	k2eAgInPc1d1	sezónní
Tony	tonus	k1gInPc1	tonus
Scaglione	Scaglion	k1gInSc5	Scaglion
–	–	k?	–
bicí	bicí	k2eAgFnPc1d1	bicí
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Pat	pat	k1gInSc1	pat
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k1gInSc1	Brien
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Gary	Gara	k1gFnSc2	Gara
Holt	Holt	k?	Holt
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slayer	Slayra	k1gFnPc2	Slayra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Slayer	Slayer	k1gInSc1	Slayer
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Official	Official	k1gInSc1	Official
Web	web	k1gInSc1	web
Slayer	Slayer	k1gInSc4	Slayer
HMB	HMB	kA	HMB
–	–	k?	–
Slayer	Slayer	k1gMnSc1	Slayer
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Hard	Hard	k1gMnSc1	Hard
Music	Music	k1gMnSc1	Music
Base	basa	k1gFnSc3	basa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Billboard	billboard	k1gInSc4	billboard
Fanouškovský	fanouškovský	k2eAgInSc4d1	fanouškovský
web	web	k1gInSc4	web
o	o	k7c4	o
Slayer	Slayer	k1gInSc4	Slayer
-	-	kIx~	-
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
