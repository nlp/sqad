<s>
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
</s>
<s>
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
(	(	kIx(
<g/>
lucembursky	lucembursky	k6eAd1
Lëtzebuerger	Lëtzebuerger	k1gMnSc1
Frang	Frang	k1gMnSc1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
franc	franc	k1gFnSc1
Luxembourgeois	Luxembourgeois	k1gFnSc2
<g/>
,	,	kIx,
německy	německy	k6eAd1
Luxemburger	Luxemburger	k1gInSc1
Franken	Frankna	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
oficiální	oficiální	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
Lucemburského	lucemburský	k2eAgNnSc2d1
velkovévodství	velkovévodství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
byl	být	k5eAaImAgInS
LUF	luf	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
franku	frank	k1gInSc2
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
cent	cent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Status	status	k1gInSc1
a	a	k8xC
historie	historie	k1gFnSc1
měny	měna	k1gFnSc2
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
a	a	k8xC
sousední	sousední	k2eAgFnPc1d1
Belgie	Belgie	k1gFnPc1
spolu	spolu	k6eAd1
spolupracují	spolupracovat	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
Belgicko-lucemburské	belgicko-lucemburský	k2eAgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
unii	unie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
měny	měna	k1gFnSc2
-	-	kIx~
lucemburský	lucemburský	k2eAgInSc1d1
a	a	k8xC
belgický	belgický	k2eAgInSc1d1
frank	frank	k1gInSc1
-	-	kIx~
byly	být	k5eAaImAgFnP
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1922	#num#	k4
a	a	k8xC
1999	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
období	období	k1gNnSc2
mezi	mezi	k7c7
1935	#num#	k4
a	a	k8xC
1944	#num#	k4
<g/>
)	)	kIx)
pevně	pevně	k6eAd1
propojené	propojený	k2eAgFnPc1d1
v	v	k7c6
paritním	paritní	k2eAgInSc6d1
kursu	kurs	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoreticky	teoreticky	k6eAd1
bylo	být	k5eAaImAgNnS
tedy	tedy	k9
možno	možno	k6eAd1
používat	používat	k5eAaImF
lucemburské	lucemburský	k2eAgInPc4d1
franky	frank	k1gInPc4
na	na	k7c6
belgickém	belgický	k2eAgNnSc6d1
území	území	k1gNnSc6
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
však	však	k9
obchodníci	obchodník	k1gMnPc1
často	často	k6eAd1
odmítali	odmítat	k5eAaImAgMnP
přijímat	přijímat	k5eAaImF
měnu	měna	k1gFnSc4
sousedního	sousední	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zavedení	zavedení	k1gNnSc6
eura	euro	k1gNnSc2
jako	jako	k8xS,k8xC
bezhotovostní	bezhotovostní	k2eAgFnSc2d1
měny	měna	k1gFnSc2
byl	být	k5eAaImAgInS
směnný	směnný	k2eAgInSc1d1
kurs	kurs	k1gInSc1
mezi	mezi	k7c7
frankem	frank	k1gInSc7
a	a	k8xC
eurem	euro	k1gNnSc7
stanoven	stanoven	k2eAgInSc4d1
na	na	k7c4
1	#num#	k4
EUR	euro	k1gNnPc2
=	=	kIx~
40,339	40,339	k4
<g/>
9	#num#	k4
LUF	lufa	k1gFnPc2
(	(	kIx(
<g/>
resp.	resp.	kA
BEF	BEF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
byl	být	k5eAaImAgInS
frank	frank	k1gInSc1
pouze	pouze	k6eAd1
dílčí	dílčí	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
eura	euro	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
je	být	k5eAaImIp3nS
po	po	k7c6
krátkém	krátký	k2eAgInSc6d1
duálním	duální	k2eAgInSc6d1
oběhu	oběh	k1gInSc6
franku	frank	k1gInSc2
a	a	k8xC
eura	euro	k1gNnSc2
v	v	k7c6
oběhu	oběh	k1gInSc6
pouze	pouze	k6eAd1
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Posledními	poslední	k2eAgFnPc7d1
mincemi	mince	k1gFnPc7
lucemburského	lucemburský	k2eAgInSc2d1
franku	frank	k1gInSc2
před	před	k7c7
zavedením	zavedení	k1gNnSc7
eura	euro	k1gNnSc2
byly	být	k5eAaImAgFnP
mince	mince	k1gFnPc1
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
a	a	k8xC
50	#num#	k4
franků	frank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
emise	emise	k1gFnSc1
mincí	mince	k1gFnPc2
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
pouze	pouze	k6eAd1
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
100	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
a	a	k8xC
5000	#num#	k4
franků	frank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
všech	všecek	k3xTgFnPc6
bankovkách	bankovka	k1gFnPc6
byl	být	k5eAaImAgMnS
vyobrazen	vyobrazen	k2eAgMnSc1d1
velkovévoda	velkovévoda	k1gMnSc1
Jean	Jean	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
byla	být	k5eAaImAgFnS
do	do	k7c2
oběhu	oběh	k1gInSc2
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
i	i	k8xC
bankovky	bankovka	k1gFnSc2
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
Lucemburský	lucemburský	k2eAgInSc1d1
peněžní	peněžní	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
Institut	institut	k1gInSc1
Monetaire	Monetair	k1gInSc5
Luxembougeois	Luxembougeois	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
sílo	síla	k1gFnSc5
v	v	k7c6
Lucemburku	Lucemburk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucemburské	lucemburský	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
šlo	jít	k5eAaImAgNnS
vyměnit	vyměnit	k5eAaPmF
za	za	k7c2
eura	euro	k1gNnSc2
jen	jen	k9
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Lucemburské	lucemburský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lucemburský	lucemburský	k2eAgInSc4d1
frank	frank	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Lucemburský	lucemburský	k2eAgInSc1d1
peněžní	peněžní	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Mince	mince	k1gFnSc1
franku	frank	k1gInSc2
</s>
<s>
Bankovky	bankovka	k1gFnPc1
franku	frank	k1gInSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnPc1
nahrazené	nahrazený	k2eAgFnPc1d1
eurem	euro	k1gNnSc7
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Estonská	estonský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Finská	finský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Francouzský	francouzský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Italská	italský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Kyperská	kyperský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Litevský	litevský	k2eAgInSc1d1
litas	litas	k1gInSc1
•	•	k?
Lotyšský	lotyšský	k2eAgInSc1d1
lat	lat	k1gInSc1
•	•	k?
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Maltská	maltský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Monacký	monacký	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Nizozemský	nizozemský	k2eAgInSc1d1
gulden	gulden	k1gInSc1
•	•	k?
Portugalské	portugalský	k2eAgNnSc4d1
escudo	escudo	k1gNnSc4
•	•	k?
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
drachma	drachma	k1gFnSc1
•	•	k?
Sanmarinská	Sanmarinský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Slovinský	slovinský	k2eAgInSc1d1
tolar	tolar	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
peseta	peseta	k1gFnSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
</s>
