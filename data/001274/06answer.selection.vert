<s>
Krypton	krypton	k1gInSc1	krypton
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Kr	Kr	k1gFnSc2	Kr
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
