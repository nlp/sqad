<s>
Krypton	krypton	k1gInSc1	krypton
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Kr	Kr	k1gFnSc2	Kr
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
nereaktivní	reaktivní	k2eNgFnSc1d1	nereaktivní
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
inertní	inertní	k2eAgNnPc1d1	inertní
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
silnými	silný	k2eAgInPc7d1	silný
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
činidly	činidlo	k1gNnPc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
dobře	dobře	k6eAd1	dobře
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
lépe	dobře	k6eAd2	dobře
v	v	k7c6	v
nepolárních	polární	k2eNgNnPc6d1	nepolární
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
zachytit	zachytit	k5eAaPmF	zachytit
na	na	k7c6	na
aktivním	aktivní	k2eAgNnSc6d1	aktivní
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
snadno	snadno	k6eAd1	snadno
ionizuje	ionizovat	k5eAaBmIp3nS	ionizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
ionizovaném	ionizovaný	k2eAgInSc6d1	ionizovaný
stavu	stav	k1gInSc6	stav
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
osvětlovací	osvětlovací	k2eAgFnSc6d1	osvětlovací
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
výbojem	výboj	k1gInSc7	výboj
v	v	k7c6	v
kryptonu	krypton	k1gInSc6	krypton
má	mít	k5eAaImIp3nS	mít
zelenavě	zelenavě	k6eAd1	zelenavě
až	až	k9	až
světle	světle	k6eAd1	světle
fialovou	fialový	k2eAgFnSc4d1	fialová
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zřeďováním	zřeďování	k1gNnSc7	zřeďování
kryptonu	krypton	k1gInSc2	krypton
přechází	přecházet	k5eAaImIp3nS	přecházet
až	až	k9	až
v	v	k7c4	v
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
objevil	objevit	k5eAaPmAgInS	objevit
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Rayleightem	Rayleight	k1gInSc7	Rayleight
argon	argon	k1gInSc1	argon
a	a	k8xC	a
správně	správně	k6eAd1	správně
oba	dva	k4xCgInPc4	dva
plyny	plyn	k1gInPc4	plyn
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
mu	on	k3xPp3gMnSc3	on
volné	volný	k2eAgNnSc1d1	volné
místo	místo	k1gNnSc1	místo
před	před	k7c7	před
a	a	k8xC	a
za	za	k7c7	za
argonem	argon	k1gInSc7	argon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
volných	volný	k2eAgNnPc2d1	volné
míst	místo	k1gNnPc2	místo
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
neon	neon	k1gInSc4	neon
a	a	k8xC	a
krypton	krypton	k1gInSc4	krypton
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
Williamem	William	k1gInSc7	William
Ramsayem	Ramsay	k1gInSc7	Ramsay
a	a	k8xC	a
Morrisem	Morris	k1gInSc7	Morris
Traversem	travers	k1gInSc7	travers
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
využil	využít	k5eAaPmAgMnS	využít
nové	nový	k2eAgFnPc4d1	nová
metody	metoda	k1gFnPc4	metoda
frakčního	frakční	k2eAgInSc2d1	frakční
destilace	destilace	k1gFnSc1	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
kryptonem	krypton	k1gInSc7	krypton
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
neon	neon	k1gInSc1	neon
a	a	k8xC	a
xenon	xenon	k1gInSc1	xenon
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
objevil	objevit	k5eAaPmAgInS	objevit
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
skrytý	skrytý	k2eAgInSc1d1	skrytý
-	-	kIx~	-
krypton	krypton	k1gInSc1	krypton
<g/>
.	.	kIx.	.
</s>
<s>
Poznal	poznat	k5eAaPmAgMnS	poznat
jej	on	k3xPp3gMnSc4	on
podle	podle	k7c2	podle
dvou	dva	k4xCgFnPc2	dva
čar	čára	k1gFnPc2	čára
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
čar	čára	k1gFnPc2	čára
ve	v	k7c6	v
žluté	žlutý	k2eAgFnSc6d1	žlutá
části	část	k1gFnSc6	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
0,0001	[number]	k4	0,0001
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
získáván	získávat	k5eAaImNgInS	získávat
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
produktů	produkt	k1gInPc2	produkt
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
plynných	plynný	k2eAgInPc6d1	plynný
produktech	produkt	k1gInPc6	produkt
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
získání	získání	k1gNnSc2	získání
kryptonu	krypton	k1gInSc2	krypton
je	být	k5eAaImIp3nS	být
frakční	frakční	k2eAgFnSc1d1	frakční
adsorpce	adsorpce	k1gFnSc1	adsorpce
na	na	k7c4	na
aktivní	aktivní	k2eAgNnSc4d1	aktivní
uhlí	uhlí	k1gNnSc4	uhlí
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
kapalného	kapalný	k2eAgInSc2d1	kapalný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
izotopů	izotop	k1gInPc2	izotop
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
6	[number]	k4	6
je	být	k5eAaImIp3nS	být
stabilních	stabilní	k2eAgInPc2d1	stabilní
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
podléhají	podléhat	k5eAaImIp3nP	podléhat
radioaktivní	radioaktivní	k2eAgFnSc3d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Určení	určení	k1gNnSc1	určení
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
poměru	poměr	k1gInSc2	poměr
různých	různý	k2eAgInPc2d1	různý
izotopů	izotop	k1gInPc2	izotop
kryptonu	krypton	k1gInSc2	krypton
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
datování	datování	k1gNnSc3	datování
stáří	stáří	k1gNnSc2	stáří
hornin	hornina	k1gFnPc2	hornina
nebo	nebo	k8xC	nebo
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
izotopy	izotop	k1gInPc1	izotop
kryptonu	krypton	k1gInSc2	krypton
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
nukleárních	nukleární	k2eAgFnPc2d1	nukleární
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
zastoupení	zastoupení	k1gNnSc2	zastoupení
vybraných	vybraný	k2eAgInPc2d1	vybraný
izotopů	izotop	k1gInPc2	izotop
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
posouzení	posouzení	k1gNnSc3	posouzení
velikosti	velikost	k1gFnSc2	velikost
depozice	depozice	k1gFnSc2	depozice
produktů	produkt	k1gInPc2	produkt
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zkoušek	zkouška	k1gFnPc2	zkouška
ve	v	k7c6	v
zkoumaných	zkoumaný	k2eAgFnPc6d1	zkoumaná
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
sloužila	sloužit	k5eAaImAgFnS	sloužit
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
emisní	emisní	k2eAgFnSc2d1	emisní
linie	linie	k1gFnSc2	linie
kryptonu	krypton	k1gInSc2	krypton
k	k	k7c3	k
definici	definice	k1gFnSc3	definice
délkové	délkový	k2eAgFnSc2d1	délková
jednotky	jednotka	k1gFnSc2	jednotka
metr	metr	k1gInSc1	metr
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
osvětlovací	osvětlovací	k2eAgFnSc6d1	osvětlovací
technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
kryptonových	kryptonový	k2eAgFnPc2d1	kryptonová
žárovek	žárovka	k1gFnPc2	žárovka
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
zářivek	zářivka	k1gFnPc2	zářivka
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
dále	daleko	k6eAd2	daleko
použít	použít	k5eAaPmF	použít
ve	v	k7c6	v
výbojkách	výbojka	k1gFnPc6	výbojka
<g/>
,	,	kIx,	,
obloukových	obloukový	k2eAgFnPc6d1	oblouková
lampách	lampa	k1gFnPc6	lampa
a	a	k8xC	a
doutnavých	doutnavý	k2eAgFnPc6d1	doutnavý
trubicích	trubice	k1gFnPc6	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
výbojem	výboj	k1gInSc7	výboj
v	v	k7c6	v
kryptonu	krypton	k1gInSc6	krypton
má	mít	k5eAaImIp3nS	mít
zelenavě	zelenavě	k6eAd1	zelenavě
až	až	k9	až
světle	světle	k6eAd1	světle
fialovou	fialový	k2eAgFnSc4d1	fialová
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc7	jeho
ředěním	ředění	k1gNnSc7	ředění
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
a	a	k8xC	a
při	při	k7c6	při
velkém	velký	k2eAgNnSc6d1	velké
zředění	zředění	k1gNnSc6	zředění
začne	začít	k5eAaPmIp3nS	začít
vydávat	vydávat	k5eAaImF	vydávat
bílé	bílý	k2eAgNnSc4d1	bílé
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Krypton	krypton	k1gInSc1	krypton
se	se	k3xPyFc4	se
také	také	k9	také
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
dalšími	další	k2eAgInPc7d1	další
inertními	inertní	k2eAgInPc7d1	inertní
plyny	plyn	k1gInPc7	plyn
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
plnění	plnění	k1gNnPc4	plnění
izolačních	izolační	k2eAgNnPc2d1	izolační
dvojskel	dvojsklo	k1gNnPc2	dvojsklo
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
krypton	krypton	k1gInSc1	krypton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
krypton	krypton	k1gInSc1	krypton
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
