<s>
Nikumaroro	Nikumarora	k1gFnSc5
</s>
<s>
Nikumaroro	Nikumarora	k1gFnSc5
</s>
<s>
Nikumaroro	Nikumarora	k1gFnSc5
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Kiribati	Kiribat	k5eAaImF,k5eAaPmF
Kiribati	Kiribati	k1gFnSc4
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
174	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Osídlení	osídlení	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nikumaroro	Nikumaroro	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
Gardnerův	Gardnerův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Phoenixských	Phoenixský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
ve	v	k7c6
státě	stát	k1gInSc6
Kiribati	Kiribati	k1gFnSc1
v	v	k7c6
západním	západní	k2eAgInSc6d1
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
odlehlý	odlehlý	k2eAgInSc4d1
<g/>
,	,	kIx,
protáhlý	protáhlý	k2eAgInSc4d1
korálový	korálový	k2eAgInSc4d1
atol	atol	k1gInSc4
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
s	s	k7c7
bohatou	bohatý	k2eAgFnSc7d1
vegetací	vegetace	k1gFnSc7
a	a	k8xC
velkou	velký	k2eAgFnSc7d1
centrální	centrální	k2eAgFnSc7d1
lagunou	laguna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikumaroro	Nikumarora	k1gFnSc5
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
7,5	7,5	k4
kilometrů	kilometr	k1gInPc2
dlouhý	dlouhý	k2eAgMnSc1d1
a	a	k8xC
2,5	2,5	k4
kilometru	kilometr	k1gInSc2
široký	široký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laguna	laguna	k1gFnSc1
je	být	k5eAaImIp3nS
při	při	k7c6
přílivu	příliv	k1gInSc6
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
mořem	moře	k1gNnSc7
dvěma	dva	k4xCgInPc7
mělkými	mělký	k2eAgInPc7d1
průlivy	průliv	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oceán	oceán	k1gInSc1
kolem	kolem	k7c2
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
mimo	mimo	k7c4
útes	útes	k1gInSc4
velmi	velmi	k6eAd1
hluboký	hluboký	k2eAgMnSc1d1
a	a	k8xC
jediné	jediný	k2eAgNnSc1d1
možné	možný	k2eAgNnSc1d1
kotviště	kotviště	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
útesu	útes	k1gInSc2
naproti	naproti	k7c3
troskám	troska	k1gFnPc3
opuštěné	opuštěný	k2eAgFnSc2d1
britské	britský	k2eAgFnSc2d1
osady	osada	k1gFnSc2
fungující	fungující	k2eAgFnSc2d1
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
až	až	k9
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kotviště	kotviště	k1gNnPc4
je	být	k5eAaImIp3nS
bezpečné	bezpečný	k2eAgNnSc1d1
pouze	pouze	k6eAd1
pokud	pokud	k8xS
vlaje	vlát	k5eAaImIp3nS
jihovýchodní	jihovýchodní	k2eAgInSc1d1
pasát	pasát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistání	přistání	k1gNnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
a	a	k8xC
většinou	většina	k1gFnSc7
probíhá	probíhat	k5eAaImIp3nS
jižně	jižně	k6eAd1
od	od	k7c2
kotviště	kotviště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
na	na	k7c6
ostrově	ostrov	k1gInSc6
nacházelo	nacházet	k5eAaImAgNnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
osídlení	osídlení	k1gNnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
ostrov	ostrov	k1gInSc1
neobydlen	obydlen	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
vyhlásilo	vyhlásit	k5eAaPmAgNnS
Kiribati	Kiribati	k1gFnPc2
na	na	k7c6
Phoenixských	Phoenixský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
rozšířen	rozšířit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
425	#num#	k4
300	#num#	k4
km²	km²	k?
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
osm	osm	k4xCc4
atolů	atol	k1gInPc2
včetně	včetně	k7c2
Nikumaroro	Nikumarora	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Nikumaroro	Nikumarora	k1gFnSc5
byl	být	k5eAaImAgInS
předmětem	předmět	k1gInSc7
významných	významný	k2eAgFnPc2d1
spekulací	spekulace	k1gFnPc2
a	a	k8xC
průzkumu	průzkum	k1gInSc2
jako	jako	k8xS,k8xC
možné	možný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
nouzového	nouzový	k2eAgNnSc2d1
přistání	přistání	k1gNnSc2
americké	americký	k2eAgFnSc2d1
letkyně	letkyně	k1gFnSc2
Amelie	Amelie	k1gFnSc2
Earhartové	Earhartová	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
jejího	její	k3xOp3gInSc2
posledního	poslední	k2eAgInSc2d1
letu	let	k1gInSc2
při	při	k7c6
pokusu	pokus	k1gInSc6
obletět	obletět	k5eAaPmF
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Nikumaroro	Nikumarora	k1gFnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
