<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Rybník	rybník	k1gInSc1	rybník
Jíkovec	Jíkovec	k1gInSc1	Jíkovec
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Ostružno	Ostružno	k6eAd1	Ostružno
<g/>
,	,	kIx,	,
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Ohařice	Ohařice	k1gFnSc2	Ohařice
<g/>
.	.	kIx.	.
</s>
