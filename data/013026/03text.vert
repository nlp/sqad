<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Rybník	rybník	k1gInSc1	rybník
Jíkovec	Jíkovec	k1gInSc1	Jíkovec
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Ostružno	Ostružno	k6eAd1	Ostružno
<g/>
,	,	kIx,	,
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Ohařice	Ohařice	k1gFnSc2	Ohařice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
spadá	spadat	k5eAaPmIp3nS	spadat
kromě	kromě	k7c2	kromě
rybníku	rybník	k1gInSc3	rybník
Jíkavec	jíkavec	k1gMnSc1	jíkavec
i	i	k9	i
rybník	rybník	k1gInSc4	rybník
U	u	k7c2	u
Sv.	sv.	kA	sv.
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
jsou	být	k5eAaImIp3nP	být
vlhké	vlhký	k2eAgFnPc1d1	vlhká
louky	louka	k1gFnPc1	louka
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
vzácných	vzácný	k2eAgFnPc2d1	vzácná
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
rybníky	rybník	k1gInPc1	rybník
jsou	být	k5eAaImIp3nP	být
napájeny	napájet	k5eAaImNgInP	napájet
potokem	potok	k1gInSc7	potok
Velký	velký	k2eAgInSc4d1	velký
Porák	Porák	k1gInSc4	Porák
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
součástí	součást	k1gFnSc7	součást
větší	veliký	k2eAgFnSc2d2	veliký
soustavy	soustava	k1gFnSc2	soustava
osmi	osm	k4xCc2	osm
Ostruženských	Ostruženský	k2eAgInPc2d1	Ostruženský
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Květena	květena	k1gFnSc1	květena
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rybníka	rybník	k1gInSc2	rybník
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
slatinné	slatinný	k2eAgFnPc1d1	slatinná
louky	louka	k1gFnPc1	louka
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nP	dařit
vlhkomilným	vlhkomilný	k2eAgInPc3d1	vlhkomilný
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
suchopýr	suchopýr	k1gInSc1	suchopýr
úzkolistý	úzkolistý	k2eAgInSc1d1	úzkolistý
(	(	kIx(	(
<g/>
Eriophorum	Eriophorum	k1gInSc1	Eriophorum
angustifolium	angustifolium	k1gNnSc1	angustifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svízel	svízel	k1gFnSc4	svízel
severní	severní	k2eAgMnSc1d1	severní
(	(	kIx(	(
<g/>
Galium	galium	k1gNnSc1	galium
boreale	boreale	k6eAd1	boreale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žluťucha	žluťucha	k1gFnSc1	žluťucha
lesklá	lesklý	k2eAgFnSc1d1	lesklá
(	(	kIx(	(
<g/>
Thalictrum	Thalictrum	k1gNnSc1	Thalictrum
lucidum	lucidum	k1gNnSc1	lucidum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kozlík	kozlík	k1gInSc4	kozlík
dvoudomý	dvoudomý	k2eAgInSc4d1	dvoudomý
(	(	kIx(	(
<g/>
Valeriana	Valeriana	k1gFnSc1	Valeriana
dioica	dioica	k1gMnSc1	dioica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violka	violka	k1gFnSc1	violka
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Viola	Viola	k1gFnSc1	Viola
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Rybník	rybník	k1gInSc1	rybník
Jíkavec	jíkavec	k1gMnSc1	jíkavec
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
rekreačního	rekreační	k2eAgInSc2d1	rekreační
areálu	areál	k1gInSc2	areál
Sklář	sklář	k1gMnSc1	sklář
Ostružno	Ostružno	k6eAd1	Ostružno
u	u	k7c2	u
Prachovských	prachovský	k2eAgFnPc2d1	Prachovská
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rybníku	rybník	k1gInSc6	rybník
je	být	k5eAaImIp3nS	být
provozováno	provozován	k2eAgNnSc1d1	provozováno
rybářské	rybářský	k2eAgNnSc1d1	rybářské
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
rybníka	rybník	k1gInSc2	rybník
vedou	vést	k5eAaImIp3nP	vést
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rybník	rybník	k1gInSc1	rybník
Jíkavec	jíkavec	k1gMnSc1	jíkavec
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc5	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
