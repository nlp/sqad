<p>
<s>
Pieve	Pieev	k1gFnPc1	Pieev
di	di	k?	di
Ledro	Ledro	k1gNnSc1	Ledro
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Ledro	Ledro	k1gNnSc1	Ledro
(	(	kIx(	(
<g/>
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
obec	obec	k1gFnSc1	obec
<g/>
)	)	kIx)	)
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Trentino	Trentin	k2eAgNnSc4d1	Trentino
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Valle	Valle	k1gFnSc2	Valle
di	di	k?	di
Ledro	Ledro	k1gNnSc1	Ledro
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
470	[number]	k4	470
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
36	[number]	k4	36
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Tridentu	Trident	k1gMnSc6	Trident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
leží	ležet	k5eAaImIp3nS	ležet
jezero	jezero	k1gNnSc4	jezero
Lago	Lago	k1gMnSc1	Lago
di	di	k?	di
Ledro	Ledro	k1gNnSc4	Ledro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
obce	obec	k1gFnSc2	obec
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1866	[number]	k4	1866
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
bitev	bitva	k1gFnPc2	bitva
II	II	kA	II
<g/>
.	.	kIx.	.
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Itálie	Itálie	k1gFnSc2	Itálie
mezi	mezi	k7c7	mezi
Rakousko-uherskou	rakouskoherský	k2eAgFnSc7d1	rakousko-uherská
monarchií	monarchie	k1gFnSc7	monarchie
a	a	k8xC	a
Italským	italský	k2eAgNnSc7d1	italské
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1915	[number]	k4	1915
byli	být	k5eAaImAgMnP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Pieve	Pieev	k1gFnSc2	Pieev
a	a	k8xC	a
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
evakuováni	evakuovat	k5eAaBmNgMnP	evakuovat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
pak	pak	k6eAd1	pak
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
mohli	moct	k5eAaImAgMnP	moct
vrátit	vrátit	k5eAaPmF	vrátit
až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
obyvatelé	obyvatel	k1gMnPc1	obyvatel
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
referendu	referendum	k1gNnSc6	referendum
schválili	schválit	k5eAaPmAgMnP	schválit
sloučení	sloučený	k2eAgMnPc1d1	sloučený
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
šesti	šest	k4xCc2	šest
obcí	obec	k1gFnPc2	obec
údolí	údolí	k1gNnSc2	údolí
Valle	Valle	k1gFnSc2	Valle
di	di	k?	di
Ledro	Ledro	k1gNnSc1	Ledro
do	do	k7c2	do
společné	společný	k2eAgFnSc2d1	společná
obce	obec	k1gFnSc2	obec
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ledro	Ledro	k1gNnSc1	Ledro
<g/>
.	.	kIx.	.
</s>
<s>
Sloučení	sloučení	k1gNnSc1	sloučení
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sporty	sport	k1gInPc4	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Pieve	Pieev	k1gFnSc2	Pieev
di	di	k?	di
Ledro	Ledro	k1gNnSc1	Ledro
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
П	П	k?	П
<g/>
'	'	kIx"	'
<g/>
є	є	k?	є
na	na	k7c6	na
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pieve	Pieev	k1gFnSc2	Pieev
di	di	k?	di
Ledro	Ledro	k1gNnSc4	Ledro
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
