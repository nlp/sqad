<s>
Pieve	Pieev	k1gFnPc1	Pieev
di	di	k?	di
Ledro	Ledro	k1gNnSc1	Ledro
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Ledro	Ledro	k1gNnSc1	Ledro
(	(	kIx(	(
<g/>
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
obec	obec	k1gFnSc1	obec
<g/>
)	)	kIx)	)
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Trentino	Trentin	k2eAgNnSc4d1	Trentino
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Valle	Valle	k1gFnSc2	Valle
di	di	k?	di
Ledro	Ledro	k1gNnSc1	Ledro
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
470	[number]	k4	470
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
36	[number]	k4	36
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Tridentu	Trident	k1gMnSc3	Trident
<g/>
.	.	kIx.	.
</s>
