<s>
TGV	TGV	kA
</s>
<s>
TGV	TGV	kA
Sud-Est	Sud-Est	k1gFnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgInSc4
vlak	vlak	k1gInSc4
používaný	používaný	k2eAgInSc4d1
ve	v	k7c6
službě	služba	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
TGV	TGV	kA
2N2	2N2	k4
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejnovější	nový	k2eAgFnSc1d3
varianta	varianta	k1gFnSc1
ve	v	k7c6
stanici	stanice	k1gFnSc6
Gare	Gar	k1gInSc2
de	de	k?
Lyon	Lyon	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čelní	čelní	k2eAgInSc1d1
vůz	vůz	k1gInSc1
prototypové	prototypový	k2eAgFnSc2d1
turbínové	turbínový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
TGV	TGV	kA
001	#num#	k4
</s>
<s>
TGV	TGV	kA
[	[	kIx(
<g/>
té	ten	k3xDgFnSc6
<g/>
:	:	kIx,
<g/>
žé	žé	k?
<g/>
:	:	kIx,
<g/>
vé	vé	k?
<g/>
:	:	kIx,
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
(	(	kIx(
<g/>
Train	Train	k2eAgInSc4d1
à	à	k?
Grande	grand	k1gMnSc5
Vitesse	Vitess	k1gMnSc5
<g/>
)	)	kIx)
francouzských	francouzský	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
drah	draha	k1gFnPc2
(	(	kIx(
<g/>
SNCF	SNCF	kA
<g/>
)	)	kIx)
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
obdobu	obdoba	k1gFnSc4
japonského	japonský	k2eAgInSc2d1
systému	systém	k1gInSc2
Šinkansen	Šinkansna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlaky	vlak	k1gInPc1
TGV	TGV	kA
jsou	být	k5eAaImIp3nP
elektricky	elektricky	k6eAd1
poháněná	poháněný	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
<g/>
,	,	kIx,
pohybující	pohybující	k2eAgInPc4d1
se	se	k3xPyFc4
po	po	k7c6
kolejích	kolej	k1gFnPc6
normálního	normální	k2eAgInSc2d1
rozchodu	rozchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
umožnění	umožnění	k1gNnSc4
vysoké	vysoký	k2eAgFnSc2d1
provozní	provozní	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
(	(	kIx(
<g/>
kolem	kolem	k7c2
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
však	však	k9
pro	pro	k7c4
ně	on	k3xPp3gFnPc4
zpravidla	zpravidla	k6eAd1
budovány	budován	k2eAgFnPc4d1
vysokorychlostní	vysokorychlostní	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
(	(	kIx(
<g/>
LGV	LGV	kA
–	–	k?
Ligne	Lign	k1gInSc5
à	à	k?
Grande	grand	k1gMnSc5
Vitesse	Vitess	k1gMnSc5
<g/>
)	)	kIx)
bez	bez	k7c2
úrovňových	úrovňový	k2eAgNnPc2d1
křížení	křížení	k1gNnPc2
a	a	k8xC
s	s	k7c7
mimořádně	mimořádně	k6eAd1
velkými	velký	k2eAgInPc7d1
poloměry	poloměr	k1gInPc7
oblouků	oblouk	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
přes	přes	k7c4
7000	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sklon	sklon	k1gInSc1
trati	trať	k1gFnSc2
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
35	#num#	k4
‰	‰	k?
<g/>
.	.	kIx.
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
byly	být	k5eAaImAgInP
ve	v	k7c4
Francii	Francie	k1gFnSc4
vyvíjeny	vyvíjet	k5eAaImNgFnP
od	od	k7c2
konce	konec	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
první	první	k4xOgFnSc1
speciální	speciální	k2eAgFnSc1d1
trať	trať	k1gFnSc1
TGV	TGV	kA
dána	dát	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc2
mezi	mezi	k7c7
Paříží	Paříž	k1gFnSc7
a	a	k8xC
Lyonem	Lyon	k1gInSc7
27	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
asi	asi	k9
550	#num#	k4
souprav	souprava	k1gFnPc2
TGV	TGV	kA
v	v	k7c6
různých	různý	k2eAgFnPc6d1
variantách	varianta	k1gFnPc6
<g/>
,	,	kIx,
vedle	vedle	k7c2
francouzských	francouzský	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
SNCF	SNCF	kA
je	on	k3xPp3gNnSc4
provozoval	provozovat	k5eAaImAgMnS
dopravce	dopravce	k1gMnSc1
Thalys	Thalysa	k1gFnPc2
<g/>
,	,	kIx,
Eurostar	Eurostara	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
úspěšné	úspěšný	k2eAgNnSc4d1
TGV	TGV	kA
navázal	navázat	k5eAaPmAgInS
roku	rok	k1gInSc2
1996	#num#	k4
dvoupodlažní	dvoupodlažní	k2eAgInSc1d1
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
(	(	kIx(
<g/>
AGV	AGV	kA
<g/>
)	)	kIx)
firmy	firma	k1gFnSc2
Alstom	Alstom	k1gInSc4
pod	pod	k7c7
značkou	značka	k1gFnSc7
Avelia	Avelium	k1gNnSc2
TGV	TGV	kA
Duplex	duplex	k1gInSc1
<g/>
,	,	kIx,
Euroduplex	Euroduplex	k1gInSc1
<g/>
,	,	kIx,
TGV	TGV	kA
2N2	2N2	k4
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jednotky	jednotka	k1gFnPc1
TGV	TGV	kA
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
ze	z	k7c2
dvou	dva	k4xCgInPc2
hnacích	hnací	k2eAgInPc2d1
(	(	kIx(
<g/>
čelních	čelní	k2eAgInPc2d1
<g/>
)	)	kIx)
a	a	k8xC
několika	několik	k4yIc7
hnaných	hnaný	k2eAgInPc2d1
(	(	kIx(
<g/>
vložených	vložený	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnací	hnací	k2eAgFnPc4d1
nápravy	náprava	k1gFnPc4
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
všechny	všechen	k3xTgFnPc4
nápravy	náprava	k1gFnPc4
hnacího	hnací	k2eAgInSc2d1
vozu	vůz	k1gInSc2
a	a	k8xC
někdy	někdy	k6eAd1
též	též	k9
sousedního	sousední	k2eAgInSc2d1
podvozku	podvozek	k1gInSc2
hnaného	hnaný	k2eAgInSc2d1
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vložené	vložený	k2eAgInPc1d1
vozy	vůz	k1gInPc1
spočívají	spočívat	k5eAaImIp3nP
na	na	k7c6
Jakobsových	Jakobsův	k2eAgInPc6d1
podvozcích	podvozek	k1gInPc6
(	(	kIx(
<g/>
tj.	tj.	kA
jeden	jeden	k4xCgInSc4
podvozek	podvozek	k1gInSc4
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
společný	společný	k2eAgInSc1d1
pro	pro	k7c4
dva	dva	k4xCgInPc4
sousedící	sousedící	k2eAgInPc4d1
vozy	vůz	k1gInPc4
<g/>
;	;	kIx,
netýká	týkat	k5eNaImIp3nS
se	se	k3xPyFc4
spřažení	spřažení	k1gNnSc2
s	s	k7c7
čelními	čelní	k2eAgInPc7d1
vozy	vůz	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předností	přednost	k1gFnSc7
tohoto	tento	k3xDgNnSc2
řešení	řešení	k1gNnSc2
je	být	k5eAaImIp3nS
snížení	snížení	k1gNnSc4
počtu	počet	k1gInSc2
dvojkolí	dvojkolí	k1gNnPc2
a	a	k8xC
větší	veliký	k2eAgFnSc4d2
bezpečnost	bezpečnost	k1gFnSc4
v	v	k7c6
případě	případ	k1gInSc6
vykolejení	vykolejení	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
spojené	spojený	k2eAgInPc1d1
vozy	vůz	k1gInPc1
méně	málo	k6eAd2
vybočují	vybočovat	k5eAaImIp3nP
z	z	k7c2
osy	osa	k1gFnSc2
koleje	kolej	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
nemožnost	nemožnost	k1gFnSc1
rozpojení	rozpojení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
TGV	TGV	kA
001	#num#	k4
byl	být	k5eAaImAgInS
poháněn	pohánět	k5eAaImNgInS
plynovou	plynový	k2eAgFnSc7d1
turbínou	turbína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvýšení	zvýšení	k1gNnSc1
cen	cena	k1gFnPc2
ropy	ropa	k1gFnSc2
během	během	k7c2
ropné	ropný	k2eAgFnSc2d1
krize	krize	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
však	však	k9
rozhodlo	rozhodnout	k5eAaPmAgNnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
elektrické	elektrický	k2eAgFnSc2d1
trakce	trakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
vlaků	vlak	k1gInPc2
je	být	k5eAaImIp3nS
napájena	napájet	k5eAaImNgFnS
soustavami	soustava	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
25	#num#	k4
kV	kV	k?
<g/>
/	/	kIx~
<g/>
50	#num#	k4
Hz	Hz	kA
<g/>
~	~	kIx~
(	(	kIx(
<g/>
vysokorychlostní	vysokorychlostní	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
a	a	k8xC
konvenční	konvenčnět	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
východě	východ	k1gInSc6
[	[	kIx(
Evian	Evian	k1gInSc1
les	les	k1gInSc1
Bains	Bains	k1gInSc4
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
</s>
<s>
1,5	1,5	k4
kV	kV	k?
<g/>
=	=	kIx~
(	(	kIx(
<g/>
konvenční	konvenční	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
východě	východ	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
mezistátní	mezistátní	k2eAgFnSc2d1
trati	trať	k1gFnSc2
do	do	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
[	[	kIx(
Bellegarde	Bellegard	k1gMnSc5
(	(	kIx(
<g/>
Ain	Ain	k1gMnSc6
<g/>
)	)	kIx)
–	–	k?
La	la	k1gNnSc1
Plaine	Plain	k1gInSc5
–	–	k?
Genéve	Genéev	k1gFnPc1
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
rovněž	rovněž	k9
</s>
<s>
3	#num#	k4
kV	kV	k?
<g/>
=	=	kIx~
(	(	kIx(
<g/>
pro	pro	k7c4
jízdy	jízda	k1gFnPc4
do	do	k7c2
Belgie	Belgie	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
</s>
<s>
15	#num#	k4
kV	kV	k?
<g/>
/	/	kIx~
<g/>
16,7	16,7	k4
Hz	Hz	kA
<g/>
~	~	kIx~
(	(	kIx(
<g/>
pro	pro	k7c4
jízdy	jízda	k1gFnPc4
do	do	k7c2
Německa	Německo	k1gNnSc2
a	a	k8xC
Švýcarska	Švýcarsko	k1gNnSc2
[	[	kIx(
Vallorbe	Vallorb	k1gInSc5
–	–	k?
Lausanne	Lausanne	k1gNnSc2
–	–	k?
Brig	briga	k1gFnPc2
<g/>
,	,	kIx,
Pontarlier	Pontarlira	k1gFnPc2
–	–	k?
Neuchâtel	Neuchâtel	k1gMnSc1
–	–	k?
Bern	Bern	k1gInSc1
–	–	k?
Curych	Curych	k1gInSc1
HB	HB	kA
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Varianty	varianta	k1gFnPc1
</s>
<s>
TGV	TGV	kA
001	#num#	k4
(	(	kIx(
<g/>
prototyp	prototyp	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
TGV	TGV	kA
Paris	Paris	k1gMnSc1
Sud-Est	Sud-Est	k1gMnSc1
</s>
<s>
TGV	TGV	kA
Atlantique	Atlantique	k1gFnSc1
</s>
<s>
AVE	ave	k1gNnSc1
S-100	S-100	k1gFnSc2
</s>
<s>
AVE	ave	k1gNnSc1
S-101	S-101	k1gMnSc1
Euromed	Euromed	k1gMnSc1
</s>
<s>
TGV	TGV	kA
La	la	k1gNnSc1
Poste	post	k1gInSc5
</s>
<s>
TGV	TGV	kA
Réseau	Réseaa	k1gMnSc4
</s>
<s>
Thalys	Thalysa	k1gFnPc2
PBA	PBA	kA
</s>
<s>
TGV	TGV	kA
Duplex	duplex	k1gInSc1
</s>
<s>
TGV	TGV	kA
Eurostar	Eurostar	k1gMnSc1
</s>
<s>
Thalys	Thalysa	k1gFnPc2
PBKA	PBKA	kA
</s>
<s>
KTX	KTX	kA
(	(	kIx(
<g/>
TGV	TGV	kA
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
TGV	TGV	kA
POS	POS	kA
</s>
<s>
AGV	AGV	kA
</s>
<s>
2	#num#	k4
soupravy	souprava	k1gFnPc1
PSE	pes	k1gMnSc5
v	v	k7c6
původním	původní	k2eAgNnSc6d1
zbarvení	zbarvení	k1gNnSc6
<g/>
,	,	kIx,
1987	#num#	k4
</s>
<s>
Souprava	souprava	k1gFnSc1
La	la	k1gNnSc2
Poste	post	k1gInSc5
</s>
<s>
Souprava	souprava	k1gFnSc1
Atlantique	Atlantiqu	k1gInSc2
č.	č.	k?
335	#num#	k4
na	na	k7c6
Gare	Gare	k1gNnSc6
Montparnasse	Montparnasse	k1gFnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
AVE	ave	k1gNnSc1
S-100	S-100	k1gFnSc2
č.	č.	k?
13	#num#	k4
u	u	k7c2
žst	žst	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madrid	Madrid	k1gInSc1
Puerta	Puert	k1gMnSc2
de	de	k?
Atocha	Atoch	k1gMnSc2
</s>
<s>
AVE	ave	k1gNnSc1
S-101	S-101	k1gMnSc1
Euromed	Euromed	k1gMnSc1
</s>
<s>
Dvoupodlažní	dvoupodlažní	k2eAgFnSc1d1
souprava	souprava	k1gFnSc1
TGV	TGV	kA
Duplex	duplex	k1gInSc1
(	(	kIx(
<g/>
TGV	TGV	kA
Euroduplex	Euroduplex	k1gInSc1
<g/>
)	)	kIx)
u	u	k7c2
města	město	k1gNnSc2
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
</s>
<s>
Tratě	trať	k1gFnPc1
</s>
<s>
Tratě	trata	k1gFnSc3
TGV	TGV	kA
<g/>
:	:	kIx,
</s>
<s>
LGV	LGV	kA
v	v	k7c6
Francii	Francie	k1gFnSc6
</s>
<s>
LGV	LGV	kA
mimo	mimo	k7c4
Francii	Francie	k1gFnSc4
</s>
<s>
standardní	standardní	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
s	s	k7c7
provozem	provoz	k1gInSc7
TGV	TGV	kA
</s>
<s>
Souprava	souprava	k1gFnSc1
Thalys	Thalysa	k1gFnPc2
PBA	PBA	kA
4537	#num#	k4
</s>
<s>
Tratě	trať	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
jednotky	jednotka	k1gFnPc1
TGV	TGV	kA
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
LGV	LGV	kA
(	(	kIx(
<g/>
ligne	lignout	k5eAaImIp3nS,k5eAaPmIp3nS
à	à	k?
grande	grand	k1gMnSc5
vitesse	vitess	k1gMnSc5
–	–	k?
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
ve	v	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
v	v	k7c6
přilehlém	přilehlý	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
v	v	k7c6
provozu	provoz	k1gInSc6
následující	následující	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
LGV	LGV	kA
Sud-Est	Sud-Est	k1gFnSc1
–	–	k?
Paříž	Paříž	k1gFnSc1
–	–	k?
Lyon	Lyon	k1gInSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1975-1983	1975-1983	k4
(	(	kIx(
<g/>
St.	st.	kA
Florentin	florentin	k1gInSc1
–	–	k?
Lyon	Lyon	k1gInSc1
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
538	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
270	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
LGV	LGV	kA
Atlantique	Atlantique	k1gFnSc1
–	–	k?
Paříž	Paříž	k1gFnSc1
–	–	k?
Le	Le	k1gFnSc1
Mans	Mans	k1gInSc1
<g/>
,	,	kIx,
Tours	Tours	k1gInSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1985	#num#	k4
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
282	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
;	;	kIx,
pokračovaní	pokračovaný	k2eAgMnPc1d1
Tours	Tours	k1gInSc4
–	–	k?
Rennes	Rennes	k1gInSc1
výstavba	výstavba	k1gFnSc1
2012	#num#	k4
až	až	k9
2017	#num#	k4
(	(	kIx(
<g/>
otevřeno	otevřít	k5eAaPmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
LGV	LGV	kA
Nord-Europe	Nord-Europ	k1gInSc5
–	–	k?
Paříž	Paříž	k1gFnSc1
–	–	k?
Lille	Lille	k1gFnSc1
–	–	k?
Calais	Calais	k1gNnSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
1993	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
333	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
LGV	LGV	kA
Rhône-Alpes	Rhône-Alpes	k1gMnSc1
–	–	k?
Montanay	Montanay	k1gInPc1
Junction	Junction	k1gInSc1
–	–	k?
Valence	valence	k1gFnSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
1994	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
115	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
LGV	LGV	kA
Méditerranée	Méditerranée	k1gNnPc2
–	–	k?
Valence	valence	k1gFnSc2
–	–	k?
Nîmes	Nîmes	k1gInSc1
<g/>
,	,	kIx,
Marseille	Marseille	k1gFnSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
2001	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
251	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
v	v	k7c6
úseku	úsek	k1gInSc6
Avignon	Avignon	k1gInSc1
–	–	k?
Aix-en-Provence	Aix-en-Provence	k1gFnSc2
povolena	povolen	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
320	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
LGV	LGV	kA
Belgium	Belgium	k1gNnSc1
/	/	kIx~
HSL	HSL	kA
1	#num#	k4
–	–	k?
Lille	Lille	k1gInSc1
–	–	k?
Brusel	Brusel	k1gInSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
1997	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
84	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
HSL-Zuid	HSL-Zuid	k1gInSc1
/	/	kIx~
HSL	HSL	kA
4	#num#	k4
–	–	k?
Antverpy	Antverpy	k1gFnPc4
–	–	k?
Amsterdam	Amsterdam	k1gInSc1
<g/>
;	;	kIx,
otevřena	otevřen	k2eAgFnSc1d1
v	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
LGV	LGV	kA
Est	Est	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
úsek	úsek	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Paříž	Paříž	k1gFnSc1
–	–	k?
Baudrecourt	Baudrecourt	k1gInSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
301	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
350	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
provoz	provoz	k1gInSc4
jen	jen	k9
na	na	k7c4
320	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
High-Speed	High-Speed	k1gInSc1
1	#num#	k4
–	–	k?
Folkestone	Folkeston	k1gInSc5
–	–	k?
Londýn	Londýn	k1gInSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
1994	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
109	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Trať	trať	k1gFnSc1
Haut-Bugey	Haut-Bugea	k1gFnSc2
–	–	k?
rekonstrukce	rekonstrukce	k1gFnSc2
tratě	trať	k1gFnSc2
mezi	mezi	k7c7
Bellegarde	Bellegard	k1gMnSc5
a	a	k8xC
Bourg-en-Bresse	Bourg-en-Bresse	k1gFnSc1
(	(	kIx(
<g/>
trasa	trasa	k1gFnSc1
Paříž	Paříž	k1gFnSc1
–	–	k?
Ženeva	Ženeva	k1gFnSc1
<g/>
]]	]]	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
otevřena	otevřen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
</s>
<s>
LGV	LGV	kA
Perpignan	Perpignan	k1gMnSc1
–	–	k?
Figueras	Figueras	k1gMnSc1
–	–	k?
Perpignan	Perpignan	k1gMnSc1
–	–	k?
Figueras	Figueras	k1gInSc1
(	(	kIx(
<g/>
Španělsko	Španělsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
44,4	44,4	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
350	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
</s>
<s>
LGV	LGV	kA
Rhin	Rhin	k1gMnSc1
–	–	k?
Rhône	Rhôn	k1gInSc5
(	(	kIx(
<g/>
východní	východní	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
)	)	kIx)
–	–	k?
Genlis	Genlis	k1gInSc1
–	–	k?
Lutterbach	Lutterbach	k1gInSc1
–	–	k?
délka	délka	k1gFnSc1
140	#num#	k4
km	km	kA
<g/>
,	,	kIx,
otevřeno	otevřít	k5eAaPmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
</s>
<s>
*	*	kIx~
LGV	LGV	kA
Est	Est	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
úsek	úsek	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Baudrecourt	Baudrecourt	k1gInSc1
–	–	k?
Strasbourg	Strasbourg	k1gInSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
106	#num#	k4
km	km	kA
<g/>
;	;	kIx,
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
350	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
provoz	provoz	k1gInSc4
jen	jen	k9
na	na	k7c4
320	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
–	–	k?
uvedení	uvedení	k1gNnSc1
do	do	k7c2
provozu	provoz	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
.	.	kIx.
</s>
<s>
LGV	LGV	kA
Sud	sud	k1gInSc1
Europe	Europ	k1gInSc5
Atlantique	Atlantique	k1gNnPc7
–	–	k?
Tours	Toursa	k1gFnPc2
–	–	k?
Bordeaux	Bordeaux	k1gNnSc1
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
zahájena	zahájen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
302	#num#	k4
km	km	kA
<g/>
;	;	kIx,
otevřena	otevřen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
Postupně	postupně	k6eAd1
byly	být	k5eAaImAgFnP
zprovozněny	zprovozněn	k2eAgFnPc1d1
i	i	k9
další	další	k2eAgFnSc7d1
tratí	trať	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
</s>
<s>
LGV	LGV	kA
Rhin	Rhin	k1gMnSc1
–	–	k?
Rhône	Rhôn	k1gInSc5
–	–	k?
Lyon	Lyon	k1gInSc1
–	–	k?
Dijon	Dijon	k1gInSc1
–	–	k?
Mylhúzy	Mylhúza	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Lyon	Lyon	k1gInSc1
Turin	Turina	k1gFnPc2
Ferroviaire	Ferroviair	k1gInSc5
–	–	k?
Lyon	Lyon	k1gInSc1
–	–	k?
Turín	Turín	k1gInSc1
</s>
<s>
Rekordy	rekord	k1gInPc1
</s>
<s>
Souprava	souprava	k1gFnSc1
AGV	AGV	kA
na	na	k7c6
cerhenickém	cerhenický	k2eAgInSc6d1
testovacím	testovací	k2eAgInSc6d1
okruhu	okruh	k1gInSc6
</s>
<s>
Rekordní	rekordní	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
TGV	TGV	kA
</s>
<s>
Souprava	souprava	k1gFnSc1
Thalys	Thalysa	k1gFnPc2
PBKA	PBKA	kA
4345	#num#	k4
</s>
<s>
Již	již	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1972	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
jednotka	jednotka	k1gFnSc1
TGV	TGV	kA
001	#num#	k4
s	s	k7c7
plynovou	plynový	k2eAgFnSc7d1
turbínou	turbína	k1gFnSc7
rychlosti	rychlost	k1gFnSc2
318	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
světový	světový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
vozidel	vozidlo	k1gNnPc2
s	s	k7c7
termickým	termický	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1981	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
jednotka	jednotka	k1gFnSc1
TGV	TGV	kA
PSE	pes	k1gMnSc5
č.	č.	k?
16	#num#	k4
na	na	k7c6
trati	trať	k1gFnSc6
Paříž	Paříž	k1gFnSc1
–	–	k?
Lyon	Lyon	k1gInSc1
rychlosti	rychlost	k1gFnSc2
380	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
překonala	překonat	k5eAaPmAgFnS
tak	tak	k6eAd1
francouzský	francouzský	k2eAgInSc4d1
rekord	rekord	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
–	–	k?
331	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Hranice	hranice	k1gFnSc1
400	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
byla	být	k5eAaImAgFnS
překonána	překonat	k5eAaPmNgFnS
německou	německý	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
ICE	ICE	kA
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
již	již	k6eAd1
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
opět	opět	k6eAd1
převzali	převzít	k5eAaPmAgMnP
štafetu	štafeta	k1gFnSc4
Francouzi	Francouz	k1gMnPc1
–	–	k?
jednotka	jednotka	k1gFnSc1
„	„	k?
<g/>
TGV-Atlantique	TGV-Atlantiqu	k1gFnSc2
<g/>
“	“	k?
č.	č.	k?
325	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
rychlosti	rychlost	k1gFnPc4
482,4	482,4	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1990	#num#	k4
dokonce	dokonce	k9
515,3	515,3	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
O	o	k7c6
možnostech	možnost	k1gFnPc6
francouzského	francouzský	k2eAgInSc2d1
systému	systém	k1gInSc2
svědčí	svědčit	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
TGV	TGV	kA
POS	POS	kA
č.	č.	k?
4402	#num#	k4
drží	držet	k5eAaImIp3nS
světový	světový	k2eAgInSc4d1
rychlostní	rychlostní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
pro	pro	k7c4
kolejová	kolejový	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
(	(	kIx(
<g/>
rychleji	rychle	k6eAd2
jel	jet	k5eAaImAgInS
jen	jen	k9
magnetický	magnetický	k2eAgInSc1d1
vlak	vlak	k1gInSc1
maglev	maglet	k5eAaPmDgInS
<g/>
,	,	kIx,
583	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
)	)	kIx)
574,8	574,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
dosažený	dosažený	k2eAgInSc4d1
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
na	na	k7c6
trati	trať	k1gFnSc6
Paříž	Paříž	k1gFnSc4
–	–	k?
Baudrecourt	Baudrecourta	k1gFnPc2
u	u	k7c2
vesničky	vesnička	k1gFnSc2
Passavant-en-Argonne	Passavant-en-Argonn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlak	vlak	k1gInSc1
tím	ten	k3xDgNnSc7
vylepšil	vylepšit	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
nedávný	dávný	k2eNgInSc4d1
rekord	rekord	k1gInSc4
553	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ze	z	k7c2
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
a	a	k8xC
předchozí	předchozí	k2eAgInSc4d1
vytvořený	vytvořený	k2eAgInSc4d1
TGV	TGV	kA
Atlantique	Atlantique	k1gInSc1
č.	č.	k?
325	#num#	k4
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jel	jet	k5eAaImAgMnS
na	na	k7c6
trati	trať	k1gFnSc6
Paříž	Paříž	k1gFnSc1
–	–	k?
Tours	Tours	k1gInSc1
poblíž	poblíž	k6eAd1
Vendôme	Vendôm	k1gInSc5
rychlostí	rychlost	k1gFnSc7
515,3	515,3	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Další	další	k2eAgInSc4d1
pozoruhodný	pozoruhodný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
předvedla	předvést	k5eAaPmAgFnS
souprava	souprava	k1gFnSc1
TGV	TGV	kA
č.	č.	k?
531	#num#	k4
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
překonala	překonat	k5eAaPmAgFnS
vzdálenost	vzdálenost	k1gFnSc4
napříč	napříč	k7c7
Francií	Francie	k1gFnSc7
z	z	k7c2
Calais	Calais	k1gNnSc2
do	do	k7c2
Marseille	Marseille	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
0	#num#	k4
<g/>
67,2	67,2	k4
km	km	kA
<g/>
)	)	kIx)
za	za	k7c4
3	#num#	k4
hodiny	hodina	k1gFnSc2
a	a	k8xC
29	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
průměrnou	průměrný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
278,4	278,4	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Na	na	k7c6
překonání	překonání	k1gNnSc6
1	#num#	k4
000	#num#	k4
km	km	kA
s	s	k7c7
letmým	letmý	k2eAgInSc7d1
startem	start	k1gInSc7
tak	tak	k6eAd1
potřebovala	potřebovat	k5eAaImAgFnS
pouhé	pouhý	k2eAgFnSc2d1
3	#num#	k4
hodiny	hodina	k1gFnSc2
a	a	k8xC
9	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
dosáhla	dosáhnout	k5eAaPmAgFnS
průměrnou	průměrný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
314,5	314,5	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Rychlost	rychlost	k1gFnSc1
na	na	k7c6
celé	celý	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
1067,2	1067,2	k4
km	km	kA
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc1d2
<g/>
,	,	kIx,
protože	protože	k8xS
čas	čas	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
počáteční	počáteční	k2eAgInSc1d1
rozjezd	rozjezd	k1gInSc1
a	a	k8xC
konečné	konečný	k2eAgNnSc1d1
brzdění	brzdění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
<g/>
:	:	kIx,
SuperCity	SuperCit	k1gInPc1
Pendolino	Pendolina	k1gFnSc5
Českých	český	k2eAgFnPc2d1
drah	draha	k1gFnPc2
jede	jet	k5eAaImIp3nS
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
250	#num#	k4
km	km	kA
cesty	cesta	k1gFnSc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Olomouce	Olomouc	k1gFnSc2
s	s	k7c7
jednou	jeden	k4xCgFnSc7
zastávkou	zastávka	k1gFnSc7
2	#num#	k4
hodiny	hodina	k1gFnPc1
a	a	k8xC
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
průměrnou	průměrný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
120,2	120,2	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
stav	stav	k1gInSc1
k	k	k7c3
roku	rok	k1gInSc3
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Thalys	Thalysa	k1gFnPc2
News	News	k1gInSc4
<g/>
↑	↑	k?
LGV	LGV	kA
Est	Est	k1gMnSc1
Phase	Phase	k1gFnSc2
2	#num#	k4
opening	opening	k1gInSc1
completes	completes	k1gMnSc1
Paris	Paris	k1gMnSc1
–	–	k?
Strasbourg	Strasbourg	k1gMnSc1
high	high	k1gMnSc1
speed	speed	k1gMnSc1
line	linout	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railway	Railwaum	k1gNnPc7
Gazette	Gazett	k1gInSc5
<g/>
,	,	kIx,
2016-07-04	2016-07-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Railteam	Railteam	k6eAd1
</s>
<s>
TGV	TGV	kA
Duplex	duplex	k1gInSc1
</s>
<s>
Turbotrain	Turbotrain	k1gMnSc1
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
TGV	TGV	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
TGV	TGV	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
TGV	TGV	kA
na	na	k7c4
Rail	Rail	k1gInSc4
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
Rekordy	rekord	k1gInPc1
TGV	TGV	kA
na	na	k7c6
Parostroj-i	Parostroj-	k1gFnSc6
</s>
<s>
TGV	TGV	kA
na	na	k7c6
Railweb-u	Railweb-us	k1gInSc6
</s>
<s>
TGV	TGV	kA
na	na	k7c4
Vysokorychlostni-zeleznice	Vysokorychlostni-zeleznice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
>	>	kIx)
<g/>
249	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
AGV	AGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
AVE	ave	k1gNnSc4
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
401	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
450	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
CRH	CRH	kA
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
460	#num#	k4
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
480	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
500	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
600	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
610	#num#	k4
(	(	kIx(
<g/>
CH	Ch	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
Eurostar	Eurostar	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
<g/>
)	)	kIx)
•	•	k?
ICE	ICE	kA
(	(	kIx(
<g/>
D	D	kA
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
A	A	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
E	E	kA
/	/	kIx~
RU	RU	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
KTX	KTX	kA
(	(	kIx(
<g/>
KR	KR	kA
<g/>
)	)	kIx)
•	•	k?
RENFE	RENFE	kA
S-120	S-120	k1gFnSc2
(	(	kIx(
<g/>
E	E	kA
/	/	kIx~
TR	TR	kA
<g/>
)	)	kIx)
•	•	k?
Šinkansen	Šinkansen	k1gInSc1
(	(	kIx(
<g/>
J	J	kA
/	/	kIx~
TW	TW	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
Talgo	Talgo	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
TGV	TGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
D	D	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
I	I	kA
/	/	kIx~
E	E	kA
/	/	kIx~
B	B	kA
/	/	kIx~
LX	LX	kA
<g/>
)	)	kIx)
•	•	k?
Thalys	Thalysa	k1gFnPc2
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
D	D	kA
<g/>
)	)	kIx)
•	•	k?
V	v	k7c6
250	#num#	k4
(	(	kIx(
<g/>
B	B	kA
/	/	kIx~
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Zefiro	Zefiro	k1gNnSc4
(	(	kIx(
<g/>
CN	CN	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
</s>
<s>
Transrapid	Transrapid	k1gInSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
D	D	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
JR-Maglev	JR-Maglev	k1gFnSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
•	•	k?
Swissmetro	Swissmetro	k1gNnSc1
<g/>
**	**	k?
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
)	)	kIx)
*	*	kIx~
<g/>
technologie	technologie	k1gFnSc1
Maglev	Maglev	k1gMnSc1
**	**	k?
<g/>
Maglev	Maglev	k1gMnSc1
ve	v	k7c6
vakuovém	vakuový	k2eAgInSc6d1
tunelu	tunel	k1gInSc6
(	(	kIx(
<g/>
Vactrain	Vactrain	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
</s>
