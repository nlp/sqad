<s>
Hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
organizmus	organizmus	k1gInSc4	organizmus
(	(	kIx(	(
<g/>
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
živočich	živočich	k1gMnSc1	živočich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
domovem	domov	k1gInSc7	domov
jinému	jiný	k1gMnSc3	jiný
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
cizopasí	cizopasit	k5eAaImIp3nP	cizopasit
<g/>
.	.	kIx.	.
</s>
