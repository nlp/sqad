<s>
Jaký	jaký	k3yRgInSc1	jaký
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
domovem	domov	k1gInSc7	domov
jiného	jiný	k2eAgInSc2d1	jiný
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
cizopasí	cizopasit	k5eAaImIp3nP	cizopasit
<g/>
?	?	kIx.	?
</s>
