<s>
Hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
organizmus	organizmus	k1gInSc4	organizmus
(	(	kIx(	(
<g/>
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
živočich	živočich	k1gMnSc1	živočich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
domovem	domov	k1gInSc7	domov
jinému	jiný	k1gMnSc3	jiný
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
cizopasí	cizopasit	k5eAaImIp3nP	cizopasit
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
parazit	parazit	k1gMnSc1	parazit
pohlavně	pohlavně	k6eAd1	pohlavně
dospívá	dospívat	k5eAaImIp3nS	dospívat
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
hostiteli	hostitel	k1gMnPc7	hostitel
existují	existovat	k5eAaImIp3nP	existovat
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
parazita	parazit	k1gMnSc2	parazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vývojový	vývojový	k2eAgInSc1d1	vývojový
cyklus	cyklus	k1gInSc1	cyklus
parazita	parazit	k1gMnSc2	parazit
začíná	začínat	k5eAaImIp3nS	začínat
i	i	k8xC	i
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
parazitů	parazit	k1gMnPc2	parazit
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
vývojem	vývoj	k1gInSc7	vývoj
je	být	k5eAaImIp3nS	být
definitivní	definitivní	k2eAgMnSc1d1	definitivní
hostitel	hostitel	k1gMnSc1	hostitel
jediným	jediný	k2eAgMnSc7d1	jediný
hostitelem	hostitel	k1gMnSc7	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hostitel	hostitel	k1gMnSc1	hostitel
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
druh	druh	k1gInSc4	druh
parazita	parazit	k1gMnSc2	parazit
netypický	typický	k2eNgMnSc1d1	netypický
<g/>
,	,	kIx,	,
parazit	parazit	k1gMnSc1	parazit
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
většinou	většina	k1gFnSc7	většina
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
určitým	určitý	k2eAgMnSc7d1	určitý
parazitem	parazit	k1gMnSc7	parazit
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
u	u	k7c2	u
aberatních	aberatní	k2eAgMnPc2d1	aberatní
hostitelů	hostitel	k1gMnPc2	hostitel
odlišně	odlišně	k6eAd1	odlišně
než	než	k8xS	než
u	u	k7c2	u
definitivního	definitivní	k2eAgMnSc2d1	definitivní
hostitele	hostitel	k1gMnSc2	hostitel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyšší	vysoký	k2eAgFnPc1d2	vyšší
či	či	k8xC	či
nižší	nízký	k2eAgFnSc1d2	nižší
patogenita	patogenita	k1gFnSc1	patogenita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
stojí	stát	k5eAaImIp3nS	stát
mimo	mimo	k7c4	mimo
vývojový	vývojový	k2eAgInSc4d1	vývojový
cyklus	cyklus	k1gInSc4	cyklus
parazita	parazit	k1gMnSc2	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Parazit	parazit	k1gMnSc1	parazit
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
hostiteli	hostitel	k1gMnSc6	hostitel
nijak	nijak	k6eAd1	nijak
nevyvíjí	vyvíjet	k5eNaImIp3nS	vyvíjet
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
definitivního	definitivní	k2eAgMnSc2d1	definitivní
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Paratenický	Paratenický	k2eAgMnSc1d1	Paratenický
hostitel	hostitel	k1gMnSc1	hostitel
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
parazitu	parazit	k1gMnSc3	parazit
přenos	přenos	k1gInSc1	přenos
do	do	k7c2	do
definitivního	definitivní	k2eAgMnSc2d1	definitivní
hostitele	hostitel	k1gMnSc2	hostitel
a	a	k8xC	a
nebo	nebo	k8xC	nebo
chrání	chránit	k5eAaImIp3nP	chránit
parazita	parazit	k1gMnSc4	parazit
před	před	k7c7	před
nepříznivým	příznivý	k2eNgNnSc7d1	nepříznivé
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
rezervoárový	rezervoárový	k2eAgMnSc1d1	rezervoárový
hostitel	hostitel	k1gMnSc1	hostitel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
i	i	k8xC	i
jiném	jiný	k2eAgInSc6d1	jiný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
"	"	kIx"	"
<g/>
rezervoárovým	rezervoárův	k2eAgMnSc7d1	rezervoárův
hostitelem	hostitel	k1gMnSc7	hostitel
trichinelózy	trichinelóza	k1gFnSc2	trichinelóza
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
chápe	chápat	k5eAaImIp3nS	chápat
pod	pod	k7c7	pod
rezerovoárovým	rezerovoárův	k2eAgMnSc7d1	rezerovoárův
hostitelem	hostitel	k1gMnSc7	hostitel
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
parazit	parazit	k1gMnSc1	parazit
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
od	od	k7c2	od
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
nakazit	nakazit	k5eAaPmF	nakazit
jiní	jiný	k2eAgMnPc1d1	jiný
hostitelé	hostitel	k1gMnPc1	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
virologii	virologie	k1gFnSc6	virologie
a	a	k8xC	a
bakterilogii	bakterilogie	k1gFnSc6	bakterilogie
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc4	termín
rezervoár	rezervoár	k1gInSc1	rezervoár
viru	vir	k1gInSc2	vir
<g/>
,	,	kIx,	,
baktérie	baktérie	k1gFnPc1	baktérie
<g/>
.	.	kIx.	.
</s>
<s>
Mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hostitel	hostitel	k1gMnSc1	hostitel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
