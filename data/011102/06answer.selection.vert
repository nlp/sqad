<s>
Saint-Martin	Saint-Martin	k1gInSc1	Saint-Martin
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
oficiálně	oficiálně	k6eAd1	oficiálně
Collectivité	Collectivitý	k2eAgNnSc4d1	Collectivitý
de	de	k?	de
Saint-Martin	Saint-Martin	k1gInSc4	Saint-Martin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zámořské	zámořský	k2eAgNnSc1d1	zámořské
společenství	společenství	k1gNnSc1	společenství
Francie	Francie	k1gFnSc2	Francie
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Svatý	svatý	k1gMnSc1	svatý
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
patřícího	patřící	k2eAgMnSc2d1	patřící
do	do	k7c2	do
karibského	karibský	k2eAgNnSc2d1	Karibské
souostroví	souostroví	k1gNnSc2	souostroví
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
<g/>
.	.	kIx.	.
</s>
