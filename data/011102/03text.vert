<p>
<s>
Saint-Martin	Saint-Martin	k1gInSc1	Saint-Martin
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
oficiálně	oficiálně	k6eAd1	oficiálně
Collectivité	Collectivitý	k2eAgNnSc4d1	Collectivitý
de	de	k?	de
Saint-Martin	Saint-Martin	k1gInSc4	Saint-Martin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zámořské	zámořský	k2eAgNnSc1d1	zámořské
společenství	společenství	k1gNnSc1	společenství
Francie	Francie	k1gFnSc2	Francie
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Svatý	svatý	k1gMnSc1	svatý
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
patřícího	patřící	k2eAgMnSc2d1	patřící
do	do	k7c2	do
karibského	karibský	k2eAgNnSc2d1	Karibské
souostroví	souostroví	k1gNnSc2	souostroví
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
francouzského	francouzský	k2eAgInSc2d1	francouzský
zámořského	zámořský	k2eAgInSc2d1	zámořský
departementu	departement	k1gInSc2	departement
Guadeloupe	Guadeloupe	k1gFnSc2	Guadeloupe
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Marigot	Marigot	k1gInSc1	Marigot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Sint	Sint	k1gInSc1	Sint
Maarten	Maartno	k1gNnPc2	Maartno
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
