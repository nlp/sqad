<p>
<s>
Nipigon	Nipigon	k1gNnSc1	Nipigon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lake	Lake	k1gFnSc7	Lake
Nipigon	Nipigon	k1gNnSc4	Nipigon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
v	v	k7c4	v
provincii	provincie	k1gFnSc4	provincie
Ontario	Ontario	k1gNnSc4	Ontario
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ledovcovo-tektonického	ledovcovoektonický	k2eAgInSc2d1	ledovcovo-tektonický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
4800	[number]	k4	4800
km2	km2	k4	km2
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
4848	[number]	k4	4848
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
260	[number]	k4	260
m.	m.	k?	m.
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc2d1	maximální
hloubky	hloubka	k1gFnSc2	hloubka
165	[number]	k4	165
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
celé	celý	k2eAgNnSc1d1	celé
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc4	Ontario
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
připočítávané	připočítávaný	k2eAgNnSc1d1	připočítávaný
jako	jako	k8xS	jako
šesté	šestý	k4xOgNnSc1	šestý
k	k	k7c3	k
Velkým	velký	k2eAgNnPc3d1	velké
jezerům	jezero	k1gNnPc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
120	[number]	k4	120
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Thunder	Thundra	k1gFnPc2	Thundra
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
členité	členitý	k2eAgNnSc1d1	členité
a	a	k8xC	a
pro	pro	k7c4	pro
jezero	jezero	k1gNnSc4	jezero
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
rozeklané	rozeklaný	k2eAgInPc1d1	rozeklaný
útesy	útes	k1gInPc1	útes
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
má	mít	k5eAaImIp3nS	mít
zelenočernou	zelenočerný	k2eAgFnSc4d1	zelenočerná
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
minerálu	minerál	k1gInSc2	minerál
pyroxenu	pyroxen	k1gInSc2	pyroxen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostrovy	ostrov	k1gInPc1	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Caribou	Cariba	k1gFnSc7	Cariba
<g/>
,	,	kIx,	,
Geikie	Geikie	k1gFnSc1	Geikie
<g/>
,	,	kIx,	,
Katatota	Katatota	k1gFnSc1	Katatota
<g/>
,	,	kIx,	,
Kelvin	kelvin	k1gInSc1	kelvin
<g/>
,	,	kIx,	,
Logan	Logan	k1gInSc1	Logan
<g/>
,	,	kIx,	,
Murchison	Murchison	k1gNnSc1	Murchison
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnPc1	Murraa
<g/>
,	,	kIx,	,
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
ústí	ústí	k1gNnSc1	ústí
řeky	řeka	k1gFnSc2	řeka
Windigo	Windigo	k1gNnSc1	Windigo
<g/>
,	,	kIx,	,
Ogoki	Ogoki	k1gNnSc1	Ogoki
<g/>
,	,	kIx,	,
Kopka	kopka	k1gFnSc1	kopka
<g/>
,	,	kIx,	,
Ombabika	Ombabika	k1gFnSc1	Ombabika
<g/>
,	,	kIx,	,
Ogaman	Ogaman	k1gMnSc1	Ogaman
<g/>
,	,	kIx,	,
Blackwater	Blackwater	k1gMnSc1	Blackwater
<g/>
,	,	kIx,	,
Gull	Gull	k1gMnSc1	Gull
<g/>
.	.	kIx.	.
</s>
<s>
Odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
řeka	řek	k1gMnSc2	řek
Nipigon	Nipigon	k1gMnSc1	Nipigon
do	do	k7c2	do
Nipigonského	Nipigonský	k2eAgInSc2d1	Nipigonský
zálivu	záliv	k1gInSc2	záliv
Hořejšího	Hořejšího	k2eAgNnSc2d1	Hořejšího
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
je	být	k5eAaImIp3nS	být
nejvodnějším	vodný	k2eAgInSc7d3	vodný
přítokem	přítok	k1gInSc7	přítok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
sob	sob	k1gMnSc1	sob
polární	polární	k2eAgInSc1d1	polární
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Lake	Lak	k1gFnSc2	Lak
Nipigon	Nipigona	k1gFnPc2	Nipigona
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
List	list	k1gInSc1	list
of	of	k?	of
rivers	rivers	k1gInSc1	rivers
of	of	k?	of
Ontario	Ontario	k1gNnSc1	Ontario
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Н	Н	k?	Н
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
