<s>
Řecko-perské	řecko-perský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Řecko-perské	řecko-perský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Perský	perský	k2eAgMnSc1d1
pěšák	pěšák	k1gMnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
při	při	k7c6
boji	boj	k1gInSc6
s	s	k7c7
řeckým	řecký	k2eAgMnSc7d1
hoplítou	hoplíta	k1gMnSc7
<g/>
,	,	kIx,
výjev	výjev	k1gInSc4
na	na	k7c6
kylixu	kylix	k1gInSc6
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
499	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
-	-	kIx~
449	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
(	(	kIx(
<g/>
51	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
a	a	k8xC
Egypt	Egypt	k1gInSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Iónské	iónský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
maloasijských	maloasijský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
proti	proti	k7c3
perské	perský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Zmaření	zmaření	k1gNnSc1
perských	perský	k2eAgFnPc2d1
snah	snaha	k1gFnPc2
o	o	k7c6
podrobení	podrobení	k1gNnSc6
Řecka	Řecko	k1gNnSc2
a	a	k8xC
osvobození	osvobození	k1gNnSc2
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
řecké	řecký	k2eAgFnPc1d1
poleis	poleis	k1gFnPc1
</s>
<s>
Athény	Athéna	k1gFnPc1
</s>
<s>
Lakedaimón	Lakedaimón	k1gInSc1
Lakedaimón	Lakedaimón	k1gInSc1
</s>
<s>
Thespiae	Thespiae	k6eAd1
</s>
<s>
Théby	Théby	k1gFnPc1
</s>
<s>
Megara	Megara	k1gFnSc1
</s>
<s>
Korint	Korint	k1gMnSc1
</s>
<s>
Tegejsko	Tegejsko	k6eAd1
</s>
<s>
Plataje	Platat	k5eAaImSgMnS
</s>
<s>
různé	různý	k2eAgFnPc1d1
další	další	k2eAgFnPc1d1
poleis	poleis	k1gFnPc1
</s>
<s>
ostatní	ostatní	k2eAgInPc1d1
řecké	řecký	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
spolky	spolek	k1gInPc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Délský	Délský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Perská	perský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
a	a	k8xC
spojenecké	spojenecký	k2eAgInPc1d1
podřízené	podřízený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Halikarnassos	Halikarnassos	k1gMnSc1
</s>
<s>
Thesálie	Thesálie	k1gFnSc1
</s>
<s>
Bojótie	Bojótie	k1gFnSc1
</s>
<s>
Théby	Théby	k1gFnPc1
</s>
<s>
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Miltiadés	Miltiadés	k1gInSc1
Themistoklés	Themistoklés	k1gInSc1
Leónidás	Leónidás	k1gInSc4
I.	I.	kA
†	†	k?
Pausaniás	Pausaniás	k1gInSc1
Kimón	Kimón	k1gMnSc1
†	†	k?
PeriklésOnesilos	PeriklésOnesilos	k1gMnSc1
†	†	k?
Xanthippos	Xanthippos	k1gMnSc1
Aristeidés	Aristeidésa	k1gFnPc2
Kallimachos	Kallimachos	k1gMnSc1
†	†	k?
EurybiadésArimnestos	EurybiadésArimnestos	k1gMnSc1
</s>
<s>
Artafernés	Artafernés	k6eAd1
Dátis	Dátis	k1gFnSc1
Artafernés	Artafernés	k1gInSc4
mladší	mladý	k2eAgInSc1d2
Xerxés	Xerxés	k1gInSc1
I.	I.	kA
Mardonios	Mardonios	k1gInSc1
†	†	k?
Hydarnés	Hydarnés	k1gInSc1
Artabazos	Artabazos	k1gMnSc1
MegabyzusArtemisia	MegabyzusArtemisia	k1gFnSc1
I.	I.	kA
z	z	k7c2
Kárie	Kárie	k1gFnSc2
Hippias	Hippias	k1gInSc1
<g/>
,	,	kIx,
sesazený	sesazený	k2eAgMnSc1d1
tyran	tyran	k1gMnSc1
</s>
<s>
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
série	série	k1gFnSc1
konfliktů	konflikt	k1gInPc2
mezi	mezi	k7c7
řeckými	řecký	k2eAgInPc7d1
městskými	městský	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
perskou	perský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
již	již	k6eAd1
začaly	začít	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
499	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
trvala	trvat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
449	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Jejich	jejich	k3xOp3gFnSc4
příčina	příčina	k1gFnSc1
tkvěla	tkvět	k5eAaImAgFnS
ve	v	k7c6
snaze	snaha	k1gFnSc6
perských	perský	k2eAgInPc2d1
velkokrálů	velkokrál	k1gInPc2
Dáreia	Dáreium	k1gNnSc2
I.	I.	kA
a	a	k8xC
obzvláště	obzvláště	k6eAd1
Xerxa	Xerxes	k1gMnSc4
I.	I.	kA
podmanit	podmanit	k5eAaPmF
si	se	k3xPyFc3
vojenskou	vojenský	k2eAgFnSc4d1
silou	síla	k1gFnSc7
starověké	starověký	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
úsilí	úsilí	k1gNnSc1
však	však	k9
navzdory	navzdory	k7c3
veškeré	veškerý	k3xTgFnSc3
materiální	materiální	k2eAgFnSc3d1
a	a	k8xC
lidské	lidský	k2eAgFnSc3d1
převaze	převaha	k1gFnSc3
Persie	Persie	k1gFnSc2
skončilo	skončit	k5eAaPmAgNnS
fiaskem	fiasko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšná	úspěšný	k2eAgFnSc1d1
třebaže	třebaže	k8xS
vyčerpávající	vyčerpávající	k2eAgFnSc1d1
obrana	obrana	k1gFnSc1
Řecka	Řecko	k1gNnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
představách	představa	k1gFnPc6
vítězných	vítězný	k2eAgMnPc2d1
Helénů	Helén	k1gMnPc2
takřka	takřka	k6eAd1
zmytizována	zmytizovat	k5eAaPmNgFnS
a	a	k8xC
řecké	řecký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
našlo	najít	k5eAaPmAgNnS
svůj	svůj	k3xOyFgInSc4
výraz	výraz	k1gInSc4
i	i	k9
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
umění	umění	k1gNnSc6
a	a	k8xC
kultuře	kultura	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
Aischylos	Aischylos	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
drama	drama	k1gNnSc4
Peršané	Peršan	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
mýtus	mýtus	k1gInSc1
zčásti	zčásti	k6eAd1
přetrval	přetrvat	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
i	i	k9
v	v	k7c6
časté	častý	k2eAgFnSc6d1
interpretaci	interpretace	k1gFnSc6
tohoto	tento	k3xDgInSc2
konfliktu	konflikt	k1gInSc2
jako	jako	k8xC,k8xS
obrany	obrana	k1gFnSc2
svobodné	svobodný	k2eAgFnSc2d1
západní	západní	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
před	před	k7c7
násilnou	násilný	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
orientální	orientální	k2eAgFnSc2d1
despocie	despocie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Událostí	událost	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
rozpoutání	rozpoutání	k1gNnSc3
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
tzv.	tzv.	kA
iónské	iónský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
probíhající	probíhající	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
499	#num#	k4
až	až	k9
493	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc7
vrcholem	vrchol	k1gInSc7
pak	pak	k6eAd1
bitvy	bitva	k1gFnPc1
u	u	k7c2
Marathónu	Marathón	k1gInSc2
(	(	kIx(
<g/>
490	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
a	a	k8xC
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
(	(	kIx(
<g/>
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porážka	porážka	k1gFnSc1
Peršanů	peršan	k1gInPc2
měla	mít	k5eAaImAgFnS
dalekosáhlé	dalekosáhlý	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
pro	pro	k7c4
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
perských	perský	k2eAgFnPc2d1
<g/>
,	,	kIx,
řeckých	řecký	k2eAgFnPc2d1
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
také	také	k6eAd1
evropských	evropský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějším	důležitý	k2eAgInSc7d3
soudobým	soudobý	k2eAgInSc7d1
pramenem	pramen	k1gInSc7
popisujícím	popisující	k2eAgInSc7d1
průběh	průběh	k1gInSc4
válek	válka	k1gFnPc2
jsou	být	k5eAaImIp3nP
Hérodotovy	Hérodotův	k2eAgFnPc1d1
Dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
k	k	k7c3
jimi	on	k3xPp3gMnPc7
předkládaným	předkládaný	k2eAgInPc3d1
údajům	údaj	k1gInPc3
je	být	k5eAaImIp3nS
ale	ale	k8xC
třeba	třeba	k6eAd1
přihlížet	přihlížet	k5eAaImF
se	s	k7c7
značnou	značný	k2eAgFnSc7d1
opatrností	opatrnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc4
konfliktu	konflikt	k1gInSc2
</s>
<s>
Poměry	poměr	k1gInPc1
Řeků	Řek	k1gMnPc2
v	v	k7c6
Asii	Asie	k1gFnSc6
</s>
<s>
Národem	národ	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
si	se	k3xPyFc3
částečně	částečně	k6eAd1
podrobil	podrobit	k5eAaPmAgMnS
Řeky	Řek	k1gMnPc4
<g/>
,	,	kIx,
obývající	obývající	k2eAgInSc4d1
asijský	asijský	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
Lýdové	Lýdus	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc1
král	král	k1gMnSc1
Alyattés	Alyattésa	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
vedl	vést	k5eAaImAgMnS
již	již	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
válku	válka	k1gFnSc4
proti	proti	k7c3
Mílétu	Míléto	k1gNnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
skončila	skončit	k5eAaPmAgFnS
uzavřením	uzavření	k1gNnSc7
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
si	se	k3xPyFc3
Mílétos	Mílétos	k1gInSc1
zachoval	zachovat	k5eAaPmAgInS
vnitřní	vnitřní	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
zahraniční	zahraniční	k2eAgFnSc6d1
politice	politika	k1gFnSc6
následoval	následovat	k5eAaImAgInS
Lýdii	Lýdia	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
proto	proto	k6eAd1
Míléťané	Míléťan	k1gMnPc1
zúčastnili	zúčastnit	k5eAaPmAgMnP
lýdské	lýdský	k2eAgFnPc4d1
války	válka	k1gFnPc4
proti	proti	k7c3
Médům	Méd	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Alyattovi	Alyatt	k1gMnSc6
usedl	usednout	k5eAaPmAgMnS
na	na	k7c4
lýdský	lýdský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
560	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
jeho	jeho	k3xOp3gFnSc7
syn	syn	k1gMnSc1
Kroisos	Kroisos	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
porazil	porazit	k5eAaPmAgMnS
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
přinutil	přinutit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
k	k	k7c3
placení	placení	k1gNnSc3
tributu	tribut	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
od	od	k7c2
rozšiřování	rozšiřování	k1gNnSc2
svého	svůj	k3xOyFgInSc2
vlivu	vliv	k1gInSc2
dále	daleko	k6eAd2
na	na	k7c4
západ	západ	k1gInSc4
upustil	upustit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
554	#num#	k4
<g/>
/	/	kIx~
<g/>
553	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
íránský	íránský	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
Persidy	Persida	k1gFnSc2
Kýros	Kýros	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliký	veliký	k2eAgInSc1d1
vzbouřil	vzbouřit	k5eAaPmAgMnS
proti	proti	k7c3
svému	svůj	k3xOyFgMnSc3
médskému	médský	k2eAgMnSc3d1
suverénovi	suverén	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
bojů	boj	k1gInPc2
si	se	k3xPyFc3
Kýros	Kýros	k1gInSc1
Médii	Médie	k1gFnSc6
podmanil	podmanit	k5eAaPmAgMnS
a	a	k8xC
založil	založit	k5eAaPmAgMnS
tak	tak	k6eAd1
perskou	perský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroisos	Kroisos	k1gMnSc1
se	se	k3xPyFc4
proti	proti	k7c3
rostoucí	rostoucí	k2eAgFnSc3d1
perské	perský	k2eAgFnSc3d1
moci	moc	k1gFnSc3
rozhodl	rozhodnout	k5eAaPmAgInS
zasáhnout	zasáhnout	k5eAaPmF
<g/>
,	,	kIx,
ovšem	ovšem	k9
dříve	dříve	k6eAd2
než	než	k8xS
tak	tak	k6eAd1
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
dotázal	dotázat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
radu	rada	k1gFnSc4
proslulé	proslulý	k2eAgFnSc2d1
věštírny	věštírna	k1gFnSc2
v	v	k7c6
Delfách	Delfy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
vzkázala	vzkázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
překročí	překročit	k5eAaPmIp3nS
<g/>
-li	-li	k?
řeku	řeka	k1gFnSc4
Halys	Halysa	k1gFnPc2
<g/>
,	,	kIx,
zničí	zničit	k5eAaPmIp3nS
velkou	velký	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
spokojený	spokojený	k2eAgMnSc1d1
s	s	k7c7
odpovědí	odpověď	k1gFnSc7
neprohlédl	prohlédnout	k5eNaPmAgMnS
dvojsmyslnost	dvojsmyslnost	k1gFnSc1
sdělení	sdělení	k1gNnSc2
a	a	k8xC
vytáhl	vytáhnout	k5eAaPmAgMnS
do	do	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
utrpěl	utrpět	k5eAaPmAgInS
zničující	zničující	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
541	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vstoupil	vstoupit	k5eAaPmAgMnS
Kýros	Kýros	k1gInSc4
do	do	k7c2
Sard	Sardy	k1gFnPc2
<g/>
,	,	kIx,
Kroisova	Kroisův	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Perská	perský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Kýros	Kýros	k1gMnSc1
již	již	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
konfliktu	konflikt	k1gInSc2
vyzval	vyzvat	k5eAaPmAgMnS
maloasijské	maloasijský	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
připojily	připojit	k5eAaPmAgInP
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
Mílétu	Mílét	k1gInSc2
však	však	k9
zůstala	zůstat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
žádost	žádost	k1gFnSc1
nevyslyšena	vyslyšen	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Lýdie	Lýdia	k1gFnSc2
Peršany	peršan	k1gInPc1
vyslaly	vyslat	k5eAaPmAgInP
řecké	řecký	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
ke	k	k7c3
Kýrovi	Kýra	k1gMnSc3
posly	posel	k1gMnPc7
a	a	k8xC
žádaly	žádat	k5eAaImAgInP
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
jeho	jeho	k3xOp3gFnPc7
poddanými	poddaná	k1gFnPc7
za	za	k7c2
týchž	týž	k3xTgFnPc2
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
jaké	jaký	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jim	on	k3xPp3gMnPc3
uložil	uložit	k5eAaPmAgInS
Kroisos	Kroisos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
ale	ale	k9
nezapomněl	zapomnět	k5eNaImAgMnS,k5eNaPmAgMnS
Řekům	Řek	k1gMnPc3
jejich	jejich	k3xOp3gFnSc4
dřívější	dřívější	k2eAgNnSc1d1
odmítnutí	odmítnutí	k1gNnSc1
<g/>
,	,	kIx,
vyslance	vyslanec	k1gMnSc2
s	s	k7c7
opovržením	opovržení	k1gNnSc7
propustil	propustit	k5eAaPmAgMnS
a	a	k8xC
jakýkoli	jakýkoli	k3yIgInSc4
odpor	odpor	k1gInSc4
Řeků	Řek	k1gMnPc2
neváhal	váhat	k5eNaImAgMnS
krutě	krutě	k6eAd1
potrestat	potrestat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Kýrově	Kýrův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
nástupcem	nástupce	k1gMnSc7
Kambýsés	Kambýsés	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
podle	podle	k7c2
Hérodota	Hérodot	k1gMnSc2
cítil	cítit	k5eAaImAgMnS
k	k	k7c3
Řekům	Řek	k1gMnPc3
respekt	respekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
525	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
podnikl	podniknout	k5eAaPmAgMnS
Kambýsés	Kambýsés	k1gInSc4
úspěšné	úspěšný	k2eAgNnSc4d1
tažení	tažení	k1gNnSc4
proti	proti	k7c3
Egyptu	Egypt	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
ho	on	k3xPp3gMnSc4
podporovala	podporovat	k5eAaImAgNnP
také	také	k9
kyperská	kyperský	k2eAgNnPc1d1
města	město	k1gNnPc1
a	a	k8xC
Polykratés	Polykratés	k1gInSc1
ze	z	k7c2
Samu	Samos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vítězství	vítězství	k1gNnSc1
zhoršilo	zhoršit	k5eAaPmAgNnS
vztahy	vztah	k1gInPc4
mezi	mezi	k7c4
Řeky	Řek	k1gMnPc4
a	a	k8xC
Peršany	Peršan	k1gMnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
dosavadní	dosavadní	k2eAgInSc1d1
řecký	řecký	k2eAgInSc1d1
obchod	obchod	k1gInSc1
s	s	k7c7
Egyptem	Egypt	k1gInSc7
získali	získat	k5eAaPmAgMnP
nyní	nyní	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
rukou	ruka	k1gFnPc2
Féničané	Féničan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perští	perský	k2eAgMnPc1d1
satrapové	satrap	k1gMnPc1
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
dosazovali	dosazovat	k5eAaImAgMnP
do	do	k7c2
řeckých	řecký	k2eAgNnPc2d1
měst	město	k1gNnPc2
tyrany	tyran	k1gMnPc4
a	a	k8xC
své	svůj	k3xOyFgFnPc4
posádky	posádka	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
prostřednictvím	prostřednictví	k1gNnSc7
vymáhali	vymáhat	k5eAaImAgMnP
z	z	k7c2
Řeků	Řek	k1gMnPc2
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
výprava	výprava	k1gFnSc1
nového	nový	k2eAgMnSc2d1
perského	perský	k2eAgMnSc2d1
vládce	vládce	k1gMnSc2
Dáreia	Dáreium	k1gNnSc2
I.	I.	kA
proti	proti	k7c3
Skythům	Skyth	k1gMnPc3
v	v	k7c6
roce	rok	k1gInSc6
512	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
upevnění	upevnění	k1gNnSc2
perské	perský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
v	v	k7c6
Thrákii	Thrákie	k1gFnSc6
a	a	k8xC
Makedonii	Makedonie	k1gFnSc6
vedly	vést	k5eAaImAgFnP
k	k	k7c3
zablokování	zablokování	k1gNnSc3
přístupů	přístup	k1gInPc2
Řeků	Řek	k1gMnPc2
k	k	k7c3
Černému	černý	k2eAgNnSc3d1
moři	moře	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
další	další	k2eAgInSc4d1
negativní	negativní	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
hospodářství	hospodářství	k1gNnSc4
Iónů	Ión	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perská	perský	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
se	se	k3xPyFc4
dotýkala	dotýkat	k5eAaImAgFnS
ekonomických	ekonomický	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
maloasijských	maloasijský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
ohrožovala	ohrožovat	k5eAaImAgFnS
svobodu	svoboda	k1gFnSc4
a	a	k8xC
nezávislost	nezávislost	k1gFnSc4
všech	všecek	k3xTgFnPc2
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Iónské	iónský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Iónské	iónský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Povstání	povstání	k1gNnSc1
iónských	iónský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
bylo	být	k5eAaImAgNnS
vyvoláno	vyvolat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
499	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
mílétským	mílétský	k2eAgMnSc7d1
tyranem	tyran	k1gMnSc7
Aristagorem	Aristagor	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
s	s	k7c7
pomocí	pomoc	k1gFnSc7
satrapy	satrapa	k1gMnSc2
Artaferna	Artaferna	k1gFnSc1
pokusil	pokusit	k5eAaPmAgMnS
obsadit	obsadit	k5eAaPmF
ostrov	ostrov	k1gInSc4
Naxos	Naxos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
výprava	výprava	k1gFnSc1
neuspěla	uspět	k5eNaPmAgFnS
<g/>
,	,	kIx,
Aristagorás	Aristagorás	k1gInSc1
<g/>
,	,	kIx,
obávající	obávající	k2eAgNnSc4d1
se	se	k3xPyFc4
potrestání	potrestání	k1gNnSc4
<g/>
,	,	kIx,
podnítil	podnítit	k5eAaPmAgMnS
povstání	povstání	k1gNnSc3
nespokojených	spokojený	k2eNgMnPc2d1
Iónů	Ión	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
záhy	záhy	k6eAd1
vypudili	vypudit	k5eAaPmAgMnP
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
měst	město	k1gNnPc2
tyrany	tyran	k1gMnPc4
ustavené	ustavený	k2eAgInPc1d1
Peršany	peršan	k1gInPc1
a	a	k8xC
vytvořili	vytvořit	k5eAaPmAgMnP
spolek	spolek	k1gInSc4
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
mluvčí	mluvčí	k1gMnPc1
požádali	požádat	k5eAaPmAgMnP
evropské	evropský	k2eAgFnPc4d1
Řeky	řeka	k1gFnPc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aristagorás	Aristagorás	k1gInSc1
se	se	k3xPyFc4
ucházel	ucházet	k5eAaImAgInS
o	o	k7c4
podporu	podpora	k1gFnSc4
spartského	spartský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Kleomena	Kleomen	k2eAgNnPc4d1
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
sice	sice	k8xC
Iónům	Ión	k1gMnPc3
nakloněn	nakloněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Sparťané	Sparťan	k1gMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
nezapojovat	zapojovat	k5eNaImF
se	se	k3xPyFc4
do	do	k7c2
tak	tak	k6eAd1
vzdáleného	vzdálený	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aristagorás	Aristagorás	k1gInSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
obrátil	obrátit	k5eAaPmAgInS
na	na	k7c4
Athéňany	Athéňan	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
následně	následně	k6eAd1
do	do	k7c2
Asie	Asie	k1gFnSc2
vyslali	vyslat	k5eAaPmAgMnP
společně	společně	k6eAd1
s	s	k7c7
Eretrijci	Eretrijce	k1gMnPc7
dvacet	dvacet	k4xCc4
resp.	resp.	kA
pět	pět	k4xCc4
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
využil	využít	k5eAaPmAgMnS
Dáreios	Dáreios	k1gMnSc1
jako	jako	k8xC,k8xS
záminky	záminka	k1gFnPc1
k	k	k7c3
tažení	tažení	k1gNnSc3
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
podpoře	podpora	k1gFnSc3
flotily	flotila	k1gFnSc2
se	se	k3xPyFc4
vzpoura	vzpoura	k1gFnSc1
brzy	brzy	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
podél	podél	k7c2
celého	celý	k2eAgNnSc2d1
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
také	také	k9
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
498	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
dobyli	dobýt	k5eAaPmAgMnP
Řekové	Řek	k1gMnPc1
Sardy	Sardy	k1gFnPc4
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc4
perského	perský	k2eAgMnSc2d1
místodržitele	místodržitel	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
poté	poté	k6eAd1
vypálili	vypálit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
tím	ten	k3xDgNnSc7
na	na	k7c4
sebe	sebe	k3xPyFc4
přivolali	přivolat	k5eAaPmAgMnP
Dáreiův	Dáreiův	k2eAgInSc4d1
hněv	hněv	k1gInSc4
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
byly	být	k5eAaImAgFnP
v	v	k7c6
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
zmobilizovány	zmobilizován	k2eAgInPc4d1
enormní	enormní	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
při	při	k7c6
zpátečním	zpáteční	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
byli	být	k5eAaImAgMnP
Řekové	Řek	k1gMnPc1
napadeni	napadnout	k5eAaPmNgMnP
a	a	k8xC
rozdrceni	rozdrtit	k5eAaPmNgMnP
perským	perský	k2eAgInPc3d1
vojskem	vojsko	k1gNnSc7
nedaleko	nedaleko	k7c2
Efesu	Efes	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
incidentu	incident	k1gInSc6
Athéňané	Athéňan	k1gMnPc1
odpluli	odplout	k5eAaPmAgMnP
domů	dům	k1gInPc2
a	a	k8xC
nadále	nadále	k6eAd1
se	se	k3xPyFc4
nepodíleli	podílet	k5eNaImAgMnP
na	na	k7c6
vojenských	vojenský	k2eAgFnPc6d1
operacích	operace	k1gFnPc6
v	v	k7c6
Iónii	Iónie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
494	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
připlulo	připlout	k5eAaPmAgNnS
k	k	k7c3
Mílétu	Mílét	k1gInSc3
600	#num#	k4
fénických	fénický	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Ládé	Ládá	k1gFnSc2
utkaly	utkat	k5eAaPmAgInP
s	s	k7c7
Řeky	Řek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
nižší	nízký	k2eAgInSc4d2
počet	počet	k1gInSc4
iónských	iónský	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vítězství	vítězství	k1gNnSc1
nejprve	nejprve	k6eAd1
klonilo	klonit	k5eAaImAgNnS
na	na	k7c4
stranu	strana	k1gFnSc4
Řeků	Řek	k1gMnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
50	#num#	k4
samských	samský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
70	#num#	k4
z	z	k7c2
Lesbu	Lesbos	k1gInSc2
uprchlo	uprchnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
ztracena	ztratit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fénické	fénický	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
následně	následně	k6eAd1
přivodily	přivodit	k5eAaPmAgInP,k5eAaBmAgInP
povstalcům	povstalec	k1gMnPc3
drtivou	drtivý	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mílétos	Mílétos	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
ze	z	k7c2
země	zem	k1gFnSc2
a	a	k8xC
z	z	k7c2
moře	moře	k1gNnSc2
dobyt	dobyt	k2eAgMnSc1d1
a	a	k8xC
srovnán	srovnán	k2eAgInSc1d1
se	se	k3xPyFc4
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
bylo	být	k5eAaImAgNnS
povstání	povstání	k1gNnSc1
fakticky	fakticky	k6eAd1
ukončeno	ukončit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Dáreiova	Dáreiův	k2eAgFnSc1d1
invaze	invaze	k1gFnSc1
</s>
<s>
Perské	perský	k2eAgFnPc1d1
invaze	invaze	k1gFnPc1
do	do	k7c2
Řecka	Řecko	k1gNnSc2
během	během	k7c2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
493	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgNnPc1d1
místa	místo	k1gNnPc4
odporu	odpor	k1gInSc3
zdolána	zdolat	k5eAaPmNgFnS
perskou	perský	k2eAgFnSc7d1
flotilou	flotila	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dáreios	Dáreios	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
nespokojil	spokojit	k5eNaPmAgMnS
s	s	k7c7
porážkou	porážka	k1gFnSc7
maloasijských	maloasijský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
využil	využít	k5eAaPmAgMnS
povstání	povstání	k1gNnSc4
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
své	svůj	k3xOyFgFnSc2
moci	moc	k1gFnSc2
na	na	k7c4
řecké	řecký	k2eAgFnPc4d1
obce	obec	k1gFnPc4
v	v	k7c6
Egejském	egejský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
a	a	k8xC
v	v	k7c6
Propontidě	Propontida	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mnohé	mnohý	k2eAgFnPc1d1
z	z	k7c2
nich	on	k3xPp3gFnPc2
dříve	dříve	k6eAd2
nepodléhaly	podléhat	k5eNaImAgFnP
Peršanům	peršan	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Mílétos	Mílétos	k1gInSc1
byl	být	k5eAaImAgInS
vypleněn	vyplenit	k5eAaPmNgInS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc4
chrámy	chrám	k1gInPc4
zbořeny	zbořen	k2eAgInPc4d1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
přesídleno	přesídlen	k2eAgNnSc4d1
do	do	k7c2
Mezopotámie	Mezopotámie	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
iónskými	iónský	k2eAgInPc7d1
městskými	městský	k2eAgInPc7d1
státy	stát	k1gInPc7
zacházeli	zacházet	k5eAaImAgMnP
Peršané	Peršan	k1gMnPc1
o	o	k7c6
poznání	poznání	k1gNnSc6
shovívavěji	shovívavě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obnovením	obnovení	k1gNnSc7
klidu	klid	k1gInSc2
v	v	k7c6
regionu	region	k1gInSc6
pověřil	pověřit	k5eAaPmAgMnS
Dáreios	Dáreios	k1gInSc4
svého	svůj	k3xOyFgMnSc2
zetě	zeť	k1gMnSc2
Mardonia	Mardonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
zmírnil	zmírnit	k5eAaPmAgMnS
daňovou	daňový	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
většině	většina	k1gFnSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
byly	být	k5eAaImAgFnP
na	na	k7c4
místo	místo	k1gNnSc4
nepopulárních	populární	k2eNgMnPc2d1
tyranů	tyran	k1gMnPc2
nastoleny	nastolit	k5eAaPmNgInP
demokratické	demokratický	k2eAgInPc1d1
režimy	režim	k1gInPc1
<g/>
,	,	kIx,
také	také	k9
zajatci	zajatec	k1gMnPc1
byli	být	k5eAaImAgMnP
propuštěni	propustit	k5eAaPmNgMnP
domů	domů	k6eAd1
a	a	k8xC
sám	sám	k3xTgMnSc1
Dáreios	Dáreios	k1gMnSc1
podněcoval	podněcovat	k5eAaImAgMnS
perské	perský	k2eAgMnPc4d1
šlechtice	šlechtic	k1gMnPc4
v	v	k7c6
přejímání	přejímání	k1gNnSc6
řeckých	řecký	k2eAgInPc2d1
náboženských	náboženský	k2eAgInPc2d1
obřadů	obřad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochované	dochovaný	k2eAgInPc4d1
záznamy	záznam	k1gInPc4
z	z	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
nasvědčují	nasvědčovat	k5eAaImIp3nP
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
perská	perský	k2eAgFnSc1d1
a	a	k8xC
řecká	řecký	k2eAgFnSc1d1
aristokracie	aristokracie	k1gFnSc1
spolu	spolu	k6eAd1
uzavíraly	uzavírat	k5eAaPmAgFnP,k5eAaImAgFnP
četné	četný	k2eAgInPc4d1
manželské	manželský	k2eAgInPc4d1
svazky	svazek	k1gInPc4
a	a	k8xC
dětem	dítě	k1gFnPc3
z	z	k7c2
nich	on	k3xPp3gMnPc6
vzešlým	vzešlý	k2eAgFnPc3d1
byla	být	k5eAaImAgNnP
dávána	dáván	k2eAgNnPc1d1
řecká	řecký	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dáreiova	Dáreiův	k2eAgFnSc1d1
smířlivá	smířlivý	k2eAgFnSc1d1
politika	politika	k1gFnSc1
byla	být	k5eAaImAgFnS
však	však	k9
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
pouze	pouze	k6eAd1
propagandistickým	propagandistický	k2eAgInSc7d1
tahem	tah	k1gInSc7
namířeným	namířený	k2eAgInSc7d1
proti	proti	k7c3
pevninským	pevninský	k2eAgMnPc3d1
Řekům	Řek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
491	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyslal	vyslat	k5eAaPmAgMnS
Dáreios	Dáreios	k1gInSc4
své	svůj	k3xOyFgMnPc4
emisary	emisar	k1gMnPc4
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zde	zde	k6eAd1
požadovali	požadovat	k5eAaImAgMnP
„	„	k?
<g/>
vodu	voda	k1gFnSc4
a	a	k8xC
zemi	zem	k1gFnSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
symbol	symbol	k1gInSc1
podřízenosti	podřízenost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Polis	Polis	k1gFnSc1
perskou	perský	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
uznala	uznat	k5eAaPmAgFnS
<g/>
,	,	kIx,
odmítla	odmítnout	k5eAaPmAgFnS
pouze	pouze	k6eAd1
některá	některý	k3yIgNnPc1
města	město	k1gNnPc1
včetně	včetně	k7c2
Sparty	Sparta	k1gFnSc2
a	a	k8xC
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
perské	perský	k2eAgFnPc1d1
vyslance	vyslanec	k1gMnSc2
usmrtily	usmrtit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Mardoniova	Mardoniův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
492	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
v	v	k7c6
Kilíkii	Kilíkie	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
shromažďovat	shromažďovat	k5eAaImF
expediční	expediční	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
pod	pod	k7c7
Mardoniovým	Mardoniový	k2eAgNnSc7d1
velením	velení	k1gNnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
podrobení	podrobení	k1gNnSc1
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
pěší	pěší	k2eAgInPc4d1
sbory	sbor	k1gInPc4
poslal	poslat	k5eAaPmAgMnS
Mardonios	Mardonios	k1gMnSc1
k	k	k7c3
Helléspontu	Helléspont	k1gMnSc3
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
prozatím	prozatím	k6eAd1
zdržel	zdržet	k5eAaPmAgInS
při	při	k7c6
egejském	egejský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Iónie	Iónie	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
dorazilo	dorazit	k5eAaPmAgNnS
jeho	jeho	k3xOp3gFnSc4
loďstvo	loďstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
odstranil	odstranit	k5eAaPmAgMnS
tyrany	tyran	k1gMnPc4
<g/>
,	,	kIx,
místo	místo	k7c2
nichž	jenž	k3xRgInPc2
ustavil	ustavit	k5eAaPmAgInS
v	v	k7c6
iónských	iónský	k2eAgNnPc6d1
městech	město	k1gNnPc6
lidové	lidový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
Mardonios	Mardonios	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
k	k	k7c3
Helléspontu	Helléspont	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
překročení	překročení	k1gNnSc6
si	se	k3xPyFc3
perské	perský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
během	během	k7c2
postupu	postup	k1gInSc2
Thrákií	Thrákie	k1gFnPc2
a	a	k8xC
Makedonií	Makedonie	k1gFnPc2
(	(	kIx(
<g/>
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
perské	perský	k2eAgNnSc4d1
říši	říš	k1gFnSc6
poplatné	poplatný	k2eAgInPc1d1
už	už	k6eAd1
v	v	k7c6
dřívějších	dřívější	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
<g/>
)	)	kIx)
podrobilo	podrobit	k5eAaPmAgNnS
všechny	všechen	k3xTgInPc4
místní	místní	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thrákie	Thrákie	k1gFnSc1
byla	být	k5eAaImAgFnS
reorganizována	reorganizovat	k5eAaBmNgFnS
v	v	k7c6
satrapii	satrapie	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Makedonie	Makedonie	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
perským	perský	k2eAgMnSc7d1
vazalem	vazal	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
loďstvo	loďstvo	k1gNnSc1
dobylo	dobýt	k5eAaPmAgNnS
ostrov	ostrov	k1gInSc4
Thasos	Thasos	k1gInSc1
a	a	k8xC
dosáhlo	dosáhnout	k5eAaPmAgNnS
poloostrova	poloostrov	k1gInSc2
Athós	Athós	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
obeplutí	obeplutí	k1gNnSc6
bylo	být	k5eAaImAgNnS
zastiženo	zastižen	k2eAgNnSc1d1
bouří	bouř	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
zničeno	zničit	k5eAaPmNgNnS
300	#num#	k4
lodí	loď	k1gFnPc2
a	a	k8xC
20	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
ztratilo	ztratit	k5eAaPmAgNnS
své	svůj	k3xOyFgInPc4
životy	život	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	dlouho	k6eNd1
nato	nato	k6eAd1
byl	být	k5eAaImAgMnS
Mardonios	Mardonios	k1gMnSc1
zraněn	zranit	k5eAaPmNgMnS
při	při	k7c6
boji	boj	k1gInSc6
s	s	k7c7
thráckým	thrácký	k2eAgInSc7d1
kmenem	kmen	k1gInSc7
Brygů	Bryg	k1gInPc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
oslabeným	oslabený	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
vrátil	vrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
na	na	k7c4
asijský	asijský	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Marathónu	Marathón	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Marathónu	Marathón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Perští	perský	k2eAgMnPc1d1
lučištníci	lučištník	k1gMnPc1
s	s	k7c7
kopím	kopí	k1gNnSc7
<g/>
,	,	kIx,
detail	detail	k1gInSc1
vlysu	vlys	k1gInSc2
Dáreiova	Dáreiův	k2eAgInSc2d1
paláce	palác	k1gInSc2
v	v	k7c6
Susách	Susy	k1gFnPc6
<g/>
,	,	kIx,
Louvre	Louvre	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
490	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
sestavili	sestavit	k5eAaPmAgMnP
vojevůdci	vojevůdce	k1gMnPc1
Dátis	Dátis	k1gFnPc2
a	a	k8xC
Artafernés	Artafernésa	k1gFnPc2
na	na	k7c4
perské	perský	k2eAgInPc4d1
poměry	poměr	k1gInPc4
sice	sice	k8xC
nepříliš	příliš	k6eNd1
početné	početný	k2eAgFnPc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
obzvláště	obzvláště	k6eAd1
vybrané	vybraný	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
perského	perský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
tyto	tento	k3xDgFnPc4
síly	síla	k1gFnPc4
přepraveny	přepraven	k2eAgFnPc4d1
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měly	mít	k5eAaImAgFnP
ztrestat	ztrestat	k5eAaPmF
Athény	Athéna	k1gFnPc4
a	a	k8xC
Eretrii	Eretrie	k1gFnSc4
za	za	k7c4
jejich	jejich	k3xOp3gFnSc4
podporu	podpora	k1gFnSc4
Iónům	Ión	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loďstvo	loďstvo	k1gNnSc1
vyplulo	vyplout	k5eAaPmAgNnS
z	z	k7c2
Kilíkie	Kilíkie	k1gFnSc2
k	k	k7c3
Rhodu	Rhodos	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Dátis	Dátis	k1gInSc1
neúspěšně	úspěšně	k6eNd1
pokusil	pokusit	k5eAaPmAgInS
dobýt	dobýt	k5eAaPmF
město	město	k1gNnSc4
Lyndos	Lyndosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Rhodu	Rhodos	k1gInSc2
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
plavila	plavit	k5eAaImAgFnS
dále	daleko	k6eAd2
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
490	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
stanuli	stanout	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
na	na	k7c6
Samu	Samos	k1gInSc6
a	a	k8xC
nedlouho	dlouho	k6eNd1
nato	nato	k6eAd1
se	se	k3xPyFc4
vylodili	vylodit	k5eAaPmAgMnP
na	na	k7c6
ostrově	ostrov	k1gInSc6
Naxos	Naxos	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
obyvatelstvo	obyvatelstvo	k1gNnSc1
uprchlo	uprchnout	k5eAaPmAgNnS
do	do	k7c2
hor.	hor.	k?
Peršané	Peršan	k1gMnPc1
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
dobývání	dobývání	k1gNnSc6
dalších	další	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
až	až	k9
posléze	posléze	k6eAd1
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
ostrova	ostrov	k1gInSc2
Euboia	Euboium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
ležící	ležící	k2eAgFnSc1d1
Eretrie	Eretrie	k1gFnSc1
se	se	k3xPyFc4
šest	šest	k4xCc1
dnů	den	k1gInPc2
statečně	statečně	k6eAd1
bránila	bránit	k5eAaImAgFnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
sedmý	sedmý	k4xOgInSc4
den	den	k1gInSc4
otevřeli	otevřít	k5eAaPmAgMnP
eretrijští	eretrijský	k2eAgMnPc1d1
zrádci	zrádce	k1gMnPc1
brány	brána	k1gFnSc2
města	město	k1gNnSc2
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškeré	veškerý	k3xTgInPc4
mužské	mužský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
pak	pak	k6eAd1
uvrženo	uvrhnout	k5eAaPmNgNnS
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
,	,	kIx,
město	město	k1gNnSc1
srovnáno	srovnán	k2eAgNnSc1d1
se	s	k7c7
zemí	zem	k1gFnSc7
a	a	k8xC
chrámy	chrám	k1gInPc1
a	a	k8xC
svatyně	svatyně	k1gFnPc1
vypleněny	vypleněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Eretrie	Eretrie	k1gFnSc2
překročili	překročit	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
v	v	k7c6
září	září	k1gNnSc6
490	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
mořskou	mořský	k2eAgFnSc4d1
úžinu	úžina	k1gFnSc4
mezi	mezi	k7c7
Euboiou	Euboia	k1gFnSc7
a	a	k8xC
pevninským	pevninský	k2eAgNnSc7d1
Řeckem	Řecko	k1gNnSc7
a	a	k8xC
na	na	k7c4
radu	rada	k1gFnSc4
Hippia	Hippius	k1gMnSc2
<g/>
,	,	kIx,
někdejšího	někdejší	k2eAgMnSc2d1
athénského	athénský	k2eAgMnSc2d1
tyrana	tyran	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
u	u	k7c2
Dáreia	Dáreius	k1gMnSc2
azyl	azyl	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
vylodili	vylodit	k5eAaPmAgMnP
v	v	k7c6
Attice	Attika	k1gFnSc6
poblíž	poblíž	k7c2
osady	osada	k1gFnSc2
Marathón	Marathón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
odehrát	odehrát	k5eAaPmF
první	první	k4xOgNnSc4
významné	významný	k2eAgNnSc4d1
střetnutí	střetnutí	k1gNnSc4
mezi	mezi	k7c4
Řeky	Řek	k1gMnPc4
a	a	k8xC
Peršany	Peršan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitevní	bitevní	k2eAgFnSc1d1
pole	pole	k1gFnSc1
tvořila	tvořit	k5eAaImAgFnS
horami	hora	k1gFnPc7
obklopená	obklopený	k2eAgFnSc1d1
<g/>
,	,	kIx,
kamenitá	kamenitý	k2eAgFnSc1d1
a	a	k8xC
bažinatá	bažinatý	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
<g/>
,	,	kIx,
nepříliš	příliš	k6eNd1
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
perské	perský	k2eAgNnSc4d1
jezdectvo	jezdectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
disponovali	disponovat	k5eAaBmAgMnP
kolem	kolem	k7c2
10	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gMnPc2
a	a	k8xC
několika	několik	k4yIc7
desítkami	desítka	k1gFnPc7
tisíc	tisíc	k4xCgInPc2
pěších	pěší	k2eAgMnPc2d1
lučištníků	lučištník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
jejich	jejich	k3xOp3gNnSc2
vojska	vojsko	k1gNnSc2
odhadují	odhadovat	k5eAaImIp3nP
moderní	moderní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
na	na	k7c4
20	#num#	k4
000	#num#	k4
až	až	k9
60	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezprostředně	bezprostředně	k6eAd1
ohrožení	ohrožený	k2eAgMnPc1d1
Athéňané	Athéňan	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
běžce	běžec	k1gMnSc4
Feidippida	Feidippid	k1gMnSc4
do	do	k7c2
Sparty	Sparta	k1gFnSc2
se	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
probíhající	probíhající	k2eAgFnPc1d1
náboženské	náboženský	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
však	však	k9
Sparťané	Sparťan	k1gMnPc1
museli	muset	k5eAaImAgMnP
pozdržet	pozdržet	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
odchod	odchod	k1gInSc4
do	do	k7c2
Attiky	Attika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
této	tento	k3xDgFnSc6
věci	věc	k1gFnSc6
se	se	k3xPyFc4
zmiňuje	zmiňovat	k5eAaImIp3nS
Platón	platón	k1gInSc1
v	v	k7c6
Zákonech	zákon	k1gInPc6
<g/>
(	(	kIx(
<g/>
698	#num#	k4
<g/>
e	e	k0
<g/>
;	;	kIx,
překlad	překlad	k1gInSc4
F.	F.	kA
<g/>
N.	N.	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
„	„	k?
<g/>
a	a	k8xC
ačkoliv	ačkoliv	k8xS
všude	všude	k6eAd1
posílali	posílat	k5eAaImAgMnP
poselstva	poselstvo	k1gNnSc2
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
jim	on	k3xPp3gMnPc3
nechtěl	chtít	k5eNaImAgMnS
jít	jít	k5eAaImF
na	na	k7c4
pomoc	pomoc	k1gFnSc4
kromě	kromě	k7c2
Lakedaimoňanů	Lakedaimoňan	k1gMnPc2
<g/>
(	(	kIx(
<g/>
Sparťanů	Sparťan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
tito	tento	k3xDgMnPc1
však	však	k9
pro	pro	k7c4
válku	válka	k1gFnSc4
s	s	k7c7
Messénou	Messéna	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
tehdy	tehdy	k6eAd1
vedli	vést	k5eAaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
snad	snad	k9
i	i	k9
pro	pro	k7c4
nějakou	nějaký	k3yIgFnSc4
jinou	jiný	k2eAgFnSc4d1
překážku	překážka	k1gFnSc4
–	–	k?
nevíme	vědět	k5eNaImIp1nP
totiž	totiž	k9
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
z	z	k7c2
vypravování	vypravování	k1gNnSc2
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
–	–	k?
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
později	pozdě	k6eAd2
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Marathónu	Marathón	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nadcházející	nadcházející	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Marathónu	Marathón	k1gInSc2
se	se	k3xPyFc4
tak	tak	k9
nakonec	nakonec	k6eAd1
Athéňané	Athéňan	k1gMnPc1
mohli	moct	k5eAaImAgMnP
spolehnout	spolehnout	k5eAaPmF
pouze	pouze	k6eAd1
na	na	k7c4
podporu	podpora	k1gFnSc4
bojótského	bojótský	k2eAgNnSc2d1
města	město	k1gNnSc2
Plataje	Plataje	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
Athény	Athéna	k1gFnSc2
uzavřely	uzavřít	k5eAaPmAgInP
spojenectví	spojenectví	k1gNnSc4
již	již	k9
na	na	k7c6
konci	konec	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Řečtí	řecký	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
vytáhli	vytáhnout	k5eAaPmAgMnP
v	v	k7c6
čele	čelo	k1gNnSc6
zhruba	zhruba	k6eAd1
9000	#num#	k4
athénských	athénský	k2eAgMnPc2d1
a	a	k8xC
1000	#num#	k4
platajských	platajský	k2eAgMnPc2d1
hoplítů	hoplíta	k1gMnPc2
vstříc	vstříc	k7c3
invazním	invazní	k2eAgFnPc3d1
silám	síla	k1gFnPc3
nepřítele	nepřítel	k1gMnSc2
u	u	k7c2
Marathónu	Marathón	k1gInSc2
a	a	k8xC
utábořili	utábořit	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c6
okolních	okolní	k2eAgInPc6d1
kopcích	kopec	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polovina	polovina	k1gFnSc1
z	z	k7c2
deseti	deset	k4xCc2
athénských	athénský	k2eAgMnPc2d1
stratégů	stratég	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
každý	každý	k3xTgInSc4
den	den	k1gInSc4
střídali	střídat	k5eAaImAgMnP
ve	v	k7c6
velení	velení	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyslovovala	vyslovovat	k5eAaImAgFnS
proti	proti	k7c3
svedení	svedení	k1gNnSc3
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
Peršané	Peršan	k1gMnPc1
byli	být	k5eAaImAgMnP
ve	v	k7c6
značné	značný	k2eAgFnSc6d1
početní	početní	k2eAgFnSc6d1
převaze	převaha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývajících	zbývající	k2eAgMnPc2d1
pět	pět	k4xCc1
stratégů	stratég	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
i	i	k9
Miltiadés	Miltiadés	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
ale	ale	k9
zasazovalo	zasazovat	k5eAaImAgNnS
o	o	k7c4
okamžité	okamžitý	k2eAgNnSc4d1
střetnutí	střetnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nerozhodnost	nerozhodnost	k1gFnSc1
velitelů	velitel	k1gMnPc2
přímo	přímo	k6eAd1
souvisela	souviset	k5eAaImAgFnS
s	s	k7c7
politickým	politický	k2eAgNnSc7d1
soupeřením	soupeření	k1gNnSc7
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
měli	mít	k5eAaImAgMnP
Peršané	Peršan	k1gMnPc1
četné	četný	k2eAgMnPc4d1
příznivce	příznivec	k1gMnPc4
převážně	převážně	k6eAd1
v	v	k7c6
řadách	řada	k1gFnPc6
aristokracie	aristokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několikadenním	několikadenní	k2eAgNnSc6d1
váhání	váhání	k1gNnSc6
Athéňanů	Athéňan	k1gMnPc2
přesvědčil	přesvědčit	k5eAaPmAgMnS
Miltiadés	Miltiadés	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
byl	být	k5eAaImAgInS
dobře	dobře	k6eAd1
obeznámen	obeznámit	k5eAaPmNgInS
se	s	k7c7
slabinami	slabina	k1gFnPc7
perského	perský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
si	se	k3xPyFc3
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
boji	boj	k1gInSc6
muže	muž	k1gMnSc2
proti	proti	k7c3
muži	muž	k1gMnSc3
má	mít	k5eAaImIp3nS
ukázněný	ukázněný	k2eAgMnSc1d1
a	a	k8xC
dobře	dobře	k6eAd1
vycvičený	vycvičený	k2eAgInSc1d1
řecký	řecký	k2eAgInSc1d1
hoplít	hoplít	k5eAaPmF
nad	nad	k7c7
lehce	lehko	k6eAd1
oděným	oděný	k2eAgMnSc7d1
perským	perský	k2eAgMnSc7d1
vojákem	voják	k1gMnSc7
převahu	převah	k1gInSc2
<g/>
,	,	kIx,
vrchního	vrchní	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
Kallimacha	Kallimach	k1gMnSc2
k	k	k7c3
zahájení	zahájení	k1gNnSc3
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miltiadés	Miltiadés	k1gInSc1
nechal	nechat	k5eAaPmAgInS
rozestavit	rozestavit	k5eAaPmF
falangu	falanga	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c4
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
umístil	umístit	k5eAaPmAgMnS
nejzdatnější	zdatný	k2eAgFnSc2d3
athénské	athénský	k2eAgFnSc2d1
hoplíty	hoplíta	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
svěřil	svěřit	k5eAaPmAgInS
oddílu	oddíl	k1gInSc3
Platajských	Platajský	k2eAgNnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
převahy	převaha	k1gFnSc2
Peršanů	peršan	k1gInPc2
a	a	k8xC
šířky	šířka	k1gFnSc2
údolí	údolí	k1gNnSc2
postrádala	postrádat	k5eAaImAgFnS
řecká	řecký	k2eAgFnSc1d1
linie	linie	k1gFnSc1
patřičnou	patřičný	k2eAgFnSc4d1
hloubku	hloubka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Miltiadés	Miltiadés	k1gInSc4
počítal	počítat	k5eAaImAgMnS
s	s	k7c7
eventuálním	eventuální	k2eAgNnSc7d1
obklíčením	obklíčení	k1gNnSc7
křídel	křídlo	k1gNnPc2
jízdou	jízda	k1gFnSc7
<g/>
,	,	kIx,
zvětšil	zvětšit	k5eAaPmAgInS
jejich	jejich	k3xOp3gFnSc4
tloušťku	tloušťka	k1gFnSc4
na	na	k7c4
úkor	úkor	k1gInSc4
středu	středa	k1gFnSc4
sestavy	sestava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Útok	útok	k1gInSc1
řecké	řecký	k2eAgFnSc2d1
falangy	falanga	k1gFnSc2
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
490	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
sestoupili	sestoupit	k5eAaPmAgMnP
Řekové	Řek	k1gMnPc1
ze	z	k7c2
svahů	svah	k1gInPc2
a	a	k8xC
jakmile	jakmile	k8xS
se	se	k3xPyFc4
přiblížili	přiblížit	k5eAaPmAgMnP
na	na	k7c4
dostřel	dostřel	k1gInSc4
perských	perský	k2eAgInPc2d1
šípů	šíp	k1gInPc2
<g/>
,	,	kIx,
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c4
Miltiadův	Miltiadův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
v	v	k7c6
plném	plný	k2eAgInSc6d1
běhu	běh	k1gInSc6
proti	proti	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlý	rychlý	k2eAgInSc1d1
postup	postup	k1gInSc1
Řeků	Řek	k1gMnPc2
zabránil	zabránit	k5eAaPmAgInS
perským	perský	k2eAgMnPc3d1
lučištníkům	lučištník	k1gMnPc3
uštědřit	uštědřit	k5eAaPmF
jim	on	k3xPp3gInPc3
citelné	citelný	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
svými	svůj	k3xOyFgInPc7
šípy	šíp	k1gInPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
nestihli	stihnout	k5eNaPmAgMnP
vypálit	vypálit	k5eAaPmF
více	hodně	k6eAd2
než	než	k8xS
několik	několik	k4yIc1
málo	málo	k4c1
salv	salva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
střetu	střet	k1gInSc6
obou	dva	k4xCgNnPc2
vojsk	vojsko	k1gNnPc2
se	se	k3xPyFc4
rozpoutal	rozpoutat	k5eAaPmAgInS
dlouhý	dlouhý	k2eAgInSc1d1
a	a	k8xC
tvrdý	tvrdý	k2eAgInSc1d1
boj	boj	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
Peršané	Peršan	k1gMnPc1
zatlačili	zatlačit	k5eAaPmAgMnP
slabý	slabý	k2eAgInSc4d1
řecký	řecký	k2eAgInSc4d1
střed	střed	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
však	však	k9
silnější	silný	k2eAgNnPc4d2
křídla	křídlo	k1gNnPc4
falangy	falanga	k1gFnSc2
přinutila	přinutit	k5eAaPmAgFnS
své	svůj	k3xOyFgMnPc4
protivníky	protivník	k1gMnPc4
k	k	k7c3
ústupu	ústup	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
Řekové	Řek	k1gMnPc1
sevřeli	sevřít	k5eAaPmAgMnP
a	a	k8xC
napadli	napadnout	k5eAaPmAgMnP
vítězící	vítězící	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
nepřátelských	přátelský	k2eNgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
athénský	athénský	k2eAgInSc1d1
útok	útok	k1gInSc1
zcela	zcela	k6eAd1
rozvrátil	rozvrátit	k5eAaPmAgInS
perské	perský	k2eAgFnPc4d1
řady	řada	k1gFnPc4
a	a	k8xC
během	během	k7c2
krátké	krátký	k2eAgFnSc2d1
chvíle	chvíle	k1gFnSc2
začali	začít	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
prchat	prchat	k5eAaImF
ke	k	k7c3
svým	svůj	k3xOyFgFnPc3
lodím	loď	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mořském	mořský	k2eAgInSc6d1
břehu	břeh	k1gInSc6
pokračoval	pokračovat	k5eAaImAgInS
pak	pak	k6eAd1
rozhořčený	rozhořčený	k2eAgInSc1d1
boj	boj	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
Řekové	Řek	k1gMnPc1
snažili	snažit	k5eAaImAgMnP
zabránit	zabránit	k5eAaPmF
Peršanům	Peršan	k1gMnPc3
v	v	k7c6
úniku	únik	k1gInSc6
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
místo	místo	k6eAd1
k	k	k7c3
pobřeží	pobřeží	k1gNnSc3
vydali	vydat	k5eAaPmAgMnP
do	do	k7c2
bažin	bažina	k1gFnPc2
směrem	směr	k1gInSc7
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
pobiti	pobit	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalém	nastalý	k2eAgNnSc6d1
pronásledování	pronásledování	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
značnému	značný	k2eAgNnSc3d1
krveprolití	krveprolití	k1gNnSc3
<g/>
,	,	kIx,
po	po	k7c6
němž	jenž	k3xRgMnSc6
bylo	být	k5eAaImAgNnS
na	na	k7c6
bojišti	bojiště	k1gNnSc6
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
takřka	takřka	k6eAd1
6400	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
Peršanů	Peršan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
bylo	být	k5eAaImAgNnS
zajato	zajmout	k5eAaPmNgNnS
sedm	sedm	k4xCc1
perských	perský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
athénské	athénský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
bylo	být	k5eAaImAgNnS
napočítáno	napočítat	k5eAaPmNgNnS
192	#num#	k4
padlých	padlý	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
byl	být	k5eAaImAgMnS
také	také	k9
Kallimachos	Kallimachos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
legendy	legenda	k1gFnSc2
byl	být	k5eAaImAgMnS
běžec	běžec	k1gMnSc1
Feidippidés	Feidippidésa	k1gFnPc2
ihned	ihned	k6eAd1
po	po	k7c6
bitvě	bitva	k1gFnSc6
poslán	poslat	k5eAaPmNgInS
Miltiadem	Miltiad	k1gInSc7
do	do	k7c2
Athén	Athéna	k1gFnPc2
oznámit	oznámit	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
obyvatelům	obyvatel	k1gMnPc3
vítězství	vítězství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
doběhnutí	doběhnutí	k1gNnSc6
na	na	k7c4
agoru	agora	k1gFnSc4
zvolal	zvolat	k5eAaPmAgInS
prý	prý	k9
Feidippidés	Feidippidés	k1gInSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
radujte	radovat	k5eAaImRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
zvítězili	zvítězit	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
pak	pak	k6eAd1
zkolaboval	zkolabovat	k5eAaPmAgMnS
a	a	k8xC
na	na	k7c6
místě	místo	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
podnětem	podnět	k1gInSc7
ke	k	k7c3
vzniku	vznik	k1gInSc3
novodobého	novodobý	k2eAgInSc2d1
olympijského	olympijský	k2eAgInSc2d1
závodu	závod	k1gInSc2
<g/>
,	,	kIx,
maratonského	maratonský	k2eAgInSc2d1
běhu	běh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
perských	perský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
navzdory	navzdory	k7c3
porážce	porážka	k1gFnSc3
řádně	řádně	k6eAd1
nalodila	nalodit	k5eAaPmAgFnS
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
perští	perský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
zamířili	zamířit	k5eAaPmAgMnP
s	s	k7c7
flotilou	flotila	k1gFnSc7
k	k	k7c3
athénskému	athénský	k2eAgInSc3d1
přístavu	přístav	k1gInSc3
Faléron	Faléron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artafernés	Artafernés	k1gInSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgInS
o	o	k7c4
náhlé	náhlý	k2eAgNnSc4d1
obsazení	obsazení	k1gNnSc4
nyní	nyní	k6eAd1
nechráněných	chráněný	k2eNgFnPc2d1
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
řečtí	řecký	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
vědomi	vědom	k2eAgMnPc1d1
si	se	k3xPyFc3
tohoto	tento	k3xDgNnSc2
nebezpečí	nebezpečí	k1gNnSc2
neponechali	ponechat	k5eNaPmAgMnP
svým	svůj	k3xOyFgMnPc3
vítězným	vítězný	k2eAgMnPc3d1
a	a	k8xC
unaveným	unavený	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
čas	čas	k1gInSc4
k	k	k7c3
odpočinku	odpočinek	k1gInSc3
a	a	k8xC
spěchali	spěchat	k5eAaImAgMnP
zpátky	zpátky	k6eAd1
do	do	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Peršané	Peršan	k1gMnPc1
připluli	připlout	k5eAaPmAgMnP
k	k	k7c3
Faléru	Falér	k1gInSc3
<g/>
,	,	kIx,
nalezli	nalézt	k5eAaBmAgMnP,k5eAaPmAgMnP
zde	zde	k6eAd1
připravené	připravený	k2eAgMnPc4d1
Athéňany	Athéňan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
Artafernés	Artafernés	k1gInSc1
nařídil	nařídit	k5eAaPmAgInS
flotile	flotila	k1gFnSc3
návrat	návrat	k1gInSc4
do	do	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
den	dna	k1gFnPc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
dorazilo	dorazit	k5eAaPmAgNnS
do	do	k7c2
Athén	Athéna	k1gFnPc2
2000	#num#	k4
Sparťanů	Sparťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
své	svůj	k3xOyFgFnSc3
lítosti	lítost	k1gFnSc3
přišli	přijít	k5eAaPmAgMnP
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
zúčastnit	zúčastnit	k5eAaPmF
boje	boj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
Marathónu	Marathón	k1gInSc2
</s>
<s>
Důsledky	důsledek	k1gInPc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Marathónu	Marathón	k1gInSc2
byly	být	k5eAaImAgFnP
zásadní	zásadní	k2eAgFnPc1d1
pro	pro	k7c4
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
si	se	k3xPyFc3
ověřili	ověřit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
bojovat	bojovat	k5eAaImF
a	a	k8xC
zvítězit	zvítězit	k5eAaPmF
nad	nad	k7c7
dosud	dosud	k6eAd1
nepřemožitelnými	přemožitelný	k2eNgInPc7d1
Peršany	peršan	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
akceptujeme	akceptovat	k5eAaBmIp1nP
Hérodotova	Hérodotův	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
pozoruhodně	pozoruhodně	k6eAd1
výjimečný	výjimečný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
zvýšily	zvýšit	k5eAaPmAgFnP
tímto	tento	k3xDgNnSc7
senzačním	senzační	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
svoji	svůj	k3xOyFgFnSc4
reputaci	reputace	k1gFnSc4
v	v	k7c6
celém	celý	k2eAgInSc6d1
řeckém	řecký	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
římský	římský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Nepos	Nepos	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
nestalo	stát	k5eNaPmAgNnS
nic	nic	k3yNnSc1
proslulejšího	proslulý	k2eAgNnSc2d2
než	než	k8xS
tato	tento	k3xDgFnSc1
bitva	bitva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
jindy	jindy	k6eAd1
tak	tak	k6eAd1
malá	malý	k2eAgFnSc1d1
hrstka	hrstka	k1gFnSc1
neporazila	porazit	k5eNaPmAgFnS
tak	tak	k6eAd1
ohromnou	ohromný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
Helénové	Helén	k1gMnPc1
díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
vítězství	vítězství	k1gNnSc3
pochopili	pochopit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
s	s	k7c7
to	ten	k3xDgNnSc4
odolat	odolat	k5eAaPmF
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	nedlouho	k1gNnSc1
po	po	k7c6
Marathónu	Marathón	k1gInSc6
proto	proto	k8xC
mnohé	mnohý	k2eAgFnPc1d1
z	z	k7c2
obcí	obec	k1gFnPc2
vypověděly	vypovědět	k5eAaPmAgFnP
svoje	svůj	k3xOyFgNnSc4
podrobení	podrobení	k1gNnSc4
se	se	k3xPyFc4
Dáreiovi	Dáreius	k1gMnSc3
a	a	k8xC
připojily	připojit	k5eAaPmAgFnP
se	se	k3xPyFc4
na	na	k7c4
stranu	strana	k1gFnSc4
Athéňanů	Athéňan	k1gMnPc2
a	a	k8xC
Sparťanů	Sparťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
ještě	ještě	k6eAd1
závažnější	závažný	k2eAgMnSc1d2
byl	být	k5eAaImAgMnS
dopad	dopad	k1gInSc4
Marathónu	Marathón	k1gInSc2
pro	pro	k7c4
Peršany	peršan	k1gInPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
první	první	k4xOgFnSc3
porážce	porážka	k1gFnSc3
pravidelného	pravidelný	k2eAgNnSc2d1
perského	perský	k2eAgNnSc2d1
pěšího	pěší	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
od	od	k7c2
dob	doba	k1gFnPc2
vlády	vláda	k1gFnSc2
Kýra	Kýrus	k1gMnSc4
Velikého	veliký	k2eAgMnSc4d1
<g/>
,	,	kIx,
před	před	k7c7
více	hodně	k6eAd2
než	než	k8xS
dvěma	dva	k4xCgFnPc7
generacemi	generace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Následujících	následující	k2eAgInPc2d1
deset	deset	k4xCc1
let	léto	k1gNnPc2
</s>
<s>
Perská	perský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Perští	perský	k2eAgMnPc1d1
a	a	k8xC
médští	médský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
vyobrazení	vyobrazení	k1gNnPc2
v	v	k7c6
Persepoli	Persepolis	k1gFnSc6
</s>
<s>
Pohroma	pohroma	k1gFnSc1
Peršanů	Peršan	k1gMnPc2
v	v	k7c6
Řecku	Řecko	k1gNnSc6
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
Dáreia	Dáreia	k1gFnSc1
o	o	k7c4
to	ten	k3xDgNnSc4
víc	hodně	k6eAd2
dychtícího	dychtící	k2eAgInSc2d1
po	po	k7c6
dobytí	dobytí	k1gNnSc6
této	tento	k3xDgFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Marathónu	Marathón	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
velkokrál	velkokrál	k1gInSc4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
budováním	budování	k1gNnSc7
nového	nový	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
dříve	dříve	k6eAd2
než	než	k8xS
byly	být	k5eAaImAgFnP
přípravy	příprava	k1gFnPc1
završeny	završen	k2eAgFnPc1d1
<g/>
,	,	kIx,
vypuklo	vypuknout	k5eAaPmAgNnS
v	v	k7c6
Egyptě	Egypt	k1gInSc6
povstání	povstání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
ho	on	k3xPp3gMnSc4
přinutilo	přinutit	k5eAaPmAgNnS
kampaň	kampaň	k1gFnSc4
do	do	k7c2
Řecka	Řecko	k1gNnSc2
odložit	odložit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Dáreiově	Dáreiův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
485	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
perský	perský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Xerxés	Xerxésa	k1gFnPc2
I.	I.	kA
Ten	ten	k3xDgInSc4
nejprve	nejprve	k6eAd1
zdolal	zdolat	k5eAaPmAgMnS
vzpouru	vzpoura	k1gFnSc4
v	v	k7c6
Egyptě	Egypt	k1gInSc6
a	a	k8xC
následně	následně	k6eAd1
také	také	k9
v	v	k7c6
Babylónu	Babylón	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
mohl	moct	k5eAaImAgInS
napřít	napřít	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
západním	západní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
k	k	k7c3
evropským	evropský	k2eAgInPc3d1
břehům	břeh	k1gInPc3
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xerxés	Xerxésa	k1gFnPc2
nařídil	nařídit	k5eAaPmAgMnS
všem	všecek	k3xTgNnPc3
městům	město	k1gNnPc3
své	svůj	k3xOyFgFnSc2
rozlehlé	rozlehlý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
poskytnout	poskytnout	k5eAaPmF
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
koně	kůň	k1gMnPc4
a	a	k8xC
nezbytné	zbytný	k2eNgFnPc4d1,k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
shromážděna	shromážděn	k2eAgFnSc1d1
obrovská	obrovský	k2eAgFnSc1d1
branná	branný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
poloostrově	poloostrov	k1gInSc6
Chalkidiké	Chalkidiká	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
hory	hora	k1gFnSc2
Athós	Athós	k1gInSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
prokopán	prokopán	k2eAgInSc1d1
kanál	kanál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
zabránit	zabránit	k5eAaPmF
opakování	opakování	k1gNnSc4
katastrofy	katastrofa	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
492	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
a	a	k8xC
rovněž	rovněž	k9
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Strýmón	Strýmón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
egyptští	egyptský	k2eAgMnPc1d1
a	a	k8xC
féničtí	fénický	k2eAgMnPc1d1
technici	technik	k1gMnPc1
zkonstruovali	zkonstruovat	k5eAaPmAgMnP
poblíž	poblíž	k6eAd1
Abýdu	Abýd	k1gMnSc3
z	z	k7c2
lodí	loď	k1gFnPc2
pontonový	pontonový	k2eAgInSc4d1
most	most	k1gInSc4
přes	přes	k7c4
Helléspont	Helléspont	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
široký	široký	k2eAgInSc1d1
pouze	pouze	k6eAd1
asi	asi	k9
jeden	jeden	k4xCgInSc4
a	a	k8xC
půl	půl	k1xP
kilometru	kilometr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
si	se	k3xPyFc3
dále	daleko	k6eAd2
získali	získat	k5eAaPmAgMnP
přízeň	přízeň	k1gFnSc4
četných	četný	k2eAgFnPc2d1
řeckých	řecký	k2eAgFnPc2d1
polis	polis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
vynikal	vynikat	k5eAaImAgMnS
peloponéský	peloponéský	k2eAgMnSc1d1
Argos	Argos	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
zavázali	zavázat	k5eAaPmAgMnP
přejít	přejít	k5eAaPmF
na	na	k7c4
stranu	strana	k1gFnSc4
velkokrále	velkokrála	k1gFnSc3
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
perské	perský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
dosáhne	dosáhnout	k5eAaPmIp3nS
hranic	hranice	k1gFnPc2
jejich	jejich	k3xOp3gNnPc4
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc4
Aleuovců	Aleuovec	k1gInPc2
vládnoucí	vládnoucí	k2eAgInSc4d1
v	v	k7c6
thesalském	thesalský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Lárísa	Lárísa	k1gFnSc1
považoval	považovat	k5eAaImAgInS
perský	perský	k2eAgInSc1d1
vpád	vpád	k1gInSc1
za	za	k7c4
příležitost	příležitost	k1gFnSc4
rozšířit	rozšířit	k5eAaPmF
vlastní	vlastní	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
Théby	Théby	k1gFnPc1
byly	být	k5eAaImAgFnP
ochotny	ochoten	k2eAgFnPc1d1
přejít	přejít	k5eAaPmF
na	na	k7c4
stranu	strana	k1gFnSc4
Peršanů	Peršan	k1gMnPc2
a	a	k8xC
učinily	učinit	k5eAaImAgInP,k5eAaPmAgInP
tak	tak	k9
ihned	ihned	k6eAd1
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Řecké	řecký	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Nedlouho	dlouho	k6eNd1
po	po	k7c6
vítězství	vítězství	k1gNnSc6
u	u	k7c2
Marathónu	Marathón	k1gInSc2
přiměl	přimět	k5eAaPmAgInS
Miltiadés	Miltiadés	k1gInSc1
Athéňany	Athéňan	k1gMnPc4
k	k	k7c3
podniknutí	podniknutí	k1gNnSc3
expedice	expedice	k1gFnSc2
na	na	k7c4
Kykladské	kykladský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
tažení	tažení	k1gNnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Paros	Paros	k1gInSc1
však	však	k9
ztroskotalo	ztroskotat	k5eAaPmAgNnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
vážně	vážně	k6eAd1
zraněn	zranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miltiadův	Miltiadův	k2eAgInSc4d1
neúspěch	neúspěch	k1gInSc4
podnítil	podnítit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnPc4
politické	politický	k2eAgMnPc4d1
soupeře	soupeř	k1gMnPc4
k	k	k7c3
protestům	protest	k1gInPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
vedly	vést	k5eAaImAgInP
k	k	k7c3
Miltiadově	Miltiadův	k2eAgInSc6d1
pádu	pád	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svém	svůj	k3xOyFgInSc6
návratu	návrat	k1gInSc6
do	do	k7c2
Athén	Athéna	k1gFnPc2
byl	být	k5eAaImAgInS
obviněn	obvinit	k5eAaPmNgMnS
z	z	k7c2
vlastizrady	vlastizrada	k1gFnSc2
a	a	k8xC
odsouzen	odsoudit	k5eAaPmNgInS,k5eAaImNgInS
k	k	k7c3
smrti	smrt	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trest	trest	k1gInSc1
byl	být	k5eAaImAgInS
sice	sice	k8xC
zmírněn	zmírnit	k5eAaPmNgInS
na	na	k7c4
pokutu	pokuta	k1gFnSc4
padesáti	padesát	k4xCc2
talentů	talent	k1gInPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
dříve	dříve	k6eAd2
než	než	k8xS
mohl	moct	k5eAaImAgMnS
tuto	tento	k3xDgFnSc4
sumu	suma	k1gFnSc4
zaplatit	zaplatit	k5eAaPmF
<g/>
,	,	kIx,
podlehl	podlehnout	k5eAaPmAgMnS
ve	v	k7c4
vězení	vězení	k1gNnSc4
utrženým	utržený	k2eAgNnSc7d1
zraněním	zranění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
athénské	athénský	k2eAgFnSc6d1
politice	politika	k1gFnSc6
se	se	k3xPyFc4
nyní	nyní	k6eAd1
svářily	svářit	k5eAaImAgFnP
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
demokraté	demokrat	k1gMnPc1
vedení	vedení	k1gNnSc2
Themistoklem	Themistokl	k1gInSc7
a	a	k8xC
aristokraté	aristokrat	k1gMnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Aristeidem	Aristeid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
487	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
užito	užít	k5eAaPmNgNnS
střepinového	střepinový	k2eAgInSc2d1
soudu	soud	k1gInSc2
(	(	kIx(
<g/>
ostrakismus	ostrakismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
byli	být	k5eAaImAgMnP
postiženi	postihnout	k5eAaPmNgMnP
převážně	převážně	k6eAd1
vůdčí	vůdčí	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
zastánců	zastánce	k1gMnPc2
míru	mír	k1gInSc2
s	s	k7c7
Persií	Persie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
zvítězil	zvítězit	k5eAaPmAgInS
Themistoklés	Themistoklés	k1gInSc1
nad	nad	k7c7
Aristeidem	Aristeid	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
ostrakismem	ostrakismus	k1gInSc7
vypovězen	vypovědět	k5eAaPmNgInS
z	z	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
probíhala	probíhat	k5eAaImAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Athénami	Athéna	k1gFnPc7
a	a	k8xC
ostrovem	ostrov	k1gInSc7
Aigínou	Aigína	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
nejsilnější	silný	k2eAgFnSc7d3
námořní	námořní	k2eAgFnSc7d1
mocností	mocnost	k1gFnSc7
Řecka	Řecko	k1gNnSc2
a	a	k8xC
jejíž	jejíž	k3xOyRp3gFnPc1
lodě	loď	k1gFnPc1
pustošily	pustošit	k5eAaImAgFnP
attické	attická	k1gFnPc4
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Themistoklovi	Themistokl	k1gMnSc6
se	se	k3xPyFc4
následně	následně	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
pohnout	pohnout	k5eAaPmF
své	svůj	k3xOyFgMnPc4
spoluobčany	spoluobčan	k1gMnPc4
k	k	k7c3
využití	využití	k1gNnSc3
zisků	zisk	k1gInPc2
z	z	k7c2
nově	nově	k6eAd1
objevené	objevený	k2eAgFnSc2d1
stříbrné	stříbrný	k2eAgFnSc2d1
žíly	žíla	k1gFnSc2
v	v	k7c6
Lauriu	Laurium	k1gNnSc6
k	k	k7c3
rozsáhlému	rozsáhlý	k2eAgInSc3d1
programu	program	k1gInSc3
výstavby	výstavba	k1gFnSc2
triér	triéra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Themistoklés	Themistoklés	k1gInSc1
nechal	nechat	k5eAaPmAgInS
vystrojit	vystrojit	k5eAaPmF
flotilu	flotila	k1gFnSc4
zdánlivě	zdánlivě	k6eAd1
proti	proti	k7c3
Aigíňanům	Aigíňan	k1gMnPc3
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
připravoval	připravovat	k5eAaImAgMnS
svoji	svůj	k3xOyFgFnSc4
obec	obec	k1gFnSc4
k	k	k7c3
novému	nový	k2eAgInSc3d1
střetu	střet	k1gInSc3
s	s	k7c7
Peršany	peršan	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
si	se	k3xPyFc3
tak	tak	k9
během	během	k7c2
dalších	další	k2eAgNnPc2d1
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
opatřili	opatřit	k5eAaPmAgMnP
loďstvo	loďstvo	k1gNnSc4
čítající	čítající	k2eAgFnSc2d1
dvě	dva	k4xCgFnPc1
stě	sto	k4xCgFnPc1
triér	triéra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
celém	celý	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
byly	být	k5eAaImAgFnP
dobře	dobře	k6eAd1
známy	znám	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
o	o	k7c6
obnovených	obnovený	k2eAgFnPc6d1
perských	perský	k2eAgFnPc6d1
přípravách	příprava	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
481	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgInS
proto	proto	k8xC
do	do	k7c2
Poseidónova	Poseidónův	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
na	na	k7c6
Isthmu	Isthm	k1gInSc6
svolán	svolán	k2eAgInSc1d1
kongres	kongres	k1gInSc1
všech	všecek	k3xTgFnPc2
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dosud	dosud	k6eAd1
nebyly	být	k5eNaImAgFnP
porobeny	poroben	k2eAgInPc4d1
Peršany	peršan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
Xerxés	Xerxés	k1gInSc1
přemístil	přemístit	k5eAaPmAgInS
do	do	k7c2
Sard	Sardy	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
hodlal	hodlat	k5eAaImAgMnS
přezimovat	přezimovat	k5eAaBmF
před	před	k7c7
blížící	blížící	k2eAgFnSc7d1
se	se	k3xPyFc4
invazí	invaze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tváří	tvář	k1gFnPc2
v	v	k7c6
tvář	tvář	k1gFnSc1
hrozícímu	hrozící	k2eAgNnSc3d1
nebezpečí	nebezpečí	k1gNnSc3
se	se	k3xPyFc4
Řekové	Řek	k1gMnPc1
pokusili	pokusit	k5eAaPmAgMnP
sjednotit	sjednotit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
obce	obec	k1gFnPc4
do	do	k7c2
jediné	jediný	k2eAgFnSc2d1
veliké	veliký	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
k	k	k7c3
obraně	obrana	k1gFnSc3
vlasti	vlast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
veřejnému	veřejný	k2eAgNnSc3d1
usmíření	usmíření	k1gNnSc3
mezi	mezi	k7c7
Athénami	Athéna	k1gFnPc7
a	a	k8xC
Aigínou	Aigína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
leckteré	leckterý	k3yIgInPc1
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
zůstaly	zůstat	k5eAaPmAgInP
stranou	stranou	k6eAd1
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
je	on	k3xPp3gFnPc4
vedla	vést	k5eAaImAgFnS
obava	obava	k1gFnSc1
z	z	k7c2
perské	perský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
žárlivost	žárlivost	k1gFnSc4
k	k	k7c3
vedoucí	vedoucí	k1gFnSc3
pozici	pozice	k1gFnSc4
Sparty	Sparta	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
odmítly	odmítnout	k5eAaPmAgFnP
účast	účast	k1gFnSc4
sicilské	sicilský	k2eAgFnSc2d1
a	a	k8xC
italské	italský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
požadovaly	požadovat	k5eAaImAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
čele	čelo	k1gNnSc6
stál	stát	k5eAaImAgMnS
syrakúský	syrakúský	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
Gelón	Gelón	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
byly	být	k5eAaImAgFnP
nakloněné	nakloněný	k2eAgFnPc4d1
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniknuvší	vzniknuvší	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Sparty	Sparta	k1gFnSc2
tvořilo	tvořit	k5eAaImAgNnS
třicet	třicet	k4xCc1
jedna	jeden	k4xCgFnSc1
států	stát	k1gInPc2
z	z	k7c2
Peloponésu	Peloponés	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
Euboie	Euboie	k1gFnSc2
a	a	k8xC
Athény	Athéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc4
tudíž	tudíž	k8xC
sdružoval	sdružovat	k5eAaImAgInS
pouze	pouze	k6eAd1
menší	malý	k2eAgFnSc4d2
část	část	k1gFnSc4
řeckých	řecký	k2eAgFnPc2d1
poleis	poleis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnSc1d1
zůstali	zůstat	k5eAaPmAgMnP
buď	buď	k8xC
neutrální	neutrální	k2eAgFnPc1d1
nebo	nebo	k8xC
se	se	k3xPyFc4
jako	jako	k9
Thesálie	Thesálie	k1gFnSc1
a	a	k8xC
posléze	posléze	k6eAd1
Bojótie	Bojótie	k1gFnPc4
poddali	poddat	k5eAaPmAgMnP
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategický	strategický	k2eAgInSc1d1
plán	plán	k1gInSc1
Helénů	Helén	k1gMnPc2
<g/>
,	,	kIx,
vypracovaný	vypracovaný	k2eAgInSc1d1
Themistoklem	Themistokl	k1gInSc7
<g/>
,	,	kIx,
spočíval	spočívat	k5eAaImAgInS
ve	v	k7c6
svedení	svedení	k1gNnSc6
námořní	námořní	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
perské	perský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
poraženo	poražen	k2eAgNnSc1d1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
perské	perský	k2eAgFnPc1d1
pozemní	pozemní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
byly	být	k5eAaImAgFnP
kvůli	kvůli	k7c3
nedostatečnému	dostatečný	k2eNgNnSc3d1
zásobování	zásobování	k1gNnSc3
přinuceny	přinutit	k5eAaPmNgFnP
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
z	z	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
přehrazen	přehrazen	k2eAgInSc1d1
Isthmos	Isthmos	k1gInSc1
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgInS
přijat	přijmout	k5eAaPmNgMnS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
by	by	kYmCp3nS
tím	ten	k3xDgNnSc7
celé	celý	k2eAgNnSc1d1
severní	severní	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
bylo	být	k5eAaImAgNnS
přenecháno	přenechat	k5eAaPmNgNnS
bez	bez	k7c2
boje	boj	k1gInSc2
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Xerxova	Xerxův	k2eAgFnSc1d1
invaze	invaze	k1gFnSc1
</s>
<s>
Velikost	velikost	k1gFnSc1
perského	perský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
</s>
<s>
Perské	perský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
z	z	k7c2
východních	východní	k2eAgFnPc2d1
satrapií	satrapie	k1gFnPc2
se	se	k3xPyFc4
shromáždilo	shromáždit	k5eAaPmAgNnS
v	v	k7c6
létě	léto	k1gNnSc6
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
481	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
Kappadokii	Kappadokie	k1gFnSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
Xerxés	Xerxés	k1gInSc4
odvedl	odvést	k5eAaPmAgMnS
do	do	k7c2
Sard	Sardy	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
strávil	strávit	k5eAaPmAgMnS
zimu	zima	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
armáda	armáda	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
k	k	k7c3
Helléspontu	Helléspont	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
u	u	k7c2
Abýdu	Abýd	k1gInSc2
spojily	spojit	k5eAaPmAgInP
oddíly	oddíl	k1gInPc1
ze	z	k7c2
západních	západní	k2eAgFnPc2d1
satrapií	satrapie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
pozemních	pozemní	k2eAgFnPc2d1
a	a	k8xC
námořních	námořní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
zahájil	zahájit	k5eAaPmAgInS
Xerxés	Xerxés	k1gInSc1
tažení	tažení	k1gNnSc2
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
předmětem	předmět	k1gInSc7
mnoha	mnoho	k4c2
diskusí	diskuse	k1gFnPc2
a	a	k8xC
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Hérodota	Hérodot	k1gMnSc2
se	se	k3xPyFc4
prý	prý	k9
Peršané	Peršan	k1gMnPc1
přesunovali	přesunovat	k5eAaImAgMnP
sedm	sedm	k4xCc4
dní	den	k1gInPc2
a	a	k8xC
sedm	sedm	k4xCc1
nocí	noc	k1gFnPc2
po	po	k7c6
mostě	most	k1gInSc6
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jejich	jejich	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
zahrnovalo	zahrnovat	k5eAaImAgNnS
1	#num#	k4
700	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
<g/>
,	,	kIx,
80	#num#	k4
000	#num#	k4
jízdy	jízda	k1gFnSc2
a	a	k8xC
1207	#num#	k4
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Moderní	moderní	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
však	však	k9
vůči	vůči	k7c3
Hérodotem	Hérodot	k1gMnSc7
a	a	k8xC
jinými	jiný	k2eAgMnPc7d1
antickými	antický	k2eAgMnPc7d1
autory	autor	k1gMnPc7
podávaným	podávaný	k2eAgInPc3d1
údajům	údaj	k1gInPc3
přistupují	přistupovat	k5eAaImIp3nP
značně	značně	k6eAd1
kriticky	kriticky	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	on	k3xPp3gInPc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
velice	velice	k6eAd1
nepřesné	přesný	k2eNgFnPc4d1
a	a	k8xC
úmyslně	úmyslně	k6eAd1
zveličené	zveličený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
všeobecně	všeobecně	k6eAd1
převládajícího	převládající	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
disponoval	disponovat	k5eAaBmAgMnS
perský	perský	k2eAgMnSc1d1
král	král	k1gMnSc1
zhruba	zhruba	k6eAd1
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
až	až	k9
250	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgFnPc4d1
výhrady	výhrada	k1gFnPc4
existují	existovat	k5eAaImIp3nP
i	i	k9
ke	k	k7c3
zmiňované	zmiňovaný	k2eAgFnSc3d1
velikosti	velikost	k1gFnSc3
perského	perský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
po	po	k7c6
třech	tři	k4xCgInPc6
měsících	měsíc	k1gInPc6
pochodu	pochod	k1gInSc2
Thrákií	Thrákie	k1gFnPc2
a	a	k8xC
Makedonií	Makedonie	k1gFnPc2
dorazili	dorazit	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
k	k	k7c3
Thermám	thermy	k1gFnPc3
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Soluň	Soluň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
cesty	cesta	k1gFnSc2
byla	být	k5eAaImAgFnS
část	část	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
přepravována	přepravován	k2eAgFnSc1d1
loďstvem	loďstvo	k1gNnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
bez	bez	k7c2
potíží	potíž	k1gFnPc2
překonalo	překonat	k5eAaPmAgNnS
kanál	kanál	k1gInSc4
při	při	k7c6
poloostrovu	poloostrov	k1gInSc3
Athós	Athósa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vplutí	vplutí	k1gNnSc6
do	do	k7c2
Thermského	Thermský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
se	se	k3xPyFc4
perská	perský	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
břehů	břeh	k1gInPc2
Magnésie	Magnésie	k1gFnSc2
poblíž	poblíž	k6eAd1
pohoří	pohořet	k5eAaPmIp3nP
Pelion	Pelion	k1gInSc4
byli	být	k5eAaImAgMnP
však	však	k9
Peršané	Peršan	k1gMnPc1
zastiženi	zastižen	k2eAgMnPc1d1
strašlivou	strašlivý	k2eAgFnSc4d1
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
trvající	trvající	k2eAgFnSc7d1
bouří	bouř	k1gFnSc7
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
za	za	k7c4
oběť	oběť	k1gFnSc4
třetina	třetina	k1gFnSc1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
výrazně	výrazně	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
morálku	morálka	k1gFnSc4
Helénů	Helén	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
spatřovali	spatřovat	k5eAaImAgMnP
odplatu	odplata	k1gFnSc4
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Leónidás	Leónidás	k6eAd1
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
<g/>
,	,	kIx,
Jacques-Louis	Jacques-Louis	k1gFnSc1
David	David	k1gMnSc1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Xerxés	Xerxés	k1gInSc1
ještě	ještě	k6eAd1
dlel	dlít	k5eAaImAgInS
v	v	k7c6
Abýdu	Abýd	k1gInSc6
<g/>
,	,	kIx,
vyslali	vyslat	k5eAaPmAgMnP
Řekové	Řek	k1gMnPc1
vojsko	vojsko	k1gNnSc4
10	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
Sparťanem	Sparťan	k1gMnSc7
Oienetem	Oienet	k1gInSc7
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
byl	být	k5eAaImAgInS
podřízen	podřízen	k2eAgInSc4d1
Themistoklés	Themistoklés	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
údolí	údolí	k1gNnSc2
Tempé	Tempý	k2eAgNnSc1d1
<g/>
,	,	kIx,
průsmyku	průsmyk	k1gInSc2
mezi	mezi	k7c7
Thesálií	Thesálie	k1gFnSc7
a	a	k8xC
Makedonií	Makedonie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
ale	ale	k9
Helénové	Helén	k1gMnPc1
nemohli	moct	k5eNaImAgMnP
spolehnout	spolehnout	k5eAaPmF
na	na	k7c4
Thesaly	Thesaly	k1gFnSc4
a	a	k8xC
navíc	navíc	k6eAd1
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Peršané	Peršan	k1gMnPc1
překročí	překročit	k5eAaPmIp3nP
hory	hora	k1gFnPc4
jinou	jiný	k2eAgFnSc4d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
ustoupili	ustoupit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
dosáhli	dosáhnout	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
Thesálie	Thesálie	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
při	při	k7c6
postupu	postup	k1gInSc6
narazili	narazit	k5eAaPmAgMnP
na	na	k7c4
jakýkoli	jakýkoli	k3yIgInSc4
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c6
témže	týž	k3xTgInSc6
momentě	moment	k1gInSc6
se	se	k3xPyFc4
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
utábořil	utábořit	k5eAaPmAgInS
menší	malý	k2eAgInSc1d2
oddíl	oddíl	k1gInSc1
vyslaný	vyslaný	k2eAgInSc1d1
řeckými	řecký	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
velel	velet	k5eAaImAgInS
spartský	spartský	k2eAgMnSc1d1
král	král	k1gMnSc1
Leónidás	Leónidása	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
příchodu	příchod	k1gInSc6
k	k	k7c3
Thermopylám	Thermopyly	k1gFnPc3
zde	zde	k6eAd1
Xerxés	Xerxés	k1gInSc1
nalezl	naleznout	k5eAaPmAgInS,k5eAaBmAgInS
hradbu	hradba	k1gFnSc4
bráněnou	bráněný	k2eAgFnSc4d1
třemi	tři	k4xCgNnPc7
sty	sto	k4xCgNnPc7
Sparťany	Sparťan	k1gMnPc4
a	a	k8xC
asi	asi	k9
7000	#num#	k4
hoplíty	hoplíta	k1gFnSc2
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xerxovi	Xerxův	k2eAgMnPc1d1
zvědové	zvěd	k1gMnPc1
informovali	informovat	k5eAaBmAgMnP
krále	král	k1gMnSc4
o	o	k7c6
nepatrném	nepatrný	k2eAgInSc6d1,k2eNgInSc6d1
počtu	počet	k1gInSc6
Řeků	Řek	k1gMnPc2
a	a	k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Sparťané	Sparťan	k1gMnPc1
si	se	k3xPyFc3
zdobí	zdobit	k5eAaImIp3nP
vlasy	vlas	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
Xerxovi	Xerxes	k1gMnSc3
vysvětlil	vysvětlit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
rádce	rádce	k1gMnSc1
Démaratos	Démaratos	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
starodávný	starodávný	k2eAgInSc1d1
zvyk	zvyk	k1gInSc1
vykonávaný	vykonávaný	k2eAgInSc1d1
Sparťany	Sparťan	k1gMnPc4
před	před	k7c7
bojem	boj	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
očekávali	očekávat	k5eAaImAgMnP
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xerxés	Xerxésa	k1gFnPc2
však	však	k9
Démaratovi	Démarat	k1gMnSc3
nevěřil	věřit	k5eNaImAgMnS
a	a	k8xC
pozdržel	pozdržet	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
útok	útok	k1gInSc4
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
spoléhal	spoléhat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Řekové	Řek	k1gMnPc1
vzhledem	vzhledem	k7c3
k	k	k7c3
ohromné	ohromný	k2eAgFnSc3d1
převaze	převaha	k1gFnSc3
Peršanů	peršan	k1gInPc2
raději	rád	k6eAd2
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
k	k	k7c3
nim	on	k3xPp3gFnPc3
poslal	poslat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
hlasatele	hlasatel	k1gMnSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
královým	králův	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
vyzval	vyzvat	k5eAaPmAgMnS
Řeky	Řek	k1gMnPc4
ke	k	k7c3
složení	složení	k1gNnSc3
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
to	ten	k3xDgNnSc4
mu	on	k3xPp3gMnSc3
Leónidás	Leónidás	k1gInSc1
odpověděl	odpovědět	k5eAaPmAgInS
<g/>
,	,	kIx,
„	„	k?
<g/>
pojď	jít	k5eAaImRp2nS
a	a	k8xC
vezmi	vzít	k5eAaPmRp2nS
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
molón	molón	k1gInSc1
labe	labat	k5eAaPmIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přesvědčen	přesvědčit	k5eAaPmNgInS
takovými	takový	k3xDgInPc7
důkazy	důkaz	k1gInPc7
o	o	k7c4
odhodlanosti	odhodlanost	k1gFnPc4
Řeků	Řek	k1gMnPc2
<g/>
,	,	kIx,
konečně	konečně	k6eAd1
pátý	pátý	k4xOgInSc4
den	den	k1gInSc4
vyslal	vyslat	k5eAaPmAgMnS
proti	proti	k7c3
nim	on	k3xPp3gFnPc3
Xerxés	Xerxés	k1gInSc1
vybraný	vybraný	k2eAgInSc4d1
oddíl	oddíl	k1gInSc4
Médů	Méd	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Médové	Méd	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
udatně	udatně	k6eAd1
<g/>
,	,	kIx,
nedosáhli	dosáhnout	k5eNaPmAgMnP
ničeho	nic	k3yNnSc2
kromě	kromě	k7c2
těžkých	těžký	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
Helénové	Helén	k1gMnPc1
králi	král	k1gMnSc3
ukázali	ukázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
„	„	k?
<g/>
že	že	k8xS
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
málo	málo	k4c1
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
vrhl	vrhnout	k5eAaImAgMnS,k5eAaPmAgMnS
Xerxés	Xerxés	k1gInSc4
do	do	k7c2
boje	boj	k1gInSc2
svoji	svůj	k3xOyFgFnSc4
elitní	elitní	k2eAgFnSc4d1
osobní	osobní	k2eAgFnSc4d1
gardu	garda	k1gFnSc4
deseti	deset	k4xCc2
tisíc	tisíc	k4xCgInPc2
Nesmrtelných	nesmrtelný	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
proti	proti	k7c3
Řekům	Řek	k1gMnPc3
neuspěli	uspět	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xerxés	Xerxés	k1gInSc1
byl	být	k5eAaImAgInS
prý	prý	k9
spatřen	spatřit	k5eAaPmNgInS
jak	jak	k6eAd1
v	v	k7c6
hněvu	hněv	k1gInSc6
třikrát	třikrát	k6eAd1
vyskočil	vyskočit	k5eAaPmAgMnS
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvrat	zvrat	k1gInSc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
bitvy	bitva	k1gFnSc2
přivodil	přivodit	k5eAaBmAgInS,k5eAaPmAgInS
teprve	teprve	k6eAd1
jistý	jistý	k2eAgMnSc1d1
Málijec	Málijec	k1gMnSc1
jménem	jméno	k1gNnSc7
Efialtés	Efialtés	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Peršanům	Peršan	k1gMnPc3
pověděl	povědět	k5eAaPmAgMnS
o	o	k7c6
tajné	tajný	k2eAgFnSc6d1
stezce	stezka	k1gFnSc6
přes	přes	k7c4
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgFnPc1d1
proniknout	proniknout	k5eAaPmF
do	do	k7c2
týlu	týl	k1gInSc2
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
byl	být	k5eAaImAgInS
vyslán	vyslat	k5eAaPmNgInS
silný	silný	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
perských	perský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
za	za	k7c4
svítání	svítání	k1gNnSc4
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
dosáhli	dosáhnout	k5eAaPmAgMnP
vrcholu	vrchol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
umístění	umístění	k1gNnSc4
Fókové	Fókus	k1gMnPc1
uprchli	uprchnout	k5eAaPmAgMnP
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
uviděli	uvidět	k5eAaPmAgMnP
blížící	blížící	k2eAgMnPc1d1
se	se	k3xPyFc4
nepřátele	nepřítel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
Leónidás	Leónidás	k1gInSc1
dozvěděl	dozvědět	k5eAaPmAgInS
o	o	k7c6
těchto	tento	k3xDgFnPc6
skutečnostech	skutečnost	k1gFnPc6
<g/>
,	,	kIx,
nařídil	nařídit	k5eAaPmAgMnS
svolat	svolat	k5eAaPmF
válečnou	válečný	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k9
uniknout	uniknout	k5eAaPmF
hrozícímu	hrozící	k2eAgNnSc3d1
obklíčení	obklíčení	k1gNnSc3
nalezením	nalezení	k1gNnSc7
lépe	dobře	k6eAd2
hájitelného	hájitelný	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
Leónidás	Leónidás	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
znal	znát	k5eAaImAgMnS
věštbu	věštba	k1gFnSc4
Pýthie	Pýthia	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
musel	muset	k5eAaImAgMnS
zhynout	zhynout	k5eAaPmF
buď	buď	k8xC
spartský	spartský	k2eAgMnSc1d1
král	král	k1gMnSc1
anebo	anebo	k8xC
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
Sparťanů	Sparťan	k1gMnPc2
a	a	k8xC
sedm	sedm	k4xCc4
set	sto	k4xCgNnPc2
Thespijců	Thespijce	k1gMnPc2
se	se	k3xPyFc4
pak	pak	k6eAd1
rozhodlo	rozhodnout	k5eAaPmAgNnS
setrvat	setrvat	k5eAaPmF
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
a	a	k8xC
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leónidás	Leónidás	k1gInSc1
nehodlal	hodlat	k5eNaImAgInS
vyčkávat	vyčkávat	k5eAaImF
na	na	k7c4
perský	perský	k2eAgInSc4d1
útok	útok	k1gInSc4
a	a	k8xC
v	v	k7c6
čele	čelo	k1gNnSc6
svých	svůj	k3xOyFgInPc2
druhů	druh	k1gInPc2
napadl	napadnout	k5eAaPmAgInS
nepřátele	nepřítel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíce	tisíc	k4xCgInPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
prý	prý	k9
byly	být	k5eAaImAgFnP
pobity	pobít	k5eAaPmNgFnP
a	a	k8xC
zbytek	zbytek	k1gInSc1
zatlačen	zatlačen	k2eAgInSc1d1
až	až	k9
k	k	k7c3
moři	moře	k1gNnSc3
<g/>
,	,	kIx,
když	když	k8xS
však	však	k9
byla	být	k5eAaImAgFnS
zlomena	zlomit	k5eAaPmNgFnS
většina	většina	k1gFnSc1
řeckých	řecký	k2eAgNnPc2d1
kopí	kopí	k1gNnPc2
<g/>
,	,	kIx,
začaly	začít	k5eAaPmAgFnP
ztráty	ztráta	k1gFnPc1
Helénů	Helén	k1gMnPc2
povážlivě	povážlivě	k6eAd1
stoupat	stoupat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
padlých	padlý	k1gMnPc2
byl	být	k5eAaImAgInS
sám	sám	k3xTgInSc1
Leónidás	Leónidás	k1gInSc1
<g/>
,	,	kIx,
kolem	kolem	k7c2
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
těla	tělo	k1gNnSc2
byl	být	k5eAaImAgInS
sveden	sveden	k2eAgInSc1d1
nejurputnější	urputný	k2eAgInSc1d3
boj	boj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
čtyřikrát	čtyřikrát	k6eAd1
zaútočili	zaútočit	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ho	on	k3xPp3gMnSc4
získali	získat	k5eAaPmAgMnP
<g/>
,	,	kIx,
pokaždé	pokaždé	k6eAd1
byli	být	k5eAaImAgMnP
odraženi	odrazit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyčerpaní	vyčerpaný	k2eAgMnPc1d1
a	a	k8xC
zranění	zraněný	k2eAgMnPc1d1
Sparťané	Sparťan	k1gMnPc1
a	a	k8xC
Thespijci	Thespijce	k1gMnPc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
i	i	k9
s	s	k7c7
Leónidovým	Leónidový	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
stáhli	stáhnout	k5eAaPmAgMnP
za	za	k7c4
hradby	hradba	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
obklíčeni	obklíčit	k5eAaPmNgMnP
a	a	k8xC
doraženi	doražen	k2eAgMnPc1d1
šípy	šíp	k1gInPc4
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
později	pozdě	k6eAd2
vztyčili	vztyčit	k5eAaPmAgMnP
na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
na	na	k7c4
počest	počest	k1gFnSc4
Leónida	Leónid	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
mužů	muž	k1gMnPc2
dva	dva	k4xCgInPc4
mramorové	mramorový	k2eAgInPc4d1
monumenty	monument	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
byla	být	k5eAaImAgNnP
vyryta	vyryt	k2eAgNnPc1d1
památná	památný	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Poutníče	Poutníec	k1gMnSc5
<g/>
,	,	kIx,
zvěstuj	zvěstovat	k5eAaImRp2nS
Lakedaimonským	Lakedaimonský	k2eAgFnPc3d1
<g/>
,	,	kIx,
poslušni	poslušen	k2eAgMnPc1d1
zákonů	zákon	k1gInPc2
jejich	jejich	k3xOp3gMnPc3
<g/>
,	,	kIx,
mrtvi	mrtev	k2eAgMnPc1d1
že	že	k8xS
ležíme	ležet	k5eAaImIp1nP
zde	zde	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Artemísion	Artemísion	k1gInSc1
<g/>
,	,	kIx,
vydrancování	vydrancování	k1gNnSc1
Athén	Athéna	k1gFnPc2
</s>
<s>
Postup	postup	k1gInSc1
Peršanů	Peršan	k1gMnPc2
k	k	k7c3
Thermopylám	Thermopyly	k1gFnPc3
a	a	k8xC
Salamíně	Salamína	k1gFnSc3
</s>
<s>
Mezitím	mezitím	k6eAd1
řecké	řecký	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgFnPc1d1
330	#num#	k4
triérami	triéra	k1gFnPc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
zhruba	zhruba	k6eAd1
dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
náležely	náležet	k5eAaImAgFnP
Athéňanům	Athéňan	k1gMnPc3
<g/>
,	,	kIx,
kotvilo	kotvit	k5eAaImAgNnS
u	u	k7c2
mysu	mys	k1gInSc2
Artemísion	Artemísion	k1gInSc4
na	na	k7c6
severu	sever	k1gInSc6
ostrova	ostrov	k1gInSc2
Euboia	Euboium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velení	velení	k1gNnSc1
příslušelo	příslušet	k5eAaImAgNnS
nikoli	nikoli	k9
Themistoklovi	Themistokl	k1gMnSc3
nýbrž	nýbrž	k8xC
spartskému	spartský	k2eAgMnSc3d1
nauarchovi	nauarch	k1gMnSc3
Eurybiadovi	Eurybiada	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
bouře	bouř	k1gFnSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Magnésie	Magnésie	k1gFnSc2
pokračovaly	pokračovat	k5eAaImAgFnP
perské	perský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
kolem	kolem	k7c2
Pagaského	Pagaský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
zakotvily	zakotvit	k5eAaPmAgFnP
u	u	k7c2
Afet	Afeta	k1gFnPc2
nedaleko	nedaleko	k7c2
řeckých	řecký	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zjištění	zjištění	k1gNnSc6
počtu	počet	k1gInSc2
perských	perský	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
chtěli	chtít	k5eAaImAgMnP
mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
Řeků	Řek	k1gMnPc2
opustit	opustit	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Themistoklés	Themistoklésa	k1gFnPc2
a	a	k8xC
Euboiané	Euboian	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
obávali	obávat	k5eAaImAgMnP
o	o	k7c4
své	svůj	k3xOyFgFnPc4
ženy	žena	k1gFnPc4
a	a	k8xC
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jen	jen	k9
s	s	k7c7
obtížemi	obtíž	k1gFnPc7
přesvědčili	přesvědčit	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vytrvali	vytrvat	k5eAaPmAgMnP
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
utkali	utkat	k5eAaPmAgMnP
se	se	k3xPyFc4
s	s	k7c7
Peršany	Peršan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
se	s	k7c7
200	#num#	k4
perských	perský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
vypravilo	vypravit	k5eAaPmAgNnS
podél	podél	k7c2
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Euboie	Euboie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Řekům	Řek	k1gMnPc3
zablokovalo	zablokovat	k5eAaPmAgNnS
únikovou	únikový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
obsazením	obsazení	k1gNnSc7
úžiny	úžina	k1gFnSc2
Euripos	Euriposa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
o	o	k7c6
tomto	tento	k3xDgInSc6
úskoku	úskok	k1gInSc6
dozvěděli	dozvědět	k5eAaPmAgMnP
a	a	k8xC
vyslali	vyslat	k5eAaPmAgMnP
na	na	k7c4
jih	jih	k1gInSc4
část	část	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
v	v	k7c6
tom	ten	k3xDgNnSc6
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
perských	perský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
pokusila	pokusit	k5eAaPmAgFnS
zabránit	zabránit	k5eAaPmF
<g/>
,	,	kIx,
rozhořel	rozhořet	k5eAaPmAgInS
se	se	k3xPyFc4
boj	boj	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
byli	být	k5eAaImAgMnP
Peršané	Peršan	k1gMnPc1
odraženi	odrazit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgFnSc6d1
noci	noc	k1gFnSc6
pak	pak	k6eAd1
bouře	bouře	k1gFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
perské	perský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
plující	plující	k2eAgFnPc1d1
k	k	k7c3
Euripu	Eurip	k1gInSc3
a	a	k8xC
většinu	většina	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gInPc2
vážně	vážně	k6eAd1
poničila	poničit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
nezdary	nezdar	k1gInPc1
ale	ale	k8xC
Peršany	peršan	k1gInPc1
neodradily	odradit	k5eNaPmAgInP
od	od	k7c2
dalšího	další	k2eAgInSc2d1
útoku	útok	k1gInSc2
o	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
celý	celý	k2eAgInSc4d1
den	den	k1gInSc4
a	a	k8xC
vyžádala	vyžádat	k5eAaPmAgFnS
si	se	k3xPyFc3
řadu	řada	k1gFnSc4
lodí	loď	k1gFnSc7
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Hérodotova	Hérodotův	k2eAgNnSc2d1
tvrzení	tvrzení	k1gNnSc2
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
nebo	nebo	k8xC
poškozena	poškodit	k5eAaPmNgFnS
polovina	polovina	k1gFnSc1
athénských	athénský	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
pádu	pád	k1gInSc6
Thermopyl	Thermopyly	k1gFnPc2
do	do	k7c2
rukou	ruka	k1gFnPc2
Peršanů	peršan	k1gInPc2
<g/>
,	,	kIx,
odpluly	odplout	k5eAaPmAgFnP
řecké	řecký	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
Euboiskou	Euboiska	k1gFnSc7
úžinou	úžina	k1gFnSc7
směrem	směr	k1gInSc7
k	k	k7c3
Athénám	Athéna	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
bitva	bitva	k1gFnSc1
u	u	k7c2
Artemísia	Artemísium	k1gNnSc2
skončila	skončit	k5eAaPmAgFnS
nerozhodně	rozhodně	k6eNd1
<g/>
,	,	kIx,
Řekům	Řek	k1gMnPc3
dodala	dodat	k5eAaPmAgFnS
naději	naděje	k1gFnSc4
a	a	k8xC
odvahu	odvaha	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
si	se	k3xPyFc3
uvědomili	uvědomit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
příznivějších	příznivý	k2eAgFnPc2d2
podmínek	podmínka	k1gFnPc2
by	by	kYmCp3nP
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
Peršany	peršan	k1gInPc4
na	na	k7c6
moři	moře	k1gNnSc6
porazit	porazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
vtrhl	vtrhnout	k5eAaPmAgMnS
Xerxés	Xerxés	k1gInSc4
do	do	k7c2
Dóridy	Dórida	k1gFnSc2
a	a	k8xC
Fókidy	Fókida	k1gFnSc2
a	a	k8xC
vypaloval	vypalovat	k5eAaImAgInS
tamní	tamní	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
Athéňané	Athéňan	k1gMnPc1
žádali	žádat	k5eAaImAgMnP
ostatní	ostatní	k2eAgMnPc4d1
Řeky	Řek	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
šli	jít	k5eAaImAgMnP
Fókům	Fók	k1gInPc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
Sparťané	Sparťan	k1gMnPc1
a	a	k8xC
většina	většina	k1gFnSc1
Peloponésanů	Peloponésan	k1gMnPc2
soustředili	soustředit	k5eAaPmAgMnP
svoje	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
na	na	k7c4
Isthmos	Isthmos	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
hodlali	hodlat	k5eAaImAgMnP
opevnit	opevnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
této	tento	k3xDgFnSc2
zoufalé	zoufalý	k2eAgFnSc2d1
situace	situace	k1gFnSc2
navrhl	navrhnout	k5eAaPmAgMnS
Themistoklés	Themistoklés	k1gInSc4
svým	svůj	k3xOyFgMnPc3
spoluobčanům	spoluobčan	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
opustili	opustit	k5eAaPmAgMnP
Athény	Athéna	k1gFnPc4
a	a	k8xC
spolehli	spolehnout	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
své	svůj	k3xOyFgFnPc4
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
podle	podle	k7c2
věštby	věštba	k1gFnSc2
„	„	k?
<g/>
dopustí	dopustit	k5eAaPmIp3nS
Zeus	Zeus	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Heladě	Helada	k1gFnSc6
zůstanou	zůstat	k5eAaPmIp3nP
dřevěné	dřevěný	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nápory	nápor	k1gInPc1
vydrží	vydržet	k5eAaPmIp3nP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgFnPc7
dřevěnými	dřevěný	k2eAgFnPc7d1
hradbami	hradba	k1gFnPc7
byly	být	k5eAaImAgFnP
podle	podle	k7c2
Themistokla	Themistokla	k1gFnSc2
myšleny	myšlen	k2eAgFnPc4d1
athénské	athénský	k2eAgFnPc4d1
triéry	triéra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
rozvažování	rozvažování	k1gNnSc6
Themistoklův	Themistoklův	k2eAgInSc4d1
návrh	návrh	k1gInSc4
přijali	přijmout	k5eAaPmAgMnP
<g/>
,	,	kIx,
načež	načež	k6eAd1
byly	být	k5eAaImAgFnP
ženy	žena	k1gFnPc1
a	a	k8xC
děti	dítě	k1gFnPc1
dopraveny	dopravit	k5eAaPmNgFnP
do	do	k7c2
Troizény	Troizéna	k1gFnSc2
a	a	k8xC
na	na	k7c4
Salamínu	Salamína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okamžiku	okamžik	k1gInSc6
největšího	veliký	k2eAgNnSc2d3
nebezpečí	nebezpečí	k1gNnSc2
svolil	svolit	k5eAaPmAgMnS
Themistoklés	Themistoklés	k1gInSc4
k	k	k7c3
návratu	návrat	k1gInSc3
vypuzených	vypuzený	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
včetně	včetně	k7c2
Aristeida	Aristeid	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
chtěl	chtít	k5eAaImAgMnS
upevnit	upevnit	k5eAaPmF
jednotu	jednota	k1gFnSc4
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xerxés	Xerxésa	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
vojáci	voják	k1gMnPc1
zatím	zatím	k6eAd1
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
opuštěných	opuštěný	k2eAgFnPc2d1
Athén	Athéna	k1gFnPc2
a	a	k8xC
oblehli	oblehnout	k5eAaPmAgMnP
Akropolis	Akropolis	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc1
obránci	obránce	k1gMnPc1
se	se	k3xPyFc4
odmítali	odmítat	k5eAaImAgMnP
vzdát	vzdát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
je	on	k3xPp3gInPc4
však	však	k9
brzy	brzy	k6eAd1
přemohli	přemoct	k5eAaPmAgMnP
a	a	k8xC
zmasakrovali	zmasakrovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
chrámy	chrám	k1gInPc4
a	a	k8xC
budovy	budova	k1gFnPc4
na	na	k7c6
athénském	athénský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
byly	být	k5eAaImAgInP
vydrancovány	vydrancován	k2eAgInPc1d1
a	a	k8xC
spáleny	spálen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
Řeky	Řek	k1gMnPc7
pak	pak	k6eAd1
Athéňané	Athéňan	k1gMnPc1
na	na	k7c6
lodích	loď	k1gFnPc6
i	i	k8xC
na	na	k7c6
Salamíně	Salamína	k1gFnSc6
viděli	vidět	k5eAaImAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
zlověstné	zlověstný	k2eAgInPc1d1
plameny	plamen	k1gInPc1
stravují	stravovat	k5eAaImIp3nP
Akropoli	Akropole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Peloponéští	peloponéský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
flotily	flotila	k1gFnSc2
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
Sparťan	Sparťan	k1gMnSc1
Eurybiadés	Eurybiadésa	k1gFnPc2
a	a	k8xC
Korinťan	Korinťan	k1gMnSc1
Adeimantos	Adeimantos	k1gMnSc1
<g/>
,	,	kIx,
chtěli	chtít	k5eAaImAgMnP
ustoupit	ustoupit	k5eAaPmF
k	k	k7c3
Isthmu	Isthmo	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
shromažďovalo	shromažďovat	k5eAaImAgNnS
řecké	řecký	k2eAgNnSc1d1
pozemní	pozemní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Themistoklés	Themistoklés	k1gInSc1
jim	on	k3xPp3gMnPc3
však	však	k9
pohrozil	pohrozit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
200	#num#	k4
athénských	athénský	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
tvořily	tvořit	k5eAaImAgFnP
jádro	jádro	k1gNnSc4
řeckého	řecký	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
,	,	kIx,
odpluje	odplout	k5eAaPmIp3nS
k	k	k7c3
jižní	jižní	k2eAgFnSc3d1
Itálii	Itálie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
zdůrazňoval	zdůrazňovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
přenechají	přenechat	k5eAaPmIp3nP
<g/>
-li	-li	k?
Helénové	Helén	k1gMnPc1
průliv	průliv	k1gInSc1
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
Peršanům	Peršan	k1gMnPc3
<g/>
,	,	kIx,
nejenže	nejenže	k6eAd1
budou	být	k5eAaImBp3nP
muset	muset	k5eAaImF
riskovat	riskovat	k5eAaBmF
bitvu	bitva	k1gFnSc4
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jejich	jejich	k3xOp3gFnPc4
šance	šance	k1gFnPc4
na	na	k7c6
vítězství	vítězství	k1gNnSc6
budou	být	k5eAaImBp3nP
podstatně	podstatně	k6eAd1
nižší	nízký	k2eAgInPc1d2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
perské	perský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
vylodit	vylodit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
pozemní	pozemní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
kdekoli	kdekoli	k6eAd1
na	na	k7c6
Peloponésu	Peloponés	k1gInSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
budou	být	k5eAaImBp3nP
Řekové	Řek	k1gMnPc1
na	na	k7c6
Isthmu	Isthm	k1gInSc6
obklíčeni	obklíčit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgFnPc7
slovy	slovo	k1gNnPc7
získal	získat	k5eAaPmAgMnS
Themistoklés	Themistoklésa	k1gFnPc2
Eurybiada	Eurybiada	k1gFnSc1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
jakmile	jakmile	k8xS
se	se	k3xPyFc4
mohutná	mohutný	k2eAgFnSc1d1
králova	králův	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
přiblížila	přiblížit	k5eAaPmAgFnS
i	i	k9
s	s	k7c7
perským	perský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
k	k	k7c3
úžině	úžina	k1gFnSc3
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
<g/>
,	,	kIx,
Řekové	Řek	k1gMnPc1
znovu	znovu	k6eAd1
znejistěli	znejistět	k5eAaPmAgMnP
a	a	k8xC
chystali	chystat	k5eAaImAgMnP
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
Themistoklés	Themistoklés	k1gInSc4
vyslal	vyslat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc2
oddaného	oddaný	k2eAgMnSc2d1
perského	perský	k2eAgMnSc2d1
otroka	otrok	k1gMnSc2
Sikkina	Sikkin	k1gMnSc2
ke	k	k7c3
Xerxovi	Xerxes	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
od	od	k7c2
Themistokla	Themistokla	k1gMnPc2
jako	jako	k8xC,k8xS
králova	králův	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
vzkázal	vzkázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Helénové	Helén	k1gMnPc1
postrádají	postrádat	k5eAaImIp3nP
svornost	svornost	k1gFnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
připraveni	připravit	k5eAaPmNgMnP
prchnout	prchnout	k5eAaPmF
od	od	k7c2
Salamíny	Salamína	k1gFnSc2
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
Xerxés	Xerxés	k1gInSc1
Řeky	řeka	k1gFnSc2
obklíčit	obklíčit	k5eAaPmF
a	a	k8xC
zajistit	zajistit	k5eAaPmF
si	se	k3xPyFc3
tak	tak	k6eAd1
snadné	snadný	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
téže	týž	k3xTgFnSc2,k3xDgFnSc2
noci	noc	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
Řekové	Řek	k1gMnPc1
radili	radit	k5eAaImAgMnP
o	o	k7c6
dalším	další	k2eAgInSc6d1
postupu	postup	k1gInSc6
<g/>
,	,	kIx,
zablokovaly	zablokovat	k5eAaPmAgFnP
královy	králův	k2eAgFnPc1d1
egyptské	egyptský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
mezi	mezi	k7c7
Megarou	Megara	k1gFnSc7
a	a	k8xC
Salamínou	Salamína	k1gFnSc7
západní	západní	k2eAgFnSc7d1
ústí	ústí	k1gNnSc4
úžiny	úžina	k1gFnSc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
si	se	k3xPyFc3
jako	jako	k9
první	první	k4xOgMnSc1
povšiml	povšimnout	k5eAaPmAgMnS
Aristeidés	Aristeidés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
Peršané	Peršan	k1gMnPc1
provedli	provést	k5eAaPmAgMnP
námořní	námořní	k2eAgInSc4d1
výsadek	výsadek	k1gInSc4
svých	svůj	k3xOyFgNnPc2
pozemních	pozemní	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
na	na	k7c6
ostrůvku	ostrůvek	k1gInSc6
Psytalleia	Psytalleium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Helénové	Helén	k1gMnPc1
byli	být	k5eAaImAgMnP
nyní	nyní	k6eAd1
zcela	zcela	k6eAd1
odříznuti	odříznout	k5eAaPmNgMnP
<g/>
,	,	kIx,
nezbylo	zbýt	k5eNaPmAgNnS
jim	on	k3xPp3gMnPc3
než	než	k8xS
svést	svést	k5eAaPmF
bitvu	bitva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
triéry	triéra	k1gFnSc2
</s>
<s>
V	v	k7c6
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
zřejmě	zřejmě	k6eAd1
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
perské	perský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Féničany	Féničan	k1gMnPc7
vydaly	vydat	k5eAaPmAgFnP
od	od	k7c2
attického	attický	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
směrem	směr	k1gInSc7
k	k	k7c3
úžině	úžina	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
seskupily	seskupit	k5eAaPmAgInP
do	do	k7c2
tří	tři	k4xCgFnPc2
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
počet	počet	k1gInSc1
byl	být	k5eAaImAgInS
zhruba	zhruba	k6eAd1
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
oproti	oproti	k7c3
350	#num#	k4
řeckým	řecký	k2eAgFnPc3d1
triérám	triéra	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
noci	noc	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Salamíny	Salamína	k1gFnSc2
připravily	připravit	k5eAaPmAgFnP
rovněž	rovněž	k9
řecké	řecký	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
paluby	paluba	k1gFnPc1
byly	být	k5eAaImAgFnP
zaplněny	zaplněn	k2eAgMnPc4d1
námořníky	námořník	k1gMnPc4
a	a	k8xC
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
zaujali	zaujmout	k5eAaPmAgMnP
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
čelili	čelit	k5eAaImAgMnP
Féničanům	Féničan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euboiané	Euboianá	k1gFnPc1
a	a	k8xC
Aigíňané	Aigíňan	k1gMnPc1
byli	být	k5eAaImAgMnP
umístěni	umístit	k5eAaPmNgMnP
ve	v	k7c6
středu	střed	k1gInSc6
bojové	bojový	k2eAgFnSc2d1
sestavy	sestava	k1gFnSc2
a	a	k8xC
na	na	k7c6
čestné	čestný	k2eAgFnSc6d1
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nacházeli	nacházet	k5eAaImAgMnP
Sparťané	Sparťan	k1gMnPc1
a	a	k8xC
ostatní	ostatní	k2eAgFnPc1d1
Peloponésané	Peloponésaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
byl	být	k5eAaImAgInS
boj	boj	k1gInSc1
zahájen	zahájit	k5eAaPmNgInS
<g/>
,	,	kIx,
usedl	usednout	k5eAaPmAgMnS
Xerxés	Xerxés	k1gInSc4
na	na	k7c4
zlatý	zlatý	k2eAgInSc4d1
trůn	trůn	k1gInSc4
na	na	k7c6
výšině	výšina	k1gFnSc6
nad	nad	k7c7
průlivem	průliv	k1gInSc7
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
pozoroval	pozorovat	k5eAaImAgMnS
střetnutí	střetnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
se	se	k3xPyFc4
rozhořela	rozhořet	k5eAaPmAgFnS
po	po	k7c6
útoku	útok	k1gInSc6
jedné	jeden	k4xCgFnSc2
z	z	k7c2
aigínských	aigínský	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
ihned	ihned	k6eAd1
přispěchali	přispěchat	k5eAaPmAgMnP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
ostatní	ostatní	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnSc1d2
velikost	velikost	k1gFnSc1
a	a	k8xC
větší	veliký	k2eAgFnSc1d2
bytelnost	bytelnost	k1gFnSc1
jejich	jejich	k3xOp3gNnPc2
plavidel	plavidlo	k1gNnPc2
jim	on	k3xPp3gFnPc3
i	i	k9
při	při	k7c6
nižší	nízký	k2eAgFnSc6d2
rychlosti	rychlost	k1gFnSc6
zajišťovala	zajišťovat	k5eAaImAgFnS
vyšší	vysoký	k2eAgFnSc1d2
manévrovatelnost	manévrovatelnost	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
chyběla	chybět	k5eAaImAgFnS
obrovským	obrovský	k2eAgInPc3d1
perským	perský	k2eAgInPc3d1
korábům	koráb	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
navíc	navíc	k6eAd1
zápolily	zápolit	k5eAaImAgInP
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
Féničané	Féničan	k1gMnPc1
přiblížili	přiblížit	k5eAaPmAgMnP
ke	k	k7c3
svým	svůj	k3xOyFgMnPc3
protivníkům	protivník	k1gMnPc3
<g/>
,	,	kIx,
vrhly	vrhnout	k5eAaImAgFnP,k5eAaPmAgFnP
se	se	k3xPyFc4
athénské	athénský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
do	do	k7c2
jejich	jejich	k3xOp3gInPc2
boků	bok	k1gInPc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
je	on	k3xPp3gFnPc4
oddělily	oddělit	k5eAaPmAgFnP
od	od	k7c2
zbytku	zbytek	k1gInSc2
flotily	flotila	k1gFnSc2
a	a	k8xC
zahnaly	zahnat	k5eAaPmAgFnP
k	k	k7c3
attickým	attický	k2eAgInPc3d1
břehům	břeh	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
ukázněné	ukázněný	k2eAgFnPc4d1
a	a	k8xC
bez	bez	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
nepořádku	nepořádek	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Peršané	Peršan	k1gMnPc1
se	se	k3xPyFc4
navzdory	navzdory	k6eAd1
své	svůj	k3xOyFgFnSc2
statečnosti	statečnost	k1gFnSc2
potýkali	potýkat	k5eAaImAgMnP
s	s	k7c7
chybějícím	chybějící	k2eAgInSc7d1
taktickým	taktický	k2eAgInSc7d1
plánem	plán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádky	posádka	k1gFnSc2
do	do	k7c2
sebe	sebe	k3xPyFc4
vrážejících	vrážející	k2eAgFnPc2d1
perských	perský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
vystavených	vystavený	k2eAgFnPc2d1
úderům	úder	k1gInPc3
zobců	zobec	k1gInPc2
triér	triéra	k1gFnPc2
posléze	posléze	k6eAd1
zpanikařily	zpanikařit	k5eAaPmAgInP
a	a	k8xC
obrátily	obrátit	k5eAaPmAgInP
své	svůj	k3xOyFgFnPc4
lodě	loď	k1gFnPc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athénské	athénský	k2eAgFnPc1d1
triéry	triéra	k1gFnPc1
dobíjely	dobíjet	k5eAaImAgFnP
izolované	izolovaný	k2eAgFnPc1d1
perské	perský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
a	a	k8xC
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
Athéňanům	Athéňan	k1gMnPc3
unikly	uniknout	k5eAaPmAgFnP
<g/>
,	,	kIx,
zničili	zničit	k5eAaPmAgMnP
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
moři	moře	k1gNnSc6
Aigíňané	Aigíňan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
průliv	průliv	k1gInSc1
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
byl	být	k5eAaImAgMnS
zaplněn	zaplněn	k2eAgInSc4d1
vraky	vrak	k1gInPc4
poškozených	poškozený	k2eAgFnPc2d1
a	a	k8xC
hořících	hořící	k2eAgFnPc2d1
perských	perský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aristeidés	Aristeidés	k1gInSc1
poté	poté	k6eAd1
s	s	k7c7
oddílem	oddíl	k1gInSc7
hoplítů	hoplít	k1gInPc2
obsadil	obsadit	k5eAaPmAgMnS
Psyttaleiu	Psyttaleius	k1gMnSc3
a	a	k8xC
pobil	pobít	k5eAaPmAgMnS
všechny	všechen	k3xTgMnPc4
zdejší	zdejší	k2eAgMnPc4d1
perské	perský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
po	po	k7c6
západu	západ	k1gInSc6
slunce	slunce	k1gNnSc2
naprostým	naprostý	k2eAgNnSc7d1
řeckým	řecký	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
v	v	k7c6
tomto	tento	k3xDgInSc6
zápase	zápas	k1gInSc6
o	o	k7c4
40	#num#	k4
triér	triéra	k1gFnPc2
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
perské	perský	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
činily	činit	k5eAaImAgFnP
200	#num#	k4
lodí	loď	k1gFnPc2
a	a	k8xC
kolem	kolem	k7c2
50	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
přes	přes	k7c4
porážku	porážka	k1gFnSc4
na	na	k7c6
moři	moře	k1gNnSc6
perské	perský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
dosud	dosud	k6eAd1
okupovalo	okupovat	k5eAaBmAgNnS
Attiku	Attika	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
loďstvo	loďstvo	k1gNnSc1
nadále	nadále	k6eAd1
převyšovalo	převyšovat	k5eAaImAgNnS
řecké	řecký	k2eAgNnSc1d1
svým	svůj	k3xOyFgInSc7
počtem	počet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zuřící	zuřící	k2eAgInSc1d1
Xerxés	Xerxés	k1gInSc1
nechal	nechat	k5eAaPmAgInS
po	po	k7c6
bitvě	bitva	k1gFnSc6
popravit	popravit	k5eAaPmF
řadu	řada	k1gFnSc4
Féničanů	Féničan	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
viníky	viník	k1gMnPc4
porážky	porážka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgFnSc1d1
fénické	fénický	k2eAgNnSc4d1
loďstvo	loďstvo	k1gNnSc4
strachující	strachující	k2eAgNnSc4d1
se	se	k3xPyFc4
králova	králův	k2eAgInSc2d1
hněvu	hněv	k1gInSc2
následně	následně	k6eAd1
odplulo	odplout	k5eAaPmAgNnS
z	z	k7c2
Faléru	Falér	k1gInSc2
zpět	zpět	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgInPc2
domovských	domovský	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perské	perský	k2eAgFnPc4d1
námořnictvo	námořnictvo	k1gNnSc1
tím	ten	k3xDgNnSc7
ztratilo	ztratit	k5eAaPmAgNnS
nadvládu	nadvláda	k1gFnSc4
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
takže	takže	k8xS
nebylo	být	k5eNaImAgNnS
schopno	schopen	k2eAgNnSc1d1
zabezpečit	zabezpečit	k5eAaPmF
králův	králův	k2eAgInSc4d1
návrat	návrat	k1gInSc4
do	do	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xerxés	Xerxés	k1gInSc1
naplněný	naplněný	k2eAgInSc1d1
obavami	obava	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
Helénové	Helén	k1gMnPc1
nepokusili	pokusit	k5eNaPmAgMnP
strhnout	strhnout	k5eAaPmF
most	most	k1gInSc4
přes	přes	k7c4
Helléspont	Helléspont	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
vzbouřily	vzbouřit	k5eAaPmAgFnP
řecké	řecký	k2eAgFnPc1d1
obce	obec	k1gFnPc1
na	na	k7c6
Chalkidiké	Chalkidiká	k1gFnSc6
<g/>
,	,	kIx,
rozkázal	rozkázat	k5eAaPmAgInS
většině	většina	k1gFnSc3
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
a	a	k8xC
zbytku	zbytek	k1gInSc2
lodím	lodit	k5eAaImIp1nS
vrátit	vrátit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
Sard	Sardy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršany	peršan	k1gInPc1
odplouvající	odplouvající	k2eAgInPc1d1
z	z	k7c2
Faléru	Falér	k1gInSc2
stíhali	stíhat	k5eAaImAgMnP
Řekové	Řek	k1gMnPc1
až	až	k6eAd1
k	k	k7c3
ostrovu	ostrov	k1gInSc3
Andros	Androsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
perského	perský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
byla	být	k5eAaImAgFnS
ponechána	ponechat	k5eAaPmNgFnS
v	v	k7c6
Řecku	Řecko	k1gNnSc6
pod	pod	k7c7
velením	velení	k1gNnSc7
Mardonia	Mardonium	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
doprovázel	doprovázet	k5eAaImAgMnS
krále	král	k1gMnSc4
při	při	k7c6
jeho	jeho	k3xOp3gInSc6
ústupu	ústup	k1gInSc6
až	až	k9
do	do	k7c2
Thesálie	Thesálie	k1gFnSc2
a	a	k8xC
zde	zde	k6eAd1
pak	pak	k6eAd1
přezimoval	přezimovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
zimy	zima	k1gFnSc2
Mardonios	Mardonios	k1gMnSc1
zkonsolidoval	zkonsolidovat	k5eAaPmAgMnS
perské	perský	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Attiky	Attika	k1gFnSc2
a	a	k8xC
znovu	znovu	k6eAd1
dobyl	dobýt	k5eAaPmAgInS
Athény	Athéna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neúspěšných	úspěšný	k2eNgInPc6d1
pokusech	pokus	k1gInPc6
přimět	přimět	k5eAaPmF
Athéňany	Athéňan	k1gMnPc4
ke	k	k7c3
změně	změna	k1gFnSc3
stran	strana	k1gFnPc2
dal	dát	k5eAaPmAgMnS
Mardonios	Mardonios	k1gMnSc1
celé	celý	k2eAgNnSc4d1
město	město	k1gNnSc4
srovnat	srovnat	k5eAaPmF
se	se	k3xPyFc4
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Závěr	závěr	k1gInSc1
tažení	tažení	k1gNnSc2
</s>
<s>
Zdrženlivost	zdrženlivost	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
Sparta	Sparta	k1gFnSc1
projevovala	projevovat	k5eAaImAgFnS
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
<g/>
,	,	kIx,
příliš	příliš	k6eAd1
neprospěla	prospět	k5eNaPmAgFnS
řecké	řecký	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
to	ten	k3xDgNnSc1
však	však	k9
byly	být	k5eAaImAgInP
právě	právě	k9
spartské	spartský	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
trvale	trvale	k6eAd1
ukončily	ukončit	k5eAaPmAgInP
perskou	perský	k2eAgFnSc4d1
hrozbu	hrozba	k1gFnSc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
dojmem	dojem	k1gInSc7
obnoveného	obnovený	k2eAgInSc2d1
perského	perský	k2eAgInSc2d1
útoku	útok	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
létě	léto	k1gNnSc6
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vypravil	vypravit	k5eAaPmAgInS
Pausaniás	Pausaniás	k1gInSc4
v	v	k7c6
čele	čelo	k1gNnSc6
5000	#num#	k4
spartiatů	spartiat	k1gMnPc2
a	a	k8xC
5000	#num#	k4
peroiků	peroik	k1gInPc2
<g/>
,	,	kIx,
doprovázených	doprovázený	k2eAgInPc2d1
podle	podle	k7c2
Hérodota	Hérodot	k1gMnSc2
35	#num#	k4
000	#num#	k4
heilóty	heilóta	k1gMnSc2
<g/>
,	,	kIx,
ze	z	k7c2
Sparty	Sparta	k1gFnSc2
na	na	k7c4
Isthmos	Isthmos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
vojenská	vojenský	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
jakou	jaký	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
Sparta	Sparta	k1gFnSc1
vyslala	vyslat	k5eAaPmAgFnS
do	do	k7c2
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
spartské	spartský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
se	se	k3xPyFc4
připojily	připojit	k5eAaPmAgInP
také	také	k9
vojenské	vojenský	k2eAgInPc1d1
sbory	sbor	k1gInPc1
mnoha	mnoho	k4c2
dalších	další	k2eAgMnPc2d1
řeckých	řecký	k2eAgMnPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
8000	#num#	k4
těžkooděnců	těžkooděnec	k1gMnPc2
<g/>
,	,	kIx,
Korinťané	Korinťan	k1gMnPc1
5000	#num#	k4
<g/>
,	,	kIx,
Megařané	Megařan	k1gMnPc1
3000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecké	řecký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
čítalo	čítat	k5eAaImAgNnS
celkově	celkově	k6eAd1
kolem	kolem	k7c2
38	#num#	k4
000	#num#	k4
hoplítů	hoplít	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgInPc3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
připočítat	připočítat	k5eAaPmF
ještě	ještě	k9
lehkooděnce	lehkooděnec	k1gMnPc4
a	a	k8xC
heilóty	heilót	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slabinu	slabina	k1gFnSc4
Helénů	Helén	k1gMnPc2
ale	ale	k8xC
představovala	představovat	k5eAaImAgFnS
absence	absence	k1gFnSc1
jízdy	jízda	k1gFnSc2
a	a	k8xC
nízký	nízký	k2eAgInSc1d1
počet	počet	k1gInSc1
lučištníků	lučištník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perské	perský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
byly	být	k5eAaImAgFnP
asi	asi	k9
trojnásobné	trojnásobný	k2eAgNnSc4d1
oproti	oproti	k7c3
Řekům	Řek	k1gMnPc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
součástí	součást	k1gFnSc7
Mardoniova	Mardoniův	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
byly	být	k5eAaImAgFnP
i	i	k9
kontingenty	kontingent	k1gInPc4
Thébanů	Théban	k1gMnPc2
a	a	k8xC
jiných	jiný	k2eAgMnPc2d1
properských	properský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Mardonios	Mardonios	k1gMnSc1
doslechl	doslechnout	k5eAaPmAgMnS
o	o	k7c6
postupu	postup	k1gInSc6
Lakedaimónských	lakedaimónský	k2eAgFnPc2d1
<g/>
,	,	kIx,
ustoupil	ustoupit	k5eAaPmAgMnS
z	z	k7c2
Attiky	Attika	k1gFnSc2
do	do	k7c2
Bojótie	Bojótie	k1gFnSc2
a	a	k8xC
zde	zde	k6eAd1
se	se	k3xPyFc4
utábořil	utábořit	k5eAaPmAgMnS
u	u	k7c2
řeky	řeka	k1gFnSc2
Ásópos	Ásóposa	k1gFnPc2
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Plataje	Plataje	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Řecký	řecký	k2eAgInSc1d1
hoplít	hoplít	k5eAaPmF
</s>
<s>
Řekové	Řek	k1gMnPc1
zamýšleli	zamýšlet	k5eAaImAgMnP
svést	svést	k5eAaPmF
obrannou	obranný	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
by	by	kYmCp3nP
využili	využít	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
falangy	falanga	k1gFnPc4
a	a	k8xC
tudíž	tudíž	k8xC
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
vyprovokovat	vyprovokovat	k5eAaPmF
Peršany	peršan	k1gInPc4
k	k	k7c3
útoku	útok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
Mardonios	Mardonios	k1gInSc1
si	se	k3xPyFc3
počínal	počínat	k5eAaImAgInS
nanejvýš	nanejvýš	k6eAd1
obezřetně	obezřetně	k6eAd1
a	a	k8xC
neprojevoval	projevovat	k5eNaImAgMnS
nejmenší	malý	k2eAgInSc4d3
úmysl	úmysl	k1gInSc4
Řeky	řeka	k1gFnSc2
napadnout	napadnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecké	řecký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
zformovalo	zformovat	k5eAaPmAgNnS
na	na	k7c6
vyvýšeninách	vyvýšenina	k1gFnPc6
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Ásópu	Ásóp	k1gInSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
asi	asi	k9
osmi	osm	k4xCc2
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Peršanů	Peršan	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
opevnili	opevnit	k5eAaPmAgMnP
na	na	k7c6
protější	protější	k2eAgFnSc6d1
straně	strana	k1gFnSc6
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nadcházející	nadcházející	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Platají	Platají	k1gFnSc2
tvořili	tvořit	k5eAaImAgMnP
Sparťané	Sparťan	k1gMnPc1
pravé	pravá	k1gFnSc2
křídlo	křídlo	k1gNnSc1
řeckého	řecký	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
nalevo	nalevo	k6eAd1
stáli	stát	k5eAaImAgMnP
Athéňané	Athéňan	k1gMnPc1
a	a	k8xC
oddíly	oddíl	k1gInPc1
ostatních	ostatní	k2eAgFnPc2d1
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
byly	být	k5eAaImAgInP
rozmístěny	rozmístit	k5eAaPmNgInP
ve	v	k7c6
středu	střed	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
deseti	deset	k4xCc2
dnech	den	k1gInPc6
vzájemného	vzájemný	k2eAgNnSc2d1
vyčkávání	vyčkávání	k1gNnSc2
<g/>
,	,	kIx,
nařídil	nařídit	k5eAaPmAgMnS
Mardonios	Mardonios	k1gInSc4
svým	svůj	k3xOyFgMnPc3
lučištníkům	lučištník	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zabránili	zabránit	k5eAaPmAgMnP
Řekům	Řek	k1gMnPc3
v	v	k7c6
přístupu	přístup	k1gInSc6
k	k	k7c3
řece	řeka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
do	do	k7c2
týlu	týl	k1gInSc2
řeckého	řecký	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
vyslal	vyslat	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
jezdce	jezdec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
znečistili	znečistit	k5eAaPmAgMnP
prameny	pramen	k1gInPc4
vody	voda	k1gFnSc2
a	a	k8xC
ohrožovali	ohrožovat	k5eAaImAgMnP
zásobovací	zásobovací	k2eAgFnPc4d1
linie	linie	k1gFnPc4
Helénů	Helén	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pausaniás	Pausaniás	k1gInSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
rozhodl	rozhodnout	k5eAaPmAgInS
stáhnout	stáhnout	k5eAaPmF
vojsko	vojsko	k1gNnSc4
výše	výše	k1gFnSc2,k1gFnSc2wB
do	do	k7c2
kopců	kopec	k1gInPc2
a	a	k8xC
zaujmout	zaujmout	k5eAaPmF
tak	tak	k6eAd1
výhodnější	výhodný	k2eAgNnSc4d2
postavení	postavení	k1gNnSc4
blíže	blíž	k1gFnSc2
Platajím	Platají	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
nočního	noční	k2eAgInSc2d1
přesunu	přesun	k1gInSc2
Řeků	Řek	k1gMnPc2
došlo	dojít	k5eAaPmAgNnS
však	však	k9
ke	k	k7c3
zmatkům	zmatek	k1gInPc3
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
Mardonios	Mardonios	k1gMnSc1
ráno	ráno	k6eAd1
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
nařídil	nařídit	k5eAaPmAgMnS
rozhodný	rozhodný	k2eAgInSc4d1
útok	útok	k1gInSc4
proti	proti	k7c3
rozptýleným	rozptýlený	k2eAgMnPc3d1
Řekům	Řek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
byli	být	k5eAaImAgMnP
perskou	perský	k2eAgFnSc7d1
jízdou	jízda	k1gFnSc7
napadeni	napaden	k2eAgMnPc1d1
Sparťané	Sparťan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
kryli	krýt	k5eAaImAgMnP
noční	noční	k2eAgInSc4d1
ústup	ústup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	dlouho	k6eNd1
nato	nato	k6eAd1
byli	být	k5eAaImAgMnP
Athéňané	Athéňan	k1gMnPc1
vystaveni	vystavit	k5eAaPmNgMnP
útoku	útok	k1gInSc3
properských	properský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
zasypávali	zasypávat	k5eAaImAgMnP
spartskou	spartský	k2eAgFnSc4d1
falangu	falanga	k1gFnSc4
salvami	salva	k1gFnPc7
šípů	šíp	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
jí	on	k3xPp3gFnSc3
způsobili	způsobit	k5eAaPmAgMnP
vážné	vážný	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
když	když	k8xS
se	se	k3xPyFc4
Mardonios	Mardonios	k1gMnSc1
přiblížil	přiblížit	k5eAaPmAgMnS
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
pěchotou	pěchota	k1gFnSc7
ke	k	k7c3
spartské	spartský	k2eAgFnSc3d1
linii	linie	k1gFnSc3
<g/>
,	,	kIx,
podnikl	podniknout	k5eAaPmAgMnS
Pausaniás	Pausaniás	k1gInSc4
okamžitý	okamžitý	k2eAgInSc4d1
a	a	k8xC
rozhodný	rozhodný	k2eAgInSc4d1
protiútok	protiútok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
snést	snést	k5eAaPmF
nápor	nápor	k1gInSc4
spartských	spartský	k2eAgMnPc2d1
těžkooděnců	těžkooděnec	k1gMnPc2
a	a	k8xC
začali	začít	k5eAaPmAgMnP
pozvolna	pozvolna	k6eAd1
ustupovat	ustupovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
momentě	moment	k1gInSc6
hrozilo	hrozit	k5eAaImAgNnS
Athéňanům	Athéňan	k1gMnPc3
na	na	k7c6
levém	levý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
obklíčení	obklíčení	k1gNnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
včasný	včasný	k2eAgInSc1d1
návrat	návrat	k1gInSc1
řeckého	řecký	k2eAgInSc2d1
středu	střed	k1gInSc2
zamezil	zamezit	k5eAaPmAgMnS
athénské	athénský	k2eAgFnSc3d1
pohromě	pohroma	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Sparťané	Sparťan	k1gMnPc1
zabili	zabít	k5eAaPmAgMnP
v	v	k7c6
lítém	lítý	k2eAgInSc6d1
boji	boj	k1gInSc6
Mardonia	Mardonium	k1gNnSc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
řeckými	řecký	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
se	se	k3xPyFc4
pustili	pustit	k5eAaPmAgMnP
do	do	k7c2
pronásledování	pronásledování	k1gNnSc2
Peršanů	peršan	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
ústup	ústup	k1gInSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
změnil	změnit	k5eAaPmAgInS
v	v	k7c4
regulérní	regulérní	k2eAgInSc4d1
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
Athéňané	Athéňan	k1gMnPc1
získali	získat	k5eAaPmAgMnP
navrch	navrch	k6eAd1
nad	nad	k7c7
proti	proti	k7c3
nim	on	k3xPp3gMnPc3
stojícími	stojící	k2eAgMnPc7d1
Thébany	Théban	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
perského	perský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
hledala	hledat	k5eAaImAgFnS
záchranu	záchrana	k1gFnSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
opevněném	opevněný	k2eAgInSc6d1
táboře	tábor	k1gInSc6
<g/>
,	,	kIx,
Řekům	Řek	k1gMnPc3
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
ho	on	k3xPp3gMnSc4
dobýt	dobýt	k5eAaPmF
<g/>
,	,	kIx,
takže	takže	k8xS
všichni	všechen	k3xTgMnPc1
královi	králův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
něm	on	k3xPp3gNnSc6
byli	být	k5eAaImAgMnP
zmasakrováni	zmasakrován	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
Peršanů	Peršan	k1gMnPc2
se	se	k3xPyFc4
pak	pak	k6eAd1
vydaly	vydat	k5eAaPmAgFnP
na	na	k7c4
strastiplnou	strastiplný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
zpět	zpět	k6eAd1
za	za	k7c4
Helléspont	Helléspont	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
bitvě	bitva	k1gFnSc6
oblehli	oblehnout	k5eAaPmAgMnP
Řekové	Řek	k1gMnPc1
Théby	Théby	k1gFnPc4
a	a	k8xC
po	po	k7c6
měsíci	měsíc	k1gInSc6
bojů	boj	k1gInPc2
město	město	k1gNnSc4
dobyli	dobýt	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Properští	Properský	k2eAgMnPc1d1
vůdcové	vůdce	k1gMnPc1
města	město	k1gNnSc2
byli	být	k5eAaImAgMnP
poté	poté	k6eAd1
odvedeni	odvést	k5eAaPmNgMnP
na	na	k7c4
Isthmos	Isthmos	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
dal	dát	k5eAaPmAgMnS
Pausaniás	Pausaniás	k1gInSc4
popravit	popravit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Údajně	údajně	k6eAd1
v	v	k7c4
tentýž	týž	k3xTgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
u	u	k7c2
Platají	Platají	k1gFnSc2
<g/>
,	,	kIx,
svedlo	svést	k5eAaPmAgNnS
110	#num#	k4
řeckých	řecký	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
spartského	spartský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Leótychida	Leótychid	k1gMnSc2
vítěznou	vítězný	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
u	u	k7c2
Mykalé	Mykalý	k2eAgFnSc2d1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
rozdrceno	rozdrtit	k5eAaPmNgNnS
300	#num#	k4
perských	perský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
pak	pak	k6eAd1
pluli	plout	k5eAaImAgMnP
k	k	k7c3
Helléspontu	Helléspont	k1gInSc3
s	s	k7c7
cílem	cíl	k1gInSc7
zničit	zničit	k5eAaPmF
most	most	k1gInSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
dorazili	dorazit	k5eAaPmAgMnP
na	na	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
však	však	k9
nalezli	nalézt	k5eAaBmAgMnP,k5eAaPmAgMnP
most	most	k1gInSc4
v	v	k7c6
troskách	troska	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Kykladech	Kyklad	k1gInPc6
<g/>
,	,	kIx,
povzbuzení	povzbuzení	k1gNnSc6
Xerxovým	Xerxův	k2eAgInSc7d1
nezdarem	nezdar	k1gInSc7
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
opět	opět	k6eAd1
povstali	povstat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
požádali	požádat	k5eAaPmAgMnP
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
Sparťané	Sparťan	k1gMnPc1
jim	on	k3xPp3gMnPc3
navrhli	navrhnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přesídlili	přesídlit	k5eAaPmAgMnP
do	do	k7c2
severního	severní	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Iónové	Ión	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
478	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
20	#num#	k4
peloponéských	peloponéský	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
a	a	k8xC
30	#num#	k4
athénských	athénský	k2eAgFnPc2d1
vypravila	vypravit	k5eAaPmAgFnS
pod	pod	k7c7
velením	velení	k1gNnSc7
Pausania	Pausanium	k1gNnSc2
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
sice	sice	k8xC
osvobodili	osvobodit	k5eAaPmAgMnP
své	svůj	k3xOyFgMnPc4
zdejší	zdejší	k2eAgMnPc4d1
krajany	krajan	k1gMnPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
fénická	fénický	k2eAgNnPc1d1
města	město	k1gNnPc1
se	se	k3xPyFc4
jim	on	k3xPp3gFnPc3
ubránila	ubránit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kypr	Kypr	k1gInSc1
tak	tak	k6eAd1
i	i	k9
nadále	nadále	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
základnou	základna	k1gFnSc7
perského	perský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecká	řecký	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
plavila	plavit	k5eAaImAgFnS
k	k	k7c3
Bosporu	Bospor	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
oblehla	oblehnout	k5eAaPmAgFnS
Byzantion	Byzantion	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
krátce	krátce	k6eAd1
předtím	předtím	k6eAd1
athénský	athénský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
Xanthippos	Xanthippos	k1gMnSc1
zmocnil	zmocnit	k5eAaPmAgMnS
Séstu	Sésta	k1gFnSc4
při	při	k7c6
Helléspontu	Helléspont	k1gInSc6
<g/>
,	,	kIx,
ovládnutím	ovládnutí	k1gNnSc7
Byzantia	Byzantion	k1gNnSc2
si	se	k3xPyFc3
Řekové	Řek	k1gMnPc1
mohli	moct	k5eAaImAgMnP
zajistit	zajistit	k5eAaPmF
spojení	spojení	k1gNnSc4
s	s	k7c7
poleis	poleis	k1gFnSc7
na	na	k7c4
pobřeží	pobřeží	k1gNnSc4
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Byzantia	Byzantion	k1gNnSc2
bylo	být	k5eAaImAgNnS
zajato	zajmout	k5eAaPmNgNnS
mnoho	mnoho	k4c1
vysoce	vysoce	k6eAd1
postavených	postavený	k2eAgMnPc2d1
perských	perský	k2eAgMnPc2d1
šlechticů	šlechtic	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Pausania	Pausanium	k1gNnPc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
pocházel	pocházet	k5eAaImAgInS
z	z	k7c2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Ágidovců	Ágidovec	k1gMnPc2
<g/>
,	,	kIx,
udělali	udělat	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
silný	silný	k2eAgInSc4d1
dojem	dojem	k1gInSc4
svými	svůj	k3xOyFgInPc7
mravy	mrav	k1gInPc7
a	a	k8xC
způsobem	způsob	k1gInSc7
odívání	odívání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
záhy	záhy	k6eAd1
převzal	převzít	k5eAaPmAgMnS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	své	k1gNnSc7
orientálním	orientální	k2eAgNnSc7d1
vystupováním	vystupování	k1gNnSc7
však	však	k8xC
pohoršoval	pohoršovat	k5eAaImAgMnS
jak	jak	k6eAd1
Ióny	Ión	k1gMnPc4
tak	tak	k8xC,k8xS
Peloponésany	Peloponésan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc1
autorita	autorita	k1gFnSc1
navíc	navíc	k6eAd1
utrpěla	utrpět	k5eAaPmAgFnS
šířícími	šířící	k2eAgFnPc7d1
se	se	k3xPyFc4
zvěstmi	zvěst	k1gFnPc7
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
tajně	tajně	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
perským	perský	k2eAgMnSc7d1
satrapou	satrapa	k1gMnSc7
Frýgie	Frýgie	k1gFnSc2
Artabazem	Artabazma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pausaniás	Pausaniás	k1gInSc1
byl	být	k5eAaImAgInS
tudíž	tudíž	k8xC
odvolán	odvolat	k5eAaPmNgMnS
do	do	k7c2
Sparty	Sparta	k1gFnSc2
a	a	k8xC
velení	velení	k1gNnSc2
nad	nad	k7c7
spojeneckým	spojenecký	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
převzal	převzít	k5eAaPmAgInS
Aristeidés	Aristeidés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
477	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyslali	vyslat	k5eAaPmAgMnP
Sparťané	Sparťan	k1gMnPc1
do	do	k7c2
Byzantia	Byzantion	k1gNnSc2
jiného	jiný	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
<g/>
,	,	kIx,
doprovázeného	doprovázený	k2eAgInSc2d1
menším	malý	k2eAgInSc7d2
oddílem	oddíl	k1gInSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Iónové	Ión	k1gMnPc1
dosud	dosud	k6eAd1
si	se	k3xPyFc3
pamatující	pamatující	k2eAgFnSc4d1
Pausaniovo	Pausaniův	k2eAgNnSc4d1
autokratické	autokratický	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
<g/>
,	,	kIx,
ho	on	k3xPp3gNnSc4
požádali	požádat	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
po	po	k7c6
vypuzení	vypuzení	k1gNnSc6
Peršanů	Peršan	k1gMnPc2
z	z	k7c2
Řecka	Řecko	k1gNnSc2
ztratili	ztratit	k5eAaPmAgMnP
Sparta	Sparta	k1gFnSc1
a	a	k8xC
ostatní	ostatní	k2eAgInPc4d1
Peloponésané	Peloponésaný	k2eAgInPc4d1
zájem	zájem	k1gInSc4
na	na	k7c6
pokračování	pokračování	k1gNnSc6
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
ještě	ještě	k6eAd1
utvrdila	utvrdit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklé	vzniklý	k2eAgNnSc4d1
mocenské	mocenský	k2eAgNnSc4d1
vakuum	vakuum	k1gNnSc4
brzy	brzy	k6eAd1
vyplnily	vyplnit	k5eAaPmAgFnP
Athény	Athéna	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
založily	založit	k5eAaPmAgFnP
svoji	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
námořní	námořní	k2eAgFnSc4d1
alianci	aliance	k1gFnSc4
<g/>
,	,	kIx,
známou	známá	k1gFnSc4
jako	jako	k8xS,k8xC
Délský	Délský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
průběh	průběh	k1gInSc1
válek	válka	k1gFnPc2
</s>
<s>
Vznik	vznik	k1gInSc1
délského	délský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
</s>
<s>
Athéňané	Athéňan	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
431	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Aristeidés	Aristeidés	k1gInSc1
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Athéňanů	Athéňan	k1gMnPc2
<g/>
,	,	kIx,
zapůsobil	zapůsobit	k5eAaPmAgMnS
svým	svůj	k3xOyFgNnSc7
jednáním	jednání	k1gNnSc7
a	a	k8xC
povahou	povaha	k1gFnSc7
na	na	k7c4
maloasijské	maloasijský	k2eAgMnPc4d1
Řeky	Řek	k1gMnPc4
velice	velice	k6eAd1
příznivě	příznivě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Athéňané	Athéňan	k1gMnPc1
byli	být	k5eAaImAgMnP
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
značná	značný	k2eAgFnSc1d1
část	část	k1gFnSc1
asijských	asijský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
iónského	iónský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
požívali	požívat	k5eAaImAgMnP
u	u	k7c2
svých	svůj	k3xOyFgMnPc2
spolubojovníků	spolubojovník	k1gMnPc2
více	hodně	k6eAd2
důvěry	důvěra	k1gFnSc2
než	než	k8xS
dórští	dórský	k2eAgMnPc1d1
Sparťané	Sparťan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
477	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgMnS
na	na	k7c4
ostrov	ostrov	k1gInSc4
Délos	Délos	k1gInSc1
svolán	svolán	k2eAgInSc1d1
kongres	kongres	k1gInSc1
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
ležících	ležící	k2eAgInPc2d1
na	na	k7c6
ostrovech	ostrov	k1gInPc6
či	či	k8xC
pobřeží	pobřeží	k1gNnSc6
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgNnSc6
posvátném	posvátný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
bylo	být	k5eAaImAgNnS
centrem	centr	k1gInSc7
Apollónova	Apollónův	k2eAgInSc2d1
kultu	kult	k1gInSc2
<g/>
,	,	kIx,
vytvořily	vytvořit	k5eAaPmAgInP
zmíněné	zmíněný	k2eAgInPc1d1
řecké	řecký	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
tzv.	tzv.	kA
délský	délský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vůdčí	vůdčí	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
alianci	aliance	k1gFnSc6
náleželo	náležet	k5eAaImAgNnS
Athéňanům	Athéňan	k1gMnPc3
díky	díky	k7c3
velikosti	velikost	k1gFnSc3
jejich	jejich	k3xOp3gNnSc2
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
spolku	spolek	k1gInSc2
se	se	k3xPyFc4
zavázali	zavázat	k5eAaPmAgMnP
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
válce	válka	k1gFnSc6
s	s	k7c7
Peršany	Peršan	k1gMnPc7
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
měli	mít	k5eAaImAgMnP
přispívat	přispívat	k5eAaImF
buď	buď	k8xC
poskytnutím	poskytnutí	k1gNnSc7
svých	svůj	k3xOyFgFnPc2
námořních	námořní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
anebo	anebo	k8xC
placením	placení	k1gNnSc7
dávek	dávka	k1gFnPc2
do	do	k7c2
společné	společný	k2eAgFnSc2d1
pokladnice	pokladnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazná	výrazný	k2eAgFnSc1d1
zásluha	zásluha	k1gFnSc1
na	na	k7c4
založení	založení	k1gNnSc4
a	a	k8xC
organizaci	organizace	k1gFnSc4
spolku	spolek	k1gInSc2
příslušela	příslušet	k5eAaImAgFnS
Aristeidovi	Aristeid	k1gMnSc3
a	a	k8xC
Themistoklovi	Themistokl	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
však	však	k9
Themistoklés	Themistoklés	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
důvěru	důvěra	k1gFnSc4
athénského	athénský	k2eAgInSc2d1
lidu	lid	k1gInSc2
a	a	k8xC
kvůli	kvůli	k7c3
podezření	podezření	k1gNnSc3
ze	z	k7c2
zapojení	zapojení	k1gNnSc2
do	do	k7c2
Pausaniova	Pausaniův	k2eAgNnSc2d1
spiknutí	spiknutí	k1gNnSc2
musel	muset	k5eAaImAgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
v	v	k7c6
Persii	Persie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Aristeidově	Aristeidův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
převzal	převzít	k5eAaPmAgInS
vedení	vedení	k1gNnSc4
aristokratické	aristokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Miltiadův	Miltiadův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Kimón	Kimón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
476	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zahájil	zahájit	k5eAaPmAgInS
tažení	tažení	k1gNnSc4
proti	proti	k7c3
městu	město	k1gNnSc3
Eion	Eiona	k1gFnPc2
při	při	k7c6
ústí	ústí	k1gNnSc6
Strýmónu	Strýmón	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
dosud	dosud	k6eAd1
střežila	střežit	k5eAaImAgFnS
perská	perský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
Kimón	Kimón	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
odklonit	odklonit	k5eAaPmF
tok	tok	k1gInSc4
řeky	řeka	k1gFnSc2
a	a	k8xC
hradby	hradba	k1gFnSc2
města	město	k1gNnSc2
se	se	k3xPyFc4
sesuly	sesout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osvobození	osvobození	k1gNnSc6
Eionu	Eion	k1gInSc2
se	se	k3xPyFc4
řada	řada	k1gFnSc1
řeckých	řecký	k2eAgNnPc2d1
měst	město	k1gNnPc2
na	na	k7c6
thráckém	thrácký	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
připojila	připojit	k5eAaPmAgFnS
k	k	k7c3
délskému	délské	k1gNnSc3
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
465	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pak	pak	k6eAd1
Kimónovy	Kimónův	k2eAgFnPc1d1
triéry	triéra	k1gFnPc1
vypudily	vypudit	k5eAaPmAgFnP
poslední	poslední	k2eAgInPc4d1
zbývající	zbývající	k2eAgInPc4d1
Peršany	peršan	k1gInPc4
z	z	k7c2
Thrákie	Thrákie	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
skončila	skončit	k5eAaPmAgFnS
perská	perský	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Athéňané	Athéňan	k1gMnPc1
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Středomoří	středomoří	k1gNnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
466	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
shromáždil	shromáždit	k5eAaPmAgInS
Kimón	Kimón	k1gInSc4
200	#num#	k4
athénských	athénský	k2eAgMnPc2d1
a	a	k8xC
100	#num#	k4
spojeneckých	spojenecký	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
jihovýchod	jihovýchod	k1gInSc4
do	do	k7c2
Pamfýlie	Pamfýlie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc1
flotila	flotila	k1gFnSc1
následně	následně	k6eAd1
rozdrtila	rozdrtit	k5eAaPmAgFnS
perské	perský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
řeky	řeka	k1gFnSc2
Eurymedóntu	Eurymedónt	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
k	k	k7c3
délskému	délské	k1gNnSc3
spolku	spolek	k1gInSc2
připojena	připojit	k5eAaPmNgFnS
také	také	k6eAd1
Lýkie	Lýkie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
u	u	k7c2
Eurymedóntu	Eurymedónt	k1gInSc2
podnikli	podniknout	k5eAaPmAgMnP
Athéňané	Athéňan	k1gMnPc1
novou	nový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
povstal	povstat	k5eAaPmAgInS
proti	proti	k7c3
perské	perský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
Egypt	Egypt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
ochotně	ochotně	k6eAd1
vyhověli	vyhovět	k5eAaPmAgMnP
egyptské	egyptský	k2eAgFnSc3d1
prosbě	prosba	k1gFnSc3
o	o	k7c4
pomoc	pomoc	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
460	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
poslali	poslat	k5eAaPmAgMnP
povstalcům	povstalec	k1gMnPc3
200	#num#	k4
triér	triéra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
předtím	předtím	k6eAd1
operovaly	operovat	k5eAaImAgFnP
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
seskupili	seskupit	k5eAaPmAgMnP
velmi	velmi	k6eAd1
početné	početný	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
(	(	kIx(
<g/>
Ktesiás	Ktesiás	k1gInSc1
a	a	k8xC
Diodóros	Diodórosa	k1gFnPc2
Sicilský	sicilský	k2eAgInSc1d1
hovoří	hovořit	k5eAaImIp3nS
až	až	k9
o	o	k7c4
400	#num#	k4
000	#num#	k4
mužích	muž	k1gMnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
mělo	mít	k5eAaImAgNnS
zadusit	zadusit	k5eAaPmF
egyptskou	egyptský	k2eAgFnSc4d1
vzpouru	vzpoura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
se	se	k3xPyFc4
prý	prý	k9
odehrála	odehrát	k5eAaPmAgFnS
na	na	k7c6
západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Nilu	Nil	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
rozhodující	rozhodující	k2eAgInSc4d1
podíl	podíl	k1gInSc4
na	na	k7c6
perské	perský	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
náležel	náležet	k5eAaImAgMnS
athénské	athénský	k2eAgFnSc3d1
falanze	falanga	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perská	perský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nato	nato	k6eAd1
unikla	uniknout	k5eAaPmAgFnS
do	do	k7c2
Memfidy	Memfida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
odsud	odsud	k6eAd1
se	se	k3xPyFc4
pak	pak	k6eAd1
strhlo	strhnout	k5eAaPmAgNnS
námořní	námořní	k2eAgNnSc1d1
střetnutí	střetnutí	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
40	#num#	k4
athénských	athénský	k2eAgMnPc2d1
a	a	k8xC
15	#num#	k4
samských	samský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
potopilo	potopit	k5eAaPmAgNnS
30	#num#	k4
perských	perský	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
20	#num#	k4
zajalo	zajmout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
léty	léto	k1gNnPc7
459	#num#	k4
až	až	k8xS
456	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pokračovali	pokračovat	k5eAaImAgMnP
Athéňané	Athéňan	k1gMnPc1
a	a	k8xC
Egypťané	Egypťan	k1gMnPc1
v	v	k7c6
obléhání	obléhání	k1gNnSc6
Peršanů	peršan	k1gInPc2
v	v	k7c6
Memfidě	Memfida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
athénského	athénský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
odvelena	odvelen	k2eAgFnSc1d1
do	do	k7c2
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
Peršané	Peršan	k1gMnPc1
sestavili	sestavit	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
300	#num#	k4
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
doprovázelo	doprovázet	k5eAaImAgNnS
značné	značný	k2eAgNnSc4d1
pozemní	pozemní	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poblíž	poblíž	k7c2
Memfidy	Memfida	k1gFnSc2
poté	poté	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nové	nový	k2eAgFnSc3d1
bitvě	bitva	k1gFnSc3
<g/>
,	,	kIx,
během	během	k7c2
níž	jenž	k3xRgFnSc2
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
athénský	athénský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obléhatelů	obléhatel	k1gMnPc2
se	se	k3xPyFc4
nyní	nyní	k6eAd1
stali	stát	k5eAaPmAgMnP
obležení	obležení	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
poražení	poražený	k2eAgMnPc1d1
Egypťané	Egypťan	k1gMnPc1
a	a	k8xC
6000	#num#	k4
Athéňanů	Athéňan	k1gMnPc2
bylo	být	k5eAaImAgNnS
Peršany	Peršan	k1gMnPc4
sevřeno	sevřen	k2eAgNnSc1d1
po	po	k7c4
dobu	doba	k1gFnSc4
osmnácti	osmnáct	k4xCc2
měsíců	měsíc	k1gInPc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Prosoptis	Prosoptis	k1gFnSc2
v	v	k7c6
deltě	delta	k1gFnSc6
Nilu	Nil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytrvalí	vytrvalý	k2eAgMnPc1d1
Peršané	Peršan	k1gMnPc1
se	se	k3xPyFc4
neodvážili	odvážit	k5eNaPmAgMnP
vylodit	vylodit	k5eAaPmF
na	na	k7c6
ostrově	ostrov	k1gInSc6
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
zahradili	zahradit	k5eAaPmAgMnP
tok	tok	k1gInSc4
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
překvapili	překvapit	k5eAaPmAgMnP
Egypťany	Egypťan	k1gMnPc4
pozemním	pozemní	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypťané	Egypťan	k1gMnPc1
se	se	k3xPyFc4
ihned	ihned	k6eAd1
vzdali	vzdát	k5eAaPmAgMnP
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
perský	perský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Megabyzos	Megabyzos	k1gMnSc1
vyzval	vyzvat	k5eAaPmAgMnS
Athéňany	Athéňan	k1gMnPc4
ke	k	k7c3
kapitulaci	kapitulace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některým	některý	k3yIgMnPc3
Athéňanům	Athéňan	k1gMnPc3
byl	být	k5eAaImAgInS
povolen	povolen	k2eAgInSc4d1
návrat	návrat	k1gInSc4
do	do	k7c2
vlasti	vlast	k1gFnSc2
přes	přes	k7c4
Kyrénu	Kyréna	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
velký	velký	k2eAgInSc1d1
počet	počet	k1gInSc1
upadl	upadnout	k5eAaPmAgInS
do	do	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loďstvo	loďstvo	k1gNnSc1
vypravené	vypravený	k2eAgFnSc2d1
z	z	k7c2
Athén	Athéna	k1gFnPc2
k	k	k7c3
vyproštění	vyproštění	k1gNnSc3
obleženého	obležený	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
bylo	být	k5eAaImAgNnS
překvapeno	překvapit	k5eAaPmNgNnS
a	a	k8xC
poraženo	porazit	k5eAaPmNgNnS
Peršany	peršan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
Kypr	Kypr	k1gInSc1
zpět	zpět	k6eAd1
do	do	k7c2
rukou	ruka	k1gFnPc2
perského	perský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Artaxerxa	Artaxerxum	k1gNnSc2
I.	I.	kA
Athéňané	Athéňan	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
v	v	k7c6
těchto	tento	k3xDgNnPc6
taženích	tažení	k1gNnPc6
údajně	údajně	k6eAd1
o	o	k7c4
20	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
450	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Kimón	Kimón	k1gInSc1
vypravil	vypravit	k5eAaPmAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
200	#num#	k4
triér	triéra	k1gFnPc2
na	na	k7c4
Kypr	Kypr	k1gInSc4
a	a	k8xC
do	do	k7c2
Kilíkie	Kilíkie	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
Peršané	Peršan	k1gMnPc1
poskytli	poskytnout	k5eAaPmAgMnP
podporu	podpora	k1gFnSc4
některým	některý	k3yIgFnPc3
iónským	iónský	k2eAgFnPc3d1
obcím	obec	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
usilovaly	usilovat	k5eAaImAgFnP
o	o	k7c6
odpadnutí	odpadnutí	k1gNnSc6
od	od	k7c2
délského	délský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Athéňanům	Athéňan	k1gMnPc3
se	se	k3xPyFc4
u	u	k7c2
Kypru	Kypr	k1gInSc2
postavilo	postavit	k5eAaPmAgNnS
300	#num#	k4
perských	perský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
Artabaza	Artabaza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
počátečních	počáteční	k2eAgInPc6d1
úspěších	úspěch	k1gInPc6
se	se	k3xPyFc4
Kimónovi	Kimón	k1gMnSc3
nepodařilo	podařit	k5eNaPmAgNnS
dobýt	dobýt	k5eAaPmF
kyperskou	kyperský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Kition	Kition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
obléhání	obléhání	k1gNnSc2
Kitia	Kitium	k1gNnSc2
Kimón	Kimón	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnPc2
či	či	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
smrtelné	smrtelný	k2eAgFnSc6d1
posteli	postel	k1gFnSc6
nařídil	nařídit	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
důstojníkům	důstojník	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
upustili	upustit	k5eAaPmAgMnP
od	od	k7c2
dobývání	dobývání	k1gNnSc2
Kitia	Kitium	k1gNnSc2
a	a	k8xC
stáhli	stáhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
ke	k	k7c3
kyperské	kyperský	k2eAgFnSc3d1
Salamíně	Salamína	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
smrt	smrt	k1gFnSc4
byla	být	k5eAaImAgFnS
pečlivě	pečlivě	k6eAd1
tajena	tajen	k2eAgFnSc1d1
před	před	k7c7
athénským	athénský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
a	a	k8xC
spojenci	spojenec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
o	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
zvítězili	zvítězit	k5eAaPmAgMnP
Athéňané	Athéňan	k1gMnPc1
nad	nad	k7c7
Peršany	peršan	k1gInPc7
v	v	k7c6
bojích	boj	k1gInPc6
odehrávajících	odehrávající	k2eAgFnPc2d1
se	se	k3xPyFc4
na	na	k7c6
moři	moře	k1gNnSc6
i	i	k8xC
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Thúkýdida	Thúkýdid	k1gMnSc2
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
bitvy	bitva	k1gFnPc1
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kimón	Kimón	k1gInSc1
tak	tak	k6eAd1
dokonce	dokonce	k9
i	i	k9
po	po	k7c6
své	svůj	k3xOyFgFnSc6
smrti	smrt	k1gFnSc6
dokázal	dokázat	k5eAaPmAgMnS
Peršanům	Peršan	k1gMnPc3
uštědřit	uštědřit	k5eAaPmF
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kalliův	Kalliův	k2eAgInSc1d1
mír	mír	k1gInSc1
</s>
<s>
Po	po	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
byli	být	k5eAaImAgMnP
délský	délský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
i	i	k9
Peršané	Peršan	k1gMnPc1
zcela	zcela	k6eAd1
vyčerpáni	vyčerpat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgFnSc1
z	z	k7c2
válčících	válčící	k2eAgFnPc2d1
stran	strana	k1gFnPc2
nebyla	být	k5eNaImAgFnS
schopna	schopen	k2eAgFnSc1d1
zajistit	zajistit	k5eAaPmF
si	se	k3xPyFc3
úplnou	úplný	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
východním	východní	k2eAgNnSc7d1
Středomořím	středomoří	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perský	perský	k2eAgInSc4d1
velkokrál	velkokrál	k1gInSc4
proto	proto	k8xC
vyslal	vyslat	k5eAaPmAgMnS
do	do	k7c2
Athén	Athéna	k1gFnPc2
své	svůj	k3xOyFgMnPc4
emisary	emisar	k1gMnPc4
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
příměří	příměří	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
byl	být	k5eAaImAgInS
Periklés	Periklés	k1gInSc1
příznivě	příznivě	k6eAd1
nakloněn	nakloněn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Diodóra	Diodór	k1gInSc2
poslal	poslat	k5eAaPmAgMnS
athénský	athénský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
na	na	k7c4
podzim	podzim	k1gInSc4
449	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bohatého	bohatý	k2eAgNnSc2d1
athénského	athénský	k2eAgNnSc2d1
politika	politikum	k1gNnSc2
Kallia	Kallium	k1gNnSc2
do	do	k7c2
perských	perský	k2eAgFnPc2d1
Sus	Sus	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
vyjednával	vyjednávat	k5eAaImAgMnS
o	o	k7c4
míru	míra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesná	přesný	k2eAgFnSc1d1
povaha	povaha	k1gFnSc1
ujednání	ujednání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
známým	známý	k2eAgInSc7d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Kalliův	Kalliův	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
nejasná	jasný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diodóros	Diodórosa	k1gFnPc2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c6
„	„	k?
<g/>
významnou	významný	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
avšak	avšak	k8xC
Thúkýdidés	Thúkýdidés	k1gInSc1
o	o	k7c6
ní	on	k3xPp3gFnSc6
nepodává	podávat	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicita	historicita	k1gFnSc1
Kalliova	Kalliův	k2eAgInSc2d1
míru	mír	k1gInSc2
je	být	k5eAaImIp3nS
přinejmenším	přinejmenším	k6eAd1
sporná	sporný	k2eAgFnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
všeřecký	všeřecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
by	by	kYmCp3nS
ztratil	ztratit	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
nebylo	být	k5eNaImAgNnS
v	v	k7c6
zájmu	zájem	k1gInSc6
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
slovy	slovo	k1gNnPc7
Thúkýdida	Thúkýdida	k1gFnSc1
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgFnP
jeho	jeho	k3xOp3gMnPc7
hegemonem	hegemon	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc1
obsažené	obsažený	k2eAgFnPc1d1
v	v	k7c6
míru	mír	k1gInSc6
byly	být	k5eAaImAgFnP
podle	podle	k7c2
Diodóra	Diodór	k1gMnSc2
následující	následující	k2eAgFnPc1d1
<g/>
:	:	kIx,
všechny	všechen	k3xTgFnPc4
řecké	řecký	k2eAgFnPc4d1
obce	obec	k1gFnPc4
v	v	k7c6
Asii	Asie	k1gFnSc6
obdrží	obdržet	k5eAaPmIp3nS
autonomii	autonomie	k1gFnSc4
<g/>
;	;	kIx,
maloasijští	maloasijský	k2eAgMnPc1d1
satrapové	satrap	k1gMnPc1
se	se	k3xPyFc4
smějí	smát	k5eAaImIp3nP
přiblížit	přiblížit	k5eAaPmF
k	k	k7c3
řeckým	řecký	k2eAgFnPc3d1
obcím	obec	k1gFnPc3
nanejvýš	nanejvýš	k6eAd1
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
tří	tři	k4xCgInPc2
dnů	den	k1gInPc2
chůze	chůze	k1gFnSc2
<g/>
;	;	kIx,
žádná	žádný	k3yNgFnSc1
perská	perský	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
se	se	k3xPyFc4
nesmí	smět	k5eNaImIp3nS
plavit	plavit	k5eAaImF
v	v	k7c6
Egejském	egejský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
nebo	nebo	k8xC
Propontidě	Propontida	k1gFnSc6
<g/>
;	;	kIx,
pokud	pokud	k8xS
velkokrál	velkokrál	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
velitelé	velitel	k1gMnPc1
budou	být	k5eAaImBp3nP
toto	tento	k3xDgNnSc4
dodržovat	dodržovat	k5eAaImF
<g/>
,	,	kIx,
zavazují	zavazovat	k5eAaImIp3nP
se	se	k3xPyFc4
Athéňané	Athéňan	k1gMnPc1
nevést	vést	k5eNaImF
válku	válka	k1gFnSc4
proti	proti	k7c3
perské	perský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
uzavření	uzavření	k1gNnSc6
míru	míra	k1gFnSc4
Athéňané	Athéňan	k1gMnPc1
odvolali	odvolat	k5eAaPmAgMnP
svých	svůj	k3xOyFgFnPc2
60	#num#	k4
triér	triéra	k1gFnPc2
z	z	k7c2
Egypta	Egypt	k1gInSc2
a	a	k8xC
své	svůj	k3xOyFgFnSc2
vojenské	vojenský	k2eAgFnSc2d1
síly	síla	k1gFnSc2
z	z	k7c2
Kypru	Kypr	k1gInSc2
(	(	kIx(
<g/>
zřejmě	zřejmě	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
součást	součást	k1gFnSc4
ujednání	ujednání	k1gNnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
to	ten	k3xDgNnSc1
nebylo	být	k5eNaImAgNnS
výslovně	výslovně	k6eAd1
uvedeno	uvést	k5eAaPmNgNnS
<g/>
)	)	kIx)
a	a	k8xC
přestali	přestat	k5eAaPmAgMnP
vést	vést	k5eAaImF
vojenské	vojenský	k2eAgFnSc2d1
operace	operace	k1gFnSc2
na	na	k7c6
těchto	tento	k3xDgNnPc6
územích	území	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
byl	být	k5eAaImAgInS
Kalliův	Kalliův	k2eAgInSc1d1
mír	mír	k1gInSc1
skutečně	skutečně	k6eAd1
uzavřen	uzavřít	k5eAaPmNgInS
<g/>
,	,	kIx,
období	období	k1gNnSc3
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
v	v	k7c6
polovině	polovina	k1gFnSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
definitivně	definitivně	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2
konflikty	konflikt	k1gInPc1
</s>
<s>
Peršané	Peršan	k1gMnPc1
a	a	k8xC
Řekové	Řek	k1gMnPc1
pokračovali	pokračovat	k5eAaImAgMnP
ve	v	k7c6
vzájemném	vzájemný	k2eAgNnSc6d1
zasahování	zasahování	k1gNnSc6
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
411	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Peršané	Peršan	k1gMnPc1
zapojili	zapojit	k5eAaPmAgMnP
do	do	k7c2
peloponéské	peloponéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
vytvořili	vytvořit	k5eAaPmAgMnP
oboustranný	oboustranný	k2eAgInSc4d1
obranný	obranný	k2eAgInSc4d1
pakt	pakt	k1gInSc4
se	s	k7c7
Spartou	Sparta	k1gFnSc7
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
finančně	finančně	k6eAd1
přispívali	přispívat	k5eAaImAgMnP
k	k	k7c3
vedení	vedení	k1gNnSc3
námořního	námořní	k2eAgInSc2d1
boje	boj	k1gInSc2
s	s	k7c7
Athénami	Athéna	k1gFnPc7
výměnou	výměna	k1gFnSc7
za	za	k7c4
kontrolu	kontrola	k1gFnSc4
Iónie	Iónie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
404	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Kýros	Kýros	k1gInSc1
mladší	mladý	k2eAgMnSc1d2
pokusil	pokusit	k5eAaPmAgMnS
zmocnit	zmocnit	k5eAaPmF
perského	perský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
a	a	k8xC
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
si	se	k3xPyFc3
opatřil	opatřit	k5eAaPmAgInS
13	#num#	k4
000	#num#	k4
žoldnéřů	žoldnéř	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
řeckého	řecký	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
Sparťanů	Sparťan	k1gMnPc2
bylo	být	k5eAaImAgNnS
asi	asi	k9
700	#num#	k4
až	až	k9
800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparťané	Sparťan	k1gMnPc1
prý	prý	k9
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
plní	plnit	k5eAaImIp3nS
podmínky	podmínka	k1gFnPc4
sjednané	sjednaný	k2eAgFnPc4d1
smlouvy	smlouva	k1gFnPc4
a	a	k8xC
neznali	znát	k5eNaImAgMnP,k5eAaImAgMnP
pravý	pravý	k2eAgInSc4d1
účel	účel	k1gInSc4
Kýrova	Kýrův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Kýrově	Kýrův	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kúnax	Kúnax	k1gInSc4
podnikli	podniknout	k5eAaPmAgMnP
přeživší	přeživší	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
legendární	legendární	k2eAgInSc4d1
„	„	k?
<g/>
pochod	pochod	k1gInSc4
deseti	deset	k4xCc2
tisíc	tisíc	k4xCgInPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Anabasis	Anabasis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artaxerxés	Artaxerxés	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgInS
využít	využít	k5eAaPmF
svého	svůj	k3xOyFgNnSc2
vítězství	vítězství	k1gNnSc2
nad	nad	k7c7
Kýrem	kýr	k1gInSc7
mladším	mladý	k2eAgInSc7d2
k	k	k7c3
obnovení	obnovení	k1gNnSc3
nadvlády	nadvláda	k1gFnSc2
nad	nad	k7c7
iónskými	iónský	k2eAgInPc7d1
městskými	městský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iónové	Ión	k1gMnPc1
se	se	k3xPyFc4
ale	ale	k9
odmítli	odmítnout	k5eAaPmAgMnP
vzdát	vzdát	k5eAaPmF
a	a	k8xC
vyzvali	vyzvat	k5eAaPmAgMnP
Spartu	Sparta	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gNnSc4
podpořila	podpořit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
v	v	k7c6
téže	tenže	k3xDgFnSc6,k3xTgFnSc6
době	doba	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dočasnému	dočasný	k2eAgNnSc3d1
sblížení	sblížení	k1gNnSc3
mezi	mezi	k7c7
perskou	perský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
a	a	k8xC
Athénami	Athéna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
následnému	následný	k2eAgNnSc3d1
vypuknutí	vypuknutí	k1gNnSc3
korintské	korintský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Řecku	Řecko	k1gNnSc6
byli	být	k5eAaImAgMnP
Sparťané	Sparťan	k1gMnPc1
nakonec	nakonec	k6eAd1
přinuceni	přinucen	k2eAgMnPc1d1
přenechat	přenechat	k5eAaPmF
maloasijské	maloasijský	k2eAgFnPc4d1
Řeky	řeka	k1gFnPc4
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
stvrzena	stvrdit	k5eAaPmNgFnS
v	v	k7c6
tzv.	tzv.	kA
královském	královský	k2eAgInSc6d1
míru	mír	k1gInSc6
(	(	kIx(
<g/>
případně	případně	k6eAd1
Antalkidův	Antalkidův	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dalších	další	k2eAgNnPc2d1
šedesát	šedesát	k4xCc4
let	léto	k1gNnPc2
se	se	k3xPyFc4
žádný	žádný	k3yNgInSc1
řecký	řecký	k2eAgInSc1d1
stát	stát	k1gInSc1
neodvážil	odvážit	k5eNaPmAgInS
střetnout	střetnout	k5eAaPmF
s	s	k7c7
Persií	Persie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
král	král	k1gMnSc1
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonský	makedonský	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
roce	rok	k1gInSc6
338	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vytvořil	vytvořit	k5eAaPmAgMnS
v	v	k7c6
Korintu	Korint	k1gInSc6
všeřecký	všeřecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
zdánlivě	zdánlivě	k6eAd1
podobný	podobný	k2eAgInSc1d1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
481	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
tomtéž	týž	k3xTgNnSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
vyhlásil	vyhlásit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
úmysl	úmysl	k1gInSc4
podmanit	podmanit	k5eAaPmF
si	se	k3xPyFc3
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
dříve	dříve	k6eAd2
než	než	k8xS
stačil	stačit	k5eAaBmAgMnS
svůj	svůj	k3xOyFgInSc4
záměr	záměr	k1gInSc4
uskutečnit	uskutečnit	k5eAaPmF
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Makedonský	makedonský	k2eAgMnSc1d1
vyrazil	vyrazit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
334	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
s	s	k7c7
38	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
vstříc	vstříc	k6eAd1
Asii	Asie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
pouhých	pouhý	k2eAgNnPc2d1
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
Alexandr	Alexandr	k1gMnSc1
vyvrátil	vyvrátit	k5eAaPmAgMnS
perskou	perský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
a	a	k8xC
ukončil	ukončit	k5eAaPmAgMnS
panování	panování	k1gNnSc4
dynastie	dynastie	k1gFnSc2
Achaimenovců	Achaimenovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působení	působení	k1gNnSc1
řecké	řecký	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
se	se	k3xPyFc4
díky	díky	k7c3
jeho	jeho	k3xOp3gInPc3
výbojům	výboj	k1gInPc3
rozšířilo	rozšířit	k5eAaPmAgNnS
až	až	k9
k	k	k7c3
břehům	břeh	k1gInPc3
řeky	řeka	k1gFnSc2
Indus	Indus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
prameny	pramen	k1gInPc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
dílo	dílo	k1gNnSc1
představuje	představovat	k5eAaImIp3nS
náš	náš	k3xOp1gInSc1
hlavní	hlavní	k2eAgInSc1d1
pramen	pramen	k1gInSc1
o	o	k7c6
tomto	tento	k3xDgInSc6
konfliktu	konflikt	k1gInSc6
(	(	kIx(
<g/>
busta	busta	k1gFnSc1
v	v	k7c6
Attalově	Attalův	k2eAgFnSc6d1
stoe	stoe	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
nám	my	k3xPp1nPc3
v	v	k7c6
současnosti	současnost	k1gFnSc6
o	o	k7c6
tomto	tento	k3xDgInSc6
konfliktu	konflikt	k1gInSc6
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
takřka	takřka	k6eAd1
výlučně	výlučně	k6eAd1
z	z	k7c2
per	pero	k1gNnPc2
starověkých	starověký	k2eAgMnPc2d1
řeckých	řecký	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějším	důležitý	k2eAgInSc7d3
pramenem	pramen	k1gInSc7
je	být	k5eAaImIp3nS
dílo	dílo	k1gNnSc1
Hérodota	Hérodot	k1gMnSc2
<g/>
,	,	kIx,
historika	historik	k1gMnSc2
původem	původ	k1gInSc7
z	z	k7c2
Halikarnássu	Halikarnáss	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
vyhnán	vyhnat	k5eAaPmNgMnS
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
rodného	rodný	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
procestoval	procestovat	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
středomořského	středomořský	k2eAgInSc2d1
světa	svět	k1gInSc2
od	od	k7c2
Skythie	Skythie	k1gFnSc2
až	až	k9
po	po	k7c4
Egypt	Egypt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
sběrem	sběr	k1gInSc7
informací	informace	k1gFnPc2
o	o	k7c6
řecko-perských	řecko-perský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
a	a	k8xC
jiných	jiný	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
pak	pak	k6eAd1
všechny	všechen	k3xTgMnPc4
sepsal	sepsat	k5eAaPmAgInS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
nazývaném	nazývaný	k2eAgInSc6d1
česky	česky	k6eAd1
Dějiny	dějiny	k1gFnPc1
(	(	kIx(
<g/>
Histories	Histories	k1gInSc1
apodeixis	apodeixis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
spis	spis	k1gInSc1
začíná	začínat	k5eAaImIp3nS
Kroisovým	Kroisův	k2eAgNnSc7d1
dobytím	dobytí	k1gNnSc7
Iónie	Iónie	k1gFnSc2
a	a	k8xC
končí	končit	k5eAaImIp3nS
pádem	pád	k1gInSc7
města	město	k1gNnSc2
Séstos	Séstosa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
této	tento	k3xDgFnSc6
práci	práce	k1gFnSc6
Hérodotos	Hérodotos	k1gMnSc1
reprodukoval	reprodukovat	k5eAaBmAgMnS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
mu	on	k3xPp3gMnSc3
sdělili	sdělit	k5eAaPmAgMnP
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
hostitele	hostitel	k1gMnSc4
a	a	k8xC
patroni	patron	k1gMnPc1
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
přitom	přitom	k6eAd1
takto	takto	k6eAd1
nabyté	nabytý	k2eAgInPc4d1
údaje	údaj	k1gInPc4
podroboval	podrobovat	k5eAaImAgMnS
bližšímu	blízký	k2eAgNnSc3d2
kritickému	kritický	k2eAgNnSc3d1
zkoumání	zkoumání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Dějinách	dějiny	k1gFnPc6
obsaženo	obsáhnout	k5eAaPmNgNnS
mnoho	mnoho	k4c1
pravdivých	pravdivý	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
často	často	k6eAd1
je	být	k5eAaImIp3nS
přítomné	přítomný	k2eAgNnSc1d1
také	také	k9
zveličování	zveličování	k1gNnSc1
skutečností	skutečnost	k1gFnPc2
a	a	k8xC
místy	místy	k6eAd1
politická	politický	k2eAgFnSc1d1
propaganda	propaganda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
antičtí	antický	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
pokládali	pokládat	k5eAaImAgMnP
jeho	jeho	k3xOp3gFnSc4
práci	práce	k1gFnSc4
za	za	k7c4
kvalitnější	kvalitní	k2eAgInPc4d2
než	než	k8xS
výtvory	výtvor	k1gInPc1
jeho	jeho	k3xOp3gMnPc2
předchůdců	předchůdce	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
Cicero	Cicero	k1gMnSc1
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
Hérodota	Hérodot	k1gMnSc4
„	„	k?
<g/>
otcem	otec	k1gMnSc7
historie	historie	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Athéňan	Athéňan	k1gMnSc1
Thúkydidés	Thúkydidés	k1gInSc4
měl	mít	k5eAaImAgMnS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
sestavit	sestavit	k5eAaPmF
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
navazovalo	navazovat	k5eAaImAgNnS
na	na	k7c4
Hérodotovo	Hérodotův	k2eAgNnSc4d1
a	a	k8xC
končilo	končit	k5eAaImAgNnS
peloponéskou	peloponéský	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
404	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Jeho	jeho	k3xOp3gNnSc7
sbírka	sbírka	k1gFnSc1
knih	kniha	k1gFnPc2
je	být	k5eAaImIp3nS
česky	česky	k6eAd1
pojmenována	pojmenován	k2eAgFnSc1d1
Dějiny	dějiny	k1gFnPc1
peloponnéské	peloponnéská	k1gFnSc2
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecně	všeobecně	k6eAd1
panuje	panovat	k5eAaImIp3nS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Thúkýdidés	Thúkýdidés	k1gInSc4
zemřel	zemřít	k5eAaPmAgMnS
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgMnS
schopen	schopen	k2eAgInSc4d1
svůj	svůj	k3xOyFgInSc4
spis	spis	k1gInSc4
dokončit	dokončit	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
jsou	být	k5eAaImIp3nP
v	v	k7c6
něm	on	k3xPp3gNnSc6
vylíčeny	vylíčen	k2eAgMnPc4d1
pouze	pouze	k6eAd1
události	událost	k1gFnPc4
prvních	první	k4xOgNnPc6
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
peloponéské	peloponéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
předchozím	předchozí	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
jen	jen	k9
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnosti	skutečnost	k1gFnPc4
důležité	důležitý	k2eAgFnPc4d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
jsou	být	k5eAaImIp3nP
shrnuty	shrnout	k5eAaPmNgInP
v	v	k7c6
první	první	k4xOgFnSc6
knize	kniha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
pozdějších	pozdní	k2eAgMnPc2d2
spisovatelů	spisovatel	k1gMnPc2
lze	lze	k6eAd1
zmínit	zmínit	k5eAaPmF
Efora	efor	k1gMnSc4
z	z	k7c2
Kýmé	Kýmé	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vytvořil	vytvořit	k5eAaPmAgInS
všeobecné	všeobecný	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
i	i	k9
události	událost	k1gFnPc4
spadající	spadající	k2eAgFnPc4d1
do	do	k7c2
období	období	k1gNnSc2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diodóros	Diodórosa	k1gFnPc2
Sicilský	sicilský	k2eAgMnSc1d1
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
knihu	kniha	k1gFnSc4
o	o	k7c6
dějinách	dějiny	k1gFnPc6
od	od	k7c2
počátku	počátek	k1gInSc2
věků	věk	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
zmiňuje	zmiňovat	k5eAaImIp3nS
rovněž	rovněž	k9
o	o	k7c6
konfliktu	konflikt	k1gInSc6
Řeků	Řek	k1gMnPc2
a	a	k8xC
Peršanů	Peršan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílo	dílo	k1gNnSc1
nejvíce	hodně	k6eAd3,k6eAd1
vycházející	vycházející	k2eAgMnSc1d1
z	z	k7c2
perských	perský	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
sepsal	sepsat	k5eAaPmAgMnS
Řek	Řek	k1gMnSc1
Ktesiás	Ktesiás	k1gInSc4
z	z	k7c2
Knidu	Knid	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
osobním	osobní	k2eAgMnSc7d1
lékařem	lékař	k1gMnSc7
krále	král	k1gMnSc2
Artaxerxa	Artaxerxum	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
se	se	k3xPyFc4
zaobírala	zaobírat	k5eAaImAgFnS
perskými	perský	k2eAgFnPc7d1
dějinami	dějiny	k1gFnPc7
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Ktesiás	Ktesiás	k1gInSc1
se	se	k3xPyFc4
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
vysmívá	vysmívat	k5eAaImIp3nS
Hérodotovi	Hérodot	k1gMnSc3
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
spis	spis	k1gInSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
přesnější	přesný	k2eAgFnSc4d2
<g/>
,	,	kIx,
neboť	neboť	k8xC
údaje	údaj	k1gInPc1
jím	on	k3xPp3gMnSc7
zaznamenané	zaznamenaný	k2eAgFnSc6d1
převzal	převzít	k5eAaPmAgMnS
od	od	k7c2
Peršanů	peršan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díla	dílo	k1gNnPc1
těchto	tento	k3xDgMnPc2
tří	tři	k4xCgMnPc2
autorů	autor	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
nedochovala	dochovat	k5eNaPmAgFnS
v	v	k7c6
celém	celý	k2eAgNnSc6d1
znění	znění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Pouze	pouze	k6eAd1
fragmenty	fragment	k1gInPc1
o	o	k7c6
řecko-perských	řecko-perský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgFnP
ve	v	k7c6
spise	spis	k1gInSc6
Myriobiblon	Myriobiblon	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
zhotovil	zhotovit	k5eAaPmAgMnS
Fotios	Fotios	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
konstantinopolským	konstantinopolský	k2eAgMnSc7d1
patriarchou	patriarcha	k1gMnSc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
v	v	k7c6
knize	kniha	k1gFnSc6
Eklogy	ekloga	k1gFnSc2
od	od	k7c2
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porfyrogenneta	Porfyrogennet	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
Sudě	suda	k1gFnSc6
<g/>
,	,	kIx,
byzantské	byzantský	k2eAgFnSc6d1
encyklopedii	encyklopedie	k1gFnSc6
z	z	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgMnPc3d1
historikům	historik	k1gMnPc3
tudíž	tudíž	k8xC
nezbývá	zbývat	k5eNaImIp3nS
než	než	k8xS
doplňovat	doplňovat	k5eAaImF
Hérodotova	Hérodotův	k2eAgNnPc4d1
a	a	k8xC
Thúkýdidova	Thúkýdidův	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
ze	z	k7c2
spisů	spis	k1gInPc2
pozdějších	pozdní	k2eAgMnPc2d2
analistů	analista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
primárně	primárně	k6eAd1
zaobírali	zaobírat	k5eAaImAgMnP
jinými	jiný	k2eAgFnPc7d1
skutečnostmi	skutečnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
taková	takový	k3xDgNnPc4
díla	dílo	k1gNnPc4
patří	patřit	k5eAaImIp3nP
Plútarchovy	Plútarchův	k2eAgInPc1d1
Životopisy	životopis	k1gInPc1
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
a	a	k8xC
cestopis	cestopis	k1gInSc1
jižního	jižní	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
sepsaný	sepsaný	k2eAgMnSc1d1
v	v	k7c6
tomtéž	týž	k3xTgNnSc6
století	století	k1gNnSc6
zeměpiscem	zeměpisec	k1gMnSc7
a	a	k8xC
cestovatelem	cestovatel	k1gMnSc7
Pausaniem	Pausanium	k1gNnSc7
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
nelze	lze	k6eNd1
zaměňovat	zaměňovat	k5eAaImF
se	s	k7c7
spartským	spartský	k2eAgMnSc7d1
vojevůdcem	vojevůdce	k1gMnSc7
téhož	týž	k3xTgNnSc2
jména	jméno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
někteří	některý	k3yIgMnPc1
římští	římský	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
se	se	k3xPyFc4
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
líčení	líčení	k1gNnSc4
průběhu	průběh	k1gInSc2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nim	on	k3xPp3gMnPc3
patří	patřit	k5eAaImIp3nS
Junianus	Junianus	k1gMnSc1
Justinus	Justinus	k1gMnSc1
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
Cornelius	Cornelius	k1gMnSc1
Nepos	Nepos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Greco-Persian	Greco-Persian	k1gInSc1
Wars	Wars	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Hérodotos	Hérodotos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VII	VII	kA
<g/>
,	,	kIx,
60	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
↑	↑	k?
Dupuy	Dupua	k1gFnSc2
<g/>
,	,	kIx,
R.	R.	kA
Ernest	Ernest	k1gMnSc1
<g/>
,	,	kIx,
Dupuy	Dupua	k1gFnPc1
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1
N.	N.	kA
Historie	historie	k1gFnSc1
vojenství	vojenství	k1gNnSc4
:	:	kIx,
Harperova	Harperův	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
3500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
do	do	k7c2
roku	rok	k1gInSc2
1700	#num#	k4
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
39	#num#	k4
<g/>
↑	↑	k?
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecký	řecký	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
244	#num#	k4
<g/>
↑	↑	k?
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecký	řecký	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
245	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
R.	R.	kA
Ernest	Ernest	k1gMnSc1
<g/>
,	,	kIx,
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1
N.	N.	kA
Historie	historie	k1gFnSc1
vojenství	vojenství	k1gNnSc4
:	:	kIx,
Harperova	Harperův	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
3500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
Kr.	Kr.	k1gMnSc1
do	do	k7c2
roku	rok	k1gInSc2
1700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Forma	forma	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7213-000-5	80-7213-000-5	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasické	klasický	k2eAgInPc1d1
Řecko	Řecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7341-404-X	80-7341-404-X	k4
</s>
<s>
HÉRODOTOS	Hérodotos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-200-1192-7	80-200-1192-7	k4
</s>
<s>
HOLLAND	HOLLAND	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perský	perský	k2eAgInSc1d1
oheň	oheň	k1gInSc1
:	:	kIx,
první	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
velmoc	velmoc	k1gFnSc1
a	a	k8xC
boj	boj	k1gInSc1
o	o	k7c4
Západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Dokořán	dokořán	k6eAd1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7363-094-2	978-80-7363-094-2	k4
</s>
<s>
KLÍMA	Klíma	k1gMnSc1
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sláva	Sláva	k1gFnSc1
a	a	k8xC
pád	pád	k1gInSc1
starého	starý	k2eAgInSc2d1
Íránu	Írán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
</s>
<s>
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolébka	kolébka	k1gFnSc1
demokracie	demokracie	k1gFnSc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
a	a	k8xC
kultura	kultura	k1gFnSc1
klasického	klasický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Arista	Arista	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86410-04-8	80-86410-04-8	k4
</s>
<s>
PLÚTARCHOS	PLÚTARCHOS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
I	I	kA
(	(	kIx(
<g/>
Themistoklés	Themistoklés	k1gInSc1
<g/>
,	,	kIx,
Aristeidés	Aristeidés	k1gInSc1
<g/>
,	,	kIx,
Kimón	Kimón	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Arista	Arista	k1gMnSc1
<g/>
,	,	kIx,
Baset	Baset	k1gMnSc1
<g/>
,	,	kIx,
Maitrea	Maitrea	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86410	#num#	k4
<g/>
-	-	kIx~
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-86410-47-0	978-80-86410-47-0	k4
</s>
<s>
THÚKÝDIDÉS	THÚKÝDIDÉS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
peloponnéské	peloponnéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecký	řecký	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gInSc1
–	–	k?
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
Erika	Erika	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-242-0403-7	80-242-0403-7	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Perská	perský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Sparta	Sparta	k1gFnSc1
</s>
<s>
Athény	Athéna	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Řecko-perské	řecko-perský	k2eAgInPc4d1
války	válek	k1gInPc4
(	(	kIx(
<g/>
stránky	stránka	k1gFnPc4
Antika	antika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Iran	Iran	k1gMnSc1
Chamber	Chamber	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Persian	Persian	k1gMnSc1
Wars	Wars	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
History	Histor	k1gInPc1
of	of	k?
THE	THE	kA
GRECO-PERSIAN	GRECO-PERSIAN	k1gFnSc2
WARS	WARS	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
History	Histor	k1gInPc1
of	of	k?
Ancient	Ancient	k1gInSc1
Athens	Athens	k1gInSc1
–	–	k?
The	The	k1gFnSc2
Persian	Persiana	k1gFnPc2
Wars	Wars	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Die	Die	k1gFnSc1
Perserkriege	Perserkrieg	k1gFnSc2
–	–	k?
Von	von	k1gInSc1
Salamis	Salamis	k1gFnSc1
bis	bis	k?
zum	zum	k?
Kallias-Frieden	Kallias-Frieden	k2eAgInSc4d1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ancient	Ancient	k1gMnSc1
Greece	Greece	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Persian	Persian	k1gMnSc1
Wars	Wars	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc1
Persian	Persian	k1gMnSc1
Wars	Wars	k1gInSc1
(	(	kIx(
<g/>
History	Histor	k1gInPc1
of	of	k?
Western	Western	kA
Civilization	Civilization	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dějiny	dějiny	k1gFnPc1
Řecka	Řecko	k1gNnSc2
Starověké	starověký	k2eAgFnSc6d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Egejská	egejský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Heladská	heladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Kykladská	kykladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Mínojská	Mínojský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Mykénská	mykénský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
Formování	formování	k1gNnSc2
řecké	řecký	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
</s>
<s>
Temné	temný	k2eAgNnSc1d1
období	období	k1gNnSc1
•	•	k?
Archaické	archaický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
•	•	k?
Achájové	Achájový	k2eAgFnSc2d1
•	•	k?
Aiólové	Aiólová	k1gFnSc2
•	•	k?
Dórové	Dór	k1gMnPc1
•	•	k?
Iónové	Ión	k1gMnPc1
Klasické	klasický	k2eAgFnSc2d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Polis	Polis	k1gInSc1
(	(	kIx(
<g/>
Athény	Athéna	k1gFnPc1
•	•	k?
Sparta	Sparta	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tyrannis	Tyrannis	k1gFnSc2
•	•	k?
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
•	•	k?
Peloponéská	peloponéský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
Helénistická	helénistický	k2eAgFnSc1d1
epocha	epocha	k1gFnSc1
</s>
<s>
Makedonské	makedonský	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
•	•	k?
Diadochové	diadoch	k1gMnPc1
(	(	kIx(
<g/>
Antigonovci	Antigonovec	k1gMnPc1
•	•	k?
Ptolemaiovci	Ptolemaiovec	k1gMnSc3
•	•	k?
Seleukovci	Seleukovec	k1gInSc6
<g/>
)	)	kIx)
•	•	k?
Války	válka	k1gFnSc2
diadochů	diadoch	k1gMnPc2
•	•	k?
Makedonské	makedonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
Římské	římský	k2eAgNnSc4d1
období	období	k1gNnSc4
</s>
<s>
Achaia	Achaia	k1gFnSc1
•	•	k?
Macedonia	Macedonium	k1gNnSc2
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Pozdní	pozdní	k2eAgFnSc1d1
antika	antika	k1gFnSc1
•	•	k?
Herakleiovci	Herakleiovec	k1gInSc6
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Amorejská	Amorejský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Makedonská	makedonský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Komnenovci	Komnenovec	k1gMnSc3
•	•	k?
Angelovci	Angelovec	k1gMnSc3
•	•	k?
Čtvrtá	čtvrtý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
nástupnické	nástupnický	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Nikájské	Nikájský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
•	•	k?
Trapezuntské	trapezuntský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
•	•	k?
Epirský	Epirský	k2eAgInSc4d1
despotát	despotát	k1gInSc4
<g/>
;	;	kIx,
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
•	•	k?
Soluňské	soluňský	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Athénské	athénský	k2eAgNnSc4d1
vévodství	vévodství	k1gNnSc4
•	•	k?
Achajské	Achajský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
<g/>
)	)	kIx)
•	•	k?
Palaiologové	Palaiolog	k1gMnPc1
•	•	k?
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
•	•	k?
Morejský	Morejský	k2eAgInSc1d1
despotát	despotát	k1gInSc1
Osmanská	osmanský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
</s>
<s>
Armatolové	Armatol	k1gMnPc1
•	•	k?
Kleftové	kleft	k1gMnPc1
•	•	k?
Kodžabašijové	Kodžabašijový	k2eAgNnSc1d1
•	•	k?
Fanarioté	Fanariotý	k2eAgFnSc2d1
•	•	k?
Válka	Válka	k1gMnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
Moderní	moderní	k2eAgInSc4d1
řecký	řecký	k2eAgInSc4d1
stát	stát	k1gInSc4
</s>
<s>
První	první	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Řecké	řecký	k2eAgNnSc4d1
království	království	k1gNnSc4
•	•	k?
Druhá	druhý	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Režim	režim	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
•	•	k?
Okupace	okupace	k1gFnSc2
(	(	kIx(
<g/>
ELAS	ELAS	kA
•	•	k?
EDES	EDES	kA
<g/>
)	)	kIx)
•	•	k?
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
•	•	k?
Třetí	třetí	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
Nová	nový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
•	•	k?
PASOK	PASOK	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Antika	antika	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
