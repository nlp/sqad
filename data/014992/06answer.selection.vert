<s>
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
série	série	k1gFnSc1
konfliktů	konflikt	k1gInPc2
mezi	mezi	k7c7
řeckými	řecký	k2eAgInPc7d1
městskými	městský	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
perskou	perský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
již	již	k6eAd1
začaly	začít	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
499	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
trvala	trvat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
449	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Jejich	jejich	k3xOp3gFnSc4
příčina	příčina	k1gFnSc1
tkvěla	tkvět	k5eAaImAgFnS
ve	v	k7c6
snaze	snaha	k1gFnSc6
perských	perský	k2eAgInPc2d1
velkokrálů	velkokrál	k1gInPc2
Dáreia	Dáreium	k1gNnSc2
I.	I.	kA
a	a	k8xC
obzvláště	obzvláště	k6eAd1
Xerxa	Xerxes	k1gMnSc4
I.	I.	kA
podmanit	podmanit	k5eAaPmF
si	se	k3xPyFc3
vojenskou	vojenský	k2eAgFnSc4d1
silou	síla	k1gFnSc7
starověké	starověký	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>