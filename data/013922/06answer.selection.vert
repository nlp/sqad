<s>
Knihovna	knihovna	k1gFnSc1
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Jáchymově	Jáchymov	k1gInSc6
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1541	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1624	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
založené	založený	k2eAgFnSc2d1
už	už	k9
roku	rok	k1gInSc2
1519	#num#	k4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
rektorem	rektor	k1gMnSc7
byl	být	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1532	#num#	k4
evangelický	evangelický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Mathesius	Mathesius	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
matriky	matrika	k1gFnSc2
a	a	k8xC
kronikář	kronikář	k1gMnSc1
<g/>
.	.	kIx.
</s>