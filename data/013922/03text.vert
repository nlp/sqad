<s>
Knihovna	knihovna	k1gFnSc1
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Knihovna	knihovna	k1gFnSc1
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Poloha	poloha	k1gFnSc1
</s>
<s>
Jáchymov	Jáchymov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Knihovna	knihovna	k1gFnSc1
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Jáchymově	Jáchymov	k1gInSc6
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1541	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1624	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
založené	založený	k2eAgFnSc2d1
už	už	k9
roku	rok	k1gInSc2
1519	#num#	k4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
rektorem	rektor	k1gMnSc7
byl	být	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1532	#num#	k4
evangelický	evangelický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Mathesius	Mathesius	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
matriky	matrika	k1gFnSc2
a	a	k8xC
kronikář	kronikář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Knihovna	knihovna	k1gFnSc1
od	od	k7c2
počátku	počátek	k1gInSc2
sloužila	sloužit	k5eAaImAgFnS
nejen	nejen	k6eAd1
žákům	žák	k1gMnPc3
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
lékařům	lékař	k1gMnPc3
<g/>
,	,	kIx,
horním	horní	k2eAgMnPc3d1
úředníkům	úředník	k1gMnPc3
a	a	k8xC
těžařům	těžař	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
byla	být	k5eAaImAgFnS
společně	společně	k6eAd1
se	s	k7c7
školou	škola	k1gFnSc7
uzavřena	uzavřen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1627	#num#	k4
jako	jako	k8xS,k8xC
hnízdo	hnízdo	k1gNnSc4
protestantismu	protestantismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc7
latinské	latinský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
Karlem	Karel	k1gMnSc7
Sieglem	Siegl	k1gMnSc7
na	na	k7c6
půdě	půda	k1gFnSc6
radnice	radnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
sbírka	sbírka	k1gFnSc1
232	#num#	k4
knih	kniha	k1gFnPc2
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
Jáchymova	Jáchymov	k1gInSc2
z	z	k7c2
různých	různý	k2eAgNnPc2d1
míst	místo	k1gNnPc2
Česka	Česko	k1gNnSc2
a	a	k8xC
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1976	#num#	k4
a	a	k8xC
1980	#num#	k4
prošla	projít	k5eAaPmAgFnS
restaurováním	restaurování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
knihovny	knihovna	k1gFnSc2
byla	být	k5eAaImAgFnS
vystavena	vystavit	k5eAaPmNgFnS
v	v	k7c6
muzeu	muzeum	k1gNnSc6
Královské	královský	k2eAgFnSc2d1
mincovny	mincovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
ledna	leden	k1gInSc2
2020	#num#	k4
vystavena	vystavit	k5eAaPmNgFnS
v	v	k7c6
renesančním	renesanční	k2eAgNnSc6d1
sklepení	sklepení	k1gNnSc6
Jáchymovské	jáchymovský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
knihovny	knihovna	k1gFnSc2
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
232	#num#	k4
knih	kniha	k1gFnPc2
obsahujících	obsahující	k2eAgFnPc2d1
358	#num#	k4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
52	#num#	k4
vytištěných	vytištěný	k2eAgFnPc2d1
do	do	k7c2
roku	rok	k1gInSc2
1500	#num#	k4
–	–	k?
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
evropské	evropský	k2eAgInPc4d1
prvotisky	prvotisk	k1gInPc4
(	(	kIx(
<g/>
nejstarší	starý	k2eAgFnPc4d3
tištěná	tištěný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jedné	jeden	k4xCgFnSc2
třetiny	třetina	k1gFnSc2
knihovnu	knihovna	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
teologické	teologický	k2eAgInPc1d1
spisy	spis	k1gInPc1
<g/>
,	,	kIx,
další	další	k2eAgFnSc1d1
třetina	třetina	k1gFnSc1
jsou	být	k5eAaImIp3nP
spisy	spis	k1gInPc1
antických	antický	k2eAgMnPc2d1
klasiků	klasik	k1gMnPc2
a	a	k8xC
zbytek	zbytek	k1gInSc4
slovníky	slovník	k1gInPc1
<g/>
,	,	kIx,
astronomická	astronomický	k2eAgNnPc1d1
a	a	k8xC
astrologická	astrologický	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
kalendáře	kalendář	k1gInPc1
a	a	k8xC
spisy	spis	k1gInPc1
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
nebo	nebo	k8xC
botanické	botanický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
knih	kniha	k1gFnPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
tiskařských	tiskařský	k2eAgFnPc2d1
dílen	dílna	k1gFnPc2
v	v	k7c6
Basileji	Basilej	k1gFnSc6
a	a	k8xC
Benátkách	Benátky	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najdeme	najít	k5eAaPmIp1nP
zde	zde	k6eAd1
například	například	k6eAd1
spis	spis	k1gInSc4
Sarepta	Sarept	k1gInSc2
od	od	k7c2
Mathesia	Mathesius	k1gMnSc2
<g/>
,	,	kIx,
epos	epos	k1gInSc4
Das	Das	k1gMnPc2
Joachimsthaler	Joachimsthaler	k1gInSc4
Bergbuchlein	Bergbuchleina	k1gFnPc2
od	od	k7c2
Hanse	Hans	k1gMnSc2
Rutharta	Ruthart	k1gMnSc2
a	a	k8xC
Ptolemaiovu	Ptolemaiův	k2eAgFnSc4d1
Kosmografii	kosmografie	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1486	#num#	k4
s	s	k7c7
32	#num#	k4
barevnými	barevný	k2eAgFnPc7d1
mapami	mapa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
70	#num#	k4
takzvaných	takzvaný	k2eAgNnPc2d1
libri	libri	k1gNnPc2
catenati	catenat	k5eAaPmF,k5eAaImF
(	(	kIx(
<g/>
knih	kniha	k1gFnPc2
s	s	k7c7
řetězem	řetěz	k1gInSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
unikátní	unikátní	k2eAgMnSc1d1
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
takovou	takový	k3xDgFnSc4
sbírku	sbírka	k1gFnSc4
můžeme	moct	k5eAaImIp1nP
srovnat	srovnat	k5eAaPmF
jen	jen	k9
se	s	k7c7
sbírkami	sbírka	k1gFnPc7
v	v	k7c6
Nizozemí	Nizozemí	k1gNnSc6
a	a	k8xC
Velké	velký	k2eAgFnSc3d1
Británii	Británie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
kniha	kniha	k1gFnSc1
sbírky	sbírka	k1gFnSc2
je	být	k5eAaImIp3nS
Starý	starý	k2eAgInSc1d1
zákon	zákon	k1gInSc1
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
kniha	kniha	k1gFnSc1
je	být	k5eAaImIp3nS
renesanční	renesanční	k2eAgInSc1d1
zpěvník	zpěvník	k1gInSc1
a	a	k8xC
nejmladší	mladý	k2eAgFnSc7d3
knihou	kniha	k1gFnSc7
je	být	k5eAaImIp3nS
Cosmographia	Cosmographia	k1gFnSc1
od	od	k7c2
Sebastiana	Sebastian	k1gMnSc2
Münstera	Münster	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1629	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jáchymov	Jáchymov	k1gInSc1
vystavuje	vystavovat	k5eAaImIp3nS
unikátní	unikátní	k2eAgInPc4d1
staré	starý	k2eAgInPc4d1
tisky	tisk	k1gInPc4
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
uvidí	uvidět	k5eAaPmIp3nP
i	i	k9
knihy	kniha	k1gFnPc1
s	s	k7c7
řetězem	řetěz	k1gInSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Latine-school	Latine-school	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Informační	informační	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
knihovnictví	knihovnictví	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
</s>
