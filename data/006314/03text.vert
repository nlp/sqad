<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
1952	[number]	k4	1952
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c4	na
40	[number]	k4	40
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
MOV	MOV	kA	MOV
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1947	[number]	k4	1947
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Československo	Československo	k1gNnSc4	Československo
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1952	[number]	k4	1952
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1952	[number]	k4	1952
na	na	k7c4	na
www.olimpic.org	www.olimpic.org	k1gInSc4	www.olimpic.org
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1952	[number]	k4	1952
na	na	k7c4	na
www	www	k?	www
<g/>
.	.	kIx.	.
<g/>
marcolympics	marcolympics	k6eAd1	marcolympics
oficiální	oficiální	k2eAgFnSc1d1	oficiální
zpráva	zpráva	k1gFnSc1	zpráva
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
</s>
