<p>
<s>
Čo	Čo	k?	Čo
Oju	Oju	k1gFnSc1	Oju
(	(	kIx(	(
<g/>
nepálsky	nepálsky	k6eAd1	nepálsky
<g/>
:	:	kIx,	:
च	च	k?	च
<g/>
ो	ो	k?	ो
<g/>
य	य	k?	य
<g/>
ु	ु	k?	ु
–	–	k?	–
Čo	Čo	k1gMnSc1	Čo
Oju	Oju	k1gMnSc1	Oju
<g/>
;	;	kIx,	;
tibetsky	tibetsky	k6eAd1	tibetsky
<g/>
:	:	kIx,	:
<g/>
ཇ	ཇ	k?	ཇ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
བ	བ	k?	བ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
ད	ད	k?	ད
<g/>
ུ	ུ	k?	ུ
<g/>
་	་	k?	་
<g/>
ཡ	ཡ	k?	ཡ
–	–	k?	–
jo-bo-dbu-jag	joobuag	k1gInSc1	jo-bo-dbu-jag
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
卓	卓	k?	卓
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Zhuó	Zhuó	k1gMnSc1	Zhuó
<g/>
'	'	kIx"	'
<g/>
à	à	k?	à
Shā	Shā	k1gFnSc1	Shā
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestá	šestý	k4xOgFnSc1	šestý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nepálu	Nepál	k1gInSc2	Nepál
20	[number]	k4	20
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tibetštině	tibetština	k1gFnSc6	tibetština
Čo	Čo	k1gMnSc1	Čo
Oju	Oju	k1gMnSc1	Oju
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
bohyně	bohyně	k1gFnSc1	bohyně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Čo	Čo	k1gFnSc2	Čo
Oju	Oju	k1gFnSc2	Oju
je	být	k5eAaImIp3nS	být
ledovcem	ledovec	k1gInSc7	ledovec
pokryté	pokrytý	k2eAgNnSc4d1	pokryté
sedlo	sedlo	k1gNnSc4	sedlo
Nangpa	Nangp	k1gMnSc2	Nangp
La	la	k1gNnSc2	la
(	(	kIx(	(
<g/>
5716	[number]	k4	5716
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
obchodní	obchodní	k2eAgFnSc1d1	obchodní
cesta	cesta	k1gFnSc1	cesta
mezi	mezi	k7c4	mezi
Šerpy	šerpa	k1gFnPc4	šerpa
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
a	a	k8xC	a
z	z	k7c2	z
nepálské	nepálský	k2eAgFnSc2d1	nepálská
oblasti	oblast	k1gFnSc2	oblast
Khumbu	Khumb	k1gInSc2	Khumb
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
vysoko	vysoko	k6eAd1	vysoko
položenému	položený	k2eAgInSc3d1	položený
průsmyku	průsmyk	k1gInSc3	průsmyk
a	a	k8xC	a
mírným	mírný	k2eAgInPc3d1	mírný
sklonům	sklon	k1gInPc3	sklon
výstupových	výstupový	k2eAgInPc2d1	výstupový
hřebenů	hřeben	k1gInPc2	hřeben
je	být	k5eAaImIp3nS	být
Čo	Čo	k1gFnSc1	Čo
Oju	Oju	k1gMnPc2	Oju
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejsnadněji	snadno	k6eAd3	snadno
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc4	první
z	z	k7c2	z
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
"	"	kIx"	"
<g/>
malých	malý	k2eAgFnPc2d1	malá
osmitisícovek	osmitisícovka	k1gFnPc2	osmitisícovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
vrcholy	vrchol	k1gInPc1	vrchol
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
na	na	k7c6	na
pěti	pět	k4xCc6	pět
"	"	kIx"	"
<g/>
velkých	velký	k2eAgFnPc6d1	velká
osmitisícovkách	osmitisícovka	k1gFnPc6	osmitisícovka
<g/>
"	"	kIx"	"
teprve	teprve	k6eAd1	teprve
budují	budovat	k5eAaImIp3nP	budovat
poslední	poslední	k2eAgInPc4d1	poslední
postupové	postupový	k2eAgInPc4d1	postupový
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
prvních	první	k4xOgInPc2	první
pět	pět	k4xCc4	pět
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
výrazně	výrazně	k6eAd1	výrazně
náročnější	náročný	k2eAgMnSc1d2	náročnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Čo	Čo	k?	Čo
Oju	Oju	k1gFnSc1	Oju
byl	být	k5eAaImAgInS	být
pátý	pátý	k4xOgInSc4	pátý
osmitisícový	osmitisícový	k2eAgInSc4d1	osmitisícový
vrchol	vrchol	k1gInSc4	vrchol
zdolaný	zdolaný	k2eAgInSc4d1	zdolaný
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
prvního	první	k4xOgNnSc2	první
dosažení	dosažení	k1gNnSc2	dosažení
Annapurny	Annapurna	k1gFnSc2	Annapurna
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mount	Mount	k1gInSc1	Mount
Everestu	Everest	k1gInSc2	Everest
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nanga	Nanga	k1gFnSc1	Nanga
Parbat	Parbat	k1gFnSc1	Parbat
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
K2	K2	k1gFnSc1	K2
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
===	===	k?	===
</s>
</p>
<p>
<s>
Čo	Čo	k?	Čo
Oju	Oju	k1gFnSc1	Oju
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zdolána	zdolán	k2eAgFnSc1d1	zdolána
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1954	[number]	k4	1954
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
expedicí	expedice	k1gFnSc7	expedice
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Joseph	Joseph	k1gMnSc1	Joseph
Joechler	Joechler	k1gMnSc1	Joechler
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Tichy	ticho	k1gNnPc7	ticho
a	a	k8xC	a
Šerpa	šerpa	k1gFnSc1	šerpa
Pasang	Pasanga	k1gFnPc2	Pasanga
Dawa	Dawa	k1gMnSc1	Dawa
Lama	lama	k1gMnSc1	lama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výstupy	výstup	k1gInPc4	výstup
českých	český	k2eAgMnPc2d1	český
horolezců	horolezec	k1gMnPc2	horolezec
===	===	k?	===
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Věra	Věra	k1gFnSc1	Věra
Komárková	Komárková	k1gFnSc1	Komárková
(	(	kIx(	(
<g/>
občanka	občanka	k1gFnSc1	občanka
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovenka	Slovenka	k1gFnSc1	Slovenka
Dina	Dina	k1gFnSc1	Dina
Štěrbová	Štěrbová	k1gFnSc1	Štěrbová
(	(	kIx(	(
<g/>
občanka	občanka	k1gFnSc1	občanka
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
prvními	první	k4xOgMnPc7	první
ženami	žena	k1gFnPc7	žena
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Čo	Čo	k1gMnSc1	Čo
Oju	Oju	k1gMnSc1	Oju
<g/>
,	,	kIx,	,
Štěrbová	Štěrbová	k1gFnSc1	Štěrbová
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
na	na	k7c6	na
osmitisícovce	osmitisícovka	k1gFnSc6	osmitisícovka
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Jarýk	Jarýk	k1gMnSc1	Jarýk
Stejskal	Stejskal	k1gMnSc1	Stejskal
–	–	k?	–
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
s	s	k7c7	s
D.	D.	kA	D.
Becíkem	Becík	k1gInSc7	Becík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
–	–	k?	–
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hrubý	Hrubý	k1gMnSc1	Hrubý
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Šilhán	šilhán	k2eAgMnSc1d1	šilhán
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Drda	Drda	k1gMnSc1	Drda
<g/>
,	,	kIx,	,
Ota	Ota	k1gMnSc1	Ota
Louka	louka	k1gFnSc1	louka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Myšík	Myšík	k1gMnSc1	Myšík
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Srovnal	srovnat	k5eAaPmAgMnS	srovnat
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Luboš	Luboš	k1gMnSc1	Luboš
Bečák	Bečák	k1gMnSc1	Bečák
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Otta	Otta	k1gMnSc1	Otta
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Sláma	Sláma	k1gMnSc1	Sláma
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Pátek	Pátek	k1gMnSc1	Pátek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Smrž	Smrž	k1gMnSc1	Smrž
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Koukal	Koukal	k1gMnSc1	Koukal
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Novák	Novák	k1gMnSc1	Novák
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
sólo	sólo	k2eAgMnSc1d1	sólo
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Wlasák	Wlasák	k1gMnSc1	Wlasák
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Michut	Michut	k1gMnSc1	Michut
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
Korbová	korbový	k2eAgFnSc1d1	Korbová
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
Poláčková	Poláčková	k1gFnSc1	Poláčková
,	,	kIx,	,
<g/>
Daniel	Daniel	k1gMnSc1	Daniel
Vlach	Vlach	k1gMnSc1	Vlach
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Ivan	Ivan	k1gMnSc1	Ivan
Náhlík	Náhlík	k1gMnSc1	Náhlík
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Mašek	Mašek	k1gMnSc1	Mašek
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Leopold	Leopold	k1gMnSc1	Leopold
Sulovský	Sulovský	k1gMnSc1	Sulovský
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
Bortel	Bortel	k1gMnSc1	Bortel
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc1	Pavol
Lupták	Lupták	k1gInSc1	Lupták
<g/>
,	,	kIx,	,
Radovan	Radovan	k1gMnSc1	Radovan
Galoš	Galoš	k1gMnSc1	Galoš
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Hofmannová	Hofmannová	k1gFnSc1	Hofmannová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Milfait	Milfait	k1gMnSc1	Milfait
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Bělík	Bělík	k1gMnSc1	Bělík
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Matoušek	Matoušek	k1gMnSc1	Matoušek
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Marek	Marek	k1gMnSc1	Marek
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
TrávníčekVšechny	TrávníčekVšechna	k1gFnSc2	TrávníčekVšechna
československé	československý	k2eAgFnSc2d1	Československá
a	a	k8xC	a
české	český	k2eAgInPc4d1	český
výstupy	výstup	k1gInPc4	výstup
byly	být	k5eAaImAgFnP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cesty	cesta	k1gFnPc4	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Čo	Čo	k1gFnSc6	Čo
Oju	Oju	k1gFnPc2	Oju
vede	vést	k5eAaImIp3nS	vést
několik	několik	k4yIc1	několik
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
je	být	k5eAaImIp3nS	být
západní	západní	k2eAgFnSc1d1	západní
stěna	stěna	k1gFnSc1	stěna
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
se	se	k3xPyFc4	se
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
leze	lézt	k5eAaImIp3nS	lézt
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgNnSc7d1	klíčové
místem	místo	k1gNnSc7	místo
výstupu	výstup	k1gInSc2	výstup
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
stává	stávat	k5eAaImIp3nS	stávat
ledopád	ledopád	k1gInSc1	ledopád
mezi	mezi	k7c7	mezi
prvním	první	k4xOgInSc7	první
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
táborem	tábor	k1gInSc7	tábor
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
6700	[number]	k4	6700
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
počtu	počet	k1gInSc2	počet
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
výstupu	výstup	k1gInSc2	výstup
většinou	většinou	k6eAd1	většinou
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
fixními	fixní	k2eAgNnPc7d1	fixní
lany	lano	k1gNnPc7	lano
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
obtížný	obtížný	k2eAgInSc1d1	obtížný
úsek	úsek	k1gInSc1	úsek
je	být	k5eAaImIp3nS	být
skalní	skalní	k2eAgInSc1d1	skalní
výšvih	výšvih	k1gInSc1	výšvih
nad	nad	k7c7	nad
třetím	třetí	k4xOgInSc7	třetí
táborem	tábor	k1gInSc7	tábor
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
7600	[number]	k4	7600
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
je	být	k5eAaImIp3nS	být
lezen	lezen	k2eAgMnSc1d1	lezen
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
blízkosti	blízkost	k1gFnSc3	blízkost
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
za	za	k7c4	za
tmy	tma	k1gFnPc4	tma
a	a	k8xC	a
zastaví	zastavit	k5eAaPmIp3nS	zastavit
ve	v	k7c6	v
výstupu	výstup	k1gInSc6	výstup
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výškový	výškový	k2eAgInSc1d1	výškový
tábor	tábor	k1gInSc1	tábor
bývá	bývat	k5eAaImIp3nS	bývat
občas	občas	k6eAd1	občas
navštěvován	navštěvován	k2eAgInSc1d1	navštěvován
i	i	k8xC	i
vyznavači	vyznavač	k1gMnPc1	vyznavač
trekingu	treking	k1gInSc2	treking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
Západní	západní	k2eAgFnPc1d1	západní
úbočí	úboč	k1gFnSc7	úboč
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1	prvovýstup
na	na	k7c4	na
Čo	Čo	k1gMnSc4	Čo
Oju	Oju	k1gMnSc4	Oju
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
<s>
Tichy	ticho	k1gNnPc7	ticho
<g/>
,	,	kIx,	,
Jöchler	Jöchler	k1gInSc4	Jöchler
a	a	k8xC	a
Pasang	Pasang	k1gInSc4	Pasang
Dava-lama	Davaamum	k1gNnSc2	Dava-lamum
zlezli	zlézt	k5eAaPmAgMnP	zlézt
horu	hora	k1gFnSc4	hora
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
Švýcaři	Švýcar	k1gMnPc1	Švýcar
(	(	kIx(	(
<g/>
7400	[number]	k4	7400
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
taktéž	taktéž	k?	taktéž
západním	západní	k2eAgFnPc3d1	západní
úbočím	úboč	k1gFnPc3	úboč
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstupu	prvovýstup	k1gInSc3	prvovýstup
předcházel	předcházet	k5eAaImAgInS	předcházet
britský	britský	k2eAgInSc1d1	britský
průzkum	průzkum	k1gInSc1	průzkum
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
cestu	cesta	k1gFnSc4	cesta
poprvé	poprvé	k6eAd1	poprvé
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
Indové	Ind	k1gMnPc1	Ind
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Indy	Ind	k1gMnPc7	Ind
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
podruhé	podruhé	k6eAd1	podruhé
také	také	k9	také
Pasang	Pasang	k1gInSc1	Pasang
Dava-lama	Davaam	k1gMnSc2	Dava-lam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Čo	Čo	k1gFnSc6	Čo
Oju	Oju	k1gFnSc1	Oju
pokusila	pokusit	k5eAaPmAgFnS	pokusit
první	první	k4xOgFnSc1	první
ženská	ženský	k2eAgFnSc1d1	ženská
expedice	expedice	k1gFnSc1	expedice
na	na	k7c4	na
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Skončila	skončit	k5eAaPmAgFnS	skončit
ale	ale	k8xC	ale
tragicky	tragicky	k6eAd1	tragicky
smrtí	smrt	k1gFnPc2	smrt
dvou	dva	k4xCgFnPc2	dva
horolezkyň	horolezkyně	k1gFnPc2	horolezkyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
ženský	ženský	k2eAgInSc4d1	ženský
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
dvojici	dvojice	k1gFnSc4	dvojice
Dina	Din	k2eAgFnSc1d1	Dina
Štěrbová	Štěrbová	k1gFnSc1	Štěrbová
(	(	kIx(	(
<g/>
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Věra	Věra	k1gFnSc1	Věra
Komárková	Komárková	k1gFnSc1	Komárková
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
s	s	k7c7	s
Šerpou	šerpa	k1gFnSc7	šerpa
Ang	Ang	k1gFnSc7	Ang
Ritou	Rita	k1gFnSc7	Rita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1985	[number]	k4	1985
završili	završit	k5eAaPmAgMnP	završit
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
Jarýk	Jarýk	k1gMnSc1	Jarýk
Stejskal	Stejskal	k1gMnSc1	Stejskal
a	a	k8xC	a
Dušan	Dušan	k1gMnSc1	Dušan
Becík	Becík	k1gMnSc1	Becík
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
výpravy	výprava	k1gFnSc2	výprava
první	první	k4xOgInSc4	první
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
na	na	k7c4	na
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
(	(	kIx(	(
<g/>
předchozí	předchozí	k2eAgInPc1d1	předchozí
zimní	zimní	k2eAgInPc1d1	zimní
výstupy	výstup	k1gInPc1	výstup
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
vždy	vždy	k6eAd1	vždy
expedičním	expediční	k2eAgInSc7d1	expediční
způsobem	způsob	k1gInSc7	způsob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
výstup	výstup	k1gInSc4	výstup
druhým	druhý	k4xOgInSc7	druhý
zimním	zimní	k2eAgInSc7d1	zimní
výstupem	výstup	k1gInSc7	výstup
na	na	k7c4	na
Čo	Čo	k1gFnSc4	Čo
Oju	Oju	k1gFnSc2	Oju
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1988	[number]	k4	1988
Španěl	Španěly	k1gInPc2	Španěly
Fernando	Fernanda	k1gFnSc5	Fernanda
Garrido	Garrida	k1gFnSc5	Garrida
úspěšně	úspěšně	k6eAd1	úspěšně
zvládá	zvládat	k5eAaImIp3nS	zvládat
první	první	k4xOgInSc1	první
zimní	zimní	k2eAgInSc1d1	zimní
sólovýstup	sólovýstup	k1gInSc1	sólovýstup
na	na	k7c4	na
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
ženský	ženský	k2eAgInSc4d1	ženský
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
podniká	podnikat	k5eAaImIp3nS	podnikat
Švýcarka	Švýcarka	k1gFnSc1	Švýcarka
Marianne	Mariann	k1gInSc5	Mariann
Chapuisat	Chapuisat	k1gMnSc1	Chapuisat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
Jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Rakušan	Rakušan	k1gMnSc1	Rakušan
Koblmüller	Koblmüller	k1gMnSc1	Koblmüller
a	a	k8xC	a
Němec	Němec	k1gMnSc1	Němec
Furtner	Furtner	k1gMnSc1	Furtner
provedli	provést	k5eAaPmAgMnP	provést
bez	bez	k7c2	bez
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
povolení	povolení	k1gNnSc2	povolení
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
stěnou	stěna	k1gFnSc7	stěna
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc4	výstup
nebyl	být	k5eNaImAgMnS	být
zopakován	zopakován	k2eAgMnSc1d1	zopakován
<g/>
.1985	.1985	k4	.1985
–	–	k?	–
Jižní	jižní	k2eAgInSc4d1	jižní
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1985	[number]	k4	1985
završila	završit	k5eAaPmAgFnS	završit
polská	polský	k2eAgFnSc1d1	polská
expedice	expedice	k1gFnSc1	expedice
prvovýstup	prvovýstup	k1gInSc4	prvovýstup
jižním	jižní	k2eAgInSc7d1	jižní
pilířem	pilíř	k1gInSc7	pilíř
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
prvovýstup	prvovýstup	k1gInSc4	prvovýstup
na	na	k7c4	na
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
otevřený	otevřený	k2eAgInSc4d1	otevřený
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Maciej	Maciej	k1gInSc4	Maciej
Berbeka	Berbeko	k1gNnSc2	Berbeko
<g/>
,	,	kIx,	,
Maciej	Maciej	k1gInSc1	Maciej
Pawlikowski	Pawlikowsk	k1gFnSc2	Pawlikowsk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Zygmunt	Zygmunt	k1gMnSc1	Zygmunt
Heinrich	Heinrich	k1gMnSc1	Heinrich
a	a	k8xC	a
Jerzy	Jerza	k1gFnPc1	Jerza
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
<g/>
.1986	.1986	k4	.1986
–	–	k?	–
Jihozápadní	jihozápadní	k2eAgInSc4d1	jihozápadní
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
expedice	expedice	k1gFnSc1	expedice
otevírá	otevírat	k5eAaImIp3nS	otevírat
novou	nový	k2eAgFnSc4d1	nová
trasu	trasa	k1gFnSc4	trasa
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
cesty	cesta	k1gFnSc2	cesta
<g/>
.1988	.1988	k4	.1988
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
horolezců	horolezec	k1gMnPc2	horolezec
ze	z	k7c2	z
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
otevírá	otevírat	k5eAaImIp3nS	otevírat
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
dosud	dosud	k6eAd1	dosud
nezlezenou	zlezený	k2eNgFnSc7d1	zlezený
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
<g/>
.1990	.1990	k4	.1990
–	–	k?	–
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Erhard	Erhard	k1gMnSc1	Erhard
Loretan	Loretan	k1gInSc1	Loretan
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Troillet	Troillet	k1gInSc1	Troillet
a	a	k8xC	a
Polák	Polák	k1gMnSc1	Polák
Vojciech	Vojciech	k1gInSc4	Vojciech
Kurtyka	Kurtyek	k1gInSc2	Kurtyek
zlézají	zlézat	k5eAaImIp3nP	zlézat
horu	hora	k1gFnSc4	hora
novou	nový	k2eAgFnSc7d1	nová
cestou	cesta	k1gFnSc7	cesta
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
stěně	stěna	k1gFnSc6	stěna
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.1990	.1990	k4	.1990
–	–	k?	–
Japonec	Japonec	k1gMnSc1	Japonec
Yamanoi	Yamano	k1gFnSc3	Yamano
otevírá	otevírat	k5eAaImIp3nS	otevírat
druhou	druhý	k4xOgFnSc4	druhý
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
stěně	stěna	k1gFnSc6	stěna
sólo	sólo	k2eAgInSc7d1	sólo
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výstup	výstup	k1gInSc4	výstup
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
stejným	stejný	k2eAgInSc7d1	stejný
stylem	styl	k1gInSc7	styl
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
čtveřice	čtveřice	k1gFnSc1	čtveřice
Japonek	Japonka	k1gFnPc2	Japonka
<g/>
.2006	.2006	k4	.2006
–	–	k?	–
Slovinec	Slovinec	k1gMnSc1	Slovinec
Pavle	Pavla	k1gFnSc3	Pavla
Kozjek	Kozjka	k1gFnPc2	Kozjka
otevírá	otevírat	k5eAaImIp3nS	otevírat
třetí	třetí	k4xOgFnSc1	třetí
trasu	trasa	k1gFnSc4	trasa
jihozápadní	jihozápadní	k2eAgFnSc7d1	jihozápadní
stěnou	stěna	k1gFnSc7	stěna
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
<g/>
Celkem	celkem	k6eAd1	celkem
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
Čo	Čo	k1gMnPc2	Čo
Oju	Oju	k1gFnSc2	Oju
8	[number]	k4	8
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
polovina	polovina	k1gFnSc1	polovina
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
prvovýstupem	prvovýstup	k1gInSc7	prvovýstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
a	a	k8xC	a
další	další	k1gNnSc1	další
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
lezců	lezec	k1gMnPc2	lezec
však	však	k9	však
postupuje	postupovat	k5eAaImIp3nS	postupovat
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
českých	český	k2eAgInPc2d1	český
výstupů	výstup	k1gInPc2	výstup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
tras	trasa	k1gFnPc2	trasa
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
zopakováno	zopakován	k2eAgNnSc1d1	zopakováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dina	Din	k2eAgFnSc1d1	Dina
Štěrbová	Štěrbová	k1gFnSc1	Štěrbová
<g/>
:	:	kIx,	:
Čo	Čo	k1gFnSc1	Čo
Oju	Oju	k1gFnSc1	Oju
-	-	kIx~	-
tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
hora	hora	k1gFnSc1	hora
<g/>
;	;	kIx,	;
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
208	[number]	k4	208
<g/>
+	+	kIx~	+
<g/>
48	[number]	k4	48
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
41	[number]	k4	41
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gInSc4	Herbert
Tichy	ticho	k1gNnPc7	ticho
<g/>
:	:	kIx,	:
Čo	Čo	k1gFnSc1	Čo
oju	oju	k?	oju
<g/>
;	;	kIx,	;
Osveta	Osveta	k1gFnSc1	Osveta
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
155	[number]	k4	155
<g/>
+	+	kIx~	+
<g/>
32	[number]	k4	32
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Himálaj	Himálaj	k1gFnSc1	Himálaj
</s>
</p>
<p>
<s>
Osmitisícovky	Osmitisícovka	k1gFnPc1	Osmitisícovka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čo	Čo	k1gFnSc2	Čo
Oju	Oju	k1gFnSc2	Oju
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Čo	Čo	k1gMnSc2	Čo
Oju	Oju	k1gMnSc2	Oju
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
CHO	cho	k0	cho
OYU	OYU	kA	OYU
2009	[number]	k4	2009
</s>
</p>
