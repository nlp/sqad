<s>
Hromnice	hromnice	k1gFnSc1	hromnice
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
svátku	svátek	k1gInSc2	svátek
slaveného	slavený	k2eAgNnSc2d1	slavené
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
v	v	k7c4	v
křesťanství	křesťanství	k1gNnSc4	křesťanství
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
svátek	svátek	k1gInSc1	svátek
Uvedení	uvedení	k1gNnSc2	uvedení
Páně	páně	k2eAgFnSc1d1	páně
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
