<s>
Hromnice	hromnice	k1gFnSc1	hromnice
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
svátku	svátek	k1gInSc2	svátek
slaveného	slavený	k2eAgNnSc2d1	slavené
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
v	v	k7c4	v
křesťanství	křesťanství	k1gNnSc4	křesťanství
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
svátek	svátek	k1gInSc1	svátek
Uvedení	uvedení	k1gNnSc2	uvedení
Páně	páně	k2eAgFnSc1d1	páně
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
křesťanství	křesťanství	k1gNnSc6	křesťanství
se	se	k3xPyFc4	se
Hromnice	Hromnice	k1gFnPc1	Hromnice
staly	stát	k5eAaPmAgFnP	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
oslavou	oslava	k1gFnSc7	oslava
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
keltský	keltský	k2eAgInSc1d1	keltský
předkřesťanský	předkřesťanský	k2eAgInSc1d1	předkřesťanský
svátek	svátek	k1gInSc1	svátek
Imbolc	Imbolc	k1gFnSc1	Imbolc
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc1	svátek
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
ochranou	ochrana	k1gFnSc7	ochrana
před	před	k7c7	před
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
bouří	bouř	k1gFnSc7	bouř
a	a	k8xC	a
bleskem	blesk	k1gInSc7	blesk
jak	jak	k8xS	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
jeho	on	k3xPp3gInSc4	on
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
této	tento	k3xDgFnSc2	tento
víry	víra	k1gFnSc2	víra
je	být	k5eAaImIp3nS	být
svěcení	svěcení	k1gNnSc1	svěcení
svící	svíce	k1gFnPc2	svíce
<g/>
,	,	kIx,	,
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
hromniček	hromnička	k1gFnPc2	hromnička
<g/>
,	,	kIx,	,
prováděné	prováděný	k2eAgFnPc1d1	prováděná
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
během	během	k7c2	během
bouře	bouř	k1gFnSc2	bouř
dávaly	dávat	k5eAaImAgFnP	dávat
do	do	k7c2	do
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
tak	tak	k6eAd1	tak
ochránit	ochránit	k5eAaPmF	ochránit
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Hromnicím	Hromnice	k1gFnPc3	Hromnice
se	se	k3xPyFc4	se
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
váže	vázat	k5eAaImIp3nS	vázat
nejvíce	hodně	k6eAd3	hodně
pranostik	pranostika	k1gFnPc2	pranostika
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
pranostikou	pranostika	k1gFnSc7	pranostika
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Na	na	k7c4	na
Hromnice	Hromnice	k1gFnPc4	Hromnice
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
více	hodně	k6eAd2	hodně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Hromnice	Hromnice	k1gFnPc4	Hromnice
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
sklízel	sklízet	k5eAaImAgInS	sklízet
betlém	betlém	k1gInSc1	betlém
a	a	k8xC	a
někde	někde	k6eAd1	někde
také	také	k9	také
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromek	stromek	k1gInSc4	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svátek	Svátek	k1gMnSc1	Svátek
Uvedení	uvedení	k1gNnSc2	uvedení
Páně	páně	k2eAgFnSc2d1	páně
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
Hromnic	Hromnice	k1gFnPc2	Hromnice
v	v	k7c4	v
katolický	katolický	k2eAgInSc4d1	katolický
svátek	svátek	k1gInSc4	svátek
Uvedení	uvedení	k1gNnSc2	uvedení
Páně	páně	k2eAgNnSc2d1	páně
do	do	k7c2	do
chrámu	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
Očišťování	očišťování	k1gNnSc1	očišťování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
připomíná	připomínat	k5eAaImIp3nS	připomínat
událost	událost	k1gFnSc1	událost
z	z	k7c2	z
evangelií	evangelium	k1gNnPc2	evangelium
(	(	kIx(	(
<g/>
Lk	Lk	k1gFnSc1	Lk
2,22	[number]	k4	2,22
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
matka	matka	k1gFnSc1	matka
Maria	Maria	k1gFnSc1	Maria
přinesla	přinést	k5eAaPmAgFnS	přinést
podle	podle	k7c2	podle
židovského	židovský	k2eAgInSc2d1	židovský
obyčeje	obyčej	k1gInSc2	obyčej
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
40	[number]	k4	40
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
do	do	k7c2	do
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gNnSc4	on
zasvětila	zasvětit	k5eAaPmAgFnS	zasvětit
jako	jako	k9	jako
prvorozeného	prvorozený	k2eAgInSc2d1	prvorozený
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
prorokyní	prorokyně	k1gFnSc7	prorokyně
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
spravedlivým	spravedlivý	k2eAgInSc7d1	spravedlivý
Simeonem	Simeon	k1gInSc7	Simeon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Ježíše	Ježíš	k1gMnSc4	Ježíš
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
světlem	světlo	k1gNnSc7	světlo
k	k	k7c3	k
osvícení	osvícení	k1gNnSc3	osvícení
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
Simeonova	Simeonův	k2eAgNnSc2d1	Simeonovo
proroctví	proroctví	k1gNnSc2	proroctví
o	o	k7c4	o
Ježíši-světle	Ježíšivětle	k1gFnPc4	Ježíši-světle
pochází	pocházet	k5eAaImIp3nS	pocházet
zvyk	zvyk	k1gInSc1	zvyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
světily	světit	k5eAaImAgFnP	světit
svíčky	svíčka	k1gFnPc1	svíčka
"	"	kIx"	"
<g/>
hromničky	hromnička	k1gFnPc1	hromnička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
bouřek	bouřka	k1gFnPc2	bouřka
zapálené	zapálený	k2eAgFnPc1d1	zapálená
dávaly	dávat	k5eAaImAgFnP	dávat
do	do	k7c2	do
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
modlitbou	modlitba	k1gFnSc7	modlitba
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
bouřkami	bouřka	k1gFnPc7	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
dnem	den	k1gInSc7	den
také	také	k9	také
dříve	dříve	k6eAd2	dříve
končila	končit	k5eAaImAgFnS	končit
vánoční	vánoční	k2eAgFnSc1d1	vánoční
doba	doba	k1gFnSc1	doba
a	a	k8xC	a
sklízely	sklízet	k5eAaImAgFnP	sklízet
se	se	k3xPyFc4	se
betlémy	betlém	k1gInPc7	betlém
z	z	k7c2	z
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
z	z	k7c2	z
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
spojován	spojovat	k5eAaImNgInS	spojovat
také	také	k6eAd1	také
s	s	k7c7	s
ochotou	ochota	k1gFnSc7	ochota
zasvětit	zasvětit	k5eAaPmF	zasvětit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
poslání	poslání	k1gNnSc4	poslání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
člověk	člověk	k1gMnSc1	člověk
dostává	dostávat	k5eAaImIp3nS	dostávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
slaven	slaven	k2eAgInSc1d1	slaven
také	také	k9	také
jako	jako	k9	jako
Den	den	k1gInSc4	den
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
řeholníků	řeholník	k1gMnPc2	řeholník
a	a	k8xC	a
řeholnic	řeholnice	k1gFnPc2	řeholnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
