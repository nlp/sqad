<p>
<s>
Rory	Ror	k2eAgFnPc1d1	Ror
McIlroy	McIlroa	k1gFnPc1	McIlroa
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Holywood	Holywood	k1gInSc1	Holywood
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severoirský	severoirský	k2eAgMnSc1d1	severoirský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
golfista	golfista	k1gMnSc1	golfista
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
řádu	řád	k1gInSc2	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
a	a	k8xC	a
čtyřnásobným	čtyřnásobný	k2eAgMnSc7d1	čtyřnásobný
vítězem	vítěz	k1gMnSc7	vítěz
majoru	major	k1gMnSc3	major
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
s	s	k7c7	s
rekordním	rekordní	k2eAgInSc7d1	rekordní
výsledkem	výsledek	k1gInSc7	výsledek
16	[number]	k4	16
ran	rána	k1gFnPc2	rána
pod	pod	k7c7	pod
par	para	k1gFnPc2	para
a	a	k8xC	a
náskokem	náskok	k1gInSc7	náskok
8	[number]	k4	8
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
PGA	PGA	kA	PGA
Championship	Championship	k1gInSc4	Championship
opět	opět	k6eAd1	opět
o	o	k7c4	o
8	[number]	k4	8
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vítězství	vítězství	k1gNnSc4	vítězství
z	z	k7c2	z
British	Britisha	k1gFnPc2	Britisha
Open	Openo	k1gNnPc2	Openo
2014	[number]	k4	2014
a	a	k8xC	a
PGA	PGA	kA	PGA
Championship	Championship	k1gInSc4	Championship
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
sponzorem	sponzor	k1gMnSc7	sponzor
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
Nike	Nike	k1gFnSc1	Nike
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
desetiletý	desetiletý	k2eAgInSc1d1	desetiletý
kontrakt	kontrakt	k1gInSc1	kontrakt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
přinést	přinést	k5eAaPmF	přinést
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejzajímavějších	zajímavý	k2eAgMnPc2d3	nejzajímavější
mladých	mladý	k2eAgMnPc2d1	mladý
golfistů	golfista	k1gMnPc2	golfista
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
novodobou	novodobý	k2eAgFnSc7d1	novodobá
ikonou	ikona	k1gFnSc7	ikona
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
Tigera	Tiger	k1gMnSc4	Tiger
Woodse	Woods	k1gMnSc4	Woods
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
časopisu	časopis	k1gInSc2	časopis
Men	Men	k1gFnPc2	Men
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Health	Healtha	k1gFnPc2	Healtha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
