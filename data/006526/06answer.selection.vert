<s>
Příběh	příběh	k1gInSc1
jednoho	jeden	k4xCgInSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
1883	[number]	k4
Miláček	miláček	k1gMnSc1
(	(	kIx(
<g/>
Bel-Ami	Bel-A	k1gFnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1885	[number]	k4
–	–	k?
společensko-kritický	společenskoritický	k2eAgInSc4d1
román	román	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
si	se	k3xPyFc3
mladý	mladý	k2eAgMnSc1d1
novinář	novinář	k1gMnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
své	svůj	k3xOyFgFnSc2
přitažlivosti	přitažlivost	k1gFnSc2
buduje	budovat	k5eAaImIp3nS
kariéru	kariéra	k1gFnSc4
využíváním	využívání	k1gNnSc7
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>