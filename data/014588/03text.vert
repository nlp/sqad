<s>
Cageri	Cageri	k6eAd1
</s>
<s>
Cageri	Cageri	k6eAd1
ც	ც	k?
Pohled	pohled	k1gInSc1
na	na	k7c4
městoPoloha	městoPoloh	k1gMnSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
42	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
475	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
4	#num#	k4
Stát	stát	k1gInSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc2
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
gruz	gruz	k1gInSc1
<g/>
:	:	kIx,
mchare	mchar	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Rača-Lečchumi	Rača-Lečchu	k1gFnPc7
a	a	k8xC
Dolní	dolní	k2eAgFnPc4d1
Svanetie	Svanetie	k1gFnPc4
okres	okres	k1gInSc1
</s>
<s>
Cageri	Cageri	k6eAd1
</s>
<s>
Cageri	Cageri	k6eAd1
</s>
<s>
Cageri	Cageri	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
320	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cageri	Cageri	k1gNnSc1
(	(	kIx(
<g/>
gruzínsky	gruzínsky	k6eAd1
ც	ც	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
administrativním	administrativní	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
okresu	okres	k1gInSc2
Cageri	Cager	k1gFnSc2
v	v	k7c6
kraji	kraj	k1gInSc6
Rača-Lečchumi	Rača-Lečchu	k1gFnPc7
a	a	k8xC
Dolní	dolní	k2eAgFnPc4d1
Svanetie	Svanetie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
asi	asi	k9
1320	#num#	k4
obyvateli	obyvatel	k1gMnPc7
je	být	k5eAaImIp3nS
nejmenším	malý	k2eAgNnSc7d3
městem	město	k1gNnSc7
Gruzie	Gruzie	k1gFnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Cageri	Cageri	k6eAd1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
Gruzie	Gruzie	k1gFnSc2
<g/>
,	,	kIx,
necelých	celý	k2eNgInPc2d1
200	#num#	k4
km	km	kA
od	od	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Tbilisi	Tbilisi	k1gNnSc2
<g/>
,	,	kIx,
35	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
krajského	krajský	k2eAgNnSc2d1
města	město	k1gNnSc2
Ambrolauri	Ambrolaur	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Cchenisckali	Cchenisckali	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
je	být	k5eAaImIp3nS
pravým	pravý	k2eAgInSc7d1
přítokem	přítok	k1gInSc7
Rioni	Rioni	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
obklopeno	obklopit	k5eAaPmNgNnS
horami	hora	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
jižně	jižně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
dosahují	dosahovat	k5eAaImIp3nP
nadmořské	nadmořský	k2eAgFnPc1d1
výšky	výška	k1gFnPc1
1000	#num#	k4
m	m	kA
a	a	k8xC
severozápadně	severozápadně	k6eAd1
3174	#num#	k4
m	m	kA
(	(	kIx(
<g/>
vrchol	vrchol	k1gInSc1
Cikuri	Cikur	k1gFnSc2
v	v	k7c6
Egrisském	Egrisský	k2eAgInSc6d1
hřbetu	hřbet	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
středověku	středověk	k1gInSc2
bylo	být	k5eAaImAgNnS
a	a	k8xC
je	on	k3xPp3gFnPc4
Cageri	Cager	k1gFnPc4
sídlem	sídlo	k1gNnSc7
eparchie	eparchie	k1gFnSc2
gruzínské	gruzínský	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historik	historik	k1gMnSc1
a	a	k8xC
geograf	geograf	k1gMnSc1
Wachušti	Wachušti	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
gruzínského	gruzínský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Vachtanga	Vachtang	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
hodnotil	hodnotit	k5eAaImAgMnS
původní	původní	k2eAgMnSc1d1
<g/>
,	,	kIx,
nádhernými	nádherný	k2eAgFnPc7d1
freskami	freska	k1gFnPc7
zdobený	zdobený	k2eAgInSc4d1
kostel	kostel	k1gInSc4
s	s	k7c7
klenutou	klenutý	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
v	v	k7c6
Cageri	Cager	k1gFnSc6
z	z	k7c2
počátku	počátek	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
výtečně	výtečně	k6eAd1
postavený	postavený	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
byl	být	k5eAaImAgInS
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
značně	značně	k6eAd1
přestavěn	přestavěn	k2eAgMnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
těchto	tento	k3xDgFnPc2
fresek	freska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Politicky	politicky	k6eAd1
patřilo	patřit	k5eAaImAgNnS
město	město	k1gNnSc1
k	k	k7c3
historické	historický	k2eAgFnSc3d1
provincii	provincie	k1gFnSc3
Lečchumi	Lečchu	k1gFnPc7
(	(	kIx(
<g/>
původně	původně	k6eAd1
zvané	zvaný	k2eAgFnPc1d1
Takveri	Takver	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
době	doba	k1gFnSc6
spojeného	spojený	k2eAgInSc2d1
gruzínského	gruzínský	k2eAgInSc2d1
státu	stát	k1gInSc2
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
postupně	postupně	k6eAd1
podřízená	podřízený	k2eAgFnSc1d1
princům	princ	k1gMnPc3
ze	z	k7c2
Svanetie	Svanetie	k1gFnSc2
a	a	k8xC
Rače	Rače	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
spojeného	spojený	k2eAgInSc2d1
státu	stát	k1gInSc2
se	se	k3xPyFc4
Cageri	Cager	k1gMnPc1
spolu	spolu	k6eAd1
s	s	k7c7
Lečchumi	Lečchu	k1gFnPc7
stalo	stát	k5eAaPmAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1455	#num#	k4
součástí	součást	k1gFnSc7
Království	království	k1gNnSc2
Imeretie	Imeretie	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
pod	pod	k7c7
místním	místní	k2eAgInSc7d1
šlechtickým	šlechtický	k2eAgInSc7d1
rodem	rod	k1gInSc7
Čikovani	Čikovaň	k1gFnSc3
se	se	k3xPyFc4
fakticky	fakticky	k6eAd1
osamostatnilo	osamostatnit	k5eAaPmAgNnS
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
roku	rok	k1gInSc2
1714	#num#	k4
díky	díky	k7c3
příbuzenským	příbuzenský	k2eAgInPc3d1
vztahům	vztah	k1gInPc3
k	k	k7c3
rodu	rod	k1gInSc3
Dadiani	Dadiaň	k1gFnSc6
z	z	k7c2
Mingrelie	Mingrelie	k1gFnSc2
s	s	k7c7
tímto	tento	k3xDgNnSc7
vévodstvím	vévodství	k1gNnSc7
sjednotilo	sjednotit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1857	#num#	k4
spadlo	spadnout	k5eAaPmAgNnS
Cageri	Cageri	k1gNnSc1
s	s	k7c7
vévodstvím	vévodství	k1gNnSc7
Mingrelie	Mingrelie	k1gFnSc2
pod	pod	k7c4
Ruské	ruský	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
a	a	k8xC
bylo	být	k5eAaImAgNnS
začleněno	začlenit	k5eAaPmNgNnS
do	do	k7c2
Gubernie	gubernie	k1gFnSc2
Kutais	Kutais	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
(	(	kIx(
<g/>
ruská	ruský	k2eAgNnPc4d1
<g/>
)	)	kIx)
městská	městský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
jako	jako	k8xS,k8xC
správní	správní	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
Ujezdu	Ujezd	k1gInSc2
Lečchumi	Lečchu	k1gFnPc7
<g/>
,	,	kIx,
pod	pod	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
spadaly	spadat	k5eAaImAgFnP,k5eAaPmAgFnP
kromě	kromě	k7c2
původního	původní	k2eAgNnSc2d1
území	území	k1gNnSc2
i	i	k8xC
části	část	k1gFnSc2
Svanetie	Svanetie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
sovětské	sovětský	k2eAgFnSc2d1
éry	éra	k1gFnSc2
mělo	mít	k5eAaImAgNnS
Cageri	Cageri	k1gNnSc1
jako	jako	k8xS,k8xC
správní	správní	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
roku	rok	k1gInSc2
1930	#num#	k4
založeného	založený	k2eAgInSc2d1
rajónu	rajón	k1gInSc2
Gruzínské	gruzínský	k2eAgFnSc2d1
SSR	SSR	kA
statut	statut	k1gInSc4
sídla	sídlo	k1gNnSc2
městského	městský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
roku	rok	k1gInSc2
1968	#num#	k4
znovu	znovu	k6eAd1
získalo	získat	k5eAaPmAgNnS
statut	statut	k1gInSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
narostl	narůst	k5eAaPmAgInS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
na	na	k7c4
dvojnásobek	dvojnásobek	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
na	na	k7c6
řekách	řeka	k1gFnPc6
Cchenisckali	Cchenisckali	k1gFnPc2
a	a	k8xC
několik	několik	k4yIc4
kilometrů	kilometr	k1gInPc2
východně	východně	k6eAd1
tekoucí	tekoucí	k2eAgFnSc2d1
Ladžanuri	Ladžanur	k1gFnSc2
vybudována	vybudován	k2eAgFnSc1d1
přehradní	přehradní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
s	s	k7c7
vodní	vodní	k2eAgFnSc7d1
elektrárnou	elektrárna	k1gFnSc7
a	a	k8xC
Cageri	Cageri	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
sloužit	sloužit	k5eAaImF
jako	jako	k9
logistické	logistický	k2eAgNnSc4d1
a	a	k8xC
pobytové	pobytový	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1897687	#num#	k4
</s>
<s>
19594709	#num#	k4
</s>
<s>
19702081	#num#	k4
</s>
<s>
19792081	#num#	k4
</s>
<s>
19891359	#num#	k4
</s>
<s>
20021913	#num#	k4
</s>
<s>
20091800	#num#	k4
</s>
<s>
Pozoruhodnosti	pozoruhodnost	k1gFnPc1
</s>
<s>
V	v	k7c6
Cageri	Cager	k1gInSc6
je	být	k5eAaImIp3nS
Katedrála	katedrála	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Eparchy	eparcha	k1gMnSc2
z	z	k7c2
Cageri	Cager	k1gFnSc2
a	a	k8xC
Lentechi	Lentechi	k1gNnSc2
gruzínské	gruzínský	k2eAgFnSc2d1
ortodoxní	ortodoxní	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Klášter	klášter	k1gInSc1
Svatého	svatý	k2eAgMnSc2d1
Maxima	Maxim	k1gMnSc2
Vyznavače	vyznavač	k1gMnSc2
se	s	k7c7
stejnojmenným	stejnojmenný	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
hrob	hrob	k1gInSc1
řeckého	řecký	k2eAgMnSc2d1
mnicha	mnich	k1gMnSc2
a	a	k8xC
teologa	teolog	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
vyhnanství	vyhnanství	k1gNnSc6
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
662	#num#	k4
v	v	k7c6
kastelu	kastel	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
podle	podle	k7c2
jedné	jeden	k4xCgFnSc2
verze	verze	k1gFnSc2
odpovídá	odpovídat	k5eAaImIp3nS
pozdější	pozdní	k2eAgFnSc2d2
pevnosti	pevnost	k1gFnSc2
Murisciche	Muriscich	k1gFnSc2
nedaleko	nedaleko	k7c2
Cageri	Cager	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
též	též	k9
vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nedaleké	daleký	k2eNgFnSc6d1
vesnici	vesnice	k1gFnSc6
Bardnala	Bardnala	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c4
muzeum	muzeum	k1gNnSc4
přeměněný	přeměněný	k2eAgInSc1d1
rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
básníka	básník	k1gMnSc2
Lado	Lada	k1gFnSc5
Assatiani	Assatiaň	k1gFnSc6
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Zageri	Zager	k1gFnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Cageri	Cager	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
