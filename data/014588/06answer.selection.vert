<s>
Cageri	Cageri	k6eAd1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
Gruzie	Gruzie	k1gFnSc2
<g/>
,	,	kIx,
necelých	celý	k2eNgInPc2d1
200	#num#	k4
km	km	kA
od	od	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Tbilisi	Tbilisi	k1gNnSc2
<g/>
,	,	kIx,
35	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
krajského	krajský	k2eAgNnSc2d1
města	město	k1gNnSc2
Ambrolauri	Ambrolaur	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Cchenisckali	Cchenisckali	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
je	být	k5eAaImIp3nS
pravým	pravý	k2eAgInSc7d1
přítokem	přítok	k1gInSc7
Rioni	Rioni	k1gFnSc2
<g/>
.	.	kIx.
</s>