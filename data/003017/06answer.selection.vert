<s>
Významným	významný	k2eAgMnSc7d1	významný
českým	český	k2eAgMnSc7d1	český
organologem	organolog	k1gMnSc7	organolog
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
Guštar	Guštar	k1gMnSc1	Guštar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
až	až	k9	až
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgInS	vydat
monumentální	monumentální	k2eAgNnSc4d1	monumentální
dílo	dílo	k1gNnSc4	dílo
Elektrofony	Elektrofon	k1gInPc1	Elektrofon
<g/>
.	.	kIx.	.
</s>
