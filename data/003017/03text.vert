<s>
Organologie	organologie	k1gFnSc1	organologie
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
hudebními	hudební	k2eAgInPc7d1	hudební
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gNnPc3	jejich
zkoumáním	zkoumání	k1gNnPc3	zkoumání
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
akustických	akustický	k2eAgFnPc2d1	akustická
a	a	k8xC	a
mechanických	mechanický	k2eAgFnPc2d1	mechanická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
technik	technika	k1gFnPc2	technika
a	a	k8xC	a
hudební	hudební	k2eAgFnSc2d1	hudební
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
ὄ	ὄ	k?	ὄ
(	(	kIx(	(
<g/>
organon	organon	k1gInSc1	organon
-	-	kIx~	-
původně	původně	k6eAd1	původně
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
-	-	kIx~	-
nejen	nejen	k6eAd1	nejen
hudební	hudební	k2eAgInSc1d1	hudební
-	-	kIx~	-
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
i	i	k9	i
vědní	vědní	k2eAgInSc4d1	vědní
podobor	podobor	k1gInSc4	podobor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
varhanami	varhany	k1gFnPc7	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
Michaelem	Michael	k1gMnSc7	Michael
Praetoriem	Praetorium	k1gNnSc7	Praetorium
v	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
Syntagma	syntagma	k1gNnSc1	syntagma
musicum	musicum	k1gNnSc1	musicum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
<g/>
.	.	kIx.	.
</s>
<s>
Organologie	organologie	k1gFnSc1	organologie
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
obory	obor	k1gInPc4	obor
<g/>
:	:	kIx,	:
Historická	historický	k2eAgFnSc1d1	historická
<g/>
:	:	kIx,	:
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
staré	starý	k2eAgInPc4d1	starý
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
skupiny	skupina	k1gFnPc4	skupina
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
<g/>
:	:	kIx,	:
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
nástroji	nástroj	k1gInPc7	nástroj
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
technikou	technika	k1gFnSc7	technika
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgFnPc2d1	poslední
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
<g/>
:	:	kIx,	:
popisuje	popisovat	k5eAaImIp3nS	popisovat
technický	technický	k2eAgInSc4d1	technický
vývoj	vývoj	k1gInSc4	vývoj
nástrojů	nástroj	k1gInPc2	nástroj
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
konstrukci	konstrukce	k1gFnSc4	konstrukce
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
související	související	k2eAgFnPc4d1	související
změny	změna	k1gFnPc4	změna
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Dodones	Dodones	k1gMnSc1	Dodones
obecně	obecně	k6eAd1	obecně
nejčastěji	často	k6eAd3	často
užívaná	užívaný	k2eAgFnSc1d1	užívaná
Hornbostel-Sachsova	Hornbostel-Sachsův	k2eAgFnSc1d1	Hornbostel-Sachsův
systematika	systematika	k1gFnSc1	systematika
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
muzikologa	muzikolog	k1gMnSc2	muzikolog
Ericha	Erich	k1gMnSc4	Erich
Moritze	moritz	k1gInSc2	moritz
von	von	k1gInSc1	von
Hornbostela	Hornbostel	k1gMnSc2	Hornbostel
a	a	k8xC	a
Curta	Curt	k1gMnSc2	Curt
Sachse	Sachs	k1gMnSc2	Sachs
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
společně	společně	k6eAd1	společně
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
"	"	kIx"	"
<g/>
Sbírce	sbírka	k1gFnSc6	sbírka
starých	starý	k2eAgInPc2d1	starý
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
při	při	k7c6	při
Státní	státní	k2eAgFnSc6d1	státní
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k8xC	jako
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
systematizaci	systematizace	k1gFnSc6	systematizace
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
o	o	k7c6	o
etnologii	etnologie	k1gFnSc6	etnologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
systému	systém	k1gInSc6	systém
od	od	k7c2	od
Victora-Charlese	Victora-Charlese	k1gFnSc2	Victora-Charlese
Mahillona	Mahillon	k1gMnSc2	Mahillon
<g/>
,	,	kIx,	,
kurátora	kurátor	k1gMnSc2	kurátor
muzea	muzeum	k1gNnSc2	muzeum
při	při	k7c6	při
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
Hans-Heinz	Hans-Heinz	k1gMnSc1	Hans-Heinz
Dräger	Dräger	k1gMnSc1	Dräger
tuto	tento	k3xDgFnSc4	tento
systematiku	systematika	k1gFnSc4	systematika
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
o	o	k7c4	o
skupinu	skupina	k1gFnSc4	skupina
elektrofonů	elektrofon	k1gInPc2	elektrofon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
Hornbostel-Sachsovy	Hornbostel-Sachsův	k2eAgFnSc2d1	Hornbostel-Sachsův
systematiki	systematik	k1gFnSc2	systematik
stojí	stát	k5eAaImIp3nS	stát
především	především	k9	především
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
tvoření	tvoření	k1gNnSc2	tvoření
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
původci	původce	k1gMnPc1	původce
tónu	tón	k1gInSc2	tón
(	(	kIx(	(
<g/>
oscilátoru	oscilátor	k1gInSc2	oscilátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
chordofonů	chordofon	k1gInPc2	chordofon
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
skříňky	skříňka	k1gFnSc2	skříňka
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Idiofony	Idiofon	k1gInPc1	Idiofon
úderové	úderový	k2eAgInPc1d1	úderový
idiofony	idiofon	k1gInPc4	idiofon
drnkací	drnkací	k2eAgMnSc1d1	drnkací
(	(	kIx(	(
<g/>
trsací	trsací	k2eAgInPc4d1	trsací
<g/>
)	)	kIx)	)
idiofony	idiofon	k1gInPc4	idiofon
třecí	třecí	k2eAgFnSc2d1	třecí
<g />
.	.	kIx.	.
</s>
<s>
idiofony	idiofon	k1gInPc1	idiofon
vzduchové	vzduchový	k2eAgInPc1d1	vzduchový
idiofony	idiofon	k1gInPc1	idiofon
Jednotné	jednotný	k2eAgFnSc2d1	jednotná
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
člěnění	člěnění	k1gNnPc2	člěnění
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Membranofony	Membranofon	k1gInPc1	Membranofon
úderové	úderový	k2eAgInPc1d1	úderový
membranofony	membranofon	k1gInPc1	membranofon
trsací	trsací	k2eAgInPc1d1	trsací
membranofony	membranofon	k1gInPc1	membranofon
třecí	třecí	k2eAgInPc1d1	třecí
membranofony	membranofon	k1gInPc1	membranofon
vzduchové	vzduchový	k2eAgInPc1d1	vzduchový
membranofony	membranofon	k1gInPc1	membranofon
(	(	kIx(	(
<g/>
mirlitony	mirliton	k1gInPc1	mirliton
<g/>
)	)	kIx)	)
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
člěnění	člěnění	k1gNnSc1	člěnění
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Chordofony	Chordofon	k1gInPc1	Chordofon
prosté	prostý	k2eAgInPc1d1	prostý
(	(	kIx(	(
<g/>
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
)	)	kIx)	)
chordofony	chordofon	k1gInPc1	chordofon
nebo	nebo	k8xC	nebo
citery	citera	k1gFnPc1	citera
složené	složený	k2eAgFnPc1d1	složená
chordofony	chordofon	k1gInPc4	chordofon
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
člěnění	člěnění	k1gNnSc1	člěnění
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Aerofony	Aerofon	k1gInPc1	Aerofon
volné	volný	k2eAgInPc1d1	volný
aerofony	aerofon	k1gInPc1	aerofon
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
dechové	dechový	k2eAgInPc1d1	dechový
nástroje	nástroj	k1gInPc1	nástroj
Jednotné	jednotný	k2eAgFnSc2d1	jednotná
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
člěnění	člěnění	k1gNnPc2	člěnění
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Elektrofony	Elektrofon	k1gInPc4	Elektrofon
Elektromechanické	elektromechanický	k2eAgInPc4d1	elektromechanický
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
Elektronické	elektronický	k2eAgInPc4d1	elektronický
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
Digitalní	Digitalný	k2eAgMnPc1d1	Digitalný
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
Historicky	historicky	k6eAd1	historicky
důležitými	důležitý	k2eAgFnPc7d1	důležitá
osobnostmi	osobnost	k1gFnPc7	osobnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
jsou	být	k5eAaImIp3nP	být
Xavier	Xavier	k1gInSc4	Xavier
Mahillon	Mahillon	k1gInSc1	Mahillon
(	(	kIx(	(
<g/>
Brusel	Brusel	k1gInSc1	Brusel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Moritz	Moritz	k1gMnSc1	Moritz
von	von	k1gInSc4	von
Hornbostel	Hornbostel	k1gInSc1	Hornbostel
(	(	kIx(	(
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
Curt	Curt	k2eAgInSc1d1	Curt
Sachs	Sachs	k1gInSc1	Sachs
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
českým	český	k2eAgMnSc7d1	český
organologem	organolog	k1gMnSc7	organolog
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
Guštar	Guštar	k1gMnSc1	Guštar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
až	až	k9	až
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgInS	vydat
monumentální	monumentální	k2eAgNnSc4d1	monumentální
dílo	dílo	k1gNnSc4	dílo
Elektrofony	Elektrofon	k1gInPc1	Elektrofon
<g/>
.	.	kIx.	.
</s>
<s>
Izidor	Izidor	k1gInSc1	Izidor
Sevilský	Sevilský	k2eAgInSc1d1	Sevilský
<g/>
,	,	kIx,	,
Etymologiarum	Etymologiarum	k1gInSc1	Etymologiarum
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
XX	XX	kA	XX
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jedna	jeden	k4xCgFnSc1	jeden
ručně	ručně	k6eAd1	ručně
psaná	psaný	k2eAgFnSc1d1	psaná
popisy	popis	k1gInPc1	popis
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Martinus	Martinus	k1gInSc1	Martinus
Agricola	Agricola	k1gFnSc1	Agricola
<g/>
,	,	kIx,	,
Musica	Music	k2eAgFnSc1d1	Musica
instumentalis	instumentalis	k1gFnSc1	instumentalis
(	(	kIx(	(
<g/>
1529	[number]	k4	1529
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Praetorius	Praetorius	k1gMnSc1	Praetorius
<g/>
,	,	kIx,	,
Syntagma	syntagma	k1gNnSc1	syntagma
Musicum	Musicum	k1gInSc1	Musicum
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
II	II	kA	II
<g/>
.	.	kIx.	.
díle	díl	k1gInSc6	díl
"	"	kIx"	"
<g/>
De	De	k?	De
orghanographia	orghanographia	k1gFnSc1	orghanographia
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
titulní	titulní	k2eAgFnSc1d1	titulní
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
tabulky	tabulka	k1gFnPc1	tabulka
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
nástrojům	nástroj	k1gInPc3	nástroj
-	-	kIx~	-
chordofonům	chordofon	k1gInPc3	chordofon
a	a	k8xC	a
aerofonům	aerofon	k1gInPc3	aerofon
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Praetorius	Praetorius	k1gMnSc1	Praetorius
<g/>
,	,	kIx,	,
Theatrum	Theatrum	k1gNnSc1	Theatrum
Instrumentorum	Instrumentorum	k1gInSc1	Instrumentorum
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Athanasius	Athanasius	k1gInSc1	Athanasius
Kircher	Kirchra	k1gFnPc2	Kirchra
<g/>
,	,	kIx,	,
Musurgia	Musurgia	k1gFnSc1	Musurgia
universalis	universalis	k1gFnSc1	universalis
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tabulky	tabulka	k1gFnPc4	tabulka
o	o	k7c4	o
intonaci	intonace	k1gFnSc4	intonace
timpanů	timpan	k1gMnPc2	timpan
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Organologie	organologie	k1gFnSc2	organologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
organologie	organologie	k1gFnSc2	organologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
