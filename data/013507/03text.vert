<s>
Operace	operace	k1gFnSc1
Greenhouse	Greenhouse	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Operace	operace	k1gFnSc1
Greenhouse	Greenhouse	k1gInSc1
byla	být	k5eAaImAgFnS
série	série	k1gFnSc1
čtyř	čtyři	k4xCgInPc2
nukleárních	nukleární	k2eAgInPc2d1
testů	test	k1gInPc2
provedených	provedený	k2eAgInPc2d1
USA	USA	kA
mezi	mezi	k7c7
dubnem	duben	k1gInSc7
a	a	k8xC
květnem	květen	k1gInSc7
roku	rok	k1gInSc2
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
zde	zde	k6eAd1
testovány	testován	k2eAgFnPc1d1
bomby	bomba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
využívaly	využívat	k5eAaPmAgFnP,k5eAaImAgFnP
termonukleární	termonukleární	k2eAgFnSc4d1
fúzi	fúze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
4	#num#	k4
testy	testa	k1gFnSc2
–	–	k?
Dog	doga	k1gFnPc2
<g/>
,	,	kIx,
Easy	Easy	k1gInPc1
<g/>
,	,	kIx,
George	George	k1gNnPc1
<g/>
,	,	kIx,
Item	Item	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
2	#num#	k4
byly	být	k5eAaImAgInP
testy	test	k1gInPc1
2	#num#	k4
nových	nový	k2eAgFnPc2d1
strategických	strategický	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
Mk-	Mk-	k1gFnSc4
<g/>
5	#num#	k4
a	a	k8xC
Mk-	Mk-	k1gMnSc1
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
2	#num#	k4
byly	být	k5eAaImAgFnP
ale	ale	k9
zajímavější	zajímavý	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	George	k1gInSc1
byl	být	k5eAaImAgInS
se	s	k7c7
sílou	síla	k1gFnSc7
225	#num#	k4
kilotun	kilotuno	k1gNnPc2
nejsilnější	silný	k2eAgFnSc1d3
bomba	bomba	k1gFnSc1
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
Item	Ite	k1gNnSc7
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
bomba	bomba	k1gFnSc1
posílená	posílený	k2eAgFnSc1d1
termonukleární	termonukleární	k2eAgFnSc7d1
fúzí	fúze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dog	doga	k1gFnPc2
</s>
<s>
Síla	síla	k1gFnSc1
<g/>
:	:	kIx,
81	#num#	k4
kilotun	kilotuna	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
</s>
<s>
Místo	místo	k1gNnSc1
<g/>
:	:	kIx,
Atol	atol	k1gInSc1
Enewetak	Enewetak	k1gInSc1
</s>
<s>
Typ	typ	k1gInSc1
a	a	k8xC
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
Věž	věž	k1gFnSc1
<g/>
,	,	kIx,
300	#num#	k4
stop	stopa	k1gFnPc2
</s>
<s>
Test	test	k1gInSc1
Dog	doga	k1gFnPc2
byl	být	k5eAaImAgInS
test	test	k1gInSc1
strategické	strategický	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
Mk-	Mk-	k1gFnSc2
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
sílou	síla	k1gFnSc7
81	#num#	k4
kilotun	kilotuna	k1gFnPc2
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
rekord	rekord	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bomba	bomba	k1gFnSc1
měla	mít	k5eAaImAgFnS
zredukovanou	zredukovaný	k2eAgFnSc4d1
váhu	váha	k1gFnSc4
a	a	k8xC
používala	používat	k5eAaImAgFnS
60	#num#	k4
bodově	bodově	k6eAd1
iniciovanou	iniciovaný	k2eAgFnSc4d1
implozi	imploze	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
lepší	dobrý	k2eAgFnSc4d2
kompresi	komprese	k1gFnSc4
než	než	k8xS
stará	starat	k5eAaImIp3nS
32	#num#	k4
bodově	bodově	k6eAd1
iniciovaná	iniciovaný	k2eAgFnSc1d1
imploze	imploze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bomby	bomba	k1gFnPc4
typu	typ	k1gInSc2
Mk-	Mk-	k1gFnSc2
<g/>
6	#num#	k4
byly	být	k5eAaImAgFnP
první	první	k4xOgFnSc6
masově	masově	k6eAd1
zavedené	zavedený	k2eAgFnPc4d1
jaderné	jaderný	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
v	v	k7c6
arzenálu	arzenál	k1gInSc6
USA	USA	kA
(	(	kIx(
<g/>
počet	počet	k1gInSc1
přes	přes	k7c4
1000	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používala	používat	k5eAaImAgFnS
kompozitní	kompozitní	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
na	na	k7c6
bázi	báze	k1gFnSc6
uranu	uran	k1gInSc2
235	#num#	k4
a	a	k8xC
plutonia	plutonium	k1gNnSc2
239	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Easy	Easa	k1gFnPc1
</s>
<s>
Síla	síla	k1gFnSc1
<g/>
:	:	kIx,
47	#num#	k4
kilotun	kilotuna	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
<g/>
:	:	kIx,
21	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1951	#num#	k4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
</s>
<s>
Místo	místo	k1gNnSc1
<g/>
:	:	kIx,
Atol	atol	k1gInSc1
Enewetak	Enewetak	k1gInSc1
</s>
<s>
Typ	typ	k1gInSc1
a	a	k8xC
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
Věž	věž	k1gFnSc1
<g/>
,	,	kIx,
300	#num#	k4
stop	stopa	k1gFnPc2
</s>
<s>
Typ	typ	k1gInSc1
bomby	bomba	k1gFnSc2
Mk-	Mk-	k1gFnSc2
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompozitní	kompozitní	k2eAgInPc1d1
jádro	jádro	k1gNnSc4
na	na	k7c6
bázi	báze	k1gFnSc6
uranu	uran	k1gInSc2
235	#num#	k4
a	a	k8xC
plutonia	plutonium	k1gNnSc2
239	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snížená	snížený	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
bomby	bomba	k1gFnSc2
<g/>
.	.	kIx.
92	#num#	k4
bodově	bodově	k6eAd1
iniciovaná	iniciovaný	k2eAgFnSc1d1
imploze	imploze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bomba	bomba	k1gFnSc1
této	tento	k3xDgFnSc2
konstrukce	konstrukce	k1gFnSc2
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
jako	jako	k9
primární	primární	k2eAgFnSc1d1
v	v	k7c6
testu	test	k1gInSc6
první	první	k4xOgFnSc2
skutečné	skutečný	k2eAgFnSc2d1
termonukleární	termonukleární	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
Ivy	Iva	k1gFnSc2
Mike	Mik	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Test	test	k1gInSc1
Easy	Easa	k1gFnSc2
sloužil	sloužit	k5eAaImAgInS
ke	k	k7c3
zkoumání	zkoumání	k1gNnSc3
efektu	efekt	k1gInSc2
jaderného	jaderný	k2eAgInSc2d1
výbuchu	výbuch	k1gInSc2
na	na	k7c4
vojenské	vojenský	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
atd.	atd.	kA
</s>
<s>
George	George	k6eAd1
</s>
<s>
Síla	síla	k1gFnSc1
<g/>
:	:	kIx,
225	#num#	k4
kilotun	kilotuna	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
<g/>
:	:	kIx,
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Místo	místo	k1gNnSc1
<g/>
:	:	kIx,
Atol	atol	k1gInSc1
Enewetak	Enewetak	k1gInSc1
</s>
<s>
Typ	typ	k1gInSc1
a	a	k8xC
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
200	#num#	k4
stop	stop	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
věž	věž	k1gFnSc1
</s>
<s>
George	George	k6eAd1
byl	být	k5eAaImAgInS
prvním	první	k4xOgInSc7
termonukleárním	termonukleární	k2eAgInSc7d1
testem	test	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
jakousi	jakýsi	k3yIgFnSc4
cylindrickou	cylindrický	k2eAgFnSc4d1
implozi	imploze	k1gFnSc4
na	na	k7c4
duté	dutý	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
bylo	být	k5eAaImAgNnS
kryogenické	kryogenický	k2eAgNnSc4d1
deuterium	deuterium	k1gNnSc4
s	s	k7c7
pár	pár	k4xCyI
procenty	procento	k1gNnPc7
tritia	tritium	k1gNnSc2
pro	pro	k7c4
snadnější	snadný	k2eAgFnSc4d2
„	„	k?
<g/>
zapálení	zapálení	k1gNnSc2
<g/>
“	“	k?
termonukleární	termonukleární	k2eAgFnSc1d1
fúze	fúze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
mělo	mít	k5eAaImAgNnS
8	#num#	k4
stop	stopa	k1gFnPc2
v	v	k7c6
průměru	průměr	k1gInSc6
a	a	k8xC
bylo	být	k5eAaImAgNnS
2	#num#	k4
stopy	stop	k1gInPc1
tlusté	tlustý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc7d1
částí	část	k1gFnSc7
se	se	k3xPyFc4
na	na	k7c6
tomto	tento	k3xDgNnSc6
podílel	podílet	k5eAaImAgMnS
Edward	Edward	k1gMnSc1
Teller	Teller	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
slepou	slepý	k2eAgFnSc4d1
uličku	ulička	k1gFnSc4
a	a	k8xC
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
směřoval	směřovat	k5eAaImAgInS
k	k	k7c3
Teller-Ulamově	Teller-Ulamově	k1gFnSc3
koncepci	koncepce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
třikrát	třikrát	k6eAd1
překročilo	překročit	k5eAaPmAgNnS
rekord	rekord	k1gInSc4
bomby	bomba	k1gFnSc2
„	„	k?
<g/>
Dog	doga	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Item	Item	k6eAd1
</s>
<s>
Síla	síla	k1gFnSc1
<g/>
:	:	kIx,
45,5	45,5	k4
kilotun	kilotuna	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
<g/>
:	:	kIx,
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
</s>
<s>
Místo	místo	k1gNnSc1
<g/>
:	:	kIx,
Atol	atol	k1gInSc1
Enewetak	Enewetak	k1gInSc1
</s>
<s>
Typ	typ	k1gInSc1
a	a	k8xC
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
200	#num#	k4
stop	stop	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
věž	věž	k1gFnSc1
</s>
<s>
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
bomba	bomba	k1gFnSc1
posílená	posílený	k2eAgFnSc1d1
termonukleární	termonukleární	k2eAgFnSc7d1
fúzí	fúze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovnitř	dovnitř	k7c2
jádra	jádro	k1gNnSc2
z	z	k7c2
obohaceného	obohacený	k2eAgInSc2d1
uranu	uran	k1gInSc2
se	se	k3xPyFc4
napustila	napustit	k5eAaPmAgFnS
směs	směs	k1gFnSc1
(	(	kIx(
<g/>
nejspíše	nejspíše	k9
plynného	plynný	k2eAgMnSc2d1
<g/>
)	)	kIx)
deuteria	deuterium	k1gNnSc2
a	a	k8xC
tritia	tritium	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zvýšila	zvýšit	k5eAaPmAgFnS
sílu	síla	k1gFnSc4
z	z	k7c2
asi	asi	k9
20	#num#	k4
na	na	k7c4
45,5	45,5	k4
kilotun	kilotuna	k1gFnPc2
TNT	TNT	kA
<g/>
.	.	kIx.
</s>
