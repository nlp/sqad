<p>
<s>
Podporučík	podporučík	k1gMnSc1	podporučík
je	být	k5eAaImIp3nS	být
důstojnická	důstojnický	k2eAgFnSc1d1	důstojnická
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Podporučík	podporučík	k1gMnSc1	podporučík
byla	být	k5eAaImAgFnS	být
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
důstojnická	důstojnický	k2eAgFnSc1d1	důstojnická
hodnost	hodnost	k1gFnSc1	hodnost
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hodnost	hodnost	k1gFnSc4	hodnost
byl	být	k5eAaImAgMnS	být
poručík	poručík	k1gMnSc1	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Hodnostní	hodnostní	k2eAgNnSc4d1	hodnostní
označení	označení	k1gNnSc4	označení
podporučíků	podporučík	k1gMnPc2	podporučík
tvořila	tvořit	k5eAaImAgFnS	tvořit
jedna	jeden	k4xCgFnSc1	jeden
trojcípá	trojcípý	k2eAgFnSc1d1	trojcípá
zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
podporučíka	podporučík	k1gMnSc2	podporučík
v	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
formálně	formálně	k6eAd1	formálně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
platností	platnost	k1gFnSc7	platnost
hodnostního	hodnostní	k2eAgNnSc2d1	hodnostní
označení	označení	k1gNnSc2	označení
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
vydaného	vydaný	k2eAgInSc2d1	vydaný
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
272	[number]	k4	272
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
upravující	upravující	k2eAgFnSc2d1	upravující
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
novou	nový	k2eAgFnSc4d1	nová
strukturu	struktura	k1gFnSc4	struktura
hodnostních	hodnostní	k2eAgInPc2d1	hodnostní
sborů	sbor	k1gInPc2	sbor
a	a	k8xC	a
hodností	hodnost	k1gFnPc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc7d3	nejnižší
důstojnickou	důstojnický	k2eAgFnSc7d1	důstojnická
hodnostostí	hodnostost	k1gFnSc7	hodnostost
se	se	k3xPyFc4	se
v	v	k7c6	v
profesionální	profesionální	k2eAgFnSc6d1	profesionální
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
stal	stát	k5eAaPmAgMnS	stát
poručík	poručík	k1gMnSc1	poručík
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
hodnostní	hodnostní	k2eAgNnPc1d1	hodnostní
označení	označení	k1gNnPc1	označení
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc4	dva
trojcípé	trojcípý	k2eAgFnPc4d1	trojcípá
zlaté	zlatý	k2eAgFnPc4d1	zlatá
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
podporučík	podporučík	k1gMnSc1	podporučík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
