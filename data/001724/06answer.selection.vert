<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
spustila	spustit	k5eAaPmAgFnS	spustit
vlna	vlna	k1gFnSc1	vlna
instrumentálního	instrumentální	k2eAgInSc2d1	instrumentální
surf	surf	k1gInSc4	surf
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jehož	k3xOyRp3gMnPc4	jehož
zakladatele	zakladatel	k1gMnPc4	zakladatel
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Dick	Dick	k1gInSc1	Dick
Dale	Dal	k1gInSc2	Dal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
typický	typický	k2eAgInSc1d1	typický
"	"	kIx"	"
<g/>
mokrý	mokrý	k2eAgInSc1d1	mokrý
<g/>
"	"	kIx"	"
reverb	reverb	k1gInSc1	reverb
<g/>
.	.	kIx.	.
</s>
