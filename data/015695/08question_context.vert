<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
kriminální	kriminální	k2eAgInSc1d1
film	film	k1gInSc1
režiséra	režisér	k1gMnSc2
Miroslava	Miroslav	k1gMnSc2
Hubáčka	Hubáček	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
,	,	kIx,
natočený	natočený	k2eAgInSc4d1
podle	podle	k7c2
rozhlasové	rozhlasový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Demižon	demižon	k1gInSc1
<g/>
.	.	kIx.
</s>