<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
71	#num#	k4
min	mina	k1gFnPc2
Žánr	žánr	k1gInSc4
</s>
<s>
krimi	krimi	k1gNnSc1
/	/	kIx~
drama	drama	k1gFnSc1
Námět	námět	k1gInSc1
</s>
<s>
rozhlasová	rozhlasový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
Demižon	demižon	k1gInSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
Scénář	scénář	k1gInSc1
</s>
<s>
Josef	Josef	k1gMnSc1
BoučekMiroslav	BoučekMiroslav	k1gMnSc1
KratochvílLubomír	KratochvílLubomír	k1gMnSc1
PokLadislav	PokLadislav	k1gMnSc1
RychmanVladimír	RychmanVladimír	k1gMnSc1
ValentaJiří	ValentaJiř	k1gFnPc2
Brdečka	Brdečka	k1gMnSc1
(	(	kIx(
<g/>
dialogy	dialog	k1gInPc1
<g/>
)	)	kIx)
Režie	režie	k1gFnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Hubáček	Hubáček	k1gMnSc1
Obsazení	obsazení	k1gNnPc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Deyl	Deyl	k1gMnSc1
ml.	ml.	kA
<g/>
Zdeněk	Zdeněk	k1gMnSc1
DítěLubor	DítěLubor	k1gMnSc1
TokošDana	TokošDan	k1gMnSc2
MedřickáJosef	MedřickáJosef	k1gMnSc1
BekFrantišek	BekFrantišek	k1gMnSc1
HolarFrantišek	HolarFrantišek	k1gMnSc1
FilipovskýJosef	FilipovskýJosef	k1gMnSc1
Kemr	Kemr	k1gMnSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Rákos	rákos	k1gInSc4
Hudba	hudba	k1gFnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Lídl	Lídl	k1gMnSc1
st.	st.	kA
Kamera	kamera	k1gFnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Holpuch	Holpuch	k1gMnSc1
Střih	střih	k1gInSc4
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Hájek	Hájek	k1gMnSc1
Zvuk	zvuk	k1gInSc4
</s>
<s>
František	František	k1gMnSc1
Šindelář	Šindelář	k1gMnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Černý	Černý	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1961	#num#	k4
Produkční	produkční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Filmové	filmový	k2eAgNnSc1d1
studio	studio	k1gNnSc1
Barrandov	Barrandov	k1gInSc1
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
na	na	k7c4
FP	FP	kA
<g/>
,	,	kIx,
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
Kinoboxu	Kinobox	k1gInSc2
<g/>
,	,	kIx,
FDb	FDb	k1gFnPc1
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
kriminální	kriminální	k2eAgInSc1d1
film	film	k1gInSc1
režiséra	režisér	k1gMnSc2
Miroslava	Miroslav	k1gMnSc2
Hubáčka	Hubáček	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
,	,	kIx,
natočený	natočený	k2eAgInSc4d1
podle	podle	k7c2
rozhlasové	rozhlasový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Demižon	demižon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
je	být	k5eAaImIp3nS
kriminálním	kriminální	k2eAgInSc7d1
příběhem	příběh	k1gInSc7
o	o	k7c6
zdánlivě	zdánlivě	k6eAd1
obyčejné	obyčejný	k2eAgFnSc3d1
krádeži	krádež	k1gFnSc3
pár	pár	k4xCyI
litrů	litr	k1gInPc2
alkoholu	alkohol	k1gInSc2
z	z	k7c2
odstaveného	odstavený	k2eAgInSc2d1
vagónu	vagón	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
však	však	k9
ohrozí	ohrozit	k5eAaPmIp3nS
životy	život	k1gInPc4
desítek	desítka	k1gFnPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
smrtelně	smrtelně	k6eAd1
jedovatý	jedovatý	k2eAgInSc4d1
metylalkohol	metylalkohol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
inspirován	inspirován	k2eAgInSc1d1
skutečnou	skutečný	k2eAgFnSc7d1
událostí	událost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autory	autor	k1gMnPc7
námětu	námět	k1gInSc2
a	a	k8xC
scénáře	scénář	k1gInSc2
filmu	film	k1gInSc2
jsou	být	k5eAaImIp3nP
Josef	Josef	k1gMnSc1
Bouček	Bouček	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Kratochvíl	Kratochvíl	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Pok	Pok	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Rychman	Rychman	k1gMnSc1
a	a	k8xC
Vladimír	Vladimír	k1gMnSc1
Valenta	Valenta	k1gMnSc1
<g/>
,	,	kIx,
filmové	filmový	k2eAgInPc1d1
dialogy	dialog	k1gInPc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
Jiří	Jiří	k1gMnSc1
Brdečka	Brdečka	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předlohou	předloha	k1gFnSc7
filmu	film	k1gInSc2
byla	být	k5eAaImAgFnS
rozhlasová	rozhlasový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
Demižon	demižon	k1gInSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
Miroslav	Miroslav	k1gMnSc1
Kratochvíl	Kratochvíl	k1gMnSc1
a	a	k8xC
Lubomír	Lubomír	k1gMnSc1
Pok	Pok	k1gMnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
nastudoval	nastudovat	k5eAaBmAgMnS
ČRo	ČRo	k1gFnSc4
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
v	v	k7c6
režii	režie	k1gFnSc6
Otakara	Otakar	k1gMnSc2
Bílka	Bílek	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nádražní	nádražní	k2eAgMnSc1d1
trafikant	trafikant	k1gMnSc1
Rezek	Rezek	k1gMnSc1
(	(	kIx(
<g/>
hraje	hrát	k5eAaImIp3nS
František	František	k1gMnSc1
Holar	Holar	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dříve	dříve	k6eAd2
vlastnil	vlastnit	k5eAaImAgInS
hospodu	hospodu	k?
<g/>
,	,	kIx,
si	se	k3xPyFc3
přivydělává	přivydělávat	k5eAaImIp3nS
drobnými	drobný	k2eAgInPc7d1
podvody	podvod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
pátek	pátek	k1gInSc4
odpoledne	odpoledne	k1gNnSc2
dostal	dostat	k5eAaPmAgMnS
od	od	k7c2
skladníka	skladník	k1gMnSc2
Kubíčka	Kubíček	k1gMnSc2
(	(	kIx(
<g/>
František	František	k1gMnSc1
Filipovský	Filipovský	k1gMnSc1
<g/>
)	)	kIx)
jeden	jeden	k4xCgInSc1
demižon	demižon	k1gInSc1
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
ukraden	ukrást	k5eAaPmNgInS
z	z	k7c2
odstaveného	odstavený	k2eAgInSc2d1
vagonu	vagon	k1gInSc2
na	na	k7c6
nádraží	nádraží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Večer	večer	k1gInSc1
v	v	k7c4
hospodě	hospodě	k?
levným	levný	k2eAgInSc7d1
alkoholem	alkohol	k1gInSc7
pohostí	pohostit	k5eAaPmIp3nS
Málka	Málka	k1gFnSc1
a	a	k8xC
Hatinu	Hatina	k1gFnSc4
a	a	k8xC
pohádá	pohádat	k5eAaPmIp3nS
se	se	k3xPyFc4
s	s	k7c7
manželkou	manželka	k1gFnSc7
(	(	kIx(
<g/>
Dana	Dana	k1gFnSc1
Medřická	Medřická	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c7
níž	jenž	k3xRgFnSc7
chodí	chodit	k5eAaImIp3nP
řidič	řidič	k1gInSc4
Jan	Jana	k1gFnPc2
Valena	valen	k2eAgFnSc1d1
(	(	kIx(
<g/>
Josef	Josef	k1gMnSc1
Bek	bek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Rezek	Rezek	k1gMnSc1
ví	vědět	k5eAaImIp3nS
a	a	k8xC
s	s	k7c7
Valenou	valený	k2eAgFnSc7d1
se	se	k3xPyFc4
porve	porvat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
sobotu	sobota	k1gFnSc4
ráno	ráno	k6eAd1
je	být	k5eAaImIp3nS
Rezek	Rezek	k1gMnSc1
nalezen	nalezen	k2eAgMnSc1d1
na	na	k7c6
kolejích	kolej	k1gFnPc6
těžce	těžce	k6eAd1
raněný	raněný	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
bezvědomí	bezvědomí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Valenu	valen	k2eAgFnSc4d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
však	však	k9
již	již	k6eAd1
brzy	brzy	k6eAd1
ráno	ráno	k6eAd1
odjel	odjet	k5eAaPmAgMnS
kamsi	kamsi	k6eAd1
vlakem	vlak	k1gInSc7
na	na	k7c4
svatbu	svatba	k1gFnSc4
kamaráda	kamarád	k1gMnSc4
<g/>
,	,	kIx,
padne	padnout	k5eAaImIp3nS,k5eAaPmIp3nS
podezření	podezření	k1gNnSc4
z	z	k7c2
pokusu	pokus	k1gInSc2
vraždy	vražda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezek	Rezek	k1gMnSc1
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
zemře	zemřít	k5eAaPmIp3nS
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc4
se	se	k3xPyFc4
ujmou	ujmout	k5eAaPmIp3nP
detektivové	detektiv	k1gMnPc1
(	(	kIx(
<g/>
pracovníci	pracovník	k1gMnPc1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
)	)	kIx)
Hájek	Hájek	k1gMnSc1
(	(	kIx(
<g/>
Rudolf	Rudolf	k1gMnSc1
Deyl	Deyl	k1gMnSc1
ml.	ml.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bláha	Bláha	k1gMnSc1
(	(	kIx(
<g/>
Zdeněk	Zdeněk	k1gMnSc1
Dítě	Dítě	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Drahota	drahota	k1gFnSc1
(	(	kIx(
<g/>
Lubor	Lubor	k1gMnSc1
Tokoš	Tokoš	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pitvě	pitva	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Rezek	Rezek	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
otravu	otrava	k1gFnSc4
metylalkoholem	metylalkohol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dne	den	k1gInSc2
zemře	zemřít	k5eAaPmIp3nS
i	i	k9
Kubíček	Kubíček	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
tento	tento	k3xDgInSc4
smrtelně	smrtelně	k6eAd1
jedovatý	jedovatý	k2eAgInSc4d1
alkohol	alkohol	k1gInSc4
ukradl	ukradnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
běžný	běžný	k2eAgInSc4d1
konzumní	konzumní	k2eAgInSc4d1
alkohol	alkohol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detektivové	detektiv	k1gMnPc1
pátrají	pátrat	k5eAaImIp3nP
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
všechno	všechen	k3xTgNnSc1
se	se	k3xPyFc4
methanolu	methanol	k1gInSc3
napil	napít	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
druhý	druhý	k4xOgInSc4
demižon	demižon	k1gInSc4
si	se	k3xPyFc3
odvezl	odvézt	k5eAaPmAgMnS
nic	nic	k3yNnSc1
netušící	tušící	k2eNgMnSc1d1
Valena	valit	k5eAaImNgNnP
jako	jako	k8xC,k8xS
dárek	dárek	k1gInSc1
na	na	k7c4
svatbu	svatba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
závod	závod	k1gInSc1
s	s	k7c7
časem	čas	k1gInSc7
a	a	k8xC
složité	složitý	k2eAgNnSc1d1
pátrání	pátrání	k1gNnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
vlastně	vlastně	k9
Valena	valen	k2eAgFnSc1d1
odjel	odjet	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zabránilo	zabránit	k5eAaPmAgNnS
hromadné	hromadný	k2eAgFnSc3d1
otravě	otrava	k1gFnSc3
svatebčanů	svatebčan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dramatickém	dramatický	k2eAgNnSc6d1
hledání	hledání	k1gNnSc6
místa	místo	k1gNnSc2
svatby	svatba	k1gFnSc2
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
i	i	k8xC
staršina	staršina	k1gMnSc1
Bělka	bělka	k1gFnSc1
(	(	kIx(
<g/>
Josef	Josef	k1gMnSc1
Kemr	Kemr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Osoby	osoba	k1gFnPc1
a	a	k8xC
obsazení	obsazení	k1gNnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Deyl	Deyl	k1gMnSc1
ml.	ml.	kA
</s>
<s>
Václav	Václav	k1gMnSc1
Hájek	Hájek	k1gMnSc1
<g/>
,	,	kIx,
poručík	poručík	k1gMnSc1
VBŽ	VBŽ	kA
<g/>
,	,	kIx,
detektiv	detektiv	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Dítě	Dítě	k1gMnSc1
</s>
<s>
Bláha	Bláha	k1gMnSc1
<g/>
,	,	kIx,
nadporučík	nadporučík	k1gMnSc1
VBŽ	VBŽ	kA
<g/>
,	,	kIx,
detektiv	detektiv	k1gMnSc1
</s>
<s>
Lubor	Lubor	k1gMnSc1
Tokoš	Tokoš	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Drahota	drahota	k1gFnSc1
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
VB	VB	kA
<g/>
,	,	kIx,
detektiv	detektiv	k1gMnSc1
</s>
<s>
Dana	Dana	k1gFnSc1
Medřická	Medřická	k1gFnSc1
</s>
<s>
Věra	Věra	k1gFnSc1
Rezková	Rezková	k1gFnSc1
<g/>
,	,	kIx,
hostinská	hostinská	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Bek	bek	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Valena	valen	k2eAgMnSc4d1
<g/>
,	,	kIx,
řidič	řidič	k1gMnSc1
ČSAD	ČSAD	kA
</s>
<s>
František	František	k1gMnSc1
Holar	Holar	k1gMnSc1
</s>
<s>
Rezek	Rezek	k1gMnSc1
<g/>
,	,	kIx,
nádražní	nádražní	k2eAgMnSc1d1
trafikant	trafikant	k1gMnSc1
<g/>
,	,	kIx,
Věřin	Věřin	k2eAgMnSc1d1
manžel	manžel	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Filipovský	Filipovský	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Kubíček	Kubíček	k1gMnSc1
<g/>
,	,	kIx,
skladník	skladník	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Kemr	Kemr	k1gMnSc1
</s>
<s>
Bělka	bělka	k1gFnSc1
<g/>
,	,	kIx,
staršina	staršina	k1gMnSc1
VB	VB	kA
</s>
<s>
Antonín	Antonín	k1gMnSc1
Šůra	Šůra	k1gMnSc1
</s>
<s>
trumpetista	trumpetista	k1gMnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Koulová	Koulová	k1gFnSc1
</s>
<s>
Hájkova	Hájkův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
</s>
<s>
Hanuš	Hanuš	k1gMnSc1
Bor	bor	k1gInSc1
</s>
<s>
Honzík	Honzík	k1gMnSc1
<g/>
,	,	kIx,
Valenův	Valenův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Lukeš	Lukeš	k1gMnSc1
</s>
<s>
Jílek	Jílek	k1gMnSc1
<g/>
,	,	kIx,
posunovač	posunovač	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Vydra	Vydra	k1gMnSc1
ml.	ml.	kA
</s>
<s>
MUDr.	MUDr.	kA
Ondrášek	Ondrášek	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Kutil	kutil	k1gMnSc1
</s>
<s>
opilec	opilec	k1gMnSc1
Málek	Málek	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Beyvl	Beyvl	k1gMnSc1
</s>
<s>
opilec	opilec	k1gMnSc1
Hatina	Hatina	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Effa	Effa	k1gMnSc1
</s>
<s>
mladík	mladík	k1gMnSc1
ve	v	k7c6
vlaku	vlak	k1gInSc6
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Fišer	Fišer	k1gMnSc1
</s>
<s>
mladík	mladík	k1gMnSc1
ve	v	k7c6
vlaku	vlak	k1gInSc6
</s>
<s>
Josef	Josef	k1gMnSc1
Hlinomaz	hlinomaz	k1gMnSc1
</s>
<s>
řidič	řidič	k1gMnSc1
Véna	Véna	k1gMnSc1
</s>
<s>
Otakar	Otakar	k1gMnSc1
Brousek	brousek	k1gInSc4
st.	st.	kA
</s>
<s>
major	major	k1gMnSc1
Vojenské	vojenský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
Táboře	Tábor	k1gInSc6
</s>
<s>
Ivanka	Ivanka	k1gFnSc1
Devátá	devátý	k4xOgFnSc1
</s>
<s>
sekretářka	sekretářka	k1gFnSc1
Vojenské	vojenský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
Táboře	Tábor	k1gInSc6
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
</s>
<s>
starý	starý	k2eAgMnSc1d1
družba	družba	k1gMnSc1
<g/>
,	,	kIx,
svatebčan	svatebčan	k1gMnSc1
v	v	k7c6
autě	auto	k1gNnSc6
</s>
<s>
Václav	Václav	k1gMnSc1
Fišer	Fišer	k1gMnSc1
</s>
<s>
strojvůdce	strojvůdce	k1gMnSc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Hoblík	hoblík	k1gInSc4
</s>
<s>
primář	primář	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Koza	koza	k1gFnSc1
</s>
<s>
úředník	úředník	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Kozák	Kozák	k1gMnSc1
</s>
<s>
svatebčan	svatebčan	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Moučka	Moučka	k1gMnSc1
</s>
<s>
ženich	ženich	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Štíbr	Štíbr	k1gMnSc1
</s>
<s>
Déda	Déda	k1gMnSc1
Papež	Papež	k1gMnSc1
</s>
<s>
předseda	předseda	k1gMnSc1
MNV	MNV	kA
</s>
<s>
Oleg	Oleg	k1gMnSc1
Reif	Reif	k1gMnSc1
</s>
<s>
harmonikář	harmonikář	k1gMnSc1
Franta	Franta	k1gMnSc1
</s>
<s>
Monika	Monika	k1gFnSc1
Soyková	Soyková	k1gFnSc1
</s>
<s>
Cilka	Cilka	k1gFnSc1
dítě	dítě	k1gNnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
Hájkových	Hájkových	k2eAgFnSc2d1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Spálený	spálený	k2eAgMnSc1d1
</s>
<s>
lékař	lékař	k1gMnSc1
</s>
<s>
Rima	Rim	k2eAgFnSc1d1
Šorochová	Šorochová	k1gFnSc1
</s>
<s>
nevěsta	nevěsta	k1gFnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Švec	Švec	k1gMnSc1
</s>
<s>
bubeník	bubeník	k1gMnSc1
</s>
<s>
Hermína	Hermína	k1gFnSc1
Vojtová	Vojtová	k1gFnSc1
</s>
<s>
bytná	bytná	k1gFnSc1
</s>
<s>
Miloš	Miloš	k1gMnSc1
Willig	Willig	k1gMnSc1
</s>
<s>
klarinetista	klarinetista	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Bezděk	Bezděk	k1gMnSc1
</s>
<s>
železničář	železničář	k1gMnSc1
Slávek	Slávek	k1gMnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
Šimánek	Šimánek	k1gMnSc1
</s>
<s>
hlas	hlas	k1gInSc1
železničáře	železničář	k1gMnSc2
Slávka	Slávek	k1gMnSc2
(	(	kIx(
<g/>
mluví	mluvit	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Bílá	bílý	k2eAgFnSc1d1
</s>
<s>
zdravotní	zdravotní	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
</s>
<s>
Alois	Alois	k1gMnSc1
Dvorský	Dvorský	k1gMnSc1
</s>
<s>
dědeček	dědeček	k1gMnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Gzela	Gzela	k1gMnSc1
</s>
<s>
host	host	k1gMnSc1
v	v	k7c6
hospodě	hospodě	k?
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
Skládal	skládat	k5eAaImAgMnS
</s>
<s>
náčelník	náčelník	k1gMnSc1
stanice	stanice	k1gFnSc2
</s>
<s>
Václav	Václav	k1gMnSc1
Voska	Voska	k1gMnSc1
</s>
<s>
komentář	komentář	k1gInSc1
(	(	kIx(
<g/>
mluví	mluvit	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Pavlík	Pavlík	k1gMnSc1
</s>
<s>
lékař	lékař	k1gMnSc1
</s>
<s>
Mirko	Mirko	k1gMnSc1
Musil	Musil	k1gMnSc1
</s>
<s>
pokladník	pokladník	k1gMnSc1
na	na	k7c6
železnici	železnice	k1gFnSc6
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Narenta	Narento	k1gNnSc2
</s>
<s>
staršina	staršina	k1gMnSc1
VB	VB	kA
</s>
<s>
Miloš	Miloš	k1gMnSc1
Vavruška	Vavruška	k1gMnSc1
</s>
<s>
laborant	laborant	k1gMnSc1
Vojta	Vojta	k1gMnSc1
</s>
<s>
Blažena	blažen	k2eAgFnSc1d1
Kramešová	Kramešová	k1gFnSc1
</s>
<s>
laborantka	laborantka	k1gFnSc1
</s>
<s>
Zdena	Zdena	k1gFnSc1
Hadrbolcová	Hadrbolcová	k1gFnSc1
</s>
<s>
nevěsta	nevěsta	k1gFnSc1
</s>
<s>
Darek	Darek	k6eAd1
Vostřel	Vostřel	k1gInSc1
</s>
<s>
ženich	ženich	k1gMnSc1
u	u	k7c2
fotografa	fotograf	k1gMnSc2
</s>
<s>
Gabriela	Gabriela	k1gFnSc1
Bártlová	Bártlová	k1gFnSc1
</s>
<s>
cestující	cestující	k1gMnPc1
ve	v	k7c6
vlaku	vlak	k1gInSc6
</s>
<s>
Josef	Josef	k1gMnSc1
Příhoda	Příhoda	k1gMnSc1
</s>
<s>
svatebčan	svatebčan	k1gMnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Nademlejnská	Nademlejnská	k1gFnSc1
</s>
<s>
žena	žena	k1gFnSc1
na	na	k7c6
svatbě	svatba	k1gFnSc6
</s>
<s>
Milka	Milka	k1gFnSc1
Balek-Brodská	Balek-Brodský	k2eAgFnSc1d1
</s>
<s>
Kubíčkova	Kubíčkův	k2eAgFnSc1d1
bytná	bytná	k1gFnSc1
</s>
<s>
Místa	místo	k1gNnPc1
natáčení	natáčení	k1gNnSc2
</s>
<s>
ateliéry	ateliér	k1gInPc1
</s>
<s>
Barrandov	Barrandov	k1gInSc1
<g/>
,	,	kIx,
Hostivař	Hostivař	k1gFnSc1
</s>
<s>
exteriéry	exteriér	k1gInPc1
</s>
<s>
Praha-Bohnice	Praha-Bohnice	k1gFnSc1
<g/>
,	,	kIx,
Točná	točný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Běchovice	Běchovice	k1gFnPc1
</s>
<s>
Praha-Košíře	Praha-Košíř	k1gInPc1
<g/>
:	:	kIx,
ulice	ulice	k1gFnPc1
Na	na	k7c6
Šmukýřce	šmukýřka	k1gFnSc6
<g/>
,	,	kIx,
V	v	k7c6
Cibulkách	cibulka	k1gFnPc6
</s>
<s>
vlakové	vlakový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
Tomice	Tomice	k1gFnSc2
II	II	kA
</s>
<s>
okolí	okolí	k1gNnSc1
Tábora	Tábor	k1gInSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Doporučujeme	doporučovat	k5eAaImIp1nP
filmy	film	k1gInPc1
na	na	k7c4
týden	týden	k1gInSc4
od	od	k7c2
1.6	1.6	k4
<g/>
.2009	.2009	k4
:	:	kIx,
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CS	CS	kA
Film	film	k1gInSc1
<g/>
,	,	kIx,
2009-06-01	2009-06-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Demižon	demižon	k1gInSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panáček	panáček	k1gInSc1
v	v	k7c6
říši	říš	k1gFnSc6
mluveného	mluvený	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
2007-04-20	2007-04-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filmová	filmový	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
(	(	kIx(
<g/>
FDb	FDb	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Methanol	methanol	k1gInSc1
</s>
<s>
Metanol	metanol	k1gInSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Otrava	otrava	k1gFnSc1
methanolem	methanol	k1gInSc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
ve	v	k7c6
Filmovém	filmový	k2eAgInSc6d1
přehledu	přehled	k1gInSc6
</s>
<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
v	v	k7c6
katalogu	katalog	k1gInSc6
NFA	NFA	kA
Český	český	k2eAgInSc1d1
hraný	hraný	k2eAgInSc1d1
film	film	k1gInSc1
1898	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
sobota	sobota	k1gFnSc1
–	–	k?
film	film	k1gInSc4
na	na	k7c6
webu	web	k1gInSc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
