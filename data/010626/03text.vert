<p>
<s>
Roberto	Roberta	k1gFnSc5	Roberta
"	"	kIx"	"
<g/>
Bobby	Bobba	k1gFnSc2	Bobba
<g/>
"	"	kIx"	"
Alfonso	Alfonso	k1gMnSc1	Alfonso
Farrell	Farrell	k1gMnSc1	Farrell
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
Aruba	Aruba	k1gFnSc1	Aruba
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
mužský	mužský	k2eAgInSc1d1	mužský
člen	člen	k1gInSc1	člen
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
popové	popový	k2eAgFnSc2d1	popová
kapely	kapela	k1gFnSc2	kapela
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Boney	Bonea	k1gFnSc2	Bonea
M	M	kA	M
.	.	kIx.	.
</s>
<s>
Arubu	Aruba	k1gFnSc4	Aruba
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
námořníkem	námořník	k1gMnSc7	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
pracoval	pracovat	k5eAaImAgMnS	pracovat
nejčastěji	často	k6eAd3	často
jako	jako	k9	jako
DJ	DJ	kA	DJ
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
producent	producent	k1gMnSc1	producent
Frank	Frank	k1gMnSc1	Frank
Farian	Farian	k1gMnSc1	Farian
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zrovna	zrovna	k6eAd1	zrovna
zakládal	zakládat	k5eAaImAgInS	zakládat
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
Boney	Bonea	k1gFnSc2	Bonea
M.	M.	kA	M.
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
sólovým	sólový	k2eAgMnSc7d1	sólový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Farian	Farian	k1gMnSc1	Farian
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bobby	Bobb	k1gMnPc7	Bobb
většinu	většina	k1gFnSc4	většina
písní	píseň	k1gFnPc2	píseň
nenazpíval	nazpívat	k5eNaPmAgMnS	nazpívat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Farian	Farian	k1gMnSc1	Farian
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
mužské	mužský	k2eAgFnSc2d1	mužská
party	parta	k1gFnSc2	parta
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Liz	liz	k1gInSc1	liz
Mitchellová	Mitchellová	k1gFnSc1	Mitchellová
také	také	k9	také
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
ona	onen	k3xDgFnSc1	onen
a	a	k8xC	a
Farian	Fariana	k1gFnPc2	Fariana
zpívali	zpívat	k5eAaImAgMnP	zpívat
na	na	k7c6	na
nahrávkách	nahrávka	k1gFnPc6	nahrávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
jako	jako	k9	jako
tanečník	tanečník	k1gMnSc1	tanečník
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
písně	píseň	k1gFnPc4	píseň
Turn	Turna	k1gFnPc2	Turna
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Music	Musice	k1gInPc2	Musice
od	od	k7c2	od
Rogera	Rogero	k1gNnSc2	Rogero
Sancheze	Sancheze	k1gFnSc2	Sancheze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Amstelveen	Amstelvena	k1gFnPc2	Amstelvena
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ráno	ráno	k6eAd1	ráno
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k1gInSc1	večer
předtím	předtím	k6eAd1	předtím
měl	mít	k5eAaImAgInS	mít
vystoupení	vystoupení	k1gNnSc4	vystoupení
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
smrti	smrt	k1gFnSc2	smrt
měl	mít	k5eAaImAgMnS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
pitvy	pitva	k1gFnSc2	pitva
selhání	selhání	k1gNnSc1	selhání
srdce	srdce	k1gNnSc2	srdce
po	po	k7c6	po
dlohoudobějších	dlohoudobý	k2eAgFnPc6d2	dlohoudobý
zdravotních	zdravotní	k2eAgFnPc6d1	zdravotní
obtížích	obtíž	k1gFnPc6	obtíž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Polizei	Polizei	k1gNnSc1	Polizei
/	/	kIx~	/
A	a	k9	a
Fool	Fool	k1gMnSc1	Fool
In	In	k1gMnSc1	In
Love	lov	k1gInSc5	lov
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
King	King	k1gInSc1	King
OF	OF	kA	OF
Dancing	dancing	k1gInSc1	dancing
/	/	kIx~	/
I	i	k9	i
See	See	k1gMnSc1	See
You	You	k1gMnSc1	You
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Hoppa	Hoppa	k1gFnSc1	Hoppa
Hoppa	Hoppa	k1gFnSc1	Hoppa
/	/	kIx~	/
Hoppa	Hoppa	k1gFnSc1	Hoppa
Hoppa	Hoppa	k1gFnSc1	Hoppa
(	(	kIx(	(
<g/>
Instrumental	Instrumental	k1gMnSc1	Instrumental
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Tribute	tribut	k1gInSc5	tribut
To	ten	k3xDgNnSc1	ten
Josephine	Josephin	k1gInSc5	Josephin
Baker	Baker	k1gMnSc1	Baker
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Aruban	Aruban	k1gInSc1	Aruban
Style	styl	k1gInSc5	styl
(	(	kIx(	(
<g/>
Mixes	Mixes	k1gInSc1	Mixes
<g/>
)	)	kIx)	)
S-Cream	S-Cream	k1gInSc1	S-Cream
Featuring	Featuring	k1gInSc1	Featuring
Bobby	Bobba	k1gFnSc2	Bobba
Farrell	Farrella	k1gFnPc2	Farrella
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
The	The	k1gMnSc1	The
Bump	Bump	k1gMnSc1	Bump
EP	EP	kA	EP
</s>
</p>
<p>
<s>
===	===	k?	===
Bobby	Bobb	k1gInPc1	Bobb
Farrell	Farrellum	k1gNnPc2	Farrellum
u	u	k7c2	u
Boney	Bonea	k1gFnSc2	Bonea
M.	M.	kA	M.
/	/	kIx~	/
Boney	Bonea	k1gFnSc2	Bonea
M.	M.	kA	M.
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bobby	Bobb	k1gMnPc7	Bobb
Farrellem	Farrell	k1gMnSc7	Farrell
/	/	kIx~	/
Bobby	Bobba	k1gMnSc2	Bobba
Farrell	Farrell	k1gInSc1	Farrell
se	s	k7c7	s
Sandy	Sand	k1gMnPc7	Sand
Chambersovou	Chambersová	k1gFnSc4	Chambersová
===	===	k?	===
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
Boney	Bonea	k1gFnSc2	Bonea
M.	M.	kA	M.
(	(	kIx(	(
<g/>
DVMore	DVMor	k1gMnSc5	DVMor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Boney	Bonea	k1gMnSc2	Bonea
M.	M.	kA	M.
–	–	k?	–
I	i	k9	i
Successi	Successe	k1gFnSc4	Successe
(	(	kIx(	(
<g/>
DVMore	DVMor	k1gMnSc5	DVMor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
Boney	Bonea	k1gFnSc2	Bonea
M.	M.	kA	M.
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
compilation	compilation	k1gInSc1	compilation
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
Boney	Bonea	k1gFnSc2	Bonea
M.	M.	kA	M.
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
compilation	compilation	k1gInSc1	compilation
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Boney	Bonea	k1gMnSc2	Bonea
M.	M.	kA	M.
–	–	k?	–
Remix	Remix	k1gInSc1	Remix
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Sandy	Sanda	k1gFnPc1	Sanda
Chambersovou	Chambersová	k1gFnSc7	Chambersová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
compilation	compilation	k1gInSc1	compilation
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Crisler	Crisler	k1gInSc1	Crisler
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Boney	Bonea	k1gMnSc2	Bonea
M.	M.	kA	M.
–	–	k?	–
Disco	disco	k1gNnPc2	disco
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
compilation	compilation	k1gInSc1	compilation
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bobby	Bobba	k1gFnSc2	Bobba
Farrell	Farrella	k1gFnPc2	Farrella
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
iDnes	iDnes	k1gInSc4	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
klip	klip	k1gInSc1	klip
Turn	Turna	k1gFnPc2	Turna
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Music	Music	k1gMnSc1	Music
-	-	kIx~	-
Roger	Roger	k1gMnSc1	Roger
Sanchez	Sanchez	k1gMnSc1	Sanchez
</s>
</p>
<p>
<s>
klip	klip	k1gInSc1	klip
Daddy	Dadda	k1gFnSc2	Dadda
Cool	Coola	k1gFnPc2	Coola
-	-	kIx~	-
Boney	Bonea	k1gFnSc2	Bonea
M	M	kA	M
</s>
</p>
