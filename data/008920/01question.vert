<s>
Kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
toskánský	toskánský	k2eAgMnSc1d1	toskánský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
těsně	těsně	k6eAd1	těsně
spjatý	spjatý	k2eAgMnSc1d1	spjatý
s	s	k7c7	s
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
?	?	kIx.	?
</s>
