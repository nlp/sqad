<p>
<s>
Ideal	Ideal	k1gInSc1	Ideal
Home	Hom	k1gInSc2	Hom
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
režíroval	režírovat	k5eAaImAgInS	režírovat
Andrew	Andrew	k1gFnSc4	Andrew
Fleming	Fleming	k1gInSc1	Fleming
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgInSc2d1	vlastní
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
gay	gay	k1gMnSc1	gay
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
kteý	kteý	k?	kteý
je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
postarat	postarat	k5eAaPmF	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
nezletilé	nezletilé	k2eAgNnSc4d1	nezletilé
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
měl	mít	k5eAaImAgInS	mít
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
Mardi	Mard	k1gMnPc1	Mard
Gras	Grasa	k1gFnPc2	Grasa
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
spolu	spolu	k6eAd1	spolu
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c4	v
Santa	Sant	k1gMnSc4	Sant
Fe	Fe	k1gMnSc4	Fe
<g/>
.	.	kIx.	.
</s>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
je	být	k5eAaImIp3nS	být
hvězdou	hvězda	k1gFnSc7	hvězda
místní	místní	k2eAgFnSc1d1	místní
televizní	televizní	k2eAgFnSc1d1	televizní
show	show	k1gFnSc1	show
o	o	k7c6	o
vaření	vaření	k1gNnSc6	vaření
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgMnSc1d1	televizní
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
objeví	objevit	k5eAaPmIp3nS	objevit
desetiletý	desetiletý	k2eAgMnSc1d1	desetiletý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Erasmovým	Erasmův	k2eAgMnSc7d1	Erasmův
vnukem	vnuk	k1gMnSc7	vnuk
<g/>
.	.	kIx.	.
</s>
<s>
Erasmův	Erasmův	k2eAgMnSc1d1	Erasmův
syn	syn	k1gMnSc1	syn
Beau	Beaus	k1gInSc2	Beaus
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
jen	jen	k6eAd1	jen
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
starat	starat	k5eAaImF	starat
o	o	k7c4	o
vlastního	vlastní	k2eAgMnSc4d1	vlastní
syna	syn	k1gMnSc4	syn
Angela	angel	k1gMnSc4	angel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
přítomnosti	přítomnost	k1gFnSc2	přítomnost
dítěte	dítě	k1gNnSc2	dítě
vůbec	vůbec	k9	vůbec
nadšený	nadšený	k2eAgInSc4d1	nadšený
<g/>
,	,	kIx,	,
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angel	angel	k1gMnSc1	angel
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
nekomunikativní	komunikativní	k2eNgNnSc1d1	nekomunikativní
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
posléze	posléze	k6eAd1	posléze
k	k	k7c3	k
sobě	se	k3xPyFc3	se
najdou	najít	k5eAaPmIp3nP	najít
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
Angel	Angela	k1gFnPc2	Angela
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
vrátí	vrátit	k5eAaPmIp3nS	vrátit
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
