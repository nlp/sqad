<s>
Sklízené	sklízený	k2eAgFnPc1d1	sklízená
jahody	jahoda	k1gFnPc1	jahoda
mají	mít	k5eAaImIp3nP	mít
svěže	svěže	k6eAd1	svěže
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
dozrálé	dozrálý	k2eAgFnPc1d1	dozrálá
<g/>
.	.	kIx.	.
</s>
