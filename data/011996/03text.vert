<p>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
lidmi	člověk	k1gMnPc7	člověk
upravený	upravený	k2eAgInSc4d1	upravený
pozemek	pozemek	k1gInSc4	pozemek
s	s	k7c7	s
uměle	uměle	k6eAd1	uměle
vysázenou	vysázený	k2eAgFnSc7d1	vysázená
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
nebo	nebo	k8xC	nebo
umístěny	umístěn	k2eAgFnPc1d1	umístěna
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
,	,	kIx,	,
volnočasovým	volnočasův	k2eAgFnPc3d1	volnočasův
aktivitám	aktivita	k1gFnPc3	aktivita
a	a	k8xC	a
reprezentaci	reprezentace	k1gFnSc3	reprezentace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
určených	určený	k2eAgFnPc2d1	určená
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
nebo	nebo	k8xC	nebo
i	i	k9	i
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Vědeckým	vědecký	k2eAgInSc7d1	vědecký
<g/>
,	,	kIx,	,
výzkumným	výzkumný	k2eAgInSc7d1	výzkumný
a	a	k8xC	a
pedagogickým	pedagogický	k2eAgInPc3d1	pedagogický
účelům	účel	k1gInPc3	účel
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nP	sloužit
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
arboreta	arboretum	k1gNnPc4	arboretum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
sbírky	sbírka	k1gFnPc4	sbírka
často	často	k6eAd1	často
také	také	k9	také
nějak	nějak	k6eAd1	nějak
architektonicky	architektonicky	k6eAd1	architektonicky
upraveny	upravit	k5eAaPmNgFnP	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
slovník	slovník	k1gInSc1	slovník
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
zahradu	zahrada	k1gFnSc4	zahrada
jako	jako	k8xC	jako
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
úplným	úplný	k2eAgMnSc7d1	úplný
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
omezeným	omezený	k2eAgInSc7d1	omezený
přístupem	přístup	k1gInSc7	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
<g/>
Zahradu	zahrada	k1gFnSc4	zahrada
zakládá	zakládat	k5eAaImIp3nS	zakládat
majitel	majitel	k1gMnSc1	majitel
<g/>
,	,	kIx,	,
zahradní	zahradní	k2eAgMnSc1d1	zahradní
architekt	architekt	k1gMnSc1	architekt
nebo	nebo	k8xC	nebo
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
majitel	majitel	k1gMnSc1	majitel
<g/>
,	,	kIx,	,
uživatel	uživatel	k1gMnSc1	uživatel
nebo	nebo	k8xC	nebo
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doporučováno	doporučován	k2eAgNnSc1d1	doporučováno
a	a	k8xC	a
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zahradu	zahrada	k1gFnSc4	zahrada
udržoval	udržovat	k5eAaImAgMnS	udržovat
stále	stále	k6eAd1	stále
týž	týž	k3xTgMnSc1	týž
zahradník	zahradník	k1gMnSc1	zahradník
nebo	nebo	k8xC	nebo
táž	týž	k3xTgFnSc1	týž
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
sadovnické	sadovnický	k2eAgFnSc2d1	Sadovnická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
znovuotevření	znovuotevření	k1gNnSc2	znovuotevření
Květné	květný	k2eAgFnSc2d1	Květná
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
ředitelka	ředitelka	k1gFnSc1	ředitelka
NPÚ	NPÚ	kA	NPÚ
Naďa	Naďa	k1gFnSc1	Naďa
Goryczková	Goryczkový	k2eAgFnSc1d1	Goryczková
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
stále	stále	k6eAd1	stále
opomíjena	opomíjet	k5eAaImNgFnS	opomíjet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
zahrada	zahrada	k1gFnSc1	zahrada
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
ohrazený	ohrazený	k2eAgInSc4d1	ohrazený
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc4d1	malá
ohradu	ohrada	k1gFnSc4	ohrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
zahrad	zahrada	k1gFnPc2	zahrada
založených	založený	k2eAgFnPc2d1	založená
pro	pro	k7c4	pro
specifické	specifický	k2eAgInPc4d1	specifický
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
;	;	kIx,	;
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pro	pro	k7c4	pro
ně	on	k3xPp3gNnPc4	on
jednoslovná	jednoslovný	k2eAgNnPc4d1	jednoslovné
pojmenování	pojmenování	k1gNnPc4	pojmenování
nebo	nebo	k8xC	nebo
bývá	bývat	k5eAaImIp3nS	bývat
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
jménu	jméno	k1gNnSc3	jméno
připojeno	připojen	k2eAgNnSc4d1	připojeno
přídavné	přídavný	k2eAgNnSc4d1	přídavné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
typ	typ	k1gInSc4	typ
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zahrada	zahrada	k1gFnSc1	zahrada
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
<g/>
,	,	kIx,	,
bylinková	bylinkový	k2eAgFnSc1d1	bylinková
či	či	k8xC	či
okrasná	okrasný	k2eAgFnSc1d1	okrasná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Park	park	k1gInSc1	park
a	a	k8xC	a
zahrada	zahrada	k1gFnSc1	zahrada
jsou	být	k5eAaImIp3nP	být
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
za	za	k7c4	za
synonymum	synonymum	k1gNnSc4	synonymum
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
zahrada	zahrada	k1gFnSc1	zahrada
považováno	považován	k2eAgNnSc1d1	považováno
slovo	slovo	k1gNnSc4	slovo
sad	sada	k1gFnPc2	sada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
u	u	k7c2	u
obytné	obytný	k2eAgFnSc2d1	obytná
budovy	budova	k1gFnSc2	budova
před	před	k7c7	před
stavbou	stavba	k1gFnSc7	stavba
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
na	na	k7c4	na
pozemek	pozemek	k1gInSc4	pozemek
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
předzahrádka	předzahrádka	k1gFnSc1	předzahrádka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
taková	takový	k3xDgFnSc1	takový
velká	velká	k1gFnSc1	velká
intenzivně	intenzivně	k6eAd1	intenzivně
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
používáno	používán	k2eAgNnSc1d1	používáno
i	i	k9	i
zde	zde	k6eAd1	zde
slovo	slovo	k1gNnSc4	slovo
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
bývala	bývat	k5eAaImAgFnS	bývat
zahrada	zahrada	k1gFnSc1	zahrada
rovněž	rovněž	k9	rovněž
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Móda	móda	k1gFnSc1	móda
japonských	japonský	k2eAgFnPc2d1	japonská
zahrad	zahrada	k1gFnPc2	zahrada
tuto	tento	k3xDgFnSc4	tento
představu	představa	k1gFnSc4	představa
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
podobách	podoba	k1gFnPc6	podoba
a	a	k8xC	a
šlo	jít	k5eAaImAgNnS	jít
původně	původně	k6eAd1	původně
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
nejintenzivněji	intenzivně	k6eAd3	intenzivně
obdělávanou	obdělávaný	k2eAgFnSc4d1	obdělávaná
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
pozemku	pozemek	k1gInSc2	pozemek
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
obytné	obytný	k2eAgFnSc2d1	obytná
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
vzniká	vznikat	k5eAaImIp3nS	vznikat
zahrada	zahrada	k1gFnSc1	zahrada
i	i	k9	i
u	u	k7c2	u
staveb	stavba	k1gFnPc2	stavba
velmožů	velmož	k1gMnPc2	velmož
a	a	k8xC	a
duchovních	duchovní	k2eAgMnPc2d1	duchovní
i	i	k8xC	i
světských	světský	k2eAgMnPc2d1	světský
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejméně	málo	k6eAd3	málo
obdělávanou	obdělávaný	k2eAgFnSc7d1	obdělávaná
částí	část	k1gFnSc7	část
pozemku	pozemek	k1gInSc2	pozemek
byla	být	k5eAaImAgFnS	být
obora	obora	k1gFnSc1	obora
se	s	k7c7	s
zvěří	zvěř	k1gFnSc7	zvěř
a	a	k8xC	a
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
úpravy	úprava	k1gFnPc1	úprava
jsou	být	k5eAaImIp3nP	být
pokládány	pokládat	k5eAaImNgFnP	pokládat
za	za	k7c4	za
základ	základ	k1gInSc4	základ
parku	park	k1gInSc2	park
a	a	k8xC	a
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
pozemků	pozemek	k1gInPc2	pozemek
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
park	park	k1gInSc1	park
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
intenzitě	intenzita	k1gFnSc6	intenzita
údržby	údržba	k1gFnSc2	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
barokní	barokní	k2eAgInPc1d1	barokní
parky	park	k1gInPc1	park
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
dodnes	dodnes	k6eAd1	dodnes
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
dobových	dobový	k2eAgFnPc2d1	dobová
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
označit	označit	k5eAaPmF	označit
jako	jako	k8xC	jako
zahrady	zahrada	k1gFnSc2	zahrada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Květná	květný	k2eAgFnSc1d1	Květná
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
úpravy	úprava	k1gFnPc4	úprava
stejné	stejný	k2eAgFnSc2d1	stejná
plochy	plocha	k1gFnSc2	plocha
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
anglického	anglický	k2eAgInSc2d1	anglický
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k8xC	jako
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozemky	pozemek	k1gInPc1	pozemek
byly	být	k5eAaImAgInP	být
ohrazeny	ohradit	k5eAaPmNgInP	ohradit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
za	za	k7c2	za
zahrady	zahrada	k1gFnSc2	zahrada
pokládali	pokládat	k5eAaImAgMnP	pokládat
udržované	udržovaný	k2eAgFnPc4d1	udržovaná
ohrazené	ohrazený	k2eAgFnPc4d1	ohrazená
plochy	plocha	k1gFnPc4	plocha
bez	bez	k7c2	bez
ovocných	ovocný	k2eAgFnPc2d1	ovocná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
plochy	plocha	k1gFnPc1	plocha
s	s	k7c7	s
ovocnými	ovocný	k2eAgFnPc7d1	ovocná
dřevinami	dřevina	k1gFnPc7	dřevina
(	(	kIx(	(
<g/>
stromy	strom	k1gInPc7	strom
<g/>
)	)	kIx)	)
vně	vně	k6eAd1	vně
hlavní	hlavní	k2eAgFnPc1d1	hlavní
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
sad	sad	k1gInSc1	sad
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
mimo	mimo	k7c4	mimo
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
celek	celek	k1gInSc4	celek
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
charakteristiku	charakteristika	k1gFnSc4	charakteristika
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc4	rozdělení
zahrad	zahrada	k1gFnPc2	zahrada
podle	podle	k7c2	podle
použití	použití	k1gNnSc2	použití
a	a	k8xC	a
účelu	účel	k1gInSc2	účel
na	na	k7c4	na
dvory	dvůr	k1gInPc4	dvůr
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
například	například	k6eAd1	například
i	i	k9	i
u	u	k7c2	u
korejských	korejský	k2eAgFnPc2d1	Korejská
zahrad	zahrada	k1gFnPc2	zahrada
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
<g/>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
dalších	další	k2eAgFnPc6d1	další
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
byly	být	k5eAaImAgFnP	být
už	už	k6eAd1	už
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
některé	některý	k3yIgFnSc2	některý
součásti	součást	k1gFnSc2	součást
parků	park	k1gInPc2	park
označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k8xS	jako
zahrady	zahrada	k1gFnPc1	zahrada
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
jich	on	k3xPp3gMnPc2	on
být	být	k5eAaImF	být
v	v	k7c6	v
parku	park	k1gInSc6	park
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Parky	park	k1gInPc1	park
a	a	k8xC	a
zahrady	zahrada	k1gFnSc2	zahrada
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
rozlišit	rozlišit	k5eAaPmF	rozlišit
právě	právě	k6eAd1	právě
způsobem	způsob	k1gInSc7	způsob
výsadby	výsadba	k1gFnSc2	výsadba
a	a	k8xC	a
intenzitou	intenzita	k1gFnSc7	intenzita
údržby	údržba	k1gFnSc2	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
slovník	slovník	k1gInSc1	slovník
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
slovo	slovo	k1gNnSc1	slovo
zahrada	zahrada	k1gFnSc1	zahrada
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
uzavřené	uzavřený	k2eAgNnSc4d1	uzavřené
víceméně	víceméně	k9	víceméně
soukromě	soukromě	k6eAd1	soukromě
plochy	plocha	k1gFnPc4	plocha
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
park	park	k1gInSc4	park
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
krásném	krásný	k2eAgNnSc6d1	krásné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
veřejné	veřejný	k2eAgInPc4d1	veřejný
parky	park	k1gInPc4	park
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgNnPc4d1	otevřené
veřejná	veřejný	k2eAgNnPc4d1	veřejné
prostranství	prostranství	k1gNnPc4	prostranství
<g/>
.	.	kIx.	.
</s>
<s>
Realita	realita	k1gFnSc1	realita
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
intenzitu	intenzita	k1gFnSc4	intenzita
údržby	údržba	k1gFnSc2	údržba
či	či	k8xC	či
styl	styl	k1gInSc4	styl
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
pozemků	pozemek	k1gInPc2	pozemek
označovaných	označovaný	k2eAgInPc2d1	označovaný
jako	jako	k8xC	jako
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
veřejných	veřejný	k2eAgInPc2d1	veřejný
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
používány	používán	k2eAgInPc1d1	používán
běžně	běžně	k6eAd1	běžně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soukromých	soukromý	k2eAgInPc2d1	soukromý
pozemků	pozemek	k1gInPc2	pozemek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
obdělávané	obdělávaný	k2eAgFnPc4d1	obdělávaná
<g/>
,	,	kIx,	,
oplocené	oplocený	k2eAgFnPc4d1	oplocená
nebo	nebo	k8xC	nebo
neoplocené	oplocený	k2eNgFnPc4d1	neoplocená
zelené	zelený	k2eAgFnPc4d1	zelená
plochy	plocha	k1gFnPc4	plocha
u	u	k7c2	u
obytné	obytný	k2eAgFnSc2d1	obytná
budovy	budova	k1gFnSc2	budova
obvykle	obvykle	k6eAd1	obvykle
správný	správný	k2eAgInSc1d1	správný
název	název	k1gInSc1	název
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
u	u	k7c2	u
slova	slovo	k1gNnSc2	slovo
zahrada	zahrada	k1gFnSc1	zahrada
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
jeho	on	k3xPp3gInSc4	on
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
zimní	zimní	k2eAgFnSc1d1	zimní
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
prosklenou	prosklený	k2eAgFnSc4d1	prosklená
obytnou	obytný	k2eAgFnSc4d1	obytná
místnost	místnost	k1gFnSc4	místnost
nebo	nebo	k8xC	nebo
i	i	k9	i
zasklený	zasklený	k2eAgInSc4d1	zasklený
balkon	balkon	k1gInSc4	balkon
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
pěstováním	pěstování	k1gNnSc7	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
označuje	označovat	k5eAaImIp3nS	označovat
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
ukázky	ukázka	k1gFnPc1	ukázka
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nejsou	být	k5eNaImIp3nP	být
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
a	a	k8xC	a
podstatné	podstatný	k2eAgFnPc1d1	podstatná
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
přirozené	přirozený	k2eAgNnSc4d1	přirozené
prostředí	prostředí	k1gNnSc4	prostředí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
takto	takto	k6eAd1	takto
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
k	k	k7c3	k
edukativním	edukativní	k2eAgInPc3d1	edukativní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ustálené	ustálený	k2eAgFnPc1d1	ustálená
fráze	fráze	k1gFnPc1	fráze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kvítko	kvítko	k1gNnSc1	kvítko
z	z	k7c2	z
čertovy	čertův	k2eAgFnSc2d1	Čertova
zahrádky	zahrádka	k1gFnSc2	zahrádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Vztah	vztah	k1gInSc1	vztah
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
k	k	k7c3	k
zahradě	zahrada	k1gFnSc3	zahrada
je	být	k5eAaImIp3nS	být
odedávna	odedávna	k6eAd1	odedávna
velmi	velmi	k6eAd1	velmi
specifický	specifický	k2eAgInSc1d1	specifický
a	a	k8xC	a
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc2d1	kulturní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
i	i	k9	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
sociálního	sociální	k2eAgInSc2d1	sociální
symbolu	symbol	k1gInSc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
okolnímu	okolní	k2eAgNnSc3d1	okolní
přírodnímu	přírodní	k2eAgNnSc3d1	přírodní
prostředí	prostředí	k1gNnSc3	prostředí
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zahrada	zahrada	k1gFnSc1	zahrada
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významná	významný	k2eAgFnSc1d1	významná
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
její	její	k3xOp3gInSc1	její
estetický	estetický	k2eAgInSc1d1	estetický
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
přínos	přínos	k1gInSc1	přínos
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
nutnost	nutnost	k1gFnSc4	nutnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
dosaženého	dosažený	k2eAgInSc2d1	dosažený
stupně	stupeň	k1gInSc2	stupeň
společenské	společenský	k2eAgFnSc2d1	společenská
formace	formace	k1gFnSc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
větších	veliký	k2eAgInPc2d2	veliký
architektonických	architektonický	k2eAgInPc2d1	architektonický
celků	celek	k1gInPc2	celek
a	a	k8xC	a
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
se	se	k3xPyFc4	se
s	s	k7c7	s
krajinnými	krajinný	k2eAgInPc7d1	krajinný
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
aleje	alej	k1gFnPc1	alej
<g/>
,	,	kIx,	,
parky	park	k1gInPc1	park
či	či	k8xC	či
lesoparky	lesopark	k1gInPc1	lesopark
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okrasné	okrasný	k2eAgFnPc1d1	okrasná
zahrady	zahrada	k1gFnPc1	zahrada
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
účelu	účel	k1gInSc6	účel
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
slouží	sloužit	k5eAaImIp3nS	sloužit
<g/>
,	,	kIx,	,
na	na	k7c6	na
možnostech	možnost	k1gFnPc6	možnost
majitele	majitel	k1gMnSc2	majitel
a	a	k8xC	a
záměru	záměr	k1gInSc2	záměr
tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Odlišnost	odlišnost	k1gFnSc1	odlišnost
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
úprav	úprava	k1gFnPc2	úprava
také	také	k9	také
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
vnímáním	vnímání	k1gNnSc7	vnímání
krásy	krása	k1gFnSc2	krása
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
s	s	k7c7	s
estetickými	estetický	k2eAgFnPc7d1	estetická
a	a	k8xC	a
architektonickými	architektonický	k2eAgFnPc7d1	architektonická
zásadami	zásada	k1gFnPc7	zásada
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
poplatnými	poplatný	k2eAgInPc7d1	poplatný
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
podněty	podnět	k1gInPc7	podnět
vytvářejícími	vytvářející	k2eAgInPc7d1	vytvářející
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
touhami	touha	k1gFnPc7	touha
zákazníka	zákazník	k1gMnSc2	zákazník
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnSc2	majitel
pozemku	pozemek	k1gInSc2	pozemek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
způsob	způsob	k1gInSc1	způsob
úpravy	úprava	k1gFnSc2	úprava
zahrady	zahrada	k1gFnSc2	zahrada
se	se	k3xPyFc4	se
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
tedy	tedy	k9	tedy
měnil	měnit	k5eAaImAgInS	měnit
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
znak	znak	k1gInSc1	znak
příslušnosti	příslušnost	k1gFnSc2	příslušnost
majitele	majitel	k1gMnSc2	majitel
zahrady	zahrada	k1gFnSc2	zahrada
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
zde	zde	k6eAd1	zde
hrála	hrát	k5eAaImAgFnS	hrát
roli	role	k1gFnSc4	role
měnící	měnící	k2eAgFnSc4d1	měnící
se	se	k3xPyFc4	se
móda	móda	k1gFnSc1	móda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
zahrady	zahrada	k1gFnPc1	zahrada
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
pouhý	pouhý	k2eAgInSc4d1	pouhý
znak	znak	k1gInSc4	znak
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
za	za	k7c4	za
estetické	estetický	k2eAgNnSc4d1	estetické
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgFnPc1d1	považována
zahrady	zahrada	k1gFnPc1	zahrada
přibližně	přibližně	k6eAd1	přibližně
shodné	shodný	k2eAgFnPc1d1	shodná
<g/>
.	.	kIx.	.
</s>
<s>
Proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
oceňovaných	oceňovaný	k2eAgInPc2d1	oceňovaný
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
kvalit	kvalita	k1gFnPc2	kvalita
je	být	k5eAaImIp3nS	být
však	však	k9	však
dána	dát	k5eAaPmNgFnS	dát
nejen	nejen	k6eAd1	nejen
vývojem	vývoj	k1gInSc7	vývoj
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
snahou	snaha	k1gFnSc7	snaha
majitele	majitel	k1gMnSc2	majitel
předvést	předvést	k5eAaPmF	předvést
svou	svůj	k3xOyFgFnSc4	svůj
jedinečnost	jedinečnost	k1gFnSc4	jedinečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
době	doba	k1gFnSc6	doba
vývoje	vývoj	k1gInSc2	vývoj
zahrad	zahrada	k1gFnPc2	zahrada
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
nejcennější	cenný	k2eAgFnPc1d3	nejcennější
právě	právě	k6eAd1	právě
unikátní	unikátní	k2eAgFnPc1d1	unikátní
úpravy	úprava	k1gFnPc1	úprava
a	a	k8xC	a
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nejobdivovanější	obdivovaný	k2eAgInPc1d3	nejobdivovanější
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
současně	současně	k6eAd1	současně
nejčastěji	často	k6eAd3	často
napodobovány	napodobován	k2eAgFnPc1d1	napodobována
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
jedinečnosti	jedinečnost	k1gFnSc2	jedinečnost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
právě	právě	k6eAd1	právě
vzácností	vzácnost	k1gFnSc7	vzácnost
nebo	nebo	k8xC	nebo
unikátností	unikátnost	k1gFnSc7	unikátnost
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
důkazem	důkaz	k1gInSc7	důkaz
schopností	schopnost	k1gFnPc2	schopnost
majitele	majitel	k1gMnPc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Opakování	opakování	k1gNnSc1	opakování
a	a	k8xC	a
napodobování	napodobování	k1gNnSc1	napodobování
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
devalvuje	devalvovat	k5eAaBmIp3nS	devalvovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
u	u	k7c2	u
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Předměty	předmět	k1gInPc1	předmět
touhy	touha	k1gFnSc2	touha
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
<g/>
;	;	kIx,	;
právě	právě	k9	právě
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
jsou	být	k5eAaImIp3nP	být
krásné	krásný	k2eAgFnPc4d1	krásná
zahrady	zahrada	k1gFnPc4	zahrada
tak	tak	k6eAd1	tak
různé	různý	k2eAgFnPc1d1	různá
a	a	k8xC	a
invence	invence	k1gFnSc1	invence
tak	tak	k6eAd1	tak
ceněná	ceněný	k2eAgFnSc1d1	ceněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
hlavní	hlavní	k2eAgInSc1d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
parků	park	k1gInPc2	park
bylo	být	k5eAaImAgNnS	být
odpočinout	odpočinout	k5eAaPmF	odpočinout
si	se	k3xPyFc3	se
v	v	k7c6	v
lůně	lůno	k1gNnSc6	lůno
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
by	by	kYmCp3nP	by
a	a	k9	a
jistě	jistě	k6eAd1	jistě
by	by	kYmCp3nS	by
celá	celý	k2eAgFnSc1d1	celá
století	století	k1gNnSc6	století
nevládly	vládnout	k5eNaImAgInP	vládnout
parky	park	k1gInPc1	park
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
geometrických	geometrický	k2eAgInPc6d1	geometrický
principech	princip	k1gInPc6	princip
se	s	k7c7	s
zářivými	zářivý	k2eAgFnPc7d1	zářivá
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
oslňující	oslňující	k2eAgFnSc1d1	oslňující
architekturou	architektura	k1gFnSc7	architektura
a	a	k8xC	a
blyštivými	blyštivý	k2eAgInPc7d1	blyštivý
doplňky	doplněk	k1gInPc7	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
těchto	tento	k3xDgFnPc2	tento
úprav	úprava	k1gFnPc2	úprava
je	být	k5eAaImIp3nS	být
oslnit	oslnit	k5eAaPmF	oslnit
návštěvníka	návštěvník	k1gMnSc4	návštěvník
bohatstvím	bohatství	k1gNnSc7	bohatství
majitele	majitel	k1gMnSc2	majitel
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
rozestupy	rozestup	k1gInPc1	rozestup
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
implikovat	implikovat	k5eAaImF	implikovat
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
majitelovy	majitelův	k2eAgFnPc4d1	majitelova
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
dokonalost	dokonalost	k1gFnSc4	dokonalost
<g/>
,	,	kIx,	,
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
,	,	kIx,	,
kázeň	kázeň	k1gFnSc4	kázeň
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
by	by	k9	by
hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
parků	park	k1gInPc2	park
a	a	k8xC	a
zahrad	zahrada	k1gFnPc2	zahrada
bylo	být	k5eAaImAgNnS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
majiteli	majitel	k1gMnSc3	majitel
zdroj	zdroj	k1gInSc4	zdroj
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
by	by	kYmCp3nP	by
okrasné	okrasný	k2eAgFnPc1d1	okrasná
zahrady	zahrada	k1gFnPc1	zahrada
a	a	k8xC	a
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
zde	zde	k6eAd1	zde
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
ty	ten	k3xDgFnPc1	ten
jedlé	jedlý	k2eAgFnPc1d1	jedlá
jen	jen	k9	jen
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
a	a	k8xC	a
bohaté	bohatý	k2eAgNnSc1d1	bohaté
květenství	květenství	k1gNnSc1	květenství
může	moct	k5eAaImIp3nS	moct
opravdu	opravdu	k6eAd1	opravdu
navozovat	navozovat	k5eAaImF	navozovat
očekávání	očekávání	k1gNnSc4	očekávání
hojnosti	hojnost	k1gFnSc2	hojnost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
zahrada	zahrada	k1gFnSc1	zahrada
jen	jen	k9	jen
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
napodobení	napodobení	k1gNnSc1	napodobení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
zahrady	zahrada	k1gFnPc4	zahrada
totiž	totiž	k9	totiž
skutečně	skutečně	k6eAd1	skutečně
měly	mít	k5eAaImAgFnP	mít
odrážet	odrážet	k5eAaImF	odrážet
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
přirozenost	přirozenost	k1gFnSc4	přirozenost
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
by	by	kYmCp3nP	by
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
i	i	k9	i
ty	ten	k3xDgInPc1	ten
přírodní	přírodní	k2eAgInPc4d1	přírodní
<g/>
,	,	kIx,	,
venkovské	venkovský	k2eAgInPc4d1	venkovský
<g/>
,	,	kIx,	,
či	či	k8xC	či
anglické	anglický	k2eAgNnSc1d1	anglické
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
okázalým	okázalý	k2eAgNnSc7d1	okázalé
dramatickým	dramatický	k2eAgNnSc7d1	dramatické
uměním	umění	k1gNnSc7	umění
využívajícím	využívající	k2eAgNnSc7d1	využívající
především	především	k9	především
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
kontrasty	kontrast	k1gInPc4	kontrast
<g/>
,	,	kIx,	,
romantické	romantický	k2eAgInPc4d1	romantický
a	a	k8xC	a
atraktivní	atraktivní	k2eAgInPc4d1	atraktivní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
v	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
kulisách	kulisa	k1gFnPc6	kulisa
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
divadlo	divadlo	k1gNnSc4	divadlo
se	se	k3xPyFc4	se
zahrada	zahrada	k1gFnSc1	zahrada
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
zručnost	zručnost	k1gFnSc4	zručnost
svých	svůj	k3xOyFgMnPc2	svůj
nejschopnějších	schopný	k2eAgMnPc2d3	nejschopnější
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
co	co	k9	co
nejvěrohodněji	věrohodně	k6eAd3	věrohodně
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
umělými	umělý	k2eAgFnPc7d1	umělá
nádržemi	nádrž	k1gFnPc7	nádrž
dojem	dojem	k1gInSc1	dojem
říčních	říční	k2eAgInPc2d1	říční
meandrů	meandr	k1gInPc2	meandr
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
záhonů	záhon	k1gInPc2	záhon
iluzi	iluze	k1gFnSc3	iluze
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
luk	louka	k1gFnPc2	louka
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
nejsou	být	k5eNaImIp3nP	být
věrnou	věrný	k2eAgFnSc7d1	věrná
napodobeninou	napodobenina	k1gFnSc7	napodobenina
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
probouzejí	probouzet	k5eAaImIp3nP	probouzet
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
jiných	jiný	k1gMnPc2	jiný
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Napodobení	napodobení	k1gNnSc1	napodobení
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
bezúčelné	bezúčelný	k2eAgNnSc1d1	bezúčelné
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
očekává	očekávat	k5eAaImIp3nS	očekávat
emoce	emoce	k1gFnPc4	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
vytváření	vytváření	k1gNnSc2	vytváření
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
sentimentu	sentiment	k1gInSc2	sentiment
<g/>
,	,	kIx,	,
radosti	radost	k1gFnSc2	radost
či	či	k8xC	či
krásného	krásný	k2eAgInSc2d1	krásný
smutku	smutek	k1gInSc2	smutek
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
kompozici	kompozice	k1gFnSc6	kompozice
právě	právě	k9	právě
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
hovoří	hovořit	k5eAaImIp3nS	hovořit
už	už	k9	už
Repton	Repton	k1gInSc1	Repton
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
kompozice	kompozice	k1gFnSc2	kompozice
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
scénářem	scénář	k1gInSc7	scénář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
architekt	architekt	k1gMnSc1	architekt
vytvářením	vytváření	k1gNnSc7	vytváření
parku	park	k1gInSc2	park
či	či	k8xC	či
zahrady	zahrada	k1gFnSc2	zahrada
píše	psát	k5eAaImIp3nS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Náznak	náznak	k1gInSc1	náznak
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
stylizovaný	stylizovaný	k2eAgInSc1d1	stylizovaný
obraz	obraz	k1gInSc1	obraz
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
podněcování	podněcování	k1gNnSc3	podněcování
emocí	emoce	k1gFnPc2	emoce
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
než	než	k8xS	než
vytváření	vytváření	k1gNnSc4	vytváření
skutečně	skutečně	k6eAd1	skutečně
reálných	reálný	k2eAgFnPc2d1	reálná
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
zahrad	zahrada	k1gFnPc2	zahrada
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
vytvářeny	vytvářit	k5eAaPmNgFnP	vytvářit
jako	jako	k8xC	jako
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
jako	jako	k8xC	jako
živé	živý	k2eAgFnPc1d1	živá
náboženské	náboženský	k2eAgFnPc1d1	náboženská
knihy	kniha	k1gFnPc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
zahrad	zahrada	k1gFnPc2	zahrada
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
se	se	k3xPyFc4	se
i	i	k9	i
během	během	k7c2	během
roku	rok	k1gInSc2	rok
nebo	nebo	k8xC	nebo
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
poutat	poutat	k5eAaImF	poutat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
dávat	dávat	k5eAaImF	dávat
svou	svůj	k3xOyFgFnSc7	svůj
úpravou	úprava	k1gFnSc7	úprava
vyšší	vysoký	k2eAgFnSc7d2	vyšší
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zkoumání	zkoumání	k1gNnSc4	zkoumání
<g/>
,	,	kIx,	,
či	či	k8xC	či
být	být	k5eAaImF	být
něčím	něco	k3yInSc7	něco
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
úhrnem	úhrn	k1gInSc7	úhrn
záhonů	záhon	k1gInPc2	záhon
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ceněno	cenit	k5eAaImNgNnS	cenit
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
touha	touha	k1gFnSc1	touha
naslouchat	naslouchat	k5eAaImF	naslouchat
příběhům	příběh	k1gInPc3	příběh
a	a	k8xC	a
poznávat	poznávat	k5eAaImF	poznávat
nové	nový	k2eAgFnPc4d1	nová
věci	věc	k1gFnPc4	věc
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
bezedná	bezedný	k2eAgFnSc1d1	bezedná
<g/>
.	.	kIx.	.
</s>
<s>
Požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vyprávět	vyprávět	k5eAaImF	vyprávět
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
naplňovat	naplňovat	k5eAaImF	naplňovat
jen	jen	k9	jen
sled	sled	k1gInSc1	sled
symbolů	symbol	k1gInPc2	symbol
dávajících	dávající	k2eAgInPc2d1	dávající
nějaký	nějaký	k3yIgInSc4	nějaký
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
tvarované	tvarovaný	k2eAgInPc1d1	tvarovaný
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
sochy	socha	k1gFnPc1	socha
rozmístěné	rozmístěný	k2eAgFnPc1d1	rozmístěná
v	v	k7c6	v
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahradě	zahrada	k1gFnSc6	zahrada
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
zelené	zelená	k1gFnSc2	zelená
a	a	k8xC	a
plné	plný	k2eAgFnSc2d1	plná
květů	květ	k1gInPc2	květ
mnohdy	mnohdy	k6eAd1	mnohdy
majitel	majitel	k1gMnSc1	majitel
touží	toužit	k5eAaImIp3nS	toužit
zcela	zcela	k6eAd1	zcela
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
po	po	k7c6	po
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
vykvétají	vykvétat	k5eAaImIp3nP	vykvétat
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnSc3d1	jiná
už	už	k9	už
plodí	plodit	k5eAaImIp3nP	plodit
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
takového	takový	k3xDgInSc2	takový
důvodu	důvod	k1gInSc2	důvod
jsou	být	k5eAaImIp3nP	být
ceněny	cenit	k5eAaImNgFnP	cenit
i	i	k9	i
pláně	pláň	k1gFnPc1	pláň
trvalek	trvalka	k1gFnPc2	trvalka
spálených	spálený	k2eAgFnPc2d1	spálená
suchých	suchý	k2eAgMnPc2d1	suchý
mrazem	mráz	k1gInSc7	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Poutavou	poutavý	k2eAgFnSc7d1	poutavá
dobrodružnou	dobrodružný	k2eAgFnSc7d1	dobrodružná
zahradou	zahrada	k1gFnSc7	zahrada
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zahrada	zahrada	k1gFnSc1	zahrada
upravená	upravený	k2eAgFnSc1d1	upravená
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
přírodní	přírodní	k2eAgFnSc2d1	přírodní
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
vždyť	vždyť	k9	vždyť
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
vítána	vítat	k5eAaImNgFnS	vítat
právě	právě	k9	právě
možnost	možnost	k1gFnSc1	možnost
pozorovat	pozorovat	k5eAaImF	pozorovat
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc4	tento
motivy	motiv	k1gInPc4	motiv
tvorby	tvorba	k1gFnSc2	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
v	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
čínských	čínský	k2eAgFnPc6d1	čínská
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
japonských	japonský	k2eAgFnPc6d1	japonská
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
zahradách	zahrada	k1gFnPc6	zahrada
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
moderních	moderní	k2eAgInPc6d1	moderní
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
takto	takto	k6eAd1	takto
motivované	motivovaný	k2eAgFnPc1d1	motivovaná
úpravy	úprava	k1gFnPc1	úprava
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
majiteli	majitel	k1gMnSc6	majitel
a	a	k8xC	a
návštěvníkovi	návštěvník	k1gMnSc3	návštěvník
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
potěšení	potěšení	k1gNnSc4	potěšení
a	a	k8xC	a
obdiv	obdiv	k1gInSc4	obdiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgInSc4d1	kulturní
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInSc4d1	sociální
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
ekologický	ekologický	k2eAgInSc4d1	ekologický
vliv	vliv	k1gInSc4	vliv
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
rodí	rodit	k5eAaImIp3nP	rodit
a	a	k8xC	a
rostou	růst	k5eAaImIp3nP	růst
jsou	být	k5eAaImIp3nP	být
li	li	k8xS	li
dobře	dobře	k6eAd1	dobře
vyživovány	vyživován	k2eAgInPc1d1	vyživován
<g/>
;	;	kIx,	;
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
raší	rašit	k5eAaImIp3nS	rašit
z	z	k7c2	z
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
materiálů	materiál	k1gInPc2	materiál
zcela	zcela	k6eAd1	zcela
různých	různý	k2eAgInPc2d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Kultivace	kultivace	k1gFnSc1	kultivace
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
...	...	k?	...
...	...	k?	...
<g/>
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
plody	plod	k1gInPc4	plod
dobrými	dobrý	k2eAgInPc7d1	dobrý
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
...	...	k?	...
<g/>
(	(	kIx(	(
<g/>
tyto	tyt	k2eAgNnSc1d1	tyto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
zdomácnělé	zdomácnělý	k2eAgFnPc1d1	zdomácnělá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
...	...	k?	...
<g/>
Proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
tymián	tymián	k1gInSc4	tymián
v	v	k7c6	v
Attice	Attika	k1gFnSc6	Attika
tak	tak	k6eAd1	tak
hořký	hořký	k2eAgMnSc1d1	hořký
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
?	?	kIx.	?
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
půda	půda	k1gFnSc1	půda
Attica	Attic	k1gInSc2	Attic
je	být	k5eAaImIp3nS	být
lehká	lehký	k2eAgFnSc1d1	lehká
a	a	k8xC	a
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nenajdou	najít	k5eNaPmIp3nP	najít
mnoho	mnoho	k6eAd1	mnoho
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
?	?	kIx.	?
</s>
<s>
...	...	k?	...
<g/>
Proč	proč	k6eAd1	proč
myrta	myrta	k1gFnSc1	myrta
promnutá	promnutý	k2eAgFnSc1d1	promnutý
mezi	mezi	k7c4	mezi
prsty	prst	k1gInPc4	prst
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
se	se	k3xPyFc4	se
není	být	k5eNaImIp3nS	být
rozemnuta	rozemnut	k2eAgFnSc1d1	rozemnut
<g/>
?	?	kIx.	?
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
to	ten	k3xDgNnSc1	ten
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
s	s	k7c7	s
hrozny	hrozen	k1gInPc7	hrozen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
svazky	svazek	k1gInPc1	svazek
sklizené	sklizený	k2eAgInPc1d1	sklizený
z	z	k7c2	z
vinice	vinice	k1gFnSc2	vinice
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
sladší	sladký	k2eAgInPc1d2	sladší
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
utržené	utržený	k2eAgFnSc2d1	utržená
z	z	k7c2	z
podnože	podnož	k1gInSc2	podnož
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
(	(	kIx(	(
<g/>
384	[number]	k4	384
<g/>
–	–	k?	–
<g/>
322	[number]	k4	322
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
<g/>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
vliv	vliv	k1gInSc1	vliv
zahrad	zahrada	k1gFnPc2	zahrada
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
existence	existence	k1gFnSc1	existence
zahrad	zahrada	k1gFnPc2	zahrada
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
literaturu	literatura	k1gFnSc4	literatura
i	i	k8xC	i
jiné	jiný	k2eAgInPc4d1	jiný
směry	směr	k1gInPc4	směr
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
básníci	básník	k1gMnPc1	básník
(	(	kIx(	(
<g/>
George	George	k1gInSc1	George
Gordon	Gordon	k1gMnSc1	Gordon
Byron	Byron	k1gMnSc1	Byron
například	například	k6eAd1	například
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
architekturu	architektura	k1gFnSc4	architektura
anglického	anglický	k2eAgInSc2d1	anglický
parku	park	k1gInSc2	park
proti	proti	k7c3	proti
francouzskému	francouzský	k2eAgInSc3d1	francouzský
baroknímu	barokní	k2eAgInSc3d1	barokní
stylu	styl	k1gInSc3	styl
<g/>
)	)	kIx)	)
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
často	často	k6eAd1	často
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
styl	styl	k1gInSc4	styl
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Politikové	politik	k1gMnPc1	politik
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
díky	díky	k7c3	díky
zřizování	zřizování	k1gNnSc3	zřizování
veřejných	veřejný	k2eAgFnPc2d1	veřejná
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
parků	park	k1gInPc2	park
získávali	získávat	k5eAaImAgMnP	získávat
přízeň	přízeň	k1gFnSc4	přízeň
poddaných	poddaná	k1gFnPc2	poddaná
nebo	nebo	k8xC	nebo
podřízených	podřízený	k1gMnPc2	podřízený
<g/>
.	.	kIx.	.
</s>
<s>
Zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
parků	park	k1gInPc2	park
si	se	k3xPyFc3	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
majitelé	majitel	k1gMnPc1	majitel
více	hodně	k6eAd2	hodně
cenili	cenit	k5eAaImAgMnP	cenit
než	než	k8xS	než
paláců	palác	k1gInPc2	palác
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
ležících	ležící	k2eAgFnPc2d1	ležící
<g/>
.	.	kIx.	.
</s>
<s>
Majetní	majetný	k2eAgMnPc1d1	majetný
lidé	člověk	k1gMnPc1	člověk
dávali	dávat	k5eAaImAgMnP	dávat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
najevo	najevo	k6eAd1	najevo
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
invenci	invence	k1gFnSc4	invence
a	a	k8xC	a
majetnost	majetnost	k1gFnSc4	majetnost
právě	právě	k9	právě
úpravami	úprava	k1gFnPc7	úprava
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
pravidelným	pravidelný	k2eAgFnPc3d1	pravidelná
zahradám	zahrada	k1gFnPc3	zahrada
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
celospolečenským	celospolečenský	k2eAgInSc7d1	celospolečenský
jevem	jev	k1gInSc7	jev
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
se	se	k3xPyFc4	se
veřejná	veřejný	k2eAgFnSc1d1	veřejná
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
vážná	vážný	k2eAgFnSc1d1	vážná
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
osobních	osobní	k2eAgInPc2d1	osobní
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
správné	správný	k2eAgFnPc4d1	správná
zahrady	zahrada	k1gFnPc4	zahrada
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Florimánie	Florimánie	k1gFnSc1	Florimánie
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
omezena	omezit	k5eAaPmNgFnS	omezit
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
posedlost	posedlost	k1gFnSc4	posedlost
holandskými	holandský	k2eAgInPc7d1	holandský
tulipány	tulipán	k1gInPc7	tulipán
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
otřásala	otřásat	k5eAaImAgFnS	otřásat
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtitel	šlechtitel	k1gMnSc1	šlechtitel
růží	růžit	k5eAaImIp3nS	růžit
David	David	k1gMnSc1	David
C.	C.	kA	C.
H.	H.	kA	H.
Austin	Austin	k1gMnSc1	Austin
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Medaili	medaile	k1gFnSc4	medaile
cti	čest	k1gFnSc2	čest
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nositelem	nositel	k1gMnSc7	nositel
Řádu	řád	k1gInSc3	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
světových	světový	k2eAgFnPc6d1	světová
válkách	válka	k1gFnPc6	válka
byla	být	k5eAaImAgFnS	být
zahrada	zahrada	k1gFnSc1	zahrada
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
místo	místo	k1gNnSc1	místo
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
aktivní	aktivní	k2eAgInSc4d1	aktivní
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
upravené	upravený	k2eAgFnPc1d1	upravená
jako	jako	k8xC	jako
pravidelně	pravidelně	k6eAd1	pravidelně
kosené	kosený	k2eAgInPc1d1	kosený
trávníky	trávník	k1gInPc1	trávník
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
hrozbu	hrozba	k1gFnSc4	hrozba
omezující	omezující	k2eAgFnSc4d1	omezující
druhovou	druhový	k2eAgFnSc4d1	druhová
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
a	a	k8xC	a
přírodní	přírodní	k2eAgFnPc4d1	přírodní
zahrady	zahrada	k1gFnPc4	zahrada
jsou	být	k5eAaImIp3nP	být
propagovány	propagován	k2eAgInPc1d1	propagován
jako	jako	k8xC	jako
osobní	osobní	k2eAgInSc1d1	osobní
přínos	přínos	k1gInSc1	přínos
pro	pro	k7c4	pro
život	život	k1gInSc4	život
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
zahrad	zahrada	k1gFnPc2	zahrada
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejlépe	dobře	k6eAd3	dobře
vykreslen	vykreslen	k2eAgInSc1d1	vykreslen
častým	častý	k2eAgNnSc7d1	časté
použitím	použití	k1gNnSc7	použití
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
"	"	kIx"	"
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slyslu	slysl	k1gInSc6	slysl
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
sdružení	sdružení	k1gNnSc2	sdružení
<g/>
,	,	kIx,	,
aktivit	aktivita	k1gFnPc2	aktivita
nebo	nebo	k8xC	nebo
komerčních	komerční	k2eAgInPc2d1	komerční
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
terapie	terapie	k1gFnSc1	terapie
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
již	již	k6eAd1	již
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
psychologických	psychologický	k2eAgFnPc2d1	psychologická
metod	metoda	k1gFnPc2	metoda
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
tělesně	tělesně	k6eAd1	tělesně
postiženými	postižený	k2eAgInPc7d1	postižený
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
například	například	k6eAd1	například
aromaterapie	aromaterapie	k1gFnSc1	aromaterapie
<g/>
.	.	kIx.	.
<g/>
Tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
zahrad	zahrada	k1gFnPc2	zahrada
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
sdružení	sdružení	k1gNnPc2	sdružení
a	a	k8xC	a
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
Český	český	k2eAgInSc1d1	český
zahrádkářský	zahrádkářský	k2eAgInSc1d1	zahrádkářský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Crafts	Crafts	k1gInSc1	Crafts
movement	movement	k1gMnSc1	movement
a	a	k8xC	a
The	The	k1gMnSc1	The
Royal	Royal	k1gMnSc1	Royal
Horticultural	Horticultural	k1gMnSc1	Horticultural
Society	societa	k1gFnSc2	societa
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
Fronta	fronta	k1gFnSc1	fronta
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
zahradních	zahradní	k2eAgMnPc2d1	zahradní
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
lze	lze	k6eAd1	lze
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k8xC	jako
objekty	objekt	k1gInPc4	objekt
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
společnost	společnost	k1gFnSc4	společnost
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
poměrně	poměrně	k6eAd1	poměrně
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
před	před	k7c7	před
Dnem	den	k1gInSc7	den
matek	matka	k1gFnPc2	matka
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
"	"	kIx"	"
<g/>
National	National	k1gMnPc1	National
Public	publicum	k1gNnPc2	publicum
Gardens	Gardensa	k1gFnPc2	Gardensa
Day	Day	k1gFnPc2	Day
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ho	on	k3xPp3gMnSc4	on
neziskové	ziskový	k2eNgFnPc1d1	nezisková
organizace	organizace	k1gFnPc1	organizace
založily	založit	k5eAaPmAgFnP	založit
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
zvýšit	zvýšit	k5eAaPmF	zvýšit
povědomí	povědomí	k1gNnSc4	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c6	o
vzdělávacích	vzdělávací	k2eAgInPc6d1	vzdělávací
zdrojích	zdroj	k1gInPc6	zdroj
veřejných	veřejný	k2eAgFnPc2d1	veřejná
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
podpořit	podpořit	k5eAaPmF	podpořit
místní	místní	k2eAgFnSc2d1	místní
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
původních	původní	k2eAgFnPc2d1	původní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
či	či	k8xC	či
zachování	zachování	k1gNnSc4	zachování
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
zahrádkářství	zahrádkářství	k1gNnPc2	zahrádkářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
starověké	starověký	k2eAgFnPc4d1	starověká
civilizace	civilizace	k1gFnPc4	civilizace
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
první	první	k4xOgFnPc1	první
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
lidské	lidský	k2eAgFnPc1d1	lidská
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rozkvétaly	rozkvétat	k5eAaImAgFnP	rozkvétat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
Fénicie	Fénicie	k1gFnSc1	Fénicie
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
Íránské	íránský	k2eAgFnSc2d1	íránská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
koncem	koncem	k7c2	koncem
pravěku	pravěk	k1gInSc2	pravěk
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
vpádem	vpád	k1gInSc7	vpád
Arabů	Arab	k1gMnPc2	Arab
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
zemědělci	zemědělec	k1gMnPc1	zemědělec
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
usadili	usadit	k5eAaPmAgMnP	usadit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
7700	[number]	k4	7700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
utvářet	utvářet	k5eAaImF	utvářet
první	první	k4xOgFnPc1	první
usedlé	usedlý	k2eAgFnPc1d1	usedlá
agrární	agrární	k2eAgFnPc1d1	agrární
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známá	k1gFnSc1	známá
města	město	k1gNnSc2	město
světa	svět	k1gInSc2	svět
a	a	k8xC	a
také	také	k6eAd1	také
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgFnPc4	první
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
zahradách	zahrada	k1gFnPc6	zahrada
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
4000	[number]	k4	4000
l.	l.	k?	l.
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
nl	nl	k?	nl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zahradní	zahradní	k2eAgFnPc1d1	zahradní
úpravy	úprava	k1gFnPc1	úprava
mají	mít	k5eAaImIp3nP	mít
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
rysy	rys	k1gInPc4	rys
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
osově	osově	k6eAd1	osově
souměrné	souměrný	k2eAgFnPc1d1	souměrná
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
geometrické	geometrický	k2eAgFnPc1d1	geometrická
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
výsadby	výsadba	k1gFnPc1	výsadba
jsou	být	k5eAaImIp3nP	být
zřetelně	zřetelně	k6eAd1	zřetelně
ve	v	k7c6	v
sponu	spon	k1gInSc6	spon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
přední	přední	k2eAgFnSc2d1	přední
zahrady	zahrada	k1gFnSc2	zahrada
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xS	jako
záhon	záhon	k1gInSc4	záhon
<g/>
,	,	kIx,	,
živý	živý	k2eAgInSc4d1	živý
plot	plot	k1gInSc4	plot
<g/>
,	,	kIx,	,
geometricky	geometricky	k6eAd1	geometricky
upravený	upravený	k2eAgInSc4d1	upravený
bazén	bazén	k1gInSc4	bazén
<g/>
,	,	kIx,	,
fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
alej	alej	k1gFnSc1	alej
<g/>
,	,	kIx,	,
terasa	terasa	k1gFnSc1	terasa
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgNnSc1d1	umělé
zavlažování	zavlažování	k1gNnSc1	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
také	také	k9	také
umisťována	umisťován	k2eAgFnSc1d1	umisťován
mytická	mytický	k2eAgFnSc1d1	mytická
rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
pozemky	pozemek	k1gInPc7	pozemek
v	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Persii	Persie	k1gFnSc6	Persie
označovanými	označovaný	k2eAgInPc7d1	označovaný
jako	jako	k8xC	jako
paradis	paradis	k1gFnSc1	paradis
které	který	k3yRgNnSc1	který
však	však	k9	však
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
lovecké	lovecký	k2eAgInPc1d1	lovecký
obory	obor	k1gInPc1	obor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lovení	lovení	k1gNnSc4	lovení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
milostné	milostný	k2eAgFnSc6d1	milostná
básni	báseň	k1gFnSc6	báseň
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
zahrada	zahrada	k1gFnSc1	zahrada
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Rákosí	rákosí	k1gNnSc3	rákosí
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
se	se	k3xPyFc4	se
zelenalo	zelenat	k5eAaImAgNnS	zelenat
<g/>
,	,	kIx,	,
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
keře	keř	k1gInPc1	keř
kvetly	kvést	k5eAaImAgInP	kvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
Zralé	zralý	k2eAgFnPc1d1	zralá
broskve	broskev	k1gFnPc1	broskev
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
se	se	k3xPyFc4	se
podobaly	podobat	k5eAaImAgInP	podobat
bronzua	bronzua	k6eAd1	bronzua
háje	háj	k1gInPc1	háj
se	se	k3xPyFc4	se
leskly	lesknout	k5eAaImAgInP	lesknout
peckami	pecka	k1gFnPc7	pecka
mandloní	mandloň	k1gFnSc7	mandloň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
podle	podle	k7c2	podle
nalezených	nalezený	k2eAgInPc2d1	nalezený
důkazů	důkaz	k1gInPc2	důkaz
ale	ale	k8xC	ale
záměrně	záměrně	k6eAd1	záměrně
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
nejen	nejen	k6eAd1	nejen
okrasné	okrasný	k2eAgFnPc4d1	okrasná
a	a	k8xC	a
ovocné	ovocný	k2eAgFnPc4d1	ovocná
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
koření	koření	k1gNnPc4	koření
a	a	k8xC	a
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
květiny	květina	k1gFnPc4	květina
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homér	Homér	k1gMnSc1	Homér
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žil	žít	k5eAaImAgMnS	žít
v	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1200	[number]	k4	1200
až	až	k8xS	až
700	[number]	k4	700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
Odyssee	Odyssee	k1gNnSc6	Odyssee
zahradu	zahrada	k1gFnSc4	zahrada
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Přímo	přímo	k6eAd1	přímo
za	za	k7c7	za
dveřmi	dveře	k1gFnPc7	dveře
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
<g/>
...	...	k?	...
rostou	růst	k5eAaImIp3nP	růst
tu	tu	k6eAd1	tu
vysoké	vysoký	k2eAgInPc1d1	vysoký
kvetoucí	kvetoucí	k2eAgInPc1d1	kvetoucí
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
hrušně	hrušeň	k1gFnPc1	hrušeň
a	a	k8xC	a
granátová	granátový	k2eAgNnPc1d1	granátové
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
a	a	k8xC	a
jabloně	jabloň	k1gFnPc1	jabloň
se	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
plody	plod	k1gInPc7	plod
a	a	k8xC	a
sladké	sladký	k2eAgInPc4d1	sladký
fíky	fík	k1gInPc4	fík
a	a	k8xC	a
olivy	oliva	k1gFnPc4	oliva
<g/>
...	...	k?	...
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stromů	strom	k1gInPc2	strom
nikdy	nikdy	k6eAd1	nikdy
nepřestávají	přestávat	k5eNaImIp3nP	přestávat
plodit	plodit	k5eAaImF	plodit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nebo	nebo	k8xC	nebo
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Foukání	foukání	k1gNnSc1	foukání
větru	vítr	k1gInSc2	vítr
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
sklidí	sklidit	k5eAaPmIp3nS	sklidit
vždy	vždy	k6eAd1	vždy
nějaké	nějaký	k3yIgNnSc4	nějaký
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
urodilo	urodit	k5eAaPmAgNnS	urodit
a	a	k8xC	a
dozrávalo	dozrávat	k5eAaImAgNnS	dozrávat
další	další	k2eAgNnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Hruška	hruška	k1gFnSc1	hruška
na	na	k7c6	na
hrušce	hruška	k1gFnSc6	hruška
voskovatí	voskovatět	k5eAaImIp3nS	voskovatět
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc1	jablko
na	na	k7c6	na
jablku	jablko	k1gNnSc6	jablko
<g/>
,	,	kIx,	,
ano	ano	k9	ano
<g/>
,	,	kIx,	,
nahloučené	nahloučený	k2eAgFnPc1d1	nahloučená
bobule	bobule	k1gFnPc1	bobule
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
v	v	k7c6	v
hroznu	hrozen	k1gInSc6	hrozen
<g/>
,	,	kIx,	,
fík	fík	k1gInSc1	fík
po	po	k7c6	po
fíku	fík	k1gInSc6	fík
<g/>
.	.	kIx.	.
<g/>
...	...	k?	...
Tam	tam	k6eAd1	tam
taky	taky	k6eAd1	taky
<g/>
,	,	kIx,	,
běží	běžet	k5eAaImIp3nP	běžet
vyvýšené	vyvýšený	k2eAgFnPc1d1	vyvýšená
linie	linie	k1gFnPc1	linie
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
záhonů	záhon	k1gInPc2	záhon
<g/>
...	...	k?	...
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
kašny	kašna	k1gFnPc1	kašna
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
<g />
.	.	kIx.	.
</s>
<s>
proudí	proudit	k5eAaPmIp3nS	proudit
voda	voda	k1gFnSc1	voda
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
Xenofón	Xenofón	k1gInSc1	Xenofón
(	(	kIx(	(
<g/>
444	[number]	k4	444
<g/>
-	-	kIx~	-
<g/>
359	[number]	k4	359
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
rozhovor	rozhovor	k1gInSc4	rozhovor
Sokrata	Sokrates	k1gMnSc2	Sokrates
a	a	k8xC	a
Critobuluse	Critobuluse	k1gFnSc2	Critobuluse
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Sokrates	Sokrates	k1gMnSc1	Sokrates
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
části	část	k1gFnSc6	část
Persie	Persie	k1gFnSc2	Persie
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
princ	princ	k1gMnSc1	princ
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
satrapa	satrapa	k1gMnSc1	satrapa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usídlí	usídlit	k5eAaPmIp3nS	usídlit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stráví	strávit	k5eAaPmIp3nS	strávit
trochu	trocha	k1gFnSc4	trocha
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
postará	postarat	k5eAaPmIp3nS	postarat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
vynikající	vynikající	k2eAgFnPc4d1	vynikající
zahrady	zahrada	k1gFnPc4	zahrada
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Paradeisioi	Paradeisio	k1gFnSc2	Paradeisio
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc4d1	plný
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
popisuje	popisovat	k5eAaImIp3nS	popisovat
ohromení	ohromení	k1gNnSc4	ohromení
řeckého	řecký	k2eAgMnSc2d1	řecký
vůdce	vůdce	k1gMnSc2	vůdce
Lýsandra	Lýsandr	k1gMnSc2	Lýsandr
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
perský	perský	k2eAgMnSc1d1	perský
vládce	vládce	k1gMnSc1	vládce
Kýros	Kýros	k1gMnSc1	Kýros
ukázal	ukázat	k5eAaPmAgMnS	ukázat
svoji	svůj	k3xOyFgFnSc4	svůj
zahradu	zahrada	k1gFnSc4	zahrada
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
byl	být	k5eAaImAgInS	být
oslněn	oslnit	k5eAaPmNgInS	oslnit
krásou	krása	k1gFnSc7	krása
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
jejich	jejich	k3xOp3gFnSc2	jejich
výsadby	výsadba	k1gFnSc2	výsadba
<g/>
,	,	kIx,	,
rovnoměrnost	rovnoměrnost	k1gFnSc1	rovnoměrnost
jejich	jejich	k3xOp3gFnPc2	jejich
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc7	jejich
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
úhly	úhel	k1gInPc7	úhel
jeden	jeden	k4xCgMnSc1	jeden
k	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jedním	jeden	k4xCgNnSc7	jeden
slovem	slovo	k1gNnSc7	slovo
krásným	krásný	k2eAgInSc7d1	krásný
trojsponem	trojspon	k1gInSc7	trojspon
(	(	kIx(	(
<g/>
quincunx	quincunx	k1gInSc1	quincunx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
byly	být	k5eAaImAgInP	být
vysazeny	vysadit	k5eAaPmNgInP	vysadit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
vůní	vůně	k1gFnSc7	vůně
<g/>
...	...	k?	...
<g />
.	.	kIx.	.
</s>
<s>
Lýsander	Lýsander	k1gMnSc1	Lýsander
nemohl	moct	k5eNaImAgMnS	moct
přestat	přestat	k5eAaPmF	přestat
vychvaloval	vychvalovat	k5eAaImAgInS	vychvalovat
krásu	krása	k1gFnSc4	krása
seřazení	seřazení	k1gNnSc2	seřazení
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
Kýros	Kýros	k1gMnSc1	Kýros
mu	on	k3xPp3gMnSc3	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
Všechny	všechen	k3xTgInPc1	všechen
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jste	být	k5eAaImIp2nP	být
zde	zde	k6eAd1	zde
hle	hle	k0	hle
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
příkazu	příkaz	k1gInSc2	příkaz
<g/>
.	.	kIx.	.
...	...	k?	...
<g/>
vyměřil	vyměřit	k5eAaPmAgMnS	vyměřit
jsem	být	k5eAaImIp1nS	být
a	a	k8xC	a
upravil	upravit	k5eAaPmAgMnS	upravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
těchto	tento	k3xDgInPc2	tento
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohu	moct	k5eAaImIp1nS	moct
vám	vy	k3xPp2nPc3	vy
některé	některý	k3yIgNnSc1	některý
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
jsem	být	k5eAaImIp1nS	být
zasadil	zasadit	k5eAaPmAgMnS	zasadit
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
dokonce	dokonce	k9	dokonce
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
John	John	k1gMnSc1	John
Claudius	Claudius	k1gMnSc1	Claudius
Loudon	Loudon	k1gMnSc1	Loudon
(	(	kIx(	(
<g/>
1783	[number]	k4	1783
–	–	k?	–
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
publikaci	publikace	k1gFnSc6	publikace
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
i	i	k9	i
historií	historie	k1gFnSc7	historie
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
píše	psát	k5eAaImIp3nS	psát
:	:	kIx,	:
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
naučili	naučit	k5eAaPmAgMnP	naučit
zahradnictví	zahradnictví	k1gNnPc4	zahradnictví
od	od	k7c2	od
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
od	od	k7c2	od
Peršanů	peršan	k1gInPc2	peršan
a	a	k8xC	a
tak	tak	k6eAd1	tak
postupovalo	postupovat	k5eAaImAgNnS	postupovat
jako	jako	k8xC	jako
každé	každý	k3xTgNnSc1	každý
další	další	k2eAgNnSc1d1	další
umění	umění	k1gNnSc1	umění
od	od	k7c2	od
civilizací	civilizace	k1gFnPc2	civilizace
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
míněno	mínit	k5eAaImNgNnS	mínit
nejen	nejen	k6eAd1	nejen
využití	využití	k1gNnSc1	využití
zahrad	zahrada	k1gFnPc2	zahrada
jako	jako	k8xS	jako
míst	místo	k1gNnPc2	místo
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
jako	jako	k9	jako
míst	místo	k1gNnPc2	místo
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
reprezentaci	reprezentace	k1gFnSc3	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
předpokládat	předpokládat	k5eAaImF	předpokládat
vysoce	vysoce	k6eAd1	vysoce
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
egyptskými	egyptský	k2eAgFnPc7d1	egyptská
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kontakty	kontakt	k1gInPc7	kontakt
řecké	řecký	k2eAgFnSc2d1	řecká
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
vyspělou	vyspělý	k2eAgFnSc7d1	vyspělá
kulturou	kultura	k1gFnSc7	kultura
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
kulturně	kulturně	k6eAd1	kulturně
vyspělé	vyspělý	k2eAgFnPc4d1	vyspělá
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
vynikaly	vynikat	k5eAaImAgFnP	vynikat
tvorbou	tvorba	k1gFnSc7	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
patří	patřit	k5eAaImIp3nP	patřit
kromě	kromě	k7c2	kromě
Koreji	Korea	k1gFnSc3	Korea
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
i	i	k8xC	i
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
způsoby	způsob	k1gInPc1	způsob
tvorby	tvorba	k1gFnSc2	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
byly	být	k5eAaImAgFnP	být
vzájemně	vzájemně	k6eAd1	vzájemně
přejímány	přejímán	k2eAgFnPc1d1	přejímána
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
kulturní	kulturní	k2eAgInPc4d1	kulturní
kontakty	kontakt	k1gInPc4	kontakt
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
více	hodně	k6eAd2	hodně
podobností	podobnost	k1gFnPc2	podobnost
než	než	k8xS	než
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
měly	mít	k5eAaImAgInP	mít
často	často	k6eAd1	často
náboženský	náboženský	k2eAgInSc4d1	náboženský
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
rekreační	rekreační	k2eAgInSc1d1	rekreační
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
objektů	objekt	k1gInPc2	objekt
-	-	kIx~	-
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
jsou	být	k5eAaImIp3nP	být
nejstarší	starý	k2eAgFnPc1d3	nejstarší
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
zahradách	zahrada	k1gFnPc6	zahrada
známy	znám	k2eAgInPc1d1	znám
především	především	k9	především
z	z	k7c2	z
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
nebo	nebo	k8xC	nebo
u	u	k7c2	u
evropských	evropský	k2eAgFnPc2d1	Evropská
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
legendách	legenda	k1gFnPc6	legenda
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
jak	jak	k6eAd1	jak
druhová	druhový	k2eAgFnSc1d1	druhová
skladba	skladba	k1gFnSc1	skladba
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zahradní	zahradní	k2eAgFnSc1d1	zahradní
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
obdiv	obdiv	k1gInSc1	obdiv
ke	k	k7c3	k
kultivovaným	kultivovaný	k2eAgFnPc3d1	kultivovaná
zahradám	zahrada	k1gFnPc3	zahrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
nejcennějším	cenný	k2eAgFnPc3d3	nejcennější
částem	část	k1gFnPc3	část
paláců	palác	k1gInPc2	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Babylón	Babylón	k1gInSc1	Babylón
a	a	k8xC	a
Asýrie	Asýrie	k1gFnSc1	Asýrie
====	====	k?	====
</s>
</p>
<p>
<s>
Diodóros	Diodórosa	k1gFnPc2	Diodórosa
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
(	(	kIx(	(
<g/>
50	[number]	k4	50
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc1d1	Semiramidin
nebyly	být	k5eNaImAgFnP	být
vystavěny	vystavěn	k2eAgFnPc1d1	vystavěna
Semiramis	Semiramis	k1gFnSc1	Semiramis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vládcem	vládce	k1gMnSc7	vládce
nazývaným	nazývaný	k2eAgFnPc3d1	nazývaná
Kýros	Kýros	k1gInSc4	Kýros
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kurtizáně	kurtizána	k1gFnSc3	kurtizána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
louky	louka	k1gFnPc4	louka
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovovala	zhotovovat	k5eAaImAgFnS	zhotovovat
umělé	umělý	k2eAgFnPc4d1	umělá
výsadby	výsadba	k1gFnPc4	výsadba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
napodobila	napodobit	k5eAaPmAgFnS	napodobit
Perskou	perský	k2eAgFnSc4d1	perská
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
400	[number]	k4	400
stop	stopa	k1gFnPc2	stopa
čtverečných	čtverečný	k2eAgFnPc2d1	čtverečná
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
stoupala	stoupat	k5eAaImAgFnS	stoupat
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
otevřené	otevřený	k2eAgFnPc1d1	otevřená
místnosti	místnost	k1gFnPc1	místnost
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
schodiště	schodiště	k1gNnSc2	schodiště
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
vystoupavě	vystoupavě	k6eAd1	vystoupavě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podepírají	podepírat	k5eAaImIp3nP	podepírat
celou	celý	k2eAgFnSc4d1	celá
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
leží	ležet	k5eAaImIp3nS	ležet
celá	celý	k2eAgFnSc1d1	celá
plocha	plocha	k1gFnSc1	plocha
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
50	[number]	k4	50
loktů	loket	k1gInPc2	loket
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
samotná	samotný	k2eAgFnSc1d1	samotná
byla	být	k5eAaImAgFnS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
cimbuřím	cimbuří	k1gNnSc7	cimbuří
a	a	k8xC	a
valy	val	k1gInPc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc4d1	silná
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnPc4d1	postavená
s	s	k7c7	s
nemalou	malý	k2eNgFnSc7d1	nemalá
péčí	péče	k1gFnSc7	péče
a	a	k8xC	a
náklady	náklad	k1gInPc7	náklad
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgFnPc1d1	silná
22	[number]	k4	22
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
kámen	kámen	k1gInSc1	kámen
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
stop	stop	k1gInSc1	stop
široký	široký	k2eAgInSc1d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
patrech	patro	k1gNnPc6	patro
této	tento	k3xDgFnSc6	tento
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
položeny	položen	k2eAgInPc4d1	položen
trámy	trám	k1gInPc4	trám
a	a	k8xC	a
velké	velký	k2eAgInPc4d1	velký
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
16	[number]	k4	16
stop	stop	k1gInSc1	stop
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc4	čtyři
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Střecha	střecha	k1gFnSc1	střecha
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
rákosím	rákosí	k1gNnSc7	rákosí
zalitým	zalitý	k2eAgNnSc7d1	zalité
množstvím	množství	k1gNnSc7	množství
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
položeny	položen	k2eAgFnPc1d1	položena
dvojitě	dvojitě	k6eAd1	dvojitě
dlaždice	dlaždice	k1gFnPc4	dlaždice
spojené	spojený	k2eAgFnPc4d1	spojená
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
a	a	k8xC	a
odolnou	odolný	k2eAgFnSc7d1	odolná
maltou	malta	k1gFnSc7	malta
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
pláty	plát	k1gInPc1	plát
olova	olovo	k1gNnSc2	olovo
uložené	uložený	k2eAgNnSc4d1	uložené
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odváděly	odvádět	k5eAaImAgFnP	odvádět
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
také	také	k9	také
aby	aby	kYmCp3nP	aby
nehnily	hnít	k5eNaImAgInP	hnít
základy	základ	k1gInPc1	základ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vším	všecek	k3xTgNnSc7	všecek
byla	být	k5eAaImAgFnS	být
položena	položen	k2eAgFnSc1d1	položena
zemina	zemina	k1gFnSc1	zemina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
hloubku	hloubka	k1gFnSc4	hloubka
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
největších	veliký	k2eAgInPc2d3	veliký
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
položena	položit	k5eAaPmNgFnS	položit
půda	půda	k1gFnSc1	půda
rovná	rovný	k2eAgFnSc1d1	rovná
a	a	k8xC	a
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
osázena	osázet	k5eAaPmNgNnP	osázet
všemi	všecek	k3xTgInPc7	všecek
druhy	druh	k1gInPc4	druh
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
krásu	krása	k1gFnSc4	krása
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
může	moct	k5eAaImIp3nS	moct
potěšit	potěšit	k5eAaPmF	potěšit
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
stály	stát	k5eAaImAgInP	stát
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
mnoho	mnoho	k4c4	mnoho
majestátních	majestátní	k2eAgFnPc2d1	majestátní
prostor	prostora	k1gFnPc2	prostora
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
tam	tam	k6eAd1	tam
stroje	stroj	k1gInPc1	stroj
které	který	k3yRgFnPc1	který
přiváděly	přivádět	k5eAaImAgFnP	přivádět
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Eufrat	Eufrat	k1gInSc1	Eufrat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
skryto	skryt	k2eAgNnSc1d1	skryto
před	před	k7c7	před
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
potrubí	potrubí	k1gNnSc1	potrubí
vodu	voda	k1gFnSc4	voda
vyvádělo	vyvádět	k5eAaImAgNnS	vyvádět
stavbou	stavba	k1gFnSc7	stavba
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
Semiramidiny	Semiramidin	k2eAgFnSc2d1	Semiramidin
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
legendu	legenda	k1gFnSc4	legenda
a	a	k8xC	a
div	div	k1gInSc4	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
odpovídající	odpovídající	k2eAgInPc1d1	odpovídající
jejich	jejich	k3xOp3gFnSc1	jejich
popisu	popis	k1gInSc3	popis
zřejmě	zřejmě	k6eAd1	zřejmě
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
Robert	Robert	k1gMnSc1	Robert
Koldewey	Koldewea	k1gFnSc2	Koldewea
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
-	-	kIx~	-
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Visuté	visutý	k2eAgFnPc4d1	visutá
zahrady	zahrada	k1gFnPc4	zahrada
nad	nad	k7c7	nad
většími	veliký	k2eAgFnPc7d2	veliký
a	a	k8xC	a
nákladnějšími	nákladný	k2eAgFnPc7d2	nákladnější
stavbami	stavba	k1gFnPc7	stavba
vynikají	vynikat	k5eAaImIp3nP	vynikat
originalitou	originalita	k1gFnSc7	originalita
<g/>
,	,	kIx,	,
jedinečností	jedinečnost	k1gFnSc7	jedinečnost
<g/>
,	,	kIx,	,
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
řešením	řešení	k1gNnSc7	řešení
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
důmyslností	důmyslnost	k1gFnPc2	důmyslnost
čerpacího	čerpací	k2eAgNnSc2d1	čerpací
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
krásou	krása	k1gFnSc7	krása
<g/>
.	.	kIx.	.
</s>
<s>
Irácká	irácký	k2eAgFnSc1d1	irácká
Památková	památkový	k2eAgFnSc1d1	památková
správa	správa	k1gFnSc1	správa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
Semiramidiny	Semiramidin	k2eAgFnSc2d1	Semiramidin
visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Babylónu	Babylón	k1gInSc6	Babylón
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
základů	základ	k1gInPc2	základ
práce	práce	k1gFnSc2	práce
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
ukončila	ukončit	k5eAaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
<g/>
Popisovány	popisován	k2eAgInPc1d1	popisován
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
visuté	visutý	k2eAgFnPc4d1	visutá
zahrady	zahrada	k1gFnPc4	zahrada
v	v	k7c6	v
Ninive	Ninive	k1gNnSc6	Ninive
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
měl	mít	k5eAaImAgMnS	mít
dát	dát	k5eAaPmF	dát
vybudovat	vybudovat	k5eAaPmF	vybudovat
asyrský	asyrský	k2eAgMnSc1d1	asyrský
král	král	k1gMnSc1	král
Sinacherib	Sinacherib	k1gMnSc1	Sinacherib
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
města	město	k1gNnSc2	město
dobyl	dobýt	k5eAaPmAgInS	dobýt
i	i	k9	i
Babylon	Babylon	k1gInSc1	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
Ninive	Ninive	k1gNnSc1	Ninive
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Nový	nový	k2eAgInSc1d1	nový
Babylon	Babylon	k1gInSc1	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
Dalšímu	další	k2eAgInSc3d1	další
výzkumu	výzkum	k1gInSc3	výzkum
brání	bránit	k5eAaImIp3nS	bránit
válečný	válečný	k2eAgInSc4d1	válečný
stav	stav	k1gInSc4	stav
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Egypt	Egypt	k1gInSc1	Egypt
====	====	k?	====
</s>
</p>
<p>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
asi	asi	k9	asi
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc1	chrám
a	a	k8xC	a
kaple	kaple	k1gFnPc1	kaple
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
z	z	k7c2	z
hrobů	hrob	k1gInPc2	hrob
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
na	na	k7c6	na
malbách	malba	k1gFnPc6	malba
v	v	k7c6	v
hrobkách	hrobka	k1gFnPc6	hrobka
nachází	nacházet	k5eAaImIp3nS	nacházet
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vzhled	vzhled	k1gInSc1	vzhled
je	být	k5eAaImIp3nS	být
podrobně	podrobně	k6eAd1	podrobně
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
dost	dost	k6eAd1	dost
dobrý	dobrý	k2eAgInSc4d1	dobrý
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
období	období	k1gNnSc6	období
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
podle	podle	k7c2	podle
takového	takový	k3xDgNnSc2	takový
obrazového	obrazový	k2eAgNnSc2d1	obrazové
svědectví	svědectví	k1gNnSc2	svědectví
i	i	k8xC	i
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc2	vrchol
úpravy	úprava	k1gFnPc4	úprava
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
v	v	k7c6	v
letech	let	k1gInPc6	let
1530	[number]	k4	1530
<g/>
–	–	k?	–
<g/>
1350	[number]	k4	1350
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Součástí	součást	k1gFnSc7	součást
zahrady	zahrada	k1gFnSc2	zahrada
býval	bývat	k5eAaImAgInS	bývat
zavlažovací	zavlažovací	k2eAgInSc1d1	zavlažovací
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
rozděloval	rozdělovat	k5eAaImAgInS	rozdělovat
na	na	k7c6	na
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
bylo	být	k5eAaImAgNnS	být
základem	základ	k1gInSc7	základ
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
živé	živý	k2eAgInPc4d1	živý
ploty	plot	k1gInPc4	plot
ze	z	k7c2	z
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
dřevin	dřevina	k1gFnPc2	dřevina
ji	on	k3xPp3gFnSc4	on
dále	daleko	k6eAd2	daleko
členily	členit	k5eAaImAgFnP	členit
na	na	k7c4	na
pravoúhlé	pravoúhlý	k2eAgInPc4d1	pravoúhlý
obrazce	obrazec	k1gInPc4	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
zahrady	zahrada	k1gFnSc2	zahrada
býval	bývat	k5eAaImAgInS	bývat
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
různé	různý	k2eAgFnPc4d1	různá
velikosti	velikost	k1gFnPc4	velikost
podle	podle	k7c2	podle
zámožnosti	zámožnost	k1gFnSc2	zámožnost
majitele	majitel	k1gMnSc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Bazény	bazén	k1gInPc1	bazén
ve	v	k7c6	v
faraonových	faraonův	k2eAgFnPc6d1	faraonova
zahradách	zahrada	k1gFnPc6	zahrada
byly	být	k5eAaImAgFnP	být
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
projíždět	projíždět	k5eAaImF	projíždět
na	na	k7c4	na
plachetnici	plachetnice	k1gFnSc4	plachetnice
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
zahrad	zahrada	k1gFnPc2	zahrada
byly	být	k5eAaImAgInP	být
také	také	k9	také
vodotrysky	vodotrysk	k1gInPc1	vodotrysk
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgFnSc7d3	nejobvyklejší
součástí	součást	k1gFnSc7	součást
egyptských	egyptský	k2eAgFnPc2d1	egyptská
zahrad	zahrada	k1gFnPc2	zahrada
bylo	být	k5eAaImAgNnS	být
stinné	stinný	k2eAgNnSc1d1	stinné
stromořadí	stromořadí	k1gNnSc1	stromořadí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
egyptských	egyptský	k2eAgFnPc2d1	egyptská
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
byly	být	k5eAaImAgFnP	být
obehnány	obehnat	k5eAaPmNgFnP	obehnat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
zdí	zeď	k1gFnSc7	zeď
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
vchodem	vchod	k1gInSc7	vchod
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
nádrž	nádrž	k1gFnSc1	nádrž
a	a	k8xC	a
zahrada	zahrada	k1gFnSc1	zahrada
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
případě	případ	k1gInSc6	případ
měla	mít	k5eAaImAgFnS	mít
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
rybník	rybník	k1gInSc4	rybník
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
oblasti	oblast	k1gFnSc6	oblast
byla	být	k5eAaImAgFnS	být
obehnána	obehnat	k5eAaPmNgFnS	obehnat
zdmi	zeď	k1gFnPc7	zeď
a	a	k8xC	a
osázena	osázet	k5eAaPmNgFnS	osázet
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byly	být	k5eAaImAgFnP	být
patrně	patrně	k6eAd1	patrně
i	i	k8xC	i
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Persie	Persie	k1gFnSc2	Persie
====	====	k?	====
</s>
</p>
<p>
<s>
Perské	perský	k2eAgFnPc1d1	perská
zahrady	zahrada	k1gFnPc1	zahrada
mohou	moct	k5eAaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
počátku	počátek	k1gInSc3	počátek
období	období	k1gNnSc2	období
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
nl	nl	k?	nl
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
osovou	osový	k2eAgFnSc7d1	Osová
linií	linie	k1gFnSc7	linie
vytvářenou	vytvářený	k2eAgFnSc7d1	vytvářená
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
nebo	nebo	k8xC	nebo
tokem	tok	k1gInSc7	tok
a	a	k8xC	a
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
ráj	ráj	k1gInSc4	ráj
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
úprav	úprava	k1gFnPc2	úprava
zahrad	zahrada	k1gFnPc2	zahrada
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Zdobená	zdobený	k2eAgFnSc1d1	zdobená
keramika	keramika	k1gFnSc1	keramika
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
typický	typický	k2eAgInSc1d1	typický
plán	plán	k1gInSc1	plán
perské	perský	k2eAgFnSc2d1	perská
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Obrys	obrys	k1gInSc1	obrys
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c4	v
Pasargad	Pasargad	k1gInSc4	Pasargad
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
kolem	kolem	k7c2	kolem
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
nl	nl	k?	nl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
viditelný	viditelný	k2eAgInSc1d1	viditelný
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
stala	stát	k5eAaPmAgFnS	stát
důležitější	důležitý	k2eAgFnSc1d2	důležitější
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
v	v	k7c6	v
zahradního	zahradní	k2eAgNnSc2d1	zahradní
designu	design	k1gInSc3	design
větším	veliký	k2eAgInSc7d2	veliký
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
fontány	fontán	k1gInPc4	fontán
a	a	k8xC	a
rybníky	rybník	k1gInPc4	rybník
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Perská	perský	k2eAgFnSc1d1	perská
zahrada	zahrada	k1gFnSc1	zahrada
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
se	se	k3xPyFc4	se
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
vlivy	vliv	k1gInPc7	vliv
<g/>
,	,	kIx,	,
základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
ale	ale	k9	ale
byly	být	k5eAaImAgInP	být
jen	jen	k9	jen
málo	málo	k6eAd1	málo
změněny	změněn	k2eAgFnPc1d1	změněna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
arabské	arabský	k2eAgFnSc2d1	arabská
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
estetika	estetika	k1gFnSc1	estetika
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
rostla	růst	k5eAaImAgFnS	růst
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Chahā	Chahā	k1gFnSc1	Chahā
Bā	Bā	k1gFnSc1	Bā
(	(	kIx(	(
<g/>
چ	چ	k?	چ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
forma	forma	k1gFnSc1	forma
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
napodobit	napodobit	k5eAaPmF	napodobit
ráj	ráj	k1gInSc4	ráj
<g/>
,	,	kIx,	,
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
řekami	řeka	k1gFnPc7	řeka
a	a	k8xC	a
čtyřmi	čtyři	k4xCgInPc7	čtyři
kvadranty	kvadrant	k1gInPc7	kvadrant
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
někdy	někdy	k6eAd1	někdy
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
jedna	jeden	k4xCgFnSc1	jeden
osu	osa	k1gFnSc4	osa
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
příčné	příčný	k2eAgFnPc1d1	příčná
osy	osa	k1gFnPc1	osa
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
vodní	vodní	k2eAgInPc1d1	vodní
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Invaze	invaze	k1gFnSc1	invaze
Mongolů	Mongol	k1gMnPc2	Mongol
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
ve	v	k7c6	v
třináctém	třináctý	k4xOgNnSc6	třináctý
století	století	k1gNnSc6	století
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
využití	využití	k1gNnSc3	využití
dalších	další	k2eAgInPc2d1	další
ozdobných	ozdobný	k2eAgInPc2d1	ozdobný
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
říše	říše	k1gFnSc1	říše
zavedla	zavést	k5eAaPmAgFnS	zavést
perské	perský	k2eAgFnPc4d1	perská
zahrady	zahrada	k1gFnPc4	zahrada
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Novodobý	novodobý	k2eAgInSc1d1	novodobý
div	div	k1gInSc1	div
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
hrobka	hrobka	k1gFnSc1	hrobka
Tádž	Tádž	k1gFnSc1	Tádž
Mahal	Mahal	k1gInSc1	Mahal
je	být	k5eAaImIp3nS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
perské	perský	k2eAgFnSc2d1	perská
představy	představa	k1gFnSc2	představa
ideální	ideální	k2eAgFnSc2d1	ideální
rajské	rajský	k2eAgFnSc2d1	rajská
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
začala	začít	k5eAaPmAgFnS	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
Persii	Persie	k1gFnSc4	Persie
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
barokní	barokní	k2eAgFnPc4d1	barokní
úpravy	úprava	k1gFnPc4	úprava
tehdy	tehdy	k6eAd1	tehdy
běžné	běžný	k2eAgFnPc4d1	běžná
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
vlivy	vliv	k1gInPc1	vliv
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
využívání	využívání	k1gNnSc6	využívání
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
použitých	použitý	k2eAgFnPc2d1	použitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Japonsko	Japonsko	k1gNnSc1	Japonsko
====	====	k?	====
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
úpravy	úprava	k1gFnSc2	úprava
okrasné	okrasný	k2eAgFnSc2d1	okrasná
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c4	za
tradiční	tradiční	k2eAgFnPc4d1	tradiční
<g/>
,	,	kIx,	,
sahají	sahat	k5eAaImIp3nP	sahat
od	od	k7c2	od
animistických	animistický	k2eAgFnPc2d1	animistická
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Určující	určující	k2eAgFnSc1d1	určující
zde	zde	k6eAd1	zde
dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgInP	být
čínské	čínský	k2eAgInPc1d1	čínský
a	a	k8xC	a
korejské	korejský	k2eAgInPc1d1	korejský
vlivy	vliv	k1gInPc1	vliv
z	z	k7c2	z
období	období	k1gNnSc2	období
Nara	Nar	k1gInSc2	Nar
(	(	kIx(	(
<g/>
710	[number]	k4	710
-	-	kIx~	-
794	[number]	k4	794
<g/>
)	)	kIx)	)
a	a	k8xC	a
Heian	Heian	k1gMnSc1	Heian
(	(	kIx(	(
<g/>
794	[number]	k4	794
-	-	kIx~	-
1185	[number]	k4	1185
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
napodobování	napodobování	k1gNnSc2	napodobování
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
japonská	japonský	k2eAgFnSc1d1	japonská
zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
mírně	mírně	k6eAd1	mírně
odchylnou	odchylný	k2eAgFnSc4d1	odchylná
estetiku	estetika	k1gFnSc4	estetika
ovlivněnou	ovlivněný	k2eAgFnSc4d1	ovlivněná
příchodem	příchod	k1gInSc7	příchod
zenového	zenový	k2eAgInSc2d1	zenový
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silný	silný	k2eAgInSc1d1	silný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
měl	mít	k5eAaImAgInS	mít
především	především	k6eAd1	především
čajový	čajový	k2eAgInSc1d1	čajový
obřad	obřad	k1gInSc1	obřad
(	(	kIx(	(
<g/>
ča-no-ju	čaou	k5eAaPmIp1nS	ča-no-ju
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgInSc1d1	výchozí
čínský	čínský	k2eAgInSc1d1	čínský
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
staletí	staletí	k1gNnPc4	staletí
Japonci	Japonec	k1gMnPc1	Japonec
kopírován	kopírován	k2eAgMnSc1d1	kopírován
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
jasně	jasně	k6eAd1	jasně
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
v	v	k7c6	v
použitých	použitý	k2eAgInPc6d1	použitý
prvcích	prvek	k1gInPc6	prvek
i	i	k8xC	i
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jsou	být	k5eAaImIp3nP	být
upraveny	upravit	k5eAaPmNgInP	upravit
jako	jako	k9	jako
parky	park	k1gInPc4	park
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
jak	jak	k8xC	jak
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
přírodně	přírodně	k6eAd1	přírodně
krajinářské	krajinářský	k2eAgFnPc4d1	krajinářská
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnPc4	zahrada
rezidenční	rezidenční	k2eAgFnPc4d1	rezidenční
a	a	k8xC	a
chrámové	chrámový	k2eAgFnPc4d1	chrámová
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgFnPc4d1	soukromá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
formu	forma	k1gFnSc4	forma
zahrad	zahrada	k1gFnPc2	zahrada
některými	některý	k3yIgMnPc7	některý
autory	autor	k1gMnPc7	autor
bývají	bývat	k5eAaImIp3nP	bývat
považovány	považován	k2eAgFnPc1d1	považována
výsadby	výsadba	k1gFnPc1	výsadba
v	v	k7c6	v
mísách	mísa	k1gFnPc6	mísa
(	(	kIx(	(
<g/>
bonseki	bonseki	k6eAd1	bonseki
a	a	k8xC	a
bonkei	bonkei	k6eAd1	bonkei
<g/>
)	)	kIx)	)
a	a	k8xC	a
bonsaje	bonsaj	k1gFnPc1	bonsaj
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
suiseki	suiseki	k6eAd1	suiseki
(	(	kIx(	(
<g/>
水	水	k?	水
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
umělecky	umělecky	k6eAd1	umělecky
ztvárněných	ztvárněný	k2eAgInPc6d1	ztvárněný
kamenech	kámen	k1gInPc6	kámen
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
kompozicích	kompozice	k1gFnPc6	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
však	však	k9	však
spíše	spíše	k9	spíše
přenesený	přenesený	k2eAgInSc1d1	přenesený
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasické	klasický	k2eAgFnPc1d1	klasická
japonské	japonský	k2eAgFnPc1d1	japonská
zahrady	zahrada	k1gFnPc1	zahrada
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
tvarovány	tvarovat	k5eAaImNgFnP	tvarovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
náboženských	náboženský	k2eAgInPc2d1	náboženský
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Vytváření	vytváření	k1gNnSc1	vytváření
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
má	mít	k5eAaImIp3nS	mít
starou	starý	k2eAgFnSc4d1	stará
historii	historie	k1gFnSc4	historie
ovlivněnou	ovlivněný	k2eAgFnSc4d1	ovlivněná
šintoismem	šintoismus	k1gInSc7	šintoismus
<g/>
,	,	kIx,	,
buddhismem	buddhismus	k1gInSc7	buddhismus
a	a	k8xC	a
taoistickou	taoistický	k2eAgFnSc7d1	taoistická
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byla	být	k5eAaImAgFnS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zahrada	zahrada	k1gFnSc1	zahrada
otázkou	otázka	k1gFnSc7	otázka
prestiže	prestiž	k1gFnSc2	prestiž
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zbohatlík	zbohatlík	k1gMnSc1	zbohatlík
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
si	se	k3xPyFc3	se
až	až	k9	až
do	do	k7c2	do
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
nepořizoval	pořizovat	k5eNaImAgInS	pořizovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
automobily	automobil	k1gInPc1	automobil
a	a	k8xC	a
obrovské	obrovský	k2eAgInPc1d1	obrovský
domy	dům	k1gInPc1	dům
s	s	k7c7	s
hejny	hejno	k1gNnPc7	hejno
sluhů	sluha	k1gMnPc2	sluha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvětšoval	zvětšovat	k5eAaImAgMnS	zvětšovat
a	a	k8xC	a
zlepšoval	zlepšovat	k5eAaImAgMnS	zlepšovat
svou	svůj	k3xOyFgFnSc4	svůj
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
získával	získávat	k5eAaImAgMnS	získávat
novou	nova	k1gFnSc7	nova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Japonské	japonský	k2eAgFnPc1d1	japonská
zahrady	zahrada	k1gFnPc1	zahrada
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
krásné	krásný	k2eAgFnPc4d1	krásná
po	po	k7c4	po
všechny	všechen	k3xTgFnPc4	všechen
čtyři	čtyři	k4xCgFnPc4	čtyři
roční	roční	k2eAgFnPc4d1	roční
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Japonce	Japonec	k1gMnPc4	Japonec
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
zahradě	zahrada	k1gFnSc6	zahrada
stejně	stejně	k6eAd1	stejně
krásná	krásný	k2eAgFnSc1d1	krásná
jako	jako	k8xS	jako
jaro	jaro	k1gNnSc1	jaro
nebo	nebo	k8xC	nebo
léto	léto	k1gNnSc1	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Čína	Čína	k1gFnSc1	Čína
====	====	k?	====
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
zahrady	zahrada	k1gFnSc2	zahrada
typický	typický	k2eAgInSc4d1	typický
pro	pro	k7c4	pro
čínský	čínský	k2eAgInSc4d1	čínský
region	region	k1gInSc4	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
zahradě	zahrada	k1gFnSc6	zahrada
jsou	být	k5eAaImIp3nP	být
určujícími	určující	k2eAgInPc7d1	určující
prvky	prvek	k1gInPc7	prvek
modelace	modelace	k1gFnSc2	modelace
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
dekorativní	dekorativní	k2eAgInPc1d1	dekorativní
stavební	stavební	k2eAgInPc1d1	stavební
doplňky	doplněk	k1gInPc1	doplněk
<g/>
,	,	kIx,	,
použité	použitý	k2eAgInPc1d1	použitý
substráty	substrát	k1gInPc1	substrát
<g/>
,	,	kIx,	,
kameny	kámen	k1gInPc1	kámen
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
zde	zde	k6eAd1	zde
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
zásadním	zásadní	k2eAgInSc7d1	zásadní
prvkem	prvek	k1gInSc7	prvek
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
zahrady	zahrada	k1gFnPc1	zahrada
jsou	být	k5eAaImIp3nP	být
zhotovovány	zhotovován	k2eAgFnPc1d1	zhotovována
jako	jako	k8xS	jako
myšlené	myšlený	k2eAgNnSc1d1	myšlené
zobrazení	zobrazení	k1gNnSc1	zobrazení
vesmíru	vesmír	k1gInSc2	vesmír
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
zahrady	zahrada	k1gFnPc1	zahrada
mají	mít	k5eAaImIp3nP	mít
úzkou	úzký	k2eAgFnSc4d1	úzká
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
celek	celek	k1gInSc1	celek
a	a	k8xC	a
části	část	k1gFnPc1	část
zahrady	zahrada	k1gFnSc2	zahrada
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
interpretovány	interpretován	k2eAgInPc1d1	interpretován
jako	jako	k8xS	jako
symboly	symbol	k1gInPc1	symbol
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
i	i	k9	i
sociálním	sociální	k2eAgNnSc7d1	sociální
postavením	postavení	k1gNnSc7	postavení
majitele	majitel	k1gMnSc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
způsobem	způsob	k1gInSc7	způsob
jak	jak	k6eAd1	jak
rafinovaně	rafinovaně	k6eAd1	rafinovaně
dát	dát	k5eAaPmF	dát
najevo	najevo	k6eAd1	najevo
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
organizační	organizační	k2eAgFnPc4d1	organizační
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
vytříbenost	vytříbenost	k1gFnSc4	vytříbenost
vkusu	vkus	k1gInSc2	vkus
<g/>
,	,	kIx,	,
zámožnost	zámožnost	k1gFnSc4	zámožnost
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
čínských	čínský	k2eAgFnPc2d1	čínská
zahrad	zahrada	k1gFnPc2	zahrada
byly	být	k5eAaImAgFnP	být
luxusním	luxusní	k2eAgNnSc7d1	luxusní
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
a	a	k8xC	a
posílily	posílit	k5eAaPmAgFnP	posílit
postavení	postavení	k1gNnSc4	postavení
jejich	jejich	k3xOp3gMnPc2	jejich
majitelů	majitel	k1gMnPc2	majitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Antika	antika	k1gFnSc1	antika
====	====	k?	====
</s>
</p>
<p>
<s>
Antické	antický	k2eAgFnPc1d1	antická
zahrady	zahrada	k1gFnPc1	zahrada
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
vlivů	vliv	k1gInPc2	vliv
orientálních	orientální	k2eAgFnPc2d1	orientální
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
jako	jako	k9	jako
obytné	obytný	k2eAgFnPc1d1	obytná
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgFnPc1d1	římská
zahrady	zahrada	k1gFnPc1	zahrada
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
díky	díky	k7c3	díky
osobitému	osobitý	k2eAgInSc3d1	osobitý
vkusu	vkus	k1gInSc3	vkus
Římanů	Říman	k1gMnPc2	Říman
vrcholnými	vrcholný	k2eAgInPc7d1	vrcholný
zahradnickými	zahradnický	k2eAgInPc7d1	zahradnický
díly	díl	k1gInPc7	díl
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
jasně	jasně	k6eAd1	jasně
odlišovali	odlišovat	k5eAaImAgMnP	odlišovat
stylově	stylově	k6eAd1	stylově
<g/>
,	,	kIx,	,
umístěním	umístění	k1gNnSc7	umístění
<g/>
,	,	kIx,	,
skladbou	skladba	k1gFnSc7	skladba
a	a	k8xC	a
použitím	použití	k1gNnSc7	použití
zeleninové	zeleninový	k2eAgFnSc2d1	zeleninová
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
užitkové	užitkový	k2eAgFnSc2d1	užitková
sady	sada	k1gFnSc2	sada
<g/>
,	,	kIx,	,
výsadby	výsadba	k1gFnPc4	výsadba
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
venkovských	venkovský	k2eAgFnPc2d1	venkovská
vil	vila	k1gFnPc2	vila
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
parky	park	k1gInPc4	park
a	a	k8xC	a
malé	malý	k2eAgFnPc4d1	malá
městské	městský	k2eAgFnPc4d1	městská
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
známé	známý	k2eAgFnPc1d1	známá
městské	městský	k2eAgFnPc1d1	městská
zahrady	zahrada	k1gFnPc1	zahrada
měly	mít	k5eAaImAgFnP	mít
často	často	k6eAd1	často
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
jako	jako	k8xC	jako
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
<g/>
,	,	kIx,	,
geometrické	geometrický	k2eAgFnPc4d1	geometrická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
zahrady	zahrada	k1gFnSc2	zahrada
jako	jako	k8xC	jako
součásti	součást	k1gFnSc2	součást
obydlí	obydlí	k1gNnSc1	obydlí
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
používanější	používaný	k2eAgMnSc1d2	používanější
než	než	k8xS	než
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
i	i	k9	i
spávalo	spávat	k5eAaImAgNnS	spávat
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
pravidelně	pravidelně	k6eAd1	pravidelně
členěného	členěný	k2eAgInSc2d1	členěný
peristylu	peristyl	k1gInSc2	peristyl
stála	stát	k5eAaImAgFnS	stát
kašna	kašna	k1gFnSc1	kašna
anebo	anebo	k8xC	anebo
vodotrysk	vodotrysk	k1gInSc1	vodotrysk
<g/>
,	,	kIx,	,
cesty	cesta	k1gFnPc1	cesta
byly	být	k5eAaImAgFnP	být
dlážděné	dlážděný	k2eAgFnPc1d1	dlážděná
<g/>
,	,	kIx,	,
lemované	lemovaný	k2eAgInPc1d1	lemovaný
květinovými	květinový	k2eAgInPc7d1	květinový
záhony	záhon	k1gInPc7	záhon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
architektonický	architektonický	k2eAgInSc4d1	architektonický
styl	styl	k1gInSc4	styl
římské	římský	k2eAgFnSc2d1	římská
zahrady	zahrada	k1gFnSc2	zahrada
výrazně	výrazně	k6eAd1	výrazně
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
zahradní	zahradní	k2eAgInPc4d1	zahradní
styly	styl	k1gInPc4	styl
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
parky	park	k1gInPc1	park
měly	mít	k5eAaImAgInP	mít
části	část	k1gFnPc4	část
podobné	podobný	k2eAgNnSc4d1	podobné
holandským	holandský	k2eAgFnPc3d1	holandská
a	a	k8xC	a
francouzským	francouzský	k2eAgFnPc3d1	francouzská
barokním	barokní	k2eAgFnPc3d1	barokní
zahradám	zahrada	k1gFnPc3	zahrada
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
popisů	popis	k1gInPc2	popis
zahrady	zahrada	k1gFnSc2	zahrada
nápadně	nápadně	k6eAd1	nápadně
podobné	podobný	k2eAgInPc1d1	podobný
přírodně	přírodně	k6eAd1	přírodně
krajinářským	krajinářský	k2eAgInPc3d1	krajinářský
parkům	park	k1gInPc3	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okrasných	okrasný	k2eAgFnPc6d1	okrasná
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nacházely	nacházet	k5eAaImAgFnP	nacházet
ovocné	ovocný	k2eAgFnPc4d1	ovocná
dřeviny	dřevina	k1gFnPc4	dřevina
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
u	u	k7c2	u
každého	každý	k3xTgNnSc2	každý
obydlí	obydlí	k1gNnSc2	obydlí
byla	být	k5eAaImAgFnS	být
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
stav	stav	k1gInSc4	stav
zodpovídala	zodpovídat	k5eAaPmAgFnS	zodpovídat
hospodyně	hospodyně	k1gFnSc1	hospodyně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pěstování	pěstování	k1gNnSc6	pěstování
květin	květina	k1gFnPc2	květina
i	i	k8xC	i
zeleniny	zelenina	k1gFnPc4	zelenina
Římané	Říman	k1gMnPc1	Říman
například	například	k6eAd1	například
používali	používat	k5eAaImAgMnP	používat
rychlení	rychlení	k1gNnSc4	rychlení
a	a	k8xC	a
oddalování	oddalování	k1gNnSc4	oddalování
kvetení	kvetení	k1gNnSc2	kvetení
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
parků	park	k1gInPc2	park
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byla	být	k5eAaImAgFnS	být
také	také	k9	také
výrazem	výraz	k1gInSc7	výraz
politické	politický	k2eAgFnSc2d1	politická
prestiže	prestiž	k1gFnSc2	prestiž
zadavatele	zadavatel	k1gMnSc2	zadavatel
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
politického	politický	k2eAgInSc2d1	politický
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
soukromé	soukromý	k2eAgFnPc1d1	soukromá
okrasné	okrasný	k2eAgFnPc1d1	okrasná
zahrady	zahrada	k1gFnPc1	zahrada
byly	být	k5eAaImAgFnP	být
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
jako	jako	k8xS	jako
projev	projev	k1gInSc4	projev
marnivosti	marnivost	k1gFnSc2	marnivost
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
vrstvami	vrstva	k1gFnPc7	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
omezováno	omezovat	k5eAaImNgNnS	omezovat
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
v	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
typickém	typický	k2eAgInSc6d1	typický
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
renesance	renesance	k1gFnSc2	renesance
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
se	se	k3xPyFc4	se
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byly	být	k5eAaImAgFnP	být
zahrady	zahrada	k1gFnPc1	zahrada
upraveny	upravit	k5eAaPmNgFnP	upravit
rozdílně	rozdílně	k6eAd1	rozdílně
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vysledovat	vysledovat	k5eAaPmF	vysledovat
podobnosti	podobnost	k1gFnPc4	podobnost
<g/>
,	,	kIx,	,
dané	daný	k2eAgInPc4d1	daný
především	především	k9	především
kontextem	kontext	k1gInSc7	kontext
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
výukovým	výukový	k2eAgInPc3d1	výukový
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
k	k	k7c3	k
reprezentaci	reprezentace	k1gFnSc3	reprezentace
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc3	pěstování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
volnočasovým	volnočasův	k2eAgFnPc3d1	volnočasův
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
takto	takto	k6eAd1	takto
zaměřeny	zaměřit	k5eAaPmNgFnP	zaměřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středověké	středověký	k2eAgFnPc4d1	středověká
zahrady	zahrada	k1gFnPc4	zahrada
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
obezděné	obezděný	k2eAgFnPc1d1	obezděná
geometrické	geometrický	k2eAgFnPc4d1	geometrická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnPc4d1	vyznačující
se	se	k3xPyFc4	se
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
symetrií	symetrie	k1gFnSc7	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
takto	takto	k6eAd1	takto
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
egyptská	egyptský	k2eAgFnSc1d1	egyptská
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
a	a	k8xC	a
perská	perský	k2eAgFnSc1d1	perská
zahrada	zahrada	k1gFnSc1	zahrada
zjevně	zjevně	k6eAd1	zjevně
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
zdmi	zeď	k1gFnPc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
namalovány	namalován	k2eAgInPc4d1	namalován
zjednodušené	zjednodušený	k2eAgInPc4d1	zjednodušený
motivy	motiv	k1gInPc4	motiv
nebo	nebo	k8xC	nebo
krajiny	krajina	k1gFnPc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
(	(	kIx(	(
<g/>
víceméně	víceméně	k9	víceméně
<g/>
)	)	kIx)	)
okrasná	okrasný	k2eAgFnSc1d1	okrasná
zahrada	zahrada	k1gFnSc1	zahrada
byla	být	k5eAaImAgFnS	být
někdy	někdy	k6eAd1	někdy
nazývána	nazývat	k5eAaImNgFnS	nazývat
Hortus	hortus	k1gInSc1	hortus
Conclusus	Conclusus	k1gInSc4	Conclusus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
termín	termín	k1gInSc1	termín
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
jako	jako	k8xC	jako
květinové	květinový	k2eAgInPc1d1	květinový
dary	dar	k1gInPc1	dar
u	u	k7c2	u
oltářů	oltář	k1gInPc2	oltář
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
léčivky	léčivka	k1gFnSc2	léčivka
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgFnPc1d1	skutečná
okrasné	okrasný	k2eAgFnPc1d1	okrasná
zahrady	zahrada	k1gFnPc1	zahrada
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
hříšné	hříšný	k2eAgNnSc4d1	hříšné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
platilo	platit	k5eAaImAgNnS	platit
obecné	obecný	k2eAgNnSc1d1	obecné
pravidlo	pravidlo	k1gNnSc1	pravidlo
ozdoba	ozdoba	k1gFnSc1	ozdoba
je	být	k5eAaImIp3nS	být
hřích	hřích	k1gInSc4	hřích
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
Hortus	hortus	k1gInSc1	hortus
Conclusus	Conclusus	k1gInSc1	Conclusus
bývala	bývat	k5eAaImAgFnS	bývat
zobrazením	zobrazení	k1gNnSc7	zobrazení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc1d1	boží
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
symboliky	symbolika	k1gFnSc2	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
panenství	panenství	k1gNnSc2	panenství
Ježíšovy	Ježíšův	k2eAgFnSc2d1	Ježíšova
matky	matka	k1gFnSc2	matka
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
středověkém	středověký	k2eAgInSc6d1	středověký
a	a	k8xC	a
renesančním	renesanční	k2eAgNnSc6d1	renesanční
umění	umění	k1gNnSc6	umění
byl	být	k5eAaImAgInS	být
opakovaně	opakovaně	k6eAd1	opakovaně
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
znázorňovaný	znázorňovaný	k2eAgInSc1d1	znázorňovaný
princip	princip	k1gInSc1	princip
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
náboženství	náboženství	k1gNnSc1	náboženství
postavu	postav	k1gInSc2	postav
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ideologicky	ideologicky	k6eAd1	ideologicky
propaguje	propagovat	k5eAaImIp3nS	propagovat
jako	jako	k9	jako
vzor	vzor	k1gInSc1	vzor
ženství	ženství	k1gNnSc2	ženství
a	a	k8xC	a
neposkvrněnosti	neposkvrněnost	k1gFnSc2	neposkvrněnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
dobrovolného	dobrovolný	k2eAgNnSc2d1	dobrovolné
trvale	trvale	k6eAd1	trvale
nesexuálního	sexuální	k2eNgNnSc2d1	nesexuální
chování	chování	k1gNnSc2	chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
obezděné	obezděný	k2eAgFnPc1d1	obezděná
byly	být	k5eAaImAgFnP	být
už	už	k9	už
nejstarší	starý	k2eAgFnPc1d3	nejstarší
egyptské	egyptský	k2eAgFnPc1d1	egyptská
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
obezdění	obezdění	k1gNnSc1	obezdění
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
symbolický	symbolický	k2eAgInSc4d1	symbolický
<g/>
,	,	kIx,	,
mystický	mystický	k2eAgInSc4d1	mystický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
symbolů	symbol	k1gInPc2	symbol
se	se	k3xPyFc4	se
Hortus	hortus	k1gInSc1	hortus
Conclusus	Conclusus	k1gInSc1	Conclusus
přiblížilo	přiblížit	k5eAaPmAgNnS	přiblížit
čínským	čínský	k2eAgInPc3d1	čínský
a	a	k8xC	a
japonským	japonský	k2eAgInPc3d1	japonský
zahradním	zahradní	k2eAgInPc3d1	zahradní
stylům	styl	k1gInPc3	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Renesance	renesance	k1gFnSc2	renesance
===	===	k?	===
</s>
</p>
<p>
<s>
Renesanční	renesanční	k2eAgFnPc1d1	renesanční
zahradní	zahradní	k2eAgFnPc1d1	zahradní
úpravy	úprava	k1gFnPc1	úprava
jsou	být	k5eAaImIp3nP	být
symetricky	symetricky	k6eAd1	symetricky
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
forma	forma	k1gFnSc1	forma
použitá	použitý	k2eAgFnSc1d1	použitá
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
další	další	k2eAgFnSc4d1	další
budoucnost	budoucnost	k1gFnSc4	budoucnost
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
italské	italský	k2eAgFnSc2d1	italská
zahrady	zahrada	k1gFnSc2	zahrada
od	od	k7c2	od
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
po	po	k7c4	po
baroko	baroko	k1gNnSc4	baroko
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgNnSc1d1	zásadní
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
vztahu	vztah	k1gInSc2	vztah
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
zahrady	zahrada	k1gFnSc2	zahrada
(	(	kIx(	(
<g/>
Leon	Leona	k1gFnPc2	Leona
Battista	Battista	k1gMnSc1	Battista
Alberti	Albert	k1gMnPc1	Albert
to	ten	k3xDgNnSc1	ten
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
De	De	k?	De
re	re	k9	re
aedificatoria	aedificatorium	k1gNnSc2	aedificatorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevýznačnější	význačný	k2eNgFnSc7d2	význačný
slohovou	slohový	k2eAgFnSc7d1	slohová
úpravou	úprava	k1gFnSc7	úprava
italského	italský	k2eAgInSc2d1	italský
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
italská	italský	k2eAgFnSc1d1	italská
terasovitá	terasovitý	k2eAgFnSc1d1	terasovitá
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
"	"	kIx"	"
a	a	k8xC	a
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xS	jako
styl	styl	k1gInSc1	styl
italské	italský	k2eAgFnSc2d1	italská
zahrady	zahrada	k1gFnSc2	zahrada
(	(	kIx(	(
<g/>
parku	park	k1gInSc2	park
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
se	s	k7c7	s
způsoby	způsob	k1gInPc7	způsob
úpravy	úprava	k1gFnSc2	úprava
italského	italský	k2eAgInSc2d1	italský
parku	park	k1gInSc2	park
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
terasovitá	terasovitý	k2eAgFnSc1d1	terasovitá
zahrada	zahrada	k1gFnSc1	zahrada
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
zejména	zejména	k9	zejména
na	na	k7c6	na
kopcích	kopec	k1gInPc6	kopec
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Arno	Arno	k6eAd1	Arno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
inspirována	inspirován	k2eAgFnSc1d1	inspirována
římskými	římský	k2eAgFnPc7d1	římská
a	a	k8xC	a
neapolskými	neapolský	k2eAgFnPc7d1	neapolská
zahradami	zahrada	k1gFnPc7	zahrada
a	a	k8xC	a
spojovala	spojovat	k5eAaImAgFnS	spojovat
jejich	jejich	k3xOp3gInSc4	jejich
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
rysem	rys	k1gInSc7	rys
stylu	styl	k1gInSc2	styl
italské	italský	k2eAgFnSc2d1	italská
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
převaha	převaha	k1gFnSc1	převaha
architektonických	architektonický	k2eAgInPc2d1	architektonický
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
svažitý	svažitý	k2eAgInSc4d1	svažitý
terén	terén	k1gInSc4	terén
<g/>
.	.	kIx.	.
</s>
<s>
Zahradě	zahrada	k1gFnSc3	zahrada
dominovala	dominovat	k5eAaImAgFnS	dominovat
budova	budova	k1gFnSc1	budova
zpravidla	zpravidla	k6eAd1	zpravidla
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Italské	italský	k2eAgFnPc1d1	italská
zahrady	zahrada	k1gFnPc1	zahrada
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
úrovně	úroveň	k1gFnPc4	úroveň
v	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xC	jako
obytný	obytný	k2eAgInSc1d1	obytný
prostor	prostor	k1gInSc1	prostor
budovaný	budovaný	k2eAgInSc1d1	budovaný
podle	podle	k7c2	podle
stejných	stejný	k2eAgNnPc2d1	stejné
hledisek	hledisko	k1gNnPc2	hledisko
jako	jako	k8xS	jako
celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
půdorys	půdorys	k1gInSc4	půdorys
navazuje	navazovat	k5eAaImIp3nS	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvorech	dvůr	k1gInPc6	dvůr
panovníků	panovník	k1gMnPc2	panovník
nesloužila	sloužit	k5eNaImAgFnS	sloužit
jen	jen	k6eAd1	jen
obytným	obytný	k2eAgInPc3d1	obytný
a	a	k8xC	a
dekoračním	dekorační	k2eAgInPc3d1	dekorační
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k8xS	jako
sbírka	sbírka	k1gFnSc1	sbírka
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
zoologická	zoologický	k2eAgFnSc1d1	zoologická
sbírka	sbírka	k1gFnSc1	sbírka
cizokrajných	cizokrajný	k2eAgMnPc2d1	cizokrajný
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
hříček	hříčka	k1gFnPc2	hříčka
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
zahrada	zahrada	k1gFnSc1	zahrada
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zvrhla	zvrhnout	k5eAaPmAgFnS	zvrhnout
v	v	k7c4	v
chaotickou	chaotický	k2eAgFnSc4d1	chaotická
změť	změť	k1gFnSc4	změť
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
architektonicky	architektonicky	k6eAd1	architektonicky
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
zídkami	zídka	k1gFnPc7	zídka
<g/>
,	,	kIx,	,
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
a	a	k8xC	a
živými	živý	k2eAgInPc7d1	živý
ploty	plot	k1gInPc7	plot
rozčlenit	rozčlenit	k5eAaPmF	rozčlenit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
sekce	sekce	k1gFnPc4	sekce
<g/>
.	.	kIx.	.
</s>
<s>
Půdorys	půdorys	k1gInSc1	půdorys
zahrady	zahrada	k1gFnSc2	zahrada
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
šachovnicovou	šachovnicový	k2eAgFnSc7d1	šachovnicová
skládankou	skládanka	k1gFnSc7	skládanka
<g/>
.	.	kIx.	.
</s>
<s>
Řady	řad	k1gInPc4	řad
monotónně	monotónně	k6eAd1	monotónně
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgInPc2d1	opakující
čtverců	čtverec	k1gInPc2	čtverec
se	se	k3xPyFc4	se
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
jen	jen	k9	jen
ornamenty	ornament	k1gInPc1	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
čtyřúhelníku	čtyřúhelník	k1gInSc2	čtyřúhelník
má	mít	k5eAaImIp3nS	mít
nejrůznější	různý	k2eAgFnSc1d3	nejrůznější
náplň	náplň	k1gFnSc1	náplň
<g/>
.	.	kIx.	.
–	–	k?	–
trávník	trávník	k1gInSc4	trávník
<g/>
,	,	kIx,	,
užitkové	užitkový	k2eAgInPc4d1	užitkový
záhony	záhon	k1gInPc4	záhon
<g/>
,	,	kIx,	,
drť	drť	k1gFnSc4	drť
<g/>
,	,	kIx,	,
stříhané	stříhaný	k2eAgInPc4d1	stříhaný
keře	keř	k1gInPc4	keř
<g/>
,	,	kIx,	,
bylinkářskou	bylinkářský	k2eAgFnSc4d1	bylinkářská
zahrádku	zahrádka	k1gFnSc4	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
století	století	k1gNnSc6	století
zahrady	zahrada	k1gFnSc2	zahrada
podléhají	podléhat	k5eAaImIp3nP	podléhat
přísnějšímu	přísný	k2eAgNnSc3d2	přísnější
geometrickému	geometrický	k2eAgNnSc3d1	geometrické
formování	formování	k1gNnSc3	formování
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
objev	objev	k1gInSc1	objev
vlivu	vliv	k1gInSc2	vliv
architektury	architektura	k1gFnSc2	architektura
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c4	na
kvality	kvalita	k1gFnPc4	kvalita
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
objevovány	objevován	k2eAgInPc4d1	objevován
vlivy	vliv	k1gInPc4	vliv
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc4	pravidlo
stavebních	stavební	k2eAgInPc2d1	stavební
a	a	k8xC	a
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křížení	křížení	k1gNnSc1	křížení
cest	cesta	k1gFnPc2	cesta
akcentuje	akcentovat	k5eAaImIp3nS	akcentovat
fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tvarovaný	tvarovaný	k2eAgInSc1d1	tvarovaný
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Nejnáročněji	náročně	k6eAd3	náročně
se	se	k3xPyFc4	se
upravuje	upravovat	k5eAaImIp3nS	upravovat
giardinetto	giardinetto	k1gNnSc1	giardinetto
na	na	k7c6	na
terase	terasa	k1gFnSc6	terasa
před	před	k7c7	před
lodžií	lodžie	k1gFnSc7	lodžie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
salou	salat	k5eAaBmIp3nP	salat
terrenou	terrená	k1gFnSc4	terrená
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
ose	osa	k1gFnSc6	osa
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
osou	osa	k1gFnSc7	osa
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
osy	osa	k1gFnSc2	osa
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
casino	casino	k1gNnSc1	casino
<g/>
,	,	kIx,	,
grotta	grotta	k1gFnSc1	grotta
s	s	k7c7	s
kaskádou	kaskáda	k1gFnSc7	kaskáda
a	a	k8xC	a
fontána	fontána	k1gFnSc1	fontána
v	v	k7c6	v
náročně	náročně	k6eAd1	náročně
řešené	řešený	k2eAgFnSc6d1	řešená
nice	nika	k1gFnSc6	nika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
theatron	theatron	k1gInSc1	theatron
<g/>
,	,	kIx,	,
rybníčky	rybníček	k1gInPc1	rybníček
<g/>
,	,	kIx,	,
štěpnice	štěpnice	k1gFnPc1	štěpnice
<g/>
,	,	kIx,	,
ptáčnice	ptáčnice	k1gFnPc1	ptáčnice
<g/>
,	,	kIx,	,
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
a	a	k8xC	a
stromovky	stromovka	k1gFnPc1	stromovka
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
na	na	k7c4	na
obvod	obvod	k1gInSc4	obvod
reprezentační	reprezentační	k2eAgFnSc2d1	reprezentační
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
přechod	přechod	k1gInSc4	přechod
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostora	k1gFnPc1	prostora
propojují	propojovat	k5eAaImIp3nP	propojovat
loubí	loubí	k1gNnSc1	loubí
<g/>
,	,	kIx,	,
pergoly	pergola	k1gFnPc1	pergola
a	a	k8xC	a
aleje	alej	k1gFnPc1	alej
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vinicím	vinice	k1gFnPc3	vinice
a	a	k8xC	a
polím	pole	k1gFnPc3	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
příkladů	příklad	k1gInPc2	příklad
italské	italský	k2eAgFnSc2d1	italská
renesanční	renesanční	k2eAgFnSc2d1	renesanční
zahrady	zahrada	k1gFnSc2	zahrada
jsou	být	k5eAaImIp3nP	být
zahrady	zahrada	k1gFnPc1	zahrada
Boboli	Bobole	k1gFnSc3	Bobole
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
u	u	k7c2	u
paláce	palác	k1gInSc2	palác
Pitti	Pitť	k1gFnSc2	Pitť
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Baroko	baroko	k1gNnSc1	baroko
===	===	k?	===
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
barokních	barokní	k2eAgInPc2d1	barokní
italských	italský	k2eAgInPc2d1	italský
a	a	k8xC	a
francouzských	francouzský	k2eAgInPc2d1	francouzský
parků	park	k1gInPc2	park
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
příkladem	příklad	k1gInSc7	příklad
vzoru	vzor	k1gInSc2	vzor
barokních	barokní	k2eAgFnPc2d1	barokní
zahrad	zahrada	k1gFnPc2	zahrada
je	být	k5eAaImIp3nS	být
úprava	úprava	k1gFnSc1	úprava
u	u	k7c2	u
paláce	palác	k1gInSc2	palác
Pitti	Pitť	k1gFnSc2	Pitť
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
tvorby	tvorba	k1gFnSc2	tvorba
francouzských	francouzský	k2eAgInPc2d1	francouzský
parků	park	k1gInPc2	park
byl	být	k5eAaImAgInS	být
park	park	k1gInSc1	park
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
Versailles	Versailles	k1gFnSc2	Versailles
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pro	pro	k7c4	pro
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
architekt	architekt	k1gMnSc1	architekt
André	André	k1gMnSc1	André
Le	Le	k1gMnSc1	Le
Nôtre	Nôtr	k1gMnSc5	Nôtr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typicky	typicky	k6eAd1	typicky
park	park	k1gInSc1	park
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
několik	několik	k4yIc4	několik
hlavních	hlavní	k2eAgFnPc2d1	hlavní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
pravoúhle	pravoúhle	k6eAd1	pravoúhle
dělí	dělit	k5eAaImIp3nS	dělit
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
lemovány	lemovat	k5eAaImNgInP	lemovat
nízkými	nízký	k2eAgFnPc7d1	nízká
živými	živá	k1gFnPc7	živá
plůtky	plůtek	k1gInPc4	plůtek
ze	z	k7c2	z
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
<g/>
,	,	kIx,	,
nejširší	široký	k2eAgFnSc1d3	nejširší
cesta	cesta	k1gFnSc1	cesta
obvykle	obvykle	k6eAd1	obvykle
vede	vést	k5eAaImIp3nS	vést
středem	střed	k1gInSc7	střed
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
osou	osa	k1gFnSc7	osa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
přehledná	přehledný	k2eAgFnSc1d1	přehledná
a	a	k8xC	a
osázena	osázen	k2eAgFnSc1d1	osázena
jen	jen	k9	jen
nízkými	nízký	k2eAgInPc7d1	nízký
plůtky	plůtek	k1gInPc7	plůtek
a	a	k8xC	a
květinovými	květinový	k2eAgInPc7d1	květinový
vzory	vzor	k1gInPc7	vzor
<g/>
,	,	kIx,	,
nejvýše	vysoce	k6eAd3	vysoce
pak	pak	k6eAd1	pak
přerušena	přerušit	k5eAaPmNgFnS	přerušit
několika	několik	k4yIc7	několik
ozdobnými	ozdobný	k2eAgInPc7d1	ozdobný
stříhanými	stříhaný	k2eAgInPc7d1	stříhaný
keři	keř	k1gInPc7	keř
nebo	nebo	k8xC	nebo
výsadbou	výsadba	k1gFnSc7	výsadba
letniček	letnička	k1gFnPc2	letnička
na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgInSc6d1	vyvýšený
záhonu	záhon	k1gInSc6	záhon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavní	hlavní	k2eAgFnSc4d1	hlavní
plochu	plocha	k1gFnSc4	plocha
navazuje	navazovat	k5eAaImIp3nS	navazovat
mnoho	mnoho	k4c1	mnoho
menších	malý	k2eAgFnPc2d2	menší
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
oddělených	oddělený	k2eAgInPc2d1	oddělený
živými	živá	k1gFnPc7	živá
ploty	plot	k1gInPc4	plot
ze	z	k7c2	z
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
vysokých	vysoký	k2eAgFnPc2d1	vysoká
často	často	k6eAd1	často
4	[number]	k4	4
m.	m.	k?	m.
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
intimní	intimní	k2eAgNnPc4d1	intimní
zákoutí	zákoutí	k1gNnPc4	zákoutí
<g/>
.	.	kIx.	.
</s>
<s>
Zákoutí	zákoutí	k1gNnSc4	zákoutí
nebyla	být	k5eNaImAgFnS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
a	a	k8xC	a
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
byl	být	k5eAaImAgMnS	být
nějaký	nějaký	k3yIgInSc4	nějaký
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
prvek	prvek	k1gInSc4	prvek
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
či	či	k8xC	či
nádoba	nádoba	k1gFnSc1	nádoba
s	s	k7c7	s
atraktivní	atraktivní	k2eAgFnSc7d1	atraktivní
nebo	nebo	k8xC	nebo
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
rostlinou	rostlina	k1gFnSc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
chována	chován	k2eAgFnSc1d1	chována
jako	jako	k8xC	jako
zpestření	zpestření	k1gNnPc1	zpestření
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pávi	páv	k1gMnPc1	páv
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
stavbou	stavba	k1gFnSc7	stavba
jsou	být	k5eAaImIp3nP	být
altány	altán	k1gInPc4	altán
a	a	k8xC	a
pergoly	pergola	k1gFnPc4	pergola
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
zahrada	zahrada	k1gFnSc1	zahrada
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
i	i	k9	i
stavby	stavba	k1gFnPc4	stavba
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
nebo	nebo	k8xC	nebo
cizích	cizí	k2eAgFnPc2d1	cizí
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
samozřejmě	samozřejmě	k6eAd1	samozřejmě
musí	muset	k5eAaImIp3nP	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
pravidlům	pravidlo	k1gNnPc3	pravidlo
symetrie	symetrie	k1gFnSc2	symetrie
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
stavby	stavba	k1gFnPc1	stavba
mohly	moct	k5eAaImAgFnP	moct
vypadat	vypadat	k5eAaImF	vypadat
i	i	k9	i
velice	velice	k6eAd1	velice
uměle	uměle	k6eAd1	uměle
a	a	k8xC	a
nepřirozeně	přirozeně	k6eNd1	přirozeně
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
stylu	styl	k1gInSc3	styl
francouzské	francouzský	k2eAgFnSc2d1	francouzská
zahrady	zahrada	k1gFnSc2	zahrada
na	na	k7c4	na
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Umělost	umělost	k1gFnSc1	umělost
a	a	k8xC	a
nepřirozenost	nepřirozenost	k1gFnSc1	nepřirozenost
byla	být	k5eAaImAgFnS	být
znakem	znak	k1gInSc7	znak
kultivovanosti	kultivovanost	k1gFnSc2	kultivovanost
a	a	k8xC	a
ušlechtilosti	ušlechtilost	k1gFnSc2	ušlechtilost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
zahrady	zahrada	k1gFnPc4	zahrada
i	i	k8xC	i
oranžerie	oranžerie	k1gFnPc4	oranžerie
nebo	nebo	k8xC	nebo
skleník	skleník	k1gInSc4	skleník
s	s	k7c7	s
tropickými	tropický	k2eAgFnPc7d1	tropická
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
konírna	konírna	k1gFnSc1	konírna
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
zahrada	zahrada	k1gFnSc1	zahrada
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
majitel	majitel	k1gMnSc1	majitel
je	být	k5eAaImIp3nS	být
movitý	movitý	k2eAgMnSc1d1	movitý
a	a	k8xC	a
kultivovaný	kultivovaný	k2eAgMnSc1d1	kultivovaný
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přesycena	přesytit	k5eAaPmNgFnS	přesytit
přehnanou	přehnaný	k2eAgFnSc7d1	přehnaná
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
opakující	opakující	k2eAgFnSc7d1	opakující
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
francouzských	francouzský	k2eAgFnPc2d1	francouzská
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
Le	Le	k1gMnSc2	Le
Nôtreho	Nôtre	k1gMnSc2	Nôtre
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
lordem	lord	k1gMnSc7	lord
Francisem	Francis	k1gInSc7	Francis
Baconem	Bacon	k1gInSc7	Bacon
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
například	například	k6eAd1	například
esej	esej	k1gFnSc1	esej
"	"	kIx"	"
<g/>
Of	Of	k1gFnSc1	Of
the	the	k?	the
Garden	Gardna	k1gFnPc2	Gardna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
původní	původní	k2eAgFnSc4d1	původní
krajinu	krajina	k1gFnSc4	krajina
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
architekturu	architektura	k1gFnSc4	architektura
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
hledána	hledat	k5eAaImNgFnS	hledat
nová	nový	k2eAgFnSc1d1	nová
vhodná	vhodný	k2eAgFnSc1d1	vhodná
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
klasicismu	klasicismus	k1gInSc2	klasicismus
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
moderní	moderní	k2eAgFnSc7d1	moderní
úpravou	úprava	k1gFnSc7	úprava
anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
opakem	opak	k1gInSc7	opak
barokního	barokní	k2eAgInSc2d1	barokní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
stylem	styl	k1gInSc7	styl
krajinářského	krajinářský	k2eAgInSc2d1	krajinářský
parku	park	k1gInSc2	park
nebo	nebo	k8xC	nebo
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
estetickým	estetický	k2eAgInSc7d1	estetický
vlivem	vliv	k1gInSc7	vliv
čínských	čínský	k2eAgFnPc2d1	čínská
zahradních	zahradní	k2eAgFnPc2d1	zahradní
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgInS	nahradit
formálnější	formální	k2eAgInSc1d2	formálnější
<g/>
,	,	kIx,	,
symetrické	symetrický	k2eAgFnSc2d1	symetrická
francouzské	francouzský	k2eAgFnSc2d1	francouzská
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
zahradnický	zahradnický	k2eAgInSc1d1	zahradnický
styl	styl	k1gInSc1	styl
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámější	známý	k2eAgFnSc7d3	nejznámější
postavou	postava	k1gFnSc7	postava
stylu	styl	k1gInSc2	styl
anglické	anglický	k2eAgFnSc2d1	anglická
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
Lancelot	Lancelot	k1gInSc1	Lancelot
"	"	kIx"	"
<g/>
Capability	Capabilita	k1gFnSc2	Capabilita
<g/>
"	"	kIx"	"
Brown	Brown	k1gMnSc1	Brown
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
parků	park	k1gInPc2	park
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
směr	směr	k1gInSc1	směr
zcela	zcela	k6eAd1	zcela
korespondoval	korespondovat	k5eAaImAgInS	korespondovat
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
dominuje	dominovat	k5eAaImIp3nS	dominovat
vliv	vliv	k1gInSc1	vliv
anglického	anglický	k2eAgInSc2d1	anglický
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc1	styl
přírodních	přírodní	k2eAgFnPc2d1	přírodní
úprav	úprava	k1gFnPc2	úprava
s	s	k7c7	s
romantickými	romantický	k2eAgNnPc7d1	romantické
zákoutími	zákoutí	k1gNnPc7	zákoutí
vystřídán	vystřídat	k5eAaPmNgMnS	vystřídat
stylem	styl	k1gInSc7	styl
s	s	k7c7	s
čínskými	čínský	k2eAgInPc7d1	čínský
pavilonky	pavilonek	k1gInPc7	pavilonek
<g/>
,	,	kIx,	,
gotickými	gotický	k2eAgFnPc7d1	gotická
troskami	troska	k1gFnPc7	troska
<g/>
,	,	kIx,	,
římskými	římský	k2eAgInPc7d1	římský
<g/>
,	,	kIx,	,
křesťanskými	křesťanský	k2eAgInPc7d1	křesťanský
i	i	k8xC	i
pohanskými	pohanský	k2eAgInPc7d1	pohanský
chrámy	chrám	k1gInPc7	chrám
a	a	k8xC	a
sochami	socha	k1gFnPc7	socha
<g/>
,	,	kIx,	,
pyramidami	pyramida	k1gFnPc7	pyramida
a	a	k8xC	a
sfingami	sfinga	k1gFnPc7	sfinga
<g/>
,	,	kIx,	,
doupaty	doupě	k1gNnPc7	doupě
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
,	,	kIx,	,
strašidelnými	strašidelný	k2eAgInPc7d1	strašidelný
domy	dům	k1gInPc7	dům
a	a	k8xC	a
trpaslíky	trpaslík	k1gMnPc7	trpaslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
zkomolení	zkomolení	k1gNnSc1	zkomolení
<g/>
,	,	kIx,	,
zkažení	zkažení	k1gNnSc1	zkažení
anglického	anglický	k2eAgInSc2d1	anglický
stylu	styl	k1gInSc2	styl
přírodně	přírodně	k6eAd1	přírodně
krajinářského	krajinářský	k2eAgInSc2d1	krajinářský
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
Loudon	Loudon	k1gInSc1	Loudon
<g/>
,	,	kIx,	,
nastalo	nastat	k5eAaPmAgNnS	nastat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
stal	stát	k5eAaPmAgInS	stát
módním	módní	k2eAgInSc7d1	módní
a	a	k8xC	a
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
tak	tak	k9	tak
stát	stát	k5eAaImF	stát
prokázanou	prokázaný	k2eAgFnSc7d1	prokázaná
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
svobodných	svobodný	k2eAgNnPc6d1	svobodné
uměních	umění	k1gNnPc6	umění
je	být	k5eAaImIp3nS	být
měřítkem	měřítko	k1gNnSc7	měřítko
míra	mír	k1gInSc2	mír
geniality	genialita	k1gFnSc2	genialita
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
houf	houf	k1gInSc1	houf
falešných	falešný	k2eAgMnPc2d1	falešný
následovníků	následovník	k1gMnPc2	následovník
<g/>
,	,	kIx,	,
poplatných	poplatný	k2eAgFnPc2d1	poplatná
době	době	k6eAd1	době
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bláznění	bláznění	k1gNnSc1	bláznění
po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
přírodně	přírodně	k6eAd1	přírodně
krajinářského	krajinářský	k2eAgInSc2d1	krajinářský
slohu	sloh	k1gInSc2	sloh
bylo	být	k5eAaImAgNnS	být
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
skutečných	skutečný	k2eAgMnPc6d1	skutečný
umělcích	umělec	k1gMnPc6	umělec
s	s	k7c7	s
vkusem	vkus	k1gInSc7	vkus
brzy	brzy	k6eAd1	brzy
převýšila	převýšit	k5eAaPmAgFnS	převýšit
jejich	jejich	k3xOp3gFnSc4	jejich
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
bylo	být	k5eAaImAgNnS	být
zjednodušeno	zjednodušit	k5eAaPmNgNnS	zjednodušit
na	na	k7c4	na
mechanické	mechanický	k2eAgFnPc4d1	mechanická
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
zobrazujícího	zobrazující	k2eAgInSc2d1	zobrazující
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
<g/>
,	,	kIx,	,
eleganci	elegance	k1gFnSc4	elegance
<g/>
,	,	kIx,	,
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
systém	systém	k1gInSc1	systém
přidávání	přidávání	k1gNnSc2	přidávání
doplňků	doplněk	k1gInPc2	doplněk
a	a	k8xC	a
uplatňování	uplatňování	k1gNnSc6	uplatňování
pevných	pevný	k2eAgFnPc2d1	pevná
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
byl	být	k5eAaImAgMnS	být
méně	málo	k6eAd2	málo
různorodý	různorodý	k2eAgMnSc1d1	různorodý
než	než	k8xS	než
prehistorické	prehistorický	k2eAgInPc1d1	prehistorický
formální	formální	k2eAgInPc1d1	formální
styly	styl	k1gInPc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
přírodně	přírodně	k6eAd1	přírodně
krajinářských	krajinářský	k2eAgFnPc2d1	krajinářská
úprav	úprava	k1gFnPc2	úprava
bylo	být	k5eAaImAgNnS	být
degradováno	degradovat	k5eAaBmNgNnS	degradovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
formy	forma	k1gFnPc4	forma
úprav	úprava	k1gFnPc2	úprava
-	-	kIx~	-
"	"	kIx"	"
<g/>
chomáče	chomáč	k1gInPc1	chomáč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pásy	pás	k1gInPc1	pás
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
clump	clump	k1gMnSc1	clump
<g/>
,	,	kIx,	,
belt	belt	k1gMnSc1	belt
<g/>
,	,	kIx,	,
solitera	soliter	k1gMnSc2	soliter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc4	pravidlo
byla	být	k5eAaImAgFnS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
:	:	kIx,	:
umístěte	umístit	k5eAaPmRp2nP	umístit
pás	pás	k1gInSc4	pás
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
,	,	kIx,	,
rozmístěte	rozmístit	k5eAaPmRp2nP	rozmístit
shluky	shluk	k1gInPc4	shluk
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
stromy	strom	k1gInPc4	strom
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
výsledek	výsledek	k1gInSc1	výsledek
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
náročná	náročný	k2eAgFnSc1d1	náročná
práce	práce	k1gFnSc1	práce
zahradního	zahradní	k2eAgMnSc2d1	zahradní
architekta	architekt	k1gMnSc2	architekt
ukončena	ukončen	k2eAgFnSc1d1	ukončena
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
byly	být	k5eAaImAgInP	být
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
nejen	nejen	k6eAd1	nejen
místními	místní	k2eAgMnPc7d1	místní
umělci	umělec	k1gMnPc7	umělec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
jistým	jistý	k2eAgMnSc7d1	jistý
anglickým	anglický	k2eAgMnSc7d1	anglický
mistrem	mistr	k1gMnSc7	mistr
(	(	kIx(	(
<g/>
míněno	mínit	k5eAaImNgNnS	mínit
Lancelotem	Lancelot	k1gInSc7	Lancelot
"	"	kIx"	"
<g/>
Capability	Capabilita	k1gFnSc2	Capabilita
<g/>
"	"	kIx"	"
Brownem	Brown	k1gMnSc7	Brown
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
sbíral	sbírat	k5eAaImAgMnS	sbírat
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
kreslil	kreslit	k5eAaImAgInS	kreslit
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Neliší	lišit	k5eNaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
úpravou	úprava	k1gFnSc7	úprava
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Jednotvárné	jednotvárný	k2eAgInPc4d1	jednotvárný
produkty	produkt	k1gInPc4	produkt
těchto	tento	k3xDgFnPc2	tento
mechanických	mechanický	k2eAgFnPc2d1	mechanická
úprav	úprava	k1gFnPc2	úprava
anglickému	anglický	k2eAgInSc3d1	anglický
stylu	styl	k1gInSc3	styl
brzy	brzy	k6eAd1	brzy
přinesly	přinést	k5eAaPmAgFnP	přinést
špatnou	špatný	k2eAgFnSc4d1	špatná
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníci	vlastník	k1gMnPc1	vlastník
byli	být	k5eAaImAgMnP	být
zesměšňováni	zesměšňovat	k5eAaImNgMnP	zesměšňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vynakládají	vynakládat	k5eAaImIp3nP	vynakládat
nesmírné	smírný	k2eNgFnPc4d1	nesmírná
částky	částka	k1gFnPc4	částka
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
starých	starý	k2eAgFnPc2d1	stará
alejí	alej	k1gFnPc2	alej
a	a	k8xC	a
lesů	les	k1gInPc2	les
a	a	k8xC	a
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
místech	místo	k1gNnPc6	místo
malé	malý	k2eAgNnSc1d1	malé
shluky	shluk	k1gInPc1	shluk
mladých	mladý	k2eAgMnPc2d1	mladý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
z	z	k7c2	z
žádného	žádný	k3yNgInSc2	žádný
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
moderní	moderní	k2eAgNnSc1d1	moderní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnSc1d1	moderní
zahrada	zahrada	k1gFnSc1	zahrada
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
zahradu	zahrada	k1gFnSc4	zahrada
měnil	měnit	k5eAaImAgMnS	měnit
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
různých	různý	k2eAgInPc2d1	různý
filosofických	filosofický	k2eAgInPc2d1	filosofický
a	a	k8xC	a
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vlivem	vliv	k1gInSc7	vliv
konzumerismu	konzumerismus	k1gInSc2	konzumerismus
a	a	k8xC	a
krizí	krize	k1gFnPc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
při	při	k7c6	při
nákladné	nákladný	k2eAgFnSc6d1	nákladná
údržbě	údržba	k1gFnSc6	údržba
zdobit	zdobit	k5eAaImF	zdobit
množstvím	množství	k1gNnSc7	množství
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
kombinací	kombinace	k1gFnPc2	kombinace
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
kompozicí	kompozice	k1gFnPc2	kompozice
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
úpravy	úprava	k1gFnPc1	úprava
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
souhrny	souhrn	k1gInPc1	souhrn
geometrických	geometrický	k2eAgInPc2d1	geometrický
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
také	také	k9	také
okázalé	okázalý	k2eAgFnPc1d1	okázalá
úpravy	úprava	k1gFnPc1	úprava
dekorativním	dekorativní	k2eAgNnSc7d1	dekorativní
zpracováním	zpracování	k1gNnSc7	zpracování
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
světelnými	světelný	k2eAgInPc7d1	světelný
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
dramaticky	dramaticky	k6eAd1	dramaticky
či	či	k8xC	či
divadelně	divadelně	k6eAd1	divadelně
působivými	působivý	k2eAgInPc7d1	působivý
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
projevy	projev	k1gInPc1	projev
a	a	k8xC	a
posuny	posun	k1gInPc1	posun
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
ponejvíce	ponejvíce	k6eAd1	ponejvíce
v	v	k7c6	v
úpravách	úprava	k1gFnPc6	úprava
pro	pro	k7c4	pro
sociálně	sociálně	k6eAd1	sociálně
nadřazené	nadřazený	k2eAgFnPc4d1	nadřazená
skupiny	skupina	k1gFnPc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
střední	střední	k2eAgFnPc4d1	střední
vrstvy	vrstva	k1gFnPc4	vrstva
změna	změna	k1gFnSc1	změna
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
napodobování	napodobování	k1gNnSc6	napodobování
vzoru	vzor	k1gInSc2	vzor
se	s	k7c7	s
snadnější	snadný	k2eAgFnSc7d2	snazší
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mechanizovanou	mechanizovaný	k2eAgFnSc7d1	mechanizovaná
údržbou	údržba	k1gFnSc7	údržba
a	a	k8xC	a
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
omezujících	omezující	k2eAgFnPc6d1	omezující
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
(	(	kIx(	(
<g/>
mulčování	mulčování	k1gNnPc1	mulčování
<g/>
,	,	kIx,	,
pesticidy	pesticid	k1gInPc1	pesticid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
příklon	příklon	k1gInSc1	příklon
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
blízkým	blízký	k2eAgMnPc3d1	blízký
ideálům	ideál	k1gInPc3	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
sociálních	sociální	k2eAgFnPc2d1	sociální
vrstev	vrstva	k1gFnPc2	vrstva
preferování	preferování	k1gNnSc2	preferování
okrasných	okrasný	k2eAgFnPc2d1	okrasná
zahrad	zahrada	k1gFnPc2	zahrada
jako	jako	k8xS	jako
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
relaxaci	relaxace	k1gFnSc4	relaxace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
meziválečné	meziválečný	k2eAgFnSc6d1	meziválečná
době	doba	k1gFnSc6	doba
hnutí	hnutí	k1gNnSc2	hnutí
Arts	Artsa	k1gFnPc2	Artsa
and	and	k?	and
Crafts	Craftsa	k1gFnPc2	Craftsa
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
i	i	k9	i
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Gertrude	Gertrude	k6eAd1	Gertrude
Jekyllová	Jekyllová	k1gFnSc1	Jekyllová
zavedla	zavést	k5eAaPmAgFnS	zavést
do	do	k7c2	do
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
novátorské	novátorský	k2eAgFnSc2d1	novátorská
propracované	propracovaný	k2eAgFnSc2d1	propracovaná
úpravy	úprava	k1gFnSc2	úprava
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
venkovské	venkovský	k2eAgFnSc2d1	venkovská
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
blízké	blízký	k2eAgInPc1d1	blízký
přirozeným	přirozený	k2eAgInSc7d1	přirozený
společenstvům	společenstvo	k1gNnPc3	společenstvo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
během	během	k7c2	během
poválečné	poválečný	k2eAgFnSc2d1	poválečná
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
architektonickým	architektonický	k2eAgInSc7d1	architektonický
slohem	sloh	k1gInSc7	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
architektonického	architektonický	k2eAgInSc2d1	architektonický
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
filosofii	filosofie	k1gFnSc3	filosofie
"	"	kIx"	"
<g/>
ozdoba	ozdoba	k1gFnSc1	ozdoba
je	být	k5eAaImIp3nS	být
zločin	zločin	k1gInSc1	zločin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dekorace	dekorace	k1gFnSc1	dekorace
je	být	k5eAaImIp3nS	být
smyslné	smyslný	k2eAgFnSc3d1	smyslná
a	a	k8xC	a
primitivní	primitivní	k2eAgFnSc2d1	primitivní
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
toliko	toliko	k6eAd1	toliko
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnPc4d2	nižší
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
sedláky	sedlák	k1gMnPc4	sedlák
a	a	k8xC	a
divochy	divoch	k1gMnPc4	divoch
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
domy	dům	k1gInPc1	dům
-	-	kIx~	-
"	"	kIx"	"
<g/>
stroje	stroj	k1gInPc1	stroj
na	na	k7c4	na
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
ČR	ČR	kA	ČR
byl	být	k5eAaImAgInS	být
ražen	ražen	k2eAgInSc1d1	ražen
trend	trend	k1gInSc1	trend
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahrada	zahrada	k1gFnSc1	zahrada
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
místem	místo	k1gNnSc7	místo
relaxace	relaxace	k1gFnSc2	relaxace
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
a	a	k8xC	a
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
zařízení	zařízení	k1gNnSc6	zařízení
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
již	již	k6eAd1	již
hodně	hodně	k6eAd1	hodně
oprostili	oprostit	k5eAaPmAgMnP	oprostit
ode	ode	k7c2	ode
všech	všecek	k3xTgFnPc2	všecek
drahých	drahý	k2eAgFnPc2d1	drahá
zbytečností	zbytečnost	k1gFnPc2	zbytečnost
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
jest	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
útočištěm	útočiště	k1gNnSc7	útočiště
ustupujícího	ustupující	k2eAgInSc2d1	ustupující
<g />
.	.	kIx.	.
</s>
<s>
dekorativismu	dekorativismus	k1gInSc2	dekorativismus
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
zajištěn	zajištěn	k2eAgInSc4d1	zajištěn
co	co	k8xS	co
největší	veliký	k2eAgInSc4d3	veliký
komfort	komfort	k1gInSc4	komfort
majitele	majitel	k1gMnSc2	majitel
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
zahrada	zahrada	k1gFnSc1	zahrada
obsahovat	obsahovat	k5eAaImF	obsahovat
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
rovný	rovný	k2eAgInSc4d1	rovný
trávník	trávník	k1gInSc4	trávník
<g/>
,	,	kIx,	,
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
stín	stín	k1gInSc4	stín
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
Vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
úpravou	úprava	k1gFnSc7	úprava
zahrady	zahrada	k1gFnPc4	zahrada
sice	sice	k8xC	sice
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
bodech	bod	k1gInPc6	bod
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
principům	princip	k1gInPc3	princip
daného	daný	k2eAgInSc2d1	daný
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zřejmě	zřejmě	k6eAd1	zřejmě
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
výsadbách	výsadba	k1gFnPc6	výsadba
prováděných	prováděný	k2eAgFnPc2d1	prováděná
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
a	a	k8xC	a
preferovaném	preferovaný	k2eAgInSc6d1	preferovaný
vkusu	vkus	k1gInSc6	vkus
tehdy	tehdy	k6eAd1	tehdy
sociálně	sociálně	k6eAd1	sociálně
nadřazené	nadřazený	k2eAgFnPc1d1	nadřazená
skupiny	skupina	k1gFnPc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
a	a	k8xC	a
jasně	jasně	k6eAd1	jasně
členěnými	členěný	k2eAgFnPc7d1	členěná
<g/>
,	,	kIx,	,
geometrickými	geometrický	k2eAgFnPc7d1	geometrická
a	a	k8xC	a
přehlednými	přehledný	k2eAgFnPc7d1	přehledná
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
holými	holý	k2eAgFnPc7d1	holá
<g/>
"	"	kIx"	"
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
,	,	kIx,	,
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
zákazníky	zákazník	k1gMnPc7	zákazník
i	i	k8xC	i
holanďanka	holanďanka	k1gFnSc1	holanďanka
Mien	Miena	k1gFnPc2	Miena
Ruys	Ruysa	k1gFnPc2	Ruysa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
výsadeb	výsadba	k1gFnPc2	výsadba
je	být	k5eAaImIp3nS	být
zjevná	zjevný	k2eAgFnSc1d1	zjevná
snaha	snaha	k1gFnSc1	snaha
úpravou	úprava	k1gFnSc7	úprava
omezit	omezit	k5eAaPmF	omezit
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
výsadeb	výsadba	k1gFnPc2	výsadba
používá	používat	k5eAaImIp3nS	používat
trvalky	trvalka	k1gFnPc4	trvalka
v	v	k7c6	v
zajímavých	zajímavý	k2eAgFnPc6d1	zajímavá
kompozicích	kompozice	k1gFnPc6	kompozice
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
geometrické	geometrický	k2eAgFnPc4d1	geometrická
úpravy	úprava	k1gFnPc4	úprava
Mien	Mieno	k1gNnPc2	Mieno
Ruys	Ruysa	k1gFnPc2	Ruysa
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
druhové	druhový	k2eAgNnSc1d1	druhové
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vnímány	vnímat	k5eAaImNgInP	vnímat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
architekturou	architektura	k1gFnSc7	architektura
jako	jako	k8xC	jako
velký	velký	k2eAgInSc1d1	velký
přínos	přínos	k1gInSc1	přínos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
naprostý	naprostý	k2eAgInSc4d1	naprostý
nedostatek	nedostatek	k1gInSc4	nedostatek
školkařských	školkařský	k2eAgInPc2d1	školkařský
výpěstků	výpěstek	k1gInPc2	výpěstek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
chybí	chybět	k5eAaImIp3nP	chybět
celé	celý	k2eAgFnPc1d1	celá
skupiny	skupina	k1gFnPc1	skupina
okrasných	okrasný	k2eAgFnPc2d1	okrasná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Výsadby	výsadba	k1gFnPc1	výsadba
alejí	alej	k1gFnPc2	alej
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
parcích	park	k1gInPc6	park
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
a	a	k8xC	a
propagován	propagován	k2eAgInSc1d1	propagován
živý	živý	k2eAgInSc1d1	živý
plot	plot	k1gInSc1	plot
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
postupně	postupně	k6eAd1	postupně
expanduje	expandovat	k5eAaImIp3nS	expandovat
u	u	k7c2	u
soukromých	soukromý	k2eAgFnPc2d1	soukromá
staveb	stavba	k1gFnPc2	stavba
móda	móda	k1gFnSc1	móda
udržovaných	udržovaný	k2eAgInPc2d1	udržovaný
trávníků	trávník	k1gInPc2	trávník
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
kopírováním	kopírování	k1gNnSc7	kopírování
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
u	u	k7c2	u
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
okrasné	okrasný	k2eAgFnSc2d1	okrasná
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
veřejná	veřejný	k2eAgFnSc1d1	veřejná
zeleň	zeleň	k1gFnSc1	zeleň
je	být	k5eAaImIp3nS	být
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
<g/>
.	.	kIx.	.
</s>
<s>
Zelinářské	zelinářský	k2eAgFnPc1d1	zelinářská
zahrady	zahrada	k1gFnPc1	zahrada
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
do	do	k7c2	do
konce	konec	k1gInSc2	konec
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
buduje	budovat	k5eAaImIp3nS	budovat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
Roberto	Roberta	k1gFnSc5	Roberta
Burle	Burl	k1gInSc5	Burl
Marx	Marx	k1gMnSc1	Marx
nákladné	nákladný	k2eAgInPc4d1	nákladný
parky	park	k1gInPc4	park
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
s	s	k7c7	s
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
<g/>
,	,	kIx,	,
pestře	pestro	k6eAd1	pestro
barevnými	barevný	k2eAgInPc7d1	barevný
vzory	vzor	k1gInPc7	vzor
a	a	k8xC	a
měkkými	měkký	k2eAgFnPc7d1	měkká
liniemi	linie	k1gFnPc7	linie
<g/>
,	,	kIx,	,
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
slavným	slavný	k2eAgMnSc7d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
působí	působit	k5eAaImIp3nS	působit
Robert	Robert	k1gMnSc1	Robert
Irwin	Irwin	k1gMnSc1	Irwin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nákladné	nákladný	k2eAgFnPc4d1	nákladná
úpravy	úprava	k1gFnPc4	úprava
s	s	k7c7	s
geometrickými	geometrický	k2eAgInPc7d1	geometrický
symboly	symbol	k1gInPc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
úprava	úprava	k1gFnSc1	úprava
Central	Central	k1gMnPc2	Central
Garden	Gardna	k1gFnPc2	Gardna
v	v	k7c4	v
Getty	Getta	k1gFnPc4	Getta
Center	centrum	k1gNnPc2	centrum
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
zahrad	zahrada	k1gFnPc2	zahrada
==	==	k?	==
</s>
</p>
<p>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
lze	lze	k6eAd1	lze
teoreticky	teoreticky	k6eAd1	teoreticky
členit	členit	k5eAaImF	členit
podle	podle	k7c2	podle
jasně	jasně	k6eAd1	jasně
daných	daný	k2eAgNnPc2d1	dané
a	a	k8xC	a
smysluplných	smysluplný	k2eAgNnPc2d1	smysluplné
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
typy	typ	k1gInPc7	typ
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
účel	účel	k1gInSc4	účel
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
požadavky	požadavek	k1gInPc1	požadavek
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
nadřazeny	nadřazen	k2eAgFnPc1d1	nadřazena
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
kategorizaci	kategorizace	k1gFnSc3	kategorizace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
budiž	budiž	k9	budiž
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prolínají	prolínat	k5eAaImIp3nP	prolínat
renesanční	renesanční	k2eAgFnSc7d1	renesanční
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc7d1	barokní
a	a	k8xC	a
romantické	romantický	k2eAgInPc4d1	romantický
vlivy	vliv	k1gInPc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zahrady	zahrada	k1gFnSc2	zahrada
ovšem	ovšem	k9	ovšem
například	například	k6eAd1	například
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
sportoviště	sportoviště	k1gNnSc4	sportoviště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sportoviště	sportoviště	k1gNnPc1	sportoviště
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnPc2	součást
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
===	===	k?	===
</s>
</p>
<p>
<s>
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
pro	pro	k7c4	pro
okrasné	okrasný	k2eAgInPc4d1	okrasný
účely	účel	k1gInPc4	účel
nebo	nebo	k8xC	nebo
účelově	účelově	k6eAd1	účelově
upravené	upravený	k2eAgFnPc1d1	upravená
výsadby	výsadba	k1gFnPc1	výsadba
zeleniny	zelenina	k1gFnSc2	zelenina
</s>
</p>
<p>
<s>
ovocná	ovocný	k2eAgFnSc1d1	ovocná
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
úprava	úprava	k1gFnSc1	úprava
evokující	evokující	k2eAgFnSc2d1	evokující
ovocný	ovocný	k2eAgInSc4d1	ovocný
sad	sad	k1gInSc4	sad
</s>
</p>
<p>
<s>
okrasná	okrasný	k2eAgFnSc1d1	okrasná
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
poutat	poutat	k5eAaImF	poutat
pozornost	pozornost	k1gFnSc4	pozornost
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
neslouží	sloužit	k5eNaImIp3nP	sloužit
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k9	jako
sportoviště	sportoviště	k1gNnSc1	sportoviště
<g/>
,	,	kIx,	,
ke	k	k7c3	k
skladování	skladování	k1gNnSc3	skladování
předmětů	předmět	k1gInPc2	předmět
ani	ani	k8xC	ani
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
potravin	potravina	k1gFnPc2	potravina
</s>
</p>
<p>
<s>
obytná	obytný	k2eAgFnSc1d1	obytná
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
zahrada	zahrada	k1gFnSc1	zahrada
tvořící	tvořící	k2eAgFnSc4d1	tvořící
místnost	místnost	k1gFnSc4	místnost
pod	pod	k7c7	pod
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
nebem	nebe	k1gNnSc7	nebe
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
římského	římský	k2eAgInSc2d1	římský
peristylu	peristyl	k1gInSc2	peristyl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
arboretum	arboretum	k1gNnSc1	arboretum
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
zeleň	zeleň	k1gFnSc1	zeleň
u	u	k7c2	u
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
upravena	upravit	k5eAaPmNgFnS	upravit
jako	jako	k8xC	jako
park	park	k1gInSc1	park
nebo	nebo	k8xC	nebo
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
bez	bez	k7c2	bez
celkové	celkový	k2eAgFnSc2d1	celková
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zimní	zimní	k2eAgFnSc1d1	zimní
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
oranžerie	oranžerie	k1gFnSc1	oranžerie
-	-	kIx~	-
skleník	skleník	k1gInSc1	skleník
se	s	k7c7	s
stálezelenými	stálezelený	k2eAgFnPc7d1	stálezelená
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hřbitov	hřbitov	k1gInSc1	hřbitov
(	(	kIx(	(
<g/>
zahrada	zahrada	k1gFnSc1	zahrada
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
upravený	upravený	k2eAgInSc4d1	upravený
prostor	prostor	k1gInSc4	prostor
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
pietní	pietní	k2eAgFnSc3d1	pietní
vzpomínce	vzpomínka	k1gFnSc3	vzpomínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zoo	zoo	k1gFnSc4	zoo
upravený	upravený	k2eAgInSc4d1	upravený
prostor	prostor	k1gInSc4	prostor
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc2	jejich
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
slepecká	slepecký	k2eAgFnSc1d1	slepecká
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
rostlin	rostlina	k1gFnPc2	rostlina
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zahrad	zahrada	k1gFnPc2	zahrada
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
dřeviny	dřevina	k1gFnPc4	dřevina
a	a	k8xC	a
byliny	bylina	k1gFnSc2	bylina
mnoha	mnoho	k4c2	mnoho
taxonomických	taxonomický	k2eAgFnPc2d1	Taxonomická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
účelu	účel	k1gInSc2	účel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k9	jako
botanickou	botanický	k2eAgFnSc4d1	botanická
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
,	,	kIx,	,
vytvářet	vytvářet	k5eAaImF	vytvářet
výsadby	výsadba	k1gFnPc4	výsadba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
bylinková	bylinkový	k2eAgFnSc1d1	bylinková
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
květinová	květinový	k2eAgFnSc1d1	květinová
zahrada	zahrada	k1gFnSc1	zahrada
okrasná	okrasný	k2eAgFnSc1d1	okrasná
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
květinami	květina	k1gFnPc7	květina
</s>
</p>
<p>
<s>
kaktusová	kaktusový	k2eAgFnSc1d1	kaktusová
zahrada	zahrada	k1gFnSc1	zahrada
,	,	kIx,	,
</s>
</p>
<p>
<s>
rozárium	rozárium	k1gNnSc1	rozárium
výsadba	výsadba	k1gFnSc1	výsadba
růží	růžit	k5eAaImIp3nS	růžit
</s>
</p>
<p>
<s>
oranžerie	oranžerie	k1gFnSc1	oranžerie
venkovní	venkovní	k2eAgFnSc1d1	venkovní
nebo	nebo	k8xC	nebo
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prostor	prostor	k1gInSc1	prostor
s	s	k7c7	s
nádobami	nádoba	k1gFnPc7	nádoba
s	s	k7c7	s
citrusy	citrus	k1gInPc7	citrus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zimní	zimní	k2eAgFnSc1d1	zimní
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
skleník	skleník	k1gInSc1	skleník
se	s	k7c7	s
stálezelenými	stálezelený	k2eAgFnPc7d1	stálezelená
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
iridárium	iridárium	k1gNnSc1	iridárium
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
další	další	k2eAgFnPc4d1	další
jednodruhové	jednodruhový	k2eAgFnPc4d1	jednodruhová
zahrady	zahrada	k1gFnPc4	zahrada
</s>
</p>
<p>
<s>
genový	genový	k2eAgInSc4d1	genový
sad	sad	k1gInSc4	sad
</s>
</p>
<p>
<s>
tropická	tropický	k2eAgFnSc1d1	tropická
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
ovocná	ovocný	k2eAgFnSc1d1	ovocná
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
evokující	evokující	k2eAgInSc1d1	evokující
ovocný	ovocný	k2eAgInSc1d1	ovocný
sad	sad	k1gInSc1	sad
</s>
</p>
<p>
<s>
kamenná	kamenný	k2eAgFnSc1d1	kamenná
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
který	který	k3yRgInSc4	který
lze	lze	k6eAd1	lze
zahrnout	zahrnout	k5eAaPmF	zahrnout
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
velkou	velký	k2eAgFnSc4d1	velká
úlohu	úloha	k1gFnSc4	úloha
hraje	hrát	k5eAaImIp3nS	hrát
množství	množství	k1gNnSc1	množství
nějak	nějak	k6eAd1	nějak
upravené	upravený	k2eAgFnSc2d1	upravená
a	a	k8xC	a
umístěné	umístěný	k2eAgFnSc2d1	umístěná
horniny	hornina	k1gFnSc2	hornina
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
materiálu	materiál	k1gInSc2	materiál
===	===	k?	===
</s>
</p>
<p>
<s>
Nadšení	nadšení	k1gNnSc4	nadšení
nad	nad	k7c7	nad
schopností	schopnost	k1gFnSc7	schopnost
vnímat	vnímat	k5eAaImF	vnímat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
odstíny	odstín	k1gInPc4	odstín
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
velí	velet	k5eAaImIp3nS	velet
některým	některý	k3yIgMnPc3	některý
pěstitelům	pěstitel	k1gMnPc3	pěstitel
vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
olistěním	olistění	k1gNnSc7	olistění
a	a	k8xC	a
květem	květ	k1gInSc7	květ
takové	takový	k3xDgFnSc2	takový
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
podobný	podobný	k2eAgInSc4d1	podobný
rozmar	rozmar	k1gInSc4	rozmar
jako	jako	k8xC	jako
vytváření	vytváření	k1gNnSc4	vytváření
pokojů	pokoj	k1gInPc2	pokoj
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
jediné	jediný	k2eAgFnSc2d1	jediná
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
stálezelená	stálezelený	k2eAgFnSc1d1	stálezelená
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
zahradaa	zahradaa	k1gFnSc1	zahradaa
další	další	k2eAgFnSc2d1	další
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
umístění	umístění	k1gNnSc2	umístění
===	===	k?	===
</s>
</p>
<p>
<s>
vertikální	vertikální	k2eAgFnSc1d1	vertikální
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
střešní	střešní	k2eAgFnSc1d1	střešní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
okenní	okenní	k2eAgFnSc1d1	okenní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
atrium	atrium	k1gNnSc1	atrium
</s>
</p>
<p>
<s>
peristyl	peristyl	k1gInSc1	peristyl
</s>
</p>
<p>
<s>
solárium	solárium	k1gNnSc1	solárium
</s>
</p>
<p>
<s>
===	===	k?	===
Sociálního	sociální	k2eAgNnSc2d1	sociální
hlediska	hledisko	k1gNnSc2	hledisko
===	===	k?	===
</s>
</p>
<p>
<s>
Zahrádkářská	zahrádkářský	k2eAgFnSc1d1	zahrádkářská
kolonie	kolonie	k1gFnSc1	kolonie
</s>
</p>
<p>
<s>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
komunitní	komunitní	k2eAgFnSc1d1	komunitní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
zeleň	zeleň	k1gFnSc1	zeleň
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
stylu	styl	k1gInSc2	styl
úpravy	úprava	k1gFnSc2	úprava
===	===	k?	===
</s>
</p>
<p>
<s>
Styl	styl	k1gInSc1	styl
úpravy	úprava	k1gFnSc2	úprava
je	být	k5eAaImIp3nS	být
hodnocen	hodnocen	k2eAgInSc1d1	hodnocen
u	u	k7c2	u
okrasných	okrasný	k2eAgFnPc2d1	okrasná
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
u	u	k7c2	u
zeleninových	zeleninový	k2eAgFnPc2d1	zeleninová
zahrad	zahrada	k1gFnPc2	zahrada
není	být	k5eNaImIp3nS	být
krása	krása	k1gFnSc1	krása
kompozice	kompozice	k1gFnSc1	kompozice
výsadby	výsadba	k1gFnSc2	výsadba
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mohou	moct	k5eAaImIp3nP	moct
zeleninové	zeleninový	k2eAgFnPc4d1	zeleninová
zahrady	zahrada	k1gFnPc4	zahrada
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgFnP	použít
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
a	a	k8xC	a
přestože	přestože	k8xS	přestože
zelenina	zelenina	k1gFnSc1	zelenina
může	moct	k5eAaImIp3nS	moct
plnit	plnit	k5eAaImF	plnit
funkci	funkce	k1gFnSc4	funkce
okrasných	okrasný	k2eAgFnPc2d1	okrasná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
stylu	styl	k1gInSc2	styl
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
architektury	architektura	k1gFnSc2	architektura
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
okrasné	okrasný	k2eAgFnPc4d1	okrasná
zahrady	zahrada	k1gFnPc4	zahrada
rozdělovány	rozdělován	k2eAgFnPc4d1	rozdělována
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
protikladné	protikladný	k2eAgInPc4d1	protikladný
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
(	(	kIx(	(
<g/>
v	v	k7c6	v
témž	týž	k3xTgInSc6	týž
významu	význam	k1gInSc6	význam
geometrické	geometrický	k2eAgFnPc1d1	geometrická
nebo	nebo	k8xC	nebo
architektonické	architektonický	k2eAgFnPc1d1	architektonická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
nepravidelné	pravidelný	k2eNgFnSc6d1	nepravidelná
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
styly	styl	k1gInPc4	styl
používány	používán	k2eAgInPc4d1	používán
také	také	k9	také
názvy	název	k1gInPc4	název
"	"	kIx"	"
<g/>
ancient	ancient	k1gMnSc1	ancient
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
formal	format	k5eAaImAgMnS	format
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
historické	historický	k2eAgFnSc2d1	historická
<g/>
,	,	kIx,	,
úpravné	úpravný	k2eAgFnSc2d1	úpravná
nebo	nebo	k8xC	nebo
společenské	společenský	k2eAgFnSc2d1	společenská
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
"	"	kIx"	"
<g/>
wild	wild	k1gInSc1	wild
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
modern	modern	k1gMnSc1	modern
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
divoké	divoký	k2eAgNnSc1d1	divoké
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
nepravidelné	pravidelný	k2eNgFnPc4d1	nepravidelná
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
název	název	k1gInSc1	název
geometrické	geometrický	k2eAgFnSc2d1	geometrická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
symetrické	symetrický	k2eAgFnSc2d1	symetrická
zahrady	zahrada	k1gFnSc2	zahrada
nebo	nebo	k8xC	nebo
architektonické	architektonický	k2eAgFnSc2d1	architektonická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
výrazy	výraz	k1gInPc1	výraz
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
nejzřetelnější	zřetelný	k2eAgInPc4d3	nejzřetelnější
rysy	rys	k1gInPc4	rys
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
zahrady	zahrada	k1gFnPc4	zahrada
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
ty	ten	k3xDgFnPc1	ten
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
upraveny	upravit	k5eAaPmNgFnP	upravit
v	v	k7c6	v
symetrických	symetrický	k2eAgInPc6d1	symetrický
celcích	celek	k1gInPc6	celek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
používají	používat	k5eAaImIp3nP	používat
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgFnSc2d1	přímá
nebo	nebo	k8xC	nebo
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
odráží	odrážet	k5eAaImIp3nS	odrážet
stavební	stavební	k2eAgInSc4d1	stavební
prvek	prvek	k1gInSc4	prvek
-	-	kIx~	-
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
úprava	úprava	k1gFnSc1	úprava
nikdy	nikdy	k6eAd1	nikdy
nenapodobuje	napodobovat	k5eNaImIp3nS	napodobovat
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úpravou	úprava	k1gFnSc7	úprava
která	který	k3yRgFnSc1	který
často	často	k6eAd1	často
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
lidskou	lidský	k2eAgFnSc4d1	lidská
moc	moc	k1gFnSc4	moc
zkrotit	zkrotit	k5eAaPmF	zkrotit
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
a	a	k8xC	a
byliny	bylina	k1gFnPc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
pokojem	pokoj	k1gInSc7	pokoj
<g/>
,	,	kIx,	,
pokračováním	pokračování	k1gNnSc7	pokračování
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
zahrady	zahrada	k1gFnPc1	zahrada
dávají	dávat	k5eAaImIp3nP	dávat
na	na	k7c4	na
odiv	odiv	k1gInSc4	odiv
krásu	krása	k1gFnSc4	krása
technické	technický	k2eAgFnSc2d1	technická
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
seřazené	seřazený	k2eAgInPc4d1	seřazený
celky	celek	k1gInPc4	celek
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
dokonalost	dokonalost	k1gFnSc4	dokonalost
úpravy	úprava	k1gFnSc2	úprava
<g/>
,	,	kIx,	,
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc4d1	vysoký
kontrast	kontrast	k1gInSc4	kontrast
použitých	použitý	k2eAgInPc2d1	použitý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1	vytvoření
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
snazší	snadný	k2eAgMnSc1d2	snazší
než	než	k8xS	než
vytvoření	vytvoření	k1gNnSc1	vytvoření
vkusné	vkusný	k2eAgFnSc2d1	vkusná
nepravidelné	pravidelný	k2eNgFnSc2d1	nepravidelná
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
především	především	k9	především
dekorativní	dekorativní	k2eAgFnSc4d1	dekorativní
úpravu	úprava	k1gFnSc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
<g/>
římská	římský	k2eAgFnSc1d1	římská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
perská	perský	k2eAgFnSc1d1	perská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
úprava	úprava	k1gFnSc1	úprava
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
parků	park	k1gInPc2	park
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nepravidelnou	pravidelný	k2eNgFnSc7d1	nepravidelná
kompozicí	kompozice	k1gFnSc7	kompozice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
a	a	k8xC	a
interpretací	interpretace	k1gFnPc2	interpretace
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
nějaké	nějaký	k3yIgInPc1	nějaký
technické	technický	k2eAgInPc1d1	technický
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
skryty	skryt	k2eAgFnPc1d1	skryta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nejsou	být	k5eNaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
na	na	k7c4	na
odiv	odiv	k1gInSc4	odiv
<g/>
.	.	kIx.	.
</s>
<s>
Nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
úprava	úprava	k1gFnSc1	úprava
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
a	a	k8xC	a
stylizuje	stylizovat	k5eAaImIp3nS	stylizovat
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc4d1	přírodní
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
zahrady	zahrada	k1gFnPc1	zahrada
mohou	moct	k5eAaImIp3nP	moct
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
některé	některý	k3yIgNnSc4	některý
skutečné	skutečný	k2eAgNnSc4d1	skutečné
biologické	biologický	k2eAgNnSc4d1	biologické
seskupení	seskupení	k1gNnSc4	seskupení
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
napodobit	napodobit	k5eAaPmF	napodobit
nebo	nebo	k8xC	nebo
vytvořit	vytvořit	k5eAaPmF	vytvořit
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
pocit	pocit	k1gInSc1	pocit
přírodního	přírodní	k2eAgNnSc2d1	přírodní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
úprav	úprava	k1gFnPc2	úprava
nápadné	nápadný	k2eAgInPc4d1	nápadný
a	a	k8xC	a
dekorativní	dekorativní	k2eAgInPc4d1	dekorativní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
poutající	poutající	k2eAgFnSc4d1	poutající
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
úpravy	úprava	k1gFnSc2	úprava
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
především	především	k6eAd1	především
klidné	klidný	k2eAgNnSc1d1	klidné
a	a	k8xC	a
příjemné	příjemný	k2eAgNnSc1d1	příjemné
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
japonská	japonský	k2eAgFnSc1d1	japonská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
čínská	čínský	k2eAgFnSc1d1	čínská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
venkovská	venkovský	k2eAgFnSc1d1	venkovská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
styly	styl	k1gInPc1	styl
zahradních	zahradní	k2eAgFnPc2d1	zahradní
úprav	úprava	k1gFnPc2	úprava
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
odlišující	odlišující	k2eAgNnPc1d1	odlišující
pravidla	pravidlo	k1gNnPc1	pravidlo
či	či	k8xC	či
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
preference	preference	k1gFnPc1	preference
odlišující	odlišující	k2eAgFnPc1d1	odlišující
některé	některý	k3yIgFnPc1	některý
úpravy	úprava	k1gFnPc1	úprava
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
styly	styl	k1gInPc1	styl
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
stejné	stejný	k2eAgInPc4d1	stejný
nebo	nebo	k8xC	nebo
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc4d1	jiné
použití	použití	k1gNnSc4	použití
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Zásady	zásada	k1gFnPc1	zásada
estetiky	estetika	k1gFnSc2	estetika
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
podobné	podobný	k2eAgFnSc6d1	podobná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
použití	použití	k1gNnSc4	použití
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
asijských	asijský	k2eAgInPc2d1	asijský
stylů	styl	k1gInPc2	styl
je	být	k5eAaImIp3nS	být
preferována	preferován	k2eAgFnSc1d1	preferována
zahrada	zahrada	k1gFnSc1	zahrada
jako	jako	k8xC	jako
rozjímavé	rozjímavý	k2eAgNnSc1d1	rozjímavé
místo	místo	k1gNnSc1	místo
se	s	k7c7	s
symboly	symbol	k1gInPc7	symbol
<g/>
,	,	kIx,	,
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
je	být	k5eAaImIp3nS	být
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
stylů	styl	k1gInPc2	styl
podřízena	podřízen	k2eAgNnPc1d1	podřízeno
složitému	složitý	k2eAgMnSc3d1	složitý
(	(	kIx(	(
<g/>
a	a	k8xC	a
různému	různý	k2eAgMnSc3d1	různý
<g/>
)	)	kIx)	)
pevnému	pevný	k2eAgInSc3d1	pevný
řádu	řád	k1gInSc3	řád
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
nápadná	nápadný	k2eAgFnSc1d1	nápadná
kultivovaná	kultivovaný	k2eAgFnSc1d1	kultivovaná
ozdoba	ozdoba	k1gFnSc1	ozdoba
nebo	nebo	k8xC	nebo
krásné	krásný	k2eAgNnSc1d1	krásné
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
známé	známý	k2eAgInPc1d1	známý
styly	styl	k1gInPc1	styl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
čínská	čínský	k2eAgFnSc1d1	čínská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
korejská	korejský	k2eAgFnSc1d1	Korejská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
babylonská	babylonský	k2eAgFnSc1d1	Babylonská
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
Visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
Semiramidiny	Semiramidin	k2eAgFnSc2d1	Semiramidin
</s>
</p>
<p>
<s>
egyptská	egyptský	k2eAgFnSc1d1	egyptská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
japonská	japonský	k2eAgFnSc1d1	japonská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
perská	perský	k2eAgFnSc1d1	perská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
římská	římský	k2eAgFnSc1d1	římská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
italská	italský	k2eAgFnSc1d1	italská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
středověká	středověký	k2eAgFnSc1d1	středověká
zahrada	zahrada	k1gFnSc1	zahrada
kterou	který	k3yIgFnSc4	který
sice	sice	k8xC	sice
lze	lze	k6eAd1	lze
reálně	reálně	k6eAd1	reálně
vidět	vidět	k5eAaImF	vidět
jako	jako	k9	jako
zahrady	zahrada	k1gFnPc4	zahrada
ve	v	k7c6	v
středověkých	středověký	k2eAgNnPc6d1	středověké
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
trocha	trocha	k1gFnSc1	trocha
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
italských	italský	k2eAgFnPc2d1	italská
vil	vila	k1gFnPc2	vila
církevních	církevní	k2eAgMnPc2d1	církevní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
(	(	kIx(	(
<g/>
přepychové	přepychový	k2eAgInPc4d1	přepychový
architektonické	architektonický	k2eAgInPc4d1	architektonický
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
okrasné	okrasný	k2eAgInPc1d1	okrasný
celky	celek	k1gInPc1	celek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hradní	hradní	k2eAgFnPc4d1	hradní
zahrady	zahrada	k1gFnPc4	zahrada
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
léčivek	léčivka	k1gFnPc2	léčivka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vinicemi	vinice	k1gFnPc7	vinice
a	a	k8xC	a
ovocnými	ovocný	k2eAgInPc7d1	ovocný
stromy	strom	k1gInPc7	strom
plundrované	plundrovaný	k2eAgFnSc2d1	plundrovaný
při	pře	k1gFnSc4	pře
obléhání	obléhání	k1gNnSc2	obléhání
<g/>
,	,	kIx,	,
venkovské	venkovský	k2eAgFnSc2d1	venkovská
zahrady	zahrada	k1gFnSc2	zahrada
které	který	k3yIgNnSc1	který
obsáhly	obsáhnout	k5eAaPmAgFnP	obsáhnout
několik	několik	k4yIc4	několik
záhonů	záhon	k1gInPc2	záhon
u	u	k7c2	u
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
klášterní	klášterní	k2eAgFnPc4d1	klášterní
zahrady	zahrada	k1gFnPc4	zahrada
sloužící	sloužící	k2eAgFnPc1d1	sloužící
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
bylinek	bylinka	k1gFnPc2	bylinka
<g/>
,	,	kIx,	,
s	s	k7c7	s
někdy	někdy	k6eAd1	někdy
malými	malý	k2eAgInPc7d1	malý
uzavřenými	uzavřený	k2eAgInPc7d1	uzavřený
okrasnými	okrasný	k2eAgInPc7d1	okrasný
celky	celek	k1gInPc7	celek
s	s	k7c7	s
náboženskými	náboženský	k2eAgInPc7d1	náboženský
motivy	motiv	k1gInPc7	motiv
"	"	kIx"	"
<g/>
hortus	hortus	k1gInSc1	hortus
conclusus	conclusus	k1gInSc1	conclusus
<g/>
"	"	kIx"	"
giardino	giardin	k2eAgNnSc1d1	giardino
secretto	secretto	k1gNnSc1	secretto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
malé	malý	k2eAgInPc1d1	malý
uzavřené	uzavřený	k2eAgInPc1d1	uzavřený
okrasné	okrasný	k2eAgInPc1d1	okrasný
celky	celek	k1gInPc1	celek
jsou	být	k5eAaImIp3nP	být
právě	právě	k6eAd1	právě
těmi	ten	k3xDgFnPc7	ten
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
jako	jako	k8xC	jako
vzor	vzor	k1gInSc1	vzor
středověké	středověký	k2eAgFnSc2d1	středověká
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
zahrada	zahrada	k1gFnSc1	zahrada
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
parky	park	k1gInPc1	park
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
stylu	styl	k1gInSc6	styl
francouzských	francouzský	k2eAgFnPc2d1	francouzská
barokních	barokní	k2eAgFnPc2d1	barokní
zahrad	zahrada	k1gFnPc2	zahrada
</s>
</p>
<p>
<s>
suché	suchý	k2eAgFnPc1d1	suchá
zahrady	zahrada	k1gFnPc1	zahrada
-	-	kIx~	-
karesansui	karesansu	k1gFnPc1	karesansu
</s>
</p>
<p>
<s>
maurská	maurský	k2eAgFnSc1d1	maurská
zahrada	zahrada	k1gFnSc1	zahrada
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
navazující	navazující	k2eAgFnPc1d1	navazující
na	na	k7c4	na
perské	perský	k2eAgInPc4d1	perský
vlivy	vliv	k1gInPc4	vliv
</s>
</p>
<p>
<s>
holandská	holandský	k2eAgFnSc1d1	holandská
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
menší	malý	k2eAgFnSc1d2	menší
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
živými	živý	k2eAgInPc7d1	živý
ploty	plot	k1gInPc7	plot
<g/>
,	,	kIx,	,
specifická	specifický	k2eAgFnSc1d1	specifická
podlouhlými	podlouhlý	k2eAgFnPc7d1	podlouhlá
vodními	vodní	k2eAgFnPc7d1	vodní
plochami	plocha	k1gFnPc7	plocha
-	-	kIx~	-
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
se	s	k7c7	s
stojatou	stojatý	k2eAgFnSc7d1	stojatá
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
francouzské	francouzský	k2eAgFnSc3d1	francouzská
barokní	barokní	k2eAgFnSc3d1	barokní
zahradě	zahrada	k1gFnSc3	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
-	-	kIx~	-
anglické	anglický	k2eAgFnSc2d1	anglická
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
parky	park	k1gInPc1	park
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
ferme	fermat	k5eAaPmIp3nS	fermat
ornée	ornée	k6eAd1	ornée
</s>
</p>
<p>
<s>
venkovská	venkovský	k2eAgFnSc1d1	venkovská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
zenová	zenový	k2eAgFnSc1d1	zenová
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
přírodní	přírodní	k2eAgFnSc1d1	přírodní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
prérijní	prérijní	k2eAgFnSc1d1	prérijní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
moderní	moderní	k2eAgFnSc1d1	moderní
zahrada	zahrada	k1gFnSc1	zahrada
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
postfunkcionalistická	postfunkcionalistický	k2eAgFnSc1d1	postfunkcionalistický
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
-	-	kIx~	-
zcela	zcela	k6eAd1	zcela
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
podřazeno	podřazen	k2eAgNnSc4d1	podřazeno
prvoplánové	prvoplánový	k2eAgFnPc4d1	prvoplánová
praktičnosti	praktičnost	k1gFnPc4	praktičnost
<g/>
,	,	kIx,	,
novosti	novost	k1gFnPc4	novost
a	a	k8xC	a
módnosti	módnost	k1gFnPc4	módnost
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
stejně	stejně	k6eAd1	stejně
nepromyšleně	promyšleně	k6eNd1	promyšleně
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
feng	feng	k1gInSc1	feng
šuej	šuej	k1gInSc1	šuej
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
zahrada	zahrada	k1gFnSc1	zahrada
vedená	vedený	k2eAgFnSc1d1	vedená
záměrem	záměr	k1gInSc7	záměr
vyhovět	vyhovět	k5eAaPmF	vyhovět
požadavkům	požadavek	k1gInPc3	požadavek
uživatele	uživatel	k1gMnSc2	uživatel
a	a	k8xC	a
ideologii	ideologie	k1gFnSc4	ideologie
Feng	Fenga	k1gFnPc2	Fenga
šuejJe	šuejJat	k5eAaPmIp3nS	šuejJat
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
úprava	úprava	k1gFnSc1	úprava
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
stylu	styl	k1gInSc6	styl
či	či	k8xC	či
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
či	či	k8xC	či
úpravy	úprava	k1gFnPc4	úprava
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
spojením	spojení	k1gNnSc7	spojení
slohů	sloh	k1gInPc2	sloh
nejsou	být	k5eNaImIp3nP	být
zárukou	záruka	k1gFnSc7	záruka
<g/>
,	,	kIx,	,
ba	ba	k9	ba
ani	ani	k8xC	ani
výrazně	výrazně	k6eAd1	výrazně
nemusí	muset	k5eNaImIp3nS	muset
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
estetické	estetický	k2eAgFnPc4d1	estetická
kvality	kvalita	k1gFnPc4	kvalita
ani	ani	k8xC	ani
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
pohodlí	pohodlí	k1gNnSc4	pohodlí
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jsou	být	k5eAaImIp3nP	být
příkladů	příklad	k1gInPc2	příklad
kdy	kdy	k6eAd1	kdy
zkušení	zkušený	k2eAgMnPc1d1	zkušený
zahradní	zahradní	k2eAgMnPc1d1	zahradní
architekti	architekt	k1gMnPc1	architekt
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
nebo	nebo	k8xC	nebo
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
úpravy	úprava	k1gFnPc1	úprava
které	který	k3yRgMnPc4	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
výhradně	výhradně	k6eAd1	výhradně
vyhovět	vyhovět	k5eAaPmF	vyhovět
požadavkům	požadavek	k1gInPc3	požadavek
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
působí	působit	k5eAaImIp3nS	působit
nepohodlí	nepohodlí	k1gNnSc4	nepohodlí
či	či	k8xC	či
esteticky	esteticky	k6eAd1	esteticky
nevhodně	vhodně	k6eNd1	vhodně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
historické	historický	k2eAgFnSc2d1	historická
zahrady	zahrada	k1gFnSc2	zahrada
Květná	květný	k2eAgFnSc1d1	Květná
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
nebyla	být	k5eNaImAgFnS	být
zcela	zcela	k6eAd1	zcela
věrně	věrně	k6eAd1	věrně
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
úprava	úprava	k1gFnSc1	úprava
zahrady	zahrada	k1gFnSc2	zahrada
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokonalost	dokonalost	k1gFnSc1	dokonalost
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
nenarušila	narušit	k5eNaPmAgFnS	narušit
vhodnost	vhodnost	k1gFnSc4	vhodnost
<g/>
,	,	kIx,	,
estetiku	estetika	k1gFnSc4	estetika
a	a	k8xC	a
věrohodné	věrohodný	k2eAgNnSc4d1	věrohodné
působení	působení	k1gNnSc4	působení
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Slohová	slohový	k2eAgFnSc1d1	slohová
čistota	čistota	k1gFnSc1	čistota
zahrad	zahrada	k1gFnPc2	zahrada
====	====	k?	====
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Historické	historický	k2eAgInPc1d1	historický
parky	park	k1gInPc1	park
a	a	k8xC	a
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
slohově	slohově	k6eAd1	slohově
hodnotné	hodnotný	k2eAgFnPc4d1	hodnotná
a	a	k8xC	a
čisté	čistý	k2eAgFnPc4d1	čistá
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hurych	Hurych	k1gMnSc1	Hurych
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
úpravami	úprava	k1gFnPc7	úprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
původními	původní	k2eAgInPc7d1	původní
díly	díl	k1gInPc7	díl
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
úpravami	úprava	k1gFnPc7	úprava
provedenými	provedený	k2eAgFnPc7d1	provedená
během	během	k7c2	během
různých	různý	k2eAgNnPc2d1	různé
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
původního	původní	k2eAgNnSc2d1	původní
díla	dílo	k1gNnSc2	dílo
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
způsob	způsob	k1gInSc4	způsob
údržby	údržba	k1gFnSc2	údržba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
nebo	nebo	k8xC	nebo
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
některé	některý	k3yIgInPc4	některý
rysy	rys	k1gInPc4	rys
ba	ba	k9	ba
i	i	k9	i
prvky	prvek	k1gInPc1	prvek
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
času	čas	k1gInSc2	čas
měnily	měnit	k5eAaImAgFnP	měnit
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
rysy	rys	k1gInPc1	rys
dekorativních	dekorativní	k2eAgMnPc2d1	dekorativní
i	i	k8xC	i
malebných	malebný	k2eAgFnPc2d1	malebná
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
okolních	okolní	k2eAgFnPc2d1	okolní
budov	budova	k1gFnPc2	budova
===	===	k?	===
</s>
</p>
<p>
<s>
zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
klášterní	klášterní	k2eAgFnSc1d1	klášterní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
hřbitov	hřbitov	k1gInSc1	hřbitov
</s>
</p>
<p>
<s>
zoo	zoo	k1gFnSc1	zoo
</s>
</p>
<p>
<s>
školní	školní	k2eAgFnSc1d1	školní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
===	===	k?	===
</s>
</p>
<p>
<s>
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
zahrady	zahrada	k1gFnPc1	zahrada
-	-	kIx~	-
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
nepravidelné	pravidelný	k2eNgFnPc4d1	nepravidelná
zahrady	zahrada	k1gFnPc4	zahrada
</s>
</p>
<p>
<s>
===	===	k?	===
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nikdy	nikdy	k6eAd1	nikdy
neexistující	existující	k2eNgInSc4d1	neexistující
<g/>
,	,	kIx,	,
ideologicky	ideologicky	k6eAd1	ideologicky
motivované	motivovaný	k2eAgFnPc1d1	motivovaná
nebo	nebo	k8xC	nebo
mytické	mytický	k2eAgFnPc1d1	mytická
zahrady	zahrada	k1gFnPc1	zahrada
===	===	k?	===
</s>
</p>
<p>
<s>
rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
Ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
Eden	Eden	k1gInSc1	Eden
<g/>
)	)	kIx)	)
-	-	kIx~	-
vzor	vzor	k1gInSc4	vzor
některých	některý	k3yIgFnPc2	některý
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
zpodobňovaná	zpodobňovaný	k2eAgFnSc1d1	zpodobňovaná
jako	jako	k8xS	jako
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
hadem	had	k1gMnSc7	had
a	a	k8xC	a
rajským	rajský	k2eAgNnSc7d1	rajské
jablkem	jablko	k1gNnSc7	jablko
uprostřed	uprostřed	k7c2	uprostřed
</s>
</p>
<p>
<s>
Hortus	hortus	k1gInSc1	hortus
Conclusus	Conclusus	k1gInSc1	Conclusus
zahrada	zahrada	k1gFnSc1	zahrada
představující	představující	k2eAgNnSc1d1	představující
panenství	panenství	k1gNnSc1	panenství
matky	matka	k1gFnSc2	matka
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzor	vzor	k1gInSc1	vzor
některých	některý	k3yIgFnPc2	některý
středověkých	středověký	k2eAgFnPc2d1	středověká
zahrad	zahrada	k1gFnPc2	zahrada
</s>
</p>
<p>
<s>
==	==	k?	==
Součásti	součást	k1gFnSc6	součást
zahrad	zahrada	k1gFnPc2	zahrada
==	==	k?	==
</s>
</p>
<p>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
tvořeny	tvořit	k5eAaImNgInP	tvořit
menšímu	malý	k2eAgInSc3d2	menší
celky	celek	k1gInPc7	celek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svým	svůj	k3xOyFgInSc7	svůj
účelem	účel	k1gInSc7	účel
<g/>
,	,	kIx,	,
uspořádáním	uspořádání	k1gNnSc7	uspořádání
nebo	nebo	k8xC	nebo
úpravou	úprava	k1gFnSc7	úprava
určují	určovat	k5eAaImIp3nP	určovat
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc4	vzhled
nebo	nebo	k8xC	nebo
styl	styl	k1gInSc4	styl
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
===	===	k?	===
</s>
</p>
<p>
<s>
Stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
části	část	k1gFnPc4	část
jenž	jenž	k3xRgMnSc1	jenž
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
estetickou	estetický	k2eAgFnSc7d1	estetická
i	i	k8xC	i
provozní	provozní	k2eAgFnSc7d1	provozní
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
okrasné	okrasný	k2eAgFnPc1d1	okrasná
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
sem	sem	k6eAd1	sem
řadit	řadit	k5eAaImF	řadit
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
lávky	lávka	k1gFnPc1	lávka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
šlapáky	šlapák	k1gInPc1	šlapák
</s>
</p>
<p>
<s>
zídky	zídka	k1gFnPc1	zídka
a	a	k8xC	a
zástěny	zástěna	k1gFnPc1	zástěna
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zdi	zeď	k1gFnPc1	zeď
a	a	k8xC	a
oplocení	oplocení	k1gNnSc1	oplocení
</s>
</p>
<p>
<s>
sloupořadí	sloupořadí	k1gNnSc1	sloupořadí
</s>
</p>
<p>
<s>
plastika	plastika	k1gFnSc1	plastika
<g/>
,	,	kIx,	,
obelisk	obelisk	k1gInSc1	obelisk
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
</s>
</p>
<p>
<s>
fontána	fontána	k1gFnSc1	fontána
</s>
</p>
<p>
<s>
zahradní	zahradní	k2eAgInSc1d1	zahradní
krb	krb	k1gInSc1	krb
nebo	nebo	k8xC	nebo
zahradní	zahradní	k2eAgInSc1d1	zahradní
gril	gril	k1gInSc1	gril
<g/>
,	,	kIx,	,
udírna	udírna	k1gFnSc1	udírna
<g/>
,	,	kIx,	,
ohniště	ohniště	k1gNnSc1	ohniště
</s>
</p>
<p>
<s>
hřiště	hřiště	k1gNnSc1	hřiště
a	a	k8xC	a
pískoviště	pískoviště	k1gNnSc1	pískoviště
</s>
</p>
<p>
<s>
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
chodník	chodník	k1gInSc1	chodník
</s>
</p>
<p>
<s>
odpočívadlo	odpočívadlo	k1gNnSc1	odpočívadlo
</s>
</p>
<p>
<s>
kaskáda	kaskáda	k1gFnSc1	kaskáda
(	(	kIx(	(
<g/>
vodopád	vodopád	k1gInSc1	vodopád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc1d1	vodní
střiky	střik	k1gInPc1	střik
</s>
</p>
<p>
<s>
jezírka	jezírko	k1gNnSc2	jezírko
</s>
</p>
<p>
<s>
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
nádrž	nádrž	k1gFnSc1	nádrž
</s>
</p>
<p>
<s>
umělé	umělý	k2eAgInPc4d1	umělý
potoky	potok	k1gInPc4	potok
<g/>
,	,	kIx,	,
napodobeniny	napodobenina	k1gFnPc4	napodobenina
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
kanály	kanál	k1gInPc1	kanál
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
vodní	vodní	k2eAgInPc1d1	vodní
prvky	prvek	k1gInPc1	prvek
</s>
</p>
<p>
<s>
pergoly	pergola	k1gFnPc1	pergola
</s>
</p>
<p>
<s>
terasa	terasa	k1gFnSc1	terasa
</s>
</p>
<p>
<s>
altán	altán	k1gInSc1	altán
</s>
</p>
<p>
<s>
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
vstupní	vstupní	k2eAgFnPc1d1	vstupní
části	část	k1gFnPc1	část
</s>
</p>
<p>
<s>
treláž	treláž	k1gFnSc1	treláž
</s>
</p>
<p>
<s>
schodiště	schodiště	k1gNnSc1	schodiště
</s>
</p>
<p>
<s>
umělé	umělý	k2eAgFnPc1d1	umělá
zříceniny	zřícenina	k1gFnPc1	zřícenina
a	a	k8xC	a
napodobeniny	napodobenina	k1gFnPc1	napodobenina
staveb	stavba	k1gFnPc2	stavba
</s>
</p>
<p>
<s>
grotto	grotto	k1gNnSc1	grotto
</s>
</p>
<p>
<s>
včelíny	včelín	k1gInPc4	včelín
<g/>
,	,	kIx,	,
úly	úl	k1gInPc4	úl
<g/>
,	,	kIx,	,
hmyzí	hmyzí	k2eAgInPc4d1	hmyzí
domky	domek	k1gInPc4	domek
</s>
</p>
<p>
<s>
oranžerie	oranžerie	k1gFnSc1	oranžerie
<g/>
,	,	kIx,	,
skleník	skleník	k1gInSc1	skleník
<g/>
,	,	kIx,	,
stínoviště	stínoviště	k1gNnSc1	stínoviště
a	a	k8xC	a
zimní	zimní	k2eAgFnSc1d1	zimní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
umělé	umělý	k2eAgNnSc1d1	umělé
zavlažování	zavlažování	k1gNnSc1	zavlažování
</s>
</p>
<p>
<s>
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
zvířat	zvíře	k1gNnPc2	zvíře
</s>
</p>
<p>
<s>
zahradní	zahradní	k2eAgInSc1d1	zahradní
nábytek	nábytek	k1gInSc1	nábytek
</s>
</p>
<p>
<s>
kompost	kompost	k1gInSc1	kompost
</s>
</p>
<p>
<s>
===	===	k?	===
Vegetativní	vegetativní	k2eAgFnPc4d1	vegetativní
úpravy	úprava	k1gFnPc4	úprava
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
určité	určitý	k2eAgFnSc2d1	určitá
způsoby	způsoba	k1gFnSc2	způsoba
úprav	úprava	k1gFnPc2	úprava
zeleně	zeleň	k1gFnSc2	zeleň
či	či	k8xC	či
úpravy	úprava	k1gFnSc2	úprava
jejich	jejich	k3xOp3gFnSc2	jejich
výsadby	výsadba	k1gFnSc2	výsadba
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
typickými	typický	k2eAgFnPc7d1	typická
součástmi	součást	k1gFnPc7	součást
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
živý	živý	k2eAgInSc4d1	živý
plot	plot	k1gInSc4	plot
</s>
</p>
<p>
<s>
topiary	topiara	k1gFnPc1	topiara
</s>
</p>
<p>
<s>
rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
</s>
</p>
<p>
<s>
trávník	trávník	k1gInSc1	trávník
</s>
</p>
<p>
<s>
okrasné	okrasný	k2eAgInPc1d1	okrasný
záhony	záhon	k1gInPc1	záhon
<g/>
,	,	kIx,	,
vyvýšené	vyvýšený	k2eAgInPc1d1	vyvýšený
záhony	záhon	k1gInPc1	záhon
</s>
</p>
<p>
<s>
skalka	skalka	k1gFnSc1	skalka
<g/>
,	,	kIx,	,
štěrbinová	štěrbinový	k2eAgFnSc1d1	štěrbinová
skalka	skalka	k1gFnSc1	skalka
<g/>
,	,	kIx,	,
skalka	skalka	k1gFnSc1	skalka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
</s>
</p>
<p>
<s>
suchá	suchý	k2eAgFnSc1d1	suchá
zídka	zídka	k1gFnSc1	zídka
</s>
</p>
<p>
<s>
bylinková	bylinkový	k2eAgFnSc1d1	bylinková
spirála	spirála	k1gFnSc1	spirála
</s>
</p>
<p>
<s>
alej	alej	k1gFnSc1	alej
</s>
</p>
<p>
<s>
bosket	bosket	k1gInSc1	bosket
</s>
</p>
<p>
<s>
palmeto	palmeta	k1gFnSc5	palmeta
</s>
</p>
<p>
<s>
===	===	k?	===
Koncepční	koncepční	k2eAgNnSc1d1	koncepční
řešení	řešení	k1gNnSc1	řešení
části	část	k1gFnSc2	část
zahrady	zahrada	k1gFnSc2	zahrada
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
koncepční	koncepční	k2eAgNnSc4d1	koncepční
řešení	řešení	k1gNnSc4	řešení
částí	část	k1gFnPc2	část
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
jako	jako	k8xC	jako
typický	typický	k2eAgInSc1d1	typický
prvek	prvek	k1gInSc1	prvek
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
slohově	slohově	k6eAd1	slohově
různých	různý	k2eAgFnPc6d1	různá
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
takové	takový	k3xDgFnPc4	takový
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
estetizujícím	estetizující	k2eAgInSc7d1	estetizující
celkem	celek	k1gInSc7	celek
<g/>
,	,	kIx,	,
předpokládajícím	předpokládající	k2eAgInSc7d1	předpokládající
určité	určitý	k2eAgNnSc4d1	určité
řešení	řešení	k1gNnPc4	řešení
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
vhodného	vhodný	k2eAgInSc2d1	vhodný
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
dané	daný	k2eAgNnSc4d1	dané
řešení	řešení	k1gNnSc4	řešení
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
jen	jen	k9	jen
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
součástí	součást	k1gFnSc7	součást
rozsáhlejší	rozsáhlý	k2eAgFnSc2d2	rozsáhlejší
úpravy	úprava	k1gFnSc2	úprava
celého	celý	k2eAgInSc2d1	celý
pozemku	pozemek	k1gInSc2	pozemek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
terénní	terénní	k2eAgFnPc1d1	terénní
úpravy	úprava	k1gFnPc1	úprava
</s>
</p>
<p>
<s>
broderie	broderie	k1gFnSc1	broderie
</s>
</p>
<p>
<s>
giardinetto	giardinetto	k1gNnSc1	giardinetto
</s>
</p>
<p>
<s>
xerofytní	xerofytný	k2eAgMnPc1d1	xerofytný
výsadba	výsadba	k1gFnSc1	výsadba
</s>
</p>
<p>
<s>
rozárium	rozárium	k1gNnSc1	rozárium
</s>
</p>
<p>
<s>
parter	parter	k1gInSc1	parter
</s>
</p>
<p>
<s>
úpravy	úprava	k1gFnPc1	úprava
se	se	k3xPyFc4	se
vzorem	vzor	k1gInSc7	vzor
uzlů	uzel	k1gInPc2	uzel
-	-	kIx~	-
knoten	knoten	k2eAgInSc1d1	knoten
</s>
</p>
<p>
<s>
labyrint	labyrint	k1gInSc1	labyrint
</s>
</p>
<p>
<s>
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
nebo	nebo	k8xC	nebo
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
vinice	vinice	k1gFnSc1	vinice
nebo	nebo	k8xC	nebo
ovocný	ovocný	k2eAgInSc1d1	ovocný
sad	sad	k1gInSc1	sad
</s>
</p>
<p>
<s>
napajedlo	napajedlo	k1gNnSc1	napajedlo
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
ptačí	ptačí	k2eAgMnPc4d1	ptačí
koupaliště	koupaliště	k1gNnSc1	koupaliště
</s>
</p>
<p>
<s>
předzahrádka	předzahrádka	k1gFnSc1	předzahrádka
</s>
</p>
<p>
<s>
vřesoviště	vřesoviště	k1gNnSc1	vřesoviště
<g/>
,	,	kIx,	,
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
</s>
</p>
<p>
<s>
střešní	střešní	k2eAgFnSc1d1	střešní
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
vertikální	vertikální	k2eAgFnSc1d1	vertikální
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgInPc1d1	jiný
doplňky	doplněk	k1gInPc1	doplněk
===	===	k?	===
</s>
</p>
<p>
<s>
Pěstovaná	pěstovaný	k2eAgNnPc1d1	pěstované
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
</s>
</p>
<p>
<s>
==	==	k?	==
Rostliny	rostlina	k1gFnPc1	rostlina
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
použité	použitý	k2eAgFnSc2d1	použitá
rostliny	rostlina	k1gFnSc2	rostlina
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
například	například	k6eAd1	například
na	na	k7c4	na
</s>
</p>
<p>
<s>
stromy	strom	k1gInPc1	strom
</s>
</p>
<p>
<s>
keře	keř	k1gInPc1	keř
</s>
</p>
<p>
<s>
pnoucí	pnoucí	k2eAgFnSc1d1	pnoucí
</s>
</p>
<p>
<s>
byliny	bylina	k1gFnPc4	bylina
-	-	kIx~	-
okrasné	okrasný	k2eAgFnPc4d1	okrasná
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
</s>
</p>
<p>
<s>
listnáče	listnáč	k1gInPc1	listnáč
a	a	k8xC	a
jehličnany	jehličnan	k1gInPc1	jehličnan
</s>
</p>
<p>
<s>
stálezelené	stálezelený	k2eAgFnPc1d1	stálezelená
<g/>
,	,	kIx,	,
poloopadavé	poloopadavý	k2eAgFnPc1d1	poloopadavá
a	a	k8xC	a
opadavé	opadavý	k2eAgFnPc1d1	opadavá
rostliny	rostlina	k1gFnPc1	rostlina
</s>
</p>
<p>
<s>
trvalky	trvalka	k1gFnPc1	trvalka
a	a	k8xC	a
letničky	letnička	k1gFnPc1	letnička
</s>
</p>
<p>
<s>
cibuloviny	cibulovina	k1gFnPc1	cibulovina
a	a	k8xC	a
hlíznaté	hlíznatý	k2eAgFnPc1d1	hlíznatá
rostliny	rostlina	k1gFnPc1	rostlina
</s>
</p>
<p>
<s>
acidiofilní	acidiofilní	k2eAgFnPc1d1	acidiofilní
a	a	k8xC	a
váponomilné	váponomilný	k2eAgFnPc1d1	váponomilný
</s>
</p>
<p>
<s>
vlhkomilné	vlhkomilný	k2eAgFnPc1d1	vlhkomilná
a	a	k8xC	a
suchomilné	suchomilný	k2eAgFnPc1d1	suchomilná
</s>
</p>
<p>
<s>
stínomilné	stínomilný	k2eAgFnPc1d1	stínomilná
a	a	k8xC	a
světlomilné	světlomilný	k2eAgFnPc1d1	světlomilná
</s>
</p>
<p>
<s>
teplomilné	teplomilný	k2eAgFnPc1d1	teplomilná
a	a	k8xC	a
otužilé	otužilý	k2eAgFnPc1d1	otužilá
</s>
</p>
<p>
<s>
okrasné	okrasný	k2eAgInPc1d1	okrasný
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
květem	květ	k1gInSc7	květ
<g/>
,	,	kIx,	,
listy	list	k1gInPc1	list
</s>
</p>
<p>
<s>
pomalurostoucí	pomalurostoucí	k2eAgFnSc1d1	pomalurostoucí
a	a	k8xC	a
rychlerostoucí	rychlerostoucí	k2eAgFnSc1d1	rychlerostoucí
</s>
</p>
<p>
<s>
==	==	k?	==
Zahrada	zahrada	k1gFnSc1	zahrada
jako	jako	k8xS	jako
ekosystém	ekosystém	k1gInSc1	ekosystém
==	==	k?	==
</s>
</p>
<p>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
pro	pro	k7c4	pro
biodiverzitu	biodiverzita	k1gFnSc4	biodiverzita
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
důležitý	důležitý	k2eAgInSc4d1	důležitý
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
pojetí	pojetí	k1gNnSc1	pojetí
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k9	jako
u	u	k7c2	u
divoké	divoký	k2eAgFnSc2d1	divoká
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
někdy	někdy	k6eAd1	někdy
být	být	k5eAaImF	být
nápomocno	nápomocen	k2eAgNnSc4d1	nápomocno
udržení	udržení	k1gNnSc4	udržení
místních	místní	k2eAgInPc2d1	místní
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
odpovídajícího	odpovídající	k2eAgInSc2d1	odpovídající
ekosystému	ekosystém	k1gInSc2	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Pojetí	pojetí	k1gNnSc1	pojetí
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
výsadby	výsadba	k1gFnSc2	výsadba
a	a	k8xC	a
především	především	k9	především
některé	některý	k3yIgFnPc1	některý
součásti	součást	k1gFnPc1	součást
zahrady	zahrada	k1gFnSc2	zahrada
například	například	k6eAd1	například
živé	živý	k2eAgInPc1d1	živý
ploty	plot	k1gInPc1	plot
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
ploty	plot	k1gInPc1	plot
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
krmítka	krmítko	k1gNnPc1	krmítko
<g/>
,	,	kIx,	,
budky	budka	k1gFnPc1	budka
<g/>
,	,	kIx,	,
ptačí	ptačit	k5eAaImIp3nS	ptačit
pítka	pítko	k1gNnSc2	pítko
nebo	nebo	k8xC	nebo
koupaliště	koupaliště	k1gNnSc2	koupaliště
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
alespoň	alespoň	k9	alespoň
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
místní	místní	k2eAgFnSc2d1	místní
fauny	fauna	k1gFnSc2	fauna
<g/>
,	,	kIx,	,
místních	místní	k2eAgFnPc2d1	místní
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
různorodost	různorodost	k1gFnSc1	různorodost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
biodiverzitu	biodiverzita	k1gFnSc4	biodiverzita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
s	s	k7c7	s
více	hodně	k6eAd2	hodně
neudržovanými	udržovaný	k2eNgFnPc7d1	neudržovaná
částmi	část	k1gFnPc7	část
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
strukturami	struktura	k1gFnPc7	struktura
<g/>
,	,	kIx,	,
prostředími	prostředí	k1gNnPc7	prostředí
a	a	k8xC	a
povrchy	povrch	k1gInPc7	povrch
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
více	hodně	k6eAd2	hodně
včel	včela	k1gFnPc2	včela
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
<g/>
Jiné	jiný	k2eAgFnPc1d1	jiná
součásti	součást	k1gFnPc1	součást
zahrady	zahrada	k1gFnSc2	zahrada
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
opačný	opačný	k2eAgInSc4d1	opačný
účinek	účinek	k1gInSc4	účinek
-	-	kIx~	-
koupací	koupací	k2eAgInSc4d1	koupací
bazén	bazén	k1gInSc4	bazén
<g/>
,	,	kIx,	,
pečlivě	pečlivě	k6eAd1	pečlivě
střižený	střižený	k2eAgInSc1d1	střižený
trávník	trávník	k1gInSc1	trávník
<g/>
,	,	kIx,	,
záhony	záhon	k1gInPc1	záhon
květin	květina	k1gFnPc2	květina
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
pečlivě	pečlivě	k6eAd1	pečlivě
ošetřované	ošetřovaný	k2eAgFnSc2d1	ošetřovaná
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
nemají	mít	k5eNaImIp3nP	mít
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
místní	místní	k2eAgFnSc2d1	místní
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flory	flor	k1gInPc4	flor
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Vůle	vůle	k1gFnSc1	vůle
udržovat	udržovat	k5eAaImF	udržovat
zahradu	zahrada	k1gFnSc4	zahrada
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
a	a	k8xC	a
tolerance	tolerance	k1gFnSc1	tolerance
prostředí	prostředí	k1gNnSc2	prostředí
vhodného	vhodný	k2eAgNnSc2d1	vhodné
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
drobných	drobný	k2eAgMnPc2d1	drobný
živočichů	živočich	k1gMnPc2	živočich
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
nižší	nízký	k2eAgFnSc2d2	nižší
úrody	úroda	k1gFnSc2	úroda
a	a	k8xC	a
klíšťat	klíště	k1gNnPc2	klíště
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc1	pěstování
směsi	směs	k1gFnSc2	směs
divokých	divoký	k2eAgFnPc2d1	divoká
trvalek	trvalka	k1gFnPc2	trvalka
a	a	k8xC	a
místních	místní	k2eAgInPc2d1	místní
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
vždy	vždy	k6eAd1	vždy
napomáhat	napomáhat	k5eAaBmF	napomáhat
sousedským	sousedský	k2eAgInPc3d1	sousedský
vztahům	vztah	k1gInPc3	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
konflikt	konflikt	k1gInSc1	konflikt
(	(	kIx(	(
<g/>
i	i	k9	i
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
<g/>
,	,	kIx,	,
v	v	k7c4	v
duši	duše	k1gFnSc4	duše
majitele	majitel	k1gMnSc2	majitel
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
tradicí	tradice	k1gFnSc7	tradice
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
estetiky	estetika	k1gFnSc2	estetika
a	a	k8xC	a
citlivostí	citlivost	k1gFnSc7	citlivost
k	k	k7c3	k
ekosystému	ekosystém	k1gInSc3	ekosystém
<g/>
,	,	kIx,	,
vnímavostí	vnímavost	k1gFnSc7	vnímavost
pro	pro	k7c4	pro
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
krásu	krása	k1gFnSc4	krása
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ochota	ochota	k1gFnSc1	ochota
vytvářet	vytvářet	k5eAaImF	vytvářet
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
zahrady	zahrada	k1gFnPc4	zahrada
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
vzhledu	vzhled	k1gInSc6	vzhled
zahrady	zahrada	k1gFnSc2	zahrada
souseda	soused	k1gMnSc2	soused
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kosí	kosit	k5eAaImIp3nS	kosit
pravidelně	pravidelně	k6eAd1	pravidelně
svůj	svůj	k3xOyFgInSc4	svůj
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
trávník	trávník	k1gInSc4	trávník
<g/>
,	,	kIx,	,
cítí	cítit	k5eAaImIp3nS	cítit
majitel	majitel	k1gMnSc1	majitel
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
zahrady	zahrada	k1gFnSc2	zahrada
povinnost	povinnost	k1gFnSc4	povinnost
udělat	udělat	k5eAaPmF	udělat
totéž	týž	k3xTgNnSc4	týž
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
tendence	tendence	k1gFnSc1	tendence
vytvářet	vytvářet	k5eAaImF	vytvářet
zahrady	zahrada	k1gFnPc4	zahrada
podobné	podobný	k2eAgFnSc3d1	podobná
přírodě	příroda	k1gFnSc3	příroda
objevují	objevovat	k5eAaImIp3nP	objevovat
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
u	u	k7c2	u
Alexandra	Alexandr	k1gMnSc2	Alexandr
Popeho	Pope	k1gMnSc2	Pope
a	a	k8xC	a
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
u	u	k7c2	u
G.	G.	kA	G.
Jekyllové	Jekyllová	k1gFnSc2	Jekyllová
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
stávají	stávat	k5eAaImIp3nP	stávat
módními	módní	k2eAgFnPc7d1	módní
přírodní	přírodní	k2eAgInPc1d1	přírodní
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
napodobující	napodobující	k2eAgFnPc4d1	napodobující
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
přírodě	příroda	k1gFnSc3	příroda
blízké	blízký	k2eAgFnSc3d1	blízká
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
architekty	architekt	k1gMnPc4	architekt
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
takové	takový	k3xDgFnPc4	takový
zahrady	zahrada	k1gFnPc4	zahrada
popularizují	popularizovat	k5eAaImIp3nP	popularizovat
<g/>
,	,	kIx,	,
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
<g/>
,	,	kIx,	,
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
a	a	k8xC	a
realizují	realizovat	k5eAaBmIp3nP	realizovat
patří	patřit	k5eAaImIp3nS	patřit
Piet	pieta	k1gFnPc2	pieta
Oudolf	Oudolf	k1gMnSc1	Oudolf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc4d1	malá
zahrady	zahrada	k1gFnPc4	zahrada
věrně	věrně	k6eAd1	věrně
napodobující	napodobující	k2eAgFnSc4d1	napodobující
přírodu	příroda	k1gFnSc4	příroda
často	často	k6eAd1	často
vypadají	vypadat	k5eAaPmIp3nP	vypadat
zanedbaně	zanedbaně	k6eAd1	zanedbaně
<g/>
,	,	kIx,	,
nevzhledně	vzhledně	k6eNd1	vzhledně
nebo	nebo	k8xC	nebo
nevhodně	vhodně	k6eNd1	vhodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	on	k3xPp3gInPc4	on
esteticky	esteticky	k6eAd1	esteticky
stylizovat	stylizovat	k5eAaImF	stylizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tato	tento	k3xDgFnSc1	tento
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
byla	být	k5eAaImAgFnS	být
estetickou	estetický	k2eAgFnSc7d1	estetická
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
zahrada	zahrada	k1gFnSc1	zahrada
===	===	k?	===
</s>
</p>
<p>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
zahrada	zahrada	k1gFnSc1	zahrada
jako	jako	k8xS	jako
pojmenování	pojmenování	k1gNnSc1	pojmenování
typu	typ	k1gInSc2	typ
nebo	nebo	k8xC	nebo
vzoru	vzor	k1gInSc2	vzor
úpravy	úprava	k1gFnSc2	úprava
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
zahrad	zahrada	k1gFnPc2	zahrada
určených	určený	k2eAgFnPc2d1	určená
specificky	specificky	k6eAd1	specificky
pro	pro	k7c4	pro
nějaké	nějaký	k3yIgNnSc4	nějaký
užití	užití	k1gNnSc4	užití
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
odedávna	odedávna	k6eAd1	odedávna
určeny	určit	k5eAaPmNgInP	určit
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
krmítka	krmítko	k1gNnPc1	krmítko
<g/>
,	,	kIx,	,
budky	budka	k1gFnPc1	budka
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
dřevin	dřevina	k1gFnPc2	dřevina
ponechané	ponechaný	k2eAgFnSc2d1	ponechaná
pro	pro	k7c4	pro
potravu	potrava	k1gFnSc4	potrava
ptákům	pták	k1gMnPc3	pták
<g/>
,	,	kIx,	,
nověji	nově	k6eAd2	nově
i	i	k9	i
ptačí	ptačí	k2eAgNnPc4d1	ptačí
koupaliště	koupaliště	k1gNnPc4	koupaliště
a	a	k8xC	a
pítka	pítko	k1gNnPc4	pítko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
činěno	činit	k5eAaImNgNnS	činit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
potěšen	potěšit	k5eAaPmNgMnS	potěšit
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
skotačení	skotačení	k1gNnSc4	skotačení
sýkorek	sýkorka	k1gFnPc2	sýkorka
a	a	k8xC	a
rád	rád	k6eAd1	rád
slyší	slyšet	k5eAaImIp3nS	slyšet
zpěv	zpěv	k1gInSc1	zpěv
kosů	kos	k1gMnPc2	kos
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
úpravy	úprava	k1gFnPc1	úprava
častější	častý	k2eAgFnPc1d2	častější
a	a	k8xC	a
Gertrude	Gertrud	k1gInSc5	Gertrud
Jekyll	Jekyll	k1gMnSc1	Jekyll
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Barvy	barva	k1gFnSc2	barva
v	v	k7c6	v
květinové	květinový	k2eAgFnSc6d1	květinová
zahradě	zahrada	k1gFnSc6	zahrada
o	o	k7c6	o
takové	takový	k3xDgFnSc6	takový
části	část	k1gFnSc6	část
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zahradní	zahradní	k2eAgFnSc3d1	zahradní
úpravě	úprava	k1gFnSc3	úprava
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
<g/>
...	...	k?	...
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
volného	volný	k2eAgInSc2d1	volný
prostoru	prostor	k1gInSc2	prostor
byly	být	k5eAaImAgFnP	být
vysázeno	vysázen	k2eAgNnSc4d1	vysázeno
ovoce	ovoce	k1gNnSc4	ovoce
divočejšího	divoký	k2eAgInSc2d2	divočejší
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
mišpule	mišpule	k1gFnPc1	mišpule
a	a	k8xC	a
kdoule	kdoule	k1gFnPc1	kdoule
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
půvabných	půvabný	k2eAgInPc2d1	půvabný
malých	malý	k2eAgInPc2d1	malý
britských	britský	k2eAgInPc2d1	britský
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jeřáby	jeřáb	k1gInPc1	jeřáb
<g/>
,	,	kIx,	,
blumy	bluma	k1gFnPc1	bluma
<g/>
,	,	kIx,	,
myrobalány	myrobalán	k1gInPc1	myrobalán
<g/>
,	,	kIx,	,
pláňata	pláně	k1gNnPc1	pláně
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
plody	plod	k1gInPc4	plod
ke	k	k7c3	k
sklizni	sklizeň	k1gFnSc3	sklizeň
nikomu	nikdo	k3yNnSc3	nikdo
jinému	jiný	k2eAgNnSc3d1	jiné
než	než	k8xS	než
ptákům	pták	k1gMnPc3	pták
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
pohled	pohled	k1gInSc4	pohled
botaniků	botanik	k1gMnPc2	botanik
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgFnPc4d1	krásná
dřeviny	dřevina	k1gFnPc4	dřevina
s	s	k7c7	s
květy	květ	k1gInPc7	květ
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
horský	horský	k2eAgInSc4d1	horský
jasan	jasan	k1gInSc4	jasan
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnPc4d1	divoká
třešně	třešeň	k1gFnPc4	třešeň
<g/>
,	,	kIx,	,
trnky	trnka	k1gFnPc4	trnka
a	a	k8xC	a
ve	v	k7c4	v
velkoplodé	velkoplodý	k2eAgInPc4d1	velkoplodý
hlohy	hloh	k1gInPc4	hloh
<g/>
,	,	kIx,	,
ptáčnice	ptáčnice	k1gFnPc4	ptáčnice
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gInSc4	jeřáb
muk	muka	k1gFnPc2	muka
<g/>
,	,	kIx,	,
cesmíny	cesmína	k1gFnSc2	cesmína
a	a	k8xC	a
muchovník	muchovník	k1gInSc1	muchovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
druhy	druh	k1gInPc1	druh
dřevin	dřevina	k1gFnPc2	dřevina
mohou	moct	k5eAaImIp3nP	moct
dobře	dobře	k6eAd1	dobře
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
i	i	k8xC	i
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
záměrně	záměrně	k6eAd1	záměrně
lákají	lákat	k5eAaImIp3nP	lákat
do	do	k7c2	do
zahrad	zahrada	k1gFnPc2	zahrada
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
upravaují	upravaovat	k5eAaPmIp3nP	upravaovat
je	on	k3xPp3gFnPc4	on
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ptačí	ptačí	k2eAgFnSc2d1	ptačí
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnSc7d1	ptačí
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
úpravou	úprava	k1gFnSc7	úprava
<g/>
,	,	kIx,	,
vyčleněním	vyčlenění	k1gNnSc7	vyčlenění
části	část	k1gFnSc2	část
zahrady	zahrada	k1gFnSc2	zahrada
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
část	část	k1gFnSc1	část
zahrady	zahrada	k1gFnSc2	zahrada
upravená	upravený	k2eAgFnSc1d1	upravená
jako	jako	k8xC	jako
velká	velký	k2eAgFnSc1d1	velká
voliéra	voliéra	k1gFnSc1	voliéra
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Isfahan	Isfahan	k1gMnSc1	Isfahan
(	(	kIx(	(
<g/>
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
zvířecí	zvířecí	k2eAgInPc4d1	zvířecí
koutky	koutek	k1gInPc4	koutek
s	s	k7c7	s
volérami	voléra	k1gFnPc7	voléra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
parcích	park	k1gInPc6	park
vyskytovaly	vyskytovat	k5eAaImAgFnP	vyskytovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc4d1	jiný
smysl	smysl	k1gInSc4	smysl
ptačí	ptačí	k2eAgFnSc2d1	ptačí
zahrady	zahrada	k1gFnSc2	zahrada
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
pozemku	pozemek	k1gInSc2	pozemek
se	s	k7c7	s
zeleninou	zelenina	k1gFnSc7	zelenina
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
pěstovanými	pěstovaný	k2eAgFnPc7d1	pěstovaná
rostlinami	rostlina	k1gFnPc7	rostlina
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
a	a	k8xC	a
nechráněna	chránit	k5eNaImNgFnS	chránit
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
zahrady	zahrada	k1gFnSc2	zahrada
před	před	k7c7	před
divokými	divoký	k2eAgNnPc7d1	divoké
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
úprava	úprava	k1gFnSc1	úprava
má	mít	k5eAaImIp3nS	mít
zabránit	zabránit	k5eAaPmF	zabránit
devastaci	devastace	k1gFnSc4	devastace
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
způsobem	způsob	k1gInSc7	způsob
ochrany	ochrana	k1gFnSc2	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Estetika	estetika	k1gFnSc1	estetika
zahrad	zahrada	k1gFnPc2	zahrada
==	==	k?	==
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhově	druhově	k6eAd1	druhově
chudé	chudý	k2eAgFnPc4d1	chudá
a	a	k8xC	a
nudné	nudný	k2eAgFnPc4d1	nudná
zahrady	zahrada	k1gFnPc4	zahrada
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
estetické	estetický	k2eAgNnSc4d1	estetické
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c2	za
krásné	krásný	k2eAgFnSc2d1	krásná
zcela	zcela	k6eAd1	zcela
chaotické	chaotický	k2eAgFnSc2d1	chaotická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
alespoň	alespoň	k9	alespoň
pestřejší	pestrý	k2eAgFnPc1d2	pestřejší
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
bohatší	bohatý	k2eAgMnPc1d2	bohatší
na	na	k7c4	na
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
podobné	podobný	k2eAgFnSc6d1	podobná
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
vhodnější	vhodný	k2eAgMnPc1d2	vhodnější
pro	pro	k7c4	pro
drobné	drobný	k2eAgMnPc4d1	drobný
živočichy	živočich	k1gMnPc4	živočich
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vzhledné	vzhledný	k2eAgFnPc1d1	vzhledná
<g/>
.	.	kIx.	.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
krásy	krása	k1gFnSc2	krása
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c4	na
mínění	mínění	k1gNnSc4	mínění
sousedů	soused	k1gMnPc2	soused
a	a	k8xC	a
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
mínění	mínění	k1gNnSc4	mínění
širšího	široký	k2eAgNnSc2d2	širší
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
prioritách	priorita	k1gFnPc6	priorita
<g/>
,	,	kIx,	,
hodnotovém	hodnotový	k2eAgInSc6d1	hodnotový
žebříčku	žebříček	k1gInSc6	žebříček
celé	celý	k2eAgFnSc2d1	celá
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
zahrad	zahrada	k1gFnPc2	zahrada
je	být	k5eAaImIp3nS	být
námětem	námět	k1gInSc7	námět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tvořivosti	tvořivost	k1gFnSc6	tvořivost
i	i	k8xC	i
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
pracích	prak	k1gInPc6	prak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
objekty	objekt	k1gInPc4	objekt
významné	významný	k2eAgFnSc2d1	významná
kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
se	se	k3xPyFc4	se
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
krásné	krásný	k2eAgFnPc4d1	krásná
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
charakterizovány	charakterizován	k2eAgInPc1d1	charakterizován
zcela	zcela	k6eAd1	zcela
opačnými	opačný	k2eAgFnPc7d1	opačná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
snadno	snadno	k6eAd1	snadno
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
kráse	krása	k1gFnSc6	krása
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
mínění	mínění	k1gNnSc4	mínění
okolí	okolí	k1gNnSc2	okolí
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
nechají	nechat	k5eAaPmIp3nP	nechat
řídit	řídit	k5eAaImF	řídit
přímo	přímo	k6eAd1	přímo
pokyny	pokyn	k1gInPc4	pokyn
takové	takový	k3xDgFnSc2	takový
osoby	osoba	k1gFnSc2	osoba
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
arbiter	arbiter	k1gMnSc1	arbiter
elegantum	elegantum	k?	elegantum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kulturtrégra	kulturtréger	k1gMnSc4	kulturtréger
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
módní	módní	k2eAgFnSc1d1	módní
policie	policie	k1gFnSc1	policie
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
se	se	k3xPyFc4	se
ale	ale	k9	ale
lze	lze	k6eAd1	lze
často	často	k6eAd1	často
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
potřebou	potřeba	k1gFnSc7	potřeba
napodobovat	napodobovat	k5eAaImF	napodobovat
sousední	sousední	k2eAgFnPc4d1	sousední
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
nevybočovat	vybočovat	k5eNaImF	vybočovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvorbou	tvorba	k1gFnSc7	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc4	dílo
architekta	architekt	k1gMnSc2	architekt
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
splňovat	splňovat	k5eAaImF	splňovat
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
následovat	následovat	k5eAaImF	následovat
architektonické	architektonický	k2eAgNnSc4d1	architektonické
řešení	řešení	k1gNnSc4	řešení
okolní	okolní	k2eAgFnSc2d1	okolní
zástavby	zástavba	k1gFnSc2	zástavba
nebo	nebo	k8xC	nebo
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nebýt	být	k5eNaImF	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
a	a	k8xC	a
esteticky	esteticky	k6eAd1	esteticky
jej	on	k3xPp3gMnSc4	on
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
splňovat	splňovat	k5eAaImF	splňovat
představy	představa	k1gFnPc4	představa
majitele	majitel	k1gMnSc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nelze	lze	k6eNd1	lze
někdy	někdy	k6eAd1	někdy
rozlišit	rozlišit	k5eAaPmF	rozlišit
(	(	kIx(	(
<g/>
špatné	špatný	k2eAgNnSc4d1	špatné
nebo	nebo	k8xC	nebo
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
)	)	kIx)	)
dílo	dílo	k1gNnSc4	dílo
laika	laik	k1gMnSc2	laik
a	a	k8xC	a
architekta	architekt	k1gMnSc2	architekt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
připsat	připsat	k5eAaPmF	připsat
estetické	estetický	k2eAgFnPc1d1	estetická
schopnosti	schopnost	k1gFnPc1	schopnost
laiků	laik	k1gMnPc2	laik
kreativně	kreativně	k6eAd1	kreativně
se	se	k3xPyFc4	se
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
architektů	architekt	k1gMnPc2	architekt
nikdy	nikdy	k6eAd1	nikdy
zahradní	zahradní	k2eAgFnSc4d1	zahradní
architekturu	architektura	k1gFnSc4	architektura
nestudovali	studovat	k5eNaImAgMnP	studovat
<g/>
,	,	kIx,	,
nechyběla	chybět	k5eNaImAgFnS	chybět
jim	on	k3xPp3gMnPc3	on
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
kreativita	kreativita	k1gFnSc1	kreativita
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
myslet	myslet	k5eAaImF	myslet
analyticky	analyticky	k6eAd1	analyticky
<g/>
,	,	kIx,	,
zohlednit	zohlednit	k5eAaPmF	zohlednit
potřeby	potřeba	k1gFnPc4	potřeba
zákazníka	zákazník	k1gMnSc2	zákazník
a	a	k8xC	a
často	často	k6eAd1	často
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
se	s	k7c7	s
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zahrada	zahrada	k1gFnSc1	zahrada
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
zahradním	zahradní	k2eAgInSc7d1	zahradní
architektem	architekt	k1gMnSc7	architekt
není	být	k5eNaImIp3nS	být
zárukou	záruka	k1gFnSc7	záruka
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nějaké	nějaký	k3yIgNnSc1	nějaký
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
i	i	k9	i
estetické	estetický	k2eAgFnSc2d1	estetická
<g/>
)	)	kIx)	)
kvality	kvalita	k1gFnSc2	kvalita
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
použitelná	použitelný	k2eAgFnSc1d1	použitelná
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
výsadby	výsadba	k1gFnSc2	výsadba
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
představě	představa	k1gFnSc6	představa
majitele	majitel	k1gMnSc2	majitel
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sadovnická	sadovnický	k2eAgFnSc1d1	Sadovnická
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
umělecký	umělecký	k2eAgInSc4d1	umělecký
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kromě	kromě	k7c2	kromě
představivosti	představivost	k1gFnSc2	představivost
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
například	například	k6eAd1	například
technické	technický	k2eAgFnPc4d1	technická
znalosti	znalost	k1gFnPc4	znalost
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
zahrad	zahrada	k1gFnPc2	zahrada
mohou	moct	k5eAaImIp3nP	moct
pomáhat	pomáhat	k5eAaImF	pomáhat
architektům	architekt	k1gMnPc3	architekt
pravidla	pravidlo	k1gNnSc2	pravidlo
kompozice	kompozice	k1gFnSc2	kompozice
zahradních	zahradní	k2eAgFnPc2d1	zahradní
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
už	už	k6eAd1	už
v	v	k7c6	v
italských	italský	k2eAgFnPc6d1	italská
renesančních	renesanční	k2eAgFnPc6d1	renesanční
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
nebo	nebo	k8xC	nebo
atributy	atribut	k1gInPc1	atribut
možné	možný	k2eAgFnSc2d1	možná
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
estetický	estetický	k2eAgInSc4d1	estetický
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
nebo	nebo	k8xC	nebo
dílo	dílo	k1gNnSc4	dílo
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
vytvořit	vytvořit	k5eAaPmF	vytvořit
či	či	k8xC	či
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
shrnuta	shrnut	k2eAgFnSc1d1	shrnuta
<g/>
,	,	kIx,	,
definována	definován	k2eAgFnSc1d1	definována
a	a	k8xC	a
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
H.	H.	kA	H.
<g/>
Reptonem	Repton	k1gInSc7	Repton
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přiměřenost	přiměřenost	k1gFnSc1	přiměřenost
</s>
</p>
<p>
<s>
Užitečnost	užitečnost	k1gFnSc1	užitečnost
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
Symetrie	symetrie	k1gFnSc1	symetrie
</s>
</p>
<p>
<s>
Malebný	malebný	k2eAgInSc1d1	malebný
účinek	účinek	k1gInSc1	účinek
</s>
</p>
<p>
<s>
Spletitost	spletitost	k1gFnSc1	spletitost
a	a	k8xC	a
komplikovanost	komplikovanost	k1gFnSc1	komplikovanost
</s>
</p>
<p>
<s>
Jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
</s>
</p>
<p>
<s>
Rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
</s>
</p>
<p>
<s>
Novost	novost	k1gFnSc1	novost
</s>
</p>
<p>
<s>
Kontrast	kontrast	k1gInSc1	kontrast
</s>
</p>
<p>
<s>
Spojitost	spojitost	k1gFnSc1	spojitost
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
</s>
</p>
<p>
<s>
Majestátnost	majestátnost	k1gFnSc1	majestátnost
</s>
</p>
<p>
<s>
Přivlastnění	přivlastnění	k1gNnSc1	přivlastnění
</s>
</p>
<p>
<s>
Oživení	oživení	k1gNnSc1	oživení
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
a	a	k8xC	a
vliv	vliv	k1gInSc1	vliv
průběhu	průběh	k1gInSc2	průběh
dne	den	k1gInSc2	den
</s>
</p>
<p>
<s>
Dokonalost	dokonalost	k1gFnSc1	dokonalost
provedeníPravidla	provedeníPravidlo	k1gNnSc2	provedeníPravidlo
kompozice	kompozice	k1gFnSc2	kompozice
v	v	k7c6	v
zahradní	zahradní	k2eAgFnSc6d1	zahradní
architektuře	architektura	k1gFnSc6	architektura
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
definují	definovat	k5eAaBmIp3nP	definovat
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
,	,	kIx,	,
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
úmyslně	úmyslně	k6eAd1	úmyslně
upraveného	upravený	k2eAgInSc2d1	upravený
zahradního	zahradní	k2eAgInSc2d1	zahradní
celku	celek	k1gInSc2	celek
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
</s>
</p>
<p>
<s>
Ucelenost	ucelenost	k1gFnSc1	ucelenost
</s>
</p>
<p>
<s>
Harmonie	harmonie	k1gFnSc1	harmonie
</s>
</p>
<p>
<s>
SmysluplnostZahradní	SmysluplnostZahradní	k2eAgFnSc1d1	SmysluplnostZahradní
architektura	architektura	k1gFnSc1	architektura
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
popisována	popisovat	k5eAaImNgFnS	popisovat
jako	jako	k8xS	jako
směs	směs	k1gFnSc1	směs
různých	různý	k2eAgInPc2d1	různý
zcela	zcela	k6eAd1	zcela
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
směrů	směr	k1gInPc2	směr
bez	bez	k7c2	bez
nějakého	nějaký	k3yIgInSc2	nějaký
vyhraněného	vyhraněný	k2eAgInSc2d1	vyhraněný
převládajícího	převládající	k2eAgInSc2d1	převládající
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
20	[number]	k4	20
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
popisováno	popisován	k2eAgNnSc1d1	popisováno
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
samotnými	samotný	k2eAgMnPc7d1	samotný
architekty	architekt	k1gMnPc7	architekt
jako	jako	k8xC	jako
stížnost	stížnost	k1gFnSc4	stížnost
na	na	k7c4	na
málo	málo	k6eAd1	málo
hodnotnou	hodnotný	k2eAgFnSc4d1	hodnotná
novodobou	novodobý	k2eAgFnSc4d1	novodobá
architekturu	architektura	k1gFnSc4	architektura
<g/>
,	,	kIx,	,
že	že	k8xS	že
...	...	k?	...
<g/>
stále	stále	k6eAd1	stále
chybí	chybět	k5eAaImIp3nS	chybět
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
široká	široký	k2eAgFnSc1d1	široká
vrstva	vrstva	k1gFnSc1	vrstva
osvícených	osvícený	k2eAgMnPc2d1	osvícený
investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nesledují	sledovat	k5eNaImIp3nP	sledovat
jen	jen	k9	jen
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
cíle	cíl	k1gInPc4	cíl
vlastní	vlastní	k2eAgFnSc2d1	vlastní
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
jim	on	k3xPp3gMnPc3	on
o	o	k7c6	o
víc	hodně	k6eAd2	hodně
-	-	kIx~	-
o	o	k7c4	o
tvorbu	tvorba	k1gFnSc4	tvorba
kulturního	kulturní	k2eAgNnSc2d1	kulturní
zázemí	zázemí	k1gNnSc2	zázemí
<g/>
,	,	kIx,	,
o	o	k7c4	o
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
architekturu	architektura	k1gFnSc4	architektura
<g/>
,	,	kIx,	,
o	o	k7c4	o
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
okamžitým	okamžitý	k2eAgNnSc7d1	okamžité
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
poučená	poučený	k2eAgFnSc1d1	poučená
a	a	k8xC	a
hluboce	hluboko	k6eAd1	hluboko
kulturní	kulturní	k2eAgFnSc1d1	kulturní
vrstva	vrstva	k1gFnSc1	vrstva
nemohla	moct	k5eNaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
za	za	k7c4	za
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
počkat	počkat	k5eAaPmF	počkat
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
...	...	k?	...
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
architektura	architektura	k1gFnSc1	architektura
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
třeba	třeba	k6eAd1	třeba
výstavba	výstavba	k1gFnSc1	výstavba
v	v	k7c6	v
Klánovicích	Klánovice	k1gFnPc6	Klánovice
u	u	k7c2	u
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
rozsah	rozsah	k1gInSc1	rozsah
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
úpravy	úprava	k1gFnSc2	úprava
zeleně	zeleně	k6eAd1	zeleně
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
Ale	ale	k9	ale
i	i	k9	i
obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
stoupajícím	stoupající	k2eAgFnPc3d1	stoupající
cenám	cena	k1gFnPc3	cena
pozemků	pozemek	k1gInPc2	pozemek
jsou	být	k5eAaImIp3nP	být
zahrady	zahrada	k1gFnPc4	zahrada
stále	stále	k6eAd1	stále
menší	malý	k2eAgFnPc4d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
zahradní	zahradní	k2eAgMnSc1d1	zahradní
architekti	architekt	k1gMnPc1	architekt
==	==	k?	==
</s>
</p>
<p>
<s>
Zahradní	zahradní	k2eAgMnPc1d1	zahradní
architekti	architekt	k1gMnPc1	architekt
jsou	být	k5eAaImIp3nP	být
tvůrci	tvůrce	k1gMnPc1	tvůrce
návrhů	návrh	k1gInPc2	návrh
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
realizátoři	realizátor	k1gMnPc1	realizátor
svých	svůj	k3xOyFgInPc2	svůj
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Mail	mail	k1gInSc1	mail
byli	být	k5eAaImAgMnP	být
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
zahradními	zahradní	k2eAgMnPc7d1	zahradní
architekty	architekt	k1gMnPc7	architekt
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Corner	Corner	k1gMnSc1	Corner
</s>
</p>
<p>
<s>
Roberto	Roberta	k1gFnSc5	Roberta
Burle	Burle	k1gFnSc5	Burle
Marx	Marx	k1gMnSc1	Marx
</s>
</p>
<p>
<s>
Helen	Helena	k1gFnPc2	Helena
Dillon	Dillon	k1gInSc4	Dillon
</s>
</p>
<p>
<s>
Mary	Mary	k1gFnSc1	Mary
Reynolds	Reynoldsa	k1gFnPc2	Reynoldsa
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Jencks	Jencksa	k1gFnPc2	Jencksa
</s>
</p>
<p>
<s>
André	André	k1gMnSc5	André
Le	Le	k1gMnSc5	Le
Nôtre	Nôtr	k1gMnSc5	Nôtr
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Sitta	Sitta	k1gMnSc1	Sitta
</s>
</p>
<p>
<s>
Lancelot	Lancelot	k1gInSc1	Lancelot
"	"	kIx"	"
<g/>
Capability	Capabilita	k1gFnSc2	Capabilita
<g/>
"	"	kIx"	"
Brown	Brown	k1gMnSc1	Brown
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Lloyd	Lloyda	k1gFnPc2	Lloyda
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gMnPc3	on
DenevanJiní	DenevanJiní	k1gFnSc3	DenevanJiní
autoři	autor	k1gMnPc1	autor
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
významné	významný	k2eAgMnPc4d1	významný
zahradní	zahradní	k2eAgMnPc4d1	zahradní
architekty	architekt	k1gMnPc4	architekt
jako	jako	k8xC	jako
Gilles	Gilles	k1gMnSc1	Gilles
Clément	Clément	k1gMnSc1	Clément
<g/>
,	,	kIx,	,
Gertrude	Gertrud	k1gInSc5	Gertrud
Jekyll	Jekyll	k1gMnSc1	Jekyll
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Dolliver	Dolliver	k1gMnSc1	Dolliver
Church	Church	k1gMnSc1	Church
,	,	kIx,	,
Geoffrey	Geoffre	k2eAgFnPc1d1	Geoffre
Jellicoe	Jellico	k1gFnPc1	Jellico
<g/>
,	,	kIx,	,
Kathryn	Kathryn	k1gMnSc1	Kathryn
Gustafson	Gustafson	k1gMnSc1	Gustafson
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Walker	Walker	k1gMnSc1	Walker
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Wirtz	Wirtz	k1gMnSc1	Wirtz
<g/>
,	,	kIx,	,
Vita	vit	k2eAgFnSc1d1	Vita
Sackville-West	Sackville-West	k1gFnSc1	Sackville-West
<g/>
,	,	kIx,	,
Penelope	Penelop	k1gMnSc5	Penelop
Hobhouse	Hobhous	k1gMnSc5	Hobhous
<g/>
,	,	kIx,	,
Beth	Beth	k1gInSc4	Beth
Chatto	Chatto	k1gNnSc4	Chatto
<g/>
,	,	kIx,	,
Piet	pieta	k1gFnPc2	pieta
Oudolf	Oudolf	k1gInSc4	Oudolf
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Isamu	Isam	k1gInSc3	Isam
Noguchi	Noguch	k1gFnSc2	Noguch
<g/>
,	,	kIx,	,
Martha	Martha	k1gMnSc1	Martha
Schwartz	Schwartz	k1gMnSc1	Schwartz
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Lloyd	Lloyd	k1gMnSc1	Lloyd
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
Ted	Ted	k1gMnSc1	Ted
Smyth	Smyth	k1gMnSc1	Smyth
<g/>
,	,	kIx,	,
Russell	Russell	k1gMnSc1	Russell
Page	Pag	k1gFnSc2	Pag
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
Kiley	Kilea	k1gFnSc2	Kilea
<g/>
,	,	kIx,	,
Topher	Tophra	k1gFnPc2	Tophra
Delaney	Delanea	k1gFnSc2	Delanea
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gFnSc1	Andrew
Jackson	Jackson	k1gMnSc1	Jackson
Downing	Downing	k1gInSc1	Downing
<g/>
,	,	kIx,	,
Humphrey	Humphrea	k1gFnPc1	Humphrea
Repton	Repton	k1gInSc1	Repton
<g/>
,	,	kIx,	,
Geoffrey	Geoffrea	k1gFnPc1	Geoffrea
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Mien	Mien	k1gMnSc1	Mien
Ruys	Ruysa	k1gFnPc2	Ruysa
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Tunnard	Tunnarda	k1gFnPc2	Tunnarda
<g/>
,	,	kIx,	,
Fletcher	Fletchra	k1gFnPc2	Fletchra
Steele	Steel	k1gInSc2	Steel
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
van	vana	k1gFnPc2	vana
Sweden	Swedna	k1gFnPc2	Swedna
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Oehme	Oehm	k1gInSc5	Oehm
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Stuart-Smith	Stuart-Smith	k1gMnSc1	Stuart-Smith
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
Cochran	Cochran	k1gInSc1	Cochran
<g/>
,	,	kIx,	,
Judy	judo	k1gNnPc7	judo
Kameon	Kameon	k1gNnSc1	Kameon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dotčené	dotčený	k2eAgInPc1d1	dotčený
obory	obor	k1gInPc1	obor
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
kompozicí	kompozice	k1gFnSc7	kompozice
a	a	k8xC	a
plánováním	plánování	k1gNnSc7	plánování
zahrad	zahrada	k1gFnPc2	zahrada
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
obory	obor	k1gInPc4	obor
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
umělecké	umělecký	k2eAgNnSc1d1	umělecké
<g/>
,	,	kIx,	,
výtvarně	výtvarně	k6eAd1	výtvarně
působící	působící	k2eAgInPc1d1	působící
objekty	objekt	k1gInPc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
musí	muset	k5eAaImIp3nS	muset
architekt	architekt	k1gMnSc1	architekt
mimo	mimo	k7c4	mimo
technické	technický	k2eAgFnPc4d1	technická
a	a	k8xC	a
biologické	biologický	k2eAgFnPc4d1	biologická
znalosti	znalost	k1gFnPc4	znalost
ovládat	ovládat	k5eAaImF	ovládat
také	také	k9	také
základní	základní	k2eAgFnPc4d1	základní
znalosti	znalost	k1gFnPc4	znalost
psychologie	psychologie	k1gFnSc2	psychologie
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
psychologicky	psychologicky	k6eAd1	psychologicky
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
zahrady	zahrada	k1gFnPc4	zahrada
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
spokojenosti	spokojenost	k1gFnSc2	spokojenost
uživatele	uživatel	k1gMnSc2	uživatel
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
i	i	k9	i
znalosti	znalost	k1gFnPc1	znalost
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
nebo	nebo	k8xC	nebo
invence	invence	k1gFnSc2	invence
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
vklady	vklad	k1gInPc1	vklad
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stávají	stávat	k5eAaImIp3nP	stávat
součástí	součást	k1gFnSc7	součást
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
architektury	architektura	k1gFnSc2	architektura
staveb	stavba	k1gFnPc2	stavba
jsou	být	k5eAaImIp3nP	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
větších	veliký	k2eAgInPc2d2	veliký
stavebních	stavební	k2eAgInPc2d1	stavební
celků	celek	k1gInPc2	celek
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nP	by
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
být	být	k5eAaImF	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
zahrady	zahrada	k1gFnSc2	zahrada
bývají	bývat	k5eAaImIp3nP	bývat
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
znalosti	znalost	k1gFnPc4	znalost
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
týkající	týkající	k2eAgMnSc1d1	týkající
se	se	k3xPyFc4	se
přesunu	přesunout	k5eAaPmIp1nS	přesunout
zeminy	zemina	k1gFnSc2	zemina
<g/>
,	,	kIx,	,
stavby	stavba	k1gFnSc2	stavba
menších	malý	k2eAgFnPc2d2	menší
stavebních	stavební	k2eAgFnPc2d1	stavební
úprav	úprava	k1gFnPc2	úprava
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
terasy	terasa	k1gFnPc1	terasa
<g/>
,	,	kIx,	,
pergoly	pergola	k1gFnPc1	pergola
u	u	k7c2	u
osoby	osoba	k1gFnSc2	osoba
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
zahradu	zahrada	k1gFnSc4	zahrada
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
zahradní	zahradní	k2eAgMnSc1d1	zahradní
architekt	architekt	k1gMnSc1	architekt
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
alespoň	alespoň	k9	alespoň
povrchně	povrchně	k6eAd1	povrchně
ovládat	ovládat	k5eAaImF	ovládat
i	i	k9	i
znalosti	znalost	k1gFnPc4	znalost
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
materiálů	materiál	k1gInPc2	materiál
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
například	například	k6eAd1	například
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
mineralogie	mineralogie	k1gFnSc2	mineralogie
i	i	k8xC	i
celkového	celkový	k2eAgInSc2d1	celkový
kontextu	kontext	k1gInSc2	kontext
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
ekologie	ekologie	k1gFnSc1	ekologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozmanitostí	rozmanitost	k1gFnSc7	rozmanitost
rostlin	rostlina	k1gFnPc2	rostlina
použitých	použitý	k2eAgFnPc2d1	použitá
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
botanika	botanika	k1gFnSc1	botanika
a	a	k8xC	a
dendrologie	dendrologie	k1gFnSc1	dendrologie
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
především	především	k6eAd1	především
mnohotvárné	mnohotvárný	k2eAgFnPc4d1	mnohotvárná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
odrůd	odrůda	k1gFnPc2	odrůda
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ovocnářství	ovocnářství	k1gNnSc1	ovocnářství
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
ovocným	ovocný	k2eAgFnPc3d1	ovocná
dřevinám	dřevina	k1gFnPc3	dřevina
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
plody	plod	k1gInPc1	plod
ovoce	ovoce	k1gNnSc2	ovoce
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pomologie	pomologie	k1gFnSc1	pomologie
<g/>
,	,	kIx,	,
květinami	květina	k1gFnPc7	květina
květinářství	květinářství	k1gNnSc2	květinářství
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
určením	určení	k1gNnSc7	určení
a	a	k8xC	a
rozdělením	rozdělení	k1gNnSc7	rozdělení
rostlin	rostlina	k1gFnPc2	rostlina
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
studiem	studio	k1gNnSc7	studio
jejich	jejich	k3xOp3gInSc2	jejich
vzhledu	vzhled	k1gInSc2	vzhled
morfologie	morfologie	k1gFnSc2	morfologie
<g/>
,	,	kIx,	,
technikami	technika	k1gFnPc7	technika
křížení	křížení	k1gNnSc2	křížení
genetika	genetika	k1gFnSc1	genetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Agrotechnika	agrotechnika	k1gFnSc1	agrotechnika
(	(	kIx(	(
<g/>
fytotechnika	fytotechnika	k1gFnSc1	fytotechnika
<g/>
)	)	kIx)	)
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
souhrn	souhrn	k1gInSc1	souhrn
a	a	k8xC	a
správný	správný	k2eAgInSc1d1	správný
sled	sled	k1gInSc1	sled
technických	technický	k2eAgFnPc2d1	technická
operací	operace	k1gFnPc2	operace
v	v	k7c6	v
daných	daný	k2eAgFnPc6d1	daná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
množení	množení	k1gNnSc3	množení
a	a	k8xC	a
umístění	umístění	k1gNnSc4	umístění
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
udržení	udržení	k1gNnSc1	udržení
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
životaschopnosti	životaschopnost	k1gFnSc2	životaschopnost
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc3	jejich
zdárnému	zdárný	k2eAgInSc3d1	zdárný
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oborem	obor	k1gInSc7	obor
zabývajícím	zabývající	k2eAgInSc7d1	zabývající
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
znalosti	znalost	k1gFnPc1	znalost
chemismu	chemismus	k1gInSc2	chemismus
a	a	k8xC	a
výživy	výživa	k1gFnSc2	výživa
rostlin	rostlina	k1gFnPc2	rostlina
ale	ale	k8xC	ale
i	i	k9	i
ochranou	ochrana	k1gFnSc7	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
a	a	k8xC	a
chorobám	choroba	k1gFnPc3	choroba
fytopatologie	fytopatologie	k1gFnSc2	fytopatologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muzeum	muzeum	k1gNnSc1	muzeum
zahrad	zahrada	k1gFnPc2	zahrada
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
zahrad	zahrada	k1gFnPc2	zahrada
(	(	kIx(	(
<g/>
Garden	Gardna	k1gFnPc2	Gardna
Museum	museum	k1gNnSc4	museum
<g/>
)	)	kIx)	)
dříve	dříve	k6eAd2	dříve
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Garden	Gardno	k1gNnPc2	Gardno
History	Histor	k1gInPc7	Histor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
zrušeného	zrušený	k2eAgInSc2d1	zrušený
kostela	kostel	k1gInSc2	kostel
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
na	na	k7c4	na
Lambeth	Lambeth	k1gInSc4	Lambeth
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
lodi	loď	k1gFnSc6	loď
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
příslušenství	příslušenství	k1gNnSc2	příslušenství
a	a	k8xC	a
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
předměty	předmět	k1gInPc4	předmět
zakoupené	zakoupený	k2eAgInPc4d1	zakoupený
v	v	k7c4	v
aukci	aukce	k1gFnSc4	aukce
a	a	k8xC	a
dary	dar	k1gInPc4	dar
od	od	k7c2	od
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
i	i	k8xC	i
zahradnických	zahradnický	k2eAgFnPc2d1	zahradnická
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgInPc1d1	grafický
tisky	tisk	k1gInPc1	tisk
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
plakáty	plakát	k1gInPc1	plakát
<g/>
,	,	kIx,	,
katalogy	katalog	k1gInPc1	katalog
a	a	k8xC	a
brožury	brožura	k1gFnPc1	brožura
<g/>
,	,	kIx,	,
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
historii	historie	k1gFnSc3	historie
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
celý	celý	k2eAgInSc4d1	celý
rozsah	rozsah	k1gInSc4	rozsah
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
z	z	k7c2	z
královských	královský	k2eAgFnPc2d1	královská
zahrad	zahrada	k1gFnPc2	zahrada
po	po	k7c4	po
zahrádkářské	zahrádkářský	k2eAgFnPc4d1	zahrádkářská
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
hřbitově	hřbitov	k1gInSc6	hřbitov
vysázena	vysázen	k2eAgFnSc1d1	vysázena
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
úprav	úprava	k1gFnPc2	úprava
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
knot	knot	k1gInSc1	knot
garden	gardna	k1gFnPc2	gardna
-	-	kIx~	-
výsadby	výsadba	k1gFnPc1	výsadba
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
uzlů	uzel	k1gInPc2	uzel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
je	být	k5eAaImIp3nS	být
osázena	osázet	k5eAaPmNgFnS	osázet
rostlinami	rostlina	k1gFnPc7	rostlina
autentickými	autentický	k2eAgInPc7d1	autentický
pro	pro	k7c4	pro
dané	daný	k2eAgNnSc4d1	dané
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Cestování	cestování	k1gNnPc2	cestování
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
poznávání	poznávání	k1gNnSc1	poznávání
významných	významný	k2eAgFnPc2d1	významná
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
botanických	botanický	k2eAgFnPc2d1	botanická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
zahradních	zahradní	k2eAgInPc2d1	zahradní
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
návštěvy	návštěva	k1gFnSc2	návštěva
nebo	nebo	k8xC	nebo
cestování	cestování	k1gNnSc2	cestování
těchto	tento	k3xDgInPc2	tento
významných	významný	k2eAgInPc2d1	významný
a	a	k8xC	a
specifických	specifický	k2eAgInPc2d1	specifický
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
někdy	někdy	k6eAd1	někdy
cestují	cestovat	k5eAaImIp3nP	cestovat
individuálně	individuálně	k6eAd1	individuálně
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
místa	místo	k1gNnPc4	místo
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
předem	předem	k6eAd1	předem
seznámeni	seznámen	k2eAgMnPc1d1	seznámen
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
turisté	turist	k1gMnPc1	turist
raději	rád	k6eAd2	rád
připojují	připojovat	k5eAaImIp3nP	připojovat
k	k	k7c3	k
organizovaným	organizovaný	k2eAgInPc3d1	organizovaný
zájezdům	zájezd	k1gInPc3	zájezd
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
zahradních	zahradní	k2eAgFnPc2d1	zahradní
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnSc2d1	významná
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Itálie	Itálie	k1gFnSc2	Itálie
===	===	k?	===
</s>
</p>
<p>
<s>
Isola	Isola	k1gMnSc1	Isola
Bella	Bella	k1gMnSc1	Bella
</s>
</p>
<p>
<s>
Zahrady	zahrada	k1gFnPc4	zahrada
Boboli	Bobole	k1gFnSc4	Bobole
</s>
</p>
<p>
<s>
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
</s>
</p>
<p>
<s>
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
Pisa	Pisa	k1gFnSc1	Pisa
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnSc2d1	významná
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Vrtbovská	Vrtbovský	k2eAgFnSc1d1	Vrtbovská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
arboretum	arboretum	k1gNnSc1	arboretum
</s>
</p>
<p>
<s>
park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
sad	sad	k1gInSc1	sad
</s>
</p>
<p>
<s>
lesopark	lesopark	k1gInSc1	lesopark
</s>
</p>
<p>
<s>
zahrádka	zahrádka	k1gFnSc1	zahrádka
</s>
</p>
<p>
<s>
genový	genový	k2eAgInSc4d1	genový
sad	sad	k1gInSc4	sad
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
zahradního	zahradní	k2eAgNnSc2d1	zahradní
umění	umění	k1gNnSc2	umění
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
po	po	k7c4	po
středověk	středověk	k1gInSc4	středověk
</s>
</p>
<p>
<s>
Slovníček	slovníček	k1gInSc1	slovníček
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
okrasných	okrasný	k2eAgFnPc2d1	okrasná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
základní	základní	k2eAgFnSc1d1	základní
údržba	údržba	k1gFnSc1	údržba
</s>
</p>
<p>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
tradičních	tradiční	k2eAgFnPc2d1	tradiční
zahrad	zahrada	k1gFnPc2	zahrada
</s>
</p>
