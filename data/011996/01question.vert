<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
upravený	upravený	k2eAgInSc1d1	upravený
pozemek	pozemek	k1gInSc1	pozemek
s	s	k7c7	s
uměle	uměle	k6eAd1	uměle
vysázenou	vysázený	k2eAgFnSc7d1	vysázená
vegetací	vegetace	k1gFnSc7	vegetace
nebo	nebo	k8xC	nebo
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
nebo	nebo	k8xC	nebo
umístěny	umístěn	k2eAgFnPc1d1	umístěna
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
?	?	kIx.	?
</s>
