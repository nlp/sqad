<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
lidmi	člověk	k1gMnPc7	člověk
upravený	upravený	k2eAgInSc4d1	upravený
pozemek	pozemek	k1gInSc4	pozemek
s	s	k7c7	s
uměle	uměle	k6eAd1	uměle
vysázenou	vysázený	k2eAgFnSc7d1	vysázená
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
nebo	nebo	k8xC	nebo
umístěny	umístěn	k2eAgFnPc1d1	umístěna
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
