<s>
Winchester	Winchester	k1gInSc1	Winchester
je	být	k5eAaImIp3nS	být
historické	historický	k2eAgNnSc4d1	historické
město	město	k1gNnSc4	město
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Anglie	Anglie	k1gFnSc2	Anglie
s	s	k7c7	s
asi	asi	k9	asi
40	[number]	k4	40
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
distriktu	distrikt	k1gInSc2	distrikt
City	City	k1gFnSc2	City
of	of	k?	of
Winchester	Winchester	k1gInSc1	Winchester
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
větší	veliký	k2eAgNnSc4d2	veliký
území	území	k1gNnSc4	území
než	než	k8xS	než
samotný	samotný	k2eAgInSc4d1	samotný
Winchester	Winchester	k1gInSc4	Winchester
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
hrabství	hrabství	k1gNnSc2	hrabství
Hampshire	Hampshir	k1gInSc5	Hampshir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
předtím	předtím	k6eAd1	předtím
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Wessexu	Wessex	k1gInSc2	Wessex
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
osídlení	osídlení	k1gNnSc2	osídlení
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
Doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
současného	současný	k2eAgNnSc2d1	současné
města	město	k1gNnSc2	město
vybudováno	vybudován	k2eAgNnSc4d1	vybudováno
opevnění	opevnění	k1gNnSc4	opevnění
Oram	Orama	k1gFnPc2	Orama
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Arbour	Arboura	k1gFnPc2	Arboura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
Británie	Británie	k1gFnSc2	Británie
Římany	Říman	k1gMnPc4	Říman
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
město	město	k1gNnSc1	město
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
Venta	Vento	k1gNnSc2	Vento
Belgarum	Belgarum	k1gNnSc4	Belgarum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Římanů	Říman	k1gMnPc2	Říman
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
používal	používat	k5eAaImAgInS	používat
název	název	k1gInSc1	název
Caergwinntguic	Caergwinntguice	k1gInPc2	Caergwinntguice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
anglosaském	anglosaský	k2eAgNnSc6d1	anglosaské
období	období	k1gNnSc6	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
519	[number]	k4	519
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
jméno	jméno	k1gNnSc1	jméno
Wintanceastre	Wintanceastr	k1gInSc5	Wintanceastr
<g/>
.	.	kIx.	.
</s>
<s>
Winchester	Winchester	k1gMnSc1	Winchester
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
686	[number]	k4	686
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Caedwala	Caedwal	k1gMnSc2	Caedwal
z	z	k7c2	z
Wessexu	Wessex	k1gInSc2	Wessex
porazil	porazit	k5eAaPmAgMnS	porazit
krále	král	k1gMnSc2	král
Atwalda	Atwalda	k1gMnSc1	Atwalda
z	z	k7c2	z
Wightu	Wight	k1gInSc2	Wight
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
středověkého	středověký	k2eAgNnSc2d1	středověké
království	království	k1gNnSc2	království
Wessex	Wessex	k1gInSc1	Wessex
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nebylo	být	k5eNaImAgNnS	být
jediným	jediný	k2eAgNnSc7d1	jediné
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Egbert	Egbert	k1gMnSc1	Egbert
ho	on	k3xPp3gMnSc4	on
učinil	učinit	k5eAaImAgMnS	učinit
roku	rok	k1gInSc2	rok
827	[number]	k4	827
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
plán	plán	k1gInSc1	plán
ulic	ulice	k1gFnPc2	ulice
vytvořený	vytvořený	k2eAgMnSc1d1	vytvořený
Alfrédem	Alfréd	k1gMnSc7	Alfréd
Velikým	veliký	k2eAgMnSc7d1	veliký
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
pásu	pás	k1gInSc2	pás
opevnění	opevnění	k1gNnSc2	opevnění
podél	podél	k7c2	podél
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Winchester	Winchester	k1gInSc1	Winchester
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Wessexu	Wessex	k1gInSc2	Wessex
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Anglie	Anglie	k1gFnSc2	Anglie
až	až	k6eAd1	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
Anglie	Anglie	k1gFnSc2	Anglie
Normany	Norman	k1gMnPc4	Norman
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
stal	stát	k5eAaPmAgInS	stát
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vážný	vážný	k2eAgInSc1d1	vážný
požár	požár	k1gInSc1	požár
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
nastartoval	nastartovat	k5eAaPmAgInS	nastartovat
postupný	postupný	k2eAgInSc1d1	postupný
úpadek	úpadek	k1gInSc1	úpadek
Winchesteru	Winchester	k1gInSc2	Winchester
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
Viléma	Vilém	k1gMnSc2	Vilém
z	z	k7c2	z
Vykehamu	Vykeham	k1gInSc2	Vykeham
(	(	kIx(	(
<g/>
1320	[number]	k4	1320
<g/>
-	-	kIx~	-
<g/>
1404	[number]	k4	1404
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
významu	význam	k1gInSc2	význam
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
biskup	biskup	k1gMnSc1	biskup
z	z	k7c2	z
Winchesteru	Winchester	k1gInSc2	Winchester
byl	být	k5eAaImAgInS	být
odpovědný	odpovědný	k2eAgInSc1d1	odpovědný
za	za	k7c4	za
současný	současný	k2eAgInSc4d1	současný
vzhled	vzhled	k1gInSc4	vzhled
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
Winchester	Winchester	k1gMnSc1	Winchester
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
New	New	k1gFnSc1	New
College	Colleg	k1gInSc2	Colleg
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
důležitým	důležitý	k2eAgInSc7d1	důležitý
centrem	centr	k1gInSc7	centr
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
vlnou	vlna	k1gFnSc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgMnPc4	tři
státem	stát	k1gInSc7	stát
spravované	spravovaný	k2eAgFnSc2d1	spravovaná
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Kings	Kings	k1gInSc1	Kings
<g/>
'	'	kIx"	'
School	School	k1gInSc1	School
Winchester	Winchester	k1gInSc1	Winchester
<g/>
,	,	kIx,	,
Westgate	Westgat	k1gInSc5	Westgat
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Henry	henry	k1gInPc2	henry
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc7d1	dobrá
pověstí	pověst	k1gFnSc7	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Winchesterská	Winchesterský	k2eAgFnSc1d1	Winchesterská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
King	King	k1gMnSc1	King
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gNnSc7	College
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
univerzitou	univerzita	k1gFnSc7	univerzita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Winchesterská	Winchesterský	k2eAgFnSc1d1	Winchesterská
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Southamptonské	Southamptonský	k2eAgFnSc2d1	Southamptonský
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Winchesterská	Winchesterský	k2eAgFnSc1d1	Winchesterská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
katedrála	katedrála	k1gFnSc1	katedrála
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1079	[number]	k4	1079
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
nádherných	nádherný	k2eAgInPc2d1	nádherný
architektonických	architektonický	k2eAgInPc2d1	architektonický
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
období	období	k1gNnSc2	období
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
mnoha	mnoho	k4c2	mnoho
biskupů	biskup	k1gInPc2	biskup
z	z	k7c2	z
Winchesteru	Winchester	k1gInSc2	Winchester
<g/>
,	,	kIx,	,
anglosaských	anglosaský	k2eAgMnPc2d1	anglosaský
panovníků	panovník	k1gMnPc2	panovník
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Canute	Canut	k1gInSc5	Canut
nebo	nebo	k8xC	nebo
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
Jane	Jan	k1gMnSc5	Jan
Austenové	Austenové	k2eAgMnSc2d1	Austenové
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
byla	být	k5eAaImAgFnS	být
i	i	k9	i
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
k	k	k7c3	k
hrobce	hrobka	k1gFnSc3	hrobka
svatého	svatý	k1gMnSc2	svatý
Swithuna	Swithuna	k1gFnSc1	Swithuna
<g/>
.	.	kIx.	.
</s>
<s>
Nádvoří	nádvoří	k1gNnSc1	nádvoří
katedrály	katedrála	k1gFnSc2	katedrála
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
historických	historický	k2eAgFnPc2d1	historická
staveb	stavba	k1gFnPc2	stavba
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
katedrála	katedrála	k1gFnSc1	katedrála
převorstvím	převorství	k1gNnSc7	převorství
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
budova	budova	k1gFnSc1	budova
děkanství	děkanství	k1gNnSc1	děkanství
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc4	sídlo
převora	převor	k1gMnSc2	převor
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
odsud	odsud	k6eAd1	odsud
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Cheyney	Cheyney	k1gInPc7	Cheyney
Court	Courta	k1gFnPc2	Courta
<g/>
,	,	kIx,	,
hrázděný	hrázděný	k2eAgInSc1d1	hrázděný
dům	dům	k1gInSc1	dům
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgNnSc1d1	bývalé
sídlo	sídlo	k1gNnSc1	sídlo
biskupových	biskupův	k2eAgMnPc2d1	biskupův
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
trámovým	trámový	k2eAgInSc7d1	trámový
stropem	strop	k1gInSc7	strop
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
postavená	postavený	k2eAgFnSc1d1	postavená
radou	rada	k1gFnSc7	rada
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
nádvoří	nádvoří	k1gNnSc1	nádvoří
katedrály	katedrála	k1gFnSc2	katedrála
vedle	vedle	k7c2	vedle
děkanské	děkanský	k2eAgFnSc2d1	Děkanská
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
normanském	normanský	k2eAgNnSc6d1	normanské
období	období	k1gNnSc6	období
biskupským	biskupský	k2eAgInSc7d1	biskupský
palácem	palác	k1gInSc7	palác
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1110	[number]	k4	1110
a	a	k8xC	a
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
předchozí	předchozí	k2eAgFnSc2d1	předchozí
saské	saský	k2eAgFnSc2d1	saská
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
a	a	k8xC	a
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
Jindřichem	Jindřich	k1gMnSc7	Jindřich
z	z	k7c2	z
Bloa	Blo	k1gInSc2	Blo
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
a	a	k8xC	a
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
trávili	trávit	k5eAaImAgMnP	trávit
čas	čas	k1gInSc4	čas
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
dochovala	dochovat	k5eAaPmAgFnS	dochovat
jen	jen	k9	jen
ruina	ruina	k1gFnSc1	ruina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
velkou	velký	k2eAgFnSc7d1	velká
síní	síň	k1gFnSc7	síň
Winchesterského	Winchesterský	k2eAgInSc2d1	Winchesterský
hradu	hrad	k1gInSc2	hrad
postaveného	postavený	k2eAgInSc2d1	postavený
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Síň	síň	k1gFnSc1	síň
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1222	[number]	k4	1222
až	až	k6eAd1	až
1235	[number]	k4	1235
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pozoruhodná	pozoruhodný	k2eAgNnPc4d1	pozoruhodné
kulatým	kulatý	k2eAgInSc7d1	kulatý
stolem	stol	k1gInSc7	stol
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěn	umístit	k5eAaPmNgInS	umístit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1463	[number]	k4	1463
<g/>
.	.	kIx.	.
</s>
<s>
Stůl	stůl	k1gInSc1	stůl
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
má	mít	k5eAaImIp3nS	mít
značnou	značný	k2eAgFnSc4d1	značná
historickou	historický	k2eAgFnSc4d1	historická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgMnS	být
stůl	stůl	k1gInSc4	stůl
bez	bez	k7c2	bez
kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
pro	pro	k7c4	pro
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1522	[number]	k4	1522
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
rytířů	rytíř	k1gMnPc2	rytíř
kulatého	kulatý	k2eAgInSc2d1	kulatý
stolu	stol	k1gInSc2	stol
i	i	k8xC	i
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
desky	deska	k1gFnSc2	deska
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
Winchesterské	Winchesterský	k2eAgFnSc2d1	Winchesterská
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgFnSc2d1	veřejná
školy	škola	k1gFnSc2	škola
založené	založený	k2eAgFnSc2d1	založená
Vilémem	Vilém	k1gMnSc7	Vilém
Wykenhamem	Wykenham	k1gInSc7	Wykenham
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1382	[number]	k4	1382
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dvě	dva	k4xCgNnPc4	dva
nádvoří	nádvoří	k1gNnPc4	nádvoří
<g/>
,	,	kIx,	,
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
aulu	aula	k1gFnSc4	aula
a	a	k8xC	a
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
chlapců	chlapec	k1gMnPc2	chlapec
z	z	k7c2	z
chudých	chudý	k2eAgFnPc2d1	chudá
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Chudobinec	chudobinec	k1gInSc1	chudobinec
a	a	k8xC	a
obrovská	obrovský	k2eAgFnSc1d1	obrovská
normanská	normanský	k2eAgFnSc1d1	normanská
kaple	kaple	k1gFnSc1	kaple
Nemocnice	nemocnice	k1gFnSc2	nemocnice
svatého	svatý	k2eAgInSc2d1	svatý
kříže	kříž	k1gInSc2	kříž
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Jindřichem	Jindřich	k1gMnSc7	Jindřich
z	z	k7c2	z
Bloa	Blo	k1gInSc2	Blo
roku	rok	k1gInSc2	rok
1130	[number]	k4	1130
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
odpočinku	odpočinek	k1gInSc2	odpočinek
poutníků	poutník	k1gMnPc2	poutník
do	do	k7c2	do
Canterbury	Canterbura	k1gFnSc2	Canterbura
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Winchester	Winchester	k1gInSc4	Winchester
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Průvodce	průvodce	k1gMnSc1	průvodce
Winchesterem	Winchester	k1gInSc7	Winchester
Virtuální	virtuální	k2eAgFnSc3d1	virtuální
průvodce	průvodka	k1gFnSc3	průvodka
Winchesterskou	Winchesterský	k2eAgFnSc7d1	Winchesterská
katedrálou	katedrála	k1gFnSc7	katedrála
Winchesterská	Winchesterský	k2eAgFnSc1d1	Winchesterská
univerzita	univerzita	k1gFnSc1	univerzita
Winchesterská	Winchesterský	k2eAgFnSc1d1	Winchesterská
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
</s>
