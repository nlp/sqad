<s>
Moscovium	Moscovium	k1gNnSc1
</s>
<s>
Moscovium	Moscovium	k1gNnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
3	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
bismutu	bismut	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
288	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mc	Mc	kA
</s>
<s>
115	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Moscovium	Moscovium	k1gNnSc1
<g/>
,	,	kIx,
Mc	Mc	k1gFnSc1
<g/>
,	,	kIx,
115	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
angl.	angl.	k?
Moscovium	Moscovium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
p	p	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54085-64-2	54085-64-2	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
288,19	288,19	k4
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
3	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
bismutu	bismut	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgFnSc3d1
pevné	pevný	k2eAgFnSc3d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
287	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
α	α	k?
</s>
<s>
283	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
288	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
α	α	k?
</s>
<s>
284	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
289	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
0,22	0,22	k4
s	s	k7c7
</s>
<s>
α	α	k?
</s>
<s>
285	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
290	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
16	#num#	k4
ms	ms	k?
</s>
<s>
α	α	k?
</s>
<s>
286	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bi	Bi	k?
<g/>
⋏	⋏	k?
</s>
<s>
Flerovium	Flerovium	k1gNnSc1
≺	≺	k?
<g/>
Mc	Mc	k1gMnSc2
<g/>
≻	≻	k?
Livermorium	Livermorium	k1gNnSc1
</s>
<s>
Moscovium	Moscovium	k1gNnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
název	název	k1gInSc1
moskovium	moskovium	k1gNnSc1
nevychází	vycházet	k5eNaImIp3nS
z	z	k7c2
odborných	odborný	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ze	z	k7c2
zprávy	zpráva	k1gFnSc2
ČTK	ČTK	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nelze	lze	k6eNd1
ho	on	k3xPp3gInSc4
zatím	zatím	k6eAd1
brát	brát	k5eAaImF
jako	jako	k8xC,k8xS
konečný	konečný	k2eAgInSc1d1
<g/>
;	;	kIx,
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Mc	Mc	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc1
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
publikoval	publikovat	k5eAaBmAgInS
tým	tým	k1gInSc4
ruských	ruský	k2eAgMnPc2d1
a	a	k8xC
amerických	americký	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
zprávu	zpráva	k1gFnSc4
o	o	k7c6
přípravě	příprava	k1gFnSc6
ununpentia	ununpentium	k1gNnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
moscovia	moscovius	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
ununtria	ununtrium	k1gNnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
nihonia	nihonium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
28	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
tyto	tento	k3xDgInPc4
výsledky	výsledek	k1gInPc4
potvrdili	potvrdit	k5eAaPmAgMnP
japonští	japonský	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
4820	#num#	k4
Ca	ca	kA
+	+	kIx~
24395	#num#	k4
Am	Am	k1gFnSc2
→	→	k?
291115	#num#	k4
Mc	Mc	k1gFnSc1
→	→	k?
288115	#num#	k4
Mc	Mc	k1gFnPc2
+	+	kIx~
3	#num#	k4
10	#num#	k4
n	n	k0
→	→	k?
284113	#num#	k4
Nh	Nh	k1gFnSc1
+	+	kIx~
α	α	k?
</s>
<s>
4820	#num#	k4
Ca	ca	kA
+	+	kIx~
24395	#num#	k4
Am	Am	k1gFnSc2
→	→	k?
291115	#num#	k4
Mc	Mc	k1gFnSc1
→	→	k?
287115	#num#	k4
Mc	Mc	k1gFnPc2
+	+	kIx~
4	#num#	k4
10	#num#	k4
n	n	k0
→	→	k?
283113	#num#	k4
Nh	Nh	k1gFnSc1
+	+	kIx~
α	α	k?
</s>
<s>
Atomy	atom	k1gInPc1
americia	americium	k1gNnSc2
byly	být	k5eAaImAgInP
bombardovány	bombardován	k2eAgInPc1d1
atomy	atom	k1gInPc1
vápníku	vápník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Produktem	produkt	k1gInSc7
byly	být	k5eAaImAgInP
čtyři	čtyři	k4xCgInPc1
atomy	atom	k1gInPc1
ununpentia	ununpentium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
atomy	atom	k1gInPc4
se	se	k3xPyFc4
během	během	k7c2
sekundy	sekunda	k1gFnSc2
rozpadly	rozpadnout	k5eAaPmAgInP
na	na	k7c4
ununtrium	ununtrium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklé	vzniklý	k2eAgNnSc1d1
ununtrium	ununtrium	k1gNnSc1
existovalo	existovat	k5eAaImAgNnS
po	po	k7c4
dobu	doba	k1gFnSc4
1,2	1,2	k4
sekundy	sekunda	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
rozpad	rozpad	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
existence	existence	k1gFnSc1
ununpentia	ununpentia	k1gFnSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
moscovia	moscovia	k1gFnSc1
<g/>
)	)	kIx)
prokázána	prokázán	k2eAgFnSc1d1
spektroskopicky	spektroskopicky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
pro	pro	k7c4
čistou	čistý	k2eAgFnSc4d1
a	a	k8xC
užitou	užitý	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
potvrdila	potvrdit	k5eAaPmAgFnS
splnění	splnění	k1gNnSc4
kritérií	kritérion	k1gNnPc2
pro	pro	k7c4
prokázání	prokázání	k1gNnSc4
objevu	objev	k1gInSc2
nového	nový	k2eAgInSc2d1
prvku	prvek	k1gInSc2
<g/>
,	,	kIx,
Uup	Uup	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
za	za	k7c4
objevené	objevený	k2eAgInPc4d1
spolupracujícími	spolupracující	k2eAgInPc7d1
týmy	tým	k1gInPc7
vědců	vědec	k1gMnPc2
ze	z	k7c2
Spojeného	spojený	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
jaderných	jaderný	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
v	v	k7c6
Dubně	Dubna	k1gFnSc6
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lawrence	Lawrence	k1gFnSc1
Livermore	Livermor	k1gInSc5
National	National	k1gFnSc1
Laboratory	Laborator	k1gInPc1
(	(	kIx(
<g/>
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
a	a	k8xC
Národní	národní	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
Oak	Oak	k1gMnSc2
Ridge	Ridg	k1gMnSc2
(	(	kIx(
<g/>
Tennessee	Tennesse	k1gMnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
a	a	k8xC
vyzvala	vyzvat	k5eAaPmAgFnS
objevitele	objevitel	k1gMnSc4
k	k	k7c3
navržení	navržení	k1gNnSc3
konečného	konečný	k2eAgInSc2d1
názvu	název	k1gInSc2
a	a	k8xC
značky	značka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Konečným	konečný	k2eAgInSc7d1
návrhem	návrh	k1gInSc7
objevitelů	objevitel	k1gMnPc2
byl	být	k5eAaImAgInS
název	název	k1gInSc1
moscovium	moscovium	k1gNnSc1
a	a	k8xC
značka	značka	k1gFnSc1
Mc	Mc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
je	být	k5eAaImIp3nS
takto	takto	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgMnS
na	na	k7c4
počest	počest	k1gFnSc4
Moskvy	Moskva	k1gFnSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
země	zem	k1gFnSc2
objevitelů	objevitel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
konečného	konečný	k2eAgNnSc2d1
pojmenování	pojmenování	k1gNnSc2
předložila	předložit	k5eAaPmAgFnS
IUPAC	IUPAC	kA
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
k	k	k7c3
veřejné	veřejný	k2eAgFnSc3d1
diskusi	diskuse	k1gFnSc3
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
schválila	schválit	k5eAaPmAgFnS
jako	jako	k9
konečné	konečný	k2eAgNnSc4d1
pojmenování	pojmenování	k1gNnSc4
a	a	k8xC
značku	značka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Dosud	dosud	k6eAd1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
následující	následující	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
moscovia	moscovium	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
IzotopRok	IzotopRok	k1gInSc1
objevuReakcePoločas	objevuReakcePoločasa	k1gFnPc2
přeměny	přeměna	k1gFnSc2
</s>
<s>
287	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
32	#num#	k4
ms	ms	k?
</s>
<s>
288	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
87	#num#	k4
ms	ms	k?
</s>
<s>
289	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
<g/>
0,22	0,22	k4
s	s	k7c7
</s>
<s>
290	#num#	k4
<g/>
Mc	Mc	k1gFnSc1
<g/>
16	#num#	k4
ms	ms	k?
</s>
<s>
291	#num#	k4
<g/>
Mc	Mc	k1gFnPc2
?	?	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
chemické	chemický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
obohatí	obohatit	k5eAaPmIp3nP
tabulku	tabulka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Oganessian	Oganessian	k1gMnSc1
<g/>
,	,	kIx,
Yu	Yu	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ts	ts	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experiments	Experiments	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
synthesis	synthesis	k1gInSc1
of	of	k?
element	element	k1gInSc1
115	#num#	k4
in	in	k?
the	the	k?
reaction	reaction	k1gInSc4
243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
<g/>
xn	xn	k?
<g/>
)	)	kIx)
<g/>
291	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
x	x	k?
<g/>
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
C.	C.	kA
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
69	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
0	#num#	k4
<g/>
21601	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevC	PhysRevC	k1gFnSc2
<g/>
.69	.69	k4
<g/>
.021601	.021601	k4
<g/>
.	.	kIx.
↑	↑	k?
Oganessian	Oganessian	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
“	“	k?
<g/>
Experiments	Experiments	k1gInSc1
on	on	k3xPp3gInSc1
the	the	k?
synthesis	synthesis	k1gInSc1
of	of	k?
element	element	k1gInSc1
115	#num#	k4
in	in	k?
the	the	k?
reaction	reaction	k1gInSc4
243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
<g/>
xn	xn	k?
<g/>
)	)	kIx)
<g/>
291	#num#	k4
<g/>
−	−	k?
<g/>
x	x	k?
<g/>
115	#num#	k4
<g/>
”	”	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JINR	JINR	kA
preprints	preprintsa	k1gFnPc2
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RUDOLPH	RUDOLPH	kA
<g/>
,	,	kIx,
D.	D.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spectroscopy	Spectroscopa	k1gFnSc2
of	of	k?
element	element	k1gInSc1
115	#num#	k4
decay	decaa	k1gFnSc2
chains	chainsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
Letters	Lettersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akceptovaný	akceptovaný	k2eAgInSc1d1
článek	článek	k1gInSc1
před	před	k7c7
vydáním	vydání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1079	#num#	k4
<g/>
-	-	kIx~
<g/>
7114	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Existence	existence	k1gFnSc1
of	of	k?
new	new	k?
element	element	k1gInSc1
confirmed	confirmed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PhysOrg	PhysOrg	k1gMnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Discovery	Discovera	k1gFnSc2
and	and	k?
Assignment	Assignment	k1gInSc1
of	of	k?
Elements	Elements	k1gInSc1
with	with	k1gMnSc1
Atomic	Atomic	k1gMnSc1
Numbers	Numbers	k1gInSc4
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUPAC	IUPAC	kA
Latest	Latest	k1gInSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
se	se	k3xPyFc4
rozrostla	rozrůst	k5eAaPmAgFnS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
nové	nový	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
IUPAC	IUPAC	kA
News	Newsa	k1gFnPc2
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
is	is	k?
naming	naming	k1gInSc1
the	the	k?
four	four	k1gInSc1
new	new	k?
elements	elements	k6eAd1
nihonium	nihonium	k1gNnSc1
<g/>
,	,	kIx,
moscovium	moscovium	k1gNnSc1
<g/>
,	,	kIx,
tennessine	tennessinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
and	and	k?
oganesson	oganesson	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IUPAC	IUPAC	kA
News	News	k1gInSc1
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
Announces	Announces	k1gMnSc1
the	the	k?
Names	Names	k1gMnSc1
of	of	k?
the	the	k?
Elements	Elements	k1gInSc1
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
<g/>
,	,	kIx,
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Pniktogeny	Pniktogen	k1gInPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
moskovium	moskovium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
moscovium	moscovium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnPc1
moscovium	moscovium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2012003555	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2012003555	#num#	k4
</s>
