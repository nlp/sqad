<p>
<s>
Gin	gin	k1gInSc1	gin
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
džin	džin	k1gInSc1	džin
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
získávaný	získávaný	k2eAgInSc4d1	získávaný
destilací	destilace	k1gFnSc7	destilace
obilné	obilný	k2eAgFnSc2d1	obilná
břečky	břečka	k1gFnSc2	břečka
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
různých	různý	k2eAgFnPc2d1	různá
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Popíjí	popíjet	k5eAaImIp3nS	popíjet
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
také	také	k9	také
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
koktejlů	koktejl	k1gInPc2	koktejl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jenever	Jenever	k1gInSc1	Jenever
–	–	k?	–
gin	gin	k1gInSc1	gin
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
periodickou	periodický	k2eAgFnSc7d1	periodická
destilací	destilace	k1gFnSc7	destilace
z	z	k7c2	z
jalovce	jalovec	k1gInSc2	jalovec
a	a	k8xC	a
obilného	obilný	k2eAgInSc2d1	obilný
destilátu	destilát	k1gInSc2	destilát
</s>
</p>
<p>
<s>
Dry	Dry	k?	Dry
gin	gin	k1gInSc1	gin
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
g.	g.	k?	g.
<g/>
)	)	kIx)	)
–	–	k?	–
gin	gin	k1gInSc1	gin
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
kontinuální	kontinuální	k2eAgFnSc7d1	kontinuální
destilací	destilace	k1gFnSc7	destilace
z	z	k7c2	z
obilného	obilný	k2eAgInSc2d1	obilný
destilátu	destilát	k1gInSc2	destilát
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Gin	gin	k1gInSc1	gin
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
objeven	objevit	k5eAaPmNgInS	objevit
r.	r.	kA	r.
1650	[number]	k4	1650
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
jej	on	k3xPp3gNnSc4	on
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Fanciscus	Fanciscus	k1gMnSc1	Fanciscus
de	de	k?	de
La	la	k1gNnSc2	la
Boie	Boie	k1gFnPc2	Boie
známý	známý	k1gMnSc1	známý
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sylvius	Sylvius	k1gMnSc1	Sylvius
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
medicíny	medicína	k1gFnSc2	medicína
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
destilátů	destilát	k1gInPc2	destilát
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc1	použití
zamýšleno	zamýšlet	k5eAaImNgNnS	zamýšlet
jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sylvius	Sylvius	k1gMnSc1	Sylvius
chtěl	chtít	k5eAaImAgMnS	chtít
nalézt	nalézt	k5eAaPmF	nalézt
levné	levný	k2eAgNnSc4d1	levné
diuretikum	diuretikum	k1gNnSc4	diuretikum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nP	by
použil	použít	k5eAaPmAgInS	použít
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
na	na	k7c4	na
ledvinové	ledvinový	k2eAgFnPc4d1	ledvinová
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Smíchal	smíchat	k5eAaPmAgMnS	smíchat
tedy	tedy	k9	tedy
dvě	dva	k4xCgFnPc1	dva
močopudné	močopudný	k2eAgFnPc1d1	močopudná
suroviny	surovina	k1gFnPc1	surovina
<g/>
:	:	kIx,	:
olej	olej	k1gInSc1	olej
z	z	k7c2	z
jalovcových	jalovcový	k2eAgFnPc2d1	Jalovcová
bobulí	bobule	k1gFnPc2	bobule
a	a	k8xC	a
obilný	obilný	k2eAgInSc4d1	obilný
destilát	destilát	k1gInSc4	destilát
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
směs	směs	k1gFnSc4	směs
nazval	nazvat	k5eAaPmAgMnS	nazvat
genever	genever	k1gMnSc1	genever
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
genévrier	genévrier	k1gInSc1	genévrier
[	[	kIx(	[
<g/>
žə	žə	k?	žə
<g/>
]	]	kIx)	]
=	=	kIx~	=
jalovec	jalovec	k1gInSc1	jalovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
vojáci	voják	k1gMnPc1	voják
nazvali	nazvat	k5eAaBmAgMnP	nazvat
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
"	"	kIx"	"
<g/>
holandská	holandský	k2eAgFnSc1d1	holandská
kuráž	kuráž	k?	kuráž
<g/>
"	"	kIx"	"
a	a	k8xC	a
přivezli	přivézt	k5eAaPmAgMnP	přivézt
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
natolik	natolik	k6eAd1	natolik
zachutnal	zachutnat	k5eAaPmAgMnS	zachutnat
<g/>
,	,	kIx,	,
že	že	k8xS	že
býval	bývat	k5eAaImAgInS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
národní	národní	k2eAgInSc4d1	národní
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
ginu	gin	k1gInSc2	gin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
londýnští	londýnský	k2eAgMnPc1d1	londýnský
lihovarníci	lihovarník	k1gMnPc1	lihovarník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dry	dry	k?	dry
gin	gin	k1gInSc1	gin
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
s	s	k7c7	s
přídomkem	přídomek	k1gInSc7	přídomek
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
se	se	k3xPyFc4	se
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
holandského	holandský	k2eAgInSc2d1	holandský
liší	lišit	k5eAaImIp3nS	lišit
nejen	nejen	k6eAd1	nejen
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
r.	r.	kA	r.
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
destilací	destilace	k1gFnSc7	destilace
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
všemožných	všemožný	k2eAgFnPc2d1	všemožná
bylin	bylina	k1gFnPc2	bylina
jako	jako	k8xS	jako
např.	např.	kA	např.
kardamom	kardamom	k1gInSc1	kardamom
<g/>
,	,	kIx,	,
pomerančová	pomerančový	k2eAgFnSc1d1	pomerančová
kůra	kůra	k1gFnSc1	kůra
nebo	nebo	k8xC	nebo
jalovčinky	jalovčinka	k1gFnPc1	jalovčinka
a	a	k8xC	a
spousta	spousta	k1gFnSc1	spousta
dalších	další	k2eAgFnPc2d1	další
přísad	přísada	k1gFnPc2	přísada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vlastním	vlastní	k2eAgInSc7d1	vlastní
tajným	tajný	k2eAgInSc7d1	tajný
receptem	recept	k1gInSc7	recept
každé	každý	k3xTgFnSc2	každý
palírny	palírna	k1gFnSc2	palírna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gin	gin	k1gInSc1	gin
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
koktejlů	koktejl	k1gInPc2	koktejl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
gimlet	gimlet	k1gInSc1	gimlet
<g/>
,	,	kIx,	,
gin-fizz	ginizz	k1gInSc1	gin-fizz
<g/>
,	,	kIx,	,
Martini	martini	k1gNnSc1	martini
nebo	nebo	k8xC	nebo
gin	gin	k1gInSc1	gin
s	s	k7c7	s
tonikem	tonik	k1gInSc7	tonik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nápoj	nápoj	k1gInSc1	nápoj
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Hasseltu	Hasselt	k1gInSc6	Hasselt
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
výrobě	výroba	k1gFnSc3	výroba
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
destiluje	destilovat	k5eAaImIp3nS	destilovat
fermentovaný	fermentovaný	k2eAgInSc1d1	fermentovaný
obilný	obilný	k2eAgInSc1d1	obilný
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
ječný	ječný	k2eAgInSc1d1	ječný
<g/>
)	)	kIx)	)
slad	slad	k1gInSc1	slad
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
destilát	destilát	k1gInSc1	destilát
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
destiluje	destilovat	k5eAaImIp3nS	destilovat
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
se	s	k7c7	s
směsí	směs	k1gFnSc7	směs
oleje	olej	k1gInSc2	olej
z	z	k7c2	z
jalovce	jalovec	k1gInSc2	jalovec
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
gin	gin	k1gInSc1	gin
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc7d1	nízká
koncentrací	koncentrace	k1gFnSc7	koncentrace
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
destiluje	destilovat	k5eAaImIp3nS	destilovat
s	s	k7c7	s
olejem	olej	k1gInSc7	olej
z	z	k7c2	z
jalovce	jalovec	k1gInSc2	jalovec
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
bylinami	bylina	k1gFnPc7	bylina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
dvakrát	dvakrát	k6eAd1	dvakrát
destilovaný	destilovaný	k2eAgInSc1d1	destilovaný
gin	gin	k1gInSc1	gin
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
double	double	k2eAgInSc4d1	double
gin	gin	k1gInSc4	gin
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zraje	zrát	k5eAaImIp3nS	zrát
v	v	k7c6	v
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
plnější	plný	k2eAgFnSc2d2	plnější
chuti	chuť	k1gFnSc2	chuť
ginu	gin	k1gInSc2	gin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dry	dry	k?	dry
gin	gin	k1gInSc1	gin
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
destilát	destilát	k1gInSc1	destilát
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
(	(	kIx(	(
<g/>
u	u	k7c2	u
levnějších	levný	k2eAgInPc2d2	levnější
ginů	gin	k1gInPc2	gin
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
suroviny	surovina	k1gFnPc1	surovina
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
nebo	nebo	k8xC	nebo
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
destilát	destilát	k1gInSc1	destilát
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
destilován	destilován	k2eAgMnSc1d1	destilován
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
destilovanou	destilovaný	k2eAgFnSc7d1	destilovaná
kapalinou	kapalina	k1gFnSc7	kapalina
je	být	k5eAaImIp3nS	být
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
prodyšný	prodyšný	k2eAgInSc1d1	prodyšný
vak	vak	k1gInSc1	vak
<g/>
,	,	kIx,	,
naplněný	naplněný	k2eAgInSc1d1	naplněný
jalovcem	jalovec	k1gInSc7	jalovec
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
bylinkami	bylinka	k1gFnPc7	bylinka
a	a	k8xC	a
kořením	koření	k1gNnSc7	koření
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgInPc1d1	Horké
páry	pár	k1gInPc1	pár
destilátu	destilát	k1gInSc2	destilát
projdou	projít	k5eAaPmIp3nP	projít
touto	tento	k3xDgFnSc7	tento
směsí	směs	k1gFnSc7	směs
<g/>
,	,	kIx,	,
kondenzují	kondenzovat	k5eAaImIp3nP	kondenzovat
navrchu	navrchu	k7c2	navrchu
kotle	kotel	k1gInSc2	kotel
a	a	k8xC	a
následně	následně	k6eAd1	následně
odkapávají	odkapávat	k5eAaImIp3nP	odkapávat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniká	vznikat	k5eAaImIp3nS	vznikat
suchý	suchý	k2eAgInSc4d1	suchý
gin	gin	k1gInSc4	gin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgInSc1d2	lehčí
než	než	k8xS	než
tradiční	tradiční	k2eAgInSc1d1	tradiční
genever	genever	k1gInSc1	genever
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nezraje	zrát	k5eNaImIp3nS	zrát
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známé	známý	k2eAgFnPc1d1	známá
značky	značka	k1gFnPc1	značka
==	==	k?	==
</s>
</p>
<p>
<s>
Beefeater	Beefeater	k1gInSc1	Beefeater
Gin	gin	k1gInSc1	gin
</s>
</p>
<p>
<s>
Tanqueray	Tanqueraa	k1gFnPc1	Tanqueraa
</s>
</p>
<p>
<s>
Bombay	Bombaa	k1gFnPc1	Bombaa
Sapphire	Sapphir	k1gInSc5	Sapphir
</s>
</p>
<p>
<s>
Hendrick	Hendrick	k1gInSc1	Hendrick
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Monkey	Monkey	k1gInPc1	Monkey
47	[number]	k4	47
</s>
</p>
<p>
<s>
Bloom	Bloom	k1gInSc1	Bloom
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Chase	chasa	k1gFnSc3	chasa
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Miller	Miller	k1gMnSc1	Miller
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
koktejlů	koktejl	k1gInPc2	koktejl
ISBN	ISBN	kA	ISBN
80-7234-217-7	[number]	k4	80-7234-217-7
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gin	gin	k1gInSc1	gin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gin	gin	k1gInSc1	gin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Gin	gin	k1gInSc1	gin
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
