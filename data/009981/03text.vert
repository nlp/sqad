<p>
<s>
Bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
(	(	kIx(	(
<g/>
Bombyx	Bombyx	k1gInSc1	Bombyx
mori	mor	k1gFnSc2	mor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
noční	noční	k2eAgMnSc1d1	noční
motýl	motýl	k1gMnSc1	motýl
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Bombycidae	Bombycida	k1gInSc2	Bombycida
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgMnSc1d1	důležitý
jako	jako	k8xS	jako
producent	producent	k1gMnSc1	producent
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
reprodukce	reprodukce	k1gFnSc2	reprodukce
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc4	pěstování
hedvábí	hedvábí	k1gNnSc2	hedvábí
praktikováno	praktikován	k2eAgNnSc4d1	praktikováno
už	už	k9	už
nejméně	málo	k6eAd3	málo
5000	[number]	k4	5000
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Goldsmith	Goldsmith	k1gInSc1	Goldsmith
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
housenek	housenka	k1gFnPc2	housenka
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgInSc2d1	morušový
sestává	sestávat	k5eAaImIp3nS	sestávat
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
listů	list	k1gInPc2	list
moruše	moruše	k1gFnSc2	moruše
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
volně	volně	k6eAd1	volně
žijícím	žijící	k2eAgMnSc7d1	žijící
příbuzným	příbuzný	k1gMnSc7	příbuzný
je	být	k5eAaImIp3nS	být
Bombyx	Bombyx	k1gInSc4	Bombyx
mandarina	mandarino	k1gNnSc2	mandarino
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
hybridní	hybridní	k2eAgFnSc2d1	hybridní
formy	forma	k1gFnSc2	forma
(	(	kIx(	(
<g/>
Goldsmith	Goldsmith	k1gInSc1	Goldsmith
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
Koreu	Korea	k1gFnSc4	Korea
až	až	k9	až
po	po	k7c4	po
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Housenka	housenka	k1gFnSc1	housenka
se	se	k3xPyFc4	se
líhne	líhnout	k5eAaImIp3nS	líhnout
z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
po	po	k7c6	po
deseti	deset	k4xCc2	deset
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Housenky	housenka	k1gFnPc1	housenka
bource	bourec	k1gMnSc2	bourec
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
žravé	žravý	k2eAgFnPc1d1	žravá
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
housenky	housenka	k1gFnPc1	housenka
všech	všecek	k3xTgMnPc2	všecek
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
požírají	požírat	k5eAaImIp3nP	požírat
listy	list	k1gInPc1	list
moruše	moruše	k1gFnSc2	moruše
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
hlava	hlava	k1gFnSc1	hlava
housenky	housenka	k1gFnSc2	housenka
zbarví	zbarvit	k5eAaPmIp3nS	zbarvit
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
svlékat	svlékat	k5eAaImF	svlékat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
svléknou	svléknout	k5eAaPmIp3nP	svléknout
po	po	k7c6	po
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
zbarví	zbarvit	k5eAaPmIp3nS	zbarvit
lehce	lehko	k6eAd1	lehko
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
a	a	k8xC	a
pokožka	pokožka	k1gFnSc1	pokožka
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
tužší	tuhý	k2eAgFnSc1d2	tužší
<g/>
.	.	kIx.	.
</s>
<s>
Larva	larva	k1gFnSc1	larva
se	se	k3xPyFc4	se
zakukluje	zakuklovat	k5eAaImIp3nS	zakuklovat
v	v	k7c6	v
kokonu	kokon	k1gInSc6	kokon
ze	z	k7c2	z
surového	surový	k2eAgNnSc2d1	surové
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
slinné	slinný	k2eAgFnPc1d1	slinná
žlázy	žláza	k1gFnPc1	žláza
housenky	housenka	k1gFnPc1	housenka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
ochranu	ochrana	k1gFnSc4	ochrana
téměř	téměř	k6eAd1	téměř
nehybné	hybný	k2eNgFnSc2d1	nehybná
kukly	kukla	k1gFnSc2	kukla
před	před	k7c7	před
zraněním	zranění	k1gNnSc7	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
nočních	noční	k2eAgMnPc2d1	noční
motýlů	motýl	k1gMnPc2	motýl
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kokony	kokon	k1gInPc1	kokon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenom	jenom	k9	jenom
větší	veliký	k2eAgInPc1d2	veliký
druhy	druh	k1gInPc1	druh
čeledí	čeleď	k1gFnPc2	čeleď
Bombycidae	Bombycida	k1gInSc2	Bombycida
a	a	k8xC	a
Saturniidae	Saturniida	k1gInPc1	Saturniida
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hedvábí	hedvábí	k1gNnSc2	hedvábí
==	==	k?	==
</s>
</p>
<p>
<s>
Kokon	kokon	k1gInSc1	kokon
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
jediným	jediný	k2eAgNnSc7d1	jediné
nepřerušeným	přerušený	k2eNgNnSc7d1	nepřerušené
hedvábným	hedvábný	k2eAgNnSc7d1	hedvábné
vláknem	vlákno	k1gNnSc7	vlákno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
300	[number]	k4	300
do	do	k7c2	do
900	[number]	k4	900
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jemná	jemný	k2eAgFnSc1d1	jemná
a	a	k8xC	a
lesklá	lesklý	k2eAgFnSc1d1	lesklá
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
kolem	kolem	k7c2	kolem
10	[number]	k4	10
mikrometrů	mikrometr	k1gInPc2	mikrometr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
1	[number]	k4	1
kilogramu	kilogram	k1gInSc6	kilogram
hedvábí	hedvábí	k1gNnSc2	hedvábí
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
asi	asi	k9	asi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
tisíce	tisíc	k4xCgInPc4	tisíc
kusů	kus	k1gInPc2	kus
kokonů	kokon	k1gInPc2	kokon
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dostaneme	dostat	k5eAaPmIp1nP	dostat
po	po	k7c6	po
rozpletení	rozpletení	k1gNnSc6	rozpletení
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kokonu	kokon	k1gInSc2	kokon
necelý	celý	k2eNgInSc4d1	necelý
kilometr	kilometr	k1gInSc4	kilometr
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
deset	deset	k4xCc4	deset
kokonů	kokon	k1gInPc2	kokon
by	by	kYmCp3nS	by
stačilo	stačit	k5eAaBmAgNnS	stačit
na	na	k7c4	na
dosažení	dosažení	k1gNnSc4	dosažení
vrcholu	vrchol	k1gInSc2	vrchol
Mount	Mounto	k1gNnPc2	Mounto
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
produkováno	produkovat	k5eAaImNgNnS	produkovat
kolem	kolem	k7c2	kolem
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
surového	surový	k2eAgNnSc2d1	surové
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gFnSc3	jehož
produkci	produkce	k1gFnSc3	produkce
housenky	housenka	k1gFnSc2	housenka
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
listů	list	k1gInPc2	list
moruše	moruše	k1gFnSc2	moruše
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
od	od	k7c2	od
E.	E.	kA	E.
L.	L.	kA	L.
Palmera	Palmera	k1gFnSc1	Palmera
(	(	kIx(	(
<g/>
Fieldbook	Fieldbook	k1gInSc1	Fieldbook
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
kilogram	kilogram	k1gInSc4	kilogram
hedvábí	hedvábí	k1gNnSc2	hedvábí
představuje	představovat	k5eAaImIp3nS	představovat
asi	asi	k9	asi
3	[number]	k4	3
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
hedvábí	hedvábí	k1gNnSc2	hedvábí
představuje	představovat	k5eAaImIp3nS	představovat
112	[number]	k4	112
miliard	miliarda	k4xCgFnPc2	miliarda
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
300	[number]	k4	300
<g/>
násobná	násobný	k2eAgFnSc1d1	násobná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
po	po	k7c6	po
zapředení	zapředení	k1gNnSc6	zapředení
bourec	bourec	k1gMnSc1	bourec
nechá	nechat	k5eAaPmIp3nS	nechat
v	v	k7c6	v
zámotku	zámotek	k1gInSc6	zámotek
přežít	přežít	k5eAaPmF	přežít
<g/>
,	,	kIx,	,
vylíhlé	vylíhlý	k2eAgNnSc1d1	vylíhlé
imago	imago	k1gNnSc1	imago
(	(	kIx(	(
<g/>
dospělý	dospělý	k2eAgMnSc1d1	dospělý
motýl	motýl	k1gMnSc1	motýl
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
prokouše	prokousat	k5eAaPmIp3nS	prokousat
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
ven	ven	k6eAd1	ven
a	a	k8xC	a
vlákno	vlákno	k1gNnSc4	vlákno
tím	ten	k3xDgNnSc7	ten
znehodnotí	znehodnotit	k5eAaPmIp3nS	znehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
kokony	kokon	k1gInPc1	kokon
bource	bourec	k1gMnPc4	bourec
morušového	morušový	k2eAgMnSc4d1	morušový
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
kolem	kolem	k7c2	kolem
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jednak	jednak	k8xC	jednak
zabije	zabít	k5eAaPmIp3nS	zabít
zakuklené	zakuklený	k2eAgFnPc4d1	zakuklená
housenky	housenka	k1gFnPc4	housenka
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zámotky	zámotek	k1gInPc1	zámotek
lépe	dobře	k6eAd2	dobře
rozplétají	rozplétat	k5eAaImIp3nP	rozplétat
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
kukly	kukla	k1gFnPc1	kukla
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
motýl	motýl	k1gMnSc1	motýl
má	mít	k5eAaImIp3nS	mít
rozpětí	rozpětí	k1gNnSc2	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
asi	asi	k9	asi
5	[number]	k4	5
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
ochmýřené	ochmýřený	k2eAgNnSc4d1	ochmýřené
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
neumí	umět	k5eNaImIp3nS	umět
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
větší	veliký	k2eAgFnSc3d2	veliký
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
podobně	podobně	k6eAd1	podobně
zbarveny	zbarven	k2eAgFnPc1d1	zbarvena
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
čeledi	čeleď	k1gFnSc2	čeleď
Bombycidae	Bombycidae	k1gNnSc2	Bombycidae
mají	mít	k5eAaImIp3nP	mít
redukováno	redukován	k2eAgNnSc4d1	redukováno
ústní	ústní	k2eAgNnSc4d1	ústní
ústrojí	ústrojí	k1gNnSc4	ústrojí
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc3	dostupnost
byl	být	k5eAaImAgMnS	být
bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgInSc4d1	morušový
modelem	model	k1gInSc7	model
ke	k	k7c3	k
studiím	studio	k1gNnPc3	studio
Lepidopterologů	lepidopterolog	k1gMnPc2	lepidopterolog
a	a	k8xC	a
biologie	biologie	k1gFnSc1	biologie
členovců	členovec	k1gMnPc2	členovec
(	(	kIx(	(
<g/>
Goldsmith	Goldsmitha	k1gFnPc2	Goldsmitha
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
objevy	objev	k1gInPc1	objev
feromonů	feromon	k1gInPc2	feromon
<g/>
,	,	kIx,	,
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
mozkové	mozkový	k2eAgFnSc2d1	mozková
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
byly	být	k5eAaImAgFnP	být
učiněny	učiněn	k2eAgFnPc1d1	učiněna
na	na	k7c6	na
bourci	bourec	k1gMnSc6	bourec
morušovém	morušový	k2eAgMnSc6d1	morušový
(	(	kIx(	(
<g/>
Grimaldi	Grimald	k1gMnPc1	Grimald
a	a	k8xC	a
Engel	Engel	k1gInSc1	Engel
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
získání	získání	k1gNnSc3	získání
extraktu	extrakt	k1gInSc2	extrakt
prvního	první	k4xOgInSc2	první
známého	známý	k2eAgInSc2d1	známý
feromonu	feromon	k1gInSc2	feromon
<g/>
,	,	kIx,	,
bombykolu	bombykol	k1gInSc2	bombykol
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
500	[number]	k4	500
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bylo	být	k5eAaImAgNnS	být
získáno	získat	k5eAaPmNgNnS	získat
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gNnSc1	jeho
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
Scoble	Scoble	k1gFnSc1	Scoble
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
výzkum	výzkum	k1gInSc1	výzkum
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
genetiku	genetika	k1gFnSc4	genetika
bource	bourec	k1gMnSc2	bourec
a	a	k8xC	a
genetické	genetický	k2eAgNnSc1d1	genetické
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
legendy	legenda	k1gFnSc2	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
hedvábí	hedvábí	k1gNnSc2	hedvábí
císařovnou	císařovna	k1gFnSc7	císařovna
Xi	Xi	k1gFnSc2	Xi
Ling-Shi	Ling-Sh	k1gFnSc2	Ling-Sh
(	(	kIx(	(
<g/>
嫘	嫘	k?	嫘
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
popíjela	popíjet	k5eAaImAgFnS	popíjet
pod	pod	k7c7	pod
stromem	strom	k1gInSc7	strom
čaj	čaj	k1gInSc1	čaj
a	a	k8xC	a
do	do	k7c2	do
šálku	šálek	k1gInSc2	šálek
jí	on	k3xPp3gFnSc2	on
spadl	spadnout	k5eAaPmAgInS	spadnout
zámotek	zámotek	k1gInSc1	zámotek
<g/>
.	.	kIx.	.
</s>
<s>
Vytahovala	vytahovat	k5eAaImAgFnS	vytahovat
jej	on	k3xPp3gInSc4	on
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konec	konec	k1gInSc1	konec
vlákna	vlákno	k1gNnSc2	vlákno
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
omotal	omotat	k5eAaPmAgMnS	omotat
kolem	kolem	k6eAd1	kolem
prstu	prst	k1gInSc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
odmotala	odmotat	k5eAaPmAgFnS	odmotat
všechno	všechen	k3xTgNnSc4	všechen
hedvábí	hedvábí	k1gNnSc4	hedvábí
uviděla	uvidět	k5eAaPmAgFnS	uvidět
kuklu	kukla	k1gFnSc4	kukla
a	a	k8xC	a
zjistila	zjistit	k5eAaPmAgFnS	zjistit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdrojem	zdroj	k1gInSc7	zdroj
toho	ten	k3xDgNnSc2	ten
hedvábí	hedvábí	k1gNnSc2	hedvábí
byla	být	k5eAaImAgFnS	být
kukla	kukla	k1gFnSc1	kukla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyprávěly	vyprávět	k5eAaImAgFnP	vyprávět
o	o	k7c6	o
hedvábí	hedvábí	k1gNnSc6	hedvábí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
si	se	k3xPyFc3	se
žárlivě	žárlivě	k6eAd1	žárlivě
střežili	střežit	k5eAaImAgMnP	střežit
své	svůj	k3xOyFgFnPc4	svůj
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
výrobě	výroba	k1gFnSc6	výroba
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
legenda	legenda	k1gFnSc1	legenda
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
čínská	čínský	k2eAgFnSc1d1	čínská
princezna	princezna	k1gFnSc1	princezna
propašovala	propašovat	k5eAaPmAgFnS	propašovat
vajíčka	vajíčko	k1gNnPc4	vajíčko
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
vlasech	vlas	k1gInPc6	vlas
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Japonci	Japonec	k1gMnPc1	Japonec
začali	začít	k5eAaPmAgMnP	začít
chovat	chovat	k5eAaImF	chovat
bource	bourec	k1gMnSc4	bourec
a	a	k8xC	a
používat	používat	k5eAaImF	používat
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zajímavost	zajímavost	k1gFnSc1	zajímavost
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
jednoho	jeden	k4xCgNnSc2	jeden
kimona	kimono	k1gNnSc2	kimono
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
hedvábí	hedvábí	k1gNnSc2	hedvábí
z	z	k7c2	z
2100	[number]	k4	2100
housenek	housenka	k1gFnPc2	housenka
bource	bourec	k1gMnSc4	bourec
morušového	morušový	k2eAgMnSc4d1	morušový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Choroby	choroba	k1gFnSc2	choroba
bource	bourec	k1gMnSc2	bourec
==	==	k?	==
</s>
</p>
<p>
<s>
Bourec	bourec	k1gMnSc1	bourec
často	často	k6eAd1	často
trpí	trpět	k5eAaImIp3nS	trpět
na	na	k7c4	na
infekční	infekční	k2eAgFnPc4d1	infekční
choroby	choroba	k1gFnPc4	choroba
způsobené	způsobený	k2eAgFnPc4d1	způsobená
prvoky	prvok	k1gMnPc7	prvok
<g/>
,	,	kIx,	,
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
viry	vir	k1gInPc7	vir
a	a	k8xC	a
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
několik	několik	k4yIc4	několik
chorob	choroba	k1gFnPc2	choroba
bource	bourec	k1gMnSc4	bourec
morušového	morušový	k2eAgMnSc4d1	morušový
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
evropskou	evropský	k2eAgFnSc4d1	Evropská
produkci	produkce	k1gFnSc4	produkce
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medicínské	medicínský	k2eAgNnSc1d1	medicínské
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
lék	lék	k1gInSc4	lék
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Bombyx	Bombyx	k1gInSc1	Bombyx
batryticatus	batryticatus	k1gInSc1	batryticatus
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
tuhý	tuhý	k2eAgMnSc1d1	tuhý
hedvábný	hedvábný	k2eAgMnSc1d1	hedvábný
červ	červ	k1gMnSc1	červ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
蚕	蚕	k?	蚕
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sušené	sušený	k2eAgFnPc1d1	sušená
housenky	housenka	k1gFnPc1	housenka
4	[number]	k4	4
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
.	.	kIx.	.
stadia	stadion	k1gNnSc2	stadion
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pošly	pojít	k5eAaPmAgInP	pojít
na	na	k7c4	na
chorobu	choroba	k1gFnSc4	choroba
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
infekcí	infekce	k1gFnSc7	infekce
houbou	houba	k1gFnSc7	houba
Beauveria	Beauverium	k1gNnSc2	Beauverium
bassiana	bassiana	k1gFnSc1	bassiana
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nadýmání	nadýmání	k1gNnSc3	nadýmání
<g/>
,	,	kIx,	,
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
hlenů	hlen	k1gInPc2	hlen
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc4	uvolnění
křečí	křeč	k1gFnPc2	křeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kuchyně	kuchyně	k1gFnSc2	kuchyně
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kukly	kukla	k1gFnSc2	kukla
nebo	nebo	k8xC	nebo
housenky	housenka	k1gFnSc2	housenka
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
pojídány	pojídán	k2eAgInPc1d1	pojídán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
kukly	kukla	k1gFnSc2	kukla
vaří	vařit	k5eAaImIp3nP	vařit
a	a	k8xC	a
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
populární	populární	k2eAgInSc1d1	populární
pokrm	pokrm	k1gInSc4	pokrm
zvaný	zvaný	k2eAgInSc4d1	zvaný
"	"	kIx"	"
<g/>
beondegi	beondege	k1gFnSc4	beondege
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
nabízejí	nabízet	k5eAaImIp3nP	nabízet
pouliční	pouliční	k2eAgMnPc1d1	pouliční
prodavači	prodavač	k1gMnPc1	prodavač
smažené	smažený	k2eAgFnSc2d1	smažená
housenky	housenka	k1gFnSc2	housenka
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
chutí	chuť	k1gFnSc7	chuť
poněkud	poněkud	k6eAd1	poněkud
připomínají	připomínat	k5eAaImIp3nP	připomínat
vepřové	vepřový	k2eAgInPc4d1	vepřový
škvarky	škvarek	k1gInPc4	škvarek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hedvábnictví	hedvábnictví	k1gNnSc1	hedvábnictví
</s>
</p>
<p>
<s>
Hedvábnictví	hedvábnictví	k1gNnSc1	hedvábnictví
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Scoble	Scoble	k6eAd1	Scoble
<g/>
,	,	kIx,	,
MJ	mj	kA	mj
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lepidoptera	Lepidopter	k1gMnSc2	Lepidopter
<g/>
:	:	kIx,	:
Form	Form	k1gInSc4	Form
<g/>
,	,	kIx,	,
function	function	k1gInSc4	function
and	and	k?	and
diversity	diversit	k1gInPc4	diversit
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
Univ	Univ	k1gInSc1	Univ
<g/>
.	.	kIx.	.
</s>
<s>
Press	Press	k6eAd1	Press
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Goldsmith	Goldsmith	k1gMnSc1	Goldsmith
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
Toru	torus	k1gInSc2	torus
Shimada	Shimada	k1gFnSc1	Shimada
<g/>
,	,	kIx,	,
and	and	k?	and
Hiroaki	Hiroak	k1gFnSc2	Hiroak
Abe	Abe	k1gFnSc2	Abe
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
genetics	genetics	k1gInSc1	genetics
and	and	k?	and
genomics	genomics	k1gInSc1	genomics
of	of	k?	of
the	the	k?	the
silkworm	silkworm	k1gInSc1	silkworm
<g/>
,	,	kIx,	,
Bombyx	Bombyx	k1gInSc1	Bombyx
mori	mor	k1gFnSc2	mor
<g/>
.	.	kIx.	.
</s>
<s>
Annual	Annual	k1gMnSc1	Annual
Review	Review	k1gMnSc1	Review
of	of	k?	of
Entomology	entomolog	k1gMnPc4	entomolog
50	[number]	k4	50
<g/>
:	:	kIx,	:
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
PMID	PMID	kA	PMID
15355234	[number]	k4	15355234
<g/>
.	.	kIx.	.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
access	access	k6eAd1	access
</s>
</p>
<p>
<s>
Grimaldi	Grimald	k1gMnPc1	Grimald
and	and	k?	and
Engel	Englo	k1gNnPc2	Englo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Evolution	Evolution	k1gInSc1	Evolution
of	of	k?	of
the	the	k?	the
Insects	Insects	k1gInSc1	Insects
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mita	Mita	k1gMnSc1	Mita
K.	K.	kA	K.
et	et	k?	et
<g/>
.	.	kIx.	.
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
genome	genom	k1gInSc5	genom
sequence	sequence	k1gFnSc2	sequence
of	of	k?	of
the	the	k?	the
silkworm	silkworm	k1gInSc1	silkworm
<g/>
,	,	kIx,	,
Bombyx	Bombyx	k1gInSc1	Bombyx
mori	mor	k1gFnSc2	mor
<g/>
.	.	kIx.	.
</s>
<s>
DNA	dna	k1gFnSc1	dna
Research	Research	k1gInSc1	Research
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
PMID	PMID	kA	PMID
15141943	[number]	k4	15141943
<g/>
.	.	kIx.	.
</s>
<s>
PDF	PDF	kA	PDF
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Studentská	studentský	k2eAgFnSc1d1	studentská
stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
housenkách	housenka	k1gFnPc6	housenka
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
WormSpit	WormSpit	k1gFnSc1	WormSpit
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
bourci	bourec	k1gMnSc6	bourec
morušovém	morušový	k2eAgMnSc6d1	morušový
(	(	kIx(	(
<g/>
housenky	housenka	k1gFnPc1	housenka
<g/>
,	,	kIx,	,
motýl	motýl	k1gMnSc1	motýl
<g/>
,	,	kIx,	,
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Podívejte	podívat	k5eAaPmRp2nP	podívat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
Lógaru	Lógar	k1gInSc6	Lógar
chová	chovat	k5eAaImIp3nS	chovat
bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
</s>
</p>
