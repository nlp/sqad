<s>
Phobos	Phobos	k1gInSc1	Phobos
<g/>
,	,	kIx,	,
též	též	k9	též
psáno	psán	k2eAgNnSc1d1	psáno
Fobos	Fobos	k1gInSc4	Fobos
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Φ	Φ	k?	Φ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Děs	děs	k1gInSc1	děs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
a	a	k8xC	a
větším	veliký	k2eAgInSc7d2	veliký
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
