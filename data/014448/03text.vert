<s>
Hekla	heknout	k5eAaPmAgFnS
</s>
<s>
Hekla	heknout	k5eAaPmAgFnS
Hekla	heknout	k5eAaPmAgFnS
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
1488	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
755	#num#	k4
m	m	kA
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Island	Island	k1gInSc1
Island	Island	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
63	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Hekla	heknout	k5eAaPmAgFnS
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
1750	#num#	k4
–	–	k?
Eggert	Eggert	k1gMnSc1
Olafsson	Olafsson	k1gMnSc1
Bjarni	Bjarni	k1gMnSc1
Palsson	Palsson	k1gMnSc1
Typ	typ	k1gInSc4
</s>
<s>
stratovulkán	stratovulkán	k1gInSc1
Erupce	erupce	k1gFnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2000	#num#	k4
Hornina	hornina	k1gFnSc1
</s>
<s>
sopečné	sopečný	k2eAgInPc1d1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stratovulkán	stratovulkán	k1gInSc1
Hekla	Hekla	k1gFnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
aktivních	aktivní	k2eAgMnPc2d1
stratovulkánů	stratovulkán	k1gInPc2
na	na	k7c6
Islandu	Island	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
40	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhého	dlouhý	k2eAgInSc2d1
vulkanického	vulkanický	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
6	#num#	k4
600	#num#	k4
let	léto	k1gNnPc2
starý	starý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
východní	východní	k2eAgFnSc2d1
riftové	riftový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hekla	heknout	k5eAaPmAgFnS
a	a	k8xC
Grímsvötn	Grímsvötn	k1gInSc4
jsou	být	k5eAaImIp3nP
nejaktivnějšími	aktivní	k2eAgFnPc7d3
sopkami	sopka	k1gFnPc7
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
ji	on	k3xPp3gFnSc4
Islanďané	Islanďan	k1gMnPc1
nazývali	nazývat	k5eAaImAgMnP
Brána	brána	k1gFnSc1
do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
nedaleko	nedaleko	k7c2
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
Landmannalaugar	Landmannalaugara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
se	se	k3xPyFc4
opakují	opakovat	k5eAaImIp3nP
téměř	téměř	k6eAd1
pravidelně	pravidelně	k6eAd1
v	v	k7c6
desetiletém	desetiletý	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
islandských	islandský	k2eAgFnPc2d1
sopek	sopka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
produkují	produkovat	k5eAaImIp3nP
tholeitické	tholeitický	k2eAgInPc4d1
bazalty	bazalt	k1gInPc4
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgInSc7d1
produktem	produkt	k1gInSc7
erupcí	erupce	k1gFnPc2
Hekly	heknout	k5eAaPmAgFnP
jsou	být	k5eAaImIp3nP
bazaltové	bazaltový	k2eAgInPc4d1
andezity	andezit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
z	z	k7c2
5,5	5,5	k4
km	km	kA
dlouhé	dlouhý	k2eAgFnSc2d1
trhliny	trhlina	k1gFnSc2
Heklugjá	Heklugjá	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
křižuje	křižovat	k5eAaImIp3nS
sopku	sopka	k1gFnSc4
a	a	k8xC
probíhají	probíhat	k5eAaImIp3nP
podél	podél	k7c2
celé	celý	k2eAgFnSc2d1
trhliny	trhlina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Časté	častý	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
v	v	k7c6
průběhu	průběh	k1gInSc6
staletí	staletí	k1gNnPc2
vytvořily	vytvořit	k5eAaPmAgFnP
a	a	k8xC
uložily	uložit	k5eAaPmAgFnP
na	na	k7c6
celém	celý	k2eAgInSc6d1
Islandu	Island	k1gInSc6
silné	silný	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
tefry	tefra	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
představují	představovat	k5eAaImIp3nP
dobrý	dobrý	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
informací	informace	k1gFnPc2
pro	pro	k7c4
vulkanology	vulkanolog	k1gMnPc4
<g/>
,	,	kIx,
studující	studující	k2eAgMnPc4d1
erupce	erupce	k1gFnSc2
sopek	sopka	k1gFnPc2
na	na	k7c6
Islandu	Island	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Erupce	erupce	k1gFnPc1
Hekly	heknout	k5eAaPmAgFnP
proběhly	proběhnout	k5eAaPmAgInP
v	v	k7c6
letech	léto	k1gNnPc6
2000	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1845	#num#	k4
<g/>
,	,	kIx,
1766	#num#	k4
<g/>
,	,	kIx,
1693	#num#	k4
<g/>
,	,	kIx,
1636	#num#	k4
<g/>
,	,	kIx,
1597	#num#	k4
<g/>
,	,	kIx,
1510	#num#	k4
<g/>
,	,	kIx,
1434	#num#	k4
<g/>
,	,	kIx,
1389	#num#	k4
<g/>
,	,	kIx,
1341	#num#	k4
<g/>
,	,	kIx,
1300	#num#	k4
<g/>
,	,	kIx,
1222	#num#	k4
<g/>
,	,	kIx,
1206	#num#	k4
<g/>
,	,	kIx,
1158	#num#	k4
a	a	k8xC
1104	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
erupcí	erupce	k1gFnPc2
byla	být	k5eAaImAgFnS
erupce	erupce	k1gFnSc1
„	„	k?
<g/>
H	H	kA
<g/>
3	#num#	k4
<g/>
“	“	k?
před	před	k7c7
téměř	téměř	k6eAd1
3	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
sopka	sopka	k1gFnSc1
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
vyvrhla	vyvrhnout	k5eAaPmAgFnS
7,3	7,3	k4
km³	km³	k?
sopečných	sopečný	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
a	a	k8xC
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
pravděpodobně	pravděpodobně	k6eAd1
způsobilo	způsobit	k5eAaPmAgNnS
i	i	k9
ochlazení	ochlazení	k1gNnSc1
globálního	globální	k2eAgNnSc2d1
klimatu	klima	k1gNnSc2
v	v	k7c6
několika	několik	k4yIc2
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Heklu	Hekla	k1gFnSc4
z	z	k7c2
Þ	Þ	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SIGURDSSON	SIGURDSSON	kA
<g/>
,	,	kIx,
Haraldur	Haraldur	k1gMnSc1
<g/>
;	;	kIx,
DAVIDSON	DAVIDSON	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
;	;	kIx,
DE	DE	k?
SILVA	Silva	k1gFnSc1
<g/>
,	,	kIx,
Shan	Shan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Volcanoes	Volcanoes	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
643140	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Composite	Composit	k1gInSc5
Volcanoes	Volcanoes	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
664	#num#	k4
<g/>
-	-	kIx~
<g/>
665	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hekla	heknout	k5eAaPmAgFnS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.volcano.si.edu	www.volcano.si.et	k5eAaPmIp1nS
-	-	kIx~
Hekla	heknout	k5eAaPmAgFnS
na	na	k7c6
Global	globat	k5eAaImAgInS
Volcanism	Volcanism	k1gInSc1
Program	program	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Photoguide	Photoguid	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Fotogalerie	Fotogalerie	k1gFnPc1
Hekly	heknout	k5eAaPmAgFnP
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Island	Island	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4264808-7	4264808-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85059976	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
249079131	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85059976	#num#	k4
</s>
