<s>
Stratovulkán	stratovulkán	k1gInSc1
Hekla	Hekla	k1gFnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
aktivních	aktivní	k2eAgMnPc2d1
stratovulkánů	stratovulkán	k1gInPc2
na	na	k7c6
Islandu	Island	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
40	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhého	dlouhý	k2eAgInSc2d1
vulkanického	vulkanický	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
6	#num#	k4
600	#num#	k4
let	léto	k1gNnPc2
starý	starý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>