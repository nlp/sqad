<s>
Zeus	Zeus	k1gInSc1	Zeus
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
relativně	relativně	k6eAd1	relativně
nedůležitý	důležitý	k2eNgMnSc1d1	nedůležitý
indoevropský	indoevropský	k2eAgMnSc1d1	indoevropský
bůh	bůh	k1gMnSc1	bůh
jasného	jasný	k2eAgNnSc2d1	jasné
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
splynul	splynout	k5eAaPmAgInS	splynout
s	s	k7c7	s
několika	několik	k4yIc7	několik
jinými	jiný	k2eAgMnPc7d1	jiný
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
bohem	bůh	k1gMnSc7	bůh
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
