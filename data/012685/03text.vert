<p>
<s>
David	David	k1gMnSc1	David
Marshall	Marshall	k1gMnSc1	Marshall
Coulthard	Coulthard	k1gMnSc1	Coulthard
<g/>
,	,	kIx,	,
MBE	MBE	kA	MBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1971	[number]	k4	1971
Twynholm	Twynholma	k1gFnPc2	Twynholma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skotský	skotský	k2eAgMnSc1d1	skotský
pilot	pilot	k1gMnSc1	pilot
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
13	[number]	k4	13
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
přezdívka	přezdívka	k1gFnSc1	přezdívka
je	být	k5eAaImIp3nS	být
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
dobrých	dobrý	k2eAgInPc6d1	dobrý
vozech	vůz	k1gInPc6	vůz
figuruje	figurovat	k5eAaImIp3nS	figurovat
David	David	k1gMnSc1	David
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
jezdci	jezdec	k1gMnPc7	jezdec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nasbírali	nasbírat	k5eAaPmAgMnP	nasbírat
nejvíce	hodně	k6eAd3	hodně
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
Britem	Brit	k1gMnSc7	Brit
co	co	k8xS	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
nasbíraných	nasbíraný	k2eAgInPc2d1	nasbíraný
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
jsou	být	k5eAaImIp3nP	být
už	už	k9	už
jen	jen	k9	jen
takoví	takový	k3xDgMnPc1	takový
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
byli	být	k5eAaImAgMnP	být
Ayrton	Ayrton	k1gInSc4	Ayrton
Senna	Senno	k1gNnSc2	Senno
<g/>
,	,	kIx,	,
Alain	Alain	k1gMnSc1	Alain
Prost	prost	k2eAgMnSc1d1	prost
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Coulthard	Coulthard	k1gInSc1	Coulthard
má	mít	k5eAaImIp3nS	mít
trvalé	trvalý	k2eAgNnSc4d1	trvalé
bydliště	bydliště	k1gNnSc4	bydliště
uvedené	uvedený	k2eAgNnSc4d1	uvedené
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
také	také	k9	také
domy	dům	k1gInPc4	dům
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastnit	k5eAaImIp3nS	vlastnit
také	také	k9	také
luxusní	luxusní	k2eAgInPc4d1	luxusní
hotely	hotel	k1gInPc4	hotel
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Monaku	Monako	k1gNnSc6	Monako
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hotelu	hotel	k1gInSc2	hotel
Columbus	Columbus	k1gInSc1	Columbus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Monackém	monacký	k2eAgInSc6d1	monacký
Fontvieille	Fontvieill	k1gInSc6	Fontvieill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
pronajatý	pronajatý	k2eAgInSc4d1	pronajatý
Learjet	Learjet	k1gInSc4	Learjet
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Davida	David	k1gMnSc2	David
Murrayho	Murray	k1gMnSc2	Murray
<g/>
,	,	kIx,	,
letadlo	letadlo	k1gNnSc1	letadlo
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
problém	problém	k1gInSc4	problém
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
měla	mít	k5eAaImAgFnS	mít
skončit	skončit	k5eAaPmF	skončit
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
letišti	letiště	k1gNnSc6	letiště
Côte	Côt	k1gInSc2	Côt
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Azur	azur	k1gInSc1	azur
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
letadlo	letadlo	k1gNnSc1	letadlo
havarovalo	havarovat	k5eAaPmAgNnS	havarovat
při	při	k7c6	při
nouzovém	nouzový	k2eAgNnSc6d1	nouzové
přistání	přistání	k1gNnSc6	přistání
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Lyon-Satolas	Lyon-Satolasa	k1gFnPc2	Lyon-Satolasa
<g/>
.	.	kIx.	.
</s>
<s>
Coulthard	Coulthard	k1gInSc1	Coulthard
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
modelka	modelka	k1gFnSc1	modelka
Heidi	Heid	k1gMnPc1	Heid
Wichlinski	Wichlinski	k1gNnPc2	Wichlinski
a	a	k8xC	a
osobní	osobní	k2eAgMnSc1d1	osobní
trenér	trenér	k1gMnSc1	trenér
Andy	Anda	k1gFnSc2	Anda
Matthews	Matthewsa	k1gFnPc2	Matthewsa
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Murrayho	Murray	k1gMnSc4	Murray
osobní	osobní	k2eAgMnSc1d1	osobní
pilot	pilot	k1gMnSc1	pilot
David	David	k1gMnSc1	David
Saunders	Saunders	k1gInSc1	Saunders
a	a	k8xC	a
co-pilot	coilot	k1gInSc1	co-pilot
Dan	Dana	k1gFnPc2	Dana
Worley	Worlea	k1gFnSc2	Worlea
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
Davidovi	David	k1gMnSc6	David
toho	ten	k3xDgNnSc2	ten
bulvární	bulvární	k2eAgInPc1d1	bulvární
deníky	deník	k1gInPc1	deník
napsali	napsat	k5eAaBmAgMnP	napsat
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
častým	častý	k2eAgNnSc7d1	časté
střídáním	střídání	k1gNnSc7	střídání
žen	žena	k1gFnPc2	žena
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
boku	bok	k1gInSc6	bok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
už	už	k6eAd1	už
stanuly	stanout	k5eAaPmAgInP	stanout
například	například	k6eAd1	například
Heidi	Heid	k1gMnPc1	Heid
Klum	Kluma	k1gFnPc2	Kluma
<g/>
,	,	kIx,	,
Lady	lady	k1gFnSc1	lady
Victoria	Victorium	k1gNnSc2	Victorium
Hervey	Hervea	k1gFnSc2	Hervea
nebo	nebo	k8xC	nebo
modelky	modelka	k1gFnSc2	modelka
Andrea	Andrea	k1gFnSc1	Andrea
Murray	Murraa	k1gFnSc2	Murraa
a	a	k8xC	a
Ruth	Ruth	k1gFnSc2	Ruth
Taylor	Taylora	k1gFnPc2	Taylora
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zasnoubený	zasnoubený	k2eAgMnSc1d1	zasnoubený
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
modelkami	modelka	k1gFnPc7	modelka
Heidi	Heid	k1gMnPc1	Heid
Wichlinski	Wichlinsk	k1gMnSc5	Wichlinsk
a	a	k8xC	a
Simone	Simon	k1gMnSc5	Simon
Abdelnour	Abdelnoura	k1gFnPc2	Abdelnoura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Karen	Karen	k1gInSc1	Karen
Minier	Miniero	k1gNnPc2	Miniero
<g/>
,	,	kIx,	,
belgickou	belgický	k2eAgFnSc7d1	belgická
novinářkou	novinářka	k1gFnSc7	novinářka
F1	F1	k1gFnSc2	F1
pracující	pracující	k2eAgFnSc2d1	pracující
pro	pro	k7c4	pro
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
TF	tf	k0wR	tf
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
plánují	plánovat	k5eAaImIp3nP	plánovat
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
vesnici	vesnice	k1gFnSc6	vesnice
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Wendy	Wenda	k1gMnSc2	Wenda
McKenzieho	McKenzie	k1gMnSc2	McKenzie
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
jej	on	k3xPp3gNnSc4	on
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Coulthardova	Coulthardův	k2eAgFnSc1d1	Coulthardova
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
tak	tak	k6eAd1	tak
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
mnoho	mnoho	k4c1	mnoho
návštěvníků	návštěvník	k1gMnPc2	návštěvník
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
vydal	vydat	k5eAaPmAgMnS	vydat
Coulthard	Coulthard	k1gMnSc1	Coulthard
svoji	svůj	k3xOyFgFnSc4	svůj
autobiografii	autobiografie	k1gFnSc4	autobiografie
<g/>
,	,	kIx,	,
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
It	It	k1gMnSc1	It
is	is	k?	is
What	What	k1gMnSc1	What
It	It	k1gMnSc1	It
Is	Is	k1gMnSc1	Is
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
také	také	k6eAd1	také
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
léčil	léčit	k5eAaImAgMnS	léčit
z	z	k7c2	z
bulimie	bulimie	k1gFnSc2	bulimie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Coulthardova	Coulthardův	k2eAgFnSc1d1	Coulthardova
druhá	druhý	k4xOgFnSc1	druhý
sestřenice	sestřenice	k1gFnSc1	sestřenice
<g/>
,	,	kIx,	,
Novozélanďanka	Novozélanďanka	k1gFnSc1	Novozélanďanka
Fabian	Fabian	k1gMnSc1	Fabian
Coulthard	Coulthard	k1gMnSc1	Coulthard
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
též	též	k9	též
závodní	závodní	k2eAgFnSc7d1	závodní
jezdkyní	jezdkyně	k1gFnSc7	jezdkyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
před	před	k7c7	před
Formulí	formule	k1gFnSc7	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
Coulthard	Coulthard	k1gMnSc1	Coulthard
začínal	začínat	k5eAaImAgMnS	začínat
již	již	k6eAd1	již
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
automobilových	automobilový	k2eAgMnPc2d1	automobilový
závodníků	závodník	k1gMnPc2	závodník
na	na	k7c6	na
motokárách	motokára	k1gFnPc6	motokára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1982	[number]	k4	1982
–	–	k?	–
1988	[number]	k4	1988
získal	získat	k5eAaPmAgInS	získat
třikrát	třikrát	k6eAd1	třikrát
titul	titul	k1gInSc1	titul
skotského	skotský	k2eAgMnSc2d1	skotský
šampióna	šampión	k1gMnSc2	šampión
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
mistrovství	mistrovství	k1gNnSc6	mistrovství
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
britského	britský	k2eAgMnSc2d1	britský
super	super	k2eAgMnSc2d1	super
šampióna	šampión	k1gMnSc2	šampión
v	v	k7c6	v
juniorské	juniorský	k2eAgFnSc6d1	juniorská
kategorii	kategorie	k1gFnSc6	kategorie
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
vítězem	vítěz	k1gMnSc7	vítěz
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
mistrovství	mistrovství	k1gNnSc2	mistrovství
Skotska	Skotsko	k1gNnSc2	Skotsko
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formule	formule	k1gFnSc1	formule
Ford	ford	k1gInSc1	ford
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
vítězství	vítězství	k1gNnPc4	vítězství
v	v	k7c6	v
juniorské	juniorský	k2eAgFnSc6d1	juniorská
kategorii	kategorie	k1gFnSc6	kategorie
1600	[number]	k4	1600
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
Formule	formule	k1gFnPc4	formule
Ford	ford	k1gInSc4	ford
Festivalu	festival	k1gInSc2	festival
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c4	v
Brands	Brands	k1gInSc4	Brands
Hatch	Hatcha	k1gFnPc2	Hatcha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
si	se	k3xPyFc3	se
zlomil	zlomit	k5eAaPmAgMnS	zlomit
nohu	noha	k1gFnSc4	noha
při	při	k7c6	při
závodech	závod	k1gInPc6	závod
GM	GM	kA	GM
Lotus	Lotus	kA	Lotus
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Spa	Spa	k1gFnSc1	Spa
Francorchamps	Francorchamps	k1gInSc1	Francorchamps
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
prvního	první	k4xOgInSc2	první
testu	test	k1gInSc2	test
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
McLaren	McLarna	k1gFnPc2	McLarna
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
pro	pro	k7c4	pro
formuli	formule	k1gFnSc4	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
David	David	k1gMnSc1	David
jezdcem	jezdec	k1gInSc7	jezdec
Formule	formule	k1gFnSc1	formule
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
ale	ale	k9	ale
skončil	skončit	k5eAaPmAgInS	skončit
až	až	k6eAd1	až
devátý	devátý	k4xOgMnSc1	devátý
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
konečnou	konečný	k2eAgFnSc4d1	konečná
třetí	třetí	k4xOgFnSc4	třetí
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Williams	Williams	k1gInSc1	Williams
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1994	[number]	k4	1994
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Coulthard	Coulthard	k1gMnSc1	Coulthard
testovacím	testovací	k2eAgMnSc7d1	testovací
jezdcem	jezdec	k1gMnSc7	jezdec
několikanásobného	několikanásobný	k2eAgNnSc2d1	několikanásobné
vítěze	vítěz	k1gMnPc4	vítěz
poháru	pohár	k1gInSc2	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
,	,	kIx,	,
týmu	tým	k1gInSc2	tým
Williams-Renault	Williams-Renaultum	k1gNnPc2	Williams-Renaultum
a	a	k8xC	a
hrál	hrát	k5eAaImAgInS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1994	[number]	k4	1994
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
u	u	k7c2	u
Williamsu	Williams	k1gInSc2	Williams
zůstat	zůstat	k5eAaPmF	zůstat
opět	opět	k6eAd1	opět
jako	jako	k8xC	jako
testovací	testovací	k2eAgInSc1d1	testovací
jezdec	jezdec	k1gInSc1	jezdec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
tragické	tragický	k2eAgFnSc3d1	tragická
smrti	smrt	k1gFnSc3	smrt
jezdce	jezdec	k1gInSc2	jezdec
Williamsu	Williams	k1gInSc2	Williams
<g/>
,	,	kIx,	,
Ayrtona	Ayrtona	k1gFnSc1	Ayrtona
Senny	Senny	k?	Senny
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
závodního	závodní	k2eAgInSc2d1	závodní
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Španělska	Španělsko	k1gNnSc2	Španělsko
už	už	k6eAd1	už
startoval	startovat	k5eAaBmAgInS	startovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Damona	Damon	k1gMnSc2	Damon
Hilla	Hill	k1gMnSc2	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
sezóny	sezóna	k1gFnSc2	sezóna
strávil	strávit	k5eAaPmAgMnS	strávit
řízením	řízení	k1gNnSc7	řízení
druhého	druhý	k4xOgInSc2	druhý
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
závodech	závod	k1gInPc6	závod
musel	muset	k5eAaImAgInS	muset
svou	svůj	k3xOyFgFnSc4	svůj
sedačku	sedačka	k1gFnSc4	sedačka
propůjčit	propůjčit	k5eAaPmF	propůjčit
ex-mistru	existr	k1gMnSc3	ex-mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Nigelu	Nigel	k1gMnSc3	Nigel
Mansellovi	Mansell	k1gMnSc3	Mansell
<g/>
.	.	kIx.	.
</s>
<s>
Williams	Williams	k1gInSc1	Williams
byl	být	k5eAaImAgInS	být
pyšný	pyšný	k2eAgMnSc1d1	pyšný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
druhý	druhý	k4xOgInSc1	druhý
vůz	vůz	k1gInSc1	vůz
pilotuje	pilotovat	k5eAaImIp3nS	pilotovat
Mansell	Mansell	k1gInSc4	Mansell
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
uřadující	uřadující	k2eAgMnSc1d1	uřadující
šampión	šampión	k1gMnSc1	šampión
Indycar	Indycar	k1gMnSc1	Indycar
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Coulthard	Coulthard	k1gMnSc1	Coulthard
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
pozadu	pozadu	k6eAd1	pozadu
<g/>
,	,	kIx,	,
prokázal	prokázat	k5eAaPmAgMnS	prokázat
svou	svůj	k3xOyFgFnSc4	svůj
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
chybám	chyba	k1gFnPc3	chyba
soupeřů	soupeř	k1gMnPc2	soupeř
a	a	k8xC	a
notné	notný	k2eAgFnSc6d1	notná
dávce	dávka	k1gFnSc6	dávka
štěstí	štěstí	k1gNnSc2	štěstí
dokonce	dokonce	k9	dokonce
podíval	podívat	k5eAaPmAgMnS	podívat
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
když	když	k8xS	když
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
poslední	poslední	k2eAgInPc4d1	poslední
3	[number]	k4	3
závody	závod	k1gInPc4	závod
už	už	k6eAd1	už
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
seděl	sedět	k5eAaImAgMnS	sedět
na	na	k7c6	na
tribuně	tribuna	k1gFnSc6	tribuna
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
vede	vést	k5eAaImIp3nS	vést
Mansell	Mansell	k1gMnSc1	Mansell
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
Nigel	Nigel	k1gInSc1	Nigel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dostal	dostat	k5eAaPmAgMnS	dostat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Coulthard	Coulthard	k1gMnSc1	Coulthard
tak	tak	k6eAd1	tak
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
neúplné	úplný	k2eNgFnSc6d1	neúplná
sezóně	sezóna	k1gFnSc6	sezóna
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
skvělém	skvělý	k2eAgMnSc6d1	skvělý
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
14	[number]	k4	14
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1995	[number]	k4	1995
====	====	k?	====
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
1995	[number]	k4	1995
konečně	konečně	k9	konečně
ukázala	ukázat	k5eAaPmAgFnS	ukázat
Davidův	Davidův	k2eAgInSc4d1	Davidův
nevyužitý	využitý	k2eNgInSc4d1	nevyužitý
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
závod	závod	k1gInSc4	závod
v	v	k7c6	v
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrát	vyhrát	k5eAaPmF	vyhrát
mohl	moct	k5eAaImAgMnS	moct
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chyby	chyba	k1gFnPc1	chyba
a	a	k8xC	a
smůla	smůla	k1gFnSc1	smůla
mu	on	k3xPp3gMnSc3	on
už	už	k6eAd1	už
víckrát	víckrát	k6eAd1	víckrát
vyhrát	vyhrát	k5eAaPmF	vyhrát
nedopřála	dopřát	k5eNaPmAgFnS	dopřát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
celkem	celek	k1gInSc7	celek
5	[number]	k4	5
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
a	a	k8xC	a
4	[number]	k4	4
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
hned	hned	k6eAd1	hned
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
mnoho	mnoho	k4c4	mnoho
pěkných	pěkná	k1gFnPc2	pěkná
umístění	umístění	k1gNnPc2	umístění
nastaly	nastat	k5eAaPmAgFnP	nastat
i	i	k8xC	i
hrubé	hrubý	k2eAgFnPc1d1	hrubá
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
v	v	k7c6	v
Monze	Monza	k1gFnSc6	Monza
chyboval	chybovat	k5eAaImAgMnS	chybovat
v	v	k7c6	v
zahřívacím	zahřívací	k2eAgNnSc6d1	zahřívací
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
Adelaide	Adelaid	k1gInSc5	Adelaid
naboural	nabourat	k5eAaPmAgMnS	nabourat
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
boxů	box	k1gInPc2	box
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
ale	ale	k9	ale
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
skvělém	skvělý	k2eAgMnSc6d1	skvělý
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
49	[number]	k4	49
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
McLaren	McLarna	k1gFnPc2	McLarna
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1996	[number]	k4	1996
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
David	David	k1gMnSc1	David
upsal	upsat	k5eAaPmAgMnS	upsat
týmu	tým	k1gInSc3	tým
McLaren	McLarna	k1gFnPc2	McLarna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
tehdy	tehdy	k6eAd1	tehdy
budoucího	budoucí	k2eAgMnSc4d1	budoucí
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Mikky	Mikka	k1gMnSc2	Mikka
Häkkinena	Häkkinen	k1gMnSc2	Häkkinen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sezóna	sezóna	k1gFnSc1	sezóna
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
týmu	tým	k1gInSc6	tým
se	se	k3xPyFc4	se
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
nepovedla	povést	k5eNaPmAgFnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
McLaren	McLarna	k1gFnPc2	McLarna
poháněly	pohánět	k5eAaImAgInP	pohánět
silné	silný	k2eAgInPc1d1	silný
motory	motor	k1gInPc1	motor
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
nemohl	moct	k5eNaImAgInS	moct
najít	najít	k5eAaPmF	najít
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
ale	ale	k9	ale
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
Imole	Imola	k1gFnSc6	Imola
a	a	k8xC	a
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
,	,	kIx,	,
Olivierem	Olivier	k1gMnSc7	Olivier
Panisem	Panis	k1gInSc7	Panis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
po	po	k7c6	po
2	[number]	k4	2
umístěních	umístění	k1gNnPc6	umístění
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
18	[number]	k4	18
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
stačilo	stačit	k5eAaBmAgNnS	stačit
jen	jen	k9	jen
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1997	[number]	k4	1997
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc6	jeho
druhé	druhý	k4xOgFnSc6	druhý
sezóně	sezóna	k1gFnSc6	sezóna
u	u	k7c2	u
McLarenu	McLaren	k1gInSc2	McLaren
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgInS	skončit
celkově	celkově	k6eAd1	celkově
s	s	k7c7	s
Jeanem	Jean	k1gMnSc7	Jean
Alesim	Alesim	k1gInSc4	Alesim
na	na	k7c4	na
stejným	stejný	k2eAgNnPc3d1	stejné
počtu	počet	k1gInSc3	počet
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
2	[number]	k4	2
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
Alesi	Alese	k1gFnSc4	Alese
žádné	žádný	k3yNgNnSc4	žádný
<g/>
,	,	kIx,	,
a	a	k8xC	a
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
přihodilo	přihodit	k5eAaPmAgNnS	přihodit
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
druhý	druhý	k4xOgMnSc1	druhý
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
šampionátu	šampionát	k1gInSc2	šampionát
diskvalifikován	diskvalifikován	k2eAgMnSc1d1	diskvalifikován
a	a	k8xC	a
Coulthard	Coulthard	k1gMnSc1	Coulthard
tak	tak	k6eAd1	tak
obsadil	obsadit	k5eAaPmAgMnS	obsadit
se	s	k7c7	s
štěstím	štěstí	k1gNnSc7	štěstí
konečné	konečná	k1gFnSc2	konečná
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
tedy	tedy	k9	tedy
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
úspěchy	úspěch	k1gInPc4	úspěch
zkazily	zkazit	k5eAaPmAgFnP	zkazit
některé	některý	k3yIgFnPc1	některý
situace	situace	k1gFnPc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Jerezu	Jerez	k1gInSc6	Jerez
se	se	k3xPyFc4	se
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
diskvalifikaci	diskvalifikace	k1gFnSc4	diskvalifikace
Schuamchera	Schuamchero	k1gNnSc2	Schuamchero
(	(	kIx(	(
<g/>
snaha	snaha	k1gFnSc1	snaha
vyřadit	vyřadit	k5eAaPmF	vyřadit
Villeneuva	Villeneuv	k1gMnSc4	Villeneuv
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posunul	posunout	k5eAaPmAgMnS	posunout
David	David	k1gMnSc1	David
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tehdy	tehdy	k6eAd1	tehdy
povolená	povolený	k2eAgFnSc1d1	povolená
týmová	týmový	k2eAgFnSc1d1	týmová
režie	režie	k1gFnSc1	režie
týmu	tým	k1gInSc2	tým
velela	velet	k5eAaImAgFnS	velet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
pustil	pustit	k5eAaPmAgMnS	pustit
Häikkinena	Häikkinena	k1gFnSc1	Häikkinena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1998	[number]	k4	1998
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
McLaren	McLarna	k1gFnPc2	McLarna
jasně	jasně	k6eAd1	jasně
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
Häkkinen	Häkkinen	k2eAgInSc1d1	Häkkinen
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
udával	udávat	k5eAaImAgMnS	udávat
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
si	se	k3xPyFc3	se
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Coulthard	Coulthard	k1gInSc1	Coulthard
si	se	k3xPyFc3	se
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
pouhé	pouhý	k2eAgNnSc4d1	pouhé
jedno	jeden	k4xCgNnSc4	jeden
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
sezóny	sezóna	k1gFnSc2	sezóna
plnil	plnit	k5eAaImAgInS	plnit
jen	jen	k9	jen
funkci	funkce	k1gFnSc4	funkce
podpory	podpora	k1gFnSc2	podpora
Häkkinena	Häkkineno	k1gNnSc2	Häkkineno
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k9	ale
získal	získat	k5eAaPmAgInS	získat
konečné	konečná	k1gFnSc2	konečná
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1999	[number]	k4	1999
====	====	k?	====
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
1999	[number]	k4	1999
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
podobala	podobat	k5eAaImAgFnS	podobat
té	ten	k3xDgFnSc2	ten
minulé	minulý	k2eAgNnSc4d1	Minulé
<g/>
.	.	kIx.	.
</s>
<s>
Davidovi	Davidův	k2eAgMnPc1d1	Davidův
štěstí	štěstí	k1gNnSc4	štěstí
nepřálo	přát	k5eNaImAgNnS	přát
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
neustále	neustále	k6eAd1	neustále
podceňován	podceňovat	k5eAaImNgInS	podceňovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
2	[number]	k4	2
<g/>
x	x	k?	x
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k8xC	tak
mu	on	k3xPp3gNnSc3	on
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
stačilo	stačit	k5eAaBmAgNnS	stačit
až	až	k9	až
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
McLarenu	McLaren	k1gInSc2	McLaren
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrál	prohrát	k5eAaPmAgMnS	prohrát
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
bitvě	bitva	k1gFnSc6	bitva
se	s	k7c7	s
stájí	stáj	k1gFnSc7	stáj
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2000	[number]	k4	2000
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
už	už	k9	už
David	David	k1gMnSc1	David
konečně	konečně	k6eAd1	konečně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
bitvu	bitva	k1gFnSc4	bitva
svedl	svést	k5eAaPmAgMnS	svést
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
Häkkinenem	Häkkinen	k1gMnSc7	Häkkinen
<g/>
,	,	kIx,	,
a	a	k8xC	a
Michaelem	Michael	k1gMnSc7	Michael
Schumacherem	Schumacher	k1gMnSc7	Schumacher
z	z	k7c2	z
Ferrari	ferrari	k1gNnSc2	ferrari
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
si	se	k3xPyFc3	se
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
konto	konto	k1gNnSc4	konto
připsal	připsat	k5eAaPmAgMnS	připsat
Coulthard	Coulthard	k1gMnSc1	Coulthard
3	[number]	k4	3
vítězství	vítězství	k1gNnPc2	vítězství
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
8	[number]	k4	8
umístění	umístění	k1gNnPc2	umístění
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
bitvu	bitva	k1gFnSc4	bitva
prohrál	prohrát	k5eAaPmAgMnS	prohrát
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
až	až	k9	až
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
ale	ale	k9	ale
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
nejvíce	hodně	k6eAd3	hodně
bodů	bod	k1gInPc2	bod
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2001-2004	[number]	k4	2001-2004
====	====	k?	====
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
2001	[number]	k4	2001
o	o	k7c4	o
sobě	se	k3xPyFc3	se
dával	dávat	k5eAaImAgMnS	dávat
David	David	k1gMnSc1	David
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
porážel	porážet	k5eAaImAgInS	porážet
i	i	k9	i
Miku	Mik	k1gMnSc3	Mik
Häkkinena	Häkkineno	k1gNnSc2	Häkkineno
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
na	na	k7c4	na
suverénního	suverénní	k2eAgMnSc4d1	suverénní
Schumachera	Schumacher	k1gMnSc4	Schumacher
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
skončil	skončit	k5eAaPmAgMnS	skončit
celkově	celkově	k6eAd1	celkově
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
méně	málo	k6eAd2	málo
bodů	bod	k1gInPc2	bod
než	než	k8xS	než
německý	německý	k2eAgMnSc1d1	německý
šampión	šampión	k1gMnSc1	šampión
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohužel	bohužel	k9	bohužel
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
šly	jít	k5eAaImAgInP	jít
Coulthardovy	Coulthardův	k2eAgInPc1d1	Coulthardův
výsledky	výsledek	k1gInPc1	výsledek
značně	značně	k6eAd1	značně
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
Mikky	Mikka	k1gMnSc2	Mikka
Häkkinena	Häkkinen	k1gMnSc2	Häkkinen
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
party	parta	k1gFnSc2	parta
dalšího	další	k2eAgMnSc4d1	další
Fina	Fin	k1gMnSc4	Fin
<g/>
,	,	kIx,	,
mladého	mladý	k2eAgMnSc4d1	mladý
Kimiho	Kimi	k1gMnSc4	Kimi
Räikkönena	Räikkönen	k1gMnSc4	Räikkönen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Skota	Skot	k1gMnSc4	Skot
jednoznačně	jednoznačně	k6eAd1	jednoznačně
porážel	porážet	k5eAaImAgMnS	porážet
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Davidův	Davidův	k2eAgInSc1d1	Davidův
pokles	pokles	k1gInSc1	pokles
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zavedla	zavést	k5eAaPmAgFnS	zavést
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
měli	mít	k5eAaImAgMnP	mít
jezdci	jezdec	k1gMnPc1	jezdec
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k9	jen
1	[number]	k4	1
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
museli	muset	k5eAaImAgMnP	muset
zajet	zajet	k2eAgInSc4d1	zajet
co	co	k8xS	co
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
formát	formát	k1gInSc4	formát
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
mu	on	k3xPp3gMnSc3	on
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
přiznal	přiznat	k5eAaPmAgMnS	přiznat
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
sezóny	sezóna	k1gFnSc2	sezóna
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
pouhá	pouhý	k2eAgFnSc1d1	pouhá
2	[number]	k4	2
vítězství	vítězství	k1gNnPc2	vítězství
a	a	k8xC	a
umístění	umístění	k1gNnSc2	umístění
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
se	se	k3xPyFc4	se
snižovala	snižovat	k5eAaImAgFnS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
celkově	celkově	k6eAd1	celkově
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
až	až	k6eAd1	až
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
poprvé	poprvé	k6eAd1	poprvé
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
nepodíval	podívat	k5eNaImAgMnS	podívat
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
desátý	desátý	k4xOgMnSc1	desátý
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
David	David	k1gMnSc1	David
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
McLarenu	McLaren	k1gInSc2	McLaren
opravdu	opravdu	k6eAd1	opravdu
namále	namále	k6eAd1	namále
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dny	den	k1gInPc1	den
u	u	k7c2	u
McLarenu	McLaren	k1gInSc2	McLaren
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
opravdu	opravdu	k6eAd1	opravdu
sečteny	sečten	k2eAgFnPc1d1	sečtena
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
ohlášen	ohlášen	k2eAgInSc4d1	ohlášen
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Räikkönena	Räikköneno	k1gNnSc2	Räikköneno
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Pablo	Pablo	k1gNnSc1	Pablo
Montoya	Montoya	k1gMnSc1	Montoya
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Red	Red	k1gFnSc1	Red
Bull	bulla	k1gFnPc2	bulla
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
2005	[number]	k4	2005
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
ale	ale	k8xC	ale
nečekal	čekat	k5eNaImAgMnS	čekat
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
bohaté	bohatý	k2eAgFnPc1d1	bohatá
zkušenosti	zkušenost	k1gFnPc1	zkušenost
se	se	k3xPyFc4	se
hodily	hodit	k5eAaPmAgFnP	hodit
novému	nový	k2eAgInSc3d1	nový
týmu	tým	k1gInSc2	tým
Red	Red	k1gFnPc2	Red
Bull	bulla	k1gFnPc2	bulla
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
nezkušenému	zkušený	k2eNgMnSc3d1	nezkušený
Christianu	Christian	k1gMnSc3	Christian
Klienovi	Klien	k1gMnSc3	Klien
a	a	k8xC	a
Vitantoniu	Vitantonium	k1gNnSc3	Vitantonium
Liuzzimu	Liuzzim	k1gInSc2	Liuzzim
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Kliena	Kliena	k1gFnSc1	Kliena
v	v	k7c6	v
několika	několik	k4yIc6	několik
závodech	závod	k1gInPc6	závod
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Coulthardův	Coulthardův	k2eAgInSc1d1	Coulthardův
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
Red	Red	k1gMnSc7	Red
Bullem	Bull	k1gMnSc7	Bull
byl	být	k5eAaImAgMnS	být
prodloužen	prodloužit	k5eAaPmNgMnS	prodloužit
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc1	Británie
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
David	David	k1gMnSc1	David
bude	být	k5eAaImBp3nS	být
jezdit	jezdit	k5eAaImF	jezdit
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
jak	jak	k8xS	jak
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
bod	bod	k1gInSc4	bod
přesně	přesně	k6eAd1	přesně
svůj	svůj	k3xOyFgInSc4	svůj
výsledek	výsledek	k1gInSc4	výsledek
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
sezóny	sezóna	k1gFnSc2	sezóna
u	u	k7c2	u
McLarenu	McLaren	k1gInSc2	McLaren
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2006	[number]	k4	2006
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
tedy	tedy	k8xC	tedy
David	David	k1gMnSc1	David
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
Red	Red	k1gFnSc6	Red
Bullu	bulla	k1gFnSc4	bulla
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Christianem	Christian	k1gMnSc7	Christian
Klienem	Klien	k1gMnSc7	Klien
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
sezónu	sezóna	k1gFnSc4	sezóna
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Red	Red	k1gFnSc1	Red
Bullu	bulla	k1gFnSc4	bulla
motory	motor	k1gInPc7	motor
stáj	stáj	k1gFnSc1	stáj
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Renaultem	renault	k1gInSc7	renault
na	na	k7c4	na
dodávání	dodávání	k1gNnSc4	dodávání
motorů	motor	k1gInPc2	motor
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
přišel	přijít	k5eAaPmAgMnS	přijít
další	další	k2eAgMnSc1d1	další
zkušený	zkušený	k2eAgMnSc1d1	zkušený
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
technický	technický	k2eAgMnSc1d1	technický
ředitel	ředitel	k1gMnSc1	ředitel
Adrian	Adrian	k1gMnSc1	Adrian
Newey	Newea	k1gMnSc2	Newea
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
McLarenu	McLaren	k1gInSc2	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
se	se	k3xPyFc4	se
ale	ale	k9	ale
Coulthardovi	Coulthardův	k2eAgMnPc1d1	Coulthardův
moc	moc	k6eAd1	moc
nepovedla	povést	k5eNaPmAgFnS	povést
<g/>
,	,	kIx,	,
bodoval	bodovat	k5eAaImAgMnS	bodovat
jen	jen	k9	jen
v	v	k7c6	v
5	[number]	k4	5
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
též	též	k6eAd1	též
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
David	David	k1gMnSc1	David
bude	být	k5eAaImBp3nS	být
závodit	závodit	k5eAaImF	závodit
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Marka	Marek	k1gMnSc2	Marek
Webbera	Webber	k1gMnSc2	Webber
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2007	[number]	k4	2007
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pomalém	pomalý	k2eAgInSc6d1	pomalý
startu	start	k1gInSc6	start
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
2007	[number]	k4	2007
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
David	David	k1gMnSc1	David
2	[number]	k4	2
dobré	dobrý	k2eAgInPc4d1	dobrý
závody	závod	k1gInPc4	závod
v	v	k7c4	v
Bahrajnu	Bahrajen	k2eAgFnSc4d1	Bahrajen
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
bodoval	bodovat	k5eAaImAgMnS	bodovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
body	bod	k1gInPc4	bod
ale	ale	k8xC	ale
přidal	přidat	k5eAaPmAgMnS	přidat
už	už	k6eAd1	už
jen	jen	k9	jen
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
získal	získat	k5eAaPmAgMnS	získat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
sezóně	sezóna	k1gFnSc6	sezóna
14	[number]	k4	14
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
to	ten	k3xDgNnSc1	ten
stačilo	stačit	k5eAaBmAgNnS	stačit
až	až	k9	až
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
Coulthardův	Coulthardův	k2eAgInSc1d1	Coulthardův
kontrakt	kontrakt	k1gInSc1	kontrakt
prodloužen	prodloužen	k2eAgInSc1d1	prodloužen
i	i	k9	i
na	na	k7c4	na
sezónu	sezóna	k1gFnSc4	sezóna
2008	[number]	k4	2008
a	a	k8xC	a
rozjede	rozjet	k5eAaPmIp3nS	rozjet
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
sezónu	sezóna	k1gFnSc4	sezóna
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
starými	starý	k2eAgMnPc7d1	starý
pány	pan	k1gMnPc7	pan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2008	[number]	k4	2008
====	====	k?	====
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2008	[number]	k4	2008
pro	pro	k7c4	pro
Davida	David	k1gMnSc4	David
nezačala	začít	k5eNaPmAgFnS	začít
vůbec	vůbec	k9	vůbec
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
incidentu	incident	k1gInSc2	incident
s	s	k7c7	s
Felipe	Felip	k1gInSc5	Felip
Massou	Massý	k2eAgFnSc4d1	Massý
<g/>
.	.	kIx.	.
</s>
<s>
Coulthard	Coulthard	k1gMnSc1	Coulthard
popřel	popřít	k5eAaPmAgMnS	popřít
svou	svůj	k3xOyFgFnSc4	svůj
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
od	od	k7c2	od
Massy	Massa	k1gFnSc2	Massa
omluvu	omluva	k1gFnSc4	omluva
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
neučinil	učinit	k5eNaPmAgMnS	učinit
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
ten	ten	k3xDgInSc1	ten
malý	malý	k2eAgInSc1d1	malý
spratek	spratek	k1gInSc1	spratek
dostat	dostat	k5eAaPmF	dostat
pěstí	pěstit	k5eAaImIp3nS	pěstit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
závodě	závod	k1gInSc6	závod
měl	mít	k5eAaImAgInS	mít
Coulthard	Coulthard	k1gInSc1	Coulthard
potíže	potíž	k1gFnSc2	potíž
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
nevýrazných	výrazný	k2eNgNnPc6d1	nevýrazné
umístěních	umístění	k1gNnPc6	umístění
přišel	přijít	k5eAaPmAgMnS	přijít
konečně	konečně	k6eAd1	konečně
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
boji	boj	k1gInSc6	boj
získal	získat	k5eAaPmAgInS	získat
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc7	jeho
2	[number]	k4	2
<g/>
.	.	kIx.	.
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
už	už	k6eAd1	už
ale	ale	k8xC	ale
vystoupal	vystoupat	k5eAaPmAgMnS	vystoupat
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
62	[number]	k4	62
<g/>
x.	x.	k?	x.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
před	před	k7c7	před
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
ukončí	ukončit	k5eAaPmIp3nS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
jezdeckou	jezdecký	k2eAgFnSc4d1	jezdecká
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
F1	F1	k1gFnSc6	F1
ale	ale	k8xC	ale
zůstane	zůstat	k5eAaPmIp3nS	zůstat
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
působit	působit	k5eAaImF	působit
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Red	Red	k1gMnPc1	Red
Bull	bulla	k1gFnPc2	bulla
jakou	jaký	k3yRgFnSc4	jaký
konzultant	konzultant	k1gMnSc1	konzultant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
a	a	k8xC	a
rekordy	rekord	k1gInPc1	rekord
===	===	k?	===
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
je	být	k5eAaImIp3nS	být
také	také	k9	také
bývalým	bývalý	k2eAgMnSc7d1	bývalý
nejlépe	dobře	k6eAd3	dobře
bodujícím	bodující	k2eAgMnSc7d1	bodující
Britem	Brit	k1gMnSc7	Brit
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
F1	F1	k1gFnSc2	F1
(	(	kIx(	(
<g/>
533	[number]	k4	533
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
překonal	překonat	k5eAaPmAgMnS	překonat
předchozí	předchozí	k2eAgInSc4d1	předchozí
rekord	rekord	k1gInSc4	rekord
Nigela	Nigel	k1gMnSc2	Nigel
Mansella	Mansell	k1gMnSc2	Mansell
(	(	kIx(	(
<g/>
482	[number]	k4	482
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Španělska	Španělsko	k1gNnSc2	Španělsko
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
osmý	osmý	k4xOgMnSc1	osmý
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
"	"	kIx"	"
<g/>
klubu	klub	k1gInSc2	klub
200	[number]	k4	200
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jezdci	jezdec	k1gMnPc1	jezdec
kteří	který	k3yIgMnPc1	který
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
přes	přes	k7c4	přes
200	[number]	k4	200
závodů	závod	k1gInPc2	závod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
se	se	k3xPyFc4	se
k	k	k7c3	k
Riccardovi	Riccard	k1gMnSc3	Riccard
Patresemu	Patresem	k1gMnSc3	Patresem
<g/>
,	,	kIx,	,
Michaelu	Michael	k1gMnSc3	Michael
Schumacherovi	Schumacher	k1gMnSc3	Schumacher
<g/>
,	,	kIx,	,
Rubensi	Rubens	k1gMnSc3	Rubens
Barrichellovi	Barrichell	k1gMnSc3	Barrichell
<g/>
,	,	kIx,	,
Gerhardu	Gerhard	k1gMnSc3	Gerhard
Bergerovi	Berger	k1gMnSc3	Berger
<g/>
,	,	kIx,	,
Andreovi	Andreus	k1gMnSc3	Andreus
de	de	k?	de
Cesariovi	Cesarius	k1gMnSc6	Cesarius
<g/>
,	,	kIx,	,
Nelsonu	Nelson	k1gMnSc6	Nelson
Piquetovi	Piquet	k1gMnSc6	Piquet
a	a	k8xC	a
Jeanu	Jean	k1gMnSc6	Jean
Alesimu	Alesim	k1gMnSc6	Alesim
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
2006	[number]	k4	2006
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
David	David	k1gMnSc1	David
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInPc4	první
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
pro	pro	k7c4	pro
stáj	stáj	k1gFnSc4	stáj
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
Racing	Racing	k1gInSc1	Racing
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dekorování	dekorování	k1gNnSc2	dekorování
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
na	na	k7c4	na
sobě	se	k3xPyFc3	se
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
propagoval	propagovat	k5eAaImAgInS	propagovat
nový	nový	k2eAgInSc4d1	nový
film	film	k1gInSc4	film
Superman	superman	k1gMnSc1	superman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Helma	helma	k1gFnSc1	helma
==	==	k?	==
</s>
</p>
<p>
<s>
Coulthardova	Coulthardův	k2eAgFnSc1d1	Coulthardova
helma	helma	k1gFnSc1	helma
je	být	k5eAaImIp3nS	být
modrá	modré	k1gNnPc4	modré
s	s	k7c7	s
Ondřejským	ondřejský	k2eAgInSc7d1	ondřejský
křížem	kříž	k1gInSc7	kříž
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
(	(	kIx(	(
<g/>
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
skotskou	skotský	k2eAgFnSc4d1	skotská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
)	)	kIx)	)
přecházející	přecházející	k2eAgInSc4d1	přecházející
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
brady	brada	k1gFnSc2	brada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
působení	působení	k1gNnSc4	působení
u	u	k7c2	u
McLarenu	McLaren	k1gInSc2	McLaren
měl	mít	k5eAaImAgInS	mít
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
helmě	helma	k1gFnSc6	helma
bílý	bílý	k2eAgInSc4d1	bílý
kruh	kruh	k1gInSc4	kruh
okolo	okolo	k6eAd1	okolo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Red	Red	k1gFnSc3	Red
Bullu	bulla	k1gFnSc4	bulla
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
tmavě	tmavě	k6eAd1	tmavě
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
uprostřed	uprostřed	k6eAd1	uprostřed
zvýrazňující	zvýrazňující	k2eAgNnSc4d1	zvýrazňující
logo	logo	k1gNnSc4	logo
Red	Red	k1gFnSc2	Red
Bullu	bulla	k1gFnSc4	bulla
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
2007	[number]	k4	2007
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
šedou	šedý	k2eAgFnSc4d1	šedá
helmu	helma	k1gFnSc4	helma
se	s	k7c7	s
stylizovaným	stylizovaný	k2eAgInSc7d1	stylizovaný
křížem	kříž	k1gInSc7	kříž
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
zesnulému	zesnulý	k2eAgMnSc3d1	zesnulý
Colinu	Colin	k2eAgMnSc3d1	Colin
McRaeovi	McRaeus	k1gMnSc3	McRaeus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
po	po	k7c6	po
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Závod	závod	k1gInSc1	závod
šampionů	šampion	k1gMnPc2	šampion
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
Závodě	závod	k1gInSc6	závod
šampionů	šampion	k1gMnPc2	šampion
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Coulthard	Coulthard	k1gMnSc1	Coulthard
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrál	prohrát	k5eAaPmAgInS	prohrát
jen	jen	k9	jen
se	s	k7c7	s
Sébastienem	Sébastien	k1gMnSc7	Sébastien
Loebem	Loeb	k1gMnSc7	Loeb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k8xS	až
2011	[number]	k4	2011
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
BBC	BBC	kA	BBC
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Coulthard	Coulthard	k1gInSc1	Coulthard
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
jako	jako	k8xS	jako
expert	expert	k1gMnSc1	expert
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgInS	stát
spolukomentátorem	spolukomentátor	k1gMnSc7	spolukomentátor
nejdříve	dříve	k6eAd3	dříve
společně	společně	k6eAd1	společně
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Brundlem	Brundlo	k1gNnSc7	Brundlo
a	a	k8xC	a
poté	poté	k6eAd1	poté
s	s	k7c7	s
Benem	Ben	k1gInSc7	Ben
Edwardsem	Edwards	k1gMnSc7	Edwards
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
záložním	záložní	k2eAgMnSc7d1	záložní
pilotem	pilot	k1gMnSc7	pilot
Red	Red	k1gFnSc2	Red
Bullu	bulla	k1gFnSc4	bulla
ve	v	k7c6	v
2	[number]	k4	2
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
a	a	k8xC	a
u	u	k7c2	u
týmu	tým	k1gInSc2	tým
působil	působit	k5eAaImAgMnS	působit
nadále	nadále	k6eAd1	nadále
jako	jako	k9	jako
konzultant	konzultant	k1gMnSc1	konzultant
a	a	k8xC	a
předváděcí	předváděcí	k2eAgMnSc1d1	předváděcí
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
závodech	závod	k1gInPc6	závod
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
regulérní	regulérní	k2eAgMnPc1d1	regulérní
rezervní	rezervní	k2eAgMnPc1d1	rezervní
jezdci	jezdec	k1gMnPc1	jezdec
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
a	a	k8xC	a
Toro	Toro	k?	Toro
Rosso	Rossa	k1gFnSc5	Rossa
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Ricciardo	Ricciardo	k1gNnSc4	Ricciardo
a	a	k8xC	a
Brendon	Brendon	k1gNnSc4	Brendon
Hartley	Hartlea	k1gFnSc2	Hartlea
<g/>
,	,	kIx,	,
závodili	závodit	k5eAaImAgMnP	závodit
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
sérii	série	k1gFnSc6	série
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Coulthard	Coulthard	k1gInSc1	Coulthard
opět	opět	k6eAd1	opět
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
záložního	záložní	k2eAgMnSc4d1	záložní
pilota	pilot	k1gMnSc4	pilot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
DTM	DTM	kA	DTM
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Coulthard	Coulthard	k1gMnSc1	Coulthard
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
sérii	série	k1gFnSc6	série
DTM	DTM	kA	DTM
<g/>
,	,	kIx,	,
u	u	k7c2	u
týmu	tým	k1gInSc2	tým
Mücke	Mücke	k1gInSc1	Mücke
Motorsport	Motorsport	k1gInSc1	Motorsport
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Mercedes	mercedes	k1gInSc1	mercedes
C-Class	C-Classa	k1gFnPc2	C-Classa
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Coulthard	Coulthard	k1gMnSc1	Coulthard
bude	být	k5eAaImBp3nS	být
závodit	závodit	k5eAaImF	závodit
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
vozem	vůz	k1gInSc7	vůz
i	i	k9	i
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
mu	on	k3xPp3gNnSc3	on
bude	být	k5eAaImBp3nS	být
Ralf	Ralf	k1gInSc4	Ralf
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
sezónách	sezóna	k1gFnPc6	sezóna
získal	získat	k5eAaPmAgInS	získat
pokaždé	pokaždé	k6eAd1	pokaždé
jen	jen	k9	jen
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
závody	závod	k1gInPc4	závod
sice	sice	k8xC	sice
pravidelně	pravidelně	k6eAd1	pravidelně
dojížděl	dojíždět	k5eAaImAgMnS	dojíždět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
nebodovaných	bodovaný	k2eNgFnPc6d1	nebodovaná
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třetí	třetí	k4xOgFnSc6	třetí
a	a	k8xC	a
poslední	poslední	k2eAgFnSc3d1	poslední
sezóně	sezóna	k1gFnSc3	sezóna
nasbíral	nasbírat	k5eAaPmAgInS	nasbírat
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
oznámil	oznámit	k5eAaPmAgInS	oznámit
ukončení	ukončení	k1gNnSc4	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
DTM	DTM	kA	DTM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Coulthardovi	Coulthard	k1gMnSc3	Coulthard
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
(	(	kIx(	(
<g/>
MBE	MBE	kA	MBE
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Souhrn	souhrn	k1gInSc1	souhrn
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
===	===	k?	===
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
–	–	k?	–
Pole	pole	k1gNnPc1	pole
positionKurzívou	positionKurzívat	k5eAaPmIp3nP	positionKurzívat
–	–	k?	–
Nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
závodu	závod	k1gInSc2	závod
</s>
</p>
<p>
<s>
†	†	k?	†
Nedojel	dojet	k5eNaPmAgMnS	dojet
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odjel	odjet	k5eAaPmAgMnS	odjet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
<g/>
%	%	kIx~	%
délky	délka	k1gFnSc2	délka
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
3000	[number]	k4	3000
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc1d1	kompletní
výsledky	výsledek	k1gInPc1	výsledek
24	[number]	k4	24
<g/>
h	h	k?	h
Le	Le	k1gFnSc2	Le
Mans	Mansa	k1gFnPc2	Mansa
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
série	série	k1gFnSc2	série
British	Britisha	k1gFnPc2	Britisha
Touring	Touring	k1gInSc1	Touring
Car	car	k1gMnSc1	car
Championship	Championship	k1gMnSc1	Championship
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
DTM	DTM	kA	DTM
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
David	David	k1gMnSc1	David
Coulthard	Coultharda	k1gFnPc2	Coultharda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Davida	David	k1gMnSc2	David
Coultharda	Coulthard	k1gMnSc2	Coulthard
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
BBC	BBC	kA	BBC
o	o	k7c6	o
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
(	(	kIx(	(
<g/>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
