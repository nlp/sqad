<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgFnSc2d1
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
petraea	petrae	k1gInSc2
<g/>
)	)	kIx)
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
bukotvaré	bukotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Fagales	Fagales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
bukovité	bukovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Fagaceae	Fagacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
dub	dub	k1gInSc1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Quercus	Quercus	k1gMnSc1
petraea	petraea	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Matt	Matt	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Liebl	Liebl	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1784	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gMnSc1
petraea	petraea	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Q.	Q.	kA
sessilis	sessilis	k1gFnPc1
<g/>
,	,	kIx,
Q.	Q.	kA
sessiliflora	sessiliflora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
zvaný	zvaný	k2eAgInSc4d1
též	též	k9
drnák	drnák	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
listnatý	listnatý	k2eAgInSc1d1
strom	strom	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
bukovitých	bukovití	k1gMnPc2
<g/>
;	;	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
dubem	dub	k1gInSc7
letním	letnit	k5eAaImIp1nS
lesnicky	lesnicky	k6eAd1
nejvýznamnější	významný	k2eAgFnSc1d3
listnatá	listnatý	k2eAgFnSc1d1
dřevina	dřevina	k1gFnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
po	po	k7c6
buku	buk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
dosahuje	dosahovat	k5eAaImIp3nS
výšky	výška	k1gFnSc2
20	#num#	k4
až	až	k9
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
štíhle	štíhle	k6eAd1
vejčitou	vejčitý	k2eAgFnSc4d1
nebo	nebo	k8xC
nepravidelnou	pravidelný	k2eNgFnSc4d1
<g/>
,	,	kIx,
i	i	k9
uvnitř	uvnitř	k6eAd1
olistěnou	olistěný	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
dubem	dub	k1gInSc7
letním	letní	k2eAgInSc7d1
dosahuje	dosahovat	k5eAaImIp3nS
menších	malý	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
(	(	kIx(
<g/>
i	i	k8xC
nižšího	nízký	k2eAgInSc2d2
věku	věk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letorosty	letorost	k1gInPc7
jsou	být	k5eAaImIp3nP
lysé	lysý	k2eAgInPc1d1
<g/>
,	,	kIx,
tmavě	tmavě	k6eAd1
olivově	olivově	k6eAd1
zelené	zelený	k2eAgInPc1d1
<g/>
,	,	kIx,
lenticely	lenticela	k1gFnPc1
řídké	řídký	k2eAgFnPc1d1
<g/>
,	,	kIx,
drobné	drobný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pupeny	pupen	k1gInPc1
vejcovité	vejcovitý	k2eAgInPc1d1
<g/>
,	,	kIx,
až	až	k9
8	#num#	k4
mm	mm	kA
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
oproti	oproti	k7c3
dubu	dub	k1gInSc3
letnímu	letní	k2eAgNnSc3d1
zřetelně	zřetelně	k6eAd1
řapíkaté	řapíkatý	k2eAgNnSc1d1
<g/>
;	;	kIx,
čepel	čepel	k1gFnSc1
většinou	většinou	k6eAd1
široce	široko	k6eAd1
obvejčitá	obvejčitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nejširší	široký	k2eAgFnSc1d3
v	v	k7c6
horní	horní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
<g/>
,	,	kIx,
až	až	k9
16	#num#	k4
cm	cm	kA
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
10	#num#	k4
cm	cm	kA
široká	široký	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c4
bázi	báze	k1gFnSc4
klínovitá	klínovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
řidčeji	řídce	k6eAd2
zaokrouhlená	zaokrouhlený	k2eAgFnSc1d1
<g/>
,	,	kIx,
nahoře	nahoře	k6eAd1
široce	široko	k6eAd1
zaokrouhlená	zaokrouhlený	k2eAgFnSc1d1
<g/>
,	,	kIx,
peřenolaločná	peřenolaločný	k2eAgFnSc1d1
až	až	k6eAd1
peřenodílná	peřenodílný	k2eAgFnSc1d1
s	s	k7c7
5	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
8	#num#	k4
(	(	kIx(
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
páry	pár	k1gInPc1
laloků	lalok	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
čepele	čepel	k1gFnSc2
s	s	k7c7
laloky	lalok	k1gInPc7
mnohem	mnohem	k6eAd1
mělčími	mělký	k2eAgInPc7d2
<g/>
,	,	kIx,
tuhá	tuhý	k2eAgNnPc1d1
<g/>
,	,	kIx,
poměrně	poměrně	k6eAd1
tenká	tenký	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
líci	líc	k1gFnSc6
lysá	lysat	k5eAaImIp3nS
a	a	k8xC
slabě	slabě	k6eAd1
lesklá	lesklý	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
rubu	rub	k1gInSc6
světlejší	světlý	k2eAgNnSc1d2
<g/>
,	,	kIx,
drobnými	drobné	k1gInPc7
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
ramennými	ramenný	k2eAgInPc7d1
chlupy	chlup	k1gInPc7
pýřitá	pýřitý	k2eAgFnSc1d1
<g/>
;	;	kIx,
bočních	boční	k2eAgFnPc2d1
žilek	žilka	k1gFnPc2
6	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
(	(	kIx(
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
)	)	kIx)
párů	pár	k1gInPc2
<g/>
,	,	kIx,
žilky	žilka	k1gFnSc2
třetího	třetí	k4xOgInSc2
řádu	řád	k1gInSc2
jen	jen	k9
slabě	slabě	k6eAd1
patrné	patrný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řapík	řapík	k1gInSc1
je	být	k5eAaImIp3nS
lysý	lysý	k2eAgMnSc1d1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
mm	mm	kA
dlouhý	dlouhý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Plody	plod	k1gInPc1
jsou	být	k5eAaImIp3nP
nažky	nažka	k1gFnPc4
(	(	kIx(
<g/>
žaludy	žalud	k1gInPc4
<g/>
)	)	kIx)
umístě	umístě	k6eAd1
v	v	k7c6
paždí	paždí	k1gNnSc6
listů	list	k1gInPc2
po	po	k7c6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
3	#num#	k4
(	(	kIx(
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
přisedlé	přisedlý	k2eAgInPc1d1
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
na	na	k7c6
stopkách	stopka	k1gFnPc6
do	do	k7c2
1,5	1,5	k4
cm	cm	kA
dlouhých	dlouhý	k2eAgInPc2d1
<g/>
,	,	kIx,
číška	číška	k1gFnSc1
tenkostěnná	tenkostěnný	k2eAgFnSc1d1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
12	#num#	k4
cm	cm	kA
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
široká	široký	k2eAgFnSc1d1
<g/>
,	,	kIx,
šupiny	šupina	k1gFnPc1
drobné	drobný	k2eAgFnPc1d1
<g/>
,	,	kIx,
vejčitě	vejčitě	k6eAd1
kopinaté	kopinatý	k2eAgInPc1d1
<g/>
,	,	kIx,
hustě	hustě	k6eAd1
pýřité	pýřitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ploché	plochý	k2eAgFnPc1d1
nebo	nebo	k8xC
jen	jen	k9
slabě	slabě	k6eAd1
vyklenuté	vyklenutý	k2eAgNnSc1d1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
hrbatě	hrbatě	k6eAd1
ztlustlé	ztlustlý	k2eAgInPc1d1
<g/>
;	;	kIx,
žaludy	žalud	k1gInPc1
podlouhlé	podlouhlý	k2eAgFnSc2d1
vejcovité	vejcovitý	k2eAgFnSc2d1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
mm	mm	kA
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
mm	mm	kA
v	v	k7c6
průměru	průměr	k1gInSc6
<g/>
,	,	kIx,
často	často	k6eAd1
klíčí	klíčit	k5eAaImIp3nS
už	už	k6eAd1
na	na	k7c6
stromě	strom	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žaludy	Žalud	k1gMnPc4
jsou	být	k5eAaImIp3nP
přisedlé	přisedlý	k2eAgFnPc1d1
k	k	k7c3
větvím	větev	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Silný	silný	k2eAgInSc1d1
kůlový	kůlový	k2eAgInSc1d1
kořen	kořen	k1gInSc1
není	být	k5eNaImIp3nS
vyvinut	vyvinout	k5eAaPmNgInS
<g/>
,	,	kIx,
proto	proto	k8xC
zřídka	zřídka	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
vývratům	vývrat	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
nadán	nadán	k2eAgInSc1d1
silnou	silný	k2eAgFnSc7d1
pařezovou	pařezový	k2eAgFnSc7d1
a	a	k8xC
kmenovou	kmenový	k2eAgFnSc7d1
výmladností	výmladnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekologické	ekologický	k2eAgInPc4d1
nároky	nárok	k1gInPc4
a	a	k8xC
stanoviště	stanoviště	k1gNnPc4
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
na	na	k7c6
židovském	židovský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Miroticích	Mirotice	k1gFnPc6
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
světlomilný	světlomilný	k2eAgMnSc1d1
a	a	k8xC
teplomilný	teplomilný	k2eAgMnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
poněkud	poněkud	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snese	snést	k5eAaPmIp3nS
oproti	oproti	k7c3
němu	on	k3xPp3gNnSc3
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
úživná	úživný	k2eAgNnPc4d1
stanoviště	stanoviště	k1gNnPc4
<g/>
,	,	kIx,
roste	růst	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
na	na	k7c6
chudých	chudý	k2eAgFnPc6d1
<g/>
,	,	kIx,
mělkých	mělký	k2eAgFnPc6d1
a	a	k8xC
kyselých	kyselý	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
propustných	propustný	k2eAgFnPc6d1
<g/>
,	,	kIx,
písčitých	písčitý	k2eAgFnPc6d1
nebo	nebo	k8xC
kamenitých	kamenitý	k2eAgFnPc6d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
ale	ale	k9
i	i	k9
na	na	k7c6
bohatších	bohatý	k2eAgNnPc6d2
stanovištích	stanoviště	k1gNnPc6
a	a	k8xC
na	na	k7c6
vápencích	vápenec	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesnáší	snášet	k5eNaImIp3nS
přemokření	přemokření	k1gNnSc2
<g/>
,	,	kIx,
záplavy	záplava	k1gFnSc2
a	a	k8xC
oglejené	oglejený	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
nížin	nížina	k1gFnPc2
po	po	k7c4
pahorkatiny	pahorkatina	k1gFnPc4
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
až	až	k9
do	do	k7c2
podhůří	podhůří	k1gNnSc2
<g/>
,	,	kIx,
maximálně	maximálně	k6eAd1
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
750-850	750-850	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Je	být	k5eAaImIp3nS
dominantní	dominantní	k2eAgFnSc7d1
dřevinou	dřevina	k1gFnSc7
v	v	k7c6
kyselých	kyselý	k2eAgFnPc6d1
doubravách	doubrava	k1gFnPc6
<g/>
,	,	kIx,
dominantou	dominanta	k1gFnSc7
či	či	k8xC
kodominantou	kodominanta	k1gFnSc7
v	v	k7c6
dubohabřinách	dubohabřina	k1gFnPc6
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
i	i	k9
příměs	příměs	k1gFnSc1
bučin	bučina	k1gFnPc2
a	a	k8xC
smíšených	smíšený	k2eAgInPc2d1
porostů	porost	k1gInPc2
s	s	k7c7
jedlí	jedle	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
teplejších	teplý	k2eAgFnPc6d2
oblastech	oblast	k1gFnPc6
bývá	bývat	k5eAaImIp3nS
místy	místy	k6eAd1
masivně	masivně	k6eAd1
napadán	napadat	k5eAaBmNgInS,k5eAaPmNgInS
poloparazitickým	poloparazitický	k2eAgInSc7d1
ochmetem	ochmet	k1gInSc7
evropským	evropský	k2eAgInSc7d1
<g/>
;	;	kIx,
na	na	k7c6
exponovanějších	exponovaný	k2eAgNnPc6d2
stanovištích	stanoviště	k1gNnPc6
trpí	trpět	k5eAaImIp3nP
silnými	silný	k2eAgInPc7d1
mrazy	mráz	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
vztahy	vztah	k1gInPc1
mykorhizy	mykorhiza	k1gFnSc2
se	se	k3xPyFc4
hřiby	hřib	k1gInPc7
dubovým	dubový	k2eAgMnSc7d1
<g/>
,	,	kIx,
královským	královský	k2eAgMnSc7d1
a	a	k8xC
kolodějem	koloděj	k1gMnSc7
<g/>
,	,	kIx,
s	s	k7c7
ryzcem	ryzec	k1gInSc7
dubovým	dubový	k2eAgInSc7d1
a	a	k8xC
zlatomléčným	zlatomléčný	k2eAgInSc7d1
<g/>
,	,	kIx,
s	s	k7c7
kozákem	kozák	k1gInSc7
dubovým	dubový	k2eAgInSc7d1
a	a	k8xC
taktéž	taktéž	k?
s	s	k7c7
muchomůrkou	muchomůrkou	k?
zelenou	zelená	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
rozšířen	rozšířen	k2eAgInSc1d1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc6d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
je	být	k5eAaImIp3nS
běžný	běžný	k2eAgInSc1d1
po	po	k7c6
61	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
zasahuje	zasahovat	k5eAaImIp3nS
po	po	k7c4
řeky	řeka	k1gFnPc4
Bug	Bug	k1gFnPc2
a	a	k8xC
Dněpr	Dněpr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izolované	izolovaný	k2eAgInPc1d1
areály	areál	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Krymu	Krym	k1gInSc6
a	a	k8xC
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
důležitý	důležitý	k2eAgInSc1d1
strom	strom	k1gInSc1
nižších	nízký	k2eAgFnPc2d2
poloh	poloha	k1gFnPc2
a	a	k8xC
pahorkatin	pahorkatina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
areálu	areál	k1gInSc2
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
hor.	hor.	k?
Vyhýbá	vyhýbat	k5eAaImIp3nS
se	se	k3xPyFc4
oblastem	oblast	k1gFnPc3
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
kontinentalitou	kontinentalita	k1gFnSc7
klimatu	klima	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
poskytuje	poskytovat	k5eAaImIp3nS
velice	velice	k6eAd1
hodnotné	hodnotný	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
<g/>
,	,	kIx,
používané	používaný	k2eAgNnSc1d1
při	při	k7c6
stavbě	stavba	k1gFnSc6
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
podlah	podlaha	k1gFnPc2
<g/>
,	,	kIx,
nábytku	nábytek	k1gInSc2
<g/>
,	,	kIx,
intarzií	intarzie	k1gFnPc2
i	i	k8xC
sudů	sud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
léčitelství	léčitelství	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
kůra	kůra	k1gFnSc1
z	z	k7c2
mladých	mladý	k2eAgInPc2d1
stromů	strom	k1gInPc2
jako	jako	k8xS,k8xC
svíravý	svíravý	k2eAgInSc1d1
a	a	k8xC
protikrvácivý	protikrvácivý	k2eAgInSc1d1
prostředek	prostředek	k1gInSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
protizánětlivé	protizánětlivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
na	na	k7c4
zanícenou	zanícený	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
a	a	k8xC
sliznici	sliznice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
velmi	velmi	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
koupelí	koupel	k1gFnPc2
z	z	k7c2
odvaru	odvar	k1gInSc2
z	z	k7c2
dubové	dubový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
léčbě	léčba	k1gFnSc3
hemoroidů	hemoroidy	k1gInPc2
<g/>
,	,	kIx,
proleženin	proleženina	k1gFnPc2
<g/>
,	,	kIx,
popálenin	popálenina	k1gFnPc2
<g/>
,	,	kIx,
omrzlin	omrzlina	k1gFnPc2
<g/>
,	,	kIx,
křečových	křečový	k2eAgFnPc2d1
žil	žíla	k1gFnPc2
<g/>
,	,	kIx,
plísní	plíseň	k1gFnPc2
nohou	noha	k1gFnPc2
<g/>
,	,	kIx,
ekzémů	ekzém	k1gInPc2
a	a	k8xC
mnoha	mnoho	k4c2
dalších	další	k2eAgInPc2d1
problémů	problém	k1gInPc2
s	s	k7c7
kůží	kůže	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
už	už	k6eAd1
zřídka	zřídka	k6eAd1
pije	pít	k5eAaImIp3nS
odvar	odvar	k1gInSc4
při	při	k7c6
průjmech	průjem	k1gInPc6
<g/>
,	,	kIx,
žaludečních	žaludeční	k2eAgInPc6d1
a	a	k8xC
střevních	střevní	k2eAgInPc6d1
katarech	katar	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Památné	památný	k2eAgInPc1d1
a	a	k8xC
významné	významný	k2eAgInPc1d1
duby	dub	k1gInPc1
zimní	zimní	k2eAgInPc1d1
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
evidováno	evidovat	k5eAaImNgNnS
64	#num#	k4
položek	položka	k1gFnPc2
(	(	kIx(
<g/>
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
skupin	skupina	k1gFnPc2
a	a	k8xC
alejí	alej	k1gFnPc2
<g/>
)	)	kIx)
dubů	dub	k1gInPc2
zimních	zimní	k2eAgInPc2d1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
21	#num#	k4
<g/>
×	×	k?
méně	málo	k6eAd2
oproti	oproti	k7c3
dubům	dub	k1gInPc3
letním	letnit	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
obvodem	obvod	k1gInSc7
nejmohutnější	mohutný	k2eAgNnSc4d3
a	a	k8xC
patrně	patrně	k6eAd1
i	i	k9
nejstarší	starý	k2eAgInPc1d3
popsané	popsaný	k2eAgInPc1d1
duby	dub	k1gInPc1
zimní	zimní	k2eAgInPc1d1
na	na	k7c6
našem	náš	k3xOp1gNnSc6
území	území	k1gNnSc6
zanikly	zaniknout	k5eAaPmAgFnP
přičiněním	přičinění	k1gNnSc7
člověka	člověk	k1gMnSc2
<g/>
:	:	kIx,
Albrechtický	albrechtický	k2eAgInSc1d1
dub	dub	k1gInSc1
dosahoval	dosahovat	k5eAaImAgInS
obvodu	obvod	k1gInSc2
1236	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
ve	v	k7c6
výčetní	výčetní	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
stáří	stáří	k1gNnSc1
bylo	být	k5eAaImAgNnS
odhadované	odhadovaný	k2eAgNnSc1d1
na	na	k7c6
800	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanikl	zaniknout	k5eAaPmAgMnS
po	po	k7c6
cíleném	cílený	k2eAgNnSc6d1
opakovaném	opakovaný	k2eAgNnSc6d1
zapálení	zapálení	k1gNnSc6
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ctiborův	Ctiborův	k2eAgInSc1d1
dub	dub	k1gInSc1
o	o	k7c6
obvodu	obvod	k1gInSc6
900	#num#	k4
cm	cm	kA
zanikl	zaniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
zánik	zánik	k1gInSc4
uspíšilo	uspíšit	k5eAaPmAgNnS
podpálení	podpálení	k1gNnPc2
dětmi	dítě	k1gFnPc7
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíciletý	tisíciletý	k2eAgInSc1d1
dub	dub	k1gInSc1
v	v	k7c6
Ahníkově	Ahníkův	k2eAgFnSc6d1
dosáhl	dosáhnout	k5eAaPmAgInS
obvodu	obvod	k1gInSc2
798	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
ve	v	k7c4
30	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
660	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
ve	v	k7c4
130	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
zanikl	zaniknout	k5eAaPmAgInS
po	po	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
z	z	k7c2
důvodu	důvod	k1gInSc2
rozšíření	rozšíření	k1gNnSc4
hnědouhelného	hnědouhelný	k2eAgInSc2d1
lomu	lom	k1gInSc2
Nástup	nástup	k1gInSc1
–	–	k?
Tušimice	Tušimika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmohutnější	mohutný	k2eAgInSc1d3
žijící	žijící	k2eAgInSc1d1
památný	památný	k2eAgInSc1d1
dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
s	s	k7c7
obvodem	obvod	k1gInSc7
660	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
Czernínských	Czernínský	k2eAgFnPc2d1
dub	dub	k1gInSc1
v	v	k7c6
Sedlické	sedlický	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
na	na	k7c6
Blatensku	Blatensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Žižkův	Žižkův	k2eAgInSc1d1
dub	dub	k1gInSc1
(	(	kIx(
<g/>
Lomnice	Lomnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Valdštejnův	Valdštejnův	k2eAgInSc1d1
dub	dub	k1gInSc1
(	(	kIx(
<g/>
Lukov	Lukov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dub	dub	k1gInSc1
na	na	k7c6
Beraníku	Beraník	k1gInSc6
</s>
<s>
Dub	dub	k1gInSc1
na	na	k7c6
rozcestí	rozcestí	k1gNnSc6
</s>
<s>
Chocenický	Chocenický	k2eAgInSc1d1
drnák	drnák	k1gInSc1
</s>
<s>
Libenovský	Libenovský	k2eAgInSc1d1
dub	dub	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
MÖLLEROVÁ	MÖLLEROVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesnická	lesnický	k2eAgFnSc1d1
dendrologie	dendrologie	k1gFnSc1
2	#num#	k4
<g/>
ː	ː	k?
Listnaté	listnatý	k2eAgFnSc2d1
dřeviny	dřevina	k1gFnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Koblížek	koblížek	k1gInSc1
J.	J.	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Quercus	Quercus	k1gMnSc1
L.	L.	kA
In	In	k1gMnSc1
<g/>
:	:	kIx,
Hejný	Hejný	k2eAgMnSc1d1
S.	S.	kA
<g/>
,	,	kIx,
Slavík	Slavík	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Květena	květena	k1gFnSc1
ČR	ČR	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
p.	p.	k?
21	#num#	k4
–	–	k?
35	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dubová	dubový	k2eAgFnSc1d1
kůra	kůra	k1gFnSc1
na	na	k7c4
hemeroidy	hemeroidy	k?
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
obtíže	obtíž	k1gFnPc4
|	|	kIx~
Hemeroidy-hemoroidy	Hemeroidy-hemoroida	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hemeroidy-hemoroidy	Hemeroidy-hemoroida	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
seznam	seznam	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
ÚSOP	ÚSOP	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Koblížek	Koblížek	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Quercus	Quercus	k1gMnSc1
L.	L.	kA
In	In	k1gMnSc1
<g/>
:	:	kIx,
Hejný	Hejný	k2eAgMnSc1d1
S.	S.	kA
<g/>
,	,	kIx,
Slavík	Slavík	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Květena	květena	k1gFnSc1
ČR	ČR	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
p.	p.	k?
21	#num#	k4
–	–	k?
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
dub	dub	k1gInSc1
zimní	zimní	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Quercus	Quercus	k1gMnSc1
petraea	petraea	k1gMnSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Podzámecké	podzámecký	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
</s>
<s>
Dub	dub	k1gInSc1
jako	jako	k8xC,k8xS
léčivka	léčivka	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
|	|	kIx~
Stromy	strom	k1gInPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4128389-2	4128389-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85040050	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85040050	#num#	k4
</s>
