<s desamb="1">
Q.	Q.	kA
sessilis	sessilis	k1gFnPc1
<g/>
,	,	kIx,
Q.	Q.	kA
sessiliflora	sessiliflora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
zvaný	zvaný	k2eAgInSc4d1
též	též	k9
drnák	drnák	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
listnatý	listnatý	k2eAgInSc1d1
strom	strom	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
bukovitých	bukovití	k1gMnPc2
<g/>
;	;	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
dubem	dub	k1gInSc7
letním	letnit	k5eAaImIp1nS
lesnicky	lesnicky	k6eAd1
nejvýznamnější	významný	k2eAgFnSc1d3
listnatá	listnatý	k2eAgFnSc1d1
dřevina	dřevina	k1gFnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
po	po	k7c6
buku	buk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gMnSc1
petraea	petraea	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>