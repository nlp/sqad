<p>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
jsou	být	k5eAaImIp3nP	být
svobodné	svobodný	k2eAgFnPc1d1	svobodná
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
se	se	k3xPyFc4	se
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
probíhají	probíhat	k5eAaImIp3nP	probíhat
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
poměrným	poměrný	k2eAgInSc7d1	poměrný
volebním	volební	k2eAgInSc7d1	volební
systémem	systém	k1gInSc7	systém
voleno	volen	k2eAgNnSc4d1	voleno
100	[number]	k4	100
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dominantní	dominantní	k2eAgFnSc2d1	dominantní
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
"	"	kIx"	"
<g/>
Shoda	shoda	k1gFnSc1	shoda
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Jednota	jednota	k1gFnSc1	jednota
</s>
</p>
<p>
<s>
Svazu	svaz	k1gInSc3	svaz
Zelených	Zelený	k1gMnPc2	Zelený
a	a	k8xC	a
Rolníků	rolník	k1gMnPc2	rolník
</s>
</p>
<p>
<s>
Nacionální	nacionální	k2eAgNnSc1d1	nacionální
sdružení	sdružení	k1gNnSc1	sdružení
</s>
</p>
<p>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
regionů	region	k1gInPc2	region
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
pro	pro	k7c4	pro
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
