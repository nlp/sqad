<s>
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnPc2	majalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
chřestovité	chřestovitý	k2eAgFnSc2d1	chřestovitý
(	(	kIx(	(
<g/>
Asparagaceae	Asparagacea	k1gFnSc2	Asparagacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
taxonomické	taxonomický	k2eAgInPc1d1	taxonomický
systémy	systém	k1gInPc1	systém
ji	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
řadily	řadit	k5eAaImAgFnP	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
listnatcovité	listnatcovitý	k2eAgFnSc2d1	listnatcovitý
(	(	kIx(	(
<g/>
Ruscaceae	Ruscacea	k1gFnSc2	Ruscacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konvalinkovité	konvalinkovitý	k2eAgNnSc4d1	konvalinkovitý
(	(	kIx(	(
<g/>
Convallariaceae	Convallariaceae	k1gNnSc4	Convallariaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
liliovité	liliovitý	k2eAgFnPc4d1	liliovitá
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
(	(	kIx(	(
<g/>
Liliaceae	Liliaceae	k1gNnSc1	Liliaceae
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
vyjma	vyjma	k7c2	vyjma
nejsevernějších	severní	k2eAgFnPc2d3	nejsevernější
a	a	k8xC	a
nejjižnějších	jižní	k2eAgFnPc2d3	nejjižnější
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
odpovídajících	odpovídající	k2eAgFnPc6d1	odpovídající
oblastech	oblast	k1gFnPc6	oblast
Asie	Asie	k1gFnSc2	Asie
až	až	k9	až
po	po	k7c4	po
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
leckde	leckde	k6eAd1	leckde
mimo	mimo	k7c4	mimo
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
konvalinka	konvalinka	k1gFnSc1	konvalinka
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnSc2	Convallarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
autory	autor	k1gMnPc4	autor
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
monotypický	monotypický	k2eAgInSc4d1	monotypický
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
rozlišovány	rozlišován	k2eAgFnPc1d1	rozlišována
3-4	[number]	k4	3-4
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
domácí	domácí	k2eAgNnSc1d1	domácí
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnSc2	majalis
var.	var.	k?	var.
majalis	majalis	k1gFnSc2	majalis
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnSc2	majalis
s.	s.	k?	s.
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
zdomácnělá	zdomácnělý	k2eAgFnSc1d1	zdomácnělá
i	i	k9	i
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnSc1d1	původní
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnSc2	majalis
var.	var.	k?	var.
montana	montan	k1gMnSc2	montan
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnSc2	Convallarium
montana	montana	k1gFnSc1	montana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
roste	růst	k5eAaImIp3nS	růst
Convallaria	Convallarium	k1gNnSc2	Convallarium
majalis	majalis	k1gFnPc2	majalis
var.	var.	k?	var.
keiskei	keiskei	k1gNnSc4	keiskei
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnSc2	Convallarium
keiskei	keiske	k1gFnSc2	keiske
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snad	snad	k9	snad
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnSc2	majalis
var.	var.	k?	var.
manschurica	manschurica	k1gMnSc1	manschurica
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
taxon	taxon	k1gInSc1	taxon
Convallaria	Convallarium	k1gNnSc2	Convallarium
transcaucasica	transcaucasic	k1gInSc2	transcaucasic
Utkin	Utkina	k1gFnPc2	Utkina
ex	ex	k6eAd1	ex
Grossh	Grossha	k1gFnPc2	Grossha
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bude	být	k5eAaImBp3nS	být
asi	asi	k9	asi
další	další	k2eAgMnSc1d1	další
velmi	velmi	k6eAd1	velmi
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
po	po	k7c6	po
případě	případ	k1gInSc6	případ
vnitrodruhový	vnitrodruhový	k2eAgInSc4d1	vnitrodruhový
taxon	taxon	k1gInSc4	taxon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
hodnota	hodnota	k1gFnSc1	hodnota
však	však	k9	však
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
plazivým	plazivý	k2eAgInSc7d1	plazivý
větveným	větvený	k2eAgInSc7d1	větvený
oddenkem	oddenek	k1gInSc7	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
zpravidla	zpravidla	k6eAd1	zpravidla
dva	dva	k4xCgInPc1	dva
(	(	kIx(	(
<g/>
vzácně	vzácně	k6eAd1	vzácně
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
s	s	k7c7	s
pochvovitými	pochvovitý	k2eAgFnPc7d1	pochvovitý
šupinami	šupina	k1gFnPc7	šupina
<g/>
,	,	kIx,	,
elipsovitě	elipsovitě	k6eAd1	elipsovitě
šupinaté	šupinatý	k2eAgInPc1d1	šupinatý
<g/>
,	,	kIx,	,
celokrajné	celokrajný	k2eAgInPc1d1	celokrajný
a	a	k8xC	a
lysé	lysý	k2eAgInPc1d1	lysý
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
v	v	k7c6	v
řídkém	řídký	k2eAgInSc6d1	řídký
jednostranném	jednostranný	k2eAgInSc6d1	jednostranný
hroznu	hrozen	k1gInSc6	hrozen
neseném	nesený	k2eAgInSc6d1	nesený
na	na	k7c6	na
přímém	přímý	k2eAgInSc6d1	přímý
stvolu	stvol	k1gInSc6	stvol
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
krátce	krátce	k6eAd1	krátce
stopkaté	stopkatý	k2eAgFnPc1d1	stopkatá
<g/>
,	,	kIx,	,
nicí	nicí	k?	nicí
a	a	k8xC	a
vonné	vonný	k2eAgFnPc1d1	vonná
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
nažloutle	nažloutle	k6eAd1	nažloutle
zarůžovělé	zarůžovělý	k2eAgNnSc1d1	zarůžovělé
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
6	[number]	k4	6
srostlých	srostlý	k2eAgInPc2d1	srostlý
okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kulatě	kulatě	k6eAd1	kulatě
zvonkovité	zvonkovitý	k2eAgNnSc1d1	zvonkovité
<g/>
,	,	kIx,	,
s	s	k7c7	s
šesti	šest	k4xCc7	šest
odstálými	odstálý	k2eAgInPc7d1	odstálý
až	až	k8xS	až
ohnutými	ohnutý	k2eAgInPc7d1	ohnutý
cípy	cíp	k1gInPc7	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
přeslenech	přeslen	k1gInPc6	přeslen
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gyneceum	gyneceum	k1gNnSc1	gyneceum
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
ze	z	k7c2	z
3	[number]	k4	3
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
synkarpní	synkarpnit	k5eAaPmIp3nP	synkarpnit
<g/>
,	,	kIx,	,
semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
svrchní	svrchní	k2eAgInSc1d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
jasně	jasně	k6eAd1	jasně
červené	červený	k2eAgFnPc1d1	červená
2-6	[number]	k4	2-6
semenné	semenný	k2eAgFnPc1d1	semenná
bobule	bobule	k1gFnPc1	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rostlinu	rostlina	k1gFnSc4	rostlina
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
lesích	les	k1gInPc6	les
a	a	k8xC	a
křovinách	křovina	k1gFnPc6	křovina
a	a	k8xC	a
na	na	k7c6	na
horských	horský	k2eAgFnPc6d1	horská
loukách	louka	k1gFnPc6	louka
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
kyselé	kyselý	k2eAgFnSc3d1	kyselá
a	a	k8xC	a
kypré	kyprý	k2eAgFnSc2d1	kyprá
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
vápnitých	vápnitý	k2eAgFnPc6d1	vápnitá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
MŽP	MŽP	kA	MŽP
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
nemá	mít	k5eNaImIp3nS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
glykosidy	glykosid	k1gInPc4	glykosid
konvalatoxin	konvalatoxina	k1gFnPc2	konvalatoxina
<g/>
,	,	kIx,	,
konvalatoxol	konvalatoxola	k1gFnPc2	konvalatoxola
<g/>
,	,	kIx,	,
konvalatoxosid	konvalatoxosida	k1gFnPc2	konvalatoxosida
a	a	k8xC	a
majalosid	majalosid	k1gInSc4	majalosid
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
žvýkáním	žvýkání	k1gNnSc7	žvýkání
stonku	stonek	k1gInSc2	stonek
či	či	k8xC	či
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pojídáním	pojídání	k1gNnSc7	pojídání
červených	červený	k2eAgInPc2d1	červený
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Otrávit	otrávit	k5eAaPmF	otrávit
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
i	i	k9	i
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
vázy	váza	k1gFnSc2	váza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byly	být	k5eAaImAgFnP	být
konvalinky	konvalinka	k1gFnPc1	konvalinka
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
jsou	být	k5eAaImIp3nP	být
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
zažívací	zažívací	k2eAgFnPc4d1	zažívací
potíže	potíž	k1gFnPc4	potíž
(	(	kIx(	(
<g/>
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
močením	močení	k1gNnSc7	močení
a	a	k8xC	a
omámenost	omámenost	k1gFnSc1	omámenost
<g/>
,	,	kIx,	,
závratěmi	závrať	k1gFnPc7	závrať
nebo	nebo	k8xC	nebo
křečemi	křeč	k1gFnPc7	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
plodů	plod	k1gInPc2	plod
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyhledat	vyhledat	k5eAaPmF	vyhledat
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
srdeční	srdeční	k2eAgFnPc4d1	srdeční
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	s	k7c7	s
slabším	slabý	k2eAgNnSc7d2	slabší
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnPc2	majalis
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dužnaté	dužnatý	k2eAgInPc1d1	dužnatý
oddenky	oddenek	k1gInPc1	oddenek
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
vhodných	vhodný	k2eAgFnPc6d1	vhodná
podmínkách	podmínka	k1gFnPc6	podmínka
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Nejraději	rád	k6eAd3	rád
má	mít	k5eAaImIp3nS	mít
kyselejší	kyselý	k2eAgFnPc4d2	kyselejší
hlinitopísčité	hlinitopísčitý	k2eAgFnPc4d1	hlinitopísčitá
půdy	půda	k1gFnPc4	půda
v	v	k7c6	v
polostínu	polostín	k1gInSc6	polostín
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
dělením	dělení	k1gNnSc7	dělení
trsů	trs	k1gInPc2	trs
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
vypadá	vypadat	k5eAaPmIp3nS	vypadat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušný	příslušný	k2eAgInSc4d1	příslušný
trs	trs	k1gInSc4	trs
opatrně	opatrně	k6eAd1	opatrně
vyryjeme	vyrýt	k5eAaPmIp1nP	vyrýt
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
rukama	ruka	k1gFnPc7	ruka
rozlámeme	rozlámat	k5eAaPmIp1nP	rozlámat
na	na	k7c4	na
několik	několik	k4yIc4	několik
dílů	díl	k1gInPc2	díl
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgInSc1	každý
konec	konec	k1gInSc1	konec
oddenku	oddenek	k1gInSc2	oddenek
(	(	kIx(	(
<g/>
puk	puk	k1gInSc1	puk
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
kořínků	kořínek	k1gInPc2	kořínek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
zasázíme	zasázet	k5eAaPmIp1nP	zasázet
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgInSc1d3	nejvhodnější
termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
včasný	včasný	k2eAgInSc4d1	včasný
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
i	i	k8xC	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vydrží	vydržet	k5eAaPmIp3nS	vydržet
bezmrazé	bezmrazý	k2eAgNnSc4d1	bezmrazé
a	a	k8xC	a
vlhké	vlhký	k2eAgNnSc4d1	vlhké
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
zvolit	zvolit	k5eAaPmF	zvolit
časné	časný	k2eAgNnSc4d1	časné
jaro	jaro	k1gNnSc4	jaro
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
-	-	kIx~	-
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bývá	bývat	k5eAaImIp3nS	bývat
zem	zem	k1gFnSc1	zem
dostatečně	dostatečně	k6eAd1	dostatečně
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
.	.	kIx.	.
</s>
<s>
Konvalinka	konvalinka	k1gFnSc1	konvalinka
je	být	k5eAaImIp3nS	být
sbírána	sbírán	k2eAgFnSc1d1	sbírána
jako	jako	k8xS	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
jako	jako	k8xC	jako
slabší	slabý	k2eAgFnSc1d2	slabší
alternativa	alternativa	k1gFnSc1	alternativa
náprstníku	náprstník	k1gInSc2	náprstník
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
či	či	k8xC	či
proti	proti	k7c3	proti
vodnatelnosti	vodnatelnost	k1gFnSc3	vodnatelnost
a	a	k8xC	a
při	při	k7c6	při
epilepsii	epilepsie	k1gFnSc6	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
toxicitě	toxicita	k1gFnSc3	toxicita
není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
používání	používání	k1gNnSc1	používání
amatérskými	amatérský	k2eAgMnPc7d1	amatérský
léčiteli	léčitel	k1gMnPc7	léčitel
<g/>
.	.	kIx.	.
</s>
<s>
Konvalinka	konvalinka	k1gFnSc1	konvalinka
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mýdel	mýdlo	k1gNnPc2	mýdlo
<g/>
,	,	kIx,	,
voňavek	voňavka	k1gFnPc2	voňavka
a	a	k8xC	a
šňupacích	šňupací	k2eAgInPc2d1	šňupací
tabáků	tabák	k1gInPc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
běžné	běžný	k2eAgFnSc2d1	běžná
divoké	divoký	k2eAgFnSc2d1	divoká
formy	forma	k1gFnSc2	forma
byly	být	k5eAaImAgFnP	být
vyšlechtěny	vyšlechtěn	k2eAgFnPc1d1	vyšlechtěna
mnohé	mnohý	k2eAgFnPc1d1	mnohá
různobarevné	různobarevný	k2eAgFnPc1d1	různobarevná
odrůdy	odrůda	k1gFnPc1	odrůda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Květoslav	Květoslava	k1gFnPc2	Květoslava
Hísek	Hísky	k1gFnPc2	Hísky
<g/>
:	:	kIx,	:
Naší	náš	k3xOp1gFnSc7	náš
přírodou	příroda	k1gFnSc7	příroda
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
102	[number]	k4	102
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Horst	Horst	k1gMnSc1	Horst
Altmann	Altmann	k1gMnSc1	Altmann
<g/>
:	:	kIx,	:
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
1156	[number]	k4	1156
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Květena	květena	k1gFnSc1	květena
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
8	[number]	k4	8
nebo	nebo	k8xC	nebo
9	[number]	k4	9
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
vyjde	vyjít	k5eAaPmIp3nS	vyjít
snad	snad	k9	snad
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
ke	k	k7c3	k
Květeně	květena	k1gFnSc3	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Kubát	Kubát	k1gMnSc1	Kubát
K.	K.	kA	K.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Nová	nový	k2eAgFnSc1d1	nová
Květena	květena	k1gFnSc1	květena
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Dostál	Dostál	k1gMnSc1	Dostál
J.	J.	kA	J.
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
na	na	k7c4	na
www.botanika.wendys.cz	www.botanika.wendys.cz	k1gInSc4	www.botanika.wendys.cz
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
na	na	k7c6	na
biolibu	biolib	k1gInSc6	biolib
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
na	na	k7c4	na
garten	garten	k2eAgInSc4d1	garten
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
)	)	kIx)	)
Flóra	Flóra	k1gFnSc1	Flóra
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
Konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
na	na	k7c4	na
prirodakarlovarska	prirodakarlovarsek	k1gMnSc4	prirodakarlovarsek
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
