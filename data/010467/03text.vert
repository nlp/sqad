<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Váňa	Váňa	k1gMnSc1	Váňa
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
stolní	stolní	k2eAgMnSc1d1	stolní
tenista	tenista	k1gMnSc1	tenista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Váňa	Váňa	k1gMnSc1	Váňa
získal	získat	k5eAaPmAgMnS	získat
celkem	celkem	k6eAd1	celkem
třicet	třicet	k4xCc4	třicet
medailí	medaile	k1gFnPc2	medaile
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
třináct	třináct	k4xCc1	třináct
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
tabulkách	tabulka	k1gFnPc6	tabulka
za	za	k7c7	za
Viktorem	Viktor	k1gMnSc7	Viktor
Barnou	Barna	k1gMnSc7	Barna
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
světová	světový	k2eAgFnSc1d1	světová
špička	špička	k1gFnSc1	špička
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyrovnanější	vyrovnaný	k2eAgInSc1d2	vyrovnanější
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
překonán	překonat	k5eAaPmNgInS	překonat
někým	někdo	k3yInSc7	někdo
dalším	další	k2eAgNnSc7d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
medailí	medaile	k1gFnPc2	medaile
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mnohem	mnohem	k6eAd1	mnohem
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
přišla	přijít	k5eAaPmAgFnS	přijít
vynucená	vynucený	k2eAgFnSc1d1	vynucená
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
přestávka	přestávka	k1gFnSc1	přestávka
způsobená	způsobený	k2eAgFnSc1d1	způsobená
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
jedinečném	jedinečný	k2eAgNnSc6d1	jedinečné
postavení	postavení	k1gNnSc6	postavení
mezi	mezi	k7c7	mezi
světovou	světový	k2eAgFnSc7d1	světová
špičkou	špička	k1gFnSc7	špička
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
svých	svůj	k3xOyFgInPc2	svůj
titulů	titul	k1gInPc2	titul
získal	získat	k5eAaPmAgInS	získat
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
část	část	k1gFnSc1	část
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
z	z	k7c2	z
přehledu	přehled	k1gInSc2	přehled
na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Váňa	Váňa	k1gMnSc1	Váňa
se	se	k3xPyFc4	se
ke	k	k7c3	k
stolnímu	stolní	k2eAgInSc3d1	stolní
tenisu	tenis	k1gInSc3	tenis
dostal	dostat	k5eAaPmAgMnS	dostat
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
hraním	hraní	k1gNnSc7	hraní
po	po	k7c6	po
sálech	sál	k1gInPc6	sál
hospod	hospod	k?	hospod
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
talent	talent	k1gInSc1	talent
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
rozpoznán	rozpoznán	k2eAgMnSc1d1	rozpoznán
a	a	k8xC	a
Váňa	Váňa	k1gMnSc1	Váňa
začal	začít	k5eAaPmAgMnS	začít
docházet	docházet	k5eAaImF	docházet
na	na	k7c4	na
tréninky	trénink	k1gInPc4	trénink
do	do	k7c2	do
herny	herna	k1gFnSc2	herna
Sparty	Sparta	k1gFnSc2	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
spoluhráčů	spoluhráč	k1gMnPc2	spoluhráč
trénoval	trénovat	k5eAaImAgMnS	trénovat
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
neuvěřitelné	uvěřitelný	k2eNgFnPc1d1	neuvěřitelná
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
denně	denně	k6eAd1	denně
-	-	kIx~	-
pokud	pokud	k8xS	pokud
neměl	mít	k5eNaImAgMnS	mít
s	s	k7c7	s
kým	kdo	k3yInSc7	kdo
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
často	často	k6eAd1	často
sám	sám	k3xTgInSc4	sám
o	o	k7c4	o
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
porazil	porazit	k5eAaPmAgMnS	porazit
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
pětinásobného	pětinásobný	k2eAgMnSc2d1	pětinásobný
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
Viktora	Viktor	k1gMnSc4	Viktor
Barnu	Barna	k1gMnSc4	Barna
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
Richarda	Richard	k1gMnSc2	Richard
Bergmanna	Bergmann	k1gMnSc2	Bergmann
<g/>
.	.	kIx.	.
</s>
<s>
Překvapil	překvapit	k5eAaPmAgInS	překvapit
tehdy	tehdy	k6eAd1	tehdy
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
velice	velice	k6eAd1	velice
útočným	útočný	k2eAgNnSc7d1	útočné
pojetím	pojetí	k1gNnSc7	pojetí
s	s	k7c7	s
forehendovým	forehendový	k2eAgInSc7d1	forehendový
drivem	drive	k1gInSc7	drive
jako	jako	k8xC	jako
hlavním	hlavní	k2eAgInSc7d1	hlavní
úderem	úder	k1gInSc7	úder
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
pálek	pálka	k1gFnPc2	pálka
s	s	k7c7	s
obyčejnou	obyčejný	k2eAgFnSc7d1	obyčejná
vroubkovanou	vroubkovaný	k2eAgFnSc7d1	vroubkovaná
gumou	guma	k1gFnSc7	guma
bez	bez	k7c2	bez
houby	houba	k1gFnSc2	houba
poměrně	poměrně	k6eAd1	poměrně
nezvyklá	zvyklý	k2eNgFnSc1d1	nezvyklá
taktika	taktika	k1gFnSc1	taktika
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
slibně	slibně	k6eAd1	slibně
vyhlížející	vyhlížející	k2eAgFnSc1d1	vyhlížející
kariéra	kariéra	k1gFnSc1	kariéra
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
přerušena	přerušit	k5eAaPmNgFnS	přerušit
válkou	válka	k1gFnSc7	válka
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
právě	právě	k9	právě
od	od	k7c2	od
Barny	Barna	k1gMnSc2	Barna
a	a	k8xC	a
Bergmanna	Bergmann	k1gMnSc2	Bergmann
nevyužil	využít	k5eNaPmAgMnS	využít
možnosti	možnost	k1gFnPc4	možnost
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c4	v
hraní	hraní	k1gNnSc4	hraní
na	na	k7c6	na
vrcholové	vrcholový	k2eAgFnSc6d1	vrcholová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstal	zůstat	k5eAaPmAgMnS	zůstat
doma	doma	k6eAd1	doma
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
a	a	k8xC	a
vyhrával	vyhrávat	k5eAaImAgMnS	vyhrávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začínaly	začínat	k5eAaImAgFnP	začínat
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
amatérský	amatérský	k2eAgInSc1d1	amatérský
sport	sport	k1gInSc1	sport
a	a	k8xC	a
Váňa	Váňa	k1gMnSc1	Váňa
byl	být	k5eAaImAgMnS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
vydělávat	vydělávat	k5eAaImF	vydělávat
si	se	k3xPyFc3	se
na	na	k7c6	na
živobytí	živobytí	k1gNnSc6	živobytí
hraním	hraní	k1gNnSc7	hraní
polooficiálních	polooficiální	k2eAgInPc2d1	polooficiální
turnajů	turnaj	k1gInPc2	turnaj
po	po	k7c6	po
hospodách	hospodách	k?	hospodách
a	a	k8xC	a
exhibicemi	exhibice	k1gFnPc7	exhibice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
zřízení	zřízení	k1gNnSc6	zřízení
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
Váňa	Váňa	k1gMnSc1	Váňa
pracoval	pracovat	k5eAaImAgMnS	pracovat
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xC	jako
kopáč	kopáč	k1gInSc4	kopáč
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
Letenského	letenský	k2eAgInSc2d1	letenský
tunelu	tunel	k1gInSc2	tunel
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
jako	jako	k9	jako
topič	topič	k1gMnSc1	topič
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začala	začít	k5eAaPmAgFnS	začít
jít	jít	k5eAaImF	jít
jeho	jeho	k3xOp3gFnSc1	jeho
výkonnost	výkonnost	k1gFnSc1	výkonnost
postupně	postupně	k6eAd1	postupně
dolů	dolů	k6eAd1	dolů
-	-	kIx~	-
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
nepříliš	příliš	k6eNd1	příliš
dobrá	dobrý	k2eAgFnSc1d1	dobrá
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
reprezentačním	reprezentační	k2eAgInSc6d1	reprezentační
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výsledky	výsledek	k1gInPc1	výsledek
přestávaly	přestávat	k5eAaImAgInP	přestávat
pozvolna	pozvolna	k6eAd1	pozvolna
být	být	k5eAaImF	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
kritériem	kritérion	k1gNnSc7	kritérion
pro	pro	k7c4	pro
nominaci	nominace	k1gFnSc4	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stolního	stolní	k2eAgInSc2d1	stolní
tenisu	tenis	k1gInSc2	tenis
tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
příchod	příchod	k1gInSc1	příchod
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
japonských	japonský	k2eAgFnPc2d1	japonská
<g/>
"	"	kIx"	"
pálek	pálka	k1gFnPc2	pálka
s	s	k7c7	s
houbovými	houbový	k2eAgInPc7d1	houbový
potahy	potah	k1gInPc7	potah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
poněkud	poněkud	k6eAd1	poněkud
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
dostal	dostat	k5eAaPmAgMnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
trenéra	trenér	k1gMnSc2	trenér
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
složením	složení	k1gNnSc7	složení
trenérských	trenérský	k2eAgFnPc2d1	trenérská
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
mnohanásobný	mnohanásobný	k2eAgMnSc1d1	mnohanásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
znechuceně	znechuceně	k6eAd1	znechuceně
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
vrcholového	vrcholový	k2eAgInSc2d1	vrcholový
stolního	stolní	k2eAgInSc2d1	stolní
tenisu	tenis	k1gInSc2	tenis
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
zatrpklý	zatrpklý	k2eAgMnSc1d1	zatrpklý
a	a	k8xC	a
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dosažené	dosažený	k2eAgInPc4d1	dosažený
úspěchy	úspěch	k1gInPc4	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mužská	mužský	k2eAgFnSc1d1	mužská
dvouhra	dvouhra	k1gFnSc1	dvouhra
1938	[number]	k4	1938
a	a	k8xC	a
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
také	také	k9	také
dvě	dva	k4xCgFnPc1	dva
stříbrné	stříbrný	k2eAgFnPc1d1	stříbrná
medaile	medaile	k1gFnPc1	medaile
1948	[number]	k4	1948
a	a	k8xC	a
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mužská	mužský	k2eAgFnSc1d1	mužská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
a	a	k8xC	a
1951	[number]	k4	1951
</s>
</p>
<p>
<s>
smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
a	a	k8xC	a
1951	[number]	k4	1951
</s>
</p>
<p>
<s>
mužské	mužský	k2eAgNnSc1d1	mužské
družstvo	družstvo	k1gNnSc1	družstvo
(	(	kIx(	(
<g/>
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
a	a	k8xC	a
1951	[number]	k4	1951
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
100	[number]	k4	100
let	let	k1gInSc1	let
českého	český	k2eAgInSc2d1	český
sportu	sport	k1gInSc2	sport
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7376	[number]	k4	7376
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
119	[number]	k4	119
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
stolním	stolní	k2eAgInSc6d1	stolní
tenisu	tenis	k1gInSc6	tenis
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Bergmann	Bergmann	k1gMnSc1	Bergmann
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Andreadis	Andreadis	k1gFnSc2	Andreadis
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Barna	Barn	k1gInSc2	Barn
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Kettnerová	Kettnerová	k1gFnSc1	Kettnerová
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Váňa	Váňa	k1gMnSc1	Váňa
-	-	kIx~	-
Pět	pět	k4xCc1	pět
stupňů	stupeň	k1gInPc2	stupeň
k	k	k7c3	k
mistrovství	mistrovství	k1gNnSc3	mistrovství
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Třicet	třicet	k4xCc1	třicet
medailí	medaile	k1gFnPc2	medaile
komunistům	komunista	k1gMnPc3	komunista
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Váňa	Váňa	k1gMnSc1	Váňa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
'	'	kIx"	'
<g/>
poustevník	poustevník	k1gMnSc1	poustevník
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
30.5	[number]	k4	30.5
<g/>
.2015	.2015	k4	.2015
</s>
</p>
