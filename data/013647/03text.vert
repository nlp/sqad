<s>
Yard	yard	k1gInSc1
</s>
<s>
Yard	yard	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
yd	yd	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
původem	původ	k1gInSc7
britská	britský	k2eAgFnSc1d1
délková	délkový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
převzata	převzít	k5eAaPmNgFnS
do	do	k7c2
tzv.	tzv.	kA
imperiálního	imperiální	k2eAgInSc2d1
systému	systém	k1gInSc2
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
různé	různý	k2eAgFnPc4d1
definice	definice	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
yd	yd	k?
=	=	kIx~
0,9144	0,9144	k4
m	m	kA
</s>
<s>
1	#num#	k4
yd	yd	k?
=	=	kIx~
3	#num#	k4
ft	ft	k?
</s>
<s>
1	#num#	k4
yd	yd	k?
=	=	kIx~
36	#num#	k4
in	in	k?
</s>
<s>
1	#num#	k4
yd	yd	k?
=	=	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1760	#num#	k4
míle	míle	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
tradice	tradice	k1gFnSc2
odpovídal	odpovídat	k5eAaImAgInS
historický	historický	k2eAgInSc1d1
yard	yard	k1gInSc1
údajně	údajně	k6eAd1
vzdálenosti	vzdálenost	k1gFnSc2
mezi	mezi	k7c7
špičkou	špička	k1gFnSc7
nosu	nos	k1gInSc2
a	a	k8xC
prostředníkem	prostředník	k1gInSc7
předpažené	předpažený	k2eAgFnSc2d1
ruky	ruka	k1gFnSc2
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
I.	I.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
USA	USA	kA
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
dříve	dříve	k6eAd2
používány	používat	k5eAaImNgFnP
různé	různý	k2eAgFnPc1d1
definice	definice	k1gFnPc1
yardu	yard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současná	současný	k2eAgFnSc1d1
definice	definice	k1gFnSc1
vztažená	vztažený	k2eAgFnSc1d1
k	k	k7c3
soustavě	soustava	k1gFnSc3
SI	si	k1gNnSc2
je	být	k5eAaImIp3nS
kompromisem	kompromis	k1gInSc7
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
(	(	kIx(
<g/>
yard	yard	k1gInSc4
k	k	k7c3
jednotkám	jednotka	k1gFnPc3
SI	si	k1gNnSc2
ale	ale	k8xC
nepatří	patřit	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Yard	yard	k1gInSc1
je	být	k5eAaImIp3nS
nepřímo	přímo	k6eNd1
používán	používat	k5eAaImNgInS
v	v	k7c6
mnoha	mnoho	k4c6
státech	stát	k1gInPc6
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
pomocí	pomoc	k1gFnSc7
jsou	být	k5eAaImIp3nP
stanoveny	stanovit	k5eAaPmNgInP
některé	některý	k3yIgInPc1
rozměry	rozměr	k1gInPc1
hřiště	hřiště	k1gNnSc2
pro	pro	k7c4
kopanou	kopaná	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
atletice	atletika	k1gFnSc6
odpovídá	odpovídat	k5eAaImIp3nS
1	#num#	k4
yardu	yard	k1gInSc2
výška	výška	k1gFnSc1
překážek	překážka	k1gFnPc2
v	v	k7c6
mužských	mužský	k2eAgInPc6d1
bězích	běh	k1gInPc6
na	na	k7c4
400	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
3000	#num#	k4
metrů	metr	k1gInPc2
překážek	překážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mužském	mužský	k2eAgInSc6d1
běhu	běh	k1gInSc6
na	na	k7c4
110	#num#	k4
metrů	metr	k1gInPc2
překážek	překážka	k1gFnPc2
odpovídají	odpovídat	k5eAaImIp3nP
rozestupy	rozestup	k1gInPc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
překážek	překážka	k1gFnPc2
vzdálenosti	vzdálenost	k1gFnSc2
10	#num#	k4
yardů	yard	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Lord	lord	k1gMnSc1
Ritchie	Ritchie	k1gFnSc2
Candler	Candler	k1gMnSc1
<g/>
:	:	kIx,
Conversion	Conversion	k1gInSc1
the	the	k?
Metric	Metric	k1gMnSc1
System	Syst	k1gMnSc7
<g/>
,	,	kIx,
Scientific	Scientific	k1gMnSc1
American	Americana	k1gFnPc2
<g/>
,	,	kIx,
July	Jula	k1gFnSc2
1970	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
yard	yard	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
