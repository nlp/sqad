<s>
Arméni	Armén	k1gMnPc1	Armén
též	též	k9	též
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
na	na	k7c6	na
světě	svět	k1gInSc6	svět
unikátní	unikátní	k2eAgFnSc4d1	unikátní
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
405	[number]	k4	405
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
Mesrop	Mesrop	k1gInSc1	Mesrop
Maštoc	Maštoc	k1gFnSc4	Maštoc
<g/>
.	.	kIx.	.
</s>
