<p>
<s>
Jihoafrický	jihoafrický	k2eAgInSc1d1	jihoafrický
rand	rand	k1gInSc1	rand
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
South	South	k1gInSc1	South
African	Africana	k1gFnPc2	Africana
rand	rand	k1gInSc1	rand
<g/>
,	,	kIx,	,
afrikánsky	afrikánsky	k6eAd1	afrikánsky
Suid-Afrikaanse	Suid-Afrikaanse	k1gFnSc1	Suid-Afrikaanse
rand	rand	k1gInSc1	rand
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zákonným	zákonný	k2eAgNnSc7d1	zákonné
platidlem	platidlo	k1gNnSc7	platidlo
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
ZAR	ZAR	kA	ZAR
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
kód	kód	k1gInSc1	kód
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
Zuid-Afrikaanse	Zuid-Afrikaans	k1gMnSc2	Zuid-Afrikaans
rand	rand	k1gInSc4	rand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
setina	setina	k1gFnSc1	setina
randu	rand	k1gInSc2	rand
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
cent	cent	k1gInSc1	cent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
členů	člen	k1gInPc2	člen
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Lesotho	Lesot	k1gMnSc2	Lesot
a	a	k8xC	a
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
státy	stát	k1gInPc1	stát
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
utvořily	utvořit	k5eAaPmAgInP	utvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
měnovou	měnový	k2eAgFnSc4d1	měnová
unii	unie	k1gFnSc4	unie
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Společný	společný	k2eAgInSc1d1	společný
měnový	měnový	k2eAgInSc1d1	měnový
prostor	prostor	k1gInSc1	prostor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
fungování	fungování	k1gNnSc1	fungování
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měny	měna	k1gFnPc4	měna
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
jsou	být	k5eAaImIp3nP	být
pevně	pevně	k6eAd1	pevně
navázány	navázat	k5eAaPmNgInP	navázat
na	na	k7c4	na
rand	rand	k1gInSc4	rand
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
platit	platit	k5eAaImF	platit
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
jak	jak	k8xS	jak
místní	místní	k2eAgFnSc7d1	místní
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
randem	rand	k1gInSc7	rand
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
ale	ale	k8xC	ale
měny	měna	k1gFnSc2	měna
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
použít	použít	k5eAaPmF	použít
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
koloniálním	koloniální	k2eAgNnSc6d1	koloniální
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
používán	používán	k2eAgInSc1d1	používán
monetární	monetární	k2eAgInSc1d1	monetární
systém	systém	k1gInSc1	systém
té	ten	k3xDgFnSc2	ten
evropské	evropský	k2eAgFnSc2d1	Evropská
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zrovna	zrovna	k6eAd1	zrovna
spravovala	spravovat	k5eAaImAgFnS	spravovat
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
především	především	k9	především
britský	britský	k2eAgMnSc1d1	britský
a	a	k8xC	a
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
libra	libra	k1gFnSc1	libra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pevně	pevně	k6eAd1	pevně
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
britskou	britský	k2eAgFnSc4d1	britská
libru	libra	k1gFnSc4	libra
v	v	k7c6	v
paritním	paritní	k2eAgInSc6d1	paritní
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
dělila	dělit	k5eAaImAgFnS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
20	[number]	k4	20
šilinků	šilink	k1gInPc2	šilink
a	a	k8xC	a
240	[number]	k4	240
pencí	pence	k1gFnPc2	pence
<g/>
.	.	kIx.	.
</s>
<s>
Rand	rand	k1gInSc1	rand
byl	být	k5eAaImAgInS	být
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradil	nahradit	k5eAaPmAgInS	nahradit
libru	libra	k1gFnSc4	libra
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
libra	libra	k1gFnSc1	libra
=	=	kIx~	=
2	[number]	k4	2
randy	rand	k1gInPc7	rand
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
desítkovou	desítkový	k2eAgFnSc4d1	desítková
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mince	mince	k1gFnPc4	mince
a	a	k8xC	a
bankovky	bankovka	k1gFnPc4	bankovka
==	==	k?	==
</s>
</p>
<p>
<s>
Mince	mince	k1gFnPc1	mince
randu	rand	k1gInSc2	rand
mají	mít	k5eAaImIp3nP	mít
hodnoty	hodnota	k1gFnSc2	hodnota
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
50	[number]	k4	50
centů	cent	k1gInPc2	cent
a	a	k8xC	a
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
randů	rand	k1gInPc2	rand
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
reversní	reversní	k2eAgFnSc6d1	reversní
straně	strana	k1gFnSc6	strana
všech	všecek	k3xTgFnPc2	všecek
mincí	mince	k1gFnPc2	mince
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
současná	současný	k2eAgFnSc1d1	současná
série	série	k1gFnSc1	série
vnikla	vniknout	k5eAaPmAgFnS	vniknout
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnSc1	mince
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
centy	cent	k1gInPc4	cent
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přestaly	přestat	k5eAaPmAgFnP	přestat
razit	razit	k5eAaImF	razit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zákonným	zákonný	k2eAgNnSc7d1	zákonné
platidlem	platidlo	k1gNnSc7	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
cirkulaci	cirkulace	k1gFnSc6	cirkulace
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc1	systém
zaokrouhlení	zaokrouhlení	k1gNnPc2	zaokrouhlení
na	na	k7c4	na
nejbližších	blízký	k2eAgNnPc2d3	nejbližší
5	[number]	k4	5
centů	cent	k1gInPc2	cent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
jsou	být	k5eAaImIp3nP	být
tisknuty	tisknout	k5eAaImNgFnP	tisknout
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
200	[number]	k4	200
randů	rand	k1gInPc2	rand
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
reversní	reversní	k2eAgFnSc6d1	reversní
straně	strana	k1gFnSc6	strana
bankovek	bankovka	k1gFnPc2	bankovka
jsou	být	k5eAaImIp3nP	být
vyobrazena	vyobrazen	k2eAgNnPc1d1	vyobrazeno
místní	místní	k2eAgNnPc1d1	místní
divoká	divoký	k2eAgNnPc1d1	divoké
zvířata	zvíře	k1gNnPc1	zvíře
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Velká	velký	k2eAgFnSc1d1	velká
pětka	pětka	k1gFnSc1	pětka
<g/>
:	:	kIx,	:
nosorožec	nosorožec	k1gMnSc1	nosorožec
(	(	kIx(	(
<g/>
10	[number]	k4	10
randů	rand	k1gInPc2	rand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
(	(	kIx(	(
<g/>
20	[number]	k4	20
randů	rand	k1gInPc2	rand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
50	[number]	k4	50
randů	rand	k1gInPc2	rand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buvol	buvol	k1gMnSc1	buvol
(	(	kIx(	(
<g/>
100	[number]	k4	100
randů	rand	k1gInPc2	rand
<g/>
)	)	kIx)	)
a	a	k8xC	a
levhart	levhart	k1gMnSc1	levhart
(	(	kIx(	(
<g/>
200	[number]	k4	200
randů	rand	k1gInPc2	rand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
série	série	k1gFnSc1	série
bankovek	bankovka	k1gFnPc2	bankovka
tištěných	tištěný	k2eAgFnPc2d1	tištěná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
aversní	aversní	k2eAgFnSc6d1	aversní
straně	strana	k1gFnSc6	strana
portrét	portrét	k1gInSc4	portrét
Nelsona	Nelson	k1gMnSc2	Nelson
Mandely	Mandela	k1gFnSc2	Mandela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jihoafrický	jihoafrický	k2eAgInSc4d1	jihoafrický
rand	rand	k1gInSc4	rand
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
