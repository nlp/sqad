<s>
Xenon	xenon	k1gInSc1	xenon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Xe	Xe	k1gFnSc2	Xe
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Xenon	xenon	k1gInSc1	xenon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc4d1	plynný
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
patřící	patřící	k2eAgInSc4d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
nereaktivní	reaktivní	k2eNgInSc1d1	nereaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
<g/>
,	,	kIx,	,
chlorem	chlor	k1gInSc7	chlor
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
silnými	silný	k2eAgInPc7d1	silný
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
činidly	činidlo	k1gNnPc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Trioxid	Trioxid	k1gInSc1	Trioxid
xenonu	xenon	k1gInSc2	xenon
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc1	oxid
xenonový	xenonový	k2eAgInSc1d1	xenonový
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
silně	silně	k6eAd1	silně
explozivní	explozivní	k2eAgFnSc1d1	explozivní
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
lépe	dobře	k6eAd2	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
v	v	k7c6	v
nepolárních	polární	k2eNgNnPc6d1	nepolární
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
snadno	snadno	k6eAd1	snadno
ionizuje	ionizovat	k5eAaBmIp3nS	ionizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
ionizovaném	ionizovaný	k2eAgInSc6d1	ionizovaný
stavu	stav	k1gInSc6	stav
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
osvětlovací	osvětlovací	k2eAgFnSc6d1	osvětlovací
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
září	zářit	k5eAaImIp3nS	zářit
fialovou	fialový	k2eAgFnSc7d1	fialová
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ředěním	ředění	k1gNnSc7	ředění
xenonu	xenon	k1gInSc2	xenon
ve	v	k7c6	v
výbojové	výbojový	k2eAgFnSc6d1	výbojová
trubici	trubice	k1gFnSc6	trubice
barva	barva	k1gFnSc1	barva
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
na	na	k7c6	na
plnosti	plnost	k1gFnSc6	plnost
a	a	k8xC	a
při	při	k7c6	při
velkém	velký	k2eAgNnSc6d1	velké
zředění	zředění	k1gNnSc6	zředění
vydává	vydávat	k5eAaImIp3nS	vydávat
xenon	xenon	k1gInSc1	xenon
pouze	pouze	k6eAd1	pouze
bílé	bílý	k2eAgNnSc4d1	bílé
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
objevil	objevit	k5eAaPmAgInS	objevit
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Rayleightem	Rayleight	k1gInSc7	Rayleight
argon	argon	k1gInSc1	argon
a	a	k8xC	a
správně	správně	k6eAd1	správně
oba	dva	k4xCgInPc4	dva
plyny	plyn	k1gInPc4	plyn
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
mu	on	k3xPp3gMnSc3	on
volné	volný	k2eAgNnSc1d1	volné
místo	místo	k1gNnSc1	místo
před	před	k7c7	před
a	a	k8xC	a
za	za	k7c7	za
argonem	argon	k1gInSc7	argon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
volných	volný	k2eAgNnPc2d1	volné
míst	místo	k1gNnPc2	místo
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
neon	neon	k1gInSc4	neon
a	a	k8xC	a
krypton	krypton	k1gInSc4	krypton
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
Williamem	William	k1gInSc7	William
Ramsayem	Ramsay	k1gInSc7	Ramsay
a	a	k8xC	a
Morrisem	Morris	k1gInSc7	Morris
Traversem	travers	k1gInSc7	travers
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
využil	využít	k5eAaPmAgMnS	využít
nové	nový	k2eAgFnPc4d1	nová
metody	metoda	k1gFnPc4	metoda
frakční	frakční	k2eAgFnSc2d1	frakční
destilace	destilace	k1gFnSc2	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
xenonem	xenon	k1gInSc7	xenon
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
neon	neon	k1gInSc1	neon
a	a	k8xC	a
krypton	krypton	k1gInSc1	krypton
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
jako	jako	k9	jako
zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c6	po
destilaci	destilace	k1gFnSc6	destilace
argonu	argon	k1gInSc2	argon
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaBmAgMnS	nazvat
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gMnSc2	Ramsaa
cizí	cizí	k2eAgFnPc1d1	cizí
-	-	kIx~	-
xenon	xenon	k1gInSc1	xenon
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
%	%	kIx~	%
(	(	kIx(	(
<g/>
ve	v	k7c6	v
100	[number]	k4	100
litrech	litr	k1gInPc6	litr
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
0,005	[number]	k4	0,005
ml	ml	kA	ml
xenonu	xenon	k1gInSc6	xenon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pramenech	pramen	k1gInPc6	pramen
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
rozpadu	rozpad	k1gInSc2	rozpad
izotopů	izotop	k1gInPc2	izotop
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
plutonia	plutonium	k1gNnSc2	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
získáván	získávat	k5eAaImNgInS	získávat
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
možností	možnost	k1gFnSc7	možnost
jak	jak	k8xC	jak
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
frakční	frakční	k2eAgFnSc1d1	frakční
adsorpce	adsorpce	k1gFnSc1	adsorpce
na	na	k7c4	na
aktivní	aktivní	k2eAgNnSc4d1	aktivní
uhlí	uhlí	k1gNnSc4	uhlí
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
kapalného	kapalný	k2eAgInSc2d1	kapalný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
izotopů	izotop	k1gInPc2	izotop
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
šest	šest	k4xCc1	šest
je	on	k3xPp3gMnPc4	on
stabilních	stabilní	k2eAgInPc2d1	stabilní
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
mají	mít	k5eAaImIp3nP	mít
poločas	poločas	k1gInSc4	poločas
přeměny	přeměna	k1gFnSc2	přeměna
delší	dlouhý	k2eAgInSc4d2	delší
než	než	k8xS	než
1014	[number]	k4	1014
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
dvacet	dvacet	k4xCc4	dvacet
nestabilních	stabilní	k2eNgMnPc2d1	nestabilní
<g/>
,	,	kIx,	,
podléhajících	podléhající	k2eAgMnPc2d1	podléhající
další	další	k2eAgFnSc4d1	další
radioaktivní	radioaktivní	k2eAgFnSc4d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Určení	určení	k1gNnSc1	určení
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
poměru	poměr	k1gInSc2	poměr
různých	různý	k2eAgInPc2d1	různý
izotopů	izotop	k1gInPc2	izotop
xenonu	xenon	k1gInSc2	xenon
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
geologických	geologický	k2eAgFnPc2d1	geologická
přeměn	přeměna	k1gFnPc2	přeměna
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc4d1	podobné
studium	studium	k1gNnSc4	studium
izotopů	izotop	k1gInPc2	izotop
xenonu	xenon	k1gInSc2	xenon
vázaného	vázaný	k2eAgInSc2d1	vázaný
v	v	k7c6	v
meteoritech	meteorit	k1gInPc6	meteorit
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
formování	formování	k1gNnSc2	formování
našeho	náš	k3xOp1gInSc2	náš
slunečního	sluneční	k2eAgInSc2d1	sluneční
systému	systém	k1gInSc2	systém
i	i	k8xC	i
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Elektrickým	elektrický	k2eAgInSc7d1	elektrický
výbojem	výboj	k1gInSc7	výboj
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
xenonu	xenon	k1gInSc2	xenon
vzniká	vznikat	k5eAaImIp3nS	vznikat
světlo	světlo	k1gNnSc1	světlo
fialové	fialový	k2eAgFnSc2d1	fialová
až	až	k8xS	až
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ředěním	ředění	k1gNnSc7	ředění
xenonu	xenon	k1gInSc2	xenon
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
až	až	k8xS	až
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pouze	pouze	k6eAd1	pouze
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
působí	působit	k5eAaImIp3nS	působit
baktericidně	baktericidně	k6eAd1	baktericidně
a	a	k8xC	a
xenonové	xenonový	k2eAgFnPc1d1	xenonová
výbojky	výbojka	k1gFnPc1	výbojka
nalézají	nalézat	k5eAaImIp3nP	nalézat
využití	využití	k1gNnSc4	využití
pro	pro	k7c4	pro
dezinfekci	dezinfekce	k1gFnSc4	dezinfekce
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zkonstruovány	zkonstruován	k2eAgFnPc1d1	zkonstruována
xenonové	xenonový	k2eAgFnPc1d1	xenonová
výbojky	výbojka	k1gFnPc1	výbojka
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
produkovat	produkovat	k5eAaImF	produkovat
mimořádně	mimořádně	k6eAd1	mimořádně
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
světelné	světelný	k2eAgInPc4d1	světelný
záblesky	záblesk	k1gInPc4	záblesk
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
krátkém	krátký	k2eAgNnSc6d1	krátké
trvání	trvání	k1gNnSc6	trvání
výboje	výboj	k1gInSc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
výbojkám	výbojka	k1gFnPc3	výbojka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
fotografovat	fotografovat	k5eAaImF	fotografovat
a	a	k8xC	a
filmovat	filmovat	k5eAaImF	filmovat
velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgInPc1d1	rychlý
děje	děj	k1gInPc1	děj
(	(	kIx(	(
<g/>
průlet	průlet	k1gInSc1	průlet
vystřelené	vystřelený	k2eAgFnSc2d1	vystřelená
kulky	kulka	k1gFnSc2	kulka
překážkou	překážka	k1gFnSc7	překážka
<g/>
,	,	kIx,	,
výbuchy	výbuch	k1gInPc7	výbuch
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Xenon	xenon	k1gInSc1	xenon
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
dále	daleko	k6eAd2	daleko
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
obloukových	obloukový	k2eAgFnPc2d1	oblouková
lamp	lampa	k1gFnPc2	lampa
a	a	k8xC	a
doutnavých	doutnavý	k2eAgFnPc2d1	doutnavý
trubic	trubice	k1gFnPc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
sportovci	sportovec	k1gMnPc1	sportovec
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2014	[number]	k4	2014
údajně	údajně	k6eAd1	údajně
inhalovali	inhalovat	k5eAaImAgMnP	inhalovat
xenon	xenon	k1gInSc4	xenon
jako	jako	k8xS	jako
doping	doping	k1gInSc4	doping
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
všechny	všechen	k3xTgInPc1	všechen
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
za	za	k7c7	za
inertní	inertní	k2eAgFnSc7d1	inertní
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
provedl	provést	k5eAaPmAgInS	provést
Neil	Neil	k1gInSc1	Neil
Bartlett	Bartlett	k2eAgInSc1d1	Bartlett
reakci	reakce	k1gFnSc4	reakce
xenonu	xenon	k1gInSc2	xenon
s	s	k7c7	s
fluoridem	fluorid	k1gInSc7	fluorid
platinovým	platinový	k2eAgInSc7d1	platinový
PtF	PtF	k1gFnSc7	PtF
<g/>
6	[number]	k4	6
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
tak	tak	k6eAd1	tak
první	první	k4xOgFnSc4	první
sloučeninu	sloučenina	k1gFnSc4	sloučenina
vzácného	vzácný	k2eAgInSc2d1	vzácný
plynu	plyn	k1gInSc2	plyn
XePtF	XePtF	k1gFnSc2	XePtF
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
ani	ani	k8xC	ani
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
provedl	provést	k5eAaPmAgMnS	provést
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hoppe	Hopp	k1gInSc5	Hopp
syntézu	syntéza	k1gFnSc4	syntéza
fluoridu	fluorid	k1gInSc2	fluorid
xenonatého	xenonatý	k2eAgMnSc4d1	xenonatý
XeF	XeF	k1gMnSc4	XeF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
40	[number]	k4	40
K	K	kA	K
relativně	relativně	k6eAd1	relativně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
xenonu	xenon	k1gInSc2	xenon
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
:	:	kIx,	:
Chlorid	chlorid	k1gInSc1	chlorid
xenonatý	xenonatý	k2eAgInSc1d1	xenonatý
(	(	kIx(	(
<g/>
Dichlorid	Dichlorid	k1gInSc1	Dichlorid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeCl	XeCl	k1gMnSc1	XeCl
<g/>
2	[number]	k4	2
Chlorid	chlorid	k1gInSc1	chlorid
xenoničitý	xenoničitý	k2eAgInSc1d1	xenoničitý
(	(	kIx(	(
<g/>
Tetrachlorid	Tetrachlorid	k1gInSc1	Tetrachlorid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeCl	XeCl	k1gMnSc1	XeCl
<g/>
4	[number]	k4	4
Fluorid	fluorid	k1gInSc1	fluorid
xenonatý	xenonatý	k2eAgInSc1d1	xenonatý
(	(	kIx(	(
<g/>
Difluorid	Difluorid	k1gInSc1	Difluorid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeF	XeF	k1gFnSc1	XeF
<g/>
2	[number]	k4	2
Fluorid	fluorid	k1gInSc1	fluorid
<g />
.	.	kIx.	.
</s>
<s>
xenoničitý	xenoničitý	k2eAgInSc1d1	xenoničitý
(	(	kIx(	(
<g/>
Tetrafluorid	Tetrafluorid	k1gInSc1	Tetrafluorid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeF	XeF	k1gFnSc1	XeF
<g/>
4	[number]	k4	4
Fluorid	fluorid	k1gInSc1	fluorid
xenonový	xenonový	k2eAgInSc1d1	xenonový
(	(	kIx(	(
<g/>
Hexafluorid	Hexafluorid	k1gInSc1	Hexafluorid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeF	XeF	k1gFnSc1	XeF
<g/>
6	[number]	k4	6
Oxid	oxid	k1gInSc1	oxid
xenonový	xenonový	k2eAgInSc1d1	xenonový
(	(	kIx(	(
<g/>
Trioxid	Trioxid	k1gInSc1	Trioxid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeO	XeO	k1gFnSc1	XeO
<g/>
3	[number]	k4	3
Oxid	oxid	k1gInSc1	oxid
xenoničelý	xenoničelý	k2eAgInSc1d1	xenoničelý
(	(	kIx(	(
<g/>
Tetraoxid	Tetraoxid	k1gInSc1	Tetraoxid
xenonu	xenon	k1gInSc2	xenon
<g/>
)	)	kIx)	)
XeO	XeO	k1gFnPc2	XeO
<g/>
4	[number]	k4	4
Xenoničelan	Xenoničelana	k1gFnPc2	Xenoničelana
sodný	sodný	k2eAgMnSc1d1	sodný
Na	na	k7c4	na
<g/>
4	[number]	k4	4
<g/>
XeO	XeO	k1gFnSc2	XeO
<g />
.	.	kIx.	.
</s>
<s>
<g/>
6	[number]	k4	6
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
xenon	xenon	k1gInSc1	xenon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
xenon	xenon	k1gInSc1	xenon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
