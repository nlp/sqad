<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Roper	Roper	k1gMnSc1	Roper
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1965	[number]	k4	1965
Concord	Concorda	k1gFnPc2	Concorda
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
herní	herní	k2eAgMnSc1d1	herní
vývojář	vývojář	k1gMnSc1	vývojář
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
postav	postava	k1gFnPc2	postava
videoherního	videoherní	k2eAgInSc2d1	videoherní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
společnosti	společnost	k1gFnSc2	společnost
Blizzard	Blizzard	k1gMnSc1	Blizzard
North	North	k1gMnSc1	North
a	a	k8xC	a
ředitelem	ředitel	k1gMnSc7	ředitel
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
v	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
pozicích	pozice	k1gFnPc6	pozice
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
komerčně	komerčně	k6eAd1	komerčně
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c4	v
úspěch	úspěch	k1gInSc4	úspěch
sérií	série	k1gFnPc2	série
Warcraft	Warcrafta	k1gFnPc2	Warcrafta
<g/>
,	,	kIx,	,
StarCraft	StarCrafta	k1gFnPc2	StarCrafta
a	a	k8xC	a
Diablo	Diablo	k1gFnPc2	Diablo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Blizzard	Blizzarda	k1gFnPc2	Blizzarda
Entertainment	Entertainment	k1gMnSc1	Entertainment
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
společnost	společnost	k1gFnSc4	společnost
Flagship	Flagship	k1gMnSc1	Flagship
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
odpovědnou	odpovědný	k2eAgFnSc4d1	odpovědná
za	za	k7c4	za
tituly	titul	k1gInPc4	titul
Hellgate	Hellgat	k1gInSc5	Hellgat
<g/>
:	:	kIx,	:
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
oproti	oproti	k7c3	oproti
očekáváním	očekávání	k1gNnSc7	očekávání
přijat	přijmout	k5eAaPmNgInS	přijmout
průměrně	průměrně	k6eAd1	průměrně
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
momentálně	momentálně	k6eAd1	momentálně
pozastavený	pozastavený	k2eAgInSc1d1	pozastavený
<g/>
)	)	kIx)	)
Mythos	Mythos	k1gInSc1	Mythos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hry	hra	k1gFnPc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
významnější	významný	k2eAgFnPc4d2	významnější
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
jeho	jeho	k3xOp3gInSc4	jeho
profil	profil	k1gInSc4	profil
na	na	k7c6	na
IMDB	IMDB	kA	IMDB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blackthorne	Blackthornout	k5eAaPmIp3nS	Blackthornout
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Warcraft	Warcraft	k1gInSc1	Warcraft
<g/>
:	:	kIx,	:
Orcs	Orcs	k1gInSc1	Orcs
&	&	k?	&
Humans	Humans	k1gInSc1	Humans
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Warcraft	Warcraft	k1gMnSc1	Warcraft
II	II	kA	II
<g/>
:	:	kIx,	:
Tides	Tides	k1gMnSc1	Tides
of	of	k?	of
Darkness	Darkness	k1gInSc1	Darkness
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Warcraft	Warcraft	k1gMnSc1	Warcraft
II	II	kA	II
<g/>
:	:	kIx,	:
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Dark	Dark	k1gMnSc1	Dark
Portal	Portal	k1gMnSc1	Portal
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
StarCraft	StarCraft	k1gInSc1	StarCraft
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
StarCraft	StarCraft	k1gInSc1	StarCraft
<g/>
:	:	kIx,	:
Brood	Brood	k1gInSc1	Brood
War	War	k1gFnSc2	War
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Warcraft	Warcraft	k1gMnSc1	Warcraft
II	II	kA	II
<g/>
:	:	kIx,	:
Battle	Battle	k1gFnSc1	Battle
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
Edition	Edition	k1gInSc1	Edition
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
II	II	kA	II
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
II	II	kA	II
<g/>
:	:	kIx,	:
Lord	lord	k1gMnSc1	lord
of	of	k?	of
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Warcraft	Warcraft	k1gMnSc1	Warcraft
III	III	kA	III
<g/>
:	:	kIx,	:
Reign	Reign	k1gMnSc1	Reign
of	of	k?	of
Chaos	chaos	k1gInSc1	chaos
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Warcraft	Warcraft	k1gMnSc1	Warcraft
III	III	kA	III
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Frozen	Frozen	k2eAgInSc1d1	Frozen
Throne	Thron	k1gInSc5	Thron
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hellgate	Hellgat	k1gMnSc5	Hellgat
<g/>
:	:	kIx,	:
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bill	Bill	k1gMnSc1	Bill
Roper	Roper	k1gMnSc1	Roper
(	(	kIx(	(
<g/>
Video	video	k1gNnSc1	video
game	game	k1gInSc1	game
producer	producer	k1gMnSc1	producer
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
