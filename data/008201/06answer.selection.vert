<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Blizzard	Blizzarda	k1gFnPc2	Blizzarda
Entertainment	Entertainment	k1gMnSc1	Entertainment
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
společnost	společnost	k1gFnSc4	společnost
Flagship	Flagship	k1gMnSc1	Flagship
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
odpovědnou	odpovědný	k2eAgFnSc4d1	odpovědná
za	za	k7c4	za
tituly	titul	k1gInPc4	titul
Hellgate	Hellgat	k1gInSc5	Hellgat
<g/>
:	:	kIx,	:
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
oproti	oproti	k7c3	oproti
očekáváním	očekávání	k1gNnSc7	očekávání
přijat	přijmout	k5eAaPmNgInS	přijmout
průměrně	průměrně	k6eAd1	průměrně
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
momentálně	momentálně	k6eAd1	momentálně
pozastavený	pozastavený	k2eAgInSc1d1	pozastavený
<g/>
)	)	kIx)	)
Mythos	Mythos	k1gInSc1	Mythos
<g/>
.	.	kIx.	.
</s>
