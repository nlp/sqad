<s>
Kočka	kočka	k1gFnSc1	kočka
je	být	k5eAaImIp3nS	být
především	především	k9	především
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
predátor	predátor	k1gMnSc1	predátor
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
ostré	ostrý	k2eAgInPc4d1	ostrý
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
potichu	potichu	k6eAd1	potichu
plížit	plížit	k5eAaImF	plížit
i	i	k8xC	i
vyvinout	vyvinout	k5eAaPmF	vyvinout
velkou	velký	k2eAgFnSc4d1	velká
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
udává	udávat	k5eAaImIp3nS	udávat
se	s	k7c7	s
48	[number]	k4	48
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyzbrojena	vyzbrojen	k2eAgFnSc1d1	vyzbrojena
zuby	zub	k1gInPc7	zub
a	a	k8xC	a
zejména	zejména	k9	zejména
drápy	dráp	k1gInPc4	dráp
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
