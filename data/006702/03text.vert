<s>
Fotometrie	fotometrie	k1gFnSc1	fotometrie
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
světlo	světlo	k1gNnSc4	světlo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
zrakový	zrakový	k2eAgInSc4d1	zrakový
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Veličiny	veličina	k1gFnPc1	veličina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
velikost	velikost	k1gFnSc4	velikost
tohoto	tento	k3xDgNnSc2	tento
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
fotometrické	fotometrický	k2eAgFnPc1d1	fotometrická
veličiny	veličina	k1gFnPc1	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
fotometrické	fotometrický	k2eAgFnPc4d1	fotometrická
veličiny	veličina	k1gFnPc4	veličina
řadíme	řadit	k5eAaImIp1nP	řadit
např.	např.	kA	např.
svítivost	svítivost	k1gFnSc4	svítivost
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
nebo	nebo	k8xC	nebo
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Fotometrie	fotometrie	k1gFnSc1	fotometrie
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
metody	metoda	k1gFnPc1	metoda
jako	jako	k8xC	jako
fotometrie	fotometrie	k1gFnPc1	fotometrie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
celého	celý	k2eAgNnSc2d1	celé
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
spektra	spektrum	k1gNnSc2	spektrum
v	v	k7c6	v
radiometrii	radiometrie	k1gFnSc6	radiometrie
<g/>
.	.	kIx.	.
</s>
<s>
Optika	optika	k1gFnSc1	optika
Radiometrie	radiometrie	k1gFnSc2	radiometrie
Fotometrické	fotometrický	k2eAgFnSc2d1	fotometrická
veličiny	veličina	k1gFnSc2	veličina
</s>
