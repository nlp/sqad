<s>
Fotometrie	fotometrie	k1gFnSc1	fotometrie
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
světlo	světlo	k1gNnSc4	světlo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
zrakový	zrakový	k2eAgInSc4d1	zrakový
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
