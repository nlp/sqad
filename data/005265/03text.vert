<s>
Navarrské	navarrský	k2eAgNnSc1d1	Navarrské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
baskicky	baskicky	k6eAd1	baskicky
<g/>
:	:	kIx,	:
Nafarroako	Nafarroako	k1gNnSc1	Nafarroako
Erresuma	Erresumum	k1gNnSc2	Erresumum
<g/>
;	;	kIx,	;
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Royaume	Royaum	k1gInSc5	Royaum
de	de	k?	de
Navarre	Navarr	k1gInSc5	Navarr
<g/>
;	;	kIx,	;
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Reino	Rein	k2eAgNnSc1d1	Reino
de	de	k?	de
Navarra	Navarra	k1gFnSc1	Navarra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Pamplonské	Pamplonský	k2eAgNnSc4d1	Pamplonský
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
evropské	evropský	k2eAgNnSc1d1	Evropské
království	království	k1gNnSc1	království
ležící	ležící	k2eAgNnSc1d1	ležící
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
u	u	k7c2	u
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
824	[number]	k4	824
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tamní	tamní	k2eAgMnSc1d1	tamní
baskický	baskický	k2eAgMnSc1d1	baskický
vůdce	vůdce	k1gMnSc1	vůdce
Íñ	Íñ	k1gMnSc1	Íñ
Arista	Arista	k1gMnSc1	Arista
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
pamplonského	pamplonský	k2eAgMnSc2d1	pamplonský
krále	král	k1gMnSc2	král
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgInS	ukončit
nadvládu	nadvláda	k1gFnSc4	nadvláda
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1513	[number]	k4	1513
zabrala	zabrat	k5eAaPmAgFnS	zabrat
Kastilie	Kastilie	k1gFnSc1	Kastilie
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Navarrského	navarrský	k2eAgNnSc2d1	Navarrské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
se	se	k3xPyFc4	se
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
do	do	k7c2	do
Francouzského	francouzský	k2eAgNnSc2d1	francouzské
království	království	k1gNnSc2	království
a	a	k8xC	a
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
užívalo	užívat	k5eAaImAgNnS	užívat
společně	společně	k6eAd1	společně
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Béarn	Béarn	k1gInSc4	Béarn
jistý	jistý	k2eAgInSc4d1	jistý
stupeň	stupeň	k1gInSc4	stupeň
autonomie	autonomie	k1gFnSc2	autonomie
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Mapy	mapa	k1gFnPc1	mapa
Znaky	znak	k1gInPc1	znak
a	a	k8xC	a
vlajky	vlajka	k1gFnPc1	vlajka
Seznam	seznam	k1gInSc1	seznam
navarrských	navarrský	k2eAgMnPc2d1	navarrský
králů	král	k1gMnPc2	král
Dějiny	dějiny	k1gFnPc1	dějiny
Španělska	Španělsko	k1gNnSc2	Španělsko
Dějiny	dějiny	k1gFnPc1	dějiny
Francie	Francie	k1gFnSc2	Francie
</s>
