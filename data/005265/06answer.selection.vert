<s>
Navarrské	navarrský	k2eAgNnSc1d1	Navarrské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
baskicky	baskicky	k6eAd1	baskicky
<g/>
:	:	kIx,	:
Nafarroako	Nafarroako	k1gNnSc1	Nafarroako
Erresuma	Erresumum	k1gNnSc2	Erresumum
<g/>
;	;	kIx,	;
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Royaume	Royaum	k1gInSc5	Royaum
de	de	k?	de
Navarre	Navarr	k1gInSc5	Navarr
<g/>
;	;	kIx,	;
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Reino	Rein	k2eAgNnSc1d1	Reino
de	de	k?	de
Navarra	Navarra	k1gFnSc1	Navarra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Pamplonské	Pamplonský	k2eAgNnSc4d1	Pamplonský
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
evropské	evropský	k2eAgNnSc1d1	Evropské
království	království	k1gNnSc1	království
ležící	ležící	k2eAgNnSc1d1	ležící
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
u	u	k7c2	u
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
