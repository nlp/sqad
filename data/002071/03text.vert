<s>
Athény	Athéna	k1gFnPc1	Athéna
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
Atény	Atény	k1gFnPc1	Atény
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
[	[	kIx(	[
<g/>
aˈ	aˈ	k?	aˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Attika	Attika	k1gFnSc1	Attika
poblíž	poblíž	k7c2	poblíž
Sarónského	Sarónský	k2eAgInSc2d1	Sarónský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
přístav	přístav	k1gInSc1	přístav
Pireus	Pireus	k1gInSc1	Pireus
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
aglomeraci	aglomerace	k1gFnSc4	aglomerace
čítající	čítající	k2eAgFnSc4d1	čítající
přes	přes	k7c4	přes
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověké	starověký	k2eAgFnSc2d1	starověká
Athény	Athéna	k1gFnSc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
Athén	Athéna	k1gFnPc2	Athéna
spadá	spadat	k5eAaPmIp3nS	spadat
až	až	k9	až
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Akropoli	Akropole	k1gFnSc6	Akropole
jako	jako	k9	jako
první	první	k4xOgInSc4	první
vybudován	vybudován	k2eAgInSc4d1	vybudován
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Písemně	písemně	k6eAd1	písemně
doložené	doložený	k2eAgFnPc1d1	doložená
dějiny	dějiny	k1gFnPc1	dějiny
Athén	Athéna	k1gFnPc2	Athéna
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
řízen	řídit	k5eAaImNgInS	řídit
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
času	čas	k1gInSc2	čas
však	však	k9	však
získávali	získávat	k5eAaImAgMnP	získávat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bezprávní	bezprávní	k2eAgMnSc1d1	bezprávní
rolníci	rolník	k1gMnPc1	rolník
a	a	k8xC	a
řemeslníci	řemeslník	k1gMnPc1	řemeslník
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
spolurozhodování	spolurozhodování	k1gNnSc2	spolurozhodování
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
621	[number]	k4	621
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
Drakón	Drakón	k1gInSc4	Drakón
tehdy	tehdy	k6eAd1	tehdy
platné	platný	k2eAgInPc4d1	platný
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Drakón	Drakón	k1gInSc1	Drakón
stanovil	stanovit	k5eAaPmAgInS	stanovit
tak	tak	k9	tak
přísné	přísný	k2eAgInPc4d1	přísný
tresty	trest	k1gInPc4	trest
za	za	k7c4	za
majetkové	majetkový	k2eAgInPc4d1	majetkový
přečiny	přečin	k1gInPc4	přečin
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
příslovečnými	příslovečný	k2eAgFnPc7d1	příslovečná
jako	jako	k9	jako
"	"	kIx"	"
<g/>
drakonické	drakonický	k2eAgNnSc1d1	drakonické
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
za	za	k7c4	za
pouhou	pouhý	k2eAgFnSc4d1	pouhá
krádež	krádež	k1gFnSc4	krádež
obilí	obilí	k1gNnSc2	obilí
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
Drakontových	Drakontův	k2eAgInPc2d1	Drakontův
zákonů	zákon	k1gInPc2	zákon
bylo	být	k5eAaImAgNnS	být
rozlišení	rozlišení	k1gNnSc1	rozlišení
úmyslného	úmyslný	k2eAgNnSc2d1	úmyslné
zabití	zabití	k1gNnSc2	zabití
(	(	kIx(	(
<g/>
vražda	vražda	k1gFnSc1	vražda
<g/>
)	)	kIx)	)
a	a	k8xC	a
neúmyslného	úmyslný	k2eNgNnSc2d1	neúmyslné
zabití	zabití	k1gNnSc2	zabití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
výkon	výkon	k1gInSc1	výkon
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
ponechával	ponechávat	k5eAaImAgInS	ponechávat
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
výkon	výkon	k1gInSc1	výkon
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
přecházel	přecházet	k5eAaImAgInS	přecházet
na	na	k7c4	na
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
za	za	k7c4	za
neúmyslné	úmyslný	k2eNgNnSc4d1	neúmyslné
zabití	zabití	k1gNnSc4	zabití
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
náhradu	náhrada	k1gFnSc4	náhrada
(	(	kIx(	(
<g/>
dobytek	dobytek	k1gMnSc1	dobytek
<g/>
,	,	kIx,	,
otrok	otrok	k1gMnSc1	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Athén	Athéna	k1gFnPc2	Athéna
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
594	[number]	k4	594
<g/>
/	/	kIx~	/
<g/>
593	[number]	k4	593
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
Solónova	Solónův	k2eAgFnSc1d1	Solónova
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
umožnila	umožnit	k5eAaPmAgFnS	umožnit
všem	všecek	k3xTgMnPc3	všecek
občanům	občan	k1gMnPc3	občan
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
soudní	soudní	k2eAgFnSc6d1	soudní
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
majetku	majetek	k1gInSc2	majetek
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgFnPc2	který
všechny	všechen	k3xTgFnPc1	všechen
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
zúčastňovat	zúčastňovat	k5eAaImF	zúčastňovat
na	na	k7c6	na
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
úřadů	úřad	k1gInPc2	úřad
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
voleni	volit	k5eAaImNgMnP	volit
jen	jen	k9	jen
příslušníci	příslušník	k1gMnPc1	příslušník
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
pentakosiomedimnoi	pentakosiomedimnoi	k6eAd1	pentakosiomedimnoi
a	a	k8xC	a
hippeis	hippeis	k1gFnSc1	hippeis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zřídil	zřídit	k5eAaPmAgInS	zřídit
radu	rada	k1gFnSc4	rada
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
moci	moc	k1gFnSc6	moc
pro	pro	k7c4	pro
nové	nový	k2eAgFnPc4d1	nová
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
547	[number]	k4	547
Peisistratos	Peisistratos	k1gInSc1	Peisistratos
nastolil	nastolit	k5eAaPmAgInS	nastolit
osobní	osobní	k2eAgFnSc4d1	osobní
vládu	vláda	k1gFnSc4	vláda
-	-	kIx~	-
tyranis	tyranis	k1gFnSc4	tyranis
<g/>
,	,	kIx,	,
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
Athény	Athéna	k1gFnPc1	Athéna
významný	významný	k2eAgInSc1d1	významný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
budovat	budovat	k5eAaImF	budovat
chrám	chrám	k1gInSc4	chrám
na	na	k7c4	na
Akropoli	Akropole	k1gFnSc4	Akropole
<g/>
,	,	kIx,	,
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
ochranné	ochranný	k2eAgFnSc2d1	ochranná
bohyně	bohyně	k1gFnSc2	bohyně
Athéně	Athéna	k1gFnSc3	Athéna
(	(	kIx(	(
<g/>
hekatonpedon	hekatonpedon	k1gInSc1	hekatonpedon
-	-	kIx~	-
stostopý	stostopý	k2eAgInSc1d1	stostopý
chrám	chrám	k1gInSc1	chrám
-	-	kIx~	-
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
sto	sto	k4xCgNnSc4	sto
stop	stopa	k1gFnPc2	stopa
=	=	kIx~	=
asi	asi	k9	asi
33	[number]	k4	33
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
přestavět	přestavět	k5eAaPmF	přestavět
Periklés	Periklés	k1gInSc4	Periklés
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Parthenón	Parthenón	k1gInSc1	Parthenón
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc2	vrchol
demokracie	demokracie	k1gFnPc1	demokracie
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
Athény	Athéna	k1gFnPc4	Athéna
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Periklovy	Periklův	k2eAgFnSc2d1	Periklova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
"	"	kIx"	"
<g/>
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
období	období	k1gNnSc6	období
<g/>
"	"	kIx"	"
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
působil	působit	k5eAaImAgMnS	působit
filozof	filozof	k1gMnSc1	filozof
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
a	a	k8xC	a
představitelé	představitel	k1gMnPc1	představitel
antické	antický	k2eAgFnSc2d1	antická
tragédie	tragédie	k1gFnSc2	tragédie
jako	jako	k8xS	jako
např.	např.	kA	např.
Sofoklés	Sofoklés	k1gInSc1	Sofoklés
<g/>
,	,	kIx,	,
Aischylos	Aischylos	k1gInSc1	Aischylos
nebo	nebo	k8xC	nebo
Eurípidés	Eurípidés	k1gInSc1	Eurípidés
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Periklově	Periklův	k2eAgFnSc6d1	Periklova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
429	[number]	k4	429
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
dominantní	dominantní	k2eAgNnSc1d1	dominantní
postavení	postavení	k1gNnSc1	postavení
Athén	Athéna	k1gFnPc2	Athéna
v	v	k7c6	v
Egejské	egejský	k2eAgFnSc6d1	Egejská
oblasti	oblast	k1gFnSc6	oblast
zhroutilo	zhroutit	k5eAaPmAgNnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Aristokracií	aristokracie	k1gFnSc7	aristokracie
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
Sparta	Sparta	k1gFnSc1	Sparta
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
peloponéští	peloponéský	k2eAgMnPc1d1	peloponéský
spojenci	spojenec	k1gMnPc1	spojenec
porazili	porazit	k5eAaPmAgMnP	porazit
Athény	Athéna	k1gFnPc4	Athéna
v	v	k7c6	v
peloponéské	peloponéský	k2eAgFnSc6d1	Peloponéská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
Athény	Athéna	k1gFnPc1	Athéna
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
významným	významný	k2eAgNnSc7d1	významné
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
nikoli	nikoli	k9	nikoli
politickou	politický	k2eAgFnSc7d1	politická
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
úpadku	úpadek	k1gInSc3	úpadek
ovšem	ovšem	k9	ovšem
nedošlo	dojít	k5eNaPmAgNnS	dojít
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
dobou	doba	k1gFnSc7	doba
velkých	velký	k2eAgMnPc2d1	velký
filozofů	filozof	k1gMnPc2	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
podmanili	podmanit	k5eAaPmAgMnP	podmanit
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc4	Athéna
ušetřili	ušetřit	k5eAaPmAgMnP	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
římští	římský	k2eAgMnPc1d1	římský
císaři	císař	k1gMnPc1	císař
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
<g/>
,	,	kIx,	,
věnovali	věnovat	k5eAaImAgMnP	věnovat
Athénám	Athéna	k1gFnPc3	Athéna
četné	četný	k2eAgFnPc1d1	četná
nové	nový	k2eAgFnPc4d1	nová
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Hadrián	Hadrián	k1gMnSc1	Hadrián
např.	např.	kA	např.
dokončil	dokončit	k5eAaPmAgMnS	dokončit
chrám	chrám	k1gInSc4	chrám
Dia	Dia	k1gFnSc2	Dia
Olympského	olympský	k2eAgInSc2d1	olympský
nebo	nebo	k8xC	nebo
bohatý	bohatý	k2eAgMnSc1d1	bohatý
římský	římský	k2eAgMnSc1d1	římský
občan	občan	k1gMnSc1	občan
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
Herodes	Herodes	k1gMnSc1	Herodes
Attikos	Attikos	k1gMnSc1	Attikos
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
po	po	k7c6	po
sobě	se	k3xPyFc3	se
nazvané	nazvaný	k2eAgNnSc4d1	nazvané
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
Heroda	Herod	k1gMnSc2	Herod
Attika	Attika	k1gFnSc1	Attika
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
ovšem	ovšem	k9	ovšem
řeckou	řecký	k2eAgFnSc4d1	řecká
kulturu	kultura	k1gFnSc4	kultura
podporovali	podporovat	k5eAaImAgMnP	podporovat
a	a	k8xC	a
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gFnSc1	jejich
nadvláda	nadvláda	k1gFnSc1	nadvláda
vedle	vedle	k6eAd1	vedle
možná	možná	k9	možná
trochu	trochu	k6eAd1	trochu
paradoxně	paradoxně	k6eAd1	paradoxně
ke	k	k7c3	k
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
rozmachu	rozmach	k1gInSc3	rozmach
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
prosperitě	prosperita	k1gFnSc3	prosperita
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Germanicus	Germanicus	k1gMnSc1	Germanicus
<g/>
,	,	kIx,	,
císaři	císař	k1gMnPc1	císař
Hadrián	Hadrián	k1gMnSc1	Hadrián
a	a	k8xC	a
Nero	Nero	k1gMnSc1	Nero
Athény	Athéna	k1gFnSc2	Athéna
navštívili	navštívit	k5eAaPmAgMnP	navštívit
a	a	k8xC	a
obdivovali	obdivovat	k5eAaImAgMnP	obdivovat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Hadrián	Hadrián	k1gMnSc1	Hadrián
zde	zde	k6eAd1	zde
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
novou	nový	k2eAgFnSc4d1	nová
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
,	,	kIx,	,
budoval	budovat	k5eAaImAgMnS	budovat
zde	zde	k6eAd1	zde
akvadukty	akvadukt	k1gInPc4	akvadukt
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc4	lázeň
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
jiné	jiný	k2eAgFnPc4d1	jiná
veřejné	veřejný	k2eAgFnPc4d1	veřejná
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Athéňan	Athéňan	k1gMnSc1	Athéňan
Herodes	Herodes	k1gMnSc1	Herodes
Attikos	Attikos	k1gMnSc1	Attikos
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
římským	římský	k2eAgMnSc7d1	římský
senátorem	senátor	k1gMnSc7	senátor
a	a	k8xC	a
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
pod	pod	k7c7	pod
Akropolí	Akropole	k1gFnSc7	Akropole
postavil	postavit	k5eAaPmAgMnS	postavit
velké	velký	k2eAgNnSc1d1	velké
divadlo	divadlo	k1gNnSc1	divadlo
Odeón	Odeón	k1gMnSc1	Odeón
Heroda	Herod	k1gMnSc2	Herod
Attika	Attika	k1gFnSc1	Attika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
stojí	stát	k5eAaImIp3nS	stát
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Řecko	Řecko	k1gNnSc1	Řecko
i	i	k8xC	i
s	s	k7c7	s
Athénami	Athéna	k1gFnPc7	Athéna
vyrabovali	vyrabovat	k5eAaPmAgMnP	vyrabovat
Gótové	Gót	k1gMnPc1	Gót
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Hunové	Hun	k1gMnPc1	Hun
<g/>
.	.	kIx.	.
</s>
<s>
Řekům	Řek	k1gMnPc3	Řek
se	se	k3xPyFc4	se
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
nedařilo	dařit	k5eNaImAgNnS	dařit
bránit	bránit	k5eAaImF	bránit
proti	proti	k7c3	proti
barbarům	barbar	k1gMnPc3	barbar
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
uchytilo	uchytit	k5eAaPmAgNnS	uchytit
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
392	[number]	k4	392
uznané	uznaný	k2eAgNnSc1d1	uznané
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
pohanský	pohanský	k2eAgInSc1d1	pohanský
význam	význam	k1gInSc1	význam
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
se	se	k3xPyFc4	se
Athény	Athéna	k1gFnSc2	Athéna
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
Řeckem	Řecko	k1gNnSc7	Řecko
stávají	stávat	k5eAaImIp3nP	stávat
součástí	součást	k1gFnSc7	součást
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgFnPc1d1	starověká
dějiny	dějiny	k1gFnPc1	dějiny
Athén	Athéna	k1gFnPc2	Athéna
končí	končit	k5eAaImIp3nP	končit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
VI	VI	kA	VI
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Pohanské	pohanský	k2eAgNnSc1d1	pohanské
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
definitivně	definitivně	k6eAd1	definitivně
zapomenuto	zapomenout	k5eAaPmNgNnS	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
Řecko	Řecko	k1gNnSc1	Řecko
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
a	a	k8xC	a
vyspělým	vyspělý	k2eAgFnPc3d1	vyspělá
částem	část	k1gFnPc3	část
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
poloostrova	poloostrov	k1gInSc2	poloostrov
Attika	Attika	k1gFnSc1	Attika
<g/>
,	,	kIx,	,
ostatně	ostatně	k6eAd1	ostatně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Athény	Athéna	k1gFnSc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
princip	princip	k1gInSc1	princip
fungoval	fungovat	k5eAaImAgInS	fungovat
i	i	k9	i
v	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
také	také	k6eAd1	také
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nastal	nastat	k5eAaPmAgInS	nastat
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
rozkvět	rozkvět	k1gInSc1	rozkvět
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc4d1	kulturní
funkci	funkce	k1gFnSc4	funkce
však	však	k9	však
převzala	převzít	k5eAaPmAgFnS	převzít
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
napadli	napadnout	k5eAaPmAgMnP	napadnout
Řecko	Řecko	k1gNnSc4	Řecko
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
i	i	k9	i
Athény	Athéna	k1gFnPc4	Athéna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neusadili	usadit	k5eNaPmAgMnP	usadit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
na	na	k7c4	na
poloostrov	poloostrov	k1gInSc4	poloostrov
Peloponés	Peloponés	k1gInSc1	Peloponés
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
žili	žít	k5eAaImAgMnP	žít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
i	i	k9	i
následkem	následkem	k7c2	následkem
slovanských	slovanský	k2eAgInPc2d1	slovanský
výpadů	výpad	k1gInPc2	výpad
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byli	být	k5eAaImAgMnP	být
již	již	k6eAd1	již
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
)	)	kIx)	)
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nevýznamné	významný	k2eNgNnSc1d1	nevýznamné
městečko	městečko	k1gNnSc1	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rozmach	rozmach	k1gInSc1	rozmach
Athén	Athéna	k1gFnPc2	Athéna
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k6eAd1	až
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
vracelo	vracet	k5eAaImAgNnS	vracet
řecké	řecký	k2eAgNnSc1d1	řecké
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
starověká	starověký	k2eAgFnSc1d1	starověká
agora	agora	k1gFnSc1	agora
a	a	k8xC	a
postaveny	postaven	k2eAgInPc1d1	postaven
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
byzantské	byzantský	k2eAgNnSc1d1	byzantské
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ránou	Rána	k1gFnSc7	Rána
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc1	zničení
Byzance	Byzanc	k1gFnSc2	Byzanc
křižáky	křižák	k1gMnPc4	křižák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
udělali	udělat	k5eAaPmAgMnP	udělat
Athénské	athénský	k2eAgNnSc4d1	athénské
vojvodství	vojvodství	k1gNnSc4	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
Poloostrov	poloostrov	k1gInSc4	poloostrov
Atiku	Atika	k1gFnSc4	Atika
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Evropanů	Evropan	k1gMnPc2	Evropan
osídlují	osídlovat	k5eAaImIp3nP	osídlovat
Arvanité	Arvanitý	k2eAgFnPc1d1	Arvanitý
<g/>
,	,	kIx,	,
hovořících	hovořící	k2eAgInPc2d1	hovořící
albánským	albánský	k2eAgInSc7d1	albánský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
Arvanité	Arvanitý	k2eAgFnPc1d1	Arvanitý
byli	být	k5eAaImAgMnP	být
pravoslavného	pravoslavný	k2eAgNnSc2d1	pravoslavné
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
původní	původní	k2eAgMnPc1d1	původní
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
začali	začít	k5eAaPmAgMnP	začít
prohlašovat	prohlašovat	k5eAaImF	prohlašovat
za	za	k7c4	za
Řeky	Řek	k1gMnPc4	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Attičtí	Attický	k2eAgMnPc1d1	Attický
Řekové	Řek	k1gMnPc1	Řek
s	s	k7c7	s
Arvaniti	Arvaniti	k1gFnSc7	Arvaniti
splynuli	splynout	k5eAaPmAgMnP	splynout
a	a	k8xC	a
albanizovali	albanizovat	k5eAaBmAgMnP	albanizovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
opět	opět	k6eAd1	opět
malým	malý	k2eAgNnSc7d1	malé
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
mohamedáni	mohamedán	k1gMnPc1	mohamedán
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
si	se	k3xPyFc3	se
v	v	k7c6	v
Pantheonu	Pantheon	k1gInSc6	Pantheon
zřídili	zřídit	k5eAaPmAgMnP	zřídit
mešitu	mešita	k1gFnSc4	mešita
<g/>
,	,	kIx,	,
zničili	zničit	k5eAaPmAgMnP	zničit
chrám	chrám	k1gInSc4	chrám
bohyně	bohyně	k1gFnSc2	bohyně
Athény	Athéna	k1gFnSc2	Athéna
Nike	Nike	k1gFnSc2	Nike
a	a	k8xC	a
z	z	k7c2	z
Akropole	Akropole	k1gFnSc2	Akropole
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
pevnost	pevnost	k1gFnSc4	pevnost
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
Benátčanům	Benátčan	k1gMnPc3	Benátčan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Benátčané	Benátčan	k1gMnPc1	Benátčan
ostřelovali	ostřelovat	k5eAaImAgMnP	ostřelovat
Akropoli	Akropole	k1gFnSc4	Akropole
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
skladiště	skladiště	k1gNnSc4	skladiště
munice	munice	k1gFnSc2	munice
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vybuchl	vybuchnout	k5eAaPmAgInS	vybuchnout
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
zůstal	zůstat	k5eAaPmAgMnS	zůstat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
Řekové	Řek	k1gMnPc1	Řek
za	za	k7c2	za
války	válka	k1gFnSc2	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
dobyli	dobýt	k5eAaPmAgMnP	dobýt
tureckou	turecký	k2eAgFnSc4d1	turecká
pevnost	pevnost	k1gFnSc4	pevnost
na	na	k7c4	na
Akropoli	Akropole	k1gFnSc4	Akropole
a	a	k8xC	a
Turky	turek	k1gInPc4	turek
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
však	však	k9	však
byli	být	k5eAaImAgMnP	být
Turci	Turek	k1gMnPc1	Turek
zpátky	zpátky	k6eAd1	zpátky
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
potlačit	potlačit	k5eAaPmF	potlačit
řecké	řecký	k2eAgNnSc1d1	řecké
povstání	povstání	k1gNnSc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
Řecko	Řecko	k1gNnSc1	Řecko
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
Athény	Athéna	k1gFnPc4	Athéna
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgInPc2	který
odtáhli	odtáhnout	k5eAaPmAgMnP	odtáhnout
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
připraveny	připravit	k5eAaPmNgFnP	připravit
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
období	období	k1gNnSc4	období
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
řecký	řecký	k2eAgMnSc1d1	řecký
král	král	k1gMnSc1	král
Otto	Otto	k1gMnSc1	Otto
I.	I.	kA	I.
přemístil	přemístit	k5eAaPmAgMnS	přemístit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
z	z	k7c2	z
Nafplia	Nafplium	k1gNnSc2	Nafplium
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měly	mít	k5eAaImAgInP	mít
snad	snad	k9	snad
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
5-11	[number]	k4	5-11
tisíci	tisíc	k4xCgInPc7	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
tady	tady	k6eAd1	tady
hlavně	hlavně	k9	hlavně
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Arvanité	Arvanitý	k2eAgFnPc1d1	Arvanitý
a	a	k8xC	a
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
usazovat	usazovat	k5eAaImF	usazovat
úřady	úřad	k1gInPc4	úřad
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
instituce	instituce	k1gFnPc4	instituce
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
Řekové	Řek	k1gMnPc1	Řek
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgInS	postavit
se	se	k3xPyFc4	se
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
Helénský	helénský	k2eAgInSc1d1	helénský
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Athénská	athénský	k2eAgFnSc1d1	Athénská
městská	městský	k2eAgFnSc1d1	městská
hala	hala	k1gFnSc1	hala
<g/>
,	,	kIx,	,
Zappeion	Zappeion	k1gInSc1	Zappeion
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Helénské	helénský	k2eAgNnSc1d1	helénské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
tak	tak	k9	tak
byly	být	k5eAaImAgFnP	být
Athény	Athéna	k1gFnPc1	Athéna
opět	opět	k6eAd1	opět
osídlené	osídlený	k2eAgFnPc1d1	osídlená
převážně	převážně	k6eAd1	převážně
řecky	řecky	k6eAd1	řecky
hovořícím	hovořící	k2eAgNnSc7d1	hovořící
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgNnSc1d1	definitivní
pořečtění	pořečtění	k1gNnSc1	pořečtění
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
řecko-turecká	řeckourecký	k2eAgFnSc1d1	řecko-turecká
výměna	výměna	k1gFnSc1	výměna
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yRgNnSc1	který
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
Anatólie	Anatólie	k1gFnSc2	Anatólie
přišlo	přijít	k5eAaPmAgNnS	přijít
přibližně	přibližně	k6eAd1	přibližně
2.000	[number]	k4	2.000
000	[number]	k4	000
etnických	etnický	k2eAgMnPc2d1	etnický
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
000	[number]	k4	000
usadilo	usadit	k5eAaPmAgNnS	usadit
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
a	a	k8xC	a
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založili	založit	k5eAaPmAgMnP	založit
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Athén	Athéna	k1gFnPc2	Athéna
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
jejich	jejich	k3xOp3gFnSc2	jejich
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Nea	Nea	k1gMnSc4	Nea
Ionia	Ionius	k1gMnSc4	Ionius
<g/>
,	,	kIx,	,
Nikea	Nikeus	k1gMnSc4	Nikeus
<g/>
,	,	kIx,	,
Nea	Nea	k1gMnSc4	Nea
Smyrni	Smyrni	k1gMnSc4	Smyrni
<g/>
,	,	kIx,	,
Nea	Nea	k1gMnSc4	Nea
Chalkidona	Chalkidon	k1gMnSc4	Chalkidon
<g/>
,	,	kIx,	,
Nea	Nea	k1gMnSc4	Nea
Erythrea	Erythreus	k1gMnSc4	Erythreus
<g/>
,	,	kIx,	,
Nea	Nea	k1gMnSc4	Nea
Filadelfia	Filadelfius	k1gMnSc4	Filadelfius
<g/>
,	,	kIx,	,
Kalithea	Kalitheus	k1gMnSc4	Kalitheus
nebo	nebo	k8xC	nebo
Argyrupoli	Argyrupole	k1gFnSc4	Argyrupole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
obdobích	období	k1gNnPc6	období
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
Řeků	Řek	k1gMnPc2	Řek
většinou	většinou	k6eAd1	většinou
ze	z	k7c2	z
středního	střední	k2eAgNnSc2d1	střední
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
poloostrova	poloostrov	k1gInSc2	poloostrov
Peloponés	Peloponés	k1gInSc4	Peloponés
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
za	za	k7c7	za
lepším	dobrý	k2eAgInSc7d2	lepší
životem	život	k1gInSc7	život
a	a	k8xC	a
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
milionové	milionový	k2eAgNnSc1d1	milionové
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
etnických	etnický	k2eAgMnPc2d1	etnický
Řeků	Řek	k1gMnPc2	Řek
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
sovětských	sovětský	k2eAgFnPc2d1	sovětská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Řeků	Řek	k1gMnPc2	Řek
dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
na	na	k7c4	na
léto	léto	k1gNnSc4	léto
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
kterých	který	k3yIgInPc2	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
města	město	k1gNnSc2	město
Athény	Athéna	k1gFnSc2	Athéna
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
kosmopolitním	kosmopolitní	k2eAgInSc7d1	kosmopolitní
jako	jako	k8xC	jako
Athény	Athéna	k1gFnPc4	Athéna
samotné	samotný	k2eAgFnPc4d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zde	zde	k6eAd1	zde
obdivovat	obdivovat	k5eAaImF	obdivovat
zachovalé	zachovalý	k2eAgFnPc1d1	zachovalá
původní	původní	k2eAgFnPc1d1	původní
starořecké	starořecký	k2eAgFnPc1d1	starořecká
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
památky	památka	k1gFnPc1	památka
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
byzantské	byzantský	k2eAgFnSc2d1	byzantská
svatyně	svatyně	k1gFnSc2	svatyně
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
zde	zde	k6eAd1	zde
můžete	moct	k5eAaImIp2nP	moct
sledovat	sledovat	k5eAaImF	sledovat
vývoj	vývoj	k1gInSc4	vývoj
moderní	moderní	k2eAgFnSc2d1	moderní
řecké	řecký	k2eAgFnSc2d1	řecká
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
budov	budova	k1gFnPc2	budova
postavených	postavený	k2eAgFnPc2d1	postavená
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
jako	jako	k9	jako
kopie	kopie	k1gFnSc1	kopie
antických	antický	k2eAgFnPc2d1	antická
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
a	a	k8xC	a
parky	park	k1gInPc1	park
obohacují	obohacovat	k5eAaImIp3nP	obohacovat
sochy	socha	k1gFnPc1	socha
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
filozofů	filozof	k1gMnPc2	filozof
<g/>
,	,	kIx,	,
hrdinů	hrdina	k1gMnPc2	hrdina
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
samostatnost	samostatnost	k1gFnSc4	samostatnost
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgFnPc2	tento
soch	socha	k1gFnPc2	socha
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
architekturou	architektura	k1gFnSc7	architektura
athénského	athénský	k2eAgNnSc2d1	athénské
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
Olympijský	olympijský	k2eAgInSc1d1	olympijský
stadión	stadión	k1gInSc1	stadión
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavými	zajímavý	k2eAgMnPc7d1	zajímavý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
domky	domek	k1gInPc4	domek
běžných	běžný	k2eAgMnPc2d1	běžný
obyvatel	obyvatel	k1gMnPc2	obyvatel
postavené	postavený	k2eAgInPc1d1	postavený
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
jezdí	jezdit	k5eAaImIp3nP	jezdit
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
hromadné	hromadný	k2eAgFnSc2d1	hromadná
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
i	i	k8xC	i
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Athénské	athénský	k2eAgNnSc1d1	athénské
metro	metro	k1gNnSc1	metro
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
okolo	okolo	k6eAd1	okolo
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
(	(	kIx(	(
<g/>
evropské	evropský	k2eAgNnSc4d1	Evropské
letiště	letiště	k1gNnSc4	letiště
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
také	také	k9	také
přístav	přístav	k1gInSc1	přístav
Pireus	Pireus	k1gInSc1	Pireus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
pevninského	pevninský	k2eAgNnSc2d1	pevninské
Řecka	Řecko	k1gNnSc2	Řecko
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
ostrovy	ostrov	k1gInPc7	ostrov
i	i	k8xC	i
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k9	také
přístavem	přístav	k1gInSc7	přístav
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
světová	světový	k2eAgFnSc1d1	světová
metropole	metropole	k1gFnSc1	metropole
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
nespočet	nespočet	k1gInSc1	nespočet
antických	antický	k2eAgFnPc2d1	antická
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
příjmů	příjem	k1gInPc2	příjem
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Athény	Athéna	k1gFnPc1	Athéna
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
přístav	přístav	k1gInSc4	přístav
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
linky	linka	k1gFnPc4	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
například	například	k6eAd1	například
i	i	k9	i
k	k	k7c3	k
přístavu	přístav	k1gInSc3	přístav
Pireus	Pireus	k1gMnSc1	Pireus
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
aglomerace	aglomerace	k1gFnSc1	aglomerace
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
38	[number]	k4	38
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dohromady	dohromady	k6eAd1	dohromady
žije	žít	k5eAaImIp3nS	žít
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
pouhých	pouhý	k2eAgNnPc6d1	pouhé
40	[number]	k4	40
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
Albánců	Albánec	k1gMnPc2	Albánec
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
zde	zde	k6eAd1	zde
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c4	na
4	[number]	k4	4
000	[number]	k4	000
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
14	[number]	k4	14
000	[number]	k4	000
taxíků	taxík	k1gInPc2	taxík
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelům	obyvatel	k1gMnPc3	obyvatel
města	město	k1gNnSc2	město
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
nepříliš	příliš	k6eNd1	příliš
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
bůh	bůh	k1gMnSc1	bůh
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
veselí	veselit	k5eAaImIp3nS	veselit
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
a	a	k8xC	a
bohyně	bohyně	k1gFnSc1	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
můžete	moct	k5eAaImIp2nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
140	[number]	k4	140
divadel	divadlo	k1gNnPc2	divadlo
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
čtvrť	čtvrť	k1gFnSc1	čtvrť
Kipseli	Kipsel	k1gInSc6	Kipsel
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Athén	Athéna	k1gFnPc2	Athéna
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Hong-Kongu	Hong-Kong	k1gInSc6	Hong-Kong
nejhustěji	husto	k6eAd3	husto
osídlené	osídlený	k2eAgNnSc4d1	osídlené
území	území	k1gNnSc4	území
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejhlučnější	hlučný	k2eAgNnSc1d3	nejhlučnější
město	město	k1gNnSc1	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc4	Athéna
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
města	město	k1gNnPc4	město
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
poměrem	poměr	k1gInSc7	poměr
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
-	-	kIx~	-
oficiálně	oficiálně	k6eAd1	oficiálně
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
čtvereční	čtvereční	k2eAgInPc4d1	čtvereční
metry	metr	k1gInPc4	metr
zeleně	zeleň	k1gFnSc2	zeleň
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Athény	Athéna	k1gFnPc1	Athéna
hostily	hostit	k5eAaImAgFnP	hostit
dvakrát	dvakrát	k6eAd1	dvakrát
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
:	:	kIx,	:
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
a	a	k8xC	a
XXVIII	XXVIII	kA	XXVIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
byly	být	k5eAaImAgFnP	být
Athény	Athéna	k1gFnPc1	Athéna
dějištěm	dějiště	k1gNnSc7	dějiště
tzv.	tzv.	kA	tzv.
Meziher	mezihra	k1gFnPc2	mezihra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
neúspěšné	úspěšný	k2eNgFnPc4d1	neúspěšná
hry	hra	k1gFnPc4	hra
v	v	k7c4	v
Saint	Saint	k1gInSc4	Saint
Louis	Louis	k1gMnSc1	Louis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
2004	[number]	k4	2004
přinesly	přinést	k5eAaPmAgFnP	přinést
městu	město	k1gNnSc3	město
<g/>
:	:	kIx,	:
65	[number]	k4	65
000	[number]	k4	000
nových	nový	k2eAgNnPc2d1	nové
stálých	stálý	k2eAgNnPc2d1	stálé
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
Bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
120	[number]	k4	120
km	km	kA	km
nových	nový	k2eAgFnPc2d1	nová
silnic	silnice	k1gFnPc2	silnice
Bylo	být	k5eAaImAgNnS	být
vysazeno	vysadit	k5eAaPmNgNnS	vysadit
290	[number]	k4	290
000	[number]	k4	000
nových	nový	k2eAgInPc2d1	nový
stromů	strom	k1gInPc2	strom
Bylo	být	k5eAaImAgNnS	být
postaveno	postaven	k2eAgNnSc4d1	postaveno
nové	nový	k2eAgNnSc4d1	nové
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
linka	linka	k1gFnSc1	linka
athénského	athénský	k2eAgNnSc2d1	athénské
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
postaveno	postavit	k5eAaPmNgNnS	postavit
24	[number]	k4	24
km	km	kA	km
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tratí	trať	k1gFnPc2	trať
Nárůst	nárůst	k1gInSc1	nárůst
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
O	o	k7c4	o
35	[number]	k4	35
%	%	kIx~	%
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
kvalita	kvalita	k1gFnSc1	kvalita
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
budova	budova	k1gFnSc1	budova
Zapion	Zapion	k1gInSc1	Zapion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
výstavy	výstava	k1gFnPc1	výstava
a	a	k8xC	a
kongresy	kongres	k1gInPc1	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
olympiádě	olympiáda	k1gFnSc6	olympiáda
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
tiskové	tiskový	k2eAgNnSc1d1	tiskové
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
Olympeion	Olympeion	k1gInSc1	Olympeion
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
104	[number]	k4	104
korintských	korintský	k2eAgInPc2d1	korintský
sloupů	sloup	k1gInPc2	sloup
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
jen	jen	k9	jen
patnáct	patnáct	k4xCc4	patnáct
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
17	[number]	k4	17
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
délku	délka	k1gFnSc4	délka
chrám	chrám	k1gInSc1	chrám
čítal	čítat	k5eAaImAgInS	čítat
asi	asi	k9	asi
96	[number]	k4	96
m	m	kA	m
a	a	k8xC	a
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
1900	[number]	k4	1900
let	léto	k1gNnPc2	léto
starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
římskou	římský	k2eAgFnSc7d1	římská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
císař	císař	k1gMnSc1	císař
Hadrián	Hadrián	k1gMnSc1	Hadrián
rozšířit	rozšířit	k5eAaPmF	rozšířit
město	město	k1gNnSc4	město
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
jednu	jeden	k4xCgFnSc4	jeden
městskou	městský	k2eAgFnSc4d1	městská
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
Starým	Starý	k1gMnSc7	Starý
a	a	k8xC	a
Novým	nový	k2eAgNnSc7d1	nové
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Akropoli	Akropole	k1gFnSc3	Akropole
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
Théseovo	Théseův	k2eAgNnSc4d1	Théseovo
<g/>
,	,	kIx,	,
staré	starý	k2eAgNnSc4d1	staré
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Hadriánovo	Hadriánův	k2eAgNnSc1d1	Hadriánovo
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
Théseovo	Théseův	k2eAgNnSc1d1	Théseovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dionýsově	Dionýsův	k2eAgNnSc6d1	Dionýsovo
divadle	divadlo	k1gNnSc6	divadlo
pod	pod	k7c7	pod
Akropolí	Akropole	k1gFnSc7	Akropole
(	(	kIx(	(
<g/>
nejstarším	starý	k2eAgNnSc6d3	nejstarší
známém	známý	k2eAgNnSc6d1	známé
divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
bývaly	bývat	k5eAaImAgFnP	bývat
premiéry	premiéra	k1gFnPc1	premiéra
Aischylových	Aischylový	k2eAgFnPc2d1	Aischylový
<g/>
,	,	kIx,	,
Aristofanových	Aristofanův	k2eAgFnPc2d1	Aristofanova
<g/>
,	,	kIx,	,
Euripidových	Euripidový	k2eAgFnPc2d1	Euripidový
a	a	k8xC	a
Sofoklových	Sofoklův	k2eAgFnPc2d1	Sofoklova
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
každoročně	každoročně	k6eAd1	každoročně
uctívali	uctívat	k5eAaImAgMnP	uctívat
tancem	tanec	k1gInSc7	tanec
a	a	k8xC	a
zpěvem	zpěv	k1gInSc7	zpěv
boha	bůh	k1gMnSc2	bůh
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
veselí	veselí	k1gNnSc2	veselí
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
534	[number]	k4	534
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
těchto	tento	k3xDgFnPc2	tento
oslav	oslava	k1gFnPc2	oslava
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jistý	jistý	k2eAgMnSc1d1	jistý
Thespis	Thespis	k1gMnSc1	Thespis
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgInS	postavit
vedle	vedle	k7c2	vedle
jednoho	jeden	k4xCgInSc2	jeden
sboru	sbor	k1gInSc2	sbor
jiný	jiný	k2eAgMnSc1d1	jiný
-	-	kIx~	-
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
dialog	dialog	k1gInSc1	dialog
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dobách	doba	k1gFnPc6	doba
sledovali	sledovat	k5eAaImAgMnP	sledovat
diváci	divák	k1gMnPc1	divák
hru	hra	k1gFnSc4	hra
ještě	ještě	k6eAd1	ještě
prostě	prostě	k9	prostě
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Pojalo	pojmout	k5eAaPmAgNnS	pojmout
až	až	k9	až
17	[number]	k4	17
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
zbudovány	zbudován	k2eAgFnPc4d1	zbudována
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
lavice	lavice	k1gFnPc4	lavice
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
330	[number]	k4	330
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
kamennými	kamenný	k2eAgInPc7d1	kamenný
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
dostalo	dostat	k5eAaPmAgNnS	dostat
přibližně	přibližně	k6eAd1	přibližně
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
doby	doba	k1gFnSc2	doba
panování	panování	k1gNnSc2	panování
císaře	císař	k1gMnSc2	císař
Nerona	Nero	k1gMnSc2	Nero
bylo	být	k5eAaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohly	moct	k5eAaImAgFnP	moct
konat	konat	k5eAaImF	konat
gladiátorské	gladiátorský	k2eAgFnPc4d1	gladiátorská
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
také	také	k9	také
reliéfy	reliéf	k1gInPc4	reliéf
na	na	k7c6	na
předscéně	předscéna	k1gFnSc6	předscéna
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
mytické	mytický	k2eAgInPc1d1	mytický
výjevy	výjev	k1gInPc1	výjev
<g/>
:	:	kIx,	:
vlevo	vlevo	k6eAd1	vlevo
zrození	zrození	k1gNnSc1	zrození
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
a	a	k8xC	a
vedle	vedle	k6eAd1	vedle
oběť	oběť	k1gFnSc4	oběť
bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
vpravo	vpravo	k6eAd1	vpravo
uctívání	uctívání	k1gNnSc1	uctívání
boha	bůh	k1gMnSc2	bůh
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
symbolem	symbol	k1gInSc7	symbol
a	a	k8xC	a
dominantou	dominanta	k1gFnSc7	dominanta
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Akropolis	Akropolis	k1gFnSc4	Akropolis
dominantou	dominanta	k1gFnSc7	dominanta
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Akropole	Akropole	k1gFnSc1	Akropole
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Akropolis	Akropolis	k1gFnSc1	Akropolis
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
náboženským	náboženský	k2eAgNnSc7d1	náboženské
centrem	centrum	k1gNnSc7	centrum
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
přirozeným	přirozený	k2eAgInSc7d1	přirozený
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
opírali	opírat	k5eAaImAgMnP	opírat
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
chtěli	chtít	k5eAaImAgMnP	chtít
Athény	Athéna	k1gFnSc2	Athéna
ovládat	ovládat	k5eAaImF	ovládat
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
opevněna	opevnit	k5eAaPmNgFnS	opevnit
již	již	k6eAd1	již
předchůdci	předchůdce	k1gMnSc3	předchůdce
Řeků	Řek	k1gMnPc2	Řek
v	v	k7c6	v
Attice	Attika	k1gFnSc6	Attika
-	-	kIx~	-
Pelasgy	Pelasga	k1gFnSc2	Pelasga
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
jejich	jejich	k3xOp3gNnSc2	jejich
mohutného	mohutný	k2eAgNnSc2d1	mohutné
opevnění	opevnění	k1gNnSc2	opevnění
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rozmach	rozmach	k1gInSc1	rozmach
stavební	stavební	k2eAgFnSc2d1	stavební
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
Akropoli	Akropole	k1gFnSc6	Akropole
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
Athénské	athénský	k2eAgFnSc2d1	Athénská
moci	moc	k1gFnSc2	moc
-	-	kIx~	-
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Periklovy	Periklův	k2eAgFnSc2d1	Periklova
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterými	který	k3yIgMnPc7	který
musíme	muset	k5eAaImIp1nP	muset
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
úžasu	úžas	k1gInSc6	úžas
ještě	ještě	k6eAd1	ještě
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rozvalinách	rozvalina	k1gFnPc6	rozvalina
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
nás	my	k3xPp1nPc4	my
nejen	nejen	k6eAd1	nejen
svoji	svůj	k3xOyFgFnSc4	svůj
mohutností	mohutnost	k1gFnSc7	mohutnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vyspělým	vyspělý	k2eAgInSc7d1	vyspělý
estetickým	estetický	k2eAgInSc7d1	estetický
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Akropoli	Akropole	k1gFnSc6	Akropole
tehdy	tehdy	k6eAd1	tehdy
stálo	stát	k5eAaImAgNnS	stát
podivuhodné	podivuhodný	k2eAgNnSc1d1	podivuhodné
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
Pallas	Pallas	k1gMnSc1	Pallas
Athény	Athéna	k1gFnPc4	Athéna
<g/>
,	,	kIx,	,
ochránkyně	ochránkyně	k1gFnPc4	ochránkyně
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Geniální	geniální	k2eAgMnSc1d1	geniální
sochař	sochař	k1gMnSc1	sochař
Feidiás	Feidiása	k1gFnPc2	Feidiása
ji	on	k3xPp3gFnSc4	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Athéňané	Athéňan	k1gMnPc1	Athéňan
jej	on	k3xPp3gMnSc4	on
obvinili	obvinit	k5eAaPmAgMnP	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpronevěřil	zpronevěřit	k5eAaPmAgMnS	zpronevěřit
zlato	zlato	k1gNnSc4	zlato
určené	určený	k2eAgFnSc2d1	určená
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
výzdobu	výzdoba	k1gFnSc4	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
umělec	umělec	k1gMnSc1	umělec
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
dílnu	dílna	k1gFnSc4	dílna
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
prokazatelně	prokazatelně	k6eAd1	prokazatelně
pravé	pravý	k2eAgInPc4d1	pravý
zbytky	zbytek	k1gInPc4	zbytek
našli	najít	k5eAaPmAgMnP	najít
archeologové	archeolog	k1gMnPc1	archeolog
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Planina	planina	k1gFnSc1	planina
na	na	k7c6	na
Akropoli	Akropole	k1gFnSc6	Akropole
je	být	k5eAaImIp3nS	být
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Parthenon	Parthenon	k1gInSc1	Parthenon
-	-	kIx~	-
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
budovat	budovat	k5eAaImF	budovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
447	[number]	k4	447
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
Parthenónu	Parthenón	k1gInSc2	Parthenón
byla	být	k5eAaImAgFnS	být
obří	obří	k2eAgFnSc1d1	obří
socha	socha	k1gFnSc1	socha
Athény	Athéna	k1gFnSc2	Athéna
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
11	[number]	k4	11
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
měl	mít	k5eAaImAgInS	mít
rozměry	rozměr	k1gInPc4	rozměr
asi	asi	k9	asi
70	[number]	k4	70
x	x	k?	x
31	[number]	k4	31
m.	m.	k?	m.
Při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Akropole	Akropole	k1gFnSc2	Akropole
Benátčany	Benátčan	k1gMnPc4	Benátčan
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zasažení	zasažení	k1gNnSc3	zasažení
Parthenónu	Parthenón	k1gInSc2	Parthenón
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
Turci	Turek	k1gMnPc1	Turek
zřídili	zřídit	k5eAaPmAgMnP	zřídit
prachárnu	prachárna	k1gFnSc4	prachárna
<g/>
,	,	kIx,	,
dělovou	dělový	k2eAgFnSc7d1	dělová
koulí	koule	k1gFnSc7	koule
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
explozi	exploze	k1gFnSc6	exploze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrám	chrám	k1gInSc4	chrám
značně	značně	k6eAd1	značně
poničila	poničit	k5eAaPmAgFnS	poničit
<g/>
.	.	kIx.	.
</s>
<s>
Erechteion	Erechteion	k1gInSc1	Erechteion
-	-	kIx~	-
zbudován	zbudován	k2eAgMnSc1d1	zbudován
v	v	k7c6	v
letech	let	k1gInPc6	let
421-406	[number]	k4	421-406
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
nejposvátnějším	posvátný	k2eAgInSc6d3	nejposvátnější
okrsku	okrsek	k1gInSc6	okrsek
Akropole	Akropole	k1gFnSc2	Akropole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
stály	stát	k5eAaImAgInP	stát
původně	původně	k6eAd1	původně
tři	tři	k4xCgInPc1	tři
samostatné	samostatný	k2eAgInPc1d1	samostatný
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
sloučeny	sloučit	k5eAaPmNgInP	sloučit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
jejich	jejich	k3xOp3gInSc1	jejich
původní	původní	k2eAgInSc1d1	původní
půdorys	půdorys	k1gInSc1	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
předsíň	předsíň	k1gFnSc4	předsíň
<g/>
,	,	kIx,	,
střechu	střecha	k1gFnSc4	střecha
které	který	k3yRgNnSc1	který
drží	držet	k5eAaImIp3nS	držet
šest	šest	k4xCc4	šest
soch	socha	k1gFnPc2	socha
půvabných	půvabný	k2eAgFnPc2d1	půvabná
dívek	dívka	k1gFnPc2	dívka
-	-	kIx~	-
karyatidy	karyatida	k1gFnSc2	karyatida
<g/>
.	.	kIx.	.
</s>
<s>
Propylaie	Propylaie	k1gFnSc1	Propylaie
-	-	kIx~	-
vstupní	vstupní	k2eAgNnSc1d1	vstupní
monumentální	monumentální	k2eAgNnSc1d1	monumentální
mramorové	mramorový	k2eAgNnSc1d1	mramorové
schodiště	schodiště	k1gNnSc1	schodiště
a	a	k8xC	a
brána	brána	k1gFnSc1	brána
s	s	k7c7	s
pěti	pět	k4xCc7	pět
vstupy	vstup	k1gInPc7	vstup
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
zavíraly	zavírat	k5eAaImAgFnP	zavírat
a	a	k8xC	a
Akropole	Akropole	k1gFnSc1	Akropole
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
na	na	k7c4	na
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nesou	nést	k5eAaImIp3nP	nést
jméno	jméno	k1gNnSc4	jméno
Boulého	Boulý	k1gMnSc2	Boulý
pevnostní	pevnostní	k2eAgFnSc1d1	pevnostní
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
jejího	její	k3xOp3gMnSc2	její
francouzského	francouzský	k2eAgMnSc2d1	francouzský
objevitele	objevitel	k1gMnSc2	objevitel
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Boulého	Boulý	k1gMnSc4	Boulý
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
na	na	k7c6	na
Akropoli	Akropole	k1gFnSc6	Akropole
-	-	kIx~	-
archeologické	archeologický	k2eAgNnSc1d1	Archeologické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
a	a	k8xC	a
co	co	k3yInSc4	co
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
muzea	muzeum	k1gNnPc4	muzeum
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
antikou	antika	k1gFnSc7	antika
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
tyto	tento	k3xDgFnPc1	tento
sochy	socha	k1gFnPc1	socha
<g/>
:	:	kIx,	:
Moschoforos	Moschoforosa	k1gFnPc2	Moschoforosa
-	-	kIx~	-
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nese	nést	k5eAaImIp3nS	nést
jalovici	jalovice	k1gFnSc4	jalovice
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
jemná	jemný	k2eAgFnSc1d1	jemná
soška	soška	k1gFnSc1	soška
vousatého	vousatý	k2eAgMnSc2d1	vousatý
mladého	mladý	k2eAgMnSc2d1	mladý
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nese	nést	k5eAaImIp3nS	nést
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
tele	tele	k1gNnSc1	tele
jako	jako	k8xS	jako
oběť	oběť	k1gFnSc1	oběť
bohyni	bohyně	k1gFnSc4	bohyně
Athéně	Athéna	k1gFnSc3	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
570	[number]	k4	570
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Karyatidy	karyatida	k1gFnSc2	karyatida
-	-	kIx~	-
panny	panna	k1gFnSc2	panna
<g/>
,	,	kIx,	,
podpírající	podpírající	k2eAgFnSc4d1	podpírající
střechu	střecha	k1gFnSc4	střecha
průčelí	průčelí	k1gNnSc2	průčelí
Erechtheionu	Erechtheion	k1gInSc2	Erechtheion
<g/>
.	.	kIx.	.
</s>
<s>
Erechtheus	Erechtheus	k1gInSc1	Erechtheus
-	-	kIx~	-
prastarý	prastarý	k2eAgMnSc1d1	prastarý
athénský	athénský	k2eAgMnSc1d1	athénský
bůh	bůh	k1gMnSc1	bůh
s	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
částí	část	k1gFnSc7	část
mužskou	mužský	k2eAgFnSc7d1	mužská
<g/>
,	,	kIx,	,
s	s	k7c7	s
dolní	dolní	k2eAgFnSc7d1	dolní
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hadího	hadí	k2eAgNnSc2d1	hadí
těla	tělo	k1gNnSc2	tělo
-	-	kIx~	-
symbol	symbol	k1gInSc4	symbol
původního	původní	k2eAgNnSc2d1	původní
autochtonního	autochtonní	k2eAgNnSc2d1	autochtonní
(	(	kIx(	(
<g/>
domácího	domácí	k2eAgNnSc2d1	domácí
<g/>
)	)	kIx)	)
božstva	božstvo	k1gNnSc2	božstvo
Centrum	centrum	k1gNnSc1	centrum
antických	antický	k2eAgFnPc2d1	antická
i	i	k8xC	i
dnešních	dnešní	k2eAgFnPc2d1	dnešní
Athén	Athéna	k1gFnPc2	Athéna
tvoří	tvořit	k5eAaImIp3nS	tvořit
starověká	starověký	k2eAgFnSc1d1	starověká
Agora	agora	k1gFnSc1	agora
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
pojem	pojem	k1gInSc1	pojem
agora	agora	k1gFnSc1	agora
znamená	znamenat	k5eAaImIp3nS	znamenat
tržiště	tržiště	k1gNnSc1	tržiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
jen	jen	k9	jen
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Stály	stát	k5eAaImAgFnP	stát
zde	zde	k6eAd1	zde
administrativní	administrativní	k2eAgFnPc1d1	administrativní
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc1	chrám
i	i	k8xC	i
soudy	soud	k1gInPc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgMnS	moct
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
najít	najít	k5eAaPmF	najít
veřejné	veřejný	k2eAgFnPc4d1	veřejná
služby	služba	k1gFnPc4	služba
i	i	k9	i
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
k	k	k7c3	k
běžnému	běžný	k2eAgInSc3d1	běžný
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
nakupovalo	nakupovat	k5eAaBmAgNnS	nakupovat
a	a	k8xC	a
prodávalo	prodávat	k5eAaImAgNnS	prodávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dělala	dělat	k5eAaImAgFnS	dělat
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
pomlouvala	pomlouvat	k5eAaImAgFnS	pomlouvat
vláda	vláda	k1gFnSc1	vláda
i	i	k8xC	i
bližní	bližní	k1gMnSc1	bližní
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
památky	památka	k1gFnPc1	památka
můžeme	moct	k5eAaImIp1nP	moct
datovat	datovat	k5eAaImF	datovat
až	až	k9	až
do	do	k7c2	do
neolitického	neolitický	k2eAgNnSc2d1	neolitické
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
obdivovat	obdivovat	k5eAaImF	obdivovat
můžeme	moct	k5eAaImIp1nP	moct
památky	památka	k1gFnPc1	památka
od	od	k7c2	od
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
až	až	k9	až
po	po	k7c4	po
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
Théseion-Héfaisteion	Théseion-Héfaisteion	k1gInSc1	Théseion-Héfaisteion
Tento	tento	k3xDgInSc4	tento
chrám	chrám	k1gInSc4	chrám
je	být	k5eAaImIp3nS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
bohu	bůh	k1gMnSc3	bůh
Héfaistovi	Héfaista	k1gMnSc3	Héfaista
a	a	k8xC	a
ochránkyni	ochránkyně	k1gFnSc3	ochránkyně
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bohyni	bohyně	k1gFnSc3	bohyně
Athéně	Athéna	k1gFnSc3	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vybudován	vybudován	k2eAgInSc1d1	vybudován
roku	rok	k1gInSc2	rok
449	[number]	k4	449
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejzachovalejším	zachovalý	k2eAgInSc7d3	nejzachovalejší
antickým	antický	k2eAgInSc7d1	antický
chrámem	chrám	k1gInSc7	chrám
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
agora	agora	k1gFnSc1	agora
Stojí	stát	k5eAaImIp3nS	stát
nedaleko	nedaleko	k7c2	nedaleko
Římské	římský	k2eAgFnSc2d1	římská
agory	agora	k1gFnSc2	agora
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
centrum	centrum	k1gNnSc1	centrum
Athén	Athéna	k1gFnPc2	Athéna
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
řecké	řecký	k2eAgFnSc2d1	řecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
částí	část	k1gFnSc7	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Theseion	Theseion	k1gInSc1	Theseion
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
Hefaisteion	Hefaisteion	k1gInSc1	Hefaisteion
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Stoa	stoa	k1gFnSc1	stoa
poikylé	poikylý	k2eAgFnSc3d1	poikylý
(	(	kIx(	(
<g/>
Malovaná	malovaný	k2eAgFnSc1d1	malovaná
stoa	stoa	k1gFnSc1	stoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
válce	válka	k1gFnSc6	válka
Američany	Američan	k1gMnPc4	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Nacházely	nacházet	k5eAaImAgFnP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
údajně	údajně	k6eAd1	údajně
první	první	k4xOgInPc1	první
veřejné	veřejný	k2eAgInPc1d1	veřejný
záchodky	záchodek	k1gInPc1	záchodek
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
větrů	vítr	k1gInPc2	vítr
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kombinace	kombinace	k1gFnPc4	kombinace
vodních	vodní	k2eAgFnPc2d1	vodní
a	a	k8xC	a
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
-	-	kIx~	-
DUBIN	dubina	k1gFnPc2	dubina
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
stavba	stavba	k1gFnSc1	stavba
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
do	do	k7c2	do
čtyřech	čtyři	k4xCgInPc2	čtyři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
reliéfy	reliéf	k1gInPc1	reliéf
příslušných	příslušný	k2eAgInPc2d1	příslušný
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Mitrópoli	Mitrópoli	k6eAd1	Mitrópoli
Katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
první	první	k4xOgInSc1	první
řecký	řecký	k2eAgMnSc1d1	řecký
král	král	k1gMnSc1	král
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1840	[number]	k4	1840
až	až	k9	až
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
sv.	sv.	kA	sv.
Filothéi	Filothéi	k1gNnSc2	Filothéi
a	a	k8xC	a
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc2	Řehoř
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
osobností	osobnost	k1gFnPc2	osobnost
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
(	(	kIx(	(
<g/>
filosofové	filosof	k1gMnPc1	filosof
Platón	Platón	k1gMnSc1	Platón
a	a	k8xC	a
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
středověk	středověk	k1gInSc4	středověk
(	(	kIx(	(
<g/>
byzantské	byzantský	k2eAgFnPc1d1	byzantská
císařovny	císařovna	k1gFnPc1	císařovna
Irena	Irena	k1gFnSc1	Irena
a	a	k8xC	a
Theofano	Theofana	k1gFnSc5	Theofana
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
(	(	kIx(	(
<g/>
objevitel	objevitel	k1gMnSc1	objevitel
Tróji	Trója	k1gFnSc6	Trója
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
<g/>
,	,	kIx,	,
řečtí	řecký	k2eAgMnPc1d1	řecký
králové	král	k1gMnPc1	král
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
Řecký	řecký	k2eAgMnSc1d1	řecký
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
Řecký	řecký	k2eAgMnSc1d1	řecký
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Athény	Athéna	k1gFnPc1	Athéna
rodištěm	rodiště	k1gNnSc7	rodiště
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
