<p>
<s>
Swing	swing	k1gInSc1	swing
je	být	k5eAaImIp3nS	být
jazzový	jazzový	k2eAgInSc1d1	jazzový
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
utvářel	utvářet	k5eAaImAgInS	utvářet
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
širšímu	široký	k2eAgNnSc3d2	širší
uplatnění	uplatnění	k1gNnSc3	uplatnění
jazzu	jazz	k1gInSc3	jazz
jako	jako	k8xC	jako
populární	populární	k2eAgFnSc2d1	populární
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
k	k	k7c3	k
míšení	míšení	k1gNnSc3	míšení
černošských	černošský	k2eAgInPc2d1	černošský
a	a	k8xC	a
bělošských	bělošský	k2eAgInPc2d1	bělošský
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
tradičního	tradiční	k2eAgInSc2d1	tradiční
(	(	kIx(	(
<g/>
historického	historický	k2eAgInSc2d1	historický
<g/>
)	)	kIx)	)
k	k	k7c3	k
modernímu	moderní	k2eAgInSc3d1	moderní
jazzu	jazz	k1gInSc3	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Swing	swing	k1gInSc1	swing
se	se	k3xPyFc4	se
formoval	formovat	k5eAaImAgInS	formovat
jako	jako	k9	jako
vývojové	vývojový	k2eAgNnSc4d1	vývojové
stádium	stádium	k1gNnSc4	stádium
tradičního	tradiční	k2eAgInSc2d1	tradiční
jazzu	jazz	k1gInSc2	jazz
v	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
slávu	sláva	k1gFnSc4	sláva
sklízel	sklízet	k5eAaImAgMnS	sklízet
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
a	a	k8xC	a
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
jazzu	jazz	k1gInSc3	jazz
má	mít	k5eAaImIp3nS	mít
swing	swing	k1gInSc1	swing
zpravidla	zpravidla	k6eAd1	zpravidla
houpavé	houpavý	k2eAgNnSc4d1	houpavé
frázování	frázování	k1gNnSc4	frázování
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
shuffle	shuffle	k6eAd1	shuffle
<g/>
,	,	kIx,	,
či	či	k8xC	či
prostě	prostě	k9	prostě
swingový	swingový	k2eAgInSc1d1	swingový
rytmus	rytmus	k1gInSc1	rytmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
blues	blues	k1gInSc1	blues
<g/>
.	.	kIx.	.
</s>
<s>
Notový	notový	k2eAgInSc1d1	notový
zápis	zápis	k1gInSc1	zápis
bývá	bývat	k5eAaImIp3nS	bývat
realizován	realizovat	k5eAaBmNgInS	realizovat
ve	v	k7c6	v
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
taktu	takt	k1gInSc2	takt
a	a	k8xC	a
houpavost	houpavost	k1gFnSc1	houpavost
rytmu	rytmus	k1gInSc2	rytmus
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
běh	běh	k1gInSc1	běh
osmin	osmina	k1gFnPc2	osmina
se	se	k3xPyFc4	se
nehraje	hrát	k5eNaImIp3nS	hrát
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každá	každý	k3xTgFnSc1	každý
sudá	sudý	k2eAgFnSc1d1	sudá
osminová	osminový	k2eAgFnSc1d1	osminová
nota	nota	k1gFnSc1	nota
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
–	–	k?	–
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
swingování	swingování	k1gNnSc1	swingování
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rytmus	rytmus	k1gInSc1	rytmus
podobný	podobný	k2eAgInSc1d1	podobný
triole	triola	k1gFnSc3	triola
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
čtvrťová	čtvrťový	k2eAgFnSc1d1	čtvrťová
+	+	kIx~	+
osminová	osminový	k2eAgFnSc1d1	osminová
nota	nota	k1gFnSc1	nota
v	v	k7c6	v
triole	triola	k1gFnSc6	triola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
pomalých	pomalý	k2eAgNnPc6d1	pomalé
tempech	tempo	k1gNnPc6	tempo
blížit	blížit	k5eAaImF	blížit
až	až	k6eAd1	až
tečkovanému	tečkovaný	k2eAgInSc3d1	tečkovaný
rytmu	rytmus	k1gInSc3	rytmus
<g/>
,	,	kIx,	,
v	v	k7c6	v
rychlých	rychlý	k2eAgNnPc6d1	rychlé
tempech	tempo	k1gNnPc6	tempo
lze	lze	k6eAd1	lze
naopak	naopak	k6eAd1	naopak
houpavost	houpavost	k1gFnSc4	houpavost
podpořit	podpořit	k5eAaPmF	podpořit
spíše	spíše	k9	spíše
akcentováním	akcentování	k1gNnSc7	akcentování
sudých	sudý	k2eAgFnPc2d1	sudá
osmin	osmina	k1gFnPc2	osmina
<g/>
,	,	kIx,	,
než	než	k8xS	než
snahou	snaha	k1gFnSc7	snaha
dodržovat	dodržovat	k5eAaImF	dodržovat
trioly	triola	k1gFnPc4	triola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nerozlučně	rozlučně	k6eNd1	rozlučně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Benny	Benn	k1gMnPc7	Benn
Goodmanem	Goodman	k1gMnSc7	Goodman
<g/>
,	,	kIx,	,
výborným	výborný	k2eAgMnSc7d1	výborný
klarinetistou	klarinetista	k1gMnSc7	klarinetista
<g/>
,	,	kIx,	,
vedoucím	vedoucí	k1gMnSc7	vedoucí
různých	různý	k2eAgInPc2d1	různý
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
hrály	hrát	k5eAaImAgInP	hrát
swing	swing	k1gInSc4	swing
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
klasické	klasický	k2eAgFnSc6d1	klasická
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Benny	Benn	k1gMnPc4	Benn
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
v	v	k7c6	v
černošském	černošský	k2eAgInSc6d1	černošský
hudebním	hudební	k2eAgInSc6d1	hudební
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
skladby	skladba	k1gFnPc1	skladba
byly	být	k5eAaImAgFnP	být
originální	originální	k2eAgFnPc1d1	originální
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
černošských	černošský	k2eAgMnPc2d1	černošský
interpretů	interpret	k1gMnPc2	interpret
je	být	k5eAaImIp3nS	být
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
evropským	evropský	k2eAgInSc7d1	evropský
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
krizi	krize	k1gFnSc6	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
nemělo	mít	k5eNaImAgNnS	mít
již	již	k6eAd1	již
publikum	publikum	k1gNnSc1	publikum
pochopení	pochopení	k1gNnSc2	pochopení
pro	pro	k7c4	pro
hýřivý	hýřivý	k2eAgInSc4d1	hýřivý
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgInSc4d1	tradiční
<g/>
)	)	kIx)	)
jazz	jazz	k1gInSc4	jazz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
skončila	skončit	k5eAaPmAgFnS	skončit
americká	americký	k2eAgFnSc1d1	americká
prohibice	prohibice	k1gFnSc1	prohibice
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
ambiciózní	ambiciózní	k2eAgMnSc1d1	ambiciózní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Uhlazený	uhlazený	k2eAgInSc1d1	uhlazený
a	a	k8xC	a
optmistický	optmistický	k2eAgInSc1d1	optmistický
swing	swing	k1gInSc1	swing
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
úděl	úděl	k1gInSc4	úděl
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Franklina	Franklin	k2eAgInSc2d1	Franklin
Delano	Delana	k1gFnSc5	Delana
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společnost	společnost	k1gFnSc1	společnost
pomalu	pomalu	k6eAd1	pomalu
získávala	získávat	k5eAaImAgFnS	získávat
opět	opět	k6eAd1	opět
svou	svůj	k3xOyFgFnSc4	svůj
naději	naděje	k1gFnSc4	naděje
a	a	k8xC	a
jistotu	jistota	k1gFnSc4	jistota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
swingu	swing	k1gInSc2	swing
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
epocha	epocha	k1gFnSc1	epocha
velkých	velký	k2eAgInPc2d1	velký
orchestrů	orchestr	k1gInPc2	orchestr
(	(	kIx(	(
<g/>
big	big	k?	big
bandů	band	k1gInPc2	band
<g/>
)	)	kIx)	)
složených	složený	k2eAgFnPc2d1	složená
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
až	až	k9	až
pěti	pět	k4xCc2	pět
saxofonů	saxofon	k1gInPc2	saxofon
<g/>
,	,	kIx,	,
dvou	dva	k4xCgNnPc2	dva
až	až	k8xS	až
čtyř	čtyři	k4xCgFnPc2	čtyři
trubek	trubka	k1gFnPc2	trubka
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
až	až	k6eAd1	až
čtyř	čtyři	k4xCgInPc2	čtyři
trombónů	trombón	k1gInPc2	trombón
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
třeba	třeba	k6eAd1	třeba
připočítat	připočítat	k5eAaPmF	připočítat
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
rytmickou	rytmický	k2eAgFnSc4d1	rytmická
část	část	k1gFnSc4	část
–	–	k?	–
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc4d1	bicí
soupravu	souprava	k1gFnSc4	souprava
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc4	kontrabas
a	a	k8xC	a
kytary	kytara	k1gFnPc4	kytara
<g/>
;	;	kIx,	;
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
ale	ale	k9	ale
stále	stále	k6eAd1	stále
existovaly	existovat	k5eAaImAgFnP	existovat
malé	malý	k2eAgFnPc1d1	malá
hudební	hudební	k2eAgFnPc1d1	hudební
skupinky	skupinka	k1gFnPc1	skupinka
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
sestav	sestava	k1gFnPc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
swingových	swingový	k2eAgInPc6d1	swingový
orchestrech	orchestr	k1gInPc6	orchestr
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
poprvé	poprvé	k6eAd1	poprvé
setkávali	setkávat	k5eAaImAgMnP	setkávat
černí	černý	k2eAgMnPc1d1	černý
i	i	k8xC	i
bílí	bílý	k2eAgMnPc1d1	bílý
muzikanti	muzikant	k1gMnPc1	muzikant
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
pódiu	pódium	k1gNnSc6	pódium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
legendární	legendární	k2eAgNnSc4d1	legendární
swingové	swingový	k2eAgNnSc4d1	swingové
a	a	k8xC	a
jazzové	jazzový	k2eAgNnSc4d1	jazzové
big	big	k?	big
bandy	bandy	k1gNnSc7	bandy
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
např.	např.	kA	např.
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
i	i	k9	i
dodnes	dodnes	k6eAd1	dodnes
legendární	legendární	k2eAgInSc4d1	legendární
Orchestr	orchestr	k1gInSc4	orchestr
Glenna	Glenn	k1gMnSc2	Glenn
Millera	Miller	k1gMnSc2	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
originální	originální	k2eAgInSc1d1	originální
a	a	k8xC	a
nezaměnitelný	zaměnitelný	k2eNgInSc1d1	nezaměnitelný
zvuk	zvuk	k1gInSc1	zvuk
orchestru	orchestr	k1gInSc2	orchestr
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Glenn	Glenn	k1gMnSc1	Glenn
Miller	Miller	k1gMnSc1	Miller
nahradil	nahradit	k5eAaPmAgMnS	nahradit
trubku	trubka	k1gFnSc4	trubka
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
melodickou	melodický	k2eAgFnSc4d1	melodická
linku	linka	k1gFnSc4	linka
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
ton	ton	k?	ton
akordů	akord	k1gInPc2	akord
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klarinetem	klarinet	k1gInSc7	klarinet
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
jej	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
milovníci	milovník	k1gMnPc1	milovník
hned	hned	k6eAd1	hned
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
už	už	k9	už
swing	swing	k1gInSc1	swing
nepředstavoval	představovat	k5eNaImAgInS	představovat
veškerý	veškerý	k3xTgInSc4	veškerý
hudební	hudební	k2eAgInSc4d1	hudební
střední	střední	k2eAgInSc4d1	střední
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
uhlazený	uhlazený	k2eAgInSc1d1	uhlazený
a	a	k8xC	a
hodil	hodit	k5eAaImAgInS	hodit
se	se	k3xPyFc4	se
jen	jen	k9	jen
jako	jako	k9	jako
taneční	taneční	k2eAgFnSc1d1	taneční
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgNnSc4	každý
okresní	okresní	k2eAgNnSc4d1	okresní
město	město	k1gNnSc4	město
svůj	svůj	k3xOyFgMnSc1	svůj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
velký	velký	k2eAgInSc1d1	velký
taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
amatérský	amatérský	k2eAgMnSc1d1	amatérský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
na	na	k7c6	na
tanečních	taneční	k2eAgFnPc6d1	taneční
zábavách	zábava	k1gFnPc6	zábava
a	a	k8xC	a
estrádách	estráda	k1gFnPc6	estráda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jejich	jejich	k3xOp3gFnSc1	jejich
éra	éra	k1gFnSc1	éra
skončila	skončit	k5eAaPmAgFnS	skončit
nástupem	nástup	k1gInSc7	nástup
elektrofonických	elektrofonický	k2eAgFnPc2d1	elektrofonická
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Přišla	přijít	k5eAaPmAgFnS	přijít
móda	móda	k1gFnSc1	móda
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnSc1	Beatles
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
bigbítu	bigbít	k1gInSc2	bigbít
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
tu	tu	k6eAd1	tu
i	i	k8xC	i
důvody	důvod	k1gInPc4	důvod
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
–	–	k?	–
pořadatel	pořadatel	k1gMnSc1	pořadatel
chtěl	chtít	k5eAaImAgMnS	chtít
na	na	k7c6	na
zábavě	zábava	k1gFnSc6	zábava
vydělat	vydělat	k5eAaPmF	vydělat
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgMnPc1	čtyři
mladíci	mladík	k1gMnPc1	mladík
s	s	k7c7	s
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
kytarami	kytara	k1gFnPc7	kytara
byli	být	k5eAaImAgMnP	být
levnější	levný	k2eAgInSc4d2	levnější
než	než	k8xS	než
velký	velký	k2eAgInSc4d1	velký
taneční	taneční	k2eAgInSc4d1	taneční
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
se	se	k3xPyFc4	se
ubíral	ubírat	k5eAaImAgInS	ubírat
neklidnějšími	klidný	k2eNgFnPc7d2	neklidnější
cestami	cesta	k1gFnPc7	cesta
bopu	bop	k1gInSc2	bop
a	a	k8xC	a
coolu	coola	k1gFnSc4	coola
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
středního	střední	k2eAgInSc2d1	střední
proudu	proud	k1gInSc2	proud
posluchačů	posluchač	k1gMnPc2	posluchač
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
převzali	převzít	k5eAaPmAgMnP	převzít
sóloví	sólový	k2eAgMnPc1d1	sólový
zpěváci	zpěvák	k1gMnPc1	zpěvák
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatrum	k1gNnSc2	Sinatrum
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
velké	velký	k2eAgInPc1d1	velký
orchestry	orchestr	k1gInPc1	orchestr
nemohly	moct	k5eNaImAgInP	moct
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
doprovázet	doprovázet	k5eAaImF	doprovázet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
americkými	americký	k2eAgMnPc7d1	americký
swingovými	swingový	k2eAgMnPc7d1	swingový
big	big	k?	big
bandy	bandy	k1gNnPc4	bandy
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
kapelníky	kapelník	k1gMnPc7	kapelník
vynikali	vynikat	k5eAaImAgMnP	vynikat
zejména	zejména	k9	zejména
Count	Count	k1gMnSc1	Count
Basie	Basie	k1gFnSc2	Basie
<g/>
,	,	kIx,	,
Jimmie	Jimmie	k1gFnSc2	Jimmie
Lunceford	Lunceforda	k1gFnPc2	Lunceforda
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnPc1	Anda
Kirk	Kirko	k1gNnPc2	Kirko
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
bělošskými	bělošský	k2eAgMnPc7d1	bělošský
muzikanty	muzikant	k1gMnPc7	muzikant
Benny	Benna	k1gFnSc2	Benna
Goodman	Goodman	k1gMnSc1	Goodman
<g/>
,	,	kIx,	,
Jimmy	Jimma	k1gFnPc1	Jimma
Dorsey	Dorsea	k1gFnSc2	Dorsea
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Tommy	Tomma	k1gFnSc2	Tomma
Dorsey	Dorsea	k1gFnSc2	Dorsea
<g/>
,	,	kIx,	,
Woody	Wooda	k1gFnSc2	Wooda
Herman	Herman	k1gMnSc1	Herman
a	a	k8xC	a
Glenn	Glenn	k1gMnSc1	Glenn
Miller	Miller	k1gMnSc1	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Swing	swing	k1gInSc4	swing
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc4	styl
zažil	zažít	k5eAaPmAgInS	zažít
největší	veliký	k2eAgInSc4d3	veliký
rozmach	rozmach	k1gInSc4	rozmach
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
hlásit	hlásit	k5eAaImF	hlásit
k	k	k7c3	k
americkým	americký	k2eAgInPc3d1	americký
vzorům	vzor	k1gInPc3	vzor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nacisté	nacista	k1gMnPc1	nacista
považovali	považovat	k5eAaImAgMnP	považovat
swing	swing	k1gInSc4	swing
–	–	k?	–
jakožto	jakožto	k8xS	jakožto
hudbu	hudba	k1gFnSc4	hudba
vycházející	vycházející	k2eAgFnSc4d1	vycházející
z	z	k7c2	z
jazzu	jazz	k1gInSc2	jazz
–	–	k?	–
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
méněcenné	méněcenný	k2eAgFnSc2d1	méněcenná
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
orchestry	orchestra	k1gFnPc4	orchestra
patřil	patřit	k5eAaImAgInS	patřit
orchestr	orchestr	k1gInSc1	orchestr
Karla	Karel	k1gMnSc2	Karel
Vlacha	Vlach	k1gMnSc2	Vlach
<g/>
,	,	kIx,	,
R.	R.	kA	R.
A.	A.	kA	A.
Dvorský	Dvorský	k1gMnSc1	Dvorský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
Melody	Meloda	k1gFnPc1	Meloda
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc4	orchestr
Emila	Emil	k1gMnSc2	Emil
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
S.	S.	kA	S.
E.	E.	kA	E.
Nováčka	Nováček	k1gMnSc2	Nováček
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc4	orchestr
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Maliny	Malina	k1gMnSc2	Malina
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zpěváků	zpěvák	k1gMnPc2	zpěvák
např.	např.	kA	např.
Inka	Inka	k1gFnSc1	Inka
Zemánková	Zemánková	k1gFnSc1	Zemánková
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
Kavka	Kavka	k1gMnSc1	Kavka
nebo	nebo	k8xC	nebo
Sestry	sestra	k1gFnPc1	sestra
Allanovy	Allanův	k2eAgFnPc1d1	Allanova
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
předčasně	předčasně	k6eAd1	předčasně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
život	život	k1gInSc4	život
několika	několik	k4yIc3	několik
významným	významný	k2eAgMnPc3d1	významný
hudebníkům	hudebník	k1gMnPc3	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
problémy	problém	k1gInPc1	problém
přišly	přijít	k5eAaPmAgInP	přijít
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
s	s	k7c7	s
obviněními	obvinění	k1gNnPc7	obvinění
ohledně	ohledně	k7c2	ohledně
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Definitivním	definitivní	k2eAgInSc7d1	definitivní
zlomem	zlom	k1gInSc7	zlom
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nástup	nástup	k1gInSc1	nástup
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Orchestry	orchestr	k1gInPc1	orchestr
byly	být	k5eAaImAgInP	být
znárodněny	znárodnit	k5eAaPmNgInP	znárodnit
a	a	k8xC	a
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
zavedeny	zavést	k5eAaPmNgInP	zavést
ideově	ideově	k6eAd1	ideově
ovlivňované	ovlivňovaný	k2eAgFnPc1d1	ovlivňovaná
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
přese	přese	k7c4	přese
všechno	všechen	k3xTgNnSc4	všechen
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
poválečné	poválečný	k2eAgMnPc4d1	poválečný
české	český	k2eAgMnPc4d1	český
swingaře	swingař	k1gMnPc4	swingař
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nP	patřit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hála	Hála	k1gMnSc1	Hála
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Adam	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
či	či	k8xC	či
Ondřej	Ondřej	k1gMnSc1	Ondřej
Havelka	Havelka	k1gMnSc1	Havelka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
Melody	Meloda	k1gFnPc1	Meloda
Makers	Makers	k1gInSc4	Makers
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
jazz	jazz	k1gInSc1	jazz
</s>
</p>
<p>
<s>
ragtime	ragtime	k1gInSc1	ragtime
</s>
</p>
<p>
<s>
tradiční	tradiční	k2eAgInSc1d1	tradiční
jazz	jazz	k1gInSc1	jazz
</s>
</p>
<p>
<s>
swing	swing	k1gInSc1	swing
(	(	kIx(	(
<g/>
tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
</s>
</p>
