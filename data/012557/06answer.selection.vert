<s>
Oproti	oproti	k7c3	oproti
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
jazzu	jazz	k1gInSc3	jazz
má	mít	k5eAaImIp3nS	mít
swing	swing	k1gInSc1	swing
zpravidla	zpravidla	k6eAd1	zpravidla
houpavé	houpavý	k2eAgNnSc4d1	houpavé
frázování	frázování	k1gNnSc4	frázování
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
shuffle	shuffle	k6eAd1	shuffle
<g/>
,	,	kIx,	,
či	či	k8xC	či
prostě	prostě	k9	prostě
swingový	swingový	k2eAgInSc1d1	swingový
rytmus	rytmus	k1gInSc1	rytmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
blues	blues	k1gInSc1	blues
<g/>
.	.	kIx.	.
</s>
