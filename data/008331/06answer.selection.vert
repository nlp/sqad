<s>
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Božanově	Božanův	k2eAgNnSc6d1	Božanův
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
