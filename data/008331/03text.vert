<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Božanově	Božanův	k2eAgNnSc6d1	Božanův
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
připomínán	připomínán	k2eAgInSc1d1	připomínán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1384	[number]	k4	1384
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1626	[number]	k4	1626
byl	být	k5eAaImAgInS	být
farním	farní	k2eAgInSc7d1	farní
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
kostela	kostel	k1gInSc2	kostel
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
za	za	k7c2	za
opata	opat	k1gMnSc2	opat
Otmara	Otmar	k1gMnSc2	Otmar
Zinkeho	Zinke	k1gMnSc2	Zinke
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1709	[number]	k4	1709
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
za	za	k7c2	za
opata	opat	k1gMnSc2	opat
Benno	Benno	k6eAd1	Benno
Löbla	Löbla	k1gMnSc1	Löbla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Program	program	k1gInSc1	program
záchrany	záchrana	k1gFnSc2	záchrana
architektonického	architektonický	k2eAgNnSc2d1	architektonické
dědictví	dědictví	k1gNnSc2	dědictví
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Programu	program	k1gInSc2	program
záchrany	záchrana	k1gFnSc2	záchrana
architektonického	architektonický	k2eAgNnSc2d1	architektonické
dědictví	dědictví	k1gNnSc2	dědictví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996-2000	[number]	k4	1996-2000
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
kostela	kostel	k1gInSc2	kostel
čerpáno	čerpat	k5eAaImNgNnS	čerpat
10	[number]	k4	10
230	[number]	k4	230
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
ke	k	k7c3	k
starší	starý	k2eAgFnSc3d2	starší
věži	věž	k1gFnSc3	věž
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
s	s	k7c7	s
okosenými	okosený	k2eAgInPc7d1	okosený
rohy	roh	k1gInPc7	roh
<g/>
.	.	kIx.	.
</s>
<s>
Předsíň	předsíň	k1gFnSc1	předsíň
s	s	k7c7	s
kruchtou	kruchta	k1gFnSc7	kruchta
a	a	k8xC	a
kněžištěm	kněžiště	k1gNnSc7	kněžiště
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
připojeny	připojen	k2eAgFnPc1d1	připojena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
patronky	patronka	k1gFnSc2	patronka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
broumovské	broumovský	k2eAgFnSc2d1	Broumovská
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zaklenutý	zaklenutý	k2eAgInSc1d1	zaklenutý
cihlovou	cihlový	k2eAgFnSc7d1	cihlová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
ke	k	k7c3	k
kostel	kostel	k1gInSc4	kostel
vede	vést	k5eAaImIp3nS	vést
původní	původní	k2eAgFnSc1d1	původní
gotická	gotický	k2eAgFnSc1d1	gotická
brána	brána	k1gFnSc1	brána
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interiér	interiér	k1gInSc1	interiér
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
je	být	k5eAaImIp3nS	být
pseudobarokní	pseudobarokní	k2eAgInSc1d1	pseudobarokní
<g/>
,	,	kIx,	,
boční	boční	k2eAgInPc1d1	boční
jsou	být	k5eAaImIp3nP	být
původní	původní	k2eAgInPc1d1	původní
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInPc1d1	barokní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
==	==	k?	==
</s>
</p>
<p>
<s>
Bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
poslední	poslední	k2eAgFnSc4d1	poslední
neděli	neděle	k1gFnSc4	neděle
v	v	k7c4	v
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
8.30	[number]	k4	8.30
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
věřícím	věřící	k1gMnPc3	věřící
i	i	k8xC	i
turistům	turist	k1gMnPc3	turist
a	a	k8xC	a
zájemcům	zájemce	k1gMnPc3	zájemce
o	o	k7c4	o
barokní	barokní	k2eAgFnSc4d1	barokní
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PROKOP	Prokop	k1gMnSc1	Prokop
<g/>
,	,	kIx,	,
Bohumír	Bohumír	k1gMnSc1	Bohumír
<g/>
;	;	kIx,	;
KOTALÍK	KOTALÍK	kA	KOTALÍK
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
T.	T.	kA	T.
<g/>
;	;	kIx,	;
SŮVA	sůva	k1gFnSc1	sůva
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Broumovská	broumovský	k2eAgFnSc1d1	Broumovská
skupina	skupina	k1gFnSc1	skupina
kostelů	kostel	k1gInPc2	kostel
:	:	kIx,	:
průvodce	průvodce	k1gMnSc1	průvodce
školou	škola	k1gFnSc7	škola
českého	český	k2eAgNnSc2d1	české
baroka	baroko	k1gNnSc2	baroko
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
pískovcových	pískovcový	k2eAgFnPc2d1	pískovcová
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Vernéřovice	Vernéřovice	k1gFnSc1	Vernéřovice
<g/>
:	:	kIx,	:
Modrý	modrý	k2eAgMnSc1d1	modrý
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
37	[number]	k4	37
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
6311	[number]	k4	6311
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
kostela	kostel	k1gInSc2	kostel
</s>
</p>
<p>
<s>
Krásné	krásný	k2eAgFnPc1d1	krásná
Čechy	Čechy	k1gFnPc1	Čechy
</s>
</p>
<p>
<s>
DSO	DSO	kA	DSO
Broumovsko	Broumovsko	k1gNnSc1	Broumovsko
</s>
</p>
<p>
<s>
Geocaching	Geocaching	k1gInSc1	Geocaching
</s>
</p>
<p>
<s>
Božanov	Božanov	k1gInSc1	Božanov
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
</s>
</p>
