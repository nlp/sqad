<p>
<s>
Knížectví	knížectví	k1gNnSc1	knížectví
označuje	označovat	k5eAaImIp3nS	označovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
vlády	vláda	k1gFnSc2	vláda
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
kníže	kníže	k1gMnSc1	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Knížectví	knížectví	k1gNnPc1	knížectví
dnes	dnes	k6eAd1	dnes
existují	existovat	k5eAaImIp3nP	existovat
jako	jako	k9	jako
suverénní	suverénní	k2eAgInPc4d1	suverénní
státní	státní	k2eAgInPc4d1	státní
celky	celek	k1gInPc4	celek
(	(	kIx(	(
<g/>
Monako	Monako	k1gNnSc1	Monako
<g/>
,	,	kIx,	,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
<g/>
,	,	kIx,	,
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
historická	historický	k2eAgNnPc1d1	historické
území	území	k1gNnPc1	území
(	(	kIx(	(
<g/>
např.	např.	kA	např.
České	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
,	,	kIx,	,
Nitranské	nitranský	k2eAgNnSc1d1	Nitranské
knížectví	knížectví	k1gNnSc1	knížectví
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
knížectví	knížectví	k1gNnSc2	knížectví
rozumí	rozumět	k5eAaImIp3nS	rozumět
právně	právně	k6eAd1	právně
svrchovaný	svrchovaný	k2eAgInSc1d1	svrchovaný
stát	stát	k1gInSc1	stát
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
(	(	kIx(	(
<g/>
suverénním	suverénní	k2eAgNnSc6d1	suverénní
<g/>
)	)	kIx)	)
území	území	k1gNnSc6	území
monarchy	monarcha	k1gMnSc2	monarcha
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
původu	původ	k1gInSc2	původ
v	v	k7c6	v
knížecí	knížecí	k2eAgFnSc6d1	knížecí
hodnosti	hodnost	k1gFnSc6	hodnost
anebo	anebo	k8xC	anebo
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Asturské	Asturský	k2eAgNnSc1d1	Asturské
knížectví	knížectví	k1gNnSc1	knížectví
ve	v	k7c6	v
Španělském	španělský	k2eAgNnSc6d1	španělské
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
evropská	evropský	k2eAgNnPc4d1	Evropské
knížectví	knížectví	k1gNnPc4	knížectví
patří	patřit	k5eAaImIp3nP	patřit
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
<g/>
,	,	kIx,	,
Monako	Monako	k1gNnSc1	Monako
a	a	k8xC	a
také	také	k9	také
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panuje	panovat	k5eAaImIp3nS	panovat
tzv.	tzv.	kA	tzv.
Dvojvládí	dvojvládí	k1gNnSc1	dvojvládí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
knížetem	kníže	k1gNnSc7wR	kníže
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
knížectví	knížectví	k1gNnSc6	knížectví
jsou	být	k5eAaImIp3nP	být
současně	současně	k6eAd1	současně
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
biskup	biskup	k1gMnSc1	biskup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
titulaturu	titulatura	k1gFnSc4	titulatura
knížectví	knížectví	k1gNnSc2	knížectví
si	se	k3xPyFc3	se
osobují	osobovat	k5eAaImIp3nP	osobovat
nárok	nárok	k1gInSc4	nárok
i	i	k8xC	i
některé	některý	k3yIgNnSc1	některý
mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
společenstvím	společenství	k1gNnSc7	společenství
neuznané	uznaný	k2eNgFnSc2d1	neuznaná
území	území	k1gNnSc3	území
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Sealand	Sealand	k1gInSc1	Sealand
<g/>
,	,	kIx,	,
Seborga	Seborga	k1gFnSc1	Seborga
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Utopia	Utopia	k1gFnSc1	Utopia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
knížectví	knížectví	k1gNnSc2	knížectví
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
majetkem	majetek	k1gInSc7	majetek
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
feudální	feudální	k2eAgInSc1d1	feudální
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
od	od	k7c2	od
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
jiném	jiný	k2eAgMnSc6d1	jiný
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
knížectví	knížectví	k1gNnPc1	knížectví
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
století	století	k1gNnPc2	století
zpravidla	zpravidla	k6eAd1	zpravidla
buď	buď	k8xC	buď
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
královstvím	království	k1gNnSc7	království
(	(	kIx(	(
<g/>
např.	např.	kA	např.
české	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
významově	významově	k6eAd1	významově
posunul	posunout	k5eAaPmAgInS	posunout
na	na	k7c4	na
území	území	k1gNnPc4	území
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
římsko-německé	římskoěmecký	k2eAgFnSc2d1	římsko-německý
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Takovýchto	takovýto	k3xDgNnPc2	takovýto
území	území	k1gNnPc2	území
existoval	existovat	k5eAaImAgInS	existovat
značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc1	území
však	však	k9	však
praktikovala	praktikovat	k5eAaImAgFnS	praktikovat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
vládu	vláda	k1gFnSc4	vláda
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
období	období	k1gNnSc6	období
slabých	slabý	k2eAgMnPc2d1	slabý
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Některá	některý	k3yIgNnPc1	některý
knížectví	knížectví	k1gNnPc1	knížectví
==	==	k?	==
</s>
</p>
<p>
<s>
Abchazské	abchazský	k2eAgNnSc1d1	abchazské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Adžarské	Adžarský	k2eAgNnSc1d1	Adžarský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Achajské	Achajský	k2eAgNnSc1d1	Achajské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Akvitánské	Akvitánský	k2eAgNnSc1d1	Akvitánský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Albánské	albánský	k2eAgNnSc1d1	albánské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Andorrské	Andorrský	k2eAgNnSc1d1	Andorrské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Anhaltská	Anhaltský	k2eAgNnPc1d1	Anhaltský
knížectví	knížectví	k1gNnPc1	knížectví
</s>
</p>
<p>
<s>
Antiochijské	antiochijský	k2eAgNnSc1d1	Antiochijské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Ansbašské	Ansbašský	k2eAgNnSc1d1	Ansbašský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Arménské	arménský	k2eAgNnSc1d1	arménské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Auersperské	Auersperský	k2eAgNnSc1d1	Auersperský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Birkenfeld	Birkenfeld	k1gInSc1	Birkenfeld
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Burgundské	burgundský	k2eAgNnSc1d1	burgundské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Calenberské	Calenberský	k2eAgNnSc1d1	Calenberský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Černohorské	Černohorské	k2eAgNnSc1d1	Černohorské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Fürstenberské	Fürstenberský	k2eAgNnSc1d1	Fürstenberský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Galilejské	galilejský	k2eAgNnSc1d1	Galilejské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Göttingenské	Göttingenský	k2eAgNnSc1d1	Göttingenské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Haličsko-volyňské	haličskoolyňský	k2eAgNnSc1d1	haličsko-volyňský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Hohenzollernské	hohenzollernský	k2eAgNnSc1d1	hohenzollernský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Chorvatské	chorvatský	k2eAgNnSc1d1	Chorvatské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
knížectví	knížectví	k1gNnSc1	knížectví
Katalánie	Katalánie	k1gFnSc2	Katalánie
</s>
</p>
<p>
<s>
Lichtenberské	Lichtenberský	k2eAgNnSc1d1	Lichtenberský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Lichtenštejnské	lichtenštejnský	k2eAgNnSc1d1	Lichtenštejnské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Leyen	Leyen	k1gInSc1	Leyen
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
knížectví	knížectví	k1gNnSc1	knížectví
Lippe	Lipp	k1gMnSc5	Lipp
</s>
</p>
<p>
<s>
knížectví	knížectví	k1gNnSc1	knížectví
Lucca	Lucc	k1gInSc2	Lucc
a	a	k8xC	a
Piombino	Piombin	k2eAgNnSc4d1	Piombino
</s>
</p>
<p>
<s>
Lübeck	Lübeck	k1gInSc1	Lübeck
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Meklenburské	meklenburský	k2eAgNnSc1d1	meklenburský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Monacké	monacký	k2eAgNnSc1d1	Monacké
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Neuchâtel	Neuchâtel	k1gInSc1	Neuchâtel
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oranžské	oranžský	k2eAgNnSc1d1	oranžský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
knížectví	knížectví	k1gNnSc1	knížectví
Oranžsko-Nasavské	Oranžsko-Nasavský	k2eAgFnSc2d1	Oranžsko-Nasavský
</s>
</p>
<p>
<s>
Piedmontské	Piedmontský	k2eAgNnSc1d1	Piedmontský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Piombino	Piombin	k2eAgNnSc4d1	Piombino
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podunajská	podunajský	k2eAgNnPc1d1	podunajské
knížectví	knížectví	k1gNnPc1	knížectví
</s>
</p>
<p>
<s>
Spojená	spojený	k2eAgNnPc1d1	spojené
knížectví	knížectví	k1gNnPc1	knížectví
Valašska	Valašsko	k1gNnSc2	Valašsko
a	a	k8xC	a
Moldávie	Moldávie	k1gFnSc2	Moldávie
(	(	kIx(	(
<g/>
Rumunské	rumunský	k2eAgNnSc1d1	rumunské
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moldavské	moldavský	k2eAgNnSc1d1	moldavské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Sedmihradské	sedmihradský	k2eAgNnSc1d1	sedmihradské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Valašské	valašský	k2eAgNnSc1d1	Valašské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Pskovské	Pskovský	k2eAgNnSc1d1	Pskovský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Reusské	Reusský	k2eAgNnSc1d1	Reusský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Řezenské	řezenský	k2eAgNnSc1d1	řezenské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
knížectví	knížectví	k1gNnSc1	knížectví
Schaumburg-Lippe	Schaumburg-Lipp	k1gInSc5	Schaumburg-Lipp
</s>
</p>
<p>
<s>
Schwarzburské	Schwarzburský	k2eAgNnSc1d1	Schwarzburský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Schwarzbursko-Rudolstadtské	Schwarzbursko-Rudolstadtský	k2eAgNnSc1d1	Schwarzbursko-Rudolstadtský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Schwarzbursko-Sondershausenské	Schwarzbursko-Sondershausenský	k2eAgNnSc1d1	Schwarzbursko-Sondershausenský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Slezská	Slezská	k1gFnSc1	Slezská
knížectví	knížectví	k1gNnSc2	knížectví
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
slezských	slezský	k2eAgNnPc2d1	Slezské
knížectví	knížectví	k1gNnPc2	knížectví
</s>
</p>
<p>
<s>
Niské	Niský	k2eAgNnSc1d1	Niský
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Opavské	opavský	k2eAgNnSc1d1	Opavské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Těšínské	Těšínské	k2eAgNnSc1d1	Těšínské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Salm	Salm	k1gInSc1	Salm
(	(	kIx(	(
<g/>
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Knížectví	knížectví	k1gNnSc1	knížectví
Sealand	Sealanda	k1gFnPc2	Sealanda
</s>
</p>
<p>
<s>
Srbské	srbský	k2eAgNnSc1d1	srbské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Varšavské	varšavský	k2eAgNnSc1d1	Varšavské
knížectví	knížectví	k1gNnSc1	knížectví
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
vévodství	vévodství	k1gNnSc2	vévodství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Volyňské	volyňský	k2eAgNnSc1d1	Volyňské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Waldecké	Waldecký	k2eAgNnSc1d1	Waldecký
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Knížecí	knížecí	k2eAgInSc1d1	knížecí
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
Velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
(	(	kIx(	(
<g/>
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vévodství	vévodství	k1gNnSc1	vévodství
</s>
</p>
<p>
<s>
Biskupské	biskupský	k2eAgNnSc1d1	Biskupské
knížectví	knížectví	k1gNnSc1	knížectví
</s>
</p>
<p>
<s>
Léno	léno	k1gNnSc1	léno
</s>
</p>
<p>
<s>
Emirát	emirát	k1gInSc1	emirát
</s>
</p>
<p>
<s>
Beylik	Beylik	k1gMnSc1	Beylik
</s>
</p>
