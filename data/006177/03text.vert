<s>
Nicolás	Nicolás	k6eAd1	Nicolás
Maduro	Madura	k1gFnSc5	Madura
Moros	morosit	k5eAaImRp2nS	morosit
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
Caracas	Caracas	k1gInSc1	Caracas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
venezuelský	venezuelský	k2eAgMnSc1d1	venezuelský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
prezidenta	prezident	k1gMnSc2	prezident
Cháveze	Cháveze	k1gFnSc2	Cháveze
stal	stát	k5eAaPmAgMnS	stát
prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
prezidentem	prezident	k1gMnSc7	prezident
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
řádně	řádně	k6eAd1	řádně
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
autobusu	autobus	k1gInSc2	autobus
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
odborovým	odborový	k2eAgMnSc7d1	odborový
předákem	předák	k1gMnSc7	předák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
–	–	k?	–
nejdříve	dříve	k6eAd3	dříve
jako	jako	k9	jako
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
předseda	předseda	k1gMnSc1	předseda
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
jako	jako	k9	jako
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgMnSc1d1	někdejší
viceprezident	viceprezident	k1gMnSc1	viceprezident
složil	složit	k5eAaPmAgMnS	složit
přísahu	přísaha	k1gFnSc4	přísaha
na	na	k7c6	na
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
zasedání	zasedání	k1gNnSc6	zasedání
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
<g/>
;	;	kIx,	;
opozice	opozice	k1gFnSc1	opozice
však	však	k9	však
schůzi	schůze	k1gFnSc4	schůze
bojkotovala	bojkotovat	k5eAaImAgFnS	bojkotovat
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
zesnulý	zesnulý	k1gMnSc1	zesnulý
Hugo	Hugo	k1gMnSc1	Hugo
Chávez	Chávez	k1gMnSc1	Chávez
nepřísahal	přísahat	k5eNaImAgMnS	přísahat
k	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
prezidentskému	prezidentský	k2eAgNnSc3d1	prezidentské
období	období	k1gNnSc3	období
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
z	z	k7c2	z
právního	právní	k2eAgNnSc2d1	právní
hlediska	hledisko	k1gNnSc2	hledisko
byla	být	k5eAaImAgFnS	být
doba	doba	k1gFnSc1	doba
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
považována	považován	k2eAgFnSc1d1	považována
stále	stále	k6eAd1	stále
za	za	k7c4	za
3	[number]	k4	3
<g/>
.	.	kIx.	.
prezidentské	prezidentský	k2eAgNnSc1d1	prezidentské
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odstoupí	odstoupit	k5eAaPmIp3nS	odstoupit
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
2	[number]	k4	2
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
pozici	pozice	k1gFnSc4	pozice
prezidenta	prezident	k1gMnSc2	prezident
přejímá	přejímat	k5eAaImIp3nS	přejímat
viceprezident	viceprezident	k1gMnSc1	viceprezident
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
svolání	svolání	k1gNnSc2	svolání
legitimních	legitimní	k2eAgFnPc2d1	legitimní
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
přísahy	přísaha	k1gFnSc2	přísaha
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
mají	mít	k5eAaImIp3nP	mít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
do	do	k7c2	do
30	[number]	k4	30
dnů	den	k1gInPc2	den
po	po	k7c6	po
prezidentově	prezidentův	k2eAgFnSc6d1	prezidentova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Maduro	Madura	k1gFnSc5	Madura
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgNnPc2d1	konané
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Socialisté	socialist	k1gMnPc1	socialist
chtěli	chtít	k5eAaImAgMnP	chtít
využít	využít	k5eAaPmF	využít
odkazu	odkaz	k1gInSc3	odkaz
masově	masově	k6eAd1	masově
oblíbeného	oblíbený	k2eAgInSc2d1	oblíbený
Cháveze	Cháveze	k1gFnSc2	Cháveze
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Madura	Madura	k1gFnSc1	Madura
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vystoupení	vystoupení	k1gNnSc6	vystoupení
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
po	po	k7c6	po
Chávezově	Chávezův	k2eAgFnSc6d1	Chávezova
smrti	smrt	k1gFnSc6	smrt
postavila	postavit	k5eAaPmAgFnS	postavit
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ústava	ústava	k1gFnSc1	ústava
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
hrát	hrát	k5eAaImF	hrát
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
roli	role	k1gFnSc4	role
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Maduro	Madura	k1gFnSc5	Madura
nakonec	nakonec	k6eAd1	nakonec
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
velice	velice	k6eAd1	velice
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
50,66	[number]	k4	50,66
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
Henrique	Henrique	k1gFnPc2	Henrique
Capriles	Capriles	k1gMnSc1	Capriles
výsledky	výsledek	k1gInPc4	výsledek
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
zfalšované	zfalšovaný	k2eAgNnSc4d1	zfalšované
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
čelí	čelit	k5eAaImIp3nS	čelit
Maduro	Madura	k1gFnSc5	Madura
masovým	masový	k2eAgFnPc3d1	masová
demonstracím	demonstrace	k1gFnPc3	demonstrace
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
podporu	podpora	k1gFnSc4	podpora
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Protesty	protest	k1gInPc1	protest
si	se	k3xPyFc3	se
již	již	k6eAd1	již
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
několik	několik	k4yIc4	několik
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
desítky	desítka	k1gFnSc2	desítka
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
vlnu	vlna	k1gFnSc4	vlna
demonstrací	demonstrace	k1gFnPc2	demonstrace
byl	být	k5eAaImAgMnS	být
kromě	kromě	k7c2	kromě
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
<g/>
,	,	kIx,	,
nedostatku	nedostatek	k1gInSc3	nedostatek
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
zločinnosti	zločinnost	k1gFnSc2	zločinnost
pokus	pokus	k1gInSc4	pokus
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
nakloněného	nakloněný	k2eAgInSc2d1	nakloněný
Madurovi	Madurův	k2eAgMnPc1d1	Madurův
<g/>
)	)	kIx)	)
převzít	převzít	k5eAaPmF	převzít
pravomoci	pravomoc	k1gFnPc4	pravomoc
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
pak	pak	k8xC	pak
Madura	Madura	k1gFnSc1	Madura
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
soud	soud	k1gInSc4	soud
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2013	[number]	k4	2013
si	se	k3xPyFc3	se
Maduro	Madura	k1gFnSc5	Madura
po	po	k7c6	po
dvacetiletém	dvacetiletý	k2eAgInSc6d1	dvacetiletý
vztahu	vztah	k1gInSc6	vztah
vzal	vzít	k5eAaPmAgMnS	vzít
generální	generální	k2eAgFnSc4d1	generální
prokurátorku	prokurátorka	k1gFnSc4	prokurátorka
Cilii	Cilie	k1gFnSc4	Cilie
Floresovou	Floresový	k2eAgFnSc7d1	Floresová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
prezidentství	prezidentství	k1gNnSc2	prezidentství
Huga	Hugo	k1gMnSc2	Hugo
Cháveze	Cháveze	k1gFnSc2	Cháveze
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ministryní	ministryně	k1gFnPc2	ministryně
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
první	první	k4xOgMnPc1	první
ženou	hnát	k5eAaImIp3nP	hnát
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zastávala	zastávat	k5eAaImAgFnS	zastávat
funkci	funkce	k1gFnSc4	funkce
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
venezuelského	venezuelský	k2eAgNnSc2d1	venezuelské
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
nahradila	nahradit	k5eAaPmAgFnS	nahradit
právě	právě	k9	právě
Madura	Madura	k1gFnSc1	Madura
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
manželství	manželství	k1gNnSc2	manželství
má	mít	k5eAaImIp3nS	mít
Floresová	Floresový	k2eAgFnSc1d1	Floresová
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Maduro	Madura	k1gFnSc5	Madura
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nicolás	Nicolása	k1gFnPc2	Nicolása
Maduro	Madura	k1gFnSc5	Madura
Moros	morosit	k5eAaImRp2nS	morosit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zpráva	zpráva	k1gFnSc1	zpráva
Venezuela	Venezuela	k1gFnSc1	Venezuela
má	mít	k5eAaImIp3nS	mít
nového	nový	k2eAgMnSc4d1	nový
prezidenta	prezident	k1gMnSc4	prezident
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
