<s>
Lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
</s>
<s>
Památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
(	(	kIx(
<g/>
chráněný	chráněný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
Lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
Evidenční	evidenční	k2eAgFnSc2d1
č.	č.	k?
</s>
<s>
104641	#num#	k4
(	(	kIx(
<g/>
11451	#num#	k4
<g/>
)	)	kIx)
Poloha	poloha	k1gFnSc1
Okres	okres	k1gInSc1
</s>
<s>
Klatovy	Klatovy	k1gInPc1
Obec	obec	k1gFnSc1
</s>
<s>
Žihobce	Žihobce	k1gMnSc1
Katastr	katastr	k1gInSc4
</s>
<s>
Žihobce	Žihobce	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
52,44	52,44	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
59,16	59,16	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
je	být	k5eAaImIp3nS
památný	památný	k2eAgInSc4d1
strom	strom	k1gInSc4
v	v	k7c6
Žihobcích	Žihobce	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lípa	lípa	k1gFnSc1
malolistá	malolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
Mill	Mill	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
roste	růst	k5eAaImIp3nS
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
u	u	k7c2
přírodního	přírodní	k2eAgInSc2d1
parketu	parket	k1gInSc2
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
stromu	strom	k1gInSc2
je	být	k5eAaImIp3nS
26	#num#	k4
m	m	kA
<g/>
,	,	kIx,
šířka	šířka	k1gFnSc1
koruny	koruna	k1gFnSc2
26	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
445	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
měřeno	měřit	k5eAaImNgNnS
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úžlabí	úžlabí	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
dutina	dutina	k1gFnSc1
<g/>
,	,	kIx,
koruna	koruna	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
zakrácena	zakrátit	k5eAaPmNgFnS
<g/>
,	,	kIx,
kmen	kmen	k1gInSc1
vykazuje	vykazovat	k5eAaImIp3nS
mírné	mírný	k2eAgNnSc4d1
poškození	poškození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byl	být	k5eAaImAgInS
proveden	provést	k5eAaPmNgInS
zdravotní	zdravotní	k2eAgInSc1d1
<g/>
,	,	kIx,
bezpečnostní	bezpečnostní	k2eAgInSc1d1
a	a	k8xC
redukční	redukční	k2eAgInSc1d1
řez	řez	k1gInSc1
<g/>
,	,	kIx,
ošetření	ošetření	k1gNnSc1
a	a	k8xC
zastřešení	zastřešení	k1gNnSc1
dutiny	dutina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Lípa	lípa	k1gFnSc1
je	být	k5eAaImIp3nS
chráněna	chránit	k5eAaImNgFnS
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2005	#num#	k4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
svým	svůj	k3xOyFgNnSc7
stářím	stáří	k1gNnSc7
a	a	k8xC
vzrůstem	vzrůst	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
<g/>
.	.	kIx.
drusop	drusop	k1gInSc1
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Odborná	odborný	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
drusop	drusop	k1gInSc4
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Stromy	strom	k1gInPc1
</s>
