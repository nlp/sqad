<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lípa	lípa	k1gFnSc1
malolistá	malolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
cordata	cordata	k1gFnSc1
Mill	Mill	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
roste	růst	k5eAaImIp3nS
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
u	u	k7c2
přírodního	přírodní	k2eAgInSc2d1
parketu	parket	k1gInSc2
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
stromu	strom	k1gInSc2
je	být	k5eAaImIp3nS
26	#num#	k4
m	m	kA
<g/>
,	,	kIx,
šířka	šířka	k1gFnSc1
koruny	koruna	k1gFnSc2
26	#num#	k4
m	m	kA
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
445	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
měřeno	měřit	k5eAaImNgNnS
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
