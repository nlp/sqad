<s>
Mangusta	Mangusta	k1gMnSc1
abuvudan	abuvudan	k1gMnSc1
</s>
<s>
Mangusta	Mangusta	k1gMnSc1
abuvudan	abuvudan	k1gMnSc1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Nadtřída	nadtřída	k1gFnSc1
</s>
<s>
čtyřnožci	čtyřnožec	k1gMnPc1
(	(	kIx(
<g/>
Tetrapoda	Tetrapoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
promykovití	promykovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Herpestidae	Herpestidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
mangusta	mangusta	k1gFnSc1
(	(	kIx(
<g/>
Ichneumia	Ichneumia	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Cuvier	Cuvier	k1gMnSc1
<g/>
,	,	kIx,
1829	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mangusta	Mangusta	k1gMnSc1
abuvudan	abuvudan	k1gMnSc1
(	(	kIx(
<g/>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
promyka	promyka	k1gFnSc1
abuvudan	abuvudana	k1gFnPc2
nebo	nebo	k8xC
promyka	promyka	k1gFnSc1
běloocasá	běloocasý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
druh	druh	k1gInSc1
mangusty	mangusta	k1gMnSc2
obývající	obývající	k2eAgFnSc4d1
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
subsaharské	subsaharský	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
a	a	k8xC
jih	jih	k1gInSc4
Arabského	arabský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
největší	veliký	k2eAgFnPc4d3
promykovité	promykovitý	k2eAgFnPc4d1
šelmy	šelma	k1gFnPc4
<g/>
,	,	kIx,
dosahuje	dosahovat	k5eAaImIp3nS
celkové	celkový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
často	často	k6eAd1
výrazně	výrazně	k6eAd1
přes	přes	k7c4
1	#num#	k4
metr	metr	k1gInSc4
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
i	i	k9
více	hodně	k6eAd2
než	než	k8xS
5	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohlavní	pohlavní	k2eAgInSc1d1
dimorfismus	dimorfismus	k1gInSc1
je	být	k5eAaImIp3nS
nevýrazný	výrazný	k2eNgMnSc1d1
<g/>
,	,	kIx,
samci	samec	k1gMnPc1
bývají	bývat	k5eAaImIp3nP
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgFnPc1d1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
mangustu	mangusta	k1gFnSc4
jsou	být	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
<g/>
,	,	kIx,
níže	nízce	k6eAd2
posazená	posazený	k2eAgFnSc1d1
přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
těla	tělo	k1gNnSc2
a	a	k8xC
šedo-hnědo-černo-bílá	šedo-hnědo-černo-bílý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
srsti	srst	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocas	ocas	k1gInSc1
obvykle	obvykle	k6eAd1
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
zbytkem	zbytek	k1gInSc7
těla	tělo	k1gNnSc2
podstatně	podstatně	k6eAd1
světlejší	světlý	k2eAgInSc1d2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
stříbřité	stříbřitý	k2eAgFnPc1d1
či	či	k8xC
bílé	bílý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgInPc6d1
druzích	druh	k1gInPc6
otevřenějších	otevřený	k2eAgInPc2d2
lesů	les	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
savanách	savana	k1gFnPc6
<g/>
,	,	kIx,
křovinatých	křovinatý	k2eAgInPc6d1
biotopech	biotop	k1gInPc6
a	a	k8xC
stepích	step	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toleruje	tolerovat	k5eAaImIp3nS
i	i	k9
lidmi	člověk	k1gMnPc7
pozměněnou	pozměněný	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhýbá	vyhýbat	k5eAaImIp3nS
se	se	k3xPyFc4
pouštím	poušť	k1gFnPc3
a	a	k8xC
hustým	hustý	k2eAgInPc3d1
lesům	les	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
převážně	převážně	k6eAd1
o	o	k7c4
nočního	noční	k2eAgMnSc4d1
živočicha	živočich	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
žije	žít	k5eAaImIp3nS
většinou	většina	k1gFnSc7
osamoceně	osamoceně	k6eAd1
<g/>
,	,	kIx,
jen	jen	k9
dočasně	dočasně	k6eAd1
v	v	k7c6
párech	pár	k1gInPc6
či	či	k8xC
menších	malý	k2eAgFnPc6d2
skupinkách	skupinka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozmnožování	rozmnožování	k1gNnSc2
bývá	bývat	k5eAaImIp3nS
sezónní	sezónní	k2eAgFnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
porodům	porod	k1gInPc3
dochází	docházet	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
mláďat	mládě	k1gNnPc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
vrhu	vrh	k1gInSc6
je	být	k5eAaImIp3nS
1	#num#	k4
až	až	k9
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělci	dospělec	k1gMnPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
především	především	k9
hmyzem	hmyz	k1gInSc7
<g/>
,	,	kIx,
jídelníček	jídelníček	k1gInSc4
si	se	k3xPyFc3
doplňují	doplňovat	k5eAaImIp3nP
i	i	k9
menšími	malý	k2eAgMnPc7d2
obratlovci	obratlovec	k1gMnPc7
a	a	k8xC
v	v	k7c6
malé	malý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
i	i	k9
plody	plod	k1gInPc4
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
se	se	k3xPyFc4
přiživují	přiživovat	k5eAaImIp3nP
i	i	k9
na	na	k7c6
mršinách	mršina	k1gFnPc6
či	či	k8xC
odpadcích	odpadek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
dožití	dožití	k1gNnSc2
v	v	k7c6
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
10	#num#	k4
do	do	k7c2
14	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
mangusta	mangusta	k1gFnSc1
nemá	mít	k5eNaImIp3nS
zásadní	zásadní	k2eAgMnPc4d1
přirozené	přirozený	k2eAgMnPc4d1
nepřátele	nepřítel	k1gMnPc4
<g/>
,	,	kIx,
mj.	mj.	kA
i	i	k8xC
díky	díky	k7c3
účinné	účinný	k2eAgFnSc3d1
antipredační	antipredační	k2eAgFnSc3d1
ochraně	ochrana	k1gFnSc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
větší	veliký	k2eAgFnPc1d2
šelmy	šelma	k1gFnPc1
ji	on	k3xPp3gFnSc4
mohou	moct	k5eAaImIp3nP
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
zabíjet	zabíjet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
neshledává	shledávat	k5eNaImIp3nS
pro	pro	k7c4
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
žádné	žádný	k3yNgFnSc2
zásadní	zásadní	k2eAgFnSc2d1
hrozby	hrozba	k1gFnSc2
a	a	k8xC
vede	vést	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
jako	jako	k9
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
Mangustu	Mangusta	k1gFnSc4
abuvudan	abuvudana	k1gFnPc2
popsal	popsat	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
přírodovědec	přírodovědec	k1gMnSc1
Georges	Georges	k1gMnSc1
Cuvier	Cuvier	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1829	#num#	k4
pod	pod	k7c7
vědeckým	vědecký	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Herpestes	Herpestes	k1gMnSc1
albicauda	albicauda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhové	druhový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
kvůli	kvůli	k7c3
jejímu	její	k3xOp3gNnSc3
bílému	bílé	k1gNnSc3
ocasu	ocas	k1gInSc2
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
albus	albus	k1gMnSc1
=	=	kIx~
bílý	bílý	k1gMnSc1
<g/>
,	,	kIx,
cauda	cauda	k1gMnSc1
=	=	kIx~
ocas	ocas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
francouzským	francouzský	k2eAgMnSc7d1
zoologem	zoolog	k1gMnSc7
Isidorem	Isidor	k1gMnSc7
Geoffroyem	Geoffroy	k1gMnSc7
Saint-Hilaire	Saint-Hilair	k1gInSc5
přeřazena	přeřazen	k2eAgNnPc4d1
do	do	k7c2
rodu	rod	k1gInSc2
Ichneumia	Ichneumius	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
monotypický	monotypický	k2eAgMnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
jen	jen	k9
tento	tento	k3xDgInSc1
jediný	jediný	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
rodu	rod	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
ichneumon	ichneumon	k1gNnSc4
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
stopař	stopař	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Patří	patřit	k5eAaImIp3nS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
promykovitých	promykovitý	k2eAgInPc2d1
a	a	k8xC
v	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
používá	používat	k5eAaImIp3nS
jak	jak	k6eAd1
název	název	k1gInSc4
mangusta	mangusta	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
promyka	promyka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nejbližšími	blízký	k2eAgMnPc7d3
příbuznými	příbuzný	k1gMnPc7
jsou	být	k5eAaImIp3nP
mangusty	mangusta	k1gFnPc1
rodů	rod	k1gInPc2
Cynictis	Cynictis	k1gFnSc3
<g/>
,	,	kIx,
Rhynochogale	Rhynochogala	k1gFnSc6
a	a	k8xC
Bdeogale	Bdeogala	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Druh	druh	k1gInSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
6	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
7	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
poddruhů	poddruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poddruhy	poddruh	k1gInPc7
byly	být	k5eAaImAgInP
vyděleny	vydělen	k2eAgFnPc4d1
převážně	převážně	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
zbarvení	zbarvení	k1gNnPc2
a	a	k8xC
validita	validita	k1gFnSc1
některých	některý	k3yIgMnPc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
sporná	sporný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
albicauda	albicauda	k1gFnSc1
(	(	kIx(
<g/>
Cuvier	Cuvier	k1gMnSc1
<g/>
,	,	kIx,
1829	#num#	k4
<g/>
)	)	kIx)
–	–	k?
oblast	oblast	k1gFnSc1
Sahelu	Sahel	k1gInSc2
od	od	k7c2
Somálska	Somálsko	k1gNnSc2
<g/>
,	,	kIx,
Eritrey	Eritrea	k1gFnSc2
a	a	k8xC
Súdánu	Súdán	k1gInSc2
po	po	k7c4
Senegal	Senegal	k1gInSc4
<g/>
,	,	kIx,
Arabský	arabský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gMnSc1
dialeucos	dialeucos	k1gMnSc1
(	(	kIx(
<g/>
Hollister	Hollister	k1gMnSc1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
–	–	k?
severovýchod	severovýchod	k1gInSc4
východní	východní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
grandis	grandis	k1gFnSc1
(	(	kIx(
<g/>
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
1890	#num#	k4
<g/>
)	)	kIx)
–	–	k?
jižní	jižní	k2eAgFnSc1d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicaudo	k1gNnSc2
haagneri	haagner	k1gFnSc2
Roberts	Robertsa	k1gFnPc2
<g/>
,	,	kIx,
1924	#num#	k4
–	–	k?
jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gMnSc1
ibeanus	ibeanus	k1gMnSc1
(	(	kIx(
<g/>
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
1904	#num#	k4
<g/>
)	)	kIx)
–	–	k?
středovýchodní	středovýchodní	k2eAgFnSc1d1
a	a	k8xC
východní	východní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
loandae	loandae	k1gFnSc1
(	(	kIx(
<g/>
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
1904	#num#	k4
<g/>
)	)	kIx)
–	–	k?
jihozápadní	jihozápadní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
a	a	k8xC
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
střední	střední	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
loempo	loempa	k1gFnSc5
(	(	kIx(
<g/>
Temminck	Temminck	k1gMnSc1
<g/>
,	,	kIx,
1853	#num#	k4
<g/>
)	)	kIx)
–	–	k?
okraje	okraj	k1gInPc1
lesů	les	k1gInPc2
západní	západní	k2eAgFnSc2d1
a	a	k8xC
severu	sever	k1gInSc2
střední	střední	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Kresba	kresba	k1gFnSc1
lebky	lebka	k1gFnSc2
mangusty	mangusta	k1gFnSc2
abuvudan	abuvudana	k1gFnPc2
</s>
<s>
Mangusta	Mangusta	k1gFnSc1
abuvudan	abuvudana	k1gFnPc2
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
promykovitá	promykovitý	k2eAgFnSc1d1
šelma	šelma	k1gFnSc1
<g/>
,	,	kIx,
zbarvená	zbarvený	k2eAgFnSc1d1
obvykle	obvykle	k6eAd1
šedě	šedě	k6eAd1
<g/>
,	,	kIx,
případně	případně	k6eAd1
šedohnědě	šedohnědě	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
černými	černý	k2eAgFnPc7d1
končetinami	končetina	k1gFnPc7
a	a	k8xC
šedým	šedý	k2eAgInSc7d1
<g/>
,	,	kIx,
stříbrošedým	stříbrošedý	k2eAgInSc7d1
až	až	k8xS
bílým	bílý	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uši	ucho	k1gNnPc4
má	mít	k5eAaImIp3nS
menší	malý	k2eAgNnSc1d2
<g/>
,	,	kIx,
kulaté	kulatý	k2eAgInPc1d1
a	a	k8xC
lehce	lehko	k6eAd1
osrstěné	osrstěný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinci	jedinec	k1gMnSc3
z	z	k7c2
oblastí	oblast	k1gFnPc2
s	s	k7c7
vyššími	vysoký	k2eAgFnPc7d2
srážkami	srážka	k1gFnPc7
mají	mít	k5eAaImIp3nP
zbarvení	zbarvený	k2eAgMnPc1d1
velmi	velmi	k6eAd1
tmavé	tmavý	k2eAgFnSc3d1
až	až	k8xS
melanické	melanický	k2eAgFnSc3d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jedinci	jedinec	k1gMnPc1
ze	z	k7c2
sušších	suchý	k2eAgNnPc2d2
stanovišť	stanoviště	k1gNnPc2
jsou	být	k5eAaImIp3nP
světle	světle	k6eAd1
šedí	šedý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
bílý	bílý	k2eAgInSc1d1
ocas	ocas	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
rozlišovací	rozlišovací	k2eAgInPc4d1
znaky	znak	k1gInPc4
této	tento	k3xDgFnSc2
mangusty	mangusta	k1gFnSc2
<g/>
,	,	kIx,
jedinci	jedinec	k1gMnPc1
s	s	k7c7
černým	černý	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
jsou	být	k5eAaImIp3nP
známí	známý	k2eAgMnPc1d1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
výskytu	výskyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
dokonce	dokonce	k9
převažují	převažovat	k5eAaImIp3nP
nad	nad	k7c7
běloocasými	běloocasý	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Končetiny	končetina	k1gFnPc1
jsou	být	k5eAaImIp3nP
tmavé	tmavý	k2eAgFnPc1d1
a	a	k8xC
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zadní	zadní	k2eAgFnPc1d1
dosahují	dosahovat	k5eAaImIp3nP
větší	veliký	k2eAgFnPc1d2
délky	délka	k1gFnPc1
než	než	k8xS
přední	přední	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakončeny	zakončen	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
pěti	pět	k4xCc3
prsty	prst	k1gInPc4
s	s	k7c7
robustními	robustní	k2eAgInPc7d1
zahnutými	zahnutý	k2eAgInPc7d1
drápy	dráp	k1gInPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
měří	měřit	k5eAaImIp3nP
až	až	k9
14	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anální	anální	k2eAgFnPc4d1
žlázy	žláza	k1gFnPc4
slouží	sloužit	k5eAaImIp3nS
nejspíše	nejspíše	k9
ke	k	k7c3
značkování	značkování	k1gNnSc3
teritoria	teritorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnPc1
mají	mít	k5eAaImIp3nP
tři	tři	k4xCgInPc4
páry	pár	k1gInPc4
bradavek	bradavka	k1gFnPc2
(	(	kIx(
<g/>
někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
udávají	udávat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
páry	pár	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Podsada	podsada	k1gFnSc1
měří	měřit	k5eAaImIp3nS
na	na	k7c4
délku	délka	k1gFnSc4
asi	asi	k9
15	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Pesíky	pesík	k1gInPc1
dorůstají	dorůstat	k5eAaImIp3nP
40	#num#	k4
mm	mm	kA
až	až	k9
100	#num#	k4
mm	mm	kA
a	a	k8xC
nejdelší	dlouhý	k2eAgFnPc1d3
jsou	být	k5eAaImIp3nP
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
těla	tělo	k1gNnSc2
a	a	k8xC
na	na	k7c6
ocase	ocas	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
1,8	1,8	k4
po	po	k7c4
5,2	5,2	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Pohlavní	pohlavní	k2eAgInSc1d1
dimorfismus	dimorfismus	k1gInSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
jako	jako	k9
nevýrazný	výrazný	k2eNgMnSc1d1
<g/>
,	,	kIx,
samci	samec	k1gMnPc1
bývají	bývat	k5eAaImIp3nP
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgMnSc1d2
a	a	k8xC
těžší	těžký	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrné	průměrný	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
hmotností	hmotnost	k1gFnSc7
pro	pro	k7c4
samce	samec	k1gMnPc4
se	se	k3xPyFc4
udávají	udávat	k5eAaImIp3nP
3,6	3,6	k4
kg	kg	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
4,5	4,5	k4
kg	kg	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
pro	pro	k7c4
samice	samice	k1gFnPc4
3,4	3,4	k4
kg	kg	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
respektive	respektive	k9
4,1	4,1	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
většinou	většina	k1gFnSc7
přesahuje	přesahovat	k5eAaImIp3nS
1	#num#	k4
metr	metr	k1gInSc4
a	a	k8xC
největší	veliký	k2eAgMnPc1d3
jedinci	jedinec	k1gMnPc1
mohou	moct	k5eAaImIp3nP
měřit	měřit	k5eAaImF
až	až	k9
okolo	okolo	k7c2
1,5	1,5	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
těla	tělo	k1gNnSc2
od	od	k7c2
špičky	špička	k1gFnSc2
čenichu	čenich	k1gInSc2
po	po	k7c4
kořen	kořen	k1gInSc4
ocasu	ocas	k1gInSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
47	#num#	k4
do	do	k7c2
104	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
ocas	ocas	k1gInSc1
měří	měřit	k5eAaImIp3nS
34	#num#	k4
cm	cm	kA
až	až	k9
49	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Délka	délka	k1gFnSc1
lebky	lebka	k1gFnSc2
je	být	k5eAaImIp3nS
9,6	9,6	k4
až	až	k9
11,6	11,6	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Špičáky	špičák	k1gInPc1
mají	mít	k5eAaImIp3nP
ostrý	ostrý	k2eAgInSc4d1
konec	konec	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
lehce	lehko	k6eAd1
zahnuté	zahnutý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
jsou	být	k5eAaImIp3nP
zuby	zub	k1gInPc1
široké	široký	k2eAgInPc1d1
a	a	k8xC
těžké	těžký	k2eAgInPc1d1
<g/>
,	,	kIx,
především	především	k6eAd1
poslední	poslední	k2eAgInPc4d1
premoláry	premolár	k1gInPc4
a	a	k8xC
první	první	k4xOgFnPc1
dvě	dva	k4xCgFnPc1
stoličky	stolička	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zubní	zubní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
I	i	k9
3	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
C	C	kA
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
P	P	kA
4	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
M	M	kA
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
=	=	kIx~
40	#num#	k4
zubů	zub	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
<g/>
,	,	kIx,
biotop	biotop	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
mangusty	mangusta	k1gFnSc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
subsaharské	subsaharský	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
od	od	k7c2
Gambie	Gambie	k1gFnSc2
a	a	k8xC
Senegalu	Senegal	k1gInSc2
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c4
Africký	africký	k2eAgInSc4d1
roh	roh	k1gInSc4
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihovýchod	jihovýchod	k1gInSc1
jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Obývá	obývat	k5eAaImIp3nS
také	také	k9
pobřežní	pobřežní	k2eAgInSc1d1
pás	pás	k1gInSc1
jižní	jižní	k2eAgFnSc2d1
části	část	k1gFnSc2
Arabského	arabský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
asi	asi	k9
před	před	k7c7
32	#num#	k4
500	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
lety	léto	k1gNnPc7
při	při	k7c6
jedné	jeden	k4xCgFnSc6
kolonizační	kolonizační	k2eAgFnSc6d1
vlně	vlna	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Rudém	rudý	k2eAgNnSc6d1
moři	moře	k1gNnSc6
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
i	i	k9
na	na	k7c6
ostrově	ostrov	k1gInSc6
Farasan	Farasana	k1gFnPc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
byla	být	k5eAaImAgFnS
nejspíše	nejspíše	k9
zavlečena	zavlečen	k2eAgFnSc1d1
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Konkrétně	konkrétně	k6eAd1
obývá	obývat	k5eAaImIp3nS
tyto	tento	k3xDgInPc1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Angola	Angola	k1gFnSc1
<g/>
,	,	kIx,
Benin	Benin	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Botswana	Botswana	k1gFnSc1
<g/>
,	,	kIx,
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
<g/>
,	,	kIx,
Čad	Čad	k1gInSc1
<g/>
,	,	kIx,
Džibutsko	Džibutsko	k1gNnSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Eritrea	Eritrea	k1gFnSc1
<g/>
,	,	kIx,
Etiopie	Etiopie	k1gFnSc1
<g/>
,	,	kIx,
Gambie	Gambie	k1gFnSc1
<g/>
,	,	kIx,
Ghana	Ghana	k1gFnSc1
<g/>
,	,	kIx,
Guinea	Guinea	k1gFnSc1
<g/>
,	,	kIx,
Guinea-Bissau	Guinea-Bissaa	k1gFnSc4
<g/>
,	,	kIx,
Jemen	Jemen	k1gInSc1
<g/>
,	,	kIx,
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
<g/>
,	,	kIx,
Kamerun	Kamerun	k1gInSc1
<g/>
,	,	kIx,
Keňa	Keňa	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Lesotho	Lesot	k1gMnSc2
<g/>
,	,	kIx,
Malawi	Malawi	k1gNnSc1
<g/>
,	,	kIx,
Mali	Mali	k1gNnSc1
<g/>
,	,	kIx,
Mosambik	Mosambik	k1gInSc1
<g/>
,	,	kIx,
Namibie	Namibie	k1gFnSc1
<g/>
,	,	kIx,
Niger	Niger	k1gInSc1
<g/>
;	;	kIx,
Nigerie	Nigerie	k1gFnSc1
<g/>
;	;	kIx,
Omán	Omán	k1gInSc1
<g/>
;	;	kIx,
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
<g/>
,	,	kIx,
Saudská	saudský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
,	,	kIx,
Senegal	Senegal	k1gInSc1
<g/>
,	,	kIx,
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
<g/>
,	,	kIx,
Somálsko	Somálsko	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
,	,	kIx,
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Súdán	Súdán	k1gInSc1
<g/>
,	,	kIx,
Svazijsko	Svazijsko	k1gNnSc1
<g/>
,	,	kIx,
Uganda	Uganda	k1gFnSc1
<g/>
,	,	kIx,
Zambie	Zambie	k1gFnSc1
<g/>
,	,	kIx,
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
pobřeží	pobřeží	k1gNnSc2
až	až	k9
do	do	k7c2
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
3500	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Typickým	typický	k2eAgNnSc7d1
stanovištěm	stanoviště	k1gNnSc7
mangusty	mangusta	k1gFnSc2
abuvudan	abuvudany	k1gInPc2
jsou	být	k5eAaImIp3nP
travnaté	travnatý	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
(	(	kIx(
<g/>
stepi	step	k1gFnPc4
<g/>
,	,	kIx,
savany	savana	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nežije	žít	k5eNaImIp3nS
v	v	k7c6
tropických	tropický	k2eAgInPc6d1
deštných	deštný	k2eAgInPc6d1
lesích	les	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
pouštích	poušť	k1gFnPc6
<g/>
,	,	kIx,
polopouštích	polopoušť	k1gFnPc6
a	a	k8xC
na	na	k7c6
horách	hora	k1gFnPc6
nad	nad	k7c7
hranicí	hranice	k1gFnSc7
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
flexibilní	flexibilní	k2eAgFnSc1d1
a	a	k8xC
dokáže	dokázat	k5eAaPmIp3nS
se	se	k3xPyFc4
adaptovat	adaptovat	k5eAaBmF
na	na	k7c4
antropogenní	antropogenní	k2eAgInPc4d1
zásahy	zásah	k1gInPc4
do	do	k7c2
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
zemědělských	zemědělský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
spatřena	spatřit	k5eAaPmNgFnS
na	na	k7c6
zahradách	zahrada	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
plantážích	plantáž	k1gFnPc6
a	a	k8xC
ve	v	k7c6
městech	město	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
přiživuje	přiživovat	k5eAaImIp3nS
na	na	k7c6
zbytcích	zbytek	k1gInPc6
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biologie	biologie	k1gFnSc1
</s>
<s>
Chování	chování	k1gNnSc1
<g/>
,	,	kIx,
teritorialita	teritorialita	k1gFnSc1
</s>
<s>
Mangusta	Mangusta	k1gMnSc1
abuvudan	abuvudan	k1gMnSc1
je	být	k5eAaImIp3nS
nejběhavější	běhavý	k2eAgMnSc1d3
ze	z	k7c2
všech	všecek	k3xTgMnPc2
mangust	mangust	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lezení	lezení	k1gNnPc1
po	po	k7c6
stromech	strom	k1gInPc6
ji	on	k3xPp3gFnSc4
činí	činit	k5eAaImIp3nP
problémy	problém	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
plavkyně	plavkyně	k1gFnSc1
je	být	k5eAaImIp3nS
obstojná	obstojný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drápy	dráp	k1gInPc4
má	mít	k5eAaImIp3nS
sice	sice	k8xC
vhodné	vhodný	k2eAgNnSc1d1
k	k	k7c3
hrabání	hrabání	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
nehrabe	hrabat	k5eNaImIp3nS
svá	svůj	k3xOyFgNnPc4
vlastní	vlastní	k2eAgNnPc4d1
doupata	doupě	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
používá	používat	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
po	po	k7c6
jiných	jiný	k2eAgInPc6d1
zvířatech	zvíře	k1gNnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
po	po	k7c6
hrabáčích	hrabáč	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
odpočinku	odpočinek	k1gInSc3
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
též	též	k9
rozhrabaná	rozhrabaný	k2eAgNnPc1d1
opuštěná	opuštěný	k2eAgNnPc1d1
termitiště	termitiště	k1gNnPc1
<g/>
,	,	kIx,
dutiny	dutina	k1gFnPc1
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgFnPc4d1
rozsedliny	rozsedlina	k1gFnPc4
a	a	k8xC
opuštěné	opuštěný	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
jedinec	jedinec	k1gMnSc1
disponuje	disponovat	k5eAaBmIp3nS
obvykle	obvykle	k6eAd1
více	hodně	k6eAd2
doupaty	doupě	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pravidelně	pravidelně	k6eAd1
střídá	střídat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
zvíře	zvíře	k1gNnSc4
s	s	k7c7
výrazně	výrazně	k6eAd1
noční	noční	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
po	po	k7c6
setmění	setmění	k1gNnSc6
opouští	opouštět	k5eAaImIp3nS
doupě	doupě	k1gNnSc4
a	a	k8xC
vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
brzy	brzy	k6eAd1
ráno	ráno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denní	denní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
byla	být	k5eAaImAgFnS
nicméně	nicméně	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
též	též	k9
zaznamenána	zaznamenán	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Žije	žít	k5eAaImIp3nS
převážně	převážně	k6eAd1
samotářsky	samotářsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
spatřeny	spatřen	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
většinou	většina	k1gFnSc7
o	o	k7c4
samici	samice	k1gFnSc4
s	s	k7c7
mláďaty	mládě	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
oblastech	oblast	k1gFnPc6
s	s	k7c7
bohatými	bohatý	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
potravy	potrava	k1gFnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
shromáždit	shromáždit	k5eAaPmF
více	hodně	k6eAd2
jedinců	jedinec	k1gMnPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
zvíře	zvíře	k1gNnSc4
teritoriální	teritoriální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
domovského	domovský	k2eAgInSc2d1
okrsku	okrsek	k1gInSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
pohybovat	pohybovat	k5eAaImF
od	od	k7c2
0,03	0,03	k4
km	km	kA
<g/>
2	#num#	k4
až	až	k8xS
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
8	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
samců	samec	k1gInPc2
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
2	#num#	k4
až	až	k9
3	#num#	k4
teritoria	teritorium	k1gNnSc2
samic	samice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střety	střet	k1gInPc1
samců	samec	k1gMnPc2
o	o	k7c6
území	území	k1gNnSc6
nebyla	být	k5eNaImAgFnS
pozorována	pozorovat	k5eAaImNgFnS
<g/>
,	,	kIx,
pachové	pachový	k2eAgFnPc1d1
značky	značka	k1gFnPc1
jsou	být	k5eAaImIp3nP
nejspíše	nejspíše	k9
dostačující	dostačující	k2eAgInSc1d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
k	k	k7c3
nim	on	k3xPp3gFnPc3
nedocházelo	docházet	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1
interakce	interakce	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
zaznamenána	zaznamenat	k5eAaPmNgFnS
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
k	k	k7c3
ní	on	k3xPp3gFnSc3
dojde	dojít	k5eAaPmIp3nS
<g/>
,	,	kIx,
zvířata	zvíře	k1gNnPc1
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
obvykle	obvykle	k6eAd1
očichávají	očichávat	k5eAaImIp3nP
(	(	kIx(
<g/>
čenichy	čenich	k1gInPc1
a	a	k8xC
genitálie	genitálie	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
pak	pak	k6eAd1
pokračují	pokračovat	k5eAaImIp3nP
každý	každý	k3xTgInSc4
svou	svůj	k3xOyFgFnSc7
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
cizincům	cizinec	k1gMnPc3
se	se	k3xPyFc4
někdy	někdy	k6eAd1
může	moct	k5eAaImIp3nS
místní	místní	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
chovat	chovat	k5eAaImF
agresivně	agresivně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vyprazdňování	vyprazdňování	k1gNnSc3
dochází	docházet	k5eAaImIp3nS
poblíž	poblíž	k6eAd1
doupat	doupě	k1gNnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
je	být	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
využívaná	využívaný	k2eAgFnSc1d1
poměrně	poměrně	k6eAd1
velká	velký	k2eAgFnSc1d1
„	„	k?
<g/>
latrína	latrína	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
emitaci	emitace	k1gFnSc3
pachu	pach	k1gInSc2
a	a	k8xC
tedy	tedy	k9
značkování	značkování	k1gNnSc1
používá	používat	k5eAaImIp3nS
anální	anální	k2eAgFnSc4d1
žlázu	žláza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mangusta	Mangusta	k1gMnSc1
je	být	k5eAaImIp3nS
zvíře	zvíře	k1gNnSc4
většinou	většinou	k6eAd1
tiché	tichý	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
někdy	někdy	k6eAd1
může	moct	k5eAaImIp3nS
vydávat	vydávat	k5eAaPmF,k5eAaImF
chrčivý	chrčivý	k2eAgInSc4d1
či	či	k8xC
štěkavý	štěkavý	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
cítí	cítit	k5eAaImIp3nS
ohrožená	ohrožený	k2eAgFnSc1d1
nebo	nebo	k8xC
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
bránit	bránit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Mangusta	Mangusta	k1gMnSc1
abuvudan	abuvudan	k1gMnSc1
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
hmyzožravá	hmyzožravý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preferuje	preferovat	k5eAaImIp3nS
přitom	přitom	k6eAd1
termity	termit	k1gMnPc4
<g/>
,	,	kIx,
brouky	brouk	k1gMnPc4
<g/>
,	,	kIx,
kobylky	kobylka	k1gFnPc4
<g/>
,	,	kIx,
cvrčky	cvrček	k1gMnPc4
a	a	k8xC
larvy	larva	k1gFnPc4
brouků	brouk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc4d1
složku	složka	k1gFnSc4
potravy	potrava	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
obojživelníci	obojživelník	k1gMnPc1
<g/>
,	,	kIx,
myšovití	myšovití	k1gMnPc1
<g/>
,	,	kIx,
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
malí	malý	k2eAgMnPc1d1
hmyzožravci	hmyzožravec	k1gMnPc1
a	a	k8xC
někteří	některý	k3yIgMnPc1
další	další	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
bezobratlých	bezobratlí	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
plži	plž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
složek	složka	k1gFnPc2
potravy	potrava	k1gFnSc2
závisí	záviset	k5eAaImIp3nS
i	i	k9
na	na	k7c6
ročním	roční	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
dešťů	dešť	k1gInPc2
převažuje	převažovat	k5eAaImIp3nS
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
suchém	suchý	k2eAgNnSc6d1
období	období	k1gNnSc6
pak	pak	k6eAd1
roste	růst	k5eAaImIp3nS
podíl	podíl	k1gInSc1
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
mnohé	mnohý	k2eAgInPc1d1
další	další	k2eAgInPc1d1
mangusty	mangust	k1gInPc1
i	i	k8xC
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
ulovit	ulovit	k5eAaPmF
jedovaté	jedovatý	k2eAgMnPc4d1
hady	had	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živí	živit	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k8xC
vejci	vejce	k1gNnSc3
a	a	k8xC
nepohrdne	pohrdnout	k5eNaPmIp3nS
ani	ani	k8xC
mršinami	mršina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplňkovou	doplňkový	k2eAgFnSc4d1
část	část	k1gFnSc4
potravy	potrava	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
ovoce	ovoce	k1gNnPc4
a	a	k8xC
rostlinný	rostlinný	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
,	,	kIx,
občas	občas	k6eAd1
se	se	k3xPyFc4
přiživí	přiživit	k5eAaPmIp3nP
na	na	k7c6
potravinových	potravinový	k2eAgInPc6d1
odpadcích	odpadek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledání	hledání	k1gNnSc1
potravy	potrava	k1gFnSc2
probíhá	probíhat	k5eAaImIp3nS
osamoceně	osamoceně	k6eAd1
a	a	k8xC
především	především	k6eAd1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
čichu	čich	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
trvat	trvat	k5eAaImF
6	#num#	k4
až	až	k9
8	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
mangusta	mangusta	k1gFnSc1
přitom	přitom	k6eAd1
urazí	urazit	k5eAaPmIp3nS
4	#num#	k4
až	až	k9
5	#num#	k4
km	km	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
prokřižuje	prokřižovat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaImIp3nSwR
prakticky	prakticky	k6eAd1
celé	celý	k2eAgNnSc4d1
své	svůj	k3xOyFgNnSc4
teritorium	teritorium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
k	k	k7c3
lovu	lov	k1gInSc2
bezobratlých	bezobratlí	k1gMnPc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
kulturní	kulturní	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
kumulaci	kumulace	k1gFnSc3
hmyzu	hmyz	k1gInSc2
(	(	kIx(
<g/>
noční	noční	k2eAgNnPc1d1
osvětlení	osvětlení	k1gNnPc1
<g/>
,	,	kIx,
odpadky	odpadek	k1gInPc1
<g/>
,	,	kIx,
silnice	silnice	k1gFnPc1
<g/>
,	,	kIx,
chovy	chov	k1gInPc1
dobytka	dobytek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
<g/>
,	,	kIx,
dožití	dožití	k1gNnSc1
</s>
<s>
O	o	k7c6
rozmnožování	rozmnožování	k1gNnSc6
mangust	mangust	k1gInSc1
abuvudan	abuvudany	k1gInPc2
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dostatečné	dostatečný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
říje	říje	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zřejmě	zřejmě	k6eAd1
bývá	bývat	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
období	období	k1gNnSc2
sucha	sucho	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
samec	samec	k1gMnSc1
a	a	k8xC
samice	samice	k1gFnSc1
na	na	k7c4
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
sdružují	sdružovat	k5eAaImIp3nP
do	do	k7c2
párů	pár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
kopulaci	kopulace	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
trvá	trvat	k5eAaImIp3nS
5	#num#	k4
až	až	k9
40	#num#	k4
sekund	sekunda	k1gFnPc2
a	a	k8xC
probíhá	probíhat	k5eAaImIp3nS
asi	asi	k9
15	#num#	k4
až	až	k9
20	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
samice	samice	k1gFnSc1
se	s	k7c7
samcem	samec	k1gInSc7
ještě	ještě	k6eAd1
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
po	po	k7c6
kopulacích	kopulace	k1gFnPc6
zůstává	zůstávat	k5eAaImIp3nS
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
si	se	k3xPyFc3
krátce	krátce	k6eAd1
poté	poté	k6eAd1
najde	najít	k5eAaPmIp3nS
jiného	jiný	k2eAgMnSc4d1
samce	samec	k1gMnSc4
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
rovněž	rovněž	k9
spáří	spářet	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
březosti	březost	k1gFnSc2
je	být	k5eAaImIp3nS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
2	#num#	k4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nP
nejčastěji	často	k6eAd3
<g/>
,	,	kIx,
ale	ale	k8xC
nikoli	nikoli	k9
výhradně	výhradně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
regionálně	regionálně	k6eAd1
časově	časově	k6eAd1
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
nejspíše	nejspíše	k9
v	v	k7c6
doupatech	doupě	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vrhu	vrh	k1gInSc6
jich	on	k3xPp3gMnPc2
bývá	bývat	k5eAaImIp3nS
1	#num#	k4
až	až	k9
4	#num#	k4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Mláďata	mládě	k1gNnPc1
jsou	být	k5eAaImIp3nP
po	po	k7c6
narození	narození	k1gNnSc6
slepá	slepý	k2eAgFnSc1d1
a	a	k8xC
holá	holý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Jedno	jeden	k4xCgNnSc1
po	po	k7c6
narození	narození	k1gNnSc6
změřené	změřený	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
mládě	mládě	k1gNnSc1
vážilo	vážit	k5eAaImAgNnS
81	#num#	k4
gramů	gram	k1gInPc2
a	a	k8xC
měřilo	měřit	k5eAaImAgNnS
i	i	k9
s	s	k7c7
ocasem	ocas	k1gInSc7
necelých	celý	k2eNgInPc2d1
25	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
odchovu	odchov	k1gInSc6
mláďat	mládě	k1gNnPc2
bývá	bývat	k5eAaImIp3nS
samice	samice	k1gFnSc1
velmi	velmi	k6eAd1
agresivní	agresivní	k2eAgFnSc1d1
a	a	k8xC
schopná	schopný	k2eAgFnSc1d1
napadnout	napadnout	k5eAaPmF
jakéhokoliv	jakýkoliv	k3yIgMnSc4
vetřelce	vetřelec	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Nezávislá	závislý	k2eNgFnSc1d1
se	se	k3xPyFc4
mláďata	mládě	k1gNnPc1
stávají	stávat	k5eAaImIp3nP
asi	asi	k9
ve	v	k7c6
věku	věk	k1gInSc6
9	#num#	k4
měsíců	měsíc	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
ale	ale	k8xC
často	často	k6eAd1
i	i	k9
poté	poté	k6eAd1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
(	(	kIx(
<g/>
cca	cca	kA
4	#num#	k4
měsíce	měsíc	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
mateřském	mateřský	k2eAgNnSc6d1
teritoriu	teritorium	k1gNnSc6
a	a	k8xC
samice	samice	k1gFnSc1
spolu	spolu	k6eAd1
mohou	moct	k5eAaImIp3nP
tvořit	tvořit	k5eAaImF
malé	malý	k2eAgFnPc1d1
<g/>
,	,	kIx,
dočasné	dočasný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
mangusty	mangusta	k1gFnPc1
mohou	moct	k5eAaImIp3nP
dožít	dožít	k5eAaPmF
10	#num#	k4
až	až	k9
14	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Doba	doba	k1gFnSc1
dožití	dožití	k1gNnSc1
v	v	k7c6
přírodě	příroda	k1gFnSc6
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Přirození	přirozený	k2eAgMnPc1d1
nepřátelé	nepřítel	k1gMnPc1
<g/>
,	,	kIx,
paraziti	parazit	k1gMnPc1
</s>
<s>
Mangusta	Mangusta	k1gMnSc1
abuvudan	abuvudan	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zabita	zabít	k5eAaPmNgFnS
různými	různý	k2eAgFnPc7d1
většími	veliký	k2eAgFnPc7d2
predátory	predátor	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nejspíše	nejspíše	k9
se	se	k3xPyFc4
tak	tak	k6eAd1
neděje	dít	k5eNaImIp3nS
často	často	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
zaznamenány	zaznamenán	k2eAgInPc1d1
případy	případ	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
usmrcena	usmrcen	k2eAgFnSc1d1
levhartem	levhart	k1gMnSc7
<g/>
,	,	kIx,
psy	pes	k1gMnPc7
hyenovými	hyenová	k1gFnPc7
a	a	k8xC
v	v	k7c6
urbánních	urbánní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
potulnými	potulný	k2eAgFnPc7d1
psy	pes	k1gMnPc7
domácími	domácí	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
stát	stát	k5eAaImF,k5eAaPmF
kořistí	kořist	k1gFnSc7
větších	veliký	k2eAgMnPc2d2
dravých	dravý	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Schopnost	schopnost	k1gFnSc1
obrany	obrana	k1gFnSc2
proti	proti	k7c3
predátorům	predátor	k1gMnPc3
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
velmi	velmi	k6eAd1
účinnou	účinný	k2eAgFnSc4d1
<g/>
,	,	kIx,
především	především	k9
pomocí	pomocí	k7c2
výhrůžného	výhrůžný	k2eAgInSc2d1
postoje	postoj	k1gInSc2
<g/>
,	,	kIx,
kousání	kousání	k1gNnSc2
a	a	k8xC
smrdutých	smrdutý	k2eAgInPc2d1
výměšků	výměšek	k1gInPc2
z	z	k7c2
análních	anální	k2eAgFnPc2d1
žláz	žláza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
parazity	parazit	k1gMnPc7
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nP
různá	různý	k2eAgNnPc1d1
klíšťata	klíště	k1gNnPc1
(	(	kIx(
<g/>
např.	např.	kA
Amblyomma	Amblyomma	k1gNnSc1
hebraeum	hebraeum	k1gNnSc1
a	a	k8xC
Rhipicephalus	Rhipicephalus	k1gMnSc1
appendiculatus	appendiculatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
blechy	blecha	k1gFnSc2
<g/>
,	,	kIx,
roztoči	roztoč	k1gMnPc1
a	a	k8xC
vši	veš	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
vnitřní	vnitřní	k2eAgMnPc1d1
paraziti	parazit	k1gMnPc1
byli	být	k5eAaImAgMnP
zaznamenáni	zaznamenán	k2eAgMnPc1d1
hlístice	hlístice	k1gFnSc2
a	a	k8xC
tasemnice	tasemnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
Jedinec	jedinec	k1gMnSc1
chycený	chycený	k2eAgMnSc1d1
v	v	k7c6
kleci	klec	k1gFnSc6
</s>
<s>
Interakce	interakce	k1gFnSc1
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
hrozby	hrozba	k1gFnPc4
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
mangustu	mangusta	k1gFnSc4
abuvudan	abuvudana	k1gFnPc2
jako	jako	k8xC,k8xS
druh	druh	k1gInSc1
málo	málo	k6eAd1
dotčený	dotčený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udává	udávat	k5eAaImIp3nS
při	při	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
nehrozí	hrozit	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc4
zásadní	zásadní	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
populace	populace	k1gFnSc2
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgFnSc1d1
a	a	k8xC
obývá	obývat	k5eAaImIp3nS
mnoho	mnoho	k4c1
chráněných	chráněný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
jediného	jediný	k2eAgMnSc4d1
skutečného	skutečný	k2eAgMnSc4d1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
jen	jen	k9
příležitostného	příležitostný	k2eAgMnSc4d1
lovce	lovec	k1gMnSc4
této	tento	k3xDgFnSc2
šelmy	šelma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Noční	noční	k2eAgNnSc1d1
hledání	hledání	k1gNnSc1
potravy	potrava	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c6
silnicích	silnice	k1gFnPc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pro	pro	k7c4
mangusty	mangusta	k1gFnPc4
nebezpečné	bezpečný	k2eNgFnPc1d1
kvůli	kvůli	k7c3
střetům	střet	k1gInPc3
s	s	k7c7
vozidly	vozidlo	k1gNnPc7
<g/>
,	,	kIx,
takových	takový	k3xDgInPc2
případů	případ	k1gInPc2
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenán	k2eAgNnSc1d1
mnoho	mnoho	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Občas	občas	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
chycena	chytit	k5eAaPmNgFnS
nebo	nebo	k8xC
zabita	zabít	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
programů	program	k1gInPc2
na	na	k7c4
kontrolu	kontrola	k1gFnSc4
predátorů	predátor	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnPc1
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
zaměřeny	zaměřit	k5eAaPmNgFnP
na	na	k7c4
šakaly	šakal	k1gMnPc4
a	a	k8xC
karakaly	karakal	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kůže	kůže	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
využívána	využívat	k5eAaPmNgNnP,k5eAaImNgNnP
některými	některý	k3yIgFnPc7
komunitami	komunita	k1gFnPc7
(	(	kIx(
<g/>
jihoafričtí	jihoafrický	k2eAgMnPc1d1
Vendové	Vend	k1gMnPc1
<g/>
)	)	kIx)
k	k	k7c3
ceremoniálním	ceremoniální	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mangusta	Mangusta	k1gFnSc1
není	být	k5eNaImIp3nS
lovena	lovit	k5eAaImNgFnS
jako	jako	k8xC,k8xS
zdroj	zdroj	k1gInSc1
masa	maso	k1gNnSc2
z	z	k7c2
divočiny	divočina	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
bushmeat	bushmeat	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
jedinci	jedinec	k1gMnPc1
byli	být	k5eAaImAgMnP
odchyceni	odchytit	k5eAaPmNgMnP
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
chování	chování	k1gNnSc2
pozorováno	pozorovat	k5eAaImNgNnS
v	v	k7c6
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelišilo	lišit	k5eNaImAgNnS
se	se	k3xPyFc4
od	od	k7c2
běžného	běžný	k2eAgNnSc2d1
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
kupříkladu	kupříkladu	k6eAd1
zvířata	zvíře	k1gNnPc1
byla	být	k5eAaImAgNnP
aktivní	aktivní	k2eAgInSc4d1
především	především	k6eAd1
v	v	k7c6
noci	noc	k1gFnSc6
a	a	k8xC
ve	v	k7c6
dne	den	k1gInSc2
odpočívala	odpočívat	k5eAaImAgFnS
a	a	k8xC
spala	spát	k5eAaImAgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
obvykle	obvykle	k6eAd1
tichá	tichý	k2eAgFnSc1d1
a	a	k8xC
hlasově	hlasově	k6eAd1
se	se	k3xPyFc4
projevovala	projevovat	k5eAaImAgFnS
jen	jen	k9
při	při	k7c6
rozrušení	rozrušení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
mangusta	mangusta	k1gFnSc1
abuvudan	abuvudany	k1gInPc2
Ichneumia	Ichneumius	k1gMnSc2
albicauda	albicaud	k1gMnSc2
(	(	kIx(
<g/>
Cuvier	Cuvier	k1gMnSc1
<g/>
,	,	kIx,
1829	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.biolib.cz	www.biolib.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
HUNTER	HUNTER	kA
<g/>
,	,	kIx,
Luke	Luke	k1gFnSc1
<g/>
;	;	kIx,
BARRETT	BARRETT	kA
<g/>
,	,	kIx,
Priscilla	Priscillo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Field	Field	k1gMnSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Carnivores	Carnivores	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
etc	etc	k?
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Panthera	Panthera	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Holland	Hollanda	k1gFnPc2
Publishers	Publishersa	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
,	,	kIx,
195	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
MATSON	MATSON	kA
<g/>
,	,	kIx,
Malorey	Malorey	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
(	(	kIx(
<g/>
white-tailed	white-tailed	k1gInSc1
mongoose	mongoosa	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Animal	animal	k1gMnSc1
Diversity	Diversit	k1gInPc4
Web	web	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
DO	do	k7c2
LINH	LINH	kA
SAN	SAN	kA
<g/>
,	,	kIx,
Emmanuel	Emmanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnPc2
<g/>
,	,	kIx,
2015-02-28	2015-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
iucn	iucna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.2015	.2015	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
rlts	rltsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
t	t	k?
<g/>
41620	#num#	k4
<g/>
a	a	k8xC
<g/>
45208640	#num#	k4
<g/>
.	.	kIx.
<g/>
en	en	k?
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
E.	E.	kA
Ichneumia	Ichneumius	k1gMnSc4
albicauda	albicaud	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mammalian	Mammalian	k1gInSc1
Species	species	k1gFnSc2
<g/>
.	.	kIx.
1972	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
3504028	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
19	#num#	k4
20	#num#	k4
21	#num#	k4
KINGDON	KINGDON	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mammals	Mammals	k1gInSc1
of	of	k?
Africa	Afric	k1gInSc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
V	V	kA
<g/>
:	:	kIx,
Carnivores	Carnivores	k1gMnSc1
<g/>
,	,	kIx,
Pangolins	Pangolins	k1gInSc1
<g/>
,	,	kIx,
Equids	Equids	k1gInSc1
and	and	k?
Rhinoceroses	Rhinoceroses	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Bloomsbury	Bloomsbura	k1gFnPc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
E.	E.	kA
<g/>
:	:	kIx,
Ichenumia	Ichenumius	k1gMnSc2
albicauda	albicaud	k1gMnSc2
<g/>
,	,	kIx,
White-tailed	White-tailed	k1gInSc1
mongoose	mongoosa	k1gFnSc3
<g/>
,	,	kIx,
s.	s.	k?
342	#num#	k4
<g/>
–	–	k?
<g/>
346	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PATOU	pata	k1gFnSc7
<g/>
,	,	kIx,
Marie-Lilith	Marie-Lilith	k1gInSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
phylogeny	phylogen	k1gInPc1
of	of	k?
the	the	k?
Herpestidae	Herpestidae	k1gInSc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
,	,	kIx,
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
with	with	k1gMnSc1
a	a	k8xC
special	speciat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
emphasis	emphasis	k1gInSc4
on	on	k3xPp3gMnSc1
the	the	k?
Asian	Asian	k1gMnSc1
Herpestes	Herpestes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Phylogenetics	Phylogenetics	k1gInSc4
and	and	k?
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
53	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1055	#num#	k4
<g/>
-	-	kIx~
<g/>
7903	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
ympev	ympev	k1gFnSc1
<g/>
.2009	.2009	k4
<g/>
.05	.05	k4
<g/>
.038	.038	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ITIS	ITIS	kA
Standard	standard	k1gInSc1
Report	report	k1gInSc1
Page	Page	k1gFnSc1
<g/>
:	:	kIx,
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
<g/>
.	.	kIx.
www.itis.gov	www.itis.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WILSON	WILSON	kA
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
E.	E.	kA
<g/>
;	;	kIx,
REEDER	REEDER	kA
<g/>
,	,	kIx,
DeeAnn	DeeAnn	k1gMnSc1
M.	M.	kA
Mammal	Mammal	k1gMnSc1
Species	species	k1gFnSc2
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Taxonomic	Taxonomic	k1gMnSc1
and	and	k?
Geographic	Geographic	k1gMnSc1
Reference	reference	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baltimore	Baltimore	k1gInSc1
<g/>
:	:	kIx,
JHU	jho	k1gNnSc3
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
2201	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8018	#num#	k4
<g/>
-	-	kIx~
<g/>
8221	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
570	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
JgAMbNSt	JgAMbNSt	k1gInSc1
<g/>
8	#num#	k4
<g/>
ikC	ikC	k?
<g/>
.	.	kIx.
↑	↑	k?
DEHGHANI	DEHGHANI	kA
<g/>
,	,	kIx,
R.	R.	kA
<g/>
;	;	kIx,
WANNTORP	WANNTORP	kA
<g/>
,	,	kIx,
L.	L.	kA
<g/>
;	;	kIx,
PAGANI	PAGANI	kA
<g/>
,	,	kIx,
P.	P.	kA
Phylogeography	Phylogeographa	k1gFnPc1
of	of	k?
the	the	k?
white-tailed	white-tailed	k1gInSc1
mongoose	mongoosa	k1gFnSc3
(	(	kIx(
<g/>
Herpestidae	Herpestidae	k1gFnSc1
<g/>
,	,	kIx,
Carnivora	Carnivora	k1gFnSc1
<g/>
,	,	kIx,
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
based	based	k1gMnSc1
on	on	k3xPp3gMnSc1
partial	partiat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
sequences	sequences	k1gInSc4
of	of	k?
the	the	k?
mtDNA	mtDNA	k?
control	control	k1gInSc1
region	region	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Zoology	zoolog	k1gMnPc7
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
276	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
385	#num#	k4
<g/>
–	–	k?
<g/>
393	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1469	#num#	k4
<g/>
-	-	kIx~
<g/>
7998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1469	.1469	k4
<g/>
-	-	kIx~
<g/>
7998.2008	7998.2008	k4
<g/>
.00502	.00502	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FERNANDES	FERNANDES	kA
<g/>
,	,	kIx,
Carlos	Carlos	k1gMnSc1
A.	A.	kA
Colonization	Colonization	k1gInSc1
time	tim	k1gMnSc2
of	of	k?
Arabia	Arabius	k1gMnSc2
by	by	k9
the	the	k?
White-tailed	White-tailed	k1gMnSc1
Mongoose	Mongoosa	k1gFnSc3
Ichneumia	Ichneumius	k1gMnSc2
albicauda	albicaud	k1gMnSc2
as	as	k1gNnSc2
inferred	inferred	k1gMnSc1
from	from	k1gMnSc1
mitochondrial	mitochondrial	k1gMnSc1
DNA	DNA	kA
sequences	sequences	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoology	zoolog	k1gMnPc4
in	in	k?
the	the	k?
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
54	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgMnSc1wB
<g/>
.	.	kIx.
sup	sup	k1gMnSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
111	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
939	#num#	k4
<g/>
-	-	kIx~
<g/>
7140	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9397140.2011	9397140.2011	k4
<g/>
.10648903	.10648903	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
DO	do	k7c2
LINH	LINH	kA
SAN	SAN	kA
<g/>
,	,	kIx,
E.	E.	kA
<g/>
;	;	kIx,
STUART	STUART	kA
<g/>
,	,	kIx,
C.	C.	kA
<g/>
;	;	kIx,
STUART	STUART	kA
<g/>
,	,	kIx,
M.	M.	kA
A	a	k9
conservation	conservation	k1gInSc4
assessment	assessment	k1gInSc1
of	of	k?
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Red	Red	k1gFnSc2
List	list	k1gInSc1
of	of	k?
Mammals	Mammals	k1gInSc1
of	of	k?
South	South	k1gInSc1
Africa	Africus	k1gMnSc2
<g/>
,	,	kIx,
Lesotho	Lesot	k1gMnSc2
and	and	k?
Swaziland	Swaziland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gMnSc1
African	African	k1gMnSc1
National	National	k1gMnSc1
Biodiversity	Biodiversit	k1gInPc7
Institute	institut	k1gInSc5
and	and	k?
Endangered	Endangered	k1gInSc1
Wildlife	Wildlif	k1gInSc5
Trust	trust	k1gInSc1
<g/>
,	,	kIx,
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
AL-SAFADI	AL-SAFADI	k1gMnSc2
<g/>
,	,	kIx,
Mousa	Mous	k1gMnSc2
M.	M.	kA
On	on	k3xPp3gMnSc1
the	the	k?
biology	biolog	k1gMnPc4
and	and	k?
ecology	ecologa	k1gFnSc2
of	of	k?
the	the	k?
White-tailed	White-tailed	k1gInSc1
and	and	k?
Bushy-tailed	Bushy-tailed	k1gInSc1
Mongoose	Mongoosa	k1gFnSc3
(	(	kIx(
Ichneumia	Ichneumia	k1gFnSc1
albicauda	albicauda	k1gFnSc1
and	and	k?
Bdeogale	Bdeogala	k1gFnSc3
crassicauda	crassicauda	k1gMnSc1
)	)	kIx)
in	in	k?
Yemen	Yemen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoology	zoolog	k1gMnPc4
in	in	k?
the	the	k?
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-XX	-XX	k?
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
939	#num#	k4
<g/>
-	-	kIx~
<g/>
7140	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9397140.1995	9397140.1995	k4
<g/>
.10637665	.10637665	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mangusta	Mangusta	k1gFnSc1
abuvudan	abuvudan	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Ichneumia	Ichneumius	k1gMnSc2
albicauda	albicaud	k1gMnSc2
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
