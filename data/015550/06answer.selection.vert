<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
největší	veliký	k2eAgFnPc4d3
promykovité	promykovitý	k2eAgFnPc4d1
šelmy	šelma	k1gFnPc4
<g/>
,	,	kIx,
dosahuje	dosahovat	k5eAaImIp3nS
celkové	celkový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
často	často	k6eAd1
výrazně	výrazně	k6eAd1
přes	přes	k7c4
1	#num#	k4
metr	metr	k1gInSc4
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
i	i	k9
více	hodně	k6eAd2
než	než	k8xS
5	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>