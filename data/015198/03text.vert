<s>
Intronizace	intronizace	k1gFnSc1
</s>
<s>
Intronizace	intronizace	k1gFnSc1
neboli	neboli	k8xC
uvedení	uvedení	k1gNnSc1
na	na	k7c4
trůn	trůn	k1gInSc4
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
inthronizatio	inthronizatio	k6eAd1
<g/>
,	,	kIx,
inthronizare	inthronizar	k1gMnSc5
nebo	nebo	k8xC
řeckého	řecký	k2eAgNnSc2d1
enthronídzein	enthronídzein	k2eAgInSc4d1
(	(	kIx(
<g/>
thrónos	thrónos	k1gInSc4
-	-	kIx~
trůn	trůn	k1gInSc4
<g/>
))	))	k?
je	být	k5eAaImIp3nS
slavnostní	slavnostní	k2eAgFnSc1d1
ceremonie	ceremonie	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
do	do	k7c2
úřadu	úřad	k1gInSc2
nový	nový	k2eAgMnSc1d1
monarcha	monarcha	k1gMnSc1
nebo	nebo	k8xC
důležitý	důležitý	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
katolické	katolický	k2eAgFnSc2d1
a	a	k8xC
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbě	volba	k1gFnSc6
nebo	nebo	k8xC
jmenování	jmenování	k1gNnSc6
jsou	být	k5eAaImIp3nP
takto	takto	k6eAd1
do	do	k7c2
funkce	funkce	k1gFnSc2
uvedeni	uveden	k2eAgMnPc1d1
papež	papež	k1gMnSc1
<g/>
,	,	kIx,
biskup	biskup	k1gMnSc1
a	a	k8xC
opat	opat	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
patriarcha	patriarcha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
intronizace	intronizace	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
intronizační	intronizační	k2eAgFnSc2d1
mše	mše	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
intronizace	intronizace	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
