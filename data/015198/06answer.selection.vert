<s>
Intronizace	intronizace	k1gFnSc1
neboli	neboli	k8xC
uvedení	uvedení	k1gNnSc1
na	na	k7c4
trůn	trůn	k1gInSc4
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
inthronizatio	inthronizatio	k6eAd1
<g/>
,	,	kIx,
inthronizare	inthronizar	k1gMnSc5
nebo	nebo	k8xC
řeckého	řecký	k2eAgNnSc2d1
enthronídzein	enthronídzein	k2eAgInSc4d1
(	(	kIx(
<g/>
thrónos	thrónos	k1gInSc4
-	-	kIx~
trůn	trůn	k1gInSc4
<g/>
))	))	k?
je	být	k5eAaImIp3nS
slavnostní	slavnostní	k2eAgFnSc1d1
ceremonie	ceremonie	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
do	do	k7c2
úřadu	úřad	k1gInSc2
nový	nový	k2eAgMnSc1d1
monarcha	monarcha	k1gMnSc1
nebo	nebo	k8xC
důležitý	důležitý	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
katolické	katolický	k2eAgFnSc2d1
a	a	k8xC
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>