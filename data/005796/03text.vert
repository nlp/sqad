<s>
Česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
je	být	k5eAaImIp3nS	být
českojazyčná	českojazyčný	k2eAgFnSc1d1	českojazyčná
verze	verze	k1gFnSc1	verze
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
internetové	internetový	k2eAgFnPc1d1	internetová
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
tvorbě	tvorba	k1gFnSc6	tvorba
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
dobrovolní	dobrovolný	k2eAgMnPc1d1	dobrovolný
přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
včetně	včetně	k7c2	včetně
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
provozuje	provozovat	k5eAaImIp3nS	provozovat
nadace	nadace	k1gFnSc1	nadace
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Foundation	Foundation	k1gInSc1	Foundation
založená	založený	k2eAgFnSc1d1	založená
podle	podle	k7c2	podle
zákonů	zákon	k1gInPc2	zákon
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
se	s	k7c7	s
cca	cca	kA	cca
10	[number]	k4	10
%	%	kIx~	%
podílejí	podílet	k5eAaImIp3nP	podílet
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
ovládající	ovládající	k2eAgFnSc4d1	ovládající
češtinu	čeština	k1gFnSc4	čeština
žijící	žijící	k2eAgFnSc4d1	žijící
mimo	mimo	k7c4	mimo
Česko	Česko	k1gNnSc4	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
26	[number]	k4	26
<g/>
.	.	kIx.	.
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
290	[number]	k4	290
existujících	existující	k2eAgFnPc2d1	existující
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
pátou	pátá	k1gFnSc4	pátá
největší	veliký	k2eAgFnSc7d3	veliký
verzí	verze	k1gFnSc7	verze
psanou	psaný	k2eAgFnSc7d1	psaná
ve	v	k7c6	v
slovanském	slovanský	k2eAgInSc6d1	slovanský
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
po	po	k7c6	po
ruské	ruský	k2eAgFnSc6d1	ruská
<g/>
,	,	kIx,	,
polské	polský	k2eAgFnSc6d1	polská
<g/>
,	,	kIx,	,
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
a	a	k8xC	a
srbochorvatské	srbochorvatský	k2eAgFnSc6d1	srbochorvatský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
denně	denně	k6eAd1	denně
přicházelo	přicházet	k5eAaImAgNnS	přicházet
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
393	[number]	k4	393
573	[number]	k4	573
dotazů	dotaz	k1gInPc2	dotaz
<g/>
.	.	kIx.	.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
používaly	používat	k5eAaImAgInP	používat
software	software	k1gInSc4	software
UseMod	UseModa	k1gFnPc2	UseModa
a	a	k8xC	a
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
MediaWiki	MediaWik	k1gFnSc3	MediaWik
nebyly	být	k5eNaImAgInP	být
původní	původní	k2eAgInPc1d1	původní
tři	tři	k4xCgInPc1	tři
krátké	krátký	k2eAgInPc1d1	krátký
články	článek	k1gInPc1	článek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
HomePage	HomePage	k1gInSc1	HomePage
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Veda	vést	k5eAaImSgMnS	vést
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kultura	kultura	k1gFnSc1	kultura
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
existovaly	existovat	k5eAaImAgFnP	existovat
<g/>
,	,	kIx,	,
převedeny	převeden	k2eAgInPc1d1	převeden
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
editací	editace	k1gFnSc7	editace
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
založení	založení	k1gNnSc4	založení
Hlavní	hlavní	k2eAgFnSc2d1	hlavní
strany	strana	k1gFnSc2	strana
jejím	její	k3xOp3gNnPc3	její
zkopírováním	zkopírování	k1gNnPc3	zkopírování
ze	z	k7c2	z
staršího	starý	k2eAgInSc2d2	starší
systému	systém	k1gInSc2	systém
UseMod	UseMod	k1gInSc1	UseMod
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
této	tento	k3xDgFnSc2	tento
editace	editace	k1gFnSc2	editace
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Brion	Brion	k1gMnSc1	Brion
Vibber	Vibber	k1gMnSc1	Vibber
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
první	první	k4xOgMnSc1	první
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
správcem	správce	k1gMnSc7	správce
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
byl	být	k5eAaImAgMnS	být
esperantista	esperantista	k1gMnSc1	esperantista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Malovec	Malovec	k1gMnSc1	Malovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
na	na	k7c6	na
Konferenci	konference	k1gFnSc6	konference
o	o	k7c4	o
užití	užití	k1gNnSc4	užití
esperanta	esperanto	k1gNnSc2	esperanto
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
technice	technika	k1gFnSc6	technika
v	v	k7c6	v
Dobřichovicích	Dobřichovice	k1gFnPc6	Dobřichovice
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
esperantistou	esperantista	k1gMnSc7	esperantista
Chuckem	Chuck	k1gInSc7	Chuck
Smithem	Smith	k1gInSc7	Smith
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
hostil	hostit	k5eAaImAgInS	hostit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Smithův	Smithův	k2eAgInSc4d1	Smithův
popud	popud	k1gInSc4	popud
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Malovec	Malovec	k1gMnSc1	Malovec
z	z	k7c2	z
esperanta	esperanto	k1gNnSc2	esperanto
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
text	text	k1gInSc4	text
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnSc4d1	hlavní
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Text	text	k1gInSc1	text
jsem	být	k5eAaImIp1nS	být
pak	pak	k6eAd1	pak
poslal	poslat	k5eAaPmAgInS	poslat
jinému	jiný	k2eAgMnSc3d1	jiný
americkému	americký	k2eAgMnSc3d1	americký
esperantistovi	esperantista	k1gMnSc3	esperantista
Brionu	Brion	k1gMnSc3	Brion
Vibberovi	Vibber	k1gMnSc3	Vibber
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nahrál	nahrát	k5eAaBmAgMnS	nahrát
text	text	k1gInSc4	text
do	do	k7c2	do
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
a	a	k8xC	a
já	já	k3xPp1nSc1	já
teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
věci	věc	k1gFnPc4	věc
jsem	být	k5eAaImIp1nS	být
přeložil	přeložit	k5eAaPmAgMnS	přeložit
špatně	špatně	k6eAd1	špatně
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
jsem	být	k5eAaImIp1nS	být
Briona	Brion	k1gMnSc4	Brion
o	o	k7c4	o
opravu	oprava	k1gFnSc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
mi	já	k3xPp1nSc3	já
právo	právo	k1gNnSc4	právo
své	svůj	k3xOyFgFnSc2	svůj
chyby	chyba	k1gFnSc2	chyba
opravit	opravit	k5eAaPmF	opravit
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
správcem	správce	k1gMnSc7	správce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
bych	by	kYmCp1nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
opravovat	opravovat	k5eAaImF	opravovat
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nevědomky	nevědomky	k6eAd1	nevědomky
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
kromě	kromě	k7c2	kromě
správce	správce	k1gMnSc2	správce
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
tvůrcem	tvůrce	k1gMnSc7	tvůrce
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
wikipedista	wikipedista	k1gMnSc1	wikipedista
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
s	s	k7c7	s
esperantským	esperantský	k2eAgNnSc7d1	esperantské
"	"	kIx"	"
<g/>
Vikipedio	Vikipedio	k1gNnSc1	Vikipedio
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
veškerá	veškerý	k3xTgNnPc1	veškerý
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
mají	mít	k5eAaImIp3nP	mít
koncovku	koncovka	k1gFnSc4	koncovka
"	"	kIx"	"
<g/>
-o	-o	k?	-o
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
enciklopedio	enciklopedio	k1gMnSc1	enciklopedio
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
navíc	navíc	k6eAd1	navíc
nazval	nazvat	k5eAaPmAgMnS	nazvat
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
jiných	jiný	k2eAgFnPc2d1	jiná
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
německé	německý	k2eAgInPc1d1	německý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
anglické	anglický	k2eAgFnPc1d1	anglická
"	"	kIx"	"
<g/>
Wikipedia	Wikipedium	k1gNnPc4	Wikipedium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
německy	německy	k6eAd1	německy
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Enzyklopädie	Enzyklopädie	k1gFnSc1	Enzyklopädie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Miroslav	Miroslav	k1gMnSc1	Miroslav
Malovec	Malovec	k1gMnSc1	Malovec
nebyl	být	k5eNaImAgMnS	být
jako	jako	k9	jako
správce	správce	k1gMnSc1	správce
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
psaní	psaní	k1gNnSc1	psaní
do	do	k7c2	do
esperantské	esperantský	k2eAgFnSc2d1	esperantská
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
druhým	druhý	k4xOgNnSc7	druhý
správcem	správce	k1gMnSc7	správce
a	a	k8xC	a
byrokratem	byrokrat	k1gMnSc7	byrokrat
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Vít	Vít	k1gMnSc1	Vít
Zvánovec	Zvánovec	k1gMnSc1	Zvánovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2003	[number]	k4	2003
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
a	a	k8xC	a
zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
diskusí	diskuse	k1gFnPc2	diskuse
a	a	k8xC	a
vytváření	vytváření	k1gNnSc2	vytváření
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze-Karlíně	Praze-Karlína	k1gFnSc6	Praze-Karlína
konalo	konat	k5eAaImAgNnS	konat
patrně	patrně	k6eAd1	patrně
první	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
setkání	setkání	k1gNnSc1	setkání
redaktorů	redaktor	k1gMnPc2	redaktor
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Florenci	Florenc	k1gFnSc6	Florenc
konalo	konat	k5eAaImAgNnS	konat
druhé	druhý	k4xOgFnSc6	druhý
takové	takový	k3xDgFnSc3	takový
české	český	k2eAgNnSc1d1	české
setkání	setkání	k1gNnSc1	setkání
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
převážně	převážně	k6eAd1	převážně
správci	správce	k1gMnPc1	správce
se	s	k7c7	s
zvláštními	zvláštní	k2eAgNnPc7d1	zvláštní
oprávněními	oprávnění	k1gNnPc7	oprávnění
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
setkání	setkání	k1gNnSc1	setkání
U	u	k7c2	u
Frgála	Frgál	k1gMnSc2	Frgál
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
domovské	domovský	k2eAgFnSc2d1	domovská
adresy	adresa	k1gFnSc2	adresa
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
získala	získat	k5eAaPmAgFnS	získat
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
též	též	k9	též
alias	alias	k9	alias
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
přesměrována	přesměrován	k2eAgFnSc1d1	přesměrována
na	na	k7c6	na
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
wikipedista	wikipedista	k1gMnSc1	wikipedista
Wikimol	Wikimol	k1gInSc4	Wikimol
získal	získat	k5eAaPmAgMnS	získat
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
doménu	doména	k1gFnSc4	doména
wikipedie	wikipedie	k1gFnSc2	wikipedie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
;	;	kIx,	;
přesměrování	přesměrování	k1gNnSc1	přesměrování
na	na	k7c4	na
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
fungovalo	fungovat	k5eAaImAgNnS	fungovat
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2004	[number]	k4	2004
se	se	k3xPyFc4	se
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
objevilo	objevit	k5eAaPmAgNnS	objevit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložené	přeložený	k2eAgNnSc4d1	přeložené
logo	logo	k1gNnSc4	logo
s	s	k7c7	s
názvem	název	k1gInSc7	název
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
a	a	k8xC	a
heslem	heslo	k1gNnSc7	heslo
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
kompletně	kompletně	k6eAd1	kompletně
přeložena	přeložen	k2eAgNnPc4d1	přeloženo
systémová	systémový	k2eAgNnPc4d1	systémové
hlášení	hlášení	k1gNnPc4	hlášení
softwaru	software	k1gInSc2	software
MediaWiki	MediaWik	k1gFnSc2	MediaWik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
měla	mít	k5eAaImAgFnS	mít
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
přes	přes	k7c4	přes
384	[number]	k4	384
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
1000	[number]	k4	1000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
početně	početně	k6eAd1	početně
překonala	překonat	k5eAaPmAgFnS	překonat
významnou	významný	k2eAgFnSc4d1	významná
hranici	hranice	k1gFnSc4	hranice
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
teprve	teprve	k6eAd1	teprve
přes	přes	k7c4	přes
hranici	hranice	k1gFnSc4	hranice
3000	[number]	k4	3000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
10	[number]	k4	10
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
20	[number]	k4	20
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
40	[number]	k4	40
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
50	[number]	k4	50
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
tempo	tempo	k1gNnSc1	tempo
růstu	růst	k1gInSc2	růst
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
sice	sice	k8xC	sice
výrazně	výrazně	k6eAd1	výrazně
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dosažené	dosažený	k2eAgInPc1d1	dosažený
absolutní	absolutní	k2eAgInPc1d1	absolutní
počty	počet	k1gInPc1	počet
článků	článek	k1gInPc2	článek
překonávaly	překonávat	k5eAaImAgInP	překonávat
nové	nový	k2eAgInPc4d1	nový
mezníky	mezník	k1gInPc4	mezník
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
přehoupla	přehoupnout	k5eAaPmAgFnS	přehoupnout
přes	přes	k7c4	přes
hranici	hranice	k1gFnSc4	hranice
100	[number]	k4	100
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
kolem	kolem	k7c2	kolem
poledne	poledne	k1gNnSc2	poledne
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
150	[number]	k4	150
000	[number]	k4	000
článků	článek	k1gInPc2	článek
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
překonána	překonat	k5eAaPmNgFnS	překonat
meta	meta	k1gFnSc1	meta
200	[number]	k4	200
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
již	již	k6eAd1	již
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
počtu	počet	k1gInSc2	počet
300	[number]	k4	300
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
přírůstek	přírůstek	k1gInSc1	přírůstek
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
stabilně	stabilně	k6eAd1	stabilně
kolem	kolem	k7c2	kolem
8	[number]	k4	8
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
představovalo	představovat	k5eAaImAgNnS	představovat
kolem	kolem	k7c2	kolem
80	[number]	k4	80
nových	nový	k2eAgInPc2d1	nový
článků	článek	k1gInPc2	článek
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
největších	veliký	k2eAgFnPc6d3	veliký
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
byl	být	k5eAaImAgInS	být
měsíční	měsíční	k2eAgInSc1d1	měsíční
přírůstek	přírůstek	k1gInSc1	přírůstek
tehdy	tehdy	k6eAd1	tehdy
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
na	na	k7c6	na
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
kolem	kolem	k7c2	kolem
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
objemu	objem	k1gInSc2	objem
databáze	databáze	k1gFnSc2	databáze
nebo	nebo	k8xC	nebo
počtu	počet	k1gInSc3	počet
slov	slovo	k1gNnPc2	slovo
byl	být	k5eAaImAgMnS	být
však	však	k9	však
poměrný	poměrný	k2eAgInSc4d1	poměrný
růst	růst	k1gInSc4	růst
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
anglickou	anglický	k2eAgFnSc7d1	anglická
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
i	i	k8xC	i
japonskou	japonský	k2eAgFnSc7d1	japonská
verzí	verze	k1gFnSc7	verze
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
7	[number]	k4	7
%	%	kIx~	%
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
přesměrování	přesměrování	k1gNnSc1	přesměrování
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc6	počet
článků	článek	k1gInPc2	článek
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
290	[number]	k4	290
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,7	[number]	k4	1,7
<g/>
×	×	k?	×
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
<g/>
×	×	k?	×
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
anglické	anglický	k2eAgFnSc2d1	anglická
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
nejrozsáhlejšími	rozsáhlý	k2eAgFnPc7d3	nejrozsáhlejší
verzemi	verze	k1gFnPc7	verze
jsou	být	k5eAaImIp3nP	být
švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
spíše	spíše	k9	spíše
symbolické	symbolický	k2eAgNnSc4d1	symbolické
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
48	[number]	k4	48
verzí	verze	k1gFnPc2	verze
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
České	český	k2eAgInPc1d1	český
články	článek	k1gInPc1	článek
tvoří	tvořit	k5eAaImIp3nP	tvořit
0,8	[number]	k4	0,8
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
tvoří	tvořit	k5eAaImIp3nS	tvořit
12	[number]	k4	12
%	%	kIx~	%
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
je	být	k5eAaImIp3nS	být
pátou	pátá	k1gFnSc7	pátá
největší	veliký	k2eAgFnSc7d3	veliký
mezi	mezi	k7c7	mezi
verzemi	verze	k1gFnPc7	verze
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
k	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
přes	přes	k7c4	přes
5	[number]	k4	5
638	[number]	k4	638
000	[number]	k4	000
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
12,4	[number]	k4	12,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
článků	článek	k1gInPc2	článek
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
světových	světový	k2eAgFnPc6d1	světová
verzích	verze	k1gFnPc6	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
největšími	veliký	k2eAgFnPc7d3	veliký
verzemi	verze	k1gFnPc7	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
ruská	ruský	k2eAgFnSc1d1	ruská
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
<g/>
,	,	kIx,	,
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
a	a	k8xC	a
srbochorvatská	srbochorvatský	k2eAgFnSc1d1	srbochorvatský
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
6,8	[number]	k4	6,8
%	%	kIx~	%
článků	článek	k1gInPc2	článek
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
Wikipediích	Wikipedie	k1gFnPc6	Wikipedie
psaných	psaný	k2eAgMnPc2d1	psaný
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
následují	následovat	k5eAaImIp3nP	následovat
srbská	srbský	k2eAgNnPc1d1	srbské
<g/>
,	,	kIx,	,
bulharská	bulharský	k2eAgNnPc1d1	bulharské
a	a	k8xC	a
slovenská	slovenský	k2eAgNnPc1d1	slovenské
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
1000	[number]	k4	1000
uživatelů	uživatel	k1gMnPc2	uživatel
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
připadá	připadat	k5eAaPmIp3nS	připadat
36	[number]	k4	36
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
verzemi	verze	k1gFnPc7	verze
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
na	na	k7c4	na
43	[number]	k4	43
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
792,7	[number]	k4	792,7
milionu	milion	k4xCgInSc2	milion
dotazů	dotaz	k1gInPc2	dotaz
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgInSc1d1	denní
průměr	průměr	k1gInSc1	průměr
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
165	[number]	k4	165
914	[number]	k4	914
a	a	k8xC	a
měsíční	měsíční	k2eAgInSc4d1	měsíční
66	[number]	k4	66
060	[number]	k4	060
367	[number]	k4	367
dotazů	dotaz	k1gInPc2	dotaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
dotazů	dotaz	k1gInPc2	dotaz
bylo	být	k5eAaImAgNnS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
(	(	kIx(	(
<g/>
78	[number]	k4	78
470	[number]	k4	470
291	[number]	k4	291
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
(	(	kIx(	(
<g/>
49	[number]	k4	49
196	[number]	k4	196
794	[number]	k4	794
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
týdně	týdně	k6eAd1	týdně
bylo	být	k5eAaImAgNnS	být
zpravidla	zpravidla	k6eAd1	zpravidla
nejvíce	hodně	k6eAd3	hodně
dotazů	dotaz	k1gInPc2	dotaz
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
(	(	kIx(	(
<g/>
2	[number]	k4	2
329	[number]	k4	329
359	[number]	k4	359
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
(	(	kIx(	(
<g/>
1	[number]	k4	1
875	[number]	k4	875
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
dotazů	dotaz	k1gInPc2	dotaz
za	za	k7c4	za
den	den	k1gInSc4	den
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
(	(	kIx(	(
<g/>
2	[number]	k4	2
938	[number]	k4	938
485	[number]	k4	485
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
1	[number]	k4	1
350	[number]	k4	350
106	[number]	k4	106
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
měla	mít	k5eAaImAgFnS	mít
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
26	[number]	k4	26
správců	správce	k1gMnPc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Správci	správce	k1gMnPc1	správce
jsou	být	k5eAaImIp3nP	být
dobrovolní	dobrovolný	k2eAgMnPc1d1	dobrovolný
editoři	editor	k1gMnPc1	editor
s	s	k7c7	s
nadstandardními	nadstandardní	k2eAgNnPc7d1	nadstandardní
redakčními	redakční	k2eAgNnPc7d1	redakční
oprávněními	oprávnění	k1gNnPc7	oprávnění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
samotné	samotný	k2eAgFnSc6d1	samotná
tvorbě	tvorba	k1gFnSc6	tvorba
obsahu	obsah	k1gInSc2	obsah
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
svými	svůj	k3xOyFgMnPc7	svůj
právy	právo	k1gNnPc7	právo
rovni	roven	k2eAgMnPc1d1	roven
všem	všecek	k3xTgMnPc3	všecek
ostatním	ostatní	k2eAgMnPc3d1	ostatní
uživatelům	uživatel	k1gMnPc3	uživatel
a	a	k8xC	a
editorům	editor	k1gMnPc3	editor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uvedenému	uvedený	k2eAgNnSc3d1	uvedené
datu	datum	k1gNnSc3	datum
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
přes	přes	k7c4	přes
375	[number]	k4	375
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
wikipedisté	wikipedista	k1gMnPc1	wikipedista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
editacím	editace	k1gFnPc3	editace
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
věnuje	věnovat	k5eAaPmIp3nS	věnovat
pouze	pouze	k6eAd1	pouze
necelá	celý	k2eNgFnSc1d1	necelá
stovka	stovka	k1gFnSc1	stovka
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
velmi	velmi	k6eAd1	velmi
aktivních	aktivní	k2eAgInPc2d1	aktivní
editorů	editor	k1gInPc2	editor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
měsíce	měsíc	k1gInPc4	měsíc
provedou	provést	k5eAaPmIp3nP	provést
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
editací	editace	k1gFnPc2	editace
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stabilní	stabilní	k2eAgFnSc4d1	stabilní
již	již	k6eAd1	již
od	od	k7c2	od
ukončení	ukončení	k1gNnSc2	ukončení
období	období	k1gNnSc2	období
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
narostl	narůst	k5eAaPmAgMnS	narůst
prakticky	prakticky	k6eAd1	prakticky
z	z	k7c2	z
nuly	nula	k1gFnSc2	nula
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
graf	graf	k1gInSc1	graf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
aktivních	aktivní	k2eAgMnPc2d1	aktivní
editorů	editor	k1gMnPc2	editor
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
měsíce	měsíc	k1gInPc4	měsíc
provedou	provést	k5eAaPmIp3nP	provést
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
editací	editace	k1gFnPc2	editace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
kolem	kolem	k7c2	kolem
2	[number]	k4	2
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
12	[number]	k4	12
%	%	kIx~	%
editací	editace	k1gFnPc2	editace
tehdy	tehdy	k6eAd1	tehdy
prováděli	provádět	k5eAaImAgMnP	provádět
neregistrovaní	registrovaný	k2eNgMnPc1d1	neregistrovaný
přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
a	a	k8xC	a
přes	přes	k7c4	přes
14	[number]	k4	14
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
provedlo	provést	k5eAaPmAgNnS	provést
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
příchodu	příchod	k1gInSc2	příchod
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
celkem	celkem	k6eAd1	celkem
alespoň	alespoň	k9	alespoň
deset	deset	k4xCc4	deset
editací	editace	k1gFnPc2	editace
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
provedli	provést	k5eAaPmAgMnP	provést
91,6	[number]	k4	91,6
%	%	kIx~	%
editací	editace	k1gFnSc7	editace
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
uživatelé	uživatel	k1gMnPc1	uživatel
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2,4	[number]	k4	2,4
%	%	kIx~	%
uživatelé	uživatel	k1gMnPc1	uživatel
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
0,8	[number]	k4	0,8
%	%	kIx~	%
uživatelé	uživatel	k1gMnPc1	uživatel
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
editováno	editovat	k5eAaImNgNnS	editovat
6,9	[number]	k4	6,9
%	%	kIx~	%
článků	článek	k1gInPc2	článek
esperantské	esperantský	k2eAgFnSc2d1	esperantská
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
nezávislý	závislý	k2eNgInSc1d1	nezávislý
výzkum	výzkum	k1gInSc1	výzkum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
kvalitou	kvalita	k1gFnSc7	kvalita
hesel	heslo	k1gNnPc2	heslo
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
srovnával	srovnávat	k5eAaImAgInS	srovnávat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
dostupnými	dostupný	k2eAgFnPc7d1	dostupná
česky	česky	k6eAd1	česky
psanými	psaný	k2eAgFnPc7d1	psaná
encyklopediemi	encyklopedie	k1gFnPc7	encyklopedie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
případě	případ	k1gInSc6	případ
studie	studie	k1gFnSc2	studie
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
srovnávány	srovnávat	k5eAaImNgInP	srovnávat
anglická	anglický	k2eAgFnSc1d1	anglická
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
a	a	k8xC	a
Britannica	Britannica	k1gFnSc1	Britannica
<g/>
.	.	kIx.	.
</s>
<s>
Trvalým	trvalý	k2eAgNnSc7d1	trvalé
ohniskem	ohnisko	k1gNnSc7	ohnisko
konfliktů	konflikt	k1gInPc2	konflikt
komunity	komunita	k1gFnSc2	komunita
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
o	o	k7c4	o
pravopis	pravopis	k1gInSc4	pravopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
vracel	vracet	k5eAaImAgInS	vracet
až	až	k6eAd1	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
významní	významný	k2eAgMnPc1d1	významný
přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
Zvánovec	Zvánovec	k1gMnSc1	Zvánovec
a	a	k8xC	a
Tompecina	Tompecina	k1gMnSc1	Tompecina
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pecina	Pecina	k1gMnSc1	Pecina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
správce	správce	k1gMnSc1	správce
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
,	,	kIx,	,
užívali	užívat	k5eAaImAgMnP	užívat
a	a	k8xC	a
obhajovali	obhajovat	k5eAaImAgMnP	obhajovat
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
archaizující	archaizující	k2eAgInSc4d1	archaizující
pravopis	pravopis	k1gInSc4	pravopis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
nevolí	nevole	k1gFnSc7	nevole
u	u	k7c2	u
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
ostatních	ostatní	k2eAgInPc2d1	ostatní
editorů	editor	k1gInPc2	editor
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktivní	konstruktivní	k2eAgFnSc1d1	konstruktivní
spolupráce	spolupráce	k1gFnSc1	spolupráce
i	i	k8xC	i
spory	spor	k1gInPc1	spor
a	a	k8xC	a
hádky	hádka	k1gFnPc1	hádka
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
,	,	kIx,	,
řeší	řešit	k5eAaImIp3nS	řešit
se	se	k3xPyFc4	se
diskusemi	diskuse	k1gFnPc7	diskuse
<g/>
,	,	kIx,	,
hlasováním	hlasování	k1gNnSc7	hlasování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
takzvanými	takzvaný	k2eAgFnPc7d1	takzvaná
revertovacími	revertovací	k2eAgFnPc7d1	revertovací
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tatáž	týž	k3xTgFnSc1	týž
úprava	úprava	k1gFnSc1	úprava
je	být	k5eAaImIp3nS	být
vícekrát	vícekrát	k6eAd1	vícekrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
vrácena	vrácen	k2eAgFnSc1d1	vrácena
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
opět	opět	k6eAd1	opět
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
například	například	k6eAd1	například
specifický	specifický	k2eAgInSc1d1	specifický
český	český	k2eAgInSc1d1	český
spor	spor	k1gInSc1	spor
o	o	k7c4	o
název	název	k1gInSc4	název
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
versus	versus	k7c1	versus
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
spor	spor	k1gInSc4	spor
o	o	k7c6	o
přechylování	přechylování	k1gNnSc6	přechylování
cizích	cizí	k2eAgNnPc2d1	cizí
příjmení	příjmení	k1gNnPc2	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kontroverzním	kontroverzní	k2eAgNnPc3d1	kontroverzní
tématům	téma	k1gNnPc3	téma
patří	patřit	k5eAaImIp3nS	patřit
také	také	k6eAd1	také
např.	např.	kA	např.
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
,	,	kIx,	,
psychotronika	psychotronika	k1gFnSc1	psychotronika
<g/>
,	,	kIx,	,
telepatie	telepatie	k1gFnSc1	telepatie
nebo	nebo	k8xC	nebo
komunismus	komunismus	k1gInSc1	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Kritizována	kritizován	k2eAgFnSc1d1	kritizována
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
i	i	k9	i
motivace	motivace	k1gFnSc1	motivace
a	a	k8xC	a
odborná	odborný	k2eAgFnSc1d1	odborná
způsobilost	způsobilost	k1gFnSc1	způsobilost
jejích	její	k3xOp3gMnPc2	její
správců	správce	k1gMnPc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
např.	např.	kA	např.
některá	některý	k3yIgNnPc1	některý
pravidla	pravidlo	k1gNnPc1	pravidlo
působí	působit	k5eAaImIp3nP	působit
mnohoznačně	mnohoznačně	k6eAd1	mnohoznačně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
působí	působit	k5eAaImIp3nP	působit
potíže	potíž	k1gFnPc4	potíž
jak	jak	k8xC	jak
běžným	běžný	k2eAgMnPc3d1	běžný
přispěvatelům	přispěvatel	k1gMnPc3	přispěvatel
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ale	ale	k9	ale
i	i	k9	i
současným	současný	k2eAgMnPc3d1	současný
správcům	správce	k1gMnPc3	správce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gFnPc4	on
mohou	moct	k5eAaImIp3nP	moct
–	–	k?	–
dle	dle	k7c2	dle
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
–	–	k?	–
interpretovat	interpretovat	k5eAaBmF	interpretovat
rozličně	rozličně	k6eAd1	rozličně
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
zmatky	zmatek	k1gInPc4	zmatek
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
nových	nový	k2eAgFnPc2d1	nová
editací	editace	k1gFnPc2	editace
již	již	k6eAd1	již
stávajících	stávající	k2eAgNnPc2d1	stávající
hesel	heslo	k1gNnPc2	heslo
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
praktiky	praktika	k1gFnPc4	praktika
s	s	k7c7	s
novými	nový	k2eAgNnPc7d1	nové
hesly	heslo	k1gNnPc7	heslo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
údajné	údajný	k2eAgFnSc2d1	údajná
neověřitelnosti	neověřitelnost	k1gFnSc2	neověřitelnost
či	či	k8xC	či
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
významnosti	významnost	k1gFnPc1	významnost
k	k	k7c3	k
redukcím	redukce	k1gFnPc3	redukce
až	až	k9	až
mazání	mazání	k1gNnSc2	mazání
některých	některý	k3yIgNnPc2	některý
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hesla	heslo	k1gNnPc1	heslo
jiná	jiný	k2eAgNnPc1d1	jiné
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
obsahově	obsahově	k6eAd1	obsahově
stejně	stejně	k6eAd1	stejně
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc1d2	starší
likvidována	likvidován	k2eAgFnSc1d1	likvidována
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Palčivým	palčivý	k2eAgInSc7d1	palčivý
problémem	problém	k1gInSc7	problém
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
také	také	k9	také
soustavné	soustavný	k2eAgNnSc1d1	soustavné
porušování	porušování	k1gNnSc1	porušování
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
nemalým	malý	k2eNgInSc7d1	nemalý
počtem	počet	k1gInSc7	počet
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
neúměrně	úměrně	k6eNd1	úměrně
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
správce	správce	k1gMnPc4	správce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
texty	text	k1gInPc1	text
porušující	porušující	k2eAgNnPc1d1	porušující
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
snižuje	snižovat	k5eAaImIp3nS	snižovat
věrohodnost	věrohodnost	k1gFnSc4	věrohodnost
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
naprostého	naprostý	k2eAgNnSc2d1	naprosté
ignorování	ignorování	k1gNnSc2	ignorování
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgMnSc7	jeden
uživatelem	uživatel	k1gMnSc7	uživatel
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
ostatních	ostatní	k2eAgMnPc2d1	ostatní
odhalených	odhalený	k2eAgMnPc2d1	odhalený
autorů	autor	k1gMnPc2	autor
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
jimi	on	k3xPp3gMnPc7	on
založených	založený	k2eAgInPc2d1	založený
článků	článek	k1gInPc2	článek
porušujících	porušující	k2eAgInPc2d1	porušující
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
maximálně	maximálně	k6eAd1	maximálně
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
českou	český	k2eAgFnSc4d1	Česká
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
Patrick	Patrick	k1gMnSc1	Patrick
Zandl	Zandl	k1gMnSc1	Zandl
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Lupa	lupa	k1gFnSc1	lupa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
článek	článek	k1gInSc1	článek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
sporů	spor	k1gInPc2	spor
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Reflex	reflex	k1gInSc1	reflex
psal	psát	k5eAaImAgInS	psát
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Lehkou	lehký	k2eAgFnSc4d1	lehká
kritiku	kritika	k1gFnSc4	kritika
několika	několik	k4yIc2	několik
politických	politický	k2eAgNnPc2d1	politické
hesel	heslo	k1gNnPc2	heslo
přinesl	přinést	k5eAaPmAgMnS	přinést
server	server	k1gInSc4	server
idnes	idnes	k1gMnSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Kritiku	kritika	k1gFnSc4	kritika
hesla	heslo	k1gNnSc2	heslo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
toho	ten	k3xDgMnSc4	ten
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
Brněnský	brněnský	k2eAgInSc1d1	brněnský
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
:	:	kIx,	:
<g/>
Ohlasy	ohlas	k1gInPc1	ohlas
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
mívají	mívat	k5eAaImIp3nP	mívat
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
umístění	umístění	k1gNnSc4	umístění
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
vyhledávači	vyhledávač	k1gMnSc6	vyhledávač
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
tato	tento	k3xDgFnSc1	tento
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
obecně	obecně	k6eAd1	obecně
známou	známý	k2eAgFnSc7d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
svolavatel	svolavatel	k1gMnSc1	svolavatel
prvního	první	k4xOgNnSc2	první
reálného	reálný	k2eAgNnSc2d1	reálné
setkání	setkání	k1gNnSc2	setkání
wikipedistů	wikipedista	k1gMnPc2	wikipedista
Paddy	Padda	k1gMnSc2	Padda
rozhovor	rozhovor	k1gInSc4	rozhovor
německému	německý	k2eAgNnSc3d1	německé
vysílání	vysílání	k1gNnSc3	vysílání
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
existenci	existence	k1gFnSc6	existence
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
a	a	k8xC	a
o	o	k7c6	o
sporech	spor	k1gInPc6	spor
uživatelů	uživatel	k1gMnPc2	uživatel
se	se	k3xPyFc4	se
zmínil	zmínit	k5eAaPmAgInS	zmínit
např.	např.	kA	např.
i	i	k8xC	i
Miloš	Miloš	k1gMnSc1	Miloš
Čermák	Čermák	k1gMnSc1	Čermák
v	v	k7c6	v
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
článku	článek	k1gInSc6	článek
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Reflex	reflex	k1gInSc1	reflex
č.	č.	k?	č.
16	[number]	k4	16
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
začal	začít	k5eAaPmAgInS	začít
portál	portál	k1gInSc1	portál
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
přebírat	přebírat	k5eAaImF	přebírat
obsah	obsah	k1gInSc4	obsah
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2007	[number]	k4	2007
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
webu	web	k1gInSc2	web
Root	Roota	k1gFnPc2	Roota
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Czech	Czech	k1gInSc1	Czech
Open	Opena	k1gFnPc2	Opena
Source	Sourec	k1gInSc2	Sourec
2007	[number]	k4	2007
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Projekt	projekt	k1gInSc1	projekt
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
absolutním	absolutní	k2eAgInSc7d1	absolutní
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
ročníku	ročník	k1gInSc6	ročník
své	svůj	k3xOyFgNnSc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
veřejnosti	veřejnost	k1gFnSc2	veřejnost
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Projekt	projekt	k1gInSc4	projekt
obhájila	obhájit	k5eAaPmAgFnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roční	roční	k2eAgFnSc6d1	roční
pauze	pauza	k1gFnSc6	pauza
opět	opět	k6eAd1	opět
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
30	[number]	k4	30
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
týdeníku	týdeník	k1gInSc2	týdeník
Dotyk	dotyk	k1gInSc1	dotyk
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
propagátorem	propagátor	k1gMnSc7	propagátor
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
i	i	k9	i
prof.	prof.	kA	prof.
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
wiki	wiki	k1gNnSc6	wiki
také	také	k9	také
aktivně	aktivně	k6eAd1	aktivně
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
edice	edice	k1gFnSc1	edice
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
články	článek	k1gInPc1	článek
Dobré	dobrý	k2eAgInPc1d1	dobrý
články	článek	k1gInPc1	článek
Historie	historie	k1gFnSc2	historie
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
Kronika	kronika	k1gFnSc1	kronika
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
Ohlasy	ohlas	k1gInPc1	ohlas
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
Statistiky	statistika	k1gFnPc1	statistika
české	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
Pramenem	pramen	k1gInSc7	pramen
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
archive	archiv	k1gInSc5	archiv
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
stránka	stránka	k1gFnSc1	stránka
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
Poslední	poslední	k2eAgFnPc1d1	poslední
změny	změna	k1gFnPc1	změna
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
Poslední	poslední	k2eAgFnPc1d1	poslední
změny	změna	k1gFnPc1	změna
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
stránka	stránka	k1gFnSc1	stránka
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
Poslední	poslední	k2eAgFnPc1d1	poslední
změny	změna	k1gFnPc1	změna
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
Poslední	poslední	k2eAgFnPc1d1	poslední
změny	změna	k1gFnPc1	změna
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
stránka	stránka	k1gFnSc1	stránka
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
</s>
