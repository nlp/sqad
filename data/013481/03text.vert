<s>
Shooting	Shooting	k1gInSc1
Stars	Stars	k1gInSc1
SC	SC	kA
</s>
<s>
Shooting	Shooting	k1gInSc1
Stars	Starsa	k1gFnPc2
SC	SC	kA
Země	zem	k1gFnSc2
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Ibadan	Ibadan	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1950	#num#	k4
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Nigerian	Nigerian	k1gMnSc1
Professional	Professional	k1gMnSc1
Football	Football	k1gMnSc1
League	Leagu	k1gFnSc2
Stadion	stadion	k1gInSc1
</s>
<s>
Lekan	Lekan	k1gMnSc1
Salami	Sala	k1gFnPc7
Stadium	stadium	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc5
</s>
<s>
7	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
9	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Shooting	Shooting	k2gNnPc1
Stars	Stars	k1gNnPc1
SC	SC	kA
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
3	#num#	k4
<g/>
SC	SC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nigerijský	nigerijský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
sídlící	sídlící	k2eAgInSc1d1
v	v	k7c6
Ibadanu	Ibadan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
roku	rok	k1gInSc2
1960	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
WNDC	WNDC	kA
(	(	kIx(
<g/>
West	West	k1gMnSc1
Nigeria	Nigerium	k1gNnSc2
Development	Development	k1gMnSc1
Company	Compana	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
byl	být	k5eAaImAgInS
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
Nigeria	Nigerium	k1gNnSc2
Premier	Premier	k1gInSc1
League	League	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
vyhrál	vyhrát	k5eAaPmAgInS
pětkrát	pětkrát	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1976	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
a	a	k8xC
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
získal	získat	k5eAaPmAgInS
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
CAF	CAF	kA
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgMnSc7
nigerijským	nigerijský	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
některého	některý	k3yIgInSc2
kontinentálního	kontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
přidali	přidat	k5eAaPmAgMnP
hráči	hráč	k1gMnPc1
3SC	3SC	k4
prvenství	prvenství	k1gNnSc2
v	v	k7c6
Poháru	pohár	k1gInSc6
CAF	CAF	kA
<g/>
,	,	kIx,
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
CAF	CAF	kA
byli	být	k5eAaImAgMnP
dvakrát	dvakrát	k6eAd1
ve	v	k7c6
finále	finále	k1gNnSc6
(	(	kIx(
<g/>
1984	#num#	k4
a	a	k8xC
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějším	známý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
byl	být	k5eAaImAgInS
Rashidi	Rashid	k1gMnPc1
Yekini	Yekin	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
http://www.shootingstarssc.com	http://www.shootingstarssc.com	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
