<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
monumentálním	monumentální	k2eAgInSc7d1	monumentální
cyklem	cyklus	k1gInSc7	cyklus
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
dílech	dílo	k1gNnPc6	dílo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1913	[number]	k4	1913
až	až	k9	až
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
