<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
ODIS	ODIS	kA	ODIS
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Ostravský	ostravský	k2eAgInSc1d1	ostravský
dopravní	dopravní	k2eAgInSc1d1	dopravní
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
na	na	k7c6	na
území	území	k1gNnSc6	území
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
však	však	k9	však
již	již	k6eAd1	již
před	před	k7c7	před
zřízením	zřízení	k1gNnSc7	zřízení
tohoto	tento	k3xDgInSc2	tento
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
