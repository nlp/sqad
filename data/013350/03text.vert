<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
ODIS	ODIS	kA	ODIS
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Ostravský	ostravský	k2eAgInSc1d1	ostravský
dopravní	dopravní	k2eAgInSc1d1	dopravní
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
na	na	k7c6	na
území	území	k1gNnSc6	území
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
však	však	k9	však
již	již	k6eAd1	již
před	před	k7c7	před
zřízením	zřízení	k1gNnSc7	zřízení
tohoto	tento	k3xDgInSc2	tento
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
ODIS	ODIS	kA	ODIS
==	==	k?	==
</s>
</p>
<p>
<s>
ODIS	ODIS	kA	ODIS
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
k	k	k7c3	k
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
několika	několik	k4yIc2	několik
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
měst	město	k1gNnPc2	město
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
na	na	k7c6	na
Hlučínsku	Hlučínsko	k1gNnSc6	Hlučínsko
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
24	[number]	k4	24
obcí	obec	k1gFnPc2	obec
včetně	včetně	k7c2	včetně
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ODIS	ODIS	kA	ODIS
integrovány	integrovat	k5eAaBmNgFnP	integrovat
všechny	všechen	k3xTgFnPc1	všechen
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
<g/>
,	,	kIx,	,
autobusové	autobusový	k2eAgFnPc1d1	autobusová
a	a	k8xC	a
trolejbusové	trolejbusový	k2eAgFnPc1d1	trolejbusová
linky	linka	k1gFnPc1	linka
provozované	provozovaný	k2eAgNnSc1d1	provozované
Dopravním	dopravní	k2eAgInSc7d1	dopravní
podnikem	podnik	k1gInSc7	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
autobusové	autobusový	k2eAgInPc4d1	autobusový
spoje	spoj	k1gInPc4	spoj
dopravců	dopravce	k1gMnPc2	dopravce
ČSAD	ČSAD	kA	ČSAD
BUS	bus	k1gInSc1	bus
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgFnSc1d1	nynější
Arriva	Arriva	k1gFnSc1	Arriva
Morava	Morava	k1gFnSc1	Morava
<g/>
)	)	kIx)	)
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
některé	některý	k3yIgInPc4	některý
vlaky	vlak	k1gInPc4	vlak
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
systém	systém	k1gInSc1	systém
rozrůstal	rozrůstat	k5eAaImAgInS	rozrůstat
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
přibývali	přibývat	k5eAaImAgMnP	přibývat
i	i	k9	i
noví	nový	k2eAgMnPc1d1	nový
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
problémem	problém	k1gInSc7	problém
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
ODIS	ODIS	kA	ODIS
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Karvinsko	Karvinsko	k1gNnSc4	Karvinsko
a	a	k8xC	a
Havířovsko	Havířovsko	k1gNnSc4	Havířovsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ODIS	ODIS	kA	ODIS
narážel	narážet	k5eAaPmAgInS	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
místních	místní	k2eAgNnPc2d1	místní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
i	i	k8xC	i
zdejších	zdejší	k2eAgMnPc2d1	zdejší
monopolních	monopolní	k2eAgMnPc2d1	monopolní
autobusových	autobusový	k2eAgMnPc2d1	autobusový
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
,	,	kIx,	,
patřících	patřící	k2eAgFnPc2d1	patřící
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
CIDEM	CIDEM	kA	CIDEM
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dopravce	dopravce	k1gMnPc4	dopravce
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
spoje	spoj	k1gInPc4	spoj
těchto	tento	k3xDgMnPc2	tento
dopravců	dopravce	k1gMnPc2	dopravce
v	v	k7c6	v
ODIS	ODIS	kA	ODIS
integrovány	integrovat	k5eAaBmNgFnP	integrovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
ODIS	ODIS	kA	ODIS
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
již	již	k6eAd1	již
celkem	celkem	k6eAd1	celkem
105	[number]	k4	105
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
1779	[number]	k4	1779
km2	km2	k4	km2
a	a	k8xC	a
s	s	k7c7	s
735	[number]	k4	735
341	[number]	k4	341
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
části	část	k1gFnSc6	část
příměstské	příměstský	k2eAgFnSc2d1	příměstská
a	a	k8xC	a
regionální	regionální	k2eAgFnSc2d1	regionální
dopravy	doprava	k1gFnSc2	doprava
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ODIS	ODIS	kA	ODIS
kompletně	kompletně	k6eAd1	kompletně
integrována	integrovat	k5eAaBmNgFnS	integrovat
také	také	k9	také
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
(	(	kIx(	(
<g/>
autobusy	autobus	k1gInPc1	autobus
a	a	k8xC	a
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozšíření	rozšíření	k1gNnSc3	rozšíření
systému	systém	k1gInSc2	systém
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
ODIS	ODIS	kA	ODIS
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
Opavska	Opavsko	k1gNnSc2	Opavsko
a	a	k8xC	a
Vítkovska	Vítkovsko	k1gNnSc2	Vítkovsko
až	až	k9	až
po	po	k7c4	po
Odry	Odra	k1gFnPc4	Odra
a	a	k8xC	a
Bruntál	Bruntál	k1gInSc4	Bruntál
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
o	o	k7c4	o
dalších	další	k2eAgFnPc2d1	další
39	[number]	k4	39
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
ODIS	ODIS	kA	ODIS
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c6	o
31	[number]	k4	31
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Novojičínsko-západ	Novojičínskoápad	k1gInSc4	Novojičínsko-západ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
2008	[number]	k4	2008
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
rozšíření	rozšíření	k1gNnSc1	rozšíření
o	o	k7c4	o
10	[number]	k4	10
obcí	obec	k1gFnPc2	obec
na	na	k7c4	na
Osoblažsku	Osoblažska	k1gFnSc4	Osoblažska
a	a	k8xC	a
město	město	k1gNnSc4	město
Orlová	Orlová	k1gFnSc1	Orlová
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
k	k	k7c3	k
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
do	do	k7c2	do
ODIS	ODIS	kA	ODIS
integrováno	integrovat	k5eAaBmNgNnS	integrovat
již	již	k6eAd1	již
186	[number]	k4	186
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
dalších	další	k2eAgFnPc2d1	další
pěti	pět	k4xCc2	pět
linek	linka	k1gFnPc2	linka
dopravce	dopravce	k1gMnSc1	dopravce
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgFnPc1d2	významnější
změny	změna	k1gFnPc1	změna
nastaly	nastat	k5eAaPmAgFnP	nastat
s	s	k7c7	s
celostátní	celostátní	k2eAgFnSc7d1	celostátní
změnou	změna	k1gFnSc7	změna
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
příměstských	příměstský	k2eAgFnPc2d1	příměstská
linek	linka	k1gFnPc2	linka
provozovaných	provozovaný	k2eAgFnPc2d1	provozovaná
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
a	a	k8xC	a
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
dosud	dosud	k6eAd1	dosud
neintegrovaných	integrovaný	k2eNgFnPc2d1	neintegrovaná
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
tratě	trať	k1gFnSc2	trať
313	[number]	k4	313
provozované	provozovaný	k2eAgNnSc1d1	provozované
dopravcem	dopravce	k1gMnSc7	dopravce
OKD	OKD	kA	OKD
<g/>
,	,	kIx,	,
Doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Změněno	změněn	k2eAgNnSc1d1	změněno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
značení	značení	k1gNnSc1	značení
vlakových	vlakový	k2eAgFnPc2d1	vlaková
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
původní	původní	k2eAgNnSc4d1	původní
traťové	traťový	k2eAgNnSc4d1	traťové
značení	značení	k1gNnSc4	značení
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
900	[number]	k4	900
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
linkovým	linkový	k2eAgNnSc7d1	linkové
značením	značení	k1gNnSc7	značení
písmenem	písmeno	k1gNnSc7	písmeno
S	s	k7c7	s
<g/>
,	,	kIx,	,
R	R	kA	R
nebo	nebo	k8xC	nebo
V	v	k7c6	v
doplněným	doplněná	k1gFnPc3	doplněná
jedno-	jedno-	k?	jedno-
nebo	nebo	k8xC	nebo
dvoumístným	dvoumístný	k2eAgNnSc7d1	dvoumístné
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
8	[number]	k4	8
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
linek	linka	k1gFnPc2	linka
MHD	MHD	kA	MHD
Havířov	Havířov	k1gInSc1	Havířov
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Havířov	Havířov	k1gInSc1	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
datu	datum	k1gNnSc3	datum
změny	změna	k1gFnSc2	změna
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
12	[number]	k4	12
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
zaintegrována	zaintegrován	k2eAgFnSc1d1	zaintegrován
trať	trať	k1gFnSc1	trať
313	[number]	k4	313
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
V	v	k7c6	v
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jsou	být	k5eAaImIp3nP	být
integrovány	integrovat	k5eAaBmNgFnP	integrovat
všechny	všechen	k3xTgFnPc1	všechen
tratě	trať	k1gFnPc1	trať
na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
linek	linka	k1gFnPc2	linka
MHD	MHD	kA	MHD
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
MHD	MHD	kA	MHD
Třinec	Třinec	k1gInSc1	Třinec
a	a	k8xC	a
meziměstských	meziměstský	k2eAgFnPc2d1	meziměstská
linek	linka	k1gFnPc2	linka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Třinecka	Třinecko	k1gNnSc2	Třinecko
a	a	k8xC	a
Jablunkovska	Jablunkovsko	k1gNnSc2	Jablunkovsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
meziměstských	meziměstský	k2eAgFnPc2d1	meziměstská
linek	linka	k1gFnPc2	linka
provozovaných	provozovaný	k2eAgFnPc2d1	provozovaná
dopravcem	dopravce	k1gMnSc7	dopravce
ČSAD	ČSAD	kA	ČSAD
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dni	den	k1gInSc3	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
se	se	k3xPyFc4	se
k	k	k7c3	k
ODISu	ODISus	k1gInSc3	ODISus
připojil	připojit	k5eAaPmAgMnS	připojit
dopravce	dopravce	k1gMnSc1	dopravce
ČSAD	ČSAD	kA	ČSAD
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
dopravce	dopravce	k1gMnSc1	dopravce
ARRIVA	ARRIVA	kA	ARRIVA
MORAVA	Morava	k1gFnSc1	Morava
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Třinecka	Třinecko	k1gNnSc2	Třinecko
a	a	k8xC	a
Jablunkovska	Jablunkovsko	k1gNnSc2	Jablunkovsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
provozuje	provozovat	k5eAaImIp3nS	provozovat
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Českotěšínska	Českotěšínsko	k1gNnSc2	Českotěšínsko
dopravce	dopravce	k1gMnSc2	dopravce
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc1	oblast
Karvinska	Karvinsko	k1gNnSc2	Karvinsko
a	a	k8xC	a
Orlovska	Orlovsko	k1gNnSc2	Orlovsko
provozovány	provozovat	k5eAaImNgFnP	provozovat
dopravcem	dopravce	k1gMnSc7	dopravce
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nové	nový	k2eAgFnSc2d1	nová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
i	i	k9	i
zde	zde	k6eAd1	zde
možnost	možnost	k1gFnSc1	možnost
platby	platba	k1gFnSc2	platba
bezkontaktní	bezkontaktní	k2eAgFnSc1d1	bezkontaktní
platební	platební	k2eAgFnSc7d1	platební
kartou	karta	k1gFnSc7	karta
nebo	nebo	k8xC	nebo
bezplatného	bezplatný	k2eAgNnSc2d1	bezplatné
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
WiFi	WiFi	k1gNnPc3	WiFi
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
brzy	brzy	k6eAd1	brzy
díky	díky	k7c3	díky
novým	nový	k2eAgInPc3d1	nový
tendrům	tendr	k1gInPc3	tendr
rozšířit	rozšířit	k5eAaPmF	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
tendrů	tendr	k1gInPc2	tendr
byli	být	k5eAaImAgMnP	být
k	k	k7c3	k
provozu	provoz	k1gInSc3	provoz
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
vybráni	vybrán	k2eAgMnPc1d1	vybrán
následující	následující	k2eAgMnPc1d1	následující
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Havířovska	Havířovsko	k1gNnSc2	Havířovsko
bude	být	k5eAaImBp3nS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
provozovat	provozovat	k5eAaImF	provozovat
konsorcium	konsorcium	k1gNnSc1	konsorcium
společností	společnost	k1gFnPc2	společnost
ČSAD	ČSAD	kA	ČSAD
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozdnímu	pozdní	k2eAgInSc3d1	pozdní
podpisu	podpis	k1gInSc2	podpis
některých	některý	k3yIgFnPc2	některý
smluv	smlouva	k1gFnPc2	smlouva
byl	být	k5eAaImAgInS	být
začátek	začátek	k1gInSc1	začátek
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
odsunut	odsunut	k2eAgInSc1d1	odsunut
až	až	k9	až
na	na	k7c4	na
červen	červen	k1gInSc4	červen
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
na	na	k7c6	na
Hlučínsku	Hlučínsko	k1gNnSc6	Hlučínsko
pak	pak	k6eAd1	pak
na	na	k7c4	na
červen	červen	k1gInSc4	červen
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
smlouvy	smlouva	k1gFnPc1	smlouva
na	na	k7c4	na
"	"	kIx"	"
<g/>
překlenovací	překlenovací	k2eAgNnSc4d1	překlenovací
<g/>
"	"	kIx"	"
období	období	k1gNnSc4	období
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
dopravci	dopravce	k1gMnPc7	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
Opavsko	Opavsko	k1gNnSc1	Opavsko
<g/>
,	,	kIx,	,
Vítkovsko	Vítkovsko	k1gNnSc1	Vítkovsko
<g/>
,	,	kIx,	,
Frýdecko-Místecko	Frýdecko-Místecko	k1gNnSc1	Frýdecko-Místecko
<g/>
,	,	kIx,	,
Bílovecko	Bílovecko	k1gNnSc1	Bílovecko
a	a	k8xC	a
Hlučínsko	Hlučínsko	k1gNnSc1	Hlučínsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
ODISka	ODISko	k1gNnSc2	ODISko
==	==	k?	==
</s>
</p>
<p>
<s>
ODISka	ODISka	k6eAd1	ODISka
je	být	k5eAaImIp3nS	být
jednotný	jednotný	k2eAgInSc4d1	jednotný
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
většina	většina	k1gFnSc1	většina
dopravců	dopravce	k1gMnPc2	dopravce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ODIS	ODIS	kA	ODIS
vydává	vydávat	k5eAaPmIp3nS	vydávat
nebo	nebo	k8xC	nebo
bude	být	k5eAaImBp3nS	být
vydávat	vydávat	k5eAaPmF	vydávat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
čipové	čipový	k2eAgFnPc4d1	čipová
karty	karta	k1gFnPc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použitelnost	použitelnost	k1gFnSc4	použitelnost
karty	karta	k1gFnSc2	karta
pro	pro	k7c4	pro
předplatné	předplatný	k2eAgMnPc4d1	předplatný
i	i	k8xC	i
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
jízdné	jízdný	k1gMnPc4	jízdný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ODIS	ODIS	kA	ODIS
a	a	k8xC	a
ve	v	k7c6	v
vymezených	vymezený	k2eAgInPc6d1	vymezený
případech	případ	k1gInPc6	případ
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
byla	být	k5eAaImAgFnS	být
e-karta	earta	k1gFnSc1	e-karta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
vydával	vydávat	k5eAaImAgInS	vydávat
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
která	který	k3yIgFnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
použitelná	použitelný	k2eAgFnSc1d1	použitelná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
předplatní	předplatní	k2eAgInPc4d1	předplatní
časové	časový	k2eAgInPc4d1	časový
kupóny	kupón	k1gInPc4	kupón
platné	platný	k2eAgInPc4d1	platný
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
vozidlech	vozidlo	k1gNnPc6	vozidlo
DPO	DPO	kA	DPO
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc1	možnost
použití	použití	k1gNnSc2	použití
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
i	i	k9	i
na	na	k7c4	na
autobusové	autobusový	k2eAgFnPc4d1	autobusová
linky	linka	k1gFnPc4	linka
dopravců	dopravce	k1gMnPc2	dopravce
ARRIVA	ARRIVA	kA	ARRIVA
MORAVA	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
TQM	TQM	kA	TQM
a	a	k8xC	a
Radovan	Radovan	k1gMnSc1	Radovan
Maxner	Maxner	k1gMnSc1	Maxner
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
platí	platit	k5eAaImIp3nS	platit
karta	karta	k1gFnSc1	karta
i	i	k9	i
ve	v	k7c6	v
vlacích	vlak	k1gInPc6	vlak
zařazených	zařazený	k2eAgInPc6d1	zařazený
do	do	k7c2	do
ODIS	ODIS	kA	ODIS
a	a	k8xC	a
kupóny	kupón	k1gInPc1	kupón
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
na	na	k7c4	na
karty	karta	k1gFnPc4	karta
nahrávány	nahráván	k2eAgFnPc4d1	nahrávána
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
železničních	železniční	k2eAgFnPc6d1	železniční
pokladnách	pokladna	k1gFnPc6	pokladna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ODIS	ODIS	kA	ODIS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
kromě	kromě	k7c2	kromě
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
vydává	vydávat	k5eAaPmIp3nS	vydávat
ODISku	ODISka	k1gFnSc4	ODISka
i	i	k9	i
Městský	městský	k2eAgInSc1d1	městský
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
ARRIVA	ARRIVA	kA	ARRIVA
MORAVA	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Trasdev	Trasdev	k1gFnSc1	Trasdev
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
TQM	TQM	kA	TQM
-	-	kIx~	-
holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
Osoblažská	osoblažský	k2eAgFnSc1d1	Osoblažská
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Frýdek	Frýdek	k1gInSc1	Frýdek
-	-	kIx~	-
Místek	Místek	k1gInSc1	Místek
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
vydavatele	vydavatel	k1gMnPc4	vydavatel
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
ODISky	ODISka	k1gFnPc1	ODISka
vzájemně	vzájemně	k6eAd1	vzájemně
uznávány	uznáván	k2eAgFnPc1d1	uznávána
všemi	všecek	k3xTgFnPc7	všecek
dopravci	dopravce	k1gMnPc1	dopravce
ODIS	ODIS	kA	ODIS
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
elektronické	elektronický	k2eAgFnSc2d1	elektronická
peněženky	peněženka	k1gFnSc2	peněženka
ODISky	ODISka	k1gFnSc2	ODISka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
platit	platit	k5eAaImF	platit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
i	i	k8xC	i
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
časové	časový	k2eAgFnPc4d1	časová
jízdné	jízdný	k2eAgFnPc4d1	Jízdná
(	(	kIx(	(
<g/>
u	u	k7c2	u
dopravců	dopravce	k1gMnPc2	dopravce
ARRIVA	ARRIVA	kA	ARRIVA
MORAVA	Morava	k1gFnSc1	Morava
a	a	k8xC	a
TQM	TQM	kA	TQM
-	-	kIx~	-
holding	holding	k1gInSc1	holding
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zakoupit	zakoupit	k5eAaPmF	zakoupit
u	u	k7c2	u
řidiče	řidič	k1gMnSc2	řidič
jednotlivé	jednotlivý	k2eAgNnSc4d1	jednotlivé
jízdné	jízdné	k1gNnSc4	jízdné
na	na	k7c6	na
území	území	k1gNnSc6	území
celé	celý	k2eAgFnSc2d1	celá
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
ODISky	ODISka	k1gFnSc2	ODISka
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
bezplatně	bezplatně	k6eAd1	bezplatně
nahrát	nahrát	k5eAaPmF	nahrát
tzv.	tzv.	kA	tzv.
Kartu	karta	k1gFnSc4	karta
ČD	ČD	kA	ČD
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nahradit	nahradit	k5eAaPmF	nahradit
IN	IN	kA	IN
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČD	ČD	kA	ČD
s	s	k7c7	s
ODISkou	ODISka	k1gFnSc7	ODISka
cestovat	cestovat	k5eAaImF	cestovat
rovněž	rovněž	k9	rovněž
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
objednání	objednání	k1gNnSc2	objednání
si	se	k3xPyFc3	se
jízdenky	jízdenka	k1gFnPc4	jízdenka
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
pohodlí	pohodlí	k1gNnSc2	pohodlí
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
jejího	její	k3xOp3gNnSc2	její
vytištění	vytištění	k1gNnSc2	vytištění
či	či	k8xC	či
čekání	čekání	k1gNnSc2	čekání
ve	v	k7c6	v
frontě	fronta	k1gFnSc6	fronta
u	u	k7c2	u
pokladny	pokladna	k1gFnSc2	pokladna
ČD	ČD	kA	ČD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ODISku	ODISka	k1gFnSc4	ODISka
použít	použít	k5eAaPmF	použít
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
dopravců	dopravce	k1gMnPc2	dopravce
zařazených	zařazený	k2eAgMnPc2d1	zařazený
v	v	k7c6	v
ODIS	ODIS	kA	ODIS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
již	již	k9	již
400	[number]	k4	400
000	[number]	k4	000
ODISek	ODISky	k1gFnPc2	ODISky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
"	"	kIx"	"
<g/>
Chytrá	chytrý	k2eAgFnSc1d1	chytrá
<g/>
"	"	kIx"	"
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
společně	společně	k6eAd1	společně
s	s	k7c7	s
ODIS	ODIS	kA	ODIS
snaží	snažit	k5eAaImIp3nS	snažit
kromě	kromě	k7c2	kromě
zvýšení	zvýšení	k1gNnSc2	zvýšení
komfortu	komfort	k1gInSc2	komfort
(	(	kIx(	(
<g/>
WiFi	WiF	k1gFnSc2	WiF
<g/>
,	,	kIx,	,
klimatizace	klimatizace	k1gFnSc2	klimatizace
<g/>
,	,	kIx,	,
USB	USB	kA	USB
nabíječky	nabíječka	k1gFnPc1	nabíječka
<g/>
,	,	kIx,	,
chytrý	chytrý	k2eAgInSc1d1	chytrý
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
i	i	k9	i
o	o	k7c4	o
snížení	snížení	k1gNnSc4	snížení
dopadu	dopad	k1gInSc2	dopad
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
žívotní	žívotní	k2eAgNnSc4d1	žívotní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vytyčení	vytyčení	k1gNnSc2	vytyčení
cílů	cíl	k1gInPc2	cíl
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
autobusů	autobus	k1gInPc2	autobus
s	s	k7c7	s
alternativním	alternativní	k2eAgInSc7d1	alternativní
pohonem	pohon	k1gInSc7	pohon
(	(	kIx(	(
<g/>
elektro	elektro	k1gNnSc1	elektro
<g/>
,	,	kIx,	,
CNG	CNG	kA	CNG
<g/>
)	)	kIx)	)
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
i	i	k9	i
o	o	k7c6	o
zavádění	zavádění	k1gNnSc6	zavádění
vozidel	vozidlo	k1gNnPc2	vozidlo
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zkušebním	zkušební	k2eAgInSc6d1	zkušební
provozu	provoz	k1gInSc6	provoz
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
plnička	plnička	k1gFnSc1	plnička
na	na	k7c6	na
Hranečníku	hranečník	k1gInSc6	hranečník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
budou	být	k5eAaImBp3nP	být
tankovat	tankovat	k5eAaImF	tankovat
autobusy	autobus	k1gInPc4	autobus
dopravců	dopravce	k1gMnPc2	dopravce
Dopravní	dopravní	k2eAgInSc4d1	dopravní
podnik	podnik	k1gInSc4	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
zprávy	zpráva	k1gFnPc1	zpráva
kraje	kraj	k1gInSc2	kraj
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
pilotním	pilotní	k2eAgInSc6d1	pilotní
provozu	provoz	k1gInSc6	provoz
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
10	[number]	k4	10
vozidel	vozidlo	k1gNnPc2	vozidlo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Karvinska	Karvinsko	k1gNnSc2	Karvinsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
doprava	doprava	k1gFnSc1	doprava
"	"	kIx"	"
<g/>
chytřejší	chytrý	k2eAgFnSc1d2	chytřejší
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
kraje	kraj	k1gInSc2	kraj
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
moderní	moderní	k2eAgFnPc1d1	moderní
odjezdové	odjezdový	k2eAgFnPc1d1	odjezdová
tabule	tabule	k1gFnPc1	tabule
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
aktuální	aktuální	k2eAgInSc4d1	aktuální
čas	čas	k1gInSc4	čas
odjezdu	odjezd	k1gInSc2	odjezd
včetně	včetně	k7c2	včetně
případného	případný	k2eAgNnSc2d1	případné
zpoždění	zpoždění	k1gNnSc2	zpoždění
spoje	spoj	k1gInPc1	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
dopravce	dopravce	k1gMnPc4	dopravce
vybrané	vybraný	k2eAgMnPc4d1	vybraný
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
zakázkách	zakázka	k1gFnPc6	zakázka
závazné	závazný	k2eAgFnSc2d1	závazná
nové	nový	k2eAgFnSc2d1	nová
Technické	technický	k2eAgFnSc2d1	technická
a	a	k8xC	a
provozní	provozní	k2eAgInPc4d1	provozní
standardy	standard	k1gInPc4	standard
ODIS	ODIS	kA	ODIS
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
potřebný	potřebný	k2eAgInSc4d1	potřebný
komfort	komfort	k1gInSc4	komfort
dopravy	doprava	k1gFnSc2	doprava
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
specifikaci	specifikace	k1gFnSc4	specifikace
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
informačním	informační	k2eAgInSc7d1	informační
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
dynamické	dynamický	k2eAgFnPc4d1	dynamická
odjezdové	odjezdový	k2eAgFnPc4d1	odjezdová
tabule	tabule	k1gFnPc4	tabule
ve	v	k7c6	v
vozidlech	vozidlo	k1gNnPc6	vozidlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
konče	konče	k7c7	konče
klimatizací	klimatizace	k1gFnSc7	klimatizace
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
sedačkami	sedačka	k1gFnPc7	sedačka
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
i	i	k9	i
moderní	moderní	k2eAgInSc1d1	moderní
dispečink	dispečink	k1gInSc1	dispečink
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sleduje	sledovat	k5eAaImIp3nS	sledovat
pohyb	pohyb	k1gInSc1	pohyb
vozidel	vozidlo	k1gNnPc2	vozidlo
na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
veřejně	veřejně	k6eAd1	veřejně
přístupný	přístupný	k2eAgInSc1d1	přístupný
na	na	k7c6	na
následujícím	následující	k2eAgInSc6d1	následující
odkazu	odkaz	k1gInSc6	odkaz
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Dopravci	dopravce	k1gMnPc7	dopravce
v	v	k7c6	v
ODIS	ODIS	kA	ODIS
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ODIS	ODIS	kA	ODIS
nyní	nyní	k6eAd1	nyní
působí	působit	k5eAaImIp3nP	působit
následující	následující	k2eAgMnPc1d1	následující
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
GW	GW	kA	GW
Train	Train	k1gMnSc1	Train
Regio	Regio	k1gMnSc1	Regio
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Arriva	Arriva	k1gFnSc1	Arriva
Morava	Morava	k1gFnSc1	Morava
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
TQM	TQM	kA	TQM
-	-	kIx~	-
holding	holding	k1gInSc1	holding
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
</s>
</p>
<p>
<s>
ČSAD	ČSAD	kA	ČSAD
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ČSAD	ČSAD	kA	ČSAD
Vsetín	Vsetín	k1gInSc1	Vsetín
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Vojtila	Vojtil	k1gMnSc2	Vojtil
Trans	trans	k1gInSc1	trans
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
</s>
</p>
<p>
<s>
Transdev	Transdev	k1gFnSc1	Transdev
Morava	Morava	k1gFnSc1	Morava
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
</s>
</p>
<p>
<s>
RegioJet	RegioJet	k1gInSc1	RegioJet
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
KODIS	KODIS	kA	KODIS
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Koordinátora	koordinátor	k1gMnSc2	koordinátor
ODIS	ODIS	kA	ODIS
</s>
</p>
<p>
<s>
DPO	DPO	kA	DPO
-	-	kIx~	-
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
MDPO	MDPO	kA	MDPO
-	-	kIx~	-
Městský	městský	k2eAgInSc1d1	městský
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Opava	Opava	k1gFnSc1	Opava
</s>
</p>
<p>
<s>
ČD	ČD	kA	ČD
-	-	kIx~	-
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
</s>
</p>
<p>
<s>
GW	GW	kA	GW
Train	Train	k1gMnSc1	Train
-	-	kIx~	-
GW	GW	kA	GW
Train	Train	k1gMnSc1	Train
Regio	Regio	k1gMnSc1	Regio
</s>
</p>
<p>
<s>
ODS	ODS	kA	ODS
-	-	kIx~	-
Osoblažská	osoblažský	k2eAgFnSc1d1	Osoblažská
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
ČSAD	ČSAD	kA	ČSAD
F-M	F-M	k1gFnSc1	F-M
a	a	k8xC	a
Karviná	Karviná	k1gFnSc1	Karviná
-	-	kIx~	-
ČSAD	ČSAD	kA	ČSAD
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Havířov	Havířov	k1gInSc1	Havířov
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Karviná	Karviná	k1gFnSc1	Karviná
</s>
</p>
<p>
<s>
Arriva	Arriva	k1gFnSc1	Arriva
Morava	Morava	k1gFnSc1	Morava
-	-	kIx~	-
Arriva	Arriva	k1gFnSc1	Arriva
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
TQM	TQM	kA	TQM
-	-	kIx~	-
TQM	TQM	kA	TQM
</s>
</p>
