<p>
<s>
Gotika	gotika	k1gFnSc1	gotika
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
sloh	sloh	k1gInSc4	sloh
pomalu	pomalu	k6eAd1	pomalu
navazující	navazující	k2eAgNnSc4d1	navazující
na	na	k7c4	na
sloh	sloh	k1gInSc4	sloh
románský	románský	k2eAgInSc4d1	románský
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
projevovat	projevovat	k5eAaImF	projevovat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c4	po
další	další	k2eAgNnPc4d1	další
tři	tři	k4xCgNnPc4	tři
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
i	i	k8xC	i
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
gotika	gotika	k1gFnSc1	gotika
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
již	již	k6eAd1	již
prosadila	prosadit	k5eAaPmAgFnS	prosadit
následující	následující	k2eAgFnSc1d1	následující
renesance	renesance	k1gFnSc1	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
z	z	k7c2	z
gotiky	gotika	k1gFnSc2	gotika
nevyvinula	vyvinout	k5eNaPmAgFnS	vyvinout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snažila	snažit	k5eAaImAgFnS	snažit
popřít	popřít	k5eAaPmF	popřít
a	a	k8xC	a
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
vývoji	vývoj	k1gInSc6	vývoj
se	se	k3xPyFc4	se
opírala	opírat	k5eAaImAgFnS	opírat
o	o	k7c4	o
antické	antický	k2eAgNnSc4d1	antické
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
gotika	gotika	k1gFnSc1	gotika
nebo	nebo	k8xC	nebo
gotický	gotický	k2eAgMnSc1d1	gotický
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
ke	k	k7c3	k
Gótům	Gót	k1gMnPc3	Gót
<g/>
.	.	kIx.	.
</s>
<s>
Předcházející	předcházející	k2eAgFnSc3d1	předcházející
epoše	epocha	k1gFnSc3	epocha
jej	on	k3xPp3gMnSc4	on
přisoudil	přisoudit	k5eAaPmAgMnS	přisoudit
s	s	k7c7	s
opovržením	opovržení	k1gNnSc7	opovržení
Giorgio	Giorgio	k1gNnSc4	Giorgio
Vasari	Vasari	k1gNnSc2	Vasari
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
italští	italský	k2eAgMnPc1d1	italský
humanisté	humanista	k1gMnPc1	humanista
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
gótské	gótský	k2eAgNnSc4d1	gótské
<g/>
"	"	kIx"	"
umění	umění	k1gNnSc4	umění
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
asociaci	asociace	k1gFnSc4	asociace
primitivního	primitivní	k2eAgMnSc2d1	primitivní
či	či	k8xC	či
barbarského	barbarský	k2eAgNnSc2d1	barbarské
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
sloh	sloh	k1gInSc1	sloh
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
style	styl	k1gInSc5	styl
ogival	ogival	k1gInSc4	ogival
<g/>
"	"	kIx"	"
–	–	k?	–
sloh	sloh	k1gInSc1	sloh
lomený	lomený	k2eAgInSc1d1	lomený
<g/>
.	.	kIx.	.
<g/>
Gotický	gotický	k2eAgInSc1d1	gotický
sloh	sloh	k1gInSc1	sloh
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
postupným	postupný	k2eAgInSc7d1	postupný
vývojem	vývoj	k1gInSc7	vývoj
ze	z	k7c2	z
slohu	sloh	k1gInSc2	sloh
románského	románský	k2eAgInSc2d1	románský
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
gotického	gotický	k2eAgInSc2d1	gotický
slohu	sloh	k1gInSc2	sloh
začínají	začínat	k5eAaImIp3nP	začínat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
vybudováním	vybudování	k1gNnSc7	vybudování
opatství	opatství	k1gNnSc2	opatství
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
poblíž	poblíž	k7c2	poblíž
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzniklém	vzniklý	k2eAgInSc6d1	vzniklý
stylu	styl	k1gInSc6	styl
je	být	k5eAaImIp3nS	být
i	i	k8xC	i
proto	proto	k8xC	proto
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
obsažena	obsažen	k2eAgFnSc1d1	obsažena
filozofie	filozofie	k1gFnSc1	filozofie
směřování	směřování	k1gNnSc2	směřování
za	za	k7c7	za
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
formální	formální	k2eAgFnSc6d1	formální
stránce	stránka	k1gFnSc6	stránka
vyjadřována	vyjadřován	k2eAgFnSc1d1	vyjadřována
vertikalismem	vertikalismus	k1gInSc7	vertikalismus
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
románskému	románský	k2eAgInSc3d1	románský
slohu	sloh	k1gInSc3	sloh
došlo	dojít	k5eAaPmAgNnS	dojít
především	především	k9	především
u	u	k7c2	u
staveb	stavba	k1gFnPc2	stavba
také	také	k9	také
k	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
prosvětlení	prosvětlení	k1gNnSc3	prosvětlení
a	a	k8xC	a
"	"	kIx"	"
<g/>
odhmotnění	odhmotnění	k1gNnSc1	odhmotnění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gotika	gotika	k1gFnSc1	gotika
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
románského	románský	k2eAgInSc2d1	románský
slohu	sloh	k1gInSc2	sloh
již	již	k6eAd1	již
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
plného	plný	k2eAgNnSc2d1	plné
rozšíření	rozšíření	k1gNnSc3	rozšíření
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
gotiky	gotika	k1gFnSc2	gotika
na	na	k7c6	na
územích	území	k1gNnPc6	území
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
na	na	k7c4	na
tamější	tamější	k2eAgNnSc4d1	tamější
umělecké	umělecký	k2eAgNnSc4d1	umělecké
cítění	cítění	k1gNnSc4	cítění
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
limitovaný	limitovaný	k2eAgInSc1d1	limitovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
zařazujeme	zařazovat	k5eAaImIp1nP	zařazovat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
období	období	k1gNnSc2	období
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
(	(	kIx(	(
<g/>
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
podnikány	podnikán	k2eAgFnPc1d1	podnikána
křížové	křížový	k2eAgFnPc1d1	křížová
výpravy	výprava	k1gFnPc1	výprava
do	do	k7c2	do
Orientu	Orient	k1gInSc2	Orient
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
Orientem	Orient	k1gInSc7	Orient
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
projevil	projevit	k5eAaPmAgInS	projevit
značně	značně	k6eAd1	značně
i	i	k9	i
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
složce	složka	k1gFnSc6	složka
instrumentální	instrumentální	k2eAgInPc1d1	instrumentální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
hlavně	hlavně	k9	hlavně
arabského	arabský	k2eAgInSc2d1	arabský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
alud	alud	k1gInSc4	alud
(	(	kIx(	(
<g/>
lauda	lauda	k1gFnSc1	lauda
=	=	kIx~	=
loutna	loutna	k1gFnSc1	loutna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zurna	zurna	k1gFnSc1	zurna
(	(	kIx(	(
<g/>
=	=	kIx~	=
šalmaj	šalmaj	k1gInSc1	šalmaj
-	-	kIx~	-
plátkový	plátkový	k2eAgInSc1d1	plátkový
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
fléten	flétna	k1gFnPc2	flétna
<g/>
,	,	kIx,	,
bicích	bicí	k2eAgInPc2d1	bicí
a	a	k8xC	a
strunných	strunný	k2eAgInPc2d1	strunný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
daidra	daidra	k1gFnSc1	daidra
(	(	kIx(	(
<g/>
=	=	kIx~	=
tamburína	tamburína	k1gFnSc1	tamburína
s	s	k7c7	s
plíšky	plíšek	k1gInPc7	plíšek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rybebky	rybebek	k1gInPc1	rybebek
(	(	kIx(	(
<g/>
=	=	kIx~	=
housle	housle	k1gFnPc4	housle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rebce	rebce	k1gMnSc1	rebce
(	(	kIx(	(
<g/>
=	=	kIx~	=
housličky	housličky	k1gFnPc4	housličky
<g/>
)	)	kIx)	)
a	a	k8xC	a
al	ala	k1gFnPc2	ala
rebab	rebaba	k1gFnPc2	rebaba
(	(	kIx(	(
<g/>
=	=	kIx~	=
arabské	arabský	k2eAgFnPc1d1	arabská
housličky	housličky	k1gFnPc1	housličky
které	který	k3yQgMnPc4	který
jsou	být	k5eAaImIp3nP	být
potaženy	potáhnout	k5eAaPmNgInP	potáhnout
kůží	kůže	k1gFnSc7	kůže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudbu	hudba	k1gFnSc4	hudba
gotiky	gotika	k1gFnSc2	gotika
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
období	období	k1gNnPc2	období
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ars	ars	k?	ars
antiqua	antiqua	k1gFnSc1	antiqua
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
starý	starý	k2eAgMnSc1d1	starý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ars	ars	k?	ars
nova	nova	k1gFnSc1	nova
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
novější	nový	k2eAgMnSc1d2	novější
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
1300	[number]	k4	1300
do	do	k7c2	do
1430	[number]	k4	1430
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ars	ars	k?	ars
subtilior	subtilior	k1gInSc1	subtilior
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zjemnělý	zjemnělý	k2eAgMnSc1d1	zjemnělý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Móda	móda	k1gFnSc1	móda
==	==	k?	==
</s>
</p>
<p>
<s>
Tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
módním	módní	k2eAgInSc7d1	módní
trendem	trend	k1gInSc7	trend
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
špičky	špička	k1gFnSc2	špička
bot	bota	k1gFnPc2	bota
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
rukávy	rukáv	k1gInPc4	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Špičky	špička	k1gFnPc1	špička
byly	být	k5eAaImAgFnP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vysoké	vysoký	k2eAgNnSc1d1	vysoké
je	být	k5eAaImIp3nS	být
mužovo	mužův	k2eAgNnSc1d1	mužovo
postavení	postavení	k1gNnSc1	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
–	–	k?	–
čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgNnSc1d2	vyšší
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
delší	dlouhý	k2eAgFnSc1d2	delší
špičky	špička	k1gFnSc2	špička
(	(	kIx(	(
<g/>
jestliže	jestliže	k8xS	jestliže
byly	být	k5eAaImAgFnP	být
špičky	špička	k1gFnPc1	špička
příliš	příliš	k6eAd1	příliš
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
zavazovali	zavazovat	k5eAaImAgMnP	zavazovat
kolem	kolem	k7c2	kolem
holení	holeň	k1gFnPc2	holeň
nebo	nebo	k8xC	nebo
kotníků	kotník	k1gInPc2	kotník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
často	často	k6eAd1	často
odívaly	odívat	k5eAaImAgFnP	odívat
do	do	k7c2	do
šatů	šat	k1gInPc2	šat
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rozstřiženými	rozstřižený	k2eAgInPc7d1	rozstřižený
rukávy	rukáv	k1gInPc7	rukáv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavách	hlava	k1gFnPc6	hlava
nosily	nosit	k5eAaImAgFnP	nosit
buď	buď	k8xC	buď
vysoké	vysoký	k2eAgFnPc1d1	vysoká
čepice	čepice	k1gFnPc1	čepice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
připomínaly	připomínat	k5eAaImAgFnP	připomínat
kužel	kužel	k1gInSc4	kužel
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zdobený	zdobený	k2eAgInSc1d1	zdobený
závojem	závoj	k1gInSc7	závoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vykukoval	vykukovat	k5eAaImAgInS	vykukovat
ze	z	k7c2	z
špičky	špička	k1gFnSc2	špička
klobouku	klobouk	k1gInSc2	klobouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
roušky	rouška	k1gFnPc1	rouška
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
různými	různý	k2eAgNnPc7d1	různé
způsoby	způsob	k1gInPc4	způsob
ovázané	ovázaný	k2eAgNnSc1d1	ovázané
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nosily	nosit	k5eAaImAgInP	nosit
henniny	hennin	k2eAgInPc1d1	hennin
-	-	kIx~	-
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc1d1	vysoký
"	"	kIx"	"
<g/>
klobouky	klobouk	k1gInPc1	klobouk
bílé	bílý	k2eAgFnSc2d1	bílá
paní	paní	k1gFnSc2	paní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mužů	muž	k1gMnPc2	muž
značil	značit	k5eAaImAgInS	značit
vyšší	vysoký	k2eAgInSc1d2	vyšší
límec	límec	k1gInSc1	límec
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
společenské	společenský	k2eAgFnSc3d1	společenská
třídě	třída	k1gFnSc3	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
mohutné	mohutný	k2eAgFnSc2d1	mohutná
stavby	stavba	k1gFnSc2	stavba
jako	jako	k8xC	jako
např.	např.	kA	např.
mnohé	mnohé	k1gNnSc4	mnohé
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
stavby	stavba	k1gFnPc1	stavba
menšího	malý	k2eAgInSc2d2	menší
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znaky	znak	k1gInPc1	znak
architektury	architektura	k1gFnSc2	architektura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vertikalismus	vertikalismus	k1gInSc1	vertikalismus
–	–	k?	–
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
stavbě	stavba	k1gFnSc6	stavba
i	i	k8xC	i
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
jejích	její	k3xOp3gFnPc6	její
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Protažení	protažení	k1gNnSc1	protažení
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
zúžení	zúžení	k1gNnSc2	zúžení
<g/>
.	.	kIx.	.
</s>
<s>
Optický	optický	k2eAgInSc1d1	optický
dojem	dojem	k1gInSc1	dojem
odhmotnění	odhmotnění	k1gNnSc2	odhmotnění
a	a	k8xC	a
přiblížení	přiblížení	k1gNnSc4	přiblížení
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lomený	lomený	k2eAgInSc1d1	lomený
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
vývojem	vývoj	k1gInSc7	vývoj
od	od	k7c2	od
širokého	široký	k2eAgInSc2d1	široký
a	a	k8xC	a
méně	málo	k6eAd2	málo
lomeného	lomený	k2eAgNnSc2d1	lomené
až	až	k9	až
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
ostře	ostro	k6eAd1	ostro
zalomenému	zalomený	k2eAgMnSc3d1	zalomený
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oblouk	oblouk	k1gInSc1	oblouk
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
oknech	okno	k1gNnPc6	okno
<g/>
,	,	kIx,	,
portálech	portál	k1gInPc6	portál
a	a	k8xC	a
všech	všecek	k3xTgFnPc6	všecek
zdobných	zdobný	k2eAgFnPc6d1	zdobná
částech	část	k1gFnPc6	část
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
řemesle	řemeslo	k1gNnSc6	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
oblouku	oblouk	k1gInSc2	oblouk
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
i	i	k8xC	i
léta	léto	k1gNnPc4	léto
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
opěrný	opěrný	k2eAgInSc1d1	opěrný
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
klenebních	klenební	k2eAgNnPc2d1	klenební
žeber	žebro	k1gNnPc2	žebro
svedených	svedený	k2eAgNnPc2d1	svedené
do	do	k7c2	do
přípor	přípora	k1gFnPc2	přípora
předsazených	předsazený	k2eAgFnPc2d1	předsazená
před	před	k7c7	před
pilíři	pilíř	k1gInPc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
stropu	strop	k1gInSc2	strop
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
úzkých	úzký	k2eAgFnPc2d1	úzká
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
postupně	postupně	k6eAd1	postupně
odlehčit	odlehčit	k5eAaPmF	odlehčit
zdi	zeď	k1gFnPc4	zeď
a	a	k8xC	a
zaplnit	zaplnit	k5eAaPmF	zaplnit
je	být	k5eAaImIp3nS	být
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
okenními	okenní	k2eAgFnPc7d1	okenní
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Nejpokročilejším	pokročilý	k2eAgInSc7d3	nejpokročilejší
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
královská	královský	k2eAgFnSc1d1	královská
kaple	kaple	k1gFnSc1	kaple
Sainte	Saint	k1gInSc5	Saint
Chapelle	Chapell	k1gMnPc4	Chapell
v	v	k7c4	v
Conciergerie	Conciergerie	k1gFnPc4	Conciergerie
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
gotickým	gotický	k2eAgInSc7d1	gotický
skleníkem	skleník	k1gInSc7	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Klenební	klenební	k2eAgNnPc1d1	klenební
žebra	žebro	k1gNnPc1	žebro
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
gotiky	gotika	k1gFnSc2	gotika
ztratila	ztratit	k5eAaPmAgFnS	ztratit
nosnou	nosný	k2eAgFnSc4d1	nosná
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
dekorativním	dekorativní	k2eAgInSc7d1	dekorativní
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
opěrný	opěrný	k2eAgInSc1d1	opěrný
systém	systém	k1gInSc1	systém
používaný	používaný	k2eAgInSc1d1	používaný
zejména	zejména	k9	zejména
u	u	k7c2	u
gotických	gotický	k2eAgFnPc2d1	gotická
katedrál	katedrála	k1gFnPc2	katedrála
je	být	k5eAaImIp3nS	být
pomocným	pomocný	k2eAgInSc7d1	pomocný
činitelem	činitel	k1gInSc7	činitel
podpírajícím	podpírající	k2eAgInSc7d1	podpírající
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
pilíře	pilíř	k1gInPc1	pilíř
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
tlak	tlak	k1gInSc4	tlak
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
opěrných	opěrný	k2eAgInPc2d1	opěrný
oblouků	oblouk	k1gInPc2	oblouk
a	a	k8xC	a
opěrných	opěrný	k2eAgInPc2d1	opěrný
pilířů	pilíř	k1gInPc2	pilíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
zdobné	zdobný	k2eAgInPc4d1	zdobný
prvky	prvek	k1gInPc4	prvek
patří	patřit	k5eAaImIp3nP	patřit
fiály	fiála	k1gFnPc1	fiála
(	(	kIx(	(
<g/>
drobné	drobný	k2eAgFnPc1d1	drobná
věžičky	věžička	k1gFnPc1	věžička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zdobily	zdobit	k5eAaImAgFnP	zdobit
střechu	střecha	k1gFnSc4	střecha
i	i	k8xC	i
umělecké	umělecký	k2eAgNnSc4d1	umělecké
řemeslo	řemeslo	k1gNnSc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
prvkem	prvek	k1gInSc7	prvek
byly	být	k5eAaImAgInP	být
chrliče	chrlič	k1gInPc1	chrlič
(	(	kIx(	(
<g/>
fantastické	fantastický	k2eAgFnPc1d1	fantastická
hlavy	hlava	k1gFnPc1	hlava
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
hubou	huba	k1gFnSc7	huba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
chrlily	chrlit	k5eAaImAgFnP	chrlit
vodu	voda	k1gFnSc4	voda
stékající	stékající	k2eAgFnSc2d1	stékající
ze	z	k7c2	z
střech	střecha	k1gFnPc2	střecha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgInSc7d1	poslední
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
rozety	rozeta	k1gFnPc1	rozeta
<g/>
,	,	kIx,	,
kruhová	kruhový	k2eAgNnPc1d1	kruhové
zdobená	zdobený	k2eAgNnPc1d1	zdobené
okna	okno	k1gNnPc1	okno
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
gotické	gotický	k2eAgFnPc1d1	gotická
katedrály	katedrála	k1gFnPc1	katedrála
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
:	:	kIx,	:
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c4	v
Chartres	Chartres	k1gInSc4	Chartres
(	(	kIx(	(
<g/>
nejvíce	hodně	k6eAd3	hodně
zdobena	zdobit	k5eAaImNgFnS	zdobit
sochařskými	sochařský	k2eAgInPc7d1	sochařský
díly	díl	k1gInPc7	díl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
portálu	portál	k1gInSc6	portál
jsou	být	k5eAaImIp3nP	být
umístěni	umístěn	k2eAgMnPc1d1	umístěn
světci	světec	k1gMnPc1	světec
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c4	v
Chartres	Chartres	k1gInSc4	Chartres
–	–	k?	–
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
gotiku	gotika	k1gFnSc4	gotika
každého	každý	k3xTgInSc2	každý
evropského	evropský	k2eAgInSc2d1	evropský
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
stalo	stát	k5eAaPmAgNnS	stát
něco	něco	k6eAd1	něco
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
stavby	stavba	k1gFnPc1	stavba
z	z	k7c2	z
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
neomítaly	omítat	k5eNaImAgInP	omítat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byly	být	k5eAaImAgInP	být
stropy	strop	k1gInPc1	strop
vyzdobeny	vyzdoben	k2eAgInPc1d1	vyzdoben
tzv.	tzv.	kA	tzv.
síťovou	síťový	k2eAgFnSc7d1	síťová
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
složitou	složitý	k2eAgFnSc4d1	složitá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohdy	mnohdy	k6eAd1	mnohdy
řešenou	řešený	k2eAgFnSc4d1	řešená
jen	jen	k6eAd1	jen
ornamentálně	ornamentálně	k6eAd1	ornamentálně
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
gotika	gotika	k1gFnSc1	gotika
(	(	kIx(	(
<g/>
Dóžecí	dóžecí	k2eAgInSc1d1	dóžecí
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
především	především	k6eAd1	především
ornamentem	ornament	k1gInSc7	ornament
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
prvky	prvek	k1gInPc7	prvek
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
gotika	gotika	k1gFnSc1	gotika
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vynikající	vynikající	k2eAgFnPc4d1	vynikající
světové	světový	k2eAgFnPc4d1	světová
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
malířské	malířský	k2eAgNnSc1d1	malířské
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
i	i	k9	i
sochařské	sochařský	k2eAgNnSc1d1	sochařské
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ceněna	cenit	k5eAaImNgFnS	cenit
na	na	k7c6	na
nejpřednějším	přední	k2eAgNnSc6d3	nejpřednější
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sochařství	sochařství	k1gNnSc2	sochařství
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
sochařské	sochařský	k2eAgInPc1d1	sochařský
projevy	projev	k1gInPc1	projev
byly	být	k5eAaImAgInP	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
doplňkem	doplněk	k1gInSc7	doplněk
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
v	v	k7c4	v
tendenci	tendence	k1gFnSc4	tendence
ideálů	ideál	k1gInPc2	ideál
gotiky	gotika	k1gFnSc2	gotika
převládala	převládat	k5eAaImAgFnS	převládat
vertikalita	vertikalita	k1gFnSc1	vertikalita
–	–	k?	–
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
drapérii	drapérie	k1gFnSc6	drapérie
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
nestály	stát	k5eNaImAgFnP	stát
volně	volně	k6eAd1	volně
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakoby	jakoby	k8xS	jakoby
byly	být	k5eAaImAgFnP	být
přilepeny	přilepen	k2eAgFnPc1d1	přilepena
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
architektuře	architektura	k1gFnSc3	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
chrámů	chrám	k1gInPc2	chrám
u	u	k7c2	u
portálů	portál	k1gInPc2	portál
se	se	k3xPyFc4	se
tesaly	tesat	k5eAaImAgFnP	tesat
sochy	socha	k1gFnPc1	socha
apoštolů	apoštol	k1gMnPc2	apoštol
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
i	i	k8xC	i
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
socha	socha	k1gFnSc1	socha
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
oltář	oltář	k1gInSc4	oltář
i	i	k9	i
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
projevem	projev	k1gInSc7	projev
gotického	gotický	k2eAgNnSc2d1	gotické
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
byla	být	k5eAaImAgFnS	být
zpodobení	zpodobení	k1gNnSc4	zpodobení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovala	zobrazovat	k5eAaImAgNnP	zobrazovat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
jako	jako	k9	jako
madona	madona	k1gFnSc1	madona
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
Ježíškem	ježíšek	k1gInSc7	ježíšek
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
pieta	pieta	k1gFnSc1	pieta
–	–	k?	–
truchlící	truchlící	k2eAgFnSc1d1	truchlící
matka	matka	k1gFnSc1	matka
s	s	k7c7	s
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
Kristovým	Kristův	k2eAgNnSc7d1	Kristovo
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
sejmutým	sejmutý	k2eAgFnPc3d1	sejmutá
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
klíně	klín	k1gInSc6	klín
(	(	kIx(	(
<g/>
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
pietà	pietà	k?	pietà
–	–	k?	–
lítost	lítost	k1gFnSc4	lítost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
námětem	námět	k1gInSc7	námět
se	se	k3xPyFc4	se
vztahovaly	vztahovat	k5eAaImAgFnP	vztahovat
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
významným	významný	k2eAgInPc3d1	významný
okamžikům	okamžik	k1gInPc3	okamžik
života	život	k1gInSc2	život
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zobrazovalo	zobrazovat	k5eAaImAgNnS	zobrazovat
gotické	gotický	k2eAgNnSc1d1	gotické
sochařství	sochařství	k1gNnSc4	sochařství
a	a	k8xC	a
malířství	malířství	k1gNnSc4	malířství
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
typické	typický	k2eAgNnSc1d1	typické
směřování	směřování	k1gNnSc1	směřování
vzhůru	vzhůru	k6eAd1	vzhůru
–	–	k?	–
byly	být	k5eAaImAgFnP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
a	a	k8xC	a
štíhlé	štíhlý	k2eAgFnPc1d1	štíhlá
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
prohnuté	prohnutý	k2eAgInPc1d1	prohnutý
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
písmene	písmeno	k1gNnSc2	písmeno
S.	S.	kA	S.
Jejich	jejich	k3xOp3gFnPc1	jejich
tváře	tvář	k1gFnPc4	tvář
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgInP	vyznačovat
novým	nový	k2eAgInSc7d1	nový
rysem	rys	k1gInSc7	rys
<g/>
.	.	kIx.	.
</s>
<s>
Nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
výrazy	výraz	k1gInPc4	výraz
zcela	zcela	k6eAd1	zcela
pozemských	pozemský	k2eAgInPc2d1	pozemský
lidských	lidský	k2eAgInPc2d1	lidský
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
prosté	prostý	k2eAgFnSc2d1	prostá
radosti	radost	k1gFnSc2	radost
nebo	nebo	k8xC	nebo
velkého	velký	k2eAgNnSc2d1	velké
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Ztvárnění	ztvárnění	k1gNnSc1	ztvárnění
Ježíše	Ježíš	k1gMnSc2	Ježíš
jako	jako	k8xS	jako
nemluvněte	nemluvně	k1gNnSc2	nemluvně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
středověkému	středověký	k2eAgNnSc3d1	středověké
chápání	chápání	k1gNnSc3	chápání
dítěte	dítě	k1gNnSc2	dítě
jako	jako	k8xS	jako
malého	malý	k2eAgMnSc2d1	malý
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typickým	typický	k2eAgInSc7d1	typický
námětem	námět	k1gInSc7	námět
byl	být	k5eAaImAgMnS	být
Kristus	Kristus	k1gMnSc1	Kristus
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
(	(	kIx(	(
<g/>
krucifix	krucifix	k1gInSc1	krucifix
<g/>
)	)	kIx)	)
a	a	k8xC	a
apoštolové	apoštol	k1gMnPc1	apoštol
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
atributy	atribut	k1gInPc7	atribut
<g/>
.	.	kIx.	.
<g/>
Materiálem	materiál	k1gInSc7	materiál
byl	být	k5eAaImAgInS	být
buď	buď	k8xC	buď
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
sochy	socha	k1gFnPc1	socha
vyřezávány	vyřezávat	k5eAaImNgFnP	vyřezávat
z	z	k7c2	z
lipového	lipový	k2eAgNnSc2d1	lipové
dřeva	dřevo	k1gNnSc2	dřevo
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
schopnými	schopný	k2eAgMnPc7d1	schopný
řezbáři	řezbář	k1gMnPc7	řezbář
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
se	se	k3xPyFc4	se
potahovala	potahovat	k5eAaImAgFnS	potahovat
jemným	jemný	k2eAgNnSc7d1	jemné
plátnem	plátno	k1gNnSc7	plátno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přilehlo	přilehnout	k5eAaPmAgNnS	přilehnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
polychromovala	polychromovat	k5eAaBmAgFnS	polychromovat
(	(	kIx(	(
<g/>
barvila	barvit	k5eAaImAgFnS	barvit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
tendenci	tendence	k1gFnSc4	tendence
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
přibližování	přibližování	k1gNnSc2	přibližování
se	se	k3xPyFc4	se
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
starších	starý	k2eAgFnPc2d2	starší
evropských	evropský	k2eAgFnPc2d1	Evropská
civilizací	civilizace	k1gFnPc2	civilizace
původně	původně	k6eAd1	původně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množství	množství	k1gNnSc1	množství
gotických	gotický	k2eAgFnPc2d1	gotická
soch	socha	k1gFnPc2	socha
překonalo	překonat	k5eAaPmAgNnS	překonat
věky	věk	k1gInPc4	věk
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
a	a	k8xC	a
galeriích	galerie	k1gFnPc6	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovené	zhotovený	k2eAgFnPc1d1	zhotovená
kopie	kopie	k1gFnPc1	kopie
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
reálných	reálný	k2eAgNnPc6d1	reálné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
Pisano	Pisana	k1gFnSc5	Pisana
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
relief	relief	k1gInSc4	relief
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
ze	z	k7c2	z
života	život	k1gInSc2	život
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
sehrála	sehrát	k5eAaPmAgFnS	sehrát
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
roli	role	k1gFnSc4	role
parléřovská	parléřovský	k2eAgFnSc1d1	parléřovská
stavební	stavební	k2eAgFnSc1d1	stavební
huť	huť	k1gFnSc1	huť
svatovítská	svatovítský	k2eAgFnSc1d1	Svatovítská
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgInSc1	první
sochařský	sochařský	k2eAgInSc1d1	sochařský
autoportrét	autoportrét	k1gInSc1	autoportrét
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Malířství	malířství	k1gNnSc2	malířství
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
gotické	gotický	k2eAgNnSc4d1	gotické
malířství	malířství	k1gNnSc4	malířství
je	být	k5eAaImIp3nS	být
příznačné	příznačný	k2eAgNnSc1d1	příznačné
řešení	řešení	k1gNnSc1	řešení
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
náboženskými	náboženský	k2eAgInPc7d1	náboženský
výjevy	výjev	k1gInPc7	výjev
a	a	k8xC	a
všedním	všední	k2eAgInSc7d1	všední
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
posvátného	posvátný	k2eAgNnSc2d1	posvátné
dění	dění	k1gNnSc2	dění
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
přístupnější	přístupný	k2eAgInSc1d2	přístupnější
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
přirozenější	přirozený	k2eAgFnSc3d2	přirozenější
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
realistické	realistický	k2eAgNnSc1d1	realistické
zachycení	zachycení	k1gNnSc1	zachycení
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
scénické	scénický	k2eAgInPc4d1	scénický
vývoje	vývoj	k1gInPc4	vývoj
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
náměty	námět	k1gInPc7	námět
nadále	nadále	k6eAd1	nadále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
devoční	devoční	k2eAgInPc4d1	devoční
a	a	k8xC	a
oltářní	oltářní	k2eAgInPc4d1	oltářní
obrazy	obraz	k1gInPc4	obraz
společně	společně	k6eAd1	společně
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
cykly	cyklus	k1gInPc7	cyklus
legend	legenda	k1gFnPc2	legenda
ze	z	k7c2	z
života	život	k1gInSc2	život
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
se	se	k3xPyFc4	se
však	však	k9	však
nově	nově	k6eAd1	nově
objevují	objevovat	k5eAaImIp3nP	objevovat
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
přírodní	přírodní	k2eAgInPc4d1	přírodní
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
roli	role	k1gFnSc6	role
atributů	atribut	k1gInPc2	atribut
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
osamostatnění	osamostatnění	k1gNnSc3	osamostatnění
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k9	tak
první	první	k4xOgNnPc1	první
zátiší	zátiší	k1gNnPc2	zátiší
a	a	k8xC	a
rané	raný	k2eAgFnSc2d1	raná
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
Útěku	útěk	k1gInSc2	útěk
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
světců	světec	k1gMnPc2	světec
získává	získávat	k5eAaImIp3nS	získávat
krajina	krajina	k1gFnSc1	krajina
postupně	postupně	k6eAd1	postupně
dominantní	dominantní	k2eAgInSc4d1	dominantní
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
objevují	objevovat	k5eAaImIp3nP	objevovat
zadavatelé	zadavatel	k1gMnPc1	zadavatel
zakázek	zakázka	k1gFnPc2	zakázka
(	(	kIx(	(
<g/>
donátoři	donátor	k1gMnPc1	donátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
získávají	získávat	k5eAaImIp3nP	získávat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
domácích	domácí	k2eAgInPc2d1	domácí
diptychů	diptych	k1gInPc2	diptych
dokonce	dokonce	k9	dokonce
rovnocenný	rovnocenný	k2eAgInSc4d1	rovnocenný
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
vybraným	vybraný	k2eAgMnSc7d1	vybraný
světcem	světec	k1gMnSc7	světec
či	či	k8xC	či
náboženským	náboženský	k2eAgInSc7d1	náboženský
výjevem	výjev	k1gInSc7	výjev
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
o	o	k7c4	o
předstupeň	předstupeň	k1gInSc4	předstupeň
budoucího	budoucí	k2eAgInSc2d1	budoucí
rozvoje	rozvoj	k1gInSc2	rozvoj
portrétního	portrétní	k2eAgNnSc2d1	portrétní
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
rané	raný	k2eAgFnSc2d1	raná
gotiky	gotika	k1gFnSc2	gotika
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
náboženská	náboženský	k2eAgFnSc1d1	náboženská
tematika	tematika	k1gFnSc1	tematika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
monumentální	monumentální	k2eAgNnSc4d1	monumentální
krajinné	krajinný	k2eAgNnSc4d1	krajinné
panoráma	panoráma	k1gNnSc4	panoráma
Ambrogia	Ambrogius	k1gMnSc2	Ambrogius
Lorenzettiho	Lorenzetti	k1gMnSc2	Lorenzetti
pro	pro	k7c4	pro
sienskou	sienský	k2eAgFnSc4d1	Sienská
radnici	radnice	k1gFnSc4	radnice
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
Dobrá	dobrá	k9	dobrá
vláda	vláda	k1gFnSc1	vláda
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
dvojportrét	dvojportrét	k1gInSc1	dvojportrét
manželů	manžel	k1gMnPc2	manžel
Arnolfiniových	Arnolfiniový	k2eAgFnPc2d1	Arnolfiniový
Jana	Jan	k1gMnSc2	Jan
van	vana	k1gFnPc2	vana
Eycka	Eycek	k1gMnSc2	Eycek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
centry	centr	k1gInPc7	centr
gotického	gotický	k2eAgNnSc2d1	gotické
malířství	malířství	k1gNnSc2	malířství
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
Duccio	Duccio	k1gMnSc1	Duccio
di	di	k?	di
Buoninsegna	Buoninsegn	k1gMnSc4	Buoninsegn
<g/>
,	,	kIx,	,
Cimabue	Cimabu	k1gMnSc4	Cimabu
<g/>
,	,	kIx,	,
Simone	Simon	k1gMnSc5	Simon
Martini	martini	k1gNnSc1	martini
<g/>
,	,	kIx,	,
Giotto	Giotto	k1gNnSc1	Giotto
di	di	k?	di
Bondone	Bondon	k1gMnSc5	Bondon
<g/>
,	,	kIx,	,
Bernardo	Bernardo	k1gNnSc4	Bernardo
Daddi	Dadd	k1gMnPc1	Dadd
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
di	di	k?	di
Bonaiuto	Bonaiut	k2eAgNnSc1d1	Bonaiut
da	da	k?	da
Firenze	Firenze	k1gFnSc1	Firenze
<g/>
,	,	kIx,	,
Pietro	Pietro	k1gNnSc1	Pietro
a	a	k8xC	a
Ambrogio	Ambrogio	k1gNnSc1	Ambrogio
Lorenzetti	Lorenzetti	k1gNnSc2	Lorenzetti
<g/>
,	,	kIx,	,
Orcagna	Orcagno	k1gNnSc2	Orcagno
<g/>
,	,	kIx,	,
Gentile	Gentil	k1gMnSc5	Gentil
da	da	k?	da
Fabriano	Fabriana	k1gFnSc5	Fabriana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc3	strana
Francie	Francie	k1gFnSc2	Francie
Bratři	bratr	k1gMnPc1	bratr
z	z	k7c2	z
Limburka	Limburek	k1gMnSc2	Limburek
a	a	k8xC	a
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
(	(	kIx(	(
<g/>
Melchior	Melchior	k1gInSc1	Melchior
Broederlam	Broederlam	k1gInSc1	Broederlam
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
van	vana	k1gFnPc2	vana
Eyck	Eyck	k1gMnSc1	Eyck
a	a	k8xC	a
Rogier	Rogier	k1gMnSc1	Rogier
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Weyden	Weydno	k1gNnPc2	Weydno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
následovníci	následovník	k1gMnPc1	následovník
již	již	k6eAd1	již
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
renesanční	renesanční	k2eAgFnSc2d1	renesanční
malby	malba	k1gFnSc2	malba
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
van	vana	k1gFnPc2	vana
Eyck	Eyck	k1gMnSc1	Eyck
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Memling	Memling	k1gInSc1	Memling
<g/>
,	,	kIx,	,
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
gotické	gotický	k2eAgFnSc2d1	gotická
malby	malba	k1gFnSc2	malba
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tvořili	tvořit	k5eAaImAgMnP	tvořit
naši	náš	k3xOp1gMnPc1	náš
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
umělci	umělec	k1gMnPc1	umělec
(	(	kIx(	(
<g/>
Mistr	mistr	k1gMnSc1	mistr
třeboňského	třeboňský	k2eAgInSc2d1	třeboňský
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
vyšebrodského	vyšebrodský	k2eAgInSc2d1	vyšebrodský
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
Rajhradského	rajhradský	k2eAgInSc2d1	rajhradský
(	(	kIx(	(
<g/>
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
<g/>
)	)	kIx)	)
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
Theodorik	Theodorik	k1gMnSc1	Theodorik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gotický	gotický	k2eAgInSc4d1	gotický
sloh	sloh	k1gInSc4	sloh
pronikal	pronikat	k5eAaImAgMnS	pronikat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
knižní	knižní	k2eAgFnSc2d1	knižní
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
vzor	vzor	k1gInSc4	vzor
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
nástěnnou	nástěnný	k2eAgFnSc4d1	nástěnná
<g/>
.	.	kIx.	.
</s>
<s>
Nejranějšími	raný	k2eAgFnPc7d3	nejranější
památkami	památka	k1gFnPc7	památka
gotické	gotický	k2eAgFnSc2d1	gotická
knižní	knižní	k2eAgFnSc2d1	knižní
malby	malba	k1gFnSc2	malba
jsou	být	k5eAaImIp3nP	být
chorální	chorální	k2eAgFnPc1d1	chorální
knihy	kniha	k1gFnPc1	kniha
Alžběty	Alžběta	k1gFnSc2	Alžběta
Rejčky	Rejčka	k1gFnSc2	Rejčka
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1323	[number]	k4	1323
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pasionál	pasionál	k1gInSc1	pasionál
abatyše	abatyše	k1gFnSc2	abatyše
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
(	(	kIx(	(
<g/>
1313	[number]	k4	1313
<g/>
–	–	k?	–
<g/>
1321	[number]	k4	1321
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zdomácnění	zdomácnění	k1gNnSc6	zdomácnění
gotického	gotický	k2eAgInSc2d1	gotický
slohu	sloh	k1gInSc2	sloh
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
nástěnnou	nástěnný	k2eAgFnSc4d1	nástěnná
malbu	malba	k1gFnSc4	malba
i	i	k9	i
malba	malba	k1gFnSc1	malba
desková	deskový	k2eAgFnSc1d1	desková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrazy	obraz	k1gInPc4	obraz
deskové	deskový	k2eAgInPc4d1	deskový
se	se	k3xPyFc4	se
malovaly	malovat	k5eAaImAgInP	malovat
na	na	k7c4	na
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
desky	deska	k1gFnPc4	deska
složené	složený	k2eAgFnPc4d1	složená
z	z	k7c2	z
malých	malý	k2eAgInPc2d1	malý
přesně	přesně	k6eAd1	přesně
opracovaných	opracovaný	k2eAgInPc2d1	opracovaný
špalíčků	špalíček	k1gInPc2	špalíček
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
prokládaných	prokládaný	k2eAgMnPc2d1	prokládaný
a	a	k8xC	a
lepených	lepený	k2eAgMnPc2d1	lepený
do	do	k7c2	do
kříže	kříž	k1gInSc2	kříž
v	v	k7c6	v
několika	několik	k4yIc6	několik
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fresky	freska	k1gFnPc1	freska
(	(	kIx(	(
<g/>
obrazy	obraz	k1gInPc1	obraz
malované	malovaný	k2eAgInPc1d1	malovaný
do	do	k7c2	do
vlhké	vlhký	k2eAgFnSc2d1	vlhká
omítky	omítka	k1gFnSc2	omítka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
používané	používaný	k2eAgFnSc2d1	používaná
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přibývá	přibývat	k5eAaImIp3nS	přibývat
i	i	k9	i
světská	světský	k2eAgFnSc1d1	světská
tematika	tematika	k1gFnSc1	tematika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
život	život	k1gInSc4	život
krále	král	k1gMnSc2	král
či	či	k8xC	či
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
malby	malba	k1gFnSc2	malba
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
známe	znát	k5eAaImIp1nP	znát
nejpřesnější	přesný	k2eAgInSc4d3	nejpřesnější
obraz	obraz	k1gInSc4	obraz
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
bohužel	bohužel	k9	bohužel
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Kidson	Kidson	k1gMnSc1	Kidson
<g/>
:	:	kIx,	:
Románské	románský	k2eAgNnSc1d1	románské
a	a	k8xC	a
gotické	gotický	k2eAgNnSc1d1	gotické
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Artia	Artia	k1gFnSc1	Artia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Rolf	Rolf	k1gMnSc1	Rolf
Toman	Toman	k1gMnSc1	Toman
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Gotika	gotika	k1gFnSc1	gotika
–	–	k?	–
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
plastika	plastika	k1gFnSc1	plastika
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7209-668-0	[number]	k4	80-7209-668-0
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Dvořáček	Dvořáček	k1gMnSc1	Dvořáček
<g/>
:	:	kIx,	:
Gotika	gotika	k1gFnSc1	gotika
<g/>
,	,	kIx,	,
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
KMa	KMa	k1gFnSc1	KMa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-273-5	[number]	k4	80-7309-273-5
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jagellonská	jagellonský	k2eAgFnSc1d1	Jagellonská
gotika	gotika	k1gFnSc1	gotika
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
gotika	gotika	k1gFnSc1	gotika
</s>
</p>
<p>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
Novogotika	novogotika	k1gFnSc1	novogotika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gotika	gotik	k1gMnSc2	gotik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gotika	gotika	k1gFnSc1	gotika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Artmuseum	Artmuseum	k1gNnSc1	Artmuseum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Gotika	gotika	k1gFnSc1	gotika
</s>
</p>
<p>
<s>
GOTHICmed	GOTHICmed	k1gMnSc1	GOTHICmed
A	a	k8xC	a
Virtual	Virtual	k1gMnSc1	Virtual
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Mediterranean	Mediterranean	k1gMnSc1	Mediterranean
Gothic	Gothic	k1gMnSc1	Gothic
Architecture	Architectur	k1gInSc5	Architectur
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
gotika	gotika	k1gFnSc1	gotika
</s>
</p>
