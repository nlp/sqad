<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Macek	Macek	k1gMnSc1	Macek
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1887	[number]	k4	1887
Krumpach	Krumpacha	k1gFnPc2	Krumpacha
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1972	[number]	k4	1972
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
teoretik	teoretik	k1gMnSc1	teoretik
i	i	k8xC	i
aktivní	aktivní	k2eAgMnSc1d1	aktivní
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
poslanec	poslanec	k1gMnSc1	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Československou	československý	k2eAgFnSc4d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
studoval	studovat	k5eAaImAgInS	studovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	práv	k2eAgInSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
přednášky	přednáška	k1gFnPc4	přednáška
ekonoma	ekonom	k1gMnSc2	ekonom
Albína	Albín	k1gMnSc2	Albín
Bráfa	Bráf	k1gMnSc2	Bráf
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
žáka	žák	k1gMnSc2	žák
Josefa	Josef	k1gMnSc2	Josef
Grubera	Gruber	k1gMnSc2	Gruber
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
své	své	k1gNnSc4	své
znalosti	znalost	k1gFnSc2	znalost
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
německé	německý	k2eAgFnSc6d1	německá
filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
zimním	zimní	k2eAgInSc6d1	zimní
semestru	semestr	k1gInSc6	semestr
1913-1914	[number]	k4	1913-1914
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1911-1912	[number]	k4	1911-1912
procházel	procházet	k5eAaImAgMnS	procházet
soudní	soudní	k2eAgMnSc1d1	soudní
a	a	k8xC	a
advokátní	advokátní	k2eAgFnSc7d1	advokátní
praxí	praxe	k1gFnSc7	praxe
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1912-1918	[number]	k4	1912-1918
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
jako	jako	k9	jako
suplující	suplující	k2eAgMnSc1d1	suplující
profesor	profesor	k1gMnSc1	profesor
obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1921	[number]	k4	1918-1921
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
odborný	odborný	k2eAgMnSc1d1	odborný
rada	rada	k1gMnSc1	rada
v	v	k7c6	v
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
mimořádným	mimořádný	k2eAgInSc7d1	mimořádný
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
obchodní	obchodní	k2eAgFnSc6d1	obchodní
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
činnosti	činnost	k1gFnSc6	činnost
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgMnS	projevovat
jako	jako	k9	jako
vynikající	vynikající	k2eAgMnSc1d1	vynikající
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
řečník	řečník	k1gMnSc1	řečník
<g/>
.	.	kIx.	.
</s>
<s>
Sdílel	sdílet	k5eAaImAgInS	sdílet
řadu	řada	k1gFnSc4	řada
názorů	názor	k1gInPc2	názor
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
Albína	Albín	k1gMnSc2	Albín
Bráfa	Bráf	k1gMnSc2	Bráf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozcházel	rozcházet	k5eAaImAgMnS	rozcházet
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
na	na	k7c4	na
povahu	povaha	k1gFnSc4	povaha
stávajícího	stávající	k2eAgInSc2d1	stávající
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
charakter	charakter	k1gInSc1	charakter
nutných	nutný	k2eAgFnPc2d1	nutná
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
směr	směr	k1gInSc4	směr
transformace	transformace	k1gFnSc2	transformace
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
diskuzí	diskuze	k1gFnPc2	diskuze
o	o	k7c6	o
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
politice	politika	k1gFnSc6	politika
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
názorovým	názorový	k2eAgMnSc7d1	názorový
protivníkem	protivník	k1gMnSc7	protivník
významný	významný	k2eAgMnSc1d1	významný
národohospodář	národohospodář	k1gMnSc1	národohospodář
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
publikační	publikační	k2eAgFnSc2d1	publikační
činnosti	činnost	k1gFnSc2	činnost
nepatřila	patřit	k5eNaImAgFnS	patřit
ryze	ryze	k6eAd1	ryze
teoretickým	teoretický	k2eAgInPc3d1	teoretický
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
aktuálním	aktuální	k2eAgFnPc3d1	aktuální
otázkám	otázka	k1gFnPc3	otázka
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
brilantním	brilantní	k2eAgMnSc7d1	brilantní
řečníkem	řečník	k1gMnSc7	řečník
a	a	k8xC	a
velkým	velký	k2eAgMnSc7d1	velký
znalcem	znalec	k1gMnSc7	znalec
světové	světový	k2eAgFnSc2d1	světová
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
napsány	napsán	k2eAgMnPc4d1	napsán
krásným	krásný	k2eAgInSc7d1	krásný
literárním	literární	k2eAgInSc7d1	literární
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
učebnice	učebnice	k1gFnSc1	učebnice
z	z	k7c2	z
didaktického	didaktický	k2eAgNnSc2d1	didaktické
hlediska	hledisko	k1gNnSc2	hledisko
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
příkladem	příklad	k1gInSc7	příklad
i	i	k8xC	i
dnešním	dnešní	k2eAgMnPc3d1	dnešní
autorům	autor	k1gMnPc3	autor
<g/>
.	.	kIx.	.
<g/>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
aktivně	aktivně	k6eAd1	aktivně
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
myšlenkám	myšlenka	k1gFnPc3	myšlenka
Františka	František	k1gMnSc4	František
Modráčka	modráček	k1gMnSc4	modráček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přikládal	přikládat	k5eAaImAgMnS	přikládat
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Modráčkem	modráček	k1gMnSc7	modráček
se	se	k3xPyFc4	se
počátkem	počátek	k1gInSc7	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
spojil	spojit	k5eAaPmAgMnS	spojit
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
nové	nový	k2eAgFnSc2d1	nová
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
československého	československý	k2eAgInSc2d1	československý
lidu	lid	k1gInSc2	lid
pracujícího	pracující	k1gMnSc4	pracující
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
Dělnické	dělnický	k2eAgFnSc6d1	Dělnická
akademii	akademie	k1gFnSc6	akademie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgMnS	být
redaktorem	redaktor	k1gMnSc7	redaktor
revue	revue	k1gFnSc2	revue
Naše	náš	k3xOp1gFnSc1	náš
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
levicově	levicově	k6eAd1	levicově
liberální	liberální	k2eAgMnPc4d1	liberální
intelektuály	intelektuál	k1gMnPc4	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
debat	debata	k1gFnPc2	debata
okolo	okolo	k7c2	okolo
nového	nový	k2eAgInSc2d1	nový
programu	program	k1gInSc2	program
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
představoval	představovat	k5eAaImAgMnS	představovat
liberální	liberální	k2eAgNnSc4d1	liberální
křídlo	křídlo	k1gNnSc4	křídlo
kritické	kritický	k2eAgNnSc4d1	kritické
k	k	k7c3	k
postulátům	postulát	k1gInPc3	postulát
marxismu	marxismus	k1gInSc2	marxismus
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
získal	získat	k5eAaPmAgMnS	získat
poslanecké	poslanecký	k2eAgNnSc4d1	poslanecké
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
nabyl	nabýt	k5eAaPmAgInS	nabýt
ale	ale	k9	ale
až	až	k6eAd1	až
dodatečně	dodatečně	k6eAd1	dodatečně
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
jako	jako	k8xC	jako
náhradník	náhradník	k1gMnSc1	náhradník
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
poslanec	poslanec	k1gMnSc1	poslanec
Albín	Albín	k1gMnSc1	Albín
Chalupa	Chalupa	k1gMnSc1	Chalupa
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
i	i	k8xC	i
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
post	post	k1gInSc1	post
si	se	k3xPyFc3	se
oficiálně	oficiálně	k6eAd1	oficiálně
podržel	podržet	k5eAaPmAgMnS	podržet
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
parlamentu	parlament	k1gInSc2	parlament
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1938	[number]	k4	1938
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
v	v	k7c6	v
druhorepublikovém	druhorepublikový	k2eAgNnSc6d1	druhorepublikový
Česko-Slovensku	Česko-Slovensko	k1gNnSc6	Česko-Slovensko
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
nově	nově	k6eAd1	nově
utvořené	utvořený	k2eAgFnSc2d1	utvořená
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
vánočních	vánoční	k2eAgInPc6d1	vánoční
svátcích	svátek	k1gInPc6	svátek
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
Josef	Josef	k1gMnSc1	Josef
Macek	Macek	k1gMnSc1	Macek
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
přes	přes	k7c4	přes
Šumavu	Šumava	k1gFnSc4	Šumava
z	z	k7c2	z
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Kanadu	Kanada	k1gFnSc4	Kanada
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Pittsburghu	Pittsburgh	k1gInSc6	Pittsburgh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957-1961	[number]	k4	1957-1961
učil	učit	k5eAaImAgMnS	učit
na	na	k7c6	na
dívčí	dívčí	k2eAgFnSc6d1	dívčí
koleji	kolej	k1gFnSc6	kolej
Chathan	Chathana	k1gFnPc2	Chathana
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
krajanských	krajanský	k2eAgInPc6d1	krajanský
spolcích	spolek	k1gInPc6	spolek
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
za	za	k7c7	za
rodinou	rodina	k1gFnSc7	rodina
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Jiřího	Jiří	k1gMnSc2	Jiří
do	do	k7c2	do
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Cesty	cesta	k1gFnPc4	cesta
sebevzdělání	sebevzdělání	k1gNnSc2	sebevzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
91	[number]	k4	91
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josef	k1gMnSc1	Josef
Macek	Macek	k1gMnSc1	Macek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Macek	Macek	k1gMnSc1	Macek
(	(	kIx(	(
<g/>
sociálnědemokratický	sociálnědemokratický	k2eAgMnSc1d1	sociálnědemokratický
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Projev	projev	k1gInSc1	projev
Josefa	Josef	k1gMnSc2	Josef
Macka	Macek	k1gMnSc2	Macek
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
</s>
</p>
