<s>
Který	který	k3yRgMnSc1	který
československý	československý	k2eAgMnSc1d1	československý
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
poslanec	poslanec	k1gMnSc1	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Československou	československý	k2eAgFnSc4d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
zemřel	zemřít	k5eAaPmAgMnS	zemřít
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1972	[number]	k4	1972
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
?	?	kIx.	?
</s>
