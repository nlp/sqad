<s desamb="1">
Štaufský	Štaufský	k2eAgInSc1d1
v	v	k7c6
Basileji	Basilej	k1gFnSc6
Přemyslu	Přemysl	k1gMnSc3
Otakarovi	Otakar	k1gMnSc3*xF
I.	I.	kA
a	a	k8xC
Vladislavu	Vladislav	k1gMnSc6
Jindřichovi	Jindřich	k1gMnSc6
tři	tři	k4xCgFnPc4
listiny	listina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1
byly	být	k5eAaImAgFnP
zpečetěny	zpečetit	k5eAaPmNgFnP
zlatou	zlatý	k2eAgFnSc7d1
pečetí	pečeť	k1gFnSc7
(	(	kIx(
<g/>
bulou	bula	k1gFnSc7
<g/>
)	)	kIx)
sicilského	sicilský	k2eAgMnSc4d1
krále	král	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1212	#num#	k4
vystavil	vystavit	k5eAaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>