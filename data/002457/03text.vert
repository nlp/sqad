<s>
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
[	[	kIx(	[
<g/>
alˈ	alˈ	k?	alˈ
kaˈ	kaˈ	k?	kaˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
[	[	kIx(	[
<g/>
albér	albér	k1gMnSc1	albér
kami	kam	k1gFnSc2	kam
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1913	[number]	k4	1913
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
čelných	čelný	k2eAgMnPc2d1	čelný
představitelů	představitel	k1gMnPc2	představitel
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
taková	takový	k3xDgNnPc1	takový
označení	označení	k1gNnPc4	označení
odmítal	odmítat	k5eAaImAgInS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
nepřál	přát	k5eNaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
filosofa	filosof	k1gMnSc4	filosof
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pozdější	pozdní	k2eAgNnSc4d2	pozdější
filosofické	filosofický	k2eAgNnSc4d1	filosofické
myšlení	myšlení	k1gNnSc4	myšlení
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
francouzsko-španělského	francouzsko-španělský	k2eAgNnSc2d1	francouzsko-španělské
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Mondovi	Mond	k1gMnSc6	Mond
(	(	kIx(	(
<g/>
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
na	na	k7c4	na
Marně	marně	k6eAd1	marně
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
chudinské	chudinský	k2eAgFnSc6d1	chudinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
Camus	Camus	k1gMnSc1	Camus
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
názorové	názorový	k2eAgFnPc4d1	názorová
neshody	neshoda	k1gFnPc4	neshoda
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
několik	několik	k4yIc1	několik
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
literatuře	literatura	k1gFnSc6	literatura
-	-	kIx~	-
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
novinářem	novinář	k1gMnSc7	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
novinářská	novinářský	k2eAgFnSc1d1	novinářská
činnost	činnost	k1gFnSc1	činnost
vedla	vést	k5eAaImAgFnS	vést
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vyhoštění	vyhoštění	k1gNnSc3	vyhoštění
z	z	k7c2	z
Alžíru	Alžír	k1gInSc2	Alžír
<g/>
,	,	kIx,	,
Camus	Camus	k1gMnSc1	Camus
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
protifašistického	protifašistický	k2eAgNnSc2d1	protifašistické
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
hnutí	hnutí	k1gNnSc2	hnutí
odporu	odpor	k1gInSc2	odpor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
především	především	k9	především
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
ilegálním	ilegální	k2eAgInSc6d1	ilegální
časopise	časopis	k1gInSc6	časopis
Combat	Combat	k1gFnSc2	Combat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gMnSc7	její
nesmiřitelným	smiřitelný	k2eNgMnSc7d1	nesmiřitelný
kritikem	kritik	k1gMnSc7	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Giovanni	Giovanň	k1gMnSc6	Giovanň
Catelli	Catell	k1gMnSc6	Catell
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jednoho	jeden	k4xCgInSc2	jeden
úryvku	úryvek	k1gInSc2	úryvek
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jana	Jan	k1gMnSc2	Jan
Zábrany	zábrana	k1gFnSc2	zábrana
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Camus	Camus	k1gInSc4	Camus
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
KGB	KGB	kA	KGB
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Dmitrije	Dmitrije	k1gFnSc2	Dmitrije
Šepilova	Šepilův	k2eAgFnSc1d1	Šepilova
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Camus	Camus	k1gInSc1	Camus
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
odcizenosti	odcizenost	k1gFnSc2	odcizenost
a	a	k8xC	a
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
téma	téma	k1gNnSc4	téma
strachu	strach	k1gInSc2	strach
ze	z	k7c2	z
samoty	samota	k1gFnSc2	samota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
ztrátu	ztráta	k1gFnSc4	ztráta
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
nesmyslnosti	nesmyslnost	k1gFnSc6	nesmyslnost
a	a	k8xC	a
absurdnosti	absurdnost	k1gFnSc6	absurdnost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Étranger	Étranger	k1gMnSc1	Étranger
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
asi	asi	k9	asi
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
část	část	k1gFnSc4	část
dějovou	dějový	k2eAgFnSc7d1	dějová
a	a	k8xC	a
úvahovou	úvahový	k2eAgFnSc7d1	úvahová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
dějová	dějový	k2eAgFnSc1d1	dějová
část	část	k1gFnSc1	část
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
vyprávěna	vyprávět	k5eAaImNgFnS	vyprávět
v	v	k7c6	v
ich-formě	ichorma	k1gFnSc6	ich-forma
a	a	k8xC	a
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
poslední	poslední	k2eAgFnSc4d1	poslední
část	část	k1gFnSc4	část
života	život	k1gInSc2	život
úředníka	úředník	k1gMnSc2	úředník
Meursaulta	Meursault	k1gMnSc2	Meursault
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
pohřbem	pohřeb	k1gInSc7	pohřeb
Meursaultovy	Meursaultův	k2eAgFnSc2d1	Meursaultův
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
matku	matka	k1gFnSc4	matka
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
necítí	cítit	k5eNaImIp3nS	cítit
lítost	lítost	k1gFnSc4	lítost
<g/>
,	,	kIx,	,
smířil	smířit	k5eAaPmAgInS	smířit
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
musí	muset	k5eAaImIp3nS	muset
někdy	někdy	k6eAd1	někdy
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Bere	brát	k5eAaImIp3nS	brát
život	život	k1gInSc4	život
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
nenechává	nechávat	k5eNaImIp3nS	nechávat
se	se	k3xPyFc4	se
vyvést	vyvést	k5eAaPmF	vyvést
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
pohřbu	pohřeb	k1gInSc6	pohřeb
naváže	navázat	k5eAaPmIp3nS	navázat
kontakt	kontakt	k1gInSc1	kontakt
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
kolegyní	kolegyně	k1gFnSc7	kolegyně
Marií	Maria	k1gFnPc2	Maria
a	a	k8xC	a
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	se	k3xPyFc4	se
se	s	k7c7	s
sousedem	soused	k1gMnSc7	soused
pochybného	pochybný	k2eAgInSc2d1	pochybný
charakteru	charakter	k1gInSc2	charakter
Raymondem	Raymond	k1gMnSc7	Raymond
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
Raymondovi	Raymond	k1gMnSc3	Raymond
napíše	napsat	k5eAaBmIp3nS	napsat
dopis	dopis	k1gInSc4	dopis
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
nevěrnou	věrný	k2eNgFnSc7d1	nevěrná
milenkou	milenka	k1gFnSc7	milenka
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Raymond	Raymond	k1gMnSc1	Raymond
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
partou	parta	k1gFnSc7	parta
Arabů	Arab	k1gMnPc2	Arab
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
party	parta	k1gFnSc2	parta
je	být	k5eAaImIp3nS	být
bratrem	bratr	k1gMnSc7	bratr
oné	onen	k3xDgFnSc2	onen
dívky	dívka	k1gFnSc2	dívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meursault	Meursault	k1gInSc1	Meursault
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
a	a	k8xC	a
Raymond	Raymond	k1gInSc1	Raymond
odcestují	odcestovat	k5eAaPmIp3nP	odcestovat
na	na	k7c4	na
chatu	chata	k1gFnSc4	chata
Raymondova	Raymondův	k2eAgMnSc2d1	Raymondův
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Procházejí	procházet	k5eAaImIp3nP	procházet
se	se	k3xPyFc4	se
po	po	k7c6	po
pláži	pláž	k1gFnSc6	pláž
<g/>
,	,	kIx,	,
Raymonda	Raymond	k1gMnSc4	Raymond
napadnou	napadnout	k5eAaPmIp3nP	napadnout
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
zasadí	zasadit	k5eAaPmIp3nP	zasadit
mu	on	k3xPp3gMnSc3	on
ránu	rána	k1gFnSc4	rána
nožem	nůž	k1gInSc7	nůž
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
prchají	prchat	k5eAaImIp3nP	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Meursault	Meursault	k1gInSc1	Meursault
se	se	k3xPyFc4	se
prochází	procházet	k5eAaImIp3nS	procházet
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
uvidí	uvidět	k5eAaPmIp3nP	uvidět
mezi	mezi	k7c7	mezi
skalisky	skalisko	k1gNnPc7	skalisko
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
útočníků	útočník	k1gMnPc2	útočník
<g/>
.	.	kIx.	.
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
horko	horko	k1gNnSc4	horko
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
dostane	dostat	k5eAaPmIp3nS	dostat
úpal	úpal	k1gInSc4	úpal
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
stínu	stín	k1gInSc2	stín
skalisek	skalisko	k1gNnPc2	skalisko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
ale	ale	k9	ale
nachází	nacházet	k5eAaImIp3nS	nacházet
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Meursault	Meursault	k1gMnSc1	Meursault
váhá	váhat	k5eAaImIp3nS	váhat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
k	k	k7c3	k
Arabovi	Arab	k1gMnSc3	Arab
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
Arab	Arab	k1gMnSc1	Arab
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
nůž	nůž	k1gInSc4	nůž
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
ho	on	k3xPp3gNnSc4	on
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
zbraní	zbraň	k1gFnPc2	zbraň
od	od	k7c2	od
Raymonda	Raymond	k1gMnSc2	Raymond
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
uchvácen	uchvácen	k2eAgMnSc1d1	uchvácen
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
do	do	k7c2	do
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
Araba	Arab	k1gMnSc2	Arab
další	další	k2eAgFnPc1d1	další
čtyři	čtyři	k4xCgFnPc1	čtyři
kulky	kulka	k1gFnPc1	kulka
lehce	lehko	k6eAd1	lehko
vnikají	vnikat	k5eAaImIp3nP	vnikat
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
úvahová	úvahový	k2eAgFnSc1d1	úvahová
část	část	k1gFnSc1	část
<g/>
:	:	kIx,	:
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
Meursault	Meursault	k1gInSc1	Meursault
zatčen	zatčen	k2eAgInSc1d1	zatčen
<g/>
,	,	kIx,	,
vyslýchán	vyslýchán	k2eAgInSc1d1	vyslýchán
<g/>
,	,	kIx,	,
vidíme	vidět	k5eAaImIp1nP	vidět
jeho	on	k3xPp3gInSc4	on
vězeňský	vězeňský	k2eAgInSc4d1	vězeňský
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
výslechy	výslech	k1gInPc4	výslech
i	i	k8xC	i
soudní	soudní	k2eAgInSc4d1	soudní
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
popravu	poprava	k1gFnSc4	poprava
<g/>
,	,	kIx,	,
rekapituluje	rekapitulovat	k5eAaBmIp3nS	rekapitulovat
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
a	a	k8xC	a
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Nechápe	chápat	k5eNaImIp3nS	chápat
absurdní	absurdní	k2eAgInSc1d1	absurdní
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
člověka	člověk	k1gMnSc4	člověk
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
jen	jen	k9	jen
z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
a	a	k8xC	a
ne	ne	k9	ne
podle	podle	k7c2	podle
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
pohnutek	pohnutka	k1gFnPc2	pohnutka
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jej	on	k3xPp3gNnSc4	on
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
bezcitného	bezcitný	k2eAgMnSc2d1	bezcitný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neplakal	plakat	k5eNaImAgMnS	plakat
na	na	k7c6	na
matčině	matčin	k2eAgInSc6d1	matčin
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
hned	hned	k6eAd1	hned
šel	jít	k5eAaImAgMnS	jít
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
navíc	navíc	k6eAd1	navíc
zabil	zabít	k5eAaPmAgMnS	zabít
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
orientovat	orientovat	k5eAaBmF	orientovat
se	se	k3xPyFc4	se
v	v	k7c6	v
hodnotovém	hodnotový	k2eAgInSc6d1	hodnotový
řádu	řád	k1gInSc6	řád
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
cizincem	cizinec	k1gMnSc7	cizinec
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
kolektivu	kolektiv	k1gInSc6	kolektiv
i	i	k8xC	i
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
lhostejný	lhostejný	k2eAgInSc1d1	lhostejný
<g/>
.	.	kIx.	.
</s>
<s>
Nevěří	věřit	k5eNaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c4	v
jedinou	jediný	k2eAgFnSc4d1	jediná
životní	životní	k2eAgFnSc4d1	životní
jistotu	jistota	k1gFnSc4	jistota
-	-	kIx~	-
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
jednání	jednání	k1gNnSc2	jednání
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vinu	vina	k1gFnSc4	vina
na	na	k7c6	na
současných	současný	k2eAgInPc6d1	současný
poměrech	poměr	k1gInPc6	poměr
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
,	,	kIx,	,
mravnosti	mravnost	k1gFnSc6	mravnost
ani	ani	k8xC	ani
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
důležitým	důležitý	k2eAgInSc7d1	důležitý
jevem	jev	k1gInSc7	jev
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
také	také	k9	také
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
právě	právě	k9	právě
ve	v	k7c6	v
chvílích	chvíle	k1gFnPc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svítí	svítit	k5eAaImIp3nS	svítit
slunce	slunce	k1gNnSc4	slunce
se	se	k3xPyFc4	se
Meursault	Meursault	k1gMnSc1	Meursault
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
nepříjemných	příjemný	k2eNgFnPc2d1	nepříjemná
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
povšimnout	povšimnout	k5eAaPmF	povšimnout
během	během	k7c2	během
pohřbu	pohřeb	k1gInSc2	pohřeb
Meursaultovy	Meursaultův	k2eAgFnSc2d1	Meursaultův
matky	matka	k1gFnSc2	matka
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
zabije	zabít	k5eAaPmIp3nS	zabít
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
konci	konec	k1gInSc6	konec
během	během	k7c2	během
Meursaultova	Meursaultův	k2eAgInSc2d1	Meursaultův
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
La	la	k0	la
Peste	Pest	k1gMnSc5	Pest
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
románovou	románový	k2eAgFnSc7d1	románová
kronikou	kronika	k1gFnSc7	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
alžírském	alžírský	k2eAgNnSc6d1	alžírské
městě	město	k1gNnSc6	město
Oranu	Oran	k1gInSc6	Oran
zamořeném	zamořený	k2eAgInSc6d1	zamořený
morem	mor	k1gInSc7	mor
<g/>
.	.	kIx.	.
</s>
<s>
Kroniku	kronika	k1gFnSc4	kronika
píše	psát	k5eAaImIp3nS	psát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hrdinů	hrdina	k1gMnPc2	hrdina
(	(	kIx(	(
<g/>
lékař	lékař	k1gMnSc1	lékař
Bernard	Bernard	k1gMnSc1	Bernard
Rieux	Rieux	k1gInSc1	Rieux
<g/>
)	)	kIx)	)
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zápisky	zápisek	k1gInPc1	zápisek
intelektuála	intelektuál	k1gMnSc2	intelektuál
Tarraoua	Tarraouus	k1gMnSc2	Tarraouus
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
Mor	mora	k1gFnPc2	mora
je	být	k5eAaImIp3nS	být
čtenářsky	čtenářsky	k6eAd1	čtenářsky
náročné	náročný	k2eAgNnSc1d1	náročné
dílo	dílo	k1gNnSc1	dílo
se	s	k7c7	s
zřetelnou	zřetelný	k2eAgFnSc7d1	zřetelná
filozofickou	filozofický	k2eAgFnSc7d1	filozofická
rovinou	rovina	k1gFnSc7	rovina
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
-	-	kIx~	-
vzpourou	vzpoura	k1gFnSc7	vzpoura
<g/>
,	,	kIx,	,
bojem	boj	k1gInSc7	boj
<g/>
,	,	kIx,	,
vzepřením	vzepření	k1gNnSc7	vzepření
se	se	k3xPyFc4	se
neradostnému	radostný	k2eNgInSc3d1	neradostný
lidskému	lidský	k2eAgInSc3d1	lidský
údělu	úděl	k1gInSc3	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Tragédie	tragédie	k1gFnSc1	tragédie
začne	začít	k5eAaPmIp3nS	začít
hromadným	hromadný	k2eAgInSc7d1	hromadný
úhynem	úhyn	k1gInSc7	úhyn
krys	krysa	k1gFnPc2	krysa
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
;	;	kIx,	;
lidské	lidský	k2eAgFnPc1d1	lidská
oběti	oběť	k1gFnPc1	oběť
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechají	nechat	k5eNaPmIp3nP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
nechce	chtít	k5eNaImIp3nS	chtít
nikdo	nikdo	k3yNnSc1	nikdo
uvěřit	uvěřit	k5eAaPmF	uvěřit
varování	varování	k1gNnSc4	varování
doktora	doktor	k1gMnSc2	doktor
Rieuxe	Rieuxe	k1gFnSc2	Rieuxe
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
záhy	záhy	k6eAd1	záhy
odhalí	odhalit	k5eAaPmIp3nS	odhalit
velké	velký	k2eAgNnSc4d1	velké
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
po	po	k7c6	po
starostovi	starosta	k1gMnSc3	starosta
zavedení	zavedení	k1gNnSc2	zavedení
tvrdých	tvrdý	k2eAgNnPc2d1	tvrdé
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
včetně	včetně	k7c2	včetně
karantény	karanténa	k1gFnSc2	karanténa
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
je	být	k5eAaImIp3nS	být
však	však	k9	však
starosta	starosta	k1gMnSc1	starosta
nucen	nutit	k5eAaImNgMnS	nutit
uznat	uznat	k5eAaPmF	uznat
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
Rieuxových	Rieuxový	k2eAgInPc2d1	Rieuxový
požadavků	požadavek	k1gInPc2	požadavek
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
hermeticky	hermeticky	k6eAd1	hermeticky
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
mikrosvět	mikrosvět	k1gInSc4	mikrosvět
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
dohlíží	dohlížet	k5eAaImIp3nP	dohlížet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
oploceného	oplocený	k2eAgNnSc2d1	oplocené
města	město	k1gNnSc2	město
nikdo	nikdo	k3yNnSc1	nikdo
nepronikl	proniknout	k5eNaPmAgInS	proniknout
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
natolik	natolik	k6eAd1	natolik
vážnou	vážný	k2eAgFnSc4d1	vážná
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
pokusí	pokusit	k5eAaPmIp3nS	pokusit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Dovnitř	dovnitř	k6eAd1	dovnitř
proudí	proudit	k5eAaImIp3nP	proudit
pouze	pouze	k6eAd1	pouze
zásoby	zásoba	k1gFnPc4	zásoba
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
zdravotnický	zdravotnický	k2eAgInSc4d1	zdravotnický
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
zhoubnou	zhoubný	k2eAgFnSc7d1	zhoubná
nákazou	nákaza	k1gFnSc7	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
panika	panika	k1gFnSc1	panika
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c6	v
odhodlání	odhodlání	k1gNnSc6	odhodlání
postavit	postavit	k5eAaPmF	postavit
se	s	k7c7	s
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvých	mrtvý	k1gMnPc2	mrtvý
ale	ale	k8xC	ale
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
,	,	kIx,	,
provizorní	provizorní	k2eAgInPc1d1	provizorní
hřbitovy	hřbitov	k1gInPc1	hřbitov
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
<g/>
,	,	kIx,	,
protilátky	protilátka	k1gFnPc1	protilátka
neúčinkují	účinkovat	k5eNaImIp3nP	účinkovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
síly	síla	k1gFnPc1	síla
docházejí	docházet	k5eAaImIp3nP	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
(	(	kIx(	(
<g/>
La	la	k0	la
Chute	Chut	k1gMnSc5	Chut
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
-	-	kIx~	-
dílo	dílo	k1gNnSc1	dílo
původně	původně	k6eAd1	původně
plánováno	plánovat	k5eAaImNgNnS	plánovat
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsahu	rozsah	k1gInSc3	rozsah
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
Camus	Camus	k1gInSc4	Camus
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
publikovat	publikovat	k5eAaBmF	publikovat
samostatně	samostatně	k6eAd1	samostatně
jako	jako	k8xC	jako
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
exil	exil	k1gInSc1	exil
et	et	k?	et
le	le	k?	le
royaume	royaum	k1gInSc5	royaum
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
-	-	kIx~	-
povídková	povídkový	k2eAgFnSc1d1	povídková
kniha	kniha	k1gFnSc1	kniha
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Mort	Mortum	k1gNnPc2	Mortum
heureuse	heureuse	k6eAd1	heureuse
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
publikováno	publikovat	k5eAaBmNgNnS	publikovat
posmrtně	posmrtně	k6eAd1	posmrtně
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
První	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
premier	premier	k1gMnSc1	premier
homme	hommat	k5eAaPmIp3nS	hommat
<g/>
,	,	kIx,	,
nedokončeno	dokončit	k5eNaPmNgNnS	dokončit
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
fragment	fragment	k1gInSc1	fragment
textu	text	k1gInSc2	text
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
autonehody	autonehoda	k1gFnSc2	autonehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
Camus	Camus	k1gMnSc1	Camus
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
především	především	k9	především
román	román	k1gInSc1	román
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
témata	téma	k1gNnPc4	téma
alžírské	alžírský	k2eAgFnSc2d1	alžírská
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
alžírský	alžírský	k2eAgMnSc1d1	alžírský
Francouz	Francouz	k1gMnSc1	Francouz
Jacques	Jacques	k1gMnSc1	Jacques
Cormery	Cormera	k1gFnSc2	Cormera
<g/>
,	,	kIx,	,
<g/>
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
navštívit	navštívit	k5eAaPmF	navštívit
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
a	a	k8xC	a
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Camus	Camus	k1gInSc1	Camus
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
rozvinout	rozvinout	k5eAaPmF	rozvinout
téma	téma	k1gNnSc4	téma
nespravedlnosti	nespravedlnost	k1gFnSc2	nespravedlnost
a	a	k8xC	a
násilí	násilí	k1gNnSc2	násilí
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
vztahu	vztah	k1gInSc6	vztah
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vlasti	vlast	k1gFnSc3	vlast
a	a	k8xC	a
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
Camusovy	Camusův	k2eAgFnSc2d1	Camusova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
nad	nad	k7c7	nad
otázkami	otázka	k1gFnPc7	otázka
smyslu	smysl	k1gInSc2	smysl
života	život	k1gInSc2	život
a	a	k8xC	a
ztráty	ztráta	k1gFnSc2	ztráta
víry	víra	k1gFnSc2	víra
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
se	se	k3xPyFc4	se
k	k	k7c3	k
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaBmF	nalézt
příčiny	příčina	k1gFnPc4	příčina
nesvárů	nesvár	k1gInPc2	nesvár
mezi	mezi	k7c7	mezi
Araby	Arab	k1gMnPc7	Arab
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
znakem	znak	k1gInSc7	znak
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
podrobné	podrobný	k2eAgNnSc1d1	podrobné
a	a	k8xC	a
realistické	realistický	k2eAgNnSc1d1	realistické
líčení	líčení	k1gNnSc1	líčení
Cormeryho	Cormery	k1gMnSc2	Cormery
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vlastně	vlastně	k9	vlastně
Camusova	Camusův	k2eAgNnSc2d1	Camusovo
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Caligula	Caligula	k1gFnSc1	Caligula
(	(	kIx(	(
<g/>
Caligula	Caligula	k1gFnSc1	Caligula
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaBmNgNnS	napsat
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
vydáno	vydat	k5eAaPmNgNnS	vydat
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
autorovo	autorův	k2eAgNnSc4d1	autorovo
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
o	o	k7c6	o
římském	římský	k2eAgInSc6d1	římský
císaři	císař	k1gMnSc3	císař
pojal	pojmout	k5eAaPmAgMnS	pojmout
opět	opět	k6eAd1	opět
netradičně	tradičně	k6eNd1	tradičně
<g/>
,	,	kIx,	,
Caligulu	Caligula	k1gFnSc4	Caligula
pojímá	pojímat	k5eAaImIp3nS	pojímat
jako	jako	k8xC	jako
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
revoltuje	revoltovat	k5eAaImIp3nS	revoltovat
proti	proti	k7c3	proti
absurditě	absurdita	k1gFnSc3	absurdita
pomocí	pomoc	k1gFnPc2	pomoc
zločinů	zločin	k1gInPc2	zločin
a	a	k8xC	a
her	hra	k1gFnPc2	hra
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nebere	brát	k5eNaImIp3nS	brát
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
blázna	blázen	k1gMnSc4	blázen
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Caligulovi	Caligul	k1gMnSc3	Caligul
vzniká	vznikat	k5eAaImIp3nS	vznikat
vzpoura	vzpoura	k1gFnSc1	vzpoura
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
však	však	k9	však
směje	smát	k5eAaImIp3nS	smát
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
proti	proti	k7c3	proti
diktátorským	diktátorský	k2eAgInPc3d1	diktátorský
režimům	režim	k1gInPc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Nedorozumění	nedorozumění	k1gNnSc1	nedorozumění
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Malentendu	Malentend	k1gInSc2	Malentend
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
penzionu	penzion	k1gInSc6	penzion
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nikdo	nikdo	k3yNnSc1	nikdo
nechodí	chodit	k5eNaImIp3nS	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
penzionu	penzion	k1gInSc2	penzion
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
záhadný	záhadný	k2eAgMnSc1d1	záhadný
host	host	k1gMnSc1	host
z	z	k7c2	z
koloniální	koloniální	k2eAgFnSc2d1	koloniální
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
penzionu	penzion	k1gInSc6	penzion
hosta	host	k1gMnSc4	host
otráví	otrávit	k5eAaPmIp3nP	otrávit
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
hodí	hodit	k5eAaImIp3nS	hodit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
zmocnit	zmocnit	k5eAaPmF	zmocnit
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
"	"	kIx"	"
<g/>
šedi	šeď	k1gFnSc2	šeď
do	do	k7c2	do
slunce	slunce	k1gNnSc2	slunce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
Camusův	Camusův	k2eAgInSc1d1	Camusův
špatný	špatný	k2eAgInSc1d1	špatný
pocit	pocit	k1gInSc1	pocit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
chmurné	chmurný	k2eAgFnPc1d1	chmurná
oproti	oproti	k7c3	oproti
veselému	veselý	k2eAgNnSc3d1	veselé
a	a	k8xC	a
slunečnému	slunečný	k2eAgNnSc3d1	slunečné
mořskému	mořský	k2eAgNnSc3d1	mořské
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
,	,	kIx,	,
koho	kdo	k3yQnSc4	kdo
chtěli	chtít	k5eAaImAgMnP	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
dramatu	drama	k1gNnSc2	drama
<g/>
:	:	kIx,	:
Ráno	ráno	k6eAd1	ráno
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
penzionu	penzion	k1gInSc2	penzion
Maria	Maria	k1gFnSc1	Maria
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
onoho	onen	k3xDgMnSc2	onen
"	"	kIx"	"
<g/>
cizince	cizinec	k1gMnSc2	cizinec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc4	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Marii	Maria	k1gFnSc3	Maria
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
člena	člen	k1gMnSc4	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neodhalil	odhalit	k5eNaPmAgMnS	odhalit
svoji	svůj	k3xOyFgFnSc4	svůj
totožnost	totožnost	k1gFnSc4	totožnost
hned	hned	k6eAd1	hned
při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
(	(	kIx(	(
<g/>
a	a	k8xC	a
proto	proto	k8xC	proto
i	i	k9	i
Marie	Marie	k1gFnSc1	Marie
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
až	až	k9	až
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Jan	Jan	k1gMnSc1	Jan
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
být	být	k5eAaImF	být
tam	tam	k6eAd1	tam
první	první	k4xOgFnSc1	první
noc	noc	k1gFnSc1	noc
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
neprozradit	prozradit	k5eNaPmF	prozradit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
svoji	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
pozorovat	pozorovat	k5eAaImF	pozorovat
"	"	kIx"	"
<g/>
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
"	"	kIx"	"
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
skutečně	skutečně	k6eAd1	skutečně
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
od	od	k7c2	od
večera	večer	k1gInSc2	večer
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
obležení	obležení	k1gNnSc2	obležení
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
Etat	Etat	k1gMnSc1	Etat
de	de	k?	de
Siege	Siege	k1gInSc1	Siege
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Spravedliví	spravedlivý	k2eAgMnPc1d1	spravedlivý
(	(	kIx(	(
<g/>
Les	les	k1gInSc4	les
Justes	Justesa	k1gFnPc2	Justesa
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Rekviem	rekviem	k1gNnSc1	rekviem
za	za	k7c4	za
jeptišku	jeptiška	k1gFnSc4	jeptiška
(	(	kIx(	(
<g/>
Requiem	Requium	k1gNnSc7	Requium
pour	pour	k1gMnSc1	pour
une	une	k?	une
nonne	nonnout	k5eAaImIp3nS	nonnout
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
-	-	kIx~	-
dramatizace	dramatizace	k1gFnSc1	dramatizace
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Williama	William	k1gMnSc4	William
Faulknera	Faulkner	k1gMnSc4	Faulkner
<g/>
.	.	kIx.	.
</s>
<s>
Běsi	běs	k1gMnPc1	běs
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Possédés	Possédésa	k1gFnPc2	Possédésa
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
-	-	kIx~	-
dramatizace	dramatizace	k1gFnSc1	dramatizace
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Fjodora	Fjodor	k1gMnSc2	Fjodor
Michajloviče	Michajlovič	k1gMnSc2	Michajlovič
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gInSc4	Camus
napsal	napsat	k5eAaBmAgMnS	napsat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
esejů	esej	k1gInPc2	esej
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
filosoficky	filosoficky	k6eAd1	filosoficky
laděna	ladit	k5eAaImNgFnS	ladit
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
více	hodně	k6eAd2	hodně
beletristickou	beletristický	k2eAgFnSc4d1	beletristická
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
Sisyfovi	Sisyfos	k1gMnSc6	Sisyfos
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc2	Le
Mythe	mythus	k1gInSc5	mythus
de	de	k?	de
Sisyphe	Sisyphe	k1gNnPc7	Sisyphe
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
antický	antický	k2eAgInSc1d1	antický
mýtus	mýtus	k1gInSc4	mýtus
o	o	k7c6	o
Sisyfovi	Sisyfos	k1gMnSc6	Sisyfos
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pojal	pojmout	k5eAaPmAgMnS	pojmout
netradičně	tradičně	k6eNd1	tradičně
-	-	kIx~	-
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Sisyfa	Sisyfos	k1gMnSc2	Sisyfos
<g/>
.	.	kIx.	.
</s>
<s>
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
šťasten	šťasten	k2eAgMnSc1d1	šťasten
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
osud	osud	k1gInSc1	osud
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xC	jako
osud	osud	k1gInSc1	osud
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
on	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
marnost	marnost	k1gFnSc1	marnost
své	svůj	k3xOyFgFnSc2	svůj
snahy	snaha	k1gFnSc2	snaha
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
pánem	pán	k1gMnSc7	pán
<g/>
"	"	kIx"	"
tohoto	tento	k3xDgMnSc4	tento
svého	svůj	k1gMnSc4	svůj
neodvratného	odvratný	k2eNgInSc2d1	neodvratný
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
marné	marný	k2eAgFnSc2d1	marná
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
svobodnější	svobodný	k2eAgNnSc1d2	svobodnější
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
postihuje	postihovat	k5eAaImIp3nS	postihovat
úděl	úděl	k1gInSc1	úděl
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
absurditě	absurdita	k1gFnSc6	absurdita
a	a	k8xC	a
absurdno	absurdno	k1gNnSc1	absurdno
povyšuje	povyšovat	k5eAaImIp3nS	povyšovat
na	na	k7c4	na
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východisek	východisko	k1gNnPc2	východisko
beznaděje	beznaděj	k1gFnSc2	beznaděj
<g/>
,	,	kIx,	,
zbytečnosti	zbytečnost	k1gFnSc2	zbytečnost
a	a	k8xC	a
lhostejnosti	lhostejnost	k1gFnSc2	lhostejnost
dospívá	dospívat	k5eAaImIp3nS	dospívat
argumentací	argumentace	k1gFnSc7	argumentace
k	k	k7c3	k
pozitivním	pozitivní	k2eAgInPc3d1	pozitivní
závěrům	závěr	k1gInPc3	závěr
<g/>
:	:	kIx,	:
pomocí	pomocí	k7c2	pomocí
absurdních	absurdní	k2eAgInPc2d1	absurdní
prožitků	prožitek	k1gInPc2	prožitek
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
člověku	člověk	k1gMnSc3	člověk
dál	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
revoltující	revoltující	k2eAgMnSc1d1	revoltující
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gInSc5	Homm
révolté	révoltý	k2eAgInPc1d1	révoltý
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
uznával	uznávat	k5eAaImAgInS	uznávat
revoltujícího	revoltující	k2eAgMnSc4d1	revoltující
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vzpoura	vzpoura	k1gFnSc1	vzpoura
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
utrpení	utrpení	k1gNnSc3	utrpení
a	a	k8xC	a
zlu	zlo	k1gNnSc3	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
revoltou	revolta	k1gFnSc7	revolta
člověk	člověk	k1gMnSc1	člověk
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
svoji	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
esej	esej	k1gInSc1	esej
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
odmítnutí	odmítnutí	k1gNnPc4	odmítnutí
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gInSc2	jeho
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Sartrem	Sartrum	k1gNnSc7	Sartrum
<g/>
.	.	kIx.	.
</s>
<s>
Zápisníky	zápisník	k1gInPc1	zápisník
I.	I.	kA	I.
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
1935	[number]	k4	1935
-	-	kIx~	-
únor	únor	k1gInSc1	únor
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Carnets	Carnets	k1gInSc1	Carnets
I	I	kA	I
<g/>
,	,	kIx,	,
mai	mai	k?	mai
1935	[number]	k4	1935
<g/>
-février	évrier	k1gInSc1	-février
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Zápisníky	zápisník	k1gInPc1	zápisník
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1942	[number]	k4	1942
-	-	kIx~	-
březen	březen	k1gInSc1	březen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Carnets	Carnets	k1gInSc1	Carnets
II	II	kA	II
<g/>
,	,	kIx,	,
janvier	janvier	k1gInSc1	janvier
1942	[number]	k4	1942
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-mars	ars	k1gInSc1	-mars
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Zápisníky	zápisník	k1gInPc1	zápisník
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1951	[number]	k4	1951
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Carnets	Carnets	k1gInSc1	Carnets
III	III	kA	III
<g/>
,	,	kIx,	,
mars	mars	k6eAd1	mars
1951	[number]	k4	1951
<g/>
-décembre	écembr	k1gInSc5	-décembr
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
Cizinec	cizinec	k1gMnSc1	cizinec
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
in	in	k?	in
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
a	a	k8xC	a
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
in	in	k?	in
Romány	román	k1gInPc1	román
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
Milena	Milena	k1gFnSc1	Milena
Tomášková	Tomášková	k1gFnSc1	Tomášková
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
a	a	k8xC	a
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc1	království
Cizinec	cizinec	k1gMnSc1	cizinec
/	/	kIx~	/
L	L	kA	L
<g/>
́	́	k?	́
étranger	étranger	k1gInSc1	étranger
<g/>
,	,	kIx,	,
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
dvojjazyčné	dvojjazyčný	k2eAgNnSc1d1	dvojjazyčné
komentované	komentovaný	k2eAgNnSc1d1	komentované
vydání	vydání	k1gNnSc1	vydání
Mor	mora	k1gFnPc2	mora
Mor	mora	k1gFnPc2	mora
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Milena	Milena	k1gFnSc1	Milena
Tomášková	Tomášková	k1gFnSc1	Tomášková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
a	a	k8xC	a
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
in	in	k?	in
Romány	román	k1gInPc1	román
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
Milena	Milena	k1gFnSc1	Milena
Tomášková	Tomášková	k1gFnSc1	Tomášková
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
a	a	k8xC	a
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc1	království
Pád	Pád	k1gInSc1	Pád
in	in	k?	in
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g />
.	.	kIx.	.
</s>
<s>
Žilina	Žilina	k1gFnSc1	Žilina
in	in	k?	in
Romány	román	k1gInPc1	román
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
Milena	Milena	k1gFnSc1	Milena
Tomášková	Tomášková	k1gFnSc1	Tomášková
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
a	a	k8xC	a
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc1	království
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g />
.	.	kIx.	.
</s>
<s>
Žilina	Žilina	k1gFnSc1	Žilina
Exil	exil	k1gInSc4	exil
a	a	k8xC	a
království	království	k1gNnSc4	království
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
in	in	k?	in
Romány	román	k1gInPc1	román
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
Milena	Milena	k1gFnSc1	Milena
Tomášková	Tomášková	k1gFnSc1	Tomášková
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
Mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
a	a	k8xC	a
Exil	exil	k1gInSc1	exil
a	a	k8xC	a
království	království	k1gNnSc2	království
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šerý	šerý	k2eAgMnSc1d1	šerý
První	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Kateřina	Kateřina	k1gFnSc1	Kateřina
Vinšová	Vinšová	k1gFnSc1	Vinšová
Caligula	Caligula	k1gFnSc1	Caligula
Caligula	Caligula	k1gFnSc1	Caligula
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Alena	Alena	k1gFnSc1	Alena
Šabatková	Šabatkový	k2eAgFnSc1d1	Šabatková
in	in	k?	in
Caligula	Caligula	k1gFnSc1	Caligula
<g/>
,	,	kIx,	,
Stav	stav	k1gInSc1	stav
obležení	obležení	k1gNnSc2	obležení
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Alena	Alena	k1gFnSc1	Alena
Šabatková	Šabatkový	k2eAgFnSc1d1	Šabatková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Konůpek	Konůpek	k1gInSc4	Konůpek
Nedorozumění	nedorozumění	k1gNnSc2	nedorozumění
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Alena	Alena	k1gFnSc1	Alena
Šabatková	Šabatkový	k2eAgFnSc1d1	Šabatková
Stav	stav	k1gInSc4	stav
obležení	obležení	k1gNnSc2	obležení
in	in	k?	in
Caligula	Caligula	k1gFnSc1	Caligula
<g/>
,	,	kIx,	,
Stav	stav	k1gInSc1	stav
obležení	obležení	k1gNnSc2	obležení
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Alena	Alena	k1gFnSc1	Alena
Šabatková	Šabatkový	k2eAgFnSc1d1	Šabatková
a	a	k8xC	a
Jiří	Jiří	k1gMnPc4	Jiří
Konůpek	Konůpek	k1gInSc4	Konůpek
Spravedliví	spravedlivý	k2eAgMnPc1d1	spravedlivý
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Alena	Alena	k1gFnSc1	Alena
Šabatková	Šabatkový	k2eAgFnSc1d1	Šabatková
Rekviem	rekviem	k1gNnSc7	rekviem
za	za	k7c4	za
jeptišku	jeptiška	k1gFnSc4	jeptiška
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Gabriela	Gabriela	k1gFnSc1	Gabriela
Vavrušková	Vavrušková	k1gFnSc1	Vavrušková
Běsi	běs	k1gMnPc1	běs
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Král	Král	k1gMnSc1	Král
Člověk	člověk	k1gMnSc1	člověk
revoltující	revoltující	k2eAgMnSc1d1	revoltující
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Kateřina	Kateřina	k1gFnSc1	Kateřina
Lukešová	Lukešová	k1gFnSc1	Lukešová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
Sisyfovi	Sisyfos	k1gMnSc6	Sisyfos
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Dagmar	Dagmar	k1gFnSc1	Dagmar
Steinová	Steinová	k1gFnSc1	Steinová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
výbory	výbor	k1gInPc7	výbor
z	z	k7c2	z
esejistické	esejistický	k2eAgFnSc2d1	esejistická
tvorby	tvorba	k1gFnSc2	tvorba
Léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Vlasta	Vlasta	k1gFnSc1	Vlasta
Dufková	Dufková	k1gFnSc1	Dufková
Rub	rub	k1gInSc1	rub
a	a	k8xC	a
líc	líc	k1gInSc1	líc
(	(	kIx(	(
<g/>
eseje	esej	k1gInPc1	esej
Léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
Svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
Rub	rub	k1gInSc1	rub
a	a	k8xC	a
líc	líc	k1gInSc1	líc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgInS	přeložit
a	a	k8xC	a
doslov	doslov	k1gInSc1	doslov
napsal	napsat	k5eAaPmAgInS	napsat
Denis	Denisa	k1gFnPc2	Denisa
Molčanov	Molčanovo	k1gNnPc2	Molčanovo
<g />
.	.	kIx.	.
</s>
<s>
Zápisníky	zápisník	k1gInPc1	zápisník
I.	I.	kA	I.
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
1935	[number]	k4	1935
-	-	kIx~	-
únor	únor	k1gInSc1	únor
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Vlasta	Vlasta	k1gFnSc1	Vlasta
Dufková	Dufková	k1gFnSc1	Dufková
Zápisníky	zápisník	k1gInPc7	zápisník
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1942	[number]	k4	1942
-	-	kIx~	-
březen	březen	k1gInSc1	březen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Vlasta	Vlasta	k1gFnSc1	Vlasta
Dufková	Dufková	k1gFnSc1	Dufková
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Mlejnek	Mlejnek	k1gMnSc1	Mlejnek
Zápisníky	zápisník	k1gInPc7	zápisník
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1951	[number]	k4	1951
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Vlasta	Vlasta	k1gFnSc1	Vlasta
Dufková	Dufková	k1gFnSc1	Dufková
</s>
