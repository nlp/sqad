<p>
<s>
Mlhovina	mlhovina	k1gFnSc1	mlhovina
je	být	k5eAaImIp3nS	být
mezihvězdné	mezihvězdný	k2eAgNnSc4d1	mezihvězdné
mračno	mračno	k1gNnSc4	mračno
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
mlhovina	mlhovina	k1gFnSc1	mlhovina
obecným	obecný	k2eAgNnSc7d1	obecné
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
rozměrný	rozměrný	k2eAgInSc4d1	rozměrný
astronomický	astronomický	k2eAgInSc4d1	astronomický
objekt	objekt	k1gInSc4	objekt
včetně	včetně	k7c2	včetně
galaxií	galaxie	k1gFnPc2	galaxie
mimo	mimo	k7c4	mimo
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
užívání	užívání	k1gNnPc1	užívání
staršího	starý	k2eAgInSc2d2	starší
významu	význam	k1gInSc2	význam
stále	stále	k6eAd1	stále
přežívají	přežívat	k5eAaImIp3nP	přežívat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
galaxie	galaxie	k1gFnPc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
mlhovina	mlhovina	k1gFnSc1	mlhovina
Andromeda	Andromeda	k1gMnSc1	Andromeda
nebo	nebo	k8xC	nebo
mlhovina	mlhovina	k1gFnSc1	mlhovina
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
objekt	objekt	k1gInSc4	objekt
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Mlhoviny	mlhovina	k1gFnPc4	mlhovina
lze	lze	k6eAd1	lze
roztřídit	roztřídit	k5eAaPmF	roztřídit
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Difúzní	difúzní	k2eAgFnPc1d1	difúzní
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jsou	být	k5eAaImIp3nP	být
osvětlené	osvětlený	k2eAgFnPc1d1	osvětlená
mlhoviny	mlhovina	k1gFnPc1	mlhovina
</s>
</p>
<p>
<s>
Emisní	emisní	k2eAgFnPc1d1	emisní
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jsou	být	k5eAaImIp3nP	být
vnitřně	vnitřně	k6eAd1	vnitřně
osvětlené	osvětlený	k2eAgInPc1d1	osvětlený
mraky	mrak	k1gInPc1	mrak
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
mlhoviny	mlhovina	k1gFnPc1	mlhovina
září	zářit	k5eAaImIp3nP	zářit
ve	v	k7c6	v
spektrálních	spektrální	k2eAgFnPc6d1	spektrální
čarách	čára	k1gFnPc6	čára
svých	svůj	k3xOyFgFnPc2	svůj
molekul	molekula	k1gFnPc2	molekula
či	či	k8xC	či
atomů	atom	k1gInPc2	atom
vybuzených	vybuzený	k2eAgInPc2d1	vybuzený
většinou	většinou	k6eAd1	většinou
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
světlem	světlo	k1gNnSc7	světlo
i	i	k8xC	i
vzdálenějších	vzdálený	k2eAgFnPc2d2	vzdálenější
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Karmínová	karmínový	k2eAgFnSc1d1	karmínová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
kupříkladu	kupříkladu	k6eAd1	kupříkladu
čára	čára	k1gFnSc1	čára
přechodu	přechod	k1gInSc2	přechod
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
atomů	atom	k1gInPc2	atom
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
nejobvyklejší	obvyklý	k2eAgInPc1d3	nejobvyklejší
typy	typ	k1gInPc1	typ
emisních	emisní	k2eAgFnPc2d1	emisní
mlhovin	mlhovina	k1gFnPc2	mlhovina
jsou	být	k5eAaImIp3nP	být
HII	HII	kA	HII
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
planetární	planetární	k2eAgFnSc2d1	planetární
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reflexní	reflexní	k2eAgFnPc1d1	reflexní
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jsou	být	k5eAaImIp3nP	být
osvětleny	osvětlen	k2eAgInPc1d1	osvětlen
odrazy	odraz	k1gInPc1	odraz
světla	světlo	k1gNnSc2	světlo
blízkých	blízký	k2eAgFnPc2d1	blízká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Odraz	odraz	k1gInSc1	odraz
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
především	především	k9	především
prach	prach	k1gInSc1	prach
obsažený	obsažený	k2eAgInSc1d1	obsažený
v	v	k7c6	v
mlhovině	mlhovina	k1gFnSc6	mlhovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
září	zářit	k5eAaImIp3nS	zářit
celým	celý	k2eAgNnSc7d1	celé
spektrem	spektrum	k1gNnSc7	spektrum
blízkých	blízký	k2eAgFnPc2d1	blízká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
mlhovina	mlhovina	k1gFnSc1	mlhovina
uvnitř	uvnitř	k7c2	uvnitř
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
Plejády	Plejáda	k1gFnSc2	Plejáda
nebo	nebo	k8xC	nebo
mlhovina	mlhovina	k1gFnSc1	mlhovina
Toby	Toba	k1gMnSc2	Toba
Jug	Jug	k1gMnSc2	Jug
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Temné	temný	k2eAgFnPc1d1	temná
mlhoviny	mlhovina	k1gFnPc1	mlhovina
nejsou	být	k5eNaImIp3nP	být
osvětleny	osvětlit	k5eAaPmNgFnP	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zakryjí	zakrýt	k5eAaPmIp3nP	zakrýt
hvězdu	hvězda	k1gFnSc4	hvězda
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc4d1	jiná
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
mlhovina	mlhovina	k1gFnSc1	mlhovina
Koňská	koňský	k2eAgFnSc1d1	koňská
hlava	hlava	k1gFnSc1	hlava
v	v	k7c6	v
Orionu	orion	k1gInSc6	orion
a	a	k8xC	a
mlhovina	mlhovina	k1gFnSc1	mlhovina
Uhelný	uhelný	k2eAgInSc1d1	uhelný
pytel	pytel	k1gInSc1	pytel
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
kříži	kříž	k1gInSc6	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
mlhovin	mlhovina	k1gFnPc2	mlhovina
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
mlhoviny	mlhovina	k1gFnPc1	mlhovina
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
života	život	k1gInSc2	život
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
projde	projít	k5eAaPmIp3nS	projít
změnou	změna	k1gFnSc7	změna
v	v	k7c4	v
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
odfoukne	odfouknout	k5eAaPmIp3nS	odfouknout
své	svůj	k3xOyFgFnPc4	svůj
vnější	vnější	k2eAgFnPc4d1	vnější
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
planetární	planetární	k2eAgFnSc4d1	planetární
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Novy	nova	k1gFnPc1	nova
a	a	k8xC	a
supernovy	supernova	k1gFnPc1	supernova
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
vytvořit	vytvořit	k5eAaPmF	vytvořit
mlhoviny	mlhovina	k1gFnPc4	mlhovina
známé	známá	k1gFnPc4	známá
jako	jako	k8xS	jako
zbytky	zbytek	k1gInPc4	zbytek
novy	nova	k1gFnSc2	nova
a	a	k8xC	a
zbytky	zbytek	k1gInPc4	zbytek
supernovy	supernova	k1gFnSc2	supernova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodiště	rodiště	k1gNnPc1	rodiště
hvězd	hvězda	k1gFnPc2	hvězda
==	==	k?	==
</s>
</p>
<p>
<s>
Mlhoviny	mlhovina	k1gFnPc4	mlhovina
jsou	být	k5eAaImIp3nP	být
rodištěm	rodiště	k1gNnSc7	rodiště
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
mlhovinové	mlhovinový	k2eAgFnSc2d1	mlhovinový
hypotézy	hypotéza	k1gFnSc2	hypotéza
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgFnPc1d1	nová
hvězdy	hvězda	k1gFnPc1	hvězda
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
zředěných	zředěný	k2eAgInPc2d1	zředěný
molekulárních	molekulární	k2eAgInPc2d1	molekulární
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
smršťovat	smršťovat	k5eAaImF	smršťovat
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
kvůli	kvůli	k7c3	kvůli
vlivu	vliv	k1gInSc3	vliv
blízké	blízký	k2eAgFnSc2d1	blízká
exploze	exploze	k1gFnSc2	exploze
supernovy	supernova	k1gFnSc2	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Oblak	oblak	k1gInSc1	oblak
se	se	k3xPyFc4	se
smršťuje	smršťovat	k5eAaImIp3nS	smršťovat
a	a	k8xC	a
trhá	trhat	k5eAaImIp3nS	trhat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
hvězdy	hvězda	k1gFnPc1	hvězda
ionizují	ionizovat	k5eAaBmIp3nP	ionizovat
okolní	okolní	k2eAgInSc4d1	okolní
plyn	plyn	k1gInSc4	plyn
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
emisní	emisní	k2eAgFnSc4d1	emisní
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
Nebula	nebula	k1gFnSc1	nebula
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
spisovatelům	spisovatel	k1gMnPc3	spisovatel
fantasy	fantas	k1gInPc1	fantas
a	a	k8xC	a
sci-fi	scii	k1gFnPc1	sci-fi
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
mlhovinách	mlhovina	k1gFnPc6	mlhovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
mlhovina	mlhovina	k1gFnSc1	mlhovina
</s>
</p>
<p>
<s>
Mlhovinová	Mlhovinový	k2eAgFnSc1d1	Mlhovinový
hypotéza	hypotéza	k1gFnSc1	hypotéza
</s>
</p>
<p>
<s>
Mezihvězdné	mezihvězdný	k2eAgNnSc1d1	mezihvězdné
prostředí	prostředí	k1gNnSc1	prostředí
</s>
</p>
<p>
<s>
Krabí	krabí	k2eAgFnSc1d1	krabí
mlhovina	mlhovina	k1gFnSc1	mlhovina
</s>
</p>
<p>
<s>
Emisní	emisní	k2eAgFnSc1d1	emisní
mlhovina	mlhovina	k1gFnSc1	mlhovina
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mlhovina	mlhovina	k1gFnSc1	mlhovina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Mlhoviny	mlhovina	k1gFnSc2	mlhovina
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mlhovina	mlhovina	k1gFnSc1	mlhovina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
