<p>
<s>
Sysel	Sysel	k1gMnSc1	Sysel
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Spermophilus	Spermophilus	k1gMnSc1	Spermophilus
citellus	citellus	k1gMnSc1	citellus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgMnSc1d1	evropský
zástupce	zástupce	k1gMnSc1	zástupce
rodu	rod	k1gInSc2	rod
sysel	sysel	k1gMnSc1	sysel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
veverkovití	veverkovitý	k2eAgMnPc1d1	veverkovitý
zástupcem	zástupce	k1gMnSc7	zástupce
řádu	řád	k1gInSc3	řád
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
příbuzné	příbuzný	k2eAgFnSc2d1	příbuzná
veverky	veverka	k1gFnSc2	veverka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlodavec	hlodavec	k1gMnSc1	hlodavec
zemní	zemní	k2eAgInSc1d1	zemní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Sysel	Sysel	k1gMnSc1	Sysel
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnPc4	délka
okolo	okolo	k7c2	okolo
20	[number]	k4	20
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
váhy	váha	k1gFnSc2	váha
200-400	[number]	k4	200-400
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
hustý	hustý	k2eAgInSc4d1	hustý
<g/>
,	,	kIx,	,
hnědavý	hnědavý	k2eAgInSc4d1	hnědavý
<g/>
,	,	kIx,	,
přiléhavý	přiléhavý	k2eAgInSc4d1	přiléhavý
kožich	kožich	k1gInSc4	kožich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sysel	Sysel	k1gMnSc1	Sysel
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc4d1	velké
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc4d1	malý
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyřprsté	čtyřprstý	k2eAgFnPc1d1	čtyřprstá
přední	přední	k2eAgFnPc1d1	přední
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
hodně	hodně	k6eAd1	hodně
kratší	krátký	k2eAgFnSc6d2	kratší
než	než	k8xS	než
zadní	zadní	k2eAgFnSc6d1	zadní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
denní	denní	k2eAgNnSc1d1	denní
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Hloubí	hloubit	k5eAaImIp3nP	hloubit
si	se	k3xPyFc3	se
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
spí	spát	k5eAaImIp3nP	spát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
především	především	k9	především
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místa	místo	k1gNnPc1	místo
jeho	jeho	k3xOp3gInPc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
až	až	k9	až
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
hojný	hojný	k2eAgMnSc1d1	hojný
škůdce	škůdce	k1gMnSc1	škůdce
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
přírodě	příroda	k1gFnSc6	příroda
stává	stávat	k5eAaImIp3nS	stávat
vzácností	vzácnost	k1gFnSc7	vzácnost
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
úbytku	úbytek	k1gInSc6	úbytek
přirozených	přirozený	k2eAgFnPc2d1	přirozená
ploch	plocha	k1gFnPc2	plocha
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
výskyt	výskyt	k1gInSc4	výskyt
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
zastává	zastávat	k5eAaImIp3nS	zastávat
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
hranice	hranice	k1gFnSc1	hranice
výskytu	výskyt	k1gInSc2	výskyt
druhů	druh	k1gMnPc2	druh
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
nezvyklé	zvyklý	k2eNgNnSc1d1	nezvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvířata	zvíře	k1gNnPc1	zvíře
dočasně	dočasně	k6eAd1	dočasně
nebo	nebo	k8xC	nebo
trvale	trvale	k6eAd1	trvale
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Sysel	Sysel	k1gMnSc1	Sysel
je	být	k5eAaImIp3nS	být
kvalifikován	kvalifikovat	k5eAaBmNgMnS	kvalifikovat
jako	jako	k9	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
podléhá	podléhat	k5eAaImIp3nS	podléhat
ochraně	ochrana	k1gFnSc3	ochrana
z	z	k7c2	z
celoevropského	celoevropský	k2eAgNnSc2d1	celoevropské
hlediska	hledisko	k1gNnSc2	hledisko
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sysel	sysel	k1gMnSc1	sysel
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Sysel	Sysel	k1gMnSc1	Sysel
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
záchranném	záchranný	k2eAgInSc6d1	záchranný
programu	program	k1gInSc6	program
Sysla	Sysel	k1gMnSc2	Sysel
obecného	obecný	k2eAgNnSc2d1	obecné
</s>
</p>
<p>
<s>
Sysel	Sysel	k1gMnSc1	Sysel
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
