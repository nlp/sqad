<s>
Močůvka	močůvka	k1gFnSc1	močůvka
je	být	k5eAaImIp3nS	být
tekuté	tekutý	k2eAgNnSc4d1	tekuté
statkové	statkový	k2eAgNnSc4d1	statkové
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
,	,	kIx,	,
zkvašená	zkvašený	k2eAgFnSc1d1	zkvašená
moč	moč	k1gFnSc1	moč
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
<g/>
.	.	kIx.	.
</s>
