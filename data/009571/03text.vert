<p>
<s>
Močůvka	močůvka	k1gFnSc1	močůvka
je	být	k5eAaImIp3nS	být
tekuté	tekutý	k2eAgNnSc4d1	tekuté
statkové	statkový	k2eAgNnSc4d1	statkové
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
,	,	kIx,	,
zkvašená	zkvašený	k2eAgFnSc1d1	zkvašená
moč	moč	k1gFnSc1	moč
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc1d1	základní
parametry	parametr	k1gInPc1	parametr
močůvky	močůvka	k1gFnSc2	močůvka
(	(	kIx(	(
<g/>
Macourek	Macourek	k1gMnSc1	Macourek
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Růžek	růžek	k1gInSc1	růžek
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Kára	kára	k1gFnSc1	kára
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Potenciální	potenciální	k2eAgInPc4d1	potenciální
problémy	problém	k1gInPc4	problém
pro	pro	k7c4	pro
anaerobní	anaerobní	k2eAgFnSc4d1	anaerobní
digesci	digesce	k1gFnSc4	digesce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přítomnost	přítomnost	k1gFnSc1	přítomnost
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
</s>
</p>
<p>
<s>
vysoké	vysoká	k1gFnPc1	vysoká
pH	ph	kA	ph
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hnojůvka	hnojůvka	k1gFnSc1	hnojůvka
</s>
</p>
<p>
<s>
Kejda	kejda	k1gFnSc1	kejda
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kára	kára	k1gFnSc1	kára
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Pastorek	Pastorek	k1gMnSc1	Pastorek
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Kompostování	kompostování	k1gNnSc4	kompostování
zbytkové	zbytkový	k2eAgFnSc2d1	zbytková
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Biom	Biom	k1gInSc1	Biom
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
31.1	[number]	k4	31.1
<g/>
.2002	.2002	k4	.2002
<g/>
,	,	kIx,	,
http://biom.cz/index.shtml?x=62847	[url]	k4	http://biom.cz/index.shtml?x=62847
</s>
</p>
<p>
<s>
Macourek	Macourek	k1gMnSc1	Macourek
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Optimalizace	optimalizace	k1gFnSc1	optimalizace
surovinové	surovinový	k2eAgFnSc2d1	surovinová
skladby	skladba	k1gFnSc2	skladba
při	při	k7c6	při
kompostování	kompostování	k1gNnSc6	kompostování
zbytkové	zbytkový	k2eAgFnSc2d1	zbytková
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Biom	Biom	k1gInSc1	Biom
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
11.3	[number]	k4	11.3
<g/>
.2002	.2002	k4	.2002
<g/>
,	,	kIx,	,
http://biom.cz/index.shtml?x=73158	[url]	k4	http://biom.cz/index.shtml?x=73158
</s>
</p>
<p>
<s>
Růžek	Růžek	k1gMnSc1	Růžek
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Kusá	kusý	k2eAgFnSc1d1	kusá
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
Hejnová	Hejnová	k1gFnSc1	Hejnová
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Rizika	riziko	k1gNnSc2	riziko
používání	používání	k1gNnSc2	používání
dusíkatých	dusíkatý	k2eAgNnPc2d1	dusíkaté
hnojiv	hnojivo	k1gNnPc2	hnojivo
ve	v	k7c6	v
zranitelných	zranitelný	k2eAgFnPc6d1	zranitelná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
</s>
</p>
