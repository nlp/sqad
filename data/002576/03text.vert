<s>
Miron	Miron	k1gInSc1	Miron
Białoszewski	Białoszewsk	k1gFnSc2	Białoszewsk
[	[	kIx(	[
<g/>
bjauoševski	bjauoševski	k6eAd1	bjauoševski
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1922	[number]	k4	1922
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
třídě	třída	k1gFnSc6	třída
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
okupace	okupace	k1gFnSc2	okupace
složil	složit	k5eAaPmAgMnS	složit
maturitu	maturita	k1gFnSc4	maturita
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
polonistiku	polonistika	k1gFnSc4	polonistika
na	na	k7c6	na
ilegální	ilegální	k2eAgFnSc6d1	ilegální
Varšavské	varšavský	k2eAgFnSc6d1	Varšavská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
-	-	kIx~	-
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
-	-	kIx~	-
absolvovat	absolvovat	k5eAaPmF	absolvovat
nemohl	moct	k5eNaImAgInS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
odvezen	odvézt	k5eAaPmNgInS	odvézt
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
do	do	k7c2	do
opolského	opolský	k2eAgInSc2d1	opolský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgMnS	být
částí	část	k1gFnPc2	část
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
básnickému	básnický	k2eAgInSc3d1	básnický
debutu	debut	k1gInSc3	debut
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
a	a	k8xC	a
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
první	první	k4xOgFnSc1	první
sbírka	sbírka	k1gFnSc1	sbírka
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
experimentální	experimentální	k2eAgNnSc4d1	experimentální
divadlo	divadlo	k1gNnSc4	divadlo
Teatr	Teatr	k1gInSc4	Teatr
na	na	k7c4	na
Tarczyńskiej	Tarczyńskiej	k1gInSc4	Tarczyńskiej
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
zkoušky	zkouška	k1gFnSc2	zkouška
a	a	k8xC	a
soukromé	soukromý	k2eAgFnSc2d1	soukromá
inscenace	inscenace	k1gFnSc2	inscenace
amatérsky	amatérsky	k6eAd1	amatérsky
natáčel	natáčet	k5eAaImAgMnS	natáčet
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dost	dost	k6eAd1	dost
slavným	slavný	k2eAgInSc7d1	slavný
a	a	k8xC	a
díky	díky	k7c3	díky
protekci	protekce	k1gFnSc3	protekce
známých	známý	k1gMnPc2	známý
osobnosti	osobnost	k1gFnSc2	osobnost
dostal	dostat	k5eAaPmAgInS	dostat
byt	byt	k1gInSc1	byt
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
partnerem	partner	k1gMnSc7	partner
-	-	kIx~	-
Leszkiem	Leszkius	k1gMnSc7	Leszkius
Solińským	Solińský	k1gMnSc7	Solińský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
dílo	dílo	k1gNnSc1	dílo
-	-	kIx~	-
"	"	kIx"	"
<g/>
Památník	památník	k1gInSc1	památník
z	z	k7c2	z
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pamiętnik	Pamiętnik	k1gInSc1	Pamiętnik
z	z	k7c2	z
powstania	powstanium	k1gNnSc2	powstanium
warszawskiego	warszawskiego	k6eAd1	warszawskiego
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
<g/>
,	,	kIx,	,
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
způsob	způsob	k1gInSc4	způsob
noční	noční	k2eAgFnSc4d1	noční
můru	můra	k1gFnSc4	můra
<g/>
,	,	kIx,	,
hrůzu	hrůza	k1gFnSc4	hrůza
pokusu	pokus	k1gInSc2	pokus
varšavských	varšavský	k2eAgMnPc2d1	varšavský
obyvatelů	obyvatel	k1gMnPc2	obyvatel
ukázat	ukázat	k5eAaPmF	ukázat
okupantovi	okupant	k1gMnSc3	okupant
sílu	síla	k1gFnSc4	síla
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
víru	víra	k1gFnSc4	víra
ve	v	k7c4	v
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
psal	psát	k5eAaImAgMnS	psát
krátké	krátký	k2eAgFnPc4d1	krátká
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
stručné	stručný	k2eAgInPc4d1	stručný
texty	text	k1gInPc4	text
-	-	kIx~	-
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
takové	takový	k3xDgFnPc1	takový
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dost	dost	k6eAd1	dost
divný	divný	k2eAgInSc4d1	divný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
-	-	kIx~	-
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
nemohl	moct	k5eNaImAgMnS	moct
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tvořil	tvořit	k5eAaImAgInS	tvořit
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
sám	sám	k3xTgMnSc1	sám
<g/>
;	;	kIx,	;
vadilo	vadit	k5eAaImAgNnS	vadit
mi	já	k3xPp1nSc3	já
sluneční	sluneční	k2eAgInSc1d1	sluneční
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgInPc4	veškerý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
jemu	on	k3xPp3gMnSc3	on
známém	známý	k2eAgInSc6d1	známý
světě	svět	k1gInSc6	svět
-	-	kIx~	-
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
nejist	nejisto	k1gNnPc2	nejisto
-	-	kIx~	-
jedl	jíst	k5eAaImAgMnS	jíst
jen	jen	k9	jen
<g/>
,	,	kIx,	,
když	když	k8xS	když
opravdu	opravdu	k6eAd1	opravdu
musel	muset	k5eAaImAgMnS	muset
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
sladkosti	sladkost	k1gFnPc4	sladkost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
byl	být	k5eAaImAgInS	být
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
infarkt	infarkt	k1gInSc4	infarkt
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
smrtelný	smrtelný	k2eAgInSc4d1	smrtelný
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
'	'	kIx"	'
<g/>
Cmentarz	Cmentarz	k1gInSc1	Cmentarz
Powązkowski	Powązkowsk	k1gFnSc2	Powązkowsk
<g/>
'	'	kIx"	'
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
hodně	hodně	k6eAd1	hodně
používal	používat	k5eAaImAgMnS	používat
humor	humor	k1gInSc4	humor
a	a	k8xC	a
sebeironii	sebeironie	k1gFnSc4	sebeironie
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
využívat	využívat	k5eAaImF	využívat
bohatosti	bohatost	k1gFnPc1	bohatost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
film	film	k1gInSc1	film
Andrzeje	Andrzeje	k1gMnSc2	Andrzeje
Barańského	Barańský	k1gMnSc2	Barańský
"	"	kIx"	"
<g/>
Pár	pár	k4xCyI	pár
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
krátkej	krátkej	k?	krátkej
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Parę	Parę	k1gMnSc1	Parę
osób	osób	k1gMnSc1	osób
<g/>
,	,	kIx,	,
mały	mał	k2eAgFnPc1d1	mał
czas	czas	k6eAd1	czas
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyprávějící	vyprávějící	k2eAgInSc4d1	vyprávějící
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
básníka	básník	k1gMnSc2	básník
s	s	k7c7	s
Jadvigou	Jadviga	k1gFnSc7	Jadviga
Stańczakovou	Stańczakový	k2eAgFnSc7d1	Stańczakový
-	-	kIx~	-
nevidomou	vidomý	k2eNgFnSc7d1	nevidomá
žurnalistkou	žurnalistka	k1gFnSc7	žurnalistka
a	a	k8xC	a
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydalo	vydat	k5eAaPmAgNnS	vydat
krakovské	krakovský	k2eAgNnSc1d1	Krakovské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Znak	znak	k1gInSc1	znak
jeho	jeho	k3xOp3gInSc1	jeho
"	"	kIx"	"
<g/>
Tajný	tajný	k2eAgInSc1d1	tajný
deník	deník	k1gInSc1	deník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tajny	Tajny	k?	Tajny
dziennik	dziennik	k1gInSc1	dziennik
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
autorova	autorův	k2eAgFnSc1d1	autorova
homoerotická	homoerotický	k2eAgFnSc1d1	homoerotická
orientace	orientace	k1gFnSc1	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Miron	Miron	k1gInSc1	Miron
Białoszewski	Białoszewski	k1gNnSc2	Białoszewski
Památník	památník	k1gInSc1	památník
z	z	k7c2	z
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Přeložila	přeložit	k5eAaPmAgFnS	přeložit
a	a	k8xC	a
doslov	doslov	k1gInSc4	doslov
napsala	napsat	k5eAaPmAgFnS	napsat
Daniela	Daniela	k1gFnSc1	Daniela
Lehárová	Lehárový	k2eAgFnSc1d1	Lehárová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1987	[number]	k4	1987
M	M	kA	M
<g/>
'	'	kIx"	'
<g/>
ironie	ironie	k1gFnSc1	ironie
[	[	kIx(	[
<g/>
básně	báseň	k1gFnPc1	báseň
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
a	a	k8xC	a
doslov	doslov	k1gInSc4	doslov
napsal	napsat	k5eAaBmAgMnS	napsat
Josef	Josef	k1gMnSc1	Josef
Vlásek	vlásek	k1gInSc4	vlásek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1988	[number]	k4	1988
</s>
