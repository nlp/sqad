<p>
<s>
Užití	užití	k1gNnSc1	užití
slova	slovo	k1gNnSc2	slovo
svatý	svatý	k2eAgMnSc1d1	svatý
či	či	k8xC	či
světec	světec	k1gMnSc1	světec
(	(	kIx(	(
<g/>
z	z	k7c2	z
praslov	praslovo	k1gNnPc2	praslovo
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
svętъ	svętъ	k?	svętъ
mocný	mocný	k2eAgInSc1d1	mocný
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
<g/>
)	)	kIx)	)
v	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
věc	věc	k1gFnSc4	věc
(	(	kIx(	(
<g/>
mše	mše	k1gFnSc2	mše
svatá	svatý	k2eAgFnSc1d1	svatá
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgInSc1d1	svatý
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
nejsvětější	nejsvětější	k2eAgFnSc1d1	nejsvětější
trojice	trojice	k1gFnSc1	trojice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
náboženstvích	náboženství	k1gNnPc6	náboženství
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc6d1	náboženská
společnostech	společnost	k1gFnPc6	společnost
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xS	jako
označení	označení	k1gNnSc1	označení
někoho	někdo	k3yInSc2	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
ideálu	ideál	k1gInSc2	ideál
příslušníka	příslušník	k1gMnSc2	příslušník
daného	daný	k2eAgNnSc2d1	dané
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něho	on	k3xPp3gNnSc2	on
následování	následování	k1gNnSc2	následování
a	a	k8xC	a
uctívání	uctívání	k1gNnSc2	uctívání
hodný	hodný	k2eAgMnSc1d1	hodný
a	a	k8xC	a
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
či	či	k8xC	či
blízký	blízký	k2eAgInSc1d1	blízký
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
slovem	slovem	k6eAd1	slovem
boží	boží	k2eAgFnSc1d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
svatý	svatý	k1gMnSc1	svatý
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sv.	sv.	kA	sv.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
křesťanských	křesťanský	k2eAgNnPc6d1	křesťanské
zobrazeních	zobrazení	k1gNnPc6	zobrazení
jsou	být	k5eAaImIp3nP	být
svatí	svatý	k1gMnPc1	svatý
často	často	k6eAd1	často
zpodobněni	zpodobnit	k5eAaPmNgMnP	zpodobnit
se	s	k7c7	s
svatozáří	svatozář	k1gFnSc7	svatozář
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
etymologií	etymologie	k1gFnSc7	etymologie
českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
blízkou	blízký	k2eAgFnSc4d1	blízká
světlu	světlo	k1gNnSc3	světlo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
záři	záře	k1gFnSc4	záře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
nenábožensky	nábožensky	k6eNd1	nábožensky
svatý	svatý	k2eAgMnSc1d1	svatý
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
,	,	kIx,	,
čistá	čistý	k2eAgNnPc1d1	čisté
<g/>
,	,	kIx,	,
hodná	hodný	k2eAgNnPc1d1	hodné
následování	následování	k1gNnPc1	následování
a	a	k8xC	a
úcty	úcta	k1gFnPc1	úcta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
svatá	svatá	k1gFnSc1	svatá
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
svatý	svatý	k1gMnSc1	svatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
odkaz	odkaz	k1gInSc1	odkaz
je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
svatý	svatý	k1gMnSc1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předkřesťanském	předkřesťanský	k2eAgInSc6d1	předkřesťanský
Římě	Řím	k1gInSc6	Řím
sacer	sacero	k1gNnPc2	sacero
znamenalo	znamenat	k5eAaImAgNnS	znamenat
oddělený	oddělený	k2eAgInSc1d1	oddělený
<g/>
,	,	kIx,	,
odehnaný	odehnaný	k2eAgInSc1d1	odehnaný
(	(	kIx(	(
<g/>
do	do	k7c2	do
světa	svět	k1gInSc2	svět
od	od	k7c2	od
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
<g/>
,	,	kIx,	,
nechráněný	chráněný	k2eNgInSc1d1	nechráněný
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
homo	homo	k1gMnSc1	homo
sacer	sacer	k1gInSc4	sacer
byl	být	k5eAaImAgMnS	být
vyhnanec	vyhnanec	k1gMnSc1	vyhnanec
<g/>
,	,	kIx,	,
psanec	psanec	k1gMnSc1	psanec
<g/>
,	,	kIx,	,
bezdomovec	bezdomovec	k1gMnSc1	bezdomovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křesťanství	křesťanství	k1gNnSc1	křesťanství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Svatost	svatost	k1gFnSc1	svatost
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
i	i	k8xC	i
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
svatost	svatost	k1gFnSc4	svatost
v	v	k7c6	v
morálním	morální	k2eAgInSc6d1	morální
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
chápe	chápat	k5eAaImIp3nS	chápat
naše	náš	k3xOp1gFnSc1	náš
současná	současný	k2eAgFnSc1d1	současná
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výraz	výraz	k1gInSc4	výraz
z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
náboženské	náboženský	k2eAgFnSc2d1	náboženská
a	a	k8xC	a
tedy	tedy	k9	tedy
kultické	kultický	k2eAgFnSc2d1	kultická
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
"	"	kIx"	"
<g/>
označoval	označovat	k5eAaImAgMnS	označovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
Bohem	bůh	k1gMnSc7	bůh
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
ostatního	ostatní	k2eAgInSc2d1	ostatní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zprostředkuje	zprostředkovat	k5eAaPmIp3nS	zprostředkovat
jeho	jeho	k3xOp3gNnSc4	jeho
poznání	poznání	k1gNnSc4	poznání
a	a	k8xC	a
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
chrám	chrám	k1gInSc1	chrám
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
staveb	stavba	k1gFnPc2	stavba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
užívá	užívat	k5eAaImIp3nS	užívat
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
svatý	svatý	k2eAgMnSc1d1	svatý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
KADOŠ	kadoš	k1gInSc1	kadoš
<g/>
,	,	kIx,	,
ק	ק	k?	ק
<g/>
ָ	ָ	k?	ָ
<g/>
ד	ד	k?	ד
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ש	ש	k?	ש
<g/>
ׁ	ׁ	k?	ׁ
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
nejen	nejen	k6eAd1	nejen
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
"	"	kIx"	"
<g/>
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zasvěceni	zasvětit	k5eAaPmNgMnP	zasvětit
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g />
.	.	kIx.	.
</s>
<s>
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
zvláštním	zvláštní	k2eAgInPc3d1	zvláštní
obřadům	obřad	k1gInPc3	obřad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
celého	celý	k2eAgInSc2d1	celý
izraelského	izraelský	k2eAgInSc2d1	izraelský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
odděleného	oddělený	k2eAgInSc2d1	oddělený
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
a	a	k8xC	a
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
Bohu	bůh	k1gMnSc3	bůh
<g/>
"	"	kIx"	"
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
svatost	svatost	k1gFnSc1	svatost
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
odvozována	odvozovat	k5eAaImNgFnS	odvozovat
od	od	k7c2	od
Boží	boží	k2eAgFnSc2d1	boží
svatosti	svatost	k1gFnSc2	svatost
<g/>
.	.	kIx.	.
<g/>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
judaismu	judaismus	k1gInSc2	judaismus
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pojmu	pojem	k1gInSc2	pojem
svatosti	svatost	k1gFnSc2	svatost
začíná	začínat	k5eAaImIp3nS	začínat
klást	klást	k5eAaImF	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
morální	morální	k2eAgInSc4d1	morální
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
chápat	chápat	k5eAaImF	chápat
člověka	člověk	k1gMnSc4	člověk
jako	jako	k8xC	jako
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
hříšného	hříšný	k2eAgNnSc2d1	hříšné
<g/>
,	,	kIx,	,
porušeného	porušený	k2eAgInSc2d1	porušený
dědičným	dědičný	k2eAgInSc7d1	dědičný
hříchem	hřích	k1gInSc7	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
předepisuje	předepisovat	k5eAaImIp3nS	předepisovat
oběti	oběť	k1gFnPc4	oběť
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gFnSc4	jejich
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
od	od	k7c2	od
hříchu	hřích	k1gInSc2	hřích
očišťován	očišťován	k2eAgInSc1d1	očišťován
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
takovéto	takovýto	k3xDgNnSc4	takovýto
očišťování	očišťování	k1gNnSc4	očišťování
nevnímá	vnímat	k5eNaImIp3nS	vnímat
jako	jako	k9	jako
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
neustále	neustále	k6eAd1	neustále
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepřijde	přijít	k5eNaPmIp3nS	přijít
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
přinese	přinést	k5eAaPmIp3nS	přinést
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
samotný	samotný	k2eAgInSc1d1	samotný
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
takovéto	takovýto	k3xDgNnSc4	takovýto
pojetí	pojetí	k1gNnSc4	pojetí
ještě	ještě	k9	ještě
nezná	znát	k5eNaImIp3nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvířecími	zvířecí	k2eAgFnPc7d1	zvířecí
oběťmi	oběť	k1gFnPc7	oběť
se	s	k7c7	s
hříchy	hřích	k1gInPc7	hřích
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
krev	krev	k1gFnSc1	krev
býků	býk	k1gMnPc2	býk
a	a	k8xC	a
kozlů	kozel	k1gMnPc2	kozel
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
hříchy	hřích	k1gInPc4	hřích
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Křesťané	křesťan	k1gMnPc1	křesťan
jsou	být	k5eAaImIp3nP	být
přesto	přesto	k8xC	přesto
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
starozákonní	starozákonní	k2eAgNnSc4d1	starozákonní
pojetí	pojetí	k1gNnSc4	pojetí
svatosti	svatost	k1gFnSc2	svatost
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
ukřižování	ukřižování	k1gNnSc2	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc2d1	základní
skutečnosti	skutečnost	k1gFnSc2	skutečnost
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
a	a	k8xC	a
již	již	k6eAd1	již
dokonanou	dokonaný	k2eAgFnSc7d1	dokonaná
obětí	oběť	k1gFnSc7	oběť
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
celého	celý	k2eAgNnSc2d1	celé
lidstva	lidstvo	k1gNnSc2	lidstvo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
novozákonním	novozákonní	k2eAgNnSc6d1	novozákonní
pojetí	pojetí	k1gNnSc6	pojetí
smrt	smrt	k1gFnSc4	smrt
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
lidstva	lidstvo	k1gNnSc2	lidstvo
zde	zde	k6eAd1	zde
dle	dle	k7c2	dle
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Bůh	bůh	k1gMnSc1	bůh
sám	sám	k3xTgMnSc1	sám
ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
synu	syn	k1gMnSc6	syn
Ježíši	Ježíš	k1gMnSc6	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jako	jako	k9	jako
nehříšného	hříšný	k2eNgInSc2d1	hříšný
ho	on	k3xPp3gMnSc4	on
smrt	smrt	k1gFnSc1	smrt
nemohla	moct	k5eNaImAgFnS	moct
udržet	udržet	k5eAaPmF	udržet
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
moci	moc	k1gFnSc6	moc
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
Kristus	Kristus	k1gMnSc1	Kristus
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
vzkříšen	vzkříšen	k2eAgInSc1d1	vzkříšen
-	-	kIx~	-
podle	podle	k7c2	podle
Božího	boží	k2eAgInSc2d1	boží
plánu	plán	k1gInSc2	plán
záchrany	záchrana	k1gFnSc2	záchrana
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
této	tento	k3xDgFnSc3	tento
zprávě	zpráva	k1gFnSc3	zpráva
uvěří	uvěřit	k5eAaPmIp3nP	uvěřit
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
odpuštěno	odpustit	k5eAaPmNgNnS	odpustit
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
Bohu	bůh	k1gMnSc3	bůh
-	-	kIx~	-
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Boha	bůh	k1gMnSc4	bůh
-	-	kIx~	-
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
svatým	svatý	k2eAgMnSc7d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Svatost	svatost	k1gFnSc1	svatost
v	v	k7c6	v
novozákonním	novozákonní	k2eAgNnSc6d1	novozákonní
pojetí	pojetí	k1gNnSc6	pojetí
proto	proto	k8xC	proto
znamená	znamenat	k5eAaImIp3nS	znamenat
život	život	k1gInSc4	život
v	v	k7c6	v
milosti	milost	k1gFnSc6	milost
skrze	skrze	k?	skrze
tuto	tento	k3xDgFnSc4	tento
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
lidský	lidský	k2eAgInSc4d1	lidský
hřích	hřích	k1gInSc4	hřích
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
zaplaceno	zaplacen	k2eAgNnSc1d1	zaplaceno
výkupné	výkupné	k1gNnSc1	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
Novozákonní	novozákonní	k2eAgFnSc1d1	novozákonní
teologie	teologie	k1gFnSc1	teologie
nazývá	nazývat	k5eAaImIp3nS	nazývat
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
termínem	termín	k1gInSc7	termín
zástupná	zástupný	k2eAgFnSc1d1	zástupná
oběť	oběť	k1gFnSc1	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Svatost	svatost	k1gFnSc4	svatost
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
toto	tento	k3xDgNnSc4	tento
pojetí	pojetí	k1gNnSc4	pojetí
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
zprostředkovaná	zprostředkovaný	k2eAgFnSc1d1	zprostředkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Neděje	dít	k5eNaImIp3nS	dít
se	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
přičiněním	přičinění	k1gNnSc7	přičinění
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
askeze	askeze	k1gFnSc2	askeze
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
Boží	boží	k2eAgFnSc7d1	boží
milostí	milost	k1gFnSc7	milost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
a	a	k8xC	a
odstranění	odstranění	k1gNnSc2	odstranění
pokračujícího	pokračující	k2eAgInSc2d1	pokračující
hříchu	hřích	k1gInSc2	hřích
nazývá	nazývat	k5eAaImIp3nS	nazývat
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
termínem	termín	k1gInSc7	termín
posvěcení	posvěcení	k1gNnSc1	posvěcení
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
teologický	teologický	k2eAgInSc1d1	teologický
termín	termín	k1gInSc1	termín
posvěcený	posvěcený	k2eAgInSc1d1	posvěcený
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Posvěcení	posvěcení	k1gNnSc1	posvěcení
není	být	k5eNaImIp3nS	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc1	prostředek
spásy	spása	k1gFnSc2	spása
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
průvodní	průvodní	k2eAgInSc4d1	průvodní
znak	znak	k1gInSc4	znak
jejího	její	k3xOp3gNnSc2	její
uskutečnění	uskutečnění	k1gNnSc2	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
jej	on	k3xPp3gMnSc4	on
nazývá	nazývat	k5eAaImIp3nS	nazývat
termínem	termín	k1gInSc7	termín
ovoce	ovoce	k1gNnSc2	ovoce
Ducha	duch	k1gMnSc2	duch
Svatého	svatý	k1gMnSc2	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
zcela	zcela	k6eAd1	zcela
obrácen	obrácen	k2eAgInSc4d1	obrácen
smysl	smysl	k1gInSc4	smysl
tohoto	tento	k3xDgNnSc2	tento
usilování	usilování	k1gNnSc2	usilování
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
původcem	původce	k1gMnSc7	původce
svatosti	svatost	k1gFnSc2	svatost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
produktem	produkt	k1gInSc7	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Katolíci	katolík	k1gMnPc1	katolík
a	a	k8xC	a
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
===	===	k?	===
</s>
</p>
<p>
<s>
Prototypem	prototyp	k1gInSc7	prototyp
svatého	svatý	k1gMnSc2	svatý
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
katolictví	katolictví	k1gNnSc4	katolictví
a	a	k8xC	a
pravoslaví	pravoslaví	k1gNnSc4	pravoslaví
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Mučedníci	mučedník	k1gMnPc1	mučedník
byli	být	k5eAaImAgMnP	být
také	také	k9	také
prvními	první	k4xOgMnPc7	první
uctívanými	uctívaný	k2eAgMnPc7d1	uctívaný
světci	světec	k1gMnPc7	světec
<g/>
.	.	kIx.	.
</s>
<s>
Mučedník	mučedník	k1gMnSc1	mučedník
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
položil	položit	k5eAaPmAgInS	položit
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obětovat	obětovat	k5eAaBmF	obětovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
i	i	k9	i
to	ten	k3xDgNnSc1	ten
nejcennější	cenný	k2eAgNnSc1d3	nejcennější
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
měl	mít	k5eAaImAgMnS	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
přijat	přijmout	k5eAaPmNgMnS	přijmout
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
aplikace	aplikace	k1gFnSc2	aplikace
Kristových	Kristův	k2eAgNnPc2d1	Kristovo
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
Kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
přizná	přiznat	k5eAaPmIp3nS	přiznat
před	před	k7c7	před
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
i	i	k8xC	i
já	já	k3xPp1nSc1	já
přiznám	přiznat	k5eAaPmIp1nS	přiznat
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
nebeským	nebeský	k2eAgMnSc7d1	nebeský
Otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
uctíván	uctívat	k5eAaImNgMnS	uctívat
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
podobný	podobný	k2eAgInSc1d1	podobný
mučedníkům	mučedník	k1gMnPc3	mučedník
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
o	o	k7c4	o
mnichy	mnich	k1gMnPc4	mnich
a	a	k8xC	a
askety	asketa	k1gMnPc4	asketa
<g/>
.	.	kIx.	.
</s>
<s>
Svatí	svatý	k1gMnPc1	svatý
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
zobrazováni	zobrazován	k2eAgMnPc1d1	zobrazován
se	se	k3xPyFc4	se
svatozáří	svatozář	k1gFnSc7	svatozář
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
věřící	věřící	k1gMnPc1	věřící
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
spásy	spása	k1gFnPc4	spása
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
důsledky	důsledek	k1gInPc4	důsledek
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
tohoto	tento	k3xDgMnSc4	tento
světce	světec	k1gMnSc4	světec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
před	před	k7c7	před
Božím	boží	k2eAgInSc7d1	boží
trůnem	trůn	k1gInSc7	trůn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
přímluvu	přímluva	k1gFnSc4	přímluva
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
tento	tento	k3xDgMnSc1	tento
světec	světec	k1gMnSc1	světec
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předkládán	předkládat	k5eAaImNgInS	předkládat
jako	jako	k8xS	jako
životní	životní	k2eAgInSc1d1	životní
příklad	příklad	k1gInSc1	příklad
k	k	k7c3	k
následování	následování	k1gNnSc3	následování
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
křesťan	křesťan	k1gMnSc1	křesťan
může	moct	k5eAaImIp3nS	moct
takto	takto	k6eAd1	takto
chovat	chovat	k5eAaImF	chovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
všech	všecek	k3xTgFnPc2	všecek
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
svatými	svatá	k1gFnPc7	svatá
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
přímluvu	přímluva	k1gFnSc4	přímluva
u	u	k7c2	u
Boha	bůh	k1gMnSc2	bůh
např.	např.	kA	např.
své	svůj	k3xOyFgMnPc4	svůj
mrtvé	mrtvý	k2eAgMnPc4d1	mrtvý
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důsledkem	důsledek	k1gInSc7	důsledek
učení	učení	k1gNnSc2	učení
o	o	k7c4	o
společenství	společenství	k1gNnSc4	společenství
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
za	za	k7c2	za
svatého	svatý	k2eAgMnSc2d1	svatý
považuje	považovat	k5eAaImIp3nS	považovat
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yQnSc4	kdo
za	za	k7c2	za
takového	takový	k3xDgInSc2	takový
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
jeho	jeho	k3xOp3gFnSc1	jeho
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
většiny	většina	k1gFnSc2	většina
církví	církev	k1gFnPc2	církev
katolických	katolický	k2eAgMnPc2d1	katolický
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
ortodoxních	ortodoxní	k2eAgMnPc2d1	ortodoxní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
Svatý	svatý	k2eAgMnSc1d1	svatý
posledním	poslední	k2eAgInSc7d1	poslední
stupněm	stupeň	k1gInSc7	stupeň
formální	formální	k2eAgFnPc4d1	formální
kanonizace	kanonizace	k1gFnSc2	kanonizace
(	(	kIx(	(
<g/>
svatořečení	svatořečení	k1gNnSc2	svatořečení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přesně	přesně	k6eAd1	přesně
vymezený	vymezený	k2eAgInSc4d1	vymezený
formální	formální	k2eAgInSc4d1	formální
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
zkoumány	zkoumán	k2eAgFnPc4d1	zkoumána
zásluhy	zásluha	k1gFnPc4	zásluha
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
a	a	k8xC	a
zázraky	zázrak	k1gInPc1	zázrak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojeny	spojen	k2eAgInPc1d1	spojen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
světce	světec	k1gMnPc4	světec
tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
projde	projít	k5eAaPmIp3nS	projít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
svatý	svatý	k1gMnSc1	svatý
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
oficielně	oficielně	k6eAd1	oficielně
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
<g/>
.	.	kIx.	.
</s>
<s>
Světci	světec	k1gMnSc3	světec
je	být	k5eAaImIp3nS	být
vyhrazen	vyhrazen	k2eAgInSc4d1	vyhrazen
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
slaven	slaven	k2eAgInSc4d1	slaven
jeho	jeho	k3xOp3gInSc4	jeho
svátek	svátek	k1gInSc4	svátek
u	u	k7c2	u
oltáře	oltář	k1gInSc2	oltář
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mu	on	k3xPp3gMnSc3	on
být	být	k5eAaImF	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
věřící	věřící	k1gMnPc1	věřící
jej	on	k3xPp3gMnSc4	on
žádají	žádat	k5eAaImIp3nP	žádat
veřejně	veřejně	k6eAd1	veřejně
o	o	k7c4	o
přímluvu	přímluva	k1gFnSc4	přímluva
u	u	k7c2	u
Boha	bůh	k1gMnSc2	bůh
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
církvích	církev	k1gFnPc6	církev
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
princip	princip	k1gInSc1	princip
svatých	svatá	k1gFnPc2	svatá
coby	coby	k?	coby
prostředníků	prostředník	k1gMnPc2	prostředník
přijímán	přijímat	k5eAaImNgMnS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
se	s	k7c7	s
slovo	slovo	k1gNnSc1	slovo
svatý	svatý	k2eAgInSc4d1	svatý
významem	význam	k1gInSc7	význam
překrývá	překrývat	k5eAaImIp3nS	překrývat
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
terminologii	terminologie	k1gFnSc4	terminologie
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Svatí	svatý	k1gMnPc1	svatý
buďte	budit	k5eAaImRp2nP	budit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
já	já	k3xPp1nSc1	já
Jsem	být	k5eAaImIp1nS	být
svatý	svatý	k2eAgMnSc1d1	svatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neuctívání	neuctívání	k1gNnSc1	neuctívání
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
svatých	svatá	k1gFnPc2	svatá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
u	u	k7c2	u
protestantů	protestant	k1gMnPc2	protestant
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
-	-	kIx~	-
Bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Veškerá	veškerý	k3xTgFnSc1	veškerý
sláva	sláva	k1gFnSc1	sláva
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc1	čest
i	i	k8xC	i
moc	moc	k1gFnSc1	moc
patří	patřit	k5eAaImIp3nS	patřit
Bohu	bůh	k1gMnSc3	bůh
našemu	náš	k3xOp1gNnSc3	náš
-	-	kIx~	-
a	a	k8xC	a
ne	ne	k9	ne
člověku	člověk	k1gMnSc3	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
významných	významný	k2eAgInPc2d1	významný
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
nevážili	vážit	k5eNaImAgMnP	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Prostě	prostě	k6eAd1	prostě
je	on	k3xPp3gMnPc4	on
neuctívají	uctívat	k5eNaImIp3nP	uctívat
a	a	k8xC	a
nemodlí	modlit	k5eNaImIp3nP	modlit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
nestavějí	stavět	k5eNaImIp3nP	stavět
jim	on	k3xPp3gMnPc3	on
poutní	poutní	k2eAgNnPc4d1	poutní
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
sochy	socha	k1gFnPc4	socha
<g/>
.	.	kIx.	.
</s>
<s>
Protestantští	protestantský	k2eAgMnPc1d1	protestantský
věřící	věřící	k1gMnPc1	věřící
mají	mít	k5eAaImIp3nP	mít
nauku	nauka	k1gFnSc4	nauka
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
jediném	jediný	k2eAgNnSc6d1	jediné
prostřednictví	prostřednictví	k1gNnSc6	prostřednictví
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jiného	jiný	k2eAgMnSc4d1	jiný
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
Ježíše	Ježíš	k1gMnSc4	Ježíš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
něhož	jenž	k3xRgMnSc4	jenž
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
spaseni	spasen	k2eAgMnPc1d1	spasen
<g/>
.	.	kIx.	.
</s>
<s>
Přistupují	přistupovat	k5eAaImIp3nP	přistupovat
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
přímo	přímo	k6eAd1	přímo
skrze	skrze	k?	skrze
Ježíše	Ježíš	k1gMnSc2	Ježíš
a	a	k8xC	a
ne	ne	k9	ne
skrze	skrze	k?	skrze
různé	různý	k2eAgNnSc1d1	různé
tzv.	tzv.	kA	tzv.
<g/>
"	"	kIx"	"
<g/>
svaté	svatý	k2eAgFnSc2d1	svatá
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Mají	mít	k5eAaImIp3nP	mít
přímý	přímý	k2eAgInSc4d1	přímý
osobní	osobní	k2eAgInSc4d1	osobní
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
plně	plně	k6eAd1	plně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Božímu	boží	k2eAgInSc3d1	boží
záměru	záměr	k1gInSc3	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
mrtvé	mrtvý	k1gMnPc4	mrtvý
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepřimlouvají	přimlouvat	k5eNaImIp3nP	přimlouvat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
nevěří	věřit	k5eNaImIp3nS	věřit
v	v	k7c4	v
očistec	očistec	k1gInSc4	očistec
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Tak	tak	k9	tak
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
jednou	jednou	k6eAd1	jednou
rodí	rodit	k5eAaImIp3nS	rodit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jednou	jednou	k6eAd1	jednou
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
potom	potom	k6eAd1	potom
bude	být	k5eAaImBp3nS	být
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Přimlouvají	přimlouvat	k5eAaImIp3nP	přimlouvat
se	se	k3xPyFc4	se
za	za	k7c4	za
živé	živý	k2eAgNnSc4d1	živé
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
také	také	k9	také
dostali	dostat	k5eAaPmAgMnP	dostat
milost	milost	k1gFnSc4	milost
ke	k	k7c3	k
spasení	spasení	k1gNnSc3	spasení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Buddhismus	buddhismus	k1gInSc4	buddhismus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
dost	dost	k6eAd1	dost
nepřesně	přesně	k6eNd1	přesně
označován	označován	k2eAgMnSc1d1	označován
probuzený	probuzený	k2eAgMnSc1d1	probuzený
(	(	kIx(	(
<g/>
osvícený	osvícený	k2eAgMnSc1d1	osvícený
<g/>
)	)	kIx)	)
člověk	člověk	k1gMnSc1	člověk
jako	jako	k8xS	jako
svatý	svatý	k2eAgMnSc1d1	svatý
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
stupně	stupeň	k1gInPc1	stupeň
probuzení	probuzení	k1gNnSc2	probuzení
popisovány	popisovat	k5eAaImNgInP	popisovat
jako	jako	k9	jako
stupně	stupeň	k1gInPc4	stupeň
svatosti	svatost	k1gFnSc2	svatost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c6	o
nedorozumění	nedorozumění	k1gNnSc6	nedorozumění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
pojmem	pojem	k1gInSc7	pojem
svatý	svatý	k1gMnSc1	svatý
a	a	k8xC	a
probuzením	probuzení	k1gNnSc7	probuzení
v	v	k7c6	v
buddhismu	buddhismus	k1gInSc6	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Probuzená	probuzený	k2eAgFnSc1d1	probuzená
bytost	bytost	k1gFnSc1	bytost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
pouze	pouze	k6eAd1	pouze
vlastním	vlastní	k2eAgNnSc7d1	vlastní
úsilím	úsilí	k1gNnSc7	úsilí
bez	bez	k7c2	bez
přispění	přispění	k1gNnSc2	přispění
jiné	jiný	k2eAgFnSc2d1	jiná
nadpřirozené	nadpřirozený	k2eAgFnSc2d1	nadpřirozená
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
guru	guru	k1gMnSc1	guru
či	či	k8xC	či
učitel	učitel	k1gMnSc1	učitel
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
úlohu	úloha	k1gFnSc4	úloha
instruktora	instruktor	k1gMnSc2	instruktor
<g/>
.	.	kIx.	.
</s>
<s>
Probuzení	probuzení	k1gNnSc1	probuzení
v	v	k7c6	v
buddhismu	buddhismus	k1gInSc6	buddhismus
je	být	k5eAaImIp3nS	být
popisováno	popisovat	k5eAaImNgNnS	popisovat
jako	jako	k9	jako
vykořenění	vykořenění	k1gNnSc3	vykořenění
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
tří	tři	k4xCgInPc2	tři
zel	zlo	k1gNnPc2	zlo
-	-	kIx~	-
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
,	,	kIx,	,
nevědomosti	nevědomost	k1gFnSc2	nevědomost
a	a	k8xC	a
chtivosti	chtivost	k1gFnSc2	chtivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
buddhistické	buddhistický	k2eAgFnSc6d1	buddhistická
škole	škola	k1gFnSc6	škola
neexistuje	existovat	k5eNaImIp3nS	existovat
oficiální	oficiální	k2eAgInSc1d1	oficiální
seznam	seznam	k1gInSc1	seznam
svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
probuzených	probuzený	k2eAgNnPc2d1	probuzené
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Probuzení	probuzení	k1gNnSc1	probuzení
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgFnSc7d1	osobní
záležitostí	záležitost	k1gFnSc7	záležitost
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
buddhismu	buddhismus	k1gInSc2	buddhismus
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
žádat	žádat	k5eAaImF	žádat
přímluvu	přímluva	k1gFnSc4	přímluva
u	u	k7c2	u
probuzené	probuzený	k2eAgFnSc2d1	probuzená
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
očistit	očistit	k5eAaPmF	očistit
mysl	mysl	k1gFnSc4	mysl
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
kolem	kolem	k7c2	kolem
významných	významný	k2eAgFnPc2d1	významná
existujících	existující	k2eAgFnPc2d1	existující
i	i	k8xC	i
legendárních	legendární	k2eAgFnPc2d1	legendární
postav	postava	k1gFnPc2	postava
buddhismu	buddhismus	k1gInSc2	buddhismus
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
lidových	lidový	k2eAgInPc2d1	lidový
kultů	kult	k1gInPc2	kult
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
připomínají	připomínat	k5eAaImIp3nP	připomínat
uctívání	uctívání	k1gNnPc4	uctívání
světců	světec	k1gMnPc2	světec
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
mahájánovém	mahájánový	k2eAgInSc6d1	mahájánový
buddhismu	buddhismus	k1gInSc6	buddhismus
je	být	k5eAaImIp3nS	být
jejími	její	k3xOp3gNnPc7	její
stoupenci	stoupenec	k1gMnPc7	stoupenec
vyhledávána	vyhledáván	k2eAgFnSc1d1	vyhledávána
ochrana	ochrana	k1gFnSc1	ochrana
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
transcendentních	transcendentní	k2eAgMnPc2d1	transcendentní
bódhisattvů	bódhisattv	k1gMnPc2	bódhisattv
a	a	k8xC	a
buddhů	buddha	k1gMnPc2	buddha
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
bódhisattva	bódhisattva	k6eAd1	bódhisattva
Džizó	Džizó	k1gFnPc2	Džizó
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
ochránce	ochránce	k1gMnPc4	ochránce
poutníků	poutník	k1gMnPc2	poutník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
ke	k	k7c3	k
Kuan-jin	Kuanin	k1gInSc1	Kuan-jin
(	(	kIx(	(
<g/>
ženská	ženský	k2eAgFnSc1d1	ženská
podoba	podoba	k1gFnSc1	podoba
bódhisattvy	bódhisattva	k1gFnSc2	bódhisattva
Avalókitéšvary	Avalókitéšvar	k1gInPc1	Avalókitéšvar
<g/>
)	)	kIx)	)
neplodné	plodný	k2eNgFnPc1d1	neplodná
ženy	žena	k1gFnPc1	žena
obracejí	obracet	k5eAaImIp3nP	obracet
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
narození	narození	k1gNnSc4	narození
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hinduismus	hinduismus	k1gInSc4	hinduismus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
hinduismu	hinduismus	k1gInSc6	hinduismus
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
uznáváni	uznávat	k5eAaImNgMnP	uznávat
svatí	svatý	k2eAgMnPc1d1	svatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
nalezení	nalezení	k1gNnSc3	nalezení
není	být	k5eNaImIp3nS	být
určen	určen	k2eAgInSc4d1	určen
formální	formální	k2eAgInSc4d1	formální
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
mučedníků	mučedník	k1gMnPc2	mučedník
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
</s>
</p>
<p>
<s>
svěcení	svěcení	k1gNnSc1	svěcení
</s>
</p>
<p>
<s>
Jurodiví	jurodivý	k2eAgMnPc1d1	jurodivý
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
svatý	svatý	k2eAgMnSc1d1	svatý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
svatý	svatý	k1gMnSc1	svatý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
