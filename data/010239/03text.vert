<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Potzinger	Potzinger	k1gMnSc1	Potzinger
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1870	[number]	k4	1870
Oberpurkla	Oberpurkla	k1gFnSc2	Oberpurkla
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1933	[number]	k4	1933
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
křesťansko	křesťansko	k6eAd1	křesťansko
sociální	sociální	k2eAgMnSc1d1	sociální
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
poslanec	poslanec	k1gMnSc1	poslanec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vychodil	vychodit	k5eAaImAgInS	vychodit
národní	národní	k2eAgFnSc4d1	národní
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
pak	pak	k6eAd1	pak
teologii	teologie	k1gFnSc4	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
kaplan	kaplan	k1gMnSc1	kaplan
v	v	k7c6	v
Judenburgu	Judenburg	k1gInSc6	Judenburg
<g/>
,	,	kIx,	,
Rottenmannu	Rottenmann	k1gInSc6	Rottenmann
<g/>
,	,	kIx,	,
Mitterndorfu	Mitterndorf	k1gInSc6	Mitterndorf
a	a	k8xC	a
Sankt	Sankt	k1gInSc1	Sankt
Lorenzen	Lorenzen	k2eAgInSc1d1	Lorenzen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
byl	být	k5eAaImAgInS	být
farářem	farář	k1gMnSc7	farář
v	v	k7c6	v
Abstallu	Abstall	k1gInSc6	Abstall
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgMnS	angažovat
se	se	k3xPyFc4	se
v	v	k7c4	v
Křesťansko	Křesťansko	k1gNnSc4	Křesťansko
sociální	sociální	k2eAgFnSc6d1	sociální
straně	strana	k1gFnSc6	strana
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
i	i	k9	i
do	do	k7c2	do
celostátní	celostátní	k2eAgFnSc2d1	celostátní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
získal	získat	k5eAaPmAgInS	získat
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgInSc1d1	celostátní
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Usedl	usednout	k5eAaPmAgMnS	usednout
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
frakce	frakce	k1gFnSc2	frakce
Křesťansko-sociální	křesťanskoociální	k2eAgInSc1d1	křesťansko-sociální
klub	klub	k1gInSc1	klub
německých	německý	k2eAgMnPc2d1	německý
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
parlamentu	parlament	k1gInSc6	parlament
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
<g/>
Profesně	profesně	k6eAd1	profesně
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1911	[number]	k4	1911
uváděn	uváděn	k2eAgMnSc1d1	uváděn
jako	jako	k8xS	jako
farář	farář	k1gMnSc1	farář
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
jako	jako	k8xC	jako
poslanec	poslanec	k1gMnSc1	poslanec
Provizorního	provizorní	k2eAgNnSc2d1	provizorní
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Německého	německý	k2eAgNnSc2d1	německé
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
Provisorische	Provisorische	k1gFnSc1	Provisorische
Nationalversammlung	Nationalversammlung	k1gMnSc1	Nationalversammlung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
