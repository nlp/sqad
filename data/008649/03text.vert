<p>
<s>
Arnaud	Arnaud	k1gMnSc1	Arnaud
kardinál	kardinál	k1gMnSc1	kardinál
Nouvel	Nouvel	k1gMnSc1	Nouvel
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1317	[number]	k4	1317
Avignon	Avignon	k1gInSc1	Avignon
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
kardinál-kněz	kardinálněza	k1gFnPc2	kardinál-kněza
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
strýcem	strýc	k1gMnSc7	strýc
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
Saverdunu	Saverdun	k1gInSc2	Saverdun
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opatem	opat	k1gMnSc7	opat
kláštera	klášter	k1gInSc2	klášter
Fontfroide	Fontfroid	k1gInSc5	Fontfroid
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1310	[number]	k4	1310
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1307-1316	[number]	k4	1307-1316
byl	být	k5eAaImAgMnS	být
vicekancléřem	vicekancléř	k1gMnSc7	vicekancléř
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1307	[number]	k4	1307
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
biskupa	biskup	k1gMnSc2	biskup
Bernarda	Bernard	k1gMnSc2	Bernard
de	de	k?	de
Castanet	Castanet	k1gMnSc1	Castanet
<g/>
,	,	kIx,	,
obviněného	obviněný	k2eAgMnSc2d1	obviněný
ze	z	k7c2	z
zneužívání	zneužívání	k1gNnSc2	zneužívání
inkvizičních	inkviziční	k2eAgNnPc2d1	inkviziční
soudních	soudní	k2eAgNnPc2d1	soudní
řízení	řízení	k1gNnPc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
papeže	papež	k1gMnSc2	papež
Klementa	Klement	k1gMnSc2	Klement
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1310	[number]	k4	1310
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
konkláve	konkláve	k1gNnSc4	konkláve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1316	[number]	k4	1316
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1317	[number]	k4	1317
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Fontfroide	Fontfroid	k1gInSc5	Fontfroid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Klement	Klement	k1gMnSc1	Klement
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Cardinals	Cardinals	k1gInSc1	Cardinals
of	of	k?	of
the	the	k?	the
Holy	hola	k1gFnSc2	hola
Roman	Roman	k1gMnSc1	Roman
Church	Church	k1gMnSc1	Church
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
