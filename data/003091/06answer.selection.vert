<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
je	být	k5eAaImIp3nS	být
arabská	arabský	k2eAgFnSc1d1	arabská
země	země	k1gFnSc1	země
s	s	k7c7	s
islámskou	islámský	k2eAgFnSc7d1	islámská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
98	[number]	k4	98
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tuniska	Tunisko	k1gNnSc2	Tunisko
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgFnSc1d1	zbylá
2	[number]	k4	2
%	%	kIx~	%
představují	představovat	k5eAaImIp3nP	představovat
židé	žid	k1gMnPc1	žid
a	a	k8xC	a
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
