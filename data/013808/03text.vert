<s>
Molekula	molekula	k1gFnSc1
</s>
<s>
Různá	různý	k2eAgNnPc1d1
vyobrazení	vyobrazení	k1gNnPc1
molekul	molekula	k1gFnPc2
</s>
<s>
Molekula	molekula	k1gFnSc1
je	být	k5eAaImIp3nS
částice	částice	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
atomů	atom	k1gInPc2
nebo	nebo	k8xC
iontů	ion	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
důležitým	důležitý	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
chemie	chemie	k1gFnSc2
a	a	k8xC
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
už	už	k9
název	název	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ústředním	ústřední	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
molekulové	molekulový	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
a	a	k8xC
kinetické	kinetický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
částici	částice	k1gFnSc4
<g/>
,	,	kIx,
představující	představující	k2eAgFnSc4d1
minimální	minimální	k2eAgNnSc4d1
množství	množství	k1gNnSc4
dané	daný	k2eAgFnSc2d1
látky	látka	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
chemické	chemický	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
a	a	k8xC
určuje	určovat	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
fyzikální	fyzikální	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
látky	látka	k1gFnSc2
obsažené	obsažený	k2eAgFnSc2d1
v	v	k7c6
částici	částice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Molekuly	molekula	k1gFnPc4
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
tvořeny	tvořit	k5eAaImNgInP
více	hodně	k6eAd2
vázanými	vázaný	k2eAgInPc7d1
atomy	atom	k1gInPc7
(	(	kIx(
<g/>
obsahují	obsahovat	k5eAaImIp3nP
více	hodně	k6eAd2
atomových	atomový	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
a	a	k8xC
sdílený	sdílený	k2eAgInSc4d1
atomový	atomový	k2eAgInSc4d1
obal	obal	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
je	být	k5eAaImIp3nS
zprostředkována	zprostředkován	k2eAgFnSc1d1
jejich	jejich	k3xOp3gFnSc1
vazba	vazba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
pohledu	pohled	k1gInSc2
molekulové	molekulový	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
však	však	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
jednoatomové	jednoatomový	k2eAgFnSc2d1
<g/>
,	,	kIx,
např.	např.	kA
u	u	k7c2
vzácných	vzácný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
buď	buď	k8xC
elektricky	elektricky	k6eAd1
neutrální	neutrální	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
mohou	moct	k5eAaImIp3nP
nést	nést	k5eAaImF
kladný	kladný	k2eAgInSc4d1
nebo	nebo	k8xC
záporný	záporný	k2eAgInSc4d1
náboj	náboj	k1gInSc4
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
ionty	ion	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
polarity	polarita	k1gFnSc2
náboje	náboj	k1gInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
molekulových	molekulový	k2eAgInPc6d1
kationtech	kation	k1gInPc6
nebo	nebo	k8xC
molekulových	molekulový	k2eAgInPc6d1
aniontech	anion	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
molekulové	molekulový	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
vymezuje	vymezovat	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc2d1
fyzikální	fyzikální	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
pojem	pojem	k1gInSc1
molekuly	molekula	k1gFnSc2
přísnější	přísný	k2eAgFnSc7d2
definicí	definice	k1gFnSc7
<g/>
,	,	kIx,
vylučující	vylučující	k2eAgFnSc2d1
jednoatomové	jednoatomový	k2eAgFnSc2d1
molekuly	molekula	k1gFnSc2
nebo	nebo	k8xC
molekulové	molekulový	k2eAgInPc4d1
ionty	ion	k1gInPc4
a	a	k8xC
zahrnující	zahrnující	k2eAgInSc4d1
i	i	k9
požadavek	požadavek	k1gInSc4
na	na	k7c4
minimální	minimální	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
vazby	vazba	k1gFnSc2
atomů	atom	k1gInPc2
v	v	k7c6
molekule	molekula	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
Molekula	molekula	k1gFnSc1
je	být	k5eAaImIp3nS
elektricky	elektricky	k6eAd1
neutrální	neutrální	k2eAgFnSc1d1
entita	entita	k1gFnSc1
sestávající	sestávající	k2eAgFnSc1d1
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
jednoho	jeden	k4xCgInSc2
atomu	atom	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
hladina	hladina	k1gFnSc1
jejího	její	k3xOp3gInSc2
vazbového	vazbový	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
musí	muset	k5eAaImIp3nS
vykazovat	vykazovat	k5eAaImF
snížení	snížení	k1gNnSc1
umožňující	umožňující	k2eAgNnSc1d1
obsáhnout	obsáhnout	k5eAaPmF
alespoň	alespoň	k9
jeden	jeden	k4xCgInSc4
vibrační	vibrační	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Molekula	molekula	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
kvantum	kvantum	k1gNnSc1
prvků	prvek	k1gInPc2
i	i	k8xC
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
může	moct	k5eAaImIp3nS
samostatně	samostatně	k6eAd1
existovat	existovat	k5eAaImF
v	v	k7c6
plynném	plynný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
kondenzovaném	kondenzovaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
látek	látka	k1gFnPc2
molekula	molekula	k1gFnSc1
jako	jako	k8xS,k8xC
částice	částice	k1gFnSc1
často	často	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
smysl	smysl	k1gInSc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
rozsáhlejší	rozsáhlý	k2eAgFnSc2d2
struktury	struktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
mnoho	mnoho	k4c1
kapalin	kapalina	k1gFnPc2
ještě	ještě	k6eAd1
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
za	za	k7c4
slabě	slabě	k6eAd1
vázané	vázaný	k2eAgFnPc4d1
molekuly	molekula	k1gFnPc4
<g/>
,	,	kIx,
u	u	k7c2
pevných	pevný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
možné	možný	k2eAgNnSc1d1
pouze	pouze	k6eAd1
u	u	k7c2
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
struktura	struktura	k1gFnSc1
(	(	kIx(
<g/>
krystalická	krystalický	k2eAgFnSc1d1
<g/>
,	,	kIx,
kvazikrystalická	kvazikrystalický	k2eAgFnSc1d1
či	či	k8xC
amorfní	amorfní	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
držena	držet	k5eAaImNgFnS
namísto	namísto	k7c2
kovalentních	kovalentní	k2eAgInPc2d1
<g/>
,	,	kIx,
koordinačně	koordinačně	k6eAd1
kovalentních	kovalentní	k2eAgFnPc2d1
<g/>
,	,	kIx,
iontových	iontový	k2eAgFnPc2d1
či	či	k8xC
kovových	kovový	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
pouze	pouze	k6eAd1
relativně	relativně	k6eAd1
slabými	slabý	k2eAgFnPc7d1
mezimolekulovými	mezimolekulův	k2eAgFnPc7d1
interakcemi	interakce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
objevitele	objevitel	k1gMnSc2
molekuly	molekula	k1gFnSc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
Amedeo	Amedeo	k6eAd1
Avogadro	Avogadra	k1gFnSc5
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
r.	r.	kA
1811	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
výsledky	výsledek	k1gInPc1
své	svůj	k3xOyFgFnPc4
práce	práce	k1gFnPc4
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
vysvětlil	vysvětlit	k5eAaPmAgMnS
rozpor	rozpor	k1gInSc4
Daltonova	Daltonův	k2eAgNnSc2d1
atomového	atomový	k2eAgNnSc2d1
vysvětlení	vysvětlení	k1gNnSc2
hmotnostních	hmotnostní	k2eAgInPc2d1
slučovacích	slučovací	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
a	a	k8xC
Gay-Lussacem	Gay-Lussace	k1gNnSc7
zjištěných	zjištěný	k2eAgInPc2d1
objemových	objemový	k2eAgInPc2d1
slučovacích	slučovací	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
a	a	k8xC
formuloval	formulovat	k5eAaImAgMnS
i	i	k9
tvrzení	tvrzení	k1gNnSc4
zvané	zvaný	k2eAgNnSc4d1
dnes	dnes	k6eAd1
Avogadrův	Avogadrův	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
plný	plný	k2eAgInSc4d1
průkaz	průkaz	k1gInSc4
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
až	až	k9
o	o	k7c4
plných	plný	k2eAgNnPc2d1
100	#num#	k4
let	léto	k1gNnPc2
mladší	mladý	k2eAgFnSc1d2
syntéza	syntéza	k1gFnSc1
výsledků	výsledek	k1gInPc2
experimentálního	experimentální	k2eAgNnSc2d1
zkoumání	zkoumání	k1gNnSc2
hustoty	hustota	k1gFnSc2
a	a	k8xC
viskozity	viskozita	k1gFnSc2
kapalin	kapalina	k1gFnPc2
<g/>
,	,	kIx,
kinetické	kinetický	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
a	a	k8xC
teorie	teorie	k1gFnSc1
Brownova	Brownův	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
prezentoval	prezentovat	k5eAaBmAgMnS
Jean	Jean	k1gMnSc1
Perrin	Perrin	k1gInSc4
na	na	k7c6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Solvayově	Solvayův	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
konaném	konaný	k2eAgInSc6d1
v	v	k7c6
r.	r.	kA
1911	#num#	k4
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
molekul	molekula	k1gFnPc2
</s>
<s>
Podle	podle	k7c2
atomů	atom	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
molekula	molekula	k1gFnSc1
složena	složen	k2eAgFnSc1d1
se	se	k3xPyFc4
molekuly	molekula	k1gFnSc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
</s>
<s>
homonukleární	homonukleárnit	k5eAaPmIp3nS
–	–	k?
Molekuly	molekula	k1gFnSc2
obsahující	obsahující	k2eAgFnPc1d1
pouze	pouze	k6eAd1
atomy	atom	k1gInPc4
stejného	stejný	k2eAgInSc2d1
prvku	prvek	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
H	H	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
O	o	k7c6
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
heteronukleární	heteronukleárnit	k5eAaPmIp3nS
–	–	k?
Molekuly	molekula	k1gFnSc2
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
z	z	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
atomů	atom	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
LiH	LiH	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Molekula	molekula	k1gFnSc1
prvku	prvek	k1gInSc2
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
atomy	atom	k1gInPc7
jednoho	jeden	k4xCgInSc2
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molekula	molekula	k1gFnSc1
sloučeniny	sloučenina	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
atomy	atom	k1gInPc4
různých	různý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
molekuly	molekula	k1gFnSc2
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
energie	energie	k1gFnSc1
systému	systém	k1gInSc2
částic	částice	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyšší	vysoký	k2eAgFnSc1d2
nebo	nebo	k8xC
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
energie	energie	k1gFnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
neinteragujících	interagující	k2eNgFnPc2d1
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
ovlivňováno	ovlivňovat	k5eAaImNgNnS
vzájemným	vzájemný	k2eAgNnSc7d1
působením	působení	k1gNnSc7
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
částicemi	částice	k1gFnPc7
(	(	kIx(
<g/>
popř.	popř.	kA
skupinami	skupina	k1gFnPc7
částic	částice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
interakce	interakce	k1gFnPc1
mezi	mezi	k7c7
částicemi	částice	k1gFnPc7
snižuje	snižovat	k5eAaImIp3nS
celkovou	celkový	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
vzájemnému	vzájemný	k2eAgNnSc3d1
přitahování	přitahování	k1gNnSc3
částic	částice	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgMnPc4
mohou	moct	k5eAaImIp3nP
vytvořit	vytvořit	k5eAaPmF
stabilní	stabilní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
interakce	interakce	k1gFnPc1
mezi	mezi	k7c7
částicemi	částice	k1gFnPc7
celkovou	celkový	k2eAgFnSc4d1
energii	energie	k1gFnSc4
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
částice	částice	k1gFnPc1
odpuzovány	odpuzován	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
nemohou	moct	k5eNaImIp3nP
vytvořit	vytvořit	k5eAaPmF
stabilní	stabilní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molekula	molekula	k1gFnSc1
tedy	tedy	k9
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
atomy	atom	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
skládat	skládat	k5eAaImF
<g/>
,	,	kIx,
vzájemně	vzájemně	k6eAd1
přitahovány	přitahován	k2eAgInPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
celková	celkový	k2eAgFnSc1d1
energie	energie	k1gFnSc1
molekuly	molekula	k1gFnSc2
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
energie	energie	k1gFnSc1
samostatných	samostatný	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
části	část	k1gFnPc1
molekuly	molekula	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
atomy	atom	k1gInPc1
<g/>
)	)	kIx)
drží	držet	k5eAaImIp3nP
pohromadě	pohromadě	k6eAd1
síly	síla	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nazýváme	nazývat	k5eAaImIp1nP
chemické	chemický	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemická	chemický	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
elektrické	elektrický	k2eAgFnSc6d1
interakci	interakce	k1gFnSc6
nabitých	nabitý	k2eAgFnPc2d1
částic	částice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
tvoří	tvořit	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
atomy	atom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síly	síl	k1gInPc1
působící	působící	k2eAgInPc1d1
mezi	mezi	k7c7
atomy	atom	k1gInPc7
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k9
výměnné	výměnný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výměnné	výměnný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
umožňují	umožňovat	k5eAaImIp3nP
popsat	popsat	k5eAaPmF
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
vlastností	vlastnost	k1gFnPc2
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Chemickou	chemický	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
můžeme	moct	k5eAaImIp1nP
definovat	definovat	k5eAaBmF
podle	podle	k7c2
rozdílu	rozdíl	k1gInSc2
elektronegativit	elektronegativita	k1gFnPc2
iontů	ion	k1gInPc2
jako	jako	k8xS,k8xC
kovovou	kovový	k2eAgFnSc4d1
<g/>
,	,	kIx,
kovalentní	kovalentní	k2eAgFnSc4d1
<g/>
,	,	kIx,
polární	polární	k2eAgFnSc4d1
nebo	nebo	k8xC
iontovou	iontový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Energie	energie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
molekule	molekula	k1gFnSc3
nutno	nutno	k6eAd1
dodat	dodat	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
jejímu	její	k3xOp3gInSc3
rozpadu	rozpad	k1gInSc3
na	na	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
atomy	atom	k1gInPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
disociační	disociační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
jev	jev	k1gInSc1
rozpadu	rozpad	k1gInSc2
molekuly	molekula	k1gFnSc2
při	při	k7c6
dodání	dodání	k1gNnSc6
disociační	disociační	k2eAgFnSc2d1
energie	energie	k1gFnSc2
pak	pak	k6eAd1
nazýváme	nazývat	k5eAaImIp1nP
disociací	disociace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molekuly	molekula	k1gFnPc1
jsou	být	k5eAaImIp3nP
základní	základní	k2eAgInPc4d1
stavební	stavební	k2eAgInPc4d1
kameny	kámen	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
jsou	být	k5eAaImIp3nP
vybudována	vybudován	k2eAgNnPc1d1
hmotná	hmotný	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různý	různý	k2eAgInSc1d1
způsob	způsob	k1gInSc1
uspořádání	uspořádání	k1gNnSc2
a	a	k8xC
vzájemného	vzájemný	k2eAgNnSc2d1
silového	silový	k2eAgNnSc2d1
působení	působení	k1gNnSc2
základních	základní	k2eAgFnPc2d1
částic	částice	k1gFnPc2
v	v	k7c6
tělesech	těleso	k1gNnPc6
pak	pak	k6eAd1
určuje	určovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnPc4
různé	různý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
HOUDEK	Houdek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molekula	molekula	k1gFnSc1
–	–	k?
tři	tři	k4xCgNnPc4
jubilea	jubileum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
-	-	kIx~
<g/>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesmír	vesmír	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Únor	únor	k1gInSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
90	#num#	k4
<g/>
(	(	kIx(
<g/>
141	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
76	#num#	k4
<g/>
-	-	kIx~
<g/>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
4544	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MULLER	MULLER	kA
<g/>
,	,	kIx,
P.	P.	kA
Glossary	Glossara	k1gFnPc1
of	of	k?
terms	terms	k1gInSc1
used	used	k1gMnSc1
in	in	k?
physical	physicat	k5eAaPmAgMnS
organic	organice	k1gFnPc2
chemistry	chemistr	k1gMnPc7
(	(	kIx(
<g/>
IUPAC	IUPAC	kA
Recommendations	Recommendations	k1gInSc4
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1077	#num#	k4
<g/>
–	–	k?
<g/>
1184	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pure	Pur	k1gInSc2
and	and	k?
Applied	Applied	k1gMnSc1
Chemistry	Chemistr	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květen	květen	k1gInSc1
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
66	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1077	#num#	k4
<g/>
–	–	k?
<g/>
1184	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1365	#num#	k4
<g/>
-	-	kIx~
<g/>
3075	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.135	10.135	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
pac	pac	k1gInSc1
<g/>
199466051077	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Editor	editor	k1gInSc1
molekul	molekula	k1gFnPc2
</s>
<s>
Atom	atom	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
molekula	molekula	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Molekula	molekula	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
molekula	molekula	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Částice	částice	k1gFnSc1
Elementární	elementární	k2eAgFnSc2d1
částice	částice	k1gFnSc2
</s>
<s>
částice	částice	k1gFnSc1
hmoty	hmota	k1gFnSc2
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
kvarky	kvark	k1gInPc1
</s>
<s>
kvark	kvark	k1gInSc1
u	u	k7c2
·	·	k?
antikvark	antikvark	k1gInSc1
u	u	k7c2
·	·	k?
kvark	kvark	k1gInSc1
d	d	k?
·	·	k?
antikvark	antikvark	k1gInSc1
d	d	k?
·	·	k?
kvark	kvark	k1gInSc1
s	s	k7c7
·	·	k?
antikvark	antikvark	k1gInSc1
s	s	k7c7
·	·	k?
kvark	kvark	k1gInSc1
c	c	k0
·	·	k?
antikvark	antikvark	k1gInSc1
c	c	k0
·	·	k?
kvark	kvark	k1gInSc1
t	t	k?
·	·	k?
antikvark	antikvark	k1gInSc1
t	t	k?
·	·	k?
kvark	kvark	k1gInSc1
b	b	k?
·	·	k?
antikvark	antikvark	k1gInSc4
b	b	k?
leptony	lepton	k1gInPc4
</s>
<s>
elektron	elektron	k1gInSc1
·	·	k?
pozitron	pozitron	k1gInSc1
·	·	k?
mion	mion	k1gInSc1
·	·	k?
antimion	antimion	k1gInSc1
·	·	k?
tauon	tauon	k1gMnSc1
·	·	k?
antitauon	antitauon	k1gMnSc1
·	·	k?
elektronové	elektronový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
elektronové	elektronový	k2eAgNnSc4d1
antineutrino	antineutrino	k1gNnSc4
·	·	k?
mionové	mionový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
mionové	mionový	k2eAgNnSc4d1
antineutrino	antineutrino	k1gNnSc4
·	·	k?
tauonové	tauonový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
tauonové	tauonová	k1gFnSc2
antineutrino	antineutrin	k2eAgNnSc1d1
</s>
<s>
částice	částice	k1gFnSc1
interakcí	interakce	k1gFnPc2
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
foton	foton	k1gInSc1
·	·	k?
gluon	gluon	k1gInSc1
·	·	k?
bosony	bosona	k1gFnSc2
W	W	kA
a	a	k8xC
Z	z	k7c2
·	·	k?
Higgsův	Higgsův	k2eAgMnSc1d1
boson	boson	k1gMnSc1
hypotetické	hypotetický	k2eAgFnSc2d1
</s>
<s>
částice	částice	k1gFnSc1
interakcí	interakce	k1gFnPc2
</s>
<s>
graviton	graviton	k1gInSc1
·	·	k?
bosony	bosona	k1gFnSc2
X	X	kA
a	a	k8xC
Y	Y	kA
·	·	k?
bosony	bosona	k1gFnPc4
W	W	kA
<g/>
'	'	kIx"
a	a	k8xC
Z	z	k7c2
<g/>
'	'	kIx"
·	·	k?
majoron	majoron	k1gInSc1
·	·	k?
duální	duální	k2eAgInSc1d1
graviton	graviton	k1gInSc1
superpartneři	superpartner	k1gMnPc1
</s>
<s>
gluino	gluino	k1gNnSc1
·	·	k?
gravitino	gravitin	k2eAgNnSc1d1
·	·	k?
chargino	chargino	k1gNnSc1
·	·	k?
neutralino	utralin	k2eNgNnSc1d1
·	·	k?
(	(	kIx(
<g/>
fotino	fotino	k1gNnSc1
·	·	k?
higgsino	higgsin	k2eAgNnSc1d1
·	·	k?
wino	wino	k1gNnSc1
·	·	k?
zino	zino	k1gMnSc1
<g/>
)	)	kIx)
·	·	k?
slepton	slepton	k1gInSc1
·	·	k?
skvark	skvark	k1gInSc1
·	·	k?
axino	axino	k6eAd1
ostatní	ostatní	k2eAgFnPc1d1
</s>
<s>
axion	axion	k1gInSc1
·	·	k?
dilaton	dilaton	k1gInSc1
·	·	k?
magnetický	magnetický	k2eAgInSc1d1
monopól	monopól	k1gInSc1
·	·	k?
Planckova	Planckov	k1gInSc2
částice	částice	k1gFnSc2
·	·	k?
preon	preon	k1gMnSc1
·	·	k?
sterilní	sterilní	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
tachyon	tachyon	k1gMnSc1
</s>
<s>
Složené	složený	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
hadrony	hadron	k1gInPc4
</s>
<s>
baryony	baryon	k1gInPc1
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
nukleony	nukleon	k1gInPc1
</s>
<s>
proton	proton	k1gInSc1
·	·	k?
antiproton	antiproton	k1gInSc1
·	·	k?
neutron	neutron	k1gInSc1
·	·	k?
antineutron	antineutron	k1gInSc1
hyperony	hyperon	k1gMnPc7
</s>
<s>
Δ	Δ	k?
·	·	k?
Λ	Λ	k?
·	·	k?
Σ	Σ	k?
<g/>
,	,	kIx,
Σ	Σ	k?
<g/>
*	*	kIx~
·	·	k?
Ξ	Ξ	k?
<g/>
,	,	kIx,
Ξ	Ξ	k?
<g/>
*	*	kIx~
·	·	k?
Ω	Ω	k?
ostatní	ostatní	k2eAgFnSc2d1
baryonové	baryonový	k2eAgFnSc2d1
rezonance	rezonance	k1gFnSc2
</s>
<s>
N	N	kA
·	·	k?
Δ	Δ	k?
·	·	k?
Λ	Λ	k?
<g/>
,	,	kIx,
Λ	Λ	k1gInSc1
<g/>
,	,	kIx,
Λ	Λ	k1gInSc1
·	·	k?
Σ	Σ	k?
<g/>
,	,	kIx,
Σ	Σ	k1gInSc1
<g/>
,	,	kIx,
Σ	Σ	k1gInSc1
·	·	k?
Ξ	Ξ	k?
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
·	·	k?
Ω	Ω	k?
<g/>
,	,	kIx,
Ω	Ω	k1gInSc1
<g/>
,	,	kIx,
Ω	Ω	k1gInSc1
</s>
<s>
mezony	mezon	k1gInPc1
<g/>
/	/	kIx~
<g/>
kvarkonia	kvarkonium	k1gNnPc1
<g/>
(	(	kIx(
<g/>
bosony	boson	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
pion	pion	k1gInSc1
·	·	k?
kaon	kaon	k1gInSc1
·	·	k?
ρ	ρ	k?
·	·	k?
η	η	k?
·	·	k?
φ	φ	k?
·	·	k?
ω	ω	k?
·	·	k?
J	J	kA
<g/>
/	/	kIx~
<g/>
ψ	ψ	k?
·	·	k?
ϒ	ϒ	k?
·	·	k?
θ	θ	k?
·	·	k?
B	B	kA
·	·	k?
D	D	kA
·	·	k?
T	T	kA
exotické	exotický	k2eAgInPc4d1
hadrony	hadron	k1gInPc4
</s>
<s>
tetrakvarky	tetrakvarka	k1gFnPc1
<g/>
/	/	kIx~
<g/>
dvoumezonové	dvoumezonový	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
X	X	kA
<g/>
(	(	kIx(
<g/>
2900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
3872	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4140	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
s	s	k7c7
excitacemi	excitace	k1gFnPc7
X	X	kA
<g/>
(	(	kIx(
<g/>
4274	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
X	X	kA
<g/>
(	(	kIx(
<g/>
4500	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
X	X	kA
<g/>
(	(	kIx(
<g/>
4700	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
))	))	k?
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4630	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4685	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
5568	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
6900	#num#	k4
<g/>
)	)	kIx)
<g/>
Zc	Zc	k1gFnSc2
<g/>
(	(	kIx(
<g/>
3900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Zc	Zc	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4430	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Zcs	Zcs	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4000	#num#	k4
<g/>
)	)	kIx)
<g/>
+	+	kIx~
·	·	k?
Zcs	Zcs	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4220	#num#	k4
<g/>
)	)	kIx)
<g/>
+	+	kIx~
pentakvarky	pentakvark	k1gInPc1
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Pc	Pc	k?
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4312	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4380	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4440	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hexakvarky	hexakvarka	k1gFnPc1
<g/>
/	/	kIx~
<g/>
dibaryony	dibaryona	k1gFnPc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
d	d	k?
<g/>
*	*	kIx~
<g/>
(	(	kIx(
<g/>
2380	#num#	k4
<g/>
)	)	kIx)
glueballs	glueballs	k1gInSc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
odderon	odderon	k1gMnSc1
·	·	k?
pomeron	pomeron	k1gMnSc1
(	(	kIx(
<g/>
hypotetický	hypotetický	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
další	další	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
atomové	atomový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
(	(	kIx(
<g/>
deuteron	deuteron	k1gInSc1
·	·	k?
triton	triton	k1gInSc1
·	·	k?
helion	helion	k1gInSc1
·	·	k?
částice	částice	k1gFnSc1
alfa	alfa	k1gFnSc1
·	·	k?
hyperjádro	hyperjádro	k6eAd1
<g/>
)	)	kIx)
·	·	k?
atom	atom	k1gInSc1
·	·	k?
superatom	superatom	k1gInSc1
·	·	k?
exotické	exotický	k2eAgInPc1d1
atomy	atom	k1gInPc1
(	(	kIx(
<g/>
pozitronium	pozitronium	k1gNnSc1
·	·	k?
mionium	mionium	k1gNnSc1
·	·	k?
onium	onium	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
molekula	molekula	k1gFnSc1
hypotetické	hypotetický	k2eAgFnPc1d1
</s>
<s>
mezony	mezon	k1gInPc1
<g/>
/	/	kIx~
<g/>
kvarkonia	kvarkonium	k1gNnPc1
</s>
<s>
mezon	mezon	k1gInSc1
théta	théta	k1gNnSc2
·	·	k?
mezon	mezon	k1gInSc1
T	T	kA
exotické	exotický	k2eAgInPc1d1
mezony	mezon	k1gInPc1
</s>
<s>
kvark-antikvark-gluonové	kvark-antikvark-gluonové	k2eAgNnPc7d1
kompozity	kompozitum	k1gNnPc7
ostatní	ostatní	k2eAgMnPc4d1
</s>
<s>
dikvarky	dikvarka	k1gFnPc1
·	·	k?
leptokvarky	leptokvarka	k1gFnSc2
·	·	k?
pomeron	pomeron	k1gMnSc1
</s>
<s>
Kvazičástice	Kvazičástika	k1gFnSc3
</s>
<s>
Davydovův	Davydovův	k2eAgInSc1d1
soliton	soliton	k1gInSc1
·	·	k?
dropleton	dropleton	k1gInSc1
·	·	k?
elektronová	elektronový	k2eAgFnSc1d1
díra	díra	k1gFnSc1
·	·	k?
exciton	exciton	k1gInSc1
·	·	k?
fonon	fonon	k1gMnSc1
·	·	k?
magnon	magnon	k1gMnSc1
·	·	k?
plazmaron	plazmaron	k1gMnSc1
·	·	k?
plazmon	plazmon	k1gMnSc1
·	·	k?
polariton	polariton	k1gInSc1
·	·	k?
polaron	polaron	k1gInSc1
·	·	k?
roton	roton	k1gInSc1
·	·	k?
skyrmion	skyrmion	k1gInSc1
·	·	k?
trion	trion	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4039972-2	4039972-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
3603	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85086597	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85086597	#num#	k4
</s>
