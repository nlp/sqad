<s>
Klausova	Klausův	k2eAgFnSc1d1	Klausova
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kandidatura	kandidatura	k1gFnSc1	kandidatura
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
opakované	opakovaný	k2eAgFnSc2d1	opakovaná
volby	volba	k1gFnSc2	volba
zvolen	zvolit	k5eAaPmNgInS	zvolit
142	[number]	k4	142
hlasy	hlas	k1gInPc7	hlas
z	z	k7c2	z
281	[number]	k4	281
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
