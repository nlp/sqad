<s>
VII	VII	kA	VII
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
osmnáct	osmnáct	k4xCc4	osmnáct
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
