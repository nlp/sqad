<p>
<s>
VII	VII	kA	VII
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
osmnáct	osmnáct	k4xCc4	osmnáct
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
však	však	k9	však
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
na	na	k7c6	na
zimním	zimní	k2eAgInSc6d1	zimní
stadionu	stadion	k1gInSc6	stadion
Palais	Palais	k1gFnSc2	Palais
de	de	k?	de
Glace	Glace	k1gMnSc1	Glace
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
přezdívána	přezdívat	k5eAaImNgFnS	přezdívat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgInSc4d1	zimní
úvod	úvod	k1gInSc4	úvod
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
zorganizovaly	zorganizovat	k5eAaPmAgFnP	zorganizovat
hry	hra	k1gFnPc4	hra
vcelku	vcelku	k9	vcelku
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
i	i	k9	i
výborně	výborně	k6eAd1	výborně
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
přidělení	přidělení	k1gNnSc4	přidělení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
tomuto	tento	k3xDgNnSc3	tento
městu	město	k1gNnSc3	město
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
her	hra	k1gFnPc2	hra
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
olympijského	olympijský	k2eAgInSc2d1	olympijský
kongresu	kongres	k1gInSc2	kongres
konaného	konaný	k2eAgInSc2d1	konaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
programu	program	k1gInSc6	program
jen	jen	k9	jen
ty	ten	k3xDgInPc4	ten
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznával	uznávat	k5eAaImAgInS	uznávat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
předpis	předpis	k1gInSc1	předpis
se	se	k3xPyFc4	se
důsledně	důsledně	k6eAd1	důsledně
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
<g/>
,	,	kIx,	,
znamenal	znamenat	k5eAaImAgInS	znamenat
výrazný	výrazný	k2eAgInSc1d1	výrazný
pokrok	pokrok	k1gInSc1	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
udrželo	udržet	k5eAaPmAgNnS	udržet
přetahování	přetahování	k1gNnSc4	přetahování
lanem	lano	k1gNnSc7	lano
<g/>
,	,	kIx,	,
pólo	pólo	k1gNnSc1	pólo
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
a	a	k8xC	a
ragby	ragby	k1gNnSc7	ragby
union	union	k1gInSc1	union
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgNnSc1d1	významné
bylo	být	k5eAaImAgNnS	být
zařazení	zařazení	k1gNnSc1	zařazení
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
a	a	k8xC	a
znovuzařazení	znovuzařazení	k1gNnSc2	znovuzařazení
pozemního	pozemní	k2eAgInSc2d1	pozemní
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
významné	významný	k2eAgNnSc1d1	významné
bylo	být	k5eAaImAgNnS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
definitivním	definitivní	k2eAgNnSc6d1	definitivní
rozdělení	rozdělení	k1gNnSc6	rozdělení
zápasení	zápasení	k1gNnSc2	zápasení
na	na	k7c4	na
styl	styl	k1gInSc4	styl
řeckořímský	řeckořímský	k2eAgInSc4d1	řeckořímský
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
styl	styl	k1gInSc4	styl
-	-	kIx~	-
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
udělalo	udělat	k5eAaPmAgNnS	udělat
ze	z	k7c2	z
zápasu	zápas	k1gInSc2	zápas
řeckořímského	řeckořímský	k2eAgInSc2d1	řeckořímský
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstabilnějších	stabilní	k2eAgFnPc2d3	nejstabilnější
olympijských	olympijský	k2eAgFnPc2d1	olympijská
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
2692	[number]	k4	2692
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
150	[number]	k4	150
sportovců	sportovec	k1gMnPc2	sportovec
více	hodně	k6eAd2	hodně
než	než	k8xS	než
před	před	k7c7	před
osmi	osm	k4xCc7	osm
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedávno	nedávno	k6eAd1	nedávno
skončené	skončený	k2eAgFnSc3d1	skončená
válce	válka	k1gFnSc3	válka
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
překvapující	překvapující	k2eAgNnSc1d1	překvapující
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
startovali	startovat	k5eAaBmAgMnP	startovat
reprezentanti	reprezentant	k1gMnPc1	reprezentant
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
i	i	k9	i
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
přihlásil	přihlásit	k5eAaPmAgInS	přihlásit
Egypt	Egypt	k1gInSc1	Egypt
a	a	k8xC	a
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnPc4	Austrálie
a	a	k8xC	a
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
už	už	k6eAd1	už
startovali	startovat	k5eAaBmAgMnP	startovat
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
nastupovalo	nastupovat	k5eAaImAgNnS	nastupovat
pod	pod	k7c7	pod
vlajkou	vlajka	k1gFnSc7	vlajka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
výprava	výprava	k1gFnSc1	výprava
-	-	kIx~	-
Belgie	Belgie	k1gFnSc1	Belgie
-	-	kIx~	-
poslala	poslat	k5eAaPmAgFnS	poslat
na	na	k7c4	na
hry	hra	k1gFnPc4	hra
324	[number]	k4	324
sportovců	sportovec	k1gMnPc2	sportovec
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
neměla	mít	k5eNaImAgFnS	mít
domácí	domácí	k2eAgFnSc1d1	domácí
výprava	výprava	k1gFnSc1	výprava
na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
převahu	převah	k1gInSc2	převah
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
12,7	[number]	k4	12,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
sportovkyň	sportovkyně	k1gFnPc2	sportovkyně
byl	být	k5eAaImAgInS	být
64	[number]	k4	64
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžily	soutěžit	k5eAaImAgInP	soutěžit
ale	ale	k9	ale
jenom	jenom	k9	jenom
v	v	k7c6	v
plavání	plavání	k1gNnSc6	plavání
<g/>
,	,	kIx,	,
tenise	tenis	k1gInSc6	tenis
a	a	k8xC	a
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
měly	mít	k5eAaImAgFnP	mít
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
možnost	možnost	k1gFnSc4	možnost
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
se	se	k3xPyFc4	se
ve	v	k7c6	v
22	[number]	k4	22
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
159	[number]	k4	159
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
29	[number]	k4	29
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
atletických	atletický	k2eAgMnPc2d1	atletický
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
lehké	lehký	k2eAgFnSc6d1	lehká
atletice	atletika	k1gFnSc6	atletika
ještě	ještě	k6eAd1	ještě
nestartovaly	startovat	k5eNaBmAgFnP	startovat
<g/>
.	.	kIx.	.
</s>
<s>
Zrodilo	zrodit	k5eAaPmAgNnS	zrodit
se	se	k3xPyFc4	se
5	[number]	k4	5
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Finové	Fin	k1gMnPc1	Fin
získali	získat	k5eAaPmAgMnP	získat
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
jako	jako	k8xC	jako
USA	USA	kA	USA
-	-	kIx~	-
devět	devět	k4xCc4	devět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
byly	být	k5eAaImAgInP	být
architektonické	architektonický	k2eAgInPc1d1	architektonický
rozdíly	rozdíl	k1gInPc1	rozdíl
patrné	patrný	k2eAgInPc1d1	patrný
<g/>
,	,	kIx,	,
nedalo	dát	k5eNaPmAgNnS	dát
se	se	k3xPyFc4	se
nezpozorovat	zpozorovat	k5eNaPmF	zpozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
byl	být	k5eAaImAgInS	být
kopií	kopie	k1gFnSc7	kopie
Stockholmského	stockholmský	k2eAgInSc2d1	stockholmský
stadionu	stadion	k1gInSc2	stadion
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
modernějšímu	moderní	k2eAgInSc3d2	modernější
sportu	sport	k1gInSc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
plocha	plocha	k1gFnSc1	plocha
měla	mít	k5eAaImAgFnS	mít
rozměry	rozměr	k1gInPc7	rozměr
100	[number]	k4	100
m	m	kA	m
x	x	k?	x
65	[number]	k4	65
m	m	kA	m
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
běžecké	běžecký	k2eAgFnSc2d1	běžecká
dráhy	dráha	k1gFnSc2	dráha
byl	být	k5eAaImAgMnS	být
optimálních	optimální	k2eAgFnPc2d1	optimální
400	[number]	k4	400
m.	m.	k?	m.
Tyto	tento	k3xDgInPc4	tento
parametry	parametr	k1gInPc4	parametr
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
využívají	využívat	k5eAaImIp3nP	využívat
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
stadiónů	stadión	k1gInPc2	stadión
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
plocha	plocha	k1gFnSc1	plocha
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
hřiště	hřiště	k1gNnSc2	hřiště
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mění	měnit	k5eAaImIp3nS	měnit
rádius	rádius	k1gInSc4	rádius
zákrut	zákruta	k1gFnPc2	zákruta
běžecké	běžecký	k2eAgFnSc2d1	běžecká
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
chránily	chránit	k5eAaImAgInP	chránit
před	před	k7c7	před
větrem	vítr	k1gInSc7	vítr
nejen	nejen	k6eAd1	nejen
tribuny	tribuna	k1gFnSc2	tribuna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
domy	dům	k1gInPc1	dům
postavené	postavený	k2eAgInPc1d1	postavený
okolo	okolo	k6eAd1	okolo
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
VII	VII	kA	VII
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
před	před	k7c4	před
zraky	zrak	k1gInPc4	zrak
40	[number]	k4	40
000	[number]	k4	000
diváky	divák	k1gMnPc7	divák
a	a	k8xC	a
nastoupenými	nastoupený	k2eAgFnPc7d1	nastoupená
výpravami	výprava	k1gFnPc7	výprava
slavnostně	slavnostně	k6eAd1	slavnostně
otevřel	otevřít	k5eAaPmAgInS	otevřít
král	král	k1gMnSc1	král
Albert	Albert	k1gMnSc1	Albert
I.	I.	kA	I.
Belgický	belgický	k2eAgMnSc1d1	belgický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
projevu	projev	k1gInSc6	projev
zahřměly	zahřmět	k5eAaPmAgFnP	zahřmět
salvy	salva	k1gFnPc1	salva
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
k	k	k7c3	k
obloze	obloha	k1gFnSc3	obloha
vylétlo	vylétnout	k5eAaPmAgNnS	vylétnout
1	[number]	k4	1
000	[number]	k4	000
bílých	bílý	k2eAgFnPc2d1	bílá
holubic	holubice	k1gFnPc2	holubice
<g/>
.	.	kIx.	.
</s>
<s>
Zavládla	zavládnout	k5eAaPmAgFnS	zavládnout
mírová	mírový	k2eAgFnSc1d1	mírová
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
==	==	k?	==
</s>
</p>
<p>
<s>
Zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
začal	začít	k5eAaPmAgInS	začít
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
novinkou	novinka	k1gFnSc7	novinka
bylo	být	k5eAaImAgNnS	být
vztyčení	vztyčení	k1gNnSc1	vztyčení
sněhobílé	sněhobílý	k2eAgFnSc2d1	sněhobílá
olympijské	olympijský	k2eAgFnSc2d1	olympijská
zástavy	zástava	k1gFnSc2	zástava
s	s	k7c7	s
pěti	pět	k4xCc2	pět
kruhy	kruh	k1gInPc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlajky	vlajka	k1gFnPc1	vlajka
vlály	vlát	k5eAaImAgFnP	vlát
i	i	k9	i
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
vlajky	vlajka	k1gFnPc4	vlajka
přivlastnili	přivlastnit	k5eAaPmAgMnP	přivlastnit
zatkli	zatknout	k5eAaPmAgMnP	zatknout
zastupitelské	zastupitelský	k2eAgInPc4d1	zastupitelský
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgFnP	mít
vlajky	vlajka	k1gFnPc4	vlajka
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
novinkou	novinka	k1gFnSc7	novinka
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
olympijské	olympijský	k2eAgFnSc2d1	olympijská
přísahy	přísaha	k1gFnSc2	přísaha
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
především	především	k6eAd1	především
z	z	k7c2	z
případu	případ	k1gInSc2	případ
Jima	Jimus	k1gMnSc2	Jimus
Thorpa	Thorp	k1gMnSc2	Thorp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
ji	on	k3xPp3gFnSc4	on
přednesl	přednést	k5eAaPmAgMnS	přednést
populární	populární	k2eAgMnSc1d1	populární
belgický	belgický	k2eAgMnSc1d1	belgický
šermíř	šermíř	k1gMnSc1	šermíř
Victor	Victor	k1gMnSc1	Victor
Boin	Boin	k1gMnSc1	Boin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zahájení	zahájení	k1gNnSc4	zahájení
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
i	i	k9	i
skladatel	skladatel	k1gMnSc1	skladatel
Pierr	Pierr	k1gMnSc1	Pierr
Benoist	Benoist	k1gMnSc1	Benoist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
==	==	k?	==
</s>
</p>
<p>
<s>
Mělo	mít	k5eAaImAgNnS	mít
zde	zde	k6eAd1	zde
reprízu	repríza	k1gFnSc4	repríza
krasobruslení	krasobruslení	k1gNnSc2	krasobruslení
<g/>
,	,	kIx,	,
disciplína	disciplína	k1gFnSc1	disciplína
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
představila	představit	k5eAaPmAgFnS	představit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
dnech	den	k1gInPc6	den
jí	on	k3xPp3gFnSc2	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
častěji	často	k6eAd2	často
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
jako	jako	k8xC	jako
kanadský	kanadský	k2eAgInSc1d1	kanadský
hokej	hokej	k1gInSc1	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Kanadu	Kanada	k1gFnSc4	Kanada
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
družstvo	družstvo	k1gNnSc1	družstvo
Winnipeg	Winnipega	k1gFnPc2	Winnipega
Falcons	Falconsa	k1gFnPc2	Falconsa
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Allanova	Allanův	k2eAgInSc2d1	Allanův
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mužstvo	mužstvo	k1gNnSc1	mužstvo
deklasovalo	deklasovat	k5eAaBmAgNnS	deklasovat
naše	náš	k3xOp1gMnPc4	náš
hokejisty	hokejista	k1gMnPc4	hokejista
hned	hned	k6eAd1	hned
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
střetnutí	střetnutí	k1gNnSc6	střetnutí
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
údajně	údajně	k6eAd1	údajně
sehrála	sehrát	k5eAaPmAgFnS	sehrát
jejich	jejich	k3xOp3gFnSc1	jejich
lepší	dobrý	k2eAgFnSc1d2	lepší
kvalita	kvalita	k1gFnSc1	kvalita
hokejové	hokejový	k2eAgFnSc2d1	hokejová
výstroje	výstroj	k1gFnSc2	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
šesti	šest	k4xCc2	šest
mužstev	mužstvo	k1gNnPc2	mužstvo
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
Bergvallova	Bergvallův	k2eAgInSc2d1	Bergvallův
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
naši	náš	k3xOp1gFnSc4	náš
hokejisté	hokejista	k1gMnPc1	hokejista
prohráli	prohrát	k5eAaPmAgMnP	prohrát
s	s	k7c7	s
USA	USA	kA	USA
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
československého	československý	k2eAgNnSc2d1	Československé
mužstva	mužstvo	k1gNnSc2	mužstvo
nad	nad	k7c7	nad
Švédskem	Švédsko	k1gNnSc7	Švédsko
v	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
gólem	gól	k1gInSc7	gól
Josef	Josef	k1gMnSc1	Josef
Šroubek	Šroubek	k1gMnSc1	Šroubek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jediný	jediný	k2eAgInSc1d1	jediný
náš	náš	k3xOp1gInSc1	náš
gól	gól	k1gInSc1	gól
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
turnaji	turnaj	k1gInSc6	turnaj
znamenal	znamenat	k5eAaImAgInS	znamenat
ohromně	ohromně	k6eAd1	ohromně
cenný	cenný	k2eAgInSc4d1	cenný
olympijský	olympijský	k2eAgInSc4d1	olympijský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
našem	náš	k3xOp1gInSc6	náš
území	území	k1gNnSc6	území
začal	začít	k5eAaPmAgInS	začít
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
hrát	hrát	k5eAaImF	hrát
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Hokejisté	hokejista	k1gMnPc1	hokejista
Československa	Československo	k1gNnSc2	Československo
získali	získat	k5eAaPmAgMnP	získat
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
výpravu	výprava	k1gFnSc4	výprava
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
turnaj	turnaj	k1gInSc1	turnaj
==	==	k?	==
</s>
</p>
<p>
<s>
Naše	náš	k3xOp1gNnSc1	náš
mužstvo	mužstvo	k1gNnSc1	mužstvo
začalo	začít	k5eAaPmAgNnS	začít
velice	velice	k6eAd1	velice
slibně	slibně	k6eAd1	slibně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
výprava	výprava	k1gFnSc1	výprava
fotbalistů	fotbalista	k1gMnPc2	fotbalista
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
proti	proti	k7c3	proti
Norsku	Norsko	k1gNnSc3	Norsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
porazilo	porazit	k5eAaPmAgNnS	porazit
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
byla	být	k5eAaImAgFnS	být
naše	náš	k3xOp1gFnSc1	náš
výprava	výprava	k1gFnSc1	výprava
vylosována	vylosován	k2eAgFnSc1d1	vylosována
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
semifinálovém	semifinálový	k2eAgInSc6d1	semifinálový
zápase	zápas	k1gInSc6	zápas
Belgie	Belgie	k1gFnSc1	Belgie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
nad	nad	k7c7	nad
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
soupeřem	soupeř	k1gMnSc7	soupeř
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc4	zápas
pískal	pískat	k5eAaImAgMnS	pískat
anglický	anglický	k2eAgMnSc1d1	anglický
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
smutně	smutně	k6eAd1	smutně
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k9	jako
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
zápasu	zápas	k1gInSc2	zápas
ČSR	ČSR	kA	ČSR
-	-	kIx~	-
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
zaujatost	zaujatost	k1gFnSc4	zaujatost
ho	on	k3xPp3gMnSc4	on
pražské	pražský	k2eAgNnSc1d1	Pražské
obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
vypískalo	vypískat	k5eAaPmAgNnS	vypískat
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
uznal	uznat	k5eAaPmAgMnS	uznat
Belgii	Belgie	k1gFnSc6	Belgie
dva	dva	k4xCgInPc4	dva
neregulérní	regulérní	k2eNgInPc4d1	neregulérní
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
neoprávněně	oprávněně	k6eNd1	oprávněně
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
v	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
čs	čs	kA	čs
<g/>
.	.	kIx.	.
hráče	hráč	k1gMnSc2	hráč
Steinera	Steiner	k1gMnSc2	Steiner
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
naše	náš	k3xOp1gFnSc1	náš
výprava	výprava	k1gFnSc1	výprava
předčasně	předčasně	k6eAd1	předčasně
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
čase	čas	k1gInSc6	čas
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
hřiště	hřiště	k1gNnSc2	hřiště
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
počínání	počínání	k1gNnSc1	počínání
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
jistě	jistě	k6eAd1	jistě
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
omluvitelné	omluvitelný	k2eAgNnSc1d1	omluvitelné
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
totiž	totiž	k9	totiž
bylo	být	k5eAaImAgNnS	být
nezachování	nezachování	k1gNnSc3	nezachování
cti	čest	k1gFnSc2	čest
naší	náš	k3xOp1gFnSc2	náš
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
diskvalifikace	diskvalifikace	k1gFnSc2	diskvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Belgii	Belgie	k1gFnSc4	Belgie
potom	potom	k6eAd1	potom
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
za	za	k7c4	za
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
porazilo	porazit	k5eAaPmAgNnS	porazit
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
třetí	třetí	k4xOgFnSc4	třetí
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nešťastném	šťastný	k2eNgInSc6d1	nešťastný
závěru	závěr	k1gInSc6	závěr
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
turnaje	turnaj	k1gInSc2	turnaj
mělo	mít	k5eAaImAgNnS	mít
vinu	vina	k1gFnSc4	vina
i	i	k8xC	i
fanatické	fanatický	k2eAgNnSc4d1	fanatické
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zneuctilo	zneuctít	k5eAaPmAgNnS	zneuctít
vlajku	vlajka	k1gFnSc4	vlajka
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
se	se	k3xPyFc4	se
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
belgické	belgický	k2eAgFnSc2d1	belgická
vlády	vláda	k1gFnSc2	vláda
omluvil	omluvit	k5eAaPmAgMnS	omluvit
našemu	náš	k3xOp1gInSc3	náš
zastupitelskému	zastupitelský	k2eAgInSc3d1	zastupitelský
úřadu	úřad	k1gInSc3	úřad
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
fiasko	fiasko	k1gNnSc1	fiasko
neblaze	blaze	k6eNd1	blaze
zapůsobilo	zapůsobit	k5eAaPmAgNnS	zapůsobit
i	i	k9	i
na	na	k7c4	na
fotbal	fotbal	k1gInSc4	fotbal
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
trvalo	trvat	k5eAaImAgNnS	trvat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
sjednat	sjednat	k5eAaPmF	sjednat
morální	morální	k2eAgFnPc4d1	morální
nápravy	náprava	k1gFnPc4	náprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
stížností	stížnost	k1gFnPc2	stížnost
==	==	k?	==
</s>
</p>
<p>
<s>
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1920	[number]	k4	1920
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
neblaze	blaze	k6eNd1	blaze
poznamenaly	poznamenat	k5eAaPmAgInP	poznamenat
velké	velký	k2eAgInPc1d1	velký
počty	počet	k1gInPc1	počet
stížností	stížnost	k1gFnPc2	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznikala	vznikat	k5eAaImAgFnS	vznikat
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
s	s	k7c7	s
verdikty	verdikt	k1gInPc7	verdikt
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
vyrovnávají	vyrovnávat	k5eAaImIp3nP	vyrovnávat
účty	účet	k1gInPc4	účet
z	z	k7c2	z
minulých	minulý	k2eAgFnPc2d1	minulá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
projev	projev	k1gInSc4	projev
neseriózního	seriózní	k2eNgInSc2d1	neseriózní
postupu	postup	k1gInSc2	postup
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
považuje	považovat	k5eAaImIp3nS	považovat
zejména	zejména	k9	zejména
rozhodování	rozhodování	k1gNnSc1	rozhodování
Angličana	Angličan	k1gMnSc2	Angličan
Lewise	Lewise	k1gFnSc2	Lewise
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhodování	rozhodování	k1gNnSc6	rozhodování
byly	být	k5eAaImAgFnP	být
stížnosti	stížnost	k1gFnPc1	stížnost
i	i	k9	i
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
,	,	kIx,	,
zápasení	zápasení	k1gNnSc6	zápasení
a	a	k8xC	a
boxu	box	k1gInSc6	box
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
disciplínách	disciplína	k1gFnPc6	disciplína
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
vyloučením	vyloučení	k1gNnPc3	vyloučení
a	a	k8xC	a
diskvalifikacím	diskvalifikace	k1gFnPc3	diskvalifikace
účinkujících	účinkující	k1gFnPc2	účinkující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Celkové	celkový	k2eAgNnSc1d1	celkové
hodnocení	hodnocení	k1gNnSc1	hodnocení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nejlépe	dobře	k6eAd3	dobře
skončily	skončit	k5eAaPmAgFnP	skončit
USA	USA	kA	USA
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
obdiv	obdiv	k1gInSc1	obdiv
patřil	patřit	k5eAaImAgInS	patřit
Finům	Fin	k1gMnPc3	Fin
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc2	jejich
57	[number]	k4	57
sportovců	sportovec	k1gMnPc2	sportovec
získalo	získat	k5eAaPmAgNnS	získat
27	[number]	k4	27
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
15	[number]	k4	15
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
připadala	připadat	k5eAaImAgFnS	připadat
skoro	skoro	k6eAd1	skoro
jedna	jeden	k4xCgFnSc1	jeden
celá	celý	k2eAgFnSc1d1	celá
medaile	medaile	k1gFnSc1	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Belgičani	belgičan	k1gMnPc1	belgičan
vcelku	vcelku	k6eAd1	vcelku
dodrželi	dodržet	k5eAaPmAgMnP	dodržet
tradici	tradice	k1gFnSc4	tradice
spjatou	spjatý	k2eAgFnSc4d1	spjatá
s	s	k7c7	s
pořadatelskou	pořadatelský	k2eAgFnSc7d1	pořadatelská
zemí	zem	k1gFnSc7	zem
se	s	k7c7	s
40	[number]	k4	40
medailemi	medaile	k1gFnPc7	medaile
(	(	kIx(	(
<g/>
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
15	[number]	k4	15
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
obsadili	obsadit	k5eAaPmAgMnP	obsadit
celkově	celkově	k6eAd1	celkově
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
ale	ale	k9	ale
zrušil	zrušit	k5eAaPmAgInS	zrušit
bodovou	bodový	k2eAgFnSc4d1	bodová
klasifikaci	klasifikace	k1gFnSc4	klasifikace
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
1912	[number]	k4	1912
oficiální	oficiální	k2eAgFnSc1d1	oficiální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
úroveň	úroveň	k1gFnSc4	úroveň
nebo	nebo	k8xC	nebo
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kritika	kritika	k1gFnSc1	kritika
především	především	k9	především
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
novinářů	novinář	k1gMnPc2	novinář
nezúčastněních	nezúčastnění	k1gNnPc6	nezúčastnění
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
Antverpám	Antverpy	k1gFnPc3	Antverpy
byl	být	k5eAaImAgInS	být
přidělen	přidělen	k2eAgInSc1d1	přidělen
pořadatelský	pořadatelský	k2eAgInSc4d1	pořadatelský
status	status	k1gInSc4	status
jen	jen	k9	jen
rok	rok	k1gInSc4	rok
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
zahájením	zahájení	k1gNnSc7	zahájení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kandidátská	kandidátský	k2eAgNnPc1d1	kandidátské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
7	[number]	k4	7
<g/>
.	.	kIx.	.
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
ucházela	ucházet	k5eAaImAgFnS	ucházet
ještě	ještě	k9	ještě
města	město	k1gNnSc2	město
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
(	(	kIx(	(
<g/>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lyon	Lyon	k1gInSc1	Lyon
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
však	však	k8xC	však
ještě	ještě	k9	ještě
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
medailí	medaile	k1gFnPc2	medaile
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Československo	Československo	k1gNnSc1	Československo
na	na	k7c4	na
LOH	LOH	kA	LOH
1920	[number]	k4	1920
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1920	[number]	k4	1920
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.olympic.cz	www.olympic.cz	k1gMnSc1	www.olympic.cz
</s>
</p>
