<s>
Český	český	k2eAgInSc1d1	český
chmel	chmel	k1gInSc1	chmel
(	(	kIx(	(
<g/>
pěstovaný	pěstovaný	k2eAgMnSc1d1	pěstovaný
v	v	k7c6	v
Poohří	Poohří	k1gNnSc6	Poohří
(	(	kIx(	(
<g/>
Žatecko	Žatecko	k1gNnSc1	Žatecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polabí	Polabí	k1gNnSc1	Polabí
(	(	kIx(	(
<g/>
Úštěcko	Úštěcko	k1gNnSc1	Úštěcko
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
(	(	kIx(	(
<g/>
Tršicko	Tršicko	k1gNnSc1	Tršicko
<g/>
))	))	k?	))
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejkvalitnějším	kvalitní	k2eAgNnPc3d3	nejkvalitnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
občas	občas	k6eAd1	občas
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
zelené	zelený	k2eAgNnSc4d1	zelené
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
