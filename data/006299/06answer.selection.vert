<s>
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazýván	k2eAgFnSc1d1	nazývána
akce	akce	k1gFnSc1	akce
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
poblíž	poblíž	k7c2	poblíž
Kyjeva	Kyjev	k1gInSc2	Kyjev
postřílely	postřílet	k5eAaPmAgFnP	postřílet
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1941	[number]	k4	1941
přes	přes	k7c4	přes
33	[number]	k4	33
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Velké	velký	k2eAgFnSc2d1	velká
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
války	válka	k1gFnSc2	válka
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
dalších	další	k2eAgMnPc2d1	další
občanů	občan	k1gMnPc2	občan
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
