<s>
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazýván	k2eAgFnSc1d1	nazývána
akce	akce	k1gFnSc1	akce
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
poblíž	poblíž	k7c2	poblíž
Kyjeva	Kyjev	k1gInSc2	Kyjev
postřílely	postřílet	k5eAaPmAgFnP	postřílet
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1941	[number]	k4	1941
přes	přes	k7c4	přes
33	[number]	k4	33
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Velké	velký	k2eAgFnSc2d1	velká
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
války	válka	k1gFnSc2	válka
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
dalších	další	k2eAgMnPc2d1	další
občanů	občan	k1gMnPc2	občan
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
roli	role	k1gFnSc4	role
katů	kat	k1gMnPc2	kat
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
200	[number]	k4	200
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
jen	jen	k9	jen
300	[number]	k4	300
Němců	Němec	k1gMnPc2	Němec
<g/>
!	!	kIx.	!
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
Babyn	Babyn	k1gInSc1	Babyn
Jar	jar	k1gInSc1	jar
(	(	kIx(	(
<g/>
Б	Б	k?	Б
Я	Я	k?	Я
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Б	Б	k?	Б
Я	Я	k?	Я
Babij	Babij	k1gFnSc1	Babij
Jar	jar	k1gFnSc1	jar
<g/>
)	)	kIx)	)
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
roklin	roklina	k1gFnPc2	roklina
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
rokle	rokle	k1gFnSc1	rokle
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
asi	asi	k9	asi
2,5	[number]	k4	2,5
km	km	kA	km
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
hloubka	hloubka	k1gFnSc1	hloubka
asi	asi	k9	asi
50	[number]	k4	50
m.	m.	k?	m.
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
tekl	téct	k5eAaImAgInS	téct
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Babyn	Babyn	k1gNnSc1	Babyn
Jar	Jara	k1gFnPc2	Jara
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
kronikách	kronika	k1gFnPc6	kronika
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1401	[number]	k4	1401
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
tohoto	tento	k3xDgInSc2	tento
pozemku	pozemek	k1gInSc2	pozemek
(	(	kIx(	(
<g/>
od	od	k7c2	od
staré	starý	k2eAgFnSc2d1	stará
ženy	žena	k1gFnSc2	žena
-	-	kIx~	-
rusky	rusky	k6eAd1	rusky
baby	baby	k1gNnSc1	baby
<g/>
)	)	kIx)	)
dominikánskému	dominikánský	k2eAgInSc3d1	dominikánský
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
sloužila	sloužit	k5eAaImAgFnS	sloužit
roklina	roklina	k1gFnSc1	roklina
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vojenských	vojenský	k2eAgInPc2d1	vojenský
táborů	tábor	k1gInPc2	tábor
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgInPc2	dva
hřbitovů	hřbitov	k1gInPc2	hřbitov
-	-	kIx~	-
jednoho	jeden	k4xCgMnSc4	jeden
pravoslavného	pravoslavný	k2eAgNnSc2d1	pravoslavné
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
židovského	židovský	k2eAgInSc2d1	židovský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
září	zářit	k5eAaImIp3nS	zářit
1941	[number]	k4	1941
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Kyjeva	Kyjev	k1gInSc2	Kyjev
německými	německý	k2eAgMnPc7d1	německý
vojsky	vojsko	k1gNnPc7	vojsko
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
tato	tento	k3xDgFnSc1	tento
vyhláška	vyhláška	k1gFnSc1	vyhláška
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Židé	Žid	k1gMnPc1	Žid
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
dostaví	dostavit	k5eAaPmIp3nS	dostavit
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1941	[number]	k4	1941
v	v	k7c4	v
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
na	na	k7c4	na
roh	roh	k1gInSc4	roh
ulic	ulice	k1gFnPc2	ulice
Melnichovská	Melnichovský	k2eAgFnSc1d1	Melnichovský
a	a	k8xC	a
Dochturovská	Dochturovský	k2eAgFnSc1d1	Dochturovský
(	(	kIx(	(
<g/>
u	u	k7c2	u
hřbitovů	hřbitov	k1gInPc2	hřbitov
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
sebou	se	k3xPyFc7	se
si	se	k3xPyFc3	se
vezmou	vzít	k5eAaPmIp3nP	vzít
osobní	osobní	k2eAgInPc4d1	osobní
doklady	doklad	k1gInPc4	doklad
<g/>
,	,	kIx,	,
cennosti	cennost	k1gFnPc4	cennost
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
teplé	teplý	k2eAgNnSc4d1	teplé
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
atd.	atd.	kA	atd.
Všichni	všechen	k3xTgMnPc1	všechen
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neuposlechnou	uposlechnout	k5eNaPmIp3nP	uposlechnout
toto	tento	k3xDgNnSc4	tento
nařízení	nařízení	k1gNnSc4	nařízení
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
přistiženi	přistižen	k2eAgMnPc1d1	přistižen
kdekoli	kdekoli	k6eAd1	kdekoli
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
civilisté	civilista	k1gMnPc1	civilista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vniknou	vniknout	k5eAaPmIp3nP	vniknout
do	do	k7c2	do
budov	budova	k1gFnPc2	budova
evakuovaných	evakuovaný	k2eAgMnPc2d1	evakuovaný
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
odcizí	odcizit	k5eAaPmIp3nS	odcizit
jejich	jejich	k3xOp3gInSc4	jejich
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
začátek	začátek	k1gInSc1	začátek
předem	předem	k6eAd1	předem
připravené	připravený	k2eAgFnSc2d1	připravená
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
SS	SS	kA	SS
a	a	k8xC	a
policie	policie	k1gFnSc2	policie
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
Obergruppenführer	Obergruppenführer	k1gMnSc1	Obergruppenführer
Friedrich	Friedrich	k1gMnSc1	Friedrich
Jeckeln	Jeckeln	k1gMnSc1	Jeckeln
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
byli	být	k5eAaImAgMnP	být
následně	následně	k6eAd1	následně
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
odvedeni	odveden	k2eAgMnPc1d1	odveden
ze	z	k7c2	z
shromažďovacích	shromažďovací	k2eAgNnPc2d1	shromažďovací
míst	místo	k1gNnPc2	místo
Sonderkomandem	Sonderkomando	k1gNnSc7	Sonderkomando
4	[number]	k4	4
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
velel	velet	k5eAaImAgInS	velet
Standartenführer	Standartenführer	k1gInSc1	Standartenführer
Paul	Paul	k1gMnSc1	Paul
Blobel	Blobel	k1gMnSc1	Blobel
<g/>
,	,	kIx,	,
do	do	k7c2	do
rokle	rokle	k1gFnSc2	rokle
Babí	babit	k5eAaImIp3nS	babit
Jar	jar	k1gFnSc1	jar
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
3	[number]	k4	3
km	km	kA	km
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgInP	účastnit
i	i	k8xC	i
dva	dva	k4xCgInPc1	dva
oddíly	oddíl	k1gInPc1	oddíl
policejního	policejní	k2eAgInSc2d1	policejní
praporu	prapor	k1gInSc2	prapor
a	a	k8xC	a
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
milice	milice	k1gFnSc1	milice
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
jim	on	k3xPp3gInPc3	on
odebrány	odebrán	k2eAgFnPc1d1	odebrána
cenné	cenný	k2eAgFnPc1d1	cenná
věci	věc	k1gFnPc1	věc
a	a	k8xC	a
osobní	osobní	k2eAgInPc1d1	osobní
dokumenty	dokument	k1gInPc1	dokument
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
nařízeno	nařízen	k2eAgNnSc1d1	nařízeno
vysvléci	vysvléct	k5eAaPmF	vysvléct
se	se	k3xPyFc4	se
do	do	k7c2	do
naha	naho	k1gNnSc2	naho
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
do	do	k7c2	do
rokle	rokle	k1gFnSc2	rokle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
převzala	převzít	k5eAaPmAgFnS	převzít
Schutzpolizei	Schutzpolizee	k1gFnSc4	Schutzpolizee
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
i	i	k8xC	i
děti	dítě	k1gFnPc1	dítě
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
lehnout	lehnout	k5eAaPmF	lehnout
si	se	k3xPyFc3	se
obličejem	obličej	k1gInSc7	obličej
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
stříleni	střílet	k5eAaImNgMnP	střílet
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
museli	muset	k5eAaImAgMnP	muset
lehnout	lehnout	k5eAaPmF	lehnout
na	na	k7c4	na
mrtvoly	mrtvola	k1gFnPc4	mrtvola
a	a	k8xC	a
očekávat	očekávat	k5eAaImF	očekávat
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
ránu	rána	k1gFnSc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Hrůzná	hrůzný	k2eAgFnSc1d1	hrůzná
jatka	jatka	k1gFnSc1	jatka
trvala	trvat	k5eAaImAgFnS	trvat
2	[number]	k4	2
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
odeslána	odeslat	k5eAaPmNgFnS	odeslat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
33	[number]	k4	33
771	[number]	k4	771
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
z	z	k7c2	z
konce	konec	k1gInSc2	konec
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
provedení	provedení	k1gNnSc2	provedení
stala	stát	k5eAaPmAgFnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
vraždou	vražda	k1gFnSc7	vražda
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
tou	ten	k3xDgFnSc7	ten
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
sloužila	sloužit	k5eAaImAgFnS	sloužit
rokle	rokle	k1gFnSc1	rokle
Babí	babí	k2eAgFnSc4d1	babí
Jar	jar	k1gFnSc4	jar
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
hromadným	hromadný	k2eAgFnPc3d1	hromadná
vraždám	vražda	k1gFnPc3	vražda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
menšího	malý	k2eAgInSc2d2	menší
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
byly	být	k5eAaImAgInP	být
zavražděny	zavražděn	k2eAgInPc1d1	zavražděn
další	další	k2eAgInPc1d1	další
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zde	zde	k6eAd1	zde
vražděni	vražděn	k2eAgMnPc1d1	vražděn
též	též	k9	též
váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
a	a	k8xC	a
partyzáni	partyzán	k1gMnPc1	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
paradoxně	paradoxně	k6eAd1	paradoxně
zapomenout	zapomenout	k5eAaPmF	zapomenout
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
Stalinově	Stalinův	k2eAgFnSc3d1	Stalinova
antisemitské	antisemitský	k2eAgFnSc3d1	antisemitská
politice	politika	k1gFnSc3	politika
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
místo	místo	k1gNnSc4	místo
navštívil	navštívit	k5eAaPmAgMnS	navštívit
známý	známý	k2eAgMnSc1d1	známý
sovětský	sovětský	k2eAgMnSc1d1	sovětský
básník	básník	k1gMnSc1	básník
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Jevtušenko	Jevtušenka	k1gFnSc5	Jevtušenka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
básní	básnit	k5eAaImIp3nS	básnit
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
místu	místo	k1gNnSc3	místo
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
diskuzi	diskuze	k1gFnSc3	diskuze
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
složil	složit	k5eAaPmAgMnS	složit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
básně	báseň	k1gFnSc2	báseň
svou	svůj	k3xOyFgFnSc7	svůj
světoznámou	světoznámý	k2eAgFnSc7d1	světoznámá
13	[number]	k4	13
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
Dmitrij	Dmitrij	k1gFnPc2	Dmitrij
Šostakovič	Šostakovič	k1gMnSc1	Šostakovič
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
všem	všecek	k3xTgFnPc3	všecek
obětem	oběť	k1gFnPc3	oběť
rokle	rokle	k1gFnSc2	rokle
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
postaven	postavit	k5eAaPmNgInS	postavit
až	až	k9	až
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Babij	Babij	k1gFnSc1	Babij
Jar	jar	k1gFnSc4	jar
na	na	k7c4	na
holocaust	holocaust	k1gInSc4	holocaust
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
