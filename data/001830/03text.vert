<s>
Le	Le	k?	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
[	[	kIx(	[
<g/>
l	l	kA	l
korbyzje	korbyzj	k1gFnSc2	korbyzj
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Charles-Édouard	Charles-Édouard	k1gMnSc1	Charles-Édouard
Jeanneret	Jeanneret	k1gMnSc1	Jeanneret
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1887	[number]	k4	1887
La	la	k1gNnPc2	la
Chaux-de-Fonds	Chauxe-Fondsa	k1gFnPc2	Chaux-de-Fondsa
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1965	[number]	k4	1965
Cap	cap	k1gMnSc1	cap
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
původem	původ	k1gInSc7	původ
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
urbanista	urbanista	k1gMnSc1	urbanista
<g/>
,	,	kIx,	,
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
radikálním	radikální	k2eAgInSc7d1	radikální
dílem	díl	k1gInSc7	díl
i	i	k8xC	i
výtvarným	výtvarný	k2eAgInSc7d1	výtvarný
citem	cit	k1gInSc7	cit
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vývoj	vývoj	k1gInSc1	vývoj
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
architekta	architekt	k1gMnSc4	architekt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
Charles-Édouard	Charles-Édouard	k1gMnSc1	Charles-Édouard
Jeanneret	Jeanneret	k1gMnSc1	Jeanneret
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1887	[number]	k4	1887
v	v	k7c6	v
La	la	k1gNnSc6	la
Chaux-de-Fonds	Chauxe-Fondsa	k1gFnPc2	Chaux-de-Fondsa
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
rytectví	rytectví	k1gNnSc2	rytectví
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
École	École	k1gFnSc2	École
des	des	k1gNnSc1	des
Arts	Artsa	k1gFnPc2	Artsa
Décoratifs	Décoratifsa	k1gFnPc2	Décoratifsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozornost	pozornost	k1gFnSc1	pozornost
k	k	k7c3	k
architektuře	architektura	k1gFnSc3	architektura
obrátil	obrátit	k5eAaPmAgMnS	obrátit
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c4	v
La	la	k1gNnSc4	la
Chaux-de-Fonds	Chauxe-Fondsa	k1gFnPc2	Chaux-de-Fondsa
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
realizaci	realizace	k1gFnSc4	realizace
-	-	kIx~	-
dům	dům	k1gInSc4	dům
pro	pro	k7c4	pro
rytce	rytec	k1gMnSc4	rytec
Louse	Louse	k1gFnSc2	Louse
Falleta	Falleta	k1gFnSc1	Falleta
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
pojal	pojmout	k5eAaPmAgInS	pojmout
natolik	natolik	k6eAd1	natolik
tradičně	tradičně	k6eAd1	tradičně
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
ani	ani	k8xC	ani
nechtěl	chtít	k5eNaImAgMnS	chtít
hlásit	hlásit	k5eAaImF	hlásit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Získal	získat	k5eAaPmAgInS	získat
však	však	k9	však
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
přízeň	přízeň	k1gFnSc1	přízeň
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
současně	současně	k6eAd1	současně
další	další	k2eAgFnPc4d1	další
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
-	-	kIx~	-
1914	[number]	k4	1914
za	za	k7c4	za
vydělané	vydělaný	k2eAgInPc4d1	vydělaný
peníze	peníz	k1gInPc4	peníz
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Poznával	poznávat	k5eAaImAgMnS	poznávat
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
také	také	k9	také
podíval	podívat	k5eAaImAgMnS	podívat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
studoval	studovat	k5eAaImAgMnS	studovat
významné	významný	k2eAgFnPc4d1	významná
památky	památka	k1gFnPc4	památka
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
kreslí	kreslit	k5eAaImIp3nS	kreslit
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
architektonického	architektonický	k2eAgNnSc2d1	architektonické
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
skic	skica	k1gFnPc2	skica
z	z	k7c2	z
poznávací	poznávací	k2eAgFnSc2d1	poznávací
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
ateliéru	ateliér	k1gInSc2	ateliér
bratrů	bratr	k1gMnPc2	bratr
Perretových	Perretový	k2eAgMnPc2d1	Perretový
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
používáním	používání	k1gNnSc7	používání
tehdy	tehdy	k6eAd1	tehdy
progresivního	progresivní	k2eAgInSc2d1	progresivní
železobetonu	železobeton	k1gInSc2	železobeton
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgInS	pracovat
také	také	k9	také
u	u	k7c2	u
dalšího	další	k2eAgMnSc2d1	další
výrazného	výrazný	k2eAgMnSc2d1	výrazný
moderního	moderní	k2eAgMnSc2d1	moderní
architekta	architekt	k1gMnSc2	architekt
-	-	kIx~	-
Petra	Petr	k1gMnSc2	Petr
Behrense	Behrens	k1gMnSc2	Behrens
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poznal	poznat	k5eAaPmAgMnS	poznat
i	i	k8xC	i
Miese	Miese	k1gFnSc1	Miese
van	vana	k1gFnPc2	vana
de	de	k?	de
Rohe	Rohe	k1gFnPc2	Rohe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914-15	[number]	k4	1914-15
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pro	pro	k7c4	pro
válkou	válka	k1gFnSc7	válka
postižené	postižený	k2eAgMnPc4d1	postižený
obyvatele	obyvatel	k1gMnPc4	obyvatel
Flander	Flandry	k1gInPc2	Flandry
známý	známý	k2eAgInSc1d1	známý
projekt	projekt	k1gInSc1	projekt
Dom-Ino	Dom-Ino	k6eAd1	Dom-Ino
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
domus	domus	k1gMnSc1	domus
a	a	k8xC	a
innovatio	innovatio	k1gMnSc1	innovatio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zcela	zcela	k6eAd1	zcela
prostou	prostý	k2eAgFnSc4d1	prostá
skeletovou	skeletový	k2eAgFnSc4d1	skeletová
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
sami	sám	k3xTgMnPc1	sám
podle	podle	k7c2	podle
libosti	libost	k1gFnSc2	libost
dokončili	dokončit	k5eAaPmAgMnP	dokončit
stavbou	stavba	k1gFnSc7	stavba
nenosných	nosný	k2eNgFnPc2d1	nenosná
zdí	zeď	k1gFnPc2	zeď
a	a	k8xC	a
příček	příčka	k1gFnPc2	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
nakonec	nakonec	k6eAd1	nakonec
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
systém	systém	k1gInSc1	systém
Dom-Ina	Dom-Ino	k1gNnSc2	Dom-Ino
byl	být	k5eAaImAgInS	být
obrazem	obraz	k1gInSc7	obraz
nového	nový	k2eAgInSc2d1	nový
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
nazván	nazván	k2eAgInSc4d1	nazván
funkcionalismus	funkcionalismus	k1gInSc4	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
postavil	postavit	k5eAaPmAgMnS	postavit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
domů	dům	k1gInPc2	dům
se	s	k7c7	s
železobetonovou	železobetonový	k2eAgFnSc7d1	železobetonová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
-	-	kIx~	-
vilu	vila	k1gFnSc4	vila
továrníka	továrník	k1gMnSc2	továrník
Schwoba	Schwoba	k1gFnSc1	Schwoba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkal	potkat	k5eAaPmAgInS	potkat
malíře	malíř	k1gMnSc4	malíř
Amédée	Amédé	k1gMnSc4	Amédé
Ozenfanta	Ozenfant	k1gMnSc4	Ozenfant
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
zvolil	zvolit	k5eAaPmAgInS	zvolit
pseudonym	pseudonym	k1gInSc1	pseudonym
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
diskutovali	diskutovat	k5eAaImAgMnP	diskutovat
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
publikovali	publikovat	k5eAaBmAgMnP	publikovat
manifest	manifest	k1gInSc4	manifest
Aprés	Aprésa	k1gFnPc2	Aprésa
le	le	k?	le
cubisme	cubismus	k1gInSc5	cubismus
(	(	kIx(	(
<g/>
protikubistický	protikubistický	k2eAgInSc1d1	protikubistický
program	program	k1gInSc1	program
purismu	purismus	k1gInSc2	purismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
-	-	kIx~	-
1923	[number]	k4	1923
spolu	spolu	k6eAd1	spolu
vydávali	vydávat	k5eAaImAgMnP	vydávat
kulturní	kulturní	k2eAgInSc4d1	kulturní
měsíčník	měsíčník	k1gInSc4	měsíčník
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Esprit	esprit	k1gInSc1	esprit
nouveau	nouveaus	k1gInSc2	nouveaus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgMnSc6	který
popularizovali	popularizovat	k5eAaImAgMnP	popularizovat
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgNnSc1d1	moderní
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc1d1	nové
umění	umění	k1gNnSc1	umění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrancem	bratranec	k1gMnSc7	bratranec
<g/>
,	,	kIx,	,
známým	známý	k2eAgMnSc7d1	známý
konstruktérem	konstruktér	k1gMnSc7	konstruktér
železobetonových	železobetonový	k2eAgFnPc2d1	železobetonová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
Piérem	Piér	k1gMnSc7	Piér
Jeanneretem	Jeanneret	k1gMnSc7	Jeanneret
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
myšlenky	myšlenka	k1gFnPc1	myšlenka
Corbusierových	Corbusierův	k2eAgFnPc2d1	Corbusierova
teorií	teorie	k1gFnPc2	teorie
byly	být	k5eAaImAgInP	být
shrnuty	shrnout	k5eAaPmNgInP	shrnout
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Vers	Versa	k1gFnPc2	Versa
une	une	k?	une
architecture	architectur	k1gMnSc5	architectur
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
Za	za	k7c4	za
novou	nový	k2eAgFnSc4d1	nová
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Rezek	Rezek	k1gMnSc1	Rezek
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ozenfantem	Ozenfant	k1gInSc7	Ozenfant
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
krásu	krása	k1gFnSc4	krása
čistě	čistě	k6eAd1	čistě
účelových	účelový	k2eAgNnPc2d1	účelové
a	a	k8xC	a
racionálně	racionálně	k6eAd1	racionálně
řešených	řešený	k2eAgFnPc2d1	řešená
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
čistotu	čistota	k1gFnSc4	čistota
povrchu	povrch	k1gInSc2	povrch
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
na	na	k7c4	na
určující	určující	k2eAgInSc4d1	určující
význam	význam	k1gInSc4	význam
půdorysu	půdorys	k1gInSc2	půdorys
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
primární	primární	k2eAgFnPc4d1	primární
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgMnPc2	který
dle	dle	k7c2	dle
něho	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
slunce	slunce	k1gNnSc1	slunce
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
v	v	k7c4	v
krásu	krása	k1gFnSc4	krása
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Architekturu	architektura	k1gFnSc4	architektura
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
"	"	kIx"	"
<g/>
překračuje	překračovat	k5eAaImIp3nS	překračovat
otázky	otázka	k1gFnPc4	otázka
užitku	užitek	k1gInSc2	užitek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
mnohých	mnohé	k1gNnPc2	mnohé
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
architekturu	architektura	k1gFnSc4	architektura
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
pouhé	pouhý	k2eAgNnSc4d1	pouhé
naplnění	naplnění	k1gNnSc4	naplnění
své	svůj	k3xOyFgFnSc2	svůj
praktické	praktický	k2eAgFnSc2d1	praktická
funkce	funkce	k1gFnSc2	funkce
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Požaduje	požadovat	k5eAaImIp3nS	požadovat
funkci	funkce	k1gFnSc4	funkce
estetickou	estetický	k2eAgFnSc4d1	estetická
i	i	k8xC	i
praktickou	praktický	k2eAgFnSc4d1	praktická
<g/>
.	.	kIx.	.
</s>
<s>
Účelové	účelový	k2eAgNnSc1d1	účelové
řešení	řešení	k1gNnSc1	řešení
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
názorů	názor	k1gInPc2	názor
krásné	krásný	k2eAgFnPc1d1	krásná
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
stroj	stroj	k1gInSc4	stroj
na	na	k7c4	na
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
úvahy	úvaha	k1gFnPc4	úvaha
často	často	k6eAd1	často
dokládal	dokládat	k5eAaImAgMnS	dokládat
fotografiemi	fotografia	k1gFnPc7	fotografia
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
železničních	železniční	k2eAgInPc2d1	železniční
vozů	vůz	k1gInPc2	vůz
či	či	k8xC	či
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
přizván	přizvat	k5eAaPmNgMnS	přizvat
Miesem	Mies	k1gMnSc7	Mies
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Rohe	k1gFnSc1	Rohe
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
kolonie	kolonie	k1gFnSc2	kolonie
Weissenhof	Weissenhof	k1gInSc1	Weissenhof
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postavil	postavit	k5eAaPmAgMnS	postavit
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
publikoval	publikovat	k5eAaBmAgInS	publikovat
proslulých	proslulý	k2eAgInPc2d1	proslulý
Pět	pět	k4xCc4	pět
bodů	bod	k1gInPc2	bod
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
pět	pět	k4xCc1	pět
tezí	teze	k1gFnPc2	teze
prakticky	prakticky	k6eAd1	prakticky
shrnovalo	shrnovat	k5eAaImAgNnS	shrnovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
vymoženosti	vymoženost	k1gFnPc4	vymoženost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
techniky	technika	k1gFnSc2	technika
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sloupy	sloup	k1gInPc1	sloup
<g/>
:	:	kIx,	:
stavět	stavět	k5eAaImF	stavět
domy	dům	k1gInPc4	dům
na	na	k7c6	na
sloupech	sloup	k1gInPc6	sloup
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
přízemí	přízemí	k1gNnSc1	přízemí
pro	pro	k7c4	pro
zeleň	zeleň	k1gFnSc4	zeleň
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Střešní	střešní	k2eAgFnPc1d1	střešní
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
:	:	kIx,	:
technika	technika	k1gFnSc1	technika
plochých	plochý	k2eAgFnPc2d1	plochá
střech	střecha	k1gFnPc2	střecha
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dům	dům	k1gInSc1	dům
místu	místo	k1gNnSc3	místo
odebral	odebrat	k5eAaPmAgInS	odebrat
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
půdorys	půdorys	k1gInSc1	půdorys
<g/>
:	:	kIx,	:
sloupy	sloup	k1gInPc1	sloup
nesou	nést	k5eAaImIp3nP	nést
síly	síla	k1gFnPc4	síla
všech	všecek	k3xTgNnPc2	všecek
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
volné	volný	k2eAgNnSc4d1	volné
členění	členění	k1gNnSc4	členění
prostoru	prostor	k1gInSc2	prostor
nenosnými	nosný	k2eNgFnPc7d1	nenosná
příčkami	příčka	k1gFnPc7	příčka
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Pásová	pásový	k2eAgNnPc1d1	pásové
okna	okno	k1gNnPc1	okno
<g/>
:	:	kIx,	:
systém	systém	k1gInSc1	systém
sloupů	sloup	k1gInPc2	sloup
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vést	vést	k5eAaImF	vést
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
okna	okno	k1gNnPc4	okno
mezi	mezi	k7c4	mezi
sloupy	sloup	k1gInPc4	sloup
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgNnSc1d1	volné
průčelí	průčelí	k1gNnSc1	průčelí
<g/>
:	:	kIx,	:
konzolovitě	konzolovitě	k6eAd1	konzolovitě
vyvedené	vyvedený	k2eAgInPc1d1	vyvedený
stropy	strop	k1gInPc1	strop
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
průčelí	průčelí	k1gNnSc4	průčelí
pro	pro	k7c4	pro
naprosto	naprosto	k6eAd1	naprosto
volné	volný	k2eAgNnSc4d1	volné
řešení	řešení	k1gNnSc4	řešení
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
průčelí	průčelí	k1gNnPc2	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
měst	město	k1gNnPc2	město
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Urbanisme	urbanismus	k1gInSc5	urbanismus
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
volal	volat	k5eAaImAgMnS	volat
po	po	k7c6	po
jasnosti	jasnost	k1gFnSc6	jasnost
a	a	k8xC	a
řádu	řád	k1gInSc6	řád
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zakřivená	zakřivený	k2eAgFnSc1d1	zakřivená
ulice	ulice	k1gFnSc1	ulice
je	být	k5eAaImIp3nS	být
cestou	cesta	k1gFnSc7	cesta
osla	osel	k1gMnSc2	osel
<g/>
,	,	kIx,	,
ne	ne	k9	ne
člověka	člověk	k1gMnSc4	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihoval	vyzdvihovat	k5eAaImAgMnS	vyzdvihovat
rovný	rovný	k2eAgInSc4d1	rovný
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
zvyšování	zvyšování	k1gNnSc4	zvyšování
hustoty	hustota	k1gFnSc2	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
vysokých	vysoký	k2eAgInPc2d1	vysoký
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
dopravu	doprava	k1gFnSc4	doprava
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
knize	kniha	k1gFnSc6	kniha
Ville	Ville	k1gFnSc2	Ville
radieuse	radieuse	k1gFnSc2	radieuse
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
popisoval	popisovat	k5eAaImAgMnS	popisovat
svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
"	"	kIx"	"
<g/>
zářícího	zářící	k2eAgNnSc2d1	zářící
města	město	k1gNnSc2	město
<g/>
"	"	kIx"	"
požadoval	požadovat	k5eAaImAgMnS	požadovat
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
obyvatele	obyvatel	k1gMnPc4	obyvatel
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
viditelnost	viditelnost	k1gFnSc1	viditelnost
oblohy	obloha	k1gFnSc2	obloha
a	a	k8xC	a
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bytě	byt	k1gInSc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
teoretik	teoretik	k1gMnSc1	teoretik
měl	mít	k5eAaImAgMnS	mít
Le	Le	k1gFnPc4	Le
Corbusier	Corbusiero	k1gNnPc2	Corbusiero
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
konferencí	konference	k1gFnPc2	konference
CIAM	CIAM	kA	CIAM
a	a	k8xC	a
na	na	k7c4	na
formulaci	formulace	k1gFnSc4	formulace
Athénské	athénský	k2eAgFnSc2d1	Athénská
charty	charta	k1gFnSc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
lze	lze	k6eAd1	lze
u	u	k7c2	u
Le	Le	k1gFnSc2	Le
Corbusiera	Corbusier	k1gMnSc4	Corbusier
nalézt	nalézt	k5eAaPmF	nalézt
výrazný	výrazný	k2eAgInSc4d1	výrazný
sociální	sociální	k2eAgInSc4d1	sociální
aspekt	aspekt	k1gInSc4	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
prefabrikaci	prefabrikace	k1gFnSc3	prefabrikace
a	a	k8xC	a
sériové	sériový	k2eAgFnSc3d1	sériová
výrobě	výroba	k1gFnSc3	výroba
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
u	u	k7c2	u
projektu	projekt	k1gInSc2	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
Citrohan	Citrohan	k1gMnSc1	Citrohan
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
jeho	jeho	k3xOp3gFnSc1	jeho
klasická	klasický	k2eAgFnSc1d1	klasická
koncepce	koncepce	k1gFnSc1	koncepce
bytu	byt	k1gInSc2	byt
<g/>
:	:	kIx,	:
přední	přední	k2eAgFnSc1d1	přední
vyšší	vysoký	k2eAgFnSc1d2	vyšší
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
,	,	kIx,	,
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
ložnici	ložnice	k1gFnSc4	ložnice
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
koncepci	koncepce	k1gFnSc4	koncepce
jako	jako	k8xC	jako
bytovou	bytový	k2eAgFnSc4d1	bytová
jednotku	jednotka	k1gFnSc4	jednotka
poprvé	poprvé	k6eAd1	poprvé
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
pavilón	pavilón	k1gInSc4	pavilón
Esprit	esprit	k1gInSc1	esprit
Nouveau	Nouveaus	k1gInSc2	Nouveaus
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
výstavě	výstava	k1gFnSc6	výstava
dekorativních	dekorativní	k2eAgNnPc2d1	dekorativní
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
realizace	realizace	k1gFnSc1	realizace
jeho	jeho	k3xOp3gFnSc2	jeho
myšlenky	myšlenka	k1gFnSc2	myšlenka
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
poválečného	poválečný	k2eAgInSc2d1	poválečný
nedostatku	nedostatek	k1gInSc2	nedostatek
bytů	byt	k1gInPc2	byt
jej	on	k3xPp3gInSc4	on
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
pověřila	pověřit	k5eAaPmAgFnS	pověřit
stavbou	stavba	k1gFnSc7	stavba
velkého	velký	k2eAgInSc2d1	velký
obytného	obytný	k2eAgInSc2d1	obytný
bloku	blok	k1gInSc2	blok
-	-	kIx~	-
Unité	unita	k1gMnPc1	unita
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
habitation	habitation	k1gInSc1	habitation
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
-	-	kIx~	-
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Unité	unita	k1gMnPc1	unita
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
habitation	habitation	k1gInSc1	habitation
(	(	kIx(	(
<g/>
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
představovala	představovat	k5eAaImAgFnS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
pozoruhodných	pozoruhodný	k2eAgFnPc2d1	pozoruhodná
staveb	stavba	k1gFnPc2	stavba
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jak	jak	k8xC	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sociálního	sociální	k2eAgInSc2d1	sociální
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Corbusier	Corbusier	k1gInSc1	Corbusier
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vložil	vložit	k5eAaPmAgMnS	vložit
337	[number]	k4	337
bytů	byt	k1gInPc2	byt
ve	v	k7c6	v
23	[number]	k4	23
rozmanitých	rozmanitý	k2eAgInPc6d1	rozmanitý
typech	typ	k1gInPc6	typ
-	-	kIx~	-
převážně	převážně	k6eAd1	převážně
dvoupodlažních	dvoupodlažní	k2eAgInPc2d1	dvoupodlažní
<g/>
.	.	kIx.	.
</s>
<s>
Sedmým	sedmý	k4xOgInSc7	sedmý
a	a	k8xC	a
osmým	osmý	k4xOgNnSc7	osmý
patrem	patro	k1gNnSc7	patro
probíhá	probíhat	k5eAaImIp3nS	probíhat
obchodní	obchodní	k2eAgFnSc1d1	obchodní
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
na	na	k7c6	na
střešní	střešní	k2eAgFnSc6d1	střešní
terase	terasa	k1gFnSc6	terasa
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
jesle	jesle	k1gFnPc1	jesle
<g/>
,	,	kIx,	,
školky	školka	k1gFnPc1	školka
<g/>
,	,	kIx,	,
tělocvična	tělocvična	k1gFnSc1	tělocvična
a	a	k8xC	a
bazén	bazén	k1gInSc1	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
střecha	střecha	k1gFnSc1	střecha
hrála	hrát	k5eAaImAgFnS	hrát
úlohu	úloha	k1gFnSc4	úloha
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
parníku	parník	k1gInSc3	parník
(	(	kIx(	(
<g/>
střecha	střecha	k1gFnSc1	střecha
pak	pak	k6eAd1	pak
k	k	k7c3	k
palubě	paluba	k1gFnSc3	paluba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typy	typ	k1gInPc1	typ
této	tento	k3xDgFnSc2	tento
budovy	budova	k1gFnSc2	budova
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
v	v	k7c6	v
okleštěné	okleštěný	k2eAgFnSc6d1	okleštěná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nantes	Nantes	k1gMnSc1	Nantes
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Meaux	Meaux	k1gInSc1	Meaux
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlíně	Berlín	k1gInSc6	Berlín
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Briey-en-Foret	Brieyn-Foret	k1gMnSc1	Briey-en-Foret
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c4	v
Firminy	Firmin	k1gInPc4	Firmin
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
dílem	díl	k1gInSc7	díl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vila	vila	k1gFnSc1	vila
Savoye	Savoy	k1gFnSc2	Savoy
v	v	k7c4	v
Poissy	Poissa	k1gFnPc4	Poissa
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1929	[number]	k4	1929
-	-	kIx~	-
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
důsledně	důsledně	k6eAd1	důsledně
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
svých	svůj	k3xOyFgInPc2	svůj
pět	pět	k4xCc4	pět
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Projevil	projevit	k5eAaPmAgMnS	projevit
zde	zde	k6eAd1	zde
náznaky	náznak	k1gInPc1	náznak
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
sochařského	sochařský	k2eAgNnSc2d1	sochařské
pojetí	pojetí	k1gNnSc2	pojetí
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
začal	začít	k5eAaPmAgMnS	začít
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
působit	působit	k5eAaImF	působit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
architektonické	architektonický	k2eAgFnSc6d1	architektonická
soutěži	soutěž	k1gFnSc6	soutěž
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
Palác	palác	k1gInSc4	palác
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vstřebal	vstřebat	k5eAaPmAgMnS	vstřebat
myšlenky	myšlenka	k1gFnPc4	myšlenka
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
promítly	promítnout	k5eAaPmAgInP	promítnout
i	i	k9	i
do	do	k7c2	do
jeho	jeho	k3xOp3gInPc2	jeho
projektů	projekt	k1gInPc2	projekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
budova	budova	k1gFnSc1	budova
Centrosajuzu	Centrosajuz	k1gInSc2	Centrosajuz
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
-	-	kIx~	-
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
velkorysostí	velkorysost	k1gFnSc7	velkorysost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
tvořil	tvořit	k5eAaImAgInS	tvořit
až	až	k6eAd1	až
do	do	k7c2	do
nástupu	nástup	k1gInSc2	nástup
historismu	historismus	k1gInSc2	historismus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
představil	představit	k5eAaPmAgInS	představit
projekt	projekt	k1gInSc4	projekt
Soudobého	soudobý	k2eAgNnSc2d1	soudobé
města	město	k1gNnSc2	město
pro	pro	k7c4	pro
tři	tři	k4xCgInPc4	tři
milióny	milión	k4xCgInPc4	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozprostíralo	rozprostírat	k5eAaImAgNnS	rozprostírat
na	na	k7c6	na
pravoúhlé	pravoúhlý	k2eAgFnSc6d1	pravoúhlá
síti	síť	k1gFnSc6	síť
a	a	k8xC	a
demonstrovalo	demonstrovat	k5eAaBmAgNnS	demonstrovat
všechny	všechen	k3xTgFnPc4	všechen
Corbusierovy	Corbusierův	k2eAgFnPc4d1	Corbusierova
urbanistické	urbanistický	k2eAgFnPc4d1	urbanistická
zásady	zásada	k1gFnPc4	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Une	Une	k1gFnSc2	Une
ville	ville	k6eAd1	ville
contemporaine	contemporainout	k5eAaPmIp3nS	contemporainout
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zní	znět	k5eAaImIp3nS	znět
jeho	on	k3xPp3gInSc4	on
originální	originální	k2eAgInSc4d1	originální
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
důsledném	důsledný	k2eAgNnSc6d1	důsledné
oddělení	oddělení	k1gNnSc6	oddělení
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
soustředění	soustředění	k1gNnSc6	soustředění
zástavby	zástavba	k1gFnSc2	zástavba
do	do	k7c2	do
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
ve	v	k7c6	v
volném	volný	k2eAgMnSc6d1	volný
<g/>
,	,	kIx,	,
vzdušném	vzdušný	k2eAgInSc6d1	vzdušný
rozvrhu	rozvrh	k1gInSc6	rozvrh
městského	městský	k2eAgInSc2d1	městský
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
představu	představa	k1gFnSc4	představa
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
na	na	k7c4	na
plán	plán	k1gInSc4	plán
důkladné	důkladný	k2eAgFnSc2d1	důkladná
přestavby	přestavba	k1gFnSc2	přestavba
Paříže	Paříž	k1gFnSc2	Paříž
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
urbanistických	urbanistický	k2eAgInPc2d1	urbanistický
plánů	plán	k1gInPc2	plán
<g/>
:	:	kIx,	:
1933	[number]	k4	1933
rozšíření	rozšíření	k1gNnPc2	rozšíření
Antverp	Antverpy	k1gFnPc2	Antverpy
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
"	"	kIx"	"
<g/>
Zářící	zářící	k2eAgNnSc1d1	zářící
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
plán	plán	k1gInSc1	plán
pro	pro	k7c4	pro
Rio	Rio	k1gFnPc4	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
a	a	k8xC	a
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
plán	plán	k1gInSc1	plán
pro	pro	k7c4	pro
Alžír	Alžír	k1gInSc4	Alžír
<g/>
.	.	kIx.	.
</s>
<s>
Realizován	realizovat	k5eAaBmNgInS	realizovat
nebyl	být	k5eNaImAgInS	být
ani	ani	k9	ani
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Corbusier	Corbusier	k1gMnSc1	Corbusier
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
také	také	k9	také
čtyři	čtyři	k4xCgInPc4	čtyři
návrhy	návrh	k1gInPc4	návrh
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
:	:	kIx,	:
urbanistický	urbanistický	k2eAgInSc4d1	urbanistický
rozvoj	rozvoj	k1gInSc4	rozvoj
Zlína	Zlín	k1gInSc2	Zlín
<g/>
,	,	kIx,	,
projekty	projekt	k1gInPc4	projekt
typových	typový	k2eAgFnPc2d1	typová
prodejen	prodejna	k1gFnPc2	prodejna
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
podnikového	podnikový	k2eAgNnSc2d1	podnikové
města	město	k1gNnSc2	město
Hellocourt	Hellocourta	k1gFnPc2	Hellocourta
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
pavilón	pavilón	k1gInSc4	pavilón
firmy	firma	k1gFnSc2	firma
pro	pro	k7c4	pro
Světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
návrhu	návrh	k1gInSc2	návrh
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgInSc4	žádný
uskutečněn	uskutečněn	k2eAgInSc4d1	uskutečněn
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
předválečných	předválečný	k2eAgFnPc6d1	předválečná
realizacích	realizace	k1gFnPc6	realizace
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
brutalismu	brutalismus	k1gInSc2	brutalismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
ještě	ještě	k6eAd1	ještě
zesílily	zesílit	k5eAaPmAgInP	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Corbusierovy	Corbusierův	k2eAgFnPc1d1	Corbusierova
stavby	stavba	k1gFnPc1	stavba
získaly	získat	k5eAaPmAgFnP	získat
na	na	k7c6	na
plastičnosti	plastičnost	k1gFnSc6	plastičnost
<g/>
,	,	kIx,	,
působily	působit	k5eAaImAgFnP	působit
více	hodně	k6eAd2	hodně
celistvě	celistvě	k6eAd1	celistvě
a	a	k8xC	a
sochařsky	sochařsky	k6eAd1	sochařsky
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
začal	začít	k5eAaPmAgInS	začít
představovat	představovat	k5eAaImF	představovat
ve	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
přirozeném	přirozený	k2eAgInSc6d1	přirozený
stavu	stav	k1gInSc6	stav
-	-	kIx~	-
ponechával	ponechávat	k5eAaImAgMnS	ponechávat
jej	on	k3xPp3gInSc4	on
neupravený	upravený	k2eNgInSc4d1	neupravený
a	a	k8xC	a
hrubý	hrubý	k2eAgInSc4d1	hrubý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
-	-	kIx~	-
1955	[number]	k4	1955
realizoval	realizovat	k5eAaBmAgInS	realizovat
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
skulpturální	skulpturální	k2eAgFnSc4d1	skulpturální
stavbu	stavba	k1gFnSc4	stavba
-	-	kIx~	-
poutní	poutní	k2eAgFnSc4d1	poutní
kapli	kaple	k1gFnSc4	kaple
Notre-Dame-du-Haut	Notre-Dameu-Haut	k2eAgMnSc1d1	Notre-Dame-du-Haut
(	(	kIx(	(
<g/>
Ronchamp	Ronchamp	k1gMnSc1	Ronchamp
<g/>
)	)	kIx)	)
u	u	k7c2	u
města	město	k1gNnSc2	město
Belfort	Belfort	k1gInSc1	Belfort
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Mariánskou	mariánský	k2eAgFnSc4d1	Mariánská
kapli	kaple	k1gFnSc4	kaple
v	v	k7c4	v
Ronchamp	Ronchamp	k1gInSc4	Ronchamp
modeloval	modelovat	k5eAaImAgMnS	modelovat
zcela	zcela	k6eAd1	zcela
volně	volně	k6eAd1	volně
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
se	se	k3xPyFc4	se
stavba	stavba	k1gFnSc1	stavba
zcela	zcela	k6eAd1	zcela
vymyká	vymykat	k5eAaImIp3nS	vymykat
architektově	architektův	k2eAgFnSc3d1	architektova
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
sakrální	sakrální	k2eAgFnSc7d1	sakrální
stavbou	stavba	k1gFnSc7	stavba
byla	být	k5eAaImAgFnS	být
zakázka	zakázka	k1gFnSc1	zakázka
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
kláštera	klášter	k1gInSc2	klášter
La	la	k1gNnSc2	la
Tourette	Tourett	k1gInSc5	Tourett
v	v	k7c4	v
Eveux	Eveux	k1gInSc4	Eveux
u	u	k7c2	u
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
,	,	kIx,	,
vybudovaného	vybudovaný	k2eAgInSc2d1	vybudovaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1957	[number]	k4	1957
-	-	kIx~	-
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Striktní	striktní	k2eAgInPc1d1	striktní
řeholní	řeholní	k2eAgInPc1d1	řeholní
předpisy	předpis	k1gInPc1	předpis
určily	určit	k5eAaPmAgInP	určit
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
však	však	k9	však
i	i	k9	i
sociální	sociální	k2eAgNnSc4d1	sociální
cítění	cítění	k1gNnSc4	cítění
a	a	k8xC	a
skromnost	skromnost	k1gFnSc4	skromnost
<g/>
.	.	kIx.	.
</s>
<s>
Příležitost	příležitost	k1gFnSc1	příležitost
realizovat	realizovat	k5eAaBmF	realizovat
své	svůj	k3xOyFgFnPc4	svůj
urbanistické	urbanistický	k2eAgFnPc4d1	urbanistická
koncepce	koncepce	k1gFnPc4	koncepce
dostal	dostat	k5eAaPmAgMnS	dostat
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
také	také	k9	také
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
plánování	plánování	k1gNnSc6	plánování
nového	nový	k2eAgNnSc2d1	nové
správního	správní	k2eAgNnSc2d1	správní
centra	centrum	k1gNnSc2	centrum
-	-	kIx~	-
Čandígarhu	Čandígarh	k1gInSc2	Čandígarh
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Corbusier	Corbusier	k1gInSc1	Corbusier
byl	být	k5eAaImAgMnS	být
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
architektem	architekt	k1gMnSc7	architekt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
architekturu	architektura	k1gFnSc4	architektura
a	a	k8xC	a
architekty	architekt	k1gMnPc4	architekt
takřka	takřka	k6eAd1	takřka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
vyspělém	vyspělý	k2eAgInSc6d1	vyspělý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
jeho	jeho	k3xOp3gInPc1	jeho
nápady	nápad	k1gInPc1	nápad
a	a	k8xC	a
principy	princip	k1gInPc1	princip
zobecněly	zobecnět	k5eAaPmAgInP	zobecnět
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
i	i	k9	i
v	v	k7c4	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
Le	Le	k1gFnSc1	Le
Corbusiera	Corbusiera	k1gFnSc1	Corbusiera
byla	být	k5eAaImAgFnS	být
osobitá	osobitý	k2eAgFnSc1d1	osobitá
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
těžko	těžko	k6eAd1	těžko
přímo	přímo	k6eAd1	přímo
přiřadit	přiřadit	k5eAaPmF	přiřadit
k	k	k7c3	k
některému	některý	k3yIgInSc3	některý
ze	z	k7c2	z
slohových	slohový	k2eAgNnPc2d1	slohové
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
a	a	k8xC	a
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
17	[number]	k4	17
jeho	jeho	k3xOp3gFnPc2	jeho
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
asi	asi	k9	asi
50	[number]	k4	50
let	léto	k1gNnPc2	léto
a	a	k8xC	a
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
sedmi	sedm	k4xCc2	sedm
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
osobností	osobnost	k1gFnPc2	osobnost
udržoval	udržovat	k5eAaImAgInS	udržovat
styky	styk	k1gInPc1	styk
především	především	k9	především
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Teigem	Teig	k1gMnSc7	Teig
<g/>
,	,	kIx,	,
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Fuchsem	Fuchs	k1gMnSc7	Fuchs
<g/>
,	,	kIx,	,
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Krejcarem	krejcar	k1gInSc7	krejcar
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Vaňkem	Vaněk	k1gMnSc7	Vaněk
<g/>
,	,	kIx,	,
Bedřichem	Bedřich	k1gMnSc7	Bedřich
Rozehnalem	Rozehnal	k1gMnSc7	Rozehnal
i	i	k8xC	i
Františkem	František	k1gMnSc7	František
L.	L.	kA	L.
Gahurou	Gahura	k1gFnSc7	Gahura
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
mu	on	k3xPp3gMnSc3	on
imponovala	imponovat	k5eAaImAgFnS	imponovat
osobnost	osobnost	k1gFnSc4	osobnost
Jana	Jan	k1gMnSc2	Jan
Antonína	Antonín	k1gMnSc2	Antonín
Bati	Baťa	k1gMnSc2	Baťa
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
měl	mít	k5eAaImAgMnS	mít
opravdu	opravdu	k6eAd1	opravdu
aktivní	aktivní	k2eAgInSc4d1	aktivní
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
rád	rád	k6eAd1	rád
by	by	kYmCp3nS	by
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
viděl	vidět	k5eAaImAgMnS	vidět
svého	svůj	k3xOyFgMnSc4	svůj
budoucího	budoucí	k2eAgMnSc4d1	budoucí
stavebníka	stavebník	k1gMnSc4	stavebník
(	(	kIx(	(
<g/>
investora	investor	k1gMnSc4	investor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
ateliéru	ateliér	k1gInSc6	ateliér
se	se	k3xPyFc4	se
také	také	k9	také
učili	učít	k5eAaPmAgMnP	učít
čeští	český	k2eAgMnPc1d1	český
architekti	architekt	k1gMnPc1	architekt
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Danda	Danda	k1gMnSc1	Danda
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
u	u	k7c2	u
Le	Le	k1gMnSc2	Le
Corbusiera	Corbusiero	k1gNnSc2	Corbusiero
uváděl	uvádět	k5eAaImAgMnS	uvádět
také	také	k9	také
Vladimír	Vladimír	k1gMnSc1	Vladimír
Karfík	Karfík	k1gMnSc1	Karfík
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
angažmá	angažmá	k1gNnSc4	angažmá
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ale	ale	k9	ale
nejasné	jasný	k2eNgNnSc1d1	nejasné
a	a	k8xC	a
nepotvrzené	potvrzený	k2eNgNnSc1d1	nepotvrzené
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stavby	stavba	k1gFnSc2	stavba
navržené	navržený	k2eAgFnSc2d1	navržená
Le	Le	k1gMnSc7	Le
Corbusierem	Corbusier	k1gMnSc7	Corbusier
<g/>
.	.	kIx.	.
1912	[number]	k4	1912
<g/>
:	:	kIx,	:
Vila	vila	k1gFnSc1	vila
Jeanneret-Perret	Jeanneret-Perret	k1gInSc1	Jeanneret-Perret
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Chaux-de-Fonds	Chauxe-Fondsa	k1gFnPc2	Chaux-de-Fondsa
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
1922	[number]	k4	1922
<g/>
:	:	kIx,	:
Vila	vila	k1gFnSc1	vila
pro	pro	k7c4	pro
Améde	Améd	k1gInSc5	Améd
Ozenfanta	Ozenfanta	k1gFnSc1	Ozenfanta
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1923	[number]	k4	1923
<g/>
:	:	kIx,	:
Vila	vít	k5eAaImAgFnS	vít
Raoula	Raoula	k1gFnSc1	Raoula
La	la	k1gNnSc2	la
Roche	Roche	k1gNnSc2	Roche
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g />
.	.	kIx.	.
</s>
<s>
1925	[number]	k4	1925
<g/>
:	:	kIx,	:
Dům	dům	k1gInSc1	dům
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgMnPc4d1	vlastní
rodiče	rodič	k1gMnPc4	rodič
u	u	k7c2	u
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
1926	[number]	k4	1926
<g/>
:	:	kIx,	:
La	la	k1gNnPc2	la
Cité	Cité	k1gNnSc7	Cité
Frugè	Frugè	k1gFnSc1	Frugè
<g/>
,	,	kIx,	,
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
:	:	kIx,	:
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
Pessacu	Pessaca	k1gFnSc4	Pessaca
u	u	k7c2	u
Bordeaux	Bordeaux	k1gNnSc2	Bordeaux
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
:	:	kIx,	:
Vila	vila	k1gFnSc1	vila
<g />
.	.	kIx.	.
</s>
<s>
Michaela	Michael	k1gMnSc4	Michael
Steina	Stein	k1gMnSc4	Stein
<g/>
,	,	kIx,	,
Garches	Garches	k1gMnSc1	Garches
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1927	[number]	k4	1927
<g/>
:	:	kIx,	:
Weissenhofsiedlung	Weissenhofsiedlung	k1gInSc1	Weissenhofsiedlung
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
:	:	kIx,	:
Palác	palác	k1gInSc1	palác
Centrosojuzu	Centrosojuz	k1gInSc2	Centrosojuz
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
1931	[number]	k4	1931
<g/>
:	:	kIx,	:
Vila	vít	k5eAaImAgFnS	vít
Savoye	Savoye	k1gFnSc1	Savoye
<g/>
,	,	kIx,	,
Poissy	Poiss	k1gInPc1	Poiss
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1935	[number]	k4	1935
<g/>
:	:	kIx,	:
Vila	vila	k1gFnSc1	vila
u	u	k7c2	u
Les	les	k1gInSc4	les
Mathes	Mathes	k1gMnSc1	Mathes
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1935	[number]	k4	1935
<g/>
:	:	kIx,	:
víkendový	víkendový	k2eAgInSc1d1	víkendový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Celle-	Celle-	k1gFnPc2	Celle-
<g/>
St.	st.	kA	st.
Cloud	Cloud	k1gInSc1	Cloud
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1934	[number]	k4	1934
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
:	:	kIx,	:
Letní	letní	k2eAgInSc1d1	letní
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Cloud	Cloud	k1gInSc4	Cloud
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
52	[number]	k4	52
<g/>
:	:	kIx,	:
Kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
dům	dům	k1gInSc1	dům
Unité	unita	k1gMnPc1	unita
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
habitation	habitation	k1gInSc1	habitation
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1952	[number]	k4	1952
<g/>
:	:	kIx,	:
Unité	unita	k1gMnPc1	unita
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
habitation	habitation	k1gInSc1	habitation
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
56	[number]	k4	56
<g/>
:	:	kIx,	:
budova	budova	k1gFnSc1	budova
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Čandígarh	Čandígarh	k1gInSc1	Čandígarh
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
1955	[number]	k4	1955
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
kaple	kaple	k1gFnSc1	kaple
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gNnPc7	Dame
du	du	k?	du
Haut	Haut	k1gMnSc1	Haut
(	(	kIx(	(
<g/>
Ronchamp	Ronchamp	k1gMnSc1	Ronchamp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ronchamp	Ronchamp	k1gInSc1	Ronchamp
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1960	[number]	k4	1960
<g/>
:	:	kIx,	:
Klášter	klášter	k1gInSc1	klášter
La	la	k0	la
Tourette	Tourett	k1gMnSc5	Tourett
<g/>
,	,	kIx,	,
Éveux-sur-l	Éveuxur	k1gMnSc1	Éveux-sur-l
<g/>
́	́	k?	́
<g/>
Arbresle	Arbresle	k1gMnSc1	Arbresle
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
:	:	kIx,	:
Carpenterovo	Carpenterův	k2eAgNnSc1d1	Carpenterovo
středisko	středisko	k1gNnSc1	středisko
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
university	universita	k1gFnSc2	universita
Harvard	Harvard	k1gInSc1	Harvard
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
1967	[number]	k4	1967
<g/>
:	:	kIx,	:
Heidi	Heid	k1gMnPc1	Heid
Weber	weber	k1gInSc1	weber
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
Centre	centr	k1gInSc5	centr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
1922	[number]	k4	1922
<g/>
:	:	kIx,	:
Výstavní	výstavní	k2eAgInSc1d1	výstavní
pavilon	pavilon	k1gInSc1	pavilon
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Esprit	esprit	k1gInSc1	esprit
Nouveau	Nouveaus	k1gInSc2	Nouveaus
1920	[number]	k4	1920
<g/>
:	:	kIx,	:
Návrh	návrh	k1gInSc1	návrh
domu	dům	k1gInSc2	dům
Citrohan	Citrohany	k1gInPc2	Citrohany
1922	[number]	k4	1922
<g/>
:	:	kIx,	:
Soudobé	soudobý	k2eAgNnSc1d1	soudobé
město	město	k1gNnSc1	město
pro	pro	k7c4	pro
tři	tři	k4xCgInPc4	tři
milióny	milión	k4xCgInPc4	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1927	[number]	k4	1927
<g/>
:	:	kIx,	:
Soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
palác	palác	k1gInSc4	palác
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
:	:	kIx,	:
Urbanistické	urbanistický	k2eAgInPc1d1	urbanistický
projekty	projekt	k1gInPc1	projekt
<g/>
:	:	kIx,	:
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
Obus	Obus	k1gInSc1	Obus
Alžír	Alžír	k1gInSc1	Alžír
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
plán	plán	k1gInSc1	plán
Buenos	Buenos	k1gInSc1	Buenos
Aires	Aires	k1gInSc1	Aires
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
:	:	kIx,	:
plán	plán	k1gInSc4	plán
městského	městský	k2eAgNnSc2d1	Městské
centra	centrum	k1gNnSc2	centrum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Saint	Saint	k1gInSc1	Saint
Dié	Dié	k1gFnSc1	Dié
1949	[number]	k4	1949
<g/>
:	:	kIx,	:
Návrh	návrh	k1gInSc4	návrh
rekreačního	rekreační	k2eAgNnSc2d1	rekreační
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
Roq	Roq	k1gFnPc2	Roq
et	et	k?	et
Rob	roba	k1gFnPc2	roba
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
:	:	kIx,	:
kompletní	kompletní	k2eAgInSc1d1	kompletní
plán	plán	k1gInSc1	plán
města	město	k1gNnSc2	město
a	a	k8xC	a
vládní	vládní	k2eAgFnSc2d1	vládní
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
Čandígarh	Čandígarh	k1gInSc1	Čandígarh
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
1923	[number]	k4	1923
<g/>
:	:	kIx,	:
Vers	Vers	k1gInSc1	Vers
une	une	k?	une
architecture	architectur	k1gMnSc5	architectur
1925	[number]	k4	1925
<g/>
:	:	kIx,	:
Urbanisme	urbanismus	k1gInSc5	urbanismus
1935	[number]	k4	1935
<g/>
:	:	kIx,	:
Ville	Villo	k1gNnSc6	Villo
radieuse	radieuse	k6eAd1	radieuse
1941	[number]	k4	1941
<g/>
:	:	kIx,	:
Destin	Destin	k1gMnSc1	Destin
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Sur	Sur	k1gMnSc1	Sur
les	les	k1gInSc4	les
4	[number]	k4	4
routes	routesa	k1gFnPc2	routesa
<g/>
.	.	kIx.	.
</s>
<s>
Terres	Terres	k1gMnSc1	Terres
<g/>
,	,	kIx,	,
air	air	k?	air
<g/>
,	,	kIx,	,
fer	fer	k?	fer
<g/>
,	,	kIx,	,
eau	eau	k?	eau
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Corbusier	Corbusier	k1gInSc1	Corbusier
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
novou	nový	k2eAgFnSc4d1	nová
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Rezek	Rezek	k1gMnSc1	Rezek
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
233	[number]	k4	233
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86027	[number]	k4	86027
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Corbusier	Corbusier	k1gInSc1	Corbusier
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
a	a	k8xC	a
potom	potom	k6eAd1	potom
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Arbor	Arbor	k1gInSc1	Arbor
vitae	vita	k1gInSc2	vita
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
s.	s.	k?	s.
příl	příl	k1gInSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
arte	arte	k1gFnSc1	arte
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86300	[number]	k4	86300
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
